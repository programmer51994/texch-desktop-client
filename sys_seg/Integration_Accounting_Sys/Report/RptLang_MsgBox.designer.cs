﻿namespace Integration_Accounting_Sys
{
    partial class RptLang_MsgBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.CboLang_Rpt = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Prt_Btn = new System.Windows.Forms.Button();
            this.Btn_Pdf = new System.Windows.Forms.Button();
            this.Btn_Excel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(118, 4);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(127, 19);
            this.label1.TabIndex = 190;
            this.label1.Text = "حــدد لغـــــة الطباعــة";
            // 
            // CboLang_Rpt
            // 
            this.CboLang_Rpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.CboLang_Rpt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboLang_Rpt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboLang_Rpt.FormattingEnabled = true;
            this.CboLang_Rpt.Items.AddRange(new object[] {
            "Arabic / عربي",
            "English / انكليزي"});
            this.CboLang_Rpt.Location = new System.Drawing.Point(64, 32);
            this.CboLang_Rpt.Name = "CboLang_Rpt";
            this.CboLang_Rpt.Size = new System.Drawing.Size(234, 24);
            this.CboLang_Rpt.TabIndex = 191;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-9, 102);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(380, 1);
            this.flowLayoutPanel1.TabIndex = 194;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(137, 106);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 193;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Prt_Btn
            // 
            this.Prt_Btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prt_Btn.ForeColor = System.Drawing.Color.Navy;
            this.Prt_Btn.Location = new System.Drawing.Point(22, 71);
            this.Prt_Btn.Name = "Prt_Btn";
            this.Prt_Btn.Size = new System.Drawing.Size(106, 26);
            this.Prt_Btn.TabIndex = 193;
            this.Prt_Btn.Text = "طباعة";
            this.Prt_Btn.UseVisualStyleBackColor = true;
            this.Prt_Btn.Click += new System.EventHandler(this.Prt_Btn_Click);
            // 
            // Btn_Pdf
            // 
            this.Btn_Pdf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Pdf.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Pdf.Location = new System.Drawing.Point(128, 71);
            this.Btn_Pdf.Name = "Btn_Pdf";
            this.Btn_Pdf.Size = new System.Drawing.Size(106, 26);
            this.Btn_Pdf.TabIndex = 195;
            this.Btn_Pdf.Text = "تصدير pdf";
            this.Btn_Pdf.UseVisualStyleBackColor = true;
            this.Btn_Pdf.Click += new System.EventHandler(this.Btn_Pdf_Click);
            // 
            // Btn_Excel
            // 
            this.Btn_Excel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Excel.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Excel.Location = new System.Drawing.Point(234, 71);
            this.Btn_Excel.Name = "Btn_Excel";
            this.Btn_Excel.Size = new System.Drawing.Size(106, 26);
            this.Btn_Excel.TabIndex = 196;
            this.Btn_Excel.Text = "تصدير Excel";
            this.Btn_Excel.UseVisualStyleBackColor = true;
            this.Btn_Excel.Click += new System.EventHandler(this.Btn_Excel_Click);
            // 
            // RptLang_MsgBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 134);
            this.ControlBox = false;
            this.Controls.Add(this.Btn_Excel);
            this.Controls.Add(this.Btn_Pdf);
            this.Controls.Add(this.Prt_Btn);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.CboLang_Rpt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RptLang_MsgBox";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "158";
            this.Text = "لغة الطباعة";
            this.Load += new System.EventHandler(this.RptLang_MsgBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboLang_Rpt;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button Prt_Btn;
        private System.Windows.Forms.Button Btn_Pdf;
        private System.Windows.Forms.Button Btn_Excel;
    }
}