﻿using System;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;

namespace Integration_Accounting_Sys
{
    public partial class RptLang_MsgBox : Form
    {
        #region MyRegion
        ReportClass ObjRpt = new ReportClass();
        ReportClass ObjRpteng = new ReportClass();
        DataTable Prm_Dt = new DataTable() ;
        DataTable[] _EXP_DT = { };
        DataGridView[] Export_GRD ;
        bool Mvisible_Flag = false;
        string TitleNameEXP = "";
        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RPT">اسم الريبورت العربي</param>
        /// <param name="RPTeng">اسم الريبورت الانكليزي</param>
        /// <param name="Print_Flag">تفعيل زر الطباعة</param>
        /// <param name="Pdf_Flag">تفعيل زر الpdf</param>
        /// <param name="Excel_Flag">تفعيل زر الاكسل</param>
        /// <param name="DT"> يستخدم ل</param>
        /// <param name="Grd">اسم الكرد المستخدم في الاكسل</param>
        /// <param name="EXP_DT">تستخدم لقراءة الداته مباشرة من datatable لاستخدامها في الاكسل</param>
        /// <param name="Visible_Flag"> طباعة الكرد كاملة مع الcolumn المخفية =True المستخدمة في الاكسل</param>
        public RptLang_MsgBox(ReportClass RPT, ReportClass RPTeng, bool Print_Flag = false, bool Pdf_Flag = false, bool Excel_Flag = false, DataTable  DT = null, DataGridView[] Grd = null, DataTable[] EXP_DT = null, bool Visible_Flag = false, bool isTerminal = false, String TitleName = "")
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Prt_Btn.Enabled = Print_Flag;
            Btn_Pdf.Enabled = Pdf_Flag;
            Btn_Excel.Enabled = Excel_Flag;
            Prm_Dt = DT;
            ObjRpt = RPT;
            ObjRpteng = RPTeng;
            Export_GRD = Grd;
            Mvisible_Flag = Visible_Flag;
            TitleNameEXP = TitleName;
            _EXP_DT = EXP_DT;
        }
        //-----------------------------------------------------
        private void RptLang_MsgBox_Load(object sender, EventArgs e)
        {
           string Sql_Text = "SELECT  AComp_Name,EComp_Name,logo FROM   Companies  where  comp_Id=" + connection.Comp_Id;
            connection.SqlExec(Sql_Text, "comp_name");
            try
            {
                ObjRpt.SetDataSource(connection.SQLDS);
                ObjRpteng.SetDataSource(connection.SQLDS);
            }
            catch { }
            if (Prm_Dt.Rows.Count > 0)
            {
                foreach (DataRow Dr in Prm_Dt.Rows)
                {
                    ObjRpt.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                    ObjRpteng.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                }
            }
            CboLang_Rpt.SelectedIndex = 0;
        }
        //-----------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            string[] Str = { "comp_name" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
            this.Close();
        }

        private void Prt_Btn_Click(object sender, EventArgs e)
        {
            if (CboLang_Rpt.SelectedIndex == 1)
            {
                ObjRpteng.PrintToPrinter(1, false, 0, 0);
            }
            else
            {
                ObjRpt.PrintToPrinter(1, false, 0, 0);
            }
            this.Close();
        }

        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
            string FileName = string.Empty;
            if (CboLang_Rpt.SelectedIndex == 1)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileName = saveFileDialog1.FileName + ".pdf";
                    ObjRpteng.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");
                    
                }
            }
            else
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileName = saveFileDialog1.FileName + ".pdf";
                   ObjRpt.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");
                }
            }
            System.Diagnostics.Process.Start(FileName);
            this.Close();
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, Mvisible_Flag, true, TitleNameEXP);
        }
    }
}
