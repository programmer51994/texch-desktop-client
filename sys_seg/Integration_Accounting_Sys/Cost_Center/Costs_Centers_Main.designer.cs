﻿namespace Integration_Accounting_Sys
{
    partial class Costs_Centers_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Costs_Centers_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnUpd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.label17 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.SearchBtn = new System.Windows.Forms.ToolStripLabel();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.Txt_ccSrch = new System.Windows.Forms.ToolStripTextBox();
            this.GRD_center = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel127 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel128 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel129 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel130 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel131 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel132 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel133 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel134 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel135 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel136 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel137 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel138 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel139 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel140 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel141 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel142 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel143 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel144 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel145 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel146 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel147 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel148 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel149 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel150 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel151 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel152 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel153 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel154 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel155 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel156 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel157 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel158 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel159 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel160 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel161 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel162 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel163 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel164 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel165 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel166 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel167 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel168 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel169 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel170 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel171 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel172 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel173 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel174 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel175 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel176 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel177 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel178 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel179 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel180 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel181 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel182 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel183 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel184 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel185 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel186 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel187 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel188 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel58 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel59 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel60 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel61 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel62 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel125 = new System.Windows.Forms.FlowLayoutPanel();
            this.label20 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDe = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel63 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GRD_center)).BeginInit();
            this.flowLayoutPanel127.SuspendLayout();
            this.flowLayoutPanel132.SuspendLayout();
            this.flowLayoutPanel137.SuspendLayout();
            this.flowLayoutPanel142.SuspendLayout();
            this.flowLayoutPanel147.SuspendLayout();
            this.flowLayoutPanel152.SuspendLayout();
            this.flowLayoutPanel157.SuspendLayout();
            this.flowLayoutPanel162.SuspendLayout();
            this.flowLayoutPanel167.SuspendLayout();
            this.flowLayoutPanel172.SuspendLayout();
            this.flowLayoutPanel177.SuspendLayout();
            this.flowLayoutPanel182.SuspendLayout();
            this.flowLayoutPanel187.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel16.SuspendLayout();
            this.flowLayoutPanel21.SuspendLayout();
            this.flowLayoutPanel26.SuspendLayout();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel36.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel46.SuspendLayout();
            this.flowLayoutPanel51.SuspendLayout();
            this.flowLayoutPanel56.SuspendLayout();
            this.flowLayoutPanel61.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(80, 27);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "اضافة جديد";
            this.BtnAdd.ToolTipText = "F9";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // BtnUpd
            // 
            this.BtnUpd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnUpd.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpd.Image")));
            this.BtnUpd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnUpd.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.BtnUpd.Name = "BtnUpd";
            this.BtnUpd.Size = new System.Drawing.Size(60, 27);
            this.BtnUpd.Tag = "3";
            this.BtnUpd.Text = "تعـديــل";
            this.BtnUpd.ToolTipText = "F7";
            this.BtnUpd.Click += new System.EventHandler(this.BtnUpd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // AllBtn
            // 
            this.AllBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(84, 27);
            this.AllBtn.Tag = "4";
            this.AllBtn.Text = "عرض الكـل";
            this.AllBtn.ToolTipText = "F4";
            this.AllBtn.Click += new System.EventHandler(this.AllBtn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // label17
            // 
            this.label17.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 27);
            this.label17.Text = "شجرة مركز الكلفة";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(70, 27);
            this.printToolStripButton.Text = "طبــــاعـة";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(154, 27);
            this.SearchBtn.Tag = "8";
            this.SearchBtn.Text = "بـحــث رقم/اسم مركز الكلفة";
            this.SearchBtn.ToolTipText = "F5";
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.BtnUpd,
            this.toolStripSeparator2,
            this.AllBtn,
            this.toolStripSeparator4,
            this.label17,
            this.toolStripSeparator1,
            this.printToolStripButton,
            this.toolStripSeparator7,
            this.SearchBtn,
            this.Txt_ccSrch});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(854, 30);
            this.BND.TabIndex = 2;
            this.BND.Text = "BND";
            // 
            // Txt_ccSrch
            // 
            this.Txt_ccSrch.Name = "Txt_ccSrch";
            this.Txt_ccSrch.Size = new System.Drawing.Size(140, 30);
            this.Txt_ccSrch.TextChanged += new System.EventHandler(this.Txt_ccSrch_TextChanged);
            // 
            // GRD_center
            // 
            this.GRD_center.AllowUserToAddRows = false;
            this.GRD_center.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GRD_center.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GRD_center.CausesValidation = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GRD_center.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GRD_center.ColumnHeadersHeight = 40;
            this.GRD_center.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column14,
            this.Column7});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GRD_center.DefaultCellStyle = dataGridViewCellStyle3;
            this.GRD_center.Location = new System.Drawing.Point(15, 33);
            this.GRD_center.Name = "GRD_center";
            this.GRD_center.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GRD_center.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GRD_center.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.GRD_center.Size = new System.Drawing.Size(828, 260);
            this.GRD_center.TabIndex = 685;
            this.GRD_center.SelectionChanged += new System.EventHandler(this.GRD_center_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "cc_Id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رقـــــم مركز الكلفة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "cc_AName";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "اسم مركز الكلفة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 250;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "cc_No";
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "رقم الدليل";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Ref_cc_No";
            this.Column5.Frozen = true;
            this.Column5.HeaderText = "المــــــرجع";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 120;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "RECORD_FLAG";
            this.Column14.Frozen = true;
            this.Column14.HeaderText = "يقبل الحركة";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Visible = false;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "USER_NAME";
            this.Column7.HeaderText = "المنظــــــم";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 225;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(16, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 14);
            this.label3.TabIndex = 686;
            this.label3.Text = "خصائص مركز الكلفة....";
            // 
            // flowLayoutPanel127
            // 
            this.flowLayoutPanel127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel128);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel129);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel130);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel131);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel132);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel142);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel157);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel187);
            this.flowLayoutPanel127.Location = new System.Drawing.Point(15, 362);
            this.flowLayoutPanel127.Name = "flowLayoutPanel127";
            this.flowLayoutPanel127.Size = new System.Drawing.Size(824, 1);
            this.flowLayoutPanel127.TabIndex = 687;
            // 
            // flowLayoutPanel128
            // 
            this.flowLayoutPanel128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel128.Location = new System.Drawing.Point(817, 3);
            this.flowLayoutPanel128.Name = "flowLayoutPanel128";
            this.flowLayoutPanel128.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel128.TabIndex = 634;
            // 
            // flowLayoutPanel129
            // 
            this.flowLayoutPanel129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel129.Location = new System.Drawing.Point(809, 3);
            this.flowLayoutPanel129.Name = "flowLayoutPanel129";
            this.flowLayoutPanel129.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel129.TabIndex = 633;
            // 
            // flowLayoutPanel130
            // 
            this.flowLayoutPanel130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel130.Location = new System.Drawing.Point(40, 3);
            this.flowLayoutPanel130.Name = "flowLayoutPanel130";
            this.flowLayoutPanel130.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel130.TabIndex = 632;
            // 
            // flowLayoutPanel131
            // 
            this.flowLayoutPanel131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel131.Location = new System.Drawing.Point(56, 241);
            this.flowLayoutPanel131.Name = "flowLayoutPanel131";
            this.flowLayoutPanel131.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel131.TabIndex = 631;
            // 
            // flowLayoutPanel132
            // 
            this.flowLayoutPanel132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel132.Controls.Add(this.flowLayoutPanel133);
            this.flowLayoutPanel132.Controls.Add(this.flowLayoutPanel134);
            this.flowLayoutPanel132.Controls.Add(this.flowLayoutPanel135);
            this.flowLayoutPanel132.Controls.Add(this.flowLayoutPanel136);
            this.flowLayoutPanel132.Controls.Add(this.flowLayoutPanel137);
            this.flowLayoutPanel132.Location = new System.Drawing.Point(211, 249);
            this.flowLayoutPanel132.Name = "flowLayoutPanel132";
            this.flowLayoutPanel132.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel132.TabIndex = 676;
            // 
            // flowLayoutPanel133
            // 
            this.flowLayoutPanel133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel133.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel133.Name = "flowLayoutPanel133";
            this.flowLayoutPanel133.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel133.TabIndex = 634;
            // 
            // flowLayoutPanel134
            // 
            this.flowLayoutPanel134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel134.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel134.Name = "flowLayoutPanel134";
            this.flowLayoutPanel134.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel134.TabIndex = 633;
            // 
            // flowLayoutPanel135
            // 
            this.flowLayoutPanel135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel135.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel135.Name = "flowLayoutPanel135";
            this.flowLayoutPanel135.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel135.TabIndex = 632;
            // 
            // flowLayoutPanel136
            // 
            this.flowLayoutPanel136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel136.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel136.Name = "flowLayoutPanel136";
            this.flowLayoutPanel136.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel136.TabIndex = 631;
            // 
            // flowLayoutPanel137
            // 
            this.flowLayoutPanel137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel138);
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel139);
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel140);
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel141);
            this.flowLayoutPanel137.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel137.Name = "flowLayoutPanel137";
            this.flowLayoutPanel137.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel137.TabIndex = 676;
            // 
            // flowLayoutPanel138
            // 
            this.flowLayoutPanel138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel138.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel138.Name = "flowLayoutPanel138";
            this.flowLayoutPanel138.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel138.TabIndex = 634;
            // 
            // flowLayoutPanel139
            // 
            this.flowLayoutPanel139.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel139.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel139.Name = "flowLayoutPanel139";
            this.flowLayoutPanel139.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel139.TabIndex = 633;
            // 
            // flowLayoutPanel140
            // 
            this.flowLayoutPanel140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel140.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel140.Name = "flowLayoutPanel140";
            this.flowLayoutPanel140.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel140.TabIndex = 632;
            // 
            // flowLayoutPanel141
            // 
            this.flowLayoutPanel141.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel141.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel141.Name = "flowLayoutPanel141";
            this.flowLayoutPanel141.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel141.TabIndex = 631;
            // 
            // flowLayoutPanel142
            // 
            this.flowLayoutPanel142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel142.Controls.Add(this.flowLayoutPanel143);
            this.flowLayoutPanel142.Controls.Add(this.flowLayoutPanel144);
            this.flowLayoutPanel142.Controls.Add(this.flowLayoutPanel145);
            this.flowLayoutPanel142.Controls.Add(this.flowLayoutPanel146);
            this.flowLayoutPanel142.Controls.Add(this.flowLayoutPanel147);
            this.flowLayoutPanel142.Location = new System.Drawing.Point(211, 256);
            this.flowLayoutPanel142.Name = "flowLayoutPanel142";
            this.flowLayoutPanel142.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel142.TabIndex = 677;
            // 
            // flowLayoutPanel143
            // 
            this.flowLayoutPanel143.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel143.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel143.Name = "flowLayoutPanel143";
            this.flowLayoutPanel143.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel143.TabIndex = 634;
            // 
            // flowLayoutPanel144
            // 
            this.flowLayoutPanel144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel144.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel144.Name = "flowLayoutPanel144";
            this.flowLayoutPanel144.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel144.TabIndex = 633;
            // 
            // flowLayoutPanel145
            // 
            this.flowLayoutPanel145.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel145.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel145.Name = "flowLayoutPanel145";
            this.flowLayoutPanel145.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel145.TabIndex = 632;
            // 
            // flowLayoutPanel146
            // 
            this.flowLayoutPanel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel146.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel146.Name = "flowLayoutPanel146";
            this.flowLayoutPanel146.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel146.TabIndex = 631;
            // 
            // flowLayoutPanel147
            // 
            this.flowLayoutPanel147.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel148);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel149);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel150);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel151);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel152);
            this.flowLayoutPanel147.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel147.Name = "flowLayoutPanel147";
            this.flowLayoutPanel147.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel147.TabIndex = 676;
            // 
            // flowLayoutPanel148
            // 
            this.flowLayoutPanel148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel148.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel148.Name = "flowLayoutPanel148";
            this.flowLayoutPanel148.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel148.TabIndex = 634;
            // 
            // flowLayoutPanel149
            // 
            this.flowLayoutPanel149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel149.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel149.Name = "flowLayoutPanel149";
            this.flowLayoutPanel149.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel149.TabIndex = 633;
            // 
            // flowLayoutPanel150
            // 
            this.flowLayoutPanel150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel150.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel150.Name = "flowLayoutPanel150";
            this.flowLayoutPanel150.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel150.TabIndex = 632;
            // 
            // flowLayoutPanel151
            // 
            this.flowLayoutPanel151.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel151.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel151.Name = "flowLayoutPanel151";
            this.flowLayoutPanel151.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel151.TabIndex = 631;
            // 
            // flowLayoutPanel152
            // 
            this.flowLayoutPanel152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel152.Controls.Add(this.flowLayoutPanel153);
            this.flowLayoutPanel152.Controls.Add(this.flowLayoutPanel154);
            this.flowLayoutPanel152.Controls.Add(this.flowLayoutPanel155);
            this.flowLayoutPanel152.Controls.Add(this.flowLayoutPanel156);
            this.flowLayoutPanel152.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel152.Name = "flowLayoutPanel152";
            this.flowLayoutPanel152.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel152.TabIndex = 676;
            // 
            // flowLayoutPanel153
            // 
            this.flowLayoutPanel153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel153.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel153.Name = "flowLayoutPanel153";
            this.flowLayoutPanel153.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel153.TabIndex = 634;
            // 
            // flowLayoutPanel154
            // 
            this.flowLayoutPanel154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel154.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel154.Name = "flowLayoutPanel154";
            this.flowLayoutPanel154.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel154.TabIndex = 633;
            // 
            // flowLayoutPanel155
            // 
            this.flowLayoutPanel155.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel155.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel155.Name = "flowLayoutPanel155";
            this.flowLayoutPanel155.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel155.TabIndex = 632;
            // 
            // flowLayoutPanel156
            // 
            this.flowLayoutPanel156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel156.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel156.Name = "flowLayoutPanel156";
            this.flowLayoutPanel156.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel156.TabIndex = 631;
            // 
            // flowLayoutPanel157
            // 
            this.flowLayoutPanel157.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel158);
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel159);
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel160);
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel161);
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel162);
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel172);
            this.flowLayoutPanel157.Location = new System.Drawing.Point(211, 263);
            this.flowLayoutPanel157.Name = "flowLayoutPanel157";
            this.flowLayoutPanel157.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel157.TabIndex = 678;
            // 
            // flowLayoutPanel158
            // 
            this.flowLayoutPanel158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel158.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel158.Name = "flowLayoutPanel158";
            this.flowLayoutPanel158.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel158.TabIndex = 634;
            // 
            // flowLayoutPanel159
            // 
            this.flowLayoutPanel159.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel159.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel159.Name = "flowLayoutPanel159";
            this.flowLayoutPanel159.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel159.TabIndex = 633;
            // 
            // flowLayoutPanel160
            // 
            this.flowLayoutPanel160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel160.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel160.Name = "flowLayoutPanel160";
            this.flowLayoutPanel160.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel160.TabIndex = 632;
            // 
            // flowLayoutPanel161
            // 
            this.flowLayoutPanel161.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel161.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel161.Name = "flowLayoutPanel161";
            this.flowLayoutPanel161.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel161.TabIndex = 631;
            // 
            // flowLayoutPanel162
            // 
            this.flowLayoutPanel162.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel162.Controls.Add(this.flowLayoutPanel163);
            this.flowLayoutPanel162.Controls.Add(this.flowLayoutPanel164);
            this.flowLayoutPanel162.Controls.Add(this.flowLayoutPanel165);
            this.flowLayoutPanel162.Controls.Add(this.flowLayoutPanel166);
            this.flowLayoutPanel162.Controls.Add(this.flowLayoutPanel167);
            this.flowLayoutPanel162.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel162.Name = "flowLayoutPanel162";
            this.flowLayoutPanel162.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel162.TabIndex = 676;
            // 
            // flowLayoutPanel163
            // 
            this.flowLayoutPanel163.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel163.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel163.Name = "flowLayoutPanel163";
            this.flowLayoutPanel163.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel163.TabIndex = 634;
            // 
            // flowLayoutPanel164
            // 
            this.flowLayoutPanel164.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel164.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel164.Name = "flowLayoutPanel164";
            this.flowLayoutPanel164.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel164.TabIndex = 633;
            // 
            // flowLayoutPanel165
            // 
            this.flowLayoutPanel165.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel165.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel165.Name = "flowLayoutPanel165";
            this.flowLayoutPanel165.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel165.TabIndex = 632;
            // 
            // flowLayoutPanel166
            // 
            this.flowLayoutPanel166.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel166.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel166.Name = "flowLayoutPanel166";
            this.flowLayoutPanel166.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel166.TabIndex = 631;
            // 
            // flowLayoutPanel167
            // 
            this.flowLayoutPanel167.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel167.Controls.Add(this.flowLayoutPanel168);
            this.flowLayoutPanel167.Controls.Add(this.flowLayoutPanel169);
            this.flowLayoutPanel167.Controls.Add(this.flowLayoutPanel170);
            this.flowLayoutPanel167.Controls.Add(this.flowLayoutPanel171);
            this.flowLayoutPanel167.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel167.Name = "flowLayoutPanel167";
            this.flowLayoutPanel167.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel167.TabIndex = 676;
            // 
            // flowLayoutPanel168
            // 
            this.flowLayoutPanel168.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel168.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel168.Name = "flowLayoutPanel168";
            this.flowLayoutPanel168.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel168.TabIndex = 634;
            // 
            // flowLayoutPanel169
            // 
            this.flowLayoutPanel169.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel169.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel169.Name = "flowLayoutPanel169";
            this.flowLayoutPanel169.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel169.TabIndex = 633;
            // 
            // flowLayoutPanel170
            // 
            this.flowLayoutPanel170.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel170.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel170.Name = "flowLayoutPanel170";
            this.flowLayoutPanel170.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel170.TabIndex = 632;
            // 
            // flowLayoutPanel171
            // 
            this.flowLayoutPanel171.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel171.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel171.Name = "flowLayoutPanel171";
            this.flowLayoutPanel171.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel171.TabIndex = 631;
            // 
            // flowLayoutPanel172
            // 
            this.flowLayoutPanel172.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel172.Controls.Add(this.flowLayoutPanel173);
            this.flowLayoutPanel172.Controls.Add(this.flowLayoutPanel174);
            this.flowLayoutPanel172.Controls.Add(this.flowLayoutPanel175);
            this.flowLayoutPanel172.Controls.Add(this.flowLayoutPanel176);
            this.flowLayoutPanel172.Controls.Add(this.flowLayoutPanel177);
            this.flowLayoutPanel172.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel172.Name = "flowLayoutPanel172";
            this.flowLayoutPanel172.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel172.TabIndex = 677;
            // 
            // flowLayoutPanel173
            // 
            this.flowLayoutPanel173.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel173.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel173.Name = "flowLayoutPanel173";
            this.flowLayoutPanel173.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel173.TabIndex = 634;
            // 
            // flowLayoutPanel174
            // 
            this.flowLayoutPanel174.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel174.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel174.Name = "flowLayoutPanel174";
            this.flowLayoutPanel174.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel174.TabIndex = 633;
            // 
            // flowLayoutPanel175
            // 
            this.flowLayoutPanel175.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel175.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel175.Name = "flowLayoutPanel175";
            this.flowLayoutPanel175.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel175.TabIndex = 632;
            // 
            // flowLayoutPanel176
            // 
            this.flowLayoutPanel176.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel176.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel176.Name = "flowLayoutPanel176";
            this.flowLayoutPanel176.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel176.TabIndex = 631;
            // 
            // flowLayoutPanel177
            // 
            this.flowLayoutPanel177.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel177.Controls.Add(this.flowLayoutPanel178);
            this.flowLayoutPanel177.Controls.Add(this.flowLayoutPanel179);
            this.flowLayoutPanel177.Controls.Add(this.flowLayoutPanel180);
            this.flowLayoutPanel177.Controls.Add(this.flowLayoutPanel181);
            this.flowLayoutPanel177.Controls.Add(this.flowLayoutPanel182);
            this.flowLayoutPanel177.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel177.Name = "flowLayoutPanel177";
            this.flowLayoutPanel177.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel177.TabIndex = 676;
            // 
            // flowLayoutPanel178
            // 
            this.flowLayoutPanel178.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel178.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel178.Name = "flowLayoutPanel178";
            this.flowLayoutPanel178.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel178.TabIndex = 634;
            // 
            // flowLayoutPanel179
            // 
            this.flowLayoutPanel179.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel179.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel179.Name = "flowLayoutPanel179";
            this.flowLayoutPanel179.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel179.TabIndex = 633;
            // 
            // flowLayoutPanel180
            // 
            this.flowLayoutPanel180.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel180.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel180.Name = "flowLayoutPanel180";
            this.flowLayoutPanel180.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel180.TabIndex = 632;
            // 
            // flowLayoutPanel181
            // 
            this.flowLayoutPanel181.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel181.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel181.Name = "flowLayoutPanel181";
            this.flowLayoutPanel181.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel181.TabIndex = 631;
            // 
            // flowLayoutPanel182
            // 
            this.flowLayoutPanel182.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel182.Controls.Add(this.flowLayoutPanel183);
            this.flowLayoutPanel182.Controls.Add(this.flowLayoutPanel184);
            this.flowLayoutPanel182.Controls.Add(this.flowLayoutPanel185);
            this.flowLayoutPanel182.Controls.Add(this.flowLayoutPanel186);
            this.flowLayoutPanel182.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel182.Name = "flowLayoutPanel182";
            this.flowLayoutPanel182.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel182.TabIndex = 676;
            // 
            // flowLayoutPanel183
            // 
            this.flowLayoutPanel183.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel183.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel183.Name = "flowLayoutPanel183";
            this.flowLayoutPanel183.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel183.TabIndex = 634;
            // 
            // flowLayoutPanel184
            // 
            this.flowLayoutPanel184.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel184.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel184.Name = "flowLayoutPanel184";
            this.flowLayoutPanel184.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel184.TabIndex = 633;
            // 
            // flowLayoutPanel185
            // 
            this.flowLayoutPanel185.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel185.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel185.Name = "flowLayoutPanel185";
            this.flowLayoutPanel185.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel185.TabIndex = 632;
            // 
            // flowLayoutPanel186
            // 
            this.flowLayoutPanel186.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel186.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel186.Name = "flowLayoutPanel186";
            this.flowLayoutPanel186.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel186.TabIndex = 631;
            // 
            // flowLayoutPanel187
            // 
            this.flowLayoutPanel187.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel187.Controls.Add(this.flowLayoutPanel188);
            this.flowLayoutPanel187.Location = new System.Drawing.Point(203, 263);
            this.flowLayoutPanel187.Name = "flowLayoutPanel187";
            this.flowLayoutPanel187.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel187.TabIndex = 679;
            // 
            // flowLayoutPanel188
            // 
            this.flowLayoutPanel188.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel188.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel188.Name = "flowLayoutPanel188";
            this.flowLayoutPanel188.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel188.TabIndex = 632;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel31);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel61);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(21, 318);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(820, 1);
            this.flowLayoutPanel1.TabIndex = 688;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(813, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel2.TabIndex = 634;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(805, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel3.TabIndex = 633;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(36, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel4.TabIndex = 632;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(52, 241);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel5.TabIndex = 631;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(207, 249);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel6.TabIndex = 676;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel7.TabIndex = 634;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel8.TabIndex = 633;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel9.TabIndex = 632;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel10.TabIndex = 631;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel11.TabIndex = 676;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel12.TabIndex = 634;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel13.TabIndex = 633;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel14.TabIndex = 632;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel15.TabIndex = 631;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel17);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel18);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel20);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(207, 256);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel16.TabIndex = 677;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel17.TabIndex = 634;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel18.TabIndex = 633;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel19.TabIndex = 632;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel20.TabIndex = 631;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel22);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel23);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel25);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel21.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel21.TabIndex = 676;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel22.TabIndex = 634;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel23.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel23.TabIndex = 633;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel24.TabIndex = 632;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel25.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel25.TabIndex = 631;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel28);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel30);
            this.flowLayoutPanel26.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel26.TabIndex = 676;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel27.TabIndex = 634;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel28.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel28.TabIndex = 633;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel29.TabIndex = 632;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel30.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel30.TabIndex = 631;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel31.Location = new System.Drawing.Point(207, 263);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel31.TabIndex = 678;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel32.TabIndex = 634;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel33.TabIndex = 633;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel34.TabIndex = 632;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel35.TabIndex = 631;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel36.TabIndex = 676;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel37.TabIndex = 634;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel38.TabIndex = 633;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel39.TabIndex = 632;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel40.TabIndex = 631;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel41.TabIndex = 676;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel42.TabIndex = 634;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel43.TabIndex = 633;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel44.TabIndex = 632;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel45.TabIndex = 631;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel49);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel46.TabIndex = 677;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel47.TabIndex = 634;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel48.TabIndex = 633;
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel49.TabIndex = 632;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel50.TabIndex = 631;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel51.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel51.TabIndex = 676;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel52.TabIndex = 634;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel53.TabIndex = 633;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel54.TabIndex = 632;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel55.TabIndex = 631;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel58);
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel59);
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel60);
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel56.TabIndex = 676;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel57.TabIndex = 634;
            // 
            // flowLayoutPanel58
            // 
            this.flowLayoutPanel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel58.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel58.Name = "flowLayoutPanel58";
            this.flowLayoutPanel58.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel58.TabIndex = 633;
            // 
            // flowLayoutPanel59
            // 
            this.flowLayoutPanel59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel59.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel59.Name = "flowLayoutPanel59";
            this.flowLayoutPanel59.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel59.TabIndex = 632;
            // 
            // flowLayoutPanel60
            // 
            this.flowLayoutPanel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel60.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel60.Name = "flowLayoutPanel60";
            this.flowLayoutPanel60.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel60.TabIndex = 631;
            // 
            // flowLayoutPanel61
            // 
            this.flowLayoutPanel61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel62);
            this.flowLayoutPanel61.Location = new System.Drawing.Point(199, 263);
            this.flowLayoutPanel61.Name = "flowLayoutPanel61";
            this.flowLayoutPanel61.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel61.TabIndex = 679;
            // 
            // flowLayoutPanel62
            // 
            this.flowLayoutPanel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel62.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel62.Name = "flowLayoutPanel62";
            this.flowLayoutPanel62.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel62.TabIndex = 632;
            // 
            // flowLayoutPanel125
            // 
            this.flowLayoutPanel125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel125.Location = new System.Drawing.Point(14, 321);
            this.flowLayoutPanel125.Name = "flowLayoutPanel125";
            this.flowLayoutPanel125.Size = new System.Drawing.Size(1, 41);
            this.flowLayoutPanel125.TabIndex = 689;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(22, 329);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 20);
            this.label20.TabIndex = 690;
            this.label20.Text = "يقبل الحركة";
            this.label20.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(18, 366);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 14);
            this.label2.TabIndex = 692;
            this.label2.Text = "التفاصـيل....";
            // 
            // txtDe
            // 
            this.txtDe.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtDe.Location = new System.Drawing.Point(12, 384);
            this.txtDe.Multiline = true;
            this.txtDe.Name = "txtDe";
            this.txtDe.Size = new System.Drawing.Size(830, 53);
            this.txtDe.TabIndex = 691;
            // 
            // flowLayoutPanel63
            // 
            this.flowLayoutPanel63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel63.Location = new System.Drawing.Point(840, 319);
            this.flowLayoutPanel63.Name = "flowLayoutPanel63";
            this.flowLayoutPanel63.Size = new System.Drawing.Size(1, 44);
            this.flowLayoutPanel63.TabIndex = 693;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 440);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(854, 22);
            this.statusStrip1.TabIndex = 694;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // Costs_Centers_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 462);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel63);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDe);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.flowLayoutPanel125);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel127);
            this.Controls.Add(this.GRD_center);
            this.Controls.Add(this.BND);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Costs_Centers_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "444";
            this.Text = "Costs_Centers_Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Costs_Centers_Main_FormClosed);
            this.Load += new System.EventHandler(this.Costs_Centers_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Costs_Centers_Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GRD_center)).EndInit();
            this.flowLayoutPanel127.ResumeLayout(false);
            this.flowLayoutPanel132.ResumeLayout(false);
            this.flowLayoutPanel137.ResumeLayout(false);
            this.flowLayoutPanel142.ResumeLayout(false);
            this.flowLayoutPanel147.ResumeLayout(false);
            this.flowLayoutPanel152.ResumeLayout(false);
            this.flowLayoutPanel157.ResumeLayout(false);
            this.flowLayoutPanel162.ResumeLayout(false);
            this.flowLayoutPanel167.ResumeLayout(false);
            this.flowLayoutPanel172.ResumeLayout(false);
            this.flowLayoutPanel177.ResumeLayout(false);
            this.flowLayoutPanel182.ResumeLayout(false);
            this.flowLayoutPanel187.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel16.ResumeLayout(false);
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel26.ResumeLayout(false);
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel36.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel46.ResumeLayout(false);
            this.flowLayoutPanel51.ResumeLayout(false);
            this.flowLayoutPanel56.ResumeLayout(false);
            this.flowLayoutPanel61.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BtnUpd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton label17;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel SearchBtn;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.DataGridView GRD_center;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel127;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel128;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel129;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel130;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel131;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel132;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel133;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel134;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel135;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel136;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel137;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel138;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel139;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel140;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel141;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel142;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel143;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel144;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel145;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel146;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel147;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel148;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel149;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel150;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel151;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel152;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel153;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel154;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel155;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel156;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel157;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel158;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel159;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel160;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel161;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel162;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel163;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel164;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel165;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel166;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel167;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel168;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel169;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel170;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel171;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel172;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel173;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel174;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel175;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel176;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel177;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel178;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel179;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel180;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel181;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel182;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel183;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel184;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel185;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel186;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel187;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel188;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel58;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel59;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel61;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel62;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel125;
        private System.Windows.Forms.CheckBox label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDe;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel63;
        private System.Windows.Forms.ToolStripTextBox Txt_ccSrch;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;

    }
}