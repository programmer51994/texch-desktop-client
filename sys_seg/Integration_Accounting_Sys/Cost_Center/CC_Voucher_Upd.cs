﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class CC_Voucher_Upd : Form
    {
        int Form_id = 0 ;
        DataTable DT_Acc = new DataTable();
        DataTable DT_CC_Cost = new DataTable();
        public CC_Voucher_Upd(int frm , DataTable _DT , DataTable _DT1)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            // connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grdacc_id.AutoGenerateColumns = false;
            Grdvo_no.AutoGenerateColumns = false;

            Grdcost_id.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grdacc_id.Columns["Column2"].DataPropertyName = "acc_ename";
                Grdvo_no.Columns["column2"].DataPropertyName = "cc_ename";/// تكتب على حسب اسماء ونوع الاعمدةgrd  
            }
            #endregion
            Form_id = frm ;
            DT_Acc = _DT;
            DT_CC_Cost = _DT1;
        }

        private void CC_Voucher_Read_Load(object sender, EventArgs e)
        {
           
             var Lst = (from row in DT_CC_Cost.AsEnumerable() select row["cc_id"]).ToList();
          String   strvalue = string.Join(",", Lst.ToArray().Select(o => o.ToString()).ToArray());
 
               var Lst1 = (from row in DT_CC_Cost.AsEnumerable() select row["R_T_Y_ID"]).ToList();
          String   strR_T_Y_ID = string.Join(",", Lst1.ToArray().Select(o => o.ToString()).ToArray());


          string Sql_Text = " select  a.CC_ID, CC_AName, CC_EName, Percentage , CC_Description AS Notes, 1 as chk ,R_T_Y_Id"
                + " from CC_voucher a , Costs_Centers b " 
                + " where a.CC_ID = b.cc_id "
                + " and a.CC_ID  in ("+ strvalue + ")"
                + " and a.R_T_Y_ID in ( "+ strR_T_Y_ID + ")"
                + " union   "
                + " select   a.CC_ID, CC_AName, CC_EName, 0 as Percentage , '' as Notes, 0 as chk , 0 as R_T_Y_Id "
                + " FROM   Costs_Centers a  " 
                + " where    a.CC_ID not in ("+ strvalue + ")"
                + " order by Percentage desc  ";
                  
            
            Grdacc_id.DataSource = connection.SQLBSLst;
            Grdvo_no.DataSource = DT_Acc;
           DT_CC_Cost = connection.SqlExec(Sql_Text, "CC_Cost_TBL");
            connection.SQLBSMainGrd.DataSource= DT_CC_Cost;
            Grdcost_id.DataSource = connection.SQLBSMainGrd;


        }

        private void Grdcost_id_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Grdcost_id.Columns["Column20"].Index)
            {
                Grdcost_id.EndEdit();  //Stop editing of cell.
                DT_CC_Cost.AcceptChanges();
                bool X = Convert.ToBoolean(DT_CC_Cost.Rows[Grdcost_id.CurrentRow.Index]["chk"]);
                if (X == false)
                {
                    DT_CC_Cost.Rows[Grdcost_id.CurrentRow.Index]["Percentage"] = 0.000;
                }
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            decimal sum_per = (from t in DT_CC_Cost.AsEnumerable()
                               select (Convert.ToDecimal(t["Percentage"]))).Sum();
            if (sum_per != 100)
            {
                MessageBox.Show("مجموع النسب لا يساوي 100", "رسالة  تحذيرية");
                return;
            }


            DataTable CC_R_T_Y_Id = DT_Acc.DefaultView.ToTable(false, "R_T_Y_Id", "chk")
                                     .Select("chk > 0").CopyToDataTable();

            DataTable CC_Voucher_data = DT_CC_Cost.DefaultView.ToTable(false, "cc_id", "Percentage", "NOTES")
                                    .Select("Percentage > 0").CopyToDataTable();
            connection.SQLCMD.Parameters.AddWithValue("@CC_R_T_Y_Id_data", CC_R_T_Y_Id);
            connection.SQLCMD.Parameters.AddWithValue("@CC_Voucher_data", CC_Voucher_data);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id", ((DataRowView)connection.SQLBSLst.Current).Row["Acc_Id"].ToString());
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Form_Id",Form_id);//تعديل
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_CC_Voucher", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
    }
}