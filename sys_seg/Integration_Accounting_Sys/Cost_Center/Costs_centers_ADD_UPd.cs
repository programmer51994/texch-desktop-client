﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Costs_centers_ADD_UPd : Form
    { 
        #region MyRegion
        string Sql_Text = "";
        string Cond_Str = "";
        string MCC_NO_Dot = "";
        int Mcc_Parent = 0;
        int Mform_flag = 0;
        int Mcc_id = 0;
        bool TxtChange_cc = false;
        string name = connection.Lang_id == 1 ? "a" : "e";
        #endregion 
        public Costs_centers_ADD_UPd(byte form_flag)
        {
            Mform_flag = form_flag;
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
           // connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);

            Grdcost_center.AutoGenerateColumns = false;
            
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grdcost_center.Columns["Column2"].DataPropertyName = "cc_ename";
            }
            #endregion
        }
        //--------------------------------------------------------------------------------
        private void Costs_centers_ADD_UPd_Load(object sender, EventArgs e)
        {
            if (Mform_flag == 1)
            {
                TxtChange_cc = true ;
                Txt_ccName_A.Text = "";
                Txt_ccName_E.Text = "";
                Txt_ADesc.Text = "";
                Txt_EDesc.Text = "";
                TxtRef_cc.Text = "";
                Txt_ccSrch.Text = "";
                label19.Checked = false;
            }

            else
            {
                this.Text = connection.Lang_id == 1 ? "تعديــل مركز الكلفة " : "Update cost center";
                GetData_update();
            }

            

        
            }
        //--------------------------------------------------------------------------------
        private void GetData_Add(string cc_Namex)
        {
            if (TxtChange_cc)
            {
                if (cc_Namex != "")
                {
                    int cc_id = 0;
                    if (int.TryParse(cc_Namex, out cc_id))
                    {
                        if (Mform_flag == 1)
                        {
                            Cond_Str = " where (cc_ID like '" + cc_id + "%'"
                                       + " or "
                                       + "  Ref_cc_No like '" + cc_id + "%')";
                        }
                        else
                        {
                            Cond_Str = " where (cc_ID like '" + cc_id + "'"
                                           + " or "
                                           + "  Ref_cc_No like '" + cc_id + "')";
                        }
                    }
                    else
                    {

                        Cond_Str = "  where cc_" + name + "Name like'" + cc_Namex + "%'"; 
                    }

                    Sql_Text = " select  A_Description,E_Description,cc_no,cc_ID,cc_" + name
                             + "Name, Ref_cc_No,cc_Level,Dot_cc_No ,pcc_id "
                             + " from Costs_Centers " + Cond_Str;

                    connection.SQLBSMainGrd.DataSource = connection.SqlExec(Sql_Text, "Costs_Centers_Tbl");
                    Grdcost_center.DataSource = connection.SQLBSMainGrd;
                }
                else
                {
                    connection.SQLBSMainGrd.DataSource = null;
                    Grdcost_center.DataSource = null;

                }
            }
        }
        //--------------------------------------------------------------------------------
        private void Txt_ccSrch_TextChanged(object sender, EventArgs e)
        {
            GetData_Add(Txt_ccSrch.Text.Trim());
        }
        //-----------------------------------------
        private void GetData_update()
        {
           // TxtChange = true;
            DataRowView Drv = connection.SQLBS.Current as DataRowView;
            DataRow Dr = Drv.Row;
            Txt_ccName_A.Text = Dr.Field<string>("cc_AName");
            Txt_ccName_E.Text = Dr.Field<string>("cc_EName");
            Txt_ADesc.Text = Dr.Field<string>("A_Description");
            Txt_EDesc.Text = Dr.Field<string>("E_Description");
            TxtRef_cc.Text = Dr.Field<string>("Ref_cc_No");         
            label19.Checked = Convert.ToBoolean(Dr.Field<byte>("RECORD_FLAG"));
            Txt_ccSrch.Text = Dr.Field<int>("pcc_Id").ToString();
            TxtChange_cc = true ;
            Txt_ccSrch_TextChanged(null, null);
            Mcc_id = Dr.Field<Int32>("cc_Id");
            Txt_ccSrch.Enabled = false;
        }
        //-----------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------------------------
        private void Txt_AccSrch_TextChanged(object sender, EventArgs e)
        {
            GetData_Add(Txt_ccSrch.Text.Trim());
        }
        //--------------------------------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (connection.SQLBSMainGrd.Count > 0)
            {
                DataRowView Drvname = connection.SQLBSMainGrd.Current as DataRowView;
                DataRow Drname = Drvname.Row;
                Mcc_Parent = Drname.Field<Int32>("cc_id");
                MCC_NO_Dot = Drname.Field<string>("Dot_cc_No");
            }
            else
            {
                Mcc_Parent = 0;
                MCC_NO_Dot = ""; 
            }

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Txt_ccName_A.Text.Trim());
            ItemList.Insert(1, Txt_ccName_E.Text.Trim());
            ItemList.Insert(2, Mcc_Parent);
            ItemList.Insert(3, TxtRef_cc.Text.Trim());
            ItemList.Insert(4, Txt_ADesc.Text.Trim());
            ItemList.Insert(5, Txt_EDesc.Text.Trim());
            ItemList.Insert(6, Convert.ToInt32(label19.Checked));
            ItemList.Insert(7, MCC_NO_Dot);
            ItemList.Insert(8, connection.user_id);
            ItemList.Insert(9, Mform_flag == 1 ? Mcc_id = 0 : Mcc_id);
            ItemList.Insert(10, Mform_flag);
            ItemList.Insert(11, "");
            connection.scalar("Add_Cost_Center", ItemList);
            MyGeneral_Lib.Copytocliptext("Add_Cost_Center", ItemList);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Mform_flag == 1)
            {                
                Costs_centers_ADD_UPd_Load(null, null);

            }
            else
            {
                this.Close();
            }
        }
        //--------------------------------------------------------------------------------
        private void ExtBtn_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Costs_centers_ADD_UPd_FormClosed(object sender, FormClosedEventArgs e)
        {
            TxtChange_cc = false;
        }

        
    }
}
