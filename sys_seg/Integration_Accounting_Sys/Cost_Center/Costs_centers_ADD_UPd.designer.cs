﻿namespace Integration_Accounting_Sys
{
    partial class Costs_centers_ADD_UPd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_ccName_A = new System.Windows.Forms.TextBox();
            this.Txt_ccName_E = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_ADesc = new System.Windows.Forms.TextBox();
            this.Txt_EDesc = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label19 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Grdcost_center = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Desc_per = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_ccSrch = new System.Windows.Forms.TextBox();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtRef_cc = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcost_center)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(2, 37);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(895, 2);
            this.flowLayoutPanel5.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 646;
            this.label1.Text = "المســــــــتـخدم:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_UsrName.Location = new System.Drawing.Point(103, 11);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.Size = new System.Drawing.Size(298, 23);
            this.Txt_UsrName.TabIndex = 643;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(588, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 645;
            this.label2.Text = "التـــــأريخ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(11, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.TabIndex = 688;
            this.label5.Text = "رقـم المرجــــــع:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(11, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 16);
            this.label4.TabIndex = 686;
            this.label4.Text = "الأسم الإنكليـزي:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(11, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 687;
            this.label3.Text = "الأســـم العربــي:";
            // 
            // Txt_ccName_A
            // 
            this.Txt_ccName_A.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ccName_A.Location = new System.Drawing.Point(103, 55);
            this.Txt_ccName_A.Name = "Txt_ccName_A";
            this.Txt_ccName_A.Size = new System.Drawing.Size(298, 23);
            this.Txt_ccName_A.TabIndex = 684;
            // 
            // Txt_ccName_E
            // 
            this.Txt_ccName_E.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ccName_E.Location = new System.Drawing.Point(103, 94);
            this.Txt_ccName_E.Name = "Txt_ccName_E";
            this.Txt_ccName_E.Size = new System.Drawing.Size(298, 23);
            this.Txt_ccName_E.TabIndex = 685;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(11, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 16);
            this.label8.TabIndex = 693;
            this.label8.Text = "وصف مركز الكلف بالعربي:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_ADesc
            // 
            this.Txt_ADesc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ADesc.Location = new System.Drawing.Point(7, 184);
            this.Txt_ADesc.Multiline = true;
            this.Txt_ADesc.Name = "Txt_ADesc";
            this.Txt_ADesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_ADesc.Size = new System.Drawing.Size(389, 52);
            this.Txt_ADesc.TabIndex = 690;
            // 
            // Txt_EDesc
            // 
            this.Txt_EDesc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EDesc.Location = new System.Drawing.Point(7, 254);
            this.Txt_EDesc.Multiline = true;
            this.Txt_EDesc.Name = "Txt_EDesc";
            this.Txt_EDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_EDesc.Size = new System.Drawing.Size(389, 52);
            this.Txt_EDesc.TabIndex = 691;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(9, 236);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 16);
            this.label9.TabIndex = 692;
            this.label9.Text = "وصف مركز الكلف بالانكليزي";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(408, 321);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1, 62);
            this.flowLayoutPanel9.TabIndex = 695;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(6, 320);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(402, 1);
            this.flowLayoutPanel8.TabIndex = 697;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(5, 383);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(404, 1);
            this.flowLayoutPanel7.TabIndex = 696;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(13, 342);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 20);
            this.label19.TabIndex = 694;
            this.label19.Text = "يقبل الحركة";
            this.label19.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.DarkRed;
            this.label10.Location = new System.Drawing.Point(5, 308);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 16);
            this.label10.TabIndex = 698;
            this.label10.Text = "خصائص مركز الكلف ....";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(5, 323);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1, 61);
            this.flowLayoutPanel10.TabIndex = 699;
            // 
            // ExtBtn
            // 
            this.ExtBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(453, 401);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 704;
            this.ExtBtn.Text = "انهاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click_1);
            // 
            // Grdcost_center
            // 
            this.Grdcost_center.AllowUserToAddRows = false;
            this.Grdcost_center.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcost_center.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdcost_center.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grdcost_center.ColumnHeadersHeight = 24;
            this.Grdcost_center.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2});
            this.Grdcost_center.Location = new System.Drawing.Point(448, 77);
            this.Grdcost_center.Name = "Grdcost_center";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcost_center.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grdcost_center.Size = new System.Drawing.Size(446, 207);
            this.Grdcost_center.TabIndex = 700;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "cc_id";
            this.Column3.HeaderText = "رقـم مركز الكلف";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Ref_cc_No";
            this.Column1.HeaderText = "رقم المرجع";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "cc_aname";
            this.Column2.HeaderText = "اسم مركز الكلف";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(3, 394);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(897, 2);
            this.flowLayoutPanel6.TabIndex = 705;
            // 
            // Txt_Desc_per
            // 
            this.Txt_Desc_per.BackColor = System.Drawing.Color.White;
            this.Txt_Desc_per.Enabled = false;
            this.Txt_Desc_per.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Desc_per.Location = new System.Drawing.Point(448, 321);
            this.Txt_Desc_per.Multiline = true;
            this.Txt_Desc_per.Name = "Txt_Desc_per";
            this.Txt_Desc_per.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_Desc_per.Size = new System.Drawing.Size(449, 63);
            this.Txt_Desc_per.TabIndex = 702;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.DarkRed;
            this.label11.Location = new System.Drawing.Point(445, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 16);
            this.label11.TabIndex = 706;
            this.label11.Text = "مــــتفــــــرع  من  :";
            // 
            // Txt_ccSrch
            // 
            this.Txt_ccSrch.BackColor = System.Drawing.Color.White;
            this.Txt_ccSrch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Txt_ccSrch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ccSrch.Location = new System.Drawing.Point(542, 53);
            this.Txt_ccSrch.Name = "Txt_ccSrch";
            this.Txt_ccSrch.Size = new System.Drawing.Size(352, 23);
            this.Txt_ccSrch.TabIndex = 701;
            this.Txt_ccSrch.TextChanged += new System.EventHandler(this.Txt_AccSrch_TextChanged);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(363, 401);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(89, 26);
            this.Btn_Add.TabIndex = 703;
            this.Btn_Add.Text = "موافـق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(445, 302);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 16);
            this.label12.TabIndex = 707;
            this.label12.Text = "وصف مركز الكلف المتفرع منه:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(897, 37);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(2, 257);
            this.flowLayoutPanel4.TabIndex = 708;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(431, 293);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(467, 2);
            this.flowLayoutPanel1.TabIndex = 710;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(429, 39);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(2, 256);
            this.flowLayoutPanel3.TabIndex = 709;
            // 
            // TxtRef_cc
            // 
            this.TxtRef_cc.AllowSpace = false;
            this.TxtRef_cc.Location = new System.Drawing.Point(103, 133);
            this.TxtRef_cc.Name = "TxtRef_cc";
            this.TxtRef_cc.Size = new System.Drawing.Size(298, 20);
            this.TxtRef_cc.TabIndex = 689;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(655, 12);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(243, 20);
            this.TxtIn_Rec_Date.TabIndex = 644;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Costs_centers_ADD_UPd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 432);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.Grdcost_center);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.Txt_Desc_per);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_ccSrch);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txt_ADesc);
            this.Controls.Add(this.Txt_EDesc);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtRef_cc);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_ccName_A);
            this.Controls.Add(this.Txt_ccName_E);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_UsrName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Costs_centers_ADD_UPd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "445";
            this.Text = "Costs_centers_ADD_UPd";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Costs_centers_ADD_UPd_FormClosed);
            this.Load += new System.EventHandler(this.Costs_centers_ADD_UPd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grdcost_center)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.Label label2;
        private NumericTextBox TxtRef_cc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_ccName_A;
        private System.Windows.Forms.TextBox Txt_ccName_E;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_ADesc;
        private System.Windows.Forms.TextBox Txt_EDesc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.CheckBox label19;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.DataGridView Grdcost_center;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.TextBox Txt_Desc_per;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_ccSrch;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}