﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Costs_Centers_Main : Form
    {
        bool GrdChange = false;
        public Costs_Centers_Main()
        {
            InitializeComponent();
            connection.SQLBS.DataSource = new BindingSource();
            GRD_center.AutoGenerateColumns = false;

            MyGeneral_Lib.Form_Orientation(this);
            //connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id != 1)
            {
                GRD_center.Columns["column2"].DataPropertyName = "cc_EName";
            }

           
        }
        //------------------------------------------------------------------------------------
        private void Costs_Centers_Main_Load(object sender, EventArgs e)
        {
            Txt_ccSrch.Text = "";
            Txt_ccSrch_TextChanged(null, null);
        }
        //------------------------------------------------------------------------------------
        private void Costs_Centers_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    BtnUpd_Click(sender, e);
                    break;
                case Keys.F5:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //--------------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Costs_centers_ADD_UPd AddFrm = new Costs_centers_ADD_UPd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Costs_Centers_Main_Load(sender, e);
            this.Visible = true;
        }
        //--------------------------------------------------------------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            Costs_centers_ADD_UPd AddFrm = new Costs_centers_ADD_UPd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Costs_Centers_Main_Load(sender, e);
            this.Visible = true;

        }
        //--------------------------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Txt_ccSrch.Text = "";
            GetData();
        }
        //--------------------------------------------------------------------------------
        private void Txt_ccSrch_TextChanged(object sender, EventArgs e)
        {
            GetData();
        }
        //------------------------------------------------------------------------------------
        private void GetData()
        {
            GrdChange = false;
            object[] Sparam = { Txt_ccSrch.Text.Trim() };
            connection.SQLBS.DataSource = connection.SqlExec("Cost_Center_main", "Cost_Center_Tbl", Sparam);
            if (connection.SQLDS.Tables["Cost_Center_Tbl"].Rows.Count > 0)
            {
                GRD_center.DataSource = connection.SQLBS;

                label20.DataBindings.Clear();
                txtDe.DataBindings.Clear();


                label20.DataBindings.Add("Checked", connection.SQLBS, "RECORD_FLAG");
                txtDe.DataBindings.Add("Text", connection.SQLBS, "A_Description");


                GrdChange = true;
                GRD_center_SelectionChanged(null, null);
            }

        }
        //------------------------------------------------------------------------------------
        private void GRD_center_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                LblRec.Text = connection.Records(connection.SQLBS);


                if (connection.SQLDS.Tables["Cost_Center_Tbl"].Rows.Count > 0)
                {
                    int Mpacc_id = 0;
                    DataRowView Drv = connection.SQLBS.Current as DataRowView;
                    DataRow Dr = Drv.Row;
                    Mpacc_id = Dr.Field<Int32>("cc_id");

                    if (Mpacc_id > 0)
                    {
                        BtnUpd.Enabled = true;
                    }
                    else
                    {
                        BtnUpd.Enabled = false;
                    }


                }

            }
        }
        //------------------------------------------------------------------------------------
        private void Costs_Centers_Main_FormClosed(object sender, FormClosedEventArgs e)
        {

            GrdChange = false;
           
            string[] Used_Tbl = { "Cost_Center_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            cc_No_Tree AddFrm1 = new cc_No_Tree();
            this.Visible = false;
            AddFrm1.ShowDialog(this);
            Costs_Centers_Main_Load(sender, e);
            this.Visible = true;
        }
    }
}