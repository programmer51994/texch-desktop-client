﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Main_CC_Voucher_Read : Form
    {

        string Sql_Txt = "";
        string Sql_Txt1 = "";
        string Sql_Txt2 = "";
        bool CboChange = false;
        bool ACCChange = false;
        bool CC_Change = false;
        DataTable _Dt = new DataTable();
        DataTable _Dt_Acc = new DataTable();
        DataTable _Dt_CC = new DataTable();
        BindingSource _BSACC = new BindingSource();
        public Main_CC_Voucher_Read()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grdacc_id.AutoGenerateColumns = false;
            Grdvo_no.AutoGenerateColumns = false;

            Grdcost_id.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grdacc_id.Columns["Column2"].DataPropertyName = "acc_ename";
                Grdacc_id.Columns["column2"].DataPropertyName = "cc_ename";/// تكتب على حسب اسماء ونوع الاعمدةgrd  
            }
            #endregion

        }

        private void CC_Voucher_Read_Load(object sender, EventArgs e)
        {
            CboChange = false;
            Sql_Txt = " select ACUST_NAME,ECUST_NAME,t_id from TERMINALS a, CUSTOMERS b  where a.CUST_ID = b.CUST_ID ";
            CboT_id.DataSource = connection.SqlExec(Sql_Txt, "t_id_tbl");
            CboT_id.ValueMember = "t_id";
            CboT_id.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
            Sql_Txt1 = " SELECT distinct  C.Acc_id,Acc_EName,Acc_AName,Ref_Acc_No ,Dot_Acc_No "
                + " FROM   CC_Voucher A ,ACCOUNT_TREE B , voucher C "
                + " where C.acc_id = B.acc_id "
                + " And A.R_T_Y_Id = C.R_T_Y_Id "
                + " And C.T_ID = " + CboT_id.SelectedValue;
            Cbo_ACC_ID.DataSource = connection.SqlExec(Sql_Txt1, "CC_ACC_TBL");
            Cbo_ACC_ID.ValueMember = "Acc_id";
            Cbo_ACC_ID.DisplayMember = connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName";
            if (connection.SQLDS.Tables["CC_ACC_TBL"].Rows.Count > 0)
            {
                CboChange = true;
                Cbo_ACC_ID_SelectedIndexChanged(null, null);
            }
            SearchBtn_Click(null, null);

        }
        //----------------------------------------------------------------------------------------------
        private void Cbo_ACC_ID_SelectedIndexChanged(object sender, EventArgs e)
        {    try{
            if (CboChange)
            {
                Sql_Txt2 = "Select convert(varchar(12),MAX(A.C_date),111) As Nrec_Date From VOUCHER A ,"
                  + " CC_Voucher  B Where A.R_T_Y_Id = B.R_T_Y_Id And A.T_ID = " + CboT_id.SelectedValue
                  + " and A.ACC_Id =  " + Cbo_ACC_ID.SelectedValue;
                connection.SqlExec(Sql_Txt2, "Nrec_TBL");

                txtdate_from.Text = connection.SQLDS.Tables["Nrec_TBL"].Rows[0]["Nrec_Date"].ToString();
                txtdate_to.Text = connection.SQLDS.Tables["Nrec_TBL"].Rows[0]["Nrec_Date"].ToString();
                    }
                }
                    catch 
                {}
           
        }
        //----------------------------------------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            ACCChange = false;
            txtdate_from.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            String date_from = txtdate_from.Text;
            txtdate_to.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            String date_to = txtdate_to.Text;

            String SQL = "Exec Main_acc_voucher " + CboT_id.SelectedValue.ToString() + "," + date_from + "," +
                date_to + "," + Txt_Vo_No.Text + "," + Cbo_ACC_ID.SelectedValue.ToString() + "," + connection.user_id;
            connection.SqlExec(SQL, "MAIN_Acc_TBL");


            if (connection.SQLDS.Tables["MAIN_Acc_TBL"].Rows.Count > 0)
            {
                _Dt = connection.SQLDS.Tables["MAIN_Acc_TBL"];
                connection.SQLBSLst.DataSource = _Dt.DefaultView.ToTable(true, "ACC_id", "ACC_Aname", "ACC_Ename", "Ref_Acc_No").Select().CopyToDataTable();
                Grdacc_id.DataSource = connection.SQLBSLst;
                ACCChange = true;
                Grdacc_id_SelectionChanged(null, null);
            }
            else
            {
                Grdacc_id.DataSource = new BindingSource();
                Txt_Vo_No.Text = "0";
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط" : "No Records for this search Conditions ", MyGeneral_Lib.LblCap);
                return;
            }

        }
        //----------------------------------------------------------------------------------------------
        private void Grdacc_id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                {
                    CC_Change = false;
                    _BSACC.DataSource = new BindingSource();
                    if (connection.SQLDS.Tables["MAIN_Acc_TBL"].Rows.Count > 0)
                    {
                        _Dt_Acc = connection.SQLDS.Tables["MAIN_Acc_TBL"].Select("acc_id =" + ((DataRowView)connection.SQLBSLst.Current).Row["Acc_Id"]).CopyToDataTable();
                        _Dt_Acc = _Dt_Acc.DefaultView.ToTable(true,"Chk" ,"Vo_no", "Nrec_Date", "R_T_Y_Id", "LOC_AMOUNT",
                            "For_AMOUNT", "NOTES","ACUST_NAME","ECUST_NAME", "Cur_Aname", "Cur_Ename" ).Select().CopyToDataTable();
                        _BSACC.DataSource = _Dt_Acc;
                        Grdvo_no.DataSource = _BSACC;
                        CC_Change = true;
                        Grdvo_no_SelectionChanged(null, null);
                    }
                    else
                    {
                        Grdvo_no.DataSource = new BindingSource();
                    }

                }
            }
        }
        //----------------------------------------------------------------------------------------------
        private void Grdvo_no_SelectionChanged(object sender, EventArgs e)
        {
            if (CC_Change)
            {

                if (connection.SQLDS.Tables["MAIN_Acc_TBL"].Rows.Count > 0)
                {
                    Grdacc_id.RefreshEdit();
                    Grdacc_id.Refresh();
                    _Dt_CC = connection.SQLDS.Tables["MAIN_Acc_TBL"].Select("R_T_Y_Id =" + ((DataRowView)_BSACC.Current).Row["R_T_Y_Id"]).CopyToDataTable();
                    connection.SQLBSSubGrd.DataSource = _Dt_CC;
                    Grdcost_id.DataSource = connection.SQLBSSubGrd;
                }

            }
            else
            {
                Grdcost_id.DataSource = new BindingSource();
            }
        }
        //----------------------------------------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            
            if (_Dt_Acc.Select("chk > 0").Count() > 0)
            {
                DialogResult Dr = MessageBox.Show("هل تريد الحذف ", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    DataTable CC_R_T_Y_Id = _Dt_Acc.DefaultView.ToTable(true, "R_T_Y_Id", "chk")
                                      .Select("chk > 0").CopyToDataTable();

                    DataTable CC_Voucher_data = _Dt_CC.DefaultView.ToTable(false, "cc_id", "Percentage", "NOTES")
                                            .Select("Percentage > 0").CopyToDataTable();

                    connection.SQLCMD.Parameters.AddWithValue("@CC_R_T_Y_Id_data", CC_R_T_Y_Id);
                    connection.SQLCMD.Parameters.AddWithValue("@CC_Voucher_data", CC_Voucher_data);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_Id", ((DataRowView)connection.SQLBSLst.Current).Row["Acc_Id"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Form_Id", 3);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SqlExec("Add_CC_Voucher", connection.SQLCMD);
                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        return;
                    }
                    connection.SQLCMD.Parameters.Clear();
                   // CC_Voucher_Read_Load(null, null);
                    SearchBtn_Click(null, null);
                }
                else
                { return; }

            }
            else
            {

                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للحذف" : "No Records for Delete ", MyGeneral_Lib.LblCap);
                return;
            }


        }
        //----------------------------------------------------------------------------------------------
        private void Grdvo_no_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Grdvo_no.Columns["Column4"].Index)
            {
                Grdvo_no.EndEdit();  //Stop editing of cell.
                _Dt_Acc.AcceptChanges();
            }
        }
        //----------------------------------------------------------------------------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            if (_Dt_Acc.Select("chk > 0").Count() > 0)
            {
                DataTable CC_R_T_Y_Id = _Dt_Acc.DefaultView.ToTable()
                                          .Select("chk > 0").CopyToDataTable();

                DataTable CC_Voucher_data = _Dt_CC.DefaultView.ToTable(false, "cc_id", "R_T_Y_ID", "Percentage")
                                        .Select("Percentage > 0").CopyToDataTable();
                CC_Voucher_Upd AddFrm = new CC_Voucher_Upd(2, CC_R_T_Y_Id, CC_Voucher_data);
                this.Visible = false;
                AddFrm.ShowDialog(this);
               // CC_Voucher_Read_Load(sender, e);
                SearchBtn_Click(null, null);
                this.Visible = true;
            }
            else
            {

                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للتعديل" : "No Records for Update ", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //-----------------------------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            CC_Voucher_Read AddFrm = new CC_Voucher_Read(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            //CC_Voucher_Read_Load(sender, e);
            SearchBtn_Click(null, null);
            this.Visible = true;
        }

        private void Main_CC_Voucher_Read_FormClosed(object sender, FormClosedEventArgs e)
        {

            CboChange = false;
            ACCChange = false;
            CC_Change = false;
        }
    }
}