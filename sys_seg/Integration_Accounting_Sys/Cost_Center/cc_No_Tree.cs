﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class cc_No_Tree : Form
    {
        string Sql_Text = "";
        String cc_No = "";
        public cc_No_Tree()
        {

            InitializeComponent();
            connection.SQLBS.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }
        //----------------------------------------------------------------------------------------------------

        private void cc_No_Tree_Load(object sender, EventArgs e)
        {
            Sql_Text = " Exec [Cost_Center_TreeView] ";
            DataTable Dt = connection.SqlExec(Sql_Text, "Tree_ftp");
            connection.SQLBS.DataSource = connection.SqlExec(Sql_Text, "Tree_ftp");


            PopulateTreeView(treeView1.Nodes, 0, Dt);

        }

        //-----------------------------------------------------------------------------------------------------
        
        protected void PopulateTreeView(TreeNodeCollection parentNode, int parentID, DataTable Dt)
        {
            foreach (DataRow folder in Dt.Rows)
            {
                if (Convert.ToInt32(folder["pcc_id"]) == parentID)
                {
                    String key = folder["cc_no"].ToString();
                    String text = folder["cc_no"].ToString() + "        " + folder[connection.Lang_id == 1 ? "cc_AName" : "cc_EName"].ToString();
                    TreeNodeCollection newParentNode = parentNode.Add(key, text, key).Nodes;
                    PopulateTreeView(newParentNode, Convert.ToInt32(folder["cc_id"]), Dt);
                }
            }
        }
        //--------------------------------------------------------------------------------------------------
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            cc_No = treeView1.SelectedNode.ImageKey.ToString();
         
            try
            {
                connection.SQLBS.DataSource = connection.SQLDS.Tables["Tree_ftp"].DefaultView.ToTable().Select("cc_No ='" + cc_No + "'").CopyToDataTable();
                Txt_Acc_Id.DataBindings.Add("text", connection.SQLBS, "cc_id");
                txtDe.DataBindings.Add("Text", connection.SQLBS, connection.Lang_id == 1 ? "A_Description" : "E_Description");
                label19.DataBindings.Add("Checked", connection.SQLBS, "RECORD_FLAG");
                
            }
            catch 
            {
            }

        }
        //----------------------------------------------------------------------------------------------
        private void Acc_No_Tree_FormClosed(object sender, FormClosedEventArgs e)
        {
           
            string[] Used_Tbl = { "Tree_ftp"  };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

      //----------------------------------------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {  //public RptLang_MsgBox(ReportClass RPT, ReportClass RPTeng, bool Print_Flag = false, bool Pdf_Flag = false, bool Excel_Flag = false, DataTable  DT = null, DataGridView[] Grd = null, DataTable[] EXP_DT = null, bool Visible_Flag = false, bool isTerminal = false, String TitleName = "")
            Account_Tree_ViewRep ObjRpt = new Account_Tree_ViewRep();
            Account_Tree_ViewRepEng ObjRpteng = new Account_Tree_ViewRepEng();
            DataTable DT = new DataTable();
            DT = CustomControls.CustomParam_Dt();
            DT.Rows.Add("User_Name", TxtUser.Text);
            RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRpteng, true, true, false, DT);
            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
        
        }
        
    }
}