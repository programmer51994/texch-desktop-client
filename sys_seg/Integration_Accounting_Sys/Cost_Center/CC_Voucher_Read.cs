﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class CC_Voucher_Read : Form
    {

        bool acc_id_chg = false;
        bool vo_no_chg = false;
        string Sql_Txt = "";
        DateTime data_to;
        int date_to1 = 0;
        BindingSource _Bsdet_vo = new BindingSource();
        BindingSource _Bsacc_vo = new BindingSource();
        BindingSource _BsAcc_id = new BindingSource();
        BindingSource _Bsac_cc_id = new BindingSource();
        BindingSource _Bst_id= new BindingSource();
        DataTable _Dt = new DataTable();
        DataTable acc_cc_id = new DataTable();
        DataTable _Dt1 = new DataTable();
        int Form_Id ;
        public CC_Voucher_Read(int Frm_id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
             connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grdacc_id.AutoGenerateColumns = false;
            Grdvo_no.AutoGenerateColumns = false;
            GrdCC_voucher_read.AutoGenerateColumns = false;
            Grdcost_id.AutoGenerateColumns = false;
               #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grdacc_id.Columns["Column1"].DataPropertyName = "Acc_eName";
                Grdvo_no.Columns["column7"].DataPropertyName = "eCUST_NAME";/// تكتب على حسب اسماء ونوع الاعمدةgrd  
             }
            #endregion
             Form_Id = Frm_id;
        }

        private void CC_Voucher_Read_Load(object sender, EventArgs e)
        {
             Sql_Txt = " select ACUST_NAME,t_id from TERMINALS a, CUSTOMERS b  where a.CUST_ID = b.CUST_ID ";
              CboOper_id.DataSource = connection.SqlExec(Sql_Txt, "t_id_tbl");
              CboOper_id.ValueMember = "t_id";
             CboOper_id.DisplayMember = "ACUST_NAME";
        }
        //--------------------------------------------------------------------------
        private void Grdacc_id_SelectionChanged(object sender, EventArgs e)
        {
            vo_no_chg = false;
            if (acc_id_chg)
            {
                {
                    object[] Sparam = { date_to1, ((DataRowView)_BsAcc_id.Current).Row["Acc_Id"],
                                  ((DataRowView)_BsAcc_id.Current).Row["Dot_Acc_No"].ToString(),CboOper_id.SelectedValue };
                    connection.SqlExec("Get_acc_voucher", "acc_cc_voucher", Sparam);
                    if (connection.SQLDS.Tables["acc_cc_voucher2"].Rows.Count > 0)
                    {
                         _Dt =  connection.SQLDS.Tables["acc_cc_voucher2"].Select("acc_id = " + ((DataRowView)_BsAcc_id.Current).Row["Acc_Id"]).CopyToDataTable();
                        _Bsacc_vo.DataSource = _Dt;
                        Grdvo_no.DataSource = _Bsacc_vo;
                        vo_no_chg = true;
                        Grdvo_no_SelectionChanged(null, null);
                    }
                    else
                    { Grdvo_no.DataSource = new DataTable();
                  
                    }

                }
            }
        }
        //-------------------------------------------------------------------------
        private void Grdvo_no_SelectionChanged(object sender, EventArgs e)
        {
          if  (vo_no_chg )
          {
              if (connection.SQLDS.Tables["acc_cc_voucher"].Rows.Count > 0)
              {

                  _Dt1 = connection.SQLDS.Tables["acc_cc_voucher"].Select("oper_id = " + ((DataRowView)_Bsacc_vo.Current).Row["oper_Id"]
                                                                      + " and  vo_no = " + ((DataRowView)_Bsacc_vo.Current).Row["vo_no"]
                                                                      + " and nrec_date = " + ((DataRowView)_Bsacc_vo.Current).Row["nrec_date"]).CopyToDataTable();
                  _Bsdet_vo.DataSource = _Dt1;
                  GrdCC_voucher_read.DataSource = _Bsdet_vo;
              }
 
            if (connection.SQLDS.Tables["acc_cc_voucher1"].Rows.Count > 0)
            {
                 acc_cc_id = connection.SQLDS.Tables["acc_cc_voucher1"];
                _Bsac_cc_id.DataSource = acc_cc_id;
                Grdcost_id.DataSource = _Bsac_cc_id;
            }

        }
          else
          { GrdCC_voucher_read.DataSource = new DataTable(); }

        }
        //-----------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            string D = MyGeneral_Lib.DateChecking(txtdate_to.Text);
            if (D == "0")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ادخال التاريخ" : "Please enter the time ", MyGeneral_Lib.LblCap);
                txtdate_to.Focus();
                return;
            }
            
                data_to = Convert.ToDateTime(txtdate_to.Text);
                date_to1 = data_to.Year * 10000 + data_to.Month * 100 + data_to.Day;
                Sql_Txt = "  SELECT distinct  A.ACC_Id,Acc_EName,Acc_AName,Ref_Acc_No ,Dot_Acc_No "
                        + " FROM    VOUCHER  A, ACCOUNT_TREE b"
                       + " where NREC_DATE <= " + date_to1
                        + " and a.Acc_Id not in( select Acc_Id from  ACCOUNT_TREE where (Dot_Acc_No like '1.%' or Dot_Acc_No like '2.%'))	"
                        + " and a.acc_id = b.acc_id ";
                _BsAcc_id.DataSource = connection.SqlExec(Sql_Txt, "Ac_vo_Tbl");
                if (connection.SQLDS.Tables["Ac_vo_Tbl"].Rows.Count > 0)
                {
                    Grdacc_id.DataSource = _BsAcc_id;
                    acc_id_chg = true;
                    Grdacc_id_SelectionChanged(null, null);
                }

        }
        //--------------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            decimal sum_per = (from t in acc_cc_id.AsEnumerable()
                               select (Convert.ToDecimal(t["Percentage"]))).Sum();
            if (sum_per != 100)
            {
                MessageBox.Show("مجموع النسب لا يساوي 100", "رسالة  تحذيرية");
                return;
            }

            int count = _Dt.Select("chk > 0").Count();
            if (count == 0)
            { MessageBox.Show("يجب تاشير القيد", "رسالة تحذيرية"); return; }
            DataTable CC_R_T_Y_Id = _Dt.DefaultView.ToTable(false, "R_T_Y_Id", "chk")
                                     .Select("chk > 0").CopyToDataTable();

            DataTable CC_Voucher_data = acc_cc_id.DefaultView.ToTable(false, "cc_id", "Percentage", "NOTES")
                                    .Select("Percentage > 0").CopyToDataTable();
            connection.SQLCMD.Parameters.AddWithValue("@CC_R_T_Y_Id_data", CC_R_T_Y_Id);
            connection.SQLCMD.Parameters.AddWithValue("@CC_Voucher_data", CC_Voucher_data);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id", ((DataRowView)_Bsacc_vo.Current).Row["Acc_Id"].ToString());
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Form_Id", Form_Id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_CC_Voucher", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Grdacc_id_SelectionChanged(null, null);
        //    this.Close();
        }

        private void CC_Voucher_Read_FormClosed(object sender, FormClosedEventArgs e)
        {
         acc_id_chg = false;
        vo_no_chg = false;
        }

    }
}
