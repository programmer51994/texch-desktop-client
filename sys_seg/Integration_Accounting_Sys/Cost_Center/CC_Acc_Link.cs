﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class CC_Acc_Link : Form
    {
        bool GrdChange = false;
          string  Sql_Txt = "";
          BindingSource _BsCc = new BindingSource();
        BindingSource _BsAcc = new BindingSource();
        DataTable _Dt = new DataTable();
        public CC_Acc_Link()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
          connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
           
            Grdacc_tree.AutoGenerateColumns = false;
            Grdcc_cost.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grdacc_tree.Columns["Column1"].DataPropertyName = "acc_ename";
                Grdcc_cost.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "cc_ename";
            }
            #endregion

        }
        //-------------------------------------------------------------------------
        private void CC_Acc_Link_Load(object sender, EventArgs e)
        {
          
            Getdata();
        }

        private void Getdata()
        {
            GrdChange = false;
            object[] Sparam = { Txt_ccSrch.Text.Trim() };

            Sql_Txt = " SELECT     Acc_Id, Acc_AName, Acc_EName, Ref_Acc_No, User_ID "
                     + " FROM         ACCOUNT_TREE "
                    + " WHERE     (RECORD_FLAG = 1) "
                    + " and (acc_id like '" + Txt_ccSrch.Text.Trim() + "%' or Acc_AName like '" + Txt_ccSrch.Text.Trim() 
                    + "%' or Ref_Acc_No like '" + Txt_ccSrch.Text.Trim() + "%')";
           _BsAcc.DataSource  = connection.SqlExec(Sql_Txt, "Acc_Tbl");
           if (connection.SQLDS.Tables["Acc_Tbl"].Rows.Count > 0)
            {
                Grdacc_tree.DataSource = _BsAcc;
                GrdChange = true;
                Grdacc_tree_SelectionChanged(null, null);
            }

        }
        //------------------------------------------------------------------------
        private void Grdacc_tree_SelectionChanged(object sender, EventArgs e)
        {


            if (GrdChange)
            {

                Sql_Txt = "select  a.CC_ID, CC_AName, CC_EName, Percentage "
                        + " from CC_Acc_Link a , Costs_Centers b"
                        + " where Acc_Id = " + ((DataRowView)_BsAcc.Current).Row["Acc_Id"].ToString()
                        + " and a.CC_ID = b.cc_id"
                        + " union "
                        + " select CC_ID, CC_AName, CC_EName, 0 as Percentage "
                        + " FROM  Costs_Centers"
                        + " where CC_ID not in (select CC_ID from CC_Acc_Link)"
                        + " order by Percentage desc";

                _BsCc.DataSource = connection.SqlExec(Sql_Txt, "cc_Tbl");
                if (connection.SQLDS.Tables["cc_Tbl"].Rows.Count > 0)
                {
                    Grdcc_cost.DataSource = _BsCc;

                }
            }
            
        }
        //------------------------------------------------------------------------
        private void CC_Acc_Link_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            { 
                   
                
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        //------------------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {

            _Dt = connection.SQLDS.Tables["cc_Tbl"].DefaultView.ToTable
                (false, "cc_id", "Percentage") ;

            decimal sum_per = (from t in _Dt.AsEnumerable()
                     select (Convert.ToDecimal(t["Percentage"]))).Sum();
            if (sum_per != 100)
            { MessageBox.Show("مجموع النسب لا يساوي 100", "رسالة  تحذيرية");
                return; }

             connection.SQLCMD.Parameters.AddWithValue("@CC_Acc_Link_data", _Dt);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id", ((DataRowView)_BsAcc.Current).Row["Acc_Id"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_CC_Acc_Link", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
           
        }
        //-----------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Txt_ccSrch.Text="";
            Getdata();
        }
        //-----------------------------------------------------------------
        private void Txt_ccSrch_TextChanged(object sender, EventArgs e)
        {
            Getdata(); 
        }

        private void CC_Acc_Link_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
        }

    }

}
