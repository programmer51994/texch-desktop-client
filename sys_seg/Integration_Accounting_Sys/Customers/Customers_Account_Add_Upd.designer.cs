﻿namespace Integration_Accounting_Sys
{
    partial class Customers_Account_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Label7 = new System.Windows.Forms.Label();
            this.TxtCust_Name = new System.Windows.Forms.TextBox();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Label8 = new System.Windows.Forms.Label();
            this.TxtCust_Type = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.TxtBirth_Da = new System.Windows.Forms.MaskedTextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtDoc_Da = new System.Windows.Forms.MaskedTextBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.TxtDoc_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.Label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CboDC_ID = new System.Windows.Forms.ComboBox();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.Label15 = new System.Windows.Forms.Label();
            this.TxtA_Address = new System.Windows.Forms.TextBox();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBank_No = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.CheckBox();
            this.Grd_cur_Id = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_cur_name = new System.Windows.Forms.TextBox();
            this.Grd_Acc_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_acc_name = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbo_term = new System.Windows.Forms.ComboBox();
            this.TxtUpper_Lmt = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_cur_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).BeginInit();
            this.SuspendLayout();
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Label7.ForeColor = System.Drawing.Color.Navy;
            this.Label7.Location = new System.Drawing.Point(17, 78);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(65, 16);
            this.Label7.TabIndex = 88;
            this.Label7.Text = "بحـــــــــــث:";
            this.Label7.Click += new System.EventHandler(this.Label7_Click);
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.BackColor = System.Drawing.Color.White;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Name.Location = new System.Drawing.Point(92, 75);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(352, 22);
            this.TxtCust_Name.TabIndex = 2;
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Aname_TextChanged);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(391, 476);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(104, 25);
            this.AddBtn.TabIndex = 18;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(494, 476);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(104, 25);
            this.ExtBtn.TabIndex = 19;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.ForeColor = System.Drawing.Color.Navy;
            this.Label8.Location = new System.Drawing.Point(462, 85);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(83, 14);
            this.Label8.TabIndex = 127;
            this.Label8.Text = "نوع الثانـــوي:";
            this.Label8.Click += new System.EventHandler(this.Label8_Click);
            // 
            // TxtCust_Type
            // 
            this.TxtCust_Type.BackColor = System.Drawing.Color.White;
            this.TxtCust_Type.Enabled = false;
            this.TxtCust_Type.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Type.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TxtCust_Type.Location = new System.Drawing.Point(554, 81);
            this.TxtCust_Type.Name = "TxtCust_Type";
            this.TxtCust_Type.Size = new System.Drawing.Size(401, 22);
            this.TxtCust_Type.TabIndex = 4;
            this.TxtCust_Type.TextChanged += new System.EventHandler(this.TxtCust_Type_TextChanged);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.ForeColor = System.Drawing.Color.Navy;
            this.Label9.Location = new System.Drawing.Point(462, 117);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(80, 14);
            this.Label9.TabIndex = 134;
            this.Label9.Text = "الهاتـــــــــــف:";
            this.Label9.Click += new System.EventHandler(this.Label9_Click);
            // 
            // TxtPhone
            // 
            this.TxtPhone.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Enabled = false;
            this.TxtPhone.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(554, 114);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(196, 22);
            this.TxtPhone.TabIndex = 5;
            this.TxtPhone.TextChanged += new System.EventHandler(this.TxtPhone_TextChanged);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.ForeColor = System.Drawing.Color.Navy;
            this.Label10.Location = new System.Drawing.Point(766, 113);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(82, 14);
            this.Label10.TabIndex = 128;
            this.Label10.Text = "تاريخ التولـــد:";
            this.Label10.Click += new System.EventHandler(this.Label10_Click);
            // 
            // TxtBirth_Da
            // 
            this.TxtBirth_Da.BackColor = System.Drawing.Color.White;
            this.TxtBirth_Da.Enabled = false;
            this.TxtBirth_Da.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirth_Da.Location = new System.Drawing.Point(852, 109);
            this.TxtBirth_Da.Mask = "0000/00/00";
            this.TxtBirth_Da.Name = "TxtBirth_Da";
            this.TxtBirth_Da.PromptChar = ' ';
            this.TxtBirth_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtBirth_Da.Size = new System.Drawing.Size(105, 22);
            this.TxtBirth_Da.TabIndex = 6;
            this.TxtBirth_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TxtBirth_Da.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.TxtBirth_Da_MaskInputRejected);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.ForeColor = System.Drawing.Color.Navy;
            this.Label12.Location = new System.Drawing.Point(462, 148);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(88, 14);
            this.Label12.TabIndex = 136;
            this.Label12.Text = "رقم الوثيقــــة:";
            this.Label12.Click += new System.EventHandler(this.Label12_Click);
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.BackColor = System.Drawing.Color.White;
            this.TxtNo_Doc.Enabled = false;
            this.TxtNo_Doc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(554, 145);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(196, 22);
            this.TxtNo_Doc.TabIndex = 7;
            this.TxtNo_Doc.TextChanged += new System.EventHandler(this.TxtNo_Doc_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(766, 144);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 14);
            this.label11.TabIndex = 137;
            this.label11.Text = "تاريخ الاصدار:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.BackColor = System.Drawing.Color.White;
            this.TxtDoc_Da.Enabled = false;
            this.TxtDoc_Da.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDoc_Da.Location = new System.Drawing.Point(851, 141);
            this.TxtDoc_Da.Mask = "0000/00/00";
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.PromptChar = ' ';
            this.TxtDoc_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDoc_Da.Size = new System.Drawing.Size(106, 22);
            this.TxtDoc_Da.TabIndex = 8;
            this.TxtDoc_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TxtDoc_Da.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.TxtDoc_Da_MaskInputRejected);
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.ForeColor = System.Drawing.Color.Navy;
            this.Label13.Location = new System.Drawing.Point(462, 184);
            this.Label13.Margin = new System.Windows.Forms.Padding(3, 0, 3, 16);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(88, 14);
            this.Label13.TabIndex = 140;
            this.Label13.Text = "جهـــة الاصدار:";
            this.Label13.Click += new System.EventHandler(this.Label13_Click);
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.BackColor = System.Drawing.Color.White;
            this.TxtIss_Doc.Enabled = false;
            this.TxtIss_Doc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(554, 180);
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(401, 22);
            this.TxtIss_Doc.TabIndex = 9;
            this.TxtIss_Doc.TextChanged += new System.EventHandler(this.TxtIss_Doc_TextChanged);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.ForeColor = System.Drawing.Color.Navy;
            this.Label14.Location = new System.Drawing.Point(462, 215);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(88, 14);
            this.Label14.TabIndex = 129;
            this.Label14.Text = "نوع الوثيقـــــة:";
            this.Label14.Click += new System.EventHandler(this.Label14_Click);
            // 
            // TxtDoc_Name
            // 
            this.TxtDoc_Name.BackColor = System.Drawing.Color.White;
            this.TxtDoc_Name.Enabled = false;
            this.TxtDoc_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDoc_Name.Location = new System.Drawing.Point(554, 212);
            this.TxtDoc_Name.Name = "TxtDoc_Name";
            this.TxtDoc_Name.Size = new System.Drawing.Size(401, 22);
            this.TxtDoc_Name.TabIndex = 10;
            this.TxtDoc_Name.TextChanged += new System.EventHandler(this.TxtDoc_Name_TextChanged);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(455, 280);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(510, 1);
            this.flowLayoutPanel3.TabIndex = 142;
            this.flowLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel3_Paint);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(456, 75);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(509, 1);
            this.flowLayoutPanel1.TabIndex = 143;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(455, 75);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 205);
            this.flowLayoutPanel5.TabIndex = 0;
            this.flowLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel5_Paint);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(964, 74);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 206);
            this.flowLayoutPanel7.TabIndex = 144;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label17.ForeColor = System.Drawing.Color.Navy;
            this.Label17.Location = new System.Drawing.Point(31, 314);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(86, 14);
            this.Label17.TabIndex = 513;
            this.Label17.Text = "العـمـلـــــــــــة:";
            this.Label17.Click += new System.EventHandler(this.Label17_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(701, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 511;
            this.label1.Text = "طبيعتــــه:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // CboDC_ID
            // 
            this.CboDC_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDC_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDC_ID.BackColor = System.Drawing.Color.White;
            this.CboDC_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDC_ID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDC_ID.FormattingEnabled = true;
            this.CboDC_ID.Location = new System.Drawing.Point(779, 314);
            this.CboDC_ID.Name = "CboDC_ID";
            this.CboDC_ID.Size = new System.Drawing.Size(184, 22);
            this.CboDC_ID.TabIndex = 14;
            this.CboDC_ID.SelectedIndexChanged += new System.EventHandler(this.CboDC_ID_SelectedIndexChanged);
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label18.ForeColor = System.Drawing.Color.Navy;
            this.Label18.Location = new System.Drawing.Point(354, 314);
            this.Label18.Margin = new System.Windows.Forms.Padding(3, 0, 3, 16);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(99, 14);
            this.Label18.TabIndex = 505;
            this.Label18.Text = "اسـم الحســـاب:";
            this.Label18.Click += new System.EventHandler(this.Label18_Click);
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.ForeColor = System.Drawing.Color.Maroon;
            this.Label16.Location = new System.Drawing.Point(15, 285);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(129, 14);
            this.Label16.TabIndex = 503;
            this.Label16.Text = "المعلـومـات المــالية...";
            this.Label16.Click += new System.EventHandler(this.Label16_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(677, 346);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 0, 3, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 14);
            this.label20.TabIndex = 506;
            this.label20.Text = "الحـد  الاعـلى:";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1, 33);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(971, 1);
            this.flowLayoutPanel9.TabIndex = 519;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(120, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(324, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(776, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(183, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(707, 7);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 515;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 514;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(11, 297);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.flowLayoutPanel10.Size = new System.Drawing.Size(958, 1);
            this.flowLayoutPanel10.TabIndex = 520;
            this.flowLayoutPanel10.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel10_Paint);
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(11, 469);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.flowLayoutPanel11.Size = new System.Drawing.Size(961, 1);
            this.flowLayoutPanel11.TabIndex = 140;
            this.flowLayoutPanel11.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel11_Paint);
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(11, 297);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1, 173);
            this.flowLayoutPanel12.TabIndex = 521;
            this.flowLayoutPanel12.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel12_Paint);
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(969, 297);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(1, 173);
            this.flowLayoutPanel14.TabIndex = 522;
            this.flowLayoutPanel14.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel14_Paint);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.ForeColor = System.Drawing.Color.Navy;
            this.Label15.Location = new System.Drawing.Point(462, 246);
            this.Label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 16);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(85, 14);
            this.Label15.TabIndex = 524;
            this.Label15.Text = "العنــــــــــوان :";
            this.Label15.Click += new System.EventHandler(this.Label15_Click);
            // 
            // TxtA_Address
            // 
            this.TxtA_Address.BackColor = System.Drawing.Color.White;
            this.TxtA_Address.Enabled = false;
            this.TxtA_Address.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtA_Address.Location = new System.Drawing.Point(554, 243);
            this.TxtA_Address.Name = "TxtA_Address";
            this.TxtA_Address.Size = new System.Drawing.Size(401, 22);
            this.TxtA_Address.TabIndex = 11;
            this.TxtA_Address.TextChanged += new System.EventHandler(this.TxtA_Address_TextChanged);
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            this.Grd_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust.ColumnHeadersHeight = 45;
            this.Grd_Cust.ColumnHeadersVisible = false;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.Grd_Cust.Location = new System.Drawing.Point(15, 99);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.ReadOnly = true;
            this.Grd_Cust.RowHeadersVisible = false;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust.Size = new System.Drawing.Size(429, 178);
            this.Grd_Cust.TabIndex = 3;
            this.Grd_Cust.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_Cust_CellContentClick);
            this.Grd_Cust.SelectionChanged += new System.EventHandler(this.Grd_Cust_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cust_Id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رقم الثانوي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 70;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Acust_Name";
            this.Column2.HeaderText = "اسم الثانوي";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 380;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(12, 442);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 14);
            this.label4.TabIndex = 528;
            this.label4.Text = "رقم الحساب المصرفي:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // TxtBank_No
            // 
            this.TxtBank_No.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBank_No.Location = new System.Drawing.Point(160, 438);
            this.TxtBank_No.MaxLength = 10;
            this.TxtBank_No.Name = "TxtBank_No";
            this.TxtBank_No.Size = new System.Drawing.Size(165, 22);
            this.TxtBank_No.TabIndex = 17;
            this.TxtBank_No.TextChanged += new System.EventHandler(this.TxtBank_No_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(332, 442);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 14);
            this.label5.TabIndex = 529;
            this.label5.Text = "للحسابات المصرفية فقط .";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(773, 370);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(166, 18);
            this.label22.TabIndex = 16;
            this.label22.Text = "ربط الثانوي بهذا الحساب";
            this.label22.UseVisualStyleBackColor = true;
            this.label22.CheckedChanged += new System.EventHandler(this.label22_CheckedChanged);
            // 
            // Grd_cur_Id
            // 
            this.Grd_cur_Id.AllowUserToAddRows = false;
            this.Grd_cur_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_cur_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_cur_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_cur_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_cur_Id.ColumnHeadersHeight = 40;
            this.Grd_cur_Id.ColumnHeadersVisible = false;
            this.Grd_cur_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column4});
            this.Grd_cur_Id.Location = new System.Drawing.Point(30, 334);
            this.Grd_cur_Id.Name = "Grd_cur_Id";
            this.Grd_cur_Id.ReadOnly = true;
            this.Grd_cur_Id.RowHeadersVisible = false;
            this.Grd_cur_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_cur_Id.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_cur_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_cur_Id.Size = new System.Drawing.Size(317, 93);
            this.Grd_cur_Id.TabIndex = 530;
            this.Grd_cur_Id.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_cur_Id_CellContentClick);
            this.Grd_cur_Id.SelectionChanged += new System.EventHandler(this.Grd_cur_Id_SelectionChanged_1);
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "cur_id";
            this.Column3.HeaderText = "CUR_ID";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Cur_Aname";
            this.Column4.HeaderText = "CUR_ANAME";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 250;
            // 
            // Txt_cur_name
            // 
            this.Txt_cur_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_cur_name.Location = new System.Drawing.Point(123, 309);
            this.Txt_cur_name.MaxLength = 199;
            this.Txt_cur_name.Name = "Txt_cur_name";
            this.Txt_cur_name.Size = new System.Drawing.Size(222, 23);
            this.Txt_cur_name.TabIndex = 531;
            this.Txt_cur_name.TextChanged += new System.EventHandler(this.Txt_cur_name_TextChanged);
            // 
            // Grd_Acc_Id
            // 
            this.Grd_Acc_Id.AllowUserToAddRows = false;
            this.Grd_Acc_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Acc_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Acc_Id.ColumnHeadersHeight = 40;
            this.Grd_Acc_Id.ColumnHeadersVisible = false;
            this.Grd_Acc_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.Grd_Acc_Id.Location = new System.Drawing.Point(360, 335);
            this.Grd_Acc_Id.Name = "Grd_Acc_Id";
            this.Grd_Acc_Id.ReadOnly = true;
            this.Grd_Acc_Id.RowHeadersVisible = false;
            this.Grd_Acc_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Acc_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Id.Size = new System.Drawing.Size(317, 93);
            this.Grd_Acc_Id.TabIndex = 532;
            this.Grd_Acc_Id.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_Acc_Id_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 70;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Acc_Aname";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // Txt_acc_name
            // 
            this.Txt_acc_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_acc_name.Location = new System.Drawing.Point(455, 310);
            this.Txt_acc_name.MaxLength = 199;
            this.Txt_acc_name.Name = "Txt_acc_name";
            this.Txt_acc_name.Size = new System.Drawing.Size(222, 23);
            this.Txt_acc_name.TabIndex = 533;
            this.Txt_acc_name.TextChanged += new System.EventHandler(this.Txt_acc_name_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(15, 51);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 16);
            this.label19.TabIndex = 988;
            this.label19.Text = "الـثـانـــــوي:";
            // 
            // cbo_term
            // 
            this.cbo_term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_term.FormattingEnabled = true;
            this.cbo_term.Items.AddRange(new object[] {
            "فعـــــال",
            "غير فعـــــال"});
            this.cbo_term.Location = new System.Drawing.Point(92, 47);
            this.cbo_term.Name = "cbo_term";
            this.cbo_term.Size = new System.Drawing.Size(352, 24);
            this.cbo_term.TabIndex = 987;
            // 
            // TxtUpper_Lmt
            // 
            this.TxtUpper_Lmt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUpper_Lmt.Location = new System.Drawing.Point(779, 342);
            this.TxtUpper_Lmt.Name = "TxtUpper_Lmt";
            this.TxtUpper_Lmt.NumberDecimalDigits = 3;
            this.TxtUpper_Lmt.NumberDecimalSeparator = ".";
            this.TxtUpper_Lmt.NumberGroupSeparator = ",";
            this.TxtUpper_Lmt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtUpper_Lmt.Size = new System.Drawing.Size(184, 22);
            this.TxtUpper_Lmt.TabIndex = 15;
            this.TxtUpper_Lmt.Text = "0.000";
            this.TxtUpper_Lmt.TextChanged += new System.EventHandler(this.TxtUpper_Lmt_TextChanged);
            // 
            // Customers_Account_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 503);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.cbo_term);
            this.Controls.Add(this.Txt_acc_name);
            this.Controls.Add(this.Grd_Acc_Id);
            this.Controls.Add(this.Txt_cur_name);
            this.Controls.Add(this.Grd_cur_Id);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtBank_No);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.Label15);
            this.Controls.Add(this.TxtA_Address);
            this.Controls.Add(this.flowLayoutPanel14);
            this.Controls.Add(this.flowLayoutPanel12);
            this.Controls.Add(this.Label16);
            this.Controls.Add(this.flowLayoutPanel11);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Label17);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CboDC_ID);
            this.Controls.Add(this.Label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.TxtUpper_Lmt);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.TxtCust_Type);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.TxtBirth_Da);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.TxtDoc_Name);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.TxtCust_Name);
            this.Controls.Add(this.Label7);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customers_Account_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "161";
            this.Text = "Customers_Account_Add_Upd";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Customers_Account_Add_Upd_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Customers_Account_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.customer_lmt_acc_add_upd_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Customers_Account_Add_Upd_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_cur_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label7;
        private System.Windows.Forms.TextBox TxtCust_Name;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Label Label8;
        private System.Windows.Forms.TextBox TxtCust_Type;
        private System.Windows.Forms.Label Label9;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label Label10;
        private System.Windows.Forms.MaskedTextBox TxtBirth_Da;
        private System.Windows.Forms.Label Label12;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox TxtDoc_Da;
        private System.Windows.Forms.Label Label13;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label Label14;
        private System.Windows.Forms.TextBox TxtDoc_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label Label17;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboDC_ID;
        private System.Windows.Forms.Label Label18;
        private System.Windows.Forms.Label Label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Sample.DecimalTextBox TxtUpper_Lmt;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.Label Label15;
        private System.Windows.Forms.TextBox TxtA_Address;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBank_No;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox label22;
        private System.Windows.Forms.DataGridView Grd_cur_Id;
        private System.Windows.Forms.TextBox Txt_cur_name;
        private System.Windows.Forms.DataGridView Grd_Acc_Id;
        private System.Windows.Forms.TextBox Txt_acc_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbo_term;
    }
}