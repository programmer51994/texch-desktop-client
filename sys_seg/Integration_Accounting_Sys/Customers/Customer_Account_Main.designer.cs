﻿namespace Integration_Accounting_Sys
{
    partial class Customer_Account_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customer_Account_Main));
            this.label6 = new System.Windows.Forms.Label();
            this.CboTerminal_id = new System.Windows.Forms.ComboBox();
            this.GrdCurrency_Cust = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Hst = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.UpdBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.SearchBtn = new System.Windows.Forms.ToolStripButton();
            this.Label1 = new System.Windows.Forms.ToolStripLabel();
            this.TxtCust_Name = new System.Windows.Forms.ToolStripTextBox();
            this.label4 = new System.Windows.Forms.ToolStripLabel();
            this.CboCur_Id = new System.Windows.Forms.ToolStripComboBox();
            this.label5 = new System.Windows.Forms.ToolStripLabel();
            this.CboAcc_Name = new System.Windows.Forms.ToolStripComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtA_Address = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCurrency_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(2, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 14);
            this.label6.TabIndex = 63;
            this.label6.Text = "اســـــــــم الـفـــرع :";
            // 
            // CboTerminal_id
            // 
            this.CboTerminal_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboTerminal_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboTerminal_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboTerminal_id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboTerminal_id.FormattingEnabled = true;
            this.CboTerminal_id.Location = new System.Drawing.Point(117, 61);
            this.CboTerminal_id.Name = "CboTerminal_id";
            this.CboTerminal_id.Size = new System.Drawing.Size(296, 22);
            this.CboTerminal_id.TabIndex = 3;
            this.CboTerminal_id.SelectedIndexChanged += new System.EventHandler(this.CboTerminal_id_SelectedIndexChanged);
            // 
            // GrdCurrency_Cust
            // 
            this.GrdCurrency_Cust.AllowUserToAddRows = false;
            this.GrdCurrency_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdCurrency_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdCurrency_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.GrdCurrency_Cust.ColumnHeadersHeight = 36;
            this.GrdCurrency_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column6,
            this.Column7,
            this.Column10,
            this.Column9,
            this.Column8});
            this.GrdCurrency_Cust.Location = new System.Drawing.Point(6, 85);
            this.GrdCurrency_Cust.Name = "GrdCurrency_Cust";
            this.GrdCurrency_Cust.ReadOnly = true;
            this.GrdCurrency_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdCurrency_Cust.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.GrdCurrency_Cust.Size = new System.Drawing.Size(996, 337);
            this.GrdCurrency_Cust.TabIndex = 4;
            this.GrdCurrency_Cust.SelectionChanged += new System.EventHandler(this.GrdCurrency_Cust_SelectionChanged);
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Cust_ID";
            this.Column5.Frozen = true;
            this.Column5.HeaderText = "الرقــــم";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 75;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Acust_Name";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "الـثـانـوي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Cur_Aname";
            this.Column2.HeaderText = "العملة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Acc_id";
            this.Column3.HeaderText = "رقم الحساب";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 75;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Acc_AName";
            this.Column4.HeaderText = "اسم الحساب";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Upper_limit";
            dataGridViewCellStyle11.Format = "N3";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column6.HeaderText = "حد التنبيه الاعلى";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 160;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "DC_Aname";
            this.Column7.HeaderText = "طبيعة الحساب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 75;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "CUS_STATE_ID";
            this.Column10.HeaderText = "حالته بالنسبة للحساب";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Bank_No";
            this.Column9.HeaderText = "رقم الحساب المصرفي";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "User_Name";
            this.Column8.HeaderText = "منظم العملية";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Btn_Hst
            // 
            this.Btn_Hst.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Hst.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Hst.Location = new System.Drawing.Point(841, 425);
            this.Btn_Hst.Name = "Btn_Hst";
            this.Btn_Hst.Size = new System.Drawing.Size(163, 27);
            this.Btn_Hst.TabIndex = 5;
            this.Btn_Hst.Text = "معلومات تاريخية";
            this.Btn_Hst.UseVisualStyleBackColor = true;
            this.Btn_Hst.Click += new System.EventHandler(this.Btn_Hst_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(9, 440);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(168, 14);
            this.label7.TabIndex = 126;
            this.label7.Text = "العنوان والمعلومات العامـة...";
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.UpdBtn,
            this.toolStripSeparator1,
            this.BtnDel,
            this.toolStripSeparator7,
            this.AllBtn,
            this.toolStripSeparator2,
            this.SearchBtn,
            this.Label1,
            this.TxtCust_Name,
            this.label4,
            this.CboCur_Id,
            this.label5,
            this.CboAcc_Name});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(1009, 31);
            this.BND.TabIndex = 0;
            this.BND.Text = "bindingNavigator1";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(92, 28);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "اضافة جديد";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // UpdBtn
            // 
            this.UpdBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdBtn.Image = ((System.Drawing.Image)(resources.GetObject("UpdBtn.Image")));
            this.UpdBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdBtn.Name = "UpdBtn";
            this.UpdBtn.Size = new System.Drawing.Size(62, 28);
            this.UpdBtn.Tag = "3";
            this.UpdBtn.Text = "تعـديل";
            this.UpdBtn.Click += new System.EventHandler(this.UpdBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDel.Image = global::Integration_Accounting_Sys.Properties.Resources._12;
            this.BtnDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(64, 28);
            this.BtnDel.Tag = "4";
            this.BtnDel.Text = "حـــذف";
            this.BtnDel.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
            // 
            // AllBtn
            // 
            this.AllBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(87, 28);
            this.AllBtn.Tag = "4";
            this.AllBtn.Text = "عرض الكل";
            this.AllBtn.Click += new System.EventHandler(this.AllBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(61, 28);
            this.SearchBtn.Tag = "8";
            this.SearchBtn.Text = "بـحــث";
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(114, 28);
            this.Label1.Tag = "5";
            this.Label1.Text = ":اسم/رقم الثانوي ";
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.AutoSize = false;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Name.Margin = new System.Windows.Forms.Padding(5, 0, 1, 0);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(130, 22);
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Name_TextChanged);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(47, 28);
            this.label4.Tag = "7";
            this.label4.Text = ":العملة";
            // 
            // CboCur_Id
            // 
            this.CboCur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCur_Id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCur_Id.Name = "CboCur_Id";
            this.CboCur_Id.Size = new System.Drawing.Size(120, 31);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(53, 28);
            this.label5.Tag = "7";
            this.label5.Text = "الحساب";
            // 
            // CboAcc_Name
            // 
            this.CboAcc_Name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboAcc_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboAcc_Name.Name = "CboAcc_Name";
            this.CboAcc_Name.Size = new System.Drawing.Size(120, 31);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 516);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(15, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1009, 22);
            this.statusStrip1.TabIndex = 128;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(56, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(2, 59);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1008, 1);
            this.flowLayoutPanel9.TabIndex = 525;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(115, 34);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(298, 22);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(833, 34);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(167, 22);
            this.TxtIn_Rec_Date.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(771, 38);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 521;
            this.label3.Text = "التاريـــخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(3, 38);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(106, 14);
            this.label2.TabIndex = 520;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(16, 488);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 0, 3, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 14);
            this.label8.TabIndex = 526;
            this.label8.Text = "العنــــــوان :";
            // 
            // TxtA_Address
            // 
            this.TxtA_Address.BackColor = System.Drawing.Color.White;
            this.TxtA_Address.Enabled = false;
            this.TxtA_Address.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtA_Address.Location = new System.Drawing.Point(101, 484);
            this.TxtA_Address.Name = "TxtA_Address";
            this.TxtA_Address.Size = new System.Drawing.Size(894, 22);
            this.TxtA_Address.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(9, 463);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 14);
            this.label9.TabIndex = 531;
            this.label9.Text = "رقــم الهاتــف:";
            // 
            // TxtPhone
            // 
            this.TxtPhone.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Enabled = false;
            this.TxtPhone.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(101, 459);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(226, 22);
            this.TxtPhone.TabIndex = 6;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(1002, 453);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(1, 59);
            this.flowLayoutPanel14.TabIndex = 538;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(5, 452);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1, 59);
            this.flowLayoutPanel12.TabIndex = 537;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(5, 511);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.flowLayoutPanel11.Size = new System.Drawing.Size(998, 1);
            this.flowLayoutPanel11.TabIndex = 535;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(5, 452);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.flowLayoutPanel10.Size = new System.Drawing.Size(998, 1);
            this.flowLayoutPanel10.TabIndex = 536;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 31);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1008, 1);
            this.flowLayoutPanel1.TabIndex = 539;
            // 
            // Customer_Account_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 538);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.flowLayoutPanel14);
            this.Controls.Add(this.flowLayoutPanel12);
            this.Controls.Add(this.flowLayoutPanel11);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtA_Address);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.Btn_Hst);
            this.Controls.Add(this.GrdCurrency_Cust);
            this.Controls.Add(this.CboTerminal_id);
            this.Controls.Add(this.label6);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customer_Account_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "160";
            this.Text = "Customer_Account_Main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Customer_Account_Main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Customer_Account_Main_FormClosed);
            this.Load += new System.EventHandler(this.Customer_Account_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Customer_Account_Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.GrdCurrency_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CboTerminal_id;
        private System.Windows.Forms.DataGridView GrdCurrency_Cust;
        private System.Windows.Forms.Button Btn_Hst;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripButton UpdBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton SearchBtn;
        private System.Windows.Forms.ToolStripLabel Label1;
        private System.Windows.Forms.ToolStripTextBox TxtCust_Name;
        private System.Windows.Forms.ToolStripLabel label4;
        private System.Windows.Forms.ToolStripComboBox CboCur_Id;
        private System.Windows.Forms.ToolStripLabel label5;
        private System.Windows.Forms.ToolStripComboBox CboAcc_Name;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtA_Address;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BtnDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}