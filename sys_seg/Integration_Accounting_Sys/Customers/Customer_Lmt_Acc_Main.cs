﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Collections;
using System.Diagnostics;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Lmt_Acc_Main : Form
    {
        #region Defintion
        bool Change = false; 
        string Sql_Text;
        BindingSource _bs = new BindingSource();
     public static    BindingSource _bs_lmt = new BindingSource();
     public static BindingSource _bs_lmt_2 = new BindingSource();
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Customer_Lmt_Acc_Main()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["column1"].DataPropertyName = "ECust_name";
                Grd_Cust.Columns["Column3"].DataPropertyName="Fmd_Ename";
                GrdCurrency_Cust.Columns["Column8"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Cust.Columns["Column9"].DataPropertyName = "ACC_Ename";
            }
            Grd_Cust.AutoGenerateColumns = false;
            GrdCurrency_Cust.AutoGenerateColumns = false;
        }
        //---------------------------------------------------------------------------------------------------------
        private void customers_limit_account_main_Load(object sender, EventArgs e)
        {
            Grd_Bind();
        }
        //---------------------------------------------------------------------------------------------------------
        private void Grd_Bind()
        {
            Change = false;
            object[] Sparam = { TxtACust_Name.Text.Trim(), 0, "Customers_limit_Account"};
            _bs_lmt.DataSource = connection.SqlExec("Main_Customers_limit_Account", "Customer_lmt_Tbl", Sparam);
            
            if (connection.SQLDS.Tables["Customer_lmt_Tbl"].Rows.Count > 0)
            {
                Grd_Cust.DataSource = _bs_lmt;
                Change = true;
                Grd_Cust_SelectionChanged(null, null);
                Btn_Hst.Enabled = true;
                UpdBtn.Enabled = true;
            }
            else
            { Btn_Hst.Enabled = false ;
            UpdBtn.Enabled = false;
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                DataRowView Drv = _bs_lmt.Current as DataRowView;
                DataRow Dr = Drv.Row;
                int Cust_Id = Dr.Field<int>("Cust_Id");

                Sql_Text =  " select Cur_AName,Cur_EName,Per_Daily,Per_Tran, Acc_Aname , Acc_ename, B.Acc_id_online, "
                        + " USER_NAME,A.Cur_Id,cust_id,B.USER_ID  "
                        + " from Cur_Tbl A ,customers_limit_Account B,Users C  , ACCOUNT_TREE D "
                        + " where A.Cur_Id = B.Cur_Id "
                        + " AND B.USER_ID = C.USER_ID "
                        + " And B.Acc_Id_Online = D.Acc_Id  "
                        + " and cust_id = "+ Cust_Id;

                _bs_lmt_2.DataSource = connection.SqlExec(Sql_Text, "Currency_Tbl");
                GrdCurrency_Cust.DataSource = _bs_lmt_2;

                DataRowView Drv1 = _bs_lmt_2.Current as DataRowView;
                DataRow DR = Drv.Row;
                TxtCoun.Text = Dr.Field<string>(connection.Lang_id == 1 ? "Con_Aname" : "Con_Ename");
                TxtCity.Text = Dr.Field<string>(connection.Lang_id == 1 ? "Cit_Aname" : "Cit_Ename");
                TxtAddress.Text = Dr.Field<string>(connection.Lang_id != 2 ? "A_ADDRESS" : "E_ADDRESS");
       
                TxtBirth.Text = Dr.Field<string>("birth_date");
                TxtPhone.Text = Dr.Field<string>("PHONE");
                TxtEmail.Text = Dr.Field<string>("EMAIL");
                //.............
                LblRec.Text = connection.Records();
            }
            else
            {      
                  GrdCurrency_Cust.DataSource = _bs ;
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Btn_Hst_Click(object sender, EventArgs e)
        {
            Hst_Customer_Lmt_Acc_Main HstFrm = new Hst_Customer_Lmt_Acc_Main();
            this.Visible = false;
            HstFrm.ShowDialog(this);
            Grd_Bind();
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Customer_Lmt_Acc_Add_Upd AddFrm = new Customer_Lmt_Acc_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Grd_Bind();
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
            Customer_Lmt_Acc_Add_Upd AddFrm = new Customer_Lmt_Acc_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Grd_Bind();
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Grd_Bind();
            TxtACust_Name.Text = "";
        }
        //---------------------------------------------------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            TxtACust_Name.Text = "";
            Grd_Bind();
        }
        //---------------------------------------------------------------------------------------------------------
        private void customers_limit_account_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            
            string[] Used_Tbl = { "Customer_lmt_Tbl", "Currency_Tbl" }; ;
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
               
            }
        }

        private void Customer_Lmt_Acc_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    AllBtn_Click(sender, e);
                    break;
              
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Customer_Lmt_Acc_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

      
    }
}