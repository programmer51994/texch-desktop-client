﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Add_Upd : Form
    {
        #region Defintion
        bool Change = false;
        string Sql_Text = "";
        object sender = null;
        EventArgs e = null;
        byte Frm_Id;
        int Cust_Id = 0;
        public static int Aut_Cust_Id = 0;
        public static Int16 IS_DOC = 1;
        int Per_id = 0;
        DataTable Per_inf_TBL = new DataTable();
        byte Frm_Id_person_info = 0;
        //DialogResult DR = new DialogResult();
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Customer_Add_Upd(byte Form_Id)
        {
            InitializeComponent();
            #region Form_Orientation
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            #endregion
            Frm_Id = Form_Id;
            Frm_Id_person_info = Form_Id;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                CboCls_Cust.Items.Clear();
                CboCls_Cust.Items.Add("Account State");
                CboCls_Cust.Items.Add("Special Secondary");
                CboCls_Cust.Items.Add("General Secondary");
                CboCls_Cust.SelectedIndex = 0;
            }

            #endregion
        }
        //---------------------------------------------------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column2 =
            {
                        //"Per_ID", "Per_AName", "Per_EName", "per_City_ID", "per_Coun_ID", "Per_Birth_place", "per_Phone", "per_Another_Phone",
                        //"per_Email", "per_Frm_Doc_ID","per_Frm_Doc_NO", "per_Frm_Doc_Date", "per_Frm_Doc_EDate", "per_Frm_Doc_IS", "per_Nationalty_ID", "per_Birth_day", "per_Gender_ID",
                        //"per_Occupation",
                        //"per_Mother_name", "cust_ID", "User_ID", "registerd_ID","COUN_K_PH", "Per_ACITY_NAME", "Per_ECITY_NAME","Per_ACOUN_NAME","Per_ECOUN_NAME","Per_Street",
                        //"Per_Suburb","Per_Post_Code","Per_State" ,"PerFRM_ADOC_NA","PerFRM_EDOC_NA","Per_A_NAT_NAME" ,"Per_E_NAT_NAME","Per_Gender_Aname","Per_Gender_Ename","Login_id","details_job"
                          "Per_ID", "Per_AName", "Per_EName", "per_City_ID", "per_Coun_ID", "Per_Birth_place", "per_Phone", "per_Another_Phone",
                        "per_Email", "per_Frm_Doc_ID","per_Frm_Doc_NO", "per_Frm_Doc_Date", "per_Frm_Doc_EDate", "per_Frm_Doc_IS", "per_Nationalty_ID", "per_Birth_day", "per_Gender_ID", "per_Occupation",
                        "per_Mother_name", "cust_ID", "User_ID", "registerd_ID","COUN_K_PH", "Per_ACITY_NAME", "Per_ECITY_NAME","Per_ACOUN_NAME","Per_ECOUN_NAME","Per_Street",
                        "Per_Suburb","Per_Post_Code","Per_State" ,"PerFRM_ADOC_NA","PerFRM_EDOC_NA","Per_A_NAT_NAME" ,"Per_E_NAT_NAME","Per_Gender_Aname","Per_Gender_Ename","Login_id",
                        "per_EFrm_Doc_IS","EOccupation","mother_Ename","Per_EBirth_place","Per_EStreet","Per_ESuburb","Per_EState","Social_No","resd_flag","Per_Occupation_id","details_job"
                       
                       
            };

            string[] DType2 =
            {
                //"System.Int32","System.String","System.String","System.Int16",  "System.Int16", "System.String", "System.String","System.String",
                //"System.String", "System.Int16", "System.String","System.String" ,"System.String","System.String","System.Int16", "System.String", "System.Byte", "System.String",
                //"System.String","System.Int16","System.Int16", "System.Int16", "System.String", "System.String", "System.String","System.String","System.String","System.String",
                //"System.String","System.String","System.String", "System.String","System.String","System.String","System.String","System.String","System.String","System.Int16", "System.String"
                 "System.Int32","System.String","System.String","System.Int16",  "System.Int16", "System.String", "System.String","System.String",
                "System.String", "System.Int16", "System.String","System.String" ,"System.String","System.String","System.Int16", "System.String", "System.Byte", "System.String",
                "System.String","System.Int16","System.Int16", "System.Int16", "System.String", "System.String", "System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String", "System.String","System.String","System.String","System.String","System.String","System.String","System.Int16",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.Int16","System.Int16", "System.String"
            };

            Per_inf_TBL = CustomControls.Custom_DataTable("Per_inf_TBL", Column2, DType2);


        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Add_Upd_Load(object sender, EventArgs e)
        {
            CboCls_Cust.SelectedIndex = 0;
            Sql_Text = "SELECT Cust_Type_ID,Cust_Type_AName,Cust_Type_EName FROM Cust_Type_Tbl"
            + " union"
            + " select 0 as Cust_Type_ID,'جميع الثانويين' as Cust_Type_AName,'All Customers' as Cust_Type_EName";
            CboType_cust.DataSource = connection.SqlExec(Sql_Text, "Type_cust");
            CboType_cust.ValueMember = "Cust_Type_ID";
            CboType_cust.DisplayMember = connection.Lang_id == 1 ? "Cust_Type_AName" : "Cust_Type_EName";
            GetData();
          //  Cbo_Gender.SelectedValue = 0;

            if (Frm_Id == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديــل الثانوي" : "Update Customer"; 
                Change = true;
                EditRecord();
            }

            Cbo_Gender.SelectedValue = 0;
   
        }
        //----------------------
        private void GetData()
        {
            Change = false;
            Sql_Text = " Exec all_master ";
            connection.SqlExec(Sql_Text, "all_master");
            CboCit_Id.DataSource = connection.SQLDS.Tables["all_master6"];
            CboCit_Id.ValueMember = "Cit_Id";
            CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "cit_Con_Aname" : "cit_Con_Ename";

            CboNat_id.DataSource = connection.SQLDS.Tables["all_master1"];
            CboNat_id.ValueMember = "NAt_Id";
            CboNat_id.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_eNAME";


            CboDoc_id.DataSource = connection.SQLDS.Tables["all_master2"];
           CboDoc_id.ValueMember = "Fmd_id";
           CboDoc_id.DisplayMember = connection.Lang_id == 1 ? "Fmd_aname" : "Fmd_Ename";

            
           Cbo_Gender.DataSource = connection.SQLDS.Tables["all_master7"];
           Cbo_Gender.ValueMember = "Gender_id";
           Cbo_Gender.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";


           Cbo_Occupation.DataSource = connection.SQLDS.Tables["all_master8"];
           Cbo_Occupation.ValueMember = "job_id";
           Cbo_Occupation.DisplayMember = connection.Lang_id == 1 ? "job_Aname" : "job_Ename";





           
            Change = true;
          
        }
        //---------------------------------------------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            string Doc_Date = "";
            string Doc_Edate = "";
            string Birth = "";
            
            #region Validation
            if (TxtCust_Aname.Text == "")
            {
               MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please enter the Arabic Name", MyGeneral_Lib.LblCap);
               TxtCust_Aname.Focus();
                return;
            }
            if (TxtCust_Ename.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاسم الاجنبي" : "Please enter the English Name", MyGeneral_Lib.LblCap);
                TxtCust_Ename.Focus();
                return;
            }
            if (CboCls_Cust.SelectedIndex == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد حالة الثانوي" : "Please select type of Account", MyGeneral_Lib.LblCap);
                CboCls_Cust.Focus();
                return;
            }

            if (Convert.ToInt16(CboCit_Id.SelectedValue) <= 0 && CboCls_Cust.SelectedIndex == 2)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد المدينة" : "Please select the Country", MyGeneral_Lib.LblCap);
                CboCit_Id.Focus();
                return;
            }
            if (Convert.ToInt16(CboDoc_id.SelectedValue) <0 && CboCls_Cust.SelectedIndex == 2)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثيقة" : "Please select the type of Doc.", MyGeneral_Lib.LblCap);
                CboDoc_id.Focus();
                return;
            }
          

            if (Convert.ToInt16(CboNat_id.SelectedValue) <= 0 && CboCls_Cust.SelectedIndex == 2)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد الجنسية" : "Please select the Nationality", MyGeneral_Lib.LblCap);
                CboNat_id.Focus();
                return;
            }

            if (Convert.ToInt16(CboDoc_id.SelectedValue) > 0)
            {
                if (TxtNo_Doc.Text == "")
                {
               MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الوثيقة" : "Please enter Doc. No.", MyGeneral_Lib.LblCap);
               TxtNo_Doc.Focus();
              return;
                }
                if (TxtIss_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل جهة الاصدار" : "Please enter Issuer", MyGeneral_Lib.LblCap);
                    TxtIss_Doc.Focus();
                    return;
                }
                Doc_Date = TxtDoc_Da.Text;
                string Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
                if (Ord_Strt == "0" || Ord_Strt == "-1")               
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل تأريخ الاصدار" : "Please enter Issuer Date", MyGeneral_Lib.LblCap);
                    TxtDoc_Da.Focus();
                    return;
                }
                
                Doc_Edate = TxtExp_Da.Text;
                Ord_Strt = MyGeneral_Lib.DateChecking(TxtExp_Da.Text);
                if (Ord_Strt == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ نفاذ الوثيقة" : "Please make sure of expire date of formal document", MyGeneral_Lib.LblCap);
                    TxtExp_Da.Focus();
                    return;
                }
                else if (Ord_Strt == "0")
                {
                    Doc_Edate = "";
                }
            }

            Birth = TxtBirth_Da.Text;
            string Ord_Strt1 = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
            if (Ord_Strt1 == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ التولد" : "Please make sure of birth date", MyGeneral_Lib.LblCap);
                TxtBirth_Da.Focus();
                return;
            }
            else if (Ord_Strt1 == "0")
            {
                Birth = "";
            }
            if (Convert.ToInt16(CboType_cust.SelectedValue) < 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الثانوي" : "Please select the type of Customer.", MyGeneral_Lib.LblCap);
                CboType_cust.Focus();
                return;
            }
          
            if (Convert.ToInt16(Cbo_Gender.SelectedValue) <= -1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد الجنس" : "Please select the Gender", MyGeneral_Lib.LblCap);
                Cbo_Gender.Focus();
                return;
            }
            
            #endregion
        
             
           
            Int16 Con_Id = Convert.ToInt16(connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["Con_ID"]);
            //Int16 Cit_Id = Convert.ToInt16(connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["Cit_Id"]);
            string ACITY_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["cit_Aname"].ToString();
            string ECITY_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["cit_Ename"].ToString();
            string ACoun_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["con_Aname"].ToString();
            string ECoun_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["con_Ename"].ToString();
            string FRM_ADOC_NA = connection.SQLDS.Tables["all_master2"].Rows[CboDoc_id.SelectedIndex]["Fmd_Aname"].ToString();
            string FRM_EDOC_NA = connection.SQLDS.Tables["all_master2"].Rows[CboDoc_id.SelectedIndex]["Fmd_Ename"].ToString();
            string A_NAT_NAME = connection.SQLDS.Tables["all_master1"].Rows[CboNat_id.SelectedIndex]["Nat_ANAME"].ToString();
            string E_NAT_NAME = connection.SQLDS.Tables["all_master1"].Rows[CboNat_id.SelectedIndex]["Nat_ENAME"].ToString();
            string Gender_Aname = connection.SQLDS.Tables["all_master7"].Rows[Cbo_Gender.SelectedIndex]["Gender_Aname"].ToString();
            string Gender_Ename = connection.SQLDS.Tables["all_master7"].Rows[Cbo_Gender.SelectedIndex]["Gender_Ename"].ToString();

            if (Chk_Cust.Checked)
            {
                if (Frm_Id == 2 && Per_id == 0)
                { Frm_Id_person_info = 1; }

                //connection.SQLCMD.Parameters.Clear();
                //connection.SQLCMD.Parameters.AddWithValue("@per_id", Per_id);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_AName", TxtCust_Aname.Text.Trim() );
                //connection.SQLCMD.Parameters.AddWithValue("@Per_EName", TxtCust_Ename.Text.Trim());

                //connection.SQLCMD.Parameters.AddWithValue("@per_City_ID", CboCit_Id.SelectedValue);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Coun_ID", Con_Id);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Address", "");

                //connection.SQLCMD.Parameters.AddWithValue("@per_Phone", TxtPhone.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Another_Phone", Txt_Another_Phone.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Email", TxtEmail.Text.Trim());

                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_ID", CboDoc_id.SelectedValue);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_NO", TxtNo_Doc.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_Date", Doc_Date);

                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_EDate", Doc_Edate);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_IS", TxtIss_Doc.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Nationalty_ID", CboNat_id.SelectedValue);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Birth_day", Birth);

                //connection.SQLCMD.Parameters.AddWithValue("@per_Gender_ID", Convert.ToByte( Cbo_Gender.SelectedValue) );
                //connection.SQLCMD.Parameters.AddWithValue("@per_Occupation", Txt_Occupation.Text );
                //connection.SQLCMD.Parameters.AddWithValue("@per_Mother_name", "");
                ////connection.SQLCMD.Parameters.AddWithValue("@cust_ID", Cust_Id );

                //connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                //connection.SQLCMD.Parameters.AddWithValue("@COUN_K_PH", "");
                //connection.SQLCMD.Parameters.AddWithValue("@Per_ACITY_NAME", ACITY_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_eCITY_NAME", ECITY_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_ACOUN_NAME", ACoun_NAME);

                //connection.SQLCMD.Parameters.AddWithValue("@Per_ECOUN_NAME", ECoun_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Street", Txt_Street.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Suburb", Txt_Suburb.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Post_Code", Txt_Post_code.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@Per_State", Txt_State.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@PerFRM_ADOC_NA", FRM_ADOC_NA);
                //connection.SQLCMD.Parameters.AddWithValue("@PerFRM_EDOC_NA", FRM_EDOC_NA);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_A_NAT_NAME", A_NAT_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_E_NAT_NAME", E_NAT_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Gender_Aname", Gender_Aname);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Gender_ename", Gender_Ename);
                //connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", Frm_Id_person_info); 
                //connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                //connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
               
                Create_Table();

                 Per_inf_TBL.Rows.Add(new object[] { Per_id, TxtCust_Aname.Text.Trim(), TxtCust_Ename.Text.Trim(),
                             CboCit_Id.SelectedValue,Con_Id , Txt_birth_place.Text.Trim()
                            ,TxtPhone.Text.Trim(),Txt_Another_Phone.Text.Trim() , TxtEmail.Text.Trim(),
                             CboDoc_id.SelectedValue,TxtNo_Doc.Text.Trim(),Doc_Date,Doc_Edate,
                            TxtIss_Doc.Text.Trim(), CboNat_id.SelectedValue,Birth,
                             Convert.ToByte(Cbo_Gender.SelectedValue), string.Empty, string.Empty ,0
                            ,connection.user_id,1,string.Empty,ACITY_NAME ,
                             ECITY_NAME,ACoun_NAME,ECoun_NAME, 
                             Txt_Street.Text.Trim(), Txt_Suburb.Text.Trim(),Txt_Post_code.Text.Trim()
                            ,Txt_State.Text.Trim(), FRM_ADOC_NA  ,FRM_EDOC_NA
                             ,A_NAT_NAME,E_NAT_NAME, Gender_Aname
                            ,Gender_Ename,0, 
                             TxtIss_Doc.Text.Trim(),
                             string.Empty,
                             string.Empty,
                             Txt_birth_place.Text.Trim(),
                             Txt_Street.Text.Trim(),
                             Txt_Suburb.Text.Trim(),
                             Txt_State.Text.Trim(),
                             string.Empty,0,Cbo_Occupation.SelectedValue,string.Empty});


                 connection.SQLCMD.Parameters.Clear();
                 connection.SQLCMD.Parameters.AddWithValue("@Person_Info_Type", Per_inf_TBL);
                 connection.SQLCMD.Parameters.AddWithValue("@registerd_ID", 1);
                 connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                 connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", Frm_Id_person_info);
                 connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                 connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                 connection.SqlExec("Person_info_Add_Upd_2", connection.SQLCMD);

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                        !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Per_id)))
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                connection.SQLCMD.Parameters.Clear();
            
            }
           

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, TxtCust_Aname.Text.Trim());
            ItemList.Insert(1, TxtCust_Ename.Text.Trim());
            ItemList.Insert(2, 0);
            ItemList.Insert(3, CboDoc_id.SelectedValue);
            ItemList.Insert(4, TxtNo_Doc.Text.Trim());
            ItemList.Insert(5, TxtIss_Doc.Text.Trim());
            ItemList.Insert(6, Doc_Date);
            ItemList.Insert(7, Doc_Edate);
            ItemList.Insert(8, "");
            ItemList.Insert(9, "");
            ItemList.Insert(10, TxtEmail.Text.Trim());
            ItemList.Insert(11, Birth);
            ItemList.Insert(12, TxtPhone.Text.Trim());
            ItemList.Insert(13, CboNat_id.SelectedValue);
            ItemList.Insert(14, Con_Id);
            ItemList.Insert(15, CboCit_Id.SelectedValue);
            ItemList.Insert(16, CboCls_Cust.SelectedIndex);
            ItemList.Insert(17, CboType_cust.SelectedValue);
            ItemList.Insert(18, Cust_Id);
            ItemList.Insert(19, connection.user_id);
            ItemList.Insert(20, Frm_Id);               //form_id
            ItemList.Insert(21, Per_id);               //Per_id from person_info
            ItemList.Insert(22, Convert.ToByte(Cbo_Gender.SelectedValue));
            ItemList.Insert(23, Cbo_Occupation.Text);
            ItemList.Insert(24, Txt_Another_Phone.Text.Trim());
            ItemList.Insert(25, Txt_Street.Text.Trim());
            ItemList.Insert(26, Txt_Suburb.Text.Trim());
            ItemList.Insert(27, Txt_Post_code.Text.Trim());
            ItemList.Insert(28, Txt_State.Text.Trim());
            ItemList.Insert(29, Convert.ToByte(Check.Checked));//له مخول
            ItemList.Insert(30, "");
           // MyGeneral_Lib.Copytocliptext("Add_Upd_Customer", ItemList);
            connection.scalar("Add_Upd_Customer", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                        !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Aut_Cust_Id)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();


            if (Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]) == 1 && CboCls_Cust.SelectedIndex == 2)//عام ويملك خاصية رفع الوثائق
            {
                IS_DOC = Convert.ToInt16(CboDoc_id.SelectedValue);
                Sql_Text = "Select    CUST_ID, ACUST_NAME, ECUST_NAME from CUSTOMERS where cust_id = " + Aut_Cust_Id;
                connection.SqlExec(Sql_Text, "Customer_tbl");
                add_Upd_Document UpdFrm = new add_Upd_Document(connection.SQLDS.Tables["Customer_tbl"].Rows[0]["ACUST_NAME"].ToString(), Aut_Cust_Id, IS_DOC, connection.SQLDS.Tables["Customer_tbl"].Rows[0]["ECUST_NAME"].ToString());
                this.Visible = false;
                UpdFrm.ShowDialog(this);
                Customer_Add_Upd_Load(sender, e);
                this.Visible = true;

            }

            if (Check.Checked == true)
            {
                //DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد اضافة مخولين  ؟" :
                //"Do you want to add  Authorized person", MyGeneral_Lib.LblCap,
                //MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (Dr == DialogResult.Yes)
                //{
                Authorized_Add_Upd AddFrm = new Authorized_Add_Upd(Aut_Cust_Id);
                this.Visible = false;
                AddFrm.ShowDialog(this);
                   // this.Visible = true;
                this.Close();

               // }
            }
            if (Frm_Id == 1)
            {
                TxtCust_Aname.Text = "";
                TxtCust_Ename.Text = "";
                TxtEmail.Text = ""; 
                TxtDoc_Da.Text = "00000000";
               // TxtE_Address.Text = "";
                TxtExp_Da.Text = "00000000"; 
                TxtIss_Doc.Text = "";
                TxtNo_Doc.Text = "";
                TxtPhone.Text = "";
                //TxtA_Address.Text = "";
                TxtBirth_Da.Text = "00000000";
                CboCit_Id.SelectedValue = 0;
                CboCls_Cust.SelectedIndex = 0;     
                CboDoc_id.SelectedValue = -1;
                CboNat_id.SelectedValue = -1;
                CboType_cust.SelectedValue = 0;
                Txt_Another_Phone.Text = "";
                Cbo_Occupation.SelectedValue = 0;
                Txt_Post_code.Text = "";
                Txt_State.Text = "";
                Txt_Street.Text = "";
                Txt_Suburb.Text = "";
                Cbo_Gender.SelectedValue = 0;
                Check.Checked =false ;
                Chk_Cust.Checked = false;
                Txt_birth_place.Text = "";

               


            }
            else
                this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void EditRecord()
        {
            DataRowView DRV = Customer_Main._Bs_Customers.Current as DataRowView;
            DataRow DR = DRV.Row;

            Cust_Id = DR.Field<int>("Cust_ID");
            //--------------------------------------------
            TxtCust_Aname.DataBindings.Clear();
            TxtCust_Ename.DataBindings.Clear();
            CboCit_Id.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            TxtBirth_Da.DataBindings.Clear();
            CboNat_id.DataBindings.Clear();
            TxtPhone.DataBindings.Clear();
            CboDoc_id.DataBindings.Clear();
            TxtNo_Doc.DataBindings.Clear();
            TxtDoc_Da.DataBindings.Clear();
            TxtIss_Doc.DataBindings.Clear();
            TxtExp_Da.DataBindings.Clear();
            //TxtA_Address.DataBindings.Clear();
            //TxtE_Address.DataBindings.Clear();
            CboCls_Cust.DataBindings.Clear();
            CboType_cust.DataBindings.Clear();
            CboCls_Cust.Enabled = false;
            Txt_Another_Phone.DataBindings.Clear();
            Check.DataBindings.Clear();
            Cbo_Gender.DataBindings.Clear();
            Cbo_Occupation.DataBindings.Clear();
            Txt_Post_code.DataBindings.Clear();
            Txt_State.DataBindings.Clear();
            Txt_Street.DataBindings.Clear();
            Txt_Suburb.DataBindings.Clear();
            Chk_Cust.DataBindings.Clear();

            TxtCust_Aname.DataBindings.Add("Text", Customer_Main._Bs_Customers, "ACust_Name");
            TxtCust_Ename.DataBindings.Add("Text", Customer_Main._Bs_Customers, "ECust_Name");
            CboCit_Id.DataBindings.Add("SelectedValue", Customer_Main._Bs_Customers, "Cit_Id");
            TxtEmail.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Email");
            TxtBirth_Da.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Birth_Date");
            CboNat_id.DataBindings.Add("SelectedValue", Customer_Main._Bs_Customers, "Nat_Id");
            TxtPhone.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Phone");
            CboDoc_id.DataBindings.Add("SelectedValue", Customer_Main._Bs_Customers, "Fmd_Id");
            TxtNo_Doc.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Frm_doc_no");
            TxtDoc_Da.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Frm_DOC_DA");
            TxtIss_Doc.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Frm_doc_Is");
            TxtExp_Da.DataBindings.Add("Text", Customer_Main._Bs_Customers, "doc_eda");
           //TxtA_Address.DataBindings.Add("Text", Customer_Main._Bs_Customers, "A_Address");
           // TxtE_Address.DataBindings.Add("Text", Customer_Main._Bs_Customers, "E_Address");
            CboCls_Cust.DataBindings.Add("SelectedIndex", Customer_Main._Bs_Customers, "CUS_Cls_ID");
            CboType_cust.DataBindings.Add("SelectedValue", Customer_Main._Bs_Customers, "Cust_Type_Id");
            Txt_Another_Phone.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Another_Phone");
            Check.DataBindings.Add("Checked", Customer_Main._Bs_Customers, "Authorized_Flag");
            Cbo_Gender.DataBindings.Add("SelectedValue", Customer_Main._Bs_Customers, "Gender_id");
            //Txt_Occupation.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Occupation");
            Txt_Post_code.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Per_Post_Code");
            Txt_State.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Per_State");
            Txt_Street.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Per_Street");
            Txt_Suburb.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Per_Suburb");
            Chk_Cust.DataBindings.Add("Checked", Customer_Main._Bs_Customers, "Chk_person");
            Cbo_Occupation.DataBindings.Add("Text", Customer_Main._Bs_Customers, "Occupation");

            if (Chk_Cust.Checked == true )
            { Chk_Cust.Enabled = false;}
            if (Check.Checked == true)
            { Check.Enabled = false;}

             
            Per_id = Convert.ToInt32(((DataRowView)Customer_Main._Bs_Customers.Current).Row["per_id"]);
            Cust_Id = Convert.ToInt32(((DataRowView)Customer_Main._Bs_Customers.Current).Row["Cust_id"]);

            string Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
            if (Ord_Strt == "0" || Ord_Strt == "-1")
            {
                TxtDoc_Da.Text = "00000000";
            }
            Ord_Strt = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
            if (Ord_Strt == "0" || Ord_Strt == "-1")
            {
                TxtBirth_Da.Text = "00000000";
            }
            Ord_Strt = MyGeneral_Lib.DateChecking(TxtExp_Da.Text);
            if (Ord_Strt == "0" || Ord_Strt == "-1")
            {
                TxtExp_Da.Text = "00000000";
            }

        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Used_Tbl = { "Type_cust","all_master1", "all_master", "all_master2",
                                   "all_master6","all_master7","Type_cust" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void CboDoc_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboDoc_id.SelectedIndex == 1)
            {
                TxtNo_Doc.Enabled = false; TxtNo_Doc.Text = "";
                TxtIss_Doc.Enabled = false;TxtIss_Doc.Text = "";
                TxtDoc_Da.Enabled = false; TxtDoc_Da.Text = "";
                TxtExp_Da.Enabled = false; TxtExp_Da.Text = "";

            }
            else
            {
                TxtNo_Doc.Enabled = true ;
                TxtIss_Doc.Enabled = true;
                TxtDoc_Da.Enabled = true;
                TxtExp_Da.Enabled = true;
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void CboCls_Cust_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCls_Cust.SelectedIndex == 1)
            {
                
                if (Convert.ToInt16(CboCit_Id.SelectedValue) < 0)
                {
                    CboCit_Id.SelectedValue = 0;
                }

                if (Convert.ToInt16(CboDoc_id.SelectedValue) < 0)
                {
                    CboDoc_id.SelectedValue = 0;
                }

                if (Convert.ToInt16(CboNat_id.SelectedValue) < 0)
                {
                    CboNat_id.SelectedValue = 0;
                }

                if (Convert.ToInt16(Cbo_Gender.SelectedValue) < 0)
                {
                    Cbo_Gender.SelectedValue = 0;
                }
            }
            else
            {
               
                if (Convert.ToInt16(CboCit_Id.SelectedValue) <= 0)
                {
                  CboCit_Id.SelectedValue = 0;}
                if (Convert.ToInt16(CboDoc_id.SelectedValue) <= 0 && Frm_Id ==1)
                {
                 CboDoc_id.SelectedValue = -1;
                }
                if (Convert.ToInt16(CboNat_id.SelectedValue) <= 0)
              {
                CboNat_id.SelectedValue = -1;
              }

                if (Convert.ToInt16(Cbo_Gender.SelectedValue) <= 0)
                {
                    Cbo_Gender.SelectedValue = -1;
                }
            
            }
        }



        private void Customer_Add_Upd_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 2;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

    }
}