﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Authorized_Add_Upd : Form
    {
        #region Defintion
        bool Change = false;
        string Sql_Text = "";

        int Auth_cust_id;
        int Cust_Id = 0;
        int Per_id = 0;
        string Per_Name = "";
        DataTable Per_inf_TBL = new DataTable();
       
        DataTable TBL = new DataTable();
        BindingSource _Bs_Grd_PerInfo = new BindingSource();

        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Authorized_Add_Upd(int Cust_id )
        {
            InitializeComponent();

            #region Form_Orientation
            MyGeneral_Lib.Form_Orientation(this);
            // connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            #endregion
            Auth_cust_id = Cust_id ;
         
            Grd_Person_info.AutoGenerateColumns = false;

            #region Enghlish
            if (connection.Lang_id != 1)
            {

            }

            #endregion
        }
        //---------------------------------------------------------------------------------------------------------

        private void Create_Table()
        {

            string[] Column =
            {
                 "Chk" , "Per_ID"      
            };

            string[] DType =
            {
                "System.Int32","System.Int32"
            };

            TBL = CustomControls.Custom_DataTable("TBL", Column, DType);


            string[] Column2 =
            {
                        "Per_ID", "Per_AName", "Per_EName", "per_City_ID", "per_Coun_ID", "Per_Birth_place", "per_Phone", "per_Another_Phone",
                        "per_Email", "per_Frm_Doc_ID","per_Frm_Doc_NO", "per_Frm_Doc_Date", "per_Frm_Doc_EDate", "per_Frm_Doc_IS", "per_Nationalty_ID", "per_Birth_day", "per_Gender_ID",
                        "per_Occupation",
                        "per_Mother_name", "cust_ID", "User_ID", "registerd_ID","COUN_K_PH", "Per_ACITY_NAME", "Per_ECITY_NAME","Per_ACOUN_NAME","Per_ECOUN_NAME","Per_Street",
                        "Per_Suburb","Per_Post_Code","Per_State" ,"PerFRM_ADOC_NA","PerFRM_EDOC_NA","Per_A_NAT_NAME" ,"Per_E_NAT_NAME","Per_Gender_Aname","Per_Gender_Ename","Login_id"
                       
            };

            string[] DType2 =
            {
                "System.Int32","System.String","System.String","System.Int16",  "System.Int16", "System.String", "System.String","System.String",
                "System.String", "System.Int16", "System.String","System.String" ,"System.String","System.String","System.Int16", "System.String", "System.Byte", "System.String",
                "System.String","System.Int16","System.Int16", "System.Int16", "System.String", "System.String", "System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String", "System.String","System.String","System.String","System.String","System.String","System.String","System.Int16"
                
            };

            Per_inf_TBL = CustomControls.Custom_DataTable("Per_inf_TBL", Column2, DType2);


        }
        //----------------------
        private void Customer_Add_Upd_Load(object sender, EventArgs e)
        {
            Create_Table();
            GetData();

        }
        //---------------------------------------------------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                _Bs_Grd_PerInfo.DataSource = connection.SqlExec("Exec main_person_info '" + Txt_Person_Name.Text.Trim() + "'," + 0 + "," + 0 + "," + "Person_info", "Per_Info_Tbl");
                Grd_Person_info.DataSource = _Bs_Grd_PerInfo;
            }
            catch
            { Grd_Person_info.DataSource = new BindingSource(); }
        }

        //-----------------------------------------------------------------------------------
        private void GetData()
        {
            Change = false;
            Sql_Text = " Exec all_master ";
            connection.SqlExec(Sql_Text, "all_master");
            CboCit_Id.DataSource = connection.SQLDS.Tables["all_master6"];
            CboCit_Id.ValueMember = "Cit_Id";
            CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "cit_Con_Aname" : "cit_Con_Ename";

            CboNat_id.DataSource = connection.SQLDS.Tables["all_master1"];
            CboNat_id.ValueMember = "NAt_Id";
            CboNat_id.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_eNAME";


            CboDoc_id.DataSource = connection.SQLDS.Tables["all_master2"];
            CboDoc_id.ValueMember = "Fmd_id";
            CboDoc_id.DisplayMember = connection.Lang_id == 1 ? "Fmd_aname" : "Fmd_Ename";


            Cbo_Gender.DataSource = connection.SQLDS.Tables["all_master7"];
            Cbo_Gender.ValueMember = "Gender_id";
            Cbo_Gender.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";

            Change = true;

        }
        //---------------------------------------------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (Chk_New_Authorized.Checked == false && Chk_Old_Authorized.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد نوع الاضافة باستخدام مخول معرف مسبقا او مخول جديد" : "Please Check new Authorized or Old Autorized", MyGeneral_Lib.LblCap);

                return;
            }
     
            if (Chk_New_Authorized.Checked == true)//مخول جديد
            {
                string Doc_Date = "";
                string Doc_Edate = "";
                string Birth = "";

                #region Validation
                if (TxtCust_Aname.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please enter the Arabic Name", MyGeneral_Lib.LblCap);
                    TxtCust_Aname.Focus();
                    return;
                }
                if (TxtCust_Ename.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاسم الاجنبي" : "Please enter the English Name", MyGeneral_Lib.LblCap);
                    TxtCust_Ename.Focus();
                    return;
                }


                if (Convert.ToInt16(CboCit_Id.SelectedValue) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " حدد المدينة" : "Please select the Country", MyGeneral_Lib.LblCap);
                    CboCit_Id.Focus();
                    return;
                }

                if (Convert.ToInt16(CboNat_id.SelectedValue) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " حدد الجنسية" : "Please select the Nationality", MyGeneral_Lib.LblCap);
                    CboNat_id.Focus();
                    return;
                }
                if (Txt_Occupation.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " ادخل المهنة " : "Please enter the Occupation", MyGeneral_Lib.LblCap);
                    Txt_Occupation.Focus();
                    return;
                }

                if (Convert.ToInt16(Cbo_Gender.SelectedValue) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " حدد الجنس" : "Please select the Gender", MyGeneral_Lib.LblCap);
                    Cbo_Gender.Focus();
                    return;
                }


                if (Convert.ToInt16(CboDoc_id.SelectedValue) < 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثيقة" : "Please select the type of Doc.", MyGeneral_Lib.LblCap);
                    CboDoc_id.Focus();
                    return;
                }

                if (TxtNo_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الوثيقة" : "Please enter Doc. No.", MyGeneral_Lib.LblCap);
                    TxtNo_Doc.Focus();
                    return;
                }
                if (TxtIss_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل جهة الاصدار" : "Please enter Issuer", MyGeneral_Lib.LblCap);
                    TxtIss_Doc.Focus();
                    return;
                }
                Doc_Date = TxtDoc_Da.Text;
                string Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
                if (Ord_Strt == "0" || Ord_Strt == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل تأريخ الاصدار" : "Please enter Issuer Date", MyGeneral_Lib.LblCap);
                    TxtDoc_Da.Focus();
                    return;
                }

                Doc_Edate = TxtExp_Da.Text;
                Ord_Strt = MyGeneral_Lib.DateChecking(TxtExp_Da.Text);
                if (Ord_Strt == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ نفاذ الوثيقة" : "Please make sure of expire date of formal document", MyGeneral_Lib.LblCap);
                    TxtExp_Da.Focus();
                    return;
                }
                else if (Ord_Strt == "0")
                {
                    Doc_Edate = "";
                }

                Birth = TxtBirth_Da.Text;
                string Ord_Strt1 = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
                if (Ord_Strt1 == "-1" || Ord_Strt1 == "0")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ التولد" : "Please make sure of birth date", MyGeneral_Lib.LblCap);
                    TxtBirth_Da.Focus();
                    return;
                }

                if (TxtPhone.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " ادخل الهاتف " : "Please enter the Phone NO.", MyGeneral_Lib.LblCap);
                    TxtPhone.Focus();
                    return;
                }
                if (Txt_State.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " ادخل الولاية " : "Please enter the State", MyGeneral_Lib.LblCap);
                    Txt_State.Focus();
                    return;
                }
                if (Txt_Street.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " ادخل الزقاق " : "Please enter the Street", MyGeneral_Lib.LblCap);
                    Txt_Street.Focus();
                    return;
                }

                if (Txt_Suburb.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " ادخل الحي " : "Please enter the subrub", MyGeneral_Lib.LblCap);
                    Txt_Suburb.Focus();
                    return;
                }


                #endregion
                Int16 Con_Id = Convert.ToInt16(connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["Con_ID"]);
                string Coun_K_PH = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["Coun_K_PH"].ToString();
                string ACITY_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["cit_Aname"].ToString();
                string ECITY_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["cit_Ename"].ToString();
                string ACoun_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["con_Aname"].ToString();
                string ECoun_NAME = connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["con_Ename"].ToString();
                string FRM_ADOC_NA = connection.SQLDS.Tables["all_master2"].Rows[CboDoc_id.SelectedIndex]["Fmd_Aname"].ToString();
                string FRM_EDOC_NA = connection.SQLDS.Tables["all_master2"].Rows[CboDoc_id.SelectedIndex]["Fmd_Ename"].ToString();
                string A_NAT_NAME = connection.SQLDS.Tables["all_master1"].Rows[CboNat_id.SelectedIndex]["Nat_ANAME"].ToString();
                string E_NAT_NAME = connection.SQLDS.Tables["all_master1"].Rows[CboNat_id.SelectedIndex]["Nat_ENAME"].ToString();
                string Gender_Aname = connection.SQLDS.Tables["all_master7"].Rows[Cbo_Gender.SelectedIndex]["Gender_Aname"].ToString();
                string Gender_Ename = connection.SQLDS.Tables["all_master7"].Rows[Cbo_Gender.SelectedIndex]["Gender_Ename"].ToString();


  Per_inf_TBL.Rows.Add(new object[] { Per_id, TxtCust_Aname.Text.Trim(), TxtCust_Ename.Text.Trim(),
      CboCit_Id.SelectedValue,Con_Id ,Txt_birth_place.Text.Trim(),TxtPhone.Text.Trim(),Txt_Another_Phone.Text.Trim() ,
                            TxtEmail.Text.Trim(), CboDoc_id.SelectedValue,TxtNo_Doc.Text.Trim(),Doc_Date,Doc_Edate,
                            TxtIss_Doc.Text.Trim(), CboNat_id.SelectedValue,Birth,Convert.ToByte(Cbo_Gender.SelectedValue),
                            Txt_Occupation.Text, string.Empty,Cust_Id ,connection.user_id,1,Coun_K_PH,ACITY_NAME ,
                           ECITY_NAME,ACoun_NAME,ECoun_NAME, Txt_Street.Text.Trim(), 
                          Txt_Suburb.Text.Trim(),Txt_Post_code.Text.Trim(),Txt_State.Text.Trim(),
                          FRM_ADOC_NA  ,FRM_EDOC_NA,A_NAT_NAME,E_NAT_NAME, Gender_Aname,Gender_Ename,1});
                         
            


                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Parameters.AddWithValue("@Person_Info_Type", Per_inf_TBL);
                connection.SQLCMD.Parameters.AddWithValue("@registerd_ID", 1);
                connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", 1);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SqlExec("Person_info_Add_Upd_1", connection.SQLCMD);



                 //connection.SQLCMD.Parameters.AddWithValue("@per_id", Per_id);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_AName", TxtCust_Aname.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@Per_EName", TxtCust_Ename.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_City_ID", CboCit_Id.SelectedValue);

                //connection.SQLCMD.Parameters.AddWithValue("@per_Coun_ID", Con_Id);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Address", "");

                //connection.SQLCMD.Parameters.AddWithValue("@per_Phone", TxtPhone.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Another_Phone", Txt_Another_Phone.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Email", TxtEmail.Text.Trim());

                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_ID", CboDoc_id.SelectedValue);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_NO", TxtNo_Doc.Text.Trim());

                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_Date", Doc_Date);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_EDate", Doc_Edate);

                //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_IS", TxtIss_Doc.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@per_Nationalty_ID", CboNat_id.SelectedValue);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Birth_day", Birth);

                //connection.SQLCMD.Parameters.AddWithValue("@per_Gender_ID", Convert.ToByte(Cbo_Gender.SelectedValue));
                //connection.SQLCMD.Parameters.AddWithValue("@per_Occupation", Txt_Occupation.Text);
                //connection.SQLCMD.Parameters.AddWithValue("@per_Mother_name", "");

               // //connection.SQLCMD.Parameters.AddWithValue("@cust_ID", Cust_Id );
                //connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                //connection.SQLCMD.Parameters.AddWithValue("@COUN_K_PH", Coun_K_PH);

                //connection.SQLCMD.Parameters.AddWithValue("@Per_ACITY_NAME", ACITY_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_eCITY_NAME", ECITY_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_ACOUN_NAME", ACoun_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_ECOUN_NAME", ECoun_NAME);

                //connection.SQLCMD.Parameters.AddWithValue("@Per_Street", Txt_Street.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Suburb", Txt_Suburb.Text.Trim());

                //connection.SQLCMD.Parameters.AddWithValue("@Per_Post_Code", Txt_Post_code.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@Per_State", Txt_State.Text.Trim());
                //connection.SQLCMD.Parameters.AddWithValue("@PerFRM_ADOC_NA", FRM_ADOC_NA);
                //connection.SQLCMD.Parameters.AddWithValue("@PerFRM_EDOC_NA", FRM_EDOC_NA);

                //connection.SQLCMD.Parameters.AddWithValue("@Per_A_NAT_NAME", A_NAT_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_E_NAT_NAME", E_NAT_NAME);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Gender_Aname", Gender_Aname);
                //connection.SQLCMD.Parameters.AddWithValue("@Per_Gender_ename", Gender_Ename);
                //connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", 1);




                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                        !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Per_id)))
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                connection.SQLCMD.Parameters.Clear();

            }
                 if (Chk_Old_Authorized.Checked == true && Convert.ToInt16(((DataRowView)_Bs_Grd_PerInfo.Current).Row["Chk"]) == 1)//مخول سابق
                 {
                     TBL = connection.SQLDS.Tables["Per_Info_Tbl"].DefaultView.ToTable(true, "Chk", "per_id").Select(" Chk = 1 ").CopyToDataTable();
                 }
                 if (Chk_New_Authorized.Checked == true) // مخول جديد  

                 {
                DataRow DRow = TBL.NewRow();
                DRow["Chk"] = 1;
                DRow["Per_id"] = Per_id;
                TBL.Rows.Add(DRow);
                 }

                connection.SQLCMD.Parameters.AddWithValue("@Aouthorized_TBL", TBL);
                connection.SQLCMD.Parameters.AddWithValue("@CUST_ID", Auth_cust_id);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@Param_Result", "");

                connection.SqlExec("Add_Aouthorized_Compaines", connection.SQLCMD);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                connection.SQLCMD.Parameters.Clear();
                this.Close();
            
        }
        //---------------------------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //---------------------------------------------------------------------------------------------------------
        private void Customer_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Used_Tbl = { "all_master1", "all_master", "all_master2",
                                   "all_master6","all_master7","Type_cust" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void CboDoc_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboDoc_id.SelectedIndex == 1)
            {
                TxtNo_Doc.Enabled = false; TxtNo_Doc.Text = "";
                TxtIss_Doc.Enabled = false; TxtIss_Doc.Text = "";
                TxtDoc_Da.Enabled = false; TxtDoc_Da.Text = "";
                TxtExp_Da.Enabled = false; TxtExp_Da.Text = "";

            }
            else
            {
                TxtNo_Doc.Enabled = true;
                TxtIss_Doc.Enabled = true;
                TxtDoc_Da.Enabled = true;
                TxtExp_Da.Enabled = true;
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Add_Upd_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 2;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}