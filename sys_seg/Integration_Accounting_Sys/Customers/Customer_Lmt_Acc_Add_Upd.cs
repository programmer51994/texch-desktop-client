﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Lmt_Acc_Add_Upd : Form
    {
        #region Defintion
        bool Change = false;
        bool Change1 = false;
        bool Change_ADD_Upd = false;
        DataTable _DT = new DataTable();
        BindingSource _Bs_Lmt_add   = new BindingSource();
        int Frm_Id = 0;
        int cust_id = 0;
        Int16 Acc_id = 0;
        Int16 cur_id = 0;
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Customer_Lmt_Acc_Add_Upd(int Form_Id)
        {
            InitializeComponent();
          
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
        connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            #endregion

            Frm_Id = Form_Id;
        }
        //----------------------------
        private void customer_lmt_acc_add_upd_Load(object sender, EventArgs e)
        {
            Change = false;
            Change1 = false;
            Change_ADD_Upd = false;
        if (Frm_Id == 1)
        {
        Change_ADD_Upd = true;
        TxtCust_Aname_TextChanged(null, null);
        }
        else
        {
            this.Text = connection.Lang_id == 1 ? "تعديــل الحساب الثانوي المركزي" : "Update customer limit account ";
            Edit_Record();
        }
            
        }
        //---------------------------------------------------------------------------------------------------------
        private void TxtCust_Aname_TextChanged(object sender, EventArgs e)
        {
            if (Change_ADD_Upd)
            {
                Getdata();
            }
        }
        //-------------------------------------------------------------------------------------
       private void  Getdata()
       {
         
           _DT = connection.SqlExec(" Exec Customer_limit_Account", "Customer_Lmt");
           _Bs_Lmt_add.DataSource = _DT.DefaultView.ToTable(true, "ACust_Name", "Ecust_name", "Cust_id", "A_Address", "e_address",
               "Phone", "birth_date", "frm_doc_no", "frm_doc_Da", "frm_doc_Is", "Fmd_AName", "fmd_ename" ).Select("ACust_Name like '%" + TxtCust_Name.Text + "%'").CopyToDataTable();
           Lst_Cust_id.DataSource = _Bs_Lmt_add;
           Lst_Cust_id.DisplayMember = connection.Lang_id == 1 ? "ACust_Name" : "Ecust_name";
           Lst_Cust_id.ValueMember = "Cust_id";

           TxtA_Address.DataBindings.Clear();
           TxtPhone.DataBindings.Clear();
           TxtBirth_Da.DataBindings.Clear();
           TxtNo_Doc.DataBindings.Clear();
           TxtDoc_Da.DataBindings.Clear();
           TxtIss_Doc.DataBindings.Clear();
           TxtDoc_Name.DataBindings.Clear();

           TxtA_Address.DataBindings.Add("Text", _Bs_Lmt_add, connection.Lang_id == 1 ? "A_Address" : "e_address");
           TxtPhone.DataBindings.Add("Text", _Bs_Lmt_add, "Phone");
           TxtBirth_Da.DataBindings.Add("Text", _Bs_Lmt_add, "birth_date");
           TxtNo_Doc.DataBindings.Add("Text", _Bs_Lmt_add, "frm_doc_no");
           TxtDoc_Da.DataBindings.Add("Text", _Bs_Lmt_add, "frm_doc_Da");
           TxtIss_Doc.DataBindings.Add("Text", _Bs_Lmt_add, "frm_doc_Is");
           TxtDoc_Name.DataBindings.Add("Text", _Bs_Lmt_add, connection.Lang_id == 1 ? "Fmd_AName" : "fmd_ename");
           if (connection.SQLDS.Tables["Customer_Lmt"].Rows.Count > 0)
           {
               Change = true;
               Lst_Cust_id_SelectedIndexChanged(null, null);

           }
       
       }     
        //---------------------------------------------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Frm_Id == 1)
            {
                if (Convert.ToInt16(CboCur_Id.SelectedValue) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "حدد اسم العملة" : "Enter the currency please", MyGeneral_Lib.LblCap);
                    CboCur_Id.Focus();
                    return;
                }
                if (Convert.ToInt16(Lst_Cust_id.SelectedValue) <= 0 && Frm_Id != 2)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "حدد اسم العميل" : "Enter Customer name please", MyGeneral_Lib.LblCap);
                    TxtCust_Name.Focus();
                    return;
                }
            }
            if (Convert.ToDecimal(TxtPer_Tran.Text) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل سقف المعاملة للحوالات" : "Enter Per transaction please", MyGeneral_Lib.LblCap);
                TxtPer_Tran.Focus();
                return;
            }
            if (Convert.ToDecimal(TxtDaily_Tran.Text) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل سقف المعاملة اليومي" : "Enter daily transaction please ", MyGeneral_Lib.LblCap);
                TxtDaily_Tran.Focus();
                return;
            }
            if (Convert.ToDecimal(TxtDaily_Tran.Text) < Convert.ToDecimal(TxtPer_Tran.Text))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون السقف اليومي اكبر من سقف معاملة التحويل" : "Per transaction must be greater than daily transaction", MyGeneral_Lib.LblCap);
                TxtDaily_Tran.Focus();
                return;
            }
            #endregion
            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0,  Frm_Id == 1 ? Lst_Cust_id.SelectedValue :cust_id);
            ItemList.Insert(1,  Frm_Id == 1 ? Convert.ToInt32(CboCur_Id.SelectedValue) : cur_id);
            ItemList.Insert(2, Convert.ToDecimal(TxtPer_Tran.Text.Trim()));
            ItemList.Insert(3, Convert.ToDecimal(TxtDaily_Tran.Text.Trim()));
            ItemList.Insert(4, connection.user_id);
            ItemList.Insert(5, Frm_Id);
            ItemList.Insert(6, Frm_Id == 1 ? Convert.ToInt16(Cbo_Acc_Id.SelectedValue) : Acc_id);
            ItemList.Insert(7, "");
            //MyGeneral_Lib.Copytocliptext("Add_Upd_Customers_limit_Account", ItemList);
            connection.scalar("Add_Upd_Customers_limit_Account", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                Getdata();
                TxtDaily_Tran.ResetText();
                TxtPer_Tran.ResetText();
            }
            else
                this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Lmt_Acc_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
             Change = false;
             Change1 = false;
             Change_ADD_Upd = false;
            
            string[] Str = {"Customer_Lmt" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //-------------------------------------------------------------------------------------
        private void Lst_Cust_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                Cbo_Acc_Id.DataSource = _DT.DefaultView.ToTable(true, "Acc_AName", "Acc_EName", "Acc_id","Cust_id").Select("Cust_id =" + Lst_Cust_id.SelectedValue).CopyToDataTable();
                Cbo_Acc_Id.DisplayMember = connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName";
                Cbo_Acc_Id.ValueMember = "ACC_Id";
                
                    Change1 = true;
                    Cbo_Acc_Id_SelectedIndexChanged(null, null);

               
            }
        }
       //-------------------------------------------------------------------------------------
        private void Cbo_Acc_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change1)
            {
                CboCur_Id.DataSource = _DT.DefaultView.ToTable(true, "cur_aname", "cur_ename", "cur_id", "Acc_id","Cust_id").Select("Acc_id =" + Cbo_Acc_Id.SelectedValue + "and Cust_id = "+Lst_Cust_id.SelectedValue).CopyToDataTable();
                CboCur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
                CboCur_Id.ValueMember = "Cur_id";
            }
        }
        //---------------------------------------------------------------------------------------
        private void Edit_Record()
        {
            Change = false;
            CboCur_Id.DataBindings.Clear();
            Lst_Cust_id.DataBindings.Clear();
            TxtCust_Name.DataBindings.Clear();
            TxtDaily_Tran.DataBindings.Clear();
            TxtPer_Tran.DataBindings.Clear();
            TxtA_Address.DataBindings.Clear();
            TxtPhone.DataBindings.Clear();
            TxtBirth_Da.DataBindings.Clear();
            TxtNo_Doc.DataBindings.Clear();
            TxtDoc_Da.DataBindings.Clear();
            TxtIss_Doc.DataBindings.Clear();
            TxtDoc_Name.DataBindings.Clear();
            TxtCust_Name.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, connection.Lang_id == 1 ? "Acust_Name" : "ecust_name");
            TxtDaily_Tran.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "Per_Daily");
            TxtPer_Tran.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "Per_Tran");
            TxtA_Address.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, connection.Lang_id == 1 ? "A_Address" : "e_address");
            TxtPhone.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "Phone");
            TxtBirth_Da.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "birth_date");
            TxtNo_Doc.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "frm_doc_no");
            TxtDoc_Da.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "frm_doc_Da");
            TxtIss_Doc.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, "frm_doc_Is");
            TxtDoc_Name.DataBindings.Add("Text", Customer_Lmt_Acc_Main._bs_lmt, connection.Lang_id == 1 ? "Fmd_AName" : "fmd_ename");
            String cur_name = ((DataRowView)Customer_Lmt_Acc_Main._bs_lmt_2.Current).Row["Cur_aname"].ToString();
            String Acc_name = ((DataRowView)Customer_Lmt_Acc_Main._bs_lmt_2.Current).Row["Acc_aname"].ToString();
            cur_id = Convert.ToInt16(((DataRowView)Customer_Lmt_Acc_Main._bs_lmt_2.Current).Row["cur_id"]);
            Acc_id = Convert.ToInt16(((DataRowView)Customer_Lmt_Acc_Main._bs_lmt_2.Current).Row["Acc_id_online"]);
            cust_id = Convert.ToInt16(((DataRowView)Customer_Lmt_Acc_Main._bs_lmt_2.Current).Row["Cust_id"]);
            Cbo_Acc_Id.Items.Add(Acc_name);
            CboCur_Id.Items.Add(cur_name);
            CboCur_Id.SelectedIndex = 0;
            Cbo_Acc_Id.SelectedIndex = 0;
            CboCur_Id.Enabled = false;
            Lst_Cust_id.Enabled = false;
            TxtCust_Name.Enabled = false;
            Cbo_Acc_Id.Enabled = false;
        }
        //-------------------------------------------------------------------------------------
    }
}