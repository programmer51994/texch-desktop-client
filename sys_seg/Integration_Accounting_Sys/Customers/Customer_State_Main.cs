﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Customer_State_Main : Form
    {
        #region Defintions
        int cust_Id = 0;
        byte Cust_Flag ; 
        bool change = false;
        string sql_txt = "";
        BindingSource cust_bnd = new BindingSource();

      BindingSource _Bs_Cust_state =  new BindingSource();
        #endregion
    
        public Customer_State_Main()
        {
            InitializeComponent();
          
            MyGeneral_Lib.Form_Orientation(this);
         
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column2"].DataPropertyName = "Ecust_name";
                Grd_Cust.Columns["Column3"].DataPropertyName = "Cust_Flag_Ename";
                Grd_Cust.Columns["Column6"].DataPropertyName = "DC_Ename";
                Grd_Cust.Columns["Column7"].DataPropertyName = "Con_EName";
                Grd_Cust.Columns["Column8"].DataPropertyName = "Cit_EName";
                Grd_Cust.Columns["Column9"].DataPropertyName = "E_Address";
            }
            #endregion
        }
        //----------------------------------------
        private void Custmer_State_Main_Load(object sender, EventArgs e)
        {
         
            Grd_Cust.AutoGenerateColumns = false;
            change = false;
            _Bs_Cust_state.DataSource = connection.SqlExec("Main_Customer "
                                                              + -1 + ","
                                                              + -1 + ",'"
                                                               + "" + "',"
                                                              + 0 + ","
                                                              + "Customers" + ","
                                                              + connection.Lang_id, "Customer_Tbl");

            if (connection.SQLDS.Tables["Customer_Tbl"].Rows.Count > 0)
            {

                Grd_Cust.DataSource = _Bs_Cust_state;
                change = true;
                Grd_Cust_SelectionChanged(null, null);

            }
            else
            {
                ActiveAdd.Enabled = false;
                StopBtn.Enabled = false;
            }
         
         }
        //----------------------------------------
        private void ActiveAdd_Click(object sender, EventArgs e)
        {
                object [] sparem = { cust_Id, 1 ,connection.user_id};
                connection.SqlExec("U_Customer_State", "Cust_state", sparem);
                Custmer_State_Main_Load(null, null);
        }
        //----------------------------------------
        private void StopBtn_Click(object sender, EventArgs e)
        {

                object[] sparem = { cust_Id, 0 ,connection.user_id};
                connection.SqlExec("U_Customer_State", "Cust_state", sparem);
                Custmer_State_Main_Load(null, null);
          
        }
        //----------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (change && connection.SQLDS.Tables["Customer_Tbl"].Rows.Count > 0)
            {
                LblRec.Text = connection.Records(_Bs_Cust_state);

                DataRowView DRV = _Bs_Cust_state.Current as DataRowView;
                    DataRow DR = DRV.Row;
                    Cust_Flag = DR.Field<byte>("Cust_Flag");
                    cust_Id = DR.Field<Int32>("CUST_ID");

                    if (Cust_Flag == 1)
                    {
                        ActiveAdd.Enabled = false;
                        StopBtn.Enabled = true;
                    }
                    else
                    {
                        ActiveAdd.Enabled = true;
                        StopBtn.Enabled = false;
                    }
                
            }
        }
        //----------------------------------------
        private void User_State_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
            
            string[] Tbl = { "Customer_Tbl", "Cust_state" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
              
            }
        }

        private void Customer_State_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    ActiveAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    StopBtn_Click(sender, e);
                    break;
     
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Customer_State_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            _Bs_Cust_state.DataSource = connection.SqlExec("Main_Customer "
                                                             + -1 + ","
                                                             + -1 + ",'"
                                                              + TxtCust_Name.Text.Trim() + "',"
                                                             + 0 + ","
                                                             + "Customers" + ","
                                                             + connection.Lang_id, "Customer_Tbl");

            if (connection.SQLDS.Tables["Customer_Tbl"].Rows.Count > 0)
            {

                Grd_Cust.DataSource = _Bs_Cust_state;
                change = true;
                Grd_Cust_SelectionChanged(null, null);

            }
            else
            {
                Grd_Cust.DataSource = new BindingSource();
                ActiveAdd.Enabled = false;
                StopBtn.Enabled = false;
            }
        }
    }
}