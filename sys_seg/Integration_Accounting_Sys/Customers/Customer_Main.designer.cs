﻿namespace Integration_Accounting_Sys
{
    partial class Customer_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customer_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtCust_Name = new System.Windows.Forms.ToolStripTextBox();
            this.label11 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.UpdBtn = new System.Windows.Forms.ToolStripButton();
            this.BtnDel = new System.Windows.Forms.ToolStripButton();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.SearchBtn = new System.Windows.Forms.ToolStripButton();
            this.CboCountry = new System.Windows.Forms.ToolStripComboBox();
            this.label33 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.Btn_Add_Doc_Img = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.Btn_Search = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.Btn_Add = new System.Windows.Forms.ToolStripButton();
            this.TxtCoun = new System.Windows.Forms.TextBox();
            this.TxtCity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Btn_Hst = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TxtCust_Cls = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Cust_Type = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_State = new System.Windows.Forms.TextBox();
            this.Txt_Post_code = new System.Windows.Forms.TextBox();
            this.Txt_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Street = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtNat_Name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_Another_phone = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_Occupation = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_Gender = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_Aohurized = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.AutoSize = false;
            this.TxtCust_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Name.Margin = new System.Windows.Forms.Padding(5, 0, 1, 0);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(250, 22);
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Name_TextChanged);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label11.RightToLeftAutoMirrorImage = true;
            this.label11.Size = new System.Drawing.Size(63, 27);
            this.label11.Tag = "6";
            this.label11.Text = "المدينة /البلد";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.UpdBtn,
            this.toolStripSeparator1,
            this.BtnDel,
            this.AllBtn,
            this.toolStripSeparator7,
            this.SearchBtn,
            this.TxtCust_Name,
            this.label11,
            this.CboCountry,
            this.label33,
            this.toolStripSeparator2,
            this.toolStripButton1,
            this.toolStripSeparator5,
            this.Btn_Add_Doc_Img,
            this.toolStripSeparator4,
            this.Btn_Search,
            this.toolStripSeparator6,
            this.Btn_Add});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(1104, 30);
            this.BND.TabIndex = 0;
            this.BND.Text = "bindingNavigator1";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(92, 27);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "&اضـافـة جـديـد";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // UpdBtn
            // 
            this.UpdBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.UpdBtn.Image = ((System.Drawing.Image)(resources.GetObject("UpdBtn.Image")));
            this.UpdBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdBtn.Name = "UpdBtn";
            this.UpdBtn.Size = new System.Drawing.Size(60, 27);
            this.UpdBtn.Tag = "3";
            this.UpdBtn.Text = "تعـديــل";
            this.UpdBtn.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDel.Image = global::Integration_Accounting_Sys.Properties.Resources._12;
            this.BtnDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(64, 27);
            this.BtnDel.Tag = "4";
            this.BtnDel.Text = "حـــذف";
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // AllBtn
            // 
            this.AllBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(87, 27);
            this.AllBtn.Tag = "4";
            this.AllBtn.Text = "عرض الكــل";
            this.AllBtn.Click += new System.EventHandler(this.BtnAll_Click);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(119, 27);
            this.SearchBtn.Tag = "8";
            this.SearchBtn.Text = "بـحــث اسم الثانوي:";
            this.SearchBtn.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // CboCountry
            // 
            this.CboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCountry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboCountry.Name = "CboCountry";
            this.CboCountry.Size = new System.Drawing.Size(250, 30);
            // 
            // label33
            // 
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(51, 27);
            this.label33.Text = "ملحقــــات";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(96, 22);
            this.toolStripButton1.Tag = "4";
            this.toolStripButton1.Text = "عرض الوثائق ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // Btn_Add_Doc_Img
            // 
            this.Btn_Add_Doc_Img.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add_Doc_Img.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Add_Doc_Img.Image")));
            this.Btn_Add_Doc_Img.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Btn_Add_Doc_Img.Name = "Btn_Add_Doc_Img";
            this.Btn_Add_Doc_Img.Size = new System.Drawing.Size(88, 22);
            this.Btn_Add_Doc_Img.Tag = "2";
            this.Btn_Add_Doc_Img.Text = "اضافة وثائــق";
            this.Btn_Add_Doc_Img.Click += new System.EventHandler(this.Btn_Add_Doc_Img_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // Btn_Search
            // 
            this.Btn_Search.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Search.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Search.Image")));
            this.Btn_Search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Btn_Search.Name = "Btn_Search";
            this.Btn_Search.Size = new System.Drawing.Size(109, 22);
            this.Btn_Search.Tag = "4";
            this.Btn_Search.Text = "عرض  المخولين ";
            this.Btn_Search.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Add.Image")));
            this.Btn_Add.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(85, 22);
            this.Btn_Add.Tag = "2";
            this.Btn_Add.Text = "اضافة مخول";
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // TxtCoun
            // 
            this.TxtCoun.BackColor = System.Drawing.Color.White;
            this.TxtCoun.Enabled = false;
            this.TxtCoun.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCoun.Location = new System.Drawing.Point(820, 394);
            this.TxtCoun.Name = "TxtCoun";
            this.TxtCoun.Size = new System.Drawing.Size(272, 23);
            this.TxtCoun.TabIndex = 7;
            // 
            // TxtCity
            // 
            this.TxtCity.BackColor = System.Drawing.Color.White;
            this.TxtCity.Enabled = false;
            this.TxtCity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCity.Location = new System.Drawing.Point(820, 420);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Size = new System.Drawing.Size(272, 23);
            this.TxtCity.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(727, 423);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 16);
            this.label7.TabIndex = 21;
            this.label7.Text = "الـمـــديــنـــــــــة:";
            // 
            // Btn_Hst
            // 
            this.Btn_Hst.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Hst.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Hst.Location = new System.Drawing.Point(898, 354);
            this.Btn_Hst.Name = "Btn_Hst";
            this.Btn_Hst.Size = new System.Drawing.Size(200, 25);
            this.Btn_Hst.TabIndex = 4;
            this.Btn_Hst.Text = "معلومات تاريخية";
            this.Btn_Hst.UseVisualStyleBackColor = true;
            this.Btn_Hst.Click += new System.EventHandler(this.Btn_Hst_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(751, 397);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "البــلـــــــــد:";
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            this.Grd_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1,
            this.Column12,
            this.Column10,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9});
            this.Grd_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Cust.Location = new System.Drawing.Point(7, 68);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.ReadOnly = true;
            this.Grd_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cust.Size = new System.Drawing.Size(1091, 280);
            this.Grd_Cust.TabIndex = 3;
            this.Grd_Cust.SelectionChanged += new System.EventHandler(this.Grd_Cust_SelectionChanged);
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CUST_ID";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "الرقم";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 75;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Acust_name";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "الاســـم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Cust_Flag";
            this.Column12.HeaderText = "حالته";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "birth_date";
            this.Column10.HeaderText = "تارخ التولد";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Fmd_AName";
            this.Column4.HeaderText = "نوع الوثيقة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 200;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "frm_doc_no";
            this.Column5.HeaderText = "رقم الوثيقة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "frm_doc_Is";
            this.Column6.HeaderText = "جهة الاصدار";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "frm_doc_Da";
            this.Column7.HeaderText = "تاريخ الاصدار";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "doc_eda";
            this.Column8.HeaderText = "تاريخ النفاذ";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "user_name";
            this.Column9.HeaderText = "المستخدم";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // TxtCust_Cls
            // 
            this.TxtCust_Cls.BackColor = System.Drawing.Color.White;
            this.TxtCust_Cls.Enabled = false;
            this.TxtCust_Cls.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Cls.Location = new System.Drawing.Point(90, 396);
            this.TxtCust_Cls.Name = "TxtCust_Cls";
            this.TxtCust_Cls.Size = new System.Drawing.Size(272, 23);
            this.TxtCust_Cls.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(16, 399);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 42;
            this.label4.Text = "حالة الثانوي:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 560);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1104, 22);
            this.statusStrip1.TabIndex = 65;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 385);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1092, 2);
            this.flowLayoutPanel3.TabIndex = 87;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 552);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1093, 2);
            this.flowLayoutPanel1.TabIndex = 88;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(263, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel2.TabIndex = 86;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(5, 386);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(2, 168);
            this.flowLayoutPanel5.TabIndex = 89;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(1096, 386);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(2, 168);
            this.flowLayoutPanel6.TabIndex = 90;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-3, 60);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1110, 2);
            this.flowLayoutPanel7.TabIndex = 497;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(89, 33);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(279, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(903, 33);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(195, 23);
            this.TxtIn_Rec_Date.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(849, 36);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 493;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(3, 36);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 492;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // Txt_Cust_Type
            // 
            this.Txt_Cust_Type.BackColor = System.Drawing.Color.White;
            this.Txt_Cust_Type.Enabled = false;
            this.Txt_Cust_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cust_Type.Location = new System.Drawing.Point(453, 396);
            this.Txt_Cust_Type.Name = "Txt_Cust_Type";
            this.Txt_Cust_Type.Size = new System.Drawing.Size(272, 23);
            this.Txt_Cust_Type.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(363, 399);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 501;
            this.label1.Text = "نـــوع الثانــوي:";
            // 
            // Txt_State
            // 
            this.Txt_State.BackColor = System.Drawing.Color.White;
            this.Txt_State.Enabled = false;
            this.Txt_State.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_State.Location = new System.Drawing.Point(820, 449);
            this.Txt_State.Name = "Txt_State";
            this.Txt_State.Size = new System.Drawing.Size(272, 23);
            this.Txt_State.TabIndex = 524;
            // 
            // Txt_Post_code
            // 
            this.Txt_Post_code.BackColor = System.Drawing.Color.White;
            this.Txt_Post_code.Enabled = false;
            this.Txt_Post_code.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Post_code.Location = new System.Drawing.Point(820, 525);
            this.Txt_Post_code.Name = "Txt_Post_code";
            this.Txt_Post_code.Size = new System.Drawing.Size(272, 23);
            this.Txt_Post_code.TabIndex = 523;
            // 
            // Txt_Suburb
            // 
            this.Txt_Suburb.BackColor = System.Drawing.Color.White;
            this.Txt_Suburb.Enabled = false;
            this.Txt_Suburb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Suburb.Location = new System.Drawing.Point(820, 475);
            this.Txt_Suburb.Name = "Txt_Suburb";
            this.Txt_Suburb.Size = new System.Drawing.Size(272, 23);
            this.Txt_Suburb.TabIndex = 522;
            // 
            // Txt_Street
            // 
            this.Txt_Street.BackColor = System.Drawing.Color.White;
            this.Txt_Street.Enabled = false;
            this.Txt_Street.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Street.Location = new System.Drawing.Point(820, 500);
            this.Txt_Street.Name = "Txt_Street";
            this.Txt_Street.Size = new System.Drawing.Size(272, 23);
            this.Txt_Street.TabIndex = 521;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(774, 452);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 16);
            this.label25.TabIndex = 520;
            this.label25.Text = "الولاية:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(740, 528);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 16);
            this.label24.TabIndex = 519;
            this.label24.Text = "الرمز البريدي:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(739, 478);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 16);
            this.label23.TabIndex = 518;
            this.label23.Text = "الحـــــــــــــي:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(774, 503);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 16);
            this.label22.TabIndex = 517;
            this.label22.Text = "زقاق :";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(728, 386);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(2, 168);
            this.flowLayoutPanel8.TabIndex = 90;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Maroon;
            this.label26.Location = new System.Drawing.Point(740, 377);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 14);
            this.label26.TabIndex = 538;
            this.label26.Text = "العنــــــــــوان...";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(8, 425);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 14);
            this.label10.TabIndex = 539;
            this.label10.Text = "معلومات شخصية .....";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 435);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(725, 2);
            this.flowLayoutPanel4.TabIndex = 88;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(8, 528);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 16);
            this.label13.TabIndex = 44;
            this.label13.Text = "الايميــــــل:";
            // 
            // TxtEmail
            // 
            this.TxtEmail.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Enabled = false;
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(89, 525);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(272, 23);
            this.TxtEmail.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(8, 450);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 16);
            this.label8.TabIndex = 499;
            this.label8.Text = "الجنسيــــة:";
            // 
            // TxtNat_Name
            // 
            this.TxtNat_Name.BackColor = System.Drawing.Color.White;
            this.TxtNat_Name.Enabled = false;
            this.TxtNat_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNat_Name.Location = new System.Drawing.Point(89, 447);
            this.TxtNat_Name.Name = "TxtNat_Name";
            this.TxtNat_Name.Size = new System.Drawing.Size(210, 23);
            this.TxtNat_Name.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(8, 502);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 503;
            this.label9.Text = "رقم الهــاتـــف:";
            // 
            // TxtPhone
            // 
            this.TxtPhone.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Enabled = false;
            this.TxtPhone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(89, 499);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(210, 23);
            this.TxtPhone.TabIndex = 502;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(402, 502);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 16);
            this.label5.TabIndex = 505;
            this.label5.Text = "رقم الهــاتــــف الاخر:";
            // 
            // Txt_Another_phone
            // 
            this.Txt_Another_phone.BackColor = System.Drawing.Color.White;
            this.Txt_Another_phone.Enabled = false;
            this.Txt_Another_phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Another_phone.Location = new System.Drawing.Point(513, 499);
            this.Txt_Another_phone.Name = "Txt_Another_phone";
            this.Txt_Another_phone.Size = new System.Drawing.Size(210, 23);
            this.Txt_Another_phone.TabIndex = 504;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(456, 450);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 16);
            this.label14.TabIndex = 507;
            this.label14.Text = "المهــــنة :";
            // 
            // Txt_Occupation
            // 
            this.Txt_Occupation.BackColor = System.Drawing.Color.White;
            this.Txt_Occupation.Enabled = false;
            this.Txt_Occupation.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Occupation.Location = new System.Drawing.Point(513, 447);
            this.Txt_Occupation.Name = "Txt_Occupation";
            this.Txt_Occupation.Size = new System.Drawing.Size(210, 23);
            this.Txt_Occupation.TabIndex = 506;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(8, 476);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 16);
            this.label15.TabIndex = 525;
            this.label15.Text = "الجنس :";
            // 
            // Txt_Gender
            // 
            this.Txt_Gender.BackColor = System.Drawing.Color.White;
            this.Txt_Gender.Enabled = false;
            this.Txt_Gender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Gender.Location = new System.Drawing.Point(89, 473);
            this.Txt_Gender.Name = "Txt_Gender";
            this.Txt_Gender.Size = new System.Drawing.Size(210, 23);
            this.Txt_Gender.TabIndex = 526;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(457, 476);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 16);
            this.label16.TabIndex = 527;
            this.label16.Text = "له مخول :";
            // 
            // Txt_Aohurized
            // 
            this.Txt_Aohurized.BackColor = System.Drawing.Color.White;
            this.Txt_Aohurized.Enabled = false;
            this.Txt_Aohurized.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Aohurized.Location = new System.Drawing.Point(513, 473);
            this.Txt_Aohurized.Name = "Txt_Aohurized";
            this.Txt_Aohurized.Size = new System.Drawing.Size(210, 23);
            this.Txt_Aohurized.TabIndex = 528;
            // 
            // Customer_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 582);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Txt_Aohurized);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_Gender);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_State);
            this.Controls.Add(this.Txt_Post_code);
            this.Controls.Add(this.Txt_Suburb);
            this.Controls.Add(this.Txt_Street);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.Txt_Occupation);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_Another_phone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Txt_Cust_Type);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtNat_Name);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TxtCust_Cls);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.TxtCoun);
            this.Controls.Add(this.TxtCity);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Btn_Hst);
            this.Controls.Add(this.label6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customer_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "154";
            this.Text = "Customer_Main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Customer_Main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Customer_Main_FormClosed);
            this.Load += new System.EventHandler(this.Customer_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Customer_Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripTextBox TxtCust_Name;
        private System.Windows.Forms.ToolStripLabel label11;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripButton SearchBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton UpdBtn;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.TextBox TxtCoun;
        private System.Windows.Forms.TextBox TxtCity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Btn_Hst;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripComboBox CboCountry;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.TextBox TxtCust_Cls;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Cust_Type;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton BtnDel;
        private System.Windows.Forms.TextBox Txt_State;
        private System.Windows.Forms.TextBox Txt_Post_code;
        private System.Windows.Forms.TextBox Txt_Suburb;
        private System.Windows.Forms.TextBox Txt_Street;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStripButton Btn_Add;
        private System.Windows.Forms.ToolStripButton Btn_Search;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtNat_Name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txt_Another_phone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_Occupation;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txt_Gender;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txt_Aohurized;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.ToolStripLabel label33;
        private System.Windows.Forms.ToolStripButton Btn_Add_Doc_Img;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;

    }
}