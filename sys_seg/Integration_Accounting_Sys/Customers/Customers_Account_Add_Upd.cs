﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Customers_Account_Add_Upd : Form
    {
        #region Defintion
        bool Change = false;
        bool CurChange = false;
        string Sql_Text = "";
        int MCust_Id = 0;
        int Edit_Cust_Id = 0;
        int Frm_Id = 0;
        int T_ID = 0;
        int Cust_id = 0;
        int Cus_acc_id = 0;
        int GrD_Cust_id = 0;
        BindingSource BS = new BindingSource();
        BindingSource bs_1 = new BindingSource();
        BindingSource bs_2 = new BindingSource();
        int Cust_CLs = 0;
        #endregion

        //---------------------------------------------------------------------------------------------------------
        public Customers_Account_Add_Upd(int Form_Id, string T_Name, int MT_Id, int Mcust_id)
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            #endregion
            Grd_Cust.AutoGenerateColumns = false;
            Grd_cur_Id.AutoGenerateColumns = false;
            Grd_Acc_Id.AutoGenerateColumns = false;

            Grd_Cust.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Cust.MultiSelect = false;

            Grd_cur_Id.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_cur_Id.MultiSelect = false;

            Grd_Acc_Id.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Acc_Id.MultiSelect = false;


           // label6.Text = T_Name;
            Frm_Id = Form_Id;
            T_ID = MT_Id;
            Cust_id = Mcust_id;
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["column2"].DataPropertyName = "ECust_Name";
                Grd_cur_Id.Columns["column4"].DataPropertyName = "Cur_Ename";
                Grd_Acc_Id.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "Acc_Ename";
            }


        }
        //---------------------------------------------------------------------------------------------------------
        private void customer_lmt_acc_add_upd_Load(object sender, EventArgs e)
        {
            string SqlTxt = "";
            SqlTxt = "select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  "
               + " From CUSTOMERS A, TERMINALS B   "
               + " Where A.CUST_ID = B.CUST_ID  "
               + " And A.Cust_Flag <> 0 ";
            cbo_term.Enabled = true;

            cbo_term.DataSource = connection.SqlExec(SqlTxt, "Term_tbl");
            cbo_term.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "Ecust_name";
            cbo_term.ValueMember = "T_id";
            
            Sql_Text = " Select DC_ANAME,DC_ENAME,DC_ID FROM Debt_Credit order by DC_ANAME";
            CboDC_ID.DataSource = connection.SqlExec(Sql_Text, "DebtCredit_Tbl");
            CboDC_ID.ValueMember = "DC_ID";
            CboDC_ID.DisplayMember = connection.Lang_id == 1 ? "DC_ANAME" : "DC_ENAME";
            if (Frm_Id == 1)
            {
                TxtCust_Aname_TextChanged(sender, e);
            }

            if (Frm_Id == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديــل الحساب الثانوي " : "Update customer account ";
                Edit_Record();

            }



        }
        //---------------------------
        private void TxtCust_Aname_TextChanged(object sender, EventArgs e)
        {
            Change = false;

            BS.DataSource = connection.SqlExec("Exec Get_Customers_Account_Add_INfo '" + TxtCust_Name.Text.Trim() + "','" + Cust_id + "'", "Customer_Lmt");

            if (Edit_Cust_Id > 0)
            {
                BS.DataSource = connection.SQLDS.Tables["Customer_Lmt"].DefaultView.ToTable().Select("Cust_Id = " + Edit_Cust_Id).CopyToDataTable();
            }

            if (connection.SQLDS.Tables["Customer_Lmt"].Rows.Count > 0)
            {
                Grd_Cust.DataSource = BS;

                #region DataBinding
                TxtCust_Type.DataBindings.Clear();
                TxtA_Address.DataBindings.Clear();
                TxtPhone.DataBindings.Clear();
                TxtBirth_Da.DataBindings.Clear();
                TxtNo_Doc.DataBindings.Clear();
                TxtDoc_Da.DataBindings.Clear();
                TxtIss_Doc.DataBindings.Clear();
                TxtDoc_Name.DataBindings.Clear();
                TxtCust_Type.DataBindings.Add("Text", BS, connection.Lang_id == 1 ? "Cust_Type_AName" : "Cust_Type_EName");
                TxtA_Address.DataBindings.Add("Text", BS, connection.Lang_id == 1 ? "A_Address" : "E_Address");
                TxtPhone.DataBindings.Add("Text", BS, "Phone");
                TxtBirth_Da.DataBindings.Add("Text", BS, "Birth_Date");
                TxtNo_Doc.DataBindings.Add("Text", BS, "FRM_DOC_NO");
                TxtDoc_Da.DataBindings.Add("Text", BS, "frm_doc_Da");
                TxtIss_Doc.DataBindings.Add("Text", BS, "frm_doc_Is");
                TxtDoc_Name.DataBindings.Add("Text", BS, connection.Lang_id == 1 ? "Fmd_AName" : "Fmd_EName");
                #endregion
            }
            else
            {
                Grd_Cust.DataSource = new BindingSource();
                TxtCust_Type.DataBindings.Clear();
                TxtA_Address.DataBindings.Clear();
                TxtPhone.DataBindings.Clear();
                TxtBirth_Da.DataBindings.Clear();
                TxtNo_Doc.DataBindings.Clear();
                TxtDoc_Da.DataBindings.Clear();
                TxtIss_Doc.DataBindings.Clear();
                TxtDoc_Name.DataBindings.Clear();
                BS.DataSource = new BindingSource();


            }
            if (connection.SQLDS.Tables["Customer_Lmt"].Rows.Count > 0)
            {
                Change = true;
                Grd_Cust_SelectionChanged(sender, e);
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (BS.List.Count > 0)
            {
                DataRowView DVR = BS.Current as DataRowView;
                DataRow Dr = DVR.Row;
                GrD_Cust_id = Dr.Field<Byte>("CUS_Cls_ID");
                if (GrD_Cust_id < 2)
                {
                    TxtUpper_Lmt.Enabled = false;
                    TxtUpper_Lmt.Text = "0.000";
                }
                else
                {
                    TxtUpper_Lmt.Enabled = true;
                }
                Txt_cur_name.Text = "";
                Txt_acc_name.Text = "";
                Txt_cur_name_TextChanged(null, null);
            }
            else
            {

                bs_1.DataSource = new BindingSource();
                bs_2.DataSource = new BindingSource();
            }
        }


        //---------------------------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if ((Grd_Cust.RowCount) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الثانوي" : "Select customer", MyGeneral_Lib.LblCap);
                TxtCust_Name.Focus();
                return;
            }

            if (Convert.ToInt16(Grd_cur_Id.RowCount) <= 0)
            {

                MessageBox.Show(connection.Lang_id == 1 ? "الـــرجاء ادخال العملة" : "Pleace Enter Currency ", MyGeneral_Lib.LblCap);
                Txt_cur_name.Focus();
                return;
            }

            if (Grd_Acc_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الـــرجاء ادخال أسم الحساب " : "Pleace Enter Account Name ", MyGeneral_Lib.LblCap);
                Txt_acc_name.Focus();
                return;
            }

            if (Convert.ToInt16(CboDC_ID.SelectedValue) < -1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الـــرجاء تحدد طبيعة الثانوي" : "Pleace Enter Customer Type ", MyGeneral_Lib.LblCap);
                CboDC_ID.Focus();
                return;
            }

            #endregion

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Cus_acc_id);
            ItemList.Insert(1, Frm_Id== 1? cbo_term.SelectedValue : T_ID);
            ItemList.Insert(2, MCust_Id);
            ItemList.Insert(3, ((DataRowView)bs_1.Current).Row["cur_id"]);
            ItemList.Insert(4, ((DataRowView)bs_2.Current).Row["Acc_id"]);
            ItemList.Insert(5, Cust_CLs);
            ItemList.Insert(6, CboDC_ID.SelectedValue);
            ItemList.Insert(7, Convert.ToDecimal(TxtUpper_Lmt.Text));
            ItemList.Insert(8, Convert.ToInt16(label22.Checked));
            ItemList.Insert(9, TxtBank_No.Text);
            ItemList.Insert(10, Frm_Id);
            ItemList.Insert(11, connection.user_id);
            ItemList.Insert(12, "");
            connection.scalar("Add_Upd_Customers_Account", ItemList);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                Change = false;
                CurChange = false;
                TxtCust_Name.Text = "";
                CboDC_ID.SelectedIndex = 0;
                TxtUpper_Lmt.ResetText();
                TxtBank_No.Text = "";
                TxtCust_Aname_TextChanged(sender, e);
                label22.Checked = false;

            }
            else
                this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void Edit_Record()
        {

            DataRowView DRV1 = Customer_Account_Main._Bs_Account_Main.Current as DataRowView;
            DataRow DR1 = DRV1.Row;
            Cus_acc_id = DR1.Field<int>("Cus_acc_id");
            Edit_Cust_Id = DR1.Field<int>("Cust_id");
            TxtCust_Name.Text = DR1.Field<string>(connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name");
            Change = false;
            CurChange = false;
            Txt_cur_name.Text = DR1.Field<Int16>("Cur_Id").ToString();
            Txt_cur_name_TextChanged(null, null);
            Txt_acc_name.Text = DR1.Field<int>("Acc_Id").ToString();
            Txt_acc_name_TextChanged(null, null);
            CboDC_ID.SelectedValue = DR1.Field<Int16>("DC_Id");
            label22.Checked = Convert.ToBoolean(DR1.Field<byte>("CUS_STATE_ID"));
            TxtUpper_Lmt.Text = DR1.Field<decimal>("Upper_limit").ToString();
            TxtBank_No.Text = DR1.Field<string>("Bank_No").ToString();
            TxtCust_Name.Enabled = false;
            Txt_cur_name.Enabled = false;
            Grd_cur_Id.Enabled = false;
            Txt_acc_name.Enabled = false;
            Grd_Acc_Id.Enabled = false;
            Grd_Cust.Enabled = false;
            cbo_term.SelectedValue = T_ID;
            cbo_term.Enabled = false;


        }
        //---------------------------------------------------------------------------------------------------------
        private void Customers_Account_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            CurChange = false;
            string[] Str = { "Cust_AccTree_Tbl", "Cust_Cur_Tbl", "Customer_Lmt", "DebtCredit_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }

        }
        //---------------------------------------------------------------------------------------------------------
        private void Customers_Account_Add_Upd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //------------------------------------------------------------------------
        private void Customers_Account_Add_Upd_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 2;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Txt_cur_name_TextChanged(object sender, EventArgs e)
        {

            string cond = "";
            if (Txt_cur_name.Text != "")
            {

                int curr_id = 0;
                if (int.TryParse(Txt_cur_name.Text.ToString(), out curr_id))
                { cond = " and cur_id = " + curr_id; }
                else
                {
                    cond = " and (Cur_ANAME  like '" + Txt_cur_name.Text.Trim() + "%'" + "or" + " Cur_ENAME like '" + Txt_cur_name.Text.Trim() + "%'" + ")";
                }
            }

            Sql_Text = " SELECT Cur_ANAME,Cur_ENAME,Cur_ID From Cur_Tbl  where cur_id <> 0 " + cond;
            bs_1.DataSource = connection.SqlExec(Sql_Text, "Cust_Cur_Tbl");

            if (connection.SQLDS.Tables["Cust_Cur_Tbl"].Rows.Count > 0)
            {

                Grd_cur_Id.DataSource = bs_1;
            }
            else
            {
                bs_1.DataSource = new BindingSource();
                Grd_cur_Id.DataSource = new BindingSource();
            }


            Grd_cur_Id_SelectionChanged_1(null, null);

        }

        //------------------------------------------------------------------------
        private void Grd_cur_Id_SelectionChanged_1(object sender, EventArgs e)
        {
            Txt_acc_name.Text = "";
            Txt_acc_name_TextChanged(null, null);
        }
        //------------------------------------------------------------------------
        private void Txt_acc_name_TextChanged(object sender, EventArgs e)
        {
            if (Grd_cur_Id.RowCount > 0)
            {

                DataRowView DRV = BS.Current as DataRowView;
                DataRow DR = DRV.Row;
                string SqlCond = Frm_Id == 1 ? "not" : "";
                MCust_Id = DR.Field<int>("Cust_Id");
                Cust_CLs = DR.Field<Byte>("CUS_Cls_ID");
                string CondStr = Frm_Id == 1 ? " And Cur_Id = " + ((DataRowView)bs_1.Current).Row["cur_id"] : "";

                string cond = "";
                if (Txt_acc_name.Text != "")
                {
                    int accc_id = 0;
                    if (int.TryParse(Txt_acc_name.Text.ToString(), out accc_id))
                    { cond = " and acc_id = " + accc_id; }
                    else
                    {
                        cond = " and (Acc_Aname  like '" + Txt_acc_name.Text.Trim() + "%'" + "or" + " Acc_Ename like '" + Txt_acc_name.Text.Trim() + "%'" + ")";
                    }
                }

                Sql_Text = " SELECT Acc_Id,Acc_Aname,Acc_Ename From Account_Tree  where RECORD_FLAG <> 0 and sub_flag = 1"
                + " and  Acc_id " + SqlCond + " in(SELECT Acc_Id from Customers_Accounts"
                                       + " Where Cust_Id = " + MCust_Id
                                            + " And T_Id = " + T_ID
                                            + CondStr + ")"
                                            + cond
                                            + " Order by " + (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename");

                bs_2.DataSource = connection.SqlExec(Sql_Text, "Cust_AccTree_Tbl");

                if (connection.SQLDS.Tables["Cust_AccTree_Tbl"].Rows.Count > 0)
                {

                    Grd_Acc_Id.DataSource = bs_2;
                }
                else
                {
                    bs_2.DataSource = new BindingSource();
                    Grd_Acc_Id.DataSource = new BindingSource();
                }
            }
        }

        private void Grd_Acc_Id_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Grd_cur_Id_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label22_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void TxtBank_No_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label15_Click(object sender, EventArgs e)
        {

        }

        private void TxtA_Address_TextChanged(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel12_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label16_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label17_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CboDC_ID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Label18_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void TxtUpper_Lmt_TextChanged(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }

        private void TxtCust_Type_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void TxtPhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label10_Click(object sender, EventArgs e)
        {

        }

        private void TxtBirth_Da_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void Label12_Click(object sender, EventArgs e)
        {

        }

        private void TxtNo_Doc_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void TxtDoc_Da_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void Label13_Click(object sender, EventArgs e)
        {

        }

        private void TxtIss_Doc_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label14_Click(object sender, EventArgs e)
        {

        }

        private void TxtDoc_Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void Grd_Cust_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void flowLayoutPanel14_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}


       

       
