﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Main : Form
    {
        #region MyRegion
        bool change = false;
        string Sql_Text = "";
        public static byte ISAuthorized = 0;
        public static string Per_id_Str = "";
        public static BindingSource _Bs_Customers = new BindingSource();
        #endregion        
        //-----------------------------------------------------
        public Customer_Main()
        {
            InitializeComponent();
       
            MyGeneral_Lib.Form_Orientation(this);
          connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Cust.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column4"].DataPropertyName = "Fmd_EName";
                //Grd_Cust.Columns["Column15"].DataPropertyName = "DC_Ename";
                Grd_Cust.Columns["Column1"].DataPropertyName = "Ecust_name";
            }
            #endregion
        }
        //-----------------------------------------------------
        private void Customer_Main_Load(object sender, EventArgs e)
        {
            change = false;
         
            //Sql_Text = " select Con_Aname,Con_Ename,Con_Id  from Con_Tbl  where con_id in(select con_id  from cit_tbl "
            //         + " where cit_id in(select Cit_ID from CUSTOMERS))"
            //         + " union "
            //         + " select ' (جميع البلدان) ' as Con_Aname,' (All Countries) ' as Con_Ename,-1 as Con_id order by " + (connection.Lang_id == 1 ? "Con_Aname" : "Con_Ename");


            Sql_Text = " Select Distinct (Cit_Aname+'_'+Con_Aname) as Cit_Con_AName ,(Cit_Ename+'_'+Con_Ename) as Cit_Con_EName,C.Con_Id ,C.CIT_ID   "
                     + " from Con_Tbl A , Cit_Tbl B ,  CUSTOMERS C "
                     + " where A.Con_ID = C.Con_Id "
                     + " and B.cit_id = C.Cit_id "
                     + " and A.Con_ID = B.Con_ID "
                     + " union  "
                     + " select ' (جميع البلدان) ' as Cit_Con_AName,' (All Countries) ' as Cit_Con_EName,-1 as Con_id ,-1 as CIT_ID "
                     + " order by Cit_Con_AName ";

            CboCountry.ComboBox.DataSource = connection.SqlExec(Sql_Text, "Country_Tbl");
            CboCountry.ComboBox.DisplayMember = connection.Lang_id == 1 ? "Cit_Con_AName" : "Cit_Con_EName";
            CboCountry.ComboBox.ValueMember = "Con_Id";
            change = true;
           // CboCountry_SelectedIndexChanged_1(sender, e);
            //-----------------
            TxtCust_Name.Text = "";
            Grd_Bind();
            if (_Bs_Customers.List.Count > 0)
            {
                Btn_Hst.Enabled = true;
                UpdBtn.Enabled = true;
            }
            else
            {
                Btn_Hst.Enabled = false;
                UpdBtn.Enabled = false;
            }
           
        }
        //-----------------------------------------------------
        private void Grd_Bind()
        {
            change = false;
            int index = Convert.ToInt32(CboCountry.ComboBox.SelectedIndex);
            Int16 Cit_id = Convert.ToInt16(connection.SQLDS.Tables["Country_Tbl"].Rows[index]["CIT_ID"]);


            _Bs_Customers.DataSource = connection.SqlExec("Main_Customer "
                                                            + CboCountry.ComboBox.SelectedValue + ","
                                                            + Cit_id + ",'"
                                                            + TxtCust_Name.Text.Trim() + "',"
                                                            + 0 + ","
                                                            + "'Customers'" + ","
                                                            + connection.Lang_id, "Customer_Tbl");

            if (connection.SQLDS.Tables["Customer_Tbl"].Rows.Count > 0)
            {
                Grd_Cust.DataSource = _Bs_Customers;
                change = true;
                Grd_Cust_SelectionChanged(null, null);

            }
                TxtCoun.DataBindings.Clear();
                TxtCity.DataBindings.Clear();
                TxtNat_Name.DataBindings.Clear();
              //  TxtAddress.DataBindings.Clear();
                TxtPhone.DataBindings.Clear();
                TxtEmail.DataBindings.Clear();
                Txt_Cust_Type.DataBindings.Clear();
                TxtCust_Cls.DataBindings.Clear();
                Txt_Another_phone.DataBindings.Clear();
                Txt_Aohurized.DataBindings.Clear();
                Txt_Gender.DataBindings.Clear();
                Txt_Occupation.DataBindings.Clear();
                Txt_Post_code.DataBindings.Clear();
                Txt_State.DataBindings.Clear();
                Txt_Street.DataBindings.Clear();
                Txt_Suburb.DataBindings.Clear();
                TxtCoun.DataBindings.Add("Text", _Bs_Customers, connection.Lang_id == 1 ? "Con_AName" : "Con_EName");
                TxtCity.DataBindings.Add("Text", _Bs_Customers, connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName");
                TxtNat_Name.DataBindings.Add("Text", _Bs_Customers, connection.Lang_id == 1 ? "Nat_Aname" : "Nat_Ename");
             //   TxtAddress.DataBindings.Add("Text", connection.SQLBSGrd, connection.Lang_id == 1 ? "A_Address" : "E_Address");
                TxtPhone.DataBindings.Add("Text", _Bs_Customers, "Phone");
                TxtEmail.DataBindings.Add("Text", _Bs_Customers, "Email");
                TxtCust_Cls.DataBindings.Add("Text", _Bs_Customers, "CUS_Cls_Name");
                Txt_Cust_Type.DataBindings.Add("Text", _Bs_Customers, connection.Lang_id == 1 ? "Cust_Type_AName" : "Cust_Type_EName");
                Txt_Another_phone.DataBindings.Add("Text", _Bs_Customers, "Another_Phone");
                Txt_Aohurized.DataBindings.Add("Text", _Bs_Customers, "Authorized_Flag_Aname");
                Txt_Gender.DataBindings.Add("Text", _Bs_Customers, "Gender_aname");
                Txt_Occupation.DataBindings.Add("Text", _Bs_Customers, "Occupation");
                Txt_Post_code.DataBindings.Add("Text", _Bs_Customers, "Per_Post_Code");
                Txt_State.DataBindings.Add("Text", _Bs_Customers, "Per_State");
                Txt_Street.DataBindings.Add("Text", _Bs_Customers, "Per_Street");
                Txt_Suburb.DataBindings.Add("Text", _Bs_Customers, "Per_Suburb");
     
        }
        //-----------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (change)
            {
                LblRec.Text = connection.Records(_Bs_Customers);
                if (Convert.ToInt16(((DataRowView)_Bs_Customers.Current).Row["Authorized_Flag"]) == 0)
                {
                    Btn_Add.Enabled = false  ;
                    Btn_Search.Enabled = false;
                }
                else
                { Btn_Add.Enabled = true ;
                Btn_Search.Enabled = true ;
                }
            }

        }
        //-----------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            change = false;
            Customer_Add_Upd AddFrm = new Customer_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this) ;
            TxtCust_Name.Text = "";
            Customer_Main_Load(sender, e);
            this.Visible = true;
        }
        //-----------------------------------------------------
        private void BtnEdit_Click(object sender, EventArgs e)
        {
            change = false;
            Customer_Add_Upd AddFrm = new Customer_Add_Upd(2);
             this.Visible = false;
             AddFrm.ShowDialog(this);
             Customer_Main_Load(sender, e);
             this.Visible = true;
        }      
        //-----------------------------------------------------
        //private void CboCountry_SelectedIndexChanged_1(object sender, EventArgs e)
        //{
            
        //    if (change)
        //    {
        //        Sql_Text = " select cit_Aname , Cit_Ename, cit_id from cit_tbl "
        //                    + " where cit_id in( select Cit_ID from CUSTOMERS) "
        //                    + " and con_id = " + CboCountry.ComboBox.SelectedValue
        //                    + " union "
        //                    + " select ' (جميع المدن) ' as Cit_Aname,' (All Cities) ' as Cit_Ename,-1 as cit_id order by " + (connection.Lang_id == 1 ? "cit_Aname" : "cit_Ename");
        //                CboCity.ComboBox.DataSource = connection.SqlExec(Sql_Text, "City_TBL");
        //                CboCity.ComboBox.DisplayMember = connection.Lang_id == 1 ? "cit_Aname" : "cit_Ename";
        //                CboCity.ComboBox.ValueMember = "cit_id";
        //    }
            
        //}
        //-----------------------------------------------------
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            Grd_Bind();
           // TxtCust_Name.Text = "";
            //CboCountry.ComboBox.SelectedIndex = 0;
          //  change = true;
           // CboCountry_SelectedIndexChanged_1(null, null);
        }
        //-----------------------------------------------------
        private void BtnAll_Click(object sender, EventArgs e)
        {
            //TxtCust_Name.Text = "";
            //CboCountry.ComboBox.SelectedIndex = 0;
           
            Grd_Bind();
        }
        //-----------------------------------------------------
        private void Btn_Hst_Click(object sender, EventArgs e)
        {
            change = false;
            DataRowView DRV = _Bs_Customers.Current as DataRowView;
            DataRow DR = DRV.Row;

            int Cust_Id = DR.Field<int>("Cust_ID");
            Hst_Customer_Main HstFrm = new Hst_Customer_Main(Cust_Id);
            this.Visible = false;
            HstFrm.ShowDialog(this);
            this.Visible = true;
        }
        //-----------------------------------------------------
        private void Customer_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;

            string[] Used_Tbl = { "Country_Tbl", "City_TBL", "Customer_Tbl", "per_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
 //-----------------------------------------------------
        private void Customer_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    BtnEdit_Click(sender, e);
                    break;
                case Keys.F5:
                    BtnSearch_Click(sender, e);
                    break;
                case Keys.F4:
                    BtnAll_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            Grd_Bind();
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            int form_id =  3 ;
            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, "");
            ItemList.Insert(1, "");
            ItemList.Insert(2, 0);
            ItemList.Insert(3, 0);
            ItemList.Insert(4,"");
            ItemList.Insert(5, "");
            ItemList.Insert(6, "");
            ItemList.Insert(7, "");
            ItemList.Insert(8, "");
            ItemList.Insert(9, "");
            ItemList.Insert(10, "");
            ItemList.Insert(11, "");
            ItemList.Insert(12, "");
            ItemList.Insert(13, 0);
            ItemList.Insert(14, 0);
            ItemList.Insert(15, 0);
            ItemList.Insert(16, 0);           
            ItemList.Insert(17, 0);
            ItemList.Insert(18, Convert.ToInt16(((DataRowView)_Bs_Customers.Current).Row["Cust_id"]));
            ItemList.Insert(19, connection.user_id);
            ItemList.Insert(20, form_id);               //form_id
            ItemList.Insert(21, 0);
            ItemList.Insert(22, 0);
            ItemList.Insert(23, "");
            ItemList.Insert(24, "");
            ItemList.Insert(25, "");
            ItemList.Insert(26, "");
            ItemList.Insert(27, "");
            ItemList.Insert(28, "");
            ItemList.Insert(29, 0);
            ItemList.Insert(30, "");

            MyGeneral_Lib.Copytocliptext("Add_Upd_Customer", ItemList);
            connection.scalar("Add_Upd_Customer", ItemList);
            int max_cust_id = 0;
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                       !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out max_cust_id)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                //this.Enabled = true;
                return;
            }

            connection.SQLCMD.Parameters.Clear();
            Customer_Main_Load(null, null);
        }

        private void Customer_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            int Cust_id = Convert.ToInt32(((DataRowView)_Bs_Customers.Current).Row["Cust_id"]);
            Authorized_Add_Upd AddFrm = new Authorized_Add_Upd(Cust_id);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            this.Visible = true;
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           

            ISAuthorized = 1; 
            string Sq_Text = "Select A.*  From "
            + " Person_Info A, Aouthorized_Compaines B "
            + " where A.per_id = B.Aouthorized_Per_id "
            + " and B.CUST_ID = " + Convert.ToInt32(((DataRowView)_Bs_Customers.Current).Row["Cust_id"]);
            connection.SqlExec(Sq_Text, "per_Tbl");//جلب الاسماء من جدول البرسن لهكذا كسوتمر
            if (connection.SQLDS.Tables["per_Tbl"].Rows.Count > 0)
            {
                Person_Info_Main AddFrm = new Person_Info_Main();
                this.Visible = false;
                AddFrm.ShowDialog(this);
                this.Visible = true;
            }
            else

            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا يوجد مخولين معرفين لهذا العميل" : "No Authorized Person Defined for this customer", MyGeneral_Lib.LblCap); 
                return;
            
            }
        }

        private void Btn_Add_Doc_Img_Click(object sender, EventArgs e)
        {
            Int16 CUS_Cls_ID = Convert.ToInt16(((DataRowView)_Bs_Customers.Current).Row["CUS_Cls_ID"]);
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1 && CUS_Cls_ID == 2)//عام ويملك خاصية رفع الوثائق
            {


                int cust_id = Convert.ToInt32(((DataRowView)_Bs_Customers.Current).Row["cust_id"]);
                string Acust_name = ((DataRowView)_Bs_Customers.Current).Row["ACust_Name"].ToString();
                string Ecust_name = ((DataRowView)_Bs_Customers.Current).Row["ECust_Name"].ToString();
                Int16  Fmd_id = Convert.ToInt16(((DataRowView)_Bs_Customers.Current).Row["Fmd_id"]);

                add_Upd_Document UpdFrm = new add_Upd_Document(Acust_name, cust_id, Fmd_id, Ecust_name);
                this.Visible = false;
                UpdFrm.ShowDialog(this);
                Customer_Main_Load(sender, e);
                this.Visible = true;

            }
            else
            {
                if (CUS_Cls_ID == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الثانوي خاص لا يملك خاصية اضافة الوثائق" : "the customer is speical", MyGeneral_Lib.LblCap);
                    return;
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " ", MyGeneral_Lib.LblCap);
                    return;
                }

            }
        }
       

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
             Int16 CUS_Cls_ID = Convert.ToInt16(((DataRowView)_Bs_Customers.Current).Row["CUS_Cls_ID"]);
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1 && CUS_Cls_ID == 2)//عام ويملك خاصية رفع الوثائق
            {

                int cust_id = Convert.ToInt32(((DataRowView)_Bs_Customers.Current).Row["cust_id"]);
                string Acust_name = ((DataRowView)_Bs_Customers.Current).Row["ACust_Name"].ToString();
                string Ecust_name = ((DataRowView)_Bs_Customers.Current).Row["ECust_Name"].ToString();


                Document_main UpdFrm = new Document_main(Acust_name, cust_id, Ecust_name);
                this.Visible = false;
                UpdFrm.ShowDialog(this);
                Customer_Main_Load(sender, e);
                this.Visible = true;
            }
            else
            {
                if (CUS_Cls_ID == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الثانوي خاص لا يملك خاصية اضافة الوثائق" : "the customer is speical", MyGeneral_Lib.LblCap);
                    return;
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " ", MyGeneral_Lib.LblCap);
                    return;
                }
            }
        }

       

     
    }
}
