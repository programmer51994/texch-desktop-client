﻿using System;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Hst_Customer_Main : Form
    {
        #region MyRegion
        bool change = false;
        int MCust_Id;
        #endregion
        
        public Hst_Customer_Main(int HCust_Id)
        {
            InitializeComponent();
            connection.SQLBSMainGrd.DataSource = new BindingSource();
            MCust_Id = HCust_Id;
          MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page( TxtUser, TxtIn_Rec_Date);
            Hst_Grd_Cust.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Hst_Grd_Cust.Columns["Column10"].DataPropertyName = "Fmd_EName";
                Hst_Grd_Cust.Columns["Column3"].DataPropertyName = "DC_Ename";
                Hst_Grd_Cust.Columns["Column1"].DataPropertyName = "Ecust_name";
            }
            #endregion
        }
        //---------------------------------------------------------------------------------------------------------
        private void Hst_Customer_Main_Load(object sender, EventArgs e)
        {
            change = false;
            connection.SQLBSMainGrd.DataSource = connection.SqlExec("Main_Customer "
                                                            + -1 + ","
                                                            + -1 + ",'"
                                                            + "" + "',"
                                                            + MCust_Id + ","
                                                            + "'Hst_Customers'" + ","
                                                            + connection.Lang_id, "Hst_Customer_Tbl");

            if (connection.SQLDS.Tables["Hst_Customer_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات تاريخية لهذا العميل" : "No History Info. for this Customer");
                this.Close();
            }

           
            Hst_Grd_Cust.DataSource = connection.SQLBSMainGrd;
            TxtCoun.DataBindings.Clear();
            TxtCity.DataBindings.Clear();
            TxtNat_Name.DataBindings.Clear();
            TxtAddress.DataBindings.Clear();
            TxtPhone.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            TxtCust_Cls.DataBindings.Clear();
            TxtCust_Cls.DataBindings.Clear();
            TxtCoun.DataBindings.Add("Text", connection.SQLBSMainGrd, connection.Lang_id == 1 ? "Con_AName" : "Con_EName");
            TxtCity.DataBindings.Add("Text", connection.SQLBSMainGrd, connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName");
            TxtNat_Name.DataBindings.Add("Text", connection.SQLBSMainGrd, connection.Lang_id == 1 ? "Nat_Aname" : "Nat_Ename");
            TxtAddress.DataBindings.Add("Text", connection.SQLBSMainGrd, connection.Lang_id == 1 ? "A_Address" : "E_Address");
            TxtPhone.DataBindings.Add("Text", connection.SQLBSMainGrd, "Phone");
            TxtEmail.DataBindings.Add("Text", connection.SQLBSMainGrd, "Email");
            TxtCust_Cls.DataBindings.Add("Text", connection.SQLBSMainGrd, "CUS_Cls_Name");
            Txt_Cust_Type.DataBindings.Add("Text", connection.SQLBSMainGrd, connection.Lang_id == 1 ? "Cust_Type_AName" : "Cust_Type_EName");
     
            if (connection.SQLDS.Tables["Hst_Customer_Tbl"].Rows.Count > 0)
            {
                change = true;
                Hst_Grd_Cust_SelectionChanged(null, null);
            }
        }
        //-----------------------
        private void Hst_Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (change)
            {
                LblRec.Text = connection.Records(connection.SQLBSMainGrd);
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Hst_Customer_Main_FormClosed(object sender, FormClosedEventArgs e)
        {

            change = false;
            
            string[] Used_Tbl = { "Hst_Customer_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
               
            }
        }

     
    }
}
