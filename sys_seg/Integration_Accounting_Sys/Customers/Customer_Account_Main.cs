﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Account_Main : Form
    {
        #region Defintion && Constructor
        string Sql_Text = "";
        bool Change = false;
      public static   BindingSource _Bs_Account_Main = new BindingSource() ;
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Customer_Account_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            this.GrdCurrency_Cust.AutoGenerateColumns = false;
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }
        //---------------------------------------------------------------------------------------------------------
        private void Get_Data()
        {

            Sql_Text = " SELECT Cur_ID,cur_aname,cur_ename from Cur_Tbl where cur_id in(select cur_id from customers_Accounts ) "
                        + " union "
                     + " select -1 as cur_id,' (جميع العملات) ' as cur_aname,' (all currencies) ' as cur_ename from Cur_Tbl order by  " + (connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename");
            connection.SqlExec(Sql_Text, "Curr_Tbl");
            CboCur_Id.ComboBox.DataSource = connection.SQLDS.Tables["Curr_Tbl"];
            CboCur_Id.ComboBox.ValueMember = "Cur_ID";
            CboCur_Id.ComboBox.DisplayMember = connection.Lang_id == 1 ? "cur_aname" : "cur_ename";

            Sql_Text = "SELECT  Acc_AName,Acc_EName,Acc_Id from  Account_Tree where Acc_Id in(select acc_Id from customers_Accounts ) "
                + " union "
                + " select ' (جميع الحسابات) ' as Acc_AName,' (None) ' as Acc_EName,-1 as acc_Id from  account_Tree order by "+ (connection.Lang_id==1?"Acc_Aname":"Acc_Ename");
            connection.SqlExec(Sql_Text, "Acc_Tbl");
            CboAcc_Name.ComboBox.DataSource = connection.SQLDS.Tables["Acc_Tbl"];
            CboAcc_Name.ComboBox.ValueMember = "acc_Id";
            CboAcc_Name.ComboBox.DisplayMember = connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName";
        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Account_Main_Load(object sender, EventArgs e)
        {
            #region DataPropertyName
            if (connection.Lang_id != 1)
            {
                GrdCurrency_Cust.Columns["Column1"].DataPropertyName = "ECust_Name";
                GrdCurrency_Cust.Columns["Column2"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Cust.Columns["Column4"].DataPropertyName = "Acc_EName";
                GrdCurrency_Cust.Columns["Column7"].DataPropertyName = "DC_Ename";
            }
            #endregion
            Get_Data();
            TxtCust_Name.Text = "";

            Change = false;
            Sql_Text = "SELECT T_Id,ACust_Name,ECust_Name,A.cust_id From CUSTOMERS A,TERMINALS B  Where A.CUST_ID = B.CUST_ID And B.TERM_STATE_ID<>0 order by " + (connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name");
            CboTerminal_id.DataSource = connection.SqlExec(Sql_Text, "Terminal_Tbl");
            CboTerminal_id.ValueMember = "T_id";
            CboTerminal_id.DisplayMember = connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name";
            if (connection.SQLDS.Tables["Terminal_Tbl"].Rows.Count > 0)
            {

                Change = true;
                CboTerminal_id_SelectedIndexChanged(sender, e);

            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الفرع اولا" : "Please Enter Terminal Name First", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void CboTerminal_id_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (Change)
            {


                object[] Sparams = { Convert.ToInt16(CboTerminal_id.SelectedValue), Convert.ToInt16(CboCur_Id.ComboBox.SelectedValue), 
                                       TxtCust_Name.Text, CboAcc_Name.ComboBox.SelectedValue, 0, "Customers_Accounts"};

                _Bs_Account_Main.DataSource = connection.SqlExec("Main_Customer_Account", "Cust_Acc_Tbl", Sparams);
                GrdCurrency_Cust.DataSource = _Bs_Account_Main;
                if (GrdCurrency_Cust.Rows.Count == 0)
                {
                    UpdBtn.Enabled = false;
                }
                else
                { UpdBtn.Enabled = true; }



                TxtPhone.DataBindings.Clear();
                TxtA_Address.DataBindings.Clear();


                TxtPhone.DataBindings.Add("Text", _Bs_Account_Main, "Phone");
                TxtA_Address.DataBindings.Add("Text", _Bs_Account_Main, connection.Lang_id == 1 ? "A_Address" : "E_Address");

                if (connection.SQLDS.Tables["Cust_Acc_Tbl"].Rows.Count > 0)
                {

                    GrdCurrency_Cust_SelectionChanged(sender, e);
                }
                else
                {
                    Btn_Hst.Enabled = false;
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void GrdCurrency_Cust_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Account_Main);
            if (GrdCurrency_Cust.RowCount > 0)
                Btn_Hst.Enabled = true;
            else
                Btn_Hst.Enabled = false;
        }
        //---------------------------------------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            DataTable DT = connection.SQLDS.Tables["Terminal_Tbl"].DefaultView.ToTable(false, "Cust_id", "T_id").Select("T_Id =" + CboTerminal_id.SelectedValue).CopyToDataTable();
            int cust_id = Convert.ToInt32(DT.Rows[0]["Cust_id"]);
            Customers_Account_Add_Upd AddFrm = new Customers_Account_Add_Upd(1, CboTerminal_id.Text, Convert.ToInt16(CboTerminal_id.SelectedValue), cust_id);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Customer_Account_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {

            Customers_Account_Add_Upd UpdFrm = new Customers_Account_Add_Upd(2, CboTerminal_id.Text, Convert.ToInt16(CboTerminal_id.SelectedValue), 0);
            this.Visible = false;
            UpdFrm.ShowDialog(this);
            Customer_Account_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            CboTerminal_id_SelectedIndexChanged(sender, e);
            TxtCust_Name.Text = ""; CboCur_Id.SelectedIndex = 0; CboAcc_Name.ComboBox.SelectedValue = 0;

        }
        //---------------------------------------------------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            TxtCust_Name.Text = ""; CboCur_Id.ComboBox.SelectedIndex = 0; CboAcc_Name.ComboBox.SelectedValue = 0;
            CboTerminal_id_SelectedIndexChanged(sender, e);
        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Account_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;

            string[] Used_Tbl = { "Cust_Acc_Tbl", "Terminal_Tbl", "Acc_Tbl", "Curr_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Btn_Hst_Click(object sender, EventArgs e)
        {
            DataRowView DRV = _Bs_Account_Main.Current as DataRowView;
            DataRow DR = DRV.Row;
            int Cust_Id = DR.Field<int>("Cust_Id");
            int cur_id = DR.Field<Int16>("Cur_Id");
            string name = DR.Field<string>("acust_name");
            Hst_Customer_Account_Main HSTFrm = new Hst_Customer_Account_Main(Cust_Id, Convert.ToInt32(CboTerminal_id.SelectedValue), cur_id, name);
            this.Visible = false;
            HSTFrm.ShowDialog(this);
            Customer_Account_Main_Load(null, null);
            this.Visible = true;
        }

        private void Customer_Account_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {

            CboTerminal_id_SelectedIndexChanged(sender, e);
            // TxtCust_Name.Text = ""; CboCur_Id.SelectedIndex = 0; CboAcc_Name.SelectedIndex = 0;
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Convert.ToInt16(((DataRowView)_Bs_Account_Main.Current).Row["CUS_ACC_ID"]));
            ItemList.Insert(1, connection.T_ID);
            ItemList.Insert(2, Convert.ToInt16(((DataRowView)_Bs_Account_Main.Current).Row["Cust_id"]));
            ItemList.Insert(3, Convert.ToInt16(((DataRowView)_Bs_Account_Main.Current).Row["Cur_id"]));
            ItemList.Insert(4, Convert.ToInt32(((DataRowView)_Bs_Account_Main.Current).Row["Acc_id"]));
            ItemList.Insert(5, 0);
            ItemList.Insert(6, 0);
            ItemList.Insert(7, 0);
            ItemList.Insert(8, 0);
            ItemList.Insert(9, "");
            ItemList.Insert(10, 3);// frm_id = 3 حذف
            ItemList.Insert(11, connection.user_id);
            ItemList.Insert(12, "");
            connection.scalar("Add_Upd_Customers_Account", ItemList);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
               
            }
            connection.SQLCMD.Parameters.Clear();
            Customer_Account_Main_Load(null, null);
        }

        private void Customer_Account_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}
