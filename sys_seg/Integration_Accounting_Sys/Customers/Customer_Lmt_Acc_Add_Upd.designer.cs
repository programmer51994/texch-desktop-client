﻿namespace Integration_Accounting_Sys
{
    partial class Customer_Lmt_Acc_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label19 = new System.Windows.Forms.Label();
            this.TxtCust_Name = new System.Windows.Forms.TextBox();
            this.Lst_Cust_id = new System.Windows.Forms.ListBox();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtA_Address = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtBirth_Da = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtDoc_Da = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtDoc_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label23 = new System.Windows.Forms.Label();
            this.CboCur_Id = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Cbo_Acc_Id = new System.Windows.Forms.ComboBox();
            this.TxtDaily_Tran = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPer_Tran = new System.Windows.Forms.Sample.DecimalTextBox();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(11, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 14);
            this.label19.TabIndex = 88;
            this.label19.Text = "الـثـانـــــوي:";
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.BackColor = System.Drawing.Color.White;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCust_Name.Location = new System.Drawing.Point(81, 42);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(288, 22);
            this.TxtCust_Name.TabIndex = 3;
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Aname_TextChanged);
            // 
            // Lst_Cust_id
            // 
            this.Lst_Cust_id.BackColor = System.Drawing.Color.White;
            this.Lst_Cust_id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Lst_Cust_id.FormattingEnabled = true;
            this.Lst_Cust_id.ItemHeight = 14;
            this.Lst_Cust_id.Location = new System.Drawing.Point(81, 64);
            this.Lst_Cust_id.Name = "Lst_Cust_id";
            this.Lst_Cust_id.Size = new System.Drawing.Size(288, 158);
            this.Lst_Cust_id.TabIndex = 4;
            this.Lst_Cust_id.SelectedIndexChanged += new System.EventHandler(this.Lst_Cust_id_SelectedIndexChanged);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(298, 343);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 27);
            this.AddBtn.TabIndex = 14;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(386, 343);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 27);
            this.ExtBtn.TabIndex = 15;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(381, 77);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 126;
            this.label1.Text = "العنــــــــــوان :";
            // 
            // TxtA_Address
            // 
            this.TxtA_Address.BackColor = System.Drawing.Color.White;
            this.TxtA_Address.Enabled = false;
            this.TxtA_Address.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtA_Address.Location = new System.Drawing.Point(472, 74);
            this.TxtA_Address.Name = "TxtA_Address";
            this.TxtA_Address.Size = new System.Drawing.Size(265, 22);
            this.TxtA_Address.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(382, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 14);
            this.label4.TabIndex = 134;
            this.label4.Text = "الهاتـــــــــــف:";
            // 
            // TxtPhone
            // 
            this.TxtPhone.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Enabled = false;
            this.TxtPhone.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtPhone.Location = new System.Drawing.Point(474, 104);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(88, 22);
            this.TxtPhone.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(566, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 14);
            this.label8.TabIndex = 128;
            this.label8.Text = "تاريخ التولـــد:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // TxtBirth_Da
            // 
            this.TxtBirth_Da.BackColor = System.Drawing.Color.White;
            this.TxtBirth_Da.Enabled = false;
            this.TxtBirth_Da.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtBirth_Da.Location = new System.Drawing.Point(651, 104);
            this.TxtBirth_Da.Mask = "0000/00/00";
            this.TxtBirth_Da.Name = "TxtBirth_Da";
            this.TxtBirth_Da.PromptChar = ' ';
            this.TxtBirth_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtBirth_Da.Size = new System.Drawing.Size(86, 22);
            this.TxtBirth_Da.TabIndex = 7;
            this.TxtBirth_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(381, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 14);
            this.label5.TabIndex = 136;
            this.label5.Text = "رقم الوثيقـــــة:";
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.BackColor = System.Drawing.Color.White;
            this.TxtNo_Doc.Enabled = false;
            this.TxtNo_Doc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtNo_Doc.Location = new System.Drawing.Point(474, 133);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(88, 22);
            this.TxtNo_Doc.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(566, 136);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 14);
            this.label9.TabIndex = 137;
            this.label9.Text = "تاريخ الاصدار:";
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.BackColor = System.Drawing.Color.White;
            this.TxtDoc_Da.Enabled = false;
            this.TxtDoc_Da.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtDoc_Da.Location = new System.Drawing.Point(650, 133);
            this.TxtDoc_Da.Mask = "0000/00/00";
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.PromptChar = ' ';
            this.TxtDoc_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDoc_Da.Size = new System.Drawing.Size(87, 22);
            this.TxtDoc_Da.TabIndex = 9;
            this.TxtDoc_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(381, 169);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 14);
            this.label6.TabIndex = 140;
            this.label6.Text = "جهــــــة الاصدار:";
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.BackColor = System.Drawing.Color.White;
            this.TxtIss_Doc.Enabled = false;
            this.TxtIss_Doc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtIss_Doc.Location = new System.Drawing.Point(491, 166);
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(246, 22);
            this.TxtIss_Doc.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(380, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 14);
            this.label7.TabIndex = 129;
            this.label7.Text = "نوع الوثيقــــــــة:";
            // 
            // TxtDoc_Name
            // 
            this.TxtDoc_Name.BackColor = System.Drawing.Color.White;
            this.TxtDoc_Name.Enabled = false;
            this.TxtDoc_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtDoc_Name.Location = new System.Drawing.Point(491, 195);
            this.TxtDoc_Name.Name = "TxtDoc_Name";
            this.TxtDoc_Name.Size = new System.Drawing.Size(246, 22);
            this.TxtDoc_Name.TabIndex = 11;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(378, 222);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(367, 1);
            this.flowLayoutPanel3.TabIndex = 142;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(377, 61);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(367, 1);
            this.flowLayoutPanel1.TabIndex = 143;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(377, 62);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 161);
            this.flowLayoutPanel5.TabIndex = 143;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(744, 61);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 161);
            this.flowLayoutPanel7.TabIndex = 144;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(377, 243);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 14);
            this.label23.TabIndex = 148;
            this.label23.Text = "نوع الـعـمـلـة:";
            // 
            // CboCur_Id
            // 
            this.CboCur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCur_Id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.CboCur_Id.FormattingEnabled = true;
            this.CboCur_Id.Location = new System.Drawing.Point(462, 239);
            this.CboCur_Id.Name = "CboCur_Id";
            this.CboCur_Id.Size = new System.Drawing.Size(283, 22);
            this.CboCur_Id.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(385, 310);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(146, 14);
            this.label12.TabIndex = 151;
            this.label12.Text = "السقف اليومي للتحويل:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(24, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 14);
            this.label11.TabIndex = 150;
            this.label11.Text = "سقف معاملة التحويل:";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.AutoScrollMargin = new System.Drawing.Size(40, 40);
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(18, 336);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Padding = new System.Windows.Forms.Padding(0, 6, 5, 0);
            this.flowLayoutPanel9.Size = new System.Drawing.Size(728, 1);
            this.flowLayoutPanel9.TabIndex = 153;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.AutoScrollMargin = new System.Drawing.Size(40, 40);
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(18, 295);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Padding = new System.Windows.Forms.Padding(0, 6, 5, 0);
            this.flowLayoutPanel10.Size = new System.Drawing.Size(728, 1);
            this.flowLayoutPanel10.TabIndex = 154;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(17, 295);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(1, 42);
            this.flowLayoutPanel11.TabIndex = 155;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-828, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel12.TabIndex = 86;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(745, 295);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(1, 42);
            this.flowLayoutPanel13.TabIndex = 156;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-828, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel14.TabIndex = 86;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(25, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 14);
            this.label10.TabIndex = 125;
            this.label10.Text = "السقوف العامة واليومية...";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-1, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(769, 2);
            this.flowLayoutPanel2.TabIndex = 525;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(112, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(183, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(576, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(161, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(520, 7);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 521;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(4, 7);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(106, 14);
            this.label2.TabIndex = 520;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(4, 243);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 14);
            this.label13.TabIndex = 527;
            this.label13.Text = "اسم الحســـــاب :";
            // 
            // Cbo_Acc_Id
            // 
            this.Cbo_Acc_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Acc_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Acc_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Acc_Id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Cbo_Acc_Id.FormattingEnabled = true;
            this.Cbo_Acc_Id.Location = new System.Drawing.Point(113, 239);
            this.Cbo_Acc_Id.Name = "Cbo_Acc_Id";
            this.Cbo_Acc_Id.Size = new System.Drawing.Size(256, 22);
            this.Cbo_Acc_Id.TabIndex = 526;
            this.Cbo_Acc_Id.SelectedIndexChanged += new System.EventHandler(this.Cbo_Acc_Id_SelectedIndexChanged);
            // 
            // TxtDaily_Tran
            // 
            this.TxtDaily_Tran.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtDaily_Tran.Location = new System.Drawing.Point(537, 306);
            this.TxtDaily_Tran.Name = "TxtDaily_Tran";
            this.TxtDaily_Tran.NumberDecimalDigits = 3;
            this.TxtDaily_Tran.NumberDecimalSeparator = ".";
            this.TxtDaily_Tran.NumberGroupSeparator = ",";
            this.TxtDaily_Tran.Size = new System.Drawing.Size(205, 22);
            this.TxtDaily_Tran.TabIndex = 13;
            this.TxtDaily_Tran.Text = "0.000";
            // 
            // TxtPer_Tran
            // 
            this.TxtPer_Tran.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtPer_Tran.Location = new System.Drawing.Point(163, 306);
            this.TxtPer_Tran.Name = "TxtPer_Tran";
            this.TxtPer_Tran.NumberDecimalDigits = 3;
            this.TxtPer_Tran.NumberDecimalSeparator = ".";
            this.TxtPer_Tran.NumberGroupSeparator = ",";
            this.TxtPer_Tran.Size = new System.Drawing.Size(212, 22);
            this.TxtPer_Tran.TabIndex = 12;
            this.TxtPer_Tran.Text = "0.000";
            // 
            // Customer_Lmt_Acc_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 389);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Cbo_Acc_Id);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.flowLayoutPanel13);
            this.Controls.Add(this.flowLayoutPanel11);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtDaily_Tran);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtPer_Tran);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.CboCur_Id);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtA_Address);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtBirth_Da);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtDoc_Name);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.Lst_Cust_id);
            this.Controls.Add(this.TxtCust_Name);
            this.Controls.Add(this.label19);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customer_Lmt_Acc_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "169";
            this.Text = "Customer_Lmt_Acc_Add_Upd";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Customer_Lmt_Acc_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.customer_lmt_acc_add_upd_Load);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel13.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TxtCust_Name;
        private System.Windows.Forms.ListBox Lst_Cust_id;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtA_Address;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox TxtBirth_Da;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox TxtDoc_Da;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtDoc_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox CboCur_Id;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDaily_Tran;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPer_Tran;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox Cbo_Acc_Id;
    }
}