﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Hst_Customer_Lmt_Acc_Main : Form
    {
        #region Defintion
        bool Change = false;
        string Sql_Text;
        BindingSource _bs_hst_lmt = new BindingSource();
        BindingSource _bs_hst_lmt_2 = new BindingSource();
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Hst_Customer_Lmt_Acc_Main()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["column1"].DataPropertyName = "ECust_name";
                Grd_Cust.Columns["Column3"].DataPropertyName = "Fmd_Ename";
                GrdCurrency_Cust.Columns["Column8"].DataPropertyName = "Cur_Ename";
            }
            Grd_Cust.AutoGenerateColumns = false;
            GrdCurrency_Cust.AutoGenerateColumns = false;
        }
        //---------------------------------------------------------------------------------------------------------
        private void customers_limit_account_main_Load(object sender, EventArgs e)
        {
            Grd_Bind();
        }
        //---------------------------------------------------------------------------------------------------------
        private void Grd_Bind()
        {
            Change = false;
            DataRowView DRV = Customer_Lmt_Acc_Main._bs_lmt.Current as DataRowView;
            DataRow DR = DRV.Row;
            int Cust_id = DR.Field<int>("Cust_Id");
            object[] Sparam = { "", Cust_id, "Hst_Customers_limit_Account" };
            _bs_hst_lmt.DataSource = connection.SqlExec("Main_Customers_limit_Account", "Hst_Customer_lmt_Tbl", Sparam);
            Grd_Cust.DataSource = _bs_hst_lmt;
            if (connection.SQLDS.Tables["Hst_Customer_lmt_Tbl"].Rows.Count > 0)
            {
                Change = true;
                Grd_Cust_SelectionChanged(null, null);
            }
            else
            {
                MessageBox.Show(connection.Lang_id ==1 ?"لا توجد معلومات تاريخية لهذا الثانوي":"No History information for this customer", MyGeneral_Lib.LblCap);
                this.Close();
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                DataRowView Drv = Customer_Lmt_Acc_Main._bs_lmt_2.Current as DataRowView;
                DataRow Dr = Drv.Row;
                int Cust_Id = Dr.Field<int>("Cust_Id");

                Sql_Text = " select Cur_AName,Cur_EName,Per_Daily,Per_Tran, "
                           + " USER_NAME,A.Cur_Id,cust_id,B.USER_ID "
                           + " from Cur_Tbl A ,Hst_customers_limit_Account B,Users C "
                           + " where A.Cur_Id = B.Cur_Id AND B.USER_ID = C.USER_ID and cust_id = " + Cust_Id;

                _bs_hst_lmt_2.DataSource = connection.SqlExec(Sql_Text, "Hst_Currency_Tbl");
                GrdCurrency_Cust.DataSource = _bs_hst_lmt_2;

                DataRow DR = Drv.Row;
                TxtCoun.Text = Dr.Field<string>(connection.Lang_id == 1 ? "Con_Aname" : "Con_Ename");
                TxtCity.Text = Dr.Field<string>(connection.Lang_id == 1 ? "Cit_Aname" : "Cit_Ename");
                TxtAddress.Text = Dr.Field<string>(connection.Lang_id != 2 ? "A_ADDRESS":"E_ADDRESS");
                TxtBirth.Text = Dr.Field<string>("birth_date");
                TxtPhone.Text = Dr.Field<string>("PHONE");
                TxtEmail.Text = Dr.Field<string>("EMAIL");
                //.............
                LblRec.Text = connection.Records();
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void customers_limit_account_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
           

            string[] Used_Tbl = { "Hst_Customer_lmt_Tbl", "Hst_Currency_Tbl" }; ;
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
               
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}