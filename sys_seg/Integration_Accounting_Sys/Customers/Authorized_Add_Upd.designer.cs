﻿namespace Integration_Accounting_Sys
{
    partial class Authorized_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtCust_Aname = new System.Windows.Forms.TextBox();
            this.TxtCust_Ename = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.CboDoc_id = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CboNat_id = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CboCit_Id = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Cbo_Gender = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_Occupation = new System.Windows.Forms.TextBox();
            this.Txt_Another_Phone = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_Street = new System.Windows.Forms.TextBox();
            this.Txt_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Post_code = new System.Windows.Forms.TextBox();
            this.Txt_State = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.Txt_Mother_name = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Person_info = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Person_Name = new System.Windows.Forms.TextBox();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.TxtExp_Da = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtBirth_Da = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtDoc_Da = new Integration_Accounting_Sys.MyDateTextBox();
            this.Chk_Old_Authorized = new System.Windows.Forms.CheckBox();
            this.Chk_New_Authorized = new System.Windows.Forms.CheckBox();
            this.Txt_birth_place = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Person_info)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtCust_Aname
            // 
            this.TxtCust_Aname.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Aname.Location = new System.Drawing.Point(96, 230);
            this.TxtCust_Aname.Name = "TxtCust_Aname";
            this.TxtCust_Aname.Size = new System.Drawing.Size(321, 23);
            this.TxtCust_Aname.TabIndex = 2;
            // 
            // TxtCust_Ename
            // 
            this.TxtCust_Ename.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Ename.Location = new System.Drawing.Point(537, 230);
            this.TxtCust_Ename.Name = "TxtCust_Ename";
            this.TxtCust_Ename.Size = new System.Drawing.Size(321, 23);
            this.TxtCust_Ename.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(3, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 16);
            this.label4.TabIndex = 53;
            this.label4.Text = "الاســـم العربــي:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(443, 233);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 54;
            this.label5.Text = "الاســم الاجنبـــي:";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(432, 629);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 19;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(343, 629);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 26);
            this.AddBtn.TabIndex = 18;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(524, 499);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 16);
            this.label16.TabIndex = 64;
            this.label16.Text = "تاريخ الاصدار:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(5, 525);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 16);
            this.label17.TabIndex = 63;
            this.label17.Text = "تاريخ النفـــــــاذ:";
            // 
            // CboDoc_id
            // 
            this.CboDoc_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDoc_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDoc_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDoc_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDoc_id.FormattingEnabled = true;
            this.CboDoc_id.Location = new System.Drawing.Point(96, 468);
            this.CboDoc_id.Name = "CboDoc_id";
            this.CboDoc_id.Size = new System.Drawing.Size(252, 24);
            this.CboDoc_id.TabIndex = 9;
            this.CboDoc_id.SelectedIndexChanged += new System.EventHandler(this.CboDoc_id_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(8, 472);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 84;
            this.label13.Text = "نوع الوثيــقــــة:";
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(608, 469);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(252, 23);
            this.TxtNo_Doc.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(517, 472);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 60;
            this.label14.Text = "رقم الوثيقـــــــــة:";
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(96, 496);
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(252, 23);
            this.TxtIss_Doc.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(9, 499);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 16);
            this.label15.TabIndex = 59;
            this.label15.Text = "جـهـة الاصــدار:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(517, 367);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 62;
            this.label10.Text = "تاريخ التولــــد:";
            // 
            // CboNat_id
            // 
            this.CboNat_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboNat_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboNat_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboNat_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNat_id.FormattingEnabled = true;
            this.CboNat_id.Location = new System.Drawing.Point(96, 334);
            this.CboNat_id.Name = "CboNat_id";
            this.CboNat_id.Size = new System.Drawing.Size(252, 24);
            this.CboNat_id.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(5, 338);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 16);
            this.label11.TabIndex = 66;
            this.label11.Text = "الجنسيــــــــــــة:";
            // 
            // TxtPhone
            // 
            this.TxtPhone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(96, 393);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(252, 23);
            this.TxtPhone.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(9, 396);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 16);
            this.label12.TabIndex = 47;
            this.label12.Text = "رقــم الهــــاتــف:";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(608, 304);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(252, 23);
            this.TxtEmail.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(517, 309);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 16);
            this.label9.TabIndex = 48;
            this.label9.Text = "البريد الالكتروني:";
            // 
            // CboCit_Id
            // 
            this.CboCit_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCit_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCit_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCit_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCit_Id.FormattingEnabled = true;
            this.CboCit_Id.Location = new System.Drawing.Point(96, 305);
            this.CboCit_Id.Name = "CboCit_Id";
            this.CboCit_Id.Size = new System.Drawing.Size(252, 24);
            this.CboCit_Id.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(4, 309);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 16);
            this.label7.TabIndex = 37;
            this.label7.Text = "المدينـة / البلـــد :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-4, 624);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(870, 2);
            this.flowLayoutPanel1.TabIndex = 85;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(0, 31);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(869, 2);
            this.flowLayoutPanel7.TabIndex = 497;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(96, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(250, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(641, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(216, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(543, 7);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(81, 16);
            this.label3.TabIndex = 493;
            this.label3.Text = "التــــــاريــــــخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(8, 7);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 492;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(21, 367);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 502;
            this.label1.Text = "الجنــــــــس:";
            // 
            // Cbo_Gender
            // 
            this.Cbo_Gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Gender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Gender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Gender.FormattingEnabled = true;
            this.Cbo_Gender.Location = new System.Drawing.Point(96, 363);
            this.Cbo_Gender.Name = "Cbo_Gender";
            this.Cbo_Gender.Size = new System.Drawing.Size(252, 24);
            this.Cbo_Gender.TabIndex = 503;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(517, 338);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 504;
            this.label8.Text = "المهـــــــــــــنة :";
            // 
            // Txt_Occupation
            // 
            this.Txt_Occupation.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Occupation.Location = new System.Drawing.Point(608, 335);
            this.Txt_Occupation.Name = "Txt_Occupation";
            this.Txt_Occupation.Size = new System.Drawing.Size(252, 23);
            this.Txt_Occupation.TabIndex = 505;
            // 
            // Txt_Another_Phone
            // 
            this.Txt_Another_Phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Another_Phone.Location = new System.Drawing.Point(96, 421);
            this.Txt_Another_Phone.Name = "Txt_Another_Phone";
            this.Txt_Another_Phone.Size = new System.Drawing.Size(252, 23);
            this.Txt_Another_Phone.TabIndex = 507;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(10, 416);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 32);
            this.label21.TabIndex = 508;
            this.label21.Text = "رقــم الهــــاتــف\r\n الثانــــــــــــي:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(49, 597);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 16);
            this.label22.TabIndex = 509;
            this.label22.Text = "زقاق :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(517, 570);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 16);
            this.label23.TabIndex = 510;
            this.label23.Text = "الحــــــــــــــي:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(527, 597);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 16);
            this.label24.TabIndex = 511;
            this.label24.Text = "الرمز البريدي:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(5, 570);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 16);
            this.label25.TabIndex = 512;
            this.label25.Text = "الولايــــــــــــــة:";
            // 
            // Txt_Street
            // 
            this.Txt_Street.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Street.Location = new System.Drawing.Point(96, 594);
            this.Txt_Street.Name = "Txt_Street";
            this.Txt_Street.Size = new System.Drawing.Size(252, 23);
            this.Txt_Street.TabIndex = 513;
            // 
            // Txt_Suburb
            // 
            this.Txt_Suburb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Suburb.Location = new System.Drawing.Point(608, 567);
            this.Txt_Suburb.Name = "Txt_Suburb";
            this.Txt_Suburb.Size = new System.Drawing.Size(252, 23);
            this.Txt_Suburb.TabIndex = 514;
            // 
            // Txt_Post_code
            // 
            this.Txt_Post_code.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Post_code.Location = new System.Drawing.Point(608, 594);
            this.Txt_Post_code.Name = "Txt_Post_code";
            this.Txt_Post_code.Size = new System.Drawing.Size(252, 23);
            this.Txt_Post_code.TabIndex = 515;
            // 
            // Txt_State
            // 
            this.Txt_State.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_State.Location = new System.Drawing.Point(96, 567);
            this.Txt_State.Name = "Txt_State";
            this.Txt_State.Size = new System.Drawing.Size(252, 23);
            this.Txt_State.TabIndex = 516;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(1, 549);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 14);
            this.label6.TabIndex = 531;
            this.label6.Text = "الــعنـــــــــــــــوان....";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 558);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(868, 1);
            this.flowLayoutPanel2.TabIndex = 530;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(-1, 450);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 14);
            this.label19.TabIndex = 533;
            this.label19.Text = "الوثــــــــــــــــيقة....";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-2, 459);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(868, 1);
            this.flowLayoutPanel3.TabIndex = 532;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(10, 261);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 16);
            this.label20.TabIndex = 535;
            this.label20.Text = "أســـــــــــم الام";
            // 
            // Txt_Mother_name
            // 
            this.Txt_Mother_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Mother_name.Location = new System.Drawing.Point(96, 258);
            this.Txt_Mother_name.Name = "Txt_Mother_name";
            this.Txt_Mother_name.Size = new System.Drawing.Size(321, 23);
            this.Txt_Mother_name.TabIndex = 534;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Maroon;
            this.label26.Location = new System.Drawing.Point(-1, 285);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(155, 14);
            this.label26.TabIndex = 537;
            this.label26.Text = "المعلومـــات العامــــــــة ....";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-2, 294);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(868, 1);
            this.flowLayoutPanel4.TabIndex = 536;
            // 
            // Grd_Person_info
            // 
            this.Grd_Person_info.AllowUserToAddRows = false;
            this.Grd_Person_info.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Person_info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Person_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Person_info.ColumnHeadersHeight = 25;
            this.Grd_Person_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21});
            this.Grd_Person_info.Location = new System.Drawing.Point(6, 85);
            this.Grd_Person_info.Name = "Grd_Person_info";
            this.Grd_Person_info.RowHeadersWidth = 25;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Person_info.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Person_info.Size = new System.Drawing.Size(852, 108);
            this.Grd_Person_info.TabIndex = 538;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Chk";
            this.Column1.FalseValue = "0";
            this.Column1.HeaderText = "تأشير ";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.TrueValue = "1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Per_AName";
            this.Column2.HeaderText = "الاسم العربي";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Per_EName";
            this.Column3.HeaderText = "الاسم الانكليزي";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "per_Phone";
            this.Column4.HeaderText = "رقم الهاتف";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "per_Email";
            this.Column5.HeaderText = "الايميل";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "per_Another_Phone";
            this.Column6.HeaderText = "رقم الهاتف الاخر";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "PerFRM_ADOC_NA";
            this.Column7.HeaderText = "الوثيقة ";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "per_Frm_Doc_NO";
            this.Column8.HeaderText = "رقم الوثيقة";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "per_Frm_Doc_IS";
            this.Column9.HeaderText = "اصدار الوثيقة";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "per_Frm_Doc_Date";
            this.Column10.HeaderText = "تاريخ الوثيقة ";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "per_Frm_Doc_EDate";
            this.Column11.HeaderText = "تاريخ النفاذ";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "per_Birth_day";
            this.Column12.HeaderText = "تاريخ الولادة";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "per_Occupation";
            this.Column13.HeaderText = "المهنة";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "per_Mother_name";
            this.Column14.HeaderText = "اسم الام";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Per_ACITY_NAME";
            this.Column15.HeaderText = "المدينة-البلد";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Per_State";
            this.Column17.HeaderText = "الولاية";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "Per_Street";
            this.Column18.HeaderText = "الحي";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Per_Suburb";
            this.Column19.HeaderText = "الزقاق";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "Per_A_NAT_NAME";
            this.Column20.HeaderText = "الجنسية";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Per_Gender_Aname";
            this.Column21.HeaderText = "الجنس";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-4, 208);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(868, 1);
            this.flowLayoutPanel5.TabIndex = 539;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-4, 50);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(868, 1);
            this.flowLayoutPanel6.TabIndex = 541;
            // 
            // Txt_Person_Name
            // 
            this.Txt_Person_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Person_Name.Location = new System.Drawing.Point(7, 61);
            this.Txt_Person_Name.Name = "Txt_Person_Name";
            this.Txt_Person_Name.Size = new System.Drawing.Size(321, 23);
            this.Txt_Person_Name.TabIndex = 543;
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.SearchBtn.ForeColor = System.Drawing.Color.Navy;
            this.SearchBtn.Location = new System.Drawing.Point(329, 60);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(89, 24);
            this.SearchBtn.TabIndex = 544;
            this.SearchBtn.Text = "بــحـــث";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // TxtExp_Da
            // 
            this.TxtExp_Da.DateSeperator = '/';
            this.TxtExp_Da.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExp_Da.Location = new System.Drawing.Point(96, 522);
            this.TxtExp_Da.Mask = "0000/00/00";
            this.TxtExp_Da.Name = "TxtExp_Da";
            this.TxtExp_Da.PromptChar = ' ';
            this.TxtExp_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtExp_Da.Size = new System.Drawing.Size(252, 23);
            this.TxtExp_Da.TabIndex = 13;
            this.TxtExp_Da.Text = "00000000";
            this.TxtExp_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtBirth_Da
            // 
            this.TxtBirth_Da.DateSeperator = '/';
            this.TxtBirth_Da.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirth_Da.Location = new System.Drawing.Point(608, 364);
            this.TxtBirth_Da.Mask = "0000/00/00";
            this.TxtBirth_Da.Name = "TxtBirth_Da";
            this.TxtBirth_Da.PromptChar = ' ';
            this.TxtBirth_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtBirth_Da.Size = new System.Drawing.Size(252, 23);
            this.TxtBirth_Da.TabIndex = 7;
            this.TxtBirth_Da.Text = "00000000";
            this.TxtBirth_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.DateSeperator = '/';
            this.TxtDoc_Da.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDoc_Da.Location = new System.Drawing.Point(608, 496);
            this.TxtDoc_Da.Mask = "0000/00/00";
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.PromptChar = ' ';
            this.TxtDoc_Da.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDoc_Da.Size = new System.Drawing.Size(252, 23);
            this.TxtDoc_Da.TabIndex = 12;
            this.TxtDoc_Da.Text = "00000000";
            this.TxtDoc_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Chk_Old_Authorized
            // 
            this.Chk_Old_Authorized.AutoSize = true;
            this.Chk_Old_Authorized.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Old_Authorized.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Old_Authorized.Location = new System.Drawing.Point(3, 39);
            this.Chk_Old_Authorized.Name = "Chk_Old_Authorized";
            this.Chk_Old_Authorized.Size = new System.Drawing.Size(180, 18);
            this.Chk_Old_Authorized.TabIndex = 545;
            this.Chk_Old_Authorized.Text = "أضــــــافة مخول سابق ......";
            this.Chk_Old_Authorized.UseVisualStyleBackColor = true;
            // 
            // Chk_New_Authorized
            // 
            this.Chk_New_Authorized.AutoSize = true;
            this.Chk_New_Authorized.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_New_Authorized.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_New_Authorized.Location = new System.Drawing.Point(3, 199);
            this.Chk_New_Authorized.Name = "Chk_New_Authorized";
            this.Chk_New_Authorized.Size = new System.Drawing.Size(173, 18);
            this.Chk_New_Authorized.TabIndex = 546;
            this.Chk_New_Authorized.Text = "أضــــــافة مخول جديد ......";
            this.Chk_New_Authorized.UseVisualStyleBackColor = true;
            // 
            // Txt_birth_place
            // 
            this.Txt_birth_place.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_birth_place.Location = new System.Drawing.Point(608, 393);
            this.Txt_birth_place.Name = "Txt_birth_place";
            this.Txt_birth_place.Size = new System.Drawing.Size(252, 23);
            this.Txt_birth_place.TabIndex = 548;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(517, 396);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 16);
            this.label18.TabIndex = 547;
            this.label18.Text = "مكان الــــــولادة:";
            // 
            // Authorized_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 658);
            this.Controls.Add(this.Txt_birth_place);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Chk_New_Authorized);
            this.Controls.Add(this.Chk_Old_Authorized);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.Txt_Person_Name);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Grd_Person_info);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Txt_Mother_name);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Txt_State);
            this.Controls.Add(this.Txt_Post_code);
            this.Controls.Add(this.Txt_Suburb);
            this.Controls.Add(this.Txt_Street);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Txt_Another_Phone);
            this.Controls.Add(this.Txt_Occupation);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Cbo_Gender);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtExp_Da);
            this.Controls.Add(this.TxtBirth_Da);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.CboDoc_id);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CboNat_id);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CboCit_Id);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtCust_Ename);
            this.Controls.Add(this.TxtCust_Aname);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Authorized_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "155";
            this.Text = "Authorized_Add_Upd";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Customer_Add_Upd_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Customer_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.Customer_Add_Upd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Person_info)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtCust_Aname;
        private System.Windows.Forms.TextBox TxtCust_Ename;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox CboDoc_id;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CboNat_id;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CboCit_Id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MyDateTextBox TxtDoc_Da;
        private MyDateTextBox TxtBirth_Da;
        private MyDateTextBox TxtExp_Da;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Cbo_Gender;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_Occupation;
        private System.Windows.Forms.TextBox Txt_Another_Phone;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_Street;
        private System.Windows.Forms.TextBox Txt_Suburb;
        private System.Windows.Forms.TextBox Txt_Post_code;
        private System.Windows.Forms.TextBox Txt_State;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Txt_Mother_name;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.DataGridView Grd_Person_info;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.TextBox Txt_Person_Name;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.CheckBox Chk_Old_Authorized;
        private System.Windows.Forms.CheckBox Chk_New_Authorized;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.TextBox Txt_birth_place;
        private System.Windows.Forms.Label label18;
    }
}