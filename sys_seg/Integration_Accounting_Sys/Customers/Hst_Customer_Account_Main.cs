﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Hst_Customer_Account_Main : Form
    {
        #region Defintion && Constructor
        bool GrdChange = false;
        int Cust_Id = 0;
        int T_Id = 0;
        int  Mcur_id = 0;
        string Mat_name = "";
   BindingSource _Bs_Hst_Customers_Account  = new BindingSource();
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Hst_Customer_Account_Main(int MCust_Id, int MT_Id, int cur_id,string name)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            this.GrdCurrency_Cust.AutoGenerateColumns = false;
           
            Cust_Id = MCust_Id;
            T_Id = MT_Id;
            Mcur_id = cur_id;
            Mat_name = name;
        }
        //---------------------------------------------------------------------------------------------------------
        private void Hst_Customer_Account_Main_Load(object sender, EventArgs e)
        {
            txtcust_name.Text = Mat_name;
            object[] Sparams = { T_Id, Mcur_id, "", 0, Cust_Id, "Hst_CUSTOMERS_ACCOUNTS" };
            _Bs_Hst_Customers_Account.DataSource = connection.SqlExec("Main_Customer_Account", "Cust_Acc_Tbl_hst", Sparams);
            GrdCurrency_Cust.DataSource = _Bs_Hst_Customers_Account;
           
            TxtPhone.DataBindings.Clear();
            TxtA_Address.DataBindings.Clear();

            TxtPhone.DataBindings.Add("Text", _Bs_Hst_Customers_Account, "Phone");
            TxtA_Address.DataBindings.Add("Text", _Bs_Hst_Customers_Account, connection.Lang_id == 1 ? "A_Address" : "E_Address");
            if (connection.SQLDS.Tables["Cust_Acc_Tbl_hst"].Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات تاريخية لهذا الثانوي" : "No History info. for this customer", MyGeneral_Lib.LblCap);
                this.Close();
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void GrdCurrency_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                LblRec.Text = connection.Records();
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Account_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
            
            string[] Used_Tbl = { "Cust_Acc_Tbl_hst" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
