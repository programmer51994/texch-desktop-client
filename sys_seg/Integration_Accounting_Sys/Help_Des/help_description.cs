﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Security.Policy;

namespace Integration_Accounting_Sys.Help_Desc
{
    public partial class help_description : Form
    {
        string sql_txt = "";
 
        public static int Page_no = 0;
        public static string tmpFile = "";
        public static string server_name = "";
        public static string PWD = "";
        public static string UID = "";
        public static string page = "";
        public static string doc_name = "";
        public static string page_temp = "";
        public static string parent_folder = "";
        public static string Web_Flag = "";
        public static string node_id = "";
        public static string x = "";
        public static string url_temp1 = "";
        bool change = false;
        //public static int btnoffset = 0;



        public help_description(int page_no)
        {
            InitializeComponent();
            Page_no = page_no;
            
        }


        private void get_date()
        {
            int agent_id = 1;
            string sql_text3 = "select * from  Agencies where agent_id = " + agent_id;
            connection.SqlExec(sql_text3, "ftp_server");

             DataTable ftp_server = new DataTable();
             ftp_server = connection.SQLDS.Tables["ftp_server"].DefaultView.ToTable();
             server_name = ftp_server.Rows[0]["server_name"].ToString();
             PWD = ftp_server.Rows[0]["PWD"].ToString();
             UID = ftp_server.Rows[0]["UID"].ToString();

             sql_txt = "select Web_Flag,node_id from  Terminal_Permissions "
                   + " where Node_Id=" + connection.Node_id;
             connection.SqlExec(sql_txt, "node_id_tbl");
             DataTable node_id_tbl = new DataTable();
             node_id_tbl = connection.SQLDS.Tables["node_id_tbl"].DefaultView.ToTable();

             try
             {
                 Web_Flag = node_id_tbl.Rows[0]["Web_Flag"].ToString();
             }
             catch { }
             node_id = connection.Node_id.ToString();
             x = Web_Flag + "/" + node_id;

             page = Convert.ToString(Page_no);
             doc_name = x + "/" + page + ".mht";
             page_temp = page + ".mht";
             parent_folder = "Help_TTS_DB";
        }


        private void check_file_exists(string url_temp)
        {
            var request = (FtpWebRequest)WebRequest.Create
             (url_temp);
            request.Credentials = new NetworkCredential(UID, PWD);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                string temp_path = "";
                temp_path = "ftp://" + UID + ":" + PWD + "@" + server_name + "/" + parent_folder + "/" + "help_not_cpmplated.htm";
                webBrowser1.Url = new Uri(temp_path);
                //FtpWebResponse response = (FtpWebResponse)ex.Response;
                //if (response.StatusCode ==
                //    FtpStatusCode.ActionNotTakenFileUnavailable)
                //{
                //    //Does not exist
                   
                //}
            }

        }

       
        private bool FtpDirectoryExists(string Directory, string Username, string Password)
        {
            //Directory = "ftp://" + FTPpath + "/" + Selectedfile;
            FtpWebRequest fwr = (FtpWebRequest)WebRequest.Create(Directory);
            FtpWebResponse fRes = default(FtpWebResponse);
            fwr.Credentials = new NetworkCredential(Username, Password);
            fwr.Method = WebRequestMethods.Ftp.GetFileSize;
            try
            {
                fRes = (FtpWebResponse)fwr.GetResponse();
                //no error occured then the file is exists 
                return true;
            }
            catch (WebException ex)
            {
                fRes = (FtpWebResponse)ex.Response;
                //Error occured then the file doesn't exists 
                if (FtpStatusCode.ActionNotTakenFileUnavailable == fRes.StatusCode)
                {
                    return false;
                }
                else
                {
                    //Any other errors you need to handle here 
                    return false;
                }
            }
        }       
        //-----------------------------------------------
        private void help_description_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate("about:blank");

           get_date();   
           change = false;
           Cbo_links_SelectedValueChanged(null, null);
           url_temp1 = "ftp://" + UID + ":" + PWD + "@" + server_name + "/" + parent_folder + "/" + doc_name;

           //bool exsist = false;
           //exsist = FtpDirectoryExists(url_temp1, UID, PWD);//"https://www.google.iq/"
           //if (exsist)
           //{
           //    //MessageBox.Show("file exists");
           //}
           //else
           //{
           //    MessageBox.Show("ftp folder does not exists");
           //    return;
           //}
          

           webBrowser1.Url = new Uri(url_temp1);

          
         
           //-------------------------------------
          check_file_exists(url_temp1);
            //-------------------------------------
            
           string sql_txt2 = "";
           sql_txt2 = " select * from Help_description "
                   + " where Node_Id = " + connection.Node_id
                   + " and page_no = " + Page_no;

           connection.SqlExec(sql_txt2, "help_id");
           DataTable dt_help = new DataTable();
           dt_help = connection.SQLDS.Tables["help_id"].DefaultView.ToTable();
           int s_help_id = Convert.ToInt32(dt_help.Rows[0]["help_id"]);


           sql_txt = "  select Node_AName + ' الواجهة ' + CAST(page_no as varchar(10)) as Node_AName, "
                   + "  Node_EName + ' Form ' + CAST(page_no as varchar(10)) as Node_EName,a.node_id,page_no, "
                   + "  '/Help_TTS_DB/0/'+ CAST(a.node_id as varchar(10))+ '/'+ CAST(a.page_no as varchar(10)) + '.mht' as path_id "
                   + "  from Help_description a,  Terminal_Permissions b "
                   + "  where a.node_id = b.Node_Id "
                   + "  and  help_id in (select t_help_id from Related_subjects where s_help_id = " + s_help_id
                   + "  )"
                   + "  order by a.node_id ,page_no ";


           connection.SqlExec(sql_txt, "related_Subject_tbl");
           DataTable related_Subject_tbl = new DataTable();
           related_Subject_tbl = connection.SQLDS.Tables["related_Subject_tbl"].DefaultView.ToTable();
           string path_id = related_Subject_tbl.Rows[0]["path_id"].ToString();

           Cbo_links.DataSource = related_Subject_tbl;
           //Cbo_links.DataSource = connection.SqlExec(sql_txt, "related_tbl");
           Cbo_links.ValueMember = "path_id";
           Cbo_links.DisplayMember = "Node_AName";
           change = true;         
        }

    
        private void help_description_Resize(object sender, EventArgs e)
        {
            webBrowser1.Width=this.Width - 25;
            webBrowser1.Height=this.Height - 75;
           // backButton.Left = this.Width - 220;
            //backButton.Width = this.Width-btnoffset;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            get_date();
            url_temp1 = "ftp://" + UID + ":" + PWD + "@" + server_name + "/" + parent_folder + "/" + doc_name;
            webBrowser1.Url = new Uri(url_temp1);
            check_file_exists(url_temp1);

        }
        private void Cbo_links_SelectedValueChanged(object sender, EventArgs e)
        {
            if (change)
            {
                string url_temp = "";
                string x = Cbo_links.SelectedValue.ToString();
                url_temp = "ftp://" + UID + ":" + PWD + "@" + server_name + x;
                webBrowser1.Url = new Uri(url_temp);
                check_file_exists(url_temp);
            }

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void help_description_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
        }

        

        
    }
}
