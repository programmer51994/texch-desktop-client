﻿using System;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Login : Form
    {
        public static string T_PWD = "";
        public static bool Wanttoopen = false;

        public Login(string Terminal_Name)
        {
            InitializeComponent();
            TxtTerminal_Name.Text = Terminal_Name;
        }
        //---------------------------------------------------------
        private void Login_Load(object sender, EventArgs e)
        {
        }
        //---------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region       Valdation
            if (TxtTerminal_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل اسم الفرع" : "Please Enter terminal Name");
                TxtTerminal_Name.Focus();
                return;
            }

            if (TxtPwd.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور" : "Please Enter Password");
                TxtPwd.Focus();
                return;
            }
            #endregion
            T_PWD = TxtPwd.Text.Trim();
            Wanttoopen = true;
            this.Close();
        }
        //---------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            Wanttoopen = false;
            this.Close();
        }

       
    }
}