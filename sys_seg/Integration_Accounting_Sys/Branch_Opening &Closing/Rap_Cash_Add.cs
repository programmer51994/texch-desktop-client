﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Rap_Cash_Add : Form
    {
        #region MyRegion
        bool Change = false;
        bool ACCChange = false;
        bool CurChange = false;
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CurId = new BindingSource();
        BindingSource Bs_CustId = new BindingSource();
        BindingSource _Bs = new BindingSource();
        DataTable Voucher_Cur = new DataTable();
        int VO_NO = 0;
        string _Date = "";
        DateTime C_Date;
        string Acc_ERpt = "";
        string Cur_Code_Rpt = "";
        string Acc_Rpt = "";
        string Cust_Rpt = "";
        string Cust_ERpt = "";
        string DB_Acc_Cur_Cust = "";
        string DB_EAcc_Cur_Cust = "";
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        #endregion
        
        public Rap_Cash_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            GrdRap_Cash.AutoGenerateColumns = false;
            Create_Table();
            
            if (connection.Lang_id != 1)
            {
                Grd_Acc_Id.Columns["column10"].DataPropertyName = "Acc_ename";
                Grd_Cur_Id.Columns["Column11"].DataPropertyName = "cur_ename";
                Grd_Cust_Id.Columns["Column12"].DataPropertyName = "Ecust_name";
                label13.Text = "Total bill";                
            }
        }
        //------------------------
        private void Rap_Cash_Add_Load(object sender, EventArgs e)
        {            
            Change = true;
            Txt_Acc_TextChanged(sender, e);
        }
        //-----------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                ACCChange = false;
                connection.SqlExec("Get_Acc_Info ", "Acc_Name_Tbl",
               new object[] { TxtBox_User.Tag, Txt_Acc.Text.Trim(), connection.T_ID, connection.user_id, 0 });
                try
                {
                    Bs_AccId.DataSource = connection.SQLDS.Tables["Acc_Name_Tbl"];//.Select("Sub_Flag > 0").CopyToDataTable();
                    Grd_Acc_Id.DataSource = Bs_AccId;

                    if (connection.SQLDS.Tables["Acc_Name_Tbl"].Rows.Count > 0)
                    {
                        ACCChange = true;
                        Grd_Acc_Id_SelectionChanged(sender, e);
                    }
                    else
                    {
                        Grd_Cur_Id.DataSource = null;
                        Grd_Cust_Id.DataSource = null;
                    }
                }
                catch
                {
                    Bs_AccId.DataSource = null;
                }
            }
        }
        //----------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                Txt_Cust.Text = "";
                TxtCurr_Name.Text = "";
                TxtCurr_Name_TextChanged(sender, e);
            }
        }
        //----------------------------
        private void TxtCurr_Name_TextChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                CurChange = false;
                Bs_CurId.DataSource = connection.SqlExec("Get_Cur_Rap_Spend ", "Cur_Acc_Tbl",
                new object[] { Txt_Loc_Cur.Tag, TxtCurr_Name.Text.Trim(), connection.T_ID, 
                 Convert.ToDecimal(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]), 
                TxtBox_User.Tag ,Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"])});

                Grd_Cur_Id.DataSource = Bs_CurId;
                if (connection.SQLDS.Tables["Cur_Acc_Tbl"].Rows.Count > 0)
                {
                    CurChange = true;
                    Grd_Cur_Id_SelectionChanged(sender, e);
                }
                else
                {
                    Grd_Cust_Id.DataSource = null;
                }
            }            
        }
        //----------------------------
        private void Grd_Cur_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                Txt_Exch_Price.Text = "0.000";
                Txt_Exch_Price.Text = ((DataRowView)Bs_CurId.Current).Row["Exch_Rate"].ToString();
                Txt_Cust_TextChanged(sender, e);
            }
        }
        //----------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                if (Grd_Acc_Id.RowCount > 0)
                {
                    Bs_CustId.DataSource = connection.SqlExec("Get_Cust_Info ", "Cust_Name_Tbl",
                               new object[] { ((DataRowView)Bs_CurId.Current).Row["Cur_Id"],
                           Txt_Cust.Text.Trim(), connection.T_ID, ((DataRowView)Bs_AccId.Current).Row["Acc_Id"],Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"])});

                    Grd_Cust_Id.DataSource = Bs_CustId;
                }
            }
        }
        //-----------------------
        private void GrdRap_Cash_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }
        //-----------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validations
            if (Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب رجاءا" : "Please Select Account", MyGeneral_Lib.LblCap);
                Txt_Acc.Focus();
                return;
            }
           // if (Convert.ToInt16(((DataRowView)Bs_CurId.Current).Row["Cur_Id"]) <= 0)
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل العملة رجاءا" : "Please Select Currency", MyGeneral_Lib.LblCap);
                TxtCurr_Name.Focus();
                return;
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الثانوي " : "Please Select The Customer", MyGeneral_Lib.LblCap);
                return;

            }

            if (Convert.ToDecimal(Txt_Exch_Price.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل سعر صرف العملة" : "Please Insert Exchange Price", MyGeneral_Lib.LblCap);
                Txt_Exch_Price.Focus();
                return;
            }
            if (Convert.ToDecimal(Txt_Amount.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Please Insert Amount", MyGeneral_Lib.LblCap);
                Txt_Amount.Focus();
                return;
            }
            #endregion

            #region Add Rows To  Table
            DataRow DRow = Voucher_Cur.NewRow();
            DRow["For_Amount"] = Convert.ToDecimal(Txt_Amount.Text);
            DRow["Acc_Name"] = ((DataRowView)Bs_AccId.Current).Row[connection.Lang_id == 1? "Acc_Aname":"Acc_ename"];
            DRow["Cur_Name"] = ((DataRowView)Bs_CurId.Current).Row[connection.Lang_id == 1?"Cur_Aname":"Cur_ename"];
            if (Bs_CustId.List.Count > 0)
            {
                DRow["Cust_Name"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1?"Acust_Name":"Ecust_name"];
                DRow["Cust_id"] = ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
            }
            DRow["Real_price"] = Txt_Exch_Price.Text;
            DRow["Exch_Price"] = Txt_Exch_Price.Text;
            DRow["Loc_Amount"] = decimal.Round(Convert.ToDecimal(Txt_Amount.Text) * Convert.ToDecimal(Txt_Exch_Price.Text), 3);
            DRow["Notes"] = Txt_Detl.Text.Trim();
            DRow["ACC_Id"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Id"];
            DRow["For_cur_id"] = ((DataRowView)Bs_CurId.Current).Row["Cur_Id"];
            DRow["loc_cur_id"] = Txt_Loc_Cur.Tag;
            DRow["V_Ref_No"] = Txt_V_Ref_No.Text.Trim();
            //------------------Report

            Acc_Rpt = ((DataRowView)Bs_AccId.Current).Row["Acc_Aname"].ToString();
            Acc_ERpt = ((DataRowView)Bs_AccId.Current).Row["Acc_Ename"].ToString();
            Cur_Code_Rpt = ((DataRowView)Bs_CurId.Current).Row["Cur_Code"].ToString();
            Cust_Rpt = ((DataRowView)Bs_CustId.Current).Row["ACust_Name"].ToString();
            Cust_ERpt = ((DataRowView)Bs_CustId.Current).Row["ECust_Name"].ToString();
            DB_Acc_Cur_Cust = Cust_Rpt + "/" + Cur_Code_Rpt + "/" + Acc_Rpt;
            DB_EAcc_Cur_Cust = Acc_ERpt = "/" + Cur_Code_Rpt + "/" + Cust_ERpt;
            DRow["DB_Acc_Cur_Cust"] = DB_Acc_Cur_Cust;
            DRow["DB_EAcc_Cur_Cust"] = DB_EAcc_Cur_Cust;
            //-----------------------
            Voucher_Cur.Rows.Add(DRow);
            _Bs.DataSource = Voucher_Cur;
            #endregion
            Change = false;
            ACCChange = false;
            CurChange = false;
            Txt_Acc.Text = string.Empty;
            Grd_Acc_Id.ClearSelection();
            TxtCurr_Name.Text = string.Empty;
            Grd_Cur_Id.ClearSelection();
            Txt_Cust.Text = string.Empty;
            Grd_Cust_Id.ClearSelection();
            Txt_Amount.ResetText(); 
            Txt_Exch_Price.ResetText();
            Txt_Detl.Text = "";
            Change = true;
            ACCChange = true;
            CurChange = true;
            Txt_Acc.Focus();
            Txt_V_Ref_No.Text = string.Empty;
            get_tot_sum();
        }
        //----------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price","Cur_Name","Acc_Name","Cust_Name","DB_Acc_Cur_Cust","DB_EAcc_Cur_Cust","Cur_ToEWord","Cur_ToWord", "V_Ref_No"
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String","System.String"
            };

            Voucher_Cur = CustomControls.Custom_DataTable("Voucher_Cur", Column, DType);
            GrdRap_Cash.DataSource = _Bs;
        }
        //----------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Voucher_Cur.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد" :
                    "Are you sure you want to remove this record", MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    Voucher_Cur.Rows[GrdRap_Cash.CurrentRow.Index].Delete();
                    GrdRap_Cash.Refresh();
                }
            }
            get_tot_sum();
        }
        //----------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No opening balances", MyGeneral_Lib.LblCap);
                TxtIn_Rec_Date.Focus();
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No Record Organizer", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(TxtBox_User.Tag) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد امين صندوق معرف " : "No Treasurer Found", MyGeneral_Lib.LblCap);
                return;
            }
            if (Voucher_Cur.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "No Records For Adding", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion

            DataTable Dt = new DataTable();
            Dt = Voucher_Cur.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "V_Ref_No"
                );

            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Box_Cust_Id", TxtBox_User.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@LOC_CUR_ID", Txt_Loc_Cur.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 2);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Box_V_Ref_No", txt_Box_v_ref_no.Text.Trim());
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_New_Voucher_Rap_Cash", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Voucher_Cur.Rows.Clear();
            txt_Box_v_ref_no.Text = string.Empty;
        }
        //-----------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------------
        private void Rap_Cash_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
             ACCChange = false;
             CurChange = false;
             string[] Used_Tbl = { "Cur_Acc_Tbl", "Acc_Name_Tbl", "Cust_Name_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void print_Rpt_Pdf()
        {
            string SqlTxt = "Exec Main_Report " + VO_NO + "," + TxtIn_Rec_Date.Text + ",2," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "main");
            DataTable Voucher_Cur = connection.SQLDS.Tables["main"].Select("For_Amount1 < 0").CopyToDataTable();
            Voucher_Cur.TableName = "Voucher_Cur";
            connection.SQLDS.Tables.Add(Voucher_Cur);
            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Voucher_Cur"].Compute("sum(LOC_AMOUNT)", "")),
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString()+ " not else.)-";
            connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            Box_ACust_Cur =  "الحساب المدين" + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
            Box_ECust_Cur = "Debit Account : " + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString();// +"/" + Cur_Code;
            Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["main"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 2);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
            Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("pos_sum", pos_sum);

            DataRow Dr;
            int y = connection.SQLDS.Tables["Voucher_Cur"].Rows.Count;
            if (y <= 10)
            {
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Voucher_Cur"].NewRow();
                    connection.SQLDS.Tables["Voucher_Cur"].Rows.Add(Dr);
                }
                Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
                Rap_Spend_Cash_Rpt_Eng ObjRptEng = new Rap_Spend_Cash_Rpt_Eng();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 2);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Voucher_Cur");
                connection.SQLDS.Tables.Remove("main");
            }
            else
            {
                Sub_Bill_Rap_Spend_MultiRow ObjRpt1 = new Sub_Bill_Rap_Spend_MultiRow();
                Sub_Bill_Rap_Spend_MultiRow ObjRptEng1 = new Sub_Bill_Rap_Spend_MultiRow();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 2);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Voucher_Cur");
                connection.SQLDS.Tables.Remove("main");
            }

            //string Cur_ToWord = "";
            //string Cur_ToEWord = "";
            //string Amount = Voucher_Cur.Compute("Sum(For_amount)", "").ToString();
            //ToWord toWord = new ToWord(Convert.ToDouble(Amount),
            //    new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            //Cur_ToWord = "-(" + toWord.ConvertToArabic() + " لاغير)- ";
            //Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " not else.)-";
            //Voucher_Cur.Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            //Voucher_Cur.Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            //String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            //Box_ACust_Cur = Cur_Code + "/" + "الحساب المدين" + ":" + TxtBox_User.Text;
            //Box_ECust_Cur = "Debit Account : " + TxtBox_User.Text + "/" + Text + Cur_Code;
            //C_Date = Convert.ToDateTime(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            //_Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            //Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
            //Rap_Spend_Cash_Rpt ObjRptEng = new Rap_Spend_Cash_Rpt();
            //DataTable Dt_Param = CustomControls.CustomParam_Dt();
            //DataRow Dr;
            //int y = Voucher_Cur.Rows.Count;
            //for (int i = 0; i < (10 - y); i++)
            //{
            //    Dr = Voucher_Cur.NewRow();
            //    Voucher_Cur.Rows.Add(Dr);
            //}
            //connection.SQLDS.Tables.Add(Voucher_Cur);
            //Dt_Param.Rows.Add("Vo_No", VO_NO);
            //Dt_Param.Rows.Add("Oper_Id", 2);
            //Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            //Dt_Param.Rows.Add("C_Date", C_Date);
            //Dt_Param.Rows.Add("_Date", _Date);
            //Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
            //Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
            //Dt_Param.Rows.Add("Frm_Id", "Add");
            //Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 2);
            //this.Visible = false;
            //RptLangFrm.ShowDialog(this);
            //this.Visible = true;
            //connection.SQLDS.Tables.Remove(Voucher_Cur);
        }

        private void get_tot_sum()
        {
            //Decimal Amount = 0;
            Tot_Amount.Text = (Voucher_Cur.Compute("Sum(For_Amount)", "").ToString());
            //Tot_Amount.Text = Amount.ToString();
        }       
    }
}
