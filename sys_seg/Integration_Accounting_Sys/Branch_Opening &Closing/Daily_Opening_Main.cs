﻿using System;
using System.Windows.Forms;
using System.Data;

namespace Integration_Accounting_Sys
{
    public partial class Daily_Opening_Main : Form
    {
        BindingSource Bs = new BindingSource();
        bool isopenflag = false;
        public Daily_Opening_Main()//--------افتتاح
        {
            InitializeComponent();
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            GrdRap_Cash.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "Acc_EName";
                Column3.DataPropertyName = "Cur_Ename";
                Column4.DataPropertyName = "ECust_Name";
            }
            isopenflag = true;
        }
        public Daily_Opening_Main(bool isopen = false)//--------كشف الاغلاق 
        {
            InitializeComponent();
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            GrdRap_Cash.AutoGenerateColumns = false;
            this.Text = connection.Lang_id == 1 ? "كشف الاغلاق" : "Reveal Closing";
            isopenflag = isopen;
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "Acc_EName";
                Column3.DataPropertyName = "Cur_Ename";
                Column4.DataPropertyName = "ECust_Name";
            }
        }
        //-----------------------------
        private void Daily_Opening_Main_Load(object sender, EventArgs e)
        {
           


                if (!isopenflag)//----في حالة الكشف 
                {
                    object[] Sparams = { connection.T_ID, connection.user_id };
                    Bs.DataSource = connection.SqlExec("Reveal_Closing_Balance", "Reveal_Closing_Balance_Tbl", Sparams);
                    GrdRap_Cash.DataSource = Bs;
                    BtnOk.Visible = false;
                }
                else
                {
                    if (Page_Setting.chk_daily_opening == 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن افتتاح السجلات لانه تم اختيار الافتتاح اوتوماتيكيا" : "Can not open the System because The opening System was automatically selected ", MyGeneral_Lib.LblCap);
                        this.Close();
                    }
                    else
                    {
                    if (Page_Setting.Chk_Value == "")
                    {
                        Active_users AddFrm = new Active_users();
                        this.Visible = false;
                        AddFrm.ShowDialog();
                        this.Visible = true;
                        if (Active_users.Is_Active == 1)
                        {
                            this.Close(); // اذا تم الغاء التفعيل للمستخدمين
                        }
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن الافتتاح لنفس يوم العمل مرتين" : "Can not open the System For the same day Twice", MyGeneral_Lib.LblCap);
                        this.Close();
                    }
                }
            }
        }
        //-----------------------------
        private void Btn_Open_Click(object sender, EventArgs e)
        {
            Login TermLogin = new Login(TxtTerm_Name.Text);
            TermLogin.ShowDialog();
            object[] Sparams = { connection.T_ID, connection.user_id, Login.T_PWD, "" };

            if (Login.Wanttoopen)
            {
                Bs.DataSource = connection.SqlExec("Open_Daily", "Open_Daily_Tbl", Sparams);
                if (connection.Col_Name != "")
                {
                    MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                    return;
                }
                GrdRap_Cash.DataSource = Bs;
                BtnOk.Enabled = false;
            }
        }
        //-----------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {


            if (GrdRap_Cash.RowCount > 0)
            {
                if (!isopenflag)//----في حالة الاغلاق 
                {
                    DataGridView[] Export_GRD = { GrdRap_Cash };
                    DataTable[] Export_DT = { connection.SQLDS.Tables["Reveal_Closing_Balance_Tbl"].DefaultView.ToTable(false ,"ACC_ID",connection.Lang_id ==1? "Acc_Aname":"Acc_Ename",
                                            connection.Lang_id ==1?"Cur_Aname":"Cur_Ename" ,connection.Lang_id ==1? "Acust_name" : "Ecust_name","Dfor_amount","Cfor_amount","Dloc_amount","Cloc_amount").Select().CopyToDataTable()};
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {

                    DataGridView[] Export_GRD = { GrdRap_Cash };
                    DataTable[] Export_DT = { connection.SQLDS.Tables["Open_Daily_Tbl"].DefaultView.ToTable(false ,"ACC_ID",connection.Lang_id ==1? "Acc_Aname":"Acc_Ename",
                                            connection.Lang_id ==1?"Cur_Aname":"Cur_Ename" ,connection.Lang_id ==1? "Acust_name" : "Ecust_name","Dfor_amount","Cfor_amount","Dloc_amount","Cloc_amount").Select().CopyToDataTable()};

                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }

            }
        }
        //-----------------------------
        private void GrdRap_Cash_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(Bs);
        }
        //-----------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            String Sql_txt = " Select Count(*) as isopen from Daily_Opening "
                + " where R_Close_Flag = 0  "
                + " and R_Close_date is null  "
                + " and T_Id = "+ connection.T_ID ;
            connection.SqlExec(Sql_txt, "ISOpen_tbl");
            if (Convert.ToInt16(connection.SQLDS.Tables["ISOpen_tbl"].Rows[0]["isopen"]) == 0 && isopenflag == true )
            {
             Sql_txt = " insert into Hst_User_Terminals "
                               + "              Select * from User_Terminals  "
                               + " where USER_State = 1 ";
            connection.SqlExec(Sql_txt, "hst_users");

             Sql_txt = " Update User_Terminals "
            + " Set USER_State = 0, "
            + " u_date = getdate() , "
            + " user_id = " + connection.user_id 
            + " WHERE USER_State = 1 "
            + " and t_id = " + connection.T_ID;
           connection.SqlExec(Sql_txt, "Dactive_users");
                this.Close();
            }
            else
            {
                this.Close();
            }

        }
        //-------------------
        private void Daily_Opening_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            isopenflag = false;
            string[] Used_Tbl = { "Open_Daily_Tbl", "Reveal_Closing_Balance_Tbl", "hst_users", "Dactive_users" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Daily_Opening_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {

        }

        private void GrdRap_Cash_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
