﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Rap_Cash_Main : Form
    {
        bool GrdChange = false;
        int Vo_No = 0;
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        BindingSource _Bs_Rap    = new BindingSource();
        BindingSource _Bs_GRD2 = new BindingSource();
        public Rap_Cash_Main()
        {
            InitializeComponent();
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cust.AutoGenerateColumns = false;
            GrdCurrency_Cust.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["column3"].DataPropertyName = "cur_Ename";
                GrdCurrency_Cust.Columns["Column9"].DataPropertyName = "Acc_ename";
                GrdCurrency_Cust.Columns["Column10"].DataPropertyName = "Ecust_name";
            }
        }
        //---------------------
        private void Rap_Cash_Main_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()

            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }
            if (TxtBox_User.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            #endregion 

            GrdChange = false;
            object[] Sparams = { connection.T_ID, connection.user_id,TxtIn_Rec_Date.Text, 
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo, 2 };
            _Bs_Rap.DataSource = connection.SqlExec("Main_Vouchers_Rap", "Voucher_Rap_Tbl", Sparams);
            Grd_Cust.DataSource = _Bs_Rap;
            //=======================
            CboCurr_Id.DataSource = connection.SQLDS.Tables["Voucher_Rap_Tbl"].DefaultView.ToTable(true, "Cur_Aname", "Cur_Ename", "For_Cur_Id");
            CboCurr_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
            CboCurr_Id.ValueMember = "For_Cur_Id";
            if (connection.SQLDS.Tables["Voucher_Rap_Tbl"].Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Cust_SelectionChanged(sender, e);
                CboCurr_Id_SelectedIndexChanged(sender, e);
            }
            else
            {
                GrdChange = true;
                GrdCurrency_Cust.DataSource = new BindingSource();
            }
        }
        //---------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                int VO_NO = Convert.ToInt32(((DataRowView)_Bs_Rap.Current).Row["VO_NO"]);
                object[] Sparams = { connection.T_ID, connection.user_id, VO_NO, TxtIn_Rec_Date.Text , 2 };

                _Bs_GRD2.DataSource = connection.SqlExec("Get_Rec_Info", "RecVoucher_Tbl", Sparams);
                GrdCurrency_Cust.DataSource = _Bs_GRD2;
                    LblRec.Text = connection.Records(_Bs_GRD2);
            }
        }
        //---------------------
        private void CboCurr_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                int For_Cur_Id = Convert.ToInt16(CboCurr_Id.SelectedValue);
                string Filter = "TFor_Amount > 0 and For_Cur_id = " + For_Cur_Id;
                string Filt = "TLoc_amount > 0 and For_Cur_id = " + For_Cur_Id;
                TxtFor_Amount.Text = connection.SQLDS.Tables["Voucher_Rap_Tbl"].Compute("Sum(TFor_amount)", Filter).ToString();
                TxtLoc_Amount.Text = connection.SQLDS.Tables["Voucher_Rap_Tbl"].Compute("Sum(TLoc_amount)", Filt).ToString();
            }
        }
        //---------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Rap_Cash_Add AddFrm = new Rap_Cash_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Rap_Cash_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(2);
            SearchFrm.ShowDialog(this);
            Rap_Cash_Main_Load(sender, e);  
            
            
        }
        //---------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Rap_Cash_Main_Load(sender, e);
        }
        //---------------------
        private void Rap_Cash_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
            
            string[] Used_Tbl = { "RecVoucher_Tbl", "Voucher_Rap_Tbl", "main", "Voucher_Cur" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------
        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            Vo_No = Convert.ToInt16(((DataRowView)_Bs_Rap.Current).Row["Vo_No"]);
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + ",2," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "main");
            DataTable Voucher_Cur = connection.SQLDS.Tables["main"].Select("For_Amount1 < 0").CopyToDataTable();
            Voucher_Cur.TableName = "Voucher_Cur";
            connection.SQLDS.Tables.Add(Voucher_Cur);
            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Voucher_Cur"].Compute("sum(LOC_AMOUNT)", "")),
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            Box_ACust_Cur =  "الحساب المدين" + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
            Box_ECust_Cur = "Debit Account : " + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString();// +"/" + Cur_Code;
            Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["main"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", 2);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
            Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
            Dt_Param.Rows.Add("Frm_Id", "Main");
            Dt_Param.Rows.Add("pos_sum", pos_sum);

            DataRow Dr;
            int y = connection.SQLDS.Tables["Voucher_Cur"].Rows.Count;
            if (y <= 10)
            {
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Voucher_Cur"].NewRow();
                    connection.SQLDS.Tables["Voucher_Cur"].Rows.Add(Dr);
                }
                Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
                Rap_Spend_Cash_Rpt_Eng ObjRptEng = new Rap_Spend_Cash_Rpt_Eng();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 2);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Voucher_Cur");
                connection.SQLDS.Tables.Remove("main");
            }
            else
            {


                Sub_Bill_Rap_Spend_MultiRow ObjRpt1 = new Sub_Bill_Rap_Spend_MultiRow();
                Sub_Bill_Rap_Spend_MultiRow ObjRptEng1 = new Sub_Bill_Rap_Spend_MultiRow();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 2);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Voucher_Cur");
                connection.SQLDS.Tables.Remove("main");
            
            
            }

        }

        private void Rap_Cash_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Rap_Cash_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        
    }
}
