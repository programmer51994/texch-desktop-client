﻿namespace Integration_Accounting_Sys
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblE_Key = new System.Windows.Forms.Label();
            this.LblA_Key = new System.Windows.Forms.Label();
            this.TxtTerminal_Name = new System.Windows.Forms.TextBox();
            this.TxtPwd = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LblE_Key
            // 
            this.LblE_Key.AutoSize = true;
            this.LblE_Key.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.LblE_Key.ForeColor = System.Drawing.Color.Navy;
            this.LblE_Key.Location = new System.Drawing.Point(3, 39);
            this.LblE_Key.Name = "LblE_Key";
            this.LblE_Key.Size = new System.Drawing.Size(116, 18);
            this.LblE_Key.TabIndex = 18;
            this.LblE_Key.Text = "Terminal Pwd  :";
            // 
            // LblA_Key
            // 
            this.LblA_Key.AutoSize = true;
            this.LblA_Key.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.LblA_Key.ForeColor = System.Drawing.Color.Navy;
            this.LblA_Key.Location = new System.Drawing.Point(3, 9);
            this.LblA_Key.Name = "LblA_Key";
            this.LblA_Key.Size = new System.Drawing.Size(117, 18);
            this.LblA_Key.TabIndex = 17;
            this.LblA_Key.Text = "Terminal Name:";
            // 
            // TxtTerminal_Name
            // 
            this.TxtTerminal_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.TxtTerminal_Name.Enabled = false;
            this.TxtTerminal_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerminal_Name.Location = new System.Drawing.Point(122, 7);
            this.TxtTerminal_Name.Name = "TxtTerminal_Name";
            this.TxtTerminal_Name.Size = new System.Drawing.Size(242, 23);
            this.TxtTerminal_Name.TabIndex = 0;
            // 
            // TxtPwd
            // 
            this.TxtPwd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.TxtPwd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPwd.Location = new System.Drawing.Point(122, 37);
            this.TxtPwd.MaxLength = 70;
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.PasswordChar = '*';
            this.TxtPwd.ShortcutsEnabled = false;
            this.TxtPwd.Size = new System.Drawing.Size(242, 23);
            this.TxtPwd.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 67);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(377, 1);
            this.flowLayoutPanel1.TabIndex = 23;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(96, 71);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 26);
            this.AddBtn.TabIndex = 2;
            this.AddBtn.Text = "Continue";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(185, 71);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 3;
            this.ExtBtn.Text = "Cancel";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 97);
            this.ControlBox = false;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.LblE_Key);
            this.Controls.Add(this.LblA_Key);
            this.Controls.Add(this.TxtTerminal_Name);
            this.Controls.Add(this.TxtPwd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login Form";
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblE_Key;
        private System.Windows.Forms.Label LblA_Key;
        private System.Windows.Forms.TextBox TxtTerminal_Name;
        private System.Windows.Forms.TextBox TxtPwd;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
    }
}