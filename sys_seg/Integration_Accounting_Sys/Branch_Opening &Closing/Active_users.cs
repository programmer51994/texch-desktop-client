﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Active_users : Form
    {
        String Sql_Text = "";
        bool Change = false;
        bool Change1 = false;
        DataTable DT_User = new DataTable();
        BindingSource _BS = new BindingSource();
        public static int Is_Active = 0;
        public Active_users()
        {
            InitializeComponent();
            Grd_Users.AutoGenerateColumns = false;
           Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

        }



        private void Terminal_Acc_Close_Load(object sender, EventArgs e)
        {
            Is_Active = 0; 
            Change = false;
            Sql_Text = "  select 0 as chk,A.STARTDATE,  A. T_ID,B.acust_name, B.ECUST_NAME "
                    + "  from  Terminals A , CUSTOMERS B "
                      + " where A.cust_Id = B.cust_id ";



            Cbo_Terminal_ID.DataSource = connection.SqlExec(Sql_Text, "Terminals_Tbl");
            Cbo_Terminal_ID.ValueMember = "T_ID";
            Cbo_Terminal_ID.DisplayMember = (connection.Lang_id) == 1 ? "ACUST_NAME" : "ECUST_NAME";
            Cbo_Terminal_ID.SelectedValue = connection.T_ID;
            Cbo_Terminal_ID.Enabled = true; 

            if (connection.SQLDS.Tables["Terminals_Tbl"].Rows.Count > 0)
            {
                Change = true;
                Cbo_Terminal_ID_SelectedIndexChanged(null, null);

            }

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            int Rec_No = (from DataRow row in connection.SQLDS.Tables["Active_user_tbl"].Rows
                          where
                           (int)row["Chk"] == 1 
                          select row).Count();
            if (Rec_No == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق المستخدمين" : "Check records Please", MyGeneral_Lib.LblCap);
                return;
            }

            //int Rec_No = connection.SQLDS.Tables["Active_user_tbl"].Select("Chk <= 0").Length;
            //if (Rec_No > 0)
            //{
            //    MessageBox.Show("لا توجد قيود للاضافة", MyGeneral_Lib.LblCap);
            //    return;
            //}

            DT_User = connection.SQLDS.Tables["Active_user_tbl"].DefaultView.ToTable(false, "User_Id", "Chk").Select("Chk > 0").CopyToDataTable();
            connection.SQLCMD.Parameters.AddWithValue("@T_ID ", Cbo_Terminal_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Tbl_Active_user", DT_User);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Opening_active_user", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
            this.Close();

        }

        private void Grd_close_date_SelectionChanged(object sender, EventArgs e)
        {
            if (Change1)
            {
                Grd_Users.RefreshEdit();
                //connection.SQLDS.Tables["Active_user_tbl"].AcceptChanges();
                //     LblRec.Text = connection.Records(_BS);
                connection.SQLDS.Tables["Active_user_tbl"].AcceptChanges();
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            Is_Active = 1; //هل تم ضغط انهاء تفعيل المستخدمين
            this.Close();
        }

        private void Terminal_Acc_Close_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            Change1 = false;
            string[] Tbl = { "Opening_active_user", "Terminals_Tbl", "Active_user_tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }

        private void Cbo_Terminal_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                Change1 = false;
                string Sql_text = "Select  0 as Chk , USER_NAME, B.USER_ID , A.T_Id "
                                    + " from User_Terminals A , users B ,TERMINALS C  "
                                    + " where A.T_User_Id = B.USER_ID "
                                    + " And  A.USER_State = 0 "
                                    + " and B.USER_State not in(2, 3 )"
                                    + " And A.T_Id = C.T_ID "
                                    + " And A.T_id = " + Cbo_Terminal_ID.SelectedValue
                                    + " And B.User_id <> 1 "
                                    + " union "
                                    + " Select  1 as Chk , USER_NAME, B.USER_ID , A.T_Id "
                                    + " from User_Terminals A , users B ,TERMINALS C  "
                                    + " where A.T_User_Id = B.USER_ID "
                                    + " And  A.USER_State = 1 "
                                    + " and B.USER_State not in(2, 3 ) "
                                    + " And A.T_Id = C.T_ID "
                                    + " And A.T_id = " + Cbo_Terminal_ID.SelectedValue
                                    + " And B.User_id <> 1 ";


                _BS.DataSource = connection.SqlExec(Sql_text, "Active_user_tbl");
                Grd_Users.DataSource = _BS;
                if (connection.SQLDS.Tables["Active_user_tbl"].Rows.Count > 0)
                {
                    Change1 = true;
                    Grd_close_date_SelectionChanged(null, null);
                }
            }
        }

        private void Chk_users_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Users.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;
            Grd_Users.RefreshEdit();
            connection.SQLDS.Tables["Active_user_tbl"].AcceptChanges();

        }

        //private void Chk_users_MouseClick(object sender, MouseEventArgs e)
        //{
        //    CheckBox HCheckBox = ((CheckBox)sender);
        //    foreach (DataGridViewRow Row in Grd_Users.Rows)
        //        ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;
        //    Grd_Users.RefreshEdit();
        //    connection.SQLDS.Tables["Active_user_tbl"].AcceptChanges();
        //}
    }
}
