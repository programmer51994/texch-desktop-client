﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Rec_Daily_Add : Form
    {
        string Sql_Text = "";
        private BindingSource _Bs = new BindingSource();
        private DataTable Rec_cur;
        int MAcc_id = 0;
        Int16 Mcur_id = 0;
        int Mcust_id = 0;
        string MACC_Name = "";
        string MCur_Name = "";
        string Mcust_Name = "";
        Decimal MExch_Rate = 0;
        byte MSub_Flag = 0;
        bool Acc_chan = false;
        bool cur_chan = false;
        bool cust_chan = false;
        bool cust_txt = false;
        bool Change = false;
        string _Date = "";
        DateTime C_Date;
        int VO_NO = 0;
        string DB_Acc_Cur_Cust = "";
        string DB_EAcc_Cur_Cust = "";
         BindingSource _Bs_Rec_Daily_Add = new BindingSource();
         BindingSource _Bs_Rec_Daily_Add_1 = new BindingSource();
         BindingSource _Bs_Rec_Daily_Add_2 = new BindingSource();
        public Rec_Daily_Add()
        {
            InitializeComponent();
         
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Create_Table();
            Grd_Rec_Daily.AutoGenerateColumns = false;
            Grd_cust.AutoGenerateColumns = false;
            Grdcur_id.AutoGenerateColumns = false;
            Grd_Acc.AutoGenerateColumns = false;


            if (connection.Lang_id != 1)
            {
                Grd_Acc.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "Acc_ename";
                Grdcur_id.Columns["dataGridViewTextBoxColumn5"].DataPropertyName = "cur_ename";
                Grd_cust.Columns["Column12"].DataPropertyName = "Ecust_name";
            }
        }

        private void Rec_Daily_Add_Load(object sender, EventArgs e)
        {
            Sql_Text = " SELECT DC_Id, DC_Aname, DC_Ename FROM Debt_Credit where DC_Id <> 0 ";
            Cbo_Cust_Type.DataSource = connection.SqlExec(Sql_Text, "DC_Id_tab");
            Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
            Cbo_Cust_Type.ValueMember = "DC_Id";
            Txt_Acc_TextChanged(null, null);

        }
        //-----------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price",
                        "cFor_Amount","cLoc_amount","ACC_Name","Cur_Name","cust_Name","dFor_Amount","dLoc_amount","DB_Acc_Cur_Cust","DB_EAcc_Cur_Cust","For_Amount1","V_Ref_No",
                         "V_Acc_AName","V_Acc_EName","V_ACUST_NAME","V_ECUST_NAME","V_For_Cur_ANAME","V_For_Cur_ENAME","V_Loc_Cur_ANAME","V_Loc_Cur_ENAME","V_A_Term_Name",
                        "V_E_Term_Name","V_CATG_ANAME","V_CATG_ENAME","V_AOPER_NAME","V_EOPER_NAME","V_ACASE_NA","V_ECASE_NA","V_S_ATerm_Name","V_S_ETerm_Name","V_D_ATerm_Name",
                        "V_D_ETerm_Name","V_User_Name"
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal",
                "System.Decimal","System.Decimal","System.String","System.String","System.String","System.Decimal","System.Decimal","System.String","System.String","System.Decimal","System.String"
                ,"System.String","System.String","System.String","System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String",
                "System.String","System.String"
            };

            Rec_cur = CustomControls.Custom_DataTable("Rec_cur", Column, DType);
            Grd_Rec_Daily.DataSource = _Bs;
        }
        //-----------------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)   
        {
            Acc_chan = false;
            int Mflag = connection.Box_Cust_Id == 0 ? 0 : 1;

            _Bs_Rec_Daily_Add.DataSource = connection.SqlExec("exec Get_Acc_Info " + connection.Box_Cust_Id + ','
                                                                                  + "'" + Txt_Acc.Text + "'" + ',' + connection.T_ID + ','
                                                                                  + connection.user_id + ',' + Mflag, "ACC_TREE_tbl");
            Grd_Acc.DataSource = _Bs_Rec_Daily_Add;
            if (connection.SQLDS.Tables["ACC_TREE_tbl"].Rows.Count > 0)
            {
                Acc_chan = true;
                Grd_Acc_SelectionChanged_1(null, null);
            }
            else
            {
                Grdcur_id.DataSource = null;
                Grd_cust.DataSource = null;
            }
        }
        //-----------------------------------------
        //-----------------------------------------
        private void Txt_Cur_TextChanged(object sender, EventArgs e)
        {
            cust_chan = false;
            if (cur_chan)
            {
                DataRowView DRV = _Bs_Rec_Daily_Add.Current as DataRowView;
                DataRow DR = DRV.Row;
                MAcc_id = DR.Field<int>("acc_id");
                MACC_Name = DR.Field<string>(connection.Lang_id == 1 ? "ACC_AName" : "Acc_ename");
                MSub_Flag = DR.Field<byte>("Sub_Flag");

                _Bs_Rec_Daily_Add_2.DataSource = connection.SqlExec("exec Get_Cur_Info " + Txt_Loc_Cur.Tag + ','
                                                                            + "'" + Txt_Cur.Text + "'" + ',' + connection.T_ID + ','
                                                                                    + MAcc_id + ',' + MSub_Flag, "Cur_tbl");
                if (connection.SQLDS.Tables["Cur_tbl"].Rows.Count > 0)
                {
                    Grdcur_id.DataSource = _Bs_Rec_Daily_Add_2;
                    cust_chan = true;
                    Grdcur_id_SelectionChanged_1(null, null);
                }
                else
                {
                    Grd_cust.DataSource = null;
                }
            }
            
        }
        //-----------------------------------------
        //-----------------------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (cust_txt)
            {
                DataRowView DRV1 = _Bs_Rec_Daily_Add_2.Current as DataRowView;
                DataRow DR1 = DRV1.Row;
                Mcur_id = DR1.Field<Int16>("cur_id");
                MCur_Name = DR1.Field<string>(connection.Lang_id == 1?"Cur_AName":"Cur_ename");

                _Bs_Rec_Daily_Add_1.DataSource = connection.SqlExec("exec Get_Cust_Info " + Mcur_id + ',' + "'" + Txt_Cust.Text + "'" + ','
                                                                                            + connection.T_ID + ',' + MAcc_id + ',' + MSub_Flag, "Cust_tbl");

                Grd_cust.DataSource = _Bs_Rec_Daily_Add_1;
            }
        }
        //-----------------------------------------
        private void Clear_Data()
        {
            Acc_chan = false; cur_chan = false; cust_chan = false; cust_txt = false;
            Txt_Acc.Text = ""; Txt_Cur.Text = ""; Txt_Cust.Text = ""; txtexch_rate.Text = "";
            Txtfor_Amount.Text = ""; Txt_Details.Text = ""; Txt_Check_No.Text = "";
            MAcc_id = 0; Mcur_id = 0; Mcust_id = 0; MACC_Name = ""; MCur_Name = "";
            Mcust_Name = ""; MExch_Rate = 0; MSub_Flag = 0;
            BindingSource B_acc = new BindingSource();
            Grd_Acc.DataSource = B_acc;
            BindingSource B_cur = new BindingSource();
            Grdcur_id.DataSource = B_cur;
            BindingSource B_cust = new BindingSource();
            Grd_cust.DataSource = B_cust;
        }
        //-----------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Change = false; 
            Mcust_id = 0;
            Mcust_Name = "";
            #region Validation

            if (Grd_Acc.RowCount <= 0)
            {

                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب " : "Please Select The Account", MyGeneral_Lib.LblCap);
                return;
            }
            if (Grdcur_id.RowCount <= 0)
            {

                MessageBox.Show(connection.Lang_id == 1 ? "حدد العملة " : "Please Select The currency", MyGeneral_Lib.LblCap);
                return;
            }

            if (MSub_Flag == 1 && connection.SQLDS.Tables["Cust_tbl"].Rows.Count > 0) 
            {
                DataRowView DRV3 = _Bs_Rec_Daily_Add_1.Current as DataRowView;
                DataRow DR3 = DRV3.Row;
                Mcust_id = DR3.Field<int>("cust_id");
                Mcust_Name = DR3.Field<string>(connection.Lang_id ==1 ?"Acust_Name":"Ecust_name");
            }
            //int Rows = Rec_cur.Rows.Count;
            //if (Rows >= 10)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "الحد الاقصى للقيود 10 عمليات " : "The Maximum Of Records 10 operation ", MyGeneral_Lib.LblCap);
            //    return;
            //}


            if (Convert.ToDouble(Txtfor_Amount.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"ادخل المبلغ":"Enter The Amount Please ", MyGeneral_Lib.LblCap);
                Txtfor_Amount.Focus();
                return;
            }

            //if (Mcur_id == 0)
            //{
            //    MessageBox.Show("اختر العملة", MyGeneral_Lib.LblCap);
            //    Txt_Cur.Focus();
            //    return;
            //}
            //if (MAcc_id == 0)
            //{
            //    MessageBox.Show("اختر الحساب", MyGeneral_Lib.LblCap);
            //    Txt_Acc.Focus();
            //    return;
            //}

            if (MSub_Flag == 1 && Mcust_id == 0)
            {
                MessageBox.Show(connection.Lang_id == 1?"يجب تعريف  ثانوي لهذا الحساب":"No Customer Defined For This Account", MyGeneral_Lib.LblCap);
                Txt_Cust.Focus();
                return;
            }

            int Rec_No = (from DataRow row in Rec_cur.Rows
                          where
                           (int)row["Acc_Id"] == Convert.ToInt32(((DataRowView)_Bs_Rec_Daily_Add.Current).Row["Acc_Id"])
                        && (int)row["for_cur_id"] == Convert.ToInt32(((DataRowView)_Bs_Rec_Daily_Add_2.Current).Row["Cur_Id"])
                        && (int)row["Cust_Id"] == Convert.ToInt32(((DataRowView)_Bs_Rec_Daily_Add_1.Current).Row["Cust_id"])
                          select row).Count();


            if (Rec_No > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود يوجد ثانويين مكررين  ؟" :
                    "Check Record Please ", MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (Dr == DialogResult.No)
                {
                    return;
                }

             //   MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود" : "Check Record Please ", MyGeneral_Lib.LblCap);
                
            }


           

            #endregion 
            Decimal MdFor_Amount = 0;
            Decimal McFor_Amount = 0;
            Decimal MdLoc_amount = 0;
            Decimal McLoc_amount = 0;
            Decimal MFor_Amount = 0;
            Decimal MLoc_amount = 0;
            string MNotes = Txt_Details.Text;
            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) == 1)
            {
                MdFor_Amount = Convert.ToDecimal(Txtfor_Amount.Text);
                MdLoc_amount = decimal.Round(MdFor_Amount * Convert.ToDecimal(txtexch_rate.Text), 3);
                MFor_Amount = MdFor_Amount;
                MLoc_amount = MdLoc_amount;
            }
            else
            {
                McFor_Amount = Convert.ToDecimal(Txtfor_Amount.Text) * -1;
                McLoc_amount = decimal.Round((McFor_Amount * -1) * Convert.ToDecimal(txtexch_rate.Text), 3) * -1;
                MFor_Amount = McFor_Amount;
                MLoc_amount = McLoc_amount;
            }


            string Acc_Aname = ((DataRowView)_Bs_Rec_Daily_Add.Current).Row["ACC_AName"].ToString();
            string Acc_Ename = ((DataRowView)_Bs_Rec_Daily_Add.Current).Row["Acc_ename"].ToString();
            string Acust_name = ((DataRowView)_Bs_Rec_Daily_Add_1.Current).Row["Acust_name"].ToString();
            string Ecust_name = ((DataRowView)_Bs_Rec_Daily_Add_1.Current).Row["Ecust_name"].ToString();
            //string cur_Aname = ((DataRowView)connection.SQLBSGrd.Current).Row["cur_Aname"].ToString();
            //string cur_Ename = ((DataRowView)connection.SQLBSGrd.Current).Row["cur_Ename"].ToString();
            string Cur_Code_Rpt = ((DataRowView)_Bs_Rec_Daily_Add_2.Current).Row["Cur_Code"].ToString();
            DB_Acc_Cur_Cust = Acust_name + " / " + Cur_Code_Rpt + " / " + Acc_Aname;
            DB_EAcc_Cur_Cust = Ecust_name + " / " + Cur_Code_Rpt + " / " + Acc_Ename;

            //*********
            string V_cur_Aname = ((DataRowView)_Bs_Rec_Daily_Add_2.Current).Row["cur_Aname"].ToString();
            string V_cur_Ename = ((DataRowView)_Bs_Rec_Daily_Add_2.Current).Row["cur_Ename"].ToString();


            DataRow DRow = Rec_cur.NewRow();
            DRow["Exch_price"] = MExch_Rate;
            DRow["acc_id"] = MAcc_id;
            DRow["For_cur_id"] = Mcur_id;
            DRow["dFor_Amount"] = MdFor_Amount;
            DRow["cFor_Amount"] = McFor_Amount;
            DRow["dLoc_amount"] = MdLoc_amount;
            DRow["cLoc_amount"] = McLoc_amount;
            DRow["ACC_Name"] = MACC_Name;
            DRow["Cur_Name"] = MCur_Name;
            DRow["cust_Name"] = Mcust_Name;
            DRow["Notes"] = Txt_Details.Text;
            DRow["Check_No"] = Txt_Check_No.Text;
            DRow["For_Amount"] = MFor_Amount;
            DRow["Loc_amount"] = MLoc_amount;
            DRow["Loc_Cur_Id"] = connection.Loc_Cur_Id;
            DRow["cust_id"] = Mcust_id;
            DRow["Real_price"] = MExch_Rate;
            DRow["DB_Acc_Cur_Cust"] = DB_Acc_Cur_Cust;
            DRow["DB_EAcc_Cur_Cust"] = DB_EAcc_Cur_Cust;
            DRow["V_ref_No"] = Txt_V_Ref_No.Text.Trim();
            DRow["For_Amount1"] = MFor_Amount;
            DRow["V_Acc_AName"] = Acc_Aname;
            DRow["V_Acc_EName"] = Acc_Ename;
            DRow["V_ACUST_NAME"] = Acust_name;
            DRow["V_ECUST_NAME"] = Ecust_name;
            DRow["V_For_Cur_ANAME"] = V_cur_Aname;
            DRow["V_For_Cur_ENAME"] = V_cur_Ename;
            DRow["V_Loc_Cur_ANAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"];
            DRow["V_Loc_Cur_ENAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"];
            DRow["V_A_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"];
            DRow["V_E_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"];
            DRow["V_CATG_ANAME"] = "";
            DRow["V_CATG_ENAME"] = "";
            DRow["V_AOPER_NAME"] = "";
            DRow["V_EOPER_NAME"] = "";
            DRow["V_ACASE_NA"] = "";
            DRow["V_ECASE_NA"] = "";
            DRow["V_S_ATerm_Name"] = "";
            DRow["V_S_ETerm_Name"] = "";
            DRow["V_D_ATerm_Name"] = "";
            DRow["V_D_ETerm_Name"] = "";
            DRow["V_User_Name"] = TxtUser.Text;
            Rec_cur.Rows.Add(DRow);
            _Bs.DataSource = Rec_cur;
            ///////////////////// او يمكن استخدام الطريقة الثانية 
            // Rec_cur.Rows[MCRow].SetField("Exch_price",MExch_Rate);
            //Rec_cur.Rows[MCRow].SetField("acc_id", MAcc_id);
            ////////////////////////////////////////////////
           Change = true;
           Grd_Rec_Daily_SelectionChanged(null,null);
        }
        //-----------------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Rec_cur.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id==1?"هل تريد بالتأكيد حذف القيد":"Are you sure delete the record", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    Change = false;
                    Rec_cur.Rows[Grd_Rec_Daily.CurrentRow.Index].Delete();
                    Change = true;
                    Grd_Rec_Daily_SelectionChanged(null, null);
                }
            }
        }
        //-----------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------
        private void Btn_OK_Click(object sender, EventArgs e)
        {

            DataTable Dt = new DataTable();
            Dt = Rec_cur.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount","Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","V_Ref_No", "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                            "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                            "V_D_ETerm_Name", "V_User_Name");

            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 31);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            
            connection.SqlExec("Add_New_Voucher_Rec_Daily", connection.SQLCMD);
           
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Rec_cur.Rows.Clear();
            

        }
        private void print_Rpt_Pdf()
        {
           
            string SqlTxt = "Exec Main_Report " + VO_NO + "," + TxtIn_Rec_Date.Text + ",31," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Rec_cur");
            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();

            Decimal neg_sum = Math.Abs(Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 < 0")));
            Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 31);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("neg_sum", neg_sum);
            Dt_Param.Rows.Add("pos_sum", pos_sum);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

            DataRow Dr;
            int y = connection.SQLDS.Tables["Rec_cur"].Rows.Count;
            if (y <= 10)
            {
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Rec_cur"].NewRow();
                    Dr["R_T_Y_Id"] = "100000000000000000"; // اضيف لترتيب القيود في الطباعة
                    connection.SQLDS.Tables["Rec_cur"].Rows.Add(Dr);
                }


                Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
                Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();


                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 31);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Rec_cur");
            }
            else
            {
                Rec_Daily_Rpt_Multirow ObjRpt1 = new Rec_Daily_Rpt_Multirow();
                Rec_Daily_Rpt_Multirow ObjRptEng1 = new Rec_Daily_Rpt_Multirow();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 31);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Rec_cur");

            }

            //DataRow Dr;
            //int y = Rec_cur.Rows.Count;
            //if (y <= 10)
            //{
            //    for (int i = 0; i < (10 - y); i++)
            //    {
            //        Dr = Rec_cur.NewRow();
            //        Rec_cur.Rows.Add(Dr);
            //    }
            //}

            //C_Date = Convert.ToDateTime(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            //_Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            ////DateTime _Date1 = Convert.ToDateTime(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString());
            //////int result = dateDate.Year * 10000 + dateDate.Month * 100
            //////+ dateDate.Day + dateDate.Hour + dateDate.Minute + dateDate.Second;

            ////long n = long.Parse(_Date1.ToString("yyyyMMdd"));
            ////DataTable loc_tbl = new DataTable();
            ////DataTable loc_tbl1 = new DataTable();
            ////loc_tbl = connection.SqlExec("select LOC_AMOUNT from VOUCHER "
            ////          + "where vo_no = " + VO_NO + " and oper_id = " + 31 + " and  NREC_DATE = " + n + " and  LOC_AMOUNT <0 ",
            ////          "VOUCHER_tbl");
            ////loc_tbl1 = connection.SqlExec("select LOC_AMOUNT from VOUCHER "
            ////      + "where vo_no = " + VO_NO + " and oper_id = " + 31 + " and  NREC_DATE = " + n + " and  LOC_AMOUNT >0 ",
            ////      "VOUCHER_tbl");
            //double neg_sum = Math.Abs(Convert.ToDouble(Rec_cur.Compute("Sum(Loc_amount)", "Loc_amount < 0")));
            //double pos_sum = (Convert.ToDouble(Rec_cur.Compute("Sum(Loc_amount)", "Loc_amount > 0")));


            //DataTable Dt_Param = CustomControls.CustomParam_Dt();

            //Dt_Param.Rows.Add("Vo_No", VO_NO);
            //Dt_Param.Rows.Add("Oper_Id", 31);
            //Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            //Dt_Param.Rows.Add("C_Date", C_Date);
            //Dt_Param.Rows.Add("_Date", _Date);
            //Dt_Param.Rows.Add("neg_sum", neg_sum);
            //Dt_Param.Rows.Add("pos_sum", pos_sum);
            //Dt_Param.Rows.Add("Frm_Id", "Add");
            //Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());



            //connection.SQLDS.Tables.Add(Rec_cur);

            //if (y <= 10)
            //{
            //    Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
            //    Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();
            //    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 31);
            //    this.Visible = false;
            //    RptLangFrm.ShowDialog(this);
            //    this.Visible = true;
            //    connection.SQLDS.Tables.Remove("Rec_cur");
            //}
            //else
            //{
            //    Rec_Daily_Rpt_Multirow ObjRpt1 = new Rec_Daily_Rpt_Multirow();
            //    Rec_Daily_Rpt_Multirow ObjRptEng1 = new Rec_Daily_Rpt_Multirow();

            //    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 31);
            //    this.Visible = false;
            //    RptLangFrm.ShowDialog(this);
            //    this.Visible = true;
            //    connection.SQLDS.Tables.Remove("Rec_cur");
            //}
        }

        private void Rec_Daily_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Acc_chan = false;
            cur_chan = false;
            cust_chan = false;
            cust_txt = false;
            Change = false;
            
            string[] Used_Tbl = { "DC_Id_tab", "ACC_TREE_tbl", "Cur_tbl", "Cust_tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Grd_Acc_SelectionChanged_1(object sender, EventArgs e)
        {
            cur_chan = false;
            if (Acc_chan)
            {
                Txt_Cur.Text = "";
                txtexch_rate.Text = "";
                cur_chan = true;
                Txt_Cur_TextChanged(null, null);

            }
        }

        private void Grdcur_id_SelectionChanged_1(object sender, EventArgs e)
        {
            cust_txt = false;
            if (cust_chan)
            {

                Txt_Cust.Text = "";
                if (connection.SQLDS.Tables["Cur_tbl"].Rows.Count > 0)
                {
                    cust_txt = true;
                    Txt_Cust_TextChanged(null, null);

                    txtexch_rate.Text = "";

                    DataRowView DRV2 = _Bs_Rec_Daily_Add_2.Current as DataRowView;
                    DataRow DR2 = DRV2.Row;
                    MExch_Rate = DR2.Field<Decimal>("Exch_Rate");
                    txtexch_rate.Text = Convert.ToString(MExch_Rate);
                }
            }
        }

        private void Grd_Rec_Daily_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                
                Txt_Sum_Debit.Text = "0.000";
                Txt_Sum_Credit.Text = "0.000";
                Txt_Sum_Debit.Text  = Rec_cur.Compute("Sum(dloc_Amount)", "").ToString();
                Txt_Sum_Credit.Text = Rec_cur.Compute("Sum(cloc_Amount)", "").ToString();
                  
                
            }
        }

        //private void Cbo_Cust_Type_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}
