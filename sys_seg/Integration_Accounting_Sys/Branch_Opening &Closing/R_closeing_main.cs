﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Report;


namespace Integration_Accounting_Sys
{

    public partial class R_closeing_main : Form
    {
        string Sql_Text = "";
        bool chang_cur = false;
        bool chang_grd = false;
        DataTable MAIN_division = new DataTable();
        DataTable div = new DataTable();
        Decimal Mdiv_qty = 0;
        decimal Mdivision = 0;
        int MCRow = 0;
        DataTable _Dt;
        bool  Falg_close = false;
        Int32 dateNow = 0;
        public R_closeing_main()
        {
            InitializeComponent();
            TextBox txt = new TextBox();
            TextBox txt_cur = new TextBox();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
        }
        private void R_closeing_main_Load(object sender, EventArgs e)
        {
            DateTime date_Now = Convert.ToDateTime(DateTime.Now.ToString("d"));
            dateNow = date_Now.Day + date_Now.Month * 100 + date_Now.Year * 10000;

            if (Page_Setting.d_cur_nrec_date == dateNow)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"لا يمكن الاغلاق لنفس يوم العمل مرتين" : "Can not close the System For the same day Twice", MyGeneral_Lib.LblCap);
                this.Close();
            }
            else
            {
                chang_cur = false;
                Create_Table();

                //---------------تجيك تاريخ الاغلاق

                Sql_Text = " select isnull((SELECT MAX(Nrec_date) 	From Daily_Opening  "
                         + " where t_id = " + connection.T_ID
                         + " and R_Close_Flag = 0  and R_Close_Date is null "
                         + " and ( SELECT MAX(Nrec_date) From Daily_Opening where t_id = " + connection.T_ID + ") <=  " + TxtIn_Rec_Date.Text + "),0) as Nrec_date ";

                connection.SqlExec(Sql_Text, "Daily_time");
                if (Convert.ToInt32(connection.SQLDS.Tables["Daily_time"].Rows[0]["Nrec_date"]) == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة للاغلاق" : "There are no balances for closeing", MyGeneral_Lib.LblCap);
                    this.Close();

                }
                ///--------------------------------------

                object[] Sparam = { connection.T_ID, TxtIn_Rec_Date.Text, connection.user_id, "", connection.Lang_id };
                connection.SqlExec("main_close_branch_auto", "balance_main_box", Sparam);
                string X = connection.Col_Name;
                if (X != "")
                {
                    MessageBox.Show(X, MyGeneral_Lib.LblCap);
                    this.Close();
                    return;
                }

                Sql_Text = "Get_Cur_Close " + connection.T_ID + "," + TxtIn_Rec_Date.Text;
                connection.SqlExec(Sql_Text, "Daily_cur_TOT");

                ////------------------------جلب العملات الاغلاق المستخدمة للتاريخ اليوم الاغلاق
                if (connection.SQLDS.Tables["Daily_cur_TOT"].Rows.Count > 0)
                {

                    Cbocur_Id.DataSource = connection.SQLDS.Tables["Daily_cur_TOT2"];
                    Cbocur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
                    Cbocur_Id.ValueMember = "cur_id";


                    chang_cur = true;
                    Cbocur_Id_SelectedIndexChanged(null, null);
                    chang_grd = true;

                }
                else
                {
                    DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عملات للاغلاق هل تود الاستمرار ؟" :
                     "No Currency for closing Do You want to continue ? ", MyGeneral_Lib.LblCap,
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Dr == DialogResult.Yes)
                    {
                        Login TermLogin = new Login(TxtTerm_Name.Text);
                        TermLogin.ShowDialog();
                        object[] Sparams = { connection.T_ID, connection.user_id, Login.T_PWD, "" };

                        if (Login.Wanttoopen)
                        {
                            Falg_close = true;

                            connection.SQLCMD.Parameters.AddWithValue("@currency_division", MAIN_division);
                            connection.SQLCMD.Parameters.AddWithValue("@nrec_date", TxtIn_Rec_Date.Text);
                            connection.SQLCMD.Parameters.AddWithValue("@Falg_close", Falg_close);
                            connection.SQLCMD.Parameters.AddWithValue("@T_id", connection.T_ID);
                            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                            connection.SqlExec("add_currency_divison", connection.SQLCMD);


                            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                            {
                                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                                connection.SQLCMD.Parameters.Clear();
                                return;
                            }
                            connection.SQLCMD.Parameters.Clear();
                            this.Visible = false;
                            Daily_Opening_Main AddFrm = new Daily_Opening_Main(false);
                            AddFrm.ShowDialog();
                            this.Close();
                        }
                        else
                        { this.Close(); }
                    }
                    else
                    { this.Close(); }

                }
            }
        }
        ////----------------------- انشاء الجداول
        private void Create_Table()
        {
            string[] Column =
            {
               
            "nrec_date","for_cur_id", "division","div_qty","t_id","notes",  "user_id",
            "Cur_ANAME", "Cur_ENAME" ,"Amount"
         
            };

            string[] DType =
            {
                "System.Int32", "System.Int32" ,"System.Decimal","System.Decimal","System.Int32","System.String","System.Int32"
                ,  "System.String" ,  "System.String" ,"System.Decimal"
            };

            MAIN_division = CustomControls.Custom_DataTable("MAIN_division", Column, DType);
        }
        ///-------------------------------
        private void Cbocur_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_cur && Convert.ToInt32(Cbocur_Id.SelectedValue) > 0)
            {
            
                div= connection.SQLDS.Tables["Daily_cur_TOT1"].DefaultView.ToTable().Select("Cur_id = " + Cbocur_Id.SelectedValue).CopyToDataTable();
                Grddivision_Cur.DataSource = div;
            }
        }
        ///-------------------------------
        private void Grddivision_Cur_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (chang_grd)
            {
                try
                {
                    Txtsum_Amount.ResetText();
                    MCRow = Grddivision_Cur.CurrentRow.Index;
                    _Dt = div;
                    Mdiv_qty = Convert.ToDecimal(_Dt.Rows[MCRow]["div_qty"]);
                    Mdivision = Convert.ToDecimal(_Dt.Rows[MCRow]["division"]);
                    _Dt.Rows[MCRow].SetField("amount", Mdiv_qty * Mdivision);
                    Txtsum_Amount.Text = _Dt.Compute("Sum(amount)", "").ToString();
                    Txtfor_Cur.Text = Cbocur_Id.Text;

                }
                catch { }
            }
        }
        ///-------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {

            DataTable balance = connection.SQLDS.Tables["balance_main_box"].DefaultView.ToTable(true, "tfor_amount", "for_cur_id").Select(" for_cur_id = " + Cbocur_Id.SelectedValue).CopyToDataTable();
            decimal balance1 = Convert.ToDecimal(balance.Rows[0]["tfor_amount"]);
            if (balance1 == Convert.ToDecimal(Txtsum_Amount.Text))
            {
                foreach (DataRow Dr1 in _Dt.DefaultView.ToTable().Rows)
                {
                    if (Convert.ToInt32(Dr1["div_qty"]) > 0 && Convert.ToDecimal(Dr1["amount"]) > 0)
                    {
                        DataRow Dr = MAIN_division.NewRow();
                        Dr["div_qty"] = Convert.ToDecimal(Dr1["div_qty"]);
                        //Dr["amount"] = Convert.ToDecimal(Dr1["amount"]);
                        Dr["notes"] = Dr1["notes"].ToString();
                        Dr["division"] = Convert.ToDecimal(Dr1["division"]);
                        Dr["for_cur_id"] = Convert.ToInt32(Dr1["cur_id"]);
                        Dr["nrec_date"] = TxtIn_Rec_Date.Text;
                        Dr["user_id"] = connection.user_id;
                        Dr["t_id"] = connection.T_ID;
                        Dr["Cur_ANAME"] = Cbocur_Id.Text ;
                        Dr["Cur_ENAME"] =  Cbocur_Id.Text ;
                        Dr["Amount"] =Convert.ToDecimal( Convert.ToDecimal(Dr1["div_qty"]) * Convert.ToDecimal(Dr1["division"]));
                        MAIN_division.Rows.Add(Dr);


                    }
                }

                TxtIn_Rec_Date.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
                connection.SQLDS.Tables["Daily_cur_TOT2"].Rows.RemoveAt(Cbocur_Id.SelectedIndex);
                if (connection.SQLDS.Tables["Daily_cur_TOT2"].Rows.Count == 0)
                {

                    connection.SQLCMD.Parameters.AddWithValue("@currency_division", MAIN_division);
                    connection.SQLCMD.Parameters.AddWithValue("@nrec_date", TxtIn_Rec_Date.Text);
                    connection.SQLCMD.Parameters.AddWithValue("@Falg_close", Falg_close);
                    connection.SQLCMD.Parameters.AddWithValue("@T_id", connection.T_ID);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                   connection.SqlExec("add_currency_divison", connection.SQLCMD);
                    

                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        return;
                    }
                    connection.SQLCMD.Parameters.Clear();
                   // MAIN_division.TableName = "MAIN_division";
                    connection.SQLDS.Tables.Add(MAIN_division);
                    connection.SQLDS.Tables["MAIN_division"].TableName = "MAIN_division";
                    print_Rpt_Pdf();
                    this.Visible = false;
                    Daily_Opening_Main AddFrm = new Daily_Opening_Main(false);
                    AddFrm.ShowDialog();
                    this.Close();
                }
                else
                {
                    chang_cur = true;
                    Cbocur_Id_SelectedIndexChanged(null, null);
                    Txtsum_Amount.ResetText();
                    Txtfor_Cur.Text = Cbocur_Id.Text;
                }

            }
            else
            {
                MessageBox.Show(connection.Lang_id==1?"الرصيد الفعلي غير مطابق":"Real balance not matched", MyGeneral_Lib.LblCap);
                return;
            }
        }
        ///-------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Daily_cur_TOT2"].Rows.Count > 0 && Convert.ToInt32(Cbocur_Id.SelectedValue) > 0)
            {
                object[] Sparam = { connection.T_ID, TxtIn_Rec_Date.Text };
                connection.SqlExec("active_user", "aa", Sparam);
                this.Close();
            }
            else
            {
                this.Close();
            }
        }
        ///-------------------------------
        private void R_closeing_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_cur = false;
            Falg_close = false;
            string[] Used_Tbl = { "aa", "balance_main_box", "Daily_cur_TOT2", "Daily_cur_TOT1", "Daily_time" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        ///-------------------------------
        private void R_closeing_main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
        //----------------------------

        private void print_Rpt_Pdf()
        {
            string User = TxtUser.Text;
            string _Date = "";
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            //DateTime n_rec_date = Convert.ToDateTime(TxtIn_Rec_Date.Text);
            string loc_cur = Txt_Loc_Cur.Text;
            string Term_Name = TxtTerm_Name.Text;
            R_Close_Rpt ObjRpt = new R_Close_Rpt();
            R_Close_Rpt ObjRptEng = new R_Close_Rpt();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("User", User);
            Dt_Param.Rows.Add("n_rec_date", _Date);
            Dt_Param.Rows.Add("loc_cur", loc_cur);
            Dt_Param.Rows.Add("Term_Name", Term_Name);
            connection.SQLDS.Tables["MAIN_division"].TableName = "MAIN_division";
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["MAIN_division"]);


        }

        private void Grddivision_Cur_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //required
        }
    }
}
