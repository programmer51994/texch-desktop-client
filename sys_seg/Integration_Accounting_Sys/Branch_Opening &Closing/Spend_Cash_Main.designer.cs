﻿namespace Integration_Accounting_Sys
{
    partial class Spend_Cash_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Spend_Cash_Main));
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.CboCurr_Id = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrdCurrency_Cust = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.SearchBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtFor_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtLoc_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCurrency_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(634, 40);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(94, 14);
            this.label3.TabIndex = 571;
            this.label3.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(731, 36);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(103, 23);
            this.Txt_Loc_Cur.TabIndex = 4;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(253, 36);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(156, 23);
            this.TxtUser.TabIndex = 2;
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(489, 36);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(142, 23);
            this.TxtBox_User.TabIndex = 3;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(10, 36);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(161, 23);
            this.TxtTerm_Name.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(837, 40);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 563;
            this.label4.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(412, 40);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(74, 14);
            this.label2.TabIndex = 567;
            this.label2.Text = "الصــــندوق:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(890, 36);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(88, 23);
            this.TxtIn_Rec_Date.TabIndex = 5;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(4, 63);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(977, 1);
            this.flowLayoutPanel9.TabIndex = 565;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(174, 40);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 562;
            this.label1.Text = "المستخــدم:";
            // 
            // CboCurr_Id
            // 
            this.CboCurr_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCurr_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCurr_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCurr_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCurr_Id.FormattingEnabled = true;
            this.CboCurr_Id.Location = new System.Drawing.Point(276, 244);
            this.CboCurr_Id.Name = "CboCurr_Id";
            this.CboCurr_Id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CboCurr_Id.Size = new System.Drawing.Size(171, 24);
            this.CboCurr_Id.TabIndex = 7;
            this.CboCurr_Id.SelectedIndexChanged += new System.EventHandler(this.CboCurr_Id_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 487);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(984, 22);
            this.statusStrip1.TabIndex = 574;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            this.Grd_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust.ColumnHeadersHeight = 40;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.Grd_Cust.Location = new System.Drawing.Point(10, 68);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.ReadOnly = true;
            this.Grd_Cust.RowHeadersWidth = 15;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust.Size = new System.Drawing.Size(968, 175);
            this.Grd_Cust.TabIndex = 6;
            this.Grd_Cust.SelectionChanged += new System.EventHandler(this.Grd_Cust_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "VO_NO";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رقم القيد";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Nrec_Date";
            this.Column2.HeaderText = "التاريخ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "cur_aname";
            this.Column3.HeaderText = "العملة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 175;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "tloc_amount";
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column4.HeaderText = "المبلغ المقبوض بالعملة المحلية";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 188;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "tfor_amount";
            dataGridViewCellStyle4.Format = "N3";
            this.Column5.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column5.HeaderText = "المبلغ المقبوض بالعملة الاصلية";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 187;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "c_date1";
            this.Column6.HeaderText = "وقت وتاريخ التنظيم";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 150;
            // 
            // GrdCurrency_Cust
            // 
            this.GrdCurrency_Cust.AllowUserToAddRows = false;
            this.GrdCurrency_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdCurrency_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdCurrency_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.GrdCurrency_Cust.ColumnHeadersHeight = 40;
            this.GrdCurrency_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column11,
            this.Column9,
            this.Column10,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column16,
            this.Column15});
            this.GrdCurrency_Cust.Location = new System.Drawing.Point(10, 283);
            this.GrdCurrency_Cust.Name = "GrdCurrency_Cust";
            this.GrdCurrency_Cust.ReadOnly = true;
            this.GrdCurrency_Cust.RowHeadersWidth = 15;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdCurrency_Cust.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.GrdCurrency_Cust.Size = new System.Drawing.Size(968, 191);
            this.GrdCurrency_Cust.TabIndex = 10;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "DFor_Amount";
            dataGridViewCellStyle8.Format = "N3";
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column7.Frozen = true;
            this.Column7.HeaderText = "الـعمـلـة الاصلية مدين";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 125;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "cFor_amount";
            dataGridViewCellStyle9.Format = "N3";
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column8.Frozen = true;
            this.Column8.HeaderText = "الـعمـلـة الاصلية دائن";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 125;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Acc_Id";
            this.Column11.Frozen = true;
            this.Column11.HeaderText = "رقم الحساب";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 75;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Acc_AName";
            dataGridViewCellStyle10.Format = "N3";
            dataGridViewCellStyle10.NullValue = null;
            this.Column9.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column9.Frozen = true;
            this.Column9.HeaderText = "اسم الحساب";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 125;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Acust_name";
            this.Column10.Frozen = true;
            this.Column10.HeaderText = "الحساب الثانوي";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 125;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "exch_price";
            dataGridViewCellStyle11.Format = "N7";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column12.Frozen = true;
            this.Column12.HeaderText = "سعر الصرف";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 75;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "dloc_amount";
            dataGridViewCellStyle12.Format = "N3";
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column13.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column13.Frozen = true;
            this.Column13.HeaderText = "العملة المحلية مدين";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "cloc_amount";
            dataGridViewCellStyle13.Format = "N3";
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column14.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column14.Frozen = true;
            this.Column14.HeaderText = "العملة المحلية دائن";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "V_Ref_No";
            this.Column16.Frozen = true;
            this.Column16.HeaderText = "الاشارة المرجعية";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "notes";
            this.Column15.Frozen = true;
            this.Column15.HeaderText = "التفاصيل";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 200;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 480);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(977, 1);
            this.flowLayoutPanel1.TabIndex = 578;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(4, 64);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 417);
            this.flowLayoutPanel7.TabIndex = 598;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(980, 63);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1, 417);
            this.flowLayoutPanel2.TabIndex = 599;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(4, 274);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(977, 1);
            this.flowLayoutPanel3.TabIndex = 600;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(14, 264);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(93, 14);
            this.label5.TabIndex = 601;
            this.label5.Text = "التفاصيــــــــل...";
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.AllBtn,
            this.toolStripSeparator1,
            this.SearchBtn,
            this.toolStripSeparator7,
            this.printToolStripButton});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(984, 30);
            this.BND.TabIndex = 0;
            this.BND.Text = "bindingNavigator1";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.Black;
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(99, 27);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "اضافة جديـد ";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // AllBtn
            // 
            this.AllBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllBtn.ForeColor = System.Drawing.Color.Black;
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(93, 27);
            this.AllBtn.Tag = "4";
            this.AllBtn.Text = "عرض الكــل";
            this.AllBtn.Click += new System.EventHandler(this.AllBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.ForeColor = System.Drawing.Color.Black;
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(61, 27);
            this.SearchBtn.Tag = "8";
            this.SearchBtn.Text = "بـحــث";
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(70, 27);
            this.printToolStripButton.Text = "طبــــاعـة";
            this.printToolStripButton.Click += new System.EventHandler(this.printToolStripButton_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 31);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(987, 1);
            this.flowLayoutPanel4.TabIndex = 603;
            // 
            // TxtFor_Amount
            // 
            this.TxtFor_Amount.BackColor = System.Drawing.Color.White;
            this.TxtFor_Amount.Enabled = false;
            this.TxtFor_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFor_Amount.Location = new System.Drawing.Point(638, 245);
            this.TxtFor_Amount.Name = "TxtFor_Amount";
            this.TxtFor_Amount.NumberDecimalDigits = 3;
            this.TxtFor_Amount.NumberDecimalSeparator = ".";
            this.TxtFor_Amount.NumberGroupSeparator = ",";
            this.TxtFor_Amount.Size = new System.Drawing.Size(187, 23);
            this.TxtFor_Amount.TabIndex = 9;
            this.TxtFor_Amount.Text = "0.000";
            // 
            // TxtLoc_Amount
            // 
            this.TxtLoc_Amount.BackColor = System.Drawing.Color.White;
            this.TxtLoc_Amount.Enabled = false;
            this.TxtLoc_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLoc_Amount.Location = new System.Drawing.Point(449, 245);
            this.TxtLoc_Amount.Name = "TxtLoc_Amount";
            this.TxtLoc_Amount.NumberDecimalDigits = 3;
            this.TxtLoc_Amount.NumberDecimalSeparator = ".";
            this.TxtLoc_Amount.NumberGroupSeparator = ",";
            this.TxtLoc_Amount.Size = new System.Drawing.Size(189, 23);
            this.TxtLoc_Amount.TabIndex = 8;
            this.TxtLoc_Amount.Text = "0.000";
            // 
            // Spend_Cash_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 509);
            this.Controls.Add(this.TxtFor_Amount);
            this.Controls.Add(this.TxtLoc_Amount);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.CboCurr_Id);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.GrdCurrency_Cust);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Spend_Cash_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "405";
            this.Text = "Spend_Cash_Main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Spend_Cash_Main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Spend_Cash_Main_FormClosed);
            this.Load += new System.EventHandler(this.Spend_Cash_Main_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCurrency_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboCurr_Id;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.DataGridView GrdCurrency_Cust;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton SearchBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Sample.DecimalTextBox TxtLoc_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtFor_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
    }
}