﻿namespace Integration_Accounting_Sys
{
    partial class financial_closing_cancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_Export_Details = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.cbo_term = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtS_pass = new System.Windows.Forms.TextBox();
            this.closeingTerm = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 142);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(529, 1);
            this.flowLayoutPanel1.TabIndex = 651;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-6, 371);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(609, 1);
            this.flowLayoutPanel2.TabIndex = 1134;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(13, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 16);
            this.label1.TabIndex = 1136;
            this.label1.Text = "كلــمــة مـــرور الفــــرع :";
            // 
            // Btn_Export_Details
            // 
            this.Btn_Export_Details.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Btn_Export_Details.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Export_Details.Location = new System.Drawing.Point(160, 149);
            this.Btn_Export_Details.Name = "Btn_Export_Details";
            this.Btn_Export_Details.Size = new System.Drawing.Size(81, 24);
            this.Btn_Export_Details.TabIndex = 1139;
            this.Btn_Export_Details.Text = "موافق";
            this.Btn_Export_Details.UseVisualStyleBackColor = true;
            this.Btn_Export_Details.Click += new System.EventHandler(this.Btn_Export_Details_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(241, 149);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(75, 24);
            this.Btn_Ext.TabIndex = 1140;
            this.Btn_Ext.Tag = "";
            this.Btn_Ext.Text = "انـهـاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Ext_Click);
            // 
            // cbo_term
            // 
            this.cbo_term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_term.FormattingEnabled = true;
            this.cbo_term.Items.AddRange(new object[] {
            ""});
            this.cbo_term.Location = new System.Drawing.Point(148, 35);
            this.cbo_term.Name = "cbo_term";
            this.cbo_term.Size = new System.Drawing.Size(304, 24);
            this.cbo_term.TabIndex = 1315;
            this.cbo_term.SelectedIndexChanged += new System.EventHandler(this.Cbo_R_Type_Id_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(17, 39);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(117, 16);
            this.label20.TabIndex = 1316;
            this.label20.Text = "الــــفــــــــــــــــــــــرع :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(-1, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 16);
            this.label2.TabIndex = 1317;
            this.label2.Text = "الغــــــاء الاغــــلاق المـــالـي ليوم";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(76, 18);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(520, 1);
            this.flowLayoutPanel6.TabIndex = 648;
            // 
            // TxtS_pass
            // 
            this.TxtS_pass.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtS_pass.Location = new System.Drawing.Point(148, 106);
            this.TxtS_pass.MaxLength = 50;
            this.TxtS_pass.Name = "TxtS_pass";
            this.TxtS_pass.PasswordChar = '*';
            this.TxtS_pass.Size = new System.Drawing.Size(304, 23);
            this.TxtS_pass.TabIndex = 1319;
            // 
            // closeingTerm
            // 
            this.closeingTerm.BackColor = System.Drawing.SystemColors.Control;
            this.closeingTerm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.closeingTerm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeingTerm.Location = new System.Drawing.Point(210, 72);
            this.closeingTerm.Name = "closeingTerm";
            this.closeingTerm.Size = new System.Drawing.Size(72, 16);
            this.closeingTerm.TabIndex = 1321;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(5, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(180, 14);
            this.label22.TabIndex = 649;
            this.label22.Text = "الغــــــاء الاغــــلاق المـــالـي ....";
            // 
            // financial_closing_cancel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 180);
            this.Controls.Add(this.closeingTerm);
            this.Controls.Add(this.TxtS_pass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbo_term);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Btn_Export_Details);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flowLayoutPanel6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "financial_closing_cancel";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "560";
            this.Text = "الغاء الاغلاق المالي";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.financial_closing_cancel_FormClosed);
            this.Load += new System.EventHandler(this.financial_closing_cancel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_Export_Details;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.ComboBox cbo_term;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.TextBox TxtS_pass;
        private System.Windows.Forms.TextBox closeingTerm;
        private System.Windows.Forms.Label label22;
    }
}