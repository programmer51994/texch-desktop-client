﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class financial_closing_cancel : Form
    {
        DataTable closing_date_Tbl = new DataTable();
        BindingSource Bs_CloseDate = new BindingSource();

        bool change_term = false;


        public financial_closing_cancel()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));


        }

        //*********************************************************************************

        private void financial_closing_cancel_Load(object sender, EventArgs e)
        {
            change_term = false;

            String SqlTxt = " Select cast (cast (Max(Nrec_date ) as varchar(8))as date ) as Nrec_date , A.t_id ,R_Close_Flag ,C.ACUST_NAME, C.ECUST_NAME "
                        + " from Daily_Opening  A  ,TERMINALS B, CUSTOMERS C "
                        + " where A.t_id not in (Select T_id from Daily_Opening where R_Close_Flag = 0 ) "
                        + " AND   B.CUST_ID = C.CUST_ID  "
                        + " AND A.T_ID= B.T_ID "
                        + " AND Acc_Close_Flag <> 1"
                        + " AND R_Close_date is not null "
                        + " group by A.t_id ,R_Close_Flag,ACUST_NAME,ECUST_NAME ";

                       connection.SqlExec(SqlTxt, "Term_tbl");

                       if (connection.SQLDS.Tables["Term_tbl"].Rows.Count > 0)
                       {
                           cbo_term.DataSource = connection.SQLDS.Tables["Term_tbl"];
                           cbo_term.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "Ecust_name";
                           cbo_term.ValueMember = "T_id";
                           change_term = true;
                           Cbo_R_Type_Id_SelectedIndexChanged(null, null);
                       }
                       else {
                           //Btn_Export_Details.Enabled = false;
                           MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد فروع لديها اغلاق مالي لألغائه" : "there is no financial closing to be canceled", MyGeneral_Lib.LblCap);
                           //return;
                           this.Close();
                            }


        }
        //private void get_active_users()
        //{
          

        //}

        //************************************************************************************
        private void Cbo_R_Type_Id_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (change_term)
            {
                
                closeingTerm.Text = connection.SQLDS.Tables["Term_tbl"].DefaultView.ToTable().Select("T_ID =" + Convert.ToInt16(cbo_term.SelectedValue)).CopyToDataTable().Rows[0]["Nrec_date"].ToString();

            }
        }

        //************************************************************************************

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {
       

            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@t_id", Convert.ToInt16(cbo_term.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@PWD", TxtS_pass.Text);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters.Add("@Param_Result2", SqlDbType.VarChar, 200).Value = "";
            //@Param_Result2



            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters["@Param_Result2"].Direction = ParameterDirection.Output;
            connection.SqlExec("cancel_close_daily", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
                //   clear_all();
                //  clear_Tables();
               
            }
            if (connection.SQLCMD.Parameters["@Param_Result2"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result2"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Parameters.Clear();
                

                //Active_users ADDFRM = new Active_users();
                //ADDFRM.ShowDialog();
                this.Close();
               
                //   clear_all();
                //  clear_Tables();

            }
            //   MessageBox.Show(connection.Lang_id == 1 ? "تم الغاء الاغلاق بنجاح" : "Closing canceled successfully", MyGeneral_Lib.LblCap);
            //clear_all();
            //  clear_Tables();
            
          //  connection.SQLCS.Close();
            
           // connection.SQLCMD.Dispose();

           //**
            
            //**
          
           
           // this.Close();

        }

        //************************************************************************************

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        //************************************************************************************

        private void financial_closing_cancel_FormClosed(object sender, FormClosedEventArgs e)
        {
            change_term = false;
            string[] comm_Tbl = { "Term_tbl", "Get_Closing_Date_To_Cancel_tbl", "Rem_Confirmation_tbl", "Rem_Confirmation_tbl1", "Rem_Confirmation_tbl2" };

            foreach (string Tbl in comm_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);

                }

            }



        }

      
    }
}

//************************************************************************************.