﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class CCP_List_Add_Upd : Form
    {
        #region Defintion
        String Sql_Text = "";
        string cur_text = "";
        BindingSource _BS_CCp_list = new BindingSource();
        public static Int64 current_cur_id = 0;
        #endregion
        //--------------------------------------------------------------------------------------
        public CCP_List_Add_Upd()
        {
            InitializeComponent();

            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), Txt_cur_date, TxtTerm_Name);
            Grd_CCp_List.AutoGenerateColumns = false;
            #endregion
            if (connection.Lang_id != 1)
            {
                Grd_CCp_List.Columns["Column1"].DataPropertyName = "Cur_ENAME";
            }
        }
        //--------------------------------------------------------------------------------------
        private void CCP_List_Add_Upd_Load(object sender, EventArgs e)
        {
            Grd_CCp_List.AutoGenerateColumns = false;
            // Grd_CCp_List.Columns["Column3"].DefaultCellStyle.Format = "N7";
            _BS_CCp_list.DataSource = connection.SqlExec("Exec Main_CCP_List " + connection.T_ID, "CCp_List_TBl");
            Grd_CCp_List.DataSource = _BS_CCp_list;
        }
        //--------------------------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {

            DataTable DT_CCP_List = new DataTable();

            DT_CCP_List = connection.SQLDS.Tables["CCp_List_TBl"].DefaultView.ToTable(false, "Cur_ANAME", "MIN_B_PRICE", "MAX_B_PRICE", "MIN_S_PRICE", "MAX_S_PRICE", "Cur_Id","B_Rem_Global_amount", "S_Rem_Global_amount").Select().CopyToDataTable();
            DT_CCP_List.Columns.Add("T_id");

            foreach (DataGridViewRow DRow1 in Grd_CCp_List.Rows)
            {

                string Mcur_id = DRow1.Cells["Column7"].Value.ToString();
                cur_text = cur_text + Mcur_id + ',';

                #region Validation
                if (Convert.ToDouble(DRow1.Cells["Column3"].Value) != 0.000 && (Convert.ToDouble(DRow1.Cells["Column4"].Value) != 0.000) && (Convert.ToDouble(DRow1.Cells["Column5"].Value) != 0.000) && (Convert.ToDouble(DRow1.Cells["Column6"].Value) != 0.000))
                {

                    if ((Convert.ToDouble(DRow1.Cells["Column3"].Value) == 0.000) || (Convert.ToString(DRow1.Cells["Column3"].Value) == ""))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال قيمة الحد الادنى للشراء رمز العملة" + Mcur_id : "Insert Minimum Price for Buy please Cur_Id " + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                    if ((Convert.ToDouble(DRow1.Cells["Column4"].Value) == 0.000) || (Convert.ToString(DRow1.Cells["Column4"].Value) == ""))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال قيمة الحد الاعلى للشراء رمز العملة" + Mcur_id : "Insert Maximum Price for Buy please Cur_Id" + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                    if ((Convert.ToDouble(DRow1.Cells["Column5"].Value) == 0.000) || (Convert.ToString(DRow1.Cells["Column5"].Value) == ""))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الرجاء ادخال قيمة الحد الادنى للبيع رمز العملة" + Mcur_id : "Insert Minimum Price for Sale  please Cur_Id" + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                    if ((Convert.ToDouble(DRow1.Cells["Column6"].Value) == 0.000) || (Convert.ToString(DRow1.Cells["Column6"].Value) == ""))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال قيمة الحد الاعلى للبيع رمز العملة " + Mcur_id : "Insert Maximum Price for Sale  please Cur_id" + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToDouble(DRow1.Cells["Column4"].Value) < (Convert.ToDouble(DRow1.Cells["Column3"].Value)))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الحد الاعلى للشراء يجب ان يكون اكبر من الحد الادنى للشراء رمز العملة" + Mcur_id : " Maximum Price for Buy Must be Greater than  Minimum Price for Buy  Cur_id " + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToDouble(DRow1.Cells["Column6"].Value) < (Convert.ToDouble(DRow1.Cells["Column5"].Value)))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الحد الاعلى للبيع يجب ان يكون اكبر من الحد الادنى للبيع رمز العملة" + Mcur_id : " Maximum Price for Sale Must be Greater than  Minimum Price for Sale  Cur_id " + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }

                    if (Convert.ToDouble(DRow1.Cells["Column5"].Value) < Convert.ToDouble(DRow1.Cells["Column3"].Value))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الشراءالحد الادنى يجب ان يكون اقل من سعر البيع الحد الادنى رمز العملة" + Mcur_id : "Minimum Buy Price must be smaller than Minimum Sale Price" + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToDouble(DRow1.Cells["Column6"].Value) < Convert.ToDouble(DRow1.Cells["Column4"].Value))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الشراءالحد الاعلى يجب ان يكون اقل من البيع الحد الاعلى رمز العملة" + Mcur_id : "Maximum Price for Buy Must be Smaller  Minimum Price for Sale" + Mcur_id, MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                #endregion

            }

            connection.SQLCMD.Parameters.AddWithValue("@Tbl_CCP_List", DT_CCP_List);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@cur_text", cur_text);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            MyGeneral_Lib.Copytocliptext("Add_Upd_CCP_List", connection.SQLCMD);
            connection.SqlExec("Add_Upd_CCP_List", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);

        }
        //--------------------------------------------------------------------------------------
        private void Grd_CCp_List_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_BS_CCp_list);
        }
        //--------------------------------------------------------------------------------------
        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            string Sqltext_rpt = "SELECT     A.Cur_ID, MIN_B_PRICE, MAX_B_PRICE, MIN_S_PRICE, MAX_S_PRICE,B_Rem_Global_amount,S_Rem_Global_amount,"
                           + " isnull (A.U_DATE ,A.C_DATE) As C_Date , Cur_ANAME ,Cur_ENAME , User_Name , Exch_Rate "
                           + " FROM         CCP_LIST A, Cur_Tbl B , USERS C , Currencies_Rate D "
                           + " where A.Cur_ID = B.Cur_ID "
                           + " And A.USER_ID = C.USER_ID "
                           + " And A.T_ID = " + connection.T_ID
                           + " And MIN_B_PRICE <>0 "
                           + " And A.Cur_ID = D.Cur_ID "
                           + "And Exch_Rate <> 0";
            connection.SqlExec(Sqltext_rpt, "Export_Dt");
            DataTable EXp_Dt = connection.SQLDS.Tables["Export_Dt"].DefaultView.ToTable(false, "Cur_Id",
            connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Exch_Rate", "MIN_B_PRICE",
            "MAX_B_PRICE", "MIN_S_PRICE", "MAX_S_PRICE", "B_Rem_Global_amount", "S_Rem_Global_amount").Select().CopyToDataTable();
            DataTable[] _EXP_DT = { EXp_Dt };
            DataGridView[] Export_GRD = { Grd_CCp_List };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

        }
        //--------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------------------------------
        private void ExtBtn_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------------------------------
        private void Customer_Lmt_Acc_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Str = { "CCp_List_TBl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void CCP_List_Add_Upd_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            current_cur_id = Convert.ToInt64(((DataRowView)_BS_CCp_list.Current).Row["cur_id"]);

            Sql_Text = " Exec reveal_hst_rate_cur " + current_cur_id;
            connection.SqlExec(Sql_Text, "tbl_hst_rate_cur_new");


            if (connection.SQLDS.Tables["tbl_hst_rate_cur_new"].Rows.Count > 0)
            {

                reveal_hst_rate_cur1 AddFrm = new reveal_hst_rate_cur1();
                AddFrm.ShowDialog(this);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 2 ? "There is no historical information" : "لا توجد معلومات تاريخية", MyGeneral_Lib.LblCap);
            }
        }
    }
}