﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Reveal_ACC_Balance : Form
    {
        #region Defintion

        string @format = "dd/MM/yyyy";
        string @format1 = "yyyy/MM/dd";
        string @format_null = "";
        //string @format_null = " ";
        DataTable DT_Cust = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        int T_Id = 0;
        string Filter = "";
        DataTable DT_Term = new DataTable();
        DataTable DT_Acc = new DataTable();
        DataTable DT_Term_TBL = new DataTable();
        DataTable DT_Acc_TBL = new DataTable();
        int T_ID = 0;
        int Acc_Id = 0;
        string Str_Id = "";
        string Con_Str = "";
        int Style = 0;
        //string from_date_null = "";
        //int from_date_null_int = 0;
        Int32 from_nrec_date = 0;
        //int to_date_null_int = 0;
        Int32 to_nrec_date = 0;
        public static Int16 level = 0;
        public static Int16 Balance = 0;
        decimal DOloc_Amount = 0;
        decimal COloc_Amount = 0;
        decimal DPloc_Amount = 0;
        decimal Cploc_Amount = 0;
        decimal DEloc_Amount = 0;
        decimal CEloc_Amount = 0;
        BindingSource _bsyears = new BindingSource();
        bool Change_grd = false;
        BindingSource bs_fromdate = new BindingSource();
        int min_nrec_date_int = 0;
        int from_min_nrec_date_int = 0;
        #endregion

        public Search_Reveal_ACC_Balance()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Cur.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Acc.AutoGenerateColumns = false;
            Create_Tbl();
            Grd_years.AutoGenerateColumns = false; 
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Column15.DataPropertyName = "Acc_Ename";
                Column16.DataPropertyName = "Acc_Ename";
                Column2.DataPropertyName = "ECUST_NAME";
                Column5.DataPropertyName = "ECUST_NAME";
                ////////////////CboDisplay.Items.Clear();
                ////////////////CboDisplay.Items.Add("Detailed Account");
                ////////////////CboDisplay.Items.Add("Accumulation Account");
                Cbo_style.Items[0] = "The balance end of the period for the company";
                Cbo_style.Items[1] = "The balance period for the company";
                Cbo_style.Items[2] = "The balance end of the period for each branch";
                Cbo_style.Items[3] = "The balance period for each branch";


                cbo_year.Items.Clear();
                cbo_year.Items.Add("current Data");
                cbo_year.Items.Add("Old period data");
            }
            #endregion
        }

        private void Reveal_Balance_Local_Amount_Load(object sender, EventArgs e)
        {
            cbo_year.SelectedIndex = 0;
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;

            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            connection.SqlExec("Exec Search_Account_Balance_new ", "Account_Balance_new_Tbl");

            Cbo_Level.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl2"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "AAcc_Level" : "EAcc_Level";
            Cbo_Level.SelectedIndex = 0;
            Cbo_style.SelectedIndex = 0;
            //TxtFromDate.CustomFormat = " ";
            //TxtToDate.CustomFormat = " ";

        }
        private void Create_Tbl()
        {
            //---
            string[] Column2 = { "Acc_id", "Acc_ANAME", "Acc_Ename", "Dot_Acc_No" };
            string[] DType2 = { "System.Int16", "System.String", "System.String", "System.String" };
            DT_Acc = CustomControls.Custom_DataTable("DT_Acc", Column2, DType2);
            Grd_Acc.DataSource = DT_Acc;
            //-----

            string[] Column = { "T_ID", "ACUST_NAME", "ECUST_NAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Term = CustomControls.Custom_DataTable("DT_Term", Column, DType);
            Grd_Cur.DataSource = DT_Term;


        }
        //----------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------
        private void Search_Trial_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Account_Balance_new_Tbl", "Account_Balance_new_Tbl1", "Account_Balance_new_Tbl2", "Account_Balance_new_Tbl3", "Reveal_Balance_Main_Tbl", "Account_Balance_new_Tbl4" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //----------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {

            #region Validation
            string InRecDate = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);

            #endregion
            DataTable DT_T = new DataTable();
            DataTable ACC_Tbl = new DataTable();
            string T_IDStr = "";
            string Acc_IDStr = "";
            MyGeneral_Lib.ColumnToString(DT_Term, "T_ID", out  T_IDStr);
            MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out  Acc_IDStr);


            if (cbo_year.SelectedIndex == 0)
            {

                //DataTable dt = new DataTable();
                //dt = connection.SQLDS.Tables["HeaderPage_Tbl2"];
                //DateTime from_date = Convert.ToDateTime(dt.Rows[0]["From_Nrec_Date"]);
                //int From_date = 0;

                //From_date = from_date.Day + from_date.Month * 100 + from_date.Year * 10000;
                

                if (TxtFromDate.Checked == true)
                {

                    DateTime date = TxtFromDate.Value.Date;
                    from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;


                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                //if (from_nrec_date < From_date)
                //{
                //    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء التأكد  من تاريخ ابتداء الفترة" : "Please Check from date", MyGeneral_Lib.LblCap);
                //    return;
                //}
                if (TxtToDate.Checked == true)
                {

                    DateTime date = TxtToDate.Value.Date;
                    to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    to_nrec_date = 0;
                }


                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }

                }

                if (Convert.ToInt32(from_nrec_date) > Convert.ToInt32(to_nrec_date) && Convert.ToInt32(to_nrec_date) != 0)

                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            if (cbo_year.SelectedIndex == 1)//old
            {
                if (TxtFromDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                DateTime date_from = TxtFromDate.Value.Date;
                from_nrec_date = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
                DateTime date_to = TxtToDate.Value.Date;
                to_nrec_date = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

                int Real_from_date = Convert.ToInt32(connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["min_nrec_date_int"]);
                int Real_To_date = Convert.ToInt32(connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["max_nrec_date_int"]);

                if ((from_nrec_date < Real_from_date) || (from_nrec_date > Real_To_date) || (to_nrec_date < Real_from_date) || (to_nrec_date > Real_To_date))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (from_nrec_date > to_nrec_date)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }

            }

            level = Convert.ToInt16(Cbo_Level.SelectedValue);
            try
            {
                ACC_Tbl = DT_Acc.DefaultView.ToTable(false, "Acc_Id").Select().CopyToDataTable();
            }
            catch
            {
                ACC_Tbl = connection.SQLDS.Tables["Account_Balance_new_Tbl"].DefaultView.ToTable(false, "Acc_Id").Select().CopyToDataTable();
            }
            try
            {
                DT_T = DT_Term.DefaultView.ToTable(false, "T_ID").Select().CopyToDataTable();
            }
            catch
            {
                DT_T = connection.SQLDS.Tables["Account_Balance_new_Tbl1"].DefaultView.ToTable(false, "T_ID").Select().CopyToDataTable();
            }
            try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "Account_Balancing_Nnew" : "Account_Balancing_Nnew_PHst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                if (cbo_year.SelectedIndex == 0)//cureent
                {
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@Tid_Tbl", DT_T);
                    connection.SQLCMD.Parameters.AddWithValue("@Accid_Tbl", ACC_Tbl);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Close", CHK_Zero_Balancing.Checked == true ? 1 : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_level", level);
                    connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
                    connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Style", Style);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Termials", Chk_Cur.Checked == true ? 1 : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_ACC", Chk_Acc.Checked == true ? 1 : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Remove_Zero", Chk_Remove_Zero.Checked);//استبعاد الارصدة المصفرة
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@DOloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@DOloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@COloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@COloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@DPloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@DPloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@Cploc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@Cploc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@DEloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@DEloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@CEloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@CEloc_Amount"].Direction = ParameterDirection.Output;
                }
                else
                {
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@Tid_Tbl", DT_T);
                    connection.SQLCMD.Parameters.AddWithValue("@Accid_Tbl", ACC_Tbl);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Close", CHK_Zero_Balancing.Checked == true ? 1 : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_level", level);
                    connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
                    connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Style", Style);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Termials", Chk_Cur.Checked == true ? 1 : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_ACC", Chk_Acc.Checked == true ? 1 : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Remove_Zero", Chk_Remove_Zero.Checked);//استبعاد الارصدة المصفرة
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@DOloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@DOloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@COloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@COloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@DPloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@DPloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@Cploc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@Cploc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@DEloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@DEloc_Amount"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@CEloc_Amount", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@CEloc_Amount"].Direction = ParameterDirection.Output;
                    //connection.SQLCMD.Parameters.AddWithValue("@p_id", Convert.ToByte(((DataRowView)_bsyears.Current).Row["P_ID"]));// 
                    //connection.SQLCMD.Parameters.AddWithValue("@chk_close_year", Convert.ToByte(checkBox1.Checked));// clsoe year
                }
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl1");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl2");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl3");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl4");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl5");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl6");
                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }

                if (Chk_Remove_Zero.Checked == true)
                {
                    DOloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@DOloc_Amount"].Value);
                    COloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@COloc_Amount"].Value);
                    DPloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@DPloc_Amount"].Value);
                    Cploc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@Cploc_Amount"].Value);
                    DEloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@DEloc_Amount"].Value);
                    CEloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@CEloc_Amount"].Value);
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }


            connection.SQLCMD.Parameters.Clear();

            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود " : "No Data To export ", MyGeneral_Lib.LblCap);
            //    return;
            //}
            Int16 chk_zero_end = 0;
            chk_zero_end = Convert.ToInt16(Chk_Remove_Zero.Checked);

            if (Cbo_style.SelectedIndex == 0)
            {

                Terminal_total_Reveal_Trial_balance Frm = new Terminal_total_Reveal_Trial_balance(2, TxtFromDate.Text, TxtToDate.Text, from_nrec_date, to_nrec_date, Balance, level, chk_zero_end, DEloc_Amount, CEloc_Amount);
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }
            if (Cbo_style.SelectedIndex == 1)
            {
                Terminal_Details_trail_balance Frm = new Terminal_Details_trail_balance(2, TxtFromDate.Text, TxtToDate.Text, from_nrec_date, to_nrec_date, Balance, level, chk_zero_end, DOloc_Amount, COloc_Amount, DPloc_Amount, Cploc_Amount, DEloc_Amount, CEloc_Amount);
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }
            if (Cbo_style.SelectedIndex == 2)
            {

                Every_Terminal_total_reveal_balance Frm = new Every_Terminal_total_reveal_balance(TxtFromDate.Text, TxtToDate.Text, from_nrec_date, to_nrec_date, Balance, level, chk_zero_end, DEloc_Amount, CEloc_Amount);
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }
            if (Cbo_style.SelectedIndex == 3)
            {

                Every_terminal_details_trial_balance Frm = new Every_terminal_details_trial_balance(TxtFromDate.Text, TxtToDate.Text, from_nrec_date, to_nrec_date, Balance, level, chk_zero_end, DOloc_Amount, COloc_Amount, DPloc_Amount, Cploc_Amount, DEloc_Amount, CEloc_Amount);
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }
            string[] Str = { "Account_Balancing_new_tbl", "Account_Balancing_new_tbl1", "Account_Balancing_new_tbl2", "Account_Balancing_new_tbl3", "Account_Balancing_new_tbl4", "Account_Balancing_new_tbl5", "Account_Balancing_new_tbl6" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
        }
        //----------------------
        private void Chk_Acc_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Acc.Checked)
            {
                TxtAcc_Name.Enabled = true;
                TxtAcc_Name_TextChanged(null, null);
            }
            else
            {
                DT_Acc.Clear();
                DT_Acc_TBL.Clear();
                TxtAcc_Name.ResetText();
                button8.Enabled = false;
                button7.Enabled = false;
                button6.Enabled = false;
                button5.Enabled = false;
                TxtAcc_Name.Enabled = false;
            }
        }
        //----------------------
        private void TxtAcc_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Acc_Id = 0;
                Con_Str = "";
                Str_Id = "";
                if (DT_Acc.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                    Con_Str = " And Acc_Id not in(" + Str_Id + ")";
                }
                int.TryParse(TxtAcc_Name.Text, out Acc_Id);

                if (Convert.ToInt16(Cbo_Level.SelectedValue) != 0)
                { Con_Str += " And Acc_Level = " + Cbo_Level.SelectedValue.ToString(); }

                if (Convert.ToInt16(Cbo_Level.SelectedValue) == 0)
                { Con_Str += " And record_flag = 1 "; }

                Filter = " (Acc_Aname like '" + TxtAcc_Name.Text + "%' or  Acc_Ename like '" + TxtAcc_Name.Text + "%'"
                            + " Or Acc_id = " + Acc_Id + ")" + Con_Str;

                DT_Acc_TBL = connection.SQLDS.Tables["Account_Balance_new_Tbl"].DefaultView.ToTable(true, "Acc_Id", "Acc_Aname", "Acc_Ename", "Acc_Level", "Dot_Acc_No", "record_flag").Select(Filter).CopyToDataTable();


                Grd_Acc_Id.DataSource = DT_Acc_TBL;

            }
            catch
            {
                Grd_Acc_Id.DataSource = new DataTable();
            }
            if (Grd_Acc_Id.Rows.Count <= 0)
            {

                button8.Enabled = false;
                button7.Enabled = false;
            }
            else
            {
                button8.Enabled = true;
                button7.Enabled = true;
            }
        }

        private void Cbo_Level_SelectedIndexChanged(object sender, EventArgs e)
        {
            Chk_Acc_CheckedChanged(null, null);
            DT_Acc.Clear();
            DT_Term.Clear();
            DT_Cust.Clear();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Acc.NewRow();

            row["Acc_id"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_Id"];
            row["Acc_aname"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_aname"];
            row["Acc_ename"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_ename"];
            row["Dot_Acc_No"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Dot_Acc_No"];
            DT_Acc.Rows.Add(row);
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);

            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Acc_Id.Rows.Count == 0)
            {
                button8.Enabled = false;
                button7.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Acc_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Acc.NewRow();
                row["Acc_id"] = DT_Acc_TBL.Rows[i]["Acc_id"];
                row["Acc_aname"] = DT_Acc_TBL.Rows[i]["Acc_aname"];
                row["Acc_ename"] = DT_Acc_TBL.Rows[i]["Acc_ename"];
                row["Dot_Acc_No"] = DT_Acc_TBL.Rows[i]["Dot_Acc_No"];
                DT_Acc.Rows.Add(row);
            }
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button8.Enabled = false;
            button7.Enabled = false;
            button6.Enabled = true;
            button5.Enabled = true;
            Chk_Cur_CheckedChanged(null, null);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows[Grd_Acc.CurrentRow.Index].Delete();
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button7.Enabled = true;
            button8.Enabled = true;
            if (Grd_Acc.Rows.Count == 0)
            {
                button6.Enabled = false;
                button5.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows.Clear();
            TxtAcc_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = false;
            button5.Enabled = false;
            Chk_Cur_CheckedChanged(null, null);
        }

        private void Chk_Cur_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked)
            {
                TxtCur_Name.Enabled = true;
                TxtCur_Name_TextChanged(null, null);
            }
            else
            {
                DT_Term.Clear();
                DT_Term_TBL.Clear();
                TxtCur_Name.ResetText();
                button12.Enabled = false;
                button11.Enabled = false;
                button10.Enabled = false;
                button9.Enabled = false;
                TxtCur_Name.Enabled = false;
            }
        }

        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Acc_Id = 0;
                T_ID = 0;
                Str_Id = "";
                Con_Str = "";


                if (DT_Term.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Term, "T_ID", out Str_Id);
                    Con_Str += " And  T_ID not in(" + Str_Id + ")";
                }
                int.TryParse(TxtCur_Name.Text, out T_ID);

                Filter = " (ACUST_NAME like '" + TxtCur_Name.Text + "%' or  ECUST_NAME like '" + TxtCur_Name.Text + "%'"
                            + " Or T_ID = " + T_ID + ")" + Con_Str;
                # endregion
                DT_Term_TBL = connection.SQLDS.Tables["Account_Balance_new_Tbl1"].Select(Filter).CopyToDataTable();
                DT_Term_TBL = DT_Term_TBL.DefaultView.ToTable(true, "T_ID", "ACUST_NAME", "ECUST_NAME").Select().CopyToDataTable();
                Grd_Cur_Id.DataSource = DT_Term_TBL;
            }
            catch
            {
                Grd_Cur_Id.DataSource = new DataTable();
            }
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                button12.Enabled = false;
                button11.Enabled = false;
            }
            else
            {
                button12.Enabled = true;
                button11.Enabled = true;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Term.NewRow();

            row["T_ID"] = DT_Term_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["T_ID"];
            row["ACUST_NAME"] = DT_Term_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["ACUST_NAME"];
            row["ECUST_NAME"] = DT_Term_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["ECUST_NAME"];
            DT_Term.Rows.Add(row);
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);

            button10.Enabled = true;
            button9.Enabled = true;
            if (Grd_Cur_Id.Rows.Count == 0)
            {
                button12.Enabled = false;
                button11.Enabled = false;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Term_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Term.NewRow();
                row["T_ID"] = DT_Term_TBL.Rows[i]["T_ID"];
                row["ACUST_NAME"] = DT_Term_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Term_TBL.Rows[i]["ECUST_NAME"];
                DT_Term.Rows.Add(row);
            }
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button12.Enabled = false;
            button11.Enabled = false;
            button10.Enabled = true;
            button9.Enabled = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DT_Term.Rows[Grd_Cur.CurrentRow.Index].Delete();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button12.Enabled = true;
            button11.Enabled = true;
            if (Grd_Cur.Rows.Count == 0)
            {
                button10.Enabled = false;
                button9.Enabled = false;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DT_Term.Rows.Clear();
            TxtCur_Name_TextChanged(null, null);
            button12.Enabled = true;
            button11.Enabled = true;
            button10.Enabled = false;
            button9.Enabled = false;
        }

        private void Search_Balance_Local_Amount_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_style.SelectedIndex == 0)
            {
                Style = 1;
            }
            if (Cbo_style.SelectedIndex == 1)
            {
                Style = 2;
            }
            if (Cbo_style.SelectedIndex == 2)
            {
                Style = 3;
            }
            if (Cbo_style.SelectedIndex == 3)
            {
                Style = 4;
            }

        }

        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            //if (TxtFromDate.Checked == true)
            //{
            //    TxtFromDate.Format = DateTimePickerFormat.Custom;
            //    TxtFromDate.CustomFormat = @format;
            //}
            //else
            //{
            //    TxtFromDate.Format = DateTimePickerFormat.Custom;
            //    TxtFromDate.CustomFormat = " ";
            //}
        }

        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            //if (TxtToDate.Checked == true)
            //{
            //    TxtToDate.Format = DateTimePickerFormat.Custom;
            //    TxtToDate.CustomFormat = @format;

            //}
            //else
            //{
            //    TxtToDate.Format = DateTimePickerFormat.Custom;
            //    TxtToDate.CustomFormat = " ";
            //}
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();

                TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

                TxtToDate.Checked = false;
               // checkBox1.Visible = false;

            }
            if (cbo_year.SelectedIndex == 1)// old 
            {
                try
                {

                    Grd_years.Enabled = true;
                    //_bsyears.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl3"];
                    //Grd_years.DataSource = _bsyears;
                    if (connection.SQLDS.Tables["Account_Balance_new_Tbl3"].Rows.Count > 0)
                    {
                        Grd_years.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl3"].Select("t_id =" + connection.T_ID).CopyToDataTable();


                        TxtFromDate.Text = connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["min_nrec_date"].ToString();
                        TxtToDate.Text = connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["max_nrec_date"].ToString();

                        // Change_grd = true;
                        // Grd_years_SelectionChanged(null, null);
                        // checkBox1.Visible = true;
                    }

                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                        cbo_year.SelectedIndex = 0;
                        return;
                    }
                }

                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }
            }
        }

        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{

        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");

        //    }
        //}
    }
}