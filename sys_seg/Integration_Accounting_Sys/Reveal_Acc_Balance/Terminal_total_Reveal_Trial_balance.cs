﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Terminal_total_Reveal_Trial_balance : Form
    {
        #region Definshion

        BindingSource BS_1 = new BindingSource();
        BindingSource BS_2 = new BindingSource();
        BindingSource BS_3 = new BindingSource();
        BindingSource BS_4 = new BindingSource();
        BindingSource BS_5 = new BindingSource();
        BindingSource BS_7 = new BindingSource();
        DataTable Grid2_tbl = new DataTable();
        DataTable DT_1 = new DataTable();
        string Acc_no_current = "";
        Int16 T_id_current = 0;
        Int32 Cust_id_current = 0;
        Int16 Cur_id_current = 0;
        Int32 Acc_id_current = 0;
        Int32 Vo_no_current = 0;
        Int16 Oper_id_current = 0;
        string nrec_date_current;
        bool Xchange1 = false;
        bool Xchange2 = false;
        bool Xchange3 = false;
        bool Xchange4 = false;
        string Dot_Acc_no_current = "";
        Int16 level_rep = 0;
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataGridView Date_Grd2 = new DataGridView();
        DataTable Date_Tbl2 = new DataTable();
        DataTable Dt_Gr = new DataTable();
        Int16 Remove_Zero = 0;
        //Int16 form_no_new = 0;
        decimal DEloc_Amount1 = 0;
        decimal CEloc_Amount1 = 0;
        Int16 form_no_new;
        #endregion
        public Terminal_total_Reveal_Trial_balance(Int16 frm_no_new, string FromDate, string ToDate, Int32 from_nrec_date, Int32 to_nrec_date, Int16 Balance, Int16 level, Int16 chk_zero_end,
         decimal DEloc_Amount = 0, decimal CEloc_Amount = 0)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            form_no_new = frm_no_new;

            G1.AutoGenerateColumns = false;
            G2.AutoGenerateColumns = false;
            G3.AutoGenerateColumns = false;
            G4.AutoGenerateColumns = false;
            G5.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Column7.DataPropertyName = "Acc_EName";
                // dataGridViewTextBoxColumn2.DataPropertyName = "ETerm_Name";
                Column10.DataPropertyName = "Ecust_Name";
                Column39.DataPropertyName = "Acc_EName";
                Column11.DataPropertyName = "cur_EName";
                Column22.DataPropertyName = "EOPER_NAME";
                dataGridViewTextBoxColumn10.DataPropertyName = "cur_ename";
                dataGridViewTextBoxColumn11.DataPropertyName = "Ecust_Name";
                dataGridViewTextBoxColumn12.DataPropertyName = "EOPER_NAME";
                Column10.DataPropertyName = "Ecust_Name";
                Column11.DataPropertyName = "cur_EName";
                Column22.DataPropertyName = "EOPER_NAME";
                dataGridViewTextBoxColumn9.DataPropertyName = "Acc_Ename";
            }
            level_rep = level;
            Remove_Zero = chk_zero_end;

            DEloc_Amount1 = DEloc_Amount;
            CEloc_Amount1 = CEloc_Amount;
        }

        private void Terminal_total_Reveal_Trial_balance_Load(object sender, EventArgs e)
        {

            Xchange1 = false;
            if (Search_Trail_Balance_new_Shadow.Balance == 0)
            { CHK_Zero_Balancing.Checked = false; }
            else { CHK_Zero_Balancing.Checked = true; }

            //-----------------------------------------------
            if (Remove_Zero == 1)
            { Chk_Remove_Zero.Checked = true; }

            //BS_7.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"];

            BS_1.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
            G1.DataSource = BS_1;
            if (G1.RowCount == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود " : "No Data To export ", MyGeneral_Lib.LblCap);

                Btn_Ext_Click(null, null);
                return;
            }

            if (level_rep == 0)
            {
                G3.Columns[2].Visible = false;
                G3.Columns[3].Visible = false;
            }
            if (Chk_Remove_Zero.Checked == false)
            {
                DataTable DT2 = new DataTable();
                DT2 = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                TxtED_LocalAmount.Text = DT2.Compute("Sum(DEloc_Amount)", "").ToString();
                TxtEC_LocalAmount.Text = DT2.Compute("Sum(CEloc_Amount)", "").ToString();
            }
            else
            {
                TxtED_LocalAmount.Text = DEloc_Amount1.ToString();
                TxtEC_LocalAmount.Text = CEloc_Amount1.ToString();

            }
            Xchange1 = true;
            G1_SelectionChanged(null, null);
        }
        //--------------------------------------------------------------
        private void G1_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange1)
            {
                try
                {
                    Acc_no_current = ((DataRowView)BS_1.Current).Row["acc_no"].ToString();
                    Dot_Acc_no_current = ((DataRowView)BS_1.Current).Row["Dot_acc_no"].ToString();
                    Xchange2 = false;
                    if (level_rep == 0)
                    {
                        BS_2.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable().Select("acc_no = '" + Acc_no_current + "'").CopyToDataTable();
                    }
                    else
                    {
                        DataTable _Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable().Select("Dot_acc_no like'" + Dot_Acc_no_current + "%'").CopyToDataTable(); ;
                        var query = from row in _Dt.AsEnumerable()
                                    group row by new
                                    {

                                        T_id = row.Field<Int16>("T_id"),
                                        ATerm_Name = connection.Lang_id == 1 ? row.Field<string>("ATerm_Name") : row.Field<string>("ETerm_Name"),
                                        From_Date1 = row.Field<DateTime>("From_Date1"),
                                        To_Date1 = row.Field<DateTime>("To_Date1"),
                                    } into grp
                                    orderby grp.Key.T_id
                                    select new
                                    {
                                        Key = grp.Key,
                                        T_id = grp.Key.T_id,
                                        ATerm_Name = grp.Key.ATerm_Name,
                                        DEloc_Amount = grp.Sum(r => r.Field<decimal>("DEloc_Amount")),
                                        CEloc_Amount = grp.Sum(r => r.Field<decimal>("CEloc_Amount")),
                                        From_Date1 = grp.Key.From_Date1,
                                        To_Date1 = grp.Key.To_Date1,


                                    };


                        DT_1 = CustomControls.IEnumerableToDataTable(query);
                        BS_2.DataSource = DT_1;



                    }
                    G2.DataSource = BS_2;
                    Xchange2 = true;
                    G2_SelectionChanged(null, null);
                }
                catch
                {
                    G2.DataSource = new DataTable();
                    G3.DataSource = new DataTable();
                }

            }
        }
        //--------------------------------------------------------------
        private void G2_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange2)
            {
                try
                {
                    Xchange3 = false;
                    T_id_current = Convert.ToInt16(((DataRowView)BS_2.Current).Row["T_id"]);
                    if (level_rep == 0)
                    {
                        BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and acc_no = '" + Acc_no_current + "'").CopyToDataTable();
                    }
                    else
                    {
                        BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and Dot_acc_no like'" + Dot_Acc_no_current + "%'").CopyToDataTable();
                    }
                    G3.DataSource = BS_3;
                    Xchange3 = true;
                    G3_SelectionChanged(null, null);
                }
                catch
                {
                    G3.DataSource = new DataTable();
                }

            }

        }
        //--------------------------------------------------------------
        private void G3_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange3)
            {
                try
                {
                    Xchange4 = false;
                    //  DataTable Dt = new DataTable();
                    decimal MBalance = 0;
                    decimal MBalance_loc = 0;
                    Cust_id_current = Convert.ToInt16(((DataRowView)BS_3.Current).Row["Cust_id"]);
                    Cur_id_current = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_Cur_id"]);
                    Acc_id_current = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Acc_id"]);
                    //BS_4.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl3"].DefaultView.ToTable()
                    //.Select("T_id = " + T_id_current + "and Cust_id = " + Cust_id_current + "and for_Cur_id = " + Cur_id_current + "and Acc_id = " + Acc_id_current).CopyToDataTable();


                    try
                    {
                        Dt_Gr = connection.SQLDS.Tables["Account_Balancing_new_tbl3"].Select("Cust_Id = " + Cust_id_current
                                                                                                       + " And For_Cur_Id = " + Cur_id_current
                                                                                                       + " and Acc_id = " + Acc_id_current
                                                                                                       + " And T_id = " + T_id_current).CopyToDataTable();


                        DataView dv = Dt_Gr.DefaultView;
                        dv.Sort = "c_date";
                        Dt_Gr = dv.ToTable();

                    }
                    catch
                    {
                        Dt_Gr = new DataTable();
                    }
                    foreach (DataRow Drow in Dt_Gr.Rows)
                    {
                        if (Convert.ToInt32(Drow["RowNum"]) == 0)
                        {
                            MBalance = Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]) * -1;
                            MBalance_loc = Convert.ToDecimal(Drow["Dloc_Amount"]) + Convert.ToDecimal(Drow["Cloc_Amount"]) * -1;
                        }

                        if (Convert.ToInt32(Drow["RowNum"]) > 0)
                        {
                            MBalance += Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]) * -1;
                            MBalance_loc += Convert.ToDecimal(Drow["Dloc_Amount"]) + Convert.ToDecimal(Drow["Cloc_Amount"]) * -1;
                        }
                        Drow["Balance_for"] = MBalance;
                        Drow["Balance_loc"] = MBalance_loc;
                        //Drow["CFor_Amount"] = Math.Abs(Convert.ToDecimal(Drow["CFor_Amount"]));
                    }
                    BS_4.DataSource = Dt_Gr;





                    G4.DataSource = BS_4;
                    Xchange4 = true;
                    G4_SelectionChanged(null, null);
                }
                catch
                {
                    G4.DataSource = new DataTable();
                }
            }
        }
        //--------------------------------------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
        }
        //--------------------------------------------------------------
        private void Btn_Browser_Click_1(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                DataGridView[] Export_GRD = { G1 };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no",connection.Lang_id==1? "Acc_AName" : "Acc_EName", "DEloc_Amount",
                "CEloc_Amount").Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //--------------------------------------------------------------

        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
            if (level_rep == 0)
            {
                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                DataGridView G7 = new DataGridView();
                G7.Columns.Add("Column1", connection.Lang_id == 1 ? "الرصيد الاجمالي" : "Total Amount");
                G7.Columns.Add("Column2", connection.Lang_id == 1 ? "الرصيد الفرعي " : "Branch amount");
                G7.Columns.Add("Column3", connection.Lang_id == 1 ? "إسم الحساب" : "Accountant name");
                G7.Columns.Add("Column4", connection.Lang_id == 1 ? "رمز الثانوي" : "Customer NO");
                G7.Columns.Add("Column5", connection.Lang_id == 1 ? "إسم الثانوي" : "Customer name");
                G7.Columns.Add("Column6", connection.Lang_id == 1 ? "العملــــة" : "Currency");
                G7.Columns.Add("Column7", connection.Lang_id == 1 ? "الرصيد _(ع.ص)_" : "Last Balance (F.C)");
                G7.Columns.Add("Column8", connection.Lang_id == 1 ? "الرصيد _(ع.م)_" : "Last Balance (L.C)");
                DataGridView[] Export_GRD = { G7 };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_Balancing_new_tbl4"].DefaultView.ToTable(false ,"Eloc_Amount","term_eloc",
          connection.Lang_id == 1 ?"Acc_AName":"Acc_EName","Cust_id",connection.Lang_id == 1 ?"ACUST_NAME" : "ECUST_NAME",
          connection.Lang_id == 1 ?"Cur_ANAME" :"Cur_ENAME" ,"Efor_amount","cust_eloc").Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                DataTable Dt = new DataTable();
                DataTable Dt_details = new DataTable();
                DataTable Rpt_dt_details = new DataTable();
                DataTable Export_DT_main = new DataTable();
                DataTable Export_DT_details = new DataTable();
                DataTable Export_DT_main1 = new DataTable();
                DataTable Dt1 = new DataTable();
                string Branch_name = ((DataRowView)BS_1.Current).Row[connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName"].ToString();
                string Term_name = ((DataRowView)BS_2.Current).Row["ATerm_Name"].ToString();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                Dt_details = connection.SQLDS.Tables["Account_Balancing_new_tbl2"];
                Export_DT_main = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "DEloc_Amount",
                    "CEloc_Amount").Select(connection.Lang_id == 1 ? "Acc_AName = '" : "Acc_EName = '" + Branch_name + "'").CopyToDataTable();
                Export_DT_main1 = DT_1.DefaultView.ToTable(false, "T_Id", "ATerm_Name", "DEloc_Amount",
                    "CEloc_Amount", "From_Date1", "To_Date1").Select("ATerm_Name = '" + Term_name + "'").CopyToDataTable();
                Export_DT_details = Dt_details.DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", "Acc_no",
                    connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_Amount").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { G1, G2, G3 };
                DataTable[] Export_DT = { Export_DT_main, Export_DT_main1, Export_DT_details };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
        }
        //--------------------------------------------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void G4_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange4)
            {
                Vo_no_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["Vo_No"]);
                nrec_date_current = ((DataRowView)BS_4.Current).Row["nrec_date"].ToString();
                Oper_id_current = Convert.ToInt16(((DataRowView)BS_4.Current).Row["oper_id"]);
                int Acc_id_current = Convert.ToInt16(((DataRowView)BS_4.Current).Row["ACC_Id"]);
                decimal DLoc_Amount = Convert.ToDecimal(((DataRowView)BS_4.Current).Row["DLoc_Amount"]);
                decimal PFor_Amount = Convert.ToDecimal(((DataRowView)BS_4.Current).Row["Balance_for"]);
                decimal Loc_Amount = Convert.ToDecimal(((DataRowView)BS_4.Current).Row["Loc_Amount"]);
                string notes_current = ((DataRowView)BS_4.Current).Row["notes"].ToString();
                try
                {
                    ////       BS_5.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl5"].DefaultView.ToTable()
                    ////.Select("T_id = " + T_id_current + " and Vo_No = " + Vo_no_current + " and oper_id = " + Oper_id_current + " and ACC_Id  = " + Acc_id_current + " and PFor_Amount = " + PFor_Amount + "and Nrec_date like'" + nrec_date_current + "%'").CopyToDataTable();
                    ////       G5.DataSource = BS_5;نورنور

                    BS_5.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl5"].DefaultView.ToTable()
                    .Select("T_id = " + T_id_current + " and Vo_No = " + Vo_no_current + " and oper_id = " + Oper_id_current + "and Nrec_date like'" + nrec_date_current + "%'").CopyToDataTable();
                    G5.DataSource = BS_5;

                    foreach (DataGridViewRow row in G5.Rows)
                    {
                        if (Convert.ToInt32(row.Cells[9].Value) == Convert.ToInt32(Vo_no_current)
                            && Convert.ToString(row.Cells[12].Value) == Convert.ToString(notes_current)
                            && (Convert.ToInt64(row.Cells[0].Value) == Convert.ToInt64(Loc_Amount))
                            && Convert.ToInt16(row.Cells[2].Value) == Convert.ToInt16(Acc_id_current)
                           )
                        {
                            ////////////////////// && Convert.ToDecimal( row.Cells[1].Value) == Convert.ToDecimal(PFor_Amount)
                            //&& Convert.ToInt32(row.Cells[3].Value) == Convert.ToInt32(Oper_id_current)
                            //&& Convert.ToInt32(row.Cells[11].Value) == Convert.ToInt32(nrec_date_current)
                            row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }

                    ////////////////          BS_5.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl5"].DefaultView.ToTable()
                    ////////////////.Select("T_id = " + T_id_current + " and Vo_No = " + Vo_no_current + " and oper_id = " + Oper_id_current + " and ACC_Id  = " + Acc_id_current + " and PFor_Amount = " + PFor_Amount + "and Nrec_date like'" + nrec_date_current + "%'").CopyToDataTable();

                }
                catch
                {
                    G5.DataSource = new DataTable();

                }

            }
        }

        private void Terminal_total_Reveal_Trial_balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            Xchange1 = false;
            Xchange2 = false;
            Xchange3 = false;
            Xchange4 = false;
        }

        private void print_det_btn_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Account_Balancing_new_tbl4"].Rows.Count > 0)
            {
                string[] Str = { "Acc_rev_details" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                Int32 Frm_id = 0;
                string date_from = (((DataRowView)BS_2.Current).Row["From_Date1"]).ToString();
                string to_date = (((DataRowView)BS_2.Current).Row["To_Date1"]).ToString();
                string frm_name = "";
                string frm_Ename = "";

                if (form_no_new == 1)
                {
                    frm_name = "ميزان المراجعة";
                    frm_Ename = "A balance detailed review";
                }
                else if (form_no_new == 2)
                {
                    frm_name = "كشف حساب";
                    frm_Ename = "Account statement";
                }
                else
                {
                    frm_name = "ارصدة نهاية المدة على مستوى المؤسسة";
                    frm_Ename = "The balance end of the period for the company";
                }

                string user_name = connection.User_Name;
                Acc_reveal ObjRpt = new Acc_reveal();
                Acc_reveal_eng ObjRptEng = new Acc_reveal_eng();
                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Frm_id", Frm_id);
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("user_name", user_name);
                Dt_Param.Rows.Add("date_from", date_from);
                Dt_Param.Rows.Add("to_date", to_date);
                Dt_Param.Rows.Add("frm_Ename", frm_Ename);

                connection.SQLDS.Tables["Account_Balancing_new_tbl4"].TableName = "Acc_rev_details";
                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables["Acc_rev_details"].TableName = "Account_Balancing_new_tbl4";
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للطباعة" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void print_total_btn_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                string[] Str = { "Acc_reveal" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                string frm_name = "";
                string frm_Ename = "";

                if (form_no_new == 1)
                {
                    frm_name = "ميزان المراجعة";
                    frm_Ename = "A balance detailed review";
                }
                else if (form_no_new == 2)
                {
                    frm_name = "كشف حساب";
                    frm_Ename = "Account statement";
                }
                else
                {
                    frm_name = "ارصدة نهاية المدة على مستوى المؤسسة";
                    frm_Ename = "The balance end of the period for the company";
                }

                string user_name = connection.User_Name;
                string date_from = (((DataRowView)BS_2.Current).Row["From_Date1"]).ToString();
                string date_to = (((DataRowView)BS_2.Current).Row["To_Date1"]).ToString();

                Acc_reveal_total ObjRpt = new Acc_reveal_total();
                Acc_reveal_total_eng ObjRptEng = new Acc_reveal_total_eng();
                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("user_name", user_name);
                Dt_Param.Rows.Add("date_from", date_from);
                Dt_Param.Rows.Add("date_to", date_to);
                Dt_Param.Rows.Add("frm_Ename", frm_Ename);

                connection.SQLDS.Tables["Account_Balancing_new_tbl"].TableName = "Acc_reveal";
                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables["Acc_reveal"].TableName = "Account_Balancing_new_tbl";
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للطباعة" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
        private void Date()
        {

            Date_Grd = new DataGridView();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date1", "To_Date1" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            string date_from = ((DataRowView)BS_2.Current).Row["From_Date1"].ToString();
            string date_To = ((DataRowView)BS_2.Current).Row["To_Date1"].ToString();
            Date_Tbl.Rows.Add(new object[] { date_from, date_To });


            Date_Grd2 = new DataGridView();
            Date_Grd2.Columns.Add("Column1", connection.Lang_id == 1 ? "الفــرع" : "terminal Name");
            Date_Grd2.Columns.Add("Column2", connection.Lang_id == 1 ? "رمزالحساب" : "Account id");
            Date_Grd2.Columns.Add("Column3", connection.Lang_id == 1 ? "الحســـاب" : "Account name");

            string[] Column2 = { "ATerm_Name", "ETerm_Name", "acc_no", "Acc_AName", "Acc_EName" };
            string[] DType2 = { "System.String", "System.String", "System.String", "System.String", "System.String" };
            Date_Tbl2 = CustomControls.Custom_DataTable("Date_Tbl2", Column2, DType2);
            string ATerm_Name = ((DataRowView)BS_2.Current).Row["ATerm_Name"].ToString();
            string acc_no = ((DataRowView)BS_1.Current).Row["acc_no"].ToString();
            string Acc_AName = ((DataRowView)BS_1.Current).Row[connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName"].ToString();
            Date_Tbl2.Rows.Add(new object[] { ATerm_Name, acc_no, Acc_AName });


        }
        private void Export_details_Click(object sender, EventArgs e)
        {
            if (G4.Rows.Count > 0)
            {
                Date();
                DataTable exp_DT = new DataTable();
                DataTable exp_DT2 = new DataTable();
                int cust_id = 0;
                Int16 for_cur_id = 0;
                cust_id = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Cust_id"]);
                for_cur_id = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_cur_id"]);
                exp_DT = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount", "T_id", "for_cur_id").Select("Cust_id = " + cust_id + "and T_id = " + T_id_current + " and for_cur_id = " + for_cur_id + "and acc_no = '" + Acc_no_current + "'").CopyToDataTable();
                exp_DT = exp_DT.DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount").Select().CopyToDataTable();
                exp_DT2 = Dt_Gr.DefaultView.ToTable(false, "DLoc_Amount", "CLoc_Amount", "Balance_Loc", "ACC_Id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "REAL_PRICE", "EXCH_PRICE", "DFor_Amount", "CFor_Amount", "Balance_for", "VO_NO", "nrec_date", "C_date", "Notes").Select().CopyToDataTable();

                DataGridView[] Export_GRD = { Date_Grd, Date_Grd2, G3, G4 };

                DataTable[] Export_DT = { Date_Tbl, Date_Tbl2, exp_DT, exp_DT2 };

                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (G3.Rows.Count > 0)
            {
                Export_details_PDF_rpt ObjRpt = new Export_details_PDF_rpt();
                Export_details_PDF_rpt_eng ObjRptEng = new Export_details_PDF_rpt_eng();

                string[] Str = { "exp_DT", "exp_DT2" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                DataTable exp_DT = new DataTable();
                DataTable exp_DT2 = new DataTable();
                int cust_id = 0;
                Int16 for_cur_id = 0;
                cust_id = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Cust_id"]);
                for_cur_id = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_cur_id"]);
                exp_DT = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable(false, "Cust_id", "Acust_Name", "Ecust_Name", "Acc_no",
                                            "Acc_Aname", "Acc_Ename", "cur_AName", "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount", "T_id",
                                            "for_cur_id").Select("Cust_id = " + cust_id + "and T_id = " + T_id_current + " and for_cur_id = " + for_cur_id + "and acc_no = '" + Acc_no_current + "'").CopyToDataTable();

                exp_DT = exp_DT.DefaultView.ToTable(false, "Cust_id", "Acust_Name", "Ecust_Name", "Acc_no", "Acc_Aname", "Acc_Ename", "cur_AName",
                                                           "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount").Select().CopyToDataTable();
                exp_DT2 = Dt_Gr.DefaultView.ToTable(false, "DLoc_Amount", "CLoc_Amount", "Balance_Loc", "ACC_Id", "AOPER_NAME", "EOPER_NAME", "REAL_PRICE",
                                           "EXCH_PRICE", "DFor_Amount", "CFor_Amount", "Balance_for", "VO_NO", "nrec_date", "C_date", "Notes").Select().CopyToDataTable();

                DataGridView[] Export_GRD = { G3, G4 };

                DataTable[] Export_DT = { exp_DT, exp_DT2 };


                string frm_name = "";
                string Efrm_name = "";
                if (form_no_new == 1)
                {
                    frm_name = "ميزان مراجعة";
                    Efrm_name = "Trial Balance";
                }
                else if (form_no_new == 2)
                {
                    frm_name = "كشف حساب";
                    Efrm_name = "Account statement";
                }
                else
                {
                    frm_name = "حركات ارصدة نهاية المدة على مستوى المؤسسة";
                    Efrm_name = "The balance end voucher of the period for the company";
                }
                //string frm_name = " حركات ارصدة نهاية المدة على مستوى المؤسسة";
                //string Efrm_name = "The balance end voucher of the period for the company";
                string date_from = (((DataRowView)BS_2.Current).Row["From_Date1"]).ToString();
                string to_date = (((DataRowView)BS_2.Current).Row["To_Date1"]).ToString();

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("Efrm_name", Efrm_name);
                Dt_Param.Rows.Add("user_name", connection.User_Name);
                Dt_Param.Rows.Add("date_from", date_from);
                Dt_Param.Rows.Add("to_date", to_date);

                exp_DT.TableName = "exp_DT";
                connection.SQLDS.Tables.Add(exp_DT);
                exp_DT2.TableName = "exp_DT2";
                connection.SQLDS.Tables.Add(exp_DT2);

                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, true, Dt_Param, Export_GRD, Export_DT, true, false, this.Text);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للطباعة" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (G3.Rows.Count > 0)
            {
                Export_details_LC_rpt ObjRpt = new Export_details_LC_rpt();
                Export_details_LC_rpt_Eng ObjRptEng = new Export_details_LC_rpt_Eng();

                string[] Str = { "exp_DT", "exp_DT2" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                DataTable exp_DT = new DataTable();
                DataTable exp_DT2 = new DataTable();
                int cust_id = 0;
                Int16 for_cur_id = 0;
                cust_id = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Cust_id"]);
                for_cur_id = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_cur_id"]);
                exp_DT = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable(false, "Cust_id", "Acust_Name", "Ecust_Name", "Acc_no",
                                            "Acc_Aname", "Acc_Ename", "cur_AName", "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount", "T_id",
                                            "for_cur_id").Select("Cust_id = " + cust_id + "and T_id = " + T_id_current + " and for_cur_id = " + for_cur_id + "and acc_no = '" + Acc_no_current + "'").CopyToDataTable();

                exp_DT = exp_DT.DefaultView.ToTable(false, "Cust_id", "Acust_Name", "Ecust_Name", "Acc_no", "Acc_Aname", "Acc_Ename", "cur_AName",
                                                           "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount").Select().CopyToDataTable();
                exp_DT2 = Dt_Gr.DefaultView.ToTable(false, "DLoc_Amount", "CLoc_Amount", "Balance_Loc", "ACC_Id", "AOPER_NAME", "EOPER_NAME", "REAL_PRICE",
                                           "EXCH_PRICE", "DFor_Amount", "CFor_Amount", "Balance_for", "VO_NO", "nrec_date", "C_date", "Notes").Select().CopyToDataTable();

                DataGridView[] Export_GRD = { G3, G4 };

                DataTable[] Export_DT = { exp_DT, exp_DT2 };


                string frm_name = "";
                string Efrm_name = "";
                if (form_no_new == 1)
                {
                    frm_name = "ميزان مراجعة";
                    Efrm_name = "Trial Balance";
                }
                else if (form_no_new == 2)
                {
                    frm_name = "كشف حساب";
                    Efrm_name = "Account statement";
                }
                else
                {
                    frm_name = "حركات ارصدة نهاية المدة على مستوى المؤسسة";
                    Efrm_name = "The balance end voucher of the period for the company";
                }
                //string frm_name = " حركات ارصدة نهاية المدة على مستوى المؤسسة";
                //string Efrm_name = "The balance end voucher of the period for the company";
                string date_from = (((DataRowView)BS_2.Current).Row["From_Date1"]).ToString();
                string to_date = (((DataRowView)BS_2.Current).Row["To_Date1"]).ToString();

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("Efrm_name", Efrm_name);
                Dt_Param.Rows.Add("user_name", connection.User_Name);
                Dt_Param.Rows.Add("date_from", date_from);
                Dt_Param.Rows.Add("to_date", to_date);

                exp_DT.TableName = "exp_DT";
                connection.SQLDS.Tables.Add(exp_DT);
                exp_DT2.TableName = "exp_DT2";
                connection.SQLDS.Tables.Add(exp_DT2);

                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, true, Dt_Param, Export_GRD, Export_DT, true, false, this.Text);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للطباعة" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void btn_details_G5_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Account_Balancing_new_tbl5"].Rows.Count > 0)
            {
                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl5"];
                Int16 Acc_ID = 0;
                Acc_ID = Convert.ToInt16(((DataRowView)BS_1.Current).Row["Acc_ID"]);
                DataGridView[] Export_GRD = { G5 };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_Balancing_new_tbl5"].DefaultView.ToTable(false, "PLoc_Amount" , 
                                  "PFor_Amount" , "ACC_Id" , connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename" , 
                                  connection.Lang_id == 1 ? "cur_aname" : "cur_ename" , connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name" , 
                                  connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME" , "Real_Price" , "EXCH_PRICE" ,
                                  "VO_NO" , "Nrec_date" , "C_date" , "Notes").Select("Acc_ID = " + Acc_ID).CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
    }
}
