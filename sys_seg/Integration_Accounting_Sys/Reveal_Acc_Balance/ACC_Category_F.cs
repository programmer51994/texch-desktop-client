﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class ACC_Category_F : Form
    {
        int T_ID = 0;
        int Acc_Id = 0;
        string Filter = "";
        string Str_Id = "";
        string Con_Str = "";
        bool Change_Acc_Category = false;
        DataTable DT_Acc = new DataTable();
        DataTable DT_Acc_TBL = new DataTable();
        DataTable DT_T = new DataTable();
        DataTable DT_ACC_Category = new DataTable();
        BindingSource Bs_ACC_Category = new BindingSource();
        BindingSource Binding_Add_Acc_Category = new BindingSource();

        public ACC_Category_F()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Acc_Category.AutoGenerateColumns = false;
            Create_Tbl();
        }

        private void Create_Tbl()
        {
            string[] Column = { "Cat_acc_AName", "Cat_acc_EName" };
            string[] DType = { "System.String", "System.String" };
            DT_ACC_Category = CustomControls.Custom_DataTable("DT_ACC_Category", Column, DType);
            Grd_Acc_Category.DataSource = DT_ACC_Category;

            string[] Column1 = { "Acc_id", "Dot_Acc_No", "Acc_ANAME", "Acc_Ename", "Acc_Level" };
            string[] DType1 = { "System.Int16", "System.String", "System.String", "System.String", "System.Int32" };
            DT_Acc = CustomControls.Custom_DataTable("DT_Acc", Column1, DType1);
            Grd_Acc.DataSource = DT_Acc;
        }

        private void category_get()
        {
            string Sql_acc_category = " SELECT * from Category_Acc order by Cat_acc_id ";
            connection.SqlExec(Sql_acc_category, "Sql_acc_category_Tbl");
            Binding_Add_Acc_Category.DataSource = connection.SQLDS.Tables["Sql_acc_category_Tbl"];
            Grd_Acc_Category.DataSource = Binding_Add_Acc_Category;
            Change_Acc_Category = true;
            Grd_Acc_Category_SelectionChanged(null, null);
        }

        private void ACC_Category_F_Load(object sender, EventArgs e)
        {
            string SqlTxt2 = " select * from Account_Tree ";
            connection.SqlExec(SqlTxt2, "Account_Tree_tab");

            category_get();
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Change_Acc_Category = false;
            string[] Str1 = { "Add_Acc_Category_tbl" };
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }

            DataRow DRow = DT_ACC_Category.NewRow();
            DRow["Cat_acc_AName"] = Txt_Acc_Category.Text.Trim();
            DRow["Cat_acc_EName"] = Txt_Acc_Category_E.Text.Trim();
            DT_ACC_Category.Rows.Add(DRow);
            Bs_ACC_Category.DataSource = DT_ACC_Category;
            Grd_Acc_Category.BeginEdit(true);

            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Add_Acc_Category";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@Acc_Category_Name", Txt_Acc_Category.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@Acc_Category_EName", Txt_Acc_Category_E.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Add_Acc_Category_tbl");
                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                Binding_Add_Acc_Category.DataSource = connection.SQLDS.Tables["Add_Acc_Category_tbl"];
                Grd_Acc_Category.DataSource = Binding_Add_Acc_Category;
                Change_Acc_Category = true;
                Grd_Acc_Category_SelectionChanged(null, null);
                Txt_Acc_Category.Text = "";
                Txt_Acc_Category_E.Text = "";
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
        }

        private void Grd_Acc_Category_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_Acc_Category)
            {
                TxT_Acc_Cat.DataBindings.Clear();
                TxT_Acc_Cat.Text = Convert.ToString(((DataRowView)Binding_Add_Acc_Category.Current).Row["Cat_acc_AName"]);
            }
        }

        private void TxtAcc_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Acc_Id = 0;
                Con_Str = "";
                Str_Id = "";
                if (DT_Acc.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                    Con_Str = " And Acc_Id not in(" + Str_Id + ")";
                }
                int.TryParse(TxtAcc_Name.Text, out Acc_Id);

                Filter = " (Acc_Aname like '" + TxtAcc_Name.Text + "%' or  Acc_Ename like '" + TxtAcc_Name.Text + "%'or  Dot_Acc_No like '" + TxtAcc_Name.Text + "%'"
                            + " Or Acc_id = " + Acc_Id + ")" + Con_Str;

                DT_Acc_TBL = connection.SQLDS.Tables["Account_Tree_tab"].DefaultView.ToTable(true, "Acc_Id", "Dot_Acc_No", "Acc_Aname", "Acc_Ename", "Acc_Level").Select(Filter).CopyToDataTable();
                string Dot_Acc_No = "";
                Dot_Acc_No = Convert.ToString(DT_Acc_TBL.Rows[0]["Dot_Acc_No"]);
                DataTable DT = DT_Acc_TBL.DefaultView.ToTable(true, "Acc_Id", "Dot_Acc_No", "Acc_Aname", "Acc_Ename", "Acc_Level").Select("Dot_Acc_No like '" + Dot_Acc_No + "%'").CopyToDataTable();
                Grd_Acc_Id.DataSource = DT;
            }
            catch
            {
                Grd_Acc_Id.DataSource = new DataTable();
            }

            if (Grd_Acc_Id.Rows.Count <= 0)
            {
                button8.Enabled = false;
                button7.Enabled = false;
            }
            else
            {
                button8.Enabled = true;
                button7.Enabled = true;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Acc.NewRow();

            row["Acc_id"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_Id"];
            row["Acc_aname"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_aname"];
            row["Acc_ename"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_ename"];
            row["Dot_Acc_No"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Dot_Acc_No"];
            row["Acc_Level"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_Level"];
            DT_Acc.Rows.Add(row);
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);

            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Acc_Id.Rows.Count == 0)
            {
                button8.Enabled = false;
                button7.Enabled = false;
            }

            Acc_details();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows.Clear();
            TxtAcc_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = false;
            button5.Enabled = false;
        }

        private void Acc_details()
        {
            BindingSource BS_Acc = new BindingSource();
            BS_Acc.DataSource = DT_Acc;
            string ACC_details = "";
            ACC_details = Convert.ToString(((DataRowView)BS_Acc.Current).Row["Dot_Acc_No"]);

            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Search_Acc_Details";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@ACC_details", ACC_details);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Search_Acc_Details_tbl");
                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Acc_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Acc.NewRow();
                row["Acc_id"] = DT_Acc_TBL.Rows[i]["Acc_id"];
                row["Acc_aname"] = DT_Acc_TBL.Rows[i]["Acc_aname"];
                row["Acc_ename"] = DT_Acc_TBL.Rows[i]["Acc_ename"];
                row["Dot_Acc_No"] = DT_Acc_TBL.Rows[i]["Dot_Acc_No"];
                DT_Acc.Rows.Add(row);
            }
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button8.Enabled = false;
            button7.Enabled = false;
            button6.Enabled = true;
            button5.Enabled = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows[Grd_Acc.CurrentRow.Index].Delete();
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button7.Enabled = true;
            button8.Enabled = true;
            if (Grd_Acc.Rows.Count == 0)
            {
                button6.Enabled = false;
                button5.Enabled = false;
            }
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            DataTable ACC_Tbl = new DataTable();
            ACC_Tbl = DT_Acc.DefaultView.ToTable(false, "Acc_Id", "Acc_aname", "Acc_ename").Select().CopyToDataTable();

            try
            {
                int Cat_acc_id = Convert.ToInt32(((DataRowView)Binding_Add_Acc_Category.Current).Row["Cat_acc_id"]);
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Add_Acc_Category_Details";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@Cat_acc_id", Cat_acc_id);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_Category", ACC_Tbl);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Add_Acc_Category_tbl");
                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                Grd_Acc.DataSource = new BindingSource();
                Create_Tbl();
                category_get();
            }
            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
