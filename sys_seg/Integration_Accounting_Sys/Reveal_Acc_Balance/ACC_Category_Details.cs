﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class ACC_Category_Details : Form
    {
        string Filter = "";
        int T_ID = 0;
        int Acc_Id = 0;
        string Str_Id = "";
        string Con_Str = "";
        string @format = "dd/MM/yyyy";
        Int32 from_nrec_date = 0;
        Int32 to_nrec_date = 0;
        DataTable DT_Term = new DataTable();
        DataTable DT_Term_TBL = new DataTable();
        BindingSource Binding_Acc_Category = new BindingSource();

        public ACC_Category_Details()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Term.AutoGenerateColumns = false;
            Grd_Term_Id.AutoGenerateColumns = false;
            Create_Tbl();
        }

        private void Create_Tbl()
        {
            string[] Column = { "T_ID", "ACUST_NAME", "ECUST_NAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Term = CustomControls.Custom_DataTable("DT_Term", Column, DType);
            Grd_Term.DataSource = DT_Term;
        }

        private void ACC_Category_Details_Load(object sender, EventArgs e)
        {
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            connection.SqlExec("Exec Search_Account_Balance_new ", "Account_Balance_new_Tbl");
        }

        private void Chk_Term_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Term.Checked)
            {
                Txt_Term_Name.Enabled = true;
                Txt_Term_Name_TextChanged(null, null);
            }
            else
            {
                DT_Term.Clear();
                DT_Term_TBL.Clear();
                Txt_Term_Name.ResetText();
                button12.Enabled = false;
                button11.Enabled = false;
                button10.Enabled = false;
                button9.Enabled = false;
                Txt_Term_Name.Enabled = false;
            }
        }

        private void Txt_Term_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Acc_Id = 0;
                T_ID = 0;
                Str_Id = "";
                Con_Str = "";

                if (DT_Term.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Term, "T_ID", out Str_Id);
                    Con_Str += " And  T_ID not in(" + Str_Id + ")";
                }
                int.TryParse(Txt_Term_Name.Text, out T_ID);

                Filter = " (ACUST_NAME like '" + Txt_Term_Name.Text + "%' or  ECUST_NAME like '" + Txt_Term_Name.Text + "%'"
                            + " Or T_ID = " + T_ID + ")" + Con_Str;
                # endregion
                DT_Term_TBL = connection.SQLDS.Tables["Account_Balance_new_Tbl1"].Select(Filter).CopyToDataTable();
                DT_Term_TBL = DT_Term_TBL.DefaultView.ToTable(true, "T_ID", "ACUST_NAME", "ECUST_NAME").Select().CopyToDataTable();
                Grd_Term_Id.DataSource = DT_Term_TBL;
            }
            catch
            {
                Grd_Term_Id.DataSource = new DataTable();
            }
            if (Grd_Term_Id.Rows.Count <= 0)
            {
                button12.Enabled = false;
                button11.Enabled = false;
            }
            else
            {
                button12.Enabled = true;
                button11.Enabled = true;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Term.NewRow();

            row["T_ID"] = DT_Term_TBL.Rows[Grd_Term_Id.CurrentRow.Index]["T_ID"];
            row["ACUST_NAME"] = DT_Term_TBL.Rows[Grd_Term_Id.CurrentRow.Index]["ACUST_NAME"];
            row["ECUST_NAME"] = DT_Term_TBL.Rows[Grd_Term_Id.CurrentRow.Index]["ECUST_NAME"];
            DT_Term.Rows.Add(row);
            Txt_Term_Name.Text = "";
            Txt_Term_Name_TextChanged(null, null);

            button10.Enabled = true;
            button9.Enabled = true;
            if (DT_Term_TBL.Rows.Count == 0)
            {
                button12.Enabled = false;
                button11.Enabled = false;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Term_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Term.NewRow();
                row["T_ID"] = DT_Term_TBL.Rows[i]["T_ID"];
                row["ACUST_NAME"] = DT_Term_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Term_TBL.Rows[i]["ECUST_NAME"];
                DT_Term.Rows.Add(row);
            }
            Txt_Term_Name.Text = "";
            Txt_Term_Name_TextChanged(null, null);
            button12.Enabled = false;
            button11.Enabled = false;
            button10.Enabled = true;
            button9.Enabled = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DT_Term.Rows[Grd_Term.CurrentRow.Index].Delete();
            Txt_Term_Name.Text = "";
            Txt_Term_Name_TextChanged(null, null);
            button12.Enabled = true;
            button11.Enabled = true;
            if (Grd_Term.Rows.Count == 0)
            {
                button10.Enabled = false;
                button9.Enabled = false;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DT_Term.Rows.Clear();
            Txt_Term_Name_TextChanged(null, null);
            button12.Enabled = true;
            button11.Enabled = true;
            button10.Enabled = false;
            button9.Enabled = false;
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            string[] Str1 = { "Account_category_new_tbl" };
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }

            if (TxtFromDate.Checked == true)
            {
                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                from_nrec_date = 0;
            }

            if (TxtToDate.Checked == true)
            {
                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                to_nrec_date = 0;
            }

            if (TxtFromDate.Checked != true)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                return;
            }

            if (TxtToDate.Checked != true)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                return;
            }

            DataTable DT_T = new DataTable();
            try
            {
                DT_T = DT_Term.DefaultView.ToTable(false, "T_ID").Select().CopyToDataTable();
            }
            catch
            {
                DT_T = connection.SQLDS.Tables["Account_Balance_new_Tbl1"].DefaultView.ToTable(false, "T_ID").Select().CopyToDataTable();
            }

            try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Account_category";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;

                connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                connection.SQLCMD.Parameters.AddWithValue("@T_Id_Tbl", DT_T);
                connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
                connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_category_new_tbl");
                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();

                Grd_Acc_Category_Details.DataSource = connection.SQLDS.Tables["Account_category_new_tbl"];
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            if (Grd_Acc_Category_Details.Rows.Count > 0)
            {
                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_category_new_tbl"];
                DataGridView[] Export_GRD = { Grd_Acc_Category_Details };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_category_new_tbl"].DefaultView.ToTable().Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
    }
}
