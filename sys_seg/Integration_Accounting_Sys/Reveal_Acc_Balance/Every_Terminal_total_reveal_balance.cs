﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;
using Integration_Accounting_Sys.Branch_Report;


namespace Integration_Accounting_Sys
{
    public partial class Every_Terminal_total_reveal_balance : Form
    {
        #region Definshion
        BindingSource BS_1 = new BindingSource();
        BindingSource BS_2 = new BindingSource();
        BindingSource BS_3 = new BindingSource();
        BindingSource BS_4 = new BindingSource();
        BindingSource BS_5 = new BindingSource();
        DataTable Grid2_tbl = new DataTable();
        string Acc_no_current = "";
        Int16 T_id_current = 0;
        Int32 Cust_id_current = 0;
        Int16 Cur_id_current = 0;
        Int32 Acc_id_current = 0;
        Int32 Vo_no_current = 0;
        Int32 Oper_id_current = 0;
        string nrec_date_current;
        bool Xchange1 = false;
        bool Xchange2 = false;
        bool Xchange3 = false;
        bool Xchange4 = false;
        string Dot_Acc_no_current = "";
        DataTable Dt_G4 = new DataTable();
        Int16 level_rep = 0;
        Int16 Remove_Zero = 0;
        decimal DEloc_Amount1 = 0;
        decimal CEloc_Amount1 = 0;
        #endregion
        //---------------------------
        public Every_Terminal_total_reveal_balance(string FromDate, string ToDate, Int32 from_nrec_date, Int32 to_nrec_date, Int16 Balance, Int16 level, Int16 chk_zero_end, decimal DEloc_Amount = 0, decimal CEloc_Amount = 0)
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            
            G1.AutoGenerateColumns = false;
            G2.AutoGenerateColumns = false;
            G3.AutoGenerateColumns = false;
            G4.AutoGenerateColumns = false;
            G5.AutoGenerateColumns = false;

            if (connection.Lang_id == 2)
            {
                dataGridViewTextBoxColumn2.DataPropertyName = "ECust_name";
                dataGridViewTextBoxColumn22.DataPropertyName = "Acc_EName";
                Column11.DataPropertyName = "cur_EName";
                Column10.DataPropertyName = "Ecust_Name";
                dataGridViewTextBoxColumn9.DataPropertyName = "Acc_EName";
                dataGridViewTextBoxColumn10.DataPropertyName = "cur_EName";
                //dataGridViewTextBoxColumn21.DataPropertyName = "Ecust_Name";
                dataGridViewTextBoxColumn12.DataPropertyName = "EOPER_NAME";
                dataGridViewTextBoxColumn11.DataPropertyName = "Ecust_Name";
                Column22.DataPropertyName = "EOPER_NAME";
            }
            level_rep = level;
            Remove_Zero = chk_zero_end;
            DEloc_Amount1 = DEloc_Amount;
            CEloc_Amount1 = CEloc_Amount;
        }
        //---------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {
            Xchange1 = false ;
            if (Search_Trail_Balance_new_Shadow.Balance == 0)
            { CHK_Zero_Balancing.Checked = false; }
            else { CHK_Zero_Balancing.Checked = true; }

            //-----------------------------------------------
            if (Remove_Zero == 1)
            { Chk_Remove_Zero.Checked = true; }

            BS_1.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
            G1.DataSource = BS_1;
            if (G1.RowCount == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود " : "No Data To export ", MyGeneral_Lib.LblCap);
                Btn_Ext_Click(null, null);
                return;
            }
            Xchange1 = true;
            G1_SelectionChanged(null, null);
        }

        //---------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Trial_Balance_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Xchange1 = false;
            Xchange2 = false;
            Xchange3 = false;
            Xchange4 = false;
        }

        private void G1_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange1)
            {
                try
                {
                    Xchange2 = false;
                    T_id_current = Convert.ToInt16(((DataRowView)BS_1.Current).Row["T_id"]);
                    DataTable n = new DataTable();
                    BS_2.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable().Select("T_ID = " + T_id_current).CopyToDataTable();

                    G2.DataSource = BS_2;
                    Xchange2 = true;
                    G2_SelectionChanged_1(null, null);

                }
                catch
                {
                    G2.DataSource = new DataTable();
                }
                if (Chk_Remove_Zero.Checked == false)
                {
                    DataTable DT2 = new DataTable();
                    DT2 = connection.SQLDS.Tables["Account_Balancing_new_tbl1"];
                    TxtED_LocalAmount.Text = DT2.Compute("Sum(DEloc_Amount)", "").ToString();
                    TxtEC_LocalAmount.Text = DT2.Compute("Sum(CEloc_Amount)", "").ToString();
                }
                else
                {
                    TxtED_LocalAmount.Text =  DEloc_Amount1.ToString();
                    TxtEC_LocalAmount.Text =  CEloc_Amount1.ToString();
                }
            }

        }

  
        private void G3_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange3)
            {

                try
                {
                    Xchange4 = false;
                   // DataTable Dt = new DataTable();
                    decimal MBalance = 0;
                    decimal MBalance_loc = 0;

                    Cust_id_current = Convert.ToInt16(((DataRowView)BS_3.Current).Row["Cust_id"]);
                    Cur_id_current = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_Cur_id"]);
                    Acc_id_current = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Acc_id"]);
                    //BS_4.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl3"].DefaultView.ToTable()
                    //.Select("T_id = " + T_id_current + "and Cust_id = " + Cust_id_current + "and for_Cur_id = " + Cur_id_current + "and Acc_id = " + Acc_id_current).CopyToDataTable();

                     try
                {
                    Dt_G4 = connection.SQLDS.Tables["Account_Balancing_new_tbl3"].Select("Cust_Id = " + Cust_id_current
                                                                                                   + " And For_Cur_Id = " + Cur_id_current
                                                                                                   + " and Acc_id = " + Acc_id_current
                                                                                                   + " And T_id = " + T_id_current).CopyToDataTable();


                    DataView dv = Dt_G4.DefaultView;
                    dv.Sort = "c_date";
                    Dt_G4 = dv.ToTable();
                }
                catch
                {
                    Dt_G4 = new DataTable();
                }
                     foreach (DataRow Drow in Dt_G4.Rows)
                {
                    if (Convert.ToInt32(Drow["RowNum"]) == 0)
                    {
                        MBalance = Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]) * -1;
                        MBalance_loc = Convert.ToDecimal(Drow["Dloc_Amount"]) + Convert.ToDecimal(Drow["Cloc_Amount"]) * -1;
                    }

                    if (Convert.ToInt32(Drow["RowNum"]) > 0)
                    {
                        MBalance += Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]) * -1;
                        MBalance_loc += Convert.ToDecimal(Drow["Dloc_Amount"]) + Convert.ToDecimal(Drow["Cloc_Amount"]) * -1;
                    }
                    Drow["Balance_for"] = MBalance;
                    Drow["Balance_loc"] = MBalance_loc;
                    //Drow["CFor_Amount"] = Math.Abs(Convert.ToDecimal(Drow["CFor_Amount"]));
                }
                BS_4.DataSource = Dt_G4;
                G4.DataSource = BS_4;
                Xchange4 = true;
                G4_SelectionChanged(null, null);
            }


                   
                

                catch
                {
                    G4.DataSource = new DataTable();
                }
            }
        }
        //--------------------------------------------------------------
        private void G2_SelectionChanged_1(object sender, EventArgs e)
        {

            if (Xchange2)
            {

                try
                {
                   // Xchange2 = false;
                    Xchange3 = false ;
                    //T_id_current = Convert.ToInt16(((DataRowView)BS_1.Current).Row["T_id"]);
                    Acc_no_current = ((DataRowView)BS_2.Current).Row["Acc_no"].ToString();
                    if (level_rep == 0)
                    {
                    BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and acc_no = '" + Acc_no_current + "'").CopyToDataTable();
                    }
                    else
                    {

                        Dot_Acc_no_current = ((DataRowView)BS_2.Current).Row["Dot_acc_no"].ToString();
                        BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and Dot_acc_no like'" + Dot_Acc_no_current + "%'").CopyToDataTable();

                    }

                    G3.DataSource = BS_3;
                    Xchange3 = true;
                    G3_SelectionChanged(null, null);
                }
                catch
                {
                    G3.DataSource = new DataTable();
                }
            }

        }
        //--------------------------------------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                DataTable Dt = new DataTable();
                DataTable Dt_details = new DataTable();
                DataTable Rpt_dt_details = new DataTable();
                DataTable Export_DT_main = new DataTable();
                DataTable Export_DT_details = new DataTable();
                string Branch_name = ((DataRowView)BS_1.Current).Row["ACust_name"].ToString();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                Dt_details = connection.SQLDS.Tables["Account_Balancing_new_tbl1"];
                Export_DT_main = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "T_Id", connection.Lang_id == 1 ? "ACust_name" : "ECust_name", "From_Date1",
                    "To_Date1").Select(connection.Lang_id == 1 ? "ACust_name = '" : "ECust_name = '" + Branch_name + "'").CopyToDataTable();
                Export_DT_details = Dt_details.DefaultView.ToTable(false, "Acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "DEloc_Amount",
                "CEloc_Amount").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { G1, G2 };
                DataTable[] Export_DT = { Export_DT_main, Export_DT_details };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //--------------------------------------------------------------
        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                if (level_rep == 0)
                {
                    DataTable Dt = new DataTable();
                    Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                    DataGridView G7 = new DataGridView();
                    G7.Columns.Add("Column1", connection.Lang_id == 1 ? "الرصيد الاجمالي" : "Total Amount");
                    G7.Columns.Add("Column2", connection.Lang_id == 1 ? "تحليل الرصيد" : "Branch amount");
                    G7.Columns.Add("Column3", connection.Lang_id == 1 ? "رمز الحساب" : "Amount details");
                    G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الحساب" : "Accountant NO");
                    G7.Columns.Add("Column5", connection.Lang_id == 1 ? "رمز الثانوي" : "Accountant name");
                    G7.Columns.Add("Column6", connection.Lang_id == 1 ? "إسم الثانوي" : "Terminal no");
                    G7.Columns.Add("Column7", connection.Lang_id == 1 ? "العملــــة" : "Terminal name");
                    G7.Columns.Add("Column8", connection.Lang_id == 1 ? "الرصيد _(ع.ص)_" : "Customer NO");
                    //DataTable Dt = new DataTable();
                    DataTable Dt_details = new DataTable();
                    DataTable Rpt_dt_details = new DataTable();
                    DataTable Export_DT_main = new DataTable();
                    DataTable Export_DT_details = new DataTable();
                    T_id_current = Convert.ToInt16(((DataRowView)BS_1.Current).Row["T_id"]);
                    Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                    Dt_details = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                    Export_DT_main = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "T_Id", connection.Lang_id == 1 ? "ACust_name" : "ECust_name", "From_Date1",
                        "To_Date1").Select("T_id = '" + T_id_current + "'").CopyToDataTable();
                    Export_DT_details = Dt_details.DefaultView.ToTable(false, "Eloc_Amount", "cust_eloc", "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "Cust_id",connection.Lang_id == 1 ? "ACUST_NAME": "ECUST_NAME",
                        connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Efor_amount").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { G1, G7 };
                    DataTable[] Export_DT = { Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    DataTable Dt = new DataTable();
                    Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl5"];
                    DataGridView G7 = new DataGridView();
                    G7.Columns.Add("Column1", connection.Lang_id == 1 ? "الرصيد الاجمالي" : "Total Amount");
                    G7.Columns.Add("Column2", connection.Lang_id == 1 ? "الرصيد الفرعي " : "Branch amount");
                    G7.Columns.Add("Column3", connection.Lang_id == 1 ? "رقم الحساب" : "Amount details");
                    G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الحساب" : "Accountant NO");
                    //G7.Columns.Add("Column4", connection.Lang_id == 1 ? " رقم الحساب الفرعي" : "Accountant name");
                    //G7.Columns.Add("Column5", connection.Lang_id == 1 ? "إسم الحساب الفرعي" : "Accountant NO");
                    G7.Columns.Add("Column8", connection.Lang_id == 1 ? "رمز الثانوي" : "Customer NO");
                    G7.Columns.Add("Column9", connection.Lang_id == 1 ? "إسم الثانوي" : "Customer name");
                    G7.Columns.Add("Column10", connection.Lang_id == 1 ? "العملــــة" : "Currency");
                    G7.Columns.Add("Column11", connection.Lang_id == 1 ? "الرصيد _(ع.ص)_" : "Last Balance (F.C)");


                    DataTable Dt_details = new DataTable();
                    DataTable Rpt_dt_details = new DataTable();
                    DataTable Export_DT_main = new DataTable();
                    DataTable Export_DT_details = new DataTable();
                    T_id_current = Convert.ToInt16(((DataRowView)BS_1.Current).Row["T_id"]);
                    Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                    Dt_details = connection.SQLDS.Tables["Account_Balancing_new_tbl5"];
                    Export_DT_main = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "T_ID", connection.Lang_id == 1 ? "ACust_name" : "ECust_name", "From_Date1",
                        "To_Date1").Select("T_id = '" + T_id_current + "'").CopyToDataTable();
                    Export_DT_details = Dt_details.DefaultView.ToTable(false, "Eloc_Amount", "cust_eloc", "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "cust_id",
                        connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Efor_amount").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { G1, G7 };
                    DataTable[] Export_DT = { Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

                }
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
            
        }

        private void G5_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void G4_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange4)
            {

                Vo_no_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["Vo_No"]);
                nrec_date_current = ((DataRowView)BS_4.Current).Row["nrec_date"].ToString();
                Oper_id_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["oper_id"]);
                decimal Loc_Amount = Convert.ToDecimal(((DataRowView)BS_4.Current).Row["Loc_Amount"]);
                //Cust_id_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["CUST_ID"]);
                try
                {
                    BS_5.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl6"].DefaultView.ToTable()
             .Select("T_id = " + T_id_current + " and Vo_No = " + Vo_no_current + " and oper_id = " + Oper_id_current + "and Nrec_date like'" + nrec_date_current + "%'").CopyToDataTable();
                    G5.DataSource = BS_5;

                    foreach (DataGridViewRow row in G5.Rows)
                    {
                        if (Convert.ToInt32(row.Cells[9].Value) == Convert.ToInt32(Vo_no_current)
                              && (Convert.ToInt64(row.Cells[0].Value) == Convert.ToInt64(Loc_Amount))
                               && Convert.ToInt16(row.Cells[2].Value) == Convert.ToInt16(Acc_id_current))
                        {
                            row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }
                }
                catch
                {
                    G5.DataSource = new DataTable();
                }
              
            }
        }

        private void Btn_Ext_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void print_det_btn_Click(object sender, EventArgs e)
        {
            string term = (((DataRowView)BS_1.Current).Row["ACust_name"]).ToString();
            string date_from = (((DataRowView)BS_1.Current).Row["From_Date1"]).ToString();
            string to_date = (((DataRowView)BS_1.Current).Row["To_Date1"]).ToString();
            string frm_name = "ارصدة نهاية المدة لكل فرع";
            string user_name = connection.User_Name;
            Acc_reveal_every_term ObjRpt = new Acc_reveal_every_term();
            Acc_reveal_every_term ObjRptEng = new Acc_reveal_every_term();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("frm_name", frm_name);
            Dt_Param.Rows.Add("user_name", user_name);
            Dt_Param.Rows.Add("date_from", date_from);
            Dt_Param.Rows.Add("to_date", to_date);
            Dt_Param.Rows.Add("term", term);
            connection.SQLDS.Tables["Account_Balancing_new_tbl4"].TableName = "Acc_e_term_rev";
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables["Acc_e_term_rev"].TableName = "Account_Balancing_new_tbl4";
        }

        private void print_total_btn_Click(object sender, EventArgs e)
        {
            string term = (((DataRowView)BS_1.Current).Row["ACust_name"]).ToString();
            string date_from = (((DataRowView)BS_1.Current).Row["From_Date1"]).ToString();
            string to_date = (((DataRowView)BS_1.Current).Row["To_Date1"]).ToString();
            string frm_name = "ارصدة نهاية المدة لكل فرع";
            string user_name = connection.User_Name;
            Acc_reveal_every_term_total ObjRpt = new Acc_reveal_every_term_total();
            Acc_reveal_every_term_total ObjRptEng = new Acc_reveal_every_term_total();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("frm_name", frm_name);
            Dt_Param.Rows.Add("user_name", user_name);
            Dt_Param.Rows.Add("date_from", date_from);
            Dt_Param.Rows.Add("to_date", to_date);
            Dt_Param.Rows.Add("term", term);
            connection.SQLDS.Tables["Account_Balancing_new_tbl1"].TableName = "Acc_e_term_total_rev";
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables["Acc_e_term_total_rev"].TableName = "Account_Balancing_new_tbl1";
        }

        private void Export_details_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable Dt_Grd1 = new DataTable();
                DataTable Dt_Grd2 = new DataTable();
                DataTable exp_DT = new DataTable();
                DataTable exp_DT2 = new DataTable();
                Int16 T_ID = 0;
                string acc_no = "";
                Int16 Cust_id = 0;
                Int16 for_cur_id = 0;
                decimal Eloc_AMount = 0;
                Eloc_AMount = Convert.ToDecimal(((DataRowView)BS_3.Current).Row["Eloc_AMount"]);
                Cust_id = Convert.ToInt16(((DataRowView)BS_3.Current).Row["Cust_id"]);
                for_cur_id = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_cur_id"]);
                acc_no = ((DataRowView)BS_2.Current).Row["acc_no"].ToString();
                T_ID = Convert.ToInt16(((DataRowView)BS_1.Current).Row["T_ID"]);
                Dt_Grd1 = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "T_ID", connection.Lang_id == 1 ? "ACust_name" : "ECust_name", "From_Date1", "To_Date1").Select("T_ID = " + T_ID).CopyToDataTable();
                Dt_Grd2 = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable(false, "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "T_ID").Select("acc_no like'" + acc_no + "'" + " and T_ID = " + T_ID).CopyToDataTable();
                Dt_Grd2 = Dt_Grd2.DefaultView.ToTable(false, "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName").Select().CopyToDataTable();
                exp_DT = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name",
                        connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount", "T_id", "for_cur_id").Select("Cust_id = " + Cust_id + "and T_id = " + T_id_current + " and for_cur_id = " + for_cur_id + " and Eloc_AMount =" + Eloc_AMount).CopyToDataTable();
                exp_DT = exp_DT.DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount").Select().CopyToDataTable();
                exp_DT2 = Dt_G4.DefaultView.ToTable(false, "DLoc_Amount", "CLoc_Amount", "Balance_Loc", "ACC_Id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "REAL_PRICE", "EXCH_PRICE", "DFor_Amount", "CFor_Amount", "Balance_for", "VO_NO", "nrec_date", "C_date", "Notes").Select().CopyToDataTable();
                DataGridView Grddata = new DataGridView();
                Grddata.Columns.Add("acc_no", connection.Lang_id == 1 ? "رمز الحساب" : " account number");
                Grddata.Columns.Add("Acc_AName", connection.Lang_id == 1 ? "اسم الحساب" : " account name");
                DataGridView[] Export_GRD = { G1, Grddata, G3, G4 };

                DataTable[] Export_DT = { Dt_Grd1, Dt_Grd2, exp_DT, exp_DT2 };

                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            catch
            { }
        }
    }
}