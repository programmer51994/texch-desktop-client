﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Terminal_Details_trail_balance : Form
    {
        #region Definshion

        DataTable DT_1 = new DataTable();
        DataTable DT_G2 = new DataTable();
        BindingSource BS_1 = new BindingSource();
        BindingSource BS_2 = new BindingSource();
        BindingSource BS_3 = new BindingSource();
        BindingSource BS_4 = new BindingSource();
        BindingSource BS_5 = new BindingSource();
        DataTable Grid2_tbl = new DataTable();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataGridView Date_Grd2 = new DataGridView();
        DataTable Date_Tbl2 = new DataTable();
        DataTable Dt = new DataTable();
        string Acc_no_current = "";
        Int16 T_id_current = 0;
        Int32 Cust_id_current = 0;
        Int16 Cur_id_current = 0;
        Int32 Acc_id_current = 0;
        Int32 Vo_no_current = 0;
        Int32 Oper_id_current = 0;
        string nrec_date_current;
        bool Xchange1 = false;
        bool Xchange2 = false;
        bool Xchange3 = false;
        bool Xchange4 = false;
        string Dot_Acc_no_current = "";
        Int16 level_rep = 0;
        Int16 Remove_Zero = 0;
        decimal DOloc_Amount1 = 0;
        decimal COloc_Amount1 = 0;
        decimal DPloc_Amount1 = 0;
        decimal Cploc_Amount1 = 0;
        decimal DEloc_Amount1 = 0;
        decimal CEloc_Amount1 = 0;
        Int16 form_no_new;
        #endregion

        public Terminal_Details_trail_balance(Int16 frm_no_new, string FromDate, string ToDate, Int32 from_nrec_date, Int32 to_nrec_date, Int16 Balance, Int16 level, Int16 chk_zero_end,
        decimal DOloc_Amount = 0, decimal COloc_Amount = 0, decimal DPloc_Amount = 0, decimal Cploc_Amount = 0, decimal DEloc_Amount = 0, decimal CEloc_Amount = 0)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            form_no_new = frm_no_new;

            level_rep = level;
            Remove_Zero = chk_zero_end;
            DOloc_Amount1 = DOloc_Amount;
            COloc_Amount1 = COloc_Amount;
            DPloc_Amount1 = DPloc_Amount;
            Cploc_Amount1 = Cploc_Amount;
            DEloc_Amount1 = DEloc_Amount;
            CEloc_Amount1 = CEloc_Amount;

            //string @format = "dd/MM/yyyy";
            //string @format_null = " ";
            //if (from_nrec_date != 0)
            //{
            //  //  TxtFromDate =  Convert.ToDateTime (FromDate) ;
            //    TxtFromDate.CustomFormat = @format;
            //}
            //else { TxtFromDate.CustomFormat = @format_null; }

            //if (to_nrec_date != 0)
            //{
            //    TxtToDate.CustomFormat = @format;
            //}
            //else { TxtToDate.CustomFormat = @format_null; }
            G1.AutoGenerateColumns = false;
            G2.AutoGenerateColumns = false;
            G3.AutoGenerateColumns = false;
            G4.AutoGenerateColumns = false;
            G5.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {

                Column7.DataPropertyName = "Acc_EName";
                //  dataGridViewTextBoxColumn2.DataPropertyName = "ETerm_Name";
                Column10.DataPropertyName = "Ecust_Name";
                Column22.DataPropertyName = "EOPER_NAME";
                dataGridViewTextBoxColumn10.DataPropertyName = "cur_ename";
                dataGridViewTextBoxColumn11.DataPropertyName = "Ecust_Name";
                dataGridViewTextBoxColumn12.DataPropertyName = "EOPER_NAME";
                dataGridViewTextBoxColumn9.DataPropertyName = "EOPER_NAME";
                Column11.DataPropertyName = "cur_ename";
            }
        }
        //--------------------------------------------------------------
        private void Terminal_Details_trail_balance_Load(object sender, EventArgs e)
        {
            Xchange1 = false;
            if (Search_Trail_Balance_new_Shadow.Balance == 0)
            { CHK_Zero_Balancing.Checked = false; }
            else { CHK_Zero_Balancing.Checked = true; }

            //-----------------------------------------------
            if (Remove_Zero == 1)
            { Chk_Remove_Zero.Checked = true; }

            BS_1.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
            G1.DataSource = BS_1;
            if (G1.RowCount == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود " : "No Data To export ", MyGeneral_Lib.LblCap);
                Btn_Ext_Click(null, null);
                return;

            }
            Xchange1 = true;
            G1_SelectionChanged(null, null);
            if (Chk_Remove_Zero.Checked == false)
            {
                DataTable DT2 = new DataTable();
                DT2 = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                TxtFD_LC.Text = DT2.Compute("Sum(DOloc_Amount)", "").ToString();
                TxtFC_LC.Text = DT2.Compute("Sum(COloc_Amount)", "").ToString();
                TxtPD_LC.Text = DT2.Compute("Sum(DPloc_Amount)", "").ToString();
                TxtPOC_LC.Text = DT2.Compute("Sum(Cploc_Amount)", "").ToString();
                TxtED_LC.Text = DT2.Compute("Sum(DEloc_Amount)", "").ToString();
                TxtEC_LC.Text = DT2.Compute("Sum(CEloc_Amount)", "").ToString();
            }
            else
            {
                TxtFD_LC.Text = DOloc_Amount1.ToString();
                TxtFC_LC.Text = COloc_Amount1.ToString();
                TxtPD_LC.Text = DPloc_Amount1.ToString();
                TxtPOC_LC.Text = Cploc_Amount1.ToString();
                TxtED_LC.Text = DEloc_Amount1.ToString();
                TxtEC_LC.Text = CEloc_Amount1.ToString();
            }
        }
        //--------------------------------------------------------------
        private void G1_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange1)
            {
                try
                {
                    Acc_no_current = ((DataRowView)BS_1.Current).Row["acc_no"].ToString();
                    Dot_Acc_no_current = ((DataRowView)BS_1.Current).Row["Dot_acc_no"].ToString();
                    Xchange2 = false;
                    if (level_rep == 0)
                    {


                        BS_2.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable().Select("acc_no = '" + Acc_no_current + "'").CopyToDataTable();
                        //BS_2.DataSource = DT_G2;

                    }
                    else
                    {
                        DataTable _Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable().Select("Dot_acc_no like'" + Dot_Acc_no_current + "%'").CopyToDataTable(); ;
                        var query = from row in _Dt.AsEnumerable()
                                    group row by new
                                    {

                                        T_id = row.Field<Int16>("T_id"),
                                        ATerm_Name = row.Field<string>("ATerm_Name"),
                                        From_Date1 = row.Field<DateTime>("From_Date1"),
                                        To_Date1 = row.Field<DateTime>("To_Date1"),
                                    } into grp
                                    orderby grp.Key.T_id
                                    select new
                                    {
                                        Key = grp.Key,
                                        T_id = grp.Key.T_id,
                                        ATerm_Name = grp.Key.ATerm_Name,
                                        DOloc_Amount = grp.Sum(r => r.Field<decimal>("DOloc_Amount")),
                                        COloc_Amount = grp.Sum(r => r.Field<decimal>("COloc_Amount")),
                                        DPloc_Amount = grp.Sum(r => r.Field<decimal>("DPloc_Amount")),
                                        Cploc_Amount = grp.Sum(r => r.Field<decimal>("Cploc_Amount")),
                                        DEloc_Amount = grp.Sum(r => r.Field<decimal>("DEloc_Amount")),
                                        CEloc_Amount = grp.Sum(r => r.Field<decimal>("CEloc_Amount")),
                                        From_Date1 = grp.Key.From_Date1,
                                        To_Date1 = grp.Key.To_Date1,
                                    };

                        DT_1 = CustomControls.IEnumerableToDataTable(query);
                        BS_2.DataSource = DT_1;



                    }
                    G2.DataSource = BS_2;
                    Xchange2 = true;
                    G2_SelectionChanged(null, null);
                }
                catch
                {
                    G2.DataSource = new DataTable();
                    G3.DataSource = new DataTable();
                }

            }

        }
        //--------------------------------------------------------------
        private void G2_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange2)
            {

                try
                {
                    Xchange3 = false;
                    T_id_current = Convert.ToInt16(((DataRowView)BS_2.Current).Row["T_id"]);
                    if (level_rep == 0)
                    {
                        BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and acc_no = '" + Acc_no_current + "'").CopyToDataTable();
                    }
                    else
                    {
                        BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and Dot_acc_no like'" + Dot_Acc_no_current + "%'").CopyToDataTable();


                    }
                    G3.DataSource = BS_3;
                    Xchange3 = true;
                    G3_SelectionChanged(null, null);
                }
                catch
                {
                    G3.DataSource = new DataTable();
                }

            }
        }
        //--------------------------------------------------------------
        private void G3_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange3)
            {
                try
                {
                    Xchange4 = false;
                    // DataTable Dt = new DataTable();
                    decimal MBalance = 0;
                    decimal MBalance_loc = 0;
                    Cust_id_current = Convert.ToInt16(((DataRowView)BS_3.Current).Row["Cust_id"]);
                    Cur_id_current = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_Cur_id"]);
                    Acc_id_current = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Acc_id"]);
                    //BS_4.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl3"].DefaultView.ToTable()
                    //.Select("T_id = " + T_id_current + "and Cust_id = " + Cust_id_current + "and for_Cur_id = " + Cur_id_current + "and Acc_id = " + Acc_id_current).CopyToDataTable();


                    try
                    {
                        Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl3"].Select("Cust_Id = " + Cust_id_current
                                                                                                       + " And For_Cur_Id = " + Cur_id_current
                                                                                                       + " and Acc_id = " + Acc_id_current
                                                                                                       + " And T_id = " + T_id_current).CopyToDataTable();

                        DataView dv = Dt.DefaultView;
                        dv.Sort = "c_date";
                        Dt = dv.ToTable();
                    }
                    catch
                    {
                        Dt = new DataTable();
                    }
                    foreach (DataRow Drow in Dt.Rows)
                    {


                        if (Convert.ToInt32(Drow["RowNum"]) == 0)
                        {
                            MBalance = Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]) * -1;
                            MBalance_loc = Convert.ToDecimal(Drow["Dloc_Amount"]) + Convert.ToDecimal(Drow["Cloc_Amount"]) * -1;
                        }

                        if (Convert.ToInt32(Drow["RowNum"]) > 0)
                        {
                            MBalance += Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]) * -1;
                            MBalance_loc += Convert.ToDecimal(Drow["Dloc_Amount"]) + Convert.ToDecimal(Drow["Cloc_Amount"]) * -1;
                        }
                        Drow["Balance_for"] = MBalance;
                        Drow["Balance_loc"] = MBalance_loc;

                    }
                    BS_4.DataSource = Dt;



                    G4.DataSource = BS_4;
                    Xchange4 = true;
                    G4_SelectionChanged(null, null);
                }
                catch
                {
                    G4.DataSource = new DataTable();
                }

            }
        }
        //--------------------------------------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {


                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                DataGridView[] Export_GRD = { G1 };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no",connection.Lang_id==1 ?  "Acc_AName" : "Acc_EName", 
               "DOloc_Amount", "COloc_Amount", "DPloc_Amount","CPloc_Amount","DEloc_Amount","CEloc_Amount").Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                //DataTable Dt = new DataTable();
                //DataTable Dt_details = new DataTable();
                //DataTable Rpt_dt_details = new DataTable();
                //DataTable Export_DT_main = new DataTable();
                //DataTable Export_DT_details = new DataTable();
                //string Acc_Name = ((DataRowView)BS_1.Current).Row["Acc_AName"].ToString();
                //Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                //Dt_details = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                //Export_DT_main = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no", "Acc_AName", "DOloc_Amount",
                //    "COloc_Amount", "DPloc_Amount", "Cploc_Amount", "DEloc_Amount", "CEloc_Amount").Select("Acc_AName = '" + Acc_Name + "'").CopyToDataTable();
                //Export_DT_details = Dt_details.DefaultView.ToTable(false, "", "", "",
                //"", "", "", "", "", "", "").Select().CopyToDataTable();
                //DataGridView[] Export_GRD = { G1, G6 };
                //DataTable[] Export_DT = { Export_DT_main, Export_DT_details };
                //MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        //--------------------------------------------------------------
        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
            if (level_rep == 0)
            {

                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                DataGridView G7 = new DataGridView();
                G7.Columns.Add("Column1", connection.Lang_id == 1 ? "رمز الحساب" : "Total Amount");
                G7.Columns.Add("Column2", connection.Lang_id == 1 ? "إسم الحساب" : "Branch amount");
                G7.Columns.Add("Column3", connection.Lang_id == 1 ? "مز الفرع" : "Amount details");
                G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الفرع" : "Accountant NO");
                G7.Columns.Add("Column3", connection.Lang_id == 1 ? "رمز الثانوي" : "Amount details");
                G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الثانوي" : "Accountant NO");
                G7.Columns.Add("Column5", connection.Lang_id == 1 ? "العملة" : "Accountant name");
                G7.Columns.Add("Column6", connection.Lang_id == 1 ? "اول المدة مدين _(ع.م)_" : "Dedit opening Balance (L.C)");
                G7.Columns.Add("Column7", connection.Lang_id == 1 ? "أول المدة دائن _(ع.م)_" : "Credit opening Balance (L.C)");
                G7.Columns.Add("Column8", connection.Lang_id == 1 ? "الفترة مدين _(ع.م)_" : "Dedit Periodic Balance (L.C)");
                G7.Columns.Add("Column9", connection.Lang_id == 1 ? "الفترة دائن _(ع.م)_" : "Credit Periodic Balance (L.C)");
                G7.Columns.Add("Column10", connection.Lang_id == 1 ? "نهاية المدة مدين _(ع.م)_" : "Dedit Last Balance (L.C)");
                G7.Columns.Add("Column11", connection.Lang_id == 1 ? "نهاية المدة دائن _(ع.م)_" : "Credit Last Balance (L.C)");
                G7.Columns.Add("Column10", connection.Lang_id == 1 ? "أول المدة _(ع.ص)_" : "Currency");
                G7.Columns.Add("Column11", connection.Lang_id == 1 ? "الفترة_(ع.ص)_" : "Periodic Balance (F.C)");
                G7.Columns.Add("Column11", connection.Lang_id == 1 ? "نهايةالمدة_(ع.ص)_" : "Last Balance (F.C)");

                DataTable Dt1 = new DataTable();
                Dt1 = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                DataGridView[] Export_GRD = { G7 };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_Balancing_new_tbl4"].DefaultView.ToTable(false, "acc_no", connection.Lang_id==1? "Acc_AName" : "Acc_EName","T_id", "ATerm_Name" , "Cust_id",
                connection.Lang_id==1 ? "ACUST_NAME" : "ECUST_NAME",connection.Lang_id==1? "Cur_ANAME" : "Cur_ENAME", "DOloc_Amount", "COloc_Amount","DPloc_Amount","CPloc_Amount","DEloc_Amount","CEloc_Amount","OFor_Amount"
                ,"PFor_Amount","EFor_Amount").Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                // // هنا خطا
                // DataTable Dt = new DataTable();
                // Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl5"];
                // DataGridView G7 = new DataGridView();
                // G7.Columns.Add("Column1", connection.Lang_id == 1 ? "الرصيد الاجمالي" : "Total Amount");
                // G7.Columns.Add("Column2", connection.Lang_id == 1 ? "الرصيد الفرعي " : "Branch amount");
                // G7.Columns.Add("Column3", connection.Lang_id == 1 ? "رقم الحساب" : "Amount details");
                // G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الحساب" : "Accountant NO");
                // //G7.Columns.Add("Column4", connection.Lang_id == 1 ? " رقم الحساب الفرعي" : "Accountant name");
                // //G7.Columns.Add("Column5", connection.Lang_id == 1 ? "إسم الحساب الفرعي" : "Accountant NO");
                // G7.Columns.Add("Column8", connection.Lang_id == 1 ? "رمز الثانوي" : "Customer NO");
                // G7.Columns.Add("Column9", connection.Lang_id == 1 ? "إسم الثانوي" : "Customer name");
                // G7.Columns.Add("Column10", connection.Lang_id == 1 ? "العملــــة" : "Currency");
                // G7.Columns.Add("Column11", connection.Lang_id == 1 ? "الرصيد _(ع.ص)_" : "Last Balance (F.C)");
                // DataTable Dt_details = new DataTable();
                // DataTable Rpt_dt_details = new DataTable();
                // DataTable Export_DT_main = new DataTable();
                // DataTable Export_DT_details = new DataTable();
                // DataTable Export_DT_main1 = new DataTable();
                // DataTable Dt1 = new DataTable();
                // string Branch_name = ((DataRowView)BS_1.Current).Row[connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName"].ToString();
                // string Term_name = ((DataRowView)BS_2.Current).Row["ATerm_Name" ].ToString();
                // Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
                // // Dt1 = connection.SQLDS.Tables["DT_1"];
                // Dt_details = connection.SQLDS.Tables["Account_Balancing_new_tbl2"];
                // Export_DT_main = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName",
                //     "DOloc_Amount", "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount").Select(connection.Lang_id == 1 ? "Acc_AName = '" : "Acc_EName = '" + Branch_name + "'").CopyToDataTable();

                // Export_DT_main1 = DT_1.DefaultView.ToTable(false, "T_Id",  "ATerm_Name" , "DOloc_Amount", "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount",
                //     "CEloc_Amount", "From_Date1", "To_Date1").Select("ATerm_Name = '" + Term_name + "'").CopyToDataTable();
                // Export_DT_details = Dt_details.DefaultView.ToTable(false, "Acc_no",
                //     connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name" ,
                //     connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "Eloc_Amount").Select().CopyToDataTable();

                // //"DOloc_Amount", "CEFor_Amount",
                // DataGridView[] Export_GRD = { G1, G2, G7 };
                // DataTable[] Export_DT = { Export_DT_main, Export_DT_main1, Export_DT_details };
                // MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                // //false, "Cust_id", "Acust_Name", "Acc_no",
                //// "Acc_Aname", "cur_AName", "DOloc_Amount", "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount"

            }
        }
        //--------------------------------------------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void G4_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange4)
            {
                Vo_no_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["Vo_No"]);
                nrec_date_current = ((DataRowView)BS_4.Current).Row["nrec_date"].ToString();
                Oper_id_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["oper_id"]);
                decimal DLoc_Amount = Convert.ToDecimal(((DataRowView)BS_4.Current).Row["DLoc_Amount"]);
                decimal Loc_Amount = Convert.ToDecimal(((DataRowView)BS_4.Current).Row["Loc_Amount"]);
                //Cust_id_current = Convert.ToInt32(((DataRowView)BS_4.Current).Row["CUST_ID"]);
                try
                {
                    BS_5.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl5"].DefaultView.ToTable()
             .Select("T_id = " + T_id_current + " and Vo_No = " + Vo_no_current + " and oper_id = " + Oper_id_current + "and Nrec_date like'" + nrec_date_current + "%'").CopyToDataTable();
                    G5.DataSource = BS_5;
                    foreach (DataGridViewRow row in G5.Rows)
                    {
                        if (Convert.ToInt32(row.Cells[9].Value) == Convert.ToInt32(Vo_no_current)
                                && Convert.ToInt16(row.Cells[2].Value) == Convert.ToInt16(Acc_id_current)
                                && (Convert.ToInt64(row.Cells[0].Value) == Convert.ToInt64(Loc_Amount)))
                        {
                            //&& Convert.ToInt32(row.Cells[14].Value) == Convert.ToInt32(Acc_Id)
                            row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }
                }
                catch
                {
                    G5.DataSource = new DataTable();

                }

            }
        }

        private void Terminal_Details_trail_balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            Xchange1 = false;
            Xchange2 = false;
            Xchange3 = false;
            Xchange4 = false;
        }



        private void Date()
        {

            Date_Grd = new DataGridView();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date1", "To_Date1" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            string date_from = ((DataRowView)BS_2.Current).Row["From_Date1"].ToString();
            string date_To = ((DataRowView)BS_2.Current).Row["To_Date1"].ToString();
            Date_Tbl.Rows.Add(new object[] { date_from, date_To });


            Date_Grd2 = new DataGridView();
            Date_Grd2.Columns.Add("Column1", connection.Lang_id == 1 ? "الفــرع" : "terminal Name");
            Date_Grd2.Columns.Add("Column2", connection.Lang_id == 1 ? "رمزالحساب" : "Account id");
            Date_Grd2.Columns.Add("Column3", connection.Lang_id == 1 ? "الحســـاب" : "Account name");

            string[] Column2 = { "ATerm_Name", "acc_no", "Acc_AName" };
            string[] DType2 = { "System.String", "System.String", "System.String" };
            Date_Tbl2 = CustomControls.Custom_DataTable("Date_Tbl2", Column2, DType2);
            string ATerm_Name = ((DataRowView)BS_2.Current).Row["ATerm_Name"].ToString();
            string acc_no = ((DataRowView)BS_1.Current).Row["acc_no"].ToString();
            string Acc_AName = ((DataRowView)BS_1.Current).Row[connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName"].ToString();
            Date_Tbl2.Rows.Add(new object[] { ATerm_Name, acc_no, Acc_AName });


        }

        private void Export_details_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                Date();
                DataTable exp_DT = new DataTable();
                DataTable exp_DT2 = new DataTable();
                int cust_id = 0;
                decimal Eloc_AMount_exp = 0;
                Int16 for_cur_id = 0;
                cust_id = Convert.ToInt32(((DataRowView)BS_3.Current).Row["Cust_id"]);
                for_cur_id = Convert.ToInt16(((DataRowView)BS_3.Current).Row["for_cur_id"]);
                Eloc_AMount_exp = Convert.ToDecimal(((DataRowView)BS_3.Current).Row["Eloc_AMount"]);
                exp_DT = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DOloc_Amount", "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount", "OFor_Amount", "PFor_Amount", "EFor_Amount", "Eloc_AMount", "T_id", "for_cur_id").Select("Cust_id = " + cust_id + "and T_id = " + T_id_current + " and for_cur_id = " + for_cur_id + "and Eloc_AMount = " + Eloc_AMount_exp).CopyToDataTable();
                exp_DT = exp_DT.DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DOloc_Amount", "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount", "OFor_Amount", "PFor_Amount", "EFor_Amount", "Eloc_AMount").Select().CopyToDataTable();
                exp_DT2 = Dt.DefaultView.ToTable(false, "DLoc_Amount", "CLoc_Amount", "Balance_Loc", "ACC_Id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "REAL_PRICE", "EXCH_PRICE", "DFor_Amount", "CFor_Amount", "Balance_for", "VO_NO", "nrec_date", "C_date", "Notes").Select().CopyToDataTable();


                DataGridView[] Export_GRD = { Date_Grd, Date_Grd2, G3, G4 };
                DataTable[] Export_DT = { Date_Tbl, Date_Tbl2, exp_DT, exp_DT2 };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Account_Balancing_new_tbl4"].Rows.Count > 0)
            {
                CrystalReport1 ObjRpt = new CrystalReport1();
                CrystalReport1_ENG ObjRptEng = new CrystalReport1_ENG();

                DataTable Exp_Dt = new DataTable();
                Exp_Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];
                DataTable[] _Exp_Dt = { Exp_Dt };
                DataGridView G7 = new DataGridView();
                G7.Columns.Add("Column1", connection.Lang_id == 1 ? "رمز الحساب" : "Total Amount");
                G7.Columns.Add("Column2", connection.Lang_id == 1 ? "إسم الحساب" : "Branch amount");
                G7.Columns.Add("Column3", connection.Lang_id == 1 ? "مز الفرع" : "Amount details");
                G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الفرع" : "Accountant NO");
                G7.Columns.Add("Column3", connection.Lang_id == 1 ? "رمز الثانوي" : "Amount details");
                G7.Columns.Add("Column4", connection.Lang_id == 1 ? "إسم الثانوي" : "Accountant NO");
                G7.Columns.Add("Column5", connection.Lang_id == 1 ? "العملة" : "Accountant name");
                G7.Columns.Add("Column6", connection.Lang_id == 1 ? "اول المدة مدين _(ع.م)_" : "Dedit opening Balance (L.C)");
                G7.Columns.Add("Column7", connection.Lang_id == 1 ? "أول المدة دائن _(ع.م)_" : "Credit opening Balance (L.C)");
                G7.Columns.Add("Column8", connection.Lang_id == 1 ? "الفترة مدين _(ع.م)_" : "Dedit Periodic Balance (L.C)");
                G7.Columns.Add("Column9", connection.Lang_id == 1 ? "الفترة دائن _(ع.م)_" : "Credit Periodic Balance (L.C)");
                G7.Columns.Add("Column10", connection.Lang_id == 1 ? "نهاية المدة مدين _(ع.م)_" : "Dedit Last Balance (L.C)");
                G7.Columns.Add("Column11", connection.Lang_id == 1 ? "نهاية المدة دائن _(ع.م)_" : "Credit Last Balance (L.C)");
                G7.Columns.Add("Column10", connection.Lang_id == 1 ? "أول المدة _(ع.ص)_" : "Currency");
                G7.Columns.Add("Column11", connection.Lang_id == 1 ? "الفترة_(ع.ص)_" : "Periodic Balance (F.C)");
                G7.Columns.Add("Column11", connection.Lang_id == 1 ? "نهايةالمدة_(ع.ص)_" : "Last Balance (F.C)");
                DataTable Dt1 = new DataTable();
                Dt1 = connection.SQLDS.Tables["Account_Balancing_new_tbl4"];

                DataGridView[] Export_GRD = { G7 };
                DataTable[] Export_DT = { connection.SQLDS.Tables["Account_Balancing_new_tbl4"].DefaultView.ToTable(false, "acc_no", connection.Lang_id==1? "Acc_AName" : "Acc_EName","T_id", "ATerm_Name" , "Cust_id",
                connection.Lang_id==1 ? "ACUST_NAME" : "ECUST_NAME",connection.Lang_id==1? "Cur_ANAME" : "Cur_ENAME", "DOloc_Amount", "COloc_Amount","DPloc_Amount","CPloc_Amount","DEloc_Amount","CEloc_Amount","OFor_Amount"
                ,"PFor_Amount","EFor_Amount").Select().CopyToDataTable()};

                string frm_name = "";
                string frm_Ename = "";

                if (form_no_new == 1)
                {
                    frm_name = "ميزان المراجعة";
                    frm_Ename = "A balance detailed review";
                }
                else if (form_no_new == 2)
                {
                    frm_name = "كشف حساب";
                    frm_Ename = "Account statement";
                }
                else
                {
                    frm_name = "ارصدة نهاية المدة على مستوى المؤسسة";
                    frm_Ename = "The balance end of the period for the company";
                }

                string date_from = (((DataRowView)BS_2.Current).Row["From_Date1"]).ToString();
                string to_date = (((DataRowView)BS_2.Current).Row["To_Date1"]).ToString();

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("user_name", connection.User_Name);
                Dt_Param.Rows.Add("date_from", date_from);
                Dt_Param.Rows.Add("to_date", to_date);
                Dt_Param.Rows.Add("frm_Ename", frm_Ename);

                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, true, Dt_Param, Export_GRD, Export_DT, true, false, this.Text);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                //connection.SQLDS.Tables["Acc_e_term_pdf"].TableName = "Account_Balancing_new_tbl4";
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للطباعة" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
    }
}