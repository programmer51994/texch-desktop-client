﻿using System;
using System.Windows.Forms;
using System.Data;

namespace Integration_Accounting_Sys
{
    public partial class User_Terminals_Add_Upd : Form
    {
        #region Definitions
        string Sql_text = "";
        int Frm_Id = 0;
        bool Change1 = false;
        
        Int16 User_ID = 0;
        String Txt = "";
        DataTable _Dt = new DataTable();
        BindingSource _Bs_user_term_add = new BindingSource();
        BindingSource _Bs_user_term_add1 = new BindingSource();
        #endregion

        public User_Terminals_Add_Upd(int Form_Id )
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

            Grd_User.AutoGenerateColumns = false;
            Grd_Terminals.AutoGenerateColumns = false;
            Frm_Id = Form_Id;
            
        }
        //---------------------------------------------------
        private void User_Companay_Add_Upd_Load(object sender, EventArgs e)
        {

            if (Frm_Id == 1)
            { TxtUser_Name_TextChanged(null, null); }
            if (Frm_Id == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديل مستخدمي الفروع" : "User_Terminals_Update";
                TxtUser_Name.Text = ((DataRowView)User_Terminals_Main._Bs_user_term.Current).Row["User_Name"].ToString();
                TxtUser_Name.Enabled = false;
               // Change1 = true;
            }
           

        }
        //---------------------------------------------------
        private void TxtUser_Name_TextChanged(object sender, EventArgs e)
        {
            Change1 = false;
            Sql_text = " Select  0 as Chk , USER_ID, User_Name, Full_name "
                        + " FROM  USERS "
                        + " where USER_State not in (2,3,0) "
                        + " And user_name like '%" + TxtUser_Name.Text + "%'"
                        + " And user_id <> 1 ";
            _Bs_user_term_add.DataSource = connection.SqlExec(Sql_text, "Users_Tbl");
            Grd_User.DataSource = _Bs_user_term_add;
            if (connection.SQLDS.Tables["Users_Tbl"].Rows.Count > 0)
            {
                Change1 = true;
                Grd_User_SelectionChanged(null, null);
            }
        }
        //---------------------------------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {

            if (Change1)
            {
               // LblRec.Text = connection.Records();

                User_ID = Convert.ToInt16(((DataRowView)_Bs_user_term_add.Current).Row["USER_ID"]);
                if (Frm_Id == 1)
                {
                    Txt = " Select  0 as Chk ,ACUST_NAME , ECUST_NAME ,T_ID"
                               + " from TERMINALS  A , CUSTOMERS  B  "
                               + " where T_ID not in ( Select T_ID from User_Terminals where T_USER_ID = " + User_ID + " ) "
                               + " AND  a.CUST_ID = b.CUST_ID ";

                }
                else
                {
                    Txt = " Select  1 as Chk ,ACUST_NAME , ECUST_NAME ,T_ID "
                   + "from TERMINALS  A , CUSTOMERS  B   where T_ID  in ( Select T_ID from User_Terminals where User_State  in(1,0) and T_USER_ID = " + User_ID + ")"
                   + " AND  a.CUST_ID = b.CUST_ID "
                    +" union "
                    + " Select  0 as Chk ,ACUST_NAME , ECUST_NAME ,T_ID "
                    + " from TERMINALS  A , CUSTOMERS  B   where T_ID  in ( Select T_ID from User_Terminals where User_State in(2) and  T_USER_ID = " + User_ID + ")"
                    + " AND  a.CUST_ID = b.CUST_ID  ";
                }

                _Dt = connection.SqlExec(Txt, "Term_TBL");
                _Bs_user_term_add1.DataSource = _Dt;
                Grd_Terminals.DataSource = _Bs_user_term_add1;
                checkBox1.Checked = false;
            }


        }
        //---------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validations

            int Rec_No = connection.SQLDS.Tables["Term_TBL"].Select("Chk > 0").Length;
            if (Rec_No <= 0 && Frm_Id == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم تحدد فرع للأضافة يجب تأشير الفرع  " : "There is no user to add,Please select the users", MyGeneral_Lib.LblCap);
                Grd_Terminals.Focus();
                return;
            }
            #endregion
            DataTable DT = new DataTable();
            if (Frm_Id == 1)
            {
   
               DT = connection.SQLDS.Tables["Term_TBL"].DefaultView.ToTable(false, "T_id", "Chk").Select("Chk > 0").CopyToDataTable();   
            }
            else
            {
                DT = _Dt.DefaultView.ToTable(false, "T_id", "Chk").Select().CopyToDataTable();    
            }
            connection.SQLCMD.Parameters.AddWithValue("@Term_Users_ID", User_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Term_Users_Tbl", DT);
            connection.SQLCMD.Parameters.AddWithValue("@Form_Id", Frm_Id);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_Users_Terminals", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                User_Companay_Add_Upd_Load(sender, e);
            }
            else
            {
                this.Close();
            }
        }
        //---------------------------------------------------        
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------        
        private void User_Companay_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change1 = false;
            
            string[] Str = { "Term_TBL", "Users_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //---------------------------------------------------        
        private void User_Companay_Add_Upd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //---------------------------------------------------        
        private void User_Companay_Add_Upd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    AddBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //---------------------------------------------------      
        private void checkBox1_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Terminals.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

            Grd_User.RefreshEdit();
        }
    }
}