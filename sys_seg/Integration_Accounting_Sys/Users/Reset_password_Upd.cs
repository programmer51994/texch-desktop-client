﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Reset_password_Upd : Form
    {
        #region MyRegion
        string SqlTxt = "";
        bool Change = false;
        bool lst_Change = false;
        bool change_1 = false;
        bool change_sub_cust = false;
        #endregion
        BindingSource _Bs_user_term_add = new BindingSource();
        BindingSource _bs_cbo_term = new BindingSource();
        BindingSource _bs_sub_cust = new BindingSource();
        public Reset_password_Upd()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_users.AutoGenerateColumns = false;

        }
        //------------------------------------------------------------------------------------
        private void Reset_password_Upd_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {
                Cmb_permission.Items[0] = "Company branch";
                Cmb_permission.Items[1] = " Sub Agent";
            }

            change_1 = false;
            SqlTxt = " Select  ACUST_NAME , ECUST_NAME ,T_ID , A.cust_id "
                + " from TERMINALS  A , CUSTOMERS  B "
                + " where  a.CUST_ID = b.CUST_ID  "
                + " And TERM_STATE_ID = 1 ";

            _bs_cbo_term.DataSource = connection.SqlExec(SqlTxt, "Term_TBL");
            Cbo_Terminals.DataSource = _bs_cbo_term;
            Cbo_Terminals.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
            Cbo_Terminals.ValueMember = "T_id";

            Cmb_permission.SelectedIndex = 0;
        }
        //-------------------------------------------------------------------------------
        private void Cmb_permission_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_permission.SelectedIndex == 0)
            {
                label9.Visible = false;
                cbo_sub_cust.Visible = false;
            }
            else
            {
                label9.Visible = true;
                cbo_sub_cust.Visible = true;
            }
            change_1 = true;
            Cbo_Terminals_SelectedIndexChanged(null, null);

        }
        //------------------------------------------------------------------------------------------
        private void Cbo_Terminals_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_1)
            {
                if (Cmb_permission.SelectedIndex == 0)//فروع
                {
                    Change = true;
                    TxtUser_Id_TextChanged(null, null);
                }
                else // وكالات
                {

                    change_sub_cust = false;
                    string SqlTxt1 = " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name , Sub_Cust_ID as cust_id "
                    + "  From  Sub_CUSTOMERS_online A   "
                    + "  Where A.Cust_state  = 1  "
                    + "  and A.t_id =  " + Cbo_Terminals.SelectedValue
                    + " 	and Sub_Cust_ID in (select cust_id from Login_Cust_Online) "
                    + "    order by cust_id  ";

                    _bs_sub_cust.DataSource = connection.SqlExec(SqlTxt1, "cust_tbl");
                    cbo_sub_cust.DataSource = _bs_sub_cust;
                    cbo_sub_cust.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                    cbo_sub_cust.ValueMember = "cust_id";
                    change_sub_cust = true;
                    cbo_sub_cust_SelectedIndexChanged(null, null);

                }

            }
        }

        private void cbo_sub_cust_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_sub_cust)
            {
                if (connection.SQLDS.Tables["cust_tbl"].Rows.Count > 0)
                {
                    Change = true;
                    TxtUser_Id_TextChanged(null, null);
                }
                else
                {
                    Change = false;
                    Grd_users.DataSource = new BindingSource();
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد وكالات معرفة" : "there is no defined customers", MyGeneral_Lib.LblCap);

                    return;
                }
            }
        }
        //---------------------------------------------------------------
        private void TxtUser_Id_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                lst_Change = false;
                if (Cmb_permission.SelectedIndex == 0)
                {

                    SqlTxt = " SELECT  0 as Chk, T_User_Id, A.T_Id  , B.user_id,User_Name "
                            + " FROM   User_Terminals A , USERS B ,TERMINALS C ,CUSTOMERS D  "
                                + " where C.CUST_ID = D.CUST_ID"
                                + " AND A.T_Id = C.T_ID"
                                + " and C.t_id = " + Cbo_Terminals.SelectedValue
                                + " And B.User_State = 1 "
                                + " And T_User_Id = B.USER_ID "
                                + " And B.user_name like '%" + TxtUser_Id.Text + "%' "
                                + " And T_User_Id <> 1 "
                                + " And A.User_State <> 3 ";
                    _Bs_user_term_add.DataSource = connection.SqlExec(SqlTxt, "Users_Tbl");
                    Grd_users.DataSource = _Bs_user_term_add;



                }
                else
                {

                    SqlTxt = " SELECT distinct  0 as Chk,login_id, C.T_id  , B.user_id, login_Aname  as User_Name, C.cust_id , A.USER_ID "
                              + " FROM   Login_Cust_Online A , USERS B , CUSTOMERS_ACCOUNTS C "
                              + " where  A.CUST_ID = " + cbo_sub_cust.SelectedValue
                              + "  And C.cust_id = A.cust_id "
                              + "  And  B.User_State = 1 "
                              + "  And A.user_id = B.USER_ID "
                              + " And  A.t_id  =  C.T_ID   "
                              + "  And  A.t_id  =  " + Cbo_Terminals.SelectedValue
                              + "  And B.user_name like '%" + TxtUser_Id.Text + "%' ";


                    _Bs_user_term_add.DataSource = connection.SqlExec(SqlTxt, "Users_Tbl");
                    Grd_users.DataSource = _Bs_user_term_add;


                }

                if (connection.SQLDS.Tables["Users_Tbl"].Rows.Count > 0)
                {
                    lst_Change = true;
                }
                else
                {
                    Grd_users.DataSource = new BindingSource();

                }
            }
        }

        //------------------------------------------------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validations

            DataTable DT = new DataTable();
            int Rec_No = connection.SQLDS.Tables["Users_Tbl"].Select("Chk > 0").Length;
            if (Rec_No <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم تحدداسم مستخدم للاضافة يجب تحديداسم  مستخدم   " : "There is no user to add,Please select the users", MyGeneral_Lib.LblCap);
                Grd_users.Focus();
                return;
            }
            if (TxtPassword.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور" : "Enter Password", MyGeneral_Lib.LblCap);
                TxtPassword.Focus();
                return;
            }
            if (TxtPassword.Text.Length < 6)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ان لا تقل كلمة المرور عن ستة مراتب" : "The Password must be at least 6 Characters", MyGeneral_Lib.LblCap);
                TxtPassword.Focus();
                return;
            }

            #endregion
            DataTable DT1 = new DataTable();


            DT1 = connection.SQLDS.Tables["Users_Tbl"].DefaultView.ToTable(false, "Chk", "user_id").Select("Chk > 0").CopyToDataTable();
            int cust_id = 0;
            if (Cmb_permission.SelectedIndex == 1)
            {
                cust_id = Convert.ToInt32(cbo_sub_cust.SelectedValue);
            }
            else
            {
                cust_id = Convert.ToInt32(((DataRowView)_bs_cbo_term.Current).Row["cust_id"]);
            }
            int Term_id = Convert.ToInt32(((DataRowView)_bs_cbo_term.Current).Row["t_id"]);
            connection.SQLCMD.Parameters.AddWithValue("@Reset_Users_Tbl", DT1);
            connection.SQLCMD.Parameters.AddWithValue("@Cust_Id", cust_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", Term_id);
            connection.SQLCMD.Parameters.AddWithValue("@web_flag", Cbo_Terminals.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@pwd", TxtPassword.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@users_type", Cmb_permission.SelectedIndex);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            //MyGeneral_Lib.Copytocliptext("Add_Users_Terminals_Permission" + "2," + connection.user_id + "," + Cbo_Terminals.SelectedValue + "," + Term_id + "," + Cbo_Terminals.SelectedValue + "," );
            connection.SqlExec("Add_Upd_Users_Reset", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);

            TxtPassword.Clear();
            Reset_password_Upd_Load(null, null);


        }
        //------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-------------------------------------------------------------------------------

        private void checkBox1_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_users.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

            Grd_users.RefreshEdit();
        }

        private void User_Terminals_Permissions_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Reset_password_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            lst_Change = false;
            change_1 = false;
            change_sub_cust = false;

            string[] Str = { "Users_Tbl", "cust_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
    }
}
