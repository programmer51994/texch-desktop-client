﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class User_Terminals_Main : Form
    {
        string SQL_Txt = "";
    public  static BindingSource _Bs_user_term = new BindingSource();
        public User_Terminals_Main()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            if (connection.Lang_id != 1)
            {
                Grd_User.Columns["Column4"].DataPropertyName = "User_State_Ename";
                Grd_User.Columns["Column7"].DataPropertyName = "e_Address";
            }
            
        }
        //-------------------------------------------
        private void User_Companies_Load(object sender, EventArgs e)
        {
            SQL_Txt = "SELECT T_Id,ACust_Name,ECust_Name From CUSTOMERS A,TERMINALS B  "
                    + " Where A.CUST_ID = B.CUST_ID "
                    + " And Cust_flag <> 0 "
                    + " And T_Id in (Select T_Id From User_Terminals)"
                    + " Union "
                    + " select 0 AS T_Id,' (جميــــع الفــروع) ' AS ACust_Name, ' (All Terminals) ' AS ECust_Name "
                    + " Order By " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");
            CboComp_ID.ComboBox.DataSource = connection.SqlExec(SQL_Txt, "TerminalsTbl");
            CboComp_ID.ComboBox.ValueMember = "T_Id";
            CboComp_ID.ComboBox.DisplayMember = connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name";

            Grd_User.AutoGenerateColumns = false;
            Get_users();

        }
        private void Get_users()
        {

            object[] Sparams = { Convert.ToInt16(CboComp_ID.ComboBox.SelectedValue), TxtUser_Name.Text };

            connection.SqlExec("Main_User_Terminals", "User_Terminal_Tbl", Sparams);

            if (connection.SQLDS.Tables["User_Terminal_Tbl"].Rows.Count <= 0)
                UpdBtn.Enabled = false;
            else
                UpdBtn.Enabled = true;

            #region Grouping
            string strCurrentValue = string.Empty;
            string strPreviousValue = string.Empty;
            DataTable DT = new DataTable();
            DT = connection.SQLDS.Tables["User_Terminal_Tbl"];
            for (int m = 0; m < DT.Rows.Count; m++)
            {
                object cellValue = DT.Rows[m]["ACust_Name"]; // Catch the value form current column
                strCurrentValue = cellValue.ToString().Trim(); // Assign the above value to 'strCurrentValue'

                if (strCurrentValue != strPreviousValue) // Now compare the current value with previous value
                {
                    DT.Rows[m]["ACust_Name"] = strCurrentValue;	// If current value is not equal to previous value 
                    // the column will display current value
                }
                else
                {
                    DT.Rows[m]["ACust_Name"] = string.Empty;	// If current value is equal to previous value 
                    // the column will be empty	
                }
                strPreviousValue = strCurrentValue;	        // Assign current value to previous value
            }
            strCurrentValue = string.Empty;					// Reset Current and Previous Value
            strPreviousValue = string.Empty;
            #endregion

            _Bs_user_term.DataSource = DT;
            Grd_User.DataSource = _Bs_user_term;

            if (DT.Rows.Count <= 0)
                UpdBtn.Enabled = false;
            else
                UpdBtn.Enabled = true;
        }

        //-------------------------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_user_term);
        }
        //-------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            User_Terminals_Add_Upd AddFrm = new User_Terminals_Add_Upd(1 );
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Companies_Load(sender, e);
            this.Visible = true;
        }
        //-------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
            
            User_Terminals_Add_Upd AddFrm = new User_Terminals_Add_Upd(2 );
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Companies_Load(sender, e);
            this.Visible = true;
        }
        //-------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Get_users();
        }
        //-------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            TxtUser_Name.Text = "";
            CboComp_ID.SelectedIndex = 0;
            User_Companies_Load(sender, e);
        }
        //-------------------------------------------
        private void User_Companies_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Str = { "User_Terminal_Tbl", "TerminalsTbl" };
            foreach (string Tbl in Str)
            {
                if(connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables[Tbl].Clear();
            }
        }
        //-------------------------------------------
        private void User_Companies_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //-------------------------------------------
        private void User_Companies_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }

        private void User_Terminals_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}
