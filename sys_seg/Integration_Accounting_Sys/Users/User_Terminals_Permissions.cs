﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class User_Terminals_Permissions : Form
    {
        #region MyRegion
        string SqlTxt = "";
        bool Change = false;
        bool lst_Change = false;
        bool change_1 = false;
        bool change_sub_cust = false;
        #endregion
        BindingSource _bs_Grd = new BindingSource();
        BindingSource _bs_cbo_term = new BindingSource();
        BindingSource _bs_sub_cust = new BindingSource();
        public User_Terminals_Permissions()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_TermPermission.AutoGenerateColumns = false;
           
        }
        //------------------------------------------------------------------------------------
        private void Comp_User_Permissions_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "Node_Ename";
                Cmb_permission.Items[0] = "Company branch";
                Cmb_permission.Items[1] = " Sub Agent";
            }

            change_1 = false;
            SqlTxt = " Select  ACUST_NAME , ECUST_NAME ,T_ID , A.cust_id "
                + " from TERMINALS  A , CUSTOMERS  B "
                + " where  a.CUST_ID = b.CUST_ID  "
                + " And TERM_STATE_ID = 1 ";

            _bs_cbo_term.DataSource = connection.SqlExec(SqlTxt, "Term_TBL");
            Cbo_Terminals.DataSource = _bs_cbo_term;
            Cbo_Terminals.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
            Cbo_Terminals.ValueMember = "T_id";

            //if (connection.Lang_id == 1)
            //{
            //    label4.Text = "فروع الشركة";
            //}
            //else
            //{
            //    label4.Text = "Company branch";
            //}
           

            Cmb_permission.SelectedIndex = 0;

            
            
            //Change = false;
            //Cmb_permission.SelectedIndex = 0;
          

            //if (connection.SQLDS.Tables["Term_TBL"].Rows.Count > 0)
            //{
            //    Change = true;
            //    TxtUser_Id_TextChanged(null, null);
            //}
        }
        //-------------------------------------------------------------------------------
        private void Cmb_permission_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_permission.SelectedIndex == 0)
            {
                label9.Visible = false;
                cbo_sub_cust.Visible = false;
            }
            else
            {
                label9.Visible = true;
                cbo_sub_cust.Visible = true;
            }
            change_1 = true;
            Cbo_Terminals_SelectedIndexChanged(null, null);

        }
        //------------------------------------------------------------------------------------------
        private void Cbo_Terminals_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_1)
            {
                if (Cmb_permission.SelectedIndex == 0)//فروع
                {
                    //if (connection.SQLDS.Tables["Term_TBL"].Rows.Count > 0)
                    //{
                        Change = true;
                        TxtUser_Id_TextChanged(null, null);
                    //}
                }
                else // وكالات
                {
                    //change_1 = false;
                    change_sub_cust = false; 
                    string SqlTxt1 = " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name , Sub_Cust_ID as cust_id "
                    + "  From  Sub_CUSTOMERS_online A   "
                    + "  Where A.Cust_state  = 1  "
                    + "  and A.t_id =  " + Cbo_Terminals.SelectedValue
                    + " 	and Sub_Cust_ID in (select cust_id from Login_Cust_Online) "
                    + "    order by cust_id  ";

                    _bs_sub_cust.DataSource = connection.SqlExec(SqlTxt1, "cust_tbl");
                    cbo_sub_cust.DataSource = _bs_sub_cust;
                    cbo_sub_cust.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                    cbo_sub_cust.ValueMember = "cust_id";
                    //label4.Text = "الوكالات الثانوية:";
                    change_sub_cust = true;
                    cbo_sub_cust_SelectedIndexChanged(null, null);

                }
                
            }
        }

        private void cbo_sub_cust_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_sub_cust)
            {
                if (connection.SQLDS.Tables["cust_tbl"].Rows.Count > 0)
                {
                    Change = true;
                    TxtUser_Id_TextChanged(null, null);
                }
                else
                {
                    Change = false;
                    LstUser_Id.DataSource = new BindingSource();
                    Grd_TermPermission.DataSource = new BindingSource();
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد وكالات معرفة" : "there is no defined customers", MyGeneral_Lib.LblCap);

                    return;
                }
            }
        }
        //---------------------------------------------------------------
        private void TxtUser_Id_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                lst_Change = false ;
                if (Cmb_permission.SelectedIndex == 0)
                {

                    SqlTxt = " SELECT  T_User_Id, A.T_Id  , User_Name "
                            + " FROM   User_Terminals A , USERS B ,TERMINALS C ,CUSTOMERS D  "
                                + " where C.CUST_ID = D.CUST_ID"
							    + " AND A.T_Id = C.T_ID"
                                + " and C.t_id = " + Cbo_Terminals.SelectedValue
                                + " And B.User_State = 1 "
                                + " And T_User_Id = B.USER_ID "
                                + " And B.user_name like '%" + TxtUser_Id.Text + "%' "
                                + " And T_User_Id <> 1 "
                                + " And A.User_State <> 3 ";
                        LstUser_Id.DataSource = connection.SqlExec(SqlTxt, "User_Term");
                        LstUser_Id.DisplayMember = "User_Name";
                        LstUser_Id.ValueMember = "T_User_Id";
                   

                 
                }
                else
                {
                    
                        SqlTxt = " SELECT distinct login_id, C.T_id  , login_Aname , C.cust_id , A.USER_ID "
                              + " FROM   Login_Cust_Online A , USERS B , CUSTOMERS_ACCOUNTS C "
                              + " where  A.CUST_ID = " + cbo_sub_cust.SelectedValue
                              + "  And C.cust_id = A.cust_id "
                              + "  And  B.User_State = 1 "
                              + "  And A.user_id = B.USER_ID "
                              + " And  A.t_id  =  C.T_ID   "
                              + "  And  A.t_id  =  " + Cbo_Terminals.SelectedValue
                              + "  And B.user_name like '%" + TxtUser_Id.Text + "%' ";


                        LstUser_Id.DataSource = connection.SqlExec(SqlTxt, "User_Term");
                        LstUser_Id.DisplayMember = "login_Aname";
                        LstUser_Id.ValueMember = "USER_ID";
                 
                
                }

                if (connection.SQLDS.Tables["User_Term"].Rows.Count > 0)
                {
                    lst_Change = true;
                    LstUser_Id_SelectedIndexChanged(null, null);
                }
                else
                {
                    Grd_TermPermission.DataSource = new BindingSource();

                }
            }
        }
        //--------------------------------------------------------------------
        private void LstUser_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            if (lst_Change)
            {
                string sql_count = "select isnull( (select count( Node_Id )from Terminal_User_Permission where Per_User_Id = "+ LstUser_Id.SelectedValue + " And t_id = " + ((DataRowView)_bs_cbo_term.Current).Row["t_id"] + ")  ,0) as count_user";
                connection.SqlExec(sql_count, "Cont_tbl");
                Int16 count_user = Convert.ToInt16( connection.SQLDS.Tables["Cont_tbl"].Rows[0]["count_user"]);
                string Per_User_Id_current="";
                if (count_user > 0)
                {
                    Per_User_Id_current = "  and  Per_User_Id = " + LstUser_Id.SelectedValue;

                }
                else { Per_User_Id_current = ""; }


              if (Cmb_permission.SelectedIndex == 0)
                {
                    SqlTxt = " SELECT distinct 1 as Chk,a.Node_Id, Parent, Frm_Name, Node_AName, Node_EName, Frm_Id, "
                             + "   Frm_Id_Main, CS_Flag, a.Web_Flag, Node_ADescription, Node_EDescription ,  Per_user_Note "
                             + "   FROM         Terminal_Permissions a , Terminal_User_Permission b  Where a.Node_Id  in "
                             + "   ( Select Node_Id from Terminal_User_Permission where Per_User_Id = " + LstUser_Id.SelectedValue + " And t_id = " + ((DataRowView)_bs_cbo_term.Current).Row["t_id"] + " ) "
                             + "   And parent <> 0  And a.Web_Flag = 0  and  Per_User_Id = " + LstUser_Id.SelectedValue 
                             + "   union "
                             + "  SELECT distinct 0 as Chk,a.Node_Id, Parent, Frm_Name, Node_AName, Node_EName, "
                             + "  Frm_Id, Frm_Id_Main, CS_Flag, a.Web_Flag, Node_ADescription, Node_EDescription  , '' as Per_user_Note  "
                             + "   FROM         Terminal_Permissions a, Terminal_User_Permission b  Where a.Node_Id Not in  "
                             + "  ( Select Node_Id from Terminal_User_Permission where Per_User_Id = " + LstUser_Id.SelectedValue + " And t_id = " + ((DataRowView)_bs_cbo_term.Current).Row["t_id"] + " ) And parent <> 0  And a.Web_Flag = 0  "
                             +  Per_User_Id_current  
                             + "   order by Chk  ";

                    _bs_Grd.DataSource = connection.SqlExec(SqlTxt, "TermPerm_Tbl");
                    Grd_TermPermission.DataSource = _bs_Grd;
                    
                    txt_old_note.DataBindings.Clear();
                    txt_old_note.DataBindings.Add("Text", _bs_Grd, "Per_user_Note");
                }
            else
                {
                    SqlTxt = " SELECT distinct 1 as Chk,a.Node_Id, Parent, Frm_Name, Node_AName, Node_EName, Frm_Id, "
                         + "   Frm_Id_Main, CS_Flag, a.Web_Flag, Node_ADescription, Node_EDescription  ,  Per_user_Note "
                         + "   FROM         Terminal_Permissions a, Terminal_User_Permission b   Where a.Node_Id  in "
                         + "   ( Select Node_Id from Terminal_User_Permission where Per_User_Id = " + LstUser_Id.SelectedValue + " And t_id = " + ((DataRowView)_bs_cbo_term.Current).Row["t_id"] + "  ) "
                         + "   And parent <> 0  And a.Web_Flag = 1  and  Per_User_Id = " + LstUser_Id.SelectedValue 
                         + "   union "
                         + "  SELECT distinct 0 as Chk,a.Node_Id, Parent, Frm_Name, Node_AName, Node_EName, "
                         + "  Frm_Id, Frm_Id_Main, CS_Flag, a.Web_Flag, Node_ADescription, Node_EDescription  , '' as Per_user_Note "
                         + "   FROM         Terminal_Permissions a ,Terminal_User_Permission b   Where a.Node_Id Not in  "
                         + "   ( Select Node_Id from Terminal_User_Permission where Per_User_Id = " + LstUser_Id.SelectedValue + " And t_id = " + ((DataRowView)_bs_cbo_term.Current).Row["t_id"] + " ) And parent <> 0  And a.Web_Flag = 1  "
                         +  Per_User_Id_current
                         + "   order by Chk  ";

                _bs_Grd.DataSource  = connection.SqlExec(SqlTxt, "TermPerm_Tbl");
                Grd_TermPermission.DataSource = _bs_Grd;

                txt_old_note.DataBindings.Clear();
                txt_old_note.DataBindings.Add("Text", _bs_Grd, "Per_user_Note");
            }
            }
            else
             {
            }
        }
      
        //------------------------------------------------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validations

            DataTable DT = new DataTable();
            //int Rec_No = connection.SQLDS.Tables["TermPerm_Tbl"].Select("Chk > 0").Length;
            //if (Rec_No <= 0 )
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "لم تحدد الصلاحيات يجب تأشير الصلاحيات للمستخدم  " : "There is no Permision,Please select the users Permission", MyGeneral_Lib.LblCap);
            //    Grd_TermPermission.Focus();
            //    return;
            //}
            #endregion
            
            try
            {
                 DT = connection.SQLDS.Tables["TermPerm_Tbl"].DefaultView.ToTable(false, "Chk", "Node_Id" ,"Web_Flag").Select("Chk = 1").CopyToDataTable();
                 DT = DT.DefaultView.ToTable(false, "Node_Id" ,"Web_Flag").Select().CopyToDataTable();
            }
            catch
            {
              DT.Columns.Add("Node_Id");
              DT.Columns.Add("web_Falg");
                // DT = connection.SQLDS.Tables["TermPerm_Tbl"].DefaultView.ToTable(false, "Chk", "Node_Id").Select("Chk = 0").CopyToDataTable();
            }
           
            int cust_id = 0;
            if (Cmb_permission.SelectedIndex == 1)
            {
                cust_id = Convert.ToInt32(cbo_sub_cust.SelectedValue);
            }
            else
            {
                cust_id = Convert.ToInt32(((DataRowView)_bs_cbo_term.Current).Row["cust_id"]);
            }

            int Term_id = Convert.ToInt32(((DataRowView)_bs_cbo_term.Current).Row["t_id"]);      //@Per_user_Note
            connection.SQLCMD.Parameters.AddWithValue("@Users_Terminals_Permissions", DT);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id );
            connection.SQLCMD.Parameters.AddWithValue("@Cust_Id", cust_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", Term_id);
            connection.SQLCMD.Parameters.AddWithValue("@web_flag", Cbo_Terminals.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@per_User_Id", LstUser_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Per_user_Note", txt_new_note.Text.Trim());
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";

            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            MyGeneral_Lib.Copytocliptext("Add_Users_Terminals_Permission " + "2," + connection.user_id + "," + Cbo_Terminals.SelectedValue + "," + Term_id + "," + Cbo_Terminals.SelectedValue + "," + LstUser_Id.SelectedValue + ",''");
            connection.SqlExec("Add_Users_Terminals_Permission", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
            txt_new_note.Text = "";

            Comp_User_Permissions_Load(null, null);
                 
        }
        //------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-------------------------------------------------------------------------------
        private void Comp_User_Permissions_FormClosed(object sender, FormClosedEventArgs e)
        {
            
             Change = false;
             lst_Change = false;
             change_1 = false;
             change_sub_cust = false;

             string[] Str = { "User_Term", "Term_TBL", "TermPerm_Tbl", "cust_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //-------------------------------------------------------------------------------
        private void checkBox1_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_TermPermission.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

            Grd_TermPermission.RefreshEdit();
        }

        private void User_Terminals_Permissions_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Btn_Print_Click(object sender, EventArgs e)
        {
            int term_id = 0;
            Int16 web_flag = Convert.ToInt16(Cmb_permission.SelectedIndex);
            term_id = Convert.ToInt16(((DataRowView)_bs_cbo_term.Current).Row["t_id"]);
            string SqlTxt_Rpt = " User_Terminals_Permissions_Rpt " + LstUser_Id.SelectedValue + "," + term_id + "," + web_flag ;
            connection.SqlExec(SqlTxt_Rpt, "TermPerm_Rpt_Tbl");


            DataTable DT_User_Term_Per = new DataTable();

            try
            {

                DT_User_Term_Per = connection.SQLDS.Tables["TermPerm_Rpt_Tbl"].DefaultView.ToTable(false, "Node_Id", "Frm_Name",
                    "Node_AName", "Node_EName", "Frm_Id", "User_Id", "User_Name", "c_date").Select().CopyToDataTable();

                string Frm_name = "صلاحيات المستخدم على الواجهات";
                string EFrm_name = "User forms Permissions";
                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("user_name", connection.User_Name);
                Dt_Param.Rows.Add("permission_name", Convert.ToString(Cmb_permission.Text));
                Dt_Param.Rows.Add("Terminals_name", Convert.ToString(Cbo_Terminals.Text));
                Dt_Param.Rows.Add("sub_cust_name", Convert.ToString(cbo_sub_cust.Text));
                Dt_Param.Rows.Add("per_User_name", Convert.ToString(LstUser_Id.Text));
                Dt_Param.Rows.Add("old_note", txt_old_note.Text.Trim());
                Dt_Param.Rows.Add("Frm_name", Frm_name);
                Dt_Param.Rows.Add("permission_id", Convert.ToInt16(Cmb_permission.SelectedIndex));
                Dt_Param.Rows.Add("EFrm_name", EFrm_name);

                DT_User_Term_Per.TableName = "DT_User_Term_Per";
                connection.SQLDS.Tables.Add(DT_User_Term_Per);

                User_Terminals_Permissions_Rpt ObjRpt = new User_Terminals_Permissions_Rpt();
                User_Terminals_Permissions_Rpt_eng ObjRptEng = new User_Terminals_Permissions_Rpt_eng();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;


                connection.SQLDS.Tables.Remove("DT_User_Term_Per");
            }

            catch { }
        }

    }
}
