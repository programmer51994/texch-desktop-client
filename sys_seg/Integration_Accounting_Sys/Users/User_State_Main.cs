﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class User_State_Main : Form
    {
        #region Defintions
        int Muser_Id = 0;
        bool UsrChange = false;
        byte USER_State;
        int Frm_id = 0;
    
        #endregion

        public User_State_Main()
        {
            InitializeComponent();
           
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #region Enghlish
            Grd_User.Columns["Column7"].DataPropertyName = connection.Lang_id == 1 ? "A_Address" : "E_Address";
            Grd_User.Columns["Column4"].DataPropertyName = connection.Lang_id == 1 ? "user_flag_Aname" : "user_flag_ename";
            Grd_User.Columns["Column3"].DataPropertyName = connection.Lang_id == 1 ? "cs_Aflag" : "cs_Eflag";
            #endregion
            Frm_id = 0;
        }
        public User_State_Main(int Form_id = 0)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #region Enghlish
            Grd_User.Columns["Column7"].DataPropertyName = connection.Lang_id == 1 ? "A_Address" : "E_Address";
            Grd_User.Columns["Column4"].DataPropertyName = connection.Lang_id == 1 ? "user_flag_Aname" : "user_flag_Ename";
            Grd_User.Columns["Column3"].DataPropertyName = connection.Lang_id == 1 ? "cs_Aflag" : "cs_Eflag";
            #endregion
            Frm_id = Form_id;
        }
        //----------------------------------------
        private void User_State_Main_Load(object sender, EventArgs e)
        {
            UsrChange = false;
            Grd_User.AutoGenerateColumns = false;
            TxtUser_Name_TextChanged(null, null);
            if (Frm_id == 1)
            {
                TxtUser_Name.Enabled = false; 
            }


        }
        //----------------------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {
            if (UsrChange && connection.SQLDS.Tables["ActUser_Tbl"].Rows.Count > 0)
            {
                LblRec.Text = connection.Records(User_Main._Bs_Users);
                DataRowView DRV = User_Main._Bs_Users.Current as DataRowView;
                DataRow DR = DRV.Row;

                USER_State = DR.Field<byte>("USER_State");
                Muser_Id = DR.Field<Int16>("User_Id");

                if (USER_State == 0)
                {
                    ActiveAdd.Enabled = true;
                    StopBtn.Enabled = true;
                    CloseBtn.Enabled = true;
                    DeactiveBtn.Enabled = false;
                }
                if (USER_State == 1)
                {
                    ActiveAdd.Enabled = false;
                    StopBtn.Enabled = true;
                    CloseBtn.Enabled = true;
                    DeactiveBtn.Enabled = true;
                }
                if (USER_State == 2)
                {
                    StopBtn.Enabled = false;
                    DeactiveBtn.Enabled = false;
                    CloseBtn.Enabled = true;
                    ActiveAdd.Enabled = true;
                }
                if (USER_State == 3)
                {
                    CloseBtn.Enabled = false;
                    ActiveAdd.Enabled = false;
                    StopBtn.Enabled = false;
                    DeactiveBtn.Enabled = false;
                }

            }
        }
        //----------------------------------------
        private void ActiveAdd_Click(object sender, EventArgs e)
        {
            object[] Sparams = { Muser_Id, 1 };
            connection.SqlExec("U_User_State", "ActUser_Tbl", Sparams);
            User_State_Main_Load(sender, e);
        }
        //----------------------------------------
        private void DeactiveBtn_Click(object sender, EventArgs e)
        {
            object[] Sparams = { Muser_Id, 0 };
            connection.SqlExec("U_User_State", "ActUser_Tbl", Sparams);
            User_State_Main_Load(sender, e);
        }
        //----------------------------------------
        private void StopBtn_Click(object sender, EventArgs e)
        {
            object[] Sparams = { Muser_Id, 2 };
            connection.SqlExec("U_User_State", "ActUser_Tbl", Sparams);
            User_State_Main_Load(sender, e);
        }
        //----------------------------------------
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            object[] Sparams = { Muser_Id, 3 };
            connection.SqlExec("U_User_State", "ActUser_Tbl", Sparams);
            User_State_Main_Load(sender, e);
        }
        //----------------------------------------
        private void User_State_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            UsrChange = false;
           
            string[] Tbl = { "ActUser_Tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }
        //-------------------------------------------------------------------------------------------------------
        private void User_State_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    ActiveAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    DeactiveBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    StopBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    CloseBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //-------------------------------------------------------------------------------------------------------
        private void TxtUser_Name_TextChanged(object sender, EventArgs e)
        {
            object[] Sparams = { Frm_id == 1 ? ((DataRowView)User_Main._Bs_Users.Current).Row["User_Name"].ToString() : TxtUser_Name.Text };

            User_Main._Bs_Users.DataSource = connection.SqlExec("Main_User", "ActUser_Tbl", Sparams);

            if (connection.SQLDS.Tables["ActUser_Tbl"].Rows.Count > 0)
            {
                Grd_User.DataSource = User_Main._Bs_Users;
                UsrChange = true;
                Grd_User_SelectionChanged(sender, e);

            }
            else
            {
                StopBtn.Enabled = false;
                DeactiveBtn.Enabled = false;
                CloseBtn.Enabled = false;
                ActiveAdd.Enabled = false;
            }
        }

        private void User_State_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}