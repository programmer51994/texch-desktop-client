﻿using System;
using System.Windows.Forms;
using System.Data;

namespace Integration_Accounting_Sys
{
    public partial class User_Account_Auth_Add_Upd : Form
    {
        #region Definitions
        string SQL_Txt = "";
        string SQL_Txt2 = "";
        int Frm_Id = 0;
        bool UserChange = false;
        int VC_Sw = 0;
        DataTable DT_User = new DataTable();
        DataTable DT_ACC = new DataTable();
        BindingSource _BSADD = new BindingSource();
        BindingSource _BSADD_2 = new BindingSource();
        #endregion

        public User_Account_Auth_Add_Upd(int Form_Id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Acc.AutoGenerateColumns = false;
            Grd_User.AutoGenerateColumns = false;
            Frm_Id = Form_Id;
        }
        //---------------------------------------------------------------------
        private void User_Account_Auth_Add_Upd_Load_1(object sender, EventArgs e)
        {
            if (connection.Lang_id != 1)
            {
                Grd_Acc.Columns["Column4"].DataPropertyName = "Acc_Ename";
            }
            UserChange = false;
            string SqlTxt = "Select VC_Sw, VC_Sw_Aname, VC_Sw_Ename from VS_SW_Tbl";
            CboAuth.DataSource = connection.SqlExec(SqlTxt, "cbo_VS_SW_Tbl");
            CboAuth.ValueMember = "VC_Sw";
            CboAuth.DisplayMember = connection.Lang_id == 1 ? "VC_Sw_Aname" : "VC_Sw_Ename";

            if (Frm_Id == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديل صلاحيات المستخدمين" : "User_Account_Auth_Update";
                DataRowView DRV1 = User_Account_Auth_Main._BSGRD.Current as DataRowView;
                DataRow DR1 = DRV1.Row;
                VC_Sw = DR1.Field<byte>("VC_Sw"); ;
                CboAuth.SelectedValue = VC_Sw;
                CboAuth.Enabled = false;
            }
            UserChange = true;
            CboAuth_SelectedIndexChanged(sender, e);

        }
        //-----------------------------------------------------------------
        private void CboAuth_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (UserChange)
            {
                int User_Id = 0;
                if (Frm_Id == 2)
                {
                    Grd_User.Enabled = false;
                    DataRowView DRV1 = User_Account_Auth_Main._BSGRD.Current as DataRowView;
                    DataRow DR1 = DRV1.Row;

                    User_Id = DR1.Field<Int16>("Auth_User_Id");
                    SQL_Txt = "SELECT 1 As Chk, User_Id, User_Name FROM USERS "
                         + " Where USER_State = 1 and USER_ID "
                         + " in ( select  Auth_User_Id from Users_Accounts_Auth where VC_Sw =" + CboAuth.SelectedValue
                         + " And Auth_User_Id = " + User_Id + " )"
                         + " And user_id <> 1 ";
                    //-----------------
                    SQL_Txt2 = " SELECT    1 as chk , A.Acc_Id, Acc_AName, Acc_EName "
                               + " FROM         ACCOUNT_TREE A,Users_Accounts_Auth B "
                               + " where A.Acc_Id = B.Acc_Id "
                               + " and Auth_User_Id = " + User_Id
                               + " And VC_Sw = " + VC_Sw
                               + " union "
                               + " SELECT    0 as chk , Acc_Id, Acc_AName, Acc_EName "
                               + " FROM         ACCOUNT_TREE  "
                               + " where Acc_Id not in (select  Acc_Id from Users_Accounts_Auth where Auth_User_Id =" + User_Id + " And VC_Sw = " + VC_Sw + " ) "
                               + " and RECORD_FLAG = 1"
                               + " Order by chk ";
                               //+ (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename");
                }
                else  //------------اضافة
                {
                    SQL_Txt = "SELECT 0 As Chk, User_Id, User_Name FROM USERS "
                              + " Where USER_State = 1 and USER_ID "
                              + " Not in ( select  Auth_User_Id from Users_Accounts_Auth where VC_Sw =" + CboAuth.SelectedValue + " )"
                               + " And user_id <> 1 ";
                    SQL_Txt2 = "Select 0 As Chk,Acc_Id ,Acc_AName,Acc_EName from Account_tree where record_flag = 1 "
                              + " Order by Chk ";
                              //+ (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename");
                }

                _BSADD.DataSource = connection.SqlExec(SQL_Txt, "Users_Auth");
                if (connection.SQLDS.Tables["Users_Auth"].Rows.Count > 0)
                {
                    Grd_User.DataSource = _BSADD;
                }


                _BSADD_2.DataSource = connection.SqlExec(SQL_Txt2, "ACC_Users_Auth");
                if (connection.SQLDS.Tables["ACC_Users_Auth"].Rows.Count > 0 && connection.SQLDS.Tables["Users_Auth"].Rows.Count > 0)
                {
                    Grd_Acc.DataSource = _BSADD_2;
                }
                else
                {
                    Grd_Acc.DataSource = new BindingSource();
                }
                //checkBox1_MouseClick( sender,  null);
                //checkBox2_MouseClick(sender, null);
            }
        }
        //----------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validations
            int Rec_No = connection.SQLDS.Tables["Users_Auth"].Select("Chk > 0").Length;
            if (Rec_No <= 0 && Frm_Id == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم تحدد مستخدمين للأضافة يجب تأشير المستخدمين اولا " : "There is no user to add,Please select the users", MyGeneral_Lib.LblCap);
                Grd_User.Focus();
                return;
            }
            int Acc_No = connection.SQLDS.Tables["ACC_Users_Auth"].Select("Chk > 0").Length;
            if (Acc_No <= 0 && Frm_Id == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم تحدد حسابات للأضافة يجب تأشير الحسابات  " : "There is no Account to add,Please select the Account", MyGeneral_Lib.LblCap);
                Grd_User.Focus();
                return;
            }
            #endregion

            this.Grd_Acc.CurrentCell = this.Grd_Acc[1, 0];

            try
            {
                DT_User = connection.SQLDS.Tables["Users_Auth"].DefaultView.ToTable(false, "User_Id", "Chk").Select("Chk > 0").CopyToDataTable();
            }
            catch
            {
                DT_User = connection.SQLDS.Tables["Users_Auth"].DefaultView.ToTable(false, "User_Id", "Chk");
                DT_User.Rows.Clear();
            }
            try
            {
                DT_ACC = connection.SQLDS.Tables["ACC_Users_Auth"].DefaultView.ToTable(false, "Acc_Id", "Chk").Select("Chk > 0").CopyToDataTable();
            }
            catch
            {
                DT_ACC = connection.SQLDS.Tables["ACC_Users_Auth"].DefaultView.ToTable(false, "Acc_Id", "Chk");
                DT_ACC.Rows.Clear();
            }
            connection.SQLCMD.Parameters.AddWithValue("@VC_SW ", CboAuth.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Tbl_User_Auth", DT_User);
            connection.SQLCMD.Parameters.AddWithValue("@Tbl_Acc_Auth", DT_ACC);
            connection.SQLCMD.Parameters.AddWithValue("@Form_Id", Frm_Id);
            connection.SQLCMD.Parameters.AddWithValue("@CUSER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_upd_User_Auth", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                CboAuth_SelectedIndexChanged(sender, e);
            }
            else
            {
                this.Close();
            }
        }
        //---------------------------------------------------        
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------------
        private void User_Companay_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserChange = false;

            string[] Str = { "Users_Auth", "ACC_Users_Auth", "cbo_VS_SW_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //------------------------------------------------------------------------
        private void User_Companay_Add_Upd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //------------------------------------------------------------------------
        private void User_Companay_Add_Upd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    AddBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //------------------------------------------------------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records();
        }

        private void checkBox1_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_User.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

            Grd_User.RefreshEdit();
            connection.SQLDS.Tables["Users_Auth"].AcceptChanges();
        }

        private void checkBox2_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Acc.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column3"]).Value = HCheckBox.Checked;
            Grd_Acc.RefreshEdit();
            connection.SQLDS.Tables["ACC_Users_Auth"].AcceptChanges();
        }
    }
}