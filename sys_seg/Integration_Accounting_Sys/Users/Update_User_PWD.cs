﻿using System;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Update_User_PWD : Form
    {
        public Update_User_PWD()
        {
            InitializeComponent();
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }
        //---------------------
        private void Update_User_PWD_Load(object sender, EventArgs e)
        {

        }
        //---------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region  Validations
            if (TxtUser_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل اسم المستخدم" : "Enter User Name", MyGeneral_Lib.LblCap);
                TxtUser_Name.Focus();
                return;
            }

            if (TxtOld_PWd.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور القديمة" : "Enter Old Password", MyGeneral_Lib.LblCap);
                TxtOld_PWd.Focus();
                return;
            }

            if (TxtNew_Pwd.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور" : "Enter Password", MyGeneral_Lib.LblCap);
                TxtNew_Pwd.Focus();
                return;
            }
            if (TxtNew_Pwd.Text.Length < 6)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ان لا تقل كلمة المرور عن ستة مراتب" : "The Password must be at least 6 Characters", MyGeneral_Lib.LblCap);
                TxtNew_Pwd.Focus();
                return;
            }
            if (TxtNew_Pwd.Text != TxtRe_Pwd.Text)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء التأكد من كلمة المرور" : "Please Confirm Password", MyGeneral_Lib.LblCap);
                TxtRe_Pwd.Focus();
                return;
            }
            #endregion

            System.Collections.ArrayList ItemList = new System.Collections.ArrayList();
            ItemList.Insert(0, connection.user_id);
            ItemList.Insert(1, TxtUser_Name.Text.Trim().ToUpper());
            ItemList.Insert(2, TxtNew_Pwd.Text.Trim().ToUpper());
            ItemList.Insert(3, TxtOld_PWd.Text.Trim());
            ItemList.Insert(4, connection.Lang_id);
            ItemList.Insert(5, connection.T_ID);
            ItemList.Insert(6, "");

            connection.scalar("Edit_User_PWD", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
        //---------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Update_User_PWD_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

      
    }
}
