﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class User_Add_Upd : Form
    {
        int Frm_Id = 0; 
        int MUser_Id = 0;
        string Per;
        //---------------------------------------------------------------------------------------------------------
        public User_Add_Upd(int Form_Id)
        {
            InitializeComponent();
            #region Arabic/English
            MyGeneral_Lib.Form_Orientation(this);
         connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #endregion
            
       
           
            Frm_Id = Form_Id;
            if (Form_Id == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديــل المستــخدم  " : "Edit Users ";
                Edit_Record();
            }
        }
        //--------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region  Validations
         
            if (TxtFull_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم الثلاثي" : "Enter Full Name", MyGeneral_Lib.LblCap);
                TxtFull_Name.Focus();
                return;
            }
            if(TxtUser_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل اسم المستخدم":"Enter User Name", MyGeneral_Lib.LblCap);
                TxtUser_Name.Focus();
                return;
            }
            if (TxtPhone.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الهاتف":"Enter Phone No.", MyGeneral_Lib.LblCap);
                TxtPhone.Focus();
                return;
            }
            if (TxtPwd.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور":"Enter Password", MyGeneral_Lib.LblCap);
                TxtPwd.Focus();
                return;
            }
            if (TxtPwd.Text.Length <6 )
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ان لا تقل كلمة المرور عن ستة مراتب" : "The Password must be at least 6 Characters", MyGeneral_Lib.LblCap);
                TxtPwd.Focus();
                return;
            }
            if (TxtPwd.Text != TxtRe_Pwd.Text)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء التأكد من كلمة المرور":"Please Confirm Password", MyGeneral_Lib.LblCap);
                TxtRe_Pwd.Focus();
                return;
            }
            #endregion

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, TxtFull_Name.Text.Trim().ToUpper());
            ItemList.Insert(1, TxtUser_Name.Text.Trim().ToUpper());
            ItemList.Insert(2, TxtPwd.Text.Trim());
            ItemList.Insert(3, TxtPhone.Text.Trim());
            ItemList.Insert(4, TxtA_Address.Text.Trim());
            ItemList.Insert(5, 2);
            ItemList.Insert(6, connection.user_id);   // For Insert
            ItemList.Insert(7, MUser_Id);             // For Edit
            ItemList.Insert(8, Frm_Id);               // Form_id
            ItemList.Insert(9, "");
            connection.scalar("Add_Upd_User", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            { 
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                TxtFull_Name.Text = "";
                TxtUser_Name.Text = "";
                TxtPwd.Text = "";
                TxtRe_Pwd.Text = "";
                TxtPhone.Text = "";
                TxtA_Address.Text = "";
                //TxtFull_Name.Focus();
               
            }
            else
                this.Close();
        }
        //--------------------------------------------------------------------
        private void Edit_Record()
        {
            DataRowView Drv = User_Main._Bs_Users.Current as DataRowView;
            DataRow Dr = Drv.Row;
            MUser_Id = Dr.Field<Int16>("User_Id");
            TxtFull_Name.Text = Dr.Field<string>("Full_Name");
            TxtUser_Name.Text = Dr.Field<string>("User_Name");
            TxtPwd.Text = "111111111111";
            TxtRe_Pwd.Text = "111111111111";
            TxtPhone.Text = Dr.Field<string>("phone_no");
            TxtA_Address.Text = Dr.Field<string>("A_Address");
            TxtFull_Name.Enabled = false;
            TxtUser_Name.Enabled = false;
            TxtPwd.Enabled = false;
            TxtRe_Pwd.Enabled = false;
            Per = Dr.Field<String>("cs_Aflag");
            //CboPermission_Id.Text = Per;

            
        }
        
        //--------------------------------------------------------------------
        private void User_Add_Upd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //--------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void User_Add_Upd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    AddBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;

            } 
        }

        private void User_Add_Upd_Load(object sender, EventArgs e)
        {

        }

       
      

    
      
    }
}
