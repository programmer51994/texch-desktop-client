﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class User_Main : Form
    {
        public static  BindingSource _Bs_Users = new BindingSource();
        public User_Main()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }
        //---------------------------------------------------------------------------------------------------------
        private void User_Main_Load(object sender, EventArgs e)
        {
            Grd_User.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
               Grd_User.Columns["Column7"].DataPropertyName = "E_Address";
                Grd_User.Columns["Column4"].DataPropertyName = "user_flag_Ename";
                Grd_User.Columns["Column3"].DataPropertyName = "cs_Eflag";
            }
            object [] Sparams = {TxtUser_Name.Text };
            _Bs_Users.DataSource = connection.SqlExec("Main_User", "User_Tbl", Sparams);
            if (connection.SQLDS.Tables["User_Tbl"].Rows.Count > 0)
            {
                Grd_User.DataSource = _Bs_Users;
                UpdBtn.Enabled = true;
            }
            else
            {
                UpdBtn.Enabled = false;
            }
            //if (connection.SQLBSGrd.List.Count <= 0)
            //    UpdBtn.Enabled = false;
            //else
            //    UpdBtn.Enabled = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Users);
        }
        //---------------------------------------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            User_Add_Upd AddFrm = new User_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
            User_Add_Upd AddFrm = new User_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            User_Main_Load( sender,  e);
        }
        //---------------------------------------------------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            TxtUser_Name.Text = "";
            User_Main_Load(sender, e);
        }
        //---------------------------------------------------------------------------------------------------------
        private void User_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //----------------------------------------
        private void User_Main_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void User_Main_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "User_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);

            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            User_State_Main AddFrm = new User_State_Main(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Main_Load(sender, e);
            this.Visible = true;
        }

        private void User_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}
