﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class User_Account_Auth_Main : Form
    {
        DataTable DT = new DataTable();
        bool Change = false;
        int page_no = 0;
        public static BindingSource _BSGRD = new BindingSource();
 
        public User_Account_Auth_Main()
        {
            InitializeComponent();
          
            CboAuth.SelectedIndex = 0;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
             Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_User.AutoGenerateColumns = false;
        }
        //-------------------------------------------
        private void User_Companies_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id != 1)
            {
                Grd_User.Columns["Column3"].DataPropertyName = "Acc_Ename";
                Grd_User.Columns["Column4"].DataPropertyName = "VC_Sw_Ename";
            }

            string SqlTxt = "Select VC_Sw, VC_Sw_Aname, VC_Sw_Ename from VS_SW_Tbl"
                            + " Union Select 0 AS VC_Sw,'جميع الصلاحيات' AS VC_Sw_Aname, 'All Authentication' As VC_Sw_Ename" ;
            CboAuth.DataSource = connection.SqlExec(SqlTxt, "cbo_VS_SW_Tbl");
            CboAuth.ValueMember = "VC_Sw";
            CboAuth.DisplayMember = connection.Lang_id == 1 ? "VC_Sw_Aname" : "VC_Sw_Ename";

            Change = false;
            CboAuth.SelectedIndex = 0;
            Change = true;
            CboAuth_SelectedIndexChanged(sender, e);
        }
            
        //-------------------------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_BSGRD);
        }
        //-------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            User_Account_Auth_Add_Upd AddFrm = new User_Account_Auth_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Companies_Load(sender, e);
            this.Visible = true;
        }
        //-------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
            DataRowView DRV = _BSGRD.Current as DataRowView;
            DataRow DR = DRV.Row;
            User_Account_Auth_Add_Upd AddFrm = new User_Account_Auth_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            User_Companies_Load(sender, e);
            this.Visible = true;
        }
        //-------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            CboAuth_SelectedIndexChanged(sender, e);
        }
        //-------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Change = false;
            CboAuth.SelectedIndex = 0;
            Change = true;
            TxtUser_Name.Text = "";
            TxtAcc_name.Text = "";
            CboAuth_SelectedIndexChanged(sender, e);
         
        }
        //-------------------------------------------
        private void User_Companies_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;

            string[] Str = { "User_Account_Auth", "cbo_VS_SW_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables[Tbl].Clear();
            }
        }
        //-------------------------------------------
        private void User_Companies_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //-------------------------------------------
        private void User_Companies_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }

        private void CboAuth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Change)
            {
            string SqlTxt = "Main_User_Account_Auth '" + TxtAcc_name.Text
                                     + "','" + TxtUser_Name.Text + "'," + CboAuth.SelectedValue;
            connection.SqlExec(SqlTxt, "User_Account_Auth");
                if (connection.SQLDS.Tables["User_Account_Auth"].Rows.Count <= 0)
                    UpdBtn.Enabled = false;
                else
                {
                    UpdBtn.Enabled = true;
                }
            #region Grouping
            string strCurrentValue = string.Empty;
            string strPreviousValue = string.Empty;
                DT = connection.SQLDS.Tables["User_Account_Auth"];
            for (int m = 0; m < DT.Rows.Count; m++)
            {
                object cellValue = DT.Rows[m]["User_Name"]; // Catch the value form current column
                strCurrentValue = cellValue.ToString().Trim(); // Assign the above value to 'strCurrentValue'

                if (strCurrentValue != strPreviousValue) // Now compare the current value with previous value
                {
                    DT.Rows[m]["User_Name"] = strCurrentValue;	// If current value is not equal to previous value 
                    // the column will display current value
                }
                else
                {
                    DT.Rows[m]["User_Name"] = string.Empty;	// If current value is equal to previous value 
                    // the column will be empty	
                }
                strPreviousValue = strCurrentValue;	        // Assign current value to previous value
            }
            strCurrentValue = string.Empty;					// Reset Current and Previous Value
            strPreviousValue = string.Empty;
            _BSGRD.DataSource = DT;
            Grd_User.DataSource = _BSGRD;
            #endregion
            if (DT.Rows.Count <= 0)
                    UpdBtn.Enabled = false;
                else
                    UpdBtn.Enabled = true;
            }
        }

        private void User_Account_Auth_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
           page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}