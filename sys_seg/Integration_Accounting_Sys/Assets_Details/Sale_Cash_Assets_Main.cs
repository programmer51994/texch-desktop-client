﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Sale_Cash_Assets_Main : Form
    {
        #region Definition
        bool GrdChange = false;
        DataTable Assets_Tbl;
        BindingSource Bs_Sal_asstes_GRD = new BindingSource();
        #endregion
        //-------------------------------------------------------
        public Sale_Cash_Assets_Main()
        {
            InitializeComponent();
           
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Vo_Assets.AutoGenerateColumns = false;
            Grd_Vo_Details.AutoGenerateColumns = false;
        }
        //-------------------------------------------------------
        private void Sale_Cash_Assets_Main_Load(object sender, EventArgs e)
        {
            Getdata(39);
        }
        //-------------------------------------------------------
        private void Getdata(int Oper_Id)
        {
            GrdChange = false;
            object[] Sparams = {TxtIn_Rec_Date.Text , connection.T_ID, connection.user_id, 
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo, Gen_Search.Catg_Id, 
                                   Oper_Id, 0, 0, 0 };
            connection.SqlExec("Main_Vouchers", "Asset_VouCher_Tbl", Sparams);
            //==============================
            var query = (from row in connection.SQLDS.Tables["Asset_VouCher_Tbl"].AsEnumerable()
                         select new
                          {
                              Vo_No = row["Vo_no"],
                              Create_date = row["alter_date"],
                              Nrec_Date = row["Nrec_date"],
                              C_date1 = row["C_Date"]
                          }).Distinct();
            Assets_Tbl = CustomControls.IEnumerableToDataTable(query);
            Bs_Sal_asstes_GRD.DataSource = Assets_Tbl;
            Grd_Vo_Assets.DataSource = Bs_Sal_asstes_GRD;
            if (Assets_Tbl.Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Vo_Assets_SelectionChanged(null, null);
            }
            else Grd_Vo_Details.DataSource = new DataTable();
        }
        //-------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Sale_Cash_Assets_Add  AddFrm = new Sale_Cash_Assets_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Sale_Cash_Assets_Main_Load(sender, e);
            this.Visible = true;
        }
        //-------------------------------------------------------
        private void Grd_Vo_Assets_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                LblRec.Text = connection.Records(Bs_Sal_asstes_GRD);
                var queryx = from Rec in connection.SQLDS.Tables["Asset_VouCher_Tbl"].AsEnumerable()
                             where
                             Rec.Field<int>("VO_NO") == Convert.ToInt32(((DataRowView)Bs_Sal_asstes_GRD.Current).Row["VO_NO"])
                             && Rec.Field<int>("Nrec_Date") == Convert.ToInt32(((DataRowView)Bs_Sal_asstes_GRD.Current).Row["Nrec_date"])
                             orderby Rec.Field<int>("Rec_id")
                             select new
                             {
                                 DFor_Amount = Rec.Field<decimal>("For_amount_asset") >= 0 ? Rec.Field<decimal>("For_amount_asset") : 0,
                                 CFor_Amount = Rec.Field<decimal>("For_amount_asset") < 0 ? Math.Abs(Rec.Field<decimal>("For_amount_asset")) : 0,
                                 DLoc_Amount = Rec.Field<decimal>("Loc_amount_asset") >= 0 ? Rec.Field<decimal>("Loc_amount_asset") : 0,
                                 CLoc_Amount = Rec.Field<decimal>("Loc_amount_asset") < 0 ? Math.Abs(Rec.Field<decimal>("Loc_amount_asset")) : 0,
                                 Acc_Id = Rec.Field<int>("Acc_Id"),
                                 Acc_Name = Rec.Field<string>(connection.Lang_id == 1 ? "Acc_AName" : "Acc_Ename"),
                                 Cur_name = Rec.Field<string>(connection.Lang_id == 1 ? "cur_aname" : "Cur_ename"),
                                 Cust_Name = Rec.Field<string>(connection.Lang_id == 1 ? "ACust_Name" : "Ecust_name"),
                                 Notes = Rec.Field<string>("notes"),
                                 Exch_Price = Rec.Field<decimal>("Exch_price")

                             };
                DataTable Vo_Assets_Tbl = CustomControls.IEnumerableToDataTable(queryx);
                Grd_Vo_Details.DataSource = Vo_Assets_Tbl;

            }
        }
        //-------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(13);
            SearchFrm.ShowDialog(this);
            Getdata(39);
        }
        //-------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Getdata(39);
        }
        //-------------------------------------------------------
        private void Sale_Cash_Assets_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
            
            string[] Used_Tbl = { "Asset_VouCher_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Sale_Cash_Assets_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
            
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            int Vo_No = Convert.ToInt16(((DataRowView)Bs_Sal_asstes_GRD.Current).Row["Vo_No"]);
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + ",39," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            string SqlTxt1 = "Exec Main_Report_Assets_Details " + Vo_No + "," + TxtIn_Rec_Date.Text + ",39," + connection.T_ID;

            connection.SqlExec(SqlTxt, "Asset_Voucher");
            connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
            Main_Assets_Rpt ObjRptEng = new Main_Assets_Rpt();



            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", 39);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Main");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

            DataRow Dr;
            int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 39);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Asset_Voucher");
            connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
        }

        private void Sale_Cash_Assets_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Sale_Cash_Assets_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            GrdChange = false;
        }

        
     
    }
}