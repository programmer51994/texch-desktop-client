﻿namespace Integration_Accounting_Sys
{
    partial class add_upd_Section
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExtBtn = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_EName = new System.Windows.Forms.TextBox();
            this.Txt_AName = new System.Windows.Forms.TextBox();
            this.Cbo_Id = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.Internet_connect = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(207, 109);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ExtBtn.Size = new System.Drawing.Size(87, 25);
            this.ExtBtn.TabIndex = 4;
            this.ExtBtn.Text = "انهاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(121, 109);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.BtnOk.Size = new System.Drawing.Size(87, 25);
            this.BtnOk.TabIndex = 3;
            this.BtnOk.Text = "موافق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.Btn_Ok_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(1, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "الاســـــم الاجـــنـــبــي:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(1, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "الاســـــم الــعـــربــــي:";
            // 
            // Txt_EName
            // 
            this.Txt_EName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EName.Location = new System.Drawing.Point(120, 72);
            this.Txt_EName.MinimumSize = new System.Drawing.Size(164, 24);
            this.Txt_EName.Name = "Txt_EName";
            this.Txt_EName.Size = new System.Drawing.Size(279, 23);
            this.Txt_EName.TabIndex = 2;
            // 
            // Txt_AName
            // 
            this.Txt_AName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AName.Location = new System.Drawing.Point(120, 41);
            this.Txt_AName.MinimumSize = new System.Drawing.Size(164, 24);
            this.Txt_AName.Name = "Txt_AName";
            this.Txt_AName.Size = new System.Drawing.Size(279, 23);
            this.Txt_AName.TabIndex = 1;
            this.Txt_AName.Leave += new System.EventHandler(this.Txt_AName_Leave);
            // 
            // Cbo_Id
            // 
            this.Cbo_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Id.FormattingEnabled = true;
            this.Cbo_Id.Location = new System.Drawing.Point(119, 10);
            this.Cbo_Id.Name = "Cbo_Id";
            this.Cbo_Id.Size = new System.Drawing.Size(280, 24);
            this.Cbo_Id.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(1, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "اســــــــم الثــــانـــــوي:";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-19, 103);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(437, 2);
            this.flowLayoutPanel3.TabIndex = 233;
            // 
            // Internet_connect
            // 
            this.Internet_connect.AutoSize = true;
            this.Internet_connect.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Internet_connect.ForeColor = System.Drawing.Color.Red;
            this.Internet_connect.Location = new System.Drawing.Point(399, 69);
            this.Internet_connect.Name = "Internet_connect";
            this.Internet_connect.Size = new System.Drawing.Size(17, 22);
            this.Internet_connect.TabIndex = 234;
            this.Internet_connect.Text = "*";
            // 
            // add_upd_Section
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 141);
            this.Controls.Add(this.Internet_connect);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Cbo_Id);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_EName);
            this.Controls.Add(this.Txt_AName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "add_upd_Section";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "204";
            this.Text = "add_upd_Section";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Frm_Add_Upd_Cit_FormClosed);
            this.Load += new System.EventHandler(this.Frm_Add_Upd_Cit_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.add_upd_Cit_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.add_upd_Cit_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_EName;
        private System.Windows.Forms.TextBox Txt_AName;
        private System.Windows.Forms.ComboBox Cbo_Id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label Internet_connect;
    }
}