﻿namespace Integration_Accounting_Sys
{
    partial class Daily_Sale_Assets_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCurr_Name = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Cust = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_Acc = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Grd_Acc_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Grd_Cur_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Month = new System.Windows.Forms.TextBox();
            this.Txt_Year = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Txtdesicription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.Txt_FullSymbol = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_Month_Id = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.GrdAssest = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_Notes = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtTotal_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtSpecific_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtAsset_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Exch_Price = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAssest)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(484, 9);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 561;
            this.label2.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(584, 5);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(175, 23);
            this.Txt_Loc_Cur.TabIndex = 2;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(309, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(173, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(2, 5);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(216, 23);
            this.TxtTerm_Name.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(764, 9);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 553;
            this.label3.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(819, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(99, 23);
            this.TxtIn_Rec_Date.TabIndex = 3;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-4, 31);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(942, 1);
            this.flowLayoutPanel9.TabIndex = 555;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(229, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 552;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtCurr_Name
            // 
            this.TxtCurr_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurr_Name.Location = new System.Drawing.Point(389, 191);
            this.TxtCurr_Name.Name = "TxtCurr_Name";
            this.TxtCurr_Name.Size = new System.Drawing.Size(210, 23);
            this.TxtCurr_Name.TabIndex = 14;
            this.TxtCurr_Name.TextChanged += new System.EventHandler(this.TxtCurr_Name_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(321, 194);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(71, 14);
            this.label13.TabIndex = 572;
            this.label13.Text = "العملــــــــة:";
            // 
            // Txt_Cust
            // 
            this.Txt_Cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cust.Location = new System.Drawing.Point(675, 191);
            this.Txt_Cust.Name = "Txt_Cust";
            this.Txt_Cust.Size = new System.Drawing.Size(243, 23);
            this.Txt_Cust.TabIndex = 16;
            this.Txt_Cust.TextChanged += new System.EventHandler(this.Txt_Cust_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(611, 194);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(62, 14);
            this.label14.TabIndex = 567;
            this.label14.Text = "الــثـانــوي";
            // 
            // Txt_Acc
            // 
            this.Txt_Acc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Acc.Location = new System.Drawing.Point(83, 191);
            this.Txt_Acc.Name = "Txt_Acc";
            this.Txt_Acc.Size = new System.Drawing.Size(222, 23);
            this.Txt_Acc.TabIndex = 12;
            this.Txt_Acc.TextChanged += new System.EventHandler(this.Txt_Acc_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(14, 194);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(75, 14);
            this.label12.TabIndex = 562;
            this.label12.Text = "الحســــــاب:";
            // 
            // Grd_Acc_Id
            // 
            this.Grd_Acc_Id.AllowUserToAddRows = false;
            this.Grd_Acc_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Acc_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Acc_Id.ColumnHeadersHeight = 40;
            this.Grd_Acc_Id.ColumnHeadersVisible = false;
            this.Grd_Acc_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column10});
            this.Grd_Acc_Id.Location = new System.Drawing.Point(12, 214);
            this.Grd_Acc_Id.Name = "Grd_Acc_Id";
            this.Grd_Acc_Id.ReadOnly = true;
            this.Grd_Acc_Id.RowHeadersVisible = false;
            this.Grd_Acc_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Acc_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Id.Size = new System.Drawing.Size(293, 68);
            this.Grd_Acc_Id.TabIndex = 13;
            this.Grd_Acc_Id.SelectionChanged += new System.EventHandler(this.Grd_Acc_Id_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 75;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Acc_Aname";
            this.Column10.HeaderText = "اسم الحساب";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 273;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(13, 290);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(145, 14);
            this.label15.TabIndex = 579;
            this.label15.Text = "القيمة الاسميـة للقطعة:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(321, 290);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(81, 14);
            this.label16.TabIndex = 578;
            this.label16.Text = "سعر التعادل:";
            // 
            // Grd_Cur_Id
            // 
            this.Grd_Cur_Id.AllowUserToAddRows = false;
            this.Grd_Cur_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cur_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cur_Id.ColumnHeadersHeight = 40;
            this.Grd_Cur_Id.ColumnHeadersVisible = false;
            this.Grd_Cur_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.Column11});
            this.Grd_Cur_Id.Location = new System.Drawing.Point(324, 214);
            this.Grd_Cur_Id.Name = "Grd_Cur_Id";
            this.Grd_Cur_Id.ReadOnly = true;
            this.Grd_Cur_Id.RowHeadersVisible = false;
            this.Grd_Cur_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Cur_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur_Id.Size = new System.Drawing.Size(275, 68);
            this.Grd_Cur_Id.TabIndex = 15;
            this.Grd_Cur_Id.SelectionChanged += new System.EventHandler(this.Grd_Cur_Id_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Cur_Id";
            this.dataGridViewTextBoxColumn3.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 128;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Cur_Aname";
            this.Column11.HeaderText = "اسم الحساب";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 210;
            // 
            // Grd_Cust_Id
            // 
            this.Grd_Cust_Id.AllowUserToAddRows = false;
            this.Grd_Cust_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Cust_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Cust_Id.ColumnHeadersHeight = 40;
            this.Grd_Cust_Id.ColumnHeadersVisible = false;
            this.Grd_Cust_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.Column12});
            this.Grd_Cust_Id.Location = new System.Drawing.Point(614, 214);
            this.Grd_Cust_Id.Name = "Grd_Cust_Id";
            this.Grd_Cust_Id.ReadOnly = true;
            this.Grd_Cust_Id.RowHeadersVisible = false;
            this.Grd_Cust_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_Id.Size = new System.Drawing.Size(304, 68);
            this.Grd_Cust_Id.TabIndex = 17;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Cust_Id";
            this.dataGridViewTextBoxColumn5.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 128;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "ACust_Name";
            this.Column12.HeaderText = "اسم الحساب";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 210;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-6, 499);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(939, 1);
            this.flowLayoutPanel1.TabIndex = 586;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(463, 503);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(99, 28);
            this.ExtBtn.TabIndex = 24;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(364, 503);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(99, 28);
            this.BtnOk.TabIndex = 23;
            this.BtnOk.Text = "مـوافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(927, 289);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(90, 28);
            this.BtnDel.TabIndex = 588;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(830, 313);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(90, 25);
            this.BtnAdd.TabIndex = 21;
            this.BtnAdd.Text = "اضــافــة";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 536);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(926, 22);
            this.statusStrip1.TabIndex = 596;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1021, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1, 469);
            this.flowLayoutPanel2.TabIndex = 598;
            // 
            // Txt_Month
            // 
            this.Txt_Month.BackColor = System.Drawing.Color.White;
            this.Txt_Month.Enabled = false;
            this.Txt_Month.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Month.Location = new System.Drawing.Point(164, 112);
            this.Txt_Month.Name = "Txt_Month";
            this.Txt_Month.Size = new System.Drawing.Size(147, 23);
            this.Txt_Month.TabIndex = 8;
            // 
            // Txt_Year
            // 
            this.Txt_Year.BackColor = System.Drawing.Color.White;
            this.Txt_Year.Enabled = false;
            this.Txt_Year.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Year.Location = new System.Drawing.Point(452, 112);
            this.Txt_Year.Name = "Txt_Year";
            this.Txt_Year.Size = new System.Drawing.Size(168, 23);
            this.Txt_Year.TabIndex = 9;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(8, 165);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(911, 1);
            this.flowLayoutPanel3.TabIndex = 724;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(11, 144);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(119, 14);
            this.label11.TabIndex = 739;
            this.label11.Text = "إسم الموجــــــــود : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(323, 116);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(118, 14);
            this.label10.TabIndex = 738;
            this.label10.Text = "اخر سنة للإندثــــار :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(11, 116);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(115, 14);
            this.label9.TabIndex = 737;
            this.label9.Text = "اخــر شهـر للإندثار :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(626, 115);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(119, 14);
            this.label8.TabIndex = 736;
            this.label8.Text = "القيمــة الدفتريـــة : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(323, 87);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(124, 14);
            this.label7.TabIndex = 735;
            this.label7.Text = "المخصص المتراكـم: ";
            // 
            // Txtdesicription
            // 
            this.Txtdesicription.BackColor = System.Drawing.Color.White;
            this.Txtdesicription.Enabled = false;
            this.Txtdesicription.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtdesicription.Location = new System.Drawing.Point(141, 140);
            this.Txtdesicription.Name = "Txtdesicription";
            this.Txtdesicription.Size = new System.Drawing.Size(772, 23);
            this.Txtdesicription.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(11, 87);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(117, 14);
            this.label6.TabIndex = 729;
            this.label6.Text = "القيمــة الأســمية  :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(9, 65);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(137, 14);
            this.label5.TabIndex = 728;
            this.label5.Text = "معلومــات الموجــود.....";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(9, 77);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(910, 1);
            this.flowLayoutPanel4.TabIndex = 723;
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.SearchBtn.ForeColor = System.Drawing.Color.Navy;
            this.SearchBtn.Location = new System.Drawing.Point(823, 41);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(98, 26);
            this.SearchBtn.TabIndex = 5;
            this.SearchBtn.Text = "بحـــث";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // Txt_FullSymbol
            // 
            this.Txt_FullSymbol.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_FullSymbol.Location = new System.Drawing.Point(141, 41);
            this.Txt_FullSymbol.MaxLength = 15;
            this.Txt_FullSymbol.Name = "Txt_FullSymbol";
            this.Txt_FullSymbol.Size = new System.Drawing.Size(170, 23);
            this.Txt_FullSymbol.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(11, 45);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(121, 14);
            this.label4.TabIndex = 725;
            this.label4.Text = "الرمــز الكامـــــــــــل :";
            // 
            // Txt_Month_Id
            // 
            this.Txt_Month_Id.BackColor = System.Drawing.Color.White;
            this.Txt_Month_Id.Enabled = false;
            this.Txt_Month_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Month_Id.Location = new System.Drawing.Point(141, 112);
            this.Txt_Month_Id.Name = "Txt_Month_Id";
            this.Txt_Month_Id.Size = new System.Drawing.Size(25, 23);
            this.Txt_Month_Id.TabIndex = 747;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Maroon;
            this.label18.Location = new System.Drawing.Point(7, 340);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(96, 14);
            this.label18.TabIndex = 751;
            this.label18.Text = "التفاصيـــــل .....";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(4, 351);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(999, 1);
            this.flowLayoutPanel5.TabIndex = 748;
            // 
            // GrdAssest
            // 
            this.GrdAssest.AllowUserToAddRows = false;
            this.GrdAssest.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAssest.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAssest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.GrdAssest.ColumnHeadersHeight = 30;
            this.GrdAssest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column13,
            this.Column14});
            this.GrdAssest.Location = new System.Drawing.Point(8, 357);
            this.GrdAssest.Name = "GrdAssest";
            this.GrdAssest.ReadOnly = true;
            this.GrdAssest.RowHeadersWidth = 15;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAssest.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.GrdAssest.Size = new System.Drawing.Size(914, 136);
            this.GrdAssest.TabIndex = 22;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column1.DataPropertyName = "Acc_id";
            this.Column1.HeaderText = "رقــم الحساب";
            this.Column1.MinimumWidth = 100;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.DataPropertyName = "Acc_Name";
            this.Column2.HeaderText = "الحســاب";
            this.Column2.MinimumWidth = 150;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.DataPropertyName = "Cust_id";
            this.Column3.HeaderText = "رمــز الثــانوي";
            this.Column3.MinimumWidth = 110;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 110;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column4.DataPropertyName = "Cust_name";
            this.Column4.HeaderText = "الثانـــوي";
            this.Column4.MinimumWidth = 150;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Cur_name";
            this.Column5.HeaderText = "العملة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column6.DataPropertyName = "For_amount";
            dataGridViewCellStyle12.Format = "N3";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column6.HeaderText = "سعر البيـــع";
            this.Column6.MinimumWidth = 100;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column7.DataPropertyName = "Full_Symbol";
            this.Column7.HeaderText = "الرمــز الكامــل";
            this.Column7.MinimumWidth = 100;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 104;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column8.DataPropertyName = "Asset_amount";
            dataGridViewCellStyle13.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column8.HeaderText = "القيـمة الاسمية";
            this.Column8.MinimumWidth = 100;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column9.DataPropertyName = "Specific_Amount";
            dataGridViewCellStyle14.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle14;
            this.Column9.HeaderText = "المخصص المتراكـــم";
            this.Column9.MinimumWidth = 100;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 128;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column13.DataPropertyName = "description1";
            this.Column13.HeaderText = "اسم الموجــــود";
            this.Column13.MinimumWidth = 200;
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 200;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column14.DataPropertyName = "Notes";
            this.Column14.HeaderText = "الملاحظـــات";
            this.Column14.MinimumWidth = 150;
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 150;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(13, 318);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(114, 14);
            this.label17.TabIndex = 753;
            this.label17.Text = "الملاحظــــــــــــات : ";
            // 
            // Txt_Notes
            // 
            this.Txt_Notes.BackColor = System.Drawing.Color.White;
            this.Txt_Notes.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Notes.Location = new System.Drawing.Point(125, 314);
            this.Txt_Notes.MaxLength = 85;
            this.Txt_Notes.Name = "Txt_Notes";
            this.Txt_Notes.Size = new System.Drawing.Size(699, 23);
            this.Txt_Notes.TabIndex = 20;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(8, 77);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1, 89);
            this.flowLayoutPanel8.TabIndex = 754;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(919, 77);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1, 89);
            this.flowLayoutPanel10.TabIndex = 755;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(9, 171);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(126, 14);
            this.label19.TabIndex = 756;
            this.label19.Text = "الحساب المدين .......";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(12, 183);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(910, 1);
            this.flowLayoutPanel7.TabIndex = 724;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(623, 87);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 757;
            this.label20.Text = "(دينار عراقي)";
            // 
            // TxtTotal_Amount
            // 
            this.TxtTotal_Amount.BackColor = System.Drawing.Color.White;
            this.TxtTotal_Amount.Enabled = false;
            this.TxtTotal_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal_Amount.Location = new System.Drawing.Point(749, 111);
            this.TxtTotal_Amount.Name = "TxtTotal_Amount";
            this.TxtTotal_Amount.NumberDecimalDigits = 3;
            this.TxtTotal_Amount.NumberDecimalSeparator = ".";
            this.TxtTotal_Amount.NumberGroupSeparator = ",";
            this.TxtTotal_Amount.Size = new System.Drawing.Size(160, 23);
            this.TxtTotal_Amount.TabIndex = 10;
            this.TxtTotal_Amount.Text = "0.000";
            // 
            // TxtSpecific_Amount
            // 
            this.TxtSpecific_Amount.BackColor = System.Drawing.Color.White;
            this.TxtSpecific_Amount.Enabled = false;
            this.TxtSpecific_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpecific_Amount.Location = new System.Drawing.Point(452, 83);
            this.TxtSpecific_Amount.Name = "TxtSpecific_Amount";
            this.TxtSpecific_Amount.NumberDecimalDigits = 3;
            this.TxtSpecific_Amount.NumberDecimalSeparator = ".";
            this.TxtSpecific_Amount.NumberGroupSeparator = ",";
            this.TxtSpecific_Amount.Size = new System.Drawing.Size(168, 23);
            this.TxtSpecific_Amount.TabIndex = 7;
            this.TxtSpecific_Amount.Text = "0.000";
            // 
            // TxtAsset_Amount
            // 
            this.TxtAsset_Amount.BackColor = System.Drawing.Color.White;
            this.TxtAsset_Amount.Enabled = false;
            this.TxtAsset_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsset_Amount.Location = new System.Drawing.Point(141, 83);
            this.TxtAsset_Amount.Name = "TxtAsset_Amount";
            this.TxtAsset_Amount.NumberDecimalDigits = 3;
            this.TxtAsset_Amount.NumberDecimalSeparator = ".";
            this.TxtAsset_Amount.NumberGroupSeparator = ",";
            this.TxtAsset_Amount.Size = new System.Drawing.Size(170, 23);
            this.TxtAsset_Amount.TabIndex = 6;
            this.TxtAsset_Amount.Text = "0.000";
            // 
            // Txt_Amount
            // 
            this.Txt_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Amount.Location = new System.Drawing.Point(156, 287);
            this.Txt_Amount.Name = "Txt_Amount";
            this.Txt_Amount.NumberDecimalDigits = 3;
            this.Txt_Amount.NumberDecimalSeparator = ".";
            this.Txt_Amount.NumberGroupSeparator = ",";
            this.Txt_Amount.Size = new System.Drawing.Size(149, 22);
            this.Txt_Amount.TabIndex = 18;
            this.Txt_Amount.Text = "0.000";
            // 
            // Txt_Exch_Price
            // 
            this.Txt_Exch_Price.BackColor = System.Drawing.Color.White;
            this.Txt_Exch_Price.Enabled = false;
            this.Txt_Exch_Price.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Exch_Price.Location = new System.Drawing.Point(408, 287);
            this.Txt_Exch_Price.Name = "Txt_Exch_Price";
            this.Txt_Exch_Price.NumberDecimalDigits = 3;
            this.Txt_Exch_Price.NumberDecimalSeparator = ".";
            this.Txt_Exch_Price.NumberGroupSeparator = ",";
            this.Txt_Exch_Price.Size = new System.Drawing.Size(191, 22);
            this.Txt_Exch_Price.TabIndex = 19;
            this.Txt_Exch_Price.Text = "0.000";
            // 
            // Daily_Sale_Assets_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 558);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_Notes);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.GrdAssest);
            this.Controls.Add(this.Txt_Month_Id);
            this.Controls.Add(this.Txt_Month);
            this.Controls.Add(this.Txt_Year);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txtdesicription);
            this.Controls.Add(this.TxtTotal_Amount);
            this.Controls.Add(this.TxtSpecific_Amount);
            this.Controls.Add(this.TxtAsset_Amount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.Txt_FullSymbol);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Grd_Cust_Id);
            this.Controls.Add(this.Grd_Cur_Id);
            this.Controls.Add(this.Txt_Amount);
            this.Controls.Add(this.Txt_Exch_Price);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Grd_Acc_Id);
            this.Controls.Add(this.TxtCurr_Name);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_Cust);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_Acc);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Daily_Sale_Assets_Add";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "425";
            this.Text = "Daily_Sale_Assets_Add";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Buy_cash_assest_Add_FormClosed);
            this.Load += new System.EventHandler(this.Buy_cash_assest_Add_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAssest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtCurr_Name;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_Cust;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_Acc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView Grd_Acc_Id;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Exch_Price;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView Grd_Cur_Id;
        private System.Windows.Forms.DataGridView Grd_Cust_Id;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.TextBox Txt_Month;
        private System.Windows.Forms.TextBox Txt_Year;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txtdesicription;
        private System.Windows.Forms.Sample.DecimalTextBox TxtTotal_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtSpecific_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtAsset_Amount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.TextBox Txt_FullSymbol;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_Month_Id;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.DataGridView GrdAssest;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_Notes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.Label label20;
    }
}