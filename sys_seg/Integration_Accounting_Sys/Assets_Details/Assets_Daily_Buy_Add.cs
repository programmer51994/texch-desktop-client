﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Assets_Daily_Buy_Add : Form
    {
        #region MyRegion
        bool Change = false;
        bool ACCChange = false;
        bool CurChange = false;
        bool DCChange = false;
        string SqlTxt = "";
        string Ord_Strt = "";
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CurId = new BindingSource();
        BindingSource Bs_CustId = new BindingSource();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        DataTable Asset_Voucher = new DataTable();
        DataTable Dt = new DataTable();
        int VO_NO = 0;
        string DB_Acc_Cur_Cust = "";
        string DB_EAcc_Cur_Cust = "";
        string @format = "dd/MM/yyyy";
        string @format_null = "0000/00/00";
        decimal Sum_Specific = 0;
        #endregion

        public Assets_Daily_Buy_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            GrdAssest.AutoGenerateColumns = false;
            Create_Table();
            if (connection.Lang_id != 1)
            {
                Grd_Acc_Id.Columns["column10"].DataPropertyName = "Acc_ename";
                Grd_Cur_Id.Columns["Column11"].DataPropertyName = "cur_ename";
                Grd_Cust_Id.Columns["Column12"].DataPropertyName = "Ecust_name";

            }
        }
        //------------------------------------------------------------------------
        private void Buy_cash_assest_Add_Load(object sender, EventArgs e)
        {
            TxtBuy_Date_ValueChanged(null, null);
            DCChange = false;
            SqlTxt = " SELECT DC_Id, DC_Aname, DC_Ename FROM Debt_Credit where DC_Id <> 0 Order By DC_ID Desc";
            Cbo_Cust_Type.DataSource = connection.SqlExec(SqlTxt, "DC_Id_tab");
            Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
            Cbo_Cust_Type.ValueMember = "DC_Id";
            {
                DCChange = true;
                Cbo_Cust_Type_SelectedIndexChanged(null, null);
            }
            SqlTxt = "Select sec_aname,sec_ename,sec_id,T_Sec_Id from sections where T_ID = " + connection.T_ID;
            CboSec.DataSource = connection.SqlExec(SqlTxt, "Sec_Tbl");
            if (connection.SQLDS.Tables["Sec_Tbl"].Rows.Count > 0)
            {
                CboSec.ValueMember = "Sec_id";
                CboSec.DisplayMember = connection.Lang_id == 1 ? "Sec_Aname" : "Sec_Ename";
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب تعريف قطاعات اولا " : "Please Add Section First", MyGeneral_Lib.LblCap);
                return;
            }

            Change = true;
            Txt_Acc_TextChanged(sender, e);
           

        }
        //-------------------------
        private void Cbo_Cust_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DCChange)
            {
                if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) < 0) //دائن
                {
                    TxtQty.Enabled = false;
                    CboSec.Enabled = false;
                    Txt_Description.Enabled = false;
                    TxtSpecific_Amount.Enabled = false;
                    TxtBuy_Date.Enabled = false;
                    TxtQty.Text = "0";
                    //TxtBuy_Date.Text = "00000000";
                    Txt_Description.Text = "";
                    TxtSpecific_Amount.Text = "0";
                }
                else // مدين
                {
                    TxtQty.Enabled = true;
                    CboSec.Enabled = true;
                    Txt_Description.Enabled = true;
                    TxtSpecific_Amount.Enabled = true;
                    TxtBuy_Date.Enabled = true;
                }

                Change = false;
                ACCChange = false;
                CurChange = false;
                Txt_Acc.Text = string.Empty;
                Grd_Acc_Id.ClearSelection();
                TxtCurr_Name.Text = string.Empty;
                Grd_Cur_Id.ClearSelection();
                Txt_Cust.Text = string.Empty;
                Grd_Cust_Id.ClearSelection();
                Txt_Amount.ResetText();
                Change = true;
                ACCChange = true;
                CurChange = true;
                Txt_Acc_TextChanged(sender, e);
                TxtSpecific_Amount_TextChanged(null, null);
                Txt_Amount_TextChanged(null, null);
            }
        }
        //----------------------------------------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                ACCChange = false;
                if (Cbo_Cust_Type.SelectedIndex == 0)//مدين
                {

                    SqlTxt = "SELECT ACC_Id,ACC_AName,Acc_EName,Sub_flag  FROM Account_Tree "
                             + " Where Record_flag = 1"
                             + " And ACC_Id   in (Select Acc_Id From CUSTOMERS_ACCOUNTS"
                             + " Where CUST_ID in(Select CUST_ID From CUSTOMERS Where Cust_Type_ID in(50) And Cust_Flag <> 0)"
                             + " And T_Id =" + connection.T_ID
                             + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth"
                             + " Where Auth_User_Id =" + connection.user_id
                             + " And VC_Sw = 1) And CUR_ID =" + Txt_Loc_Cur.Tag + " And CUS_STATE_ID <> 0 )"
                             + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth"
                             + " Where Auth_User_Id =" + connection.user_id
                             + " And VC_Sw = 1)"
                            + " And (ACC_AName like '" + Txt_Acc.Text + "%' Or ACC_EName Like '" + Txt_Acc.Text + "%' Or ACC_Id Like '" + Txt_Acc.Text + "')"
                            + " Order By " + (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename");

                    Bs_AccId.DataSource = connection.SqlExec(SqlTxt, "Tbl_Acc_Name");


                }//دائن
                else
                {

                    Bs_AccId.DataSource = connection.SqlExec("Get_Acc_Info ", "Tbl_Acc_Name",
                        new object[] { 0, Txt_Acc.Text.Trim(), connection.T_ID, connection.user_id, 0 });
                }
                Grd_Acc_Id.DataSource = Bs_AccId;

                if (connection.SQLDS.Tables["Tbl_Acc_Name"].Rows.Count > 0)
                {
                    ACCChange = true;
                    Grd_Acc_Id_SelectionChanged(sender, e);
                }
                else
                {

                    Grd_Cur_Id.DataSource = null;
                    Grd_Cust_Id.DataSource = null;

                }
            }
        }
        //---------------------------------------------------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                Txt_Cust.Text = "";
                TxtCurr_Name.Text = "";
                TxtCurr_Name_TextChanged(sender, e);
            }
        }
        //--------------------------------------------------------------------
        private void TxtCurr_Name_TextChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                CurChange = false;
                Bs_CurId.DataSource = connection.SqlExec("Get_Cur_Assets_Details", "Tbl_Cur_Acc",
                new object[] { Txt_Loc_Cur.Tag, TxtCurr_Name.Text.Trim(), connection.T_ID, 
                 Convert.ToDecimal(((DataRowView)Bs_AccId.Current).Row["Acc_Id"])
                ,Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"])});

                Grd_Cur_Id.DataSource = Bs_CurId;
                if (connection.SQLDS.Tables["Tbl_Cur_Acc"].Rows.Count > 0)
                {

                    CurChange = true;
                    Grd_Cur_Id_SelectionChanged(sender, e);
                }
                else
                {
                    Grd_Cust_Id.DataSource = null;
                }
            }
        }
        //-------------------------
        private void Grd_Cur_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                label21.Text = "(" + ((DataRowView)Bs_CurId.Current).Row["Cur_aname"].ToString() + ")";
                label21.Text = "(" + ((DataRowView)Bs_CurId.Current).Row["Cur_ename"].ToString() + ")";
                Txt_Exch_Price.Text = ((DataRowView)Bs_CurId.Current).Row["Exch_Rate"].ToString();
                Txt_Cust_TextChanged(sender, e);
            }
        }
        //-----------------------------------------------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                if (Grd_Acc_Id.RowCount > 0)
                {
                    Bs_CustId.DataSource = connection.SqlExec("Get_Cust_Info ", "Tbl_Cust_Name",
                               new object[] { ((DataRowView)Bs_CurId.Current).Row["Cur_Id"],
                           Txt_Cust.Text.Trim(), connection.T_ID, ((DataRowView)Bs_AccId.Current).Row["Acc_Id"],Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"])});

                    Grd_Cust_Id.DataSource = Bs_CustId;
                }
            }
        }
        //-------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Cbo_Cust_Type.Enabled = true;
            #region Validations
            if (Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب رجاءا" : "Please Select Account", MyGeneral_Lib.LblCap);
                Txt_Acc.Focus();
                return;
            }
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل العملة رجاءا" : "Please Select Currency", MyGeneral_Lib.LblCap);
                TxtCurr_Name.Focus();
                return;
            }
            if (Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"]) == 1)
            {
                if (Bs_CustId.List.Count > 0)
                {
                    if (Convert.ToInt16(((DataRowView)Bs_CustId.Current).Row["Cust_Id"]) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "الحساب للثانوي غير معرف في جدول الحسابات المركزية لهذه العملة" :
                                                            "Customer Account Unknown at Central Account For this Currency", MyGeneral_Lib.LblCap);
                        Txt_Cust.Focus();
                        return;
                    }
                }
            }
            if (Convert.ToDecimal(Txt_Amount.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Please Insert Amount", MyGeneral_Lib.LblCap);
                Txt_Amount.Focus();
                return;
            }

            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) == 1) // مدين
            {
                if (TxtQty.Text == "" || Convert.ToInt16(TxtQty.Text) == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الكمية" : "Please Insert Quantity please", MyGeneral_Lib.LblCap);
                    TxtQty.Focus();
                    return;
                }
                if (Txt_Description.Text == "" && Convert.ToInt16(Cbo_Cust_Type.SelectedValue) == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الوصف الدقيق" : "Please Insert Description please", MyGeneral_Lib.LblCap);
                    Txt_Description.Focus();
                    return;
                }
               // Ord_Strt = MyGeneral_Lib.DateChecking(TxtBuy_Date.Text);
                if (TxtBuy_Date.Text == "0000/00/00" || TxtBuy_Date.Text == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ الشراء" : "Please Insert Date of Buy", MyGeneral_Lib.LblCap);
                    TxtBuy_Date.Focus();
                    return;
                }
               // Ord_Strt = MyGeneral_Lib.DateChecking(TxtBuy_Date.Text);
            }

            #endregion
            Decimal MdFor_Amount = 0;
            Decimal MdLoc_amount = 0;
            Decimal McFor_Amount = 0;
            Decimal McLoc_amount = 0;

            Decimal MFor_Amount = 0;
            Decimal MLoc_amount = 0;


            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) == 1)
            {
                MdFor_Amount = Convert.ToDecimal(Txt_Amount.Text) * Convert.ToDecimal(TxtQty.Text);
                MdLoc_amount = decimal.Round(MdFor_Amount * Convert.ToDecimal(Txt_Exch_Price.Text), 3);
                MFor_Amount = MdFor_Amount;
                MLoc_amount = MdLoc_amount;

            }
            else
            {
                McFor_Amount = Convert.ToDecimal(Txt_Amount.Text);
                McLoc_amount = decimal.Round((McFor_Amount) * Convert.ToDecimal(Txt_Exch_Price.Text), 3);
                MFor_Amount = McFor_Amount * -1;
                MLoc_amount = McLoc_amount * -1;
            }

            #region Add Rows To  Table


            DataRow DRow = Asset_Voucher.NewRow();
            //----------------------------------------report----------------------------------------
            //string Acc_Aname = ((DataRowView)Bs_AccId.Current).Row["ACC_AName"].ToString();
            //string Acc_Ename = ((DataRowView)Bs_AccId.Current).Row["Acc_ename"].ToString();
            //string Acust_name = ((DataRowView)Bs_CustId.Current).Row["Acust_name"].ToString();
            //string Ecust_name = ((DataRowView)Bs_CustId.Current).Row["Ecust_name"].ToString();
            //string Cur_Code_Rpt = ((DataRowView)Bs_CurId.Current).Row["Cur_Code"].ToString();
            //DB_Acc_Cur_Cust = Acc_Aname + " / " + Cur_Code_Rpt + " / " + Acust_name;
            //DB_EAcc_Cur_Cust = Acc_Ename + " / " + Cur_Code_Rpt + " / " + Ecust_name;
            //DRow["DB_Acc_Cur_Cust"] = DB_Acc_Cur_Cust;
            //DRow["DB_EAcc_Cur_Cust"] = DB_EAcc_Cur_Cust;
            ////---------------------------------------------------------------------------------
            DRow["For_Amount"] = MFor_Amount;
            DRow["Loc_Amount"] = MLoc_amount;

            DRow["dFor_Amount"] = MdFor_Amount;
            DRow["cFor_Amount"] = McFor_Amount;

            DRow["dLoc_amount"] = MdLoc_amount;
            DRow["cLoc_amount"] = McLoc_amount;

            DRow["Acc_Name"] = ((DataRowView)Bs_AccId.Current).Row[connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename"];
            DRow["Acc_Id"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Id"];
            DRow["Cur_Name"] = ((DataRowView)Bs_CurId.Current).Row[connection.Lang_id == 1 ? "Cur_Aname" : "Cur_ename"];
            string Notes =  Convert.ToInt16(TxtQty.Text) > 0 ? connection.Lang_id ==1?"الكمية_" :"Amount_"+ TxtQty.Text : "";
            DRow["Notes"] = TxTNote.Text.Trim() + Notes;
            string Buy_Date = TxtBuy_Date.Value.ToString("yyyy/MM/dd");
            DRow["Buy_Date"] = Buy_Date;
            if (Bs_CustId.List.Count > 0)
            {
                DRow["Cust_Name"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"];
                DRow["Cust_id"] = ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
            }
            DRow["Exch_Price"] = Txt_Exch_Price.Text;
            DRow["Real_Price"] = Txt_Exch_Price.Text;

            DRow["Description1"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"];
            DRow["Description2"] = Txt_Description.Text;
            DRow["Qty_id"] = TxtQty.Text.Trim();
            DRow["Sec_Name"] = CboSec.Text;
            DRow["Sec_Id"] = CboSec.SelectedValue;
            DRow["For_cur_id"] = ((DataRowView)Bs_CurId.Current).Row["Cur_Id"];
            DRow["loc_cur_id"] = Txt_Loc_Cur.Tag;

            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) > 0)
            {
                DataRow[] Dr = connection.SQLDS.Tables["Sec_Tbl"].Select("Sec_Id = " + CboSec.SelectedValue);

                DRow["T_SEC_Id"] = Dr[0]["T_Sec_Id"];
            }
            DRow["Specific_Amount"] = Convert.ToDecimal(TxtSpecific_Amount.Text) * Convert.ToDecimal(TxtQty.Text);
            Sum_Specific += Convert.ToDecimal(TxtSpecific_Amount.Text) * Convert.ToDecimal(TxtQty.Text);
            Asset_Voucher.Rows.Add(DRow);
            _Bs.DataSource = Asset_Voucher;
            #endregion
            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) == 1)//مدين
            {
                TxTNote.Text = "";
                Txt_Amount.ResetText();
                TxtQty.Text = "";
                Txt_Description.Text = "";
                TxTNote.Text = "";
                TxtSpecific_Amount.ResetText();
                Grd_Acc_Id.ClearSelection();
            }
            Sum_Amount();
        }
        //---------------------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price","Cur_Name","Acc_Name","Cust_Name",
                        "Description1","Description2","Sec_name","Qty","Sec_Id","Buy_Date","Full_Symbol","T_Sec_Id",
                         "cFor_Amount","cLoc_amount","dFor_Amount","dLoc_amount","Specific_Amount", "Qty_id" , "Cur_ToWord", "Cur_ToEWord" ,"DB_Acc_Cur_Cust","DB_EAcc_Cur_Cust","For_Amount1"
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal", "System.String", "System.String", "System.String",
                "System.String","System.String","System.String","System.Int32","System.Int32", "System.String","System.String","System.String",
                "System.Decimal","System.Decimal","System.Decimal","System.Decimal","System.Decimal","System.Int16" , "System.String" ,"System.String","System.String","System.String","System.Int32"
            };

            Asset_Voucher = CustomControls.Custom_DataTable("Asset_Voucher", Column, DType);
            GrdAssest.DataSource = _Bs;
        }
        //---------------------------------------------------------------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Asset_Voucher.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد" :
                    "Are you sure you want to remove this record", MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {

                    if (Asset_Voucher.Rows.Count == 1)//حالة مسح المدين 
                    {
                        Cbo_Cust_Type.SelectedIndex = 0;
                        TxtBuy_Date.Text = "00000000";
                        Txt_Amount.ResetText();
                        TxtQty.Text = "";
                        TxtSpecific_Amount.ResetText();
                        Txt_Amount.Enabled = true;
                    }
                    Asset_Voucher.Rows[GrdAssest.CurrentRow.Index].Delete();
                    GrdAssest.Refresh();
                    Sum_Amount();
                    if (Asset_Voucher.Rows.Count == 0)
                    {
                        TxtTDLoc_Amount.ResetText();
                        TxtDFor_Amount.ResetText();
                    }
                }
            }
        }
        //-------------------------------------------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No opening balances", MyGeneral_Lib.LblCap);
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No Record Organizer", MyGeneral_Lib.LblCap);
                return;
            }
            if (Asset_Voucher.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "No Records For Adding", MyGeneral_Lib.LblCap);
                return;
            }
            Decimal Count = 0;
            if (Convert.ToDecimal(Asset_Voucher.Compute("Sum(Specific_Amount)", "")) != 0)
            {
                Count = Convert.ToDecimal(Asset_Voucher.Compute("Sum(Loc_Amount)", "")) - (Convert.ToDecimal(Asset_Voucher.Compute("Sum(Specific_Amount)", "")) );
            }
            else
            {
                Count = Convert.ToDecimal(Asset_Voucher.Compute("Sum(Loc_Amount)", ""));

            }
            if ((Count != 0))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "القيد غير متوازن" : "UnBalance Record", MyGeneral_Lib.LblCap);
                return;
            }

            #endregion

            Dt = Asset_Voucher.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "Description1", "Description2", "Sec_id", "Full_symbol", "Buy_date", "T_Sec_ID",
                              "Specific_Amount", "Qty_id");

            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@LOC_CUR_ID", Txt_Loc_Cur.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 38);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;

            connection.SqlExec("Add_Daily_Asset_Voucher", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            Change = false;
            ACCChange = false;
            CurChange = false;
            Txt_Acc.ResetText();
            Txt_Amount.ResetText();
            Txt_Cust.ResetText();
            TxtCurr_Name.ResetText();
            TxTNote.ResetText();
            TxtQty.ResetText();
            TxtSpecific_Amount.ResetText();
            Change = true;
            ACCChange = true;
            CurChange = true;
            Sum_Amount();
            Print_Rpt_Pdf1();
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
        //-------------------------
        private void GrdAssest_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }
        //-------------------------------------------------------------------
        private void Sum_Amount()
        {
            decimal DLoc_Amount = 0;
            decimal DFor_Amount = 0;
            decimal CLoc_Amount = 0;
            decimal CFor_Amount = 0;

            if (Asset_Voucher.Rows.Count > 0)
            {
                decimal Sum_Tot_CLoc_Amount = 0;
                decimal Sum_Tot_CFor_Amount = 0;

                DLoc_Amount = Convert.ToDecimal(Asset_Voucher.Compute("Sum(DLoc_Amount)", ""));
              
                TxtTDLoc_Amount.Text = DLoc_Amount.ToString();

                DFor_Amount = Convert.ToDecimal(Asset_Voucher.Compute("Sum(DFor_Amount)", ""));
              
                TxtDFor_Amount.Text = DLoc_Amount.ToString();

                //DLoc_Amount = Convert.ToDecimal(Asset_Voucher.Compute("Sum(DLoc_Amount)", ""));
                //TxtTDLoc_Amount.Text = DLoc_Amount.ToString();

                //DFor_Amount = Convert.ToDecimal(Asset_Voucher.Compute("Sum(DFor_Amount)", ""));
                //TxtDFor_Amount.Text = DFor_Amount.ToString();

                CLoc_Amount = Convert.ToDecimal(Asset_Voucher.Compute("Sum(CLoc_Amount)", ""));
                Sum_Tot_CLoc_Amount = CLoc_Amount + Sum_Specific;
                TxtTCLoc_Amount.Text = Sum_Tot_CLoc_Amount.ToString();
                CFor_Amount = Convert.ToDecimal(Asset_Voucher.Compute("Sum(CFor_Amount)", ""));
                Sum_Tot_CFor_Amount = CFor_Amount + Sum_Specific;
                TxtCFor_Amount.Text = Sum_Tot_CFor_Amount.ToString();
            }
        }
        //-------------------------------------------------------------------
        private void Buy_cash_assest_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            ACCChange = false;
            CurChange = false;
            DCChange = false;
            string[] Used_Tbl = { "Tbl_Cur_Acc", "Tbl_Acc_Name", "Tbl_Cust_Name", "Sec_Tbl", "DC_Id_tab" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-------------------------------------------------------------------
        private void Txt_Amount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Txt_Loc_Assets_Amount.Text = (Convert.ToDecimal(Txt_Amount.Text) * Convert.ToDecimal(Txt_Exch_Price.Text)).ToString();
            }
            catch
            { }
        }
        //-------------------------------------------------------------------
        private void TxtSpecific_Amount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Txt_Loc_Specific_Amount.Text = (Convert.ToDecimal(TxtSpecific_Amount.Text) * Convert.ToDecimal(Txt_Exch_Price.Text)).ToString();
            }
            catch
            { }

        }
        //-----------------------------------------------------------------------
        private void Print_Rpt_Pdf1()
        {
            string SqlTxt = "Exec Main_Report " + VO_NO + "," + TxtIn_Rec_Date.Text + ",38," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            string SqlTxt1 = "Exec Main_Report_Assets_Details " + VO_NO + "," + TxtIn_Rec_Date.Text + ",38," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Asset_Voucher");
            connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

            Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
            Main_Assets_Rpt_Eng ObjRptEng = new Main_Assets_Rpt_Eng();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 38);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

            DataRow Dr;
            int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 38);
            try
            {
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Asset_Voucher");
                connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
            }
            catch
            { }


        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void TxtBuy_Date_ValueChanged(object sender, EventArgs e)
        {
            if (TxtBuy_Date.Checked == true)
            {
                TxtBuy_Date.Format = DateTimePickerFormat.Custom;
                TxtBuy_Date.CustomFormat = @format;
            }
            else
            {
                TxtBuy_Date.Format = DateTimePickerFormat.Custom;
                TxtBuy_Date.CustomFormat = @format_null;
            }
        }
    }
}