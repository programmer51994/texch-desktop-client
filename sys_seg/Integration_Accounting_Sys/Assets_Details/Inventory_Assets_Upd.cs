﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Inventory_Assets_Upd : Form
    {
        int Sec_ID = 0;
        string Full_Symbol = "";
        int CUST_ID = 0;
        BindingSource _Bs_GRD1 = new BindingSource();
        BindingSource _Bs_GRD2 = new BindingSource();
        public Inventory_Assets_Upd()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

        }

        private void Inventory_Assets_Upd_Load(object sender, EventArgs e)
        {
            Grd_Assets.AutoGenerateColumns = false;
            DataRowView Drvname = Inventory_Assets_Main._Bs_GRD_Main.Current as DataRowView;
            DataRow Drname = Drvname.Row;
            Sec_ID = Drname.Field<Byte>("sec_id");
            Full_Symbol = Drname.Field<String>("Full_symbol");
            CUST_ID = Drname.Field<int>("CUST_ID");


            _Bs_GRD1.DataSource = connection.SQLDS.Tables["Asset_Tbl"].DefaultView.ToTable().Select("Full_Symbol like '" + Full_Symbol + "' And Sec_ID = " + Sec_ID).CopyToDataTable();
            Grd_Assets.DataSource = _Bs_GRD1;
            Txt_Seq_id.Text = ((DataRowView)Inventory_Assets_Main._Bs_GRD_Main.Current).Row["Seq_id"].ToString();

            string Sqltext = "SELECT T_sec_id, sec_id, sec_aname, sec_ename, T_ID FROM sections where t_id = " + connection.T_ID;
            _Bs_GRD2.DataSource = connection.SqlExec(Sqltext, "SEC_TBL");
            CboSec.DataSource = _Bs_GRD2;
            CboSec.DisplayMember = connection.Lang_id == 1 ? "sec_aname" : "sec_ename";
            CboSec.ValueMember = "Sec_ID";
            CboSec.Text = ((DataRowView)Inventory_Assets_Main._Bs_GRD_Main.Current).Row["sec_aname"].ToString();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, CboSec.SelectedValue);
            ItemList.Insert(1, Full_Symbol);
            ItemList.Insert(2, Txt_Seq_id.Text);
            ItemList.Insert(3, connection.user_id);
            ItemList.Insert(4, connection.T_ID);
            ItemList.Insert(5, CUST_ID);
            ItemList.Insert(6, "");
            connection.scalar("Update_Assets_Section", ItemList);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            connection.SQLCMD.Parameters.Clear();
            this.Close();
            
        }


        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ExtBtn_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Grd_Assets_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_GRD1);
        }

        private void Inventory_Assets_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {

            
            string[] Used_Tbl = { "Sec_Tbl", "Acc_Tbl", "Cust_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void CboSec_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}