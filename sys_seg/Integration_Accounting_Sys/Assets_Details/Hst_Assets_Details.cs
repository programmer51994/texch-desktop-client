﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Hst_Assets_Details : Form
    {
        int T_ID = 0;
        String Hst_Full_symbol = "";
        public Hst_Assets_Details(String Full_Symbol)
        {
            InitializeComponent();
            connection.SQLBSMainGrd.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Hst_Full_symbol = Full_Symbol;
            Grd_Hst_Assets_Details.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Hst_Assets_Details.Columns["Column3"].DataPropertyName = "Ecust_Name";
                Grd_Hst_Assets_Details.Columns["Column5"].DataPropertyName = "Acc_Ename";
                Grd_Hst_Assets_Details.Columns["Column6"].DataPropertyName = "Sec_ename";
                Grd_Hst_Assets_Details.Columns["Column11"].DataPropertyName = "EDescription";
                
            }

        }
        //---------------------------------------------------
        private void Hst_Drop_Assets_Details_Load(object sender, EventArgs e)
        {
            Txt_Term.Text = ((DataRowView)connection.SQLBSCbo.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name"].ToString();
            T_ID = Convert.ToInt16(((DataRowView)connection.SQLBSCbo.Current).Row["T_Id"]);
            object[] Sparm = { T_ID, 0, 0, Hst_Full_symbol, "Hst_Assets_Details" };
           connection.SQLBSMainGrd.DataSource = connection.SqlExec("Reveal_Assets_details", "TBL_HST_Assets_details", Sparm);
           if (connection.SQLDS.Tables["TBL_HST_Assets_details"].Rows.Count > 0)
           {
               Grd_Hst_Assets_Details.DataSource = connection.SQLBSMainGrd;
           }
           else
           {
                  MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات تاريخية لهذا الرمز" : "No History info. for this Symbol", MyGeneral_Lib.LblCap);
                   this.Close();
              
           }
        }
        //---------------------------------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------
        private void Grd_Hst_Assets_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(connection.SQLBSMainGrd);
        }
        //---------------------------------------------------
        private void Hst_Drop_Assets_Details_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Used_Tbl = { "TBL_HST_Assets_details" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Prt_Btn_Click(object sender, EventArgs e)
        {

        }
    }
}
