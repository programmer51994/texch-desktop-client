﻿using System;
using System.Data;
using System.Windows.Forms;
namespace Integration_Accounting_Sys
{
    public partial class Inventory_Asset_Add : Form
    {
        #region Definition
        bool Change = false;
        string Sql_Txt = "";
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CustId =new BindingSource();
        BindingSource BS_assets = new BindingSource();
        DataTable Asset_Tbl = new DataTable();
        string @format = "dd/MM/yyyy";
        string @format_null = "0000/00/00";
        #endregion
        //------------------------------
        public Inventory_Asset_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            Grd_Assets.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Column10.DataPropertyName = "Acc_Ename";
                Column12.DataPropertyName = "Ecust_name";
            }
            Create_Table();  
        }
        //------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                "Id", "Description1","Description2","Acc_id","Cust_id","T_id","Sec_id",
                "Seq_id","Full_symbol","Asset_amount","dep_month","dep_year","Buy_date","Specific_Amount",
                "Month_year","T_sec_id","Vo_No","Nrec_Date","Dep_nrec_date","Loc_Cur_id","cdep_period",
                "cdep_per", "cdep_amount","Sec_Name"
            };
            string[] DType =
            {
                "System.Int32","System.String","System.String","System.Int32","System.Int32","System.Int32","System.Byte",
                "System.Int32","System.String","System.Decimal","System.Int32","System.Int32","System.DateTime","System.Decimal",
                "System.Int32","System.String","System.Int32","System.Int32","System.Int32","System.Int32","System.Int32",
                "System.Decimal","System.Decimal","System.String"
            };

            Asset_Tbl = CustomControls.Custom_DataTable("Asset_Voucher", Column, DType);
            Grd_Assets.DataSource = BS_assets;
        }
        //------------------------------
        private void Inventory_Asset_Add_Load(object sender, EventArgs e)
        {
           
            Sql_Txt = "Select sec_aname,sec_ename,sec_id from sections where T_ID = " + connection.T_ID;
            CboSec.DataSource = connection.SqlExec(Sql_Txt, "Sec_Tbl");
            CboSec.ValueMember = "Sec_id";
            CboSec.DisplayMember = connection.Lang_id == 1 ? "Sec_Aname" : "Sec_Ename";

            Sql_Txt = "Select Cast( " + connection.T_ID +" * 100 as Varchar(10)) + Cast((Select ISNULL(max(Rec_ID),0) From Assets_Details Where T_id = "+ connection.T_ID 
                +")  + (ROW_NUMBER() OVER(ORDER BY (Select ISNULL(max(Rec_ID),0) From Assets_Details Where T_id ="+ connection.T_ID +") DESC))as Varchar(100)) As ID ";
            Txt_Seq_id.Text = connection.SqlExec(Sql_Txt, "Tbl_Max_Id").Rows[0][0].ToString();
            Txt_Seq_id.Enabled = false;
            Txt_Acc_TextChanged(sender, e);
        }
        //------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            Change = false;

            Sql_Txt = "SELECT ACC_Id,ACC_AName,Acc_EName,Sub_flag  FROM Account_Tree"
                       + " WHere Record_flag = 1 "
                       + " And ACC_Id   in (Select Acc_Id From CUSTOMERS_ACCOUNTS "
                       + " Where CUST_ID in(Select CUST_ID From CUSTOMERS Where Cust_Type_ID in(50) AND Cust_flag <> 0)"
                       + " And T_Id = " + connection.T_ID
                       + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth Where Auth_User_Id = " + connection.user_id
                       + " And VC_Sw = 1 ) and Cur_id = 1 And CUS_STATE_ID <> 0)"
                       + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth "
                       + " Where Auth_User_Id = " + connection.user_id
                       + " And VC_Sw = 1) "
                       + " And (ACC_AName like '" + Txt_Acc.Text + "%' Or ACC_EName Like '" + Txt_Acc.Text + "%' Or ACC_Id Like '" + Txt_Acc.Text + "')"
                       + " Order By " + (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_EName");
            Bs_AccId.DataSource = connection.SqlExec(Sql_Txt, "Acc_Tbl");
            Grd_Acc_Id.DataSource = Bs_AccId;
            if (connection.SQLDS.Tables["Acc_Tbl"].Rows.Count > 0)
            {
                Change = true;
                Grd_Acc_Id_SelectionChanged(sender, e);
            }
            else
            {
                Grd_Cust_Id.DataSource = new DataTable();
            }
        }
        //------------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            Txt_Cust.ResetText();
            Txt_Cust_TextChanged(sender, e);
        }
        //------------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                Sql_Txt = "SELECT distinct Acust_Name,Ecust_name,A.Cust_Id,Cust_Type_ID "
                       + " From Customers_Accounts A, Customers B "
                       + " Where  A.Cust_Id = B.Cust_Id  And A.Cus_State_Id = 1 "
                       + " And  ACC_Id = " + ((DataRowView)Bs_AccId.Current).Row["Acc_Id"].ToString()
                       + " AND A.T_Id =  " + connection.T_ID
                       + " And Cust_Flag  <> 0 And A.CUS_STATE_ID <> 0 "
                       + " And (ACUST_NAME Like '" + Txt_Cust.Text.Trim() + "%' "
                       + " Or	ECUST_NAME Like '" + Txt_Cust.Text.Trim() + "%' "
                       + " Or	A.CUST_ID Like'" + Txt_Cust.Text.Trim() + "%' )"
                       + " and Cust_Type_ID = 50  "
                       + " Order by " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");

                Bs_CustId.DataSource = connection.SqlExec(Sql_Txt, "Cust_Tbl");
                Grd_Cust_Id.DataSource = Bs_CustId;
            }
        }
        //------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validations
            if (Grd_Acc_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب " : "Select Account Please", MyGeneral_Lib.LblCap);
                Txt_Acc.Focus();
                return;
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الثــانوي" : "Select Customer Please", MyGeneral_Lib.LblCap);
                Txt_Cust.Focus();
                return;
            }
            if (Convert.ToDecimal(Txt_Amount.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Insert Amount Please", MyGeneral_Lib.LblCap);
                Txt_Amount.Focus();
                return;
            }
            //if (Txt_Seq_id.Text == "" || Convert.ToInt16(Txt_Seq_id.Text) <= 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "ادخل التسلســل" : "Insert Sequence Please", MyGeneral_Lib.LblCap);
            //    Txt_Seq_id.Focus();
            //    return;
            //}
            if (Convert.ToInt32(CboSec.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تاكد من اختيار القطاع" : "Please Make Sure Of Select The Sector", MyGeneral_Lib.LblCap);
                Txt_Detl.Focus();
                return;
            }
            if (Txt_Detl.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الوصف الدقيق" : "Insert Description please", MyGeneral_Lib.LblCap);
                Txt_Detl.Focus();
                return;
            }
           // string Ord_Strt = MyGeneral_Lib.DateChecking(Txt_Buy_date.Text);
            if (Txt_Buy_date.Text == "0000/00/00" || Txt_Buy_date.Text == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تاكد من تأريخ الشــراء" : "Please make sure of Buy date", MyGeneral_Lib.LblCap);
                Txt_Buy_date.Focus();
                //Ord_Strt = "0";
                return;
            }
            if (TxtMonth.Text == "" || Convert.ToInt32(TxtMonth.Text) > 12)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تأكد من الشهر" : "Please Make Sure of THe Month", MyGeneral_Lib.LblCap);
                TxtMonth.Focus();
                return;
            }
            if (TxtYear.Text.Length < 4)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تاكد من السـنة" : "Please Make Sure of The Year", MyGeneral_Lib.LblCap);
                TxtYear.Focus();
                return;
            }
            #endregion
            DataRow DRow = Asset_Tbl.NewRow();
            DRow["Description1"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"];
            DRow["Description2"] = Txt_Detl.Text.Trim();
            DRow["Seq_id"] = Txt_Seq_id.Text.Trim();
            DRow["Sec_Name"] = CboSec.Text;
            DRow["Cust_id"] = ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
            DRow["Acc_Id"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Id"];
            DRow["Asset_Amount"] = Convert.ToDecimal(Txt_Amount.Text);
            string Buy_Date = Txt_Buy_date.Value.ToString("yyyy/MM/dd");
            DRow["Buy_date"] = Buy_Date;
            DRow["Dep_Month"] =Convert.ToInt32(TxtMonth.Text) ;
            DRow["Dep_year"] = Convert.ToInt32(TxtYear.Text);
            DRow["loc_cur_id"] = 1;// Txt_Loc_Cur.Tag;
            DRow["Specific_Amount"] = TxTSpecific.Text.Trim();
            DRow["Sec_id"] = CboSec.SelectedValue ;
            DRow["T_id"] = connection.T_ID;
            DRow["nrec_date"] = TxtIn_Rec_Date.Text ;
            Asset_Tbl.Rows.Add(DRow);
            Txt_Acc.ResetText();
            Txt_Cust.ResetText();
            Txt_Amount.ResetText();
            Txt_Detl.ResetText();
           // Txt_Seq_id.ResetText();
            string X = Txt_Seq_id.Text;
            int Y = Convert.ToInt32(X) + 1;
            Txt_Seq_id.Text = Y.ToString();
            TxTSpecific.ResetText();
            Txt_Buy_date.Text = "00000000";
            TxtMonth.ResetText();
            TxtYear.ResetText();
            Grd_Acc_Id.ClearSelection();
            Grd_Cust_Id.ClearSelection();
            BS_assets.DataSource = Asset_Tbl ;
            Grd_Assets_SelectionChanged(sender, e);
        }
        //------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Asset_Tbl.Rows.Count > 0)
            {
                Asset_Tbl.Rows[Grd_Assets.CurrentRow.Index].Delete();
            }
        }
        //------------------------------
        private void Inventory_Asset_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Used_Tbl = { "Sec_Tbl", "Acc_Tbl", "Cust_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
        #region Validation

            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد مستخدم معرف " : "No  User Defined", MyGeneral_Lib.LblCap);
                return;
            }

            //if (Convert.ToInt16(TxtBox_User.Tag) <= 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد امين صندوق معرف " : "No Treasurer Found", MyGeneral_Lib.LblCap);
            //    return;
            //}
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No opening balances", MyGeneral_Lib.LblCap);
                return;
            }
            if (Asset_Tbl.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات للاضافة" : "No Data To Add", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            DataTable DT = Asset_Tbl.DefaultView.ToTable(false, "Id", "Description1", "Description2", "Acc_id", "Cust_id", "T_id", "Sec_id",
                "Seq_id", "Full_symbol", "Asset_amount", "dep_month", "dep_year", "Buy_date", "Specific_Amount",
                "Month_year", "T_sec_id", "Vo_No", "Nrec_Date", "Dep_nrec_date", "Loc_Cur_id", "cdep_period",
                "cdep_per", "cdep_amount");
            connection.SQLCMD.Parameters.AddWithValue("@Asset_Table", DT);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Inventory_Asset", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" )
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Asset_Tbl.Rows.Clear();
            Txt_Acc.ResetText();
            Txt_Cust.ResetText();
            Txt_Amount.ResetText();
            Txt_Detl.ResetText();
           // Txt_Seq_id.ResetText();
            TxTSpecific.ResetText();
            Txt_Buy_date.Text = "00000000";
            TxtMonth.ResetText();
            TxtYear.ResetText();
            Grd_Acc_Id.ClearSelection();
            Grd_Cust_Id.ClearSelection();
           
        }
        //------------------------------
        private void Grd_Assets_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(BS_assets);
        }

        private void Inventory_Asset_Add_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Txt_Buy_date_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_Buy_date.Checked == true)
            {
                Txt_Buy_date.Format = DateTimePickerFormat.Custom;
                Txt_Buy_date.CustomFormat = @format;
            }
            else
            {
                Txt_Buy_date.Format = DateTimePickerFormat.Custom;
                Txt_Buy_date.CustomFormat = @format_null;
            }
        }
    }
}