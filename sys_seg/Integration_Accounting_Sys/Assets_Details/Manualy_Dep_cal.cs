﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Manualy_Dep_cal : Form
    {
        #region Declareations
        string Sql_Text = "";
        string Sql_test1 = "";
        bool x = false;
        #endregion

        public Manualy_Dep_cal()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }
        //----------------------------------------
        private void Companies_Settings_Load(object sender, EventArgs e)
        {
            x = false;

            Sql_Text = "select B.T_id,A.ACUST_NAME , A.ECUST_NAME "
                  + " From CUSTOMERS A, TERMINALS B "
                  + " Where A.CUST_ID = B.CUST_ID "
                  + " And A.Cust_Flag <> 0 "
                  + " And B.T_id in ( Select T_id from Assets_Details ) "
                  + " Order By " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");

            Cbo_term.DataSource = connection.SqlExec(Sql_Text, "Terminals_Tbl");
            Cbo_term.DisplayMember = (connection.Lang_id) == 1 ? "ACUST_NAME" : "ECUST_NAME";
            Cbo_term.ValueMember = "T_ID";
            if (connection.SQLDS.Tables["Terminals_Tbl"].Rows.Count > 0)
            {
                x = true;
                Cbo_term_SelectedIndexChanged(null, null);
            }
            Sql_Text = " Select top(1)   Dep_Day ,Dep_Reord  , Dep_Cal_Method from Companies ";
            connection.SqlExec(Sql_Text, "Setting_tbl");


            Txt_Dep_day.Text = connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Day"].ToString();
            CHK_Dep_Reord.Checked = Convert.ToBoolean(connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Reord"]);

            Sql_test1 = " Select  Method_Id, method_aname, method_ename from calc_method";
            Cbo_method.DataSource = connection.SqlExec(Sql_test1, "Method_tbl");
            Cbo_method.DisplayMember = (connection.Lang_id) == 1 ? "method_aname" : "method_ename";
            Cbo_method.ValueMember = "Method_Id";

            Cbo_method.SelectedValue = Convert.ToInt16(connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Cal_Method"]);

        }
        //----------------------------------------
        private void Cbo_term_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (x)
            {
                Sql_Text = " SELECT DISTINCT dep_month,dep_year FROM Assets_Details"
                       + " where t_id = " + Convert.ToInt16(Cbo_term.SelectedValue);


                connection.SqlExec(Sql_Text, "monthyear_Ass_Tbl");
                Txt_month.Text = connection.SQLDS.Tables["monthyear_Ass_Tbl"].Rows[0]["dep_month"].ToString();
                Txt_year.Text = connection.SQLDS.Tables["monthyear_Ass_Tbl"].Rows[0]["dep_year"].ToString();

                Sql_Text = " select convert( varchar(10) ,Nrec_Date,111) as Nrec_Date from Daily_Opening "
                         + " where R_Close_Flag = 0"
                         + " and R_Close_Date is null "
                         + " and t_id = " + Convert.ToInt16(Cbo_term.SelectedValue);

                try
                {
                    Txt_Nrec_date.Text = connection.SqlExec(Sql_Text, "date_tbl").Rows[0]["Nrec_Date"].ToString();
                }
                catch { }
            }
        }
        //----------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {

            connection.SQLCMD.Parameters.AddWithValue("@T_Id", Cbo_term.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Dep_User_Date", Txt_User_date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Manulay_Dep_Cal", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                return;
            }
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();
            this.Close();
        }
        //----------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Manualy_Dep_cal_FormClosed(object sender, FormClosedEventArgs e)
        {
            x = false;
        }
    }
}