﻿namespace Integration_Accounting_Sys
{
    partial class Assets_Daily_Buy_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCurr_Name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_Cust = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_Acc = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Grd_Acc_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Description = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Grd_Cur_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.GrdAssest = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.CboSec = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxTNote = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Cbo_Cust_Type = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.Txt_Loc_Specific_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Loc_Assets_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtSpecific_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtTDLoc_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtTCLoc_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtCFor_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtDFor_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtQty = new Integration_Accounting_Sys.NumericTextBox();
            this.Txt_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Exch_Price = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtBuy_Date = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAssest)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(641, 9);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(94, 14);
            this.label3.TabIndex = 561;
            this.label3.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(740, 5);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(118, 23);
            this.Txt_Loc_Cur.TabIndex = 560;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(398, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(237, 23);
            this.TxtUser.TabIndex = 559;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(2, 5);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(309, 23);
            this.TxtTerm_Name.TabIndex = 554;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(863, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 553;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(918, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(99, 23);
            this.TxtIn_Rec_Date.TabIndex = 556;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(3, 31);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel9.TabIndex = 555;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(317, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 552;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtCurr_Name
            // 
            this.TxtCurr_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurr_Name.Location = new System.Drawing.Point(451, 70);
            this.TxtCurr_Name.Name = "TxtCurr_Name";
            this.TxtCurr_Name.Size = new System.Drawing.Size(210, 23);
            this.TxtCurr_Name.TabIndex = 1;
            this.TxtCurr_Name.TextChanged += new System.EventHandler(this.TxtCurr_Name_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(383, 73);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(61, 16);
            this.label6.TabIndex = 572;
            this.label6.Text = "العملــــــــة:";
            // 
            // Txt_Cust
            // 
            this.Txt_Cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cust.Location = new System.Drawing.Point(763, 70);
            this.Txt_Cust.Name = "Txt_Cust";
            this.Txt_Cust.Size = new System.Drawing.Size(254, 23);
            this.Txt_Cust.TabIndex = 2;
            this.Txt_Cust.TextChanged += new System.EventHandler(this.Txt_Cust_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(699, 73);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 567;
            this.label7.Text = "الــثـانــوي:";
            // 
            // Txt_Acc
            // 
            this.Txt_Acc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Acc.Location = new System.Drawing.Point(81, 70);
            this.Txt_Acc.Name = "Txt_Acc";
            this.Txt_Acc.Size = new System.Drawing.Size(279, 23);
            this.Txt_Acc.TabIndex = 0;
            this.Txt_Acc.TextChanged += new System.EventHandler(this.Txt_Acc_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(12, 73);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(66, 16);
            this.label5.TabIndex = 562;
            this.label5.Text = "الحســــــاب:";
            // 
            // Grd_Acc_Id
            // 
            this.Grd_Acc_Id.AllowUserToAddRows = false;
            this.Grd_Acc_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.Grd_Acc_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_Acc_Id.ColumnHeadersHeight = 40;
            this.Grd_Acc_Id.ColumnHeadersVisible = false;
            this.Grd_Acc_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column10});
            this.Grd_Acc_Id.Location = new System.Drawing.Point(10, 93);
            this.Grd_Acc_Id.Name = "Grd_Acc_Id";
            this.Grd_Acc_Id.ReadOnly = true;
            this.Grd_Acc_Id.RowHeadersVisible = false;
            this.Grd_Acc_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_Acc_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Id.Size = new System.Drawing.Size(350, 113);
            this.Grd_Acc_Id.TabIndex = 1;
            this.Grd_Acc_Id.SelectionChanged += new System.EventHandler(this.Grd_Acc_Id_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 128;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Acc_Aname";
            this.Column10.HeaderText = "اسم الحساب";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 220;
            // 
            // Txt_Description
            // 
            this.Txt_Description.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Description.Location = new System.Drawing.Point(86, 270);
            this.Txt_Description.MaxLength = 100;
            this.Txt_Description.Name = "Txt_Description";
            this.Txt_Description.Size = new System.Drawing.Size(674, 23);
            this.Txt_Description.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(4, 273);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(81, 16);
            this.label10.TabIndex = 582;
            this.label10.Text = "الوصف الدقيــق:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(5, 214);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 579;
            this.label8.Text = "المبلـــــــــــــغ :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(391, 214);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 578;
            this.label9.Text = "سعر التعادل:";
            // 
            // Grd_Cur_Id
            // 
            this.Grd_Cur_Id.AllowUserToAddRows = false;
            this.Grd_Cur_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_Cur_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_Cur_Id.ColumnHeadersHeight = 40;
            this.Grd_Cur_Id.ColumnHeadersVisible = false;
            this.Grd_Cur_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.Column11});
            this.Grd_Cur_Id.Location = new System.Drawing.Point(386, 93);
            this.Grd_Cur_Id.Name = "Grd_Cur_Id";
            this.Grd_Cur_Id.ReadOnly = true;
            this.Grd_Cur_Id.RowHeadersVisible = false;
            this.Grd_Cur_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.Grd_Cur_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur_Id.Size = new System.Drawing.Size(275, 113);
            this.Grd_Cur_Id.TabIndex = 3;
            this.Grd_Cur_Id.SelectionChanged += new System.EventHandler(this.Grd_Cur_Id_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Cur_Id";
            this.dataGridViewTextBoxColumn3.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 128;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Cur_Aname";
            this.Column11.HeaderText = "اسم الحساب";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 210;
            // 
            // Grd_Cust_Id
            // 
            this.Grd_Cust_Id.AllowUserToAddRows = false;
            this.Grd_Cust_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.Grd_Cust_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.Grd_Cust_Id.ColumnHeadersHeight = 40;
            this.Grd_Cust_Id.ColumnHeadersVisible = false;
            this.Grd_Cust_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.Column12});
            this.Grd_Cust_Id.Location = new System.Drawing.Point(702, 93);
            this.Grd_Cust_Id.Name = "Grd_Cust_Id";
            this.Grd_Cust_Id.ReadOnly = true;
            this.Grd_Cust_Id.RowHeadersVisible = false;
            this.Grd_Cust_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.Grd_Cust_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_Id.Size = new System.Drawing.Size(315, 113);
            this.Grd_Cust_Id.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Cust_Id";
            this.dataGridViewTextBoxColumn5.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 128;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "ACust_Name";
            this.Column12.HeaderText = "اسم الحساب";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 210;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 555);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 586;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(512, 558);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(99, 28);
            this.ExtBtn.TabIndex = 10;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(413, 558);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(99, 28);
            this.BtnOk.TabIndex = 9;
            this.BtnOk.Text = "مـوافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // GrdAssest
            // 
            this.GrdAssest.AllowUserToAddRows = false;
            this.GrdAssest.AllowUserToDeleteRows = false;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAssest.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle28;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAssest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.GrdAssest.ColumnHeadersHeight = 40;
            this.GrdAssest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column3,
            this.Column4,
            this.Column6,
            this.Column15,
            this.Column16,
            this.Column9,
            this.Column13,
            this.Column17,
            this.Column7,
            this.Column14,
            this.Column8});
            this.GrdAssest.Location = new System.Drawing.Point(10, 326);
            this.GrdAssest.Name = "GrdAssest";
            this.GrdAssest.ReadOnly = true;
            this.GrdAssest.RowHeadersWidth = 10;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAssest.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.GrdAssest.Size = new System.Drawing.Size(1007, 168);
            this.GrdAssest.TabIndex = 589;
            this.GrdAssest.SelectionChanged += new System.EventHandler(this.GrdAssest_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "dFor_Amount";
            dataGridViewCellStyle30.Format = "N3";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle30;
            this.Column1.HeaderText = "مــدين (ع.ص)";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 125;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CFor_Amount";
            dataGridViewCellStyle31.Format = "N3";
            dataGridViewCellStyle31.NullValue = null;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle31;
            this.Column2.HeaderText = "دائــن (ع.ص)";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 125;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Acc_name";
            this.Column5.HeaderText = "الحساب";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Cur_name";
            this.Column3.HeaderText = "الـعمــــــلـة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Cust_Name";
            this.Column4.HeaderText = "الثانــــوي";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "exch_price";
            dataGridViewCellStyle32.Format = "N7";
            dataGridViewCellStyle32.NullValue = null;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle32;
            this.Column6.HeaderText = "سعر التعادل";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 110;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "dLoc_Amount";
            dataGridViewCellStyle33.Format = "N3";
            this.Column15.DefaultCellStyle = dataGridViewCellStyle33;
            this.Column15.HeaderText = "مديــن ( ع.م)";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Cloc_Amount";
            dataGridViewCellStyle34.Format = "N3";
            this.Column16.DefaultCellStyle = dataGridViewCellStyle34;
            this.Column16.HeaderText = "دائـن (ع.م)";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column9.DataPropertyName = "Sec_name";
            this.Column9.HeaderText = "اسم القطاع";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 78;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column13.DataPropertyName = "Qty";
            this.Column13.HeaderText = "الكميـة";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 65;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Specific_Amount";
            dataGridViewCellStyle35.Format = "N3";
            this.Column17.DefaultCellStyle = dataGridViewCellStyle35;
            this.Column17.HeaderText = "المخصص المتراكم";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "تاريخ الشراء الفعلي";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Description1";
            this.Column14.HeaderText = "الوصف الدقيـق";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 150;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Description2";
            this.Column8.HeaderText = "الملاحظات";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 194;
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(925, 297);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(90, 26);
            this.BtnDel.TabIndex = 8;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(834, 297);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(90, 26);
            this.BtnAdd.TabIndex = 7;
            this.BtnAdd.Text = "اضــافــة";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 590);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1025, 22);
            this.statusStrip1.TabIndex = 596;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(3, 31);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 525);
            this.flowLayoutPanel7.TabIndex = 597;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1021, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1, 525);
            this.flowLayoutPanel2.TabIndex = 598;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(219, 245);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(61, 16);
            this.label11.TabIndex = 599;
            this.label11.Text = "الكميـــــــة:";
            // 
            // CboSec
            // 
            this.CboSec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboSec.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboSec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboSec.FormattingEnabled = true;
            this.CboSec.Location = new System.Drawing.Point(854, 269);
            this.CboSec.Name = "CboSec";
            this.CboSec.Size = new System.Drawing.Size(161, 24);
            this.CboSec.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(759, 273);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 16);
            this.label12.TabIndex = 602;
            this.label12.Text = "القطــــــــــــــاع:";
            // 
            // TxTNote
            // 
            this.TxTNote.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxTNote.Location = new System.Drawing.Point(86, 298);
            this.TxTNote.MaxLength = 85;
            this.TxTNote.Name = "TxTNote";
            this.TxTNote.Size = new System.Drawing.Size(741, 23);
            this.TxTNote.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(4, 301);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(81, 16);
            this.label13.TabIndex = 603;
            this.label13.Text = "الملاحظـــــــات:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(673, 502);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(186, 14);
            this.label14.TabIndex = 606;
            this.label14.Text = "المبلغ بالعملة المحلية مديــــــن:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(12, 502);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(184, 14);
            this.label15.TabIndex = 608;
            this.label15.Text = "المبلغ بالعملة الاصلية مديــــــن:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(5, 245);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 16);
            this.label16.TabIndex = 611;
            this.label16.Text = "تاريخ الشـــراء:";
            // 
            // Cbo_Cust_Type
            // 
            this.Cbo_Cust_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Cust_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Cust_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Cust_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Cust_Type.FormattingEnabled = true;
            this.Cbo_Cust_Type.Location = new System.Drawing.Point(83, 38);
            this.Cbo_Cust_Type.Name = "Cbo_Cust_Type";
            this.Cbo_Cust_Type.Size = new System.Drawing.Size(155, 24);
            this.Cbo_Cust_Type.TabIndex = 612;
            this.Cbo_Cust_Type.SelectedIndexChanged += new System.EventHandler(this.Cbo_Cust_Type_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(21, 42);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(56, 16);
            this.label17.TabIndex = 613;
            this.label17.Text = "الــــحـالـة:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(10, 531);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(187, 14);
            this.label18.TabIndex = 614;
            this.label18.Text = "المبلغ بالعملة الاصلية دائــــــــن:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(671, 531);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(189, 14);
            this.label19.TabIndex = 616;
            this.label19.Text = "المبلغ بالعملة المحلية دائــــــــن:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(360, 245);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(150, 16);
            this.label20.TabIndex = 619;
            this.label20.Text = "المخصص المتراكم للقطعـــــــة:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(622, 245);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(84, 16);
            this.label21.TabIndex = 620;
            this.label21.Text = "(دينار عراقـــي)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(713, 214);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(116, 16);
            this.label2.TabIndex = 630;
            this.label2.Text = "القيمــة الاسميـــة (ع.م):\r\n";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(710, 237);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(119, 32);
            this.label22.TabIndex = 632;
            this.label22.Text = "القيمة الاسمية للمخصص\r\n المتراكم  (ع.م):";
            // 
            // Txt_Loc_Specific_Amount
            // 
            this.Txt_Loc_Specific_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Specific_Amount.Enabled = false;
            this.Txt_Loc_Specific_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Loc_Specific_Amount.Location = new System.Drawing.Point(869, 242);
            this.Txt_Loc_Specific_Amount.Name = "Txt_Loc_Specific_Amount";
            this.Txt_Loc_Specific_Amount.NumberDecimalDigits = 3;
            this.Txt_Loc_Specific_Amount.NumberDecimalSeparator = ".";
            this.Txt_Loc_Specific_Amount.NumberGroupSeparator = ",";
            this.Txt_Loc_Specific_Amount.Size = new System.Drawing.Size(145, 22);
            this.Txt_Loc_Specific_Amount.TabIndex = 12;
            this.Txt_Loc_Specific_Amount.Text = "0.000";
            // 
            // Txt_Loc_Assets_Amount
            // 
            this.Txt_Loc_Assets_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Assets_Amount.Enabled = false;
            this.Txt_Loc_Assets_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Loc_Assets_Amount.Location = new System.Drawing.Point(869, 211);
            this.Txt_Loc_Assets_Amount.Name = "Txt_Loc_Assets_Amount";
            this.Txt_Loc_Assets_Amount.NumberDecimalDigits = 3;
            this.Txt_Loc_Assets_Amount.NumberDecimalSeparator = ".";
            this.Txt_Loc_Assets_Amount.NumberGroupSeparator = ",";
            this.Txt_Loc_Assets_Amount.Size = new System.Drawing.Size(145, 22);
            this.Txt_Loc_Assets_Amount.TabIndex = 8;
            this.Txt_Loc_Assets_Amount.Text = "0.000";
            // 
            // TxtSpecific_Amount
            // 
            this.TxtSpecific_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtSpecific_Amount.Location = new System.Drawing.Point(523, 242);
            this.TxtSpecific_Amount.Name = "TxtSpecific_Amount";
            this.TxtSpecific_Amount.NumberDecimalDigits = 3;
            this.TxtSpecific_Amount.NumberDecimalSeparator = ".";
            this.TxtSpecific_Amount.NumberGroupSeparator = ",";
            this.TxtSpecific_Amount.Size = new System.Drawing.Size(97, 22);
            this.TxtSpecific_Amount.TabIndex = 3;
            this.TxtSpecific_Amount.Text = "0.000";
            this.TxtSpecific_Amount.TextChanged += new System.EventHandler(this.TxtSpecific_Amount_TextChanged);
            // 
            // TxtTDLoc_Amount
            // 
            this.TxtTDLoc_Amount.BackColor = System.Drawing.Color.White;
            this.TxtTDLoc_Amount.Enabled = false;
            this.TxtTDLoc_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtTDLoc_Amount.Location = new System.Drawing.Point(869, 498);
            this.TxtTDLoc_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtTDLoc_Amount.Name = "TxtTDLoc_Amount";
            this.TxtTDLoc_Amount.NumberDecimalDigits = 3;
            this.TxtTDLoc_Amount.NumberDecimalSeparator = ".";
            this.TxtTDLoc_Amount.NumberGroupSeparator = ",";
            this.TxtTDLoc_Amount.Size = new System.Drawing.Size(142, 22);
            this.TxtTDLoc_Amount.TabIndex = 618;
            this.TxtTDLoc_Amount.Text = "0.000";
            // 
            // TxtTCLoc_Amount
            // 
            this.TxtTCLoc_Amount.BackColor = System.Drawing.Color.White;
            this.TxtTCLoc_Amount.Enabled = false;
            this.TxtTCLoc_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtTCLoc_Amount.Location = new System.Drawing.Point(869, 527);
            this.TxtTCLoc_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtTCLoc_Amount.Name = "TxtTCLoc_Amount";
            this.TxtTCLoc_Amount.NumberDecimalDigits = 3;
            this.TxtTCLoc_Amount.NumberDecimalSeparator = ".";
            this.TxtTCLoc_Amount.NumberGroupSeparator = ",";
            this.TxtTCLoc_Amount.Size = new System.Drawing.Size(142, 22);
            this.TxtTCLoc_Amount.TabIndex = 617;
            this.TxtTCLoc_Amount.Text = "0.000";
            // 
            // TxtCFor_Amount
            // 
            this.TxtCFor_Amount.BackColor = System.Drawing.Color.White;
            this.TxtCFor_Amount.Enabled = false;
            this.TxtCFor_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCFor_Amount.Location = new System.Drawing.Point(210, 527);
            this.TxtCFor_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtCFor_Amount.Name = "TxtCFor_Amount";
            this.TxtCFor_Amount.NumberDecimalDigits = 3;
            this.TxtCFor_Amount.NumberDecimalSeparator = ".";
            this.TxtCFor_Amount.NumberGroupSeparator = ",";
            this.TxtCFor_Amount.Size = new System.Drawing.Size(142, 22);
            this.TxtCFor_Amount.TabIndex = 615;
            this.TxtCFor_Amount.Text = "0.000";
            // 
            // TxtDFor_Amount
            // 
            this.TxtDFor_Amount.BackColor = System.Drawing.Color.White;
            this.TxtDFor_Amount.Enabled = false;
            this.TxtDFor_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtDFor_Amount.Location = new System.Drawing.Point(210, 498);
            this.TxtDFor_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtDFor_Amount.Name = "TxtDFor_Amount";
            this.TxtDFor_Amount.NumberDecimalDigits = 3;
            this.TxtDFor_Amount.NumberDecimalSeparator = ".";
            this.TxtDFor_Amount.NumberGroupSeparator = ",";
            this.TxtDFor_Amount.Size = new System.Drawing.Size(142, 22);
            this.TxtDFor_Amount.TabIndex = 609;
            this.TxtDFor_Amount.Text = "0.000";
            // 
            // TxtQty
            // 
            this.TxtQty.AllowSpace = false;
            this.TxtQty.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtQty.Location = new System.Drawing.Point(285, 242);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Size = new System.Drawing.Size(74, 22);
            this.TxtQty.TabIndex = 2;
            // 
            // Txt_Amount
            // 
            this.Txt_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Amount.Location = new System.Drawing.Point(86, 211);
            this.Txt_Amount.Name = "Txt_Amount";
            this.Txt_Amount.NumberDecimalDigits = 3;
            this.Txt_Amount.NumberDecimalSeparator = ".";
            this.Txt_Amount.NumberGroupSeparator = ",";
            this.Txt_Amount.Size = new System.Drawing.Size(189, 22);
            this.Txt_Amount.TabIndex = 0;
            this.Txt_Amount.Text = "0.000";
            this.Txt_Amount.TextChanged += new System.EventHandler(this.Txt_Amount_TextChanged);
            // 
            // Txt_Exch_Price
            // 
            this.Txt_Exch_Price.BackColor = System.Drawing.Color.White;
            this.Txt_Exch_Price.Enabled = false;
            this.Txt_Exch_Price.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Exch_Price.Location = new System.Drawing.Point(468, 211);
            this.Txt_Exch_Price.Name = "Txt_Exch_Price";
            this.Txt_Exch_Price.NumberDecimalDigits = 3;
            this.Txt_Exch_Price.NumberDecimalSeparator = ".";
            this.Txt_Exch_Price.NumberGroupSeparator = ",";
            this.Txt_Exch_Price.Size = new System.Drawing.Size(193, 22);
            this.Txt_Exch_Price.TabIndex = 4;
            this.Txt_Exch_Price.Text = "0.000";
            // 
            // TxtBuy_Date
            // 
            this.TxtBuy_Date.Checked = false;
            this.TxtBuy_Date.CustomFormat = "dd/mm/yyyy";
            this.TxtBuy_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtBuy_Date.Location = new System.Drawing.Point(86, 244);
            this.TxtBuy_Date.Name = "TxtBuy_Date";
            this.TxtBuy_Date.ShowCheckBox = true;
            this.TxtBuy_Date.Size = new System.Drawing.Size(127, 20);
            this.TxtBuy_Date.TabIndex = 633;
            this.TxtBuy_Date.ValueChanged += new System.EventHandler(this.TxtBuy_Date_ValueChanged);
            // 
            // Assets_Daily_Buy_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 612);
            this.Controls.Add(this.TxtBuy_Date);
            this.Controls.Add(this.Txt_Loc_Specific_Amount);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.Txt_Loc_Assets_Amount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.TxtSpecific_Amount);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.TxtTDLoc_Amount);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.TxtTCLoc_Amount);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.TxtCFor_Amount);
            this.Controls.Add(this.Cbo_Cust_Type);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtDFor_Amount);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TxtQty);
            this.Controls.Add(this.TxTNote);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.CboSec);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.GrdAssest);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Grd_Cust_Id);
            this.Controls.Add(this.Grd_Cur_Id);
            this.Controls.Add(this.Txt_Description);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_Amount);
            this.Controls.Add(this.Txt_Exch_Price);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Grd_Acc_Id);
            this.Controls.Add(this.TxtCurr_Name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_Cust);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_Acc);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Assets_Daily_Buy_Add";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "450";
            this.Text = "Assets_Daily_Buy_Add";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Buy_cash_assest_Add_FormClosed);
            this.Load += new System.EventHandler(this.Buy_cash_assest_Add_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAssest)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtCurr_Name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_Cust;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_Acc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView Grd_Acc_Id;
        private System.Windows.Forms.TextBox Txt_Description;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Exch_Price;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView Grd_Cur_Id;
        private System.Windows.Forms.DataGridView Grd_Cust_Id;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.DataGridView GrdAssest;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CboSec;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxTNote;
        private System.Windows.Forms.Label label13;
        private NumericTextBox TxtQty;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDFor_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox Cbo_Cust_Type;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Sample.DecimalTextBox TxtCFor_Amount;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Sample.DecimalTextBox TxtTCLoc_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtTDLoc_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtSpecific_Amount;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Loc_Assets_Amount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Loc_Specific_Amount;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DateTimePicker TxtBuy_Date;
    }
}