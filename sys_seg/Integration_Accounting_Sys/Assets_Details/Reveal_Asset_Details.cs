﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Asset_Details : Form
    {
        string SqlTxt = "";
        bool Change = false;
        public Search_Reveal_Assets_details LaunchOrigin { get; set; }
        String Full_Symbol = "";
        DataTable DT = new DataTable();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        BindingSource Bs = new BindingSource();
        BindingSource BSGrd = new BindingSource();
        public static string Vo_no_Str = "";
        public static string Nrec_date_Str = "";
        public Reveal_Asset_Details()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Hst_Assets_Details.AutoGenerateColumns = false;
            Grd_Assets_Details.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {

                Column5.DataPropertyName = "Acc_Ename";
                Column6.DataPropertyName = "Sec_ename";
                Column14.DataPropertyName = "Ecust_Name";
                Column17.DataPropertyName = "Sec_ename";
                Column22.DataPropertyName = "EDescription";
            }

        }
        //--------------------------------------------------------------
        private void Asset_Details_Terminals_Main_Load(object sender, EventArgs e)
        {
            SearchBtn_Click(null, null);
        }

        //--------------------------------------------------------------
        private void Asset_Details_Terminals_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;


            string[] Used_Tbl = { "assets_tbl", "assets_tbl1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //--------------------------------------------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------

        private void Btn_Browser_Click(object sender, EventArgs e)
        {

            if (connection.SQLDS.Tables["TBL_Assets_drop_details1"].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DataTable Exp_Dt = connection.SQLDS.Tables["TBL_Assets_drop_details1"].DefaultView.ToTable(false, "Full_symbol", "CUST_ID", connection.Lang_id == 1 ? "Acust_name" : "Ecust_name",
                                                                                              "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "sec_aname" : "sec_ename", "Asset_amount", "Specific_Amount",
                                                                                              "Total_Amount", "Buy_date", connection.Lang_id == 1 ? "Description2" : "Description2").Select().CopyToDataTable();
                DataTable[] _EXP_DT = { Date_Tbl, Exp_Dt };
                DataGridView[] Export_GRD = { Date_Grd, Grd_Hst_Assets_Details };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, DT, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف المسقط والمباع" : "Assets Drop & Sale Reveal ");
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد موجودات مسقطه او مباعة" : " No Assets for Buying or drop", MyGeneral_Lib.LblCap);
                return;

            }
        }

        private void Prt_Btn_Click(object sender, EventArgs e)
        {

            DataTable Exp_Dt = connection.SQLDS.Tables["TBL_Assets_drop_details"].DefaultView.ToTable(false, "Full_symbol", "CUST_ID", connection.Lang_id == 1 ? "Acust_name" : "Ecust_name",
                                                                                          "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "sec_aname" : "sec_ename", "Asset_amount", "Specific_Amount",
                                                                                          "Total_Amount", "Buy_date", connection.Lang_id == 1 ? "Description2" : "Description2").Select().CopyToDataTable();


            DataTable[] _EXP_DT = { Date_Tbl, Exp_Dt };
            DataGridView[] Export_GRD = { Date_Grd, Grd_Assets_Details };
            RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, DT, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? " كشف الموجودات" : "Assets Reveal ");
            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
        }
        //-------------------------


        private void Grd_Assets_Details_SelectionChanged(object sender, EventArgs e)
        {

            LblRec.Text = connection.Records(Bs);

            try
            {
                string full_sympol = Convert.ToString(((DataRowView)Bs.Current).Row["Full_symbol"]);

                BSGrd.DataSource = connection.SQLDS.Tables["assets_tbl1"].Select("Full_symbol like'" + full_sympol + "'").CopyToDataTable();

                Grd_Hst_Assets_Details.DataSource = BSGrd;
            }

            catch { Grd_Hst_Assets_Details.DataSource = new BindingSource(); }

        }


        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            DataTable ExpDt_Term = new DataTable();
            if (Txt_FullSymbol.Text.Trim() != "")
            {
                 ExpDt_Term = connection.SQLDS.Tables["assets_tbl"].DefaultView.ToTable(false, "assets_State", "Full_symbol", connection.Lang_id == 1 ? "sec_aname" : "sec_ename",
                "Description1", "Description2", "dep_month", "dep_year", "cdep_amount", "cdep_period", "Buy_date", "Acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName",
                "CUST_ID", "Asset_amount", "Specific_Amount", "book_Amount", "t_id", connection.Lang_id == 1 ? "Acust_name" : "Ecust_name").Select("Full_symbol = '" + Txt_FullSymbol.Text.Trim() + "'").CopyToDataTable();
            }

            if (Txt_FullSymbol.Text.Trim() == "")
            {
                 ExpDt_Term = connection.SQLDS.Tables["assets_tbl"].DefaultView.ToTable(false, "assets_State", "Full_symbol", connection.Lang_id == 1 ? "sec_aname" : "sec_ename",
                "Description1", "Description2", "dep_month", "dep_year", "cdep_amount", "cdep_period", "Buy_date", "Acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName",
                "CUST_ID", "Asset_amount", "Specific_Amount", "book_Amount", "t_id", connection.Lang_id == 1 ? "Acust_name" : "Ecust_name").Select().CopyToDataTable();
            }
            DataTable[] _EXP_DT = { ExpDt_Term };
            DataGridView[] Export_GRD = { Grd_Assets_Details };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {

            DataTable ExpDt_Assets = connection.SQLDS.Tables["assets_tbl"].DefaultView.ToTable(false, "assets_State", "Full_symbol",
            connection.Lang_id == 1 ? "sec_aname" : "sec_ename", "Description1", "Description2", "dep_month", "dep_year", "cdep_amount", "cdep_period", "Buy_date", "Acc_id",
            connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "CUST_ID", "Asset_amount", "Specific_Amount", "book_Amount", "t_id", connection.Lang_id == 1 ? "Acust_name" : "Ecust_name")
           .Select("Full_symbol like'" + ((DataRowView)Bs.Current).Row["Full_symbol"] + "'").CopyToDataTable();

            DataTable ExpDt_Hst_Assets = connection.SQLDS.Tables["assets_tbl1"].DefaultView.ToTable(false, "Full_symbol", connection.Lang_id == 1 ? "sec_aname" : "sec_ename",
             "Description1", "Description2", "dep_month", "dep_year", "cdep_amount", "cdep_period", "Acc_no",
            connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "CUST_ID", "Asset_amount", "Specific_Amount", "book_Amount", "t_id", connection.Lang_id == 1 ? "Acust_name" : "Ecust_name")
            .Select("Full_symbol like'" + ((DataRowView)Bs.Current).Row["Full_symbol"] + "'").CopyToDataTable();
            DataTable[] _EXP_DT = { ExpDt_Assets, ExpDt_Hst_Assets };
            DataGridView[] Export_GRD = { Grd_Assets_Details, Grd_Hst_Assets_Details };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable DD = new DataTable();
            if (Txt_FullSymbol.Text.Trim() != "")
            {
                 DD = connection.SQLDS.Tables["assets_tbl2"].DefaultView.ToTable(false, "assets_State", "Full_symbol", connection.Lang_id == 1 ? "sec_aname" : "sec_ename",
                "Description1", "Description2", "dep_month", "dep_year", "cdep_amount", "cdep_period", "Buy_date", "Acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "CUST_ID", "Asset_amount", "Specific_Amount", "book_Amount", "t_id",
                connection.Lang_id == 1 ? "Acust_name" : "Ecust_name").Select("Full_symbol = '" + Txt_FullSymbol.Text.Trim() + "'").CopyToDataTable();
            }

            if (Txt_FullSymbol.Text.Trim() == "")
            {
                 DD = connection.SQLDS.Tables["assets_tbl2"].DefaultView.ToTable(false, "assets_State", "Full_symbol", connection.Lang_id == 1 ? "sec_aname" : "sec_ename",
                "Description1", "Description2", "dep_month", "dep_year", "cdep_amount", "cdep_period", "Buy_date", "Acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "CUST_ID", "Asset_amount", "Specific_Amount", "book_Amount", "t_id",
                connection.Lang_id == 1 ? "Acust_name" : "Ecust_name").Select().CopyToDataTable();
            }


            DD.TableName = "DD";
            DataTable[] _EXP_DT = { DD };
            DataGridView[] Export_GRD = { Grd_Assets_Details };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Vo_no_Str = "";
                Nrec_date_Str = "";
                MyGeneral_Lib.ColumnToString(connection.SQLDS.Tables["assets_tbl"].DefaultView.ToTable(true, "Vo_no", "oper_id").Select("oper_id = 42").CopyToDataTable(), "Vo_no", out  Vo_no_Str);
                MyGeneral_Lib.ColumnToString(connection.SQLDS.Tables["assets_tbl"].DefaultView.ToTable(true, "Dep_Nrec_date", "oper_id").Select("oper_id = 42").CopyToDataTable(), "Dep_Nrec_date", out  Nrec_date_Str);
                Reveal_Asset_Details_Record Frm = new Reveal_Asset_Details_Record();
                this.Visible = false;
                Frm.ShowDialog();
                this.Visible = true;
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لايوجد اندثارات لهذا الشهر" : "there is no Assets for this month", MyGeneral_Lib.LblCap);
            }
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {

            if (connection.SQLDS.Tables["assets_tbl"].Rows.Count > 0)
            {

                if (Txt_FullSymbol.Text.Trim() == "")
                {
                    Bs.DataSource = connection.SQLDS.Tables["assets_tbl"];
                    Grd_Assets_Details.DataSource = Bs;
                }

                else
                {
                    try
                    {
                        Bs.DataSource = connection.SQLDS.Tables["assets_tbl"].Select("Full_symbol like'" + Txt_FullSymbol.Text.Trim() + "'").CopyToDataTable();
                        Grd_Assets_Details.DataSource = Bs;
                    }

                    catch
                    {
                        Grd_Assets_Details.DataSource = new BindingSource();
                        Grd_Hst_Assets_Details.DataSource = new BindingSource();

                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط" : " there is no data perform the conditions ", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط" : " there is no data perform the conditions ", MyGeneral_Lib.LblCap);
                this.Close();
                return;
            }

            if (Search_Reveal_Assets_details.Z == 1 || Search_Reveal_Assets_details.Z == 2)
            {
                button2.Enabled = false;

            }
        }
    }
}