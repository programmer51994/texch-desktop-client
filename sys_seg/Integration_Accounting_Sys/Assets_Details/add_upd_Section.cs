﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

namespace Integration_Accounting_Sys
{
    public partial class add_upd_Section : Form
    {
        int Frm_Id = 1;
        String T_sec_Id = "";

        public add_upd_Section(int Form_Id)
        {
            InitializeComponent();
             
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Frm_Id = Form_Id;        
        }
       //-----------------------------------------------------------------
        private void Frm_Add_Upd_Cit_Load(object sender, EventArgs e)
        {

            String Sqltext = " Select B.T_id ,ACUST_NAME,ECUST_NAME from CUSTOMERS A, TERMINALS B where A.CUST_ID = B.CUST_ID"
                            +" And Cust_Flag <> 0 "
                            +" order by " + (connection.Lang_id == 1 ? "Acust_name" : "Ecust_name");
            Cbo_Id.DataSource = connection.SqlExec(Sqltext, "Term_TBL");
            Cbo_Id.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
            Cbo_Id.ValueMember = "T_id";
            if (Frm_Id == 2)
            {
                Get_Data();
            }
        }
        //---------------------------------------------------------------
        private void Get_Data()
        {
            DataRowView Drv = Main_Section._BS_Section.Current as DataRowView;
            DataRow Dr = Drv.Row;
            Txt_AName.Text = Dr.Field<string>("sec_aname");
            Txt_EName.Text = Dr.Field<string>("sec_Ename");
            Cbo_Id.SelectedValue = Dr.Field<Int16>("T_ID");
            T_sec_Id = Dr.Field<String>("T_sec_Id");
            Cbo_Id.Enabled = false;
        }
        //---------------------------------------------------------------
        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Convert.ToInt16(Cbo_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الفرع اولا" : "Please select Terminals ", MyGeneral_Lib.LblCap);
                Txt_AName.Focus();
                return;
            }
            if (Txt_AName.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please Insert Arabic Name", MyGeneral_Lib.LblCap);
                Txt_AName.Focus();
                return;
            }
            if (Txt_EName.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاسم الاجنبي" : "Please Insert English Name", MyGeneral_Lib.LblCap);
                Txt_EName.Focus();
                return;
            }
#endregion
            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Convert.ToInt32(Cbo_Id.SelectedValue));
            ItemList.Insert(1, Txt_AName.Text.Trim().ToUpper());
            ItemList.Insert(2, Txt_EName.Text.Trim().ToUpper());
            ItemList.Insert(3, T_sec_Id);
            ItemList.Insert(4, connection.user_id);
            ItemList.Insert(5, Frm_Id);   // Form_Id
            ItemList.Insert(6, "");
            connection.scalar("Add_Upd_Section", ItemList);
            if (connection.SQLCMD.Parameters["@param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                Txt_AName.Text = "";
                Txt_EName.Text = "";
                Txt_AName.Focus();
            }
            else
                this.Close();
        }
        //---------------------------------------------------------------
        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------- 
        private void Frm_Add_Upd_Cit_FormClosed(object sender, FormClosedEventArgs e)
        {
          
            if (connection.SQLDS.Tables.Contains("Term_TBL"))
                connection.SQLDS.Tables["Term_TBL"].Clear();
        }
        //---------------------------------------------------------------
        private void add_upd_Cit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //---------------------------------------------------------------
        private void add_upd_Cit_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Ok_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //---------------------------------------------------------------
        private void Txt_AName_Leave(object sender, EventArgs e)
        {
            Txt_EName.Text = Txt_AName.Text != "" ? MyGeneral_Lib.TranslateText(Txt_AName.Text, "ar", "en") : "";
            if (Txt_EName.Text != "-1")
                Internet_connect.Visible = false;
            else
            {
                Internet_connect.Visible = true;
                Txt_EName.Text = "";
            }
        }
    }
}