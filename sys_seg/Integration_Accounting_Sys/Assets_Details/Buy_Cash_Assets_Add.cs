﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using Integration_Accounting_Sys.Report;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Buy_Cash_Assets_Add : Form
    {
        #region MyRegion
        bool Change = false;
        bool ACCChange = false;
        bool CustChange = false;
        bool CurChange = false;
        string SqlTxt = "";
        string Ord_Strt = "";
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CurId = new BindingSource();
        BindingSource Bs_CustId = new BindingSource();
        BindingSource Bs_Sec_ID = new BindingSource();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        DataTable Asset_Voucher = new DataTable();
        string Cur_ToWord = "";
        string Cur_ToEWord = "";
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        int VO_NO = 0;
        string _Date = "";
        DateTime C_Date;
        string Acc_Rpt = "";
        string Acc_ERpt = "";
        string Cust_Rpt = "";
        string Cust_ERpt = "";
        string DB_Acc_Cur_Cust = "";
        string DB_EAcc_Cur_Cust = "";
        string Cur_Code_Rpt = "";
        string @format = "dd/MM/yyyy";
        string @format_null = "0000/00/00";

        #endregion

        public Buy_Cash_Assets_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            GrdAssets.AutoGenerateColumns = false;
            Grdcur_id.AutoGenerateColumns = false;
            Create_Table();
            if (connection.Lang_id != 1)
            {
                Grd_Acc_Id.Columns["column10"].DataPropertyName = "Acc_ename";
                Grd_Cust_Id.Columns["Column12"].DataPropertyName = "Ecust_name";
                dataGridViewTextBoxColumn2.DataPropertyName = "Cur_Ename";

            }
        }
        //------------------------------------------------------------------------
        private void Buy_cash_assest_Add_Load(object sender, EventArgs e)
        {
            LblRec.Text = "";
            CurChange = false;
            TxtBuy_Date_ValueChanged(null, null);
            SqlTxt = "Select sec_aname,sec_ename,sec_id,T_sec_id from sections where T_ID = " + connection.T_ID
                    + "Order By " + (connection.Lang_id == 1 ? "sec_Aname" : "sec_ename");
            Bs_Sec_ID.DataSource = connection.SqlExec(SqlTxt, "Sec_Tbl");
            CboSec.DataSource = Bs_Sec_ID;
            CboSec.ValueMember = "Sec_id";
            CboSec.DisplayMember = connection.Lang_id == 1 ? "Sec_Aname" : "Sec_Ename";
            if (connection.SQLDS.Tables["Sec_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب تعريف قطاعات اولا" : "Please Define Section First", MyGeneral_Lib.LblCap);
                this.Close();
            }

            Change = true;
            Txt_Acc_TextChanged(sender, e);

        }
        //------------------------------------------------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                ACCChange = false;
                object[] sparam = { 0, Txt_Acc.Text.Trim(), connection.T_ID, connection.user_id, 0 };
                SqlTxt = "SELECT ACC_Id,ACC_AName,Acc_EName,Sub_flag  FROM Account_Tree "
                         + " Where Record_flag = 1"
                         + " And ACC_Id   in (Select Acc_Id From CUSTOMERS_ACCOUNTS"
                         + " Where CUST_ID in(Select CUST_ID From CUSTOMERS Where Cust_Type_ID in(50) And Cust_Flag <> 0)"
                         + " And T_Id =" + connection.T_ID
                         + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth"
                         + " Where Auth_User_Id =" + connection.user_id
                         + " And VC_Sw = 1) And CUR_ID =" + Txt_Loc_Cur.Tag + " And CUS_STATE_ID <> 0 )"
                         + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth"
                         + " Where Auth_User_Id =" + connection.user_id
                         + " And VC_Sw = 1)"
                        + " And (ACC_AName like '" + Txt_Acc.Text + "%' Or ACC_EName Like '" + Txt_Acc.Text + "%' Or ACC_Id Like '" + Txt_Acc.Text + "')"
                        + " Order By " + (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename");

                Bs_AccId.DataSource = connection.SqlExec(SqlTxt, "Tbl_Acc_Name");

                Grd_Acc_Id.DataSource = Bs_AccId;

                if (connection.SQLDS.Tables["Tbl_Acc_Name"].Rows.Count > 0)
                {
                    ACCChange = true;
                    Grd_Acc_Id_SelectionChanged(sender, e);
                }
                else
                {
                    Grd_Cust_Id.DataSource = null;
                    Grdcur_id.DataSource = null;
                }
            }
        }
        //------------------------------------------------------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                Txt_Cust.Text = "";
                Txt_Cur.Text = "";
                Txt_Cur_TextChanged(sender, e);
                Txt_Cust.Text = "";
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                if (Grd_Acc_Id.RowCount > 0)
                {
                    Bs_CustId.DataSource = connection.SqlExec("Get_Cust_Info ", "Cust_Name_Tbl",
                               new object[] { ((DataRowView)Bs_CurId.Current).Row["Cur_Id"],
                           Txt_Cust.Text.Trim(), connection.T_ID, ((DataRowView)Bs_AccId.Current).Row["Acc_Id"],Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"])});

                    Grd_Cust_Id.DataSource = Bs_CustId;
                }
            }
            //if (cccACCChange)
            //{
            //    CustChange = false;
            //    string SqlTxt = "SELECT Acust_Name,Ecust_name,A.Cust_Id "
            //                    + " From Customers_Accounts A, Customers B "
            //                    + " Where  A.Cust_Id = B.Cust_Id "
            //                    + " And A.Cus_State_Id = 1 "
            //                    + " And  ACC_Id = " + ((DataRowView)Bs_AccId.Current).Row["Acc_Id"]
            //                    + " AND A.T_Id =  " + connection.T_ID
            //                    + " And Cust_Flag  <> 0 "
            //                    + " And A.CUS_STATE_ID <> 0"
            //                    + " And A.Cur_Id = " + Txt_Loc_Cur.Tag
            //                    + " And (ACUST_NAME Like '" + Txt_Cust.Text + "%' "
            //                                + " Or ECUST_NAME Like '" + Txt_Cust.Text + "%'"
            //                                + " Or A.CUST_ID Like '" + Txt_Cust.Text + "%')"
            //                   + " Order by " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");

            //    Bs_CustId.DataSource = connection.SqlExec(SqlTxt, "Tbl_Cust_Name");

            //    Grd_Cust_Id.DataSource = Bs_CustId;
            //}
        }
        //------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validations


            if (Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب رجاءا" : "Please Select Account", MyGeneral_Lib.LblCap);
                Txt_Acc.Focus();
                return;
            }
            if (Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"]) == 1)
            {
                if (Bs_CustId.List.Count > 0)
                {
                    if (Convert.ToInt16(((DataRowView)Bs_CustId.Current).Row["Cust_Id"]) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "الحساب للثانوي غير معرف في جدول الحسابات المركزية لهذه العملة" :
                                                            "Customer Account Unknown at Central Account For this Currency", MyGeneral_Lib.LblCap);
                        Txt_Cust.Focus();
                        return;
                    }
                }
            }
            if (Convert.ToDecimal(Txt_Exch_Price.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل سعر صرف العملة" : "Please Insert Exchange Price", MyGeneral_Lib.LblCap);
                Txt_Exch_Price.Focus();
                return;
            }
            if (Convert.ToDecimal(Txt_Amount.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Please Insert Amount", MyGeneral_Lib.LblCap);
                Txt_Amount.Focus();
                return;
            }
            if (TxtQty.Text == "" || Convert.ToInt16(TxtQty.Text) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الكمية" : "Please Insert Quantity please", MyGeneral_Lib.LblCap);
                TxtQty.Focus();
                return;
            }
            if (Txt_Detl.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الوصف الدقيق" : "Please Insert Description please", MyGeneral_Lib.LblCap);
                Txt_Detl.Focus();
                return;
            }
           // Ord_Strt = MyGeneral_Lib.DateChecking(TxtBuy_Date.Text);
            if (TxtBuy_Date.Text == "0000/00/00" || TxtBuy_Date.Text == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ الشراء" : "Please Insert Date of Buy", MyGeneral_Lib.LblCap);
                TxtBuy_Date.Focus();
                return;
            }
          //  Ord_Strt = MyGeneral_Lib.DateChecking(TxtBuy_Date.Text);
            Decimal LocAmount = decimal.Round(Convert.ToDecimal(Txt_Amount.Text) * Convert.ToDecimal(Txt_Exch_Price.Text), 3);
            LocAmount = LocAmount - Convert.ToDecimal(TxtSpecific_Amount.Text);
            if (LocAmount < 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "المخصص المتراكم اكبر من القيمة الاسمية!" : "Specific Amount more than Amount!", MyGeneral_Lib.LblCap);
                TxtSpecific_Amount.Focus();
                return;
            }

            int Rec_No = (from DataRow row in Asset_Voucher.Rows
                          where
                             (decimal)row["For_Amount"] == Convert.ToDecimal(Txt_Amount.Text) * Convert.ToInt16(TxtQty.Text)
                             && (int)row["For_cur_id"] == Convert.ToInt16(((DataRowView)Bs_CurId.Current).Row["cur_id"])
                             && (int)row["Cust_Id"] == Convert.ToInt16(((DataRowView)Bs_CustId.Current).Row["Cust_Id"])
                             && (int)row["Acc_Id"] == Convert.ToInt16(((DataRowView)Bs_AccId.Current).Row["Acc_Id"])
                             && (decimal)row["Specific_Amount"] == Convert.ToDecimal(TxtSpecific_Amount.Text)
                             && (int)row["Qty"] == Convert.ToInt16(TxtQty.Text)
                          select row).Count();

            if (Rec_No > 0 && Asset_Voucher.Rows.Count > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "القيود متشابه!" : "Duplicate Record!", MyGeneral_Lib.LblCap);

                return;
            }
            #endregion

            #region Add Rows To  Table
            decimal For_Amount = Convert.ToDecimal(Txt_Amount.Text) * Convert.ToInt16(TxtQty.Text);
            decimal loc_amount = decimal.Round(For_Amount * Convert.ToDecimal(Txt_Exch_Price.Text), 3);
            DataRow DRow = Asset_Voucher.NewRow();
            DRow["For_Amount"] = For_Amount;
            DRow["Acc_Name"] = ((DataRowView)Bs_AccId.Current).Row[connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename"];
            DRow["Acc_Id"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Id"];
            DRow["Cur_Name"] = ((DataRowView)Bs_CurId.Current).Row[connection.Lang_id == 1 ? "Cur_Aname" : "Cur_ename"];
            if (Bs_CustId.List.Count > 0)
            {
                DRow["Cust_Name"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"];
                DRow["Cust_id"] = ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
            }
            DRow["Exch_Price"] = Txt_Exch_Price.Text;
            DRow["Real_Price"] = Txt_Exch_Price.Text;
            DRow["Loc_Amount"] = loc_amount;
            DRow["Description2"] = Txt_Detl.Text;
            DRow["Description1"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"];
            DRow["Qty"] = TxtQty.Text.Trim();
            DRow["Sec_Name"] = CboSec.Text;
            DRow["Sec_Id"] = Convert.ToInt16(CboSec.SelectedValue);
            DRow["T_sec_id"] = ((DataRowView)Bs_Sec_ID.Current).Row["T_sec_id"];
            DRow["For_cur_id"] = ((DataRowView)Bs_CurId.Current).Row["cur_id"];
            DRow["loc_cur_id"] = Txt_Loc_Cur.Tag;
            string Notes = Convert.ToInt16(TxtQty.Text) > 0 ? " الكمية _ " + TxtQty.Text : "";
            DRow["Notes"] = TxTNote.Text.Trim() + Notes;
            string Buy_Date = TxtBuy_Date.Value.ToString("yyyy/MM/dd");
            DRow["Buy_Date"] = TxtBuy_Date.Text;
            DRow["Specific_Amount"] = Convert.ToDecimal(TxtSpecific_Amount.Text) * Convert.ToInt16(TxtQty.Text);
            DRow["Qty_id"] = TxtQty.Text;
            //------------------Report

            Acc_Rpt = ((DataRowView)Bs_AccId.Current).Row["Acc_Aname"].ToString();
            Acc_ERpt = ((DataRowView)Bs_AccId.Current).Row["Acc_Ename"].ToString();
            Cur_Code_Rpt = ((DataRowView)Bs_CurId.Current).Row["Cur_Code"].ToString();
            Cust_Rpt = ((DataRowView)Bs_CustId.Current).Row["ACust_Name"].ToString();
            Cust_ERpt = ((DataRowView)Bs_CustId.Current).Row["ECust_Name"].ToString();
            DB_Acc_Cur_Cust = Cust_Rpt + "/" + Cur_Code_Rpt + "/" + Acc_Rpt;
            DB_EAcc_Cur_Cust = Acc_ERpt = "/" + Cur_Code_Rpt + "/" + Cust_ERpt;
            DRow["DB_Acc_Cur_Cust"] = DB_Acc_Cur_Cust;
            DRow["DB_EAcc_Cur_Cust"] = DB_EAcc_Cur_Cust;
            //-----------------------
            Asset_Voucher.Rows.Add(DRow);
            _Bs.DataSource = Asset_Voucher;
            #endregion
            Change = false;
            ACCChange = false;
            CustChange = false;
            Txt_Acc.Text = string.Empty;
            Grd_Acc_Id.ClearSelection();
            Txt_Cust.Text = string.Empty;
            Grd_Cust_Id.ClearSelection();
            Txt_Amount.ResetText();
            TxtSpecific_Amount.ResetText();
            Txt_Detl.Text = "";
            TxtQty.Text = "";
            TxTNote.Text = "";
            Change = true;
            ACCChange = true;
            CustChange = true;
            Txt_Acc.Focus();
        }
        //------------------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price","Cur_Name","Acc_Name","Cust_Name",
                        "Description1","Description2","Sec_name","Qty","Sec_Id","Buy_Date","Specific_Amount","Full_Symbol","T_Sec_Id","Qty_id" 
                        ,"DB_Acc_Cur_Cust","DB_EAcc_Cur_Cust","Cur_ToEWord","Cur_ToWord"
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal", "System.String", "System.String", "System.String",
                "System.String","System.String","System.String","System.Int32","System.Int32","System.DateTime","System.Decimal","System.String","System.String","System.Int16", 
                "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String"
            };

            Asset_Voucher = CustomControls.Custom_DataTable("Asset_Voucher", Column, DType);
            GrdAssets.DataSource = _Bs;
        }
        //------------------------------------------------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Asset_Voucher.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد" :
                    "Are you sure you want to remove this record", MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    Asset_Voucher.Rows[GrdAssets.CurrentRow.Index].Delete();
                    GrdAssets.Refresh();

                }
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قيود للحذف" :
                       "No records for delete", MyGeneral_Lib.LblCap);
            }
        }
        //-------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No opening balances", MyGeneral_Lib.LblCap);
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No Record Organizer", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(TxtBox_User.Tag) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد امين صندوق معرف " : "No Treasurer Found", MyGeneral_Lib.LblCap);
                return;
            }
            if (Asset_Voucher.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "No Records For Adding", MyGeneral_Lib.LblCap);
                return;
            }

            #endregion

            DataTable Dt = new DataTable();
            Dt = Asset_Voucher.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "Description1", "Description2", "Sec_id", "Full_symbol", "Buy_date", "T_Sec_ID",
                              "Specific_Amount", "Qty_id");
            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Cust_Id", TxtBox_User.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@LOC_CUR_ID", Txt_Loc_Cur.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 37);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Cash_Asset_Voucher", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                   !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Change = false;
            ACCChange = false;
            CustChange = false;
            Txt_Acc.ResetText();
            Txt_Amount.ResetText();
            Txt_Cust.ResetText();
            TxTNote.ResetText();
            TxtQty.ResetText();
            TxtBuy_Date.Text = "00000000";
            TxtSpecific_Amount.ResetText();
            Change = true;
            ACCChange = true;
            CustChange = true;
            Asset_Voucher.Clear();

        }
        //-------------------------
        private void GrdAssest_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }
        //-------------------------
        private void Buy_cash_assest_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            ACCChange = false;
            CustChange = false;
            CurChange = false;
            string[] Used_Tbl = { "Tbl_Cur_Acc", "Tbl_Acc_Name", "Tbl_Cust_Name", "Sec_Tbl", "Asset_Details_Tbl", "Asset_Voucher" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Cur_TextChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                CurChange = false;
                Bs_CurId.DataSource = connection.SqlExec("Get_Cur_Rap_Spend ", "Cur_Acc_Tbl",
                new object[] { Txt_Loc_Cur.Tag, Txt_Cur.Text.Trim(), connection.T_ID, 
                 Convert.ToDecimal(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]), 
                TxtBox_User.Tag ,Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"])});

                Grdcur_id.DataSource = Bs_CurId;
                if (connection.SQLDS.Tables["Cur_Acc_Tbl"].Rows.Count > 0)
                {
                    CurChange = true;
                    Grdcur_id_SelectionChanged(sender, e);
                }
                else
                {
                    Grd_Cust_Id.DataSource = null;
                }

                //SqlTxt = "	SELECT Cur_AName,Cur_EName,A.Cur_id as For_Cur_Id,Cur_Code,Exch_Rate	"
                //       + " FROM Cur_Tbl A ,Currencies_Rate B "
                //       + " where A.Cur_id = B.Cur_ID "
                //       + " And A.Cur_id in (Select Cur_ID from CUSTOMERS_ACCOUNTS "
                //       + " Where Cust_id = " + TxtBox_User.Tag
                //       + " And CUS_STATE_ID <> 0)"
                //       + " union "
                //       + " SELECT Cur_AName,Cur_EName,Cur_id as For_Cur_Id,Cur_Code,1 as Exch_Rate "
                //       + " FROM Cur_Tbl where  Cur_id = " + Txt_Loc_Cur.Tag
                //   + " order by " + (connection.Lang_id == 1 ? "Cur_aname" : "Cur_Ename");
                //connection.SQLBSCbo.DataSource = connection.SqlExec(SqlTxt, "Tbl_Cur_Acc");
                //CboCur.DataSource = connection.SQLBSCbo;
                //CboCur.ValueMember = "For_Cur_Id";
                //CboCur.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
                //if (connection.SQLDS.Tables["Tbl_Cur_Acc"].Rows.Count > 0)
                //{
                //    CurChange = true;
                //    CboCur_SelectedIndexChanged(null, null);
                //}
            }
        }
        //------------------------------------------------------------------------
        private void Grdcur_id_SelectionChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                label18.Text = "(" + ((DataRowView)Bs_CurId.Current).Row[connection.Lang_id == 1 ? "Cur_aname" : "Cur_Ename"].ToString() + ")";
                Txt_Exch_Price.Text = ((DataRowView)Bs_CurId.Current).Row["Exch_Rate"].ToString();
                Txt_Amount_TextChanged(null, null);
                TxtSpecific_Amount_TextChanged(null, null);
                Txt_Cust_TextChanged(sender, e);
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Amount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Txt_Loc_Assets_Amount.Text = (Convert.ToDecimal(Txt_Amount.Text) * Convert.ToDecimal(Txt_Exch_Price.Text)).ToString();
            }
            catch
            { }
        }
        //------------------------------------------------------------------------
        private void TxtSpecific_Amount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Txt_Loc_Specific_Amount.Text = (Convert.ToDecimal(TxtSpecific_Amount.Text) * Convert.ToDecimal(Txt_Exch_Price.Text)).ToString();
            }
            catch
            { }

        }
        //------------------------------------------------------------------------
        private void print_Rpt_Pdf()
        {

            string SqlTxt = "Exec Main_Report " + VO_NO + "," + TxtIn_Rec_Date.Text + ",37," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            string SqlTxt1 = "Exec Main_Report_Assets_Details " + VO_NO + "," + TxtIn_Rec_Date.Text + ",37," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Asset_Voucher");
            connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

            Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
            Main_Assets_Rpt_Eng ObjRptEng = new Main_Assets_Rpt_Eng();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 37);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

            DataRow Dr;
            int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 37);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Asset_Voucher");
            connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void TxtBuy_Date_ValueChanged(object sender, EventArgs e)
        {
            if (TxtBuy_Date.Checked == true)
            {
                TxtBuy_Date.Format = DateTimePickerFormat.Custom;
                TxtBuy_Date.CustomFormat = @format;
            }
            else
            {
                TxtBuy_Date.Format = DateTimePickerFormat.Custom;
                TxtBuy_Date.CustomFormat = @format_null;
            }
        }
    }
}