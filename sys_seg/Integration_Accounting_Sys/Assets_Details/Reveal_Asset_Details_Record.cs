﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Asset_Details_Record : Form
    {
        #region Definition
        string SqlTxt = "";
        BindingSource BS_GrdVou_Details = new BindingSource();
        #endregion
        public Reveal_Asset_Details_Record()
        {
            InitializeComponent();
            connection.SQLBSGrd.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            GrdVou_Details.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
                Column7.DataPropertyName = "Eterm_Name";
                Column3.DataPropertyName = "Acc_EName";
                Column5.DataPropertyName = "ECUST_NAME";
            }


        }
        //------------------------------------------
        private void Reveal_Asset_Details_Record_Load(object sender, EventArgs e)
        {
            SqlTxt = "select a.VO_NO,a.ACC_Id, D.Acc_AName , D.Acc_EName ,a.CUST_ID,b.ACUST_NAME , "
                    + " b.ECUST_NAME, a.FOR_AMOUNT , cast(cast( a.NREC_DATE As varchar(8)) As Date) As NREC_DATE, c.T_id ,"
                    + " e.User_Name  , F.ACUST_NAME as Aterm_Name, F.ECUST_NAME as Eterm_Name"
                    + " from VOUCHER a ,CUSTOMERS b, TERMINALS c,USERS e , ACCOUNT_TREE D , CUSTOMERS F "
                    + " where a.CUST_ID=b.CUST_ID "
                    + " and a.T_ID = c.T_ID "
                    + " and a.USER_ID = e.USER_ID "
                    + " and a.ACC_Id = D.Acc_Id "
                    + " and a.T_ID = F.CUST_ID "
                    + " and  a.NREC_DATE in (" + Reveal_Asset_Details.Nrec_date_Str + ") "
                    + " and a.T_ID in (" + Search_Reveal_Assets_details.T_IDStr + ") "
                    + " and a.VO_NO in (" + Reveal_Asset_Details.Vo_no_Str + ") "
                    + " and a.OPER_ID = 42 ";

            BS_GrdVou_Details.DataSource = connection.SqlExec(SqlTxt, "voucher_Tbl");
            GrdVou_Details.DataSource = BS_GrdVou_Details;
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            DataTable ExpDt_Term = connection.SQLDS.Tables["voucher_Tbl"].DefaultView.ToTable(false,  "T_id", connection.Lang_id == 1 ? "Aterm_Name" : "Eterm_Name",
                "Vo_no","ACC_Id", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "FOR_AMOUNT", "CUST_ID",
                connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME", "NREC_DATE", "User_Name").Select().CopyToDataTable();
            DataTable[] _EXP_DT = { ExpDt_Term };
            DataGridView[] Export_GRD = { GrdVou_Details };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

        }

        private void GrdVou_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(BS_GrdVou_Details);
        }

       
    }
}
