﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Sale_Cash_Assets_Add : Form
    {
        #region Definition
        DataTable Asset_Tbl;
        string Sql_Txt;
        int VO_NO = 0;
        #endregion
        //---------------------------------
        public Sale_Cash_Assets_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Assets.AutoGenerateColumns = false;
            Assets_Table();
        }
        //---------------------------------
        private void Assets_Table()
        {
            string[] Column =
            {
                "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Description1","Description2","Sec_id","Full_symbol","Buy_date ",
                "Asset_amount",  "Specific_Amount", "Acc_Name",  "Cust_name", "Sec_Name", "description","T_SEC_ID","Qty_id"
            };
            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte","System.String","System.String","System.Byte","System.String","System.DateTime",
                "System.Decimal","System.Decimal","System.String","System.String","System.String","System.String","System.Int32","System.Int32"
            };
            Asset_Tbl = CustomControls.Custom_DataTable("Asset_Tbl", Column, DType);
            Grd_Assets.DataSource = Asset_Tbl;
        }
        //---------------------------------
        private void Sale_Cash_Assets_Load(object sender, EventArgs e)
        {
            Sql_Txt = "	SELECT Cur_AName,Cur_EName,A.Cur_id as For_Cur_Id,Exch_Rate	"
                    + " FROM Cur_Tbl A ,Currencies_Rate B "
                    + " where A.Cur_id = B.Cur_ID "
                    + " And A.Cur_id in (Select Cur_ID from dbo.CUSTOMERS_ACCOUNTS "
                    + " Where   Cust_id = " + TxtBox_User.Tag + " And CUS_STATE_ID <> 0)"
                    + " union "
                    + " SELECT Cur_AName,Cur_EName,Cur_id as For_Cur_Id,1 as Exch_Rate "
                    + " FROM Cur_Tbl where  Cur_id = " + Txt_Loc_Cur.Tag
                    + " order by " + (connection.Lang_id==1?"Cur_aname":"Cur_Ename");
            Cbo_Cur.DataSource =  connection.SqlExec(Sql_Txt , "Cur_Tbl");
            Cbo_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_aname" : "Cur_ename";
            Cbo_Cur.ValueMember = "For_Cur_id";
        }
        //---------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            #region Valdition
            if (Txt_FullSymbol.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الرمز رجاءاً" : "Enter The Symbol Please ", MyGeneral_Lib.LblCap);
                Txt_FullSymbol.Focus();
                return;
            }
            #endregion
            object[] Sparam = { Txt_FullSymbol.Text.Trim() ,connection.T_ID };
            connection.SqlExec("Sale_Assets_details", "Details_Tbl", Sparam);
            if (connection.SQLDS.Tables["Details_Tbl"].Rows.Count <= 0)
            {
                TxtAsset_Amount.ResetText();
                TxtSpecific_Amount.ResetText();
                TxtTotal_Amount.ResetText();
                Txt_Month_Id.ResetText();
                Txt_Month.ResetText();
                Txt_Year.ResetText();
                Txtdesicription.ResetText();
                TxtSale_amount.ResetText();
                Txt_Notes.ResetText();
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد نتائج للبحث" : "There are no search results", MyGeneral_Lib.LblCap);
                return;
            }
            DataRow Row = connection.SQLDS.Tables["Details_Tbl"].Rows[0];
            TxtAsset_Amount.Text = Row["Asset_amount"].ToString();
            TxtSpecific_Amount.Text = Row["Specific_Amount"].ToString();
            TxtTotal_Amount.Text = Row["Total_Amount"].ToString();
            Txt_Month_Id.Text = Row["dep_month"].ToString();  
            Txt_Month.Text = Row[connection.Lang_id == 1 ? "Mon_aname" : "Mon_Ename"].ToString();
            Txt_Year.Text = Row["dep_year"].ToString();
            Txtdesicription.Text = Row[connection.Lang_id == 1 ? "ADescription" : "EDescription"].ToString();
            BtnAdd.Enabled = true;
            BtnDel.Enabled = true;
            Txt_Notes.ResetText();
            TxtSale_amount.ResetText();
        }
        //---------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if (connection.SQLDS.Tables["Details_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للإضافة" : "No Data to add", MyGeneral_Lib.LblCap);
                return;
            }

            int Recount = (from rec in Asset_Tbl.AsEnumerable()
                           where (string)rec["Full_Symbol"] == connection.SQLDS.Tables["Details_Tbl"].Rows[0]["Full_Symbol"].ToString()
                           select new { }).Count();
            if (Recount > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يمكنك بيع الموجود لأكثر من مرة" : "You can not sell the asset more than once", MyGeneral_Lib.LblCap);
                return;
            }

            if (Convert.ToDecimal(TxtSale_amount.Text) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل سعر البيع رجاءا" : "Check The Sale Amount Please", MyGeneral_Lib.LblCap);
                TxtSale_amount.Focus();
                return;
            }
            #endregion
            DataRow DRow1 = Asset_Tbl.NewRow();
            DataRow Row = connection.SQLDS.Tables["Details_Tbl"].Rows[0];
            //========
            DRow1["Acc_id"]       = Row["Acc_id"];
            DRow1["CUST_ID"]      = Row["Cust_id"];
            DRow1["For_cur_id"]   = Cbo_Cur.SelectedValue;
            DataRow[] ExchRow     = connection.SQLDS.Tables["Cur_Tbl"].Select("For_cur_id = " + Cbo_Cur.SelectedValue);
            DRow1["EXCH_price"]   = ExchRow[0]["EXCH_RATE"];
            DRow1["Real_price"]   = ExchRow[0]["EXCH_RATE"];
            DRow1["For_amount"]   = Convert.ToDecimal(TxtSale_amount.Text);
            DRow1["Loc_amount"]   = Convert.ToDecimal(TxtSale_amount.Text) * Convert.ToDecimal(ExchRow[0]["EXCH_RATE"]);
            DRow1["LOC_CUR_ID"]   = Txt_Loc_Cur.Tag;
            DRow1["Notes"]        = Txt_Notes.Text;
            DRow1["Description1"] = Row["Description1"];
            DRow1["Description1"] = Row["Description2"];
            DRow1["Full_Symbol"]  = Row["Full_Symbol"];
            DRow1["Asset_amount"] = Row["Asset_amount"];
            DRow1["Specific_Amount"] = Row["Specific_Amount"];
            DRow1["Acc_name"]     = Row[connection.Lang_id == 1 ? "Acc_AName" : "Acc_Ename"];
            DRow1["Cust_name"]    = Row[connection.Lang_id == 1 ? "ACust_name" : "Ecust_name"];
            DRow1["Sec_Name"] = Row[connection.Lang_id == 1 ? "Sec_aname" : "sec_ename"];
            DRow1["description"]  = Row[connection.Lang_id == 1 ? "ADescription" : "EDescription"];
            //========
            Asset_Tbl.Rows.Add(DRow1);
            TxtAsset_Amount.ResetText();
            TxtSpecific_Amount.ResetText();
            TxtTotal_Amount.ResetText();
            Txt_Month_Id.ResetText();
            Txt_Month.ResetText();
            Txt_Year.ResetText();
            Txtdesicription.ResetText();
            TxtSale_amount.ResetText();
            Txt_FullSymbol.ResetText();
            Txt_Notes.ResetText();
            connection.SQLDS.Tables["Details_Tbl"].Clear();
        }
        //---------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Asset_Tbl.Rows.Count > 0)
            {
                Asset_Tbl.Rows[Grd_Assets.CurrentRow.Index].Delete();
            }
        }
        //---------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No opening balances", MyGeneral_Lib.LblCap);
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No Record Organizer", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(TxtBox_User.Tag) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد امين صندوق معرف " : "No Treasurer Found", MyGeneral_Lib.LblCap);
                return;
            }
            if (Asset_Tbl.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "No Records To Add", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            DataTable Dt = new DataTable();
            Dt = Asset_Tbl.DefaultView.ToTable(false,"Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "Description1", "Description2", "Sec_id", "Full_symbol", "Buy_date ", "T_SEC_ID", "Specific_Amount", "Qty_id");
            connection.SQLCMD.Parameters.AddWithValue("@Asset_Tbl", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 39);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@box_cust", TxtBox_User.Tag);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;

            connection.SqlExec("Sale_Cash_Assets_Details", connection.SQLCMD);
           
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            
            print_Rpt_Pdf();
            Asset_Tbl.Rows.Clear();
            TxtAsset_Amount.ResetText();
            TxtSpecific_Amount.ResetText();
            TxtTotal_Amount.ResetText();
            Txt_Month_Id.ResetText();
            Txt_Month.ResetText();
            Txt_Year.ResetText();
            Txtdesicription.ResetText();
            TxtSale_amount.ResetText();
            Txt_Notes.ResetText();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLDS.Tables["Details_Tbl"].Rows.Clear();
        }
        //---------------------------------
        private void Sale_Cash_Assets_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Cur_Tbl","Details_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        private void print_Rpt_Pdf()
        {

            string SqlTxt = "Exec Main_Report " + VO_NO + "," + TxtIn_Rec_Date.Text + ",39," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            string SqlTxt1 = "Exec Main_Report_Assets_Details " + VO_NO + "," + TxtIn_Rec_Date.Text + ",39," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Asset_Voucher");
            connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

            Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
            Main_Assets_Rpt ObjRptEng = new Main_Assets_Rpt();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 39);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

            DataRow Dr;
            int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 39);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Asset_Voucher");
            connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
        }
    }
}