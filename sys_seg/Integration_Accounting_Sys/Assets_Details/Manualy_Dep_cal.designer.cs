﻿namespace Integration_Accounting_Sys
{
    partial class Manualy_Dep_cal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_User_date = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_year = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_Nrec_date = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Cbo_term = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Cbo_method = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.CHK_Dep_Reord = new System.Windows.Forms.CheckBox();
            this.Txt_month = new System.Windows.Forms.TextBox();
            this.Txt_Dep_day = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(6, 28);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(691, 2);
            this.flowLayoutPanel7.TabIndex = 491;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(96, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(209, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(537, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(160, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(473, 5);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 487;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(8, 6);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 486;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(6, 193);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(691, 1);
            this.flowLayoutPanel3.TabIndex = 690;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(351, 198);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 29);
            this.ExtBtn.TabIndex = 23;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(262, 198);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 29);
            this.AddBtn.TabIndex = 22;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 28);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 166);
            this.flowLayoutPanel6.TabIndex = 691;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(697, 30);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1, 164);
            this.flowLayoutPanel8.TabIndex = 692;
            // 
            // Txt_User_date
            // 
            this.Txt_User_date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.Txt_User_date.Location = new System.Drawing.Point(504, 46);
            this.Txt_User_date.Mask = "0000/00/00";
            this.Txt_User_date.Name = "Txt_User_date";
            this.Txt_User_date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_User_date.Size = new System.Drawing.Size(128, 23);
            this.Txt_User_date.TabIndex = 706;
            this.Txt_User_date.Text = "00000000";
            this.Txt_User_date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(397, 50);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(107, 14);
            this.label6.TabIndex = 705;
            this.label6.Text = "تاريخ تنفيذ القيــد:";
            // 
            // Txt_year
            // 
            this.Txt_year.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_year.Location = new System.Drawing.Point(205, 140);
            this.Txt_year.Multiline = true;
            this.Txt_year.Name = "Txt_year";
            this.Txt_year.ReadOnly = true;
            this.Txt_year.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_year.Size = new System.Drawing.Size(70, 20);
            this.Txt_year.TabIndex = 703;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(16, 143);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(115, 14);
            this.label7.TabIndex = 701;
            this.label7.Text = "شهر وسنة الاندثار:";
            // 
            // Txt_Nrec_date
            // 
            this.Txt_Nrec_date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.Txt_Nrec_date.Location = new System.Drawing.Point(503, 139);
            this.Txt_Nrec_date.Mask = "0000/00/00";
            this.Txt_Nrec_date.Name = "Txt_Nrec_date";
            this.Txt_Nrec_date.ReadOnly = true;
            this.Txt_Nrec_date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Nrec_date.Size = new System.Drawing.Size(129, 23);
            this.Txt_Nrec_date.TabIndex = 700;
            this.Txt_Nrec_date.Text = "00000000";
            this.Txt_Nrec_date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(407, 143);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(93, 14);
            this.label5.TabIndex = 699;
            this.label5.Text = ":تاريخ السجلات";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(18, 51);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 698;
            this.label1.Text = "الفـــــــــــرع:";
            // 
            // Cbo_term
            // 
            this.Cbo_term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_term.FormattingEnabled = true;
            this.Cbo_term.Location = new System.Drawing.Point(96, 46);
            this.Cbo_term.Name = "Cbo_term";
            this.Cbo_term.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cbo_term.Size = new System.Drawing.Size(234, 24);
            this.Cbo_term.TabIndex = 697;
            this.Cbo_term.SelectedIndexChanged += new System.EventHandler(this.Cbo_term_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(580, 107);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(75, 14);
            this.label4.TabIndex = 696;
            this.label4.Text = "من كل شهر";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(299, 107);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(233, 14);
            this.label8.TabIndex = 695;
            this.label8.Text = ":تنفيــذ قيــد الاندثــار اعتبــــــــــارا مـــــــن";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(17, 107);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(111, 14);
            this.label9.TabIndex = 694;
            this.label9.Text = "طريقــــة التنفيــــذ:";
            // 
            // Cbo_method
            // 
            this.Cbo_method.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_method.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_method.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_method.Enabled = false;
            this.Cbo_method.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_method.FormattingEnabled = true;
            this.Cbo_method.Location = new System.Drawing.Point(131, 102);
            this.Cbo_method.Name = "Cbo_method";
            this.Cbo_method.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cbo_method.Size = new System.Drawing.Size(144, 24);
            this.Cbo_method.TabIndex = 693;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 92);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(691, 1);
            this.flowLayoutPanel1.TabIndex = 691;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(7, 81);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(122, 14);
            this.label16.TabIndex = 763;
            this.label16.Text = "اعدادت المؤسسة...";
            // 
            // CHK_Dep_Reord
            // 
            this.CHK_Dep_Reord.AutoSize = true;
            this.CHK_Dep_Reord.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_Dep_Reord.ForeColor = System.Drawing.Color.Navy;
            this.CHK_Dep_Reord.Location = new System.Drawing.Point(15, 169);
            this.CHK_Dep_Reord.Name = "CHK_Dep_Reord";
            this.CHK_Dep_Reord.Size = new System.Drawing.Size(419, 18);
            this.CHK_Dep_Reord.TabIndex = 764;
            this.CHK_Dep_Reord.Text = "تنفيذ قيد الاندثارات وفتح السجلات عند عدم وجود أفتتاح لتاريخ التنفيذ";
            this.CHK_Dep_Reord.UseVisualStyleBackColor = true;
            // 
            // Txt_month
            // 
            this.Txt_month.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_month.Location = new System.Drawing.Point(138, 140);
            this.Txt_month.Multiline = true;
            this.Txt_month.Name = "Txt_month";
            this.Txt_month.ReadOnly = true;
            this.Txt_month.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_month.Size = new System.Drawing.Size(65, 20);
            this.Txt_month.TabIndex = 765;
            // 
            // Txt_Dep_day
            // 
            this.Txt_Dep_day.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Dep_day.Location = new System.Drawing.Point(536, 104);
            this.Txt_Dep_day.Multiline = true;
            this.Txt_Dep_day.Name = "Txt_Dep_day";
            this.Txt_Dep_day.ReadOnly = true;
            this.Txt_Dep_day.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Dep_day.Size = new System.Drawing.Size(40, 20);
            this.Txt_Dep_day.TabIndex = 766;
            // 
            // Manualy_Dep_cal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 229);
            this.Controls.Add(this.Txt_Dep_day);
            this.Controls.Add(this.Txt_month);
            this.Controls.Add(this.CHK_Dep_Reord);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Txt_User_date);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_year);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_Nrec_date);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cbo_term);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Cbo_method);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Manualy_Dep_cal";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "468";
            this.Text = "حساب الاندثارات يدويا";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Manualy_Dep_cal_FormClosed);
            this.Load += new System.EventHandler(this.Companies_Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.MaskedTextBox Txt_User_date;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_year;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox Txt_Nrec_date;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Cbo_term;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Cbo_method;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox CHK_Dep_Reord;
        private System.Windows.Forms.TextBox Txt_month;
        private System.Windows.Forms.TextBox Txt_Dep_day;
    }
}