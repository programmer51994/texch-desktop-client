﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Main_Section : Form
    {
        bool change = false;
        public static  BindingSource _BS_Section = new BindingSource(); 
        public Main_Section()
        {
            InitializeComponent();
          
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #endregion
            Grd_Term.AutoGenerateColumns = false;
        }
        //-----------------------------------------------------------------------------
        private void Main_Section_Load(object sender, EventArgs e)
        {   change = false ;
        String Sqltext = " Select B.T_id ,ACUST_NAME,ECUST_NAME from CUSTOMERS A, TERMINALS B where A.CUST_ID = B.CUST_ID"
                       +" And T_Id in (Select T_id from Sections) "
                       +" And Cust_Flag <> 0"
                       + " Order by " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");
            CboTerm.DataSource = connection.SqlExec(Sqltext, "Term_TBL");
            CboTerm.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
            CboTerm.ValueMember = "T_id";

            if (connection.SQLDS.Tables["Term_TBL"].Rows.Count > 0)
            {
            change = true ;
            CboTerm_SelectedIndexChanged(null, null);
            }
        }
        //-----------------------------------------------------------------------------
        private void CboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change)
            { 
                String SQl_Text = "  SELECT T_sec_id, sec_id, sec_aname, sec_ename, T_ID, User_Name "
                                + "  From sections A , Users B where "
                                + "  A.User_Id = B.USER_ID "
                                + "  And A.T_ID = " + Convert.ToInt32(CboTerm.SelectedValue);

                _BS_Section.DataSource = connection.SqlExec(SQl_Text, "Grd_Term_TBL");
                Grd_Term.DataSource = _BS_Section;
                //LblRec.Text = connection.Records(_BS_Section);
            }
        }
        //-----------------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            add_upd_Section AddFrm = new add_upd_Section(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Main_Section_Load(sender, e);
            this.Visible = true;
        }
        //-----------------------------------------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
            add_upd_Section AddFrm = new add_upd_Section(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Main_Section_Load(sender, e);
            this.Visible = true;

        }
        //-----------------------------------------------------------------------------
        private void Grd_Term_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_BS_Section);
        }
        //-----------------------------------------------------------------------------
        private void Main_Section_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //-----------------------------------------------------------------------------
        private void Main_Section_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
       //-----------------------------------------------------------------------------
        private void Main_Section_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
          
            string[] Tbl = { "Grd_Term_TBL", "Term_TBL"};
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }

    }
}