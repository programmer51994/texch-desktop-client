﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Daily_Sale_Assets_Add : Form
    {
        #region MyRegion
        bool Change = false;
        bool ACCChange = false;
        bool CurChange = false;
        string SqlTxt = "";
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CurId = new BindingSource();
        BindingSource Bs_CustId = new BindingSource();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        DataTable Asset_Voucher = new DataTable();
        int VO_NO = 0;
        BindingSource _Bs_Cbo = new BindingSource();
        #endregion
        public Daily_Sale_Assets_Add()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            GrdAssest.AutoGenerateColumns = false;
            Create_Table();
            if (connection.Lang_id != 1)
            {
                Grd_Acc_Id.Columns["column10"].DataPropertyName = "Acc_ename";
                Grd_Cur_Id.Columns["Column11"].DataPropertyName = "cur_ename";
                Grd_Cust_Id.Columns["Column12"].DataPropertyName = "Ecust_name";
            }
        }
        //-----------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Description1","Description2","Sec_id","Full_symbol","Buy_date ",
                "Asset_amount",  "Specific_Amount", "Acc_Name",  "Cust_name", "Sec_Name", "description","T_SEC_ID","Cur_name","Qty_id"
            };
            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte","System.String","System.String","System.Byte","System.String","System.DateTime",
                "System.Decimal","System.Decimal","System.String","System.String","System.String","System.String","System.Int32","System.String","System.Int16"
            };

            Asset_Voucher = CustomControls.Custom_DataTable("Asset_Voucher", Column, DType);
            GrdAssest.DataSource = _Bs;
        }
        //-----------------------------------------------------------
        private void Buy_cash_assest_Add_Load(object sender, EventArgs e)
        {
            Change = true;
            Txt_Acc_TextChanged(sender, e);
            BtnAdd.Enabled = false;
        }
        //-------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                ACCChange = false;
                SqlTxt = "SELECT ACC_Id,ACC_AName,Acc_EName,Sub_flag  FROM Account_Tree "
            + " WHere Record_flag = 1 "
            + " And ACC_Id  in (Select Acc_Id From CUSTOMERS_ACCOUNTS "
            + " Where CUST_ID not in(Select CUST_ID From CUSTOMERS Where Cust_Type_ID in(6,7,50) And Cust_flag <> 0 )"
            + " And T_Id =" + connection.T_ID + " And CUS_STATE_ID <> 0 ) "
            + " And Acc_Id in(Select Acc_Id From Users_Accounts_Auth Where Auth_User_Id =" + connection.user_id + " And  VC_Sw = 1)"
            + " And (Acc_AName like '" + Txt_Acc.Text + "%' Or  Acc_EName like '" + Txt_Acc.Text + "%' Or  Acc_Id   Like '" + Txt_Acc.Text + "%'Or  Ref_Acc_No like '" + Txt_Acc.Text + "%' )  Order by " + (connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename");
                Bs_AccId.DataSource = connection.SqlExec(SqlTxt, "Tbl_Acc_Name");
                Grd_Acc_Id.DataSource = Bs_AccId;

                if (connection.SQLDS.Tables["Tbl_Acc_Name"].Rows.Count > 0)
                {
                    ACCChange = true;
                    Grd_Acc_Id_SelectionChanged(sender, e);
                }
                else
                {

                    Grd_Cur_Id.DataSource = null;
                    Grd_Cust_Id.DataSource = null;
                }
            }
        }
        //-------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                Txt_Cust.Text = "";
                TxtCurr_Name.Text = "";
                Txt_Exch_Price.ResetText();
                TxtCurr_Name_TextChanged(sender, e);
            }
        }
        //-------------------------
        private void TxtCurr_Name_TextChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                CurChange = false;

                SqlTxt = " SELECT Cur_AName,Cur_EName,A.Cur_id,Exch_Rate "
                           + " FROM Cur_Tbl A ,Currencies_Rate B  "
                           + " 	Where A.Cur_id = B.Cur_id "
                           + " 	And (A.Cur_id like  '" + TxtCurr_Name.Text.Trim() + "%' "
                           + "			Or Cur_AName like '" + TxtCurr_Name.Text.Trim() + "%' "
                           + "			Or Cur_EName like '" + TxtCurr_Name.Text.Trim() + "%' ) "
                           + " And A.Cur_Id > 0 "
                           + " And A.Cur_ID in(Select Cur_ID From CUSTOMERS_ACCOUNTS Where ACC_Id = " + Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) + " And CUS_STATE_ID  <>0 ) "
                           + " UNION "
                           + " SELECT Cur_AName,Cur_EName,Cur_id,1 as Exch_Rate "
                           + " FROM Cur_Tbl 	Where	Cur_ID in(Select Cur_ID From CUSTOMERS_ACCOUNTS Where ACC_Id = " + Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) + " And CUS_STATE_ID  <>0 "
                           + " AND CUR_ID IN (Select loc_cur_id from TERMINALS where T_ID =" + connection.T_ID + ")) "
                           + " Order by " + (connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName");

                _Bs_Cbo.DataSource = connection.SqlExec(SqlTxt, "Tbl_Cur_Acc");

                Grd_Cur_Id.DataSource = _Bs_Cbo;
                if (connection.SQLDS.Tables["Tbl_Cur_Acc"].Rows.Count > 0)
                {
                    CurChange = true;
                    Grd_Cur_Id_SelectionChanged(sender, e);
                }
                else
                {
                    Grd_Cust_Id.DataSource = null;
                }
            }
        }
        //-------------------------
        private void Grd_Cur_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                Txt_Exch_Price.Text = ((DataRowView)_Bs_Cbo.Current).Row["Exch_Rate"].ToString();
                Txt_Cust_TextChanged(sender, e);
            }
        }
        //-------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                if (Grd_Acc_Id.RowCount > 0)
                {
                    Bs_CustId.DataSource = connection.SqlExec("Get_Cust_Info ", "Tbl_Cust_Name",
                    new object[] { ((DataRowView) _Bs_Cbo.Current).Row["Cur_Id"],
                    Txt_Cust.Text.Trim(), connection.T_ID, ((DataRowView)Bs_AccId.Current).Row["Acc_Id"],
                    ((DataRowView) Bs_AccId.Current).Row["Sub_flag"]});

                    Grd_Cust_Id.DataSource = Bs_CustId;
                }
            }
        }
        //-------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            #region Valdition
            if (Txt_FullSymbol.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الرمز رجاءاً" : "Enter The Symbol Please ", MyGeneral_Lib.LblCap);
                Txt_FullSymbol.Focus();
                return;
            }
            #endregion
            object[] Sparam = { Txt_FullSymbol.Text.Trim(), connection.T_ID };
            connection.SqlExec("Sale_Assets_details", "Assets_Det_Tbl", Sparam);
            if (connection.SQLDS.Tables["Assets_Det_Tbl"].Rows.Count <= 0)
            {
                TxtAsset_Amount.ResetText();
                TxtSpecific_Amount.ResetText();
                TxtTotal_Amount.ResetText();
                Txt_Month_Id.ResetText();
                Txt_Month.ResetText();
                Txt_Year.ResetText();
                Txtdesicription.ResetText();
                BtnAdd.Enabled = false;
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد نتائج للبحث" : "There are no search results", MyGeneral_Lib.LblCap);
                return;
            }
            DataRow Row = connection.SQLDS.Tables["Assets_Det_Tbl"].Rows[0];
            TxtAsset_Amount.Text = Row["Asset_amount"].ToString();
            TxtSpecific_Amount.Text = Row["Specific_Amount"].ToString();
            TxtTotal_Amount.Text = Row["Total_Amount"].ToString();
            Txt_Month_Id.Text = Row["dep_month"].ToString();
            Txt_Month.Text = Row[connection.Lang_id == 1 ? "Mon_aname" : "Mon_Ename"].ToString();
            Txt_Year.Text = Row["dep_year"].ToString();
            Txtdesicription.Text = Row[connection.Lang_id == 1 ? "ADescription" : "EDescription"].ToString();
            BtnAdd.Enabled = true;
            label20.Text = "(" + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + ")";
        }
        //-------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validations
            if (Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب رجاءا" : "Please Select Account", MyGeneral_Lib.LblCap);
                Txt_Acc.Focus();
                return;
            }
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل العملة رجاءا" : "Please Select Currency", MyGeneral_Lib.LblCap);
                TxtCurr_Name.Focus();
                return;
            }
            if (Convert.ToByte(((DataRowView)Bs_AccId.Current).Row["Sub_Flag"]) == 1)
            {
                if (Bs_CustId.List.Count <= 0)
                {
                    //if (Convert.ToInt16(((DataRowView)Bs_CustId.Current).Row["Cust_Id"]) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "الحساب للثانوي غير معرف في جدول الحسابات المركزية لهذه العملة" :
                                                            "Customer Account Unknown at Central Account For this Currency", MyGeneral_Lib.LblCap);
                        Txt_Cust.Focus();
                        return;
                    }
                }
            }
            if (Convert.ToDecimal(Txt_Exch_Price.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل سعر صرف العملة" : "Please Insert Exchange Price", MyGeneral_Lib.LblCap);
                Txt_Exch_Price.Focus();
                return;
            }
            if (Convert.ToDecimal(Txt_Amount.Text) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Please Insert Amount", MyGeneral_Lib.LblCap);
                Txt_Amount.Focus();
                return;
            }

            int Recount = (from rec in Asset_Voucher.AsEnumerable()
                           where (string)rec["Full_Symbol"] == connection.SQLDS.Tables["Assets_Det_Tbl"].Rows[0]["Full_Symbol"].ToString()
                           select new { }).Count();
            if (Recount > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تاكد من رمز الموجود " : "Check Full Symbol Please", MyGeneral_Lib.LblCap);
                return;
            }


            #endregion
            DataRow DRow1 = Asset_Voucher.NewRow();
            DataRow Row = connection.SQLDS.Tables["Assets_Det_Tbl"].Rows[0];

            //========
            DRow1["Acc_id"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Id"];
            DRow1["Acc_name"] = ((DataRowView)Bs_AccId.Current).Row[connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename"];
            if (Bs_CustId.List.Count > 0)
            {
                DRow1["Cust_Name"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"];
                DRow1["Cust_id"] = ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
            }
            DRow1["Cur_name"] = ((DataRowView)_Bs_Cbo.Current).Row[connection.Lang_id == 1 ? "Cur_AName" : "Cur_Ename"];
            DRow1["for_cur_id"] = ((DataRowView)_Bs_Cbo.Current).Row["cur_id"];
            DRow1["loc_cur_id"] = Txt_Loc_Cur.Tag;
            DRow1["Loc_amount"] = decimal.Round((Convert.ToDecimal(Txt_Amount.Text)) * Convert.ToDecimal(Txt_Exch_Price.Text), 3);
            DRow1["For_amount"] = Convert.ToDecimal(Txt_Amount.Text);
            DRow1["Full_Symbol"] = Row["Full_Symbol"];

            DRow1["Asset_amount"] = Row["Asset_amount"];
            DRow1["Specific_Amount"] = Row["Specific_Amount"];
            DRow1["Description1"] = Row[connection.Lang_id == 1 ? "ADescription" : "EDescription"];
            DRow1["Exch_price"] = Convert.ToDecimal(Txt_Exch_Price.Text);
            DRow1["real_price"] = Convert.ToDecimal(Txt_Exch_Price.Text);
            DRow1["Notes"] = Txt_Notes.Text;
            Asset_Voucher.Rows.Add(DRow1);


            _Bs.DataSource = Asset_Voucher;
            TxtAsset_Amount.ResetText();
            TxtSpecific_Amount.ResetText();
            TxtTotal_Amount.ResetText();
            Txt_Month_Id.ResetText();
            Txt_Month.ResetText();
            Txt_Year.ResetText();
            Txtdesicription.ResetText();
            Txt_FullSymbol.ResetText();
            Txt_Notes.ResetText();

            connection.SQLDS.Tables["Assets_Det_Tbl"].Clear();

            Change = false;
            ACCChange = false;
            CurChange = false;
            Txt_Acc.Text = string.Empty;
            Grd_Acc_Id.ClearSelection();
            TxtCurr_Name.Text = string.Empty;
            Grd_Cur_Id.ClearSelection();
            Txt_Cust.Text = string.Empty;
            Grd_Cust_Id.ClearSelection();
            Txt_Amount.ResetText();
            Txt_Exch_Price.ResetText();
            TxtSpecific_Amount.ResetText();
            Txt_Notes.Text = "";
            Change = true;
            ACCChange = true;
            CurChange = true;
            Txt_Acc.Focus();

        }
        //-------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Asset_Voucher.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد" :
                    "Are you sure you want to remove this record", MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    Asset_Voucher.Rows[GrdAssest.CurrentRow.Index].Delete();
                    GrdAssest.Refresh();

                }
            }
        }
        //-------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No opening balances", MyGeneral_Lib.LblCap);
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No Record Organizer", MyGeneral_Lib.LblCap);
                return;
            }

            if (Asset_Voucher.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "No Records For Adding", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            DataTable Dt = new DataTable();
            Dt = Asset_Voucher.DefaultView.ToTable(false, "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                 "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                 "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "Description1", "Description2", "Sec_id", "Full_symbol", "Buy_date ", "T_SEC_ID", "Specific_Amount", "Qty_id");

            connection.SQLCMD.Parameters.AddWithValue("@Asset_Tbl", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 40);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Sale_Daily_Assets_Details", connection.SQLCMD);
           
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Change = false;
            ACCChange = false;
            CurChange = false;
            Txt_Acc.ResetText();
            Txt_Amount.ResetText();
            Txt_Cust.ResetText();
            TxtCurr_Name.ResetText();
            Txt_Notes.ResetText();
            TxtSpecific_Amount.ResetText();
            Change = true;
            ACCChange = true;
            CurChange = true;
            Asset_Voucher.Clear();

        }
        //--------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------------------------------------------
        private void GrdAssest_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }
        //-----------------------------------------------------------------
        private void Buy_cash_assest_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            ACCChange = false;
            CurChange = false;
           
            string[] Used_Tbl = { "Tbl_Cur_Acc", "Tbl_Acc_Name", "Tbl_Cust_Name", "Assets_Det_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-----------------------------------------------------------------
        private void print_Rpt_Pdf()
        {

            string SqlTxt = "Exec Main_Report " + VO_NO + "," + TxtIn_Rec_Date.Text + ",40," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            string SqlTxt1 = "Exec Main_Report_Assets_Details " + VO_NO + "," + TxtIn_Rec_Date.Text + ",40," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Asset_Voucher");
            connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

            Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
            Main_Assets_Rpt_Eng ObjRptEng = new Main_Assets_Rpt_Eng();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 40);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

            DataRow Dr;
            int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 40);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Asset_Voucher");
            connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
        }
    }
}