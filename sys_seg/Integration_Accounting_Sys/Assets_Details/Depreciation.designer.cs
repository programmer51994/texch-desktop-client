﻿namespace Integration_Accounting_Sys
{
    partial class Depreciation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grdacc_no = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_Desc_per = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_AccSrch = new System.Windows.Forms.TextBox();
            this.Ext_RD = new System.Windows.Forms.RadioButton();
            this.Dep_RD = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.Grddb_acc = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txtdb_acc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txtcd_acc = new System.Windows.Forms.TextBox();
            this.Grdcd_acc = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.txtper_acc = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grdacc_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grddb_acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcd_acc)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(11, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 646;
            this.label1.Text = "المستـخـــــدم:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_UsrName.Location = new System.Drawing.Point(91, 5);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.Size = new System.Drawing.Size(256, 23);
            this.Txt_UsrName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(482, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 645;
            this.label2.Text = "التأريخ:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(2, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(698, 1);
            this.flowLayoutPanel2.TabIndex = 651;
            // 
            // Grdacc_no
            // 
            this.Grdacc_no.AllowUserToAddRows = false;
            this.Grdacc_no.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdacc_no.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdacc_no.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grdacc_no.ColumnHeadersHeight = 24;
            this.Grdacc_no.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdacc_no.DefaultCellStyle = dataGridViewCellStyle18;
            this.Grdacc_no.Location = new System.Drawing.Point(9, 63);
            this.Grdacc_no.Name = "Grdacc_no";
            this.Grdacc_no.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdacc_no.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdacc_no.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.Grdacc_no.Size = new System.Drawing.Size(685, 124);
            this.Grdacc_no.TabIndex = 3;
            this.Grdacc_no.SelectionChanged += new System.EventHandler(this.Grdacc_no_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "acc_id";
            this.Column1.HeaderText = "الرقـــــم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ref_acc_no";
            this.Column2.HeaderText = "رقـــم المـرجع";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "acc_aname";
            this.Column3.HeaderText = "الاســــم";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 440;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(11, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 16);
            this.label4.TabIndex = 664;
            this.label4.Text = "وصــــــف الحســــــــــاب :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_Desc_per
            // 
            this.Txt_Desc_per.BackColor = System.Drawing.Color.White;
            this.Txt_Desc_per.Enabled = false;
            this.Txt_Desc_per.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Desc_per.Location = new System.Drawing.Point(154, 195);
            this.Txt_Desc_per.Multiline = true;
            this.Txt_Desc_per.Name = "Txt_Desc_per";
            this.Txt_Desc_per.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_Desc_per.Size = new System.Drawing.Size(536, 52);
            this.Txt_Desc_per.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(6, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 16);
            this.label3.TabIndex = 665;
            this.label3.Text = "الحســــــــــاب:";
            // 
            // Txt_AccSrch
            // 
            this.Txt_AccSrch.BackColor = System.Drawing.Color.White;
            this.Txt_AccSrch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Txt_AccSrch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AccSrch.Location = new System.Drawing.Point(91, 37);
            this.Txt_AccSrch.Multiline = true;
            this.Txt_AccSrch.Name = "Txt_AccSrch";
            this.Txt_AccSrch.Size = new System.Drawing.Size(256, 23);
            this.Txt_AccSrch.TabIndex = 2;
            this.Txt_AccSrch.TextChanged += new System.EventHandler(this.Txt_AccSrch_TextChanged);
            // 
            // Ext_RD
            // 
            this.Ext_RD.AutoSize = true;
            this.Ext_RD.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Ext_RD.ForeColor = System.Drawing.Color.Navy;
            this.Ext_RD.Location = new System.Drawing.Point(592, 279);
            this.Ext_RD.Name = "Ext_RD";
            this.Ext_RD.Size = new System.Drawing.Size(60, 20);
            this.Ext_RD.TabIndex = 7;
            this.Ext_RD.Text = "اطفـــاء";
            this.Ext_RD.UseVisualStyleBackColor = true;
            // 
            // Dep_RD
            // 
            this.Dep_RD.AutoSize = true;
            this.Dep_RD.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Dep_RD.ForeColor = System.Drawing.Color.Navy;
            this.Dep_RD.Location = new System.Drawing.Point(487, 279);
            this.Dep_RD.Name = "Dep_RD";
            this.Dep_RD.Size = new System.Drawing.Size(62, 20);
            this.Dep_RD.TabIndex = 6;
            this.Dep_RD.Text = "اندثـــار";
            this.Dep_RD.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(11, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 16);
            this.label5.TabIndex = 690;
            this.label5.Text = "نسبـــــة الاندثـــار السنــــــــــــــــوي:";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label160.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label160.Location = new System.Drawing.Point(343, 283);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(21, 14);
            this.label160.TabIndex = 691;
            this.label160.Text = "%";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 264);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(697, 1);
            this.flowLayoutPanel1.TabIndex = 695;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(4, 321);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(697, 1);
            this.flowLayoutPanel3.TabIndex = 696;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(11, 331);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 697;
            this.label6.Text = "الحســاب المديـــــن:";
            // 
            // Grddb_acc
            // 
            this.Grddb_acc.AllowUserToAddRows = false;
            this.Grddb_acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grddb_acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grddb_acc.BackgroundColor = System.Drawing.Color.White;
            this.Grddb_acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grddb_acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Grddb_acc.ColumnHeadersHeight = 45;
            this.Grddb_acc.ColumnHeadersVisible = false;
            this.Grddb_acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grddb_acc.DefaultCellStyle = dataGridViewCellStyle23;
            this.Grddb_acc.Location = new System.Drawing.Point(15, 352);
            this.Grddb_acc.Name = "Grddb_acc";
            this.Grddb_acc.ReadOnly = true;
            this.Grddb_acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grddb_acc.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.Grddb_acc.RowHeadersVisible = false;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grddb_acc.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this.Grddb_acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grddb_acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grddb_acc.Size = new System.Drawing.Size(327, 62);
            this.Grddb_acc.TabIndex = 9;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "acc_id";
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "رقم الحساب";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 50;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Ref_Acc_No";
            this.Column5.HeaderText = "رقم المرجع";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 50;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "acc_aname";
            this.Column6.HeaderText = "اسم الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 200;
            // 
            // Txtdb_acc
            // 
            this.Txtdb_acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtdb_acc.Location = new System.Drawing.Point(109, 329);
            this.Txtdb_acc.Name = "Txtdb_acc";
            this.Txtdb_acc.Size = new System.Drawing.Size(232, 22);
            this.Txtdb_acc.TabIndex = 8;
            this.Txtdb_acc.TextChanged += new System.EventHandler(this.Txtdb_acc_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(362, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 16);
            this.label7.TabIndex = 700;
            this.label7.Text = "الحســاب الدائـــــن:";
            // 
            // Txtcd_acc
            // 
            this.Txtcd_acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtcd_acc.Location = new System.Drawing.Point(462, 331);
            this.Txtcd_acc.Name = "Txtcd_acc";
            this.Txtcd_acc.Size = new System.Drawing.Size(232, 22);
            this.Txtcd_acc.TabIndex = 10;
            this.Txtcd_acc.TextChanged += new System.EventHandler(this.Txtcd_acc_TextChanged);
            // 
            // Grdcd_acc
            // 
            this.Grdcd_acc.AllowUserToAddRows = false;
            this.Grdcd_acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcd_acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle26;
            this.Grdcd_acc.BackgroundColor = System.Drawing.Color.White;
            this.Grdcd_acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdcd_acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.Grdcd_acc.ColumnHeadersHeight = 45;
            this.Grdcd_acc.ColumnHeadersVisible = false;
            this.Grdcd_acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column9});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdcd_acc.DefaultCellStyle = dataGridViewCellStyle28;
            this.Grdcd_acc.Location = new System.Drawing.Point(369, 353);
            this.Grdcd_acc.Name = "Grdcd_acc";
            this.Grdcd_acc.ReadOnly = true;
            this.Grdcd_acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdcd_acc.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.Grdcd_acc.RowHeadersVisible = false;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcd_acc.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.Grdcd_acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grdcd_acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grdcd_acc.Size = new System.Drawing.Size(327, 62);
            this.Grdcd_acc.TabIndex = 11;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "acc_id";
            this.Column7.Frozen = true;
            this.Column7.HeaderText = "رقم الحساب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Ref_Acc_No";
            this.Column8.HeaderText = "رقم المرجع";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 50;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Acc_AName";
            this.Column9.HeaderText = "اسم الحساب";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 200;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(262, 427);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(89, 26);
            this.Btn_Add.TabIndex = 12;
            this.Btn_Add.Text = "موافـق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(350, 427);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 13;
            this.ExtBtn.Text = "انهاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(2, 420);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(697, 1);
            this.flowLayoutPanel4.TabIndex = 705;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(351, 323);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1, 97);
            this.flowLayoutPanel9.TabIndex = 4;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(2, 32);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 390);
            this.flowLayoutPanel5.TabIndex = 707;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(700, 31);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 390);
            this.flowLayoutPanel6.TabIndex = 708;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(535, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(156, 20);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // txtper_acc
            // 
            this.txtper_acc.BackColor = System.Drawing.Color.White;
            this.txtper_acc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtper_acc.Location = new System.Drawing.Point(195, 279);
            this.txtper_acc.Name = "txtper_acc";
            this.txtper_acc.NumberDecimalDigits = 3;
            this.txtper_acc.NumberDecimalSeparator = ".";
            this.txtper_acc.NumberGroupSeparator = ",";
            this.txtper_acc.Size = new System.Drawing.Size(144, 23);
            this.txtper_acc.TabIndex = 709;
            this.txtper_acc.Text = "0.000";
            // 
            // Depreciation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 459);
            this.Controls.Add(this.txtper_acc);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Grdcd_acc);
            this.Controls.Add(this.Txtcd_acc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txtdb_acc);
            this.Controls.Add(this.Grddb_acc);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Ext_RD);
            this.Controls.Add(this.Dep_RD);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.Grdacc_no);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Txt_Desc_per);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_AccSrch);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_UsrName);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Depreciation";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "433";
            this.Text = "Depreciation";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Depreciation_FormClosed);
            this.Load += new System.EventHandler(this.Depreciation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grdacc_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grddb_acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcd_acc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridView Grdacc_no;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_Desc_per;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_AccSrch;
        private System.Windows.Forms.RadioButton Ext_RD;
        private System.Windows.Forms.RadioButton Dep_RD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView Grddb_acc;
        private System.Windows.Forms.TextBox Txtdb_acc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txtcd_acc;
        private System.Windows.Forms.DataGridView Grdcd_acc;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Sample.DecimalTextBox txtper_acc;
    }
}