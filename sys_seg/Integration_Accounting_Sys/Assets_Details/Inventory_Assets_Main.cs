﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Inventory_Assets_Main : Form
    {
        public static BindingSource _Bs_GRD_Main = new BindingSource();
        public Inventory_Assets_Main()
        {
            InitializeComponent();
            //connection.SQLBS.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            if (connection.Lang_id == 2)
            {
                Column4.DataPropertyName = "Sec_eName";
            }


        }

        private void Inventory_Assets_Main_Load(object sender, EventArgs e)
        {

            Grd_Assets.AutoGenerateColumns = false;
            Txt_Search.Text = "";
            Txt_Search_TextChanged(sender, e); 
        }

        private void Txt_Search_TextChanged(object sender, EventArgs e)
        {
            object[] Sparam = { Txt_Search.Text.Trim(), connection.T_ID };
            _Bs_GRD_Main.DataSource = connection.SqlExec("Main_Inventory_Asset", "Asset_Tbl", Sparam);
            if (connection.SQLDS.Tables["Asset_Tbl"].Rows.Count > 0)
            {
                Grd_Assets.DataSource = _Bs_GRD_Main;
                UpdBtn.Enabled = true;
            }
            else
            {
                UpdBtn.Enabled = false;
            
            }
        }

        private void Inventory_Assets_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Str = {"Asset_Tbl"};
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables[Tbl].Clear();
            }
        }

        private void AllBtn_Click(object sender, EventArgs e)
        {
            Txt_Search.Text = "";
            Inventory_Assets_Main_Load(sender, e);
        }

        private void Grd_Assets_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_GRD_Main);
        }

        private void UpdBtn_Click(object sender, EventArgs e)
        {
            DataRowView DRV = _Bs_GRD_Main.Current as DataRowView;
            DataRow DR = DRV.Row;
            Inventory_Assets_Upd AddFrm = new Inventory_Assets_Upd();
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Inventory_Assets_Main_Load(sender, e);
            this.Visible = true;
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            DataTable ExpDt_Assets = connection.SQLDS.Tables["Asset_Tbl"].DefaultView.ToTable(false, "Sec_id", connection.Lang_id == 1 ? "Sec_aname" : "Sec_Ename",
                                     "Full_symbol" ,"Cust_Id","Description1","Description2","Acc_id","T_id","Asset_amount","Buy_Date","Dep_month","Dep_year" ,"Specific_Amount").Select().CopyToDataTable();
       
            DataTable[] _EXP_DT = {ExpDt_Assets };
            DataGridView[] Export_GRD = { Grd_Assets };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Inventory_Assets_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Inventory_Assets_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

      

    }
}