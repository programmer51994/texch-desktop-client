﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Reveal_Assets_details : Form
    {
        #region Defintion
        DataTable DT_Cust = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        public static BindingSource SQLBSCbo = new BindingSource();
        int T_Id = 0;
        string Filter = "";
        string Sql_Text = "";
        public static string T_IDStr = "";
        public static Int16 Z = 0;
        bool chang = false;
        #endregion
        public Search_Reveal_Assets_details()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;



            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust_Id.Columns["Column8"].DataPropertyName = "Ecust_name";
                Grd_Cust.Columns["Column11"].DataPropertyName = "Ecust_name";

            }
            #endregion
        }
        //----------------------------------------------------
        private void Create_Tbl()
        {
            string[] Column1 = { "T_Id", "ACUST_NAME", "ECUST_NAME" };
            string[] DType1 = { "System.Int16", "System.String", "System.String" };
            DT_Cust = CustomControls.Custom_DataTable("DT_Cust", Column1, DType1);
            Grd_Cust.DataSource = DT_Cust;

        }
        //----------------------------------------------------
        private void Search_Trial_Balance_Load(object sender, EventArgs e)
        {
            Create_Tbl();
            if (connection.Lang_id == 1)
            {
                 Sql_Text = "Select  ' (جميع المستويات) ' as Asset_name, -1 As Asset_Flag "
                        + " Union all "
                        + " Select 'عامــــل' AS Asset_name ,0 As Asset_Flag "
                        + " Union  "
                        + " SELECT  case Asset_Flag when   1 then 'مباع' when 2 then 'مسقط' end "
                        + " as Asset_name,Asset_Flag "
                        + " from  Drop_Sal_Assets ";
            }
            if (connection.Lang_id == 2)
            {
                 Sql_Text = "Select  ' (All levels) ' as Asset_name, -1 As Asset_Flag "
                        + " Union all "
                        + " Select 'active' AS Asset_name ,0 As Asset_Flag "
                        + " Union  "
                        + " SELECT  case Asset_Flag when   1 then 'sold' when 2 then 'inactive' end "
                        + " as Asset_name,Asset_Flag "
                        + " from  Drop_Sal_Assets ";
            }




            connection.SQLBSCbo.DataSource = connection.SqlExec(Sql_Text, "State_tbl");
            Cbo_state_ass.DataSource = connection.SQLBSCbo;
            Cbo_state_ass.DisplayMember = "Asset_name";
            Cbo_state_ass.ValueMember = "Asset_Flag";
           



            Sql_Text = "select B.T_id,A.ACUST_NAME , A.ECUST_NAME "
                  + " From CUSTOMERS A, TERMINALS B "
                  + " Where A.CUST_ID = B.CUST_ID "
                  + " And A.Cust_Flag <>0 "
                  + " and b.t_id in( select t_id from Assets_Details Union select t_id from Drop_Sal_Assets)"
                  + " Order By " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");
            connection.SqlExec(Sql_Text, "term_tbl");


            TxtCust_Name_TextChanged(null, null);
        }
        //----------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------------------------------------
        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                T_Id = 0;
                string Cond_Str_Cust_Id = "";
                string Con_Str = "";
                if (DT_Cust.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cust, "T_Id", out Cond_Str_Cust_Id);
                    Con_Str = " And T_Id not in(" + Cond_Str_Cust_Id + ")";
                }
                int.TryParse(TxtCust_Name.Text, out T_Id);


                Filter = " (Acust_name like '" + TxtCust_Name.Text + "%' or  Ecust_name like '" + TxtCust_Name.Text + "%'"
                            + " Or T_Id = " + T_Id + ")" + Con_Str;
                DT_Cust_TBL = connection.SQLDS.Tables["term_tbl"].DefaultView.ToTable(true, "T_Id", "ACust_name", "Ecust_name").Select(Filter).CopyToDataTable();
                Grd_Cust_Id.DataSource = DT_Cust_TBL;

            }
            catch
            {
                Grd_Cust_Id.DataSource = new DataTable();
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {

                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }
        //----------------------------------------------------
        private void Search_Trial_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang = false;
            string[] Used_Tbl = { "SearchTrial_Balance_Tbl", "SearchTrial_Balance_Tbl1", "Trail_Balance_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        //----------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cust.NewRow();

            row["T_id"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["T_Id"];
            row["ACUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ACust_name"];
            row["ECUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ECust_name"];
            DT_Cust.Rows.Add(row);
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);

            button3.Enabled = true;
            button4.Enabled = true;
            if (Grd_Cust_Id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }
        //----------------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cust_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cust.NewRow();
                row["T_id"] = DT_Cust_TBL.Rows[i]["T_Id"];
                row["ACUST_NAME"] = DT_Cust_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Cust_TBL.Rows[i]["ECUST_NAME"];
                DT_Cust.Rows.Add(row);
            }
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Enabled = true;
        }
        //----------------------------------------------------
        private void button3_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows[Grd_Cust.CurrentRow.Index].Delete();
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            if (Grd_Cust.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
        }
        //----------------------------------------------------
        private void button4_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows.Clear();
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
        }
        //----------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            string[] Used_Tbl = { "assets_tbl", "assets_tbl1", "assets_tbl2" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

            if (DT_Cust.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجـــى اختيـــار الفـــرع " : "select the terminal please ", MyGeneral_Lib.LblCap);

                return;
            }
            else
            {

                MyGeneral_Lib.ColumnToString(DT_Cust, "T_Id", out  T_IDStr);
                //assets = DT_Term.DefaultView.ToTable(false, "T_ID", "ACUST_NAME", "ECUST_NAME").Select().CopyToDataTable();

                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[Reveal_Assets_Details2]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@ast_State_Id", Cbo_state_ass.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@T_IDStr", T_IDStr);
                    connection.SQLCMD.Parameters.AddWithValue("@chk_month_year", Chk_date.Checked);
                    connection.SQLCMD.Parameters.AddWithValue("@Month_Year", Cbo_date.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "assets_tbl");
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "assets_tbl1");
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "assets_tbl2");
                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch (Exception _Err)
                {
                    connection.SQLCMD.Dispose();
                    connection.SQLCMD.Parameters.Clear();
                    MyErrorHandler.ExceptionHandler(_Err);

                }

                //-------------------------------------
                Z = Convert.ToInt16(Cbo_state_ass.SelectedValue);
                Reveal_Asset_Details Frm = new Reveal_Asset_Details();
                this.Visible = false;
                Frm.ShowDialog();
                this.Visible = true;
                Search_Trial_Balance_Load(null, null);
            }
        }
        //----------------------------------------------------
        private void Chk_date_CheckedChanged(object sender, EventArgs e)
        {

            if (Chk_date.Checked)
            {
                Cbo_date.Enabled = true;
                Sql_Text = " select distinct Month_year ,"
                            + " ltrim(rtrim(STR( dep_year))) + '/' +ltrim(rtrim(str(dep_month)))  as N_Month_year "
                            + " from Hst_Assets_Details "
                            + " order by Month_year ";

                Cbo_date.DataSource = connection.SqlExec(Sql_Text, "date_tbl");
                Cbo_date.DisplayMember = "N_Month_year";
                Cbo_date.ValueMember = "Month_year";
                Cbo_date.Enabled = true;
            }
            else
            {
                Cbo_date.DataSource = null;
                Cbo_date.Enabled = false;
            }
        }

        private void Search_Reveal_Assets_details_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Cbo_date_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}