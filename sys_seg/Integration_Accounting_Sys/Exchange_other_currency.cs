﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Exchange_other_currency : Form
    {
        string sql_txt = "";
        bool change = false;
        bool change1 = false;
        bool Acc_Change = false;
        bool Cust_Change = false;
        bool Acc_Change1 = false;
        bool Cust_Change1 = false;
        bool chanch_paid = false;
        bool Cust_Change_loc = false;
        bool Cust_Change_loc1 = false;
        private decimal Mdiff_Amoun = 0;
        BindingSource In_BS = new BindingSource();
        BindingSource Out_BS = new BindingSource();
        BindingSource Bs_Acc = new BindingSource();
        BindingSource Bs_Cust = new BindingSource();
        BindingSource Bs_Acc_paid = new BindingSource();
        BindingSource Bs_Cust_paid = new BindingSource();
        BindingSource Bs_Acc_loc = new BindingSource();
        BindingSource Bs_Cust_loc = new BindingSource();
        decimal exch_out = 0;
        decimal exch_in = 0;
        // TxtBox_User,
        

        public Exchange_other_currency()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtBox_User, new TextBox(), TxtIn_Rec_Date, TxtUser);
            Grd_Rec_Acc.AutoGenerateColumns = false;
            Grd_Cust_rec.AutoGenerateColumns = false;
            Grd_loc_acc.AutoGenerateColumns = false;
            Grd_loc_cust.AutoGenerateColumns = false;
        }

        private void Exchange_other_currency_Load(object sender, EventArgs e)
        {

            Local_acc_txt.Enabled = false;
            cust_local_txt.Enabled = false;

            change = false;
            {

                sql_txt = "select  A.Cur_ID, A.Exch_Rate, B.Cur_ANAME, B.Cur_ENAME from  Currencies_Rate A ,   Cur_Tbl B"
                             + " WHERE A.Cur_ID=B.Cur_ID "
                             + " and A.Exch_Rate > 0 ";


                In_BS.DataSource = connection.SqlExec(sql_txt, "receve_cur_tbl");
                cbo__cur_in.DataSource = In_BS;
                cbo__cur_in.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                cbo__cur_in.ValueMember = "Cur_ID";





                sql_txt = "  Select  CATG_ID, CATG_ANAME, CATG_ENAME "
                         + " from  CATG_Tbl ";

                Cbo_Catogreis.DataSource = connection.SqlExec(sql_txt, "Catg_tbl");

                Cbo_Catogreis.DisplayMember = connection.Lang_id == 1 ? "CATG_ANAME" : "CATG_ENAME";
                Cbo_Catogreis.ValueMember = "CATG_ID";
            }


            change = true;
            cbo__cur_in_SelectedIndexChanged(null, null);



        }

        private void cbo__cur_in_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (change)
            {
                Int16 n = Convert.ToInt16(cbo__cur_in.SelectedValue);

                sql_txt = "select  A.Cur_ID, A.Exch_Rate, B.Cur_ANAME, B.Cur_ENAME from  Currencies_Rate A ,  Cur_Tbl B"
                            + " WHERE A.Cur_ID=B.Cur_ID "
                            + " and A.Exch_Rate > 0 "
                            + " and A.Cur_ID <> " + n;

                Out_BS.DataSource = connection.SqlExec(sql_txt, "paid_cur_tbl");
                Cbo_cur_out.DataSource = Out_BS;

                Cbo_cur_out.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                Cbo_cur_out.ValueMember = "Cur_ID";

                if (TxtAcc_Name.Text != "")
                {

                    TxtCur_Name_TextChanged(null, null);

                }
                
                change1 = true;
                Cbo_cur_out_SelectedIndexChanged(null, null);

            
            }

        }



        private void Cbo_cur_out_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change1)
            {
                Int16 X =Convert.ToInt16( cbo__cur_in.SelectedValue); 
             //  decimal exch_in = Convert.ToDecimal(((DataRowView)In_BS.Current).Row["Exch_Rate"]);

                connection.SQLDS.Tables["receve_cur_tbl"].DefaultView.ToTable(false, "Cur_id", "Exch_Rate").Select("Cur_id = " + X).CopyToDataTable();

                 exch_in =Convert.ToDecimal(connection.SQLDS.Tables["receve_cur_tbl"].Rows[cbo__cur_in.SelectedIndex]["Exch_Rate"]);
                 exch_out = Convert.ToDecimal(connection.SQLDS.Tables["paid_cur_tbl"].Rows[Cbo_cur_out.SelectedIndex]["Exch_Rate"]);
               
                decimal result = exch_in / exch_out;
                Exchang_rat_txt.Text = Convert.ToString(result);


                label25.Text = cbo__cur_in.Text + "/" + Cbo_cur_out.Text;

                if (txt_paid_acc.Text != "")
                {

                    txt_paid_acc_TextChanged(null, null);

                }
            }
        }

        private void Real_txt_TextChanged(object sender, EventArgs e)
        {
            paid_Txt.Text = decimal.Round((Convert.ToDecimal(Real_txt.Text) * Convert.ToDecimal(recv_txt.Text)), 3).ToString();

          //  txt_loc_amount.Text = decimal.Round(((Convert.ToDecimal(Real_txt.Text) * Convert.ToDecimal(recv_txt.Text)) - Convert.ToDecimal(paid_Txt.Text)) * Convert.ToDecimal(Exchang_rat_txt.Text), 3).ToString();

        }

        private void recv_txt_TextChanged(object sender, EventArgs e)
        {
           paid_Txt.Text = decimal.Round((Convert.ToDecimal(Real_txt.Text) * Convert.ToDecimal(recv_txt.Text)), 3).ToString();
          //  txt_loc_amount.Text = decimal.Round(((Convert.ToDecimal(Real_txt.Text) * Convert.ToDecimal(recv_txt.Text)) - Convert.ToDecimal(paid_Txt.Text)) * Convert.ToDecimal(Exchang_rat_txt.Text), 3).ToString();
        }

        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {

            Acc_Change = false;

            connection.SqlExec("exec Get_Acc_Info_Other_Cur "   + "'" + TxtAcc_Name.Text + "'" + ',' + connection.T_ID + ','
                                                                     + cbo__cur_in.SelectedValue, "Rec_ACC_tbl");

            Bs_Acc.DataSource = connection.SQLDS.Tables["Rec_ACC_tbl"];

            Grd_Rec_Acc.DataSource = Bs_Acc;
            if (connection.SQLDS.Tables["Rec_ACC_tbl"].Rows.Count > 0)
            {
                Acc_Change = true;
                Grd_Rec_Acc_SelectionChanged(null, null);
            }
            //else Grd_Rec_Acc.DataSource = null; 
        }


        private void paid_Txt_TextChanged(object sender, EventArgs e)
        {

            //if (chanch_paid=true)
            //{
                Local_acc_txt.Enabled = true;
                cust_local_txt.Enabled = true;
                decimal result2 = 0;
               // txt_loc_amount.Text = decimal.Round(((Convert.ToDecimal(Real_txt.Text) * Convert.ToDecimal(recv_txt.Text)) - Convert.ToDecimal(paid_Txt.Text)) * Convert.ToDecimal(Exchang_rat_txt.Text), 3).ToString();


                result2 = decimal.Round(((Convert.ToDecimal(Real_txt.Text) * Convert.ToDecimal(recv_txt.Text)) - Convert.ToDecimal(paid_Txt.Text)) * exch_out, 3);
               // decimal amount = Convert.ToDecimal(result2);
                //-------------------------------------------discount
                //decimal Loc_Amount = Bill_Cur.Compute("Sum(Loc_amount)", "") == DBNull.Value ? 0 : Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_amount)", ""));
                Mdiff_Amoun = result2 % connection.Discount_Amount;
                if (Mdiff_Amoun > (connection.Discount_Amount / 2))
                {
                    Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
                }
                else
                {
                    Mdiff_Amoun = Mdiff_Amoun * -1; //----مكتسب
                }

                //--------------------------------------------
                //if (Convert.ToInt32(paid_Txt.Text) > 0)
                //{
                     Discount_txt.Text = Mdiff_Amoun.ToString();
                     if (Mdiff_Amoun > 0)
                     {
                         label22.Text = "خصـــــــــــم ممنــوح:";
                     }
                     else
                     {
                         label22.Text = "خصـــــــــــم مكتـسب:";
                     }
                     txt_loc_amount.Text = (result2 + Convert.ToDecimal(Discount_txt.Text)).ToString();
                  
                //}
            }
            
            


        private void txt_rec_acc_TextChanged(object sender, EventArgs e)
        {
            if (Cust_Change && connection.SQLDS.Tables["Rec_ACC_tbl"].Rows.Count > 0)
            {
                connection.SqlExec("exec Get_Cust_Info " + cbo__cur_in.SelectedValue + ',' + "'" + txt_rec_cust.Text + "'" + ',' +
                                    connection.T_ID + ',' + ((DataRowView)Bs_Acc.Current).Row["Acc_id"] + ',' + ((DataRowView)Bs_Acc.Current).Row["Sub_Flag"], "Rec_Cust_tbl");

                Bs_Cust.DataSource = connection.SQLDS.Tables["Rec_Cust_tbl"];
                Grd_Cust_rec.DataSource = Bs_Cust;
            }
        }

        private void Grd_Rec_Acc_SelectionChanged(object sender, EventArgs e)
        {

            if (Acc_Change)
            {
                txt_rec_cust.Text = "";
                Cust_Change = true;

                txt_rec_acc_TextChanged(null, null);

            }
        }

        private void txt_paid_acc_TextChanged(object sender, EventArgs e)
        {
            
            Acc_Change1 = false;

            connection.SqlExec("exec Get_Acc_Info_Other_Cur " + "'" + txt_paid_acc.Text + "'" + ',' + connection.T_ID + ','
                                                                     + Cbo_cur_out.SelectedValue, "Paid_ACC_tbl");

            Bs_Acc_paid.DataSource = connection.SQLDS.Tables["Paid_ACC_tbl"];

            Grd_Paid_Acc.DataSource = Bs_Acc_paid;
            if (connection.SQLDS.Tables["Paid_ACC_tbl"].Rows.Count > 0)
            {
                Acc_Change1 = true;
                Grd_Paid_Acc_SelectionChanged(null, null);
            }

        }

        private void Grd_Paid_Acc_SelectionChanged(object sender, EventArgs e)
        {
            if (Acc_Change1)
            {
                txt_paid_cust.Text = "";
                Cust_Change1 = true;
                txt_paid_cust_TextChanged(null, null);

            
            }

        }

        private void txt_paid_cust_TextChanged(object sender, EventArgs e)
        {
            if (Cust_Change1 && connection.SQLDS.Tables["Paid_ACC_tbl"].Rows.Count > 0)
            {
                connection.SqlExec("exec Get_Cust_Info " + Cbo_cur_out.SelectedValue + ',' + "'" + txt_paid_cust.Text + "'" + ',' +
                                    connection.T_ID + ',' + ((DataRowView)Bs_Acc_paid.Current).Row["Acc_id"] + ',' + ((DataRowView)Bs_Acc_paid.Current).Row["Sub_Flag"], "locacc_Cust_tbl");

                Bs_Cust_paid.DataSource = connection.SQLDS.Tables["Paid_Cust_tbl"];
                Grd_paid_cust.DataSource = Bs_Cust_paid;
            }
        }

        private void Local_acc_txt_TextChanged(object sender, EventArgs e)
        {
           Cust_Change_loc=false;
                connection.SqlExec("exec Get_Acc_Info_Other_Cur " + "'" + Local_acc_txt.Text + "'" + ',' + connection.T_ID + ','
                                                                         + connection.Loc_Cur_Id, "loc_ACC_tbl");

                Bs_Acc_loc.DataSource = connection.SQLDS.Tables["loc_ACC_tbl"];

                Grd_loc_acc.DataSource = Bs_Acc_loc;
                if (connection.SQLDS.Tables["loc_ACC_tbl"].Rows.Count > 0)
                {
                    Cust_Change_loc = true;
                    Grd_loc_acc_SelectionChanged(null, null);
                }
            
        }

        private void cust_local_txt_TextChanged(object sender, EventArgs e)
        {
            if (Cust_Change_loc1 && connection.SQLDS.Tables["loc_ACC_tbl"].Rows.Count > 0)
            {
                connection.SqlExec("exec Get_Cust_Info " + connection.Loc_Cur_Id + ',' + "'" + cust_local_txt.Text + "'" + ',' +
                                    connection.T_ID + ',' + ((DataRowView)Bs_Acc_loc.Current).Row["Acc_id"] + ',' + ((DataRowView)Bs_Acc_loc.Current).Row["Sub_Flag"], "loc_Cust_tbl");

                Bs_Cust_loc.DataSource = connection.SQLDS.Tables["loc_Cust_tbl"];
                Grd_loc_cust.DataSource = Bs_Cust_loc;
            }
        }

        private void Grd_loc_acc_SelectionChanged(object sender, EventArgs e)
        {
            if (Cust_Change_loc=true)
            {
                cust_local_txt.Text = "";
                Cust_Change_loc1 = true;

                cust_local_txt_TextChanged(null, null);

            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Exchange_other_currency_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
            change1 = false;
            Acc_Change = false;
            Cust_Change = false;
            Acc_Change1 = false;
            Cust_Change1 = false;
            chanch_paid = false;
            Cust_Change_loc = false;
            Cust_Change_loc1 = false;
        }
    }
}