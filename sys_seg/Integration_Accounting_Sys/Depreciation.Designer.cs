﻿namespace Accounting_Sys
{
    partial class Depreciation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtIn_Rec_Date = new Accounting_Sys.MyDateTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grdacc_no = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_Desc_per = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_AccSrch = new System.Windows.Forms.TextBox();
            this.Ext_RD = new System.Windows.Forms.RadioButton();
            this.Dep_RD = new System.Windows.Forms.RadioButton();
            this.txtper_acc = new Accounting_Sys.NumericTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.Grddb_acc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txtdb_acc = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txtcd_acc = new System.Windows.Forms.TextBox();
            this.Grdcd_acc = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.Grdacc_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grddb_acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcd_acc)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(535, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(156, 20);
            this.TxtIn_Rec_Date.TabIndex = 644;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(7, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 646;
            this.label1.Text = "المستـخدم:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_UsrName.Location = new System.Drawing.Point(78, 3);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.Size = new System.Drawing.Size(282, 23);
            this.Txt_UsrName.TabIndex = 643;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(482, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 645;
            this.label2.Text = "التأريخ:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(697, 2);
            this.flowLayoutPanel2.TabIndex = 651;
            // 
            // Grdacc_no
            // 
            this.Grdacc_no.AllowUserToAddRows = false;
            this.Grdacc_no.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdacc_no.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdacc_no.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grdacc_no.ColumnHeadersHeight = 24;
            this.Grdacc_no.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.Grdacc_no.Location = new System.Drawing.Point(35, 78);
            this.Grdacc_no.Name = "Grdacc_no";
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdacc_no.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grdacc_no.Size = new System.Drawing.Size(643, 124);
            this.Grdacc_no.TabIndex = 661;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(18, 216);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 16);
            this.label12.TabIndex = 664;
            this.label12.Text = "وصف الحساب :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_Desc_per
            // 
            this.Txt_Desc_per.BackColor = System.Drawing.Color.White;
            this.Txt_Desc_per.Enabled = false;
            this.Txt_Desc_per.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Desc_per.Location = new System.Drawing.Point(104, 208);
            this.Txt_Desc_per.Multiline = true;
            this.Txt_Desc_per.Name = "Txt_Desc_per";
            this.Txt_Desc_per.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_Desc_per.Size = new System.Drawing.Size(574, 52);
            this.Txt_Desc_per.TabIndex = 663;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.DarkRed;
            this.label11.Location = new System.Drawing.Point(6, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 16);
            this.label11.TabIndex = 665;
            this.label11.Text = "الحســــــــــاب:";
            // 
            // Txt_AccSrch
            // 
            this.Txt_AccSrch.BackColor = System.Drawing.Color.White;
            this.Txt_AccSrch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Txt_AccSrch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AccSrch.Location = new System.Drawing.Point(84, 39);
            this.Txt_AccSrch.Multiline = true;
            this.Txt_AccSrch.Name = "Txt_AccSrch";
            this.Txt_AccSrch.Size = new System.Drawing.Size(352, 23);
            this.Txt_AccSrch.TabIndex = 662;
            // 
            // Ext_RD
            // 
            this.Ext_RD.AutoSize = true;
            this.Ext_RD.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Ext_RD.ForeColor = System.Drawing.Color.Navy;
            this.Ext_RD.Location = new System.Drawing.Point(549, 279);
            this.Ext_RD.Name = "Ext_RD";
            this.Ext_RD.Size = new System.Drawing.Size(60, 20);
            this.Ext_RD.TabIndex = 694;
            this.Ext_RD.Text = "اطفـــاء";
            this.Ext_RD.UseVisualStyleBackColor = true;
            // 
            // Dep_RD
            // 
            this.Dep_RD.AutoSize = true;
            this.Dep_RD.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Dep_RD.ForeColor = System.Drawing.Color.Navy;
            this.Dep_RD.Location = new System.Drawing.Point(434, 279);
            this.Dep_RD.Name = "Dep_RD";
            this.Dep_RD.Size = new System.Drawing.Size(62, 20);
            this.Dep_RD.TabIndex = 693;
            this.Dep_RD.Text = "اندثـــار";
            this.Dep_RD.UseVisualStyleBackColor = true;
            // 
            // txtper_acc
            // 
            this.txtper_acc.AllowSpace = false;
            this.txtper_acc.Location = new System.Drawing.Point(205, 282);
            this.txtper_acc.Name = "txtper_acc";
            this.txtper_acc.Size = new System.Drawing.Size(138, 20);
            this.txtper_acc.TabIndex = 692;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(39, 283);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(156, 16);
            this.label15.TabIndex = 690;
            this.label15.Text = "نسبة الاندثار السنــــــــــــــــوي:";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label160.ForeColor = System.Drawing.Color.Navy;
            this.label160.Location = new System.Drawing.Point(349, 284);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(21, 14);
            this.label160.TabIndex = 691;
            this.label160.Text = "%";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 264);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(697, 2);
            this.flowLayoutPanel1.TabIndex = 695;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(4, 321);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(697, 2);
            this.flowLayoutPanel3.TabIndex = 696;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(11, 331);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 16);
            this.label16.TabIndex = 697;
            this.label16.Text = "الحساب المدين:";
            // 
            // Grddb_acc
            // 
            this.Grddb_acc.AllowUserToAddRows = false;
            this.Grddb_acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grddb_acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grddb_acc.BackgroundColor = System.Drawing.Color.White;
            this.Grddb_acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grddb_acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grddb_acc.ColumnHeadersHeight = 45;
            this.Grddb_acc.ColumnHeadersVisible = false;
            this.Grddb_acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7,
            this.Column8});
            this.Grddb_acc.Location = new System.Drawing.Point(15, 357);
            this.Grddb_acc.Name = "Grddb_acc";
            this.Grddb_acc.ReadOnly = true;
            this.Grddb_acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grddb_acc.RowHeadersVisible = false;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grddb_acc.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.Grddb_acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grddb_acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grddb_acc.Size = new System.Drawing.Size(306, 53);
            this.Grddb_acc.TabIndex = 698;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "acc_id";
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 50;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Ref_Acc_No";
            this.dataGridViewTextBoxColumn7.HeaderText = "رقم المرجع";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "acc_aname";
            this.Column8.HeaderText = "اسم الحساب";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Txtdb_acc
            // 
            this.Txtdb_acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtdb_acc.Location = new System.Drawing.Point(90, 329);
            this.Txtdb_acc.Name = "Txtdb_acc";
            this.Txtdb_acc.Size = new System.Drawing.Size(232, 22);
            this.Txtdb_acc.TabIndex = 699;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(375, 333);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 16);
            this.label17.TabIndex = 700;
            this.label17.Text = "الحساب الدائن:";
            // 
            // Txtcd_acc
            // 
            this.Txtcd_acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtcd_acc.Location = new System.Drawing.Point(453, 329);
            this.Txtcd_acc.Name = "Txtcd_acc";
            this.Txtcd_acc.Size = new System.Drawing.Size(232, 22);
            this.Txtcd_acc.TabIndex = 701;
            // 
            // Grdcd_acc
            // 
            this.Grdcd_acc.AllowUserToAddRows = false;
            this.Grdcd_acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcd_acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.Grdcd_acc.BackgroundColor = System.Drawing.Color.White;
            this.Grdcd_acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdcd_acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grdcd_acc.ColumnHeadersHeight = 45;
            this.Grdcd_acc.ColumnHeadersVisible = false;
            this.Grdcd_acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.dataGridViewTextBoxColumn2,
            this.Column30});
            this.Grdcd_acc.Location = new System.Drawing.Point(387, 358);
            this.Grdcd_acc.Name = "Grdcd_acc";
            this.Grdcd_acc.ReadOnly = true;
            this.Grdcd_acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grdcd_acc.RowHeadersVisible = false;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcd_acc.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.Grdcd_acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grdcd_acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grdcd_acc.Size = new System.Drawing.Size(306, 53);
            this.Grdcd_acc.TabIndex = 702;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "acc_id";
            this.Column10.Frozen = true;
            this.Column10.HeaderText = "رقم الحساب";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Ref_Acc_No";
            this.dataGridViewTextBoxColumn2.HeaderText = "رقم المرجع";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "Acc_AName";
            this.Column30.HeaderText = "اسم الحساب";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(262, 428);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(89, 26);
            this.Btn_Add.TabIndex = 703;
            this.Btn_Add.Text = "موافـق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            // 
            // ExtBtn
            // 
            this.ExtBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(351, 428);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 704;
            this.ExtBtn.Text = "انهاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(4, 420);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(697, 2);
            this.flowLayoutPanel4.TabIndex = 705;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "رقـــــم";
            this.Column1.Name = "Column1";
            this.Column1.Width = 200;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "رقـــم المـرجع";
            this.Column2.Name = "Column2";
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "الاســــم";
            this.Column3.Name = "Column3";
            this.Column3.Width = 200;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(351, 323);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(2, 97);
            this.flowLayoutPanel9.TabIndex = 706;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(2, 32);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(2, 390);
            this.flowLayoutPanel5.TabIndex = 707;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(700, 31);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(2, 390);
            this.flowLayoutPanel6.TabIndex = 708;
            // 
            // Depreciation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 459);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Grdcd_acc);
            this.Controls.Add(this.Txtcd_acc);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txtdb_acc);
            this.Controls.Add(this.Grddb_acc);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Ext_RD);
            this.Controls.Add(this.Dep_RD);
            this.Controls.Add(this.txtper_acc);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.Grdacc_no);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_Desc_per);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_AccSrch);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_UsrName);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Depreciation";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Depreciation";
            ((System.ComponentModel.ISupportInitialize)(this.Grdacc_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grddb_acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcd_acc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridView Grdacc_no;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_Desc_per;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_AccSrch;
        private System.Windows.Forms.RadioButton Ext_RD;
        private System.Windows.Forms.RadioButton Dep_RD;
        private NumericTextBox txtper_acc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView Grddb_acc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.TextBox Txtdb_acc;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txtcd_acc;
        private System.Windows.Forms.DataGridView Grdcd_acc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
    }
}