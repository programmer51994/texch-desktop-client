﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class reveal_hst_rate_cur1 : Form
    {  
         String Sql_Text = "";
         Int64 current_t_id;
        BindingSource Bs_Hst_Rate_Cur = new BindingSource();
        Int64 cur_id = 0;

        public reveal_hst_rate_cur1()
        {
            InitializeComponent();
            Grd_Hst_Rate_Currency.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            Column2.DataPropertyName = "Cur_ENAME";

        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void reveal_hst_rate_cur1_Load_1(object sender, EventArgs e)
        {
            cur_id = CCP_List_Add_Upd.current_cur_id;
            current_t_id = connection.T_ID;
            Sql_Text = " Exec reveal_Hst_CCP_LIST " + current_t_id + "," + cur_id;
            Bs_Hst_Rate_Cur.DataSource = connection.SqlExec(Sql_Text, "Hst_CCP_LIST_TBL");
            Grd_Hst_Rate_Currency.DataSource = Bs_Hst_Rate_Cur;
        }
    }
}


