﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class reveal_hst_rate_cur : Form
    {
        BindingSource Bs_Hst_Rate_Cur = new BindingSource();
        public reveal_hst_rate_cur()
        {
            InitializeComponent();


            Grd_Hst_Rate_Currency.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            Column2.DataPropertyName = "Cur_ENAME";

        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void reveal_hst_rate_cur_Load(object sender, EventArgs e)
        {
            Bs_Hst_Rate_Cur.DataSource = connection.SQLDS.Tables["tbl_hst_rate_cur"];
            Grd_Hst_Rate_Currency.DataSource = Bs_Hst_Rate_Cur;
        }
    }
}
