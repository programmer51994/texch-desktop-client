﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Rate_Currency_ADD_Upd : Form
    {
        String Sql_Text = "";
        DataTable DT = new DataTable();
        BindingSource _Bs_Rate_Cur = new BindingSource();
        Int64 current_cur_id = 0;

        public Rate_Currency_ADD_Upd()
        {
            InitializeComponent();
           
            Grd_Rate_Currency.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
          connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
        }

        private void Rate_Currency_ADD_Upd_Load(object sender, EventArgs e)
        {
            Grd_Rate_Currency.Columns["Column2"].DataPropertyName = "Cur_ENAME";

            string Cur_id_Comp = "";
            Cur_id_Comp = connection.SqlExec("Select Distinct Base_Cur_Id from Companies", "Cur_comp").Rows[0]["Base_Cur_Id"].ToString();
            Sql_Text = "Select  A.Cur_ID , B.Cur_ANAME , B.Cur_ENAME,  cast ( Exch_Rate as decimal(20,7)) as Exch_Rate ,C.User_Name , ISNULL(A.U_date ,A.C_Date) As C_Date,cast ( Exch_Rate_Real as decimal(20,7)) as Exch_Rate_Real "
                    + " From  Currencies_Rate A , Cur_Tbl B  , USERS C "
                    + " where A.cur_id = B.Cur_ID "
                    + " And A.User_id =  C.USER_ID "
                    + " And A.cur_id not in ( 0 ," + Cur_id_Comp + ") "
                    + " union "
                    + " Select    B.Cur_ID , B.Cur_ANAME , B.Cur_ENAME,  cast (0 as decimal(20,7)) as Exch_Rate ,'' as user_name , GETDATE() As C_Date,cast ( 0 as decimal(20,7)) as Exch_Rate_Real "
                    + " From      Cur_Tbl B   "
                    + " where  B.cur_id not in ( 0 ," + Cur_id_Comp + ")"
                    + " And B.Cur_ID Not in ( Select Cur_ID  from  Currencies_Rate ) ";
            _Bs_Rate_Cur.DataSource = connection.SqlExec(Sql_Text, "Rate_Cur_TBL");
            Grd_Rate_Currency.DataSource = _Bs_Rate_Cur;
        }


        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            string cur_text = "";
            DataTable DT_rate_Cur_TBL = new DataTable();
            DT_rate_Cur_TBL = connection.SQLDS.Tables["Rate_Cur_TBL"].DefaultView.ToTable(false, "Cur_ID", "Exch_Rate", "User_Name", "C_Date").Select().CopyToDataTable();
            foreach (DataGridViewRow DRow1 in Grd_Rate_Currency.Rows)
            {

                string Mcur_id = DRow1.Cells["Column1"].Value.ToString();
                cur_text = cur_text + Mcur_id + ',';  

            }

            connection.SQLCMD.Parameters.AddWithValue("@Tbl_Rate_Cur", DT_rate_Cur_TBL);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@cur_text", cur_text);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_Currencies_Rate", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
            this.Close();
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Print_Click(object sender, EventArgs e)
        {
            
            DataTable ExpDt = connection.SQLDS.Tables["Rate_Cur_TBL"].DefaultView.ToTable(false,
           "Cur_Id", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Exch_Rate", "Exch_Rate_Real", "C_Date",
           "User_Name").Select("Exch_Rate <> 0").CopyToDataTable();
            DataTable[] _EXP_DT = { ExpDt };
           DataGridView[] Export_GRD = { Grd_Rate_Currency };

            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Rate_Currency_ADD_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
           
            if (connection.SQLDS.Tables.Contains("Rate_Cur_TBL"))
                connection.SQLDS.Tables.Remove("Rate_Cur_TBL");
        }

        private void Rate_Currency_ADD_Upd_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            current_cur_id = Convert.ToInt64(((DataRowView)_Bs_Rate_Cur.Current).Row["cur_id"]);

            Sql_Text = " Exec reveal_hst_rate_cur " + current_cur_id;
            connection.SqlExec(Sql_Text, "tbl_hst_rate_cur");



            if (connection.SQLDS.Tables["tbl_hst_rate_cur"].Rows.Count > 0)
            {

                reveal_hst_rate_cur AddFrm = new reveal_hst_rate_cur();
                AddFrm.ShowDialog(this);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 2 ? "There is no historical information" : "لا توجد معلومات تاريخية", MyGeneral_Lib.LblCap);
            }
        }

    }
}