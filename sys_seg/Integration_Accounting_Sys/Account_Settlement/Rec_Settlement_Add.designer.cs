﻿namespace Integration_Accounting_Sys
{
    partial class Rec_Settlement_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Txt_Cur = new System.Windows.Forms.TextBox();
            this.Txt_Cust = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_Acc = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_Details = new System.Windows.Forms.TextBox();
            this.Cbo_Cust_Type = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_Check_No = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Grd_Acc = new System.Windows.Forms.DataGridView();
            this.Column101 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grdcur_id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_V_Ref_No = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Cbo_Rec_Date = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_Sum_Credit = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txtexch_rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txtfor_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Sum_Debit = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Grd_Rec_Daily = new System.Windows.Forms.DataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcur_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rec_Daily)).BeginInit();
            this.SuspendLayout();
            // 
            // Txt_Cur
            // 
            this.Txt_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cur.Location = new System.Drawing.Point(428, 47);
            this.Txt_Cur.Name = "Txt_Cur";
            this.Txt_Cur.Size = new System.Drawing.Size(218, 23);
            this.Txt_Cur.TabIndex = 6;
            this.Txt_Cur.TextChanged += new System.EventHandler(this.Txt_Cur_TextChanged);
            // 
            // Txt_Cust
            // 
            this.Txt_Cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cust.Location = new System.Drawing.Point(754, 47);
            this.Txt_Cust.Name = "Txt_Cust";
            this.Txt_Cust.Size = new System.Drawing.Size(226, 23);
            this.Txt_Cust.TabIndex = 8;
            this.Txt_Cust.TextChanged += new System.EventHandler(this.Txt_Cust_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(680, 49);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 214;
            this.label7.Text = "الــثـانـوي:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(349, 50);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(68, 14);
            this.label6.TabIndex = 213;
            this.label6.Text = "العــــمـلــة:";
            // 
            // Txt_Acc
            // 
            this.Txt_Acc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Acc.Location = new System.Drawing.Point(79, 47);
            this.Txt_Acc.Name = "Txt_Acc";
            this.Txt_Acc.Size = new System.Drawing.Size(233, 23);
            this.Txt_Acc.TabIndex = 4;
            this.Txt_Acc.TextChanged += new System.EventHandler(this.Txt_Acc_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(15, 49);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(63, 14);
            this.label5.TabIndex = 212;
            this.label5.Text = "الحســاب:";
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(890, 217);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(90, 24);
            this.BtnDel.TabIndex = 16;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(800, 217);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(90, 24);
            this.BtnAdd.TabIndex = 15;
            this.BtnAdd.Text = "اضــافــة";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(11, 222);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(76, 14);
            this.label10.TabIndex = 239;
            this.label10.Text = "الـتـفـاصـيـل:";
            // 
            // Txt_Details
            // 
            this.Txt_Details.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Details.Location = new System.Drawing.Point(94, 218);
            this.Txt_Details.Name = "Txt_Details";
            this.Txt_Details.Size = new System.Drawing.Size(549, 23);
            this.Txt_Details.TabIndex = 14;
            // 
            // Cbo_Cust_Type
            // 
            this.Cbo_Cust_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Cust_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Cust_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Cust_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Cust_Type.FormattingEnabled = true;
            this.Cbo_Cust_Type.Location = new System.Drawing.Point(94, 162);
            this.Cbo_Cust_Type.Name = "Cbo_Cust_Type";
            this.Cbo_Cust_Type.Size = new System.Drawing.Size(216, 24);
            this.Cbo_Cust_Type.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(11, 167);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(74, 14);
            this.label8.TabIndex = 238;
            this.label8.Text = "الـــحـالــــــة:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(717, 194);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(77, 14);
            this.label12.TabIndex = 237;
            this.label12.Text = "رقـم الـصـك:";
            // 
            // Txt_Check_No
            // 
            this.Txt_Check_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Check_No.Location = new System.Drawing.Point(800, 190);
            this.Txt_Check_No.Name = "Txt_Check_No";
            this.Txt_Check_No.Size = new System.Drawing.Size(180, 23);
            this.Txt_Check_No.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(327, 194);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(108, 14);
            this.label11.TabIndex = 236;
            this.label11.Text = "سعـــر التعــــــادل:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(11, 194);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 235;
            this.label9.Text = "المــبــلــــــغ:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(368, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(141, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(15, 6);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(232, 23);
            this.TxtTerm_Name.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(772, 10);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 249;
            this.label3.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(836, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 3;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(253, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 14);
            this.label1.TabIndex = 248;
            this.label1.Text = "اسم المستخــدم:";
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Btn_OK.ForeColor = System.Drawing.Color.Navy;
            this.Btn_OK.Location = new System.Drawing.Point(399, 527);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(99, 28);
            this.Btn_OK.TabIndex = 18;
            this.Btn_OK.Text = "موافق";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(4, 38);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(986, 1);
            this.flowLayoutPanel9.TabIndex = 254;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(498, 527);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(99, 28);
            this.ExtBtn.TabIndex = 19;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Grd_Acc
            // 
            this.Grd_Acc.AllowUserToAddRows = false;
            this.Grd_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Acc.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Acc.ColumnHeadersHeight = 40;
            this.Grd_Acc.ColumnHeadersVisible = false;
            this.Grd_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column101,
            this.dataGridViewTextBoxColumn2});
            this.Grd_Acc.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Grd_Acc.Location = new System.Drawing.Point(15, 70);
            this.Grd_Acc.Name = "Grd_Acc";
            this.Grd_Acc.ReadOnly = true;
            this.Grd_Acc.RowHeadersVisible = false;
            this.Grd_Acc.RowHeadersWidth = 15;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc.Size = new System.Drawing.Size(297, 88);
            this.Grd_Acc.TabIndex = 5;
            this.Grd_Acc.SelectionChanged += new System.EventHandler(this.Grd_Acc_SelectionChanged_1);
            // 
            // Column101
            // 
            this.Column101.DataPropertyName = "Acc_Id";
            this.Column101.HeaderText = "رقم الحساب";
            this.Column101.Name = "Column101";
            this.Column101.ReadOnly = true;
            this.Column101.Width = 128;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Acc_Aname";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 210;
            // 
            // Grdcur_id
            // 
            this.Grdcur_id.AllowUserToAddRows = false;
            this.Grdcur_id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcur_id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grdcur_id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdcur_id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grdcur_id.ColumnHeadersHeight = 40;
            this.Grdcur_id.ColumnHeadersVisible = false;
            this.Grdcur_id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.Grdcur_id.Location = new System.Drawing.Point(349, 70);
            this.Grdcur_id.Name = "Grdcur_id";
            this.Grdcur_id.ReadOnly = true;
            this.Grdcur_id.RowHeadersVisible = false;
            this.Grdcur_id.RowHeadersWidth = 15;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdcur_id.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grdcur_id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grdcur_id.Size = new System.Drawing.Size(297, 88);
            this.Grdcur_id.TabIndex = 7;
            this.Grdcur_id.SelectionChanged += new System.EventHandler(this.Grdcur_id_SelectionChanged_1);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Cur_Id";
            this.dataGridViewTextBoxColumn4.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 128;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Cur_Aname";
            this.dataGridViewTextBoxColumn5.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 210;
            // 
            // Grd_cust
            // 
            this.Grd_cust.AllowUserToAddRows = false;
            this.Grd_cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_cust.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_cust.ColumnHeadersHeight = 40;
            this.Grd_cust.ColumnHeadersVisible = false;
            this.Grd_cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column12});
            this.Grd_cust.Location = new System.Drawing.Point(683, 70);
            this.Grd_cust.Name = "Grd_cust";
            this.Grd_cust.ReadOnly = true;
            this.Grd_cust.RowHeadersVisible = false;
            this.Grd_cust.RowHeadersWidth = 15;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_cust.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_cust.Size = new System.Drawing.Size(297, 88);
            this.Grd_cust.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Cust_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 128;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "ACust_Name";
            this.Column12.HeaderText = "اسم الحساب";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 210;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 38);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 483);
            this.flowLayoutPanel1.TabIndex = 587;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(4, 521);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(986, 1);
            this.flowLayoutPanel2.TabIndex = 588;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(989, 38);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1, 483);
            this.flowLayoutPanel3.TabIndex = 589;
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(616, 5);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(150, 23);
            this.Txt_Loc_Cur.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(519, 9);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 591;
            this.label2.Text = "العملة المحلية:";
            // 
            // Txt_V_Ref_No
            // 
            this.Txt_V_Ref_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_V_Ref_No.Location = new System.Drawing.Point(433, 164);
            this.Txt_V_Ref_No.Name = "Txt_V_Ref_No";
            this.Txt_V_Ref_No.Size = new System.Drawing.Size(211, 23);
            this.Txt_V_Ref_No.TabIndex = 603;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(328, 167);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(107, 14);
            this.label4.TabIndex = 604;
            this.label4.Text = "أشــــارة مرجعية :";
            // 
            // Cbo_Rec_Date
            // 
            this.Cbo_Rec_Date.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Rec_Date.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Rec_Date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rec_Date.FormattingEnabled = true;
            this.Cbo_Rec_Date.Location = new System.Drawing.Point(200, 490);
            this.Cbo_Rec_Date.Name = "Cbo_Rec_Date";
            this.Cbo_Rec_Date.Size = new System.Drawing.Size(170, 24);
            this.Cbo_Rec_Date.TabIndex = 605;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(11, 495);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(187, 14);
            this.label13.TabIndex = 606;
            this.label13.Text = "تنظيـــم القيد على سجلات يوم :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(674, 534);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(131, 14);
            this.label15.TabIndex = 610;
            this.label15.Text = "مجموع الطرف الدائن :";
            // 
            // Txt_Sum_Credit
            // 
            this.Txt_Sum_Credit.BackColor = System.Drawing.Color.White;
            this.Txt_Sum_Credit.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sum_Credit.Location = new System.Drawing.Point(807, 530);
            this.Txt_Sum_Credit.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Sum_Credit.Name = "Txt_Sum_Credit";
            this.Txt_Sum_Credit.NumberDecimalDigits = 3;
            this.Txt_Sum_Credit.NumberDecimalSeparator = ".";
            this.Txt_Sum_Credit.NumberGroupSeparator = ",";
            this.Txt_Sum_Credit.Size = new System.Drawing.Size(173, 23);
            this.Txt_Sum_Credit.TabIndex = 611;
            this.Txt_Sum_Credit.Text = "0.000";
            // 
            // txtexch_rate
            // 
            this.txtexch_rate.BackColor = System.Drawing.Color.White;
            this.txtexch_rate.Enabled = false;
            this.txtexch_rate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexch_rate.Location = new System.Drawing.Point(433, 190);
            this.txtexch_rate.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txtexch_rate.Name = "txtexch_rate";
            this.txtexch_rate.NumberDecimalDigits = 7;
            this.txtexch_rate.NumberDecimalSeparator = ".";
            this.txtexch_rate.NumberGroupSeparator = ",";
            this.txtexch_rate.Size = new System.Drawing.Size(211, 23);
            this.txtexch_rate.TabIndex = 12;
            this.txtexch_rate.Text = "0.0000000";
            // 
            // Txtfor_Amount
            // 
            this.Txtfor_Amount.BackColor = System.Drawing.Color.White;
            this.Txtfor_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtfor_Amount.Location = new System.Drawing.Point(94, 190);
            this.Txtfor_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtfor_Amount.Name = "Txtfor_Amount";
            this.Txtfor_Amount.NumberDecimalDigits = 3;
            this.Txtfor_Amount.NumberDecimalSeparator = ".";
            this.Txtfor_Amount.NumberGroupSeparator = ",";
            this.Txtfor_Amount.Size = new System.Drawing.Size(216, 23);
            this.Txtfor_Amount.TabIndex = 11;
            this.Txtfor_Amount.Text = "0.000";
            // 
            // Txt_Sum_Debit
            // 
            this.Txt_Sum_Debit.BackColor = System.Drawing.Color.White;
            this.Txt_Sum_Debit.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sum_Debit.Location = new System.Drawing.Point(149, 528);
            this.Txt_Sum_Debit.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Sum_Debit.Name = "Txt_Sum_Debit";
            this.Txt_Sum_Debit.NumberDecimalDigits = 3;
            this.Txt_Sum_Debit.NumberDecimalSeparator = ".";
            this.Txt_Sum_Debit.NumberGroupSeparator = ",";
            this.Txt_Sum_Debit.Size = new System.Drawing.Size(173, 23);
            this.Txt_Sum_Debit.TabIndex = 613;
            this.Txt_Sum_Debit.Text = "0.000";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(10, 532);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(135, 14);
            this.label14.TabIndex = 612;
            this.label14.Text = "مجموع الطرف المدين :";
            // 
            // Grd_Rec_Daily
            // 
            this.Grd_Rec_Daily.AllowUserToAddRows = false;
            this.Grd_Rec_Daily.AllowUserToDeleteRows = false;
            this.Grd_Rec_Daily.AllowUserToOrderColumns = true;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Grd_Rec_Daily.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Rec_Daily.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.Grd_Rec_Daily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Rec_Daily.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.Grd_Rec_Daily.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.Grd_Rec_Daily.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rec_Daily.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Rec_Daily.ColumnHeadersHeight = 35;
            this.Grd_Rec_Daily.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column9,
            this.Column6,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column1,
            this.Column2,
            this.Column7,
            this.Column10,
            this.Column11});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Rec_Daily.DefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_Rec_Daily.Location = new System.Drawing.Point(14, 245);
            this.Grd_Rec_Daily.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Grd_Rec_Daily.Name = "Grd_Rec_Daily";
            this.Grd_Rec_Daily.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.Grd_Rec_Daily.RowHeadersWidth = 15;
            this.Grd_Rec_Daily.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grd_Rec_Daily.RowTemplate.Height = 26;
            this.Grd_Rec_Daily.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Rec_Daily.Size = new System.Drawing.Size(967, 243);
            this.Grd_Rec_Daily.TabIndex = 614;
            this.Grd_Rec_Daily.SelectionChanged += new System.EventHandler(this.Grd_Rec_Daily_SelectionChanged_1);
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column8.DataPropertyName = "dLoc_amount";
            dataGridViewCellStyle12.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column8.HeaderText = "مدين (ع .م)";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 70;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column9.DataPropertyName = "cLoc_amount";
            dataGridViewCellStyle13.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column9.HeaderText = "دائن(ع.م)";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Width = 60;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column6.DataPropertyName = "Acc_id";
            this.Column6.HeaderText = "رقم الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column6.Width = 69;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.DataPropertyName = "ACC_Name";
            this.Column3.HeaderText = "اسم الحساب";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 69;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column4.DataPropertyName = "Cur_Name";
            this.Column4.HeaderText = "العملة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 38;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column5.DataPropertyName = "cust_Name";
            this.Column5.HeaderText = "الحساب الثانوي";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column5.Width = 83;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column1.DataPropertyName = "dFor_Amount";
            dataGridViewCellStyle14.Format = "N3";
            dataGridViewCellStyle14.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle14;
            this.Column1.HeaderText = "مدين (ع .ص)";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.DataPropertyName = "cFor_Amount";
            dataGridViewCellStyle15.Format = "N3";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle15;
            this.Column2.HeaderText = "دائن(ع.ص)";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 70;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column7.DataPropertyName = "Exch_price";
            dataGridViewCellStyle16.Format = "N7";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle16;
            this.Column7.HeaderText = "سعر التعادل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 67;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column10.DataPropertyName = "check_no";
            this.Column10.HeaderText = "رقم الصك";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column10.Width = 60;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column11.DataPropertyName = "notes";
            this.Column11.HeaderText = "التفاصيل";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column11.Width = 52;
            // 
            // Rec_Settlement_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 563);
            this.Controls.Add(this.Grd_Rec_Daily);
            this.Controls.Add(this.Txt_Sum_Debit);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_Sum_Credit);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Cbo_Rec_Date);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_V_Ref_No);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Grd_cust);
            this.Controls.Add(this.Grdcur_id);
            this.Controls.Add(this.Grd_Acc);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtexch_rate);
            this.Controls.Add(this.Txtfor_Amount);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_Details);
            this.Controls.Add(this.Cbo_Cust_Type);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_Check_No);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Txt_Cur);
            this.Controls.Add(this.Txt_Cust);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_Acc);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Rec_Settlement_Add";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "456";
            this.Text = "اضافة قيد تسوية محاسبية";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Rec_Daily_Add_FormClosed);
            this.Load += new System.EventHandler(this.Rec_Daily_Add_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdcur_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rec_Daily)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Txt_Cur;
        private System.Windows.Forms.TextBox Txt_Cust;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_Acc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_Details;
        private System.Windows.Forms.ComboBox Cbo_Cust_Type;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_Check_No;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Sample.DecimalTextBox Txtfor_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox txtexch_rate;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.DataGridView Grd_Acc;
        private System.Windows.Forms.DataGridView Grdcur_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridView Grd_cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column101;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_V_Ref_No;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Cbo_Rec_Date;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Sum_Credit;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Sum_Debit;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView Grd_Rec_Daily;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
    }
}