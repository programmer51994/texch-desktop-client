﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Rec_Settlement_Main : Form
    {
        bool GrdChange = false;
        int VO_NO = 0;
        int nrec_date = 0;
        DataTable Dt_Details = new DataTable();
        DataTable Voucher_Cur = new DataTable();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs_Rec_Daily = new BindingSource();
        BindingSource _Bs_Rec_Daily1 = new BindingSource();
        public Rec_Settlement_Main()
        {
            InitializeComponent();
            TextBox txt = new TextBox();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, txt, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Vo_Rec.AutoGenerateColumns = false;
            Grd_Rec_Daily.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                Grd_Vo_Rec.Columns["Column3"].DataPropertyName = "Cur_ename";
                Grd_Rec_Daily.Columns["Column14"].DataPropertyName = "Acc_ename";
                Grd_Rec_Daily.Columns["Column15"].DataPropertyName = "cur_ename";
                Grd_Rec_Daily.Columns["Column16"].DataPropertyName = "Ecust_name";
            }
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
        }

        private void Rec_Daily_Main_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()
            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            #endregion 
            Getdata(75);
        }

        private void Getdata(int Oper_Id)
        {
            GrdChange = false;
            Voucher_Cur.Rows.Clear();
            Dt_Details.Rows.Clear();
            _Bs = new BindingSource();

            object[] Sparams = { connection.T_ID, connection.user_id, string.Empty ,
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo,Oper_Id };
            MyGeneral_Lib.Copytocliptext("Main_Vouchers_Rap", Sparams);
            _Bs_Rec_Daily.DataSource = connection.SqlExec("Main_Vouchers_Rap", "Rec_daily_Tbl", Sparams);
            Grd_Vo_Rec.DataSource = _Bs_Rec_Daily;
            //      //==============================
            if (connection.SQLDS.Tables["Rec_daily_Tbl"].Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Vo_Rec_SelectionChanged(null, null);
            }
            else
            {
                BindingSource _B_c = new BindingSource();
                Grd_Rec_Daily.DataSource = _B_c;
            }

        }


        private void Grd_Vo_Rec_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                DataRowView DRV = _Bs_Rec_Daily.Current as DataRowView;
                DataRow DR = DRV.Row;
                VO_NO = DR.Field<int>("VO_NO");
                nrec_date = DR.Field<int>("N_r_c");
                LblRec.Text = connection.Records(_Bs_Rec_Daily);
                object[] vparam = { connection.T_ID, connection.user_id, VO_NO, nrec_date, 75 };
                _Bs_Rec_Daily1.DataSource = connection.SqlExec("get_rec_info", "V_Tbl", vparam);
                Grd_Rec_Daily.DataSource = _Bs_Rec_Daily1;
               
            }
 
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Rec_Settlement_Add AddFrm = new Rec_Settlement_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Getdata(75);
            this.Visible = true;
        }

        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Getdata(75);
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(75);
            SearchFrm.ShowDialog(this);
            Getdata(75);
  
        }


        private void Rec_Daily_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;


            string[] Used_Tbl = { "Rec_daily_Tbl", "V_Tbl", "Rec_cur" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }

            }
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            int Vo_No = Convert.ToInt16(((DataRowView)_Bs_Rec_Daily.Current).Row["Vo_No"]);
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + Convert.ToInt32(((DataRowView)_Bs_Rec_Daily.Current).Row["N_r_c"]) + ",75," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Rec_cur");
            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date"]);
            string _Date = ((DataRowView)_Bs_Rec_Daily.Current).Row["Nrec_Date"].ToString();
           // string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            //string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب الدائن" + ": " +
            //                 ((DataRowView)_Bs_Rec_Daily1.Current).Row["Acust_Name"].ToString();
            //string ECur_Cust = "Credit Account" + ": " + ((DataRowView)_Bs_Rec_Daily1.Current).Row["Ecust_Name"].ToString() + " / " +
            //                          connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

            Decimal neg_sum = Math.Abs(Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 < 0")));
            Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", 75);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("neg_sum", neg_sum);
            Dt_Param.Rows.Add("pos_sum", pos_sum);
            Dt_Param.Rows.Add("Frm_Id", "Main");
            Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());
            DataRow Dr;
            int y = connection.SQLDS.Tables["Rec_cur"].Rows.Count;
            if (y <= 10)
            {
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Rec_cur"].NewRow();
                    Dr["R_T_Y_Id"] = "100000000000000000"; // اضيف لترتيب القيود في الطباعة
                    connection.SQLDS.Tables["Rec_cur"].Rows.Add(Dr);
                }


                Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
                Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();


                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 75);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Rec_cur");
            }
            else
            {
                Rec_Daily_Rpt_Multirow ObjRpt1 = new Rec_Daily_Rpt_Multirow();
                Rec_Daily_Rpt_Multirow ObjRptEng1 = new Rec_Daily_Rpt_Multirow();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 75);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Rec_cur");

            }

           
            
           // connection.SqlExec(SqlTxt, "Rec_cur");
           // DateTime C_Date;
           // C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date"]);
           //// string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
          // string _Date = ((DataRowView)_Bs_Rec_Daily.Current).Row["Nrec_Date"].ToString();
           // string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب الدائن" + ": " +
           //                  ((DataRowView)_Bs_Rec_Daily1.Current).Row["Acust_Name"].ToString();
           // string ECur_Cust = "Credit Account" + ": " + ((DataRowView)_Bs_Rec_Daily1.Current).Row["Ecust_Name"].ToString() + " / " +
           //                           connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

           // Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
           // Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();
           // DataTable Dt_Param = CustomControls.CustomParam_Dt();
           // Dt_Param.Rows.Add("Vo_No", Vo_No);
           // Dt_Param.Rows.Add("Oper_Id", 75);
           // Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
           // Dt_Param.Rows.Add("C_Date", C_Date);
           // Dt_Param.Rows.Add("_Date", _Date);
           // Dt_Param.Rows.Add("Frm_Id", "Main");
           // Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());
           // DataRow Dr;
           // int y = connection.SQLDS.Tables["Rec_cur"].Rows.Count;
           // for (int i = 0; i < (10 - y); i++)
           // {
           //     Dr = connection.SQLDS.Tables["Rec_cur"].NewRow();
           //     connection.SQLDS.Tables["Rec_cur"].Rows.Add(Dr);
           // }

           // Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param,75);

           // this.Visible = false;
           // RptLangFrm.ShowDialog(this);
           // this.Visible = true;
           // connection.SQLDS.Tables.Remove("Rec_cur");
        }

        private void Rec_Daily_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Rec_Settlement_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        
        }
    }
}