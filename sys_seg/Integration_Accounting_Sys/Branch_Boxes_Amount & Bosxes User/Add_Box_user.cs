﻿using System;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Add_Box_user : Form
    {
        public Add_Box_user()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), Txt_cur_date, new TextBox());
        }
        //-----------------------
        private void Add_Box_user_Load(object sender, EventArgs e)
        {
            connection.SqlExec("exec Add_Del_Box " + connection.T_ID + ",0,0,0,1,0", "Box_tbl");

            Cbo_Box.DataSource = connection.SQLDS.Tables["Box_tbl"];
            Cbo_Box.ValueMember = "Cust_id";
            Cbo_Box.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";

            Cbo_user.DataSource = connection.SQLDS.Tables["Box_tbl1"];
            Cbo_user.ValueMember = "User_id";
            Cbo_user.DisplayMember = "User_name";
        }
        //-----------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (connection.SQLDS.Tables["Box_tbl"].Rows.Count == 0 && connection.SQLDS.Tables["Box_tbl1"].Rows.Count ==0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لايوجد صندوق و مستخدم للتعريف" : "No Box and user defined");
                return; 
            }
            if (connection.SQLDS.Tables["Box_tbl"].Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لايوجد صندوق للتعريف" : "No Box defined");
                return;
            }
            if (connection.SQLDS.Tables["Box_tbl1"].Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لايوجد مستخدم للتعريف" : "No user defined");
                return;
            }
            #endregion 
            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, connection.T_ID );
            ItemList.Insert(1, Cbo_Box.SelectedValue );
            ItemList.Insert(2, Cbo_user.SelectedValue );
            ItemList.Insert(3, connection.user_id );
            ItemList.Insert(4, 2);
            ItemList.Insert(5, "");
            connection.scalar("Add_Del_Box", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Add_Box_user_Load(null, null);
        }
        //-----------------------
        private void Add_Box_user_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Box_tbl", "Box_tbl1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Cbo_user_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
