﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys 
{
    public partial class cost_rate_frm : Form
    {
        #region MyRegion
        string @format = "dd/MM/yyyy";
        BindingSource BS_Revel_Box_Amount = new BindingSource();
        #endregion

        public cost_rate_frm()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cust.AutoGenerateColumns = false;
            //Grd_Rec_Box_Amount.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column7"].DataPropertyName = "cur_EName";
                //Grd_Rec_Box_Amount.Columns["Column11"].DataPropertyName = "Eoper_Name";
            }
        }
        //----------------------------------
        private void cost_rate_frm_Load(object sender, EventArgs e)
        {
            Txt_To.CustomFormat = " ";
            Txt_From.CustomFormat = " ";
            
                Txt_To.Format = DateTimePickerFormat.Custom;
                Txt_To.CustomFormat = @format;
        
                Txt_From.Format = DateTimePickerFormat.Custom;
                Txt_From.CustomFormat = @format;
            
                string SqlTxt = " Select  ACUST_NAME, ECUST_NAME , T_ID  , b.loc_cur_id,c.CIT_ID,Cit_AName ,isnull(cust_online_id,0) as cust_online_id,isnull(A.cust_id,0) as Term_cust_id , comm_rem_flag,period_password  "
                     + " From CUSTOMERS A, TERMINALS B, Cit_Tbl c "
                     + " where A.CUST_ID = B.CUST_ID "
                     + " and a.CIT_ID = c.CIT_ID";

                Cbo_Term.DataSource = connection.SqlExec(SqlTxt, "Term_Tbl");
                Cbo_Term.ValueMember = "T_ID";
                Cbo_Term.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";



        }
        //-------------------------------------------------
        private void Emp_Box_Amount_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Str = { "Revel_Box_Amount_TBL", "Revel_Box_Amount_TBL1", "Term_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //----------------------------------------------------
      
        //----------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            int FromDATE = 0;
            int TODATE = 0;

            if (Txt_From.Checked == true)
            {
                DateTime date_call = Txt_From.Value.Date;
                FromDATE = date_call.Day + date_call.Month * 100 + date_call.Year * 10000;
            }
            else
            {
                FromDATE = 0;
            }

            if (Txt_To.Checked == true)
            {
                DateTime date_rec = Txt_To.Value.Date;
                TODATE = date_rec.Day + date_rec.Month * 100 + date_rec.Year * 10000;
            }
            else
            {
                TODATE = 0;
            }

            String Sql_Txt = "Exec Reveal_Buy_Sale_rate_s " + FromDATE + "," + TODATE + "," + Convert.ToInt16(Cbo_Term.SelectedValue);
            connection.SqlExec(Sql_Txt, "Revel_Box_Amount_TBL");

            if (connection.SQLDS.Tables["Revel_Box_Amount_TBL1"].Rows.Count > 0)
            {
                BS_Revel_Box_Amount.DataSource = connection.SQLDS.Tables["Revel_Box_Amount_TBL1"];
                Grd_Cust.DataSource = BS_Revel_Box_Amount;
            }
            else
            {
                Grd_Cust.DataSource = new BindingSource();
            }
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            DataTable DT_Revel_Box_Amount = connection.SQLDS.Tables["Revel_Box_Amount_TBL1"].DefaultView.ToTable(false, "For_cur_id",
                 connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME" , "cost_rate");
            DataTable[] _EXP_DT = { DT_Revel_Box_Amount };
            DataGridView[] Export_GRD = { Grd_Cust };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cbo_Term_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grd_Cust.DataSource = new BindingSource();
        }
    }
}