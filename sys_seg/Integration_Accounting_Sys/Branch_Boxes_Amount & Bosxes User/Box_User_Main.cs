﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Box_User_Main : Form
    {
        #region MyRegion
        bool change = false;
        string Sql_Text = "";
        BindingSource _BS_Grd_main = new BindingSource();
        #endregion
        public Box_User_Main()
        {
            InitializeComponent();
           
            Grd_Box.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), Txt_cur_date, TxtTerm_Name);
            if (connection.Lang_id != 1)
            {
                Grd_Box.Columns["column2"].DataPropertyName = "ECust_Name";
            }
        }
        //-----------------------
        private void Box_user_main_Load(object sender, EventArgs e)
        {
            change = false;

            Sql_Text = " select 0 as chk ,a.CUST_ID,b.ACUST_NAME,b.ECUST_NAME,a.Buser_Id,C.User_Name "
                    + " from Box_User a,CUSTOMERS b , USERS c "
                    + " where a.CUST_ID = b.CUST_ID "
                    + " and a.Buser_Id = c.USER_ID "
                    + " and a.T_Id = " + connection.T_ID;
            _BS_Grd_main.DataSource = connection.SqlExec(Sql_Text, "Box_tbl_v");
            if (connection.SQLDS.Tables["Box_tbl_v"].Rows.Count > 0)
            {
                DelBtn.Enabled = true;
                Grd_Box.DataSource = _BS_Grd_main;
                change = true;
                Grd_Box_SelectionChanged(null, null);
            }
            else DelBtn.Enabled = false;
        }
        //-----------------------
        private void Grd_Box_SelectionChanged(object sender, EventArgs e)
        {
            if (change)
            {
                LblRec.Text = connection.Records(_BS_Grd_main);
            }
        }
        //-----------------------
        private void Box_user_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
          
            string[] Used_Tbl = { "Box_tbl_v" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-----------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Add_Box_user addbox = new Add_Box_user();
            this.Visible = false;
            addbox.ShowDialog(this);
            Box_user_main_Load(null, null);
            this.Visible = true;
        }
        //-----------------------
        private void DelBtn_Click(object sender, EventArgs e)
        {
            #region Validations
            int Rec = connection.SQLDS.Tables["Box_tbl_v"].Select("Chk > 0").Length;
            #endregion
            if (Rec > 0)
            {
                DialogResult dr;
                dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد ؟" : "Are you sure you want to delete this recored ?", connection.Lang_id == 1 ? "تأكيد الحذف" : "Confirm Deletion", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    foreach (DataRow Boxrow in connection.SQLDS.Tables["Box_tbl_v"].Rows)
                    {
                        if (Boxrow["Chk"].ToString() != "0" && Boxrow["Chk"].ToString() != "")
                        {
                            int Mcust_id = Convert.ToInt32(Boxrow["Cust_Id"]);
                            connection.SqlExec("exec Add_Del_Box " + connection.T_ID + "," + Mcust_id + ",0,0,3,0");
                        }
                    }
                  Box_user_main_Load(null, null);
                }
            }
            else MessageBox.Show(connection.Lang_id == 1 ? "لم يتم التأشير من أجل الحذف" : "No Selected Item");
        }

        private void Box_User_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}