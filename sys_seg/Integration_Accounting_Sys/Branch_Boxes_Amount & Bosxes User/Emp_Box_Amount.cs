﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using Excel = Microsoft.Office.Interop.Excel;
namespace Integration_Accounting_Sys
{
    public partial class Emp_Box_Amount : Form
    {
        #region MyRegion
        bool Curchange = false;
        bool Grdchange = false;
        string Sql_Txt = "";
        private BindingSource BS = new BindingSource();
         BindingSource BS_Main_Emp_box = new BindingSource();
        #endregion

        public Emp_Box_Amount()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Rec_Box_Amount.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column1"].DataPropertyName = "Ecust_Name";
                Grd_Rec_Box_Amount.Columns["Column11"].DataPropertyName = "Eoper_Name";
            }
        }
        //----------------------------------
        private void Emp_Box_Amount_Load(object sender, EventArgs e)
        {
            Curchange = false  ;
            connection.T_Name = TxtTerm_Name.Text;
            Btn_Excel.Enabled = false;
            Btn_OK.Enabled = false;
            Sql_Txt = "Select cur_id,Cur_ANAME,Cur_ENAME from Cur_Tbl where Cur_ID in(Select for_cur_id from VOUCHER where CUST_ID in(Select CUST_ID from CUSTOMERS where Cust_Type_ID in (6,7) And Cust_Flag <> 0 ))"
                      + "Or Cur_ID in(Select for_cur_id from F_T_B where CUST_ID in "
                      + "(Select CUST_ID from CUSTOMERS where Cust_Type_ID in (6,7) And Cust_Flag <> 0 ))"
                      + " order by " + (connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME");
                     

            Cbo_Cur.DataSource = connection.SqlExec(Sql_Txt, "Cur_Tbl");
            Cbo_Cur.ValueMember = "Cur_Id";
            Cbo_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
            if (connection.SQLDS.Tables["Cur_Tbl"].Rows.Count > 0 )
            {
                Curchange = true;
                Cbo_Cur_SelectedIndexChanged(null, null);
            }

        }
        //----------------------------------------------------
        private void Cbo_Cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Curchange)
            {
                //Grd_Cust.DataSource = new BindingSource();
                //Grd_Rec_Box_Amount.DataSource = new BindingSource();
                //if (Grd_Rec_Box_Amount.RowCount <= 0)
                //    LblRec.Text = connection.Records(new BindingSource());
                //Btn_Excel.Enabled = false;
                //Btn_OK.Enabled = false;
                //Txt_Doc_S_Issue.Text = ((DataRowView)binding_phone.Current).Row["S_doc_issue"].ToString();

                Grdchange = false;
                Sql_Txt = "Exec Revel_Get_Rec_Info_Nnew " + Cbo_Cur.SelectedValue + "," + connection.T_ID;
                connection.SqlExec(Sql_Txt, "Revel_Box_Amount_TBL");
              
                if (connection.SQLDS.Tables["Revel_Box_Amount_TBL1"].Rows.Count > 0)
                {
                    BS_Main_Emp_box.DataSource = connection.SQLDS.Tables["Revel_Box_Amount_TBL1"];
                    Grd_Cust.DataSource = BS_Main_Emp_box;
                    Btn_OK.Enabled = true;
                    Btn_Excel.Enabled = true;
                    Grdchange = true;
                    Grd_Cust_SelectionChanged(null, null);
                    //cost_rate.Text = ((DataRowView)BS_Main_Emp_box.Current).Row["average_rate"].ToString();
                }
                else
                { Grd_Cust.DataSource = new BindingSource();
                Grd_Rec_Box_Amount.DataSource = new BindingSource();
                }
            }
        }
        
   
        //-------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (Grdchange)
            {
                DataTable Cust_Acc = new DataTable();
                Int32 Cust_Id = 0;
                decimal MBalance = 0;
                decimal MBalance_loc = 0;

                Cust_Id = Convert.ToInt32(((DataRowView)BS_Main_Emp_box.Current).Row["Cust_Id"]);
                try
                {
                    Cust_Acc = connection.SQLDS.Tables["Revel_Box_Amount_TBL"].Select("Cust_Id = " + Cust_Id).CopyToDataTable();
                
                }
                catch
                {
                    Cust_Acc = new DataTable();
                }
                foreach (DataRow Drow in Cust_Acc.Rows)
                {
                    if (Convert.ToInt32(Drow["RowNum"]) == 0)
                    {
                        MBalance = Convert.ToDecimal(Drow["DFor_Amount"]) - Convert.ToDecimal(Drow["CFor_Amount"]);
                        MBalance_loc =  Convert.ToDecimal(Drow["DLoc_Amount"]) - Convert.ToDecimal(Drow["Cloc_Amount"]);
                    }

                    if (Convert.ToInt32(Drow["RowNum"]) >= 1)
                    {
                        MBalance += Convert.ToDecimal(Drow["DFor_Amount"]) - Convert.ToDecimal(Drow["CFor_Amount"]);
                        MBalance_loc += Convert.ToDecimal(Drow["Dloc_Amount"]) - Convert.ToDecimal(Drow["Cloc_Amount"]);
                    }
                    Drow["Balance_for"] = MBalance;
                    Drow["Balance_loc"] = MBalance_loc;
                }
                //---------------

                BS.DataSource = Cust_Acc;
                Grd_Rec_Box_Amount.DataSource = BS;
                

                //int Cust_Id = Convert.ToInt32(((DataRowView)BS_Main_Emp_box.Current).Row["Cust_id"]);
                //int Cur_Id = Convert.ToInt32(((DataRowView)BS_Main_Emp_box.Current).Row["for_cur_Id"]);
                //int T_Id = connection.T_ID;
                //int Nrec_Date = Convert.ToInt32(((DataRowView)BS_Main_Emp_box.Current).Row["Nrec_Date"]);
                //String Parameter = T_Id + "," + Cust_Id + "," + Nrec_Date + "," + Cur_Id;
                //Sql_Txt = "Exec Revel_Get_Rec_Info " + Parameter;
                //BS.DataSource = connection.SqlExec(Sql_Txt, "Revel_Get_Rec_Tbl");
                //if (connection.SQLDS.Tables["Revel_Get_Rec_Tbl"].Rows.Count > 0)
                //{

                //    Grd_Rec_Box_Amount.DataSource = BS;
                //}
               
            }

        }
        //-------------------------------------------
        private void Grd_Rec_Box_Amount_SelectionChanged(object sender, EventArgs e)
        {
            if (Grd_Rec_Box_Amount.RowCount > 0)
                LblRec.Text = connection.Records(BS);
            else
                LblRec.Text = connection.Records(new BindingSource());
        }
        //-------------------------------------------
        private void Btn_Excel_Click(object sender, EventArgs e)
        {

            DataTable All_Cust_DT = connection.SQLDS.Tables["Revel_Box_Amount_TBL"].DefaultView.ToTable(false, "for_cur_id", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename",
                                                     connection.Lang_id == 1 ? "ACust_name" : "ECust_name", "for_Amount", "Nrec_Date1");
            DataTable[] _EXP_DT = { All_Cust_DT };
            DataGridView[] Export_GRD = { Grd_Cust };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
           

        }
        //-------------------------------------------------
        private void Emp_Box_Amount_FormClosed(object sender, FormClosedEventArgs e)
        {
            Curchange = false;
            Grdchange = false;
            
            string[] Str = { "Cur_Tbl", "Revel_Box_Amount_TBL", "Revel_Get_Rec_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //-----------------------------------------------------
        private void Btn_OK_Click(object sender, EventArgs e)
        {
            int cur_id = Convert.ToInt16(((DataRowView)BS_Main_Emp_box.Current).Row["for_cur_id"]);
            int Cust_id = Convert.ToInt16(((DataRowView)BS_Main_Emp_box.Current).Row["cust_id"]);

            DataTable Dt_Cust = connection.SQLDS.Tables["Revel_Box_Amount_TBL"].DefaultView.ToTable(false,
            "ACust_name", "ECust_name", "for_Amount", "Nrec_Date1", "Cur_Aname", "Cur_Ename", "for_cur_id", "cust_id").Select("for_cur_id = " + cur_id + " And cust_id = " + Cust_id).CopyToDataTable();
            Dt_Cust = Dt_Cust.DefaultView.ToTable(false, "for_cur_id", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename",
            connection.Lang_id == 1 ? "ACust_name" : "ECust_name", "for_Amount", "Nrec_Date1").Select().CopyToDataTable();

            DataTable DT_Rec_Box_Amount = connection.SQLDS.Tables["Revel_Get_Rec_Tbl"].DefaultView.ToTable(false,
            "Aoper_Name" ,"DFor_Amount", "CFor_Amount", "Balance_for", "Real_price", "Exch_price", "DLoc_Amount", "CLoc_Amount"
            , "Balance_Loc", "C_Date", "VO_NO", "Date_time", "User_name");
            DataTable[] _EXP_DT = { Dt_Cust, DT_Rec_Box_Amount };
            DataGridView[] Export_GRD = { Grd_Cust ,Grd_Rec_Box_Amount };

            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }
        //----------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Emp_Box_Amount_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
        
    }
}