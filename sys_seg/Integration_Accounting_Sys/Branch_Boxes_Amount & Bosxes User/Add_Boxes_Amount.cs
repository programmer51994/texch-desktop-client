﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;
namespace Integration_Accounting_Sys
{
    public partial class Add_Boxes_Amount : Form
    {
        #region MyRegion
        bool Curchange = false;
        bool Cbochange = false;
        string Sql_Txt = "";
        private BindingSource B_S = new BindingSource();
        BindingSource _DB_BS = new BindingSource();
        BindingSource _CD_BS = new BindingSource();
        BindingSource _BS_Grd = new BindingSource();
        private DataTable Box_Amt;
        string _Date = "";
        DateTime C_Date;
        int VO_NO = 0;
        string Cur_ToWord = "";
        string Cur_ToEWord = "";
        DataTable Dt = new DataTable();
        DataTable Dt2 = new DataTable();
        int count = 1;
        #endregion
        //----------------------------------------------------------
        public Add_Boxes_Amount()
        {
            InitializeComponent();
           
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_BoxAmount.AutoGenerateColumns = false;
            Create_Table();
        }
        //----------------------------------------------------------
        private void Add_Boxes_Amount_Load(object sender, EventArgs e)
        {
            Curchange = false;
            Sql_Txt = " Select ACUST_NAME,ECUST_NAME,A.CUST_ID ,"
                     + " B.User_Name + ' / ' +A.ACUST_NAME As DB_ABox_User,"
                     + " ECUST_NAME + ' / ' +B.User_Name As DB_EBox_User"
                     + " From CUSTOMERS  A,USERS B , Box_User C"
                     + " Where A.CUST_ID in(select CUST_ID from Box_User)"
                     + " And Cust_Flag <> 0"
                     + " And A.CUST_ID = C.CUST_ID"
                     + " And B.USER_ID = C.Buser_Id"
                     + " and C.t_id = "+ connection.T_ID
                     + " Order by " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");
            _DB_BS.DataSource = connection.SqlExec(Sql_Txt, "DebtBox_Tbl");
            Cbo_Debt.DataSource = _DB_BS;
            Cbo_Debt.DisplayMember = connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name";
            Cbo_Debt.ValueMember = "Cust_Id";
            Curchange = true;
            Cbo_Debt_SelectedValueChanged(null, null);
            //--------------
            Sql_Txt = "Select a.Cur_id,Cur_aname,Cur_ename,EXCH_RATE "
            + " from Currencies_Rate a,Cur_tbl b where a.cur_id = b.cur_id and a.cur_id in (select cur_id from ccp_list where  t_id = " + connection.T_ID + ")";
            connection.SqlExec(Sql_Txt, "Exch_Tbl");
        }
        //----------------------------------------------------------
        private void Cbo_Debt_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Curchange)
            {
                Cbochange = false;
                Sql_Txt = " Select ACUST_NAME,ECUST_NAME,A.CUST_ID ,"
                    + " B.User_Name + ' / ' +A.ACUST_NAME As CD_ABox_User,"
                    + " ECUST_NAME + ' / ' +B.User_Name As CD_EBox_User"
                    + " From CUSTOMERS  A,USERS B , Box_User C"
                    + " Where A.CUST_ID in(select CUST_ID from Box_User where t_id = "+ connection.T_ID+")"
                    + " And Cust_Flag <> 0"
                    + " And A.CUST_ID = C.CUST_ID"
                    + " And B.USER_ID = C.Buser_Id"
                    + " and C.t_id = " + connection.T_ID
                    + " And A.cust_id <> " + Cbo_Debt.SelectedValue;
                _CD_BS.DataSource = connection.SqlExec(Sql_Txt, "CreditBox_Tbl");
                if (connection.SQLDS.Tables["CreditBox_Tbl"].Rows.Count > 0)
                {
                    Cbo_Credit.DataSource = _CD_BS;
                    Cbo_Credit.DisplayMember = connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name";
                    Cbo_Credit.ValueMember = "Cust_Id";
                    Cbochange = true;
                    Cbo_Credit_SelectedIndexChanged(sender, e);
                }
                else
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد صندوق فرعي" : " there are no Boexs ", MyGeneral_Lib.LblCap);
                    return;
                }
            }
        }
        //----------------------------------------------------------
        private void Cbo_Credit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbochange)
            {
                Sql_Txt = " Select cur_id as For_Cur_Id,Cur_ANAME,Cur_ENAME,cur_code  from Cur_Tbl "
                         + "where Cur_ID in(select cur_id from CUSTOMERS_ACCOUNTS where CUST_ID in(" + Cbo_Credit.SelectedValue + ")"
                         + " and T_id = " + connection.T_ID
                         + " intersect Select cur_id from CUSTOMERS_ACCOUNTS where CUST_ID in(" + Cbo_Debt.SelectedValue + ")"+ " and T_id = "+ connection.T_ID +" )"
                         + "and Cur_ID in (select Cur_ID from CCP_list   where T_id = "+ connection.T_ID +")"
                         + " Union   SELECT Cur_id as For_Cur_Id,Cur_AName,Cur_EName,cur_code FROM Cur_Tbl "
                         + " Where Cur_id = " + Txt_Loc_Cur.Tag
                         + " And Cur_ID in(select cur_id from CUSTOMERS_ACCOUNTS "
                         + " where CUST_ID in(" + Cbo_Credit.SelectedValue + ") and T_id = "+ connection.T_ID + "intersect Select cur_id from CUSTOMERS_ACCOUNTS "
                         + " where  T_id = "+ connection.T_ID + " and CUST_ID in(" + Cbo_Debt.SelectedValue + ")) order by Cur_ANAME ";
                _BS_Grd.DataSource = connection.SqlExec(Sql_Txt, "box_cur_tbl");
                Cbo_Cur.DataSource = _BS_Grd;
                Cbo_Cur.ValueMember = "For_Cur_Id";
                Cbo_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
            }
        }
        //----------------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id" ,"Discount_amnt", "Min_price", "Max_price","Cur_Name",
                         "Cur_ToWord","Cur_ToEWord","DB_AUser_Box","DB_EUser_Box","Cur_Code","CD_AUser_Box","CD_EUser_Box","Id" , "V_Ref_No"
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal", "System.String",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.Int16" ,"System.String"
            };
            Box_Amt = CustomControls.Custom_DataTable("Box_Amt", Column, DType);
            Grd_BoxAmount.DataSource = B_S;
        }
        //----------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Valedation
            int Rows = Box_Amt.Rows.Count;
            if (Convert.ToInt16(Cbo_Debt.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد الحساب الاآخذ" : "Please Select Debt Account", MyGeneral_Lib.LblCap);
                Cbo_Debt.Focus();
                return;
            }

            if (Convert.ToInt16(Cbo_Credit.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد الحساب المعطي" : "Please Select Credit Account", MyGeneral_Lib.LblCap);
                Cbo_Credit.Focus();
                return;
            }

            if (Convert.ToDecimal(TxtTLoc_Amount.Text) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ رجاءا" : "Please Check The Amount ", MyGeneral_Lib.LblCap);
                TxtTLoc_Amount.Focus();
                return;
            }

            if (Rows >= 5)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الحد الاقصى للمناقلة 5 عملات " : "Maximum Number Of Transfers 5 Currencies", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(Cbo_Cur.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد العملة" : "Please Select The Currency ", MyGeneral_Lib.LblCap);
                Cbo_Cur.Focus();
                return;
            }

            int Recount = (from rec in Box_Amt.AsEnumerable()
                           where (int)rec["For_Cur_Id"] == (Int16)Cbo_Cur.SelectedValue
                           select new { }).Count();
            if (Recount > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يمكنك نقل اكثر من مبلغ لنفس العملة " : "You Cannot Transfer More Than Amount Of The Same Currency", MyGeneral_Lib.LblCap);
                Cbo_Cur.Focus();
                return;
            }
            #endregion

            DataRow DRow1 = Box_Amt.NewRow();
            decimal MExch_Price = 1;
            //=========================
            DataRow[] Row = connection.SQLDS.Tables["Exch_Tbl"].Select("Cur_id = " + Cbo_Cur.SelectedValue);
            if (Convert.ToInt16(Txt_Loc_Cur.Tag) != Convert.ToInt16(Cbo_Cur.SelectedValue))
            {
                MExch_Price = Convert.ToDecimal(Row[0]["EXCH_RATE"]);
            }
            double Total_Amount = Convert.ToDouble(TxtTLoc_Amount.Text);
            ToWord toWord = new ToWord(Total_Amount,
            new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "(" + toWord.ConvertToArabic() + " " + ((DataRowView)_BS_Grd.Current).Row["Cur_AName"].ToString() + " لاغير) ";
            Cur_ToEWord = "(" + toWord.ConvertToEnglish() + " " + ((DataRowView)_BS_Grd.Current).Row["Cur_EName"].ToString() + " not else.)";
            DRow1["Real_Price"] = MExch_Price;
            DRow1["Exch_Price"] = MExch_Price;
            DRow1["For_amount"] = TxtTLoc_Amount.Text;
            DRow1["Loc_cur_id"] = Txt_Loc_Cur.Tag;
            DRow1["Notes"] = Txt_Notes.Text;
            DRow1["For_cur_id"] = Cbo_Cur.SelectedValue;
            DRow1["Cur_Name"] = Cbo_Cur.Text;
            DRow1["Cur_ToWord"] = Cur_ToWord;
            DRow1["Cur_ToEWord"] = Cur_ToEWord;
            DRow1["Cur_code"] = ((DataRowView)_BS_Grd.Current).Row["Cur_code"].ToString();
            DRow1["Id"] = count;
            count++;

            Box_Amt.Rows.Add(DRow1);
            B_S.DataSource = Box_Amt;
            Grd_BoxAmount.Refresh();

            TxtTLoc_Amount.ResetText();
            Txt_Notes.ResetText();
            Cbo_Credit.Enabled = false;
            Cbo_Debt.Enabled = false;
            Grd_BoxAmount_SelectionChanged(null, null);
        }
        //----------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No User Defined", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(TxtBox_User.Tag) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد امين صندوق معرف " : "No Cashier Defined", MyGeneral_Lib.LblCap);
                return;
            }

            if (Box_Amt.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "There Are No Records To Add", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion

            DataTable Dt = new DataTable();
            Dt = Box_Amt.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "V_Ref_No");
            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Credit_Cust_Id", Cbo_Credit.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Debit_Cust_Id", Cbo_Debt.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 26);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;

            connection.SqlExec("Add_Boxes_Amount", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
               !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Box_Amt.Rows.Clear();
            Cbo_Credit.Enabled = true;
            Cbo_Debt.Enabled = true;
            Add_Boxes_Amount_Load(null, null);
        }
        //----------------------------------------------------------
        private void Add_Boxes_Amount_FormClosed(object sender, FormClosedEventArgs e)
        {
            Curchange = false;
            Cbochange = false;
            
            string[] Used_Tbl = { "DebtBox_Tbl", "Exch_Tbl", "CreditBox_Tbl", "box_cur_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //----------------------------------------------------------
        private void Grd_BoxAmount_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(B_S);
        }
        //----------------------------------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Box_Amt.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show((connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد " : "Are Sure You Want To Delete The Record ") + (Grd_BoxAmount.CurrentRow.Index + 1), MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    Box_Amt.Rows[Grd_BoxAmount.CurrentRow.Index].Delete();
                }
                if (Grd_BoxAmount.RowCount <= 0)
                {
                    Cbo_Debt.Enabled = true;
                    Cbo_Credit.Enabled = true;
                }
            }
        }
        //----------------------------------------------------------
        private void Cbo_Cur_SelectedValueChanged(object sender, EventArgs e)
        {
            TxtTLoc_Amount.ResetText();
            Txt_Notes.ResetText();
        }
        //-----------------
        private void print_Rpt_Pdf()
        {
            //-------------------------report
            Box_Amt.Rows[0].SetField("DB_AUser_Box", ((DataRowView)_DB_BS.Current).Row["DB_ABox_User"].ToString());
            Box_Amt.Rows[0].SetField("DB_EUser_Box", ((DataRowView)_DB_BS.Current).Row["DB_EBox_User"].ToString());
            Box_Amt.Rows[0].SetField("CD_AUser_Box", ((DataRowView)_CD_BS.Current).Row["CD_ABox_User"].ToString());
            Box_Amt.Rows[0].SetField("CD_EUser_Box", ((DataRowView)_CD_BS.Current).Row["CD_EBox_User"].ToString());
            //--------------------------

            C_Date = Convert.ToDateTime(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            string X = Convert.ToString(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            Box_Amount_Rpt ObjRpt = new Box_Amount_Rpt();
            Box_Amount_Rpt_Eng ObjRptEng = new Box_Amount_Rpt_Eng();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 26);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            DataRow Dr;
            int y = Box_Amt.Rows.Count;
            for (int i = 0; i < (5 - y); i++)
            {
                Dr = Box_Amt.NewRow();
                Box_Amt.Rows.Add(Dr);
            }
            connection.SQLDS.Tables.Add(Box_Amt);

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 26, true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Box_Amt");
        }

        //private void Cbo_Debt_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}