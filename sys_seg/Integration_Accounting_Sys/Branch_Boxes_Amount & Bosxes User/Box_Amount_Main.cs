﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Box_Amount_Main : Form
    {
        #region Declaration
        bool GrdChange = false;
        DataTable DT = new DataTable();
        string Sql_Txt = "";
        int Vo_No = 0;
        string _Date = "";
        DateTime C_Date;
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        string ACur_Cust = "";
        string ECur_Cust = "";
        BindingSource _Bs_Grd1 = new BindingSource();
        BindingSource _Bs_Grd2 = new BindingSource();

        #endregion
        //-----------------------
        public Box_Amount_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            GrdCurrency_Details.AutoGenerateColumns = false;
            Grd_Cust.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                GrdCurrency_Details.Columns["Column10"].DataPropertyName = "Acc_ename";
                GrdCurrency_Details.Columns["Column11"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Details.Columns["column12"].DataPropertyName = "Ecust_name";
            }

        }
        //-----------------------------------------------------
        private void Box_Amount_Main_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()
            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }

            if (TxtBox_User.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }

            #endregion 
            Get_Data(26);
        }
        //-----------------------------------------------------
        private void Get_Data(int oper_id)
        {
            Sql_Txt = "Main_Box_Amount "
                        + TxtIn_Rec_Date.Text + ","
                        + connection.T_ID + ","
                        + connection.user_id + ","
                        + oper_id;

            _Bs_Grd1.DataSource = connection.SqlExec(Sql_Txt, "Boxes_Tbl").DefaultView.ToTable(true, "Vo_No", "Nrec_Date", "User_Name", "C_Date1", "Acc_Id");
            Grd_Cust.DataSource = _Bs_Grd1;
            GrdChange = true;
            Grd_Cust_SelectionChanged(null, null);
        }
        //-----------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                if (_Bs_Grd1.Count > 0)
                {
                    #region Get_Current Columns

                    DataRowView DRV = _Bs_Grd1.Current as DataRowView;
                    DataRow DR = DRV.Row;
                    string Nrec_Date = DR.Field<string>("Nrec_Date");
                    int Vo_No = DR.Field<int>("Vo_No");
                    int Acc_id = DR.Field<int>("Acc_Id");

                    #endregion

                    var queryx = from Rec in connection.SQLDS.Tables["Boxes_Tbl"].AsEnumerable()
                                 where Rec.Field<int>("Acc_Id") == Acc_id
                               && Rec.Field<int>("Vo_No") == Vo_No
                               && Rec.Field<string>("Nrec_Date") == Nrec_Date
                                 orderby Rec.Field<Int16>("For_cur_id")
                                 select new
                                 {
                                     D_Amount = Rec.Field<decimal>("for_amount") >= 0 ? Rec.Field<decimal>("for_amount") : 0,
                                     For_Amount = Rec.Field<decimal>("for_amount") < 0 ? Math.Abs(Rec.Field<decimal>("for_amount")) : 0,
                                     Acc_Id = Rec.Field<int>("Acc_Id"),
                                     Acc_AName = Rec.Field<string>("Acc_AName"),
                                     Cur_Aname = Rec.Field<string>("cur_aname"),
                                     ACust_Name = Rec.Field<string>("ACust_Name"),
                                     Notes = Rec.Field<string>("notes"),
                                     Acc_EName = Rec.Field<string>("Acc_EName"),
                                     cur_Ename = Rec.Field<string>("cur_ename"),
                                     ECust_Name = Rec.Field<string>("ECust_Name")
                                 };

                    DataTable Dt1 = CustomControls.IEnumerableToDataTable(queryx);
                    _Bs_Grd2.DataSource = Dt1;
                    GrdCurrency_Details.DataSource = _Bs_Grd2;
                    Txt_Detl.DataBindings.Clear();
                    Txt_Detl.DataBindings.Add("Text", _Bs_Grd2, "Notes");
                    LblRec.Text = connection.Records();
                }
            }
        }
        //-----------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Add_Boxes_Amount frm = new Add_Boxes_Amount();
            this.Visible = false;
            frm.ShowDialog();
            Get_Data(26);
            this.Visible = true;
        }
        //-----------------------------------------------------
        private void Box_Amount_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
            
            if (connection.SQLDS.Tables.Contains("Boxes_Tbl", "Box_Amt"))
            {
                connection.SQLDS.Tables.Remove("Boxes_Tbl", "Box_Amt");
            }
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            int i;
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            double For_Amount = 0.000;
            Vo_No = Convert.ToInt32(((DataRowView)_Bs_Grd1.Current).Row["VO_NO"]);
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            string SqlTxt = "Exec Main_Report2 " + Vo_No + "," + TxtIn_Rec_Date.Text + "," + 26 + "," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Box_Amt");


            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Box_Amt"].Rows[0]["C_Date"]);
            int Count = connection.SQLDS.Tables["Box_Amt"].Rows.Count;
            connection.SQLDS.Tables["Box_Amt"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Box_Amt"].Columns.Add("Cur_ToEWord");
            for (i = 0; i < Count; i++)
            {
                For_Amount = Convert.ToDouble(connection.SQLDS.Tables["Box_Amt"].Rows[i]["For_Amount"]);

                ToWord toWord = new ToWord(Convert.ToDouble(For_Amount),
                    new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + connection.SQLDS.Tables["Box_Amt"].Rows[i]["Cur_Aname"].ToString() + " لاغير)- ";
                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["Box_Amt"].Rows[i]["cur_Ename"].ToString() + " not else.)-";

                
                connection.SQLDS.Tables["Box_Amt"].Rows[i].SetField("Cur_ToWord", Cur_ToWord);
                connection.SQLDS.Tables["Box_Amt"].Rows[i].SetField("Cur_ToEWord", Cur_ToEWord);
            }
            connection.SQLDS.Tables["Box_Amt"].Columns.Add("DB_AUser_Box");
            connection.SQLDS.Tables["Box_Amt"].Columns.Add("DB_EUser_Box");
            connection.SQLDS.Tables["Box_Amt"].Columns.Add("CD_AUser_Box");
            connection.SQLDS.Tables["Box_Amt"].Columns.Add("CD_EUser_Box");

            connection.SQLDS.Tables["Box_Amt"].Rows[0].SetField("DB_AUser_Box", connection.SQLDS.Tables["Box_Amt1"].Rows[1]["CD_ABox_User"]);
            connection.SQLDS.Tables["Box_Amt"].Rows[0].SetField("DB_EUser_Box", connection.SQLDS.Tables["Box_Amt1"].Rows[1]["CD_EBox_User"]);
            connection.SQLDS.Tables["Box_Amt"].Rows[0].SetField("CD_AUser_Box", connection.SQLDS.Tables["Box_Amt1"].Rows[0]["CD_ABox_User"]);
            connection.SQLDS.Tables["Box_Amt"].Rows[0].SetField("CD_EUser_Box", connection.SQLDS.Tables["Box_Amt1"].Rows[0]["CD_EBox_User"]);

             Box_Amount_Rpt ObjRpt = new Box_Amount_Rpt();
            Box_Amount_Rpt_Eng ObjRptEng = new Box_Amount_Rpt_Eng();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", 26);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Main");

            DataRow Dr;
            int y = connection.SQLDS.Tables["Box_Amt"].Rows.Count;
            for ( i = 0; i < (5 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Box_Amt"].NewRow();
                connection.SQLDS.Tables["Box_Amt"].Rows.Add(Dr);
            }
            Branch_RptLang_MsgBox RptLangFrm;
            RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 26,true);
            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Box_Amt");
            connection.SQLDS.Tables.Remove("Box_Amt1");
        }

        private void Box_Amount_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    printToolStripButton_Click(sender, e);
                    break;
               
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Box_Amount_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}