﻿namespace Integration_Accounting_Sys
{
    partial class Exchange_other_currency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo__cur_in = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Cbo_cur_out = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Cbo_Catogreis = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtAcc_Name = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt_paid_acc = new System.Windows.Forms.TextBox();
            this.txt_rec_cust = new System.Windows.Forms.TextBox();
            this.txt_paid_cust = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Local_acc_txt = new System.Windows.Forms.TextBox();
            this.cust_local_txt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Grd_Rec_Acc = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Paid_Acc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_rec = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_paid_cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_loc_acc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_loc_cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.Discount_txt = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_loc_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Exchang_rat_txt = new System.Windows.Forms.Sample.DecimalTextBox();
            this.paid_Txt = new System.Windows.Forms.Sample.DecimalTextBox();
            this.recv_txt = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Real_txt = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rec_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Paid_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_rec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_paid_cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_loc_acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_loc_cust)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(6, 38);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(97, 14);
            this.label3.TabIndex = 744;
            this.label3.Text = "العملـة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(105, 34);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(178, 23);
            this.Txt_Loc_Cur.TabIndex = 740;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(105, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(178, 23);
            this.TxtUser.TabIndex = 738;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(97, 14);
            this.label1.TabIndex = 742;
            this.label1.Text = "المـستـخـــــــدم:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(419, 38);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(86, 14);
            this.label2.TabIndex = 746;
            this.label2.Text = "التاريـــــــــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(513, 34);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(166, 23);
            this.TxtIn_Rec_Date.TabIndex = 745;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(1, 60);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(703, 1);
            this.flowLayoutPanel4.TabIndex = 747;
            // 
            // cbo__cur_in
            // 
            this.cbo__cur_in.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo__cur_in.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo__cur_in.FormattingEnabled = true;
            this.cbo__cur_in.Location = new System.Drawing.Point(124, 72);
            this.cbo__cur_in.Name = "cbo__cur_in";
            this.cbo__cur_in.Size = new System.Drawing.Size(159, 24);
            this.cbo__cur_in.TabIndex = 748;
            this.cbo__cur_in.SelectedIndexChanged += new System.EventHandler(this.cbo__cur_in_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(9, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 14);
            this.label5.TabIndex = 749;
            this.label5.Text = "العملـة المقبوضة:";
            // 
            // Cbo_cur_out
            // 
            this.Cbo_cur_out.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_cur_out.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_cur_out.FormattingEnabled = true;
            this.Cbo_cur_out.Location = new System.Drawing.Point(514, 72);
            this.Cbo_cur_out.Name = "Cbo_cur_out";
            this.Cbo_cur_out.Size = new System.Drawing.Size(165, 24);
            this.Cbo_cur_out.TabIndex = 750;
            this.Cbo_cur_out.SelectedIndexChanged += new System.EventHandler(this.Cbo_cur_out_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(399, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 14);
            this.label6.TabIndex = 751;
            this.label6.Text = "العملـة المدفوعة:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(401, 105);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(105, 14);
            this.label7.TabIndex = 753;
            this.label7.Text = "ســـعر التعـــــادل:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(11, 102);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(106, 14);
            this.label8.TabIndex = 755;
            this.label8.Text = "السعر الفعلـــــي:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(9, 128);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(108, 14);
            this.label9.TabIndex = 759;
            this.label9.Text = "الكمية المقبوضـة:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(402, 131);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(104, 14);
            this.label10.TabIndex = 757;
            this.label10.Text = "الكمية المدفوعة:";
            // 
            // Cbo_Catogreis
            // 
            this.Cbo_Catogreis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Catogreis.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Catogreis.FormattingEnabled = true;
            this.Cbo_Catogreis.Location = new System.Drawing.Point(124, 155);
            this.Cbo_Catogreis.Name = "Cbo_Catogreis";
            this.Cbo_Catogreis.Size = new System.Drawing.Size(159, 24);
            this.Cbo_Catogreis.TabIndex = 760;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(8, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 14);
            this.label11.TabIndex = 761;
            this.label11.Text = "الجهــــــــــــــــــــــة:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(195, 193);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(218, 1);
            this.flowLayoutPanel1.TabIndex = 762;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(68, 184);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(153, 14);
            this.label12.TabIndex = 764;
            this.label12.Text = "الحســــاب القابـــــض.......";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(5, 195);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(61, 1);
            this.flowLayoutPanel2.TabIndex = 763;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(33, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 14);
            this.label13.TabIndex = 767;
            this.label13.Text = "الحســــاب:";
            // 
            // TxtAcc_Name
            // 
            this.TxtAcc_Name.BackColor = System.Drawing.Color.White;
            this.TxtAcc_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtAcc_Name.Location = new System.Drawing.Point(109, 209);
            this.TxtAcc_Name.Multiline = true;
            this.TxtAcc_Name.Name = "TxtAcc_Name";
            this.TxtAcc_Name.Size = new System.Drawing.Size(216, 22);
            this.TxtAcc_Name.TabIndex = 765;
            this.TxtAcc_Name.TextChanged += new System.EventHandler(this.TxtCur_Name_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(404, 213);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 14);
            this.label14.TabIndex = 770;
            this.label14.Text = "الحســــاب:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(413, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(155, 14);
            this.label15.TabIndex = 771;
            this.label15.Text = "الحســــاب الدافــــــــع.......";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(565, 192);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(136, 1);
            this.flowLayoutPanel3.TabIndex = 772;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(407, 335);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 14);
            this.label16.TabIndex = 775;
            this.label16.Text = "الثانـــــوي:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(36, 335);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 14);
            this.label17.TabIndex = 778;
            this.label17.Text = "الثانـــــوي:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(218, 451);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(488, 1);
            this.flowLayoutPanel5.TabIndex = 779;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.Maroon;
            this.label18.Location = new System.Drawing.Point(56, 441);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(160, 14);
            this.label18.TabIndex = 780;
            this.label18.Text = "حساب العملة المحلية.......";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(0, 449);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(56, 1);
            this.flowLayoutPanel6.TabIndex = 781;
            // 
            // txt_paid_acc
            // 
            this.txt_paid_acc.BackColor = System.Drawing.Color.White;
            this.txt_paid_acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txt_paid_acc.Location = new System.Drawing.Point(479, 209);
            this.txt_paid_acc.Multiline = true;
            this.txt_paid_acc.Name = "txt_paid_acc";
            this.txt_paid_acc.Size = new System.Drawing.Size(200, 22);
            this.txt_paid_acc.TabIndex = 782;
            this.txt_paid_acc.TextChanged += new System.EventHandler(this.txt_paid_acc_TextChanged);
            // 
            // txt_rec_cust
            // 
            this.txt_rec_cust.BackColor = System.Drawing.Color.White;
            this.txt_rec_cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txt_rec_cust.Location = new System.Drawing.Point(109, 331);
            this.txt_rec_cust.Multiline = true;
            this.txt_rec_cust.Name = "txt_rec_cust";
            this.txt_rec_cust.Size = new System.Drawing.Size(217, 22);
            this.txt_rec_cust.TabIndex = 783;
            this.txt_rec_cust.TextChanged += new System.EventHandler(this.txt_rec_acc_TextChanged);
            // 
            // txt_paid_cust
            // 
            this.txt_paid_cust.BackColor = System.Drawing.Color.White;
            this.txt_paid_cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txt_paid_cust.Location = new System.Drawing.Point(479, 331);
            this.txt_paid_cust.Multiline = true;
            this.txt_paid_cust.Name = "txt_paid_cust";
            this.txt_paid_cust.Size = new System.Drawing.Size(200, 22);
            this.txt_paid_cust.TabIndex = 784;
            this.txt_paid_cust.TextChanged += new System.EventHandler(this.txt_paid_cust_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(34, 468);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 14);
            this.label19.TabIndex = 787;
            this.label19.Text = "الحســــاب:";
            // 
            // Local_acc_txt
            // 
            this.Local_acc_txt.BackColor = System.Drawing.Color.White;
            this.Local_acc_txt.Enabled = false;
            this.Local_acc_txt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Local_acc_txt.Location = new System.Drawing.Point(110, 464);
            this.Local_acc_txt.Multiline = true;
            this.Local_acc_txt.Name = "Local_acc_txt";
            this.Local_acc_txt.Size = new System.Drawing.Size(216, 22);
            this.Local_acc_txt.TabIndex = 785;
            this.Local_acc_txt.TextChanged += new System.EventHandler(this.Local_acc_txt_TextChanged);
            // 
            // cust_local_txt
            // 
            this.cust_local_txt.BackColor = System.Drawing.Color.White;
            this.cust_local_txt.Enabled = false;
            this.cust_local_txt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.cust_local_txt.Location = new System.Drawing.Point(478, 464);
            this.cust_local_txt.Multiline = true;
            this.cust_local_txt.Name = "cust_local_txt";
            this.cust_local_txt.Size = new System.Drawing.Size(201, 22);
            this.cust_local_txt.TabIndex = 790;
            this.cust_local_txt.TextChanged += new System.EventHandler(this.cust_local_txt_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(407, 468);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(66, 14);
            this.label20.TabIndex = 789;
            this.label20.Text = "الثانـــــوي:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(14, 575);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(136, 14);
            this.label21.TabIndex = 792;
            this.label21.Text = "المبلغ بالعملة المحلية:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(440, 575);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(74, 14);
            this.label22.TabIndex = 793;
            this.label22.Text = "خصـــــــــــم:";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.White;
            this.textBox12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(160, 599);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(519, 23);
            this.textBox12.TabIndex = 795;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(41, 603);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(109, 14);
            this.label23.TabIndex = 796;
            this.label23.Text = "الملاحظـــــــــــــات:";
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(276, 634);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(84, 27);
            this.AddBtn.TabIndex = 797;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(364, 634);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(84, 27);
            this.ExtBtn.TabIndex = 798;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-10, 632);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(717, 1);
            this.flowLayoutPanel7.TabIndex = 799;
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(513, 6);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(166, 23);
            this.TxtBox_User.TabIndex = 800;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(419, 10);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(86, 14);
            this.label24.TabIndex = 801;
            this.label24.Text = "الصــــــــندوق:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(308, 105);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(0, 14);
            this.label4.TabIndex = 809;
            // 
            // Grd_Rec_Acc
            // 
            this.Grd_Rec_Acc.AllowUserToAddRows = false;
            this.Grd_Rec_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rec_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Rec_Acc.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Rec_Acc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Rec_Acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rec_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Rec_Acc.ColumnHeadersHeight = 45;
            this.Grd_Rec_Acc.ColumnHeadersVisible = false;
            this.Grd_Rec_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Rec_Acc.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Rec_Acc.Location = new System.Drawing.Point(44, 232);
            this.Grd_Rec_Acc.Name = "Grd_Rec_Acc";
            this.Grd_Rec_Acc.ReadOnly = true;
            this.Grd_Rec_Acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Rec_Acc.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rec_Acc.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Rec_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Rec_Acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Rec_Acc.Size = new System.Drawing.Size(281, 86);
            this.Grd_Rec_Acc.TabIndex = 810;
            this.Grd_Rec_Acc.SelectionChanged += new System.EventHandler(this.Grd_Rec_Acc_SelectionChanged);
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "ACC_Id";
            this.Column7.Frozen = true;
            this.Column7.HeaderText = "رمز الثانوي";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 60;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Acc_aName";
            this.Column8.HeaderText = "اسم الثانوي";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 160;
            // 
            // Grd_Paid_Acc
            // 
            this.Grd_Paid_Acc.AllowUserToAddRows = false;
            this.Grd_Paid_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Paid_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Paid_Acc.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Paid_Acc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Paid_Acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Paid_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Paid_Acc.ColumnHeadersHeight = 45;
            this.Grd_Paid_Acc.ColumnHeadersVisible = false;
            this.Grd_Paid_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Paid_Acc.DefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Paid_Acc.Location = new System.Drawing.Point(407, 232);
            this.Grd_Paid_Acc.Name = "Grd_Paid_Acc";
            this.Grd_Paid_Acc.ReadOnly = true;
            this.Grd_Paid_Acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Paid_Acc.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Paid_Acc.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Paid_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Paid_Acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Paid_Acc.Size = new System.Drawing.Size(272, 86);
            this.Grd_Paid_Acc.TabIndex = 811;
            this.Grd_Paid_Acc.SelectionChanged += new System.EventHandler(this.Grd_Paid_Acc_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "acc_id";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "acc_aname";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم الثانوي";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 160;
            // 
            // Grd_Cust_rec
            // 
            this.Grd_Cust_rec.AllowUserToAddRows = false;
            this.Grd_Cust_rec.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_rec.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust_rec.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust_rec.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust_rec.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_rec.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Cust_rec.ColumnHeadersHeight = 45;
            this.Grd_Cust_rec.ColumnHeadersVisible = false;
            this.Grd_Cust_rec.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust_rec.DefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Cust_rec.Location = new System.Drawing.Point(44, 354);
            this.Grd_Cust_rec.Name = "Grd_Cust_rec";
            this.Grd_Cust_rec.ReadOnly = true;
            this.Grd_Cust_rec.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust_rec.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_rec.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Cust_rec.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_rec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust_rec.Size = new System.Drawing.Size(281, 79);
            this.Grd_Cust_rec.TabIndex = 812;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "cust_id";
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 60;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn6.HeaderText = "اسم الثانوي";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 160;
            // 
            // Grd_paid_cust
            // 
            this.Grd_paid_cust.AllowUserToAddRows = false;
            this.Grd_paid_cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_paid_cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_paid_cust.BackgroundColor = System.Drawing.Color.White;
            this.Grd_paid_cust.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_paid_cust.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_paid_cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_paid_cust.ColumnHeadersHeight = 45;
            this.Grd_paid_cust.ColumnHeadersVisible = false;
            this.Grd_paid_cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_paid_cust.DefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_paid_cust.Location = new System.Drawing.Point(407, 354);
            this.Grd_paid_cust.Name = "Grd_paid_cust";
            this.Grd_paid_cust.ReadOnly = true;
            this.Grd_paid_cust.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_paid_cust.RowHeadersVisible = false;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_paid_cust.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_paid_cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_paid_cust.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_paid_cust.Size = new System.Drawing.Size(272, 81);
            this.Grd_paid_cust.TabIndex = 813;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "cust_id";
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn4.HeaderText = "اسم الثانوي";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 160;
            // 
            // Grd_loc_acc
            // 
            this.Grd_loc_acc.AllowUserToAddRows = false;
            this.Grd_loc_acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_loc_acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_loc_acc.BackgroundColor = System.Drawing.Color.White;
            this.Grd_loc_acc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_loc_acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_loc_acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_loc_acc.ColumnHeadersHeight = 45;
            this.Grd_loc_acc.ColumnHeadersVisible = false;
            this.Grd_loc_acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_loc_acc.DefaultCellStyle = dataGridViewCellStyle19;
            this.Grd_loc_acc.Location = new System.Drawing.Point(45, 488);
            this.Grd_loc_acc.Name = "Grd_loc_acc";
            this.Grd_loc_acc.ReadOnly = true;
            this.Grd_loc_acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_loc_acc.RowHeadersVisible = false;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_loc_acc.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_loc_acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_loc_acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_loc_acc.Size = new System.Drawing.Size(281, 76);
            this.Grd_loc_acc.TabIndex = 814;
            this.Grd_loc_acc.SelectionChanged += new System.EventHandler(this.Grd_loc_acc_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "acc_id";
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 60;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "acc_aname";
            this.dataGridViewTextBoxColumn8.HeaderText = "اسم الثانوي";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 160;
            // 
            // Grd_loc_cust
            // 
            this.Grd_loc_cust.AllowUserToAddRows = false;
            this.Grd_loc_cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_loc_cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_loc_cust.BackgroundColor = System.Drawing.Color.White;
            this.Grd_loc_cust.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_loc_cust.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_loc_cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_loc_cust.ColumnHeadersHeight = 45;
            this.Grd_loc_cust.ColumnHeadersVisible = false;
            this.Grd_loc_cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_loc_cust.DefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_loc_cust.Location = new System.Drawing.Point(406, 487);
            this.Grd_loc_cust.Name = "Grd_loc_cust";
            this.Grd_loc_cust.ReadOnly = true;
            this.Grd_loc_cust.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_loc_cust.RowHeadersVisible = false;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_loc_cust.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.Grd_loc_cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_loc_cust.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_loc_cust.Size = new System.Drawing.Size(273, 77);
            this.Grd_loc_cust.TabIndex = 815;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "cust_id";
            this.dataGridViewTextBoxColumn9.Frozen = true;
            this.dataGridViewTextBoxColumn9.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 60;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn10.HeaderText = "اسم الثانوي";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 160;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Location = new System.Drawing.Point(321, 198);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(440, 1);
            this.flowLayoutPanel8.TabIndex = 820;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(367, 193);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1, 258);
            this.flowLayoutPanel9.TabIndex = 821;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(286, 105);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(0, 14);
            this.label25.TabIndex = 822;
            // 
            // Discount_txt
            // 
            this.Discount_txt.BackColor = System.Drawing.Color.White;
            this.Discount_txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Discount_txt.Location = new System.Drawing.Point(558, 571);
            this.Discount_txt.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Discount_txt.Name = "Discount_txt";
            this.Discount_txt.NumberDecimalDigits = 3;
            this.Discount_txt.NumberDecimalSeparator = ".";
            this.Discount_txt.NumberGroupSeparator = ",";
            this.Discount_txt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Discount_txt.Size = new System.Drawing.Size(121, 23);
            this.Discount_txt.TabIndex = 819;
            this.Discount_txt.Text = "0.000";
            // 
            // txt_loc_amount
            // 
            this.txt_loc_amount.BackColor = System.Drawing.Color.White;
            this.txt_loc_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_loc_amount.Location = new System.Drawing.Point(160, 571);
            this.txt_loc_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_loc_amount.Name = "txt_loc_amount";
            this.txt_loc_amount.NumberDecimalDigits = 3;
            this.txt_loc_amount.NumberDecimalSeparator = ".";
            this.txt_loc_amount.NumberGroupSeparator = ",";
            this.txt_loc_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_loc_amount.Size = new System.Drawing.Size(165, 23);
            this.txt_loc_amount.TabIndex = 818;
            this.txt_loc_amount.Text = "0.000";
            // 
            // Exchang_rat_txt
            // 
            this.Exchang_rat_txt.BackColor = System.Drawing.Color.White;
            this.Exchang_rat_txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exchang_rat_txt.Location = new System.Drawing.Point(513, 101);
            this.Exchang_rat_txt.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Exchang_rat_txt.Name = "Exchang_rat_txt";
            this.Exchang_rat_txt.NumberDecimalDigits = 7;
            this.Exchang_rat_txt.NumberDecimalSeparator = ".";
            this.Exchang_rat_txt.NumberGroupSeparator = ",";
            this.Exchang_rat_txt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Exchang_rat_txt.Size = new System.Drawing.Size(166, 23);
            this.Exchang_rat_txt.TabIndex = 816;
            this.Exchang_rat_txt.Text = "0.0000000";
            // 
            // paid_Txt
            // 
            this.paid_Txt.BackColor = System.Drawing.Color.White;
            this.paid_Txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paid_Txt.Location = new System.Drawing.Point(513, 127);
            this.paid_Txt.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.paid_Txt.Name = "paid_Txt";
            this.paid_Txt.NumberDecimalDigits = 3;
            this.paid_Txt.NumberDecimalSeparator = ".";
            this.paid_Txt.NumberGroupSeparator = ",";
            this.paid_Txt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.paid_Txt.Size = new System.Drawing.Size(166, 23);
            this.paid_Txt.TabIndex = 808;
            this.paid_Txt.Text = "0.000";
            this.paid_Txt.TextChanged += new System.EventHandler(this.paid_Txt_TextChanged);
            // 
            // recv_txt
            // 
            this.recv_txt.BackColor = System.Drawing.Color.White;
            this.recv_txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recv_txt.Location = new System.Drawing.Point(124, 127);
            this.recv_txt.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.recv_txt.Name = "recv_txt";
            this.recv_txt.NumberDecimalDigits = 3;
            this.recv_txt.NumberDecimalSeparator = ".";
            this.recv_txt.NumberGroupSeparator = ",";
            this.recv_txt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.recv_txt.Size = new System.Drawing.Size(159, 23);
            this.recv_txt.TabIndex = 807;
            this.recv_txt.Text = "0.000";
            this.recv_txt.TextChanged += new System.EventHandler(this.recv_txt_TextChanged);
            // 
            // Real_txt
            // 
            this.Real_txt.BackColor = System.Drawing.Color.White;
            this.Real_txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Real_txt.Location = new System.Drawing.Point(124, 101);
            this.Real_txt.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Real_txt.Name = "Real_txt";
            this.Real_txt.NumberDecimalDigits = 7;
            this.Real_txt.NumberDecimalSeparator = ".";
            this.Real_txt.NumberGroupSeparator = ",";
            this.Real_txt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Real_txt.Size = new System.Drawing.Size(159, 23);
            this.Real_txt.TabIndex = 806;
            this.Real_txt.Text = "0.0000000";
            this.Real_txt.TextChanged += new System.EventHandler(this.Real_txt_TextChanged);
            // 
            // Exchange_other_currency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 666);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Discount_txt);
            this.Controls.Add(this.txt_loc_amount);
            this.Controls.Add(this.Exchang_rat_txt);
            this.Controls.Add(this.Grd_loc_cust);
            this.Controls.Add(this.Grd_loc_acc);
            this.Controls.Add(this.Grd_paid_cust);
            this.Controls.Add(this.Grd_Cust_rec);
            this.Controls.Add(this.Grd_Paid_Acc);
            this.Controls.Add(this.Grd_Rec_Acc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.paid_Txt);
            this.Controls.Add(this.recv_txt);
            this.Controls.Add(this.Real_txt);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.cust_local_txt);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Local_acc_txt);
            this.Controls.Add(this.txt_paid_cust);
            this.Controls.Add(this.txt_rec_cust);
            this.Controls.Add(this.txt_paid_acc);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TxtAcc_Name);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Cbo_Catogreis);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Cbo_cur_out);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbo__cur_in);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Exchange_other_currency";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exchange_other_currency";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Exchange_other_currency_FormClosed);
            this.Load += new System.EventHandler(this.Exchange_other_currency_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rec_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Paid_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_rec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_paid_cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_loc_acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_loc_cust)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.ComboBox cbo__cur_in;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox Cbo_cur_out;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Cbo_Catogreis;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtAcc_Name;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.TextBox txt_paid_acc;
        private System.Windows.Forms.TextBox txt_rec_cust;
        private System.Windows.Forms.TextBox txt_paid_cust;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Local_acc_txt;
        private System.Windows.Forms.TextBox cust_local_txt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Sample.DecimalTextBox Real_txt;
        private System.Windows.Forms.Sample.DecimalTextBox recv_txt;
        private System.Windows.Forms.Sample.DecimalTextBox paid_Txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView Grd_Rec_Acc;
        private System.Windows.Forms.DataGridView Grd_Paid_Acc;
        private System.Windows.Forms.DataGridView Grd_Cust_rec;
        private System.Windows.Forms.DataGridView Grd_paid_cust;
        private System.Windows.Forms.DataGridView Grd_loc_acc;
        private System.Windows.Forms.DataGridView Grd_loc_cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Sample.DecimalTextBox Exchang_rat_txt;
        private System.Windows.Forms.Sample.DecimalTextBox txt_loc_amount;
        private System.Windows.Forms.Sample.DecimalTextBox Discount_txt;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label25;
    }
}