﻿namespace Integration_Accounting_Sys
{
    partial class Terminals_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Terminals_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.UpdBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.SearchBtn = new System.Windows.Forms.ToolStripButton();
            this.Txt_AName = new System.Windows.Forms.ToolStripTextBox();
            this.Grd_Companies = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_AAdress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtFmd_Name = new System.Windows.Forms.TextBox();
            this.TxtCit_Name = new System.Windows.Forms.TextBox();
            this.TxtCon_Name = new System.Windows.Forms.TextBox();
            this.TxtTerm_State = new System.Windows.Forms.TextBox();
            this.TxtCur_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_tax_no = new System.Windows.Forms.TextBox();
            this.myDateTextBox1 = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtDiscount_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_phone = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtDoc_Da = new Integration_Accounting_Sys.MyDateTextBox();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Companies)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 500);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(844, 22);
            this.statusStrip1.TabIndex = 468;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator2,
            this.UpdBtn,
            this.toolStripSeparator4,
            this.AllBtn,
            this.toolStripSeparator1,
            this.SearchBtn,
            this.Txt_AName});
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(844, 25);
            this.BND.TabIndex = 0;
            this.BND.Text = "bindingNavigator1";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(82, 22);
            this.BtnAdd.Text = "اضافة جديد";
            this.BtnAdd.ToolTipText = "F9";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // UpdBtn
            // 
            this.UpdBtn.Image = ((System.Drawing.Image)(resources.GetObject("UpdBtn.Image")));
            this.UpdBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdBtn.Name = "UpdBtn";
            this.UpdBtn.Size = new System.Drawing.Size(54, 22);
            this.UpdBtn.Text = "تعديل";
            this.UpdBtn.ToolTipText = "F7";
            this.UpdBtn.Click += new System.EventHandler(this.UpdBtn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // AllBtn
            // 
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(80, 22);
            this.AllBtn.Text = "عرض الكل";
            this.AllBtn.ToolTipText = "F4";
            this.AllBtn.Click += new System.EventHandler(this.AllBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(121, 22);
            this.SearchBtn.Text = "بـحـــــث الاسم/رقم :";
            this.SearchBtn.ToolTipText = "F5";
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // Txt_AName
            // 
            this.Txt_AName.Name = "Txt_AName";
            this.Txt_AName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_AName.Size = new System.Drawing.Size(450, 25);
            // 
            // Grd_Companies
            // 
            this.Grd_Companies.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Companies.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Companies.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Companies.ColumnHeadersHeight = 40;
            this.Grd_Companies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column16,
            this.Column2,
            this.Column4,
            this.Column5,
            this.Column3,
            this.Column19});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.NullValue = null;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Companies.DefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Companies.Location = new System.Drawing.Point(6, 66);
            this.Grd_Companies.Name = "Grd_Companies";
            this.Grd_Companies.ReadOnly = true;
            this.Grd_Companies.RowHeadersWidth = 18;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Companies.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Companies.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Companies.Size = new System.Drawing.Size(826, 193);
            this.Grd_Companies.TabIndex = 3;
            this.Grd_Companies.SelectionChanged += new System.EventHandler(this.Grd_Companies_SelectionChanged);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-6, 58);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(989, 1);
            this.flowLayoutPanel7.TabIndex = 497;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(100, 28);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(275, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(669, 28);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(163, 23);
            this.TxtIn_Rec_Date.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(615, 31);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 493;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(9, 31);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 492;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(8, 424);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(91, 16);
            this.label19.TabIndex = 729;
            this.label19.Text = "معلومات الوثيقة...";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 436);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(978, 1);
            this.flowLayoutPanel1.TabIndex = 730;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(603, 448);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(78, 18);
            this.label9.TabIndex = 724;
            this.label9.Text = "رقــم الوثـيـقـة :";
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.BackColor = System.Drawing.Color.White;
            this.TxtNo_Doc.Enabled = false;
            this.TxtNo_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(687, 446);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(148, 23);
            this.TxtNo_Doc.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(4, 448);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(82, 18);
            this.label10.TabIndex = 725;
            this.label10.Text = "نـــوع الوثـيـقـة :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(288, 475);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(82, 18);
            this.label13.TabIndex = 726;
            this.label13.Text = "تاريخ الاصدار :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(4, 475);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(85, 18);
            this.label15.TabIndex = 727;
            this.label15.Text = "جـهـة الاصـدار :";
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.BackColor = System.Drawing.Color.White;
            this.TxtIss_Doc.Enabled = false;
            this.TxtIss_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(95, 473);
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(187, 23);
            this.TxtIss_Doc.TabIndex = 15;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Maroon;
            this.label29.Location = new System.Drawing.Point(5, 310);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(98, 16);
            this.label29.TabIndex = 765;
            this.label29.Text = " المعلومات العامة...";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 323);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(978, 1);
            this.flowLayoutPanel4.TabIndex = 766;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(7, 363);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(85, 18);
            this.label5.TabIndex = 760;
            this.label5.Text = "العنـــــــــــــوان :";
            // 
            // Txt_AAdress
            // 
            this.Txt_AAdress.BackColor = System.Drawing.Color.White;
            this.Txt_AAdress.Enabled = false;
            this.Txt_AAdress.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AAdress.Location = new System.Drawing.Point(92, 361);
            this.Txt_AAdress.Name = "Txt_AAdress";
            this.Txt_AAdress.Size = new System.Drawing.Size(746, 23);
            this.Txt_AAdress.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(579, 334);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(83, 18);
            this.label7.TabIndex = 762;
            this.label7.Text = "الــهــــــاتـــــف :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(7, 394);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 18);
            this.label18.TabIndex = 764;
            this.label18.Text = "البريد الالكتروني:";
            // 
            // TxtEmail
            // 
            this.TxtEmail.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Enabled = false;
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(92, 394);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(746, 23);
            this.TxtEmail.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(7, 334);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 18);
            this.label11.TabIndex = 757;
            this.label11.Text = "البــلـــــــــــــــد:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(314, 334);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 18);
            this.label12.TabIndex = 758;
            this.label12.Text = "المـديـــنــة:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(8, 262);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(96, 16);
            this.label4.TabIndex = 797;
            this.label4.Text = "المعلومات المالية...";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(463, 284);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 18);
            this.label23.TabIndex = 792;
            this.label23.Text = "حالـة الفــرع:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(246, 285);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 16);
            this.label37.TabIndex = 791;
            this.label37.Text = "اعلى قيمة خصـم:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(4, 284);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 18);
            this.label21.TabIndex = 789;
            this.label21.Text = "العملــــــة المحليــــــة:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(1, 273);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(978, 1);
            this.flowLayoutPanel5.TabIndex = 788;
            // 
            // TxtFmd_Name
            // 
            this.TxtFmd_Name.BackColor = System.Drawing.Color.White;
            this.TxtFmd_Name.Enabled = false;
            this.TxtFmd_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFmd_Name.Location = new System.Drawing.Point(95, 446);
            this.TxtFmd_Name.Name = "TxtFmd_Name";
            this.TxtFmd_Name.Size = new System.Drawing.Size(502, 23);
            this.TxtFmd_Name.TabIndex = 13;
            // 
            // TxtCit_Name
            // 
            this.TxtCit_Name.BackColor = System.Drawing.Color.White;
            this.TxtCit_Name.Enabled = false;
            this.TxtCit_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCit_Name.Location = new System.Drawing.Point(373, 332);
            this.TxtCit_Name.Name = "TxtCit_Name";
            this.TxtCit_Name.Size = new System.Drawing.Size(188, 23);
            this.TxtCit_Name.TabIndex = 9;
            // 
            // TxtCon_Name
            // 
            this.TxtCon_Name.BackColor = System.Drawing.Color.White;
            this.TxtCon_Name.Enabled = false;
            this.TxtCon_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCon_Name.Location = new System.Drawing.Point(89, 332);
            this.TxtCon_Name.Name = "TxtCon_Name";
            this.TxtCon_Name.Size = new System.Drawing.Size(197, 23);
            this.TxtCon_Name.TabIndex = 8;
            // 
            // TxtTerm_State
            // 
            this.TxtTerm_State.BackColor = System.Drawing.Color.White;
            this.TxtTerm_State.Enabled = false;
            this.TxtTerm_State.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_State.Location = new System.Drawing.Point(533, 282);
            this.TxtTerm_State.Name = "TxtTerm_State";
            this.TxtTerm_State.Size = new System.Drawing.Size(117, 23);
            this.TxtTerm_State.TabIndex = 6;
            // 
            // TxtCur_Name
            // 
            this.TxtCur_Name.BackColor = System.Drawing.Color.White;
            this.TxtCur_Name.Enabled = false;
            this.TxtCur_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCur_Name.Location = new System.Drawing.Point(110, 282);
            this.TxtCur_Name.Name = "TxtCur_Name";
            this.TxtCur_Name.Size = new System.Drawing.Size(129, 23);
            this.TxtCur_Name.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(603, 475);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(77, 18);
            this.label1.TabIndex = 806;
            this.label1.Text = "تاريخ النفــــاذ :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(650, 284);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 18);
            this.label6.TabIndex = 807;
            this.label6.Text = "الرقم الضــريبي:";
            // 
            // Txt_tax_no
            // 
            this.Txt_tax_no.BackColor = System.Drawing.Color.White;
            this.Txt_tax_no.Enabled = false;
            this.Txt_tax_no.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_tax_no.Location = new System.Drawing.Point(732, 282);
            this.Txt_tax_no.Name = "Txt_tax_no";
            this.Txt_tax_no.Size = new System.Drawing.Size(103, 23);
            this.Txt_tax_no.TabIndex = 7;
            // 
            // myDateTextBox1
            // 
            this.myDateTextBox1.BackColor = System.Drawing.Color.White;
            this.myDateTextBox1.DateSeperator = '/';
            this.myDateTextBox1.Enabled = false;
            this.myDateTextBox1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myDateTextBox1.Location = new System.Drawing.Point(686, 473);
            this.myDateTextBox1.Mask = "0000/00/00";
            this.myDateTextBox1.Name = "myDateTextBox1";
            this.myDateTextBox1.PromptChar = ' ';
            this.myDateTextBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.myDateTextBox1.Size = new System.Drawing.Size(150, 23);
            this.myDateTextBox1.TabIndex = 17;
            this.myDateTextBox1.Text = "00000000";
            this.myDateTextBox1.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtDiscount_Amount
            // 
            this.TxtDiscount_Amount.BackColor = System.Drawing.Color.White;
            this.TxtDiscount_Amount.Enabled = false;
            this.TxtDiscount_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscount_Amount.Location = new System.Drawing.Point(333, 282);
            this.TxtDiscount_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtDiscount_Amount.Name = "TxtDiscount_Amount";
            this.TxtDiscount_Amount.NumberDecimalDigits = 3;
            this.TxtDiscount_Amount.NumberDecimalSeparator = ".";
            this.TxtDiscount_Amount.NumberGroupSeparator = ",";
            this.TxtDiscount_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDiscount_Amount.Size = new System.Drawing.Size(129, 23);
            this.TxtDiscount_Amount.TabIndex = 5;
            this.TxtDiscount_Amount.Text = "0.000";
            // 
            // Txt_phone
            // 
            this.Txt_phone.AllowSpace = false;
            this.Txt_phone.BackColor = System.Drawing.Color.White;
            this.Txt_phone.Enabled = false;
            this.Txt_phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_phone.Location = new System.Drawing.Point(670, 332);
            this.Txt_phone.Name = "Txt_phone";
            this.Txt_phone.Size = new System.Drawing.Size(167, 23);
            this.Txt_phone.TabIndex = 10;
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.BackColor = System.Drawing.Color.White;
            this.TxtDoc_Da.DateSeperator = '/';
            this.TxtDoc_Da.Enabled = false;
            this.TxtDoc_Da.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDoc_Da.Location = new System.Drawing.Point(376, 473);
            this.TxtDoc_Da.Mask = "0000/00/00";
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.PromptChar = ' ';
            this.TxtDoc_Da.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtDoc_Da.Size = new System.Drawing.Size(221, 23);
            this.TxtDoc_Da.TabIndex = 16;
            this.TxtDoc_Da.Text = "00000000";
            this.TxtDoc_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "T_ID";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "الرقـــم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "ACust_Name";
            this.Column16.Frozen = true;
            this.Column16.HeaderText = "الاســـم ";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column16.Width = 343;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "T_Symbol";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "الرمــز";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 80;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Center_Flag";
            this.Column4.HeaderText = "الادارة المالية";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column4.Width = 70;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "comm_rem_flag";
            this.Column5.HeaderText = "يعتمد نسب العمولات";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "StartDate";
            this.Column3.HeaderText = "تاريخ تطبيق النظام";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 110;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "User_Name";
            dataGridViewCellStyle3.NullValue = null;
            this.Column19.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column19.HeaderText = "المنظــم";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 152;
            // 
            // Terminals_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 522);
            this.Controls.Add(this.Txt_tax_no);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.myDateTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtCur_Name);
            this.Controls.Add(this.TxtTerm_State);
            this.Controls.Add(this.TxtCon_Name);
            this.Controls.Add(this.TxtCit_Name);
            this.Controls.Add(this.TxtFmd_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.TxtDiscount_Amount);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Txt_phone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_AAdress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Grd_Companies);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Terminals_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "152";
            this.Text = "Terminals_Main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Terminals_Main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Terminals_Main_FormClosed);
            this.Load += new System.EventHandler(this.Terminals_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Terminals_Main_KeyDown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Companies)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton UpdBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton SearchBtn;
        private System.Windows.Forms.ToolStripTextBox Txt_AName;
        private System.Windows.Forms.DataGridView Grd_Companies;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MyDateTextBox TxtDoc_Da;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private Integration_Accounting_Sys.NumericTextBox Txt_phone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txt_AAdress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDiscount_Amount;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.TextBox TxtFmd_Name;
        private System.Windows.Forms.TextBox TxtCit_Name;
        private System.Windows.Forms.TextBox TxtCon_Name;
        private System.Windows.Forms.TextBox TxtTerm_State;
        private System.Windows.Forms.TextBox TxtCur_Name;
        private MyDateTextBox myDateTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_tax_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
    }
}