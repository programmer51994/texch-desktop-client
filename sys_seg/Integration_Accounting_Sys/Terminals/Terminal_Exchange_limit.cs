﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Terminal_Exchange_limit : Form
    {
        #region MyRegion
        string SqlTxt = "";
        bool CboChange = false;
        bool GrdChange = false;
        BindingSource _Bs_Exchange = new BindingSource();
        DataTable DT = new DataTable();
        BindingSource BS_Grd = new BindingSource();
        DataTable TBL = new DataTable();
        #endregion

        public Terminal_Exchange_limit()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_limit.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                //    Column1.DataPropertyName = "Chk";
                Column2.DataPropertyName = "ECust_name";
                //    Column3.DataPropertyName = "Acc_Ename";
                //    Column4.DataPropertyName = "ECust_name";
            }
        }
        //-------------------------------------------------------------
        private void Terminal_Exchange_limit_Load(object sender, EventArgs e)
        {
            SqlTxt = "Select T_ID,A.CUST_ID, acust_name , Ecust_name ,  A.LOC_CUR_ID , Cur_ANAME, Cur_ENAME ,Limit_Exchange_LC "
                  + " from TERMINALS A ,Cur_Tbl B , customers C "
                  + "  where A.LOC_CUR_ID = B.Cur_ID "
                  + " and A.cust_id = C.cust_id "
                  + "   And C.Cust_Flag <> 0 ";

            BS_Grd.DataSource = connection.SqlExec(SqlTxt, "limit_Tbl");
            Grd_limit.DataSource = BS_Grd;
        }
        //-------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            DT = connection.SQLDS.Tables["limit_Tbl"].DefaultView.ToTable(false, "T_ID", "CUST_ID", "Limit_Exchange_LC").Select().CopyToDataTable();
            connection.SQLCMD.Parameters.AddWithValue("@tbl_Limit_exch_lc", DT);
            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Term_Limit_exchage_lc", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);

            connection.SQLCMD.Parameters.Clear();

        }
        //-------------------------------------------------------------
        private void Grd_limit_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(BS_Grd);
        }
        //-------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Terminal_Exchange_limit_FormClosed(object sender, FormClosedEventArgs e)
        {
            CboChange = false;
            GrdChange = false;
        }
        //-------------------------------------------------------------
    }
}