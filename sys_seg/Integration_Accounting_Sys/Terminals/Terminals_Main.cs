﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Terminals_Main : Form
    {
        string s = "";
        bool GrdChange = false;
        public  static BindingSource _Bs_Term = new BindingSource();
        public Terminals_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Companies.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Companies.Columns["column16"].DataPropertyName = "ECust_Name";
           
            }
            

        }

        private void Terminals_Main_Load(object sender, EventArgs e)
        {
            GrdChange = false;
            _Bs_Term.DataSource = connection.SqlExec("Exec Main_Terminals '" + Txt_AName.Text.Trim() + "'", "Term_Tbl");
            if (connection.Lang_id == 1)
            {
                Grd_Companies.Columns["Column5"].DataPropertyName = "Acomm_rem_flag";
            }
            if (connection.Lang_id == 2)
            {
                Column16.DataPropertyName = "ECust_Name";
                Column5.DataPropertyName = "Ecomm_rem_flag";

            }

            

            TxtCur_Name.DataBindings.Clear();
            TxtTerm_State.DataBindings.Clear();
            TxtDiscount_Amount.DataBindings.Clear();
            TxtCon_Name.DataBindings.Clear();
            TxtCit_Name.DataBindings.Clear();
            Txt_AAdress.DataBindings.Clear();
            Txt_phone.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            TxtFmd_Name.DataBindings.Clear();
            TxtNo_Doc.DataBindings.Clear();
            TxtIss_Doc.DataBindings.Clear();
            TxtDoc_Da.DataBindings.Clear();
            myDateTextBox1.DataBindings.Clear();
            Txt_tax_no.DataBindings.Clear();
            TxtCur_Name.DataBindings.Add("Text", _Bs_Term, connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename");
            TxtTerm_State.DataBindings.Add("Text", _Bs_Term, connection.Lang_id == 1 ? "TERM_STATE_Aname" : "TERM_STATE_Ename");
            TxtDiscount_Amount.DataBindings.Add("Text", _Bs_Term, "discount_amnt");
            TxtCon_Name.DataBindings.Add("Text", _Bs_Term, connection.Lang_id == 1 ? "Con_Aname" : "Con_Ename");
            TxtCit_Name.DataBindings.Add("Text", _Bs_Term, connection.Lang_id == 1 ? "Cit_Aname" : "Cit_Ename");
            Txt_AAdress.DataBindings.Add("Text", _Bs_Term, connection.Lang_id == 1 ? "A_address" : "E_address");
            Txt_phone.DataBindings.Add("Text", _Bs_Term, "PHONE");
            TxtEmail.DataBindings.Add("Text", _Bs_Term, "EMAIL");
            TxtFmd_Name.DataBindings.Add("Text", _Bs_Term, connection.Lang_id == 1 ? "Fmd_Aname" : "Fmd_Ename");
            TxtNo_Doc.DataBindings.Add("Text", _Bs_Term, "FRM_DOC_NO");
            TxtIss_Doc.DataBindings.Add("Text", _Bs_Term, "frm_doc_Is");
            TxtDoc_Da.DataBindings.Add("Text", _Bs_Term, "frm_doc_Da");
            myDateTextBox1.DataBindings.Add("Text", _Bs_Term, "doc_eda");
            Txt_tax_no.DataBindings.Add("Text", _Bs_Term, "TAX_NO");


            if (connection.SQLDS.Tables["Term_Tbl"].Rows.Count > 0)
            {
                UpdBtn.Enabled = true;
                Grd_Companies.DataSource = _Bs_Term;
                GrdChange = true;
                Grd_Companies_SelectionChanged(sender, e);
            }
            else
            {
                UpdBtn.Enabled = false;
            }
        }

        private void Grd_Companies_SelectionChanged(object sender, EventArgs e)
        {
            string news = "";

            if (GrdChange)
            {
                LblRec.Text = connection.Records();
               
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Terminals_Add_Upd EmpFrm = new Terminals_Add_Upd(1);
            this.Visible = false;
            EmpFrm.ShowDialog(this);
            Terminals_Main_Load(sender, e);
            this.Visible = true;
        }

        private void UpdBtn_Click(object sender, EventArgs e)
        {

            Terminals_Add_Upd EmpFrm = new Terminals_Add_Upd(2);
            this.Visible = false;
            EmpFrm.ShowDialog(this);
            Terminals_Main_Load(sender, e);
            this.Visible = true;
        }

        private void AllBtn_Click(object sender, EventArgs e)
        {
            Txt_AName.Text = "";
            Terminals_Main_Load(sender, e);
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Terminals_Main_Load(sender, e);
        }
        private void Terminals_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Terminals_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
           
            string[] Str = { "Term_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void Terminals_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}