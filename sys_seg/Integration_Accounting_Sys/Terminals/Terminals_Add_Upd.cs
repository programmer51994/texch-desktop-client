﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Terminals_Add_Upd : Form
    {
        #region Declareations
        string Sql_Text = "";
        bool ConChange = false;
        int Frm_Id = 0;
        int T_ID = 0;
        int cust_id = 0;
        string End_year = "";
        string Ord_Strt ="";
        #endregion
       
        public Terminals_Add_Upd(int Form_Id )
        {
            InitializeComponent(); 
           
            Frm_Id = Form_Id;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            
        }
        //------------------------------------------------------------------------------
        private void GetData()
        {
         ConChange = false;
            Sql_Text = " Exec all_master ";
            connection.SqlExec(Sql_Text, "all_master");
            CboCit_Id.DataSource = connection.SQLDS.Tables["all_master6"];
            CboCit_Id.ValueMember = "Cit_id";
            CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "Cit_aname" : "Cit_Ename";

            CboDoc_id.DataSource = connection.SQLDS.Tables["all_master2"];
            CboDoc_id.ValueMember = "Fmd_id";
            CboDoc_id.DisplayMember = connection.Lang_id == 1 ? "Fmd_aname" : "Fmd_Ename";

            CboBaseCur_Id.DataSource = connection.SQLDS.Tables["all_master5"];
            CboBaseCur_Id.ValueMember = "cur_id";
            CboBaseCur_Id.DisplayMember = connection.Lang_id == 1 ? "cur_aname" : "cur_Ename";
            ConChange = true;
            CboCoun_Id_SelectedIndexChanged(null , null );
        
        }
        //------------------------------------------------------------------------------
        private void Terminals_Add_Upd_Load(object sender, EventArgs e)
        {
           
         

            GetData(); 
            CboTerm_State.SelectedIndex = 0;
            Cbo_loc_int_case.SelectedIndex = 0;
            if (connection.Lang_id != 1)
            {

                Cbo_loc_int_case.Items[0] = "All Cases";
                Cbo_loc_int_case.Items[1] = "IN Only";
                Cbo_loc_int_case.Items[2] = "Out Only";
                CboTerm_State.Items.Clear();
                CboTerm_State.Items.Add("Active");
                CboTerm_State.Items.Add("Stoped");
                CboTerm_State.SelectedIndex = 0;
            }

            if (Frm_Id == 2)
            {
                ConChange = true;
                Edit_Record();
            } 


        }
        //------------------------------------------------------------------------------
        private void CboCoun_Id_SelectedIndexChanged(object sender, EventArgs e)
        {

            //if (ConChange )
            //{
            //    CboCit_Id.DataSource = connection.SQLDS.Tables["all_master4"].DefaultView.ToTable().Select("Con_Id = " + CboCoun_Id.SelectedValue).CopyToDataTable();
            //    CboCit_Id.ValueMember = "cit_id";
            //    CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "cit_aname" : "cit_Ename";
            //}
        }
        //------------------------------------------------------------------------------
        private void Edit_Record()
        {
            this.Text = connection.Lang_id == 1 ? " تعديـــل الفــرع " : "Update Terminals";
            DataRowView Drv =Terminals_Main._Bs_Term.Current as DataRowView;
            DataRow Dr = Drv.Row;
            T_ID = Dr.Field<Int16>("T_Id");
            cust_id = Dr.Field<Int32>("Cust_Id");
          
            

            Txt_Aname.DataBindings.Clear();
            Txt_Ename.DataBindings.Clear();
            CboCit_Id.DataBindings.Clear();
            Txt_AAdress.DataBindings.Clear();
            Txt_EAdress.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            Txt_phone.DataBindings.Clear();
            CboDoc_id.DataBindings.Clear();
            TxtNo_Doc.DataBindings.Clear();
            TxtIss_Doc.DataBindings.Clear();
            TxtDoc_Da.DataBindings.Clear();
            TxtDoc_EDa.DataBindings.Clear();
            TxtT_Symbol.DataBindings.Clear();
            ChkCenter_Flag.DataBindings.Clear();
            TxtDiscount_Amount.DataBindings.Clear();
            Txt_period_pass.DataBindings.Clear();
            CboTerm_State.DataBindings.Clear();
            Cbo_loc_int_case.DataBindings.Clear();
            CboBaseCur_Id.DataBindings.Clear();
            TxtPwd.DataBindings.Clear();
            TxtTax_No.DataBindings.Clear();
            CHK_Rem_c.DataBindings.Clear();
            CHK_comm.DataBindings.Clear();
            chk_daily_opening.DataBindings.Clear();


          //====================================================================


            Txt_Aname.DataBindings.Add("Text", Terminals_Main._Bs_Term, "ACust_Name");
            Txt_Ename.DataBindings.Add("Text", Terminals_Main._Bs_Term, "ECust_Name");
            TxtT_Symbol.DataBindings.Add("Text", Terminals_Main._Bs_Term, "T_SYMBOL");
            ChkCenter_Flag.DataBindings.Add("Checked", Terminals_Main._Bs_Term, "Center_Flag");
            TxtTax_No.DataBindings.Add("Text", Terminals_Main._Bs_Term, "TAX_NO");
            Txt_AAdress.DataBindings.Add("Text", Terminals_Main._Bs_Term, "A_address");
            Txt_EAdress.DataBindings.Add("Text", Terminals_Main._Bs_Term, "E_address");
            TxtEmail.DataBindings.Add("Text", Terminals_Main._Bs_Term, "Email");
            Txt_phone.DataBindings.Add("Text", Terminals_Main._Bs_Term, "Phone");
            TxtDiscount_Amount.DataBindings.Add("Text", Terminals_Main._Bs_Term, "DISCOUNT_AMNT");
            Txt_period_pass.DataBindings.Add("Text", Terminals_Main._Bs_Term, "period_password");
            TxtIss_Doc.DataBindings.Add("Text", Terminals_Main._Bs_Term, "frm_doc_Is");
            TxtPwd.DataBindings.Add("text", Terminals_Main._Bs_Term, "password");
            TxtNo_Doc.DataBindings.Add("Text", Terminals_Main._Bs_Term, "Frm_DOC_NO");
            TxtDoc_Da.DataBindings.Add("Text", Terminals_Main._Bs_Term, "frm_doc_Da");
            TxtDoc_EDa.DataBindings.Add("text", Terminals_Main._Bs_Term, "doc_eda");
            CboCit_Id.DataBindings.Add("SelectedValue", Terminals_Main._Bs_Term, "Cit_Id");
            CboBaseCur_Id.DataBindings.Add("SelectedValue", Terminals_Main._Bs_Term, "Cur_ID");
            CboTerm_State.DataBindings.Add("SelectedIndex", Terminals_Main._Bs_Term, "TERM_STATE_ID");
            Cbo_loc_int_case.DataBindings.Add("SelectedIndex", Terminals_Main._Bs_Term, "loc_int_case");
            CboDoc_id.DataBindings.Add("SelectedValue", Terminals_Main._Bs_Term, "Fmd_id");
            TxtStartDate.DataBindings.Add("Text", Terminals_Main._Bs_Term, "STARTDATE");
            CHK_Rem_c.DataBindings.Add("Checked", Terminals_Main._Bs_Term, "ver_check");
            CHK_comm.DataBindings.Add("Checked", Terminals_Main._Bs_Term, "comm_rem_flag");
            chk_daily_opening.DataBindings.Add("Checked", Terminals_Main._Bs_Term, "chk_daily_opening");
            //CHK_comm

        }
        //------------------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            string Doc_Date = "";
            string Doc_Edate = "";

            #region Validation
           

            if (Txt_Aname.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please Insert Arabic Name", MyGeneral_Lib.LblCap);
                Txt_Aname.Focus();
                return;
            }
            if (Txt_Ename.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم الاجنبي" : "Please Insert English Name", MyGeneral_Lib.LblCap);
                Txt_Ename.Focus();
                return;
            }

            if (TxtT_Symbol.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الرمز" : "Please Insert terminal symbol", MyGeneral_Lib.LblCap);
                TxtT_Symbol.Focus();
                return;
            }
            if (TxtTax_No.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الرقم الضريبي " : "Please Insert Tax Number", MyGeneral_Lib.LblCap);
                Txt_Ename.Focus();
                return;
            }
            if (Txt_AAdress.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل العنوان العربي" : "Please Insert Arabic Address", MyGeneral_Lib.LblCap);
                Txt_AAdress.Focus();
                return;
            }
            if (Txt_EAdress.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل العنوان الاجنبي" : "Please Insert English Address", MyGeneral_Lib.LblCap);
                Txt_EAdress.Focus();
                return;

            }
            if (Txt_phone.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الهاتف" : "Please Insert Phone1", MyGeneral_Lib.LblCap);
                Txt_phone.Focus();
                return;
            }


            if (TxtPwd.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور" : "Please enter password", MyGeneral_Lib.LblCap);
                TxtPwd.Focus();
                return;
            }

           

            if (CboDoc_id.SelectedIndex > 1)
            {

                if (TxtNo_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الوثيقة" : "Please Insert Formal Document No.", MyGeneral_Lib.LblCap);
                    TxtNo_Doc.Focus();
                    return;
                }

                Doc_Date = TxtDoc_Da.Text;
                 Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
                if (Ord_Strt == "0" || Ord_Strt == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ اصدار الوثيقة" : "Please Insert Issue Date of formal Document", MyGeneral_Lib.LblCap);
                    TxtDoc_Da.Focus();
                    return;
                }

                Doc_Edate = TxtDoc_EDa.Text;
                Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_EDa.Text);
                if (Ord_Strt == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ نفاذ الوثيقة" : "Please make sure of expire date of formal document", MyGeneral_Lib.LblCap);
                    TxtDoc_EDa.Focus();
                    return;
                }
                else if (Ord_Strt == "0")
                {
                    Doc_Edate = "";
                }

                
                //Doc_Edate = TxtDoc_EDa.Text;

                if (TxtIss_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل جهة اصدار الوثيقة" : "Please Insert Issuer of Formal Document", MyGeneral_Lib.LblCap);
                    TxtIss_Doc.Focus();
                    return;
                }
            }

            Ord_Strt = MyGeneral_Lib.DateChecking(TxtStartDate.Text);
            if (Ord_Strt == "0" || Ord_Strt == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ تطبيق النظام" : "Please Insert Start Date", MyGeneral_Lib.LblCap);
                TxtStartDate.Focus();
                return;
            }

            if (TxtDiscount_Amount.Text == "0.000")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل قيمة الخصم" : "Please Insert Discount Amount", MyGeneral_Lib.LblCap);
                TxtDiscount_Amount.Focus();
                return;
            }


            if (Txt_period_pass.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل فترة تغيير كلمة المرور" : "Please Insert period password", MyGeneral_Lib.LblCap);
                Txt_period_pass.Focus();
                return;
            }
            //if (Convert.ToInt16(CboTerm_State.SelectedIndex) < 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "حدد حالة الفرع" : "Please select teminal state", MyGeneral_Lib.LblCap);
            //    CboTerm_State.Focus();
            //    return;
            //}
            //if (Convert.ToInt16( CboCoun_Id.SelectedValue ) < 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " حدد البــلد" : "Please select the Country", MyGeneral_Lib.LblCap);
            //    return;
            //}
            if (Convert.ToInt16(CboDoc_id.SelectedValue) < 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثيقة" : "Please select the type of Doc.", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(CboBaseCur_Id.SelectedValue)< 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع العملة " : "Please select the type of Currency.", MyGeneral_Lib.LblCap);
                return;
            }

            #endregion

           
            Int16 Con_Id = Convert.ToInt16( connection.SQLDS.Tables["all_master6"].Rows[CboCit_Id.SelectedIndex]["Con_id"]);

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Txt_Aname.Text.Trim());
            ItemList.Insert(1, Txt_Ename.Text.Trim());
            ItemList.Insert(2, Con_Id);
            ItemList.Insert(3, CboCit_Id.SelectedValue);
            ItemList.Insert(4, Txt_AAdress.Text.Trim());
            ItemList.Insert(5, Txt_EAdress.Text.Trim());
            ItemList.Insert(6, TxtEmail.Text.Trim());
            ItemList.Insert(7, Txt_phone.Text.Trim());
            ItemList.Insert(8, CboDoc_id.SelectedValue);
            ItemList.Insert(9, TxtNo_Doc.Text.Trim());
            ItemList.Insert(10, TxtIss_Doc.Text.Trim());
            ItemList.Insert(11, Doc_Date);
            ItemList.Insert(12, Doc_Edate);
            ItemList.Insert(13, 0);
            ItemList.Insert(14, TxtT_Symbol.Text.Trim());
            ItemList.Insert(15, Convert.ToInt16(ChkCenter_Flag.Checked));   
            ItemList.Insert(16, TxtStartDate.Text.ToString());
            ItemList.Insert(17, TxtDiscount_Amount.Text.Trim());
            ItemList.Insert(18, CboTerm_State.SelectedIndex);
            ItemList.Insert(19, CboBaseCur_Id.SelectedValue );
            ItemList.Insert(20, TxtPwd.Text.Trim());
           // ItemList.Insert(21, TxtDay_Close.Text.Trim() + TxtMonth_Close.Text.Trim() );
            ItemList.Insert(21, TxtTax_No.Text.Trim());
            ItemList.Insert(22, Frm_Id);
            ItemList.Insert(23, cust_id);
            ItemList.Insert(24, T_ID);
            ItemList.Insert(25, connection.user_id);
            ItemList.Insert(26, connection.Lang_id);
            ItemList.Insert(27, Convert.ToInt16( CHK_Rem_c.Checked));
            ItemList.Insert(28, Convert.ToInt16(CHK_comm.Checked));
            ItemList.Insert(29, Txt_period_pass.Text.Trim());
            ItemList.Insert(30, "");
            ItemList.Insert(31, Cbo_loc_int_case.SelectedIndex);
            ItemList.Insert(32, Convert.ToInt16(chk_daily_opening.Checked));
            
            MyGeneral_Lib.Copytocliptext("Add_Upd_Terminals", ItemList);
           
            connection.scalar("Add_Upd_Terminals", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
            if (Frm_Id == 1)
            {
          
                ConChange = false;
         
                Txt_Aname.Text = "";
                Txt_Ename.Text = "";
                TxtT_Symbol.Text = "";
                ChkCenter_Flag.Checked = false;     
                CboCit_Id.SelectedIndex = 0;
                Txt_AAdress.Text = "";
                TxtTax_No.Text = "";
                Txt_EAdress.Text = "";
                Txt_phone.Text = "";
                TxtEmail.Text = "";
                CboDoc_id.SelectedIndex = 0;
                ConChange = true;
                CboCoun_Id_SelectedIndexChanged(null, null);
                CboTerm_State.SelectedIndex = 0;
                TxtDoc_Da.Text = "00000000";
                TxtDoc_EDa.Text = "00000000";
                TxtNo_Doc.Text = "";
                TxtIss_Doc.Text = "";
                TxtStartDate.Text = "00000000";
                TxtDiscount_Amount.ResetText();
                Txt_period_pass.Text = "";
                CboTerm_State.SelectedIndex = 0;
                CboBaseCur_Id.SelectedIndex = 0;
                TxtPwd.Text = "";
                ConChange = true;
                CHK_Rem_c.Checked = false;
                CHK_comm.Checked = false;
                chk_daily_opening.Checked = false;
            }
            else this.Close();
        }
        //------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------------------------------------------------------
        private void Terminals_Add_Upd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //------------------------------------------------------------------------------
        private void Terminals_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            ConChange = false;
            string[] Str = { "all_master", "all_master2", "all_master5","all_master6" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //------------------------------------------------------------------------------
        private void CboDoc_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboDoc_id.SelectedIndex == 1)
            {
                TxtNo_Doc.Enabled = false;
                TxtIss_Doc.Enabled = false;
                TxtDoc_Da.Enabled = false;
                TxtDoc_EDa.Enabled = false;
                TxtNo_Doc.Text = "";
                TxtIss_Doc.Text = "";
                TxtDoc_Da.Text = "0000/00/00";
                TxtDoc_EDa.Text = "0000/00/00";
            }
            else
            {
                TxtNo_Doc.Enabled = true;
                TxtIss_Doc.Enabled = true;
                TxtDoc_Da.Enabled = true;
                TxtDoc_EDa.Enabled = true;
            }
        }
  
    }
}
