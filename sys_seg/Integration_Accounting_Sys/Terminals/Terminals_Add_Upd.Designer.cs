﻿namespace Integration_Accounting_Sys
{
    partial class Terminals_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Txt_Aname = new System.Windows.Forms.TextBox();
            this.Txt_Ename = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtT_Symbol = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CboCit_Id = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_EAdress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_AAdress = new System.Windows.Forms.TextBox();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CboDoc_id = new System.Windows.Forms.ComboBox();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.CboBaseCur_Id = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.CboTerm_State = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtPwd = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.ChkCenter_Flag = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AddBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_phone = new System.Windows.Forms.TextBox();
            this.CHK_Rem_c = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtTax_No = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtDoc_EDa = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtDiscount_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtStartDate = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtDoc_Da = new Integration_Accounting_Sys.MyDateTextBox();
            this.CHK_comm = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_period_pass = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Cbo_loc_int_case = new System.Windows.Forms.ComboBox();
            this.chk_daily_opening = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(6, 28);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(830, 2);
            this.flowLayoutPanel7.TabIndex = 491;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(96, 2);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(209, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(660, 2);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(160, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(596, 5);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 487;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(8, 5);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 486;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(5, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 18);
            this.label16.TabIndex = 494;
            this.label16.Text = "الاســم العربـــــــي:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(10, 160);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(88, 18);
            this.label5.TabIndex = 511;
            this.label5.Text = "العنـوان بالعـربي :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(6, 217);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(91, 16);
            this.label19.TabIndex = 526;
            this.label19.Text = "معلومات الوثيقة...";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(8, 241);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(82, 18);
            this.label10.TabIndex = 522;
            this.label10.Text = "نوع الوثـيـقــــة :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(8, 268);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(85, 18);
            this.label15.TabIndex = 524;
            this.label15.Text = "جـهـة الاصـدار :";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(440, 451);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 29);
            this.ExtBtn.TabIndex = 23;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Txt_Aname
            // 
            this.Txt_Aname.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Aname.Location = new System.Drawing.Point(118, 43);
            this.Txt_Aname.Name = "Txt_Aname";
            this.Txt_Aname.Size = new System.Drawing.Size(341, 23);
            this.Txt_Aname.TabIndex = 2;
            // 
            // Txt_Ename
            // 
            this.Txt_Ename.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Ename.Location = new System.Drawing.Point(118, 73);
            this.Txt_Ename.Name = "Txt_Ename";
            this.Txt_Ename.Size = new System.Drawing.Size(340, 23);
            this.Txt_Ename.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(6, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 18);
            this.label17.TabIndex = 495;
            this.label17.Text = "الاسم الاجنبـــــــي:";
            // 
            // TxtT_Symbol
            // 
            this.TxtT_Symbol.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtT_Symbol.Location = new System.Drawing.Point(670, 73);
            this.TxtT_Symbol.Name = "TxtT_Symbol";
            this.TxtT_Symbol.Size = new System.Drawing.Size(152, 23);
            this.TxtT_Symbol.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(580, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 18);
            this.label1.TabIndex = 497;
            this.label1.Text = "رمــــز الفــــرع:";
            // 
            // CboCit_Id
            // 
            this.CboCit_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCit_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCit_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCit_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCit_Id.FormattingEnabled = true;
            this.CboCit_Id.Location = new System.Drawing.Point(118, 126);
            this.CboCit_Id.Name = "CboCit_Id";
            this.CboCit_Id.Size = new System.Drawing.Size(291, 24);
            this.CboCit_Id.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(10, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 18);
            this.label12.TabIndex = 501;
            this.label12.Text = "المديـنـــة /البـــــلد :";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(535, 184);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(288, 23);
            this.TxtEmail.TabIndex = 11;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(421, 186);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 18);
            this.label18.TabIndex = 515;
            this.label18.Text = "البـــريد الالكتروني:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(10, 188);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(83, 18);
            this.label7.TabIndex = 513;
            this.label7.Text = "الــهــــــاتـــــف :";
            // 
            // Txt_EAdress
            // 
            this.Txt_EAdress.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_EAdress.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EAdress.Location = new System.Drawing.Point(535, 157);
            this.Txt_EAdress.Name = "Txt_EAdress";
            this.Txt_EAdress.Size = new System.Drawing.Size(288, 23);
            this.Txt_EAdress.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(420, 159);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(98, 18);
            this.label6.TabIndex = 512;
            this.label6.Text = "العنـــوان بالاجنبـي :";
            // 
            // Txt_AAdress
            // 
            this.Txt_AAdress.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_AAdress.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AAdress.Location = new System.Drawing.Point(118, 158);
            this.Txt_AAdress.Name = "Txt_AAdress";
            this.Txt_AAdress.Size = new System.Drawing.Size(291, 23);
            this.Txt_AAdress.TabIndex = 8;
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(118, 266);
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(491, 23);
            this.TxtIss_Doc.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(614, 241);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(88, 18);
            this.label13.TabIndex = 523;
            this.label13.Text = "تاريخ الاصــدار :";
            // 
            // CboDoc_id
            // 
            this.CboDoc_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDoc_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDoc_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDoc_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDoc_id.FormattingEnabled = true;
            this.CboDoc_id.Location = new System.Drawing.Point(118, 236);
            this.CboDoc_id.Name = "CboDoc_id";
            this.CboDoc_id.Size = new System.Drawing.Size(218, 24);
            this.CboDoc_id.TabIndex = 12;
            this.CboDoc_id.SelectedIndexChanged += new System.EventHandler(this.CboDoc_id_SelectedIndexChanged);
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(427, 239);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(182, 23);
            this.TxtNo_Doc.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(337, 241);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(78, 18);
            this.label9.TabIndex = 521;
            this.label9.Text = "رقــم الوثـيـقـة :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 229);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(830, 1);
            this.flowLayoutPanel1.TabIndex = 527;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(5, 309);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(830, 1);
            this.flowLayoutPanel5.TabIndex = 528;
            // 
            // CboBaseCur_Id
            // 
            this.CboBaseCur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboBaseCur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboBaseCur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboBaseCur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboBaseCur_Id.FormattingEnabled = true;
            this.CboBaseCur_Id.Location = new System.Drawing.Point(118, 356);
            this.CboBaseCur_Id.Name = "CboBaseCur_Id";
            this.CboBaseCur_Id.Size = new System.Drawing.Size(192, 24);
            this.CboBaseCur_Id.TabIndex = 20;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(10, 359);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 18);
            this.label21.TabIndex = 532;
            this.label21.Text = "العمـلـة المحليــــة:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(8, 330);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(98, 18);
            this.label22.TabIndex = 533;
            this.label22.Text = "تاريخ تطبيق النظام :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(314, 331);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(111, 16);
            this.label37.TabIndex = 674;
            this.label37.Text = "اعلى قيمة خصـــــــــم:";
            // 
            // CboTerm_State
            // 
            this.CboTerm_State.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboTerm_State.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboTerm_State.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboTerm_State.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboTerm_State.FormattingEnabled = true;
            this.CboTerm_State.Items.AddRange(new object[] {
            "موقوف",
            "فعال"});
            this.CboTerm_State.Location = new System.Drawing.Point(699, 327);
            this.CboTerm_State.Name = "CboTerm_State";
            this.CboTerm_State.Size = new System.Drawing.Size(124, 24);
            this.CboTerm_State.TabIndex = 19;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(596, 330);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 18);
            this.label23.TabIndex = 676;
            this.label23.Text = "حالــــــة الفـــــــرع:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(318, 360);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 16);
            this.label25.TabIndex = 681;
            this.label25.Text = "كـلمـــــــــة المـرور:";
            // 
            // TxtPwd
            // 
            this.TxtPwd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPwd.Location = new System.Drawing.Point(427, 357);
            this.TxtPwd.MaxLength = 50;
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.PasswordChar = '*';
            this.TxtPwd.Size = new System.Drawing.Size(395, 23);
            this.TxtPwd.TabIndex = 21;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Maroon;
            this.label29.Location = new System.Drawing.Point(6, 103);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(136, 16);
            this.label29.TabIndex = 691;
            this.label29.Text = "العنوان والمعلومات العامة...";
            // 
            // ChkCenter_Flag
            // 
            this.ChkCenter_Flag.AutoSize = true;
            this.ChkCenter_Flag.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCenter_Flag.ForeColor = System.Drawing.Color.Navy;
            this.ChkCenter_Flag.Location = new System.Drawing.Point(463, 44);
            this.ChkCenter_Flag.Name = "ChkCenter_Flag";
            this.ChkCenter_Flag.Size = new System.Drawing.Size(92, 20);
            this.ChkCenter_Flag.TabIndex = 3;
            this.ChkCenter_Flag.Text = "الادارة المركز";
            this.ChkCenter_Flag.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Maroon;
            this.label20.Location = new System.Drawing.Point(6, 298);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(96, 16);
            this.label20.TabIndex = 798;
            this.label20.Text = "المعلومات المالية...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(616, 268);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(90, 18);
            this.label4.TabIndex = 800;
            this.label4.Text = "تأريخ النفـــــــاذ : ";
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(351, 451);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 29);
            this.AddBtn.TabIndex = 22;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 115);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(830, 1);
            this.flowLayoutPanel4.TabIndex = 692;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(580, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 18);
            this.label14.TabIndex = 804;
            this.label14.Text = "الرقم الضـــريبي";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 29);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 420);
            this.flowLayoutPanel6.TabIndex = 691;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(835, 29);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1, 420);
            this.flowLayoutPanel8.TabIndex = 692;
            // 
            // Txt_phone
            // 
            this.Txt_phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_phone.Location = new System.Drawing.Point(118, 187);
            this.Txt_phone.Name = "Txt_phone";
            this.Txt_phone.Size = new System.Drawing.Size(291, 23);
            this.Txt_phone.TabIndex = 10;
            // 
            // CHK_Rem_c
            // 
            this.CHK_Rem_c.AutoSize = true;
            this.CHK_Rem_c.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_Rem_c.ForeColor = System.Drawing.Color.Navy;
            this.CHK_Rem_c.Location = new System.Drawing.Point(8, 386);
            this.CHK_Rem_c.Name = "CHK_Rem_c";
            this.CHK_Rem_c.Size = new System.Drawing.Size(114, 20);
            this.CHK_Rem_c.TabIndex = 807;
            this.CHK_Rem_c.Text = "تأكيد الحــــــوالات";
            this.CHK_Rem_c.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 448);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(830, 1);
            this.flowLayoutPanel2.TabIndex = 808;
            // 
            // TxtTax_No
            // 
            this.TxtTax_No.AllowSpace = false;
            this.TxtTax_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTax_No.Location = new System.Drawing.Point(669, 43);
            this.TxtTax_No.Name = "TxtTax_No";
            this.TxtTax_No.Size = new System.Drawing.Size(152, 23);
            this.TxtTax_No.TabIndex = 4;
            // 
            // TxtDoc_EDa
            // 
            this.TxtDoc_EDa.DateSeperator = '/';
            this.TxtDoc_EDa.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDoc_EDa.Location = new System.Drawing.Point(705, 266);
            this.TxtDoc_EDa.Mask = "0000/00/00";
            this.TxtDoc_EDa.Name = "TxtDoc_EDa";
            this.TxtDoc_EDa.PromptChar = ' ';
            this.TxtDoc_EDa.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtDoc_EDa.Size = new System.Drawing.Size(121, 23);
            this.TxtDoc_EDa.TabIndex = 16;
            this.TxtDoc_EDa.Text = "00000000";
            this.TxtDoc_EDa.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtDiscount_Amount
            // 
            this.TxtDiscount_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscount_Amount.Location = new System.Drawing.Point(427, 328);
            this.TxtDiscount_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtDiscount_Amount.Name = "TxtDiscount_Amount";
            this.TxtDiscount_Amount.NumberDecimalDigits = 3;
            this.TxtDiscount_Amount.NumberDecimalSeparator = ".";
            this.TxtDiscount_Amount.NumberGroupSeparator = ",";
            this.TxtDiscount_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDiscount_Amount.Size = new System.Drawing.Size(163, 23);
            this.TxtDiscount_Amount.TabIndex = 18;
            this.TxtDiscount_Amount.Text = "0.000";
            // 
            // TxtStartDate
            // 
            this.TxtStartDate.DateSeperator = '/';
            this.TxtStartDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStartDate.Location = new System.Drawing.Point(118, 327);
            this.TxtStartDate.Mask = "0000/00/00";
            this.TxtStartDate.Name = "TxtStartDate";
            this.TxtStartDate.PromptChar = ' ';
            this.TxtStartDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtStartDate.Size = new System.Drawing.Size(192, 23);
            this.TxtStartDate.TabIndex = 17;
            this.TxtStartDate.Text = "00000000";
            this.TxtStartDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.DateSeperator = '/';
            this.TxtDoc_Da.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDoc_Da.Location = new System.Drawing.Point(705, 239);
            this.TxtDoc_Da.Mask = "0000/00/00";
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.PromptChar = ' ';
            this.TxtDoc_Da.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtDoc_Da.Size = new System.Drawing.Size(121, 23);
            this.TxtDoc_Da.TabIndex = 14;
            this.TxtDoc_Da.Text = "00000000";
            this.TxtDoc_Da.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // CHK_comm
            // 
            this.CHK_comm.AutoSize = true;
            this.CHK_comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_comm.ForeColor = System.Drawing.Color.Navy;
            this.CHK_comm.Location = new System.Drawing.Point(155, 386);
            this.CHK_comm.Name = "CHK_comm";
            this.CHK_comm.Size = new System.Drawing.Size(143, 20);
            this.CHK_comm.TabIndex = 1200;
            this.CHK_comm.Text = "اعتــماد نســب العمــولات";
            this.CHK_comm.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(313, 387);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 16);
            this.label8.TabIndex = 1202;
            this.label8.Text = "تغيير كلمة المرور كل:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(532, 387);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 16);
            this.label11.TabIndex = 1203;
            this.label11.Text = "يوم";
            // 
            // Txt_period_pass
            // 
            this.Txt_period_pass.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_period_pass.Location = new System.Drawing.Point(427, 384);
            this.Txt_period_pass.Name = "Txt_period_pass";
            this.Txt_period_pass.Size = new System.Drawing.Size(102, 23);
            this.Txt_period_pass.TabIndex = 1204;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(599, 390);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 18);
            this.label24.TabIndex = 1206;
            this.label24.Text = "مسارالحوالة:";
            // 
            // Cbo_loc_int_case
            // 
            this.Cbo_loc_int_case.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_loc_int_case.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_loc_int_case.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_loc_int_case.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_loc_int_case.FormattingEnabled = true;
            this.Cbo_loc_int_case.Items.AddRange(new object[] {
            "جميع الحالات",
            "داخلي فقط",
            "خارجي فقط"});
            this.Cbo_loc_int_case.Location = new System.Drawing.Point(671, 387);
            this.Cbo_loc_int_case.Name = "Cbo_loc_int_case";
            this.Cbo_loc_int_case.Size = new System.Drawing.Size(152, 24);
            this.Cbo_loc_int_case.TabIndex = 1205;
            // 
            // chk_daily_opening
            // 
            this.chk_daily_opening.AutoSize = true;
            this.chk_daily_opening.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_daily_opening.ForeColor = System.Drawing.Color.Navy;
            this.chk_daily_opening.Location = new System.Drawing.Point(31, 412);
            this.chk_daily_opening.Name = "chk_daily_opening";
            this.chk_daily_opening.Size = new System.Drawing.Size(206, 20);
            this.chk_daily_opening.TabIndex = 1207;
            this.chk_daily_opening.Text = "اغـلاق مالــي للسجــــلات اوتوماتيكـي";
            this.chk_daily_opening.UseVisualStyleBackColor = true;
            // 
            // Terminals_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 486);
            this.Controls.Add(this.chk_daily_opening);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Cbo_loc_int_case);
            this.Controls.Add(this.Txt_period_pass);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.CHK_comm);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.CHK_Rem_c);
            this.Controls.Add(this.Txt_phone);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.TxtTax_No);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TxtDoc_EDa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.ChkCenter_Flag);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.TxtPwd);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.CboTerm_State);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.TxtDiscount_Amount);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.CboBaseCur_Id);
            this.Controls.Add(this.TxtStartDate);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.CboDoc_id);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_AAdress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_EAdress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CboCit_Id);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtT_Symbol);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_Ename);
            this.Controls.Add(this.Txt_Aname);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Terminals_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "153";
            this.Text = "Terminals_Add_Upd";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Terminals_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.Terminals_Add_Upd_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Terminals_Add_Upd_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.TextBox Txt_Aname;
        private System.Windows.Forms.TextBox Txt_Ename;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TxtT_Symbol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboCit_Id;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_EAdress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_AAdress;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private MyDateTextBox TxtDoc_Da;
        private MyDateTextBox TxtStartDate;
        private System.Windows.Forms.ComboBox CboBaseCur_Id;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDiscount_Amount;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox CboTerm_State;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox TxtPwd;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox ChkCenter_Flag;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label4;
        private MyDateTextBox TxtDoc_EDa;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.ComboBox CboDoc_id;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label14;
        private NumericTextBox TxtTax_No;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.TextBox Txt_phone;
        private System.Windows.Forms.CheckBox CHK_Rem_c;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.CheckBox CHK_comm;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_period_pass;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox Cbo_loc_int_case;
        private System.Windows.Forms.CheckBox chk_daily_opening;
    }
}