﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Rem_Blacklist_confirm : Form
    {
        BindingSource _Bs_Blacklist = new BindingSource();
        public static Int16 Back_btn = 0;
        public static string  Notes_BL = "";
        BindingSource BS_rem_reveal = new BindingSource();
      
        //-----------------------
        public Rem_Blacklist_confirm()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_Rem_Blacklist.AutoGenerateColumns = false;
          
        }


        private void Grd_Rem_Blacklist_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Blacklist);
        }

        private void Rem_Blacklist_OK_Load(object sender, EventArgs e)
        {
            Back_btn = 0;

            if (connection.Lang_id != 1)
            {

                Btn_Add.Text = "Export";
                //Btn_Ok.Text = "Continue";
                //Btn_End.Text = "Refuse";
                //label12.Text = "Reason to continue ........";
                Column1.HeaderText = "Name";
                Column2.HeaderText = "Title";
                Column3.HeaderText = "Date of Birth";
                Column5.HeaderText = "Address";
                Column8.HeaderText = "Nationality";
                Column9.HeaderText = "Alias Information";
                Column18.HeaderText = "ORIGINAL NAME";
                Column4.HeaderText = "List Name";

            }

            try
            {
                _Bs_Blacklist.DataSource = connection.SQLDS.Tables["per_rem_blacklist_tbl"];
                Grd_Rem_Blacklist.DataSource = _Bs_Blacklist;
            }
            catch { };
            try
            {

                BS_rem_reveal.DataSource = connection.SQLDS.Tables["per_rem_blacklist_tbl1"];


                Txt_S_ACOUN_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "Sa_ACOUN_NAME" : "Sa_ECOUN_NAME"].ToString();
                Txt_R_ACOUN_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME"].ToString();
                Txt_S_NAT_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME"].ToString();
                Txt_R_NAT_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME"].ToString();
                Txt_S_job.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "s_job" : "s_Ejob"].ToString();
                Txt_R_job.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "r_job" : "r_Ejob"].ToString();
                Txt_sFRM_EDOC_NA.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA"].ToString();
                Txt_RFRM_EDOC_NA.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA"].ToString();

                Chk_S_NAT_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_S_count_nat" : "E_S_count_nat"].ToString();
                Chk_R_NAT_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_R_count_nat" : "E_R_count_nat"].ToString();


                Chk_S_ACOUN_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_count_S_coun" : "E_count_S_coun"].ToString();
                Chk_R_ACOUN_NAME.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_count_R_coun" : "E_count_R_coun"].ToString();

                Chk_S_job.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_count_S_job" : "E_count_S_job"].ToString();
                Chk_R_job.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_count_R_job" : "E_count_R_job"].ToString();

                Chk_sFRM_EDOC_NA.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_count_S_Frm_doc" : "E_count_S_Frm_doc"].ToString();
                chk_RFRM_EDOC_NA.Text = ((DataRowView)BS_rem_reveal.Current).Row[connection.Lang_id == 1 ? "A_count_R_Frm_doc" : "E_count_R_Frm_doc"].ToString();

            }
            catch { };
             

        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Back_btn = 0;

            DataTable ExpDt = connection.SQLDS.Tables["per_rem_blacklist_tbl"].DefaultView.ToTable(false,"List_Name", "Name", "NAME_ORIGINAL_SCRIPT", "Alias_Information", "Title",
                                                                                                         "Date_of_Birth", "Address", "Nationality" );
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_Rem_Blacklist };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Btn_End_Click(object sender, EventArgs e)
        {
           

            Back_btn = 1;
           // Notes_BL = Txt_Notes_BL.Text.Trim();
            this.Close();
       
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {

            //if (Txt_Notes_BL.Text.Trim() == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "يرجى ادخال الملاحظات" : "Please Enter The Notes", MyGeneral_Lib.LblCap);
            //    Txt_Notes_BL.Focus();
            //    return;
            //}

            //Notes_BL = Txt_Notes_BL.Text.Trim();
            this.Close();
        }

        private void Rem_Blacklist_OK_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "per_rem_blacklist_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        

    }
}