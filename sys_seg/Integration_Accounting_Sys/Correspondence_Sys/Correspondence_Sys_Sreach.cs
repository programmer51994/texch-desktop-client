﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Correspondence_Sys_Sreach : Form
    {
        string @format = "dd/MM/yyyy";
        Int32 from_nrec_date = 0;
        Int32 to_nrec_date = 0;
        //byte R_Type_id = 0;
        public static Int16 Rem_type = 0;
        public Correspondence_Sys_Sreach()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }
        private void Remittances_Query_search_Load(object sender, EventArgs e)
        {
            TxtToDate.CustomFormat = " ";
            TxtFromDate.CustomFormat = " ";
            //cmb_Loc_international.SelectedIndex = 1;
           // Cmb_rem_type.SelectedIndex = 0;

            Type_rem.SelectedIndex = 0;
            connection.SqlExec("exec Remittances_Query_search " + 2 + ',' + connection.T_ID, "Remittances_Query_search_TBl");

            CboCust_Id.DataSource = connection.SQLDS.Tables["Remittances_Query_search_TBl"];
            CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            CboCust_Id.ValueMember = "cust_id";

            cmb_t_city.DataSource = connection.SQLDS.Tables["Remittances_Query_search_TBl1"];
            cmb_t_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";
            cmb_t_city.ValueMember = "T_city_id";


            Cmb_Scity_ID.DataSource = connection.SQLDS.Tables["Remittances_Query_search_TBl2"];
            Cmb_Scity_ID.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";
            Cmb_Scity_ID.ValueMember = "S_city_id";


            Cmb_R_CUR_ID.DataSource = connection.SQLDS.Tables["Remittances_Query_search_TBl3"];
            Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "cur_aname" : "cur_ename";
            Cmb_R_CUR_ID.ValueMember = "R_Cur_Id";


            //Cmb_rem_case.DataSource = connection.SQLDS.Tables["Remittances_Query_search_TBl4"];
            //Cmb_rem_case.DisplayMember = connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA";
            //Cmb_rem_case.ValueMember = "case_id";

            cmb_range.SelectedIndex = 0;
            if (connection.Lang_id == 2)
            {
                //Cmb_rem_type.Items[0] = "out remittance";
                //Cmb_rem_type.Items[1] = "in remittance";

                //cmb_Loc_international.Items[0] = "local network";
                //cmb_Loc_international.Items[1] = "online network";

                cmb_range.Items[0] = "Equal";
                cmb_range.Items[1] = "Above";
                cmb_range.Items[2] = "less";
           
                Cbo_Loc_Inter.Items[0] = "Local network-Offline";
                Cbo_Loc_Inter.Items[1] = "Online Network";
               
            }
            Cbo_Loc_Inter.SelectedIndex = 1;
            
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmb_rem_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Cmb_rem_type.SelectedIndex ==  0)
            //{R_Type_id = 2 ; }
            //else
            //{R_Type_id = 1 ; }
            //string[] Tbl = { "Remittances_Query_search_TBl", "Remittances_Query_search_TBl1", "Remittances_Query_search_TBl2", "Remittances_Query_search_TBl3", "Remittances_Query_search_TBl4" };
            //foreach (string Str in Tbl)
            //{
            //    if (connection.SQLDS.Tables.Contains(Str))
            //        connection.SQLDS.Tables.Remove(Str);
            //}
           

        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
                string from_Casedate="";
                string to_Casedate = "";

            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                from_Casedate = Convert.ToString(from_nrec_date);
            }
            else
            {
                from_nrec_date = 0;
            }
            if (TxtToDate.Checked == true)
            {

                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                to_Casedate = Convert.ToString(to_nrec_date);
            }
            else
            {
                to_nrec_date = 0;
            }

            string Sing = ""; 
            if (cmb_range.SelectedIndex == 0)
            { Sing = "="; }
            if (cmb_range.SelectedIndex == 1)
            { Sing = ">"; }
            if (cmb_range.SelectedIndex == 2)
            { Sing = "<"; }

            Int16 Cust_type = 0;
            Int16 Cust_ID = 0;
            if (CboCust_Id.SelectedIndex >= 0)
            {
                Cust_type = 1;
                Cust_ID = Convert.ToInt16(CboCust_Id.SelectedValue);
                
            }// جميع الثانويين

            if (CboCust_Id.SelectedIndex == 0)// محدد من الكومبو
            {
                Cust_type = 0;
                Cust_ID = Convert.ToInt16(connection.SQLDS.Tables["Remittances_Query_search_TBl"].DefaultView.ToTable(false, "t_id", "Cust_ID").Select("T_id = " + connection.T_ID).CopyToDataTable().Rows[0]["Cust_ID"]);
            }

            Rem_type =Convert.ToInt16( Type_rem.SelectedIndex) ;
            string Sqltexe = "Exec Buffer_Remittances_Query " + "'" + from_Casedate + "'," + "'" + to_Casedate + "'," + 2 + "," + 101 + "," + Cust_ID
                + "," + Cbo_Loc_Inter.SelectedIndex + ",'" + Txt_S_name.Text.Trim() + "'," + Cust_Check.Checked + ",'" + Txt_R_name.Text.Trim() + "'," + Chk_Cust.Checked
                + ",'" + Txt_Rec_Phone.Text.Trim() + "'," + Chk_Print.Checked + ",'" + Txtrem_no.Text.Trim() + "'," + Cmb_Scity_ID.SelectedValue + "," + cmb_t_city.SelectedValue
                + "," + Cmb_R_CUR_ID.SelectedValue + "," + Convert.ToDecimal(Txtr_amount.Text.Trim()) + ",'" + Sing + "','" + txt_purpose.Text.Trim() + "'," + Check.Checked
                + ",'" + txt_notes.Text.Trim() + "'," + User_Check.Checked + "," + Cust_type + "," + Rem_type;


             connection.SqlExec(Sqltexe, "TBL_Remittances_Query");
            if (connection.SQLDS.Tables["TBL_Remittances_Query"].Rows.Count > 0)
            {

                Correspondence_Sys_Confirm Frm = new Correspondence_Sys_Confirm();
                this.Visible = false;
                Frm.ShowDialog();
                this.Visible = true;
            }
            else 
            {
                string[] Str = { "TBL_Remittances_Query" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                MessageBox.Show("لا توجد حوالة تحقق الشروط" + (Char)Keys.Enter + " No Rem. For this Condition");
                return;
                
            }
            
        }

        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtFromDate.Checked == true)
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = @format;
            }
            else
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = " ";
            }
        }

        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtToDate.Checked == true)
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = @format;
            }
            else
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = " ";
            }
        }
        //------------------------------------------------------------------------
        private void Remittances_Query_search_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "TBL_Remittances_Query", "TBL_Remittances_Query1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Cbo_Loc_Inter_SelectedIndexChanged(object sender, EventArgs e)
        {
            get_deatails();
        }
        //------------------------------------------------------------------------
        private void get_deatails()
        {

            if (Cbo_Loc_Inter.SelectedIndex == 1 && Type_rem.SelectedIndex == 0)//حوالات اونلاين وصندوق ما حاجة وكلاء
            { CboCust_Id.Enabled = false;
            CboCust_Id.SelectedIndex = 0;
            }
            else
            { CboCust_Id.Enabled = true; }
        
        }
        //------------------------------------------------------------------------
        private void Type_rem_SelectedIndexChanged(object sender, EventArgs e)
        {
            get_deatails();
        }

    }
}