﻿namespace Integration_Accounting_Sys
{
    partial class Rem_Blacklist_confirm_ok
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.Grd_Rem_Blacklist = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Btn_End = new System.Windows.Forms.Button();
            this.Btn_Ok = new System.Windows.Forms.Button();
            this.Txt_Notes_BL = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_S_ACOUN_NAME = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Chk_S_ACOUN_NAME = new System.Windows.Forms.Label();
            this.Chk_R_ACOUN_NAME = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_R_ACOUN_NAME = new System.Windows.Forms.TextBox();
            this.Chk_R_NAT_NAME = new System.Windows.Forms.Label();
            this.Txt_R_NAT_NAME = new System.Windows.Forms.TextBox();
            this.Chk_S_NAT_NAME = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_S_NAT_NAME = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Chk_R_job = new System.Windows.Forms.Label();
            this.Txt_R_job = new System.Windows.Forms.TextBox();
            this.Chk_S_job = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Txt_S_job = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chk_RFRM_EDOC_NA = new System.Windows.Forms.Label();
            this.Txt_RFRM_EDOC_NA = new System.Windows.Forms.TextBox();
            this.Chk_sFRM_EDOC_NA = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Txt_sFRM_EDOC_NA = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rem_Blacklist)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 480);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(837, 22);
            this.statusStrip1.TabIndex = 121;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // Grd_Rem_Blacklist
            // 
            this.Grd_Rem_Blacklist.AllowUserToAddRows = false;
            this.Grd_Rem_Blacklist.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rem_Blacklist.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rem_Blacklist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Rem_Blacklist.ColumnHeadersHeight = 30;
            this.Grd_Rem_Blacklist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.Column18,
            this.Column9,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column8});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Rem_Blacklist.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Rem_Blacklist.Location = new System.Drawing.Point(13, 30);
            this.Grd_Rem_Blacklist.Name = "Grd_Rem_Blacklist";
            this.Grd_Rem_Blacklist.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rem_Blacklist.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Rem_Blacklist.RowHeadersWidth = 22;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rem_Blacklist.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Rem_Blacklist.Size = new System.Drawing.Size(814, 202);
            this.Grd_Rem_Blacklist.TabIndex = 6;
            this.Grd_Rem_Blacklist.SelectionChanged += new System.EventHandler(this.Grd_Rem_Blacklist_SelectionChanged);
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column4.DataPropertyName = "List_Name";
            this.Column4.HeaderText = "اسم القائمة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 84;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column1.DataPropertyName = "Name";
            this.Column1.HeaderText = "الاســـم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 66;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column18.DataPropertyName = "NAME_ORIGINAL_SCRIPT";
            this.Column18.HeaderText = "الاسم الاصلي";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 93;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column9.DataPropertyName = "Alias_Information";
            this.Column9.HeaderText = "اســم الشـــهــرة";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 107;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.DataPropertyName = "Title";
            this.Column2.HeaderText = "لـــقب";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 59;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.DataPropertyName = "Date_of_Birth";
            this.Column3.HeaderText = "تــاريخ الــولادة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 106;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column5.DataPropertyName = "Address";
            this.Column5.HeaderText = "العنــوان";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 71;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Nationality";
            this.Column8.HeaderText = "الجنــسيـة";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(8, 7);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(824, 1);
            this.flowLayoutPanel9.TabIndex = 185;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(8, 446);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(825, 1);
            this.flowLayoutPanel2.TabIndex = 667;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(8, 8);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1, 439);
            this.flowLayoutPanel3.TabIndex = 668;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(832, 8);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 439);
            this.flowLayoutPanel4.TabIndex = 669;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Btn_Add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Add.Location = new System.Drawing.Point(361, 451);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(115, 27);
            this.Btn_Add.TabIndex = 671;
            this.Btn_Add.Text = "تـــصــــديــر";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Btn_End
            // 
            this.Btn_End.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_End.ForeColor = System.Drawing.Color.Red;
            this.Btn_End.Location = new System.Drawing.Point(475, 451);
            this.Btn_End.Name = "Btn_End";
            this.Btn_End.Size = new System.Drawing.Size(115, 27);
            this.Btn_End.TabIndex = 672;
            this.Btn_End.Text = "لا اريد الاستمرار";
            this.Btn_End.UseVisualStyleBackColor = true;
            this.Btn_End.Click += new System.EventHandler(this.Btn_End_Click);
            // 
            // Btn_Ok
            // 
            this.Btn_Ok.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ok.ForeColor = System.Drawing.Color.Green;
            this.Btn_Ok.Location = new System.Drawing.Point(247, 451);
            this.Btn_Ok.Name = "Btn_Ok";
            this.Btn_Ok.Size = new System.Drawing.Size(115, 27);
            this.Btn_Ok.TabIndex = 673;
            this.Btn_Ok.Text = "اريد الاستمرار";
            this.Btn_Ok.UseVisualStyleBackColor = true;
            this.Btn_Ok.Click += new System.EventHandler(this.Btn_Ok_Click);
            // 
            // Txt_Notes_BL
            // 
            this.Txt_Notes_BL.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Notes_BL.Location = new System.Drawing.Point(14, 386);
            this.Txt_Notes_BL.Multiline = true;
            this.Txt_Notes_BL.Name = "Txt_Notes_BL";
            this.Txt_Notes_BL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_Notes_BL.Size = new System.Drawing.Size(814, 54);
            this.Txt_Notes_BL.TabIndex = 674;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(12, 368);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(146, 16);
            this.label12.TabIndex = 675;
            this.label12.Text = "ســـبــب  الاســــتــمـــرار........";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 380);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(823, 1);
            this.flowLayoutPanel1.TabIndex = 676;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(10, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 16);
            this.label1.TabIndex = 678;
            this.label1.Text = "معلومات اعدادات الحوالات :-\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(17, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 16);
            this.label2.TabIndex = 679;
            this.label2.Text = "معلومات قوائم المنع :-\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(137, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 680;
            this.label3.Text = "المرســـــل";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(532, 252);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 681;
            this.label4.Text = "المستلـــــــم";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_S_ACOUN_NAME
            // 
            this.Txt_S_ACOUN_NAME.BackColor = System.Drawing.Color.White;
            this.Txt_S_ACOUN_NAME.Location = new System.Drawing.Point(69, 277);
            this.Txt_S_ACOUN_NAME.MaxLength = 99;
            this.Txt_S_ACOUN_NAME.Name = "Txt_S_ACOUN_NAME";
            this.Txt_S_ACOUN_NAME.ReadOnly = true;
            this.Txt_S_ACOUN_NAME.Size = new System.Drawing.Size(215, 20);
            this.Txt_S_ACOUN_NAME.TabIndex = 682;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(118, 261);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 16);
            this.label5.TabIndex = 683;
            this.label5.Text = "ــــــــــــــــــــــــــــ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(514, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 16);
            this.label6.TabIndex = 684;
            this.label6.Text = "ــــــــــــــــــــــــــــ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(21, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 16);
            this.label7.TabIndex = 685;
            this.label7.Text = "البلـــــد :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Chk_S_ACOUN_NAME
            // 
            this.Chk_S_ACOUN_NAME.AutoSize = true;
            this.Chk_S_ACOUN_NAME.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_S_ACOUN_NAME.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_ACOUN_NAME.Location = new System.Drawing.Point(289, 279);
            this.Chk_S_ACOUN_NAME.Name = "Chk_S_ACOUN_NAME";
            this.Chk_S_ACOUN_NAME.Size = new System.Drawing.Size(17, 16);
            this.Chk_S_ACOUN_NAME.TabIndex = 686;
            this.Chk_S_ACOUN_NAME.Text = "*";
            this.Chk_S_ACOUN_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Chk_R_ACOUN_NAME
            // 
            this.Chk_R_ACOUN_NAME.AutoSize = true;
            this.Chk_R_ACOUN_NAME.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_R_ACOUN_NAME.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_ACOUN_NAME.Location = new System.Drawing.Point(700, 279);
            this.Chk_R_ACOUN_NAME.Name = "Chk_R_ACOUN_NAME";
            this.Chk_R_ACOUN_NAME.Size = new System.Drawing.Size(17, 16);
            this.Chk_R_ACOUN_NAME.TabIndex = 689;
            this.Chk_R_ACOUN_NAME.Text = "*";
            this.Chk_R_ACOUN_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(433, 279);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 16);
            this.label10.TabIndex = 688;
            this.label10.Text = "البلــــد :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_R_ACOUN_NAME
            // 
            this.Txt_R_ACOUN_NAME.BackColor = System.Drawing.Color.White;
            this.Txt_R_ACOUN_NAME.Location = new System.Drawing.Point(481, 277);
            this.Txt_R_ACOUN_NAME.MaxLength = 99;
            this.Txt_R_ACOUN_NAME.Name = "Txt_R_ACOUN_NAME";
            this.Txt_R_ACOUN_NAME.ReadOnly = true;
            this.Txt_R_ACOUN_NAME.Size = new System.Drawing.Size(215, 20);
            this.Txt_R_ACOUN_NAME.TabIndex = 687;
            // 
            // Chk_R_NAT_NAME
            // 
            this.Chk_R_NAT_NAME.AutoSize = true;
            this.Chk_R_NAT_NAME.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_R_NAT_NAME.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_NAT_NAME.Location = new System.Drawing.Point(700, 301);
            this.Chk_R_NAT_NAME.Name = "Chk_R_NAT_NAME";
            this.Chk_R_NAT_NAME.Size = new System.Drawing.Size(17, 16);
            this.Chk_R_NAT_NAME.TabIndex = 695;
            this.Chk_R_NAT_NAME.Text = "*";
            this.Chk_R_NAT_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_R_NAT_NAME
            // 
            this.Txt_R_NAT_NAME.BackColor = System.Drawing.Color.White;
            this.Txt_R_NAT_NAME.Location = new System.Drawing.Point(481, 299);
            this.Txt_R_NAT_NAME.MaxLength = 99;
            this.Txt_R_NAT_NAME.Name = "Txt_R_NAT_NAME";
            this.Txt_R_NAT_NAME.ReadOnly = true;
            this.Txt_R_NAT_NAME.Size = new System.Drawing.Size(215, 20);
            this.Txt_R_NAT_NAME.TabIndex = 693;
            // 
            // Chk_S_NAT_NAME
            // 
            this.Chk_S_NAT_NAME.AutoSize = true;
            this.Chk_S_NAT_NAME.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_S_NAT_NAME.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_NAT_NAME.Location = new System.Drawing.Point(289, 301);
            this.Chk_S_NAT_NAME.Name = "Chk_S_NAT_NAME";
            this.Chk_S_NAT_NAME.Size = new System.Drawing.Size(17, 16);
            this.Chk_S_NAT_NAME.TabIndex = 692;
            this.Chk_S_NAT_NAME.Text = "*";
            this.Chk_S_NAT_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(21, 301);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 16);
            this.label15.TabIndex = 691;
            this.label15.Text = "الجنسية:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_S_NAT_NAME
            // 
            this.Txt_S_NAT_NAME.BackColor = System.Drawing.Color.White;
            this.Txt_S_NAT_NAME.Location = new System.Drawing.Point(69, 299);
            this.Txt_S_NAT_NAME.MaxLength = 99;
            this.Txt_S_NAT_NAME.Name = "Txt_S_NAT_NAME";
            this.Txt_S_NAT_NAME.ReadOnly = true;
            this.Txt_S_NAT_NAME.Size = new System.Drawing.Size(215, 20);
            this.Txt_S_NAT_NAME.TabIndex = 690;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(432, 301);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 16);
            this.label16.TabIndex = 696;
            this.label16.Text = "الجنسية:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Chk_R_job
            // 
            this.Chk_R_job.AutoSize = true;
            this.Chk_R_job.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_R_job.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_job.Location = new System.Drawing.Point(700, 322);
            this.Chk_R_job.Name = "Chk_R_job";
            this.Chk_R_job.Size = new System.Drawing.Size(17, 16);
            this.Chk_R_job.TabIndex = 701;
            this.Chk_R_job.Text = "*";
            this.Chk_R_job.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_R_job
            // 
            this.Txt_R_job.BackColor = System.Drawing.Color.White;
            this.Txt_R_job.Location = new System.Drawing.Point(481, 320);
            this.Txt_R_job.MaxLength = 99;
            this.Txt_R_job.Name = "Txt_R_job";
            this.Txt_R_job.ReadOnly = true;
            this.Txt_R_job.Size = new System.Drawing.Size(215, 20);
            this.Txt_R_job.TabIndex = 700;
            // 
            // Chk_S_job
            // 
            this.Chk_S_job.AutoSize = true;
            this.Chk_S_job.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_S_job.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_job.Location = new System.Drawing.Point(289, 322);
            this.Chk_S_job.Name = "Chk_S_job";
            this.Chk_S_job.Size = new System.Drawing.Size(17, 16);
            this.Chk_S_job.TabIndex = 699;
            this.Chk_S_job.Text = "*";
            this.Chk_S_job.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(21, 322);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 16);
            this.label19.TabIndex = 698;
            this.label19.Text = "المهنـــة:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_S_job
            // 
            this.Txt_S_job.BackColor = System.Drawing.Color.White;
            this.Txt_S_job.Location = new System.Drawing.Point(69, 320);
            this.Txt_S_job.MaxLength = 99;
            this.Txt_S_job.Name = "Txt_S_job";
            this.Txt_S_job.ReadOnly = true;
            this.Txt_S_job.Size = new System.Drawing.Size(215, 20);
            this.Txt_S_job.TabIndex = 697;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(432, 322);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 16);
            this.label13.TabIndex = 702;
            this.label13.Text = "المهنـــة:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chk_RFRM_EDOC_NA
            // 
            this.chk_RFRM_EDOC_NA.AutoSize = true;
            this.chk_RFRM_EDOC_NA.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.chk_RFRM_EDOC_NA.ForeColor = System.Drawing.Color.Maroon;
            this.chk_RFRM_EDOC_NA.Location = new System.Drawing.Point(700, 345);
            this.chk_RFRM_EDOC_NA.Name = "chk_RFRM_EDOC_NA";
            this.chk_RFRM_EDOC_NA.Size = new System.Drawing.Size(17, 16);
            this.chk_RFRM_EDOC_NA.TabIndex = 707;
            this.chk_RFRM_EDOC_NA.Text = "*";
            this.chk_RFRM_EDOC_NA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_RFRM_EDOC_NA
            // 
            this.Txt_RFRM_EDOC_NA.BackColor = System.Drawing.Color.White;
            this.Txt_RFRM_EDOC_NA.Location = new System.Drawing.Point(481, 343);
            this.Txt_RFRM_EDOC_NA.MaxLength = 99;
            this.Txt_RFRM_EDOC_NA.Name = "Txt_RFRM_EDOC_NA";
            this.Txt_RFRM_EDOC_NA.ReadOnly = true;
            this.Txt_RFRM_EDOC_NA.Size = new System.Drawing.Size(215, 20);
            this.Txt_RFRM_EDOC_NA.TabIndex = 706;
            // 
            // Chk_sFRM_EDOC_NA
            // 
            this.Chk_sFRM_EDOC_NA.AutoSize = true;
            this.Chk_sFRM_EDOC_NA.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_sFRM_EDOC_NA.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_sFRM_EDOC_NA.Location = new System.Drawing.Point(289, 345);
            this.Chk_sFRM_EDOC_NA.Name = "Chk_sFRM_EDOC_NA";
            this.Chk_sFRM_EDOC_NA.Size = new System.Drawing.Size(17, 16);
            this.Chk_sFRM_EDOC_NA.TabIndex = 705;
            this.Chk_sFRM_EDOC_NA.Text = "*";
            this.Chk_sFRM_EDOC_NA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(21, 345);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 16);
            this.label23.TabIndex = 704;
            this.label23.Text = "الوثيقــة:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_sFRM_EDOC_NA
            // 
            this.Txt_sFRM_EDOC_NA.BackColor = System.Drawing.Color.White;
            this.Txt_sFRM_EDOC_NA.Location = new System.Drawing.Point(69, 343);
            this.Txt_sFRM_EDOC_NA.MaxLength = 99;
            this.Txt_sFRM_EDOC_NA.Name = "Txt_sFRM_EDOC_NA";
            this.Txt_sFRM_EDOC_NA.ReadOnly = true;
            this.Txt_sFRM_EDOC_NA.Size = new System.Drawing.Size(215, 20);
            this.Txt_sFRM_EDOC_NA.TabIndex = 703;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(432, 345);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 16);
            this.label20.TabIndex = 708;
            this.label20.Text = "الوثيقــة:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(8, 249);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(825, 1);
            this.flowLayoutPanel5.TabIndex = 186;
            // 
            // Rem_Blacklist_confirm_ok
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 502);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.chk_RFRM_EDOC_NA);
            this.Controls.Add(this.Txt_RFRM_EDOC_NA);
            this.Controls.Add(this.Chk_sFRM_EDOC_NA);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.Txt_sFRM_EDOC_NA);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Chk_R_job);
            this.Controls.Add(this.Txt_R_job);
            this.Controls.Add(this.Chk_S_job);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Txt_S_job);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Chk_R_NAT_NAME);
            this.Controls.Add(this.Txt_R_NAT_NAME);
            this.Controls.Add(this.Chk_S_NAT_NAME);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_S_NAT_NAME);
            this.Controls.Add(this.Chk_R_ACOUN_NAME);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_R_ACOUN_NAME);
            this.Controls.Add(this.Chk_S_ACOUN_NAME);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_S_ACOUN_NAME);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Txt_Notes_BL);
            this.Controls.Add(this.Btn_Ok);
            this.Controls.Add(this.Btn_End);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Grd_Rem_Blacklist);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Rem_Blacklist_confirm_ok";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "557";
            this.Text = "Blacklist & Reveal Rem";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Rem_Blacklist_OK_FormClosed);
            this.Load += new System.EventHandler(this.Rem_Blacklist_OK_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rem_Blacklist)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView Grd_Rem_Blacklist;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Button Btn_End;
        private System.Windows.Forms.Button Btn_Ok;
        private System.Windows.Forms.TextBox Txt_Notes_BL;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_S_ACOUN_NAME;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Chk_S_ACOUN_NAME;
        private System.Windows.Forms.Label Chk_R_ACOUN_NAME;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_R_ACOUN_NAME;
        private System.Windows.Forms.Label Chk_R_NAT_NAME;
        private System.Windows.Forms.TextBox Txt_R_NAT_NAME;
        private System.Windows.Forms.Label Chk_S_NAT_NAME;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txt_S_NAT_NAME;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label Chk_R_job;
        private System.Windows.Forms.TextBox Txt_R_job;
        private System.Windows.Forms.Label Chk_S_job;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Txt_S_job;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label chk_RFRM_EDOC_NA;
        private System.Windows.Forms.TextBox Txt_RFRM_EDOC_NA;
        private System.Windows.Forms.Label Chk_sFRM_EDOC_NA;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Txt_sFRM_EDOC_NA;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
    }
}