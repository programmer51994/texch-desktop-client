﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Correspondence_Sys_Confirm : Form
    {
        BindingSource binding_Grd1 = new BindingSource();
        BindingSource binding_Grd2 = new BindingSource();
        BindingSource binding_Grd3 = new BindingSource();
        BindingSource binding_Cbo = new BindingSource();
        bool Change_Grd1_details = false;
        bool Change_Grd2_details = false;
        bool Change_Bob = false;
        DataTable Voucher_Cur = new DataTable();
        string Rec_Date = "";
        DataTable DT_GRD1 = new DataTable();
        DataTable DT_rem = new DataTable();
        Int64 rem_id = 0;
        string rem_no = "";

        public Correspondence_Sys_Confirm()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_rem_view.AutoGenerateColumns = false;
            Grd_User_Deatils.AutoGenerateColumns = false;
            Grd_rem_vo.AutoGenerateColumns = false;
        }

        private void Remittances_Inquery_main_Load(object sender, EventArgs e)
        {

            if (connection.SQLDS.Tables["TBL_Remittances_Query"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "No Data fro this selection " : "لا توجد بيانات تحقق الشروط", MyGeneral_Lib.LblCap);
                this.Close();
            }
            Change_Grd1_details = false;
            binding_Grd1.DataSource = connection.SQLDS.Tables["TBL_Remittances_Query"];
            Grd_rem_view.DataSource = binding_Grd1;
            Change_Grd1_details = true;
            Grd_rem_view_SelectionChanged(null, null);

            Change_Bob = false;
            binding_Cbo.DataSource = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(true, "R_cur_id", "R_ACUR_NAME", "R_ECUR_NAME").Select().CopyToDataTable();
            Cbo_Rcur.DataSource = binding_Cbo;
            Cbo_Rcur.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
            Cbo_Rcur.ValueMember = "R_cur_id";
            Change_Bob = true;
            Cbo_Rcur_SelectedIndexChanged(null, null);
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "R_ECUR_NAME";
                Column4.DataPropertyName = "PR_ECUR_NAME";
                Column8.DataPropertyName = "t_ECITY_NAME";
                Column9.DataPropertyName = "S_ECITY_NAME";
                Column5.DataPropertyName = "ECASE_NA";
                Column29.DataPropertyName = "s_E_NAT_NAME";
                Column33.DataPropertyName = "r_ECITY_NAME";
                Column34.DataPropertyName = "r_ECOUN_NAME";
                Column54.DataPropertyName = "s_ECUR_NAME";
                Column56.DataPropertyName = "c_ECUR_NAME";
                Column13.DataPropertyName = "e_Send_rem_flag";
                Column10.DataPropertyName = "E_local_international";
                Column14.DataPropertyName = "customer_Ename";
                //dataGridViewTextBoxColumn2.DataPropertyName = "ECase_na";
                dataGridViewTextBoxColumn8.DataPropertyName = "Acc_EName";
                dataGridViewTextBoxColumn9.DataPropertyName = "Cur_ENAME";
                dataGridViewTextBoxColumn4.DataPropertyName = "threshold_eng";
            }
        }

        private void Grd_rem_view_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_Grd1_details)
            {

                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtSacity.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                TxtR_name.DataBindings.Clear();
                Txtr_phone.DataBindings.Clear();
                Txtr_nat.DataBindings.Clear();
                Txtracity.DataBindings.Clear();
                Txtr_address.DataBindings.Clear();


                TxtS_name.DataBindings.Add("Text", binding_Grd1, "S_name");
                TxtS_phone.DataBindings.Add("Text", binding_Grd1, "S_phone");
                Txts_nat.DataBindings.Add("Text", binding_Grd1, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                TxtSacity.DataBindings.Add("Text", binding_Grd1, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                TxtS_address.DataBindings.Add("Text", binding_Grd1, "S_address");
                TxtR_name.DataBindings.Add("Text", binding_Grd1, "r_name");
                Txtr_phone.DataBindings.Add("Text", binding_Grd1, "r_phone");
                Txtr_nat.DataBindings.Add("Text", binding_Grd1, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                Txtracity.DataBindings.Add("Text", binding_Grd1, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                Txtr_address.DataBindings.Add("Text", binding_Grd1, "r_address");
                Txt_Purpose.DataBindings.Add("Text", binding_Grd1, "T_purpose");


                DataTable DT_VOU = new DataTable();
                //  int vo_no = 0;
                //  int Nrec_Date = 0;
                //  int CASE_ID = 0;
                /// vo_no = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["vo_no"]);
                // DateTime   Nrec_Date1 = Convert.ToDateTime(((DataRowView)binding_Grd2.Current).Row["Nrec_Date1"]);
                //  Nrec_Date = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["Nrec_Date"]);
                //  CASE_ID = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["CASE_ID"]);

                rem_id = Convert.ToInt64(((DataRowView)binding_Grd1.Current).Row["rem_id"]);
                rem_no = ((DataRowView)binding_Grd1.Current).Row["rem_no"].ToString();
                DT_VOU = connection.SQLDS.Tables["TBL_Remittances_Query2"].DefaultView.ToTable().Select("Ref_id = " + rem_id).CopyToDataTable();
                binding_Grd3.DataSource = DT_VOU;
                Grd_rem_vo.DataSource = binding_Grd3;

                txt_RC_amount_deb.Text = DT_VOU.Compute("Sum(DFor_Amount)", "").ToString();
                txt_RC_amount_crd.Text = DT_VOU.Compute("Sum(CFor_Amount)", "").ToString();
                txt_LC_amount_deb.Text = DT_VOU.Compute("Sum(Dloc_Amount)", "").ToString();
                txt_LC_amount_crd.Text = DT_VOU.Compute("Sum(Cloc_Amount)", "").ToString();

                // Change_Grd2_details = false;

                //DataTable rem_case_dt = new DataTable();
                //rem_case_dt = connection.SQLDS.Tables["TBL_Remittances_Query1"].Select().CopyToDataTable();
                //   DataView dv1 = rem_case_dt.DefaultView;
                //  dv1.Sort = "Nrec_Date1  ASC";
                //   DataTable sortedDT = dv1.ToTable();
                // binding_Grd2.DataSource = rem_case_dt.DefaultView.ToTable(true, "vo_no", "Acase_Na", "Ecase_Na", "Nrec_Date1", "Nrec_Date", "User_Name", "Ref_id", "CASE_ID").Select("Ref_id = " + rem_id).CopyToDataTable();
                //  Grd_Rem_Cases.DataSource = binding_Grd2;
                //Change_Grd2_details = true;
                //Grd_Rem_Cases_SelectionChanged(null, null);

                try
                {
                    Grd_User_Deatils.DataSource = connection.SQLDS.Tables["TBL_Remittances_Query1"].DefaultView.ToTable().Select("rem_no like'" + rem_no + "'").CopyToDataTable();
                }
                catch
                { Grd_User_Deatils.DataSource = new BindingSource(); }
            }

        }

        //private void Grd_Rem_Cases_SelectionChanged(object sender, EventArgs e)
        //{
            //if (Change_Grd2_details)
            //{
            //    DataTable DT_VOU = new DataTable();
            //    int vo_no = 0;
            //    int Nrec_Date = 0;
            //    int CASE_ID = 0;
            //    vo_no = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["vo_no"]);
            //   // DateTime   Nrec_Date1 = Convert.ToDateTime(((DataRowView)binding_Grd2.Current).Row["Nrec_Date1"]);
            //    Nrec_Date = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["Nrec_Date"]);
            //    CASE_ID = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["CASE_ID"]);


            //    DT_VOU = connection.SQLDS.Tables["TBL_Remittances_Query1"].DefaultView.ToTable().Select("Ref_id = " + rem_id + "and Vo_no = " + vo_no + "and Nrec_Date = " + Nrec_Date + " and CASE_ID = " + CASE_ID).CopyToDataTable();
            //    binding_Grd3.DataSource = DT_VOU;
            //    Grd_rem_vo.DataSource = binding_Grd3;

            //    txt_RC_amount_deb.Text = DT_VOU.Compute("Sum(DFor_Amount)", "").ToString();
            //    txt_RC_amount_crd.Text = DT_VOU.Compute("Sum(CFor_Amount)", "").ToString();
            //    txt_LC_amount_deb.Text = DT_VOU.Compute("Sum(Dloc_Amount)", "").ToString();
            //    txt_LC_amount_crd.Text = DT_VOU.Compute("Sum(Cloc_Amount)", "").ToString();
            //}


     //   }

        private void Cbo_Rcur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change_Bob)
            {
                Decimal Amount = 0;
                DataTable DT = new DataTable();
                DT = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable().Select("r_Cur_id = " + Cbo_Rcur.SelectedValue).CopyToDataTable();
                Amount = Convert.ToDecimal(DT.Compute("Sum(R_Amount)", ""));
                Tot_Amount.Text = Amount.ToString();
                Txt_RCount.Text = DT.Rows.Count.ToString();
            }
        }

        private void Btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Print_All_Click(object sender, EventArgs e)
        {
            if (Grd_rem_view.Rows.Count > 0)
            {
                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["TBL_Remittances_Query"];
                DataGridView[] Export_GRD = { Grd_rem_view };
                DataTable[] Export_DT = { connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false,"Chk",connection.Lang_id==1?"a_local_international":"E_local_international",connection.Lang_id==1?"a_Send_rem_flag":"E_Send_rem_flag", "rem_no",connection.Lang_id==1?"customer_Aname" : "customer_Ename" ,"R_amount", 
              connection.Lang_id==1? "R_ACUR_NAME":"R_ECUR_NAME", connection.Lang_id==1?"PR_ACUR_NAME":"PR_ECUR_NAME", connection.Lang_id==1?"t_ACITY_NAME":"t_ECITY_NAME",connection.Lang_id==1?"S_ACITY_NAME":"S_ECITY_NAME",connection.Lang_id==1?"ACASE_NA":"ECASE_NA","Case_Date", "C_DATE","user_NAME","S_name","S_Ename","S_phone", "S_address","S_Street"
               ,"S_Suburb","S_Post_Code", "S_State","S_doc_no","S_doc_ida","S_doc_eda", "S_doc_issue","s_Email",connection.Lang_id==1?"s_A_NAT_NAME":"s_E_NAT_NAME","s_notes", "r_name",
               "R_phone",connection.Lang_id==1?"r_ACITY_NAME":"r_ECITY_NAME",connection.Lang_id==1?"r_ACOUN_NAME":"r_ECOUN_NAME", "r_address","r_Street","r_Suburb","r_Post_Code", "r_State","r_doc_no","r_doc_ida","r_doc_eda"
               , "r_doc_issue","r_Email","T_purpose","r_notes","In_rec_date","sbirth_date","rbirth_date","SBirth_Place","s_ocomm",connection.Lang_id==1?"s_ACUR_NAME":"s_ECUR_NAME"
               ,"c_ocomm",connection.Lang_id==1?"c_ACUR_NAME":"c_ECUR_NAME","rate_online","Source_money","Relation_S_R").Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Print_Details_Click(object sender, EventArgs e)
        {
            DataTable Dt = new DataTable();
            DataTable Dt_details = new DataTable();
            DataTable Export_DT_main = new DataTable();
            DataTable Export_DT_details = new DataTable();
            string rem_no = ((DataRowView)binding_Grd1.Current).Row["rem_no"].ToString();
            rem_id = Convert.ToInt64(((DataRowView)binding_Grd1.Current).Row["rem_id"]);



            DataGridView G = new DataGridView();
            G.Columns.Add("Column1", connection.Lang_id == 1 ? "نوع الحوالة" : "Remmitances type");
            G.Columns.Add("Column2", connection.Lang_id == 1 ? "رقم الحوالة" : "Remmitance NO");
             G.Columns.Add("Column3", connection.Lang_id == 1 ? " اسم الوكيل" : "Customer Name");
            G.Columns.Add("Column4", connection.Lang_id == 1 ? "مبلغ الحوالة" : "remittence amount");
            G.Columns.Add("Column5", connection.Lang_id == 1 ? "عملة الحوالة" : "remittence currency");
            G.Columns.Add("Column6", connection.Lang_id == 1 ? "عملة التسليم" : "Paying currency");
            G.Columns.Add("Column7", connection.Lang_id == 1 ? "مدينة التسليم" : "Paying city");
            G.Columns.Add("Column8", connection.Lang_id == 1 ? "مدينة الارسال" : "sending city");
            G.Columns.Add("Column9", connection.Lang_id == 1 ? "إسم المرسل" : "Sender name");
            G.Columns.Add("Column9", connection.Lang_id == 1 ? " إسم المرسل الاخر" : "Sender another name");
            G.Columns.Add("Column10", connection.Lang_id == 1 ? "هاتف المرسل" : "sender phone");
            G.Columns.Add("Column11", connection.Lang_id == 1 ? "عنوان المرسل" : "sender address");
            G.Columns.Add("Column12", connection.Lang_id == 1 ? "الزقاق" : "street");
            G.Columns.Add("Column13", connection.Lang_id == 1 ? "الحي" : "suburb");
            G.Columns.Add("Column14", connection.Lang_id == 1 ? "الرمز البريدي" : "Post NO");
            G.Columns.Add("Column15", connection.Lang_id == 1 ? "المحافظة" : "city");
            G.Columns.Add("Column16", connection.Lang_id == 1 ? "رقم هوية المرسل" : "Sender Document no.");
            G.Columns.Add("Column17", connection.Lang_id == 1 ? "تاريخ الاصدار" : "Issuing date");
            G.Columns.Add("Column18", connection.Lang_id == 1 ? "تاريخ الانتهاء" : "expire date");
            G.Columns.Add("Column19", connection.Lang_id == 1 ? "نوع الوثيقة" : "Document type");
            G.Columns.Add("Column20", connection.Lang_id == 1 ? "ايميل المرسل" : "Sender Email");
            G.Columns.Add("Column21", connection.Lang_id == 1 ? "جنسية المرسل" : "Sender nationality");
            G.Columns.Add("Column22", connection.Lang_id == 1 ? "ملاحظات المرسل" : "Sender notes");
            G.Columns.Add("Column23", connection.Lang_id == 1 ? "اسم المستلم" : "Reciver name");
            G.Columns.Add("Column24", connection.Lang_id == 1 ? "هاتف المستلم" : "Reciver phone");
            G.Columns.Add("Column25", connection.Lang_id == 1 ? "مدينة المستلم" : "Reciver city");
            G.Columns.Add("Column26", connection.Lang_id == 1 ? "بلد المستلم" : "Reciver country");
            G.Columns.Add("Column27", connection.Lang_id == 1 ? "العنوان" : "Address");
            G.Columns.Add("Column28", connection.Lang_id == 1 ? "الزقاق" : "Street");
            G.Columns.Add("Column29", connection.Lang_id == 1 ? "الحي" : "Suburb");
            G.Columns.Add("Column30", connection.Lang_id == 1 ? "الرمز البريدي" : "Post code");
            G.Columns.Add("Column31", connection.Lang_id == 1 ? "محافظة" : "city");
            G.Columns.Add("Column32", connection.Lang_id == 1 ? "رقم الوثيقة" : "Document NO");
            G.Columns.Add("Column33", connection.Lang_id == 1 ? "تاريخ الاصدار" : "issuing date");
            G.Columns.Add("Column34", connection.Lang_id == 1 ? "تاريخ الانتهاء" : "Expire date");
            G.Columns.Add("Column35", connection.Lang_id == 1 ? "نوع الوثيقة" : "Document type");
            G.Columns.Add("Column36", connection.Lang_id == 1 ? "ايميل المستلم" : "Reciver Email");
            G.Columns.Add("Column37", connection.Lang_id == 1 ? "غرض الارسال" : "Sending purpse");
            G.Columns.Add("Column38", connection.Lang_id == 1 ? "ملاحظات المستلم" : "Reciver notes");
            G.Columns.Add("Column39", connection.Lang_id == 1 ? "رقم السند" : "Bond NO");
            G.Columns.Add("Column40", connection.Lang_id == 1 ? "تولد مرسل" : "sender birthdate");
            G.Columns.Add("Column41", connection.Lang_id == 1 ? "تولد مستلم" : "Reciver birthdate");
            G.Columns.Add("Column42", connection.Lang_id == 1 ? "محل تولد المرسل" : "Sender birth place");
            G.Columns.Add("Column43", connection.Lang_id == 1 ? "عمولة مقبوضة من الزبون" : "Commission recived from customer");
            G.Columns.Add("Column44", connection.Lang_id == 1 ? "عملة العمولة المقبوضة من الزبون" : "Commission currency recived from customer");
            G.Columns.Add("Column45", connection.Lang_id == 1 ? "عمولة مركز الاون لاين" : "Online center commission");
            G.Columns.Add("Column46", connection.Lang_id == 1 ? "عملة عمولة مركز الاون لاين" : "Online center commission currency");
            G.Columns.Add("Column47", connection.Lang_id == 1 ? "سعر التعادل" : "Exchande rate");
            G.Columns.Add("Column48", connection.Lang_id == 1 ? "مصدر المال" : "Money source");
            G.Columns.Add("Column49", connection.Lang_id == 1 ? "علاقة المرسل والمستلم" : "sender and reciver relaithionship");



            DataGridView G1 = new DataGridView();
         //   G1.Columns.Add("Column1", connection.Lang_id == 1 ? "رقم السند" : "Bond NO");
            G1.Columns.Add("Column2", connection.Lang_id == 1 ? "حالة الحوالة" : "Remmitance Case");
            G1.Columns.Add("Column3", connection.Lang_id == 1 ? "تاريخ و وقت الانشاء" : "Issuing date and time");
            G1.Columns.Add("Column4", connection.Lang_id == 1 ? "المستخدم" : "User");
            G1.Columns.Add("Column3", connection.Lang_id == 1 ? "العملة الاصلية مدين" : "Debit original currency");
            G1.Columns.Add("Column4", connection.Lang_id == 1 ? "العملة الاصلية دائن" : "Credit original currency");
            G1.Columns.Add("Column5", connection.Lang_id == 1 ? "رقم الحساب" : "Accountant NO");
            G1.Columns.Add("Column6", connection.Lang_id == 1 ? "إسم الحساب" : "Accountant name");
            G1.Columns.Add("Column7", connection.Lang_id == 1 ? "العملة" : "Currency");
            G1.Columns.Add("Column8", connection.Lang_id == 1 ? "سعر الصرف" : "exchange rate");
            G1.Columns.Add("Column9", connection.Lang_id == 1 ? "السعر الفعلي" : "Real price");
            G1.Columns.Add("Column10", connection.Lang_id == 1 ? "العملة المحلية مدين" : "Dedit in local currency");
            G1.Columns.Add("Column11", connection.Lang_id == 1 ? "العملة المحلية دائن" : "credit in local currency");
            G1.Columns.Add("Column12", connection.Lang_id == 1 ? "وقت وتاريخ الانشاء" : "issuing date and time");



            Dt = connection.SQLDS.Tables["TBL_Remittances_Query"];
            Dt_details = connection.SQLDS.Tables["TBL_Remittances_Query2"].Select("Ref_id = " + rem_id).CopyToDataTable();
            Export_DT_main = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "a_local_international" : "E_local_international", "rem_no", connection.Lang_id == 1 ? "customer_Aname" : "customer_Ename", "R_amount",
                connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME", connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", "S_name", "S_Ename", "S_phone", "S_address", "S_Street"
                , "S_Suburb", "S_Post_Code", "S_State", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME", "s_notes", "r_name",
                "R_phone", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "r_doc_no", "r_doc_ida", "r_doc_eda"
                , "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "sbirth_date", "rbirth_date", "SBirth_Place", "s_ocomm", connection.Lang_id == 1 ? "s_ACUR_NAME" : "s_ECUR_NAME"
                , "c_ocomm", connection.Lang_id == 1 ? "c_ACUR_NAME" : "c_ECUR_NAME", "rate_online", "Source_money", "Relation_S_R").Select("rem_no = '" + rem_no + "'").CopyToDataTable();

            Export_DT_details = Dt_details.DefaultView.ToTable(false, connection.Lang_id == 1 ? "ACase_na" : "ECase_na", "Nrec_Date1", "User_name",
                "DFor_Amount", "CFor_Amount", "Acc_id", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Exch_Price", "REAL_PRICE", "Dloc_Amount",
            "Cloc_Amount", "C_date").Select().CopyToDataTable();


            //Export_DT_details.DefaultView.Sort = "vo_no ASC";

            DataView dv = Export_DT_details.DefaultView;
            dv.Sort = "C_date ,vo_no  ASC";
            DataTable sortedDT = dv.ToTable();



            DataGridView[] Export_GRD = { G, G1 };
            DataTable[] Export_DT = { Export_DT_main, sortedDT };
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void Remittances_Query_Vou_Details_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change_Grd1_details = false;
            Change_Grd2_details = false;
            Change_Bob = false;
            string[] Used_Tbl = { "TBL_Remittances_Query", "TBL_Remittances_Query1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
          //  DataTable DT_rem = new DataTable();
            try
            {
                DT_rem = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false, "Chk", "rem_no", "rem_id", "S_name", "S_EName").Select("Chk > 0").CopyToDataTable();

                if (DT_rem.Rows.Count > 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يرجى تأشير حوالة واحدة فقط" : " Please check One Remittance only", MyGeneral_Lib.LblCap);
                    return;
                }

            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يرجى التأشير اولا" : " Please check Remittance Fisrt", MyGeneral_Lib.LblCap);
                return;

            }
            try
            {
                connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
                Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

                if (connection.SQLDS.Tables.Contains("per_rem_blacklists_tbl"))
                {
                    connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
                }

                if (cond_bl_rem != 0)
                {
  
                    DataTable Per_blacklist_tbl = new DataTable();
                    string[] Column4 = { "Per_AName", "Per_EName" };
                    string[] DType4 = { "System.String", "System.String" };

                    Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                    Per_blacklist_tbl.Rows.Clear();

                    Per_blacklist_tbl.Rows.Add(DT_rem.Rows[0]["S_Name"], DT_rem.Rows[0]["S_EName"]);

                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                      //  connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)binding_Grd1.Current).Row["rem_no"].ToString());
                        connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)binding_Grd1.Current).Row["snat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", ((DataRowView)binding_Grd1.Current).Row["rnat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)binding_Grd1.Current).Row["sa_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", ((DataRowView)binding_Grd1.Current).Row["r_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@s_job", ((DataRowView)binding_Grd1.Current).Row["s_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_job", ((DataRowView)binding_Grd1.Current).Row["r_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", ((DataRowView)binding_Grd1.Current).Row["rFrm_doc_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)binding_Grd1.Current).Row["sFrm_doc_id"]); 

                    
                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                       
                    }
                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        
                    }

                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                    {
                        Rem_Blacklist_confirm_ok AddFrm = new Rem_Blacklist_confirm_ok();
                        AddFrm.ShowDialog(this);

                        if (Rem_Blacklist_confirm_ok.Back_btn == 1)// في حال لا اريد الاستمرار
                        {
                            Get_Black_list();
                            MessageBox.Show(connection.Lang_id == 1 ? "تم رفض الحوالة" : "Rimmetance has been refused", MyGeneral_Lib.LblCap);
                            Btn_Add.Enabled = true;
                            this.Close();
                            return;
                        }
                    }
                    else
                    {
                        Rem_Blacklist_confirm_ok.Back_btn = 0;
                        Rem_Blacklist_confirm_ok.Notes_BL = "";
                    }
                    
                }
            }
            catch
            {
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
            
          
            DT_rem = DT_rem.DefaultView.ToTable(false, "rem_no", "rem_id").Select().CopyToDataTable();


            connection.SQLCMD.Parameters.AddWithValue("@rem_buffer", DT_rem);
            connection.SQLCMD.Parameters.AddWithValue("@nrec_date", Convert.ToInt32(TxtIn_Rec_Date.Text));
            connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@chk_confirm", 1);
            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.AddWithValue("@type_rem", Correspondence_Sys_Sreach.Rem_type);
            connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL);
            connection.SqlExec("insert_buffer_rem", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();

            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
           

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable DT_rem = new DataTable();
            try
            {
                DT_rem = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false, "Chk", "rem_no", "rem_id").Select("Chk > 0").CopyToDataTable();
                DT_rem = DT_rem.DefaultView.ToTable(false, "rem_no", "rem_id").Select().CopyToDataTable();
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يرجى التأشير اولا" : " Please check Remittance Fisrt  ", MyGeneral_Lib.LblCap);
                return;

            }

            Notes_from AddFrm = new Notes_from();
            AddFrm.ShowDialog(this);

            connection.SQLCMD.Parameters.AddWithValue("@rem_buffer", DT_rem);
            connection.SQLCMD.Parameters.AddWithValue("@nrec_date", Convert.ToInt32(TxtIn_Rec_Date.Text));
            connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@chk_confirm", -1);
            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.AddWithValue("@type_rem", Correspondence_Sys_Sreach.Rem_type);
            connection.SQLCMD.Parameters.AddWithValue("@note_user", Notes_from.Notes_BL);
            connection.SqlExec("insert_buffer_rem", connection.SQLCMD);
            Notes_from.Notes_BL = "";
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();

            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
        }
        //-------------------------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            Correspondence_Sys_Refues_Reveal AddFrm = new Correspondence_Sys_Refues_Reveal();
            //this.Visible = false;
            AddFrm.ShowDialog(this);
           
           // this.Visible = true;
        }
        //--------------------------------------------------------------------
        private void label60_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (Grd_rem_view.RowCount > 0)
                {

                    label60.Enabled = false;
                    DataTable Per_blacklist_tbl = new DataTable();
                    string[] Column4 = { "Per_AName", "Per_EName" };
                    string[] DType4 = { "System.String", "System.String" };

                    Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                    Per_blacklist_tbl.Rows.Clear();

                    Per_blacklist_tbl.Rows.Add(((DataRowView)binding_Grd1.Current).Row["S_name"].ToString(), ((DataRowView)binding_Grd1.Current).Row["S_Ename"].ToString());

                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                       // connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)binding_Grd1.Current).Row["rem_no"].ToString());
                        connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)binding_Grd1.Current).Row["snat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", ((DataRowView)binding_Grd1.Current).Row["rnat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)binding_Grd1.Current).Row["sa_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", ((DataRowView)binding_Grd1.Current).Row["r_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@s_job", ((DataRowView)binding_Grd1.Current).Row["s_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_job", ((DataRowView)binding_Grd1.Current).Row["r_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", ((DataRowView)binding_Grd1.Current).Row["rFrm_doc_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id",((DataRowView)binding_Grd1.Current).Row["sFrm_doc_id"]); 

  
                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        label60.Enabled = true;
                    }
                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        label60.Enabled = true;
                    }

                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                    {
                        Rem_Blacklist_confirm AddFrm = new Rem_Blacklist_confirm();
                        AddFrm.ShowDialog(this);
                        string[] Str = { "per_rem_blacklist_tbl", "per_rem_blacklist_tbl1" };
                        foreach (string Tbl in Str)
                        {
                            if (connection.SQLDS.Tables.Contains(Tbl))
                                connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                        }

                    }
                    else
                    {
                        label60.Enabled = true;
                        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                        return;
                    }

                }
            }
            catch
            {
                label60.Enabled = true;
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
           
        }

        private void label61_Click(object sender, EventArgs e)
        {
            try
            {

                label61.Enabled = false;
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@name", ((DataRowView)binding_Grd1.Current).Row["S_name"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@S_doc_no", ((DataRowView)binding_Grd1.Current).Row["S_doc_no"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@S_Social_No", ((DataRowView)binding_Grd1.Current).Row["S_Social_No"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)binding_Grd1.Current).Row["sFrm_doc_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", "");
                connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", "");
                connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 2);
                connection.SQLCMD.Parameters.AddWithValue("@sbirth_date", ((DataRowView)binding_Grd1.Current).Row["sbirth_date"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", "");


                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                label61.Enabled = true;
                if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
                {

                    Rem_Count AddFrm = new Rem_Count(1);
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label61.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            catch
            {
                label61.Enabled = true;
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
        }
        //---------------------------------------------------------------------
        private void Get_Black_list()
        {
           try
           {
                DataTable Dt_ref = new DataTable();
                Dt_ref =  connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false, "Chk","oper_id", "rem_no", "S_name","r_cur_id", "vo_no","s_per_id" ,"r_amount").Select("Chk > 0").CopyToDataTable();
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Refuse_confirm_All]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@rem_no", Dt_ref.Rows[0]["rem_no"]);
                connection.SQLCMD.Parameters.AddWithValue("@case_id", 101);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@note_prog", "corspondence sys.");
                connection.SQLCMD.Parameters.AddWithValue("@oper_id", Dt_ref.Rows[0]["oper_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@vo_no", Dt_ref.Rows[0]["vo_no"]);
                connection.SQLCMD.Parameters.AddWithValue("@per_id", Dt_ref.Rows[0]["S_per_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", Dt_ref.Rows[0]["r_cur_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount", Dt_ref.Rows[0]["r_amount"]);
                connection.SQLCMD.Parameters.AddWithValue("@Per_Name", Dt_ref.Rows[0]["S_name"]);

                IDataReader obj = connection.SQLCMD.ExecuteReader();

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch
            {
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
        }
    }
}