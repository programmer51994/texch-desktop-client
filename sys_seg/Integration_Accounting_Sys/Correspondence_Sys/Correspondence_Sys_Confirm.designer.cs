﻿namespace Integration_Accounting_Sys 
{
    partial class Correspondence_Sys_Confirm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSacity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_PrintAll = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Btn_Print_Details = new System.Windows.Forms.Button();
            this.Grd_rem_view = new System.Windows.Forms.DataGridView();
            this.Column15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_rem_vo = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_RCount = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Cbo_Rcur = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.Txtracity = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txtr_nat = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtR_name = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Txtr_phone = new System.Windows.Forms.TextBox();
            this.Txtr_address = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.Grd_User_Deatils = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.txt_LC_amount_crd = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_LC_amount_deb = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_RC_amount_crd = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_RC_amount_deb = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Tot_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_rem_view)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_rem_vo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_User_Deatils)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(0, 16);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(179, 14);
            this.label2.TabIndex = 909;
            this.label2.Text = "معلومــــات الحوالـــة الانـــي....";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(7, 235);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(147, 14);
            this.label3.TabIndex = 910;
            this.label3.Text = "معلومات الحوالــــــــــــة...";
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.Color.White;
            this.TxtS_address.Location = new System.Drawing.Point(578, 279);
            this.TxtS_address.Multiline = true;
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(371, 25);
            this.TxtS_address.TabIndex = 917;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(491, 283);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(75, 16);
            this.label23.TabIndex = 924;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(5, 283);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 16);
            this.label19.TabIndex = 918;
            this.label19.Text = "هاتف المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.Color.White;
            this.TxtS_phone.Location = new System.Drawing.Point(82, 279);
            this.TxtS_phone.Multiline = true;
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(166, 25);
            this.TxtS_phone.TabIndex = 925;
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.Color.White;
            this.TxtS_name.Location = new System.Drawing.Point(82, 252);
            this.TxtS_name.Multiline = true;
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(554, 25);
            this.TxtS_name.TabIndex = 926;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(5, 256);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 927;
            this.label5.Text = "اسم المرسل:";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.Color.White;
            this.Txts_nat.Location = new System.Drawing.Point(724, 252);
            this.Txts_nat.Multiline = true;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(226, 25);
            this.Txts_nat.TabIndex = 930;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(640, 256);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 931;
            this.label7.Text = "جنسية المرسل:";
            // 
            // TxtSacity
            // 
            this.TxtSacity.BackColor = System.Drawing.Color.White;
            this.TxtSacity.Location = new System.Drawing.Point(329, 279);
            this.TxtSacity.Multiline = true;
            this.TxtSacity.Name = "TxtSacity";
            this.TxtSacity.ReadOnly = true;
            this.TxtSacity.Size = new System.Drawing.Size(164, 25);
            this.TxtSacity.TabIndex = 932;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(250, 283);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 933;
            this.label8.Text = "مدينة المرسل:";
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(125, 394);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(852, 1);
            this.flowLayoutPanel17.TabIndex = 945;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(4, 383);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(117, 14);
            this.label14.TabIndex = 944;
            this.label14.Text = "المعلومات المالية...";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.Color.White;
            this.Txt_Purpose.Location = new System.Drawing.Point(83, 358);
            this.Txt_Purpose.Multiline = true;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(867, 25);
            this.Txt_Purpose.TabIndex = 972;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(5, 362);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 973;
            this.label17.Text = "غرض التحويل:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(159, 245);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(818, 1);
            this.flowLayoutPanel5.TabIndex = 993;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(179, 27);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(798, 1);
            this.flowLayoutPanel6.TabIndex = 994;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 581);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(978, 1);
            this.flowLayoutPanel4.TabIndex = 986;
            // 
            // Btn_PrintAll
            // 
            this.Btn_PrintAll.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_PrintAll.ForeColor = System.Drawing.Color.Navy;
            this.Btn_PrintAll.Location = new System.Drawing.Point(321, 584);
            this.Btn_PrintAll.Name = "Btn_PrintAll";
            this.Btn_PrintAll.Size = new System.Drawing.Size(105, 29);
            this.Btn_PrintAll.TabIndex = 987;
            this.Btn_PrintAll.Text = "تصدير اجمالي";
            this.Btn_PrintAll.UseVisualStyleBackColor = true;
            this.Btn_PrintAll.Click += new System.EventHandler(this.Print_All_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(633, 584);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(105, 29);
            this.Btn_Ext.TabIndex = 989;
            this.Btn_Ext.Text = "انهـــــاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Exist_Click);
            // 
            // Btn_Print_Details
            // 
            this.Btn_Print_Details.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Print_Details.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Print_Details.Location = new System.Drawing.Point(425, 584);
            this.Btn_Print_Details.Name = "Btn_Print_Details";
            this.Btn_Print_Details.Size = new System.Drawing.Size(105, 29);
            this.Btn_Print_Details.TabIndex = 990;
            this.Btn_Print_Details.Text = "تصدير تفصيلي";
            this.Btn_Print_Details.UseVisualStyleBackColor = true;
            this.Btn_Print_Details.Click += new System.EventHandler(this.Print_Details_Click);
            // 
            // Grd_rem_view
            // 
            this.Grd_rem_view.AllowUserToAddRows = false;
            this.Grd_rem_view.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_rem_view.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_rem_view.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_rem_view.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_rem_view.ColumnHeadersHeight = 24;
            this.Grd_rem_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column10,
            this.Column13,
            this.Column3,
            this.Column14,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column8,
            this.Column9,
            this.Column5,
            this.Column6,
            this.Column11,
            this.Column19,
            this.Column7,
            this.Column48,
            this.Column12,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column40,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44,
            this.Column45,
            this.Column46,
            this.Column47,
            this.Column49,
            this.Column50,
            this.Column51,
            this.Column52,
            this.Column53,
            this.Column54,
            this.Column55,
            this.Column56,
            this.Column35,
            this.Column28,
            this.Column27});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_rem_view.DefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_rem_view.Location = new System.Drawing.Point(7, 34);
            this.Grd_rem_view.Name = "Grd_rem_view";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_rem_view.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_rem_view.RowHeadersVisible = false;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_rem_view.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_rem_view.Size = new System.Drawing.Size(964, 104);
            this.Grd_rem_view.TabIndex = 712;
            this.Grd_rem_view.VirtualMode = true;
            this.Grd_rem_view.SelectionChanged += new System.EventHandler(this.Grd_rem_view_SelectionChanged);
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Chk";
            this.Column15.HeaderText = "تأشير";
            this.Column15.Name = "Column15";
            this.Column15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column15.Width = 50;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column10.DataPropertyName = "a_local_international";
            this.Column10.HeaderText = "نوع الحوالة";
            this.Column10.Name = "Column10";
            this.Column10.Width = 85;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "a_Send_rem_flag";
            this.Column13.HeaderText = "حالة الحوالة";
            this.Column13.Name = "Column13";
            this.Column13.Width = 88;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحوالة";
            this.Column3.Name = "Column3";
            this.Column3.Width = 84;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "customer_Aname";
            this.Column14.HeaderText = "اسم الوكيل";
            this.Column14.Name = "Column14";
            this.Column14.Width = 84;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "R_amount";
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.Width = 88;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.Width = 88;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PR_ACUR_NAME";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.Width = 88;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "t_ACITY_NAME";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.Width = 96;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "S_ACITY_NAME";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.Width = 97;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.Width = 88;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Case_Date";
            dataGridViewCellStyle4.Format = "G";
            dataGridViewCellStyle4.NullValue = "dd/MM/yyyy";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.Width = 91;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "C_DATE";
            dataGridViewCellStyle5.NullValue = "dd\'/\'MM\'/\'yyyy";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column11.HeaderText = "تاريخ ووقت الانشاء";
            this.Column11.Name = "Column11";
            this.Column11.Width = 127;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "user_NAME";
            this.Column19.HeaderText = "المستخدم";
            this.Column19.Name = "Column19";
            this.Column19.Width = 76;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "اسم المرسل";
            this.Column7.Name = "Column7";
            this.Column7.Width = 87;
            // 
            // Column48
            // 
            this.Column48.DataPropertyName = "S_Ename";
            this.Column48.HeaderText = "الاسم الاخر";
            this.Column48.Name = "Column48";
            this.Column48.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "S_phone";
            this.Column12.HeaderText = "هاتف المرسل";
            this.Column12.Name = "Column12";
            this.Column12.Width = 97;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "S_address";
            this.Column16.HeaderText = "عنوان المرسل";
            this.Column16.Name = "Column16";
            this.Column16.Width = 97;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "S_Street";
            this.Column17.HeaderText = "الزقاق";
            this.Column17.Name = "Column17";
            this.Column17.Width = 64;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "S_Suburb";
            this.Column18.HeaderText = "الحي";
            this.Column18.Name = "Column18";
            this.Column18.Width = 57;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column20.DataPropertyName = "S_Post_Code";
            this.Column20.HeaderText = "الرقم البريدي";
            this.Column20.Name = "Column20";
            this.Column20.Width = 94;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column21.DataPropertyName = "S_State";
            this.Column21.HeaderText = "المحافظة";
            this.Column21.Name = "Column21";
            this.Column21.Width = 76;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column22.DataPropertyName = "S_doc_no";
            this.Column22.HeaderText = "رقم هوية المرسل";
            this.Column22.Name = "Column22";
            this.Column22.Width = 113;
            // 
            // Column23
            // 
            this.Column23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column23.DataPropertyName = "S_doc_ida";
            this.Column23.HeaderText = "تاريخ اصدار هوية المرسل";
            this.Column23.Name = "Column23";
            this.Column23.Width = 154;
            // 
            // Column24
            // 
            this.Column24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column24.DataPropertyName = "S_doc_eda";
            this.Column24.HeaderText = "تاريخ انتهاء هوية المرسل";
            this.Column24.Name = "Column24";
            this.Column24.Width = 154;
            // 
            // Column25
            // 
            this.Column25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column25.DataPropertyName = "S_doc_issue";
            this.Column25.HeaderText = "نوع الوثيقة للمرسل";
            this.Column25.Name = "Column25";
            this.Column25.Width = 121;
            // 
            // Column26
            // 
            this.Column26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column26.DataPropertyName = "s_Email";
            this.Column26.HeaderText = "ايميل المرسل";
            this.Column26.Name = "Column26";
            this.Column26.Width = 94;
            // 
            // Column29
            // 
            this.Column29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column29.DataPropertyName = "s_A_NAT_NAME";
            this.Column29.HeaderText = "جنسية المرسل";
            this.Column29.Name = "Column29";
            // 
            // Column30
            // 
            this.Column30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column30.DataPropertyName = "s_notes";
            this.Column30.HeaderText = "ملاحظات المرسل";
            this.Column30.Name = "Column30";
            this.Column30.Width = 113;
            // 
            // Column31
            // 
            this.Column31.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column31.DataPropertyName = "r_name";
            this.Column31.HeaderText = "اسم المستلم";
            this.Column31.Name = "Column31";
            this.Column31.Width = 86;
            // 
            // Column32
            // 
            this.Column32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column32.DataPropertyName = "R_phone";
            this.Column32.HeaderText = "هاتف المستلم";
            this.Column32.Name = "Column32";
            this.Column32.Width = 96;
            // 
            // Column33
            // 
            this.Column33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column33.DataPropertyName = "r_ACITY_NAME";
            this.Column33.HeaderText = "مدينة المستلم";
            this.Column33.Name = "Column33";
            this.Column33.Width = 93;
            // 
            // Column34
            // 
            this.Column34.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column34.DataPropertyName = "r_ACOUN_NAME";
            this.Column34.HeaderText = "بلد المستلم";
            this.Column34.Name = "Column34";
            this.Column34.Width = 81;
            // 
            // Column36
            // 
            this.Column36.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column36.DataPropertyName = "r_address";
            this.Column36.HeaderText = "عنوان المستلم";
            this.Column36.Name = "Column36";
            this.Column36.Width = 96;
            // 
            // Column37
            // 
            this.Column37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column37.DataPropertyName = "r_Street";
            this.Column37.HeaderText = "الزقاق";
            this.Column37.Name = "Column37";
            this.Column37.Width = 64;
            // 
            // Column38
            // 
            this.Column38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column38.DataPropertyName = "r_Suburb";
            this.Column38.HeaderText = "الحي للمستلم";
            this.Column38.Name = "Column38";
            this.Column38.Width = 93;
            // 
            // Column39
            // 
            this.Column39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column39.DataPropertyName = "r_Post_Code";
            this.Column39.HeaderText = "الرمز البريدي للمستلم";
            this.Column39.Name = "Column39";
            this.Column39.Width = 131;
            // 
            // Column40
            // 
            this.Column40.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column40.DataPropertyName = "r_State";
            this.Column40.HeaderText = "محافظة المستلم";
            this.Column40.Name = "Column40";
            this.Column40.Width = 106;
            // 
            // Column41
            // 
            this.Column41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column41.DataPropertyName = "r_doc_no";
            this.Column41.HeaderText = "رقم وثيقة المستلم";
            this.Column41.Name = "Column41";
            this.Column41.Width = 113;
            // 
            // Column42
            // 
            this.Column42.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column42.DataPropertyName = "r_doc_ida";
            this.Column42.HeaderText = "تاريخ اصدار المستلم";
            this.Column42.Name = "Column42";
            this.Column42.Width = 127;
            // 
            // Column43
            // 
            this.Column43.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column43.DataPropertyName = "r_doc_eda";
            this.Column43.HeaderText = "تاريخ انتهاء الوثيقة";
            this.Column43.Name = "Column43";
            this.Column43.Width = 124;
            // 
            // Column44
            // 
            this.Column44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column44.DataPropertyName = "r_doc_issue";
            this.Column44.HeaderText = "نوع الوثيقة";
            this.Column44.Name = "Column44";
            this.Column44.Width = 84;
            // 
            // Column45
            // 
            this.Column45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column45.DataPropertyName = "r_Email";
            this.Column45.HeaderText = "ايميل المستلم";
            this.Column45.Name = "Column45";
            this.Column45.Width = 93;
            // 
            // Column46
            // 
            this.Column46.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column46.DataPropertyName = "T_purpose";
            this.Column46.HeaderText = "غرض الارسال";
            this.Column46.Name = "Column46";
            this.Column46.Width = 101;
            // 
            // Column47
            // 
            this.Column47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column47.DataPropertyName = "r_notes";
            this.Column47.HeaderText = "ملاحظات المستلم";
            this.Column47.Name = "Column47";
            this.Column47.Width = 112;
            // 
            // Column49
            // 
            this.Column49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column49.DataPropertyName = "In_rec_date";
            this.Column49.HeaderText = "تاريخ السجلات";
            this.Column49.Name = "Column49";
            this.Column49.Width = 104;
            // 
            // Column50
            // 
            this.Column50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column50.DataPropertyName = "sbirth_date";
            this.Column50.HeaderText = "تولد المرسل";
            this.Column50.Name = "Column50";
            this.Column50.Width = 88;
            // 
            // Column51
            // 
            this.Column51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column51.DataPropertyName = "rbirth_date";
            this.Column51.HeaderText = "تولد المستلم";
            this.Column51.Name = "Column51";
            this.Column51.Width = 87;
            // 
            // Column52
            // 
            this.Column52.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column52.DataPropertyName = "SBirth_Place";
            this.Column52.HeaderText = "مكان تولد المرسل";
            this.Column52.Name = "Column52";
            this.Column52.Width = 117;
            // 
            // Column53
            // 
            this.Column53.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column53.DataPropertyName = "s_ocomm";
            this.Column53.HeaderText = "العمولة المقبوضة من الزبون";
            this.Column53.Name = "Column53";
            this.Column53.Width = 158;
            // 
            // Column54
            // 
            this.Column54.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column54.DataPropertyName = "s_ACUR_NAME";
            this.Column54.HeaderText = "عملة العمولة المقبوضة من الزبون";
            this.Column54.Name = "Column54";
            this.Column54.Width = 182;
            // 
            // Column55
            // 
            this.Column55.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column55.DataPropertyName = "c_ocomm";
            this.Column55.HeaderText = "عمولة مركز الاونلاين";
            this.Column55.Name = "Column55";
            this.Column55.Width = 131;
            // 
            // Column56
            // 
            this.Column56.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column56.DataPropertyName = "c_ACUR_NAME";
            this.Column56.HeaderText = "عملة عمولة مركز الاونلاين";
            this.Column56.Name = "Column56";
            this.Column56.Width = 155;
            // 
            // Column35
            // 
            this.Column35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column35.DataPropertyName = "rate_online";
            dataGridViewCellStyle6.Format = "N3";
            dataGridViewCellStyle6.NullValue = null;
            this.Column35.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column35.HeaderText = "سعر التعادل";
            this.Column35.Name = "Column35";
            this.Column35.Width = 90;
            // 
            // Column28
            // 
            this.Column28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column28.DataPropertyName = "Source_money";
            this.Column28.HeaderText = "مصدر المال";
            this.Column28.Name = "Column28";
            this.Column28.Width = 87;
            // 
            // Column27
            // 
            this.Column27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column27.DataPropertyName = "Relation_S_R";
            this.Column27.HeaderText = "علاقة المرسل بالمستلم";
            this.Column27.Name = "Column27";
            this.Column27.Width = 138;
            // 
            // Grd_rem_vo
            // 
            this.Grd_rem_vo.AllowUserToAddRows = false;
            this.Grd_rem_vo.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_rem_vo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_rem_vo.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_rem_vo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_rem_vo.ColumnHeadersHeight = 24;
            this.Grd_rem_vo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_rem_vo.DefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_rem_vo.Location = new System.Drawing.Point(7, 401);
            this.Grd_rem_vo.Name = "Grd_rem_vo";
            this.Grd_rem_vo.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_rem_vo.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.Grd_rem_vo.RowHeadersVisible = false;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_rem_vo.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_rem_vo.Size = new System.Drawing.Size(964, 125);
            this.Grd_rem_vo.TabIndex = 1120;
            this.Grd_rem_vo.VirtualMode = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "DFor_Amount";
            dataGridViewCellStyle12.Format = "N3";
            dataGridViewCellStyle12.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn5.HeaderText = "العملة الاصلية مدين";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 119;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "CFor_Amount";
            dataGridViewCellStyle13.Format = "N3";
            dataGridViewCellStyle13.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn6.HeaderText = "العملة الاصلية دائن";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 116;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Acc_id";
            this.dataGridViewTextBoxColumn7.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Acc_AName";
            this.dataGridViewTextBoxColumn8.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Cur_ANAME";
            this.dataGridViewTextBoxColumn9.HeaderText = "العملة";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 59;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Exch_Price";
            dataGridViewCellStyle14.Format = "N3";
            dataGridViewCellStyle14.NullValue = null;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn10.HeaderText = "سعر الصرف";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 89;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "REAL_PRICE";
            dataGridViewCellStyle15.Format = "N3";
            dataGridViewCellStyle15.NullValue = null;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn11.HeaderText = "السعر الفعلي";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 93;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Dloc_Amount";
            dataGridViewCellStyle16.Format = "N3";
            dataGridViewCellStyle16.NullValue = null;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn12.HeaderText = "العملة المحلية مدين";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 119;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Cloc_Amount";
            dataGridViewCellStyle17.Format = "N3";
            dataGridViewCellStyle17.NullValue = null;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn13.HeaderText = "العملة المحلية دائن";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 116;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(10, 534);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 14);
            this.label6.TabIndex = 1121;
            this.label6.Text = "مجموع مدين عملة اصلية:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(10, 559);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(149, 14);
            this.label10.TabIndex = 1123;
            this.label10.Text = "مجموع دائن عملة اصلية:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(634, 534);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 14);
            this.label11.TabIndex = 1125;
            this.label11.Text = "مجموع مدين عملة محلية:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(637, 559);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(150, 14);
            this.label12.TabIndex = 1127;
            this.label12.Text = "مجموع دائن عملة محلية:";
            // 
            // Txt_RCount
            // 
            this.Txt_RCount.BackColor = System.Drawing.Color.White;
            this.Txt_RCount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_RCount.Location = new System.Drawing.Point(802, 211);
            this.Txt_RCount.Name = "Txt_RCount";
            this.Txt_RCount.ReadOnly = true;
            this.Txt_RCount.Size = new System.Drawing.Size(157, 22);
            this.Txt_RCount.TabIndex = 1114;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(712, 215);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 14);
            this.label18.TabIndex = 1115;
            this.label18.Text = "عدد الحوالات:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(712, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 14);
            this.label9.TabIndex = 1116;
            this.label9.Text = "عملة الحوالة:";
            // 
            // Cbo_Rcur
            // 
            this.Cbo_Rcur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rcur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rcur.FormattingEnabled = true;
            this.Cbo_Rcur.Location = new System.Drawing.Point(802, 159);
            this.Cbo_Rcur.Name = "Cbo_Rcur";
            this.Cbo_Rcur.Size = new System.Drawing.Size(157, 22);
            this.Cbo_Rcur.TabIndex = 1117;
            this.Cbo_Rcur.SelectedIndexChanged += new System.EventHandler(this.Cbo_Rcur_SelectedIndexChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(706, 188);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(90, 14);
            this.label34.TabIndex = 1118;
            this.label34.Text = "مبالغ الحوالات:";
            // 
            // Txtracity
            // 
            this.Txtracity.BackColor = System.Drawing.Color.White;
            this.Txtracity.Location = new System.Drawing.Point(329, 332);
            this.Txtracity.Multiline = true;
            this.Txtracity.Name = "Txtracity";
            this.Txtracity.ReadOnly = true;
            this.Txtracity.Size = new System.Drawing.Size(164, 25);
            this.Txtracity.TabIndex = 1137;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(252, 336);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(71, 16);
            this.label13.TabIndex = 1138;
            this.label13.Text = "مدينة المستلم:";
            // 
            // Txtr_nat
            // 
            this.Txtr_nat.BackColor = System.Drawing.Color.White;
            this.Txtr_nat.Location = new System.Drawing.Point(726, 306);
            this.Txtr_nat.Multiline = true;
            this.Txtr_nat.Name = "Txtr_nat";
            this.Txtr_nat.ReadOnly = true;
            this.Txtr_nat.Size = new System.Drawing.Size(226, 25);
            this.Txtr_nat.TabIndex = 1135;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(642, 310);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(77, 16);
            this.label15.TabIndex = 1136;
            this.label15.Text = "جنسية المستلم:";
            // 
            // TxtR_name
            // 
            this.TxtR_name.BackColor = System.Drawing.Color.White;
            this.TxtR_name.Location = new System.Drawing.Point(82, 306);
            this.TxtR_name.Multiline = true;
            this.TxtR_name.Name = "TxtR_name";
            this.TxtR_name.ReadOnly = true;
            this.TxtR_name.Size = new System.Drawing.Size(554, 25);
            this.TxtR_name.TabIndex = 1133;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(5, 310);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(64, 16);
            this.label16.TabIndex = 1134;
            this.label16.Text = "اسم المستلم:";
            // 
            // Txtr_phone
            // 
            this.Txtr_phone.BackColor = System.Drawing.Color.White;
            this.Txtr_phone.Location = new System.Drawing.Point(82, 332);
            this.Txtr_phone.Multiline = true;
            this.Txtr_phone.Name = "Txtr_phone";
            this.Txtr_phone.ReadOnly = true;
            this.Txtr_phone.Size = new System.Drawing.Size(166, 25);
            this.Txtr_phone.TabIndex = 1132;
            // 
            // Txtr_address
            // 
            this.Txtr_address.BackColor = System.Drawing.Color.White;
            this.Txtr_address.Location = new System.Drawing.Point(580, 332);
            this.Txtr_address.Multiline = true;
            this.Txtr_address.Name = "Txtr_address";
            this.Txtr_address.ReadOnly = true;
            this.Txtr_address.Size = new System.Drawing.Size(371, 25);
            this.Txtr_address.TabIndex = 1129;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(493, 336);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(74, 16);
            this.label20.TabIndex = 1131;
            this.label20.Text = "عنوان المستلم:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(5, 336);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 16);
            this.label21.TabIndex = 1130;
            this.label21.Text = "هاتف المستلم:";
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(217, 584);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(105, 29);
            this.Btn_Add.TabIndex = 1139;
            this.Btn_Add.Text = "تأكيـــــد";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(529, 584);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 29);
            this.button1.TabIndex = 1140;
            this.button1.Text = "رفــــــض";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Maroon;
            this.label60.Location = new System.Drawing.Point(2, 140);
            this.label60.Name = "label60";
            this.label60.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label60.Size = new System.Drawing.Size(146, 14);
            this.label60.TabIndex = 1142;
            this.label60.Text = "قـــــوائـــــــــم الـــمــــنــــع";
            this.label60.Click += new System.EventHandler(this.label60_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Maroon;
            this.label61.Location = new System.Drawing.Point(265, 140);
            this.label61.Name = "label61";
            this.label61.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label61.Size = new System.Drawing.Size(168, 14);
            this.label61.TabIndex = 1141;
            this.label61.Text = "عـــدد الحــوالات الــمــرســلـة";
            this.label61.Click += new System.EventHandler(this.label61_Click);
            // 
            // Grd_User_Deatils
            // 
            this.Grd_User_Deatils.AllowUserToAddRows = false;
            this.Grd_User_Deatils.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_User_Deatils.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_User_Deatils.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_User_Deatils.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_User_Deatils.ColumnHeadersHeight = 24;
            this.Grd_User_Deatils.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn15});
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_User_Deatils.DefaultCellStyle = dataGridViewCellStyle24;
            this.Grd_User_Deatils.Location = new System.Drawing.Point(7, 158);
            this.Grd_User_Deatils.Name = "Grd_User_Deatils";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_User_Deatils.RowHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.Grd_User_Deatils.RowHeadersVisible = false;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_User_Deatils.RowsDefaultCellStyle = dataGridViewCellStyle26;
            this.Grd_User_Deatils.Size = new System.Drawing.Size(695, 74);
            this.Grd_User_Deatils.TabIndex = 1143;
            this.Grd_User_Deatils.VirtualMode = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn14.DataPropertyName = "note_user";
            dataGridViewCellStyle23.Format = "N3";
            dataGridViewCellStyle23.NullValue = null;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn14.HeaderText = "ملاحظات المستخدم";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 122;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "threshold";
            this.dataGridViewTextBoxColumn4.HeaderText = "نسبة التشابه";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 91;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "c_date";
            this.dataGridViewTextBoxColumn15.HeaderText = "تاريخ الحوالة";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 95;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(743, 6);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 1146;
            this.label4.Text = "التاريــــــخ:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(529, 2);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(199, 23);
            this.TxtUser.TabIndex = 1145;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(430, 6);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 1144;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(185, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(241, 23);
            this.TxtTerm_Name.TabIndex = 1148;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(839, 2);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(132, 22);
            this.TxtIn_Rec_Date.TabIndex = 1147;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txt_LC_amount_crd
            // 
            this.txt_LC_amount_crd.BackColor = System.Drawing.Color.White;
            this.txt_LC_amount_crd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LC_amount_crd.Location = new System.Drawing.Point(789, 555);
            this.txt_LC_amount_crd.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_LC_amount_crd.Name = "txt_LC_amount_crd";
            this.txt_LC_amount_crd.NumberDecimalDigits = 3;
            this.txt_LC_amount_crd.NumberDecimalSeparator = ".";
            this.txt_LC_amount_crd.NumberGroupSeparator = ",";
            this.txt_LC_amount_crd.ReadOnly = true;
            this.txt_LC_amount_crd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_LC_amount_crd.Size = new System.Drawing.Size(159, 23);
            this.txt_LC_amount_crd.TabIndex = 1128;
            this.txt_LC_amount_crd.Text = "0.000";
            // 
            // txt_LC_amount_deb
            // 
            this.txt_LC_amount_deb.BackColor = System.Drawing.Color.White;
            this.txt_LC_amount_deb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LC_amount_deb.Location = new System.Drawing.Point(789, 530);
            this.txt_LC_amount_deb.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_LC_amount_deb.Name = "txt_LC_amount_deb";
            this.txt_LC_amount_deb.NumberDecimalDigits = 3;
            this.txt_LC_amount_deb.NumberDecimalSeparator = ".";
            this.txt_LC_amount_deb.NumberGroupSeparator = ",";
            this.txt_LC_amount_deb.ReadOnly = true;
            this.txt_LC_amount_deb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_LC_amount_deb.Size = new System.Drawing.Size(159, 23);
            this.txt_LC_amount_deb.TabIndex = 1126;
            this.txt_LC_amount_deb.Text = "0.000";
            // 
            // txt_RC_amount_crd
            // 
            this.txt_RC_amount_crd.BackColor = System.Drawing.Color.White;
            this.txt_RC_amount_crd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RC_amount_crd.Location = new System.Drawing.Point(167, 555);
            this.txt_RC_amount_crd.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_RC_amount_crd.Name = "txt_RC_amount_crd";
            this.txt_RC_amount_crd.NumberDecimalDigits = 3;
            this.txt_RC_amount_crd.NumberDecimalSeparator = ".";
            this.txt_RC_amount_crd.NumberGroupSeparator = ",";
            this.txt_RC_amount_crd.ReadOnly = true;
            this.txt_RC_amount_crd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_RC_amount_crd.Size = new System.Drawing.Size(162, 23);
            this.txt_RC_amount_crd.TabIndex = 1124;
            this.txt_RC_amount_crd.Text = "0.000";
            // 
            // txt_RC_amount_deb
            // 
            this.txt_RC_amount_deb.BackColor = System.Drawing.Color.White;
            this.txt_RC_amount_deb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RC_amount_deb.Location = new System.Drawing.Point(168, 530);
            this.txt_RC_amount_deb.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_RC_amount_deb.Name = "txt_RC_amount_deb";
            this.txt_RC_amount_deb.NumberDecimalDigits = 3;
            this.txt_RC_amount_deb.NumberDecimalSeparator = ".";
            this.txt_RC_amount_deb.NumberGroupSeparator = ",";
            this.txt_RC_amount_deb.ReadOnly = true;
            this.txt_RC_amount_deb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_RC_amount_deb.Size = new System.Drawing.Size(161, 23);
            this.txt_RC_amount_deb.TabIndex = 1122;
            this.txt_RC_amount_deb.Text = "0.000";
            // 
            // Tot_Amount
            // 
            this.Tot_Amount.BackColor = System.Drawing.Color.White;
            this.Tot_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount.Location = new System.Drawing.Point(802, 184);
            this.Tot_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount.Name = "Tot_Amount";
            this.Tot_Amount.NumberDecimalDigits = 3;
            this.Tot_Amount.NumberDecimalSeparator = ".";
            this.Tot_Amount.NumberGroupSeparator = ",";
            this.Tot_Amount.ReadOnly = true;
            this.Tot_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Tot_Amount.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount.TabIndex = 1119;
            this.Tot_Amount.Text = "0.000";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(854, 582);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 31);
            this.button2.TabIndex = 1149;
            this.button2.Text = "كشف الحوالات المرفوضة";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Correspondence_Sys_Confirm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 616);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Grd_User_Deatils);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Txtracity);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txtr_nat);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtR_name);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txtr_phone);
            this.Controls.Add(this.Txtr_address);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txt_LC_amount_crd);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txt_LC_amount_deb);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_RC_amount_crd);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txt_RC_amount_deb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Grd_rem_vo);
            this.Controls.Add(this.Tot_Amount);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.Cbo_Rcur);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Txt_RCount);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Btn_Print_Details);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.Btn_PrintAll);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TxtSacity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.TxtS_address);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Grd_rem_view);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Correspondence_Sys_Confirm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "544";
            this.Text = "تأكيد الحوالات الصادرة";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Remittances_Query_Vou_Details_FormClosed);
            this.Load += new System.EventHandler(this.Remittances_Inquery_main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_rem_view)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_rem_vo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_User_Deatils)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtSacity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button Btn_PrintAll;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.Button Btn_Print_Details;
        private System.Windows.Forms.DataGridView Grd_rem_view;
        private System.Windows.Forms.DataGridView Grd_rem_vo;
        private System.Windows.Forms.Sample.DecimalTextBox txt_RC_amount_deb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Sample.DecimalTextBox txt_RC_amount_crd;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Sample.DecimalTextBox txt_LC_amount_deb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Sample.DecimalTextBox txt_LC_amount_crd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_RCount;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Cbo_Rcur;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.TextBox Txtracity;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txtr_nat;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TxtR_name;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txtr_phone;
        private System.Windows.Forms.TextBox Txtr_address;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.DataGridView Grd_User_Deatils;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column50;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column51;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column52;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column53;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column54;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column55;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column56;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
    }
}