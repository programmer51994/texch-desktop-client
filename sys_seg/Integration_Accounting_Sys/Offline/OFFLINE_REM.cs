﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class OFFLINE_REM : Form
    {
        string @format = "dd/MM/yyyy";
        string @format_null = "";
        string S_doc_exp_null = "0000/00/00";
        string Sql_Txt = "";
        bool chang_scity = false;
        bool chang_rcity = false;
        bool chang_sender = false;
        bool chang_rec = false;
        bool chang_cur_rrate = false;
        bool chang_cur_crate = false;
        decimal param_Exch_rate_rem = 0;
        decimal param_Exch_rate_com = 0;
        string _Date = "";
        string rem_no = "";
        string local_Cur = "";
        string forgin_Cur = "";
        int Oper_Id = 0;
        int chk_tran_flag = 0;
        string Vo_No = "";
        string User = "";
        string Exch_rate_com = "";
        double Rem_Amount = 0;
        string term = "";
        string SqlTxt1 = "";
        public static Int16 Del_Btn = 0;
        int cust_id_online = 0;
        public static string Remmitance_Amount = "";
        public static string Remmitance_Com = "";
        public static string Remmitance_cur = "";
        public static string out_rem_cur = "";
        public static string com_cur = "";
        //----
        DataTable Dt_tcity_id = new DataTable();
        DataTable Dt_rcur_rem = new DataTable();
        DataTable Dt_pcur_rem = new DataTable();
        DataTable Dt_rcur_comm = new DataTable();
        DataTable Dt_comm = new DataTable();
        DataTable Dt_cur_comm = new DataTable();
        DataTable buy_sale_cur = new DataTable();
        //---------
        BindingSource binding_cbo_scity_con = new BindingSource();
        BindingSource binding_cb_snat = new BindingSource();
        BindingSource binding_sfmd = new BindingSource();
        BindingSource binding_scode_phone = new BindingSource();
        BindingSource binding_Code_phone_S = new BindingSource();
        BindingSource binding_Code_phone_R = new BindingSource();

        //-----
        BindingSource binding_cbo_rcity_con = new BindingSource();
        BindingSource binding_cb_rnat = new BindingSource();
        BindingSource binding_rfmd = new BindingSource();
        BindingSource binding_rcode_phone = new BindingSource();
        //--------
        BindingSource binding_cbo_city = new BindingSource();
        BindingSource binding_Gender_id = new BindingSource();
        BindingSource binding_Cmb_Case_Purpose = new BindingSource();
        BindingSource binding_Grd_cust_sender = new BindingSource();
        BindingSource binding_Grd_cust_Reciever = new BindingSource();
        //----
        BindingSource binding_cur_bsrem = new BindingSource();
        BindingSource binding_cur_bscomm = new BindingSource();
        //-----
        BindingSource binding_rcur = new BindingSource();
        BindingSource binding_pcur = new BindingSource();
        BindingSource binding_tcity_id = new BindingSource();
        BindingSource cur_comm = new BindingSource();
        BindingSource _BS_cust = new BindingSource();
        decimal Mdiff_Amoun = 0;
        BindingSource _BS_sub_cust = new BindingSource();
        BindingSource _BS_sub_agent = new BindingSource();
        BindingSource _BS_curr = new BindingSource();
        bool chang_agent = false;
        string SqlTxt_sub = "";
        string SqlTxt_agent = "";
        BindingSource _BS_sub_agent2 = new BindingSource();
        int x = 0;
        String Txt_cur_rem = "";
        bool chang_proc = false;
        bool change_Cbo_agent = false;
        bool Grd_agent_Change = false;
        bool TXt_agent_Change = false;
        bool change_txt_procer = false;
        bool change_Cur_REM = false;
        Int16 btnlang = 1;
        Int16 btnlang_rec = 1;
        DataTable DT_Box_Cur = new DataTable();
        BindingSource binding_cmb_resd = new BindingSource();
        BindingSource binding_cmb_resd_rec = new BindingSource();
        BindingSource binding_cmb_job_sender = new BindingSource();
        BindingSource binding_cmb_job_receiver = new BindingSource();
        string s_Ejob = "";
        string r_Ejob = "";
        string S_Social_No = "";
        string R_Social_No = "";
        //   Int64 Comm_Rem_ID = 0;
        BindingSource binding_type_INOUT = new BindingSource();
        //-------------------------
        public OFFLINE_REM()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_CustSen_Name.AutoGenerateColumns = false;
            Grd_CustRec_Name.AutoGenerateColumns = false;
            Grd_agent.AutoGenerateColumns = false;
            Grd_procer.AutoGenerateColumns = false;
        }
        //------------------------------------------------------------------------
        private void Add_rem_Load(object sender, EventArgs e)
        {
            Txt_Doc_S_Exp.Format = DateTimePickerFormat.Custom;
            Txt_Doc_S_Exp.CustomFormat = @format;

            Txt_Sbirth_Date.Format = DateTimePickerFormat.Custom;
            Txt_Sbirth_Date.CustomFormat = @format;

            Txt_Doc_S_Date.Format = DateTimePickerFormat.Custom;
            Txt_Doc_S_Date.CustomFormat = @format;

            ENAR_BTN.Enabled = false;
            en_rec_btn.Enabled = false;
            ar_sen.BackColor = System.Drawing.Color.LightGreen;
            ar_rec_btn.BackColor = System.Drawing.Color.LightGreen;
            label5.Text = "";
            label8.Text = "";

            string SqlTxt = " Select  CUST_ID From  TERMINALS where t_id = " + connection.T_ID;
            connection.SqlExec(SqlTxt, "Cust_tbl");


            cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);



            SqlTxt1 = " exec Full_information_Web_offline " + connection.Lang_id + "," + 0 + "," + cust_id_online;
            connection.SqlExec(SqlTxt1, "Full_information_tab");


            //-----مدينة الاصدار

            connection.SqlExec("exec Get_information_inrem " + cust_id_online, "Get_info_Tbl");
            //binding_cbo_tcity.DataSource = connection.SQLDS.Tables["Get_info_Tbl"];
            //Cbo_city.DataSource = binding_cbo_tcity.DataSource;
            //Cbo_city.ValueMember = "CITY_ID";
            //Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";

            binding_cbo_city.DataSource = connection.SQLDS.Tables["Get_info_Tbl"].Select("CITY_ID <>" + 0).CopyToDataTable();
            Cbo_city.DataSource = binding_cbo_city;
            Cbo_city.ValueMember = "CITY_ID";
            Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "eCity_Name";
            //----اسباب التحويل
            binding_Cmb_Case_Purpose.DataSource = connection.SQLDS.Tables["Full_information_tab5"];
            Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
            Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
            Cmb_Case_Purpose.DisplayMember = connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename";
            //----مدينة الاستلام
            binding_tcity_id.DataSource = connection.SQLDS.Tables["Full_information_tab"];

            Cmb_T_City.DataSource = binding_tcity_id;
            Cmb_T_City.ValueMember = "Cit_ID";
            Cmb_T_City.DisplayMember = connection.Lang_id == 1 ? "acity_name" : "ecity_name";
            //----عملة الاصدار
            //Dt_rcur_rem = MicrosoftOfficeInterop.OneNote.SQLDS.Tables["Sub_cust_tbl2"].DefaultView.ToTable(true, "ACur_Name_rem", "eCur_Name_rem", "R_CUR_ID").Select("").CopyToDataTable();
            //binding_rcur.DataSource = connection.SQLDS.Tables["Full_information_tab4"];

            //Cmb_R_CUR_ID.DataSource = binding_rcur;
            //Cmb_R_CUR_ID.ValueMember = "cur_id";
            //Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
            //--------عملة الدفع
            // Dt_pcur_rem = MicrosoftOfficeInterop.OneNote.SQLDS.Tables["Sub_cust_tbl2"].DefaultView.ToTable(true, "ACur_Name_pay", "eCur_Name_pay", "pR_CUR_ID").Select("").CopyToDataTable();
            binding_pcur.DataSource = connection.SQLDS.Tables["Full_information_tab4"];

            Cmb_PR_Cur_Id.DataSource = binding_pcur;
            Cmb_PR_Cur_Id.ValueMember = "cur_id";
            Cmb_PR_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
            //-------






            //----احضار الجداول الثابتة 


            if (connection.SQLDS.Tables["Full_information_tab"].Rows.Count > 0)
            {
                chang_scity = false;
                chang_rcity = false;
                chang_agent = false;

                binding_cbo_scity_con.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                Cmb_S_City.DataSource = binding_cbo_scity_con;
                Cmb_S_City.ValueMember = "Cit_ID";
                Cmb_S_City.DisplayMember = "ACity_Name";
                binding_Code_phone_S.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                Cmb_Code_phone_S.DataSource = binding_Code_phone_S;
                Cmb_Code_phone_S.ValueMember = "COUN_K_PH";
                Cmb_Code_phone_S.DisplayMember = "COUN_K_PH";

                chang_scity = true;
                Cmb_S_City_SelectedIndexChanged(null, null);
                //-----------------------
                binding_cbo_rcity_con.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                Cmb_R_City.DataSource = binding_cbo_rcity_con;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = "ACity_Name";

                binding_Code_phone_R.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                Cmb_phone_Code_R.DataSource = binding_Code_phone_R;
                Cmb_phone_Code_R.ValueMember = "COUN_K_PH";
                Cmb_phone_Code_R.DisplayMember = "COUN_K_PH";
                chang_rcity = true;
                Cmb_R_City_SelectedIndexChanged(null, null);
            }
            if (connection.SQLDS.Tables["Full_information_tab1"].Rows.Count > 0)
            {
                binding_sfmd.DataSource = connection.SQLDS.Tables["Full_information_tab1"];
                Cmb_S_Doc_Type.DataSource = binding_sfmd;
                Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_S_Doc_Type.DisplayMember = "Fmd_AName";
            }
            if (connection.SQLDS.Tables["Full_information_tab2"].Rows.Count > 0)
            {
                binding_cb_snat.DataSource = connection.SQLDS.Tables["Full_information_tab2"];
                cmb_s_nat.DataSource = binding_cb_snat;
                cmb_s_nat.ValueMember = "Nat_ID";
                cmb_s_nat.DisplayMember = "Nat_AName";
                ///------------------
                binding_cb_rnat.DataSource = connection.SQLDS.Tables["Full_information_tab2"];
                Cmb_R_Nat.DataSource = binding_cb_rnat;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = "Nat_AName";
            }

            if (connection.SQLDS.Tables["Full_information_tab3"].Rows.Count > 0)
            {
                binding_Gender_id.DataSource = connection.SQLDS.Tables["Full_information_tab3"];
                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = "Gender_Aname";
            }
            //---------------------------------------------------------

            binding_cmb_resd.DataSource = connection.SQLDS.Tables["Full_information_tab8"];

            resd_cmb.DataSource = binding_cmb_resd;
            resd_cmb.ValueMember = "resd_flag";
            resd_cmb.DisplayMember = connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename";

            //----------------------------------------------------

            binding_cmb_resd_rec.DataSource = connection.SQLDS.Tables["Full_information_tab8"];

            resd_cbm_rec.DataSource = binding_cmb_resd_rec;
            resd_cbm_rec.ValueMember = "resd_flag";
            resd_cbm_rec.DisplayMember = connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename";

            //------------------------------------------------------
            binding_cmb_job_sender.DataSource = connection.SQLDS.Tables["Full_information_tab9"];

            cmb_job_sender.DataSource = binding_cmb_job_sender;
            cmb_job_sender.ValueMember = "Job_ID";
            cmb_job_sender.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";

            //---------------------------------------------
            binding_cmb_job_receiver.DataSource = connection.SQLDS.Tables["Full_information_tab9"];

            cmb_job_receiver.DataSource = binding_cmb_job_receiver;
            cmb_job_receiver.ValueMember = "Job_ID";
            cmb_job_receiver.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";

            binding_type_INOUT.DataSource = connection.SQLDS.Tables["Full_information_tab11"].Select("type_rem_ID <> -1").CopyToDataTable();
            cmb_comm_type.DataSource = binding_type_INOUT;
            cmb_comm_type.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            cmb_comm_type.ValueMember = "type_rem_ID";



            //-----
            // resd_cmb.SelectedIndex = 0;
            //  resd_cbm_rec.SelectedIndex = 0;
            Cbo_agent_comm_type.SelectedIndex = 0;
            cmb_comm_type.SelectedIndex = 0;
            Cbo_type.SelectedIndex = 0;
            TYPE_CBO_OUT_IN.SelectedIndex = 0;



        }
        //-------------------------------------------------
        private void get_Box_Cur() //جلب عملات الصندوق 
        {
            if (TxtBox_User.Text != "")
            {
                Txt_cur_rem = " SELECT  B.cur_id,Cur_aNAME  as ACur_Name,Cur_Aname,Cur_Ename,Cur_eNAME  as ECur_Name "
                         + " from Cur_Tbl  A, CUSTOMERS_ACCOUNTS B "
                         + " where A.cur_id =  B.cur_id "
                         + " And  CUST_ID =  " + TxtBox_User.Tag
                         + " And T_id =  " + connection.T_ID;

                DT_Box_Cur = connection.SqlExec(Txt_cur_rem, "TbL_cur_rem");

                binding_rcur.DataSource = DT_Box_Cur;
                change_Cur_REM = false;
                Cmb_R_CUR_ID.DataSource = binding_rcur;
                Cmb_R_CUR_ID.ValueMember = "cur_id";
                Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                change_Cur_REM = true;

                cur_comm.DataSource = DT_Box_Cur;
                Cmb_Comm_Cur.DataSource = cur_comm;
                Cmb_Comm_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                Cmb_Comm_Cur.ValueMember = "cur_id";
            }
        }
        //-------------------------------------------------
        private void get_comm()
        {
            checkBox1.Checked = false;
            chang_cur_rrate = false;
            chang_cur_crate = false;
            cmb_cur.DataSource = new DataTable();
            cmb_cur_comm.DataSource = new DataTable();
            Txt_ExRate_Rem.ResetText();
            txt_maxrate_rem.ResetText();
            txt_minrate_rem.ResetText();
            Txt_ExRate_comm.ResetText();
            txt_maxrate_comm.ResetText();
            txt_minrate_comm.ResetText();
            Txt_Rem_Amount.ResetText();
            Txt_Com_Amnt.ResetText();
            Txt_Tot_amount.ResetText();
            //if (Cbo_type.SelectedIndex == 1)
            //{
            //    TxtDiscount_Amount.ResetText();
            //}

        }
        //------------------------------------------------------------------------
        private void Txt_Rem_Amount_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            { cmb_cur_SelectedIndexChanged(null, null); }
        }
        //------------------------------------------------------------------------
        private void Txt_Com_Amnt_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            { cmb_cur_comm_SelectedIndexChanged_1(null, null); }
        }
        //------------------------------------------------------------------------
        private void Cmb_S_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_scity)
            {
                Cmb_Code_phone_S.DataBindings.Clear();
                Cmb_Code_phone_S.DataBindings.Add("SelectedValue", binding_cbo_scity_con, "COUN_K_PH");
            }
        }
        //------------------------------------------------------------------------
        private void Cmb_R_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_rcity)
            {
                Cmb_phone_Code_R.DataBindings.Clear();
                Cmb_phone_Code_R.DataBindings.Add("SelectedValue", binding_cbo_rcity_con, "COUN_K_PH");

                try
                {
                    Cmb_T_City.SelectedValue = Cmb_R_City.SelectedValue;
                    if (Cmb_T_City.SelectedValue == null)
                    {
                        Cmb_T_City.SelectedIndex = 0;
                    }
                }
                catch
                { }
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Sender_TextChanged(object sender, EventArgs e)
        {
            ENAR_BTN.Enabled = true;
            ar_sen.Enabled = true;
            chang_sender = false;
            chang_rec = false;
            Txt_another_Sen.Enabled = true;
            Chk_Term.Enabled = true;

            if (Txt_Sender.Text.Trim() != "")
            {
                Grd_CustSen_Name.Columns["Column17"].DataPropertyName = "Per_AName";
                btnlang = 1;
                ar_sen.BackColor = System.Drawing.Color.LightGreen;
                ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;
                //ENAR_BTN.Text = "EN";
                label5.Text = "";

                Cmb_S_City.DataSource = binding_cbo_scity_con;
                Cmb_S_City.ValueMember = "Cit_ID";
                Cmb_S_City.DisplayMember = "ACity_Name";



                Cmb_S_Doc_Type.DataSource = binding_sfmd;
                Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_S_Doc_Type.DisplayMember = "Fmd_AName";

                cmb_s_nat.DataSource = binding_cb_snat;
                cmb_s_nat.ValueMember = "Nat_ID";
                cmb_s_nat.DisplayMember = "Nat_AName";

                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = "Gender_Aname";

                Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
                Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
                Cmb_Case_Purpose.DisplayMember = "Case_purpose_Aname";


                resd_cmb.DataSource = binding_cmb_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = "resd_flag_rec_Aname";

                cmb_job_sender.DataSource = binding_cmb_job_sender;
                cmb_job_sender.ValueMember = "Job_ID";
                cmb_job_sender.DisplayMember = "Job_AName";



                //resd_cmb.Items.Clear();
                //resd_cmb.Items.Add("مقيم");
                //resd_cmb.Items.Add("غير مقيم");
                //resd_cmb.SelectedIndex = 0;

                //---------------------------------------------------
                int per_info_id = 0;
                int.TryParse(Txt_Sender.Text.Trim(), out per_info_id);
                Sql_Txt = " Exec get_per_info " + "'" + Txt_Sender.Text.Trim() + "'";
                connection.SqlExec(Sql_Txt, "per_info_tbl");

                if (connection.SQLDS.Tables["per_info_tbl"].Rows.Count > 0)
                {

                    binding_Grd_cust_sender.DataSource = connection.SQLDS.Tables["per_info_tbl"];
                    Grd_CustSen_Name.DataSource = binding_Grd_cust_sender;
                    Txt_S_Phone.Text = "";
                    cmb_job_sender.SelectedIndex = 0;
                    txtS_Suburb.Text = ""; TxtS_State.Text = ""; Txts_street.Text = ""; TxtS_Post_Code.Text = "";
                    Txt_Doc_S_Date.Text = "";
                    Txt_Doc_S_Exp.Text = "";
                    Txt_Doc_S_Issue.Text = "";
                    Txt_S_Birth_Place.Text = ""; Txt_Sbirth_Date.Text = ""; Txt_T_Purpose.Text = "";
                    Txt_Notes.Text = ""; Txt_S_Doc_No.Text = ""; Txt_Soruce_money.Text = "";
                    Txt_Relionship.Text = ""; Txt_mail.Text = ""; txt_Mother_name.Text = "";
                    Txt_Notes.Text = ""; Txt_T_Purpose.Text = "";
                    cmb_Gender_id.SelectedIndex = 0;
                    Cmb_phone_Code_R.SelectedIndex = 0;
                    Cmb_Case_Purpose.SelectedIndex = 0;
                    Cmb_S_City.SelectedIndex = 0;
                    Cmb_S_Doc_Type.SelectedIndex = 0;
                    cmb_s_nat.SelectedIndex = 0;
                    Txt_another_Sen.Text = "";
                    ////------المرسل
                    Txtr_Suburb.Text = ""; Txt_R_Phone.Text = ""; cmb_job_receiver.SelectedIndex = 0;
                    Txtr_Street.Text = ""; Txtr_State.Text = ""; Txtr_Post_Code.Text = "";
                    //Txt_another_rec.Text = ""; 
                    Txt_Reciever.Text = "";
                    Cmb_R_Nat.SelectedIndex = 0;
                    Cmb_phone_Code_R.SelectedIndex = 0;
                    Cmb_R_City.SelectedIndex = 0;
                    resd_cmb.SelectedIndex = 0;
                    Txt_S_details_job.Text = "";
                    chang_sender = true;
                    Grd_CustSen_Name_SelectionChanged(null, null);

                    if (connection.SQLDS.Tables["per_info_tbl1"].Rows.Count > 0)
                    {
                        binding_Grd_cust_Reciever.DataSource = connection.SQLDS.Tables["per_info_tbl1"];
                        Grd_CustRec_Name.DataSource = binding_Grd_cust_Reciever;
                        chang_rec = true;
                        Grd_CustRec_Name_SelectionChanged(null, null);
                    }

                }
                else
                {
                    chang_sender = false;
                    chang_rec = false;
                    Grd_CustSen_Name.DataSource = new DataTable();
                    Grd_CustRec_Name.DataSource = new DataTable();
                    Txt_S_Phone.Text = ""; cmb_job_sender.SelectedIndex = 0; txtS_Suburb.Text = ""; TxtS_State.Text = ""; Txts_street.Text = "";
                    TxtS_Post_Code.Text = "";
                    Txt_Doc_S_Date.Text = ""; Txt_Doc_S_Exp.Text = ""; Txt_Doc_S_Issue.Text = "";
                    Txt_S_Birth_Place.Text = ""; Txt_Sbirth_Date.Text = ""; Txt_T_Purpose.Text = "";
                    Txt_Notes.Text = ""; Txt_S_Doc_No.Text = ""; Txt_Soruce_money.Text = "";
                    Txt_Relionship.Text = ""; Txt_mail.Text = ""; txt_Mother_name.Text = "";
                    Txt_Notes.Text = ""; Txt_T_Purpose.Text = "";
                    cmb_Gender_id.SelectedIndex = 0;
                    Cmb_phone_Code_R.SelectedIndex = 0;
                    Cmb_Case_Purpose.SelectedIndex = 0;
                    Cmb_S_City.SelectedIndex = 0;
                    Cmb_S_Doc_Type.SelectedIndex = 0;
                    cmb_s_nat.SelectedIndex = 0;
                    Txt_another_Sen.Text = "";
                    Cmb_Code_phone_S.SelectedIndex = 0;
                    cmb_s_nat.SelectedIndex = 0;

                    ////------المرسل
                    Txtr_Suburb.Text = ""; Txt_R_Phone.Text = ""; cmb_job_receiver.SelectedIndex = 0;
                    Txtr_Street.Text = ""; Txtr_State.Text = ""; Txtr_Post_Code.Text = "";
                    //Txt_another_rec.Text = "";
                    Txt_Reciever.Text = "";
                    Cmb_R_Nat.SelectedIndex = 0;
                    Cmb_phone_Code_R.SelectedIndex = 0;
                    Cmb_R_City.SelectedIndex = 0;
                    resd_cmb.SelectedIndex = 0;
                    Txt_S_details_job.Text = "";
                    // binding_Grd_cust_sender.DataSource = new BindingSource();
                    //if (connection.SQLDS.Tables.Contains("per_info_tbl"))
                    //connection.SQLDS.Tables.Remove("per_info_tbl");
                    Btn_Browser.Enabled = false;

                }
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Reciever_TextChanged(object sender, EventArgs e)
        {
            Txt_another_Rec.Enabled = true;
            Chk_Term1.Enabled = true;

            en_rec_btn.Enabled = true;
            ar_rec_btn.Enabled = true;
            chang_rec = false;
            if (Txt_Reciever.Text.Trim() != "")
            {
                Grd_CustRec_Name.Columns["dataGridViewTextBoxColumn4"].DataPropertyName = "Per_AName";
                btnlang_rec = 1;
                ar_rec_btn.BackColor = System.Drawing.Color.LightGreen;
                en_rec_btn.BackColor = System.Drawing.Color.WhiteSmoke;
                //  en_rec_btn.Text = "EN";
                label8.Text = "";

                Cmb_R_City.DataSource = binding_cbo_rcity_con;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = "ACity_Name";

                Cmb_R_Nat.DataSource = binding_cb_rnat;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = "Nat_AName";


                resd_cbm_rec.DataSource = binding_cmb_resd_rec;
                resd_cbm_rec.ValueMember = "resd_flag";
                resd_cbm_rec.DisplayMember = "resd_flag_rec_Aname";


                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = "Job_AName";




                //---------------------------------------------------

                int per_info_id = 0;
                int.TryParse(Txt_Reciever.Text.Trim(), out per_info_id);
                Sql_Txt = " Exec get_per_info " + "'" + Txt_Reciever.Text.Trim() + "'";
                connection.SqlExec(Sql_Txt, "per_info_tbl_rec");

                if (connection.SQLDS.Tables["per_info_tbl_rec"].Rows.Count > 0)
                {
                    binding_Grd_cust_Reciever.DataSource = connection.SQLDS.Tables["per_info_tbl_rec"];
                    Grd_CustRec_Name.DataSource = binding_Grd_cust_Reciever;
                    Txtr_Suburb.Text = ""; Txt_R_Phone.Text = ""; cmb_job_receiver.SelectedIndex = 0;
                    Txtr_Street.Text = ""; Txtr_State.Text = ""; Txtr_Post_Code.Text = "";
                    Txt_another_Rec.Text = "";
                    //Txt_another_rec.Text = "";
                    Cmb_R_Nat.SelectedIndex = 0;
                    Cmb_phone_Code_R.SelectedIndex = 0;
                    Cmb_R_City.SelectedIndex = 0;
                    resd_cbm_rec.SelectedIndex = 0;
                    chang_rec = true;
                    Grd_CustRec_Name_SelectionChanged(null, null);

                }
                else
                {
                    Grd_CustRec_Name.DataSource = new DataTable();
                    Txtr_Suburb.Text = ""; Txt_R_Phone.Text = ""; cmb_job_receiver.SelectedIndex = 0;
                    Txtr_Street.Text = ""; Txtr_State.Text = ""; Txtr_Post_Code.Text = "";
                    //Txt_another_rec.Text = "";
                    Cmb_R_Nat.SelectedIndex = 0;
                    Cmb_phone_Code_R.SelectedIndex = 0;
                    Cmb_R_City.SelectedIndex = 0;
                    resd_cbm_rec.SelectedIndex = 0;
                    Txt_another_Rec.Text = ""; Chk_Term1.Checked = false;
                    button1.Enabled = false;
                    //binding_Grd_cust_Reciever.DataSource = new BindingSource();
                    //if (connection.SQLDS.Tables.Contains("per_info_tbl1"))
                    //    connection.SQLDS.Tables.Remove("per_info_tbl1");
                }
            }
        }
        //------------------------------------------------------------------------
        private void Grd_CustSen_Name_SelectionChanged(object sender, EventArgs e)
        {
            if (chang_sender)
            {
                //Txt_S_Phone.DataBindings.Clear();
                //Txt_job_Sender.DataBindings.Clear();
                //txtS_Suburb.DataBindings.Clear();
                //Txts_street.DataBindings.Clear();
                //TxtS_State.DataBindings.Clear();
                //TxtS_Post_Code.DataBindings.Clear();
                //Txt_Doc_S_Date.DataBindings.Clear();
                //Txt_Doc_S_Exp.DataBindings.Clear();
                //Txt_Doc_S_Issue.DataBindings.Clear();
                //Txt_S_Birth_Place.DataBindings.Clear();
                //Txt_Sbirth_Date.DataBindings.Clear();
                //Txt_S_Doc_No.DataBindings.Clear();
                //// Txt_another.DataBindings.Clear();
                //txt_Mother_name.DataBindings.Clear();
                //Txt_mail.DataBindings.Clear();
                //Cmb_S_Doc_Type.DataBindings.Clear();
                //Cmb_Code_phone_S.DataBindings.Clear();
                //Cmb_S_City.DataBindings.Clear();
                //cmb_s_nat.DataBindings.Clear();
                //cmb_Gender_id.DataBindings.Clear();
                //Txt_Sender.DataBindings.Clear();
                //resd_cmb.SelectedIndex = 0;

                //Txt_S_Phone.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Phone");
                //Txt_job_Sender.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Occupation");
                //txtS_Suburb.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_Suburb");
                //Txts_street.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_street");
                //TxtS_State.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_State");
                //TxtS_Post_Code.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_Post_Code");
                //Txt_Doc_S_Date.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_Date");
                //Txt_Doc_S_Exp.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_EDate");
                //Txt_Doc_S_Issue.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_IS");
                //Txt_S_Birth_Place.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_Birth_place");
                //Txt_Sbirth_Date.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Birth_day");

                //Txt_S_Doc_No.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_NO");
                ////Txt_another.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_eName");
                //txt_Mother_name.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Mother_name");
                //Txt_mail.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Email");
                //Cmb_S_City.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_City_ID");
                //Cmb_S_Doc_Type.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_Frm_Doc_ID");
                //cmb_s_nat.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_Nationalty_ID");
                //cmb_Gender_id.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_Gender_ID");
                //// Txt_Sender.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_AName");
                ENAR_BTN.Enabled = true;
                ar_rec_btn.Enabled = true;
                label5.Text = "";
                Txt_S_Phone.DataBindings.Clear();
                cmb_job_sender.DataBindings.Clear();
                txtS_Suburb.DataBindings.Clear();
                Txts_street.DataBindings.Clear();
                TxtS_State.DataBindings.Clear();
                TxtS_Post_Code.DataBindings.Clear();
                Txt_Doc_S_Date.DataBindings.Clear();
                Txt_Doc_S_Exp.DataBindings.Clear();
                Txt_Doc_S_Issue.DataBindings.Clear();
                Txt_S_Birth_Place.DataBindings.Clear();
                Txt_Sbirth_Date.DataBindings.Clear();
                Txt_S_Doc_No.DataBindings.Clear();
                txt_Mother_name.DataBindings.Clear();
                Txt_mail.DataBindings.Clear();
                Cmb_S_Doc_Type.DataBindings.Clear();
                Cmb_Code_phone_S.DataBindings.Clear();
                Cmb_S_City.DataBindings.Clear();
                cmb_s_nat.DataBindings.Clear();
                cmb_Gender_id.DataBindings.Clear();
                Txt_Sender.DataBindings.Clear();
                Tex_Social_ID.DataBindings.Clear();
                resd_cmb.DataBindings.Clear();
                Txt_S_details_job.DataBindings.Clear();


                Txt_S_Phone.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Phone");
                TxtS_Post_Code.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_Post_Code");
                Txt_Doc_S_Date.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_Date");
                Txt_Doc_S_Exp.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_EDate");
                Txt_mail.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Email");
                Cmb_S_City.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_City_ID");
                Cmb_S_Doc_Type.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_Frm_Doc_ID");
                cmb_s_nat.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_Nationalty_ID");
                cmb_Gender_id.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "per_Gender_ID");
                Tex_Social_ID.DataBindings.Add("Text", binding_Grd_cust_sender, "Social_No");
                Txt_S_details_job.DataBindings.Add("Text", binding_Grd_cust_sender, "details_job");



                resd_cmb.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "resd_flag");
                cmb_job_sender.DataBindings.Add("SelectedValue", binding_Grd_cust_sender, "Per_Occupation_id");

                Chk_Term.Checked = false;
                Chk_Term_CheckedChanged(null, null);

                if (btnlang == 1)
                {
                    ar_sen.BackColor = System.Drawing.Color.LightGreen;
                    ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;
                    //ENAR_BTN.Text = "EN";
                    label5.Text = "";

                    if (((DataRowView)binding_Grd_cust_sender.Current).Row["Per_AName"].ToString() != "")
                    {
                        // Txt_job_Sender.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Occupation");
                        txtS_Suburb.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_Suburb");
                        Txts_street.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_street");
                        TxtS_State.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_State");
                        Txt_Doc_S_Issue.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_IS");
                        Txt_S_Birth_Place.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_Birth_place");
                        Txt_Sbirth_Date.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Birth_day");
                        Txt_S_Doc_No.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_NO");
                        txt_Mother_name.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Mother_name");
                    }
                    else
                    {
                        btnlang = 2;
                        ENAR_BTN_Click(null, null);
                        //ar_sen.Enabled = false;

                    }
                }
                else
                {

                    // Txt_job_Sender.DataBindings.Add("Text", binding_Grd_cust_sender, "EOccupation");
                    txtS_Suburb.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_ESuburb");
                    Txts_street.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_EStreet");
                    TxtS_State.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_EState");
                    Txt_Doc_S_Issue.DataBindings.Add("Text", binding_Grd_cust_sender, "per_EFrm_Doc_IS");
                    Txt_S_Birth_Place.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_EBirth_place");
                    Txt_Sbirth_Date.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Birth_day");
                    Txt_S_Doc_No.DataBindings.Add("Text", binding_Grd_cust_sender, "per_Frm_Doc_NO");
                    txt_Mother_name.DataBindings.Add("Text", binding_Grd_cust_sender, "mother_Ename");
                }
                byte Count = 0;
                Count = Convert.ToByte(((DataRowView)binding_Grd_cust_sender.Current).Row["upload_flag"]);
                if (Count == 1)
                { Btn_Browser.Enabled = true; }
                else
                { Btn_Browser.Enabled = false; }

            }

        }
        //------------------------------------------------------------------------
        private void Grd_CustRec_Name_SelectionChanged(object sender, EventArgs e)
        {
            if (chang_rec)
            {
                //Txtr_Suburb.DataBindings.Clear();
                //Txt_R_Phone.DataBindings.Clear();
                //Txt_R_job.DataBindings.Clear();
                //Txtr_Street.DataBindings.Clear();
                //Txtr_State.DataBindings.Clear();
                //Txtr_Post_Code.DataBindings.Clear();
                ////Txt_another_rec.DataBindings.Clear();
                //Cmb_R_City.DataBindings.Clear();
                //Cmb_R_Nat.DataBindings.Clear();
                //resd_cbm_rec.SelectedIndex = 0;
                ////   Txt_Reciever.DataBindings.Clear();

                //Txtr_Suburb.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Suburb");
                //Txt_R_Phone.DataBindings.Add("Text", binding_Grd_cust_Reciever, "per_Phone");
                //Txt_R_job.DataBindings.Add("Text", binding_Grd_cust_Reciever, "per_Occupation");
                //Txtr_Street.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Street");
                //Txtr_State.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_State");
                //Txtr_Post_Code.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Post_Code");
                //// Txt_another_rec.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_eName");
                //Cmb_R_City.DataBindings.Add("SelectedValue", binding_Grd_cust_Reciever, "per_City_ID");
                //Cmb_R_Nat.DataBindings.Add("SelectedValue", binding_Grd_cust_Reciever, "per_Nationalty_ID");
                ////  Txt_Reciever.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_AName");

                en_rec_btn.Enabled = true;
                ar_rec_btn.Enabled = true;
                label8.Text = "";
                Txtr_Suburb.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                cmb_job_receiver.DataBindings.Clear();
                Txtr_Street.DataBindings.Clear();
                Txtr_State.DataBindings.Clear();
                Txtr_Post_Code.DataBindings.Clear();
                Cmb_R_City.DataBindings.Clear();
                Cmb_R_Nat.DataBindings.Clear();
                resd_cbm_rec.DataBindings.Clear();



                Txt_R_Phone.DataBindings.Add("Text", binding_Grd_cust_Reciever, "per_Phone");
                Txtr_Post_Code.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Post_Code");
                Cmb_R_City.DataBindings.Add("SelectedValue", binding_Grd_cust_Reciever, "per_City_ID");
                Cmb_R_Nat.DataBindings.Add("SelectedValue", binding_Grd_cust_Reciever, "per_Nationalty_ID");

                resd_cbm_rec.DataBindings.Add("SelectedValue", binding_Grd_cust_Reciever, "resd_flag");
                cmb_job_receiver.DataBindings.Add("SelectedValue", binding_Grd_cust_Reciever, "Per_Occupation_id");

                Chk_Term1.Checked = false;
                Chk_Term1_CheckedChanged_1(null, null);

                if (btnlang_rec == 1)
                {
                    ar_rec_btn.BackColor = System.Drawing.Color.LightGreen;
                    en_rec_btn.BackColor = System.Drawing.Color.WhiteSmoke;
                    //  en_rec_btn.Text = "EN";
                    label8.Text = "";
                    if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString() != "")
                    {

                        Txtr_Suburb.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Suburb");
                        //Txt_R_job.DataBindings.Add("Text", binding_Grd_cust_Reciever, "per_Occupation");
                        Txtr_Street.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Street");
                        Txtr_State.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_State");
                    }
                    else
                    {
                        btnlang_rec = 2;
                        en_rec_btn_Click(null, null);
                        // ar_rec_btn.Enabled = false;

                    }

                }
                else
                {
                    Txtr_Suburb.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_ESuburb");
                    //  Txt_R_job.DataBindings.Add("Text", binding_Grd_cust_Reciever, "EOccupation");
                    Txtr_Street.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_EStreet");
                    Txtr_State.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_EState");


                }
                byte Count = 0;
                Count = Convert.ToByte(((DataRowView)binding_Grd_cust_Reciever.Current).Row["upload_flag"]);
                if (Count == 1)
                { button1.Enabled = true; }
                else
                { button1.Enabled = false; }


            }

        }
        //----عملية البيع و شراء
        private void cmb_cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_cur_rrate)
            {
                Txt_ExRate_Rem.DataBindings.Clear();
                txt_maxrate_rem.DataBindings.Clear();
                txt_minrate_rem.DataBindings.Clear();
                txt_locamount_rem.ResetText();

                param_Exch_rate_rem = Convert.ToDecimal(((DataRowView)binding_cur_bsrem.Current).Row["Exch_rate"]);
                Txt_ExRate_Rem.DataBindings.Add("Text", binding_cur_bsrem, "Exch_rate");
                txt_maxrate_rem.DataBindings.Add("Text", binding_cur_bsrem, "Max_S_price");
                txt_minrate_rem.DataBindings.Add("Text", binding_cur_bsrem, "Min_S_price");
                txt_locamount_rem.Text = (Convert.ToDecimal(Txt_ExRate_Rem.Text) * Convert.ToDecimal(Txt_Rem_Amount.Text)).ToString();
                // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                if (Cbo_type.SelectedIndex == 1)
                {
                    // TxtDiscount_Amount.Text = "0.000";
                    Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString(); //+ Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                }
                else
                {
                    // Discount_Calculator();
                    Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString(); //+ Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                }
            }
        }
        //------------------------------------------------------------------------
        private void Txt_ExRate_Rem_TextChanged(object sender, EventArgs e)
        {
            txt_locamount_rem.ResetText();
            txt_locamount_rem.Text = (Convert.ToDecimal(Txt_ExRate_Rem.Text) * Convert.ToDecimal(Txt_Rem_Amount.Text)).ToString();
            // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();

            if (Cbo_type.SelectedIndex == 1)
            {
                //  TxtDiscount_Amount.Text = "0.000";
                Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString();// + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            }
            else
            {
                //Discount_Calculator();
                Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString();// + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            }
        }
        //------------------------------------------------------------------------
        private void cmb_cur_comm_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (chang_cur_crate)
            {
                Txt_ExRate_comm.DataBindings.Clear();
                txt_maxrate_comm.DataBindings.Clear();
                txt_minrate_comm.DataBindings.Clear();
                txt_locamount_comm.ResetText();

                param_Exch_rate_com = Convert.ToDecimal(((DataRowView)binding_cur_bscomm.Current).Row["Exch_rate"]);
                Txt_ExRate_comm.DataBindings.Add("Text", binding_cur_bscomm, "Exch_rate");
                txt_maxrate_comm.DataBindings.Add("Text", binding_cur_bscomm, "Max_S_price");
                txt_minrate_comm.DataBindings.Add("Text", binding_cur_bscomm, "Min_S_price");
                txt_locamount_comm.Text = (Convert.ToDecimal(Txt_ExRate_comm.Text) * Convert.ToDecimal(Txt_Com_Amnt.Text)).ToString();
                // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                if (Cbo_type.SelectedIndex == 1)
                {
                    //TxtDiscount_Amount.Text = "0.000";
                    Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString();// + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                }
                else
                {
                    //   Discount_Calculator();
                    Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString();// + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                }
            }
        }
        //------------------------------------------------------------------------
        private void Txt_ExRate_comm_TextChanged(object sender, EventArgs e)
        {
            txt_locamount_comm.ResetText();
            txt_locamount_comm.Text = (Convert.ToDecimal(Txt_ExRate_comm.Text) * Convert.ToDecimal(Txt_Com_Amnt.Text)).ToString();
            // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            if (Cbo_type.SelectedIndex == 1)
            {
                // TxtDiscount_Amount.Text = "0.000";
                Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString();// + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            }
            else
            {
                // Discount_Calculator();
                Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + (Convert.ToInt16(cmb_comm_type.SelectedValue) == 1 ? Convert.ToDecimal(txt_locamount_comm.Text) : 0)).ToString();// + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            }
        }
        //------------------------------------------------------------------------
        private void Cmb_R_CUR_ID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //checkBox5.Checked = false;
            //lbl_comm.Visible = false;
            if (change_Cur_REM)
            {
                //?????? Cbo_agent_cur.SelectedValue = Cmb_R_CUR_ID.SelectedValue;
                get_comm();
                try
                {
                    Cmb_Comm_Cur.SelectedValue = Cmb_R_CUR_ID.SelectedValue;

                    if (Cmb_Comm_Cur.SelectedValue == null)
                    {
                        Cmb_Comm_Cur.SelectedIndex = 0;
                    }
                }
                catch
                { }
                //if (chk_comm_info.Checked)
                //{
                //    checkBox2_CheckedChanged(null, null);
                //}
            }
        }
        //------------------------------------------------------------------------
        private void Cmb_PR_Cur_Id_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //checkBox5.Checked = false;
            //lbl_comm.Visible = false;
            get_comm();
            //if (chk_comm_info.Checked)
            //{
            //    checkBox2_CheckedChanged(null, null);
            //}
        }
        //------------------------------------------------------------------------
        private void Cmb_T_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            //checkBox5.Checked = false;
            //lbl_comm.Visible = false;
            get_comm();
            //if (chk_comm_info.Checked)
            //{
            //    checkBox2_CheckedChanged(null, null);
            //}
        }
        //------------------------------------------------------------------------
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //-----احضار اسهعار الصرف


            if (checkBox1.Checked)
            {
                if (Convert.ToInt16(Cmb_R_CUR_ID.SelectedValue) != connection.Loc_Cur_Id)
                {

                    chang_cur_rrate = false;
                    chang_cur_crate = false;
                    Txt_Rem_Amount.Text = Txtr_amount.Text;
                    Txt_Com_Amnt.Text = Txt_Com_Amount.Text;

                    try
                    {

                        connection.SqlExec("Exec Get_Cur_Buy_Sal_rem " + connection.T_ID + " , " + TxtBox_User.Tag + " , "
                                                        + 0, "Cur_Buy_Sal_Tbl");
                        DataTable Dt_currr = connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"]
                                            .DefaultView.ToTable(true, "for_cur_id", "Cur_AName", "Cur_EName", "Exch_rate", "Max_S_price",
                                            "Min_S_price", "Cur_Code")
                                            .Select(" for_cur_id = " + Cmb_R_CUR_ID.SelectedValue).CopyToDataTable();
                        binding_cur_bsrem.DataSource = Dt_currr;
                        cmb_cur.DataSource = binding_cur_bsrem;
                        cmb_cur.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You must define the Exchange price for the currency" : " يجب تعريف سعر التعادل للعملة", MyGeneral_Lib.LblCap);
                        return;
                    }

                    cmb_cur.ValueMember = "for_cur_id";
                    try
                    {
                        DataTable Dt_currc = connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"]
                                            .DefaultView.ToTable(true, "for_cur_id", "Cur_AName", "Cur_EName", "Exch_rate", "Max_S_price",
                                            "Min_S_price", "Cur_Code")
                                            .Select(" for_cur_id = " + Cmb_Comm_Cur.SelectedValue).CopyToDataTable();
                        binding_cur_bscomm.DataSource = Dt_currc;
                        cmb_cur_comm.DataSource = binding_cur_bscomm;
                        cmb_cur_comm.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
                        cmb_cur_comm.ValueMember = "for_cur_id";
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You must define the Exchange price for the currency" : " يجب تعريف سعر التعادل للعملة", MyGeneral_Lib.LblCap);
                        return;
                    }

                    if (connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"].Rows.Count > 0)
                    {
                        chang_cur_rrate = true;
                        cmb_cur_SelectedIndexChanged(null, null);
                        chang_cur_crate = true;
                        cmb_cur_comm_SelectedIndexChanged_1(null, null);


                        if (Convert.ToInt16(cmb_comm_type.SelectedValue) == -1)
                        {
                            cmb_cur_comm.Enabled = false;
                            Txt_ExRate_comm.Enabled = false;

                        }
                        else
                        {
                            cmb_cur_comm.Enabled = true;
                            Txt_ExRate_comm.Enabled = true;
                        }
                    }

                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Document expired" : "عملة الحوالة تشبة العملة المحلية", MyGeneral_Lib.LblCap);

                    checkBox1.Checked = false;
                    chang_cur_rrate = false;
                    chang_cur_crate = false;
                    cmb_cur.DataSource = new DataTable();
                    cmb_cur_comm.DataSource = new DataTable();
                    Txt_ExRate_Rem.ResetText();
                    txt_maxrate_rem.ResetText();
                    txt_minrate_rem.ResetText();
                    Txt_ExRate_comm.ResetText();
                    txt_maxrate_comm.ResetText();
                    txt_minrate_comm.ResetText();
                    Txt_Rem_Amount.ResetText();
                    Txt_Com_Amnt.ResetText();
                    Txt_Tot_amount.ResetText();
                    //if (Cbo_type.SelectedIndex == 1)
                    //{
                    //    TxtDiscount_Amount.ResetText();
                    //}

                }
            }
            else
            {
                chang_cur_rrate = false;
                chang_cur_crate = false;
                cmb_cur.DataSource = new DataTable();
                cmb_cur_comm.DataSource = new DataTable();
                Txt_ExRate_Rem.ResetText();
                txt_maxrate_rem.ResetText();
                txt_minrate_rem.ResetText();
                Txt_ExRate_comm.ResetText();
                txt_maxrate_comm.ResetText();
                txt_minrate_comm.ResetText();
                Txt_Rem_Amount.ResetText();
                Txt_Com_Amnt.ResetText();
                Txt_Tot_amount.ResetText();
                //if (Cbo_type.SelectedIndex == 1)
                //{
                //    TxtDiscount_Amount.ResetText();
                //}

            }
        }
        //------------------------------------------------------------------------
        //private void Discount_Calculator()//------الخصم
        //{
        //    //Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
        //    decimal Loc_Amount = Convert.ToDecimal(Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text));
        //    Mdiff_Amoun = 0;
        //    decimal x = 0;
        //    Mdiff_Amoun = Convert.ToDecimal(Loc_Amount % connection.Discount_Amount);

        //    x = (connection.Discount_Amount / 2);
        //    if (Mdiff_Amoun > x)
        //    {

        //        Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
        //    }
        //    else
        //    {
        //        Mdiff_Amoun = Mdiff_Amoun * -1; //----مكتسب
        //    }
        //    // Mdiff_Amoun = Convert.ToDecimal(Convert.ToDecimal( "14, 000") % connection.Discount_Amount);
        //    TxtDiscount_Amount.Text = Mdiff_Amoun.ToString();
        //}
        //------------------------------------------------------------------------
        private void button3_Click(object sender, EventArgs e)
        {

            #region Validation
            // new work
            //if (Grd_procer.Rows.Count > 0)
            //{
            //    if (Convert.ToByte(((DataRowView)_BS_sub_cust.Current).Row["Chk_Correspondence_Sys"]) == 1 && TYPE_CBO_OUT_IN.SelectedIndex == 2)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "الوسيط يمتلك نظام المراسلات لايمكن اجراء الحوالة" : " the customer has Correspondence system you cannont insert this rem. ", MyGeneral_Lib.LblCap);
            //        return;
            //    }
            //}

            if (Grd_CustSen_Name.RowCount > 0)
            {
                if (btnlang == 1 && ((DataRowView)binding_Grd_cust_sender.Current).Row["Per_AName"].ToString() == "" && Grd_CustSen_Name.RowCount > 0)
                //&& Convert.ToInt16(((DataRowView)binding_Grd_cust_sender.Current).Row["Per_id"])  != 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون اسم المرسل معرف" : "you must define sender name ", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }

                if (btnlang == 2 && ((DataRowView)binding_Grd_cust_sender.Current).Row["Per_EName"].ToString() == "" && Grd_CustSen_Name.RowCount > 0)
                //Convert.ToInt16(((DataRowView)binding_Grd_cust_sender.Current).Row["Per_id"]) !=0 )
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون اسم المرسل معرف" : "you must define sender name ", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
            }
            if (Grd_CustRec_Name.RowCount > 0)
            {
                if (btnlang_rec == 1 && ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString() == "" && Grd_CustRec_Name.RowCount > 0)
                //&& Convert.ToInt16(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_id"]) != 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون اسم المستلم معرف" : "you must define Reciever name ", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }

                if (btnlang_rec == 2 && ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString() == "" && Grd_CustRec_Name.RowCount > 0)
                //&& Convert.ToInt16(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_id"]) != 0 )
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون اسم المستلم معرف" : "you must define Reciever name ", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
            }

            if (TYPE_CBO_OUT_IN.SelectedIndex == 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Check Type of Send Rem. " : "اختر نوع ارسال الحوالة");
                return;
            }
            //fff


            if (chk_Agent.Checked == true)
            {
                try
                {
                    DataTable DT_REM_ID = new DataTable();
                    DT_REM_ID = connection.SQLDS.Tables["TBL_Sub_cur"].DefaultView.ToTable(false, "cur_id").Select("cur_id =" + Convert.ToInt32(Cmb_R_CUR_ID.SelectedValue)).CopyToDataTable();
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Rem Currency must be included in customers currency paid " : "عملة الحوالة يجب ان تكون موجودة ضمن عملة الوكيل الدافع");
                    return;
                }


            }
            //Ord_Strt1 = MyGeneral_Lib.DateChecking(Txt_Doc_S_Exp.Text);
            //if (Ord_Strt1 == "-1")
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Be sure to write the Docement expier date correctly " : "ادخل تاريخ انتهاء الوثيقة المرسل بشكل صحيح", MyGeneral_Lib.LblCap);
            //    return;
            //}

            // string Doc_r_Exp = Ord_Strt1 == "0" || Ord_Strt1 == "-1" ? "" : Ord_Strt1;

            //-------------------------------


            //Ord_Strt1 = MyGeneral_Lib.DateChecking(Txt_Sbirth_Date.Text);
            //if (Ord_Strt1 == "-1" || Ord_Strt1 == "0")
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Be sure to write the birth date of sender correctly " : "ادخل تاريخ تولد المرسل بشكل صحيح", MyGeneral_Lib.LblCap);
            //    return;
            //}

            //   string Sbirth_Date = Ord_Strt1 == "0" || Ord_Strt1 == "-1" ? "" : Ord_Strt1;





            //Ord_Strt1 = MyGeneral_Lib.DateChecking(Txt_Doc_S_Date.Text);
            //if (Ord_Strt1 == "-1" || Ord_Strt1 == "0")
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Be sure to write the birth date of sender correctly " : "ادخل تاريخ وثيقة المرسل بشكل صحيح", MyGeneral_Lib.LblCap);
            //    return;
            //}

            //    string Doc_S_Date = Ord_Strt1 == "0" || Ord_Strt1 == "-1" ? "" : Ord_Strt1;



            //try // 


            if (Txt_Sender.Text.Trim() == "" && binding_Grd_cust_sender.Count < 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter Sender Name" : "ادخل اسم المرسل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            //if (Txt_another.Text == "" && binding_Grd_cust_Reciever.Count > 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter Another Sender Name" : "ادخل اسم المرسل الاخر", MyGeneral_Lib.LblCap);
            //    button3.Enabled = true;
            //    return;
            //}
            if (binding_cbo_scity_con.Count < 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter City of Sender " : "ادخل مدينة المرسل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (txtS_Suburb.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the suburb" : "ادخل الزقاق المرسل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (Txts_street.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the street" : "ادخل الحـــي المرسل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (TxtS_State.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the State" : "ادخل المحافظة المرسل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Txt_S_Phone.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the phone No." : "  ادخل رقم الهاتــف", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (TYPE_CBO_OUT_IN.SelectedIndex == 2)// في حال الصادر فقط نجيك
            {
                if (Convert.ToInt16(Cmb_S_Doc_Type.SelectedValue) == 0 || Cmb_S_Doc_Type.SelectedValue == null)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter type of the Document" : "اختر نوع  الوثيقـة المرسل", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------
                if (Txt_S_Doc_No.Text.Trim() == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the Document No." : "ادخل رقم الوثيقة المرسل", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }

                //-----------------------------
                DateTime date_Now = Convert.ToDateTime(DateTime.Now.ToString("d"));
                DateTime Doc_S_date = Convert.ToDateTime(Txt_Doc_S_Date.Value.Date);

                if (date_Now == Doc_S_date)
                {
                    DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "تاريخ اصدار وثيقة المرسل مساوي الى التاريخ الحالي هل انت متأكد من تاريخ الاصدار" :
                    "The date of issuance of the document sender is equal to the current date. Are you sure of the release date ", MyGeneral_Lib.LblCap,
                     MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (Dr == DialogResult.No)
                    {
                        Txt_Doc_S_Date.Focus();
                        button3.Enabled = true;
                        return;
                    }
                }
                //-----------------------------
                DateTime date = Convert.ToDateTime(DateTime.Now.ToString("d"));
                DateTime text = Convert.ToDateTime(Txt_Doc_S_Exp.Value.Date.ToString("d"));

                if (text < date)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Document expired" : "الوثيقةالمرسل منتهيةالصلاحية ", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------
                if (Txt_Doc_S_Issue.Text.Trim() == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the issue" : "ادخل محل اصدار الوثيقة المرسل", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------
                DateTime from_date = Convert.ToDateTime(DateTime.Now.ToString("d"));
                DateTime to = Convert.ToDateTime(Txt_Sbirth_Date.Value.Date);
                try
                {
                    TimeSpan difference = from_date - to;

                    if (Convert.ToDecimal(difference.TotalDays / 365.25) < 18)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter Sender Birth Date" : "عمرالمرسل اصغر من السن القانوني", MyGeneral_Lib.LblCap);
                        button3.Enabled = true;
                        return;
                    }
                }
                catch
                { }

                //-----------------------------
                if (Txt_S_Birth_Place.Text.Trim() == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the birth place" : "ادخل محل الولادة", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------
                if (Convert.ToInt16(cmb_s_nat.SelectedValue) == 0 || cmb_s_nat.SelectedValue == null)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the Nationalty" : "ادخل الجنسيــة المرسل", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------
                if (Convert.ToInt16(cmb_job_sender.SelectedValue) == 0 || cmb_job_sender.SelectedValue == null)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the job" : "ادخل المهنة المرسل", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------
                if (Convert.ToInt16(cmb_Gender_id.SelectedValue) == 0 || cmb_Gender_id.SelectedValue == null)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the Gender" : " ادخل الجنـــس", MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                    return;
                }
                //-----------------------------

            }
            ////////ff

            if (Txt_Doc_S_Exp.Checked == false)
            {
                Txt_Doc_S_Exp.Format = DateTimePickerFormat.Custom;
                S_doc_exp_null = Txt_Doc_S_Exp.CustomFormat = @format_null;
            }

            if (Convert.ToInt16(Cmb_R_City.SelectedValue) == 0 || Cmb_R_City.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Rec. City" : "ادخل مدينة المستلم  ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }



            if (Convert.ToInt16(Cmb_S_City.SelectedValue) == 0 || Cmb_S_City.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the sender city" : " ادخل مدينة المرسل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Txt_Soruce_money.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Soruce money" : "ادخل مصدر المال", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (Txt_Relionship.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Relationship of sender and receiver" : "ادخل علاقة المرسل بالمستلم", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (Txt_T_Purpose.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Purpose of remittences" : "ادخل الغرض من التحويل", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }


            //if (Txt_another_rec.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter Another Name of the receiver " : "ادخل اسم المستلم الاخر", MyGeneral_Lib.LblCap);
            //    button3.Enabled = true;
            //    return;
            //}

            if (Convert.ToInt16(Cmb_R_Nat.SelectedValue) == 0 || Cmb_R_Nat.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Nationalty" : " ادخل الجنسية", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Txt_R_Phone.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the phone No." : "ادخل رقم الهاتــف", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            //if (Txt_R_job.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter the job" : "ادخل المهنة المستلم", MyGeneral_Lib.LblCap);
            //    return;
            //}

            if (Txtr_amount.Text == "0.000")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter amount of the remittences" : "ادخل مبلغ الحوالة", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }


            //if (Txt_Cs_comm_cur.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Thear are No Currencies know " : "لاتوجد عملات معرفة ", MyGeneral_Lib.LblCap);
            //    return;
            //}
            if (checkBox1.Checked == true && (Txt_Rem_Amount.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Remittences Amount" : "ادخل مبلغ الحوالـة ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (checkBox1.Checked == true && (Txt_ExRate_Rem.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Exchange Rate" : "ادخل المعادل ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (checkBox1.Checked == true && (txt_locamount_rem.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Amount in local currency" : "ادخل المبلغ بالعملة المحلية ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (checkBox1.Checked == true && (Txt_Com_Amnt.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Commission Amount" : "ادخل مبلغ العمولــة ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            //if (checkBox1.Checked == true && (cmb_cur_comm.SelectedValue == ""))
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter Local Currency " : "ادخل العملة المحلية ", MyGeneral_Lib.LblCap);
            //    return;
            //}
            if (checkBox1.Checked == true && (Txt_ExRate_comm.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Exchange Rate" : "ادخل المعادل ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (checkBox1.Checked == true && (txt_locamount_comm.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Amount in local currency" : "ادخل المبلغ بالعملة المحلية ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }
            if (checkBox1.Checked == true && (Txt_Tot_amount.Text.Trim() == ""))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Total" : "ادخل المجموع ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) > Convert.ToDecimal(txt_maxrate_rem.Text))
            {
                MessageBox.Show(connection.Lang_id == 2 ? " السعر الفعلي اعلى من الحد الاعلى" : "السعر الفعلي اعلى من الحد الاعلى ", MyGeneral_Lib.LblCap);


            }

            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) < Convert.ToDecimal(txt_minrate_rem.Text))
            {
                MessageBox.Show(connection.Lang_id == 2 ? " السعر الفعلي اقل من الحد الادنى" : "السعر الفعلي اقل من الحد الادنى ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Cbo_type.SelectedIndex == 1 && Txt_procer.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? " يرجى اختيار الوسيط" : "يرجى اختيار الوسيط ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (chk_Agent.Checked == true && Txt_Agent.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? " يرجى اختيار الوكيل الدافع" : "يرجى اختيار الوكيل الدافع ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Cbo_type.SelectedIndex == 0 && TxtBox_User.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? " يرجى ربط المستخدم بصندوق" : "يرجى ربط المستخدم بصندوق ", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }

            if (Convert.ToInt32(Cmb_T_City.SelectedValue) == 0 )
            {
                MessageBox.Show(connection.Lang_id == 2 ? "يرجى أختيار مدينة التسليم" : "Please select the delivery city", MyGeneral_Lib.LblCap);
                button3.Enabled = true;
                return;
            }


            if (chk_trans_flag.Checked == true)
            { chk_tran_flag = 1; }
            else { chk_tran_flag = 0; }

            #endregion

            string Sbirth_Date = Txt_Sbirth_Date.Value.ToString("yyyy/MM/dd");
            string doc_S_date_ida = Txt_Doc_S_Date.Value.ToString("yyyy/MM/dd");
            string Doc_S_Exp = Txt_Doc_S_Exp.Value.ToString("yyyy/MM/dd");


            string sen = connection.SQLDS.Tables["per_info_tbl"].Rows.Count > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Sender.Text.Trim();
            string rec = Grd_CustRec_Name.RowCount > 0 ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang_rec == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim();
            string mv_notes = " :ح-مر" + sen + "," + " :مس " + rec;

            Int16 Cbo_agent_Comm_INOUT = 0;

            if (Cbo_agent_comm_type.SelectedIndex == 0) // مقبوضة او مدفوعة 
            { Cbo_agent_Comm_INOUT = 1; }// مقبوضة
            else
            { Cbo_agent_Comm_INOUT = -1; } // مدفوعة

            button3.Enabled = false;

            if (TYPE_CBO_OUT_IN.SelectedIndex != 1) //--في حاله الصادر فقط
            {
                //--------------------
                try
                {
                    connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
                    Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

                    if (cond_bl_rem != 0)//في حال اختيار نوع البحث بلا من واجهة السيتنك فلا يبحث في قوائم البحث 
                    {

                        DataTable Per_blacklist_tbl = new DataTable();
                        string[] Column4 = { "Per_AName", "Per_EName" };
                        string[] DType4 = { "System.String", "System.String" };

                        Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                        Per_blacklist_tbl.Rows.Clear();

                        Per_blacklist_tbl.Rows.Add(Grd_CustSen_Name.Rows.Count > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Sender.Text.Trim(), Txt_another_Sen.Text.Trim());
                        Per_blacklist_tbl.Rows.Add(Grd_CustRec_Name.Rows.Count > 0 ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim(), Txt_another_Rec.Text.Trim());
                        try
                        {
                            Int16 snat = Convert.ToInt16(((DataRowView)binding_cb_snat.Current).Row["nat_Id"]);
                            Int16 rnat = Convert.ToInt16(((DataRowView)binding_cb_rnat.Current).Row["nat_Id"]);
                            Int16 sa_coun = Convert.ToInt16(((DataRowView)binding_cbo_scity_con.Current).Row["con_id"]);
                            Int16 r_coun = Convert.ToInt16(((DataRowView)binding_cbo_rcity_con.Current).Row["con_id"]);
                            Int16 s_job = Convert.ToInt16(((DataRowView)binding_cmb_job_sender.Current).Row["job_ID"]);
                            Int16 sfrm_doc = Convert.ToInt16(((DataRowView)binding_sfmd.Current).Row["Fmd_id"]);

                            connection.SQLCS.Open();
                            connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                            connection.SQLCMD.Connection = connection.SQLCS;
                            connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                            connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)binding_cb_snat.Current).Row["nat_Id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", ((DataRowView)binding_cb_rnat.Current).Row["nat_Id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)binding_cbo_scity_con.Current).Row["con_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", ((DataRowView)binding_cbo_rcity_con.Current).Row["con_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@s_job", ((DataRowView)binding_cmb_job_sender.Current).Row["job_ID"]);
                            connection.SQLCMD.Parameters.AddWithValue("@r_job", 0);
                            connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", 0);
                            connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)binding_sfmd.Current).Row["Fmd_id"]);


                            IDataReader obj = connection.SQLCMD.ExecuteReader();
                            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                            obj.Close();
                            connection.SQLCS.Close();
                            connection.SQLCMD.Parameters.Clear();
                            connection.SQLCMD.Dispose();

                        }
                        catch
                        {
                            connection.SQLCS.Close();
                            connection.SQLCMD.Parameters.Clear();
                            connection.SQLCMD.Dispose();

                        }
                        if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                        {
                            Rem_Blacklist_confirm_ok AddFrm = new Rem_Blacklist_confirm_ok();
                            AddFrm.ShowDialog(this);
                            if (Rem_Blacklist_confirm_ok.Back_btn == 1)
                            {
                                Get_Black_list();
                                MessageBox.Show(connection.Lang_id == 1 ? "تم رفض الحوالة" : "Rimmetance has been refused", MyGeneral_Lib.LblCap);
                                button3.Enabled = true;
                                this.Close();
                                return;
                            }
                        }
                        else
                        {
                            Rem_Blacklist_confirm_ok.Back_btn = 0;
                            Rem_Blacklist_confirm_ok.Notes_BL = "";
                        }

                    }
                }

                catch { }
            }
            if (Cbo_type.SelectedIndex == 1)
            {

                ArrayList ItemList1 = new ArrayList();

                ItemList1.Insert(0, Convert.ToInt32(((DataRowView)binding_rcur.Current).Row["cur_id"]));
                ItemList1.Insert(1, Convert.ToDecimal(Txtr_amount.Text));//----@for_amount
                ItemList1.Insert(2, sen);
                ItemList1.Insert(3, Txt_S_Phone.Text.Trim());
                ItemList1.Insert(4, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_tcity_id.Current).Row["Cit_ID"] : Convert.ToInt32(((DataRowView)binding_cbo_city.Current).Row["CITY_ID"]));////////////////fffffff S_city_id
                ItemList1.Insert(5, TxtS_State.Text.Trim() + '/' + txtS_Suburb.Text.Trim() + '/' + Txts_street.Text.Trim() + '/' + TxtS_Post_Code.Text.Trim());
                ItemList1.Insert(6, Txt_S_Doc_No.Text.Trim());
                ItemList1.Insert(7, doc_S_date_ida);
                ItemList1.Insert(8, Txt_Doc_S_Exp.Checked == false ? S_doc_exp_null : Doc_S_Exp);
                ItemList1.Insert(9, Txt_Doc_S_Issue.Text.Trim());
                ItemList1.Insert(10, ((DataRowView)binding_sfmd.Current).Row["fmd_id"]);//----@sfrm_doc_id
                ItemList1.Insert(11, ((DataRowView)binding_cb_snat.Current).Row["Nat_id"]);//-----@snat_id
                ItemList1.Insert(12, rec);
                ItemList1.Insert(13, Txt_R_Phone.Text.Trim());
                ItemList1.Insert(14, ((DataRowView)binding_cbo_rcity_con.Current).Row["cit_id"]);//----@r_city_id;   binding_cbo_rcity_con           
                ItemList1.Insert(15, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? Convert.ToInt32(((DataRowView)binding_cbo_city.Current).Row["CITY_ID"]) : ((DataRowView)binding_tcity_id.Current).Row["Cit_ID"]);//-----@T_city_id ////////////////fffffff S_city_id
                ItemList1.Insert(16, Txtr_State.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());
                ItemList1.Insert(17, ((DataRowView)binding_cb_rnat.Current).Row["nat_id"]);//----@rnat_id  binding_cb_rnat
                ItemList1.Insert(18, Txt_T_Purpose.Text.Trim());
                ItemList1.Insert(19, Txt_Notes.Text.Trim());
                ItemList1.Insert(20, Convert.ToInt32(TxtIn_Rec_Date.Text));//txt_Date.Tag              
                ItemList1.Insert(21, 0);
                ItemList1.Insert(22, 0);
                ItemList1.Insert(23, mv_notes.Trim());
                ItemList1.Insert(24, Sbirth_Date);
                ItemList1.Insert(25, ((DataRowView)binding_pcur.Current).Row["cur_id"]);//----@Pr_cur_id-----//////////!!!
                ItemList1.Insert(26, connection.user_id);//login_id
                ItemList1.Insert(27, 0);//cust_id_online             
                //ItemList1.Insert(28, ((DataRowView)cur_comm.Current).Row["cur_id"]);//------@r_ccur_id  cur_comm
                //ItemList1.Insert(27, Convert.ToDecimal(Txt_Com_Amount.Text));
                ItemList1.Insert(28, 0);// @rem_amount_exch----cur_comm
                ItemList1.Insert(29, 0);// @com_amount_exch----cur_comm
                ItemList1.Insert(30, 0);//-----@cur_amount_exch  binding_cur_bsrem
                ItemList1.Insert(31, 0);//----@cur_comm_exch  binding_cur_bscomm
                ItemList1.Insert(32, Convert.ToDecimal(Txt_ExRate_Rem.Text));
                ItemList1.Insert(33, Txt_S_Birth_Place.Text.Trim());
                ItemList1.Insert(34, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_ID"]);//------@sa_city_id  binding_cbo_scity_con
                ItemList1.Insert(35, Convert.ToInt32(((DataRowView)binding_cmb_job_sender.Current).Row["Job_ID"]));
                ItemList1.Insert(36, connection.Lang_id);
                ItemList1.Insert(37, Txt_Soruce_money.Text.Trim());
                ItemList1.Insert(38, Txt_Relionship.Text.Trim());
                ItemList1.Insert(39, Convert.ToInt32(((DataRowView)binding_cmb_job_receiver.Current).Row["Job_ID"]));
                ItemList1.Insert(40, Cmb_Case_Purpose.SelectedValue);
                ItemList1.Insert(41, ((DataRowView)binding_rcur.Current).Row["Cur_aNAME"]);//----@R_ACUR_NAME              
                ItemList1.Insert(42, ((DataRowView)binding_rcur.Current).Row["Cur_ENAME"]);//----@R_ECUR_NAME              
                ItemList1.Insert(43, ((DataRowView)binding_pcur.Current).Row["Cur_ANAME"]);//---@PR_ACUR_NAME  "ACur_Name_pay" : "eCur_Name_pay";             
                ItemList1.Insert(44, ((DataRowView)binding_pcur.Current).Row["Cur_ENAME"]);//----PR_eCUR_NAME               
                ItemList1.Insert(45, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_tcity_id.Current).Row["acity_name"] : ((DataRowView)binding_cbo_city.Current).Row["Cit_AName"]);//----@S_ACITY_NAME   مدينة الاصدار  "acity_name" : "ecity_name";               
                ItemList1.Insert(46, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_tcity_id.Current).Row["ecity_name"] : ((DataRowView)binding_cbo_city.Current).Row["Cit_EName"]);//----@S_eCITY_NAME               
                ItemList1.Insert(47, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_tcity_id.Current).Row["Con_AName"] : ((DataRowView)binding_cbo_city.Current).Row["Con_AName"]);//----@@S_ACOUN_NAME               
                ItemList1.Insert(48, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_tcity_id.Current).Row["Con_EName"] : ((DataRowView)binding_cbo_city.Current).Row["Con_EName"]);//----@@S_ECOUN_NAME
                ItemList1.Insert(49, Txts_street.Text.Trim());//---@S_Street
                ItemList1.Insert(50, txtS_Suburb.Text.Trim()); //--@S_Suburb
                ItemList1.Insert(51, TxtS_Post_Code.Text.Trim());//----@S_Post_Code
                ItemList1.Insert(52, TxtS_State.Text.Trim());//----@S_State
                ItemList1.Insert(53, ((DataRowView)binding_sfmd.Current).Row["Fmd_AName"]);//---@sFRM_ADOC_NA  binding_sfmd
                ItemList1.Insert(54, ((DataRowView)binding_sfmd.Current).Row["Fmd_eName"]);//---@sFRM_eDOC_NA  binding_sfmd
                ItemList1.Insert(55, ((DataRowView)binding_cb_snat.Current).Row["Nat_AName"]);//---@@s_A_NAT_NAME  binding_sfmd
                ItemList1.Insert(56, ((DataRowView)binding_cb_snat.Current).Row["Nat_eName"]);//---@@s_e_NAT_NAME  binding_sfmd
                ItemList1.Insert(57, ((DataRowView)binding_cbo_rcity_con.Current).Row["Cit_AName"]);//---@r_ACITY_NAME 
                ItemList1.Insert(58, ((DataRowView)binding_cbo_rcity_con.Current).Row["Cit_eName"]);//---@r_ECITY_NAME
                ItemList1.Insert(59, ((DataRowView)binding_cbo_rcity_con.Current).Row["Con_AName"]);//---@r_ACOUN_NAME
                ItemList1.Insert(60, ((DataRowView)binding_cbo_rcity_con.Current).Row["Con_eName"]);//---@r_eCOUN_NAME
                ItemList1.Insert(61, Txtr_Street.Text.Trim());//---@r_Street
                ItemList1.Insert(62, Txtr_Suburb.Text.Trim());//---@r_Suburb
                ItemList1.Insert(63, Txtr_Post_Code.Text.Trim());//---@@r_Post_Code
                ItemList1.Insert(64, Txtr_State.Text.Trim());//---@r_State
                ItemList1.Insert(65, "");//---@rFRM_ADOC_NA
                ItemList1.Insert(66, "");//---@rFRM_EDOC_NA
                ItemList1.Insert(67, ((DataRowView)binding_cb_rnat.Current).Row["Nat_AName"]);//---@@r_A_NAT_NAME
                ItemList1.Insert(68, ((DataRowView)binding_cb_rnat.Current).Row["Nat_EName"]);//---@@r_E_NAT_NAME
                ItemList1.Insert(69, "101");//----@ACASE_NA  binding_Cmb_Case_Purpose
                ItemList1.Insert(70, "101");//----@eCASE_NA  binding_Cmb_Case_Purpose
                ItemList1.Insert(71, "");//---@@r_CCur_ACUR_NAME
                ItemList1.Insert(72, "");//---@@r_CCur_ECUR_NAME
                ItemList1.Insert(73, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_cbo_city.Current).Row["Cit_AName"] : ((DataRowView)binding_tcity_id.Current).Row["acity_name"]);//-----@@t_ACITY_NAME   binding_tcity_id
                ItemList1.Insert(74, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_cbo_city.Current).Row["Cit_EName"] : ((DataRowView)binding_tcity_id.Current).Row["ecity_name"]);//-----@@t_eCITY_NAME   binding_tcity_id
                ItemList1.Insert(75, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_AName"]);//-----@@@Sa_ACITY_NAME   binding_cbo_scity_con
                ItemList1.Insert(76, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_eName"]);//-----@@@Sa_ECITY_NAME   binding_cbo_scity_con
                ItemList1.Insert(77, ((DataRowView)binding_cbo_scity_con.Current).Row["Con_AName"]);//-----@Sa_ACOUN_NAME   binding_cbo_scity_con
                ItemList1.Insert(78, ((DataRowView)binding_cbo_scity_con.Current).Row["Con_eName"]);//-----@Sa_ECOUN_NAME   binding_cbo_scity_con
                ItemList1.Insert(79, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_cbo_city.Current).Row["Con_AName"] : ((DataRowView)binding_tcity_id.Current).Row["Con_AName"]);//-----@t_ACOUN_NAME   binding_tcity_id
                ItemList1.Insert(80, TYPE_CBO_OUT_IN.SelectedIndex == 1 ? ((DataRowView)binding_cbo_city.Current).Row["Con_EName"] : ((DataRowView)binding_tcity_id.Current).Row["Con_EName"]);//-----@t_ECOUN_NAME   binding_tcity_id
                ItemList1.Insert(81, ((DataRowView)binding_Cmb_Case_Purpose.Current).Row["Case_purpose_Aname"]);//----@@Case_purpose_Aname  binding_Cmb_Case_Purpose
                ItemList1.Insert(82, ((DataRowView)binding_Cmb_Case_Purpose.Current).Row["Case_purpose_ename"]);//----@@Case_purpose_ename  binding_Cmb_Case_Purpose
                ItemList1.Insert(83, Convert.ToString(Cmb_Code_phone_S.SelectedValue));//----@Sender_Code_phone
                ItemList1.Insert(84, Convert.ToString(Cmb_phone_Code_R.SelectedValue));//----@@Receiver_Code_phone
                ItemList1.Insert(85, connection.User_Name);
                ItemList1.Insert(86, connection.T_ID);
                ItemList1.Insert(87, 0);//Sub_Cust_ID
                ItemList1.Insert(88, 1);
                //ItemList1.Insert(90, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);// @rem_amount_real----cur_comm
                //ItemList1.Insert(91, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_comm.Text) : 0);// @rem_amount_real----cur_comm
                //ItemList1.Insert(92, connection.Loc_Cur_Id);            
                ItemList1.Insert(89, 0);//comm_type
                ItemList1.Insert(90, Grd_CustSen_Name.RowCount > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row["per_id"] : 0);//----@S_per_id
                ItemList1.Insert(91, Grd_CustRec_Name.RowCount > 0 ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_id"] : 0);//----@R_per_id
                ItemList1.Insert(92, Txt_another_Sen.Text.Trim()); //another    
                ItemList1.Insert(93, Txt_another_Rec.Text.Trim());//    
                ItemList1.Insert(94, Txt_mail.Text.Trim());
                ItemList1.Insert(95, txt_Mother_name.Text.Trim());
                ItemList1.Insert(96, cmb_Gender_id.SelectedValue);
                ItemList1.Insert(97, connection.user_id);
                ItemList1.Insert(98, 0);
                ItemList1.Insert(99, "");
                ItemList1.Insert(100, "");
                ItemList1.Insert(101, 0); //---VO_No
                ItemList1.Insert(102, 0); //---Code_rem
                ItemList1.Insert(103, ""); //---Case_Date  
                ItemList1.Insert(104, "");
                ItemList1.Insert(105, ((DataRowView)binding_Gender_id.Current).Row["Gender_Aname"]);
                ItemList1.Insert(106, ((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]);
                ItemList1.Insert(107, 1);
                ItemList1.Insert(108, chk_Agent.Checked == true ? 1 : 0);
                ItemList1.Insert(109, Convert.ToInt16(((DataRowView)cur_comm.Current).Row["cur_id"]));
                ItemList1.Insert(110, Convert.ToDecimal(Txt_Com_Amount.Text));
                ItemList1.Insert(111, ((DataRowView)cur_comm.Current).Row["cur_AName"]);
                ItemList1.Insert(112, ((DataRowView)cur_comm.Current).Row["Cur_EName"]);
                ItemList1.Insert(113, Grd_procer.RowCount > 0 ? Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]) : 0);
                ItemList1.Insert(114, Grd_procer.RowCount > 0 ? ((DataRowView)_BS_sub_cust.Current).Row["ASub_CustName"] : "");
                ItemList1.Insert(115, Grd_procer.RowCount > 0 ? ((DataRowView)_BS_sub_cust.Current).Row["ESub_CustNmae"] : "");
                ItemList1.Insert(116, chk_Agent.Checked == true && Txt_comm_agent.Text != "0.000" ? Convert.ToInt16(((DataRowView)_BS_curr.Current).Row["cur_id"]) : 0);
                ItemList1.Insert(117, chk_Agent.Checked == true ? Convert.ToDecimal(Txt_comm_agent.Text) : 0);
                ItemList1.Insert(118, chk_Agent.Checked == true && Txt_comm_agent.Text != "0.000" ? ((DataRowView)_BS_curr.Current).Row["Cur_ANAME"] : "");
                ItemList1.Insert(119, chk_Agent.Checked == true && Txt_comm_agent.Text != "0.000" ? ((DataRowView)_BS_curr.Current).Row["Cur_ENAME"] : "");
                ItemList1.Insert(120, chk_Agent.Checked == true ? Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"]) : 0);
                ItemList1.Insert(121, chk_Agent.Checked == true ? ((DataRowView)_BS_sub_agent.Current).Row["ASub_CustName"] : "");
                ItemList1.Insert(122, chk_Agent.Checked == true ? ((DataRowView)_BS_sub_agent.Current).Row["ESub_CustNmae"] : "");
                ItemList1.Insert(123, Cbo_agent_Comm_INOUT);
                ItemList1.Insert(124, checkBox1.Checked == true ? 1 : 0);
                ItemList1.Insert(125, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);
                ItemList1.Insert(126, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_comm.Text) : 0);
                ItemList1.Insert(127, checkBox1.Checked == true ? Convert.ToDecimal(Txt_Tot_amount.Text) : 0);
                ItemList1.Insert(128, checkBox1.Checked == true ? param_Exch_rate_rem : 0);
                ItemList1.Insert(129, checkBox1.Checked == true ? param_Exch_rate_com : 0);
                ItemList1.Insert(130, TYPE_CBO_OUT_IN.SelectedIndex == 2 ? Convert.ToInt16(resd_cmb.SelectedValue) : Convert.ToInt16(resd_cbm_rec.SelectedValue));
                ItemList1.Insert(131, btnlang);
                ItemList1.Insert(132, btnlang_rec);
                ItemList1.Insert(133, Tex_Social_ID.Text.Trim());

                //MyGeneral_Lib.Copytocliptext("Add_rem_out_Voucher_daily", ItemList1);
                if (TYPE_CBO_OUT_IN.SelectedIndex == 2)//صادر
                {
                    ItemList1.Insert(134, chk_tran_flag);
                    ItemList1.Insert(135, Txt_S_details_job.Text.Trim() != "" ? Txt_S_details_job.Text : "");
                    ItemList1.Insert(136, Convert.ToInt16(cmb_comm_type.SelectedValue));
                    ItemList1.Insert(137, 0);//@Txt_Add_Com_Amount
                    ItemList1.Insert(138, 0);//Comm_Rem_ID
                    ItemList1.Insert(139, Rem_Blacklist_confirm_ok.Notes_BL);
                    ItemList1.Insert(140, 0);// نظام ويندوز
                    ItemList1.Insert(141, 0);// txنظام ويندوز
                    ItemList1.Insert(142, 0);
                    ItemList1.Insert(143, 0);
                    ItemList1.Insert(144, 0);
                    ItemList1.Insert(145, "");
                    ItemList1.Insert(146, "");
                    ItemList1.Insert(147, 0);

                    connection.scalar("Add_rem_out_Voucher_daily", ItemList1);
                }
                if (TYPE_CBO_OUT_IN.SelectedIndex == 1)//وارد
                {
                    ItemList1.Insert(134, Rem_Blacklist_confirm_ok.Notes_BL);// 

                    connection.scalar("Add_rem_outin_Voucher_daily", ItemList1);
                }
                rem_no = connection.SQLCMD.Parameters["@Param_Result_rem_no"].Value.ToString();
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    button3.Enabled = true;
                    this.Enabled = true;
                    return;

                }
                if (connection.SQLCMD.Parameters["@Param_Result_err"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result_err"].Value.ToString(), MyGeneral_Lib.LblCap);
                    button3.Enabled = true;
                }

                print_Rpt_Pdf_NEW();
                button3.Enabled = true;
                connection.SQLCMD.Parameters.Clear();
                this.Close();
                //-------  
            }
            else
            {
                ArrayList ItemList1 = new ArrayList();

                ItemList1.Insert(0, Convert.ToInt32(((DataRowView)binding_rcur.Current).Row["cur_id"]));
                ItemList1.Insert(1, Convert.ToDecimal(Txtr_amount.Text));//----@for_amount
                ItemList1.Insert(2, sen);
                ItemList1.Insert(3, Txt_S_Phone.Text.Trim());
                ItemList1.Insert(4, Convert.ToInt32(((DataRowView)binding_cbo_city.Current).Row["CITY_ID"]));
                ItemList1.Insert(5, TxtS_State.Text.Trim() + '/' + txtS_Suburb.Text.Trim() + '/' + Txts_street.Text.Trim() + '/' + TxtS_Post_Code.Text.Trim());
                ItemList1.Insert(6, Txt_S_Doc_No.Text.Trim());
                ItemList1.Insert(7, doc_S_date_ida);
                ItemList1.Insert(8, Txt_Doc_S_Exp.Checked == false ? S_doc_exp_null : Doc_S_Exp);
                ItemList1.Insert(9, Txt_Doc_S_Issue.Text.Trim());
                ItemList1.Insert(10, ((DataRowView)binding_sfmd.Current).Row["fmd_id"]);//----@sfrm_doc_id
                ItemList1.Insert(11, ((DataRowView)binding_cb_snat.Current).Row["Nat_id"]);//-----@snat_id
                ItemList1.Insert(12, rec);
                ItemList1.Insert(13, Txt_R_Phone.Text.Trim());
                ItemList1.Insert(14, ((DataRowView)binding_cbo_rcity_con.Current).Row["cit_id"]);//----@r_city_id;   binding_cbo_rcity_con           
                ItemList1.Insert(15, ((DataRowView)binding_tcity_id.Current).Row["Cit_ID"]);//-----@T_city_id
                ItemList1.Insert(16, Txtr_State.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());
                ItemList1.Insert(17, ((DataRowView)binding_cb_rnat.Current).Row["nat_id"]);//----@rnat_id  binding_cb_rnat
                ItemList1.Insert(18, Txt_T_Purpose.Text.Trim());
                ItemList1.Insert(19, Txt_Notes.Text.Trim());
                ItemList1.Insert(20, Convert.ToInt32(TxtIn_Rec_Date.Text));//txt_Date.Tag              
                ItemList1.Insert(21, mv_notes.Trim());
                ItemList1.Insert(22, Sbirth_Date);
                ItemList1.Insert(23, ((DataRowView)binding_pcur.Current).Row["cur_id"]);//----@Pr_cur_id-----//////////!!!
                ItemList1.Insert(24, connection.user_id);//login_id
                ItemList1.Insert(25, connection.Cust_online_Id);//cust_id_online             
                ItemList1.Insert(26, ((DataRowView)cur_comm.Current).Row["cur_id"]);//------@r_ccur_id  cur_comm
                ItemList1.Insert(27, Convert.ToDecimal(Txt_Com_Amount.Text)); //في حال وجود عموله من الفرع
                ItemList1.Insert(28, checkBox1.Checked == true ? param_Exch_rate_rem : 0);// @rem_amount_exch----cur_comm
                ItemList1.Insert(29, checkBox1.Checked == true ? param_Exch_rate_com : 0);// @com_amount_exch----cur_comm
                ItemList1.Insert(30, checkBox1.Checked == true ? ((DataRowView)binding_cur_bsrem.Current).Row["for_cur_id"] : 0);//-----@cur_amount_exch  binding_cur_bsrem
                ItemList1.Insert(31, checkBox1.Checked == true ? ((DataRowView)binding_cur_bscomm.Current).Row["for_cur_id"] : 0);//----@cur_comm_exch  binding_cur_bscomm
                ItemList1.Insert(32, Convert.ToDecimal(Txt_ExRate_Rem.Text));
                ItemList1.Insert(33, Txt_S_Birth_Place.Text.Trim());
                ItemList1.Insert(34, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_ID"]);//------@sa_city_id  binding_cbo_scity_con
                ItemList1.Insert(35, "");   // Param _Result
                ItemList1.Insert(36, "");   // Param_Result_rem_no
                ItemList1.Insert(37, Convert.ToInt32(((DataRowView)binding_cmb_job_sender.Current).Row["Job_ID"]));
                ItemList1.Insert(38, connection.Lang_id);
                ItemList1.Insert(39, Txt_Soruce_money.Text.Trim());
                ItemList1.Insert(40, Txt_Relionship.Text.Trim());
                ItemList1.Insert(41, Convert.ToInt32(((DataRowView)binding_cmb_job_receiver.Current).Row["Job_ID"]));
                ItemList1.Insert(42, Cmb_Case_Purpose.SelectedValue);
                ItemList1.Insert(43, 0); //---VO_No
                ItemList1.Insert(44, 0); //---Code_rem
                ItemList1.Insert(45, ""); //---Case_Date             
                ItemList1.Insert(46, ((DataRowView)binding_rcur.Current).Row["Cur_aNAME"]);//----@R_ACUR_NAME              
                ItemList1.Insert(47, ((DataRowView)binding_rcur.Current).Row["Cur_ENAME"]);//----@R_ECUR_NAME              
                ItemList1.Insert(48, ((DataRowView)binding_pcur.Current).Row["Cur_ANAME"]);//---@PR_ACUR_NAME  "ACur_Name_pay" : "eCur_Name_pay";             
                ItemList1.Insert(49, ((DataRowView)binding_pcur.Current).Row["Cur_ENAME"]);//----PR_eCUR_NAME               
                ItemList1.Insert(50, ((DataRowView)binding_cbo_city.Current).Row["Cit_AName"]);//----@S_ACITY_NAME   مدينة الاصدار  "acity_name" : "ecity_name";               
                ItemList1.Insert(51, ((DataRowView)binding_cbo_city.Current).Row["Cit_EName"]);//----@S_eCITY_NAME               
                ItemList1.Insert(52, ((DataRowView)binding_cbo_city.Current).Row["Con_AName"]);//----@@S_ACOUN_NAME               
                ItemList1.Insert(53, ((DataRowView)binding_cbo_city.Current).Row["Con_EName"]);//----@@S_ECOUN_NAME
                ItemList1.Insert(54, Txts_street.Text.Trim());//---@S_Street
                ItemList1.Insert(55, txtS_Suburb.Text.Trim()); //--@S_Suburb
                ItemList1.Insert(56, TxtS_Post_Code.Text.Trim());//----@S_Post_Code
                ItemList1.Insert(57, TxtS_State.Text.Trim());//----@S_State
                ItemList1.Insert(58, ((DataRowView)binding_sfmd.Current).Row["Fmd_AName"]);//---@sFRM_ADOC_NA  binding_sfmd
                ItemList1.Insert(59, ((DataRowView)binding_sfmd.Current).Row["Fmd_eName"]);//---@sFRM_eDOC_NA  binding_sfmd
                ItemList1.Insert(60, ((DataRowView)binding_cb_snat.Current).Row["Nat_AName"]);//---@@s_A_NAT_NAME  binding_sfmd
                ItemList1.Insert(61, ((DataRowView)binding_cb_snat.Current).Row["Nat_eName"]);//---@@s_e_NAT_NAME  binding_sfmd
                ItemList1.Insert(62, ((DataRowView)binding_cbo_rcity_con.Current).Row["Cit_AName"]);//---@r_ACITY_NAME 
                ItemList1.Insert(63, ((DataRowView)binding_cbo_rcity_con.Current).Row["Cit_eName"]);//---@r_ECITY_NAME
                ItemList1.Insert(64, ((DataRowView)binding_cbo_rcity_con.Current).Row["Con_AName"]);//---@r_ACOUN_NAME
                ItemList1.Insert(65, ((DataRowView)binding_cbo_rcity_con.Current).Row["Con_eName"]);//---@r_eCOUN_NAME
                ItemList1.Insert(66, Txtr_Street.Text.Trim());//---@r_Street
                ItemList1.Insert(67, Txtr_Suburb.Text.Trim());//---@r_Suburb
                ItemList1.Insert(68, Txtr_Post_Code.Text.Trim());//---@@r_Post_Code
                ItemList1.Insert(69, Txtr_State.Text.Trim());//---@r_State
                ItemList1.Insert(70, "");//---@rFRM_ADOC_NA
                ItemList1.Insert(71, "");//---@rFRM_EDOC_NA
                ItemList1.Insert(72, ((DataRowView)binding_cb_rnat.Current).Row["Nat_AName"]);//---@@r_A_NAT_NAME
                ItemList1.Insert(73, ((DataRowView)binding_cb_rnat.Current).Row["Nat_EName"]);//---@@r_E_NAT_NAME
                ItemList1.Insert(74, "101");//----@ACASE_NA  binding_Cmb_Case_Purpose
                ItemList1.Insert(75, "101");//----@eCASE_NA  binding_Cmb_Case_Purpose
                ItemList1.Insert(76, ((DataRowView)cur_comm.Current).Row["Cur_AName"]);//---@@r_CCur_ACUR_NAME
                ItemList1.Insert(77, ((DataRowView)cur_comm.Current).Row["Cur_eName"]);//---@@r_CCur_ECUR_NAME
                ItemList1.Insert(78, ((DataRowView)binding_tcity_id.Current).Row["acity_name"]);//-----@@t_ACITY_NAME   binding_tcity_id
                ItemList1.Insert(79, ((DataRowView)binding_tcity_id.Current).Row["ecity_name"]);//-----@@t_eCITY_NAME   binding_tcity_id
                ItemList1.Insert(80, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_AName"]);//-----@@@Sa_ACITY_NAME   binding_cbo_scity_con
                ItemList1.Insert(81, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_eName"]);//-----@@@Sa_ECITY_NAME   binding_cbo_scity_con
                ItemList1.Insert(82, ((DataRowView)binding_cbo_scity_con.Current).Row["Con_AName"]);//-----@Sa_ACOUN_NAME   binding_cbo_scity_con
                ItemList1.Insert(83, ((DataRowView)binding_cbo_scity_con.Current).Row["Con_eName"]);//-----@Sa_ECOUN_NAME   binding_cbo_scity_con
                ItemList1.Insert(84, ((DataRowView)binding_tcity_id.Current).Row["Con_AName"]);//-----@t_ACOUN_NAME   binding_tcity_id
                ItemList1.Insert(85, ((DataRowView)binding_tcity_id.Current).Row["Con_EName"]);//-----@t_ECOUN_NAME   binding_tcity_id
                ItemList1.Insert(86, ((DataRowView)binding_Cmb_Case_Purpose.Current).Row["Case_purpose_Aname"]);//----@@Case_purpose_Aname  binding_Cmb_Case_Purpose
                ItemList1.Insert(87, ((DataRowView)binding_Cmb_Case_Purpose.Current).Row["Case_purpose_ename"]);//----@@Case_purpose_ename  binding_Cmb_Case_Purpose
                ItemList1.Insert(88, connection.T_ID);
                ItemList1.Insert(89, connection.Box_Cust_Id);
                ItemList1.Insert(90, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);// @rem_amount_real----cur_comm
                ItemList1.Insert(91, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_comm.Text) : 0);// @rem_amount_real----cur_comm
                ItemList1.Insert(92, connection.Loc_Cur_Id);
                ItemList1.Insert(93, 1);
                ItemList1.Insert(94, Convert.ToInt16(cmb_comm_type.SelectedValue));
                ItemList1.Insert(95, connection.login_Id);//login_id
                ItemList1.Insert(96, Grd_CustSen_Name.RowCount > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row["per_id"] : 0);//----@S_per_id
                ItemList1.Insert(97, Grd_CustRec_Name.RowCount > 0 ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_id"] : 0);//----@R_per_id
                ItemList1.Insert(98, connection.User_Name);
                ItemList1.Insert(99, Convert.ToByte(checkBox1.Checked));
                ItemList1.Insert(100, Convert.ToDecimal(Txt_Tot_amount.Text));
                ItemList1.Insert(101, 0);//Convert.ToDecimal(TxtDiscount_Amount.Text)تم الرفع
                //ItemList1.Insert(102, Txt_another.Text.Trim());
                //ItemList1.Insert(103, Txt_another_rec.Text.Trim());
                ItemList1.Insert(102, Txt_mail.Text.Trim());
                ItemList1.Insert(103, txt_Mother_name.Text.Trim());
                ItemList1.Insert(104, cmb_Gender_id.SelectedValue);
                ItemList1.Insert(105, Convert.ToString(Cmb_Code_phone_S.SelectedValue));//----@Sender_Code_phone
                ItemList1.Insert(106, Convert.ToString(Cmb_phone_Code_R.SelectedValue));//----@@Receiver_Code_phone
                ItemList1.Insert(107, 0);
                ItemList1.Insert(108, "");
                ItemList1.Insert(109, ((DataRowView)binding_Gender_id.Current).Row["Gender_Aname"]);
                ItemList1.Insert(110, ((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]);
                ItemList1.Insert(111, 1);
                ItemList1.Insert(112, chk_Agent.Checked == true ? 1 : 0);
                ItemList1.Insert(113, chk_Agent.Checked == true && Txt_comm_agent.Text != "0.000" ? Convert.ToInt16(((DataRowView)_BS_curr.Current).Row["cur_id"]) : 0);
                ItemList1.Insert(114, chk_Agent.Checked == true ? Convert.ToDecimal(Txt_comm_agent.Text) : 0);
                ItemList1.Insert(115, chk_Agent.Checked == true && Txt_comm_agent.Text != "0.000" ? ((DataRowView)_BS_curr.Current).Row["Cur_ANAME"] : "");
                ItemList1.Insert(116, chk_Agent.Checked == true && Txt_comm_agent.Text != "0.000" ? ((DataRowView)_BS_curr.Current).Row["Cur_ENAME"] : "");
                ItemList1.Insert(117, chk_Agent.Checked == true ? Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"]) : 0);
                ItemList1.Insert(118, chk_Agent.Checked == true ? ((DataRowView)_BS_sub_agent.Current).Row["ASub_CustName"] : "");
                ItemList1.Insert(119, chk_Agent.Checked == true ? ((DataRowView)_BS_sub_agent.Current).Row["ESub_CustNmae"] : "");
                ItemList1.Insert(120, Cbo_agent_Comm_INOUT);
                ItemList1.Insert(121, Convert.ToInt16(resd_cmb.SelectedValue));
                ItemList1.Insert(122, btnlang);
                ItemList1.Insert(123, btnlang_rec);
                ItemList1.Insert(124, Tex_Social_ID.Text.Trim());
                ItemList1.Insert(125, Txt_S_details_job.Text.Trim() != "" ? Txt_S_details_job.Text : "");
                ItemList1.Insert(126, "");
                ItemList1.Insert(127, 0); //@Add_Com_Amount 
                ItemList1.Insert(128, 0);//@Comm_Rem_ID
                ItemList1.Insert(129, Rem_Blacklist_confirm_ok.Notes_BL);// 
                ItemList1.Insert(130, Txt_another_Sen.Text.Trim());// 
                ItemList1.Insert(131, Txt_another_Rec.Text.Trim());// 

                MyGeneral_Lib.Copytocliptext("Add_rem_out_Voucher", ItemList1);
                connection.scalar("Add_rem_out_Voucher", ItemList1);
                rem_no = connection.SQLCMD.Parameters["@Param_Result_rem_no"].Value.ToString();
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);//
                    connection.SQLCMD.Parameters.Clear();
                    this.Enabled = true;
                    return;

                }
                if (connection.SQLCMD.Parameters["@Param_Result_err"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result_err"].Value.ToString(), MyGeneral_Lib.LblCap);
                    button3.Enabled = true;

                }

                print_Rpt_Pdf_NEW();
                button3.Enabled = true;
                connection.SQLCMD.Parameters.Clear();
                this.Close();

            }
        }
        //--------------------------------------------------------
        private void print_Rpt_Pdf_NEW()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int int_nrecdate = 0;
            string Frm_id = "";
            User = TxtUser.Text;
            string phone = "";
            int procker = 0;
            string prockr_name = "";
            string procker_ename = "";
            int Paid = 0;
            string Paid_name = "";
            string Paid_ename = "";
            string local_Cur1 = "";
            string local_ECur1 = "";
            string forgin_Cur1 = "";
            string forgin_ECur1 = "";
            string Com_ToWord = "";
            string Com_ToEWord = "";
            double com_amount = 0;
            string com_Cur_code = "";
            byte chk_s_ocomm = 0;
            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
           
            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();


            Exch_rate_com = Txt_ExRate_Rem.Text;
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);


            string SqlTxt = "Exec Report_Rem_Information_new" + "'" + rem_no + "'," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Rem_Info");

            string sql_report_txt = "select * from Reports_setting_Tbl";
            connection.SqlExec(sql_report_txt, "Reports_setting_Tbl");
            if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار اعدادات الطباعة" : "please add reporting setting", MyGeneral_Lib.LblCap);
                return;
            }
            else
            {
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
                }
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]) != 0)
                {
                    Paid = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]);
                    Paid_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_aname"].ToString();
                    Paid_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_ename"].ToString();
                }

                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_Aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_ename"].ToString();
                }


                local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
                local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

                forgin_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Cur_Code"].ToString();
                forgin_Cur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ACUR_NAME"].ToString();
                forgin_ECur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ECUR_NAME"].ToString();



                Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();
                s_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_Ejob"].ToString();
                r_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_Ejob"].ToString();
                S_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["S_Social_No"].ToString();
                R_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Social_No"].ToString();

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                    {
                        com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]);
                        com_Cur_code = connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_code"].ToString();
                        ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                        Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                    }
                }
                else// عملة محلية
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                    {
                        com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]);
                        com_Cur_code = local_Cur;
                        ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                        Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                    }
                }

                Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();


                term = TxtTerm_Name.Text;

                //if (TYPE_CBO_OUT_IN.SelectedIndex == 2) // صادر
                //{
                //    if
                //      ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) == 0) ||
                //       (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) == 0))
                //    {
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //        {
                //            Oper_Id = 3;

                //        }
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //        {
                //            Oper_Id = 6;
                //        }
                //    }

                //    if
                //       ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0) ||
                //        (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0))
                //    {
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //        {
                //            Oper_Id = 27;

                //        }
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //        {
                //            Oper_Id = 33;
                //        }
                //    }
                //}
                //if (TYPE_CBO_OUT_IN.SelectedIndex == 1) //وارد
                //{ Oper_Id = 17; }

                Oper_Id = Convert.ToInt16(connection.SQLDS.Tables["Rem_Info"].Rows[0]["oper_id"]);
                decimal txt_comm_amount = 0;

                chk_s_ocomm = 0;
                txt_comm_amount = 0;

                try
                {

                    Add_Rem_cash_daily_rpt_1 ObjRpt = new Add_Rem_cash_daily_rpt_1();
                    Add_Rem_cash_daily_rpt_eng_1 ObjRptEng = new Add_Rem_cash_daily_rpt_eng_1();


                    DataTable Dt_Param = CustomControls.CustomParam_Dt();
                    Dt_Param.Rows.Add("_Date", _Date);
                    Dt_Param.Rows.Add("local_Cur", local_Cur);
                    Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                    Dt_Param.Rows.Add("Vo_No", Vo_No);
                    Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                    Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                    Dt_Param.Rows.Add("User", User);
                    Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                    Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                    Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                    Dt_Param.Rows.Add("term", term);
                    Dt_Param.Rows.Add("Frm_id", Frm_id);
                    Dt_Param.Rows.Add("phone", phone);
                    Dt_Param.Rows.Add("com_cur", com_Cur_code);
                    Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                    Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                    Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                    Dt_Param.Rows.Add("procker", procker);
                    Dt_Param.Rows.Add("procker_ename", procker_ename);
                    Dt_Param.Rows.Add("prockr_name", prockr_name);
                    Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                    Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                    Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                    Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                    Dt_Param.Rows.Add("Anote_report", Anote_report);
                    Dt_Param.Rows.Add("Enote_report", Enote_report);
                    Dt_Param.Rows.Add("chk_s_ocomm", chk_s_ocomm);
                    Dt_Param.Rows.Add("txt_comm_amount", txt_comm_amount);
                    Dt_Param.Rows.Add("Paid", Paid);
                    Dt_Param.Rows.Add("Paid_ename", Paid_ename);
                    Dt_Param.Rows.Add("Paid_name", Paid_name);

                    connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
                    connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);
                }
                catch
                { }
            }
        }
        //------------------------------------------------------------------------
        private void Add_rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_scity = false;
            chang_rcity = false;
            chang_sender = false;
            chang_rec = false;
            chang_cur_rrate = false;
            chang_cur_crate = false;
            string[] Used_Tbl = { "Full_information_tab" ,"Full_information_tab1" ,"Full_information_tab2" ,"Full_information_tab3" ,"Full_information_tab4" ,"Full_information_tab5" , 
                                     "Full_information_tab6" ,"Full_information_tab7" ,"Get_info_Tbl" ,"Get_info_Tbl1" ,"Get_info_Tbl2","Get_info_Tbl3" , "Sub_cust_tbl", "Sub_cust_tbl1", "Sub_cust_tbl2", "Cur_Buy_Sal_Tbl", 
                                    "Cust_tbl", "per_info_tbl", "per_info_tbl1", "Full_information_tab","per_info_tbl_rec" ,"Sub_cust_tbl","Get_comm_online_tbl","Dt_comm",
                                    "Cur_loc_tbl" , "Rem_Info" , "Is_Archive_tbl","Sub_agent_tbl","Sub_Cust_tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
            Del_Btn = 0;
        }
        //------------------------------------------------------------------------
        private void Txtr_amount_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                Txt_Rem_Amount.Text = Txtr_amount.Text;
                Txt_Rem_Amount_TextChanged(null, null);
            }

        }
        //------------------------------------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------------------------------------------------
        private void Txt_Sbirth_Date_Enter(object sender, EventArgs e)
        {
            this.Focus();
        }
        //------------------------------------------------------------------------
        private void Txt_Doc_S_Date_Enter(object sender, EventArgs e)
        {
            this.Focus();
        }
        //------------------------------------------------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1)// يملك خاصية رفع الوثائق
            {

                int Per_id = Convert.ToInt32(((DataRowView)binding_Grd_cust_sender.Current).Row["Per_id"]);
                string Per_AName = ((DataRowView)binding_Grd_cust_sender.Current).Row["Per_AName"].ToString();
                string Per_EName = ((DataRowView)binding_Grd_cust_sender.Current).Row["Per_EName"].ToString();
                Del_Btn = 1;

                Person_Document_main UpdFrm = new Person_Document_main(Per_AName, Per_id, Per_EName);
                //this.Visible = false;
                UpdFrm.ShowDialog(this);
                //this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " ", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1)// يملك خاصية رفع الوثائق
            {

                int Per_id = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_id"]);
                string Per_AName = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString();
                string Per_EName = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString();


                Del_Btn = 1;

                Person_Document_main UpdFrm = new Person_Document_main(Per_AName, Per_id, Per_EName);
                //this.Visible = false;
                UpdFrm.ShowDialog(this);
                //this.Visible = true;


            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " ", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //------------------------------------------------------------------------
        private void chk_Agent_CheckedChanged(object sender, EventArgs e)
        {
            if ((chk_Agent.Checked == true && Cbo_type.SelectedIndex == 1) || (chk_Agent.Checked == true && Cbo_type.SelectedIndex == 0))
            {
                TXt_agent_Change = true;
                Grd_agent_Change = true;
                Txt_Agent.Enabled = true;
                Grd_agent.Enabled = true;
                Txt_comm_agent.Enabled = true;
                Cbo_agent_cur.Enabled = true;
                Cbo_agent_comm_type.Enabled = true;
                chang_agent = true;
                Agent_name.Enabled = true;
                proc_Name.Enabled = true;

            }
            else
            {
                TXt_agent_Change = false;
                Txt_Agent.Text = "";
                Txt_Agent.Enabled = false;

                Grd_agent.DataSource = new BindingSource();
                Grd_agent.Enabled = false;



                Cbo_agent_comm_type.SelectedIndex = 0;
                Cbo_agent_comm_type.Enabled = false;

                Cbo_agent_cur.DataSource = new BindingSource();
                Cbo_agent_cur.Enabled = false;

                Txt_comm_agent.Text = "0.000";
                Txt_comm_agent.Enabled = false;

                proc_Name.Enabled = false;
                Agent_name.Text = "";
                Agent_name.Enabled = false;
                TXt_agent_Change = false;
                Grd_agent_Change = false;
                try { _BS_sub_agent.DataSource = new BindingSource(); }
                catch { }
                Cbo_type_SelectedIndexChanged(null, null);


            }
        }
        //------------------------------------------------------------------------
        private void Txt_procer_TextChanged(object sender, EventArgs e)
        {

            string Constr1 = "";
            chang_proc = false;
            if (Cbo_type.SelectedIndex == 1)// قيد يومية
            {
                if (Txt_procer.Text != "")
                {
                    if (Cbo_type.SelectedIndex == 1)
                    {
                        try
                        {
                            x = Convert.ToInt32(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"]);
                            Constr1 = " And  Sub_Cust_ID <>" + x;
                        }
                        catch
                        { }
                    }

                    //SqlTxt_sub = " Select  *  From  Sub_CUSTOMERS_online where "
                    //    + " ASub_CustName like '%" + Txt_procer.Text + "%' and t_id = " + connection.T_ID + Constr1;

                    SqlTxt_sub = " Select  * From  Sub_CUSTOMERS_online where T_id = " + connection.T_ID
                    + " And ( ASub_CustName like '" + Txt_procer.Text + "%' or sub_cust_id like '" + Txt_procer.Text + "%')" + Constr1;

                    connection.SqlExec(SqlTxt_sub, "Sub_Cust_tbl");


                    if (connection.SQLDS.Tables["Sub_Cust_tbl"].Rows.Count > 0)
                    {
                        _BS_sub_cust.DataSource = connection.SQLDS.Tables["Sub_Cust_tbl"].DefaultView.ToTable(true, "ASub_CustName", "ESub_CustNmae", "Sub_cust_id", "Acc_AName", "Acc_EName", "comm_rem_flag").Select().CopyToDataTable();
                        chang_proc = false;
                        Grd_procer.DataSource = _BS_sub_cust;
                        chang_proc = true;
                        Grd_procer_SelectionChanged(null, null);
                        //  chk_Agent_CheckedChanged(null, null);
                    }
                    else
                    {
                        Grd_procer.DataSource = new BindingSource();
                        _BS_sub_cust = new BindingSource();
                        chang_proc = false;
                        proc_Name.Text = "";
                        Cmb_R_CUR_ID.DataSource = new BindingSource();
                        Cmb_Comm_Cur.DataSource = new BindingSource();
                    }

                }
                else
                {
                    Grd_procer.DataSource = new BindingSource();
                    _BS_sub_cust = new BindingSource();
                    chang_proc = false;
                    proc_Name.Text = "";
                    Cmb_R_CUR_ID.DataSource = new BindingSource();
                    Cmb_Comm_Cur.DataSource = new BindingSource();
                }

            }
        }
        //------------------------------------------------------------------------
        private void Grd_procer_SelectionChanged(object sender, EventArgs e)
        {
            if (chang_proc)
            {

                chk_Agent_CheckedChanged(null, null);


                if (chk_Agent.Checked == true && Grd_agent.RowCount > 0)
                {
                    string sql_txtt = "";
                    sql_txtt = " Select Distinct Cur_ANAME   , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                    + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"])
                    + " INTERSECT "
                    + "   Select Distinct Cur_ANAME   , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                    + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]);

                    binding_rcur.DataSource = connection.SqlExec(sql_txtt, "TbL_cur_Intersect");
                    Cmb_R_CUR_ID.DataSource = binding_rcur;
                    Cmb_R_CUR_ID.ValueMember = "cur_id";
                    Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";


                    cur_comm.DataSource = connection.SQLDS.Tables["TbL_cur_Intersect"];
                    Cmb_Comm_Cur.DataSource = cur_comm;
                    Cmb_Comm_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                    Cmb_Comm_Cur.ValueMember = "cur_id";
                    //checkBox5.Checked = false;
                    //lbl_comm.Visible = false;
                }
                if (Grd_procer.RowCount > 0)
                {
                    Txt_cur_rem = "   Select Distinct Cur_ANAME , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                            + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]);
                    binding_rcur.DataSource = connection.SqlExec(Txt_cur_rem, "TbL_cur_rem");

                    Cmb_R_CUR_ID.DataSource = binding_rcur;
                    Cmb_R_CUR_ID.ValueMember = "cur_id";
                    Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";

                    cur_comm.DataSource = connection.SqlExec(Txt_cur_rem, "TbL_cur_rem");
                    Cmb_Comm_Cur.DataSource = cur_comm;
                    Cmb_Comm_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                    Cmb_Comm_Cur.ValueMember = "cur_id";
                }

                proc_Name.DataBindings.Clear();
                proc_Name.DataBindings.Add("Text", _BS_sub_cust, connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName");
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Agent_TextChanged(object sender, EventArgs e)
        {

            if (TXt_agent_Change)
            {
                if (Txt_Agent.Text != "")
                {
                    string Constr = "";
                    if (Cbo_type.SelectedIndex == 1)
                    {
                        try
                        {
                            x = Convert.ToInt32(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]);
                            Constr = " And  Sub_Cust_ID <>" + x;
                        }
                        catch
                        { }
                    }
                    SqlTxt_agent = "Select  * From  Sub_CUSTOMERS_online where  ASub_CustName like '%" + Txt_Agent.Text + "%' and T_id = " + connection.T_ID + Constr;

                    connection.SqlExec(SqlTxt_agent, "Sub_agent_tbl");
                    if (connection.SQLDS.Tables["Sub_agent_tbl"].Rows.Count > 0)
                    {
                        Grd_agent_Change = false;
                        _BS_sub_agent.DataSource = connection.SQLDS.Tables["Sub_agent_tbl"].DefaultView.ToTable(true, "ASub_CustName", "ESub_CustNmae", "Sub_cust_id", "Acc_AName", "Acc_EName").Select().CopyToDataTable();
                        Grd_agent.DataSource = _BS_sub_agent;
                        Grd_agent_Change = true;
                        Grd_agent_SelectionChanged(null, null);
                        Agent_name.DataBindings.Clear();
                        Agent_name.DataBindings.Add("Text", _BS_sub_agent, "Acc_AName");
                        //checkBox5.Checked = false;
                        //lbl_comm.Visible = false;
                    }
                    else
                    {
                        Grd_agent_Change = false;
                        Txt_comm_agent.Text = "0.000";
                        Grd_agent.DataSource = new BindingSource();
                        Cbo_agent_cur.DataSource = new BindingSource();
                        _BS_sub_agent = new BindingSource();
                        Agent_name.Text = "";
                        Cbo_agent_comm_type.SelectedIndex = 0;
                        Cmb_R_CUR_ID.DataSource = new BindingSource();
                        Cmb_Comm_Cur.DataSource = new BindingSource();
                        if (Cbo_type.SelectedIndex == 1 && Grd_procer.RowCount > 0)
                        {

                            Grd_procer_SelectionChanged(null, null);
                        }
                    }

                }

                else
                {
                    Grd_agent_Change = false;
                    Txt_comm_agent.Text = "0.000";
                    Grd_agent.DataSource = new BindingSource();
                    Cbo_agent_cur.DataSource = new BindingSource();
                    _BS_sub_agent = new BindingSource();
                    Agent_name.Text = "";
                    Cbo_agent_comm_type.SelectedIndex = 0;

                    if (Cbo_type.SelectedIndex == 1 && Grd_procer.RowCount > 0)
                    {
                        Grd_procer_SelectionChanged(null, null);
                    }
                    if (Cbo_type.SelectedIndex == 0 && Grd_agent.RowCount <= 0)
                    {
                        get_Box_Cur();
                    }
                }
            }
        }
        //------------------------------------------------------------------------
        private void Grd_agent_SelectionChanged(object sender, EventArgs e)
        {
            if (Grd_agent_Change)
            {
                change_Cbo_agent = false;
                string Txt = "";
                //جلب عملات الوكيل الدافع للحوالة
                Txt = "   Select Distinct Cur_ANAME   , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"]);
                _BS_curr.DataSource = connection.SqlExec(Txt, "TBL_Sub_cur");
                Cbo_agent_cur.DataSource = _BS_curr;
                Cbo_agent_cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                Cbo_agent_cur.ValueMember = "cur_id";

                //تقطاع وكيل مع صندوق
                string sql_txtt = "";
                if (Cbo_type.SelectedIndex == 0 && Txt_Agent.Text != "")
                {


                    sql_txtt = " Select Distinct Cur_ANAME   , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                    + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"])
                    + " INTERSECT    "
                    + " SELECT Cur_ANAME  ,Cur_Ename  , B.cur_id "
                    + " from Cur_Tbl  A, CUSTOMERS_ACCOUNTS B "
                    + " where A.cur_id =  B.cur_id "
                    + " And  CUST_ID =  " + TxtBox_User.Tag
                    + " And T_id =  " + connection.T_ID;

                    binding_rcur.DataSource = connection.SqlExec(sql_txtt, "TbL_cur_Intersect");
                    Cmb_R_CUR_ID.DataSource = binding_rcur;
                    Cmb_R_CUR_ID.ValueMember = "cur_id";
                    Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";


                    cur_comm.DataSource = connection.SQLDS.Tables["TbL_cur_Intersect"];
                    Cmb_Comm_Cur.DataSource = cur_comm;
                    Cmb_Comm_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                    Cmb_Comm_Cur.ValueMember = "cur_id";
                }
                if (Cbo_type.SelectedIndex == 1 && Grd_procer.RowCount > 0) //تقطاع وكيل مع وسيط
                {
                    sql_txtt = " Select Distinct Cur_ANAME   , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                    + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"])
                    + " INTERSECT "
                    + " Select Distinct Cur_ANAME   , Cur_ENAME , Cur_Id From  Sub_CUSTOMERS_online "
                    + " where Sub_Cust_ID = " + Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]);

                    binding_rcur.DataSource = connection.SqlExec(sql_txtt, "TbL_cur_Intersect");
                    Cmb_R_CUR_ID.DataSource = binding_rcur;
                    Cmb_R_CUR_ID.ValueMember = "cur_id";
                    Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";


                    cur_comm.DataSource = connection.SQLDS.Tables["TbL_cur_Intersect"];
                    Cmb_Comm_Cur.DataSource = cur_comm;
                    Cmb_Comm_Cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                    Cmb_Comm_Cur.ValueMember = "cur_id";

                }
                change_Cbo_agent = true;
                Cbo_agent_cur_SelectedIndexChanged(null, null);
            }

        }
        //----------------------------------------------------------------
        private void Cbo_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_type.SelectedIndex == 0) //نقدي
            {
                Txt_procer.Visible = false;
                Grd_procer.Visible = false;
                proc_Name.Visible = false;
                label64.Visible = false;
                label66.Visible = false;
                label61.Visible = false;


                //TxtDiscount_Amount.Visible = true;
                //label59.Visible = true;
                Grd_agent_Change = false;
                Grd_agent.DataSource = new BindingSource();


                checkBox1.Checked = false;
                chang_cur_rrate = false;
                chang_cur_crate = false;

                chk_trans_flag.Visible = false;
                chk_trans_flag.Checked = false;

                cmb_cur.DataSource = new DataTable();
                cmb_cur_comm.DataSource = new DataTable();
                Txt_ExRate_Rem.ResetText();
                txt_maxrate_rem.ResetText();
                txt_minrate_rem.ResetText();
                Txt_ExRate_comm.ResetText();
                txt_maxrate_comm.ResetText();
                txt_minrate_comm.ResetText();
                Txt_Rem_Amount.ResetText();
                Txt_Com_Amnt.ResetText();
                Txt_Tot_amount.ResetText();
                proc_Name.Text = "";
                Txt_procer.Text = "";
                Grd_procer.DataSource = new BindingSource();
                get_Box_Cur();

                chk_Agent.Checked = false;


            }
            else// قيد يومية
            {

                Txt_procer.Visible = true;
                Grd_procer.Visible = true;
                proc_Name.Visible = true;
                label64.Visible = true;
                label66.Visible = true;
                label61.Visible = true;
                Txt_procer.Enabled = true;
                Grd_procer.Enabled = true;
                //TxtDiscount_Amount.Visible = false;
                //label59.Visible = false;
                checkBox1.Checked = false;
                chang_cur_rrate = false;
                chang_cur_crate = false;
                chk_trans_flag.Visible = false;
                chk_trans_flag.Checked = false;

                if (TYPE_CBO_OUT_IN.SelectedIndex == 2)
                {

                    chk_trans_flag.Visible = true;
                    chk_trans_flag.Checked = false;
                }

                cmb_cur.DataSource = new DataTable();
                cmb_cur_comm.DataSource = new DataTable();
                Txt_ExRate_Rem.ResetText();
                txt_maxrate_rem.ResetText();
                txt_minrate_rem.ResetText();
                Txt_ExRate_comm.ResetText();
                txt_maxrate_comm.ResetText();
                txt_minrate_comm.ResetText();
                Txt_Rem_Amount.ResetText();
                Txt_Com_Amnt.ResetText();
                Txt_Tot_amount.ResetText();
                if (Grd_procer.RowCount <= 0)
                {
                    Cmb_R_CUR_ID.DataSource = new BindingSource();
                    Cmb_Comm_Cur.DataSource = new BindingSource();
                }

                change_txt_procer = true;
            }
        }
        //------------------------------------------------------------------------
        private void Cbo_agent_cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_Cbo_agent && Cbo_type.SelectedIndex == 0)
            {
                try
                {
                    Cmb_R_CUR_ID.SelectedValue = Cbo_agent_cur.SelectedValue;
                }
                catch { }
            }
        }
        //------------------------------------------------------------------------
        private void Txt_Com_Amount_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                Txt_Com_Amnt.Text = Txt_Com_Amount.Text;
                Txt_Com_Amnt_TextChanged(null, null);
            }
        }

        private void TYPE_CBO_OUT_IN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TYPE_CBO_OUT_IN.SelectedIndex == 1) // اشعار مقبول
            {
                Cbo_type.SelectedIndex = 1;
                Cbo_type.Enabled = false;
                chk_Agent.Enabled = false;
                checkBox1.Enabled = false;
                label54.Text = "مدينة الارســال  :";

                chk_trans_flag.Visible = false;
                chk_trans_flag.Checked = false;



            }
            if (TYPE_CBO_OUT_IN.SelectedIndex == 2) //  صادر
            {
                Cbo_type.SelectedIndex = 0;
                Cbo_type.Enabled = true;
                chk_Agent.Enabled = true;
                checkBox1.Enabled = true;
                label54.Text = "مدينة التسليـم  :";
            }
            if (TYPE_CBO_OUT_IN.SelectedIndex == 0) // الكل
            {
                Cbo_type.SelectedIndex = 0;
                Cbo_type.Enabled = true;
                chk_Agent.Enabled = true;
                checkBox1.Enabled = true;
                label54.Text = "مدينة التسليـم  :";
            }
        }

        private void ar_sen_Click(object sender, EventArgs e)
        {
            Txt_another_Sen.Enabled = true;
            Chk_Term.Enabled = true;

            if (Grd_CustSen_Name.Rows.Count != 0)//اذا كان معرف مسبقا
            {
                if (((DataRowView)binding_Grd_cust_sender.Current).Row["Per_AName"].ToString() != "") //ويحتوي على اسم انكليزي
                {
                    btnlang = 1;
                    ar_sen.BackColor = System.Drawing.Color.LightGreen;
                    ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;

                    chang_sender = true;
                    Grd_CustSen_Name_SelectionChanged(null, null);
                    Grd_CustSen_Name.Columns["Column17"].DataPropertyName = "Per_AName";

                    Cmb_S_City.DataSource = binding_cbo_scity_con;
                    Cmb_S_City.ValueMember = "Cit_ID";
                    Cmb_S_City.DisplayMember = "ACity_Name";



                    Cmb_S_Doc_Type.DataSource = binding_sfmd;
                    Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_S_Doc_Type.DisplayMember = "Fmd_AName";

                    cmb_s_nat.DataSource = binding_cb_snat;
                    cmb_s_nat.ValueMember = "Nat_ID";
                    cmb_s_nat.DisplayMember = "Nat_AName";

                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = "Gender_Aname";

                    Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
                    Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
                    Cmb_Case_Purpose.DisplayMember = "Case_purpose_Aname";


                    resd_cmb.DataSource = binding_cmb_resd;
                    resd_cmb.ValueMember = "resd_flag";
                    resd_cmb.DisplayMember = "resd_flag_rec_Aname";


                    cmb_job_sender.DataSource = binding_cmb_job_sender;
                    cmb_job_sender.ValueMember = "Job_ID";
                    cmb_job_sender.DisplayMember = "Job_AName";


                    //resd_cmb.Items.Clear();
                    //resd_cmb.Items.Add("مقيم");
                    //resd_cmb.Items.Add("غير مقيم");
                    //resd_cmb.SelectedIndex = 0;

                }
                else// في حال لا يحتوي على per_aname
                {
                    ar_sen.Enabled = false;
                    label5.Text = "يرجى تعريف الاسم باللغة العربية";
                    btnlang = 2;
                }
            }
            else// في حال اسم جديد فقط تنكلب الكومبو
            {
                btnlang = 1;
                ar_sen.BackColor = System.Drawing.Color.LightGreen;
                ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;


                Cmb_S_City.DataSource = binding_cbo_scity_con;
                Cmb_S_City.ValueMember = "Cit_ID";
                Cmb_S_City.DisplayMember = "ACity_Name";

                Cmb_S_Doc_Type.DataSource = binding_sfmd;
                Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_S_Doc_Type.DisplayMember = "Fmd_AName";

                cmb_s_nat.DataSource = binding_cb_snat;
                cmb_s_nat.ValueMember = "Nat_ID";
                cmb_s_nat.DisplayMember = "Nat_AName";

                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = "Gender_Aname";

                Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
                Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
                Cmb_Case_Purpose.DisplayMember = "Case_purpose_Aname";

                resd_cmb.DataSource = binding_cmb_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = "resd_flag_rec_Aname";


                cmb_job_sender.DataSource = binding_cmb_job_sender;
                cmb_job_sender.ValueMember = "Job_ID";
                cmb_job_sender.DisplayMember = "Job_AName";


                //resd_cmb.Items.Clear();
                //resd_cmb.Items.Add("مقيم");
                //resd_cmb.Items.Add("غير مقيم");
                //resd_cmb.SelectedIndex = 0;
            }

        }

        private void ENAR_BTN_Click(object sender, EventArgs e)
        {
            //انكليزي

            if (Grd_CustSen_Name.Rows.Count != 0)//اذا كان معرف مسبقا
            {
                if (((DataRowView)binding_Grd_cust_sender.Current).Row["Per_EName"].ToString() != "") //ويحتوي على اسم انكليزي
                {
                    Txt_another_Sen.Enabled = false;
                    Chk_Term.Enabled = false;

                    btnlang = 2;
                    ENAR_BTN.BackColor = System.Drawing.Color.LightGreen;
                    ar_sen.BackColor = System.Drawing.Color.WhiteSmoke;
                    chang_sender = true;
                    Grd_CustSen_Name_SelectionChanged(null, null);
                    Grd_CustSen_Name.Columns["Column17"].DataPropertyName = "Per_EName";

                    Cmb_S_City.DataSource = binding_cbo_scity_con;
                    Cmb_S_City.ValueMember = "Cit_ID";
                    Cmb_S_City.DisplayMember = "ECity_Name";

                    Cmb_S_Doc_Type.DataSource = binding_sfmd;
                    Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_S_Doc_Type.DisplayMember = "Fmd_EName";

                    cmb_s_nat.DataSource = binding_cb_snat;
                    cmb_s_nat.ValueMember = "Nat_ID";
                    cmb_s_nat.DisplayMember = "Nat_EName";

                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = "Gender_Ename";

                    Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
                    Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
                    Cmb_Case_Purpose.DisplayMember = "Case_purpose_Ename";

                    resd_cmb.DataSource = binding_cmb_resd;
                    resd_cmb.ValueMember = "resd_flag";
                    resd_cmb.DisplayMember = "resd_flag_rec_Ename";

                    cmb_job_sender.DataSource = binding_cmb_job_sender;
                    cmb_job_sender.ValueMember = "Job_ID";
                    cmb_job_sender.DisplayMember = "Job_EName";

                    //resd_cmb.Items.Clear();
                    //resd_cmb.Items.Add("Resident");
                    //resd_cmb.Items.Add("Non-Resident");
                    //resd_cmb.SelectedIndex = 0;


                }
                else// في حال لا يحتوي على per_ename
                {
                    ENAR_BTN.Enabled = false;
                    //ENAR_BTN.Text = "No";
                    // ENAR_BTN.BackColor = System.Drawing.Color.Red;
                    label5.Text = "يرجى تعريف الاسم باللغة الانكليزية";
                    btnlang = 1;
                }
            }
            else// في حال اسم جديد فقط تنكلب الكومبو
            {
                btnlang = 2;
                ENAR_BTN.BackColor = System.Drawing.Color.LightGreen;
                ar_sen.BackColor = System.Drawing.Color.WhiteSmoke;


                Cmb_S_City.DataSource = binding_cbo_scity_con;
                Cmb_S_City.ValueMember = "Cit_ID";
                Cmb_S_City.DisplayMember = "ECity_Name";

                Cmb_S_Doc_Type.DataSource = binding_sfmd;
                Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_S_Doc_Type.DisplayMember = "Fmd_EName";

                cmb_s_nat.DataSource = binding_cb_snat;
                cmb_s_nat.ValueMember = "Nat_ID";
                cmb_s_nat.DisplayMember = "Nat_EName";

                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = "Gender_Ename";

                Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
                Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
                Cmb_Case_Purpose.DisplayMember = "Case_purpose_Ename";


                resd_cmb.DataSource = binding_cmb_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = "resd_flag_rec_Ename";


                cmb_job_sender.DataSource = binding_cmb_job_sender;
                cmb_job_sender.ValueMember = "Job_ID";
                cmb_job_sender.DisplayMember = "Job_EName";

                //resd_cmb.Items.Clear();
                //resd_cmb.Items.Add("Resident");
                //resd_cmb.Items.Add("Non-Resident");
                //resd_cmb.SelectedIndex = 0;
            }
        }

        private void ar_rec_btn_Click(object sender, EventArgs e)
        {
            Txt_another_Rec.Enabled = true;
            Chk_Term1.Enabled = true;

            if (Grd_CustRec_Name.Rows.Count != 0)
            {
                if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString() != "")
                {
                    btnlang_rec = 1;
                    label5.Text = "";
                    ar_rec_btn.BackColor = System.Drawing.Color.LightGreen;
                    en_rec_btn.BackColor = System.Drawing.Color.WhiteSmoke;

                    chang_rec = true;
                    Grd_CustRec_Name_SelectionChanged(null, null);
                    Grd_CustRec_Name.Columns["dataGridViewTextBoxColumn4"].DataPropertyName = "Per_AName";

                    Cmb_R_City.DataSource = binding_cbo_rcity_con;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = "ACity_Name";

                    Cmb_R_Nat.DataSource = binding_cb_rnat;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = "Nat_AName";

                    resd_cbm_rec.DataSource = binding_cmb_resd_rec;
                    resd_cbm_rec.ValueMember = "resd_flag";
                    resd_cbm_rec.DisplayMember = "resd_flag_rec_Aname";


                    cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                    cmb_job_receiver.ValueMember = "Job_ID";
                    cmb_job_receiver.DisplayMember = "Job_AName";



                }
                else
                {
                    ar_rec_btn.Enabled = false;
                    label8.Text = "يرجى تعريف الاسم باللغة العربية";
                    btnlang_rec = 2;

                }
            }

            else
            {
                btnlang_rec = 1;
                en_rec_btn.BackColor = System.Drawing.Color.WhiteSmoke;
                ar_rec_btn.BackColor = System.Drawing.Color.LightGreen;

                Cmb_R_City.DataSource = binding_cbo_rcity_con;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = "ACity_Name";

                Cmb_R_Nat.DataSource = binding_cb_rnat;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = "Nat_AName";


                resd_cbm_rec.DataSource = binding_cmb_resd_rec;
                resd_cbm_rec.ValueMember = "resd_flag";
                resd_cbm_rec.DisplayMember = "resd_flag_rec_Aname";

                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = "Job_AName";


            }
        }

        private void en_rec_btn_Click(object sender, EventArgs e)
        {
            //انكليزي
            if (Grd_CustRec_Name.Rows.Count != 0)
            {
                if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString() != "")
                {
                    Txt_another_Rec.Enabled = false;
                    Chk_Term1.Enabled = false;

                    btnlang_rec = 2;
                    en_rec_btn.BackColor = System.Drawing.Color.LightGreen;
                    ar_rec_btn.BackColor = System.Drawing.Color.WhiteSmoke;
                    chang_rec = true;
                    Grd_CustRec_Name_SelectionChanged(null, null);
                    Grd_CustRec_Name.Columns["dataGridViewTextBoxColumn4"].DataPropertyName = "Per_EName";

                    Cmb_R_City.DataSource = binding_cbo_rcity_con;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = "ECity_Name";

                    Cmb_R_Nat.DataSource = binding_cb_rnat;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = "Nat_EName";

                    resd_cbm_rec.DataSource = binding_cmb_resd_rec;
                    resd_cbm_rec.ValueMember = "resd_flag";
                    resd_cbm_rec.DisplayMember = "resd_flag_rec_Ename";

                    cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                    cmb_job_receiver.ValueMember = "Job_ID";
                    cmb_job_receiver.DisplayMember = "Job_EName";

                }
                else
                {
                    // en_rec_btn.Text = "No";
                    // en_rec_btn.BackColor = System.Drawing.Color.Red;
                    en_rec_btn.Enabled = false;
                    label8.Text = "يرجى تعريف الاسم باللغة الانكليزية";
                    btnlang_rec = 1;
                }
            }
            else
            {
                btnlang_rec = 2;
                en_rec_btn.BackColor = System.Drawing.Color.LightGreen;
                ar_rec_btn.BackColor = System.Drawing.Color.WhiteSmoke;

                Cmb_R_City.DataSource = binding_cbo_rcity_con;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = "ECity_Name";

                Cmb_R_Nat.DataSource = binding_cb_rnat;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = "Nat_EName";


                resd_cbm_rec.DataSource = binding_cmb_resd_rec;
                resd_cbm_rec.ValueMember = "resd_flag";
                resd_cbm_rec.DisplayMember = "resd_flag_rec_Ename";

                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = "Job_EName";
            }
        }
        //--------------------------------------------------------------
        private void Chk_Term_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Term.Checked == true)
            {
                string name_ar = "";
                if (Grd_CustSen_Name.Rows.Count > 0)
                {
                    name_ar = ((DataRowView)binding_Grd_cust_sender.Current).Row["Per_AName"].ToString();
                }
                else
                {
                    name_ar = Txt_Sender.Text.Trim();

                }
                if (name_ar != "")
                {
                    string strTranslatedText = null;
                    try
                    {
                        TranslatorService.LanguageServiceClient client = new TranslatorService.LanguageServiceClient();
                        client = new TranslatorService.LanguageServiceClient();
                        strTranslatedText = client.Translate("6CE9C85A41571C050C379F60DA173D286384E0F2", name_ar, "", "en");
                        Txt_another_Sen.Text = strTranslatedText;
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يوجد خطأ ما،يرجى المحاولة لاحقاً" : "Tere is something went wrong,Try again later", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
            }
            else
            {
                if (Grd_CustSen_Name.Rows.Count > 0)
                {
                    Txt_another_Sen.DataBindings.Clear();
                    Txt_another_Sen.DataBindings.Add("Text", binding_Grd_cust_sender, "Per_EName");
                }
                else

                { Txt_another_Sen.Text = ""; }
            }
        }

        //--------------------------------------------------------
        private void Chk_Term1_CheckedChanged_1(object sender, EventArgs e)
        {

            if (Chk_Term1.Checked == true)
            {
                string R_name_ar = "";
                if (Grd_CustRec_Name.Rows.Count > 0)
                {
                    R_name_ar = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString();
                }
                else
                {
                    R_name_ar = Txt_Reciever.Text.Trim();

                }
                if (R_name_ar != "")
                {
                    string strTranslatedText = null;
                    try
                    {
                        TranslatorService.LanguageServiceClient client = new TranslatorService.LanguageServiceClient();
                        client = new TranslatorService.LanguageServiceClient();
                        strTranslatedText = client.Translate("6CE9C85A41571C050C379F60DA173D286384E0F2", R_name_ar, "", "en");
                        Txt_another_Rec.Text = strTranslatedText;
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يوجد خطأ ما،يرجى المحاولة لاحقاً" : "Tere is something went wrong,Try again later", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
            }
            else
            {
                if (Grd_CustRec_Name.Rows.Count > 0)
                {
                    Txt_another_Rec.DataBindings.Clear();
                    Txt_another_Rec.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_EName");
                }
                else

                { Txt_another_Rec.Text = ""; }


            }

        }

        private void label80_Click(object sender, EventArgs e)
        {
            try
            {
                if (Txt_Sender.Text == "" && Grd_CustSen_Name.RowCount <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد اسم للبحث" : "Please Choose Name to Search", MyGeneral_Lib.LblCap);
                    return;
                }

                label80.Enabled = false;
                DataTable Per_blacklist_tbl = new DataTable();
                string[] Column4 = { "Per_AName", "Per_EName" };
                string[] DType4 = { "System.String", "System.String" };

                Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                Per_blacklist_tbl.Rows.Clear();

                if (connection.SQLDS.Tables["per_info_tbl"].Rows.Count > 0)
                {
                    Per_blacklist_tbl.Rows.Add(((DataRowView)binding_Grd_cust_sender.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString(), Txt_another_Sen.Text.Trim());
                }
                else
                { Per_blacklist_tbl.Rows.Add(Txt_Sender.Text.Trim(), Txt_another_Sen.Text.Trim()); }

                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label80.Enabled = true;
                }
                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label80.Enabled = true;
                }
                if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
                {
                    Rem_Blacklist AddFrm = new Rem_Blacklist();
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label80.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                    return;

                }
            }
            catch { label80.Enabled = true; }
        }

        private void label63_Click(object sender, EventArgs e)
        {
            try
            {
                if (Txt_Sender.Text == "" && Grd_CustSen_Name.RowCount <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد اسم للبحث" : "Please Choose Name to display", MyGeneral_Lib.LblCap);
                    return;
                }

                string S_birth_Date = Txt_Sbirth_Date.Value.ToString("yyyy/MM/dd");

                string current_date = DateTime.Now.ToString("yyyy/MM/dd");

                if (S_birth_Date == current_date)
                {

                    S_birth_Date = "";
                }

                label63.Enabled = false;
                string sender_name_rem = "";
                if (Txt_Sender.Text == "" && Grd_CustSen_Name.RowCount <= 0)
                {
                    sender_name_rem = "";
                }

                else
                {
                    sender_name_rem = connection.SQLDS.Tables["per_info_tbl"].Rows.Count > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Sender.Text.Trim();
                }
                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.CommandTimeout = 0;
                    connection.SQLCMD.Parameters.AddWithValue("@name", sender_name_rem);
                    connection.SQLCMD.Parameters.AddWithValue("@S_doc_no", Txt_S_Doc_No.Text.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@S_Social_No", Tex_Social_ID.Text.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", Convert.ToInt16(Cmb_S_Doc_Type.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", "");
                    connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", "");
                    connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", 0);
                    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 2);
                    connection.SQLCMD.Parameters.AddWithValue("@sbirth_date", S_birth_Date.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", "");


                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label63.Enabled = true;
                }
                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label63.Enabled = true;
                }
                if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
                {

                    Rem_Count AddFrm = new Rem_Count(1);
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label63.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            catch
            { label63.Enabled = true; }
        }
        //--------------------------------------------------------------
        //---------------------------
        private void Get_Black_list()
        {
            try
            {
                //Int16 oper_id_rem = 0;
                //if ((Cbo_Oper.SelectedIndex == 0) && (checkBox2.Checked == false && checkBox1.Checked == false))
                //{ oper_id_rem = 3; }
                //if ((Cbo_Oper.SelectedIndex == 1) && (checkBox2.Checked == false && checkBox1.Checked == false))
                //{ oper_id_rem = 27; }
                //if ((Cbo_Oper.SelectedIndex == 0) && (checkBox2.Checked == true || checkBox1.Checked == true))
                //{ oper_id_rem = 6; }
                //if ((Cbo_Oper.SelectedIndex == 1) && (checkBox2.Checked == true || checkBox1.Checked == true))
                //{ oper_id_rem = 33; }

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Refuse_confirm_All]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@rem_no", "");
                connection.SQLCMD.Parameters.AddWithValue("@case_id", 101);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@note_prog", "ADD Rem.");
                connection.SQLCMD.Parameters.AddWithValue("@oper_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@vo_no", 0);
                connection.SQLCMD.Parameters.AddWithValue("@per_id", Grd_CustSen_Name.RowCount > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row["per_id"] : 0);
                connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", Cmb_R_CUR_ID.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount", Convert.ToDecimal(Txtr_amount.Text));
                connection.SQLCMD.Parameters.AddWithValue("@Per_Name", Grd_CustSen_Name.Rows.Count > 0 ? ((DataRowView)binding_Grd_cust_sender.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Sender.Text.Trim());

                IDataReader obj = connection.SQLCMD.ExecuteReader();

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch
            {
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
        }
    }
}

        //        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        //        {
        //            if (checkBox5.Checked == true)
        //            {
        //                if (Txtr_amount.Text != "0.000" && Convert.ToInt16( Cmb_R_CUR_ID.SelectedValue )> 0)
        //                {
        //                    Txt_Com_Amount.Text = "0.000";
        //                   // Txt_Add_Com_Amount.Text = "0.000";
        //                   // Txt_Add_Com_Amount.Enabled = true;
        //                    Get_comm_online();

        //                  //  decimal tot_comm = Convert.ToDecimal(Txt_Com_Amount.Text) + Convert.ToDecimal(Txt_Add_Com_Amount.Text);
        //                  //  TXt_Total_Comm.Text = tot_comm.ToString();
        //                    //if (Cbo_Oper.SelectedIndex == 0)// اكو  اضافي نقدي
        //                    //{
        //                    //    Txt_Add_Com_Amount.Enabled = true;
        //                    //}
        //                    //else // على استحقاق ليس فيه اضافي
        //                    //{
        //                    //    Txt_Add_Com_Amount.Enabled = false;
        //                    //}

        //                    //if (Convert.ToInt16(cmb_comm_type.SelectedValue) == -1) // مدفوعة ليس  لدية اضافي
        //                    //{
        //                    //    Txt_Add_Com_Amount.Enabled = false;
        //                    //}
        //                    //else
        //                    //{ Txt_Add_Com_Amount.Enabled = true; }

        //                }
        //                else
        //                {
        //                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد مبلغ الحوالة" : "you must choose Rem. Amount", MyGeneral_Lib.LblCap);
        //                    checkBox5.Checked = false;
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                Txt_Com_Amount.Text = "0.000";
        //                //Txt_Add_Com_Amount.Text = "0.000";
        //                //Txt_Add_Com_Amount.Enabled = false;
        //                //TXt_Total_Comm.Text = "0.000";
        //                Cmb_Comm_Cur.SelectedIndex = -1;
        //                cmb_comm_type.SelectedIndex = -1;
        //                //Txt_Add_Com_Amount.Enabled = false;
        //                Cmb_Comm_Cur.Enabled = false;
        //                cmb_comm_type.Enabled = false;
        //                Txt_Com_Amount.Enabled = false;


        //            }
        //        }

        //        private void Get_comm_online()
        //        {
        //            try
        //            {
        //                if ((connection.comm_rem_flag == 1 && Cbo_type.SelectedIndex == 0) || (Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["comm_rem_flag"]) == 1 && Cbo_type.SelectedIndex == 1))
        //                {
        //                    lbl_comm.Visible = false;
        //                    connection.SqlExec("Exec comm_out_online_get "
        //                        + Convert.ToDecimal(Txtr_amount.Text)
        //                        + "," + Convert.ToInt16(Cmb_R_CUR_ID.SelectedValue)
        //                        + "," + (Convert.ToInt16(Cbo_type.SelectedIndex) == 0 ? Convert.ToInt32(connection.Term_cust_ID) : Convert.ToInt32(((DataRowView)_BS_sub_cust.Current).Row["sub_cust_id"]))
        //                        + "," + Convert.ToInt16(Cbo_city.SelectedValue)
        //                        + "," + ((DataRowView)binding_cbo_city.Current).Row["COUN_ID"]
        //                        + "," + ((DataRowView)binding_tcity_id.Current).Row["Con_ID"]
        //                        + "," + Convert.ToInt16(Cmb_T_City.SelectedValue)
        //                        + "," + Convert.ToInt16(Cmb_PR_Cur_Id.SelectedValue)
        //                        , "Get_Tbl_Comm_Online");

        //                    Txt_Com_Amount.Text = Cbo_type.SelectedIndex == 0 ? connection.SQLDS.Tables["Get_Tbl_Comm_Online"].Rows[0]["comm_amount"].ToString() : connection.SQLDS.Tables["Get_Tbl_Comm_Online"].Rows[0]["comm_amount_cs"].ToString();
        //                    Cmb_Comm_Cur.SelectedValue = Convert.ToInt16(connection.SQLDS.Tables["Get_Tbl_Comm_Online"].Rows[0]["out_comm_cur"]);
        //                    cmb_comm_type.SelectedValue = Cbo_type.SelectedIndex == 0 ? Convert.ToInt16(connection.SQLDS.Tables["Get_Tbl_Comm_Online"].Rows[0]["out_send_id"]) : Convert.ToInt16(connection.SQLDS.Tables["Get_Tbl_Comm_Online"].Rows[0]["OUT_cs_Id"]);
        //                    Comm_Rem_ID = Convert.ToInt64(connection.SQLDS.Tables["Get_Tbl_Comm_Online"].Rows[0]["Comm_Rem_ID"]);
        //                    Txt_Com_Amount.Enabled = false;

        //                }
        //                else
        //                {
        //                    Comm_Rem_ID = 0;
        //                    lbl_comm.Visible = true;
        //                    Cmb_Comm_Cur.Enabled = true;
        //                    cmb_comm_type.Enabled = true;
        //                    Cmb_Comm_Cur.SelectedIndex = 0;
        //                    cmb_comm_type.SelectedIndex = 0;
        //                    Txt_Com_Amount.Enabled = true;

        //                }
        //            }
        //            catch
        //            {
        //                Comm_Rem_ID = 0;
        //                lbl_comm.Visible = true;
        //                Cmb_Comm_Cur.Enabled = true;
        //                cmb_comm_type.Enabled = true;
        //                Cmb_Comm_Cur.SelectedIndex = 0;
        //                cmb_comm_type.SelectedIndex = 0;
        //                Txt_Com_Amount.Enabled = true;
        //            }

        //        }

        //        private void Cbo_city_SelectedIndexChanged(object sender, EventArgs e)
        //        {
        //            checkBox5.Checked = false;
        //            lbl_comm.Visible = false;
        //        }



   