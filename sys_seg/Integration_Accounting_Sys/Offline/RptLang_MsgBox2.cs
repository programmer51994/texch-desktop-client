﻿using System;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.IO;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class RptLang_MsgBox2 : Form
    {
        #region MyRegion
        //ReportClass ObjRpt = new ReportClass();
        //ReportClass ObjRpteng = new ReportClass();
      //  DataTable Prm_Dt = new DataTable();
        //DataTable[] _EXP_DT = { };
        //DataGridView[] Export_GRD;
        //bool Mvisible_Flag = false;
        //string TitleNameEXP = "";
        DataTable Dt_Param = new DataTable();
        string phone = "";
        string local_Cur = "";
        string User = "";
        string _Date = "";
        string term = "";

        #endregion
        DataGridView GrdRem_Details1 = new DataGridView();
        offlin_rem_info_details_rpt ObjRpt = new offlin_rem_info_details_rpt();
        offlin_rem_info_details_rpt ObjRptEng = new offlin_rem_info_details_rpt();
        Offline_Rem_Info_rpt ObjRpt1 = new Offline_Rem_Info_rpt();
        Offline_Rem_Info_rpt ObjRptEng1 = new Offline_Rem_Info_rpt();

        public RptLang_MsgBox2( )
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
       
        }
        //-----------------------------------------------------
        private void RptLang_MsgBox_Load(object sender, EventArgs e)
        {
            Cbo_print_typ.SelectedIndex = 0;
            CboLang_Rpt.SelectedIndex = 0;
            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
            local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            User = connection.User_Name;
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            term = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_name"].ToString();


            Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("local_Cur", local_Cur);
            Dt_Param.Rows.Add("Oper_Id", 57);
            Dt_Param.Rows.Add("Year", connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"].ToString().Substring(0, 4));
            Dt_Param.Rows.Add("User", User);
            Dt_Param.Rows.Add("term", term);
            Dt_Param.Rows.Add("phone", phone);
            Dt_Param.Rows.Add("cust_name", connection.Lang_id == 1 ? Offline_Add_paid_customer.cust_paid_name : Offline_Add_paid_customer.cust_paid_ename);
            Dt_Param.Rows.Add("fax_no", Offline_Add_paid_customer.fax_no);
        }

       
        //------------------------------------------------
        private void Get_Grd()
        {
            GrdRem_Details1.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المرســــل" : "Sender name");
            GrdRem_Details1.Columns.Add("Column2", connection.Lang_id == 1 ? "اسم المستلـــم" : "Reciever name");
            GrdRem_Details1.Columns.Add("Column3", connection.Lang_id == 1 ? "مبلغ الحوالة" : "Rem. amount");

            GrdRem_Details1.Columns.Add("Column4", connection.Lang_id == 1 ? "عملة الحوالـــة" : "Rem. Currency");
            GrdRem_Details1.Columns.Add("Column5", connection.Lang_id == 1 ? "عملة التسليـــم" : "Delivery currency");
            GrdRem_Details1.Columns.Add("Column6", connection.Lang_id == 1 ? "عمولة الوكيل" : "Rem. amount");

            GrdRem_Details1.Columns.Add("Column7", connection.Lang_id == 1 ? "عملة العمولة" : "Sender name");
            GrdRem_Details1.Columns.Add("Column8", connection.Lang_id == 1 ? "نوع العمولة" : "Reciever name");

        }
        //-----------------------------------------------------
        private void Prt_Btn_Click(object sender, EventArgs e)
        {
            
            if (Cbo_print_typ.SelectedIndex == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار نوع الطباعة  " : "Choose the print type please", MyGeneral_Lib.LblCap);
                return;
            }
            try
            {
              

                if (Cbo_print_typ.SelectedIndex == 1 )
                {

                    try
                    {
                        ObjRpt.SetDataSource(connection.SQLDS);
                        ObjRptEng.SetDataSource(connection.SQLDS);
                    }
                    catch
                    { }
                    if (Dt_Param.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Param.Rows)
                        {
                            try
                            {
                                ObjRpt.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                                ObjRptEng.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                            }
                            catch { }
                        }
                    }

                    if (CboLang_Rpt.SelectedIndex == 1)
                    {

                        ObjRptEng.PrintToPrinter(1, false, 0, 0);
                    }
                    else
                    {

                        ObjRpt.PrintToPrinter(1, false, 0, 0);

                    }

                   
                }
                else
                {


                    try
                    {
                        ObjRpt1.SetDataSource(connection.SQLDS);
                        ObjRptEng1.SetDataSource(connection.SQLDS);
                    }
                    catch
                    { }
                    if (Dt_Param.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Param.Rows)
                        {
                            try
                            {
                                ObjRpt1.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                                ObjRptEng1.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                            }
                            catch { }
                        }
                    }

                    if (CboLang_Rpt.SelectedIndex == 1)
                    {

                        ObjRptEng1.PrintToPrinter(1, false, 0, 0);
                    }
                    else
                    {

                        ObjRpt1.PrintToPrinter(1, false, 0, 0);

                    }
                }

                this.Close();
            }
            catch
            { }
           // this.Close();
        }

        private void Btn_Pdf_Click(object sender, EventArgs e)
        {

            if (Cbo_print_typ.SelectedIndex == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار نوع التصدير  " : "Choose  export type please", MyGeneral_Lib.LblCap);
                return;
            }  
            try
            {
                string FileName = string.Empty;
                if (Cbo_print_typ.SelectedIndex == 0)
                {
                    try
                    {
                        ObjRpt.SetDataSource(connection.SQLDS);
                        ObjRptEng.SetDataSource(connection.SQLDS);
                    }
                    catch
                    { }
                    if (Dt_Param.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Param.Rows)
                        {
                            try
                            {
                                ObjRpt.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                                ObjRptEng.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                            }
                            catch { }
                        }
                    }

                    if (CboLang_Rpt.SelectedIndex == 1)
                    {
                        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            FileName = saveFileDialog1.FileName + ".pdf";
                            ObjRptEng.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");

                        }
                    }
                    else
                    {
                        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            FileName = saveFileDialog1.FileName + ".pdf";
                            ObjRpt.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");
                        }
                    }
                }
                else
                {
                    try
                    {
                        ObjRpt1.SetDataSource(connection.SQLDS);
                        ObjRptEng1.SetDataSource(connection.SQLDS);
                    }
                    catch
                    { }
                    if (Dt_Param.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Param.Rows)
                        {
                            try
                            {
                                ObjRpt1.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                                ObjRptEng1.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                            }
                            catch { }
                        }
                    }

                    if (CboLang_Rpt.SelectedIndex == 1)
                    {
                        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            FileName = saveFileDialog1.FileName + ".pdf";
                            ObjRptEng1.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");

                        }
                    }
                    else
                    {
                        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {

                            FileName = saveFileDialog1.FileName + ".pdf";
                            ObjRpt1.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");          
                        }
                    }
                }
                System.Diagnostics.Process.Start(FileName);

                DialogResult Drm = MessageBox.Show(connection.Lang_id == 1 ? "ارسال الحوالات عبر الايميل؟" : "Send Rem. By Email ?", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Drm == DialogResult.Yes)
                {

                    //  string extension = System.IO.Path.GetExtension(FileName);
                    // Get all the files in the sourceDir
                    // string []filePaths = Directory.GetFiles(FileName);





                    Send_Email AddFrm = new Send_Email(FileName);
                    this.Visible = false;
                    AddFrm.ShowDialog(this);
                    this.Visible = true;

                    this.Close();


                }
                else
                {
                    this.Close();
                }
            }
            catch { };
        }
        

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            Get_Grd();
            DataGridView[] Export_GRD = { GrdRem_Details1 };
            DataTable[] Export_DT = {connection.SQLDS.Tables["DT_export"].DefaultView.ToTable(false,"S_name", "r_name",  "R_amount", connection.Lang_id == 1 ?"R_ACUR_NAME" : "R_ECUR_NAME",
                                      connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME",  "i_ocomm" , connection.Lang_id == 1 ?"i_ACUR_NAME" : "i_ECUR_NAME" ,"i_comm_type" ).Select().CopyToDataTable()};

            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, connection.Lang_id == 1 ? "معلومات حوالات الاوفلاين" : "Offline Rem. Informations");
            this.Close();
        }
      

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //if (Chk_Mail.Checked == true)
            //{
            //    OpenFileDialog Open_Pic = new OpenFileDialog();
            //    // Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PDF)|*.BMP;*.JPG;*.GIF;*.PDF|" + "All files (*.*)|*.*";
            //    //  Open_Pic.Title = connection.Lang_id == 1 ? "اختر صورة " : "Select  Photo";
            //    //   Open_Pic.Multiselect = true;

            //    if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
            //    {
            //        foreach (String file in Open_Pic.FileNames)
            //        {

            //            string file_Name = Path.GetFileName(file);

            //            packages.Send_Email AddFrm = new packages.Send_Email(file);
            //            this.Visible = false;
            //            AddFrm.ShowDialog(this);
            //            // search_rem_agent_Load(sender, e);
            //            this.Visible = true;
            //        }
            //    }
            //}
        }

      

       
    }
}
