﻿namespace Integration_Accounting_Sys
{
    partial class Send_Email
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Txt_sender_mail = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.Txt_note = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_password = new System.Windows.Forms.TextBox();
            this.Txt_rec_mail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.send_btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Txt_subject = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TXT_CC = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Txt_sender_mail
            // 
            this.Txt_sender_mail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sender_mail.Location = new System.Drawing.Point(210, 5);
            this.Txt_sender_mail.MaxLength = 49;
            this.Txt_sender_mail.Name = "Txt_sender_mail";
            this.Txt_sender_mail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_sender_mail.Size = new System.Drawing.Size(350, 22);
            this.Txt_sender_mail.TabIndex = 1278;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(564, 9);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(49, 14);
            this.label39.TabIndex = 1279;
            this.label39.Text = "...From";
            // 
            // Txt_note
            // 
            this.Txt_note.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_note.Location = new System.Drawing.Point(5, 99);
            this.Txt_note.MaxLength = 49;
            this.Txt_note.Multiline = true;
            this.Txt_note.Name = "Txt_note";
            this.Txt_note.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_note.Size = new System.Drawing.Size(613, 137);
            this.Txt_note.TabIndex = 1280;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(564, 79);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(54, 14);
            this.label1.TabIndex = 1281;
            this.label1.Text = "Subject";
            // 
            // Txt_password
            // 
            this.Txt_password.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_password.Location = new System.Drawing.Point(5, 5);
            this.Txt_password.MaxLength = 49;
            this.Txt_password.Name = "Txt_password";
            this.Txt_password.PasswordChar = '*';
            this.Txt_password.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_password.Size = new System.Drawing.Size(161, 22);
            this.Txt_password.TabIndex = 1282;
            this.Txt_password.UseSystemPasswordChar = true;
            // 
            // Txt_rec_mail
            // 
            this.Txt_rec_mail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_rec_mail.Location = new System.Drawing.Point(5, 28);
            this.Txt_rec_mail.MaxLength = 49;
            this.Txt_rec_mail.Name = "Txt_rec_mail";
            this.Txt_rec_mail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_rec_mail.Size = new System.Drawing.Size(555, 22);
            this.Txt_rec_mail.TabIndex = 1284;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(564, 32);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(34, 14);
            this.label3.TabIndex = 1285;
            this.label3.Text = "...To";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-1, 241);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(640, 1);
            this.flowLayoutPanel3.TabIndex = 1286;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-125, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel5.TabIndex = 758;
            // 
            // send_btn
            // 
            this.send_btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.send_btn.ForeColor = System.Drawing.Color.Navy;
            this.send_btn.Location = new System.Drawing.Point(311, 245);
            this.send_btn.Name = "send_btn";
            this.send_btn.Size = new System.Drawing.Size(81, 26);
            this.send_btn.TabIndex = 1288;
            this.send_btn.Text = "send";
            this.send_btn.UseVisualStyleBackColor = true;
            this.send_btn.Click += new System.EventHandler(this.send_btn_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(230, 245);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 26);
            this.button1.TabIndex = 1289;
            this.button1.Text = "cancle";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Txt_subject
            // 
            this.Txt_subject.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_subject.Location = new System.Drawing.Point(5, 75);
            this.Txt_subject.MaxLength = 49;
            this.Txt_subject.Name = "Txt_subject";
            this.Txt_subject.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_subject.Size = new System.Drawing.Size(555, 22);
            this.Txt_subject.TabIndex = 1290;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(171, 9);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(36, 14);
            this.label5.TabIndex = 1292;
            this.label5.Text = "PWD";
            // 
            // TXT_CC
            // 
            this.TXT_CC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_CC.Location = new System.Drawing.Point(5, 51);
            this.TXT_CC.MaxLength = 49;
            this.TXT_CC.Name = "TXT_CC";
            this.TXT_CC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TXT_CC.Size = new System.Drawing.Size(555, 22);
            this.TXT_CC.TabIndex = 1293;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(564, 55);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(31, 14);
            this.label2.TabIndex = 1294;
            this.label2.Text = "...cc";
            // 
            // Send_Email
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 277);
            this.Controls.Add(this.TXT_CC);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_subject);
            this.Controls.Add(this.send_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.Txt_rec_mail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_password);
            this.Controls.Add(this.Txt_note);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_sender_mail);
            this.Controls.Add(this.label39);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Send_Email";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Send_Email";
            this.Load += new System.EventHandler(this.Send_Email_Load);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Txt_sender_mail;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox Txt_note;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_password;
        private System.Windows.Forms.TextBox Txt_rec_mail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Button send_btn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Txt_subject;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TXT_CC;
        private System.Windows.Forms.Label label2;
    }
}