﻿namespace Integration_Accounting_Sys
{
    partial class Offline_Add_paid_customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.Txt_s_job = new System.Windows.Forms.TextBox();
            this.Txt_S_doc_type = new System.Windows.Forms.TextBox();
            this.TxtScity = new System.Windows.Forms.TextBox();
            this.Txt_S_doc_issue = new System.Windows.Forms.TextBox();
            this.Txt_Rem_No = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtTo_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtFrom_Da = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.Txt_Discreption = new System.Windows.Forms.TextBox();
            this.Txt_r_job = new System.Windows.Forms.TextBox();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_address = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.Cbo_R_cur = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label67 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.Txt_Agent = new System.Windows.Forms.TextBox();
            this.Cbo_PR_Cur = new System.Windows.Forms.ComboBox();
            this.Txt_Accagent = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.Grd_agent = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt_rname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Txt_sname = new System.Windows.Forms.TextBox();
            this.Cbo_case = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Cbo_t_city = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.GrdRem_Details = new System.Windows.Forms.DataGridView();
            this.label80 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.Chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_ocom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_Paid_comm_cur = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbo_paid_data = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_s_docdate = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_SBirth = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_RBirth = new Integration_Accounting_Sys.MyDateTextBox();
            this.flowLayoutPanel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_agent)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdRem_Details)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(864, 11);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 1021;
            this.label5.Text = "اسم المرسل:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(865, 124);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(65, 16);
            this.label21.TabIndex = 1016;
            this.label21.Text = "مصدر المال:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(222, 124);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(69, 16);
            this.label22.TabIndex = 1017;
            this.label22.Text = "مهنة المرسل:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(866, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 1022;
            this.label6.Text = "نوع الوثيقة:";
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_address.Location = new System.Drawing.Point(312, 37);
            this.TxtS_address.Multiline = true;
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(547, 25);
            this.TxtS_address.TabIndex = 1109;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(222, 41);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 1027;
            this.label8.Text = "مدينة المرسل:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(859, 95);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 1031;
            this.label10.Text = "محل اصدارها:";
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_name.Location = new System.Drawing.Point(312, 7);
            this.TxtS_name.Multiline = true;
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(547, 25);
            this.TxtS_name.TabIndex = 1020;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(612, 120);
            this.Txt_Soruce_money.Multiline = true;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(247, 25);
            this.Txt_Soruce_money.TabIndex = 1009;
            // 
            // Txt_s_job
            // 
            this.Txt_s_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_s_job.Location = new System.Drawing.Point(3, 120);
            this.Txt_s_job.Multiline = true;
            this.Txt_s_job.Name = "Txt_s_job";
            this.Txt_s_job.ReadOnly = true;
            this.Txt_s_job.Size = new System.Drawing.Size(217, 25);
            this.Txt_s_job.TabIndex = 1010;
            // 
            // Txt_S_doc_type
            // 
            this.Txt_S_doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_type.Location = new System.Drawing.Point(612, 64);
            this.Txt_S_doc_type.Multiline = true;
            this.Txt_S_doc_type.Name = "Txt_S_doc_type";
            this.Txt_S_doc_type.ReadOnly = true;
            this.Txt_S_doc_type.Size = new System.Drawing.Size(247, 25);
            this.Txt_S_doc_type.TabIndex = 1023;
            // 
            // TxtScity
            // 
            this.TxtScity.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtScity.Location = new System.Drawing.Point(8, 37);
            this.TxtScity.Multiline = true;
            this.TxtScity.Name = "TxtScity";
            this.TxtScity.ReadOnly = true;
            this.TxtScity.Size = new System.Drawing.Size(212, 25);
            this.TxtScity.TabIndex = 1026;
            // 
            // Txt_S_doc_issue
            // 
            this.Txt_S_doc_issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_issue.Location = new System.Drawing.Point(612, 91);
            this.Txt_S_doc_issue.Multiline = true;
            this.Txt_S_doc_issue.Name = "Txt_S_doc_issue";
            this.Txt_S_doc_issue.ReadOnly = true;
            this.Txt_S_doc_issue.Size = new System.Drawing.Size(247, 25);
            this.Txt_S_doc_issue.TabIndex = 1030;
            // 
            // Txt_Rem_No
            // 
            this.Txt_Rem_No.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_No.Location = new System.Drawing.Point(440, 38);
            this.Txt_Rem_No.MaxLength = 49;
            this.Txt_Rem_No.Name = "Txt_Rem_No";
            this.Txt_Rem_No.Size = new System.Drawing.Size(221, 22);
            this.Txt_Rem_No.TabIndex = 1251;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(222, 10);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 16);
            this.label19.TabIndex = 1012;
            this.label19.Text = "هاتف المرسل:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(530, 95);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(65, 16);
            this.label20.TabIndex = 1015;
            this.label20.Text = "التولــــــــــد:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(859, 41);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(75, 16);
            this.label23.TabIndex = 1018;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(530, 68);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 1029;
            this.label9.Text = "رقم الوثيقـــــة:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(490, 124);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(116, 16);
            this.label3.TabIndex = 1035;
            this.label3.Text = "علاقة المستلم بالمرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_phone.Location = new System.Drawing.Point(2, 6);
            this.TxtS_phone.Multiline = true;
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(218, 25);
            this.TxtS_phone.TabIndex = 1019;
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_no.Location = new System.Drawing.Point(312, 64);
            this.Txt_S_doc_no.Multiline = true;
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.ReadOnly = true;
            this.Txt_S_doc_no.Size = new System.Drawing.Size(216, 25);
            this.Txt_S_doc_no.TabIndex = 1028;
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relionship.Location = new System.Drawing.Point(295, 120);
            this.Txt_Relionship.Multiline = true;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(194, 25);
            this.Txt_Relionship.TabIndex = 1034;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(312, 96);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(87, 14);
            this.label13.TabIndex = 1037;
            this.label13.Text = "dd/mm/yyyy";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txts_nat.Location = new System.Drawing.Point(3, 91);
            this.Txts_nat.Multiline = true;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(217, 25);
            this.Txts_nat.TabIndex = 1024;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(32, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel5.TabIndex = 758;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(339, 69);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(90, 14);
            this.label26.TabIndex = 1253;
            this.label26.Text = "عمـلة الحوالـة:";
            // 
            // TxtTo_Da
            // 
            this.TxtTo_Da.Checked = false;
            this.TxtTo_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtTo_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtTo_Da.Location = new System.Drawing.Point(237, 40);
            this.TxtTo_Da.Name = "TxtTo_Da";
            this.TxtTo_Da.ShowCheckBox = true;
            this.TxtTo_Da.Size = new System.Drawing.Size(94, 20);
            this.TxtTo_Da.TabIndex = 1249;
            // 
            // TxtFrom_Da
            // 
            this.TxtFrom_Da.Checked = false;
            this.TxtFrom_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtFrom_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFrom_Da.Location = new System.Drawing.Point(89, 40);
            this.TxtFrom_Da.Name = "TxtFrom_Da";
            this.TxtFrom_Da.ShowCheckBox = true;
            this.TxtFrom_Da.Size = new System.Drawing.Size(94, 20);
            this.TxtFrom_Da.TabIndex = 1248;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(199, 43);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(36, 14);
            this.label1.TabIndex = 1247;
            this.label1.Text = "الى :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(4, 43);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(85, 14);
            this.label14.TabIndex = 1246;
            this.label14.Text = "تاريخ الانشاء :";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(154, 127);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(797, 1);
            this.flowLayoutPanel3.TabIndex = 1245;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(4, 117);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(146, 14);
            this.label15.TabIndex = 1244;
            this.label15.Text = "معلومات الوكيل الدافع...";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(222, 95);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 1025;
            this.label7.Text = "جنسية المرسل:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(4, 414);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(943, 179);
            this.tabControl1.TabIndex = 1226;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.Txt_s_docdate);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.Txt_SBirth);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.TxtS_address);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.TxtS_name);
            this.tabPage2.Controls.Add(this.Txt_Soruce_money);
            this.tabPage2.Controls.Add(this.Txt_s_job);
            this.tabPage2.Controls.Add(this.Txt_S_doc_type);
            this.tabPage2.Controls.Add(this.TxtScity);
            this.tabPage2.Controls.Add(this.Txt_S_doc_issue);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.TxtS_phone);
            this.tabPage2.Controls.Add(this.Txt_S_doc_no);
            this.tabPage2.Controls.Add(this.Txt_Relionship);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.Txts_nat);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(935, 153);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "معلومات المرسل";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(222, 68);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(73, 16);
            this.label18.TabIndex = 1013;
            this.label18.Text = "تاريخهــــــــا:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(6, 68);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 1014;
            this.label34.Text = "dd/mm/yyyy";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.Controls.Add(this.label30);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Controls.Add(this.label32);
            this.tabPage3.Controls.Add(this.label27);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.Txt_R_Name);
            this.tabPage3.Controls.Add(this.Txt_Discreption);
            this.tabPage3.Controls.Add(this.Txt_r_job);
            this.tabPage3.Controls.Add(this.Txt_R_City);
            this.tabPage3.Controls.Add(this.Txt_Purpose);
            this.tabPage3.Controls.Add(this.label38);
            this.tabPage3.Controls.Add(this.Txt_R_Nat);
            this.tabPage3.Controls.Add(this.label28);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.Txt_R_Phone);
            this.tabPage3.Controls.Add(this.Txt_R_address);
            this.tabPage3.Controls.Add(this.Txt_RBirth);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(935, 153);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "معلومات المستلم";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(866, 15);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(64, 16);
            this.label30.TabIndex = 1053;
            this.label30.Text = "اسم المستلم:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(857, 123);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 1048;
            this.label33.Text = "التفاصيـــــــل:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(862, 68);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(68, 16);
            this.label32.TabIndex = 1049;
            this.label32.Text = "مهنة المستلم:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(859, 42);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(71, 16);
            this.label27.TabIndex = 1059;
            this.label27.Text = "مدينة المستلم:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(854, 95);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 1067;
            this.label17.Text = "غرض التحويل:";
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Name.Location = new System.Drawing.Point(598, 11);
            this.Txt_R_Name.Multiline = true;
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_Name.TabIndex = 1052;
            // 
            // Txt_Discreption
            // 
            this.Txt_Discreption.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Discreption.Location = new System.Drawing.Point(346, 119);
            this.Txt_Discreption.Multiline = true;
            this.Txt_Discreption.Name = "Txt_Discreption";
            this.Txt_Discreption.ReadOnly = true;
            this.Txt_Discreption.Size = new System.Drawing.Size(499, 25);
            this.Txt_Discreption.TabIndex = 1041;
            // 
            // Txt_r_job
            // 
            this.Txt_r_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_r_job.Location = new System.Drawing.Point(598, 64);
            this.Txt_r_job.Multiline = true;
            this.Txt_r_job.Name = "Txt_r_job";
            this.Txt_r_job.ReadOnly = true;
            this.Txt_r_job.Size = new System.Drawing.Size(247, 25);
            this.Txt_r_job.TabIndex = 1042;
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_City.Location = new System.Drawing.Point(598, 38);
            this.Txt_R_City.Multiline = true;
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_City.TabIndex = 1058;
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Purpose.Location = new System.Drawing.Point(346, 91);
            this.Txt_Purpose.Multiline = true;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(499, 25);
            this.Txt_Purpose.TabIndex = 1066;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(521, 15);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 16);
            this.label38.TabIndex = 1044;
            this.label38.Text = "هاتف المستلم:";
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Nat.Location = new System.Drawing.Point(20, 11);
            this.Txt_R_Nat.Multiline = true;
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(197, 25);
            this.Txt_R_Nat.TabIndex = 1056;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(216, 15);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(77, 16);
            this.label28.TabIndex = 1057;
            this.label28.Text = "جنسية المستلم:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(299, 43);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(87, 14);
            this.label16.TabIndex = 1069;
            this.label16.Text = "dd/mm/yyyy";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(523, 42);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(71, 16);
            this.label35.TabIndex = 1047;
            this.label35.Text = "التولــــــــــــد:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(521, 68);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(74, 16);
            this.label31.TabIndex = 1050;
            this.label31.Text = "عنوان المستلم:";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Phone.Location = new System.Drawing.Point(299, 11);
            this.Txt_R_Phone.Multiline = true;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(218, 25);
            this.Txt_R_Phone.TabIndex = 1051;
            // 
            // Txt_R_address
            // 
            this.Txt_R_address.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_address.Location = new System.Drawing.Point(22, 64);
            this.Txt_R_address.Multiline = true;
            this.Txt_R_address.Name = "Txt_R_address";
            this.Txt_R_address.ReadOnly = true;
            this.Txt_R_address.Size = new System.Drawing.Size(497, 25);
            this.Txt_R_address.TabIndex = 1043;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(339, 43);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(92, 14);
            this.label39.TabIndex = 1252;
            this.label39.Text = "رقـم الحوالــــة:";
            // 
            // Cbo_R_cur
            // 
            this.Cbo_R_cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_R_cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_R_cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_R_cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_R_cur.FormattingEnabled = true;
            this.Cbo_R_cur.Items.AddRange(new object[] {
            "حوالـة صادرة",
            "حوالــة واردة"});
            this.Cbo_R_cur.Location = new System.Drawing.Point(440, 64);
            this.Cbo_R_cur.Name = "Cbo_R_cur";
            this.Cbo_R_cur.Size = new System.Drawing.Size(221, 24);
            this.Cbo_R_cur.TabIndex = 1250;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(7, 69);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(80, 14);
            this.label11.TabIndex = 1238;
            this.label11.Text = "الحـالـــــــــــة:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-71, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Navy;
            this.label67.Location = new System.Drawing.Point(60, 233);
            this.label67.Name = "label67";
            this.label67.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label67.Size = new System.Drawing.Size(67, 14);
            this.label67.TabIndex = 1231;
            this.label67.Text = "الحســاب :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(660, 69);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 14);
            this.label12.TabIndex = 1237;
            this.label12.Text = "عملة الدفــــع:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(1, 96);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(86, 14);
            this.label29.TabIndex = 1239;
            this.label29.Text = "اسم المرسل:";
            // 
            // Txt_Agent
            // 
            this.Txt_Agent.BackColor = System.Drawing.Color.White;
            this.Txt_Agent.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Agent.Location = new System.Drawing.Point(127, 136);
            this.Txt_Agent.Name = "Txt_Agent";
            this.Txt_Agent.Size = new System.Drawing.Size(276, 22);
            this.Txt_Agent.TabIndex = 1229;
            this.Txt_Agent.TextChanged += new System.EventHandler(this.Txt_Agent_TextChanged);
            // 
            // Cbo_PR_Cur
            // 
            this.Cbo_PR_Cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_PR_Cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_PR_Cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_PR_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_PR_Cur.FormattingEnabled = true;
            this.Cbo_PR_Cur.Items.AddRange(new object[] {
            "حوالـة صادرة",
            "حوالــة واردة"});
            this.Cbo_PR_Cur.Location = new System.Drawing.Point(753, 64);
            this.Cbo_PR_Cur.Name = "Cbo_PR_Cur";
            this.Cbo_PR_Cur.Size = new System.Drawing.Size(202, 24);
            this.Cbo_PR_Cur.TabIndex = 1242;
            // 
            // Txt_Accagent
            // 
            this.Txt_Accagent.BackColor = System.Drawing.Color.White;
            this.Txt_Accagent.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Accagent.Location = new System.Drawing.Point(127, 229);
            this.Txt_Accagent.Name = "Txt_Accagent";
            this.Txt_Accagent.ReadOnly = true;
            this.Txt_Accagent.Size = new System.Drawing.Size(276, 23);
            this.Txt_Accagent.TabIndex = 1228;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(60, 139);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(60, 16);
            this.label65.TabIndex = 1227;
            this.label65.Text = "الوكيــــــل:";
            // 
            // Grd_agent
            // 
            this.Grd_agent.AllowUserToAddRows = false;
            this.Grd_agent.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_agent.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_agent.BackgroundColor = System.Drawing.Color.White;
            this.Grd_agent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_agent.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_agent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_agent.ColumnHeadersHeight = 45;
            this.Grd_agent.ColumnHeadersVisible = false;
            this.Grd_agent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_agent.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_agent.Location = new System.Drawing.Point(63, 160);
            this.Grd_agent.Name = "Grd_agent";
            this.Grd_agent.ReadOnly = true;
            this.Grd_agent.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_agent.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_agent.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_agent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_agent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_agent.Size = new System.Drawing.Size(340, 68);
            this.Grd_agent.TabIndex = 1230;
            this.Grd_agent.SelectionChanged += new System.EventHandler(this.Grd_agent_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "رمز الوكيل";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ASub_CustName";
            this.dataGridViewTextBoxColumn6.HeaderText = "اسم الوكيل";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 300;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(381, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(267, 23);
            this.TxtUser.TabIndex = 1222;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(304, 10);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(76, 14);
            this.label2.TabIndex = 1221;
            this.label2.Text = "المستخــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(8, 3);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(284, 25);
            this.TxtTerm_Name.TabIndex = 1220;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-2, 570);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(953, 1);
            this.flowLayoutPanel2.TabIndex = 1219;
            // 
            // txt_rname
            // 
            this.txt_rname.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_rname.Location = new System.Drawing.Point(440, 92);
            this.txt_rname.MaxLength = 49;
            this.txt_rname.Name = "txt_rname";
            this.txt_rname.Size = new System.Drawing.Size(221, 22);
            this.txt_rname.TabIndex = 1233;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(672, 10);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 1223;
            this.label4.Text = "التاريـخ:";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(385, 595);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 30);
            this.button2.TabIndex = 1217;
            this.button2.Text = "موافق";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(465, 595);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 30);
            this.button1.TabIndex = 1218;
            this.button1.Text = "انهـــاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Txt_sname
            // 
            this.Txt_sname.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sname.Location = new System.Drawing.Point(89, 92);
            this.Txt_sname.MaxLength = 49;
            this.Txt_sname.Name = "Txt_sname";
            this.Txt_sname.Size = new System.Drawing.Size(242, 22);
            this.Txt_sname.TabIndex = 1241;
            // 
            // Cbo_case
            // 
            this.Cbo_case.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_case.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_case.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_case.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_case.FormattingEnabled = true;
            this.Cbo_case.Location = new System.Drawing.Point(89, 64);
            this.Cbo_case.Name = "Cbo_case";
            this.Cbo_case.Size = new System.Drawing.Size(242, 24);
            this.Cbo_case.TabIndex = 1240;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(660, 43);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(93, 14);
            this.label24.TabIndex = 1236;
            this.label24.Text = "مدينة الاستلام:";
            // 
            // Cbo_t_city
            // 
            this.Cbo_t_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_t_city.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_t_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_t_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_t_city.FormattingEnabled = true;
            this.Cbo_t_city.Items.AddRange(new object[] {
            "حوالـة صادرة",
            "حوالــة واردة"});
            this.Cbo_t_city.Location = new System.Drawing.Point(753, 39);
            this.Cbo_t_city.Name = "Cbo_t_city";
            this.Cbo_t_city.Size = new System.Drawing.Size(202, 24);
            this.Cbo_t_city.TabIndex = 1235;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button3.ForeColor = System.Drawing.Color.Navy;
            this.button3.Location = new System.Drawing.Point(858, 91);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(97, 25);
            this.button3.TabIndex = 1232;
            this.button3.Text = "بــحــث";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(339, 96);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(90, 14);
            this.label25.TabIndex = 1234;
            this.label25.Text = "اسم المستلم:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(957, 1);
            this.flowLayoutPanel1.TabIndex = 1225;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(726, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(221, 23);
            this.TxtIn_Rec_Date.TabIndex = 1224;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // GrdRem_Details
            // 
            this.GrdRem_Details.AllowUserToAddRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdRem_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdRem_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.GrdRem_Details.ColumnHeadersHeight = 24;
            this.GrdRem_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Chk,
            this.Column6,
            this.Column3,
            this.Column1,
            this.Column2,
            this.Column4,
            this.txt_ocom,
            this.cbo_Paid_comm_cur,
            this.cbo_paid_data,
            this.Column5});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdRem_Details.DefaultCellStyle = dataGridViewCellStyle8;
            this.GrdRem_Details.Enabled = false;
            this.GrdRem_Details.Location = new System.Drawing.Point(8, 254);
            this.GrdRem_Details.Name = "GrdRem_Details";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdRem_Details.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdRem_Details.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.GrdRem_Details.Size = new System.Drawing.Size(936, 142);
            this.GrdRem_Details.TabIndex = 1243;
            this.GrdRem_Details.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.GrdRem_Details_DataError);
            this.GrdRem_Details.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GrdRem_Details_EditingControlShowing);
            this.GrdRem_Details.SelectionChanged += new System.EventHandler(this.GrdRem_Details_SelectionChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Maroon;
            this.label80.Location = new System.Drawing.Point(11, 399);
            this.label80.Name = "label80";
            this.label80.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label80.Size = new System.Drawing.Size(146, 14);
            this.label80.TabIndex = 1254;
            this.label80.Text = "قـــــوائـــــــــم الـــمــــنــــع";
            this.label80.Click += new System.EventHandler(this.label80_Click);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Maroon;
            this.label77.Location = new System.Drawing.Point(263, 399);
            this.label77.Name = "label77";
            this.label77.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label77.Size = new System.Drawing.Size(168, 14);
            this.label77.TabIndex = 1255;
            this.label77.Text = "عـــدد الحــوالات الــمــرســلـة";
            this.label77.Click += new System.EventHandler(this.label77_Click);
            // 
            // Chk
            // 
            this.Chk.DataPropertyName = "chk";
            this.Chk.FalseValue = "0";
            this.Chk.HeaderText = "تأشير";
            this.Chk.Name = "Chk";
            this.Chk.TrueValue = "1";
            this.Chk.Width = 50;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "chk_trans";
            this.Column6.FalseValue = "0";
            this.Column6.HeaderText = "ترانزيت";
            this.Column6.Name = "Column6";
            this.Column6.TrueValue = "1";
            this.Column6.Width = 50;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحوالة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 65;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "R_amount";
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 69;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 69;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PR_ACUR_NAME";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 69;
            // 
            // txt_ocom
            // 
            this.txt_ocom.DataPropertyName = "i_ocomm";
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = "0.000";
            this.txt_ocom.DefaultCellStyle = dataGridViewCellStyle7;
            this.txt_ocom.HeaderText = "عمولة الوكيل";
            this.txt_ocom.Name = "txt_ocom";
            this.txt_ocom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cbo_Paid_comm_cur
            // 
            this.cbo_Paid_comm_cur.HeaderText = "العملة";
            this.cbo_Paid_comm_cur.Name = "cbo_Paid_comm_cur";
            this.cbo_Paid_comm_cur.Width = 99;
            // 
            // cbo_paid_data
            // 
            this.cbo_paid_data.HeaderText = "نوعــها";
            this.cbo_paid_data.Name = "cbo_paid_data";
            this.cbo_paid_data.Width = 99;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "t_ACITY_NAME";
            this.Column5.HeaderText = "مدينة التسليم";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column5.Width = 72;
            // 
            // Txt_s_docdate
            // 
            this.Txt_s_docdate.BackColor = System.Drawing.Color.White;
            this.Txt_s_docdate.DateSeperator = '/';
            this.Txt_s_docdate.Enabled = false;
            this.Txt_s_docdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_s_docdate.Location = new System.Drawing.Point(98, 65);
            this.Txt_s_docdate.Mask = "00/00/0000";
            this.Txt_s_docdate.Name = "Txt_s_docdate";
            this.Txt_s_docdate.PromptChar = ' ';
            this.Txt_s_docdate.ReadOnly = true;
            this.Txt_s_docdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_s_docdate.Size = new System.Drawing.Size(122, 22);
            this.Txt_s_docdate.TabIndex = 1126;
            this.Txt_s_docdate.Text = "00000000";
            this.Txt_s_docdate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_SBirth
            // 
            this.Txt_SBirth.BackColor = System.Drawing.Color.White;
            this.Txt_SBirth.DateSeperator = '/';
            this.Txt_SBirth.Enabled = false;
            this.Txt_SBirth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SBirth.Location = new System.Drawing.Point(408, 92);
            this.Txt_SBirth.Mask = "00/00/0000";
            this.Txt_SBirth.Name = "Txt_SBirth";
            this.Txt_SBirth.PromptChar = ' ';
            this.Txt_SBirth.ReadOnly = true;
            this.Txt_SBirth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_SBirth.Size = new System.Drawing.Size(122, 22);
            this.Txt_SBirth.TabIndex = 1123;
            this.Txt_SBirth.Text = "00000000";
            this.Txt_SBirth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_RBirth
            // 
            this.Txt_RBirth.BackColor = System.Drawing.Color.White;
            this.Txt_RBirth.DateSeperator = '/';
            this.Txt_RBirth.Enabled = false;
            this.Txt_RBirth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_RBirth.Location = new System.Drawing.Point(395, 39);
            this.Txt_RBirth.Mask = "00/00/0000";
            this.Txt_RBirth.Name = "Txt_RBirth";
            this.Txt_RBirth.PromptChar = ' ';
            this.Txt_RBirth.ReadOnly = true;
            this.Txt_RBirth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_RBirth.Size = new System.Drawing.Size(122, 22);
            this.Txt_RBirth.TabIndex = 1124;
            this.Txt_RBirth.Text = "00000000";
            this.Txt_RBirth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Offline_Add_paid_customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 627);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.Txt_Rem_No);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.TxtTo_Da);
            this.Controls.Add(this.TxtFrom_Da);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.GrdRem_Details);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.Cbo_R_cur);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Txt_Agent);
            this.Controls.Add(this.Cbo_PR_Cur);
            this.Controls.Add(this.Txt_Accagent);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.Grd_agent);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.txt_rname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Txt_sname);
            this.Controls.Add(this.Cbo_case);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Cbo_t_city);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Name = "Offline_Add_paid_customer";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Offline_Add_paid_customer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.send_rem_FormClosed);
            this.Load += new System.EventHandler(this.send_rem_Load);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_agent)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrdRem_Details)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private MyDateTextBox Txt_s_docdate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private MyDateTextBox Txt_SBirth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.TextBox Txt_s_job;
        private System.Windows.Forms.TextBox Txt_S_doc_type;
        private System.Windows.Forms.TextBox TxtScity;
        private System.Windows.Forms.TextBox Txt_S_doc_issue;
        private System.Windows.Forms.TextBox Txt_Rem_No;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker TxtTo_Da;
        private System.Windows.Forms.DateTimePicker TxtFrom_Da;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.TextBox Txt_Discreption;
        private System.Windows.Forms.TextBox Txt_r_job;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_R_address;
        private MyDateTextBox Txt_RBirth;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox Cbo_R_cur;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txt_Agent;
        private System.Windows.Forms.ComboBox Cbo_PR_Cur;
        private System.Windows.Forms.TextBox Txt_Accagent;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.DataGridView Grd_agent;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox txt_rname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Txt_sname;
        private System.Windows.Forms.ComboBox Cbo_case;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox Cbo_t_city;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridView GrdRem_Details;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Chk;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn txt_ocom;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbo_Paid_comm_cur;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbo_paid_data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}