﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Offline_Add_paid_customer : Form
    {

        string SqlTxt_t_city = "";
        string SqlTxt_r_cur = "";
        string SqlTxt_pr_cur = "";
        string SqlTxt = "";
        string SqlTxt_Paid_cur = "";
        string SqlTxt_agent = "";
        string SqlTxt_cases = "";
        int sub_cust_id = 0;
        private DataTable Rem_info;
        BindingSource binding_Comm_Cur = new BindingSource();
        BindingSource binding_rcur = new BindingSource();
        BindingSource binding_pcur = new BindingSource();
        BindingSource binding_tcity_id = new BindingSource();
        BindingSource binding_Rem_Info = new BindingSource();
        BindingSource binding_Paid_cur = new BindingSource();
        BindingSource binding_cases = new BindingSource();
        BindingSource _BS_sub_agent = new BindingSource();
        BindingSource _BS_Type = new BindingSource();
        bool Change = false;
        string @format = "yyyy/MM/dd";
        string @null = "0000/00/00 ";
        int FromDate = 0;
        int ToDate = 0;
        bool X = false;
        bool y = false;
        bool cbo1 = false;
        bool cbo2 = false;
        bool txt = false;
        bool cbo_agent_chk = false;
        public static string cust_paid_name = "";
        public static string cust_paid_ename = "";
        Int16 cust_paid_Type = 0;
        public static string fax_no = "";
        public Offline_Add_paid_customer()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_agent.AutoGenerateColumns = false;
            GrdRem_Details.AutoGenerateColumns = false;

        }

        private void send_rem_Load(object sender, EventArgs e)
        {
            TxtFrom_Da.Format = DateTimePickerFormat.Custom;
            TxtFrom_Da.CustomFormat = @format;

            TxtTo_Da.Format = DateTimePickerFormat.Custom;
            TxtTo_Da.CustomFormat = @format;

            Change = false;
            SqlTxt_t_city = " select distinct t_city_id,t_ACITY_NAME,t_ECITY_NAME from remittences_online "
                          + " where   rem_flag =  1"
            + " union SELECT  0 as  t_city_id , 'جميع المــدن ' as   t_ACITY_NAME, 'All Cities'   as t_ECITY_NAME  order by t_city_id";

            binding_tcity_id.DataSource = connection.SqlExec(SqlTxt_t_city, "t_city_tbl");
            Cbo_t_city.DataSource = binding_tcity_id;
            Cbo_t_city.DisplayMember = connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME";
            Cbo_t_city.ValueMember = "t_city_id";

            SqlTxt_r_cur = " select distinct R_Cur_Id,R_ACUR_NAME,R_ECUR_NAME from remittences_online "
                         + " where  rem_flag  =   1"
                         + " union SELECT  0 as  R_Cur_Id , 'جميع العملات ' as   R_ACUR_NAME, 'All Currencies'   as R_ECUR_NAME ";
            binding_rcur.DataSource = connection.SqlExec(SqlTxt_r_cur, "R_cur_tbl");
            Cbo_R_cur.DataSource = binding_rcur;
            Cbo_R_cur.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
            Cbo_R_cur.ValueMember = "R_Cur_Id";

            SqlTxt_pr_cur = " select distinct PR_cur_id,PR_ACUR_NAME ,PR_ECUR_NAME from remittences_online "
                            + " where  rem_flag=1 "
                            + " union SELECT  0 as  PR_cur_id , 'جميع العملات ' as   PR_ACUR_NAME, 'All Currencies'   as PR_ECUR_NAME ";
            binding_pcur.DataSource = connection.SqlExec(SqlTxt_pr_cur, "pr_cur_tbl");
            Cbo_PR_Cur.DataSource = binding_pcur;
            Cbo_PR_Cur.DisplayMember = connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME";
            Cbo_PR_Cur.ValueMember = "PR_cur_id";

            SqlTxt_cases = "SELECT   CASE_ID, ACASE_NA, ECASE_NA FROM REMITTENCE_CASES where case_id in (101,153) "
                         + " union SELECT  0 as  CASE_ID , 'جميع الحالات ' as   ACASE_NA, 'All Cases'   as ECASE_NA ";
            binding_cases.DataSource = connection.SqlExec(SqlTxt_cases, "cases_tbl");
            Cbo_case.DataSource = binding_cases;
            Cbo_case.DisplayMember = connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA";
            Cbo_case.ValueMember = "CASE_ID";
            //-----------------------------------------

            SqlTxt_Paid_cur = "SELECT type_rem_ID, Type_rem_ANAME, Type_rem_ENAME  FROM  type_rem_INOUT ";
            _BS_Type.DataSource = connection.SqlExec(SqlTxt_Paid_cur, "TBL_REM_TYPE");

            cbo_paid_data.DataSource = _BS_Type;
            cbo_paid_data.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            cbo_paid_data.ValueMember = "type_rem_ID";
            cbo_paid_data.DataPropertyName = "type_rem_ID";

        }

        private void Txt_Agent_TextChanged(object sender, EventArgs e)
        {
            if (Txt_Agent.Text != "")
            {
                cbo_agent_chk = false;

                SqlTxt_agent = "Select distinct ASub_CustName,ESub_CustNmae,Sub_Cust_ID,Acc_AName,Acc_EName From  Sub_CUSTOMERS_online "
                    + " where  ASub_CustName like '%" + Txt_Agent.Text + "%' and T_id = " + connection.T_ID;

                _BS_sub_agent.DataSource = connection.SqlExec(SqlTxt_agent, "Sub_agent_tbl");
                if (connection.SQLDS.Tables["Sub_agent_tbl"].Rows.Count > 0)
                {
                    GrdRem_Details.Enabled = true;
                    Grd_agent.DataSource = _BS_sub_agent;
                    Txt_Accagent.Text = ((DataRowView)_BS_sub_agent.Current).Row["Acc_AName"].ToString();
                    cbo_agent_chk = true;

                    Grd_agent_SelectionChanged(null, null);

                }
                else
                {
                    Grd_agent.DataSource = new BindingSource();
                    Txt_Accagent.Text = "";
                    cbo_agent_chk = false;
                    GrdRem_Details.Enabled = false;
                    //  cbo1 = false;
                    //   cbo_Paid_comm_cur.DataSource = new BindingSource();

                    //  binding_Paid_cur.DataSource = new BindingSource();
                }

            }
            else
            {
                Grd_agent.DataSource = new BindingSource();
                Txt_Accagent.Text = "";
                cbo_agent_chk = false;
                GrdRem_Details.Enabled = false;
                //  cbo1 = false;
                //    cbo_Paid_comm_cur.DataSource = new BindingSource();
                //binding_Paid_cur.DataSource = new BindingSource();

            }
        }

        private void Grd_agent_SelectionChanged(object sender, EventArgs e)
        {

            if (cbo_agent_chk && Txt_Agent.Text != "")
            {

                try
                {
                    sub_cust_id = Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["sub_cust_id"]);
                    SqlTxt_Paid_cur = "SELECT distinct Cur_Id, Cur_ANAME, Cur_ENAME FROM Sub_CUSTOMERS_online where sub_cust_id=" + sub_cust_id;
                    binding_Paid_cur.DataSource = connection.SqlExec(SqlTxt_Paid_cur, "Paid_cur_tbl");

                    cbo_Paid_comm_cur.DataSource = binding_Paid_cur;
                    cbo_Paid_comm_cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                    cbo_Paid_comm_cur.ValueMember = "Cur_Id";
                    cbo_Paid_comm_cur.DataPropertyName = "Cur_Id";

                }
                catch { }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (TxtFrom_Da.Text == "0000/00/00 " || TxtFrom_Da.Text == "" || TxtFrom_Da.Checked == false)
            { FromDate = 0; }
            else
            {
                DateTime picker = TxtFrom_Da.Value;
                FromDate = picker.Date.Day + picker.Date.Month * 100 + picker.Date.Year * 10000;

            }

            if (TxtTo_Da.Text == "0000/00/00 " || TxtTo_Da.Text == "" || TxtTo_Da.Checked == false)
            { ToDate = 0; }
            else
            {

                DateTime picker_to = TxtTo_Da.Value;
                ToDate = picker_to.Date.Day + picker_to.Date.Month * 100 + picker_to.Date.Year * 10000;
            }
            string[] Str = { "remittences_offline_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
            SqlTxt = " exec main_remittences_off_line " + "'" + txt_rname.Text + "'" + "," + "'" + Txt_sname.Text + "'" + "," + Cbo_t_city.SelectedValue + "," + Cbo_R_cur.SelectedValue
                + "," + "'" + Txt_Rem_No.Text + "'" + "," + Cbo_PR_Cur.SelectedValue + "," + Cbo_case.SelectedValue + "," + FromDate + "," + ToDate;

            binding_Rem_Info.DataSource = connection.SqlExec(SqlTxt, "remittences_offline_tbl");
            GrdRem_Details.DataSource = binding_Rem_Info;


            if (connection.SQLDS.Tables["remittences_offline_tbl"].Rows.Count > 0)
            {
                Change = true;
                GrdRem_Details_SelectionChanged(null, null);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 2 ? "no Rem. " : "لا توجد حوالة تحقق الشروط", MyGeneral_Lib.LblCap);
                GrdRem_Details.DataSource = new BindingSource();
                TxtS_name.Text = "";
                TxtS_phone.Text = "";
                Txts_nat.Text = "";
                TxtScity.Text = "";
                Txt_SBirth.Text = "0000/00/00";
                Txt_s_job.Text = "";
                TxtS_address.Text = "";
                Txt_S_doc_type.Text = "";
                Txt_S_doc_no.Text = "";
                Txt_s_docdate.Text = "0000/00/00";
                Txt_S_doc_issue.Text = "";
                Txt_Relionship.Text = "";
                Txt_Soruce_money.Text = "";
                Txt_R_Name.Text = "";
                Txt_R_Phone.Text = "";
                Txt_R_Nat.Text = "";
                Txt_R_City.Text = "";
                Txt_RBirth.Text = "0000/00/00";
                Txt_r_job.Text = "";
                Txt_R_address.Text = "";
                Txt_Purpose.Text = "";
                Txt_Discreption.Text = "";
                Txt_Agent.Text = "";
                Txt_Accagent.Text = "";
                Grd_agent.DataSource = new DataTable();
                return;
            }

        }

        private void GrdRem_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtScity.DataBindings.Clear();
                Txt_SBirth.DataBindings.Clear();
                //Txt_S_Birth_Place.DataBindings.Clear();
                Txt_s_job.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                Txt_S_doc_type.DataBindings.Clear();
                Txt_S_doc_no.DataBindings.Clear();
                Txt_s_docdate.DataBindings.Clear();
                Txt_S_doc_issue.DataBindings.Clear();
                Txt_Relionship.DataBindings.Clear();
                Txt_Soruce_money.DataBindings.Clear();
                Txt_R_Name.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                Txt_R_Nat.DataBindings.Clear();
                Txt_R_City.DataBindings.Clear();
                Txt_RBirth.DataBindings.Clear();
                // Txt_R_Birth_Place.DataBindings.Clear();
                Txt_r_job.DataBindings.Clear();
                Txt_R_address.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                Txt_Discreption.DataBindings.Clear();
                //CboR_cur_name.DataBindings.Clear();
                //Txtreccount.DataBindings.Clear();
                //Txttotal_amuont.DataBindings.Clear();
                TxtS_name.DataBindings.Add("Text", binding_Rem_Info, "S_name");
                TxtS_phone.DataBindings.Add("Text", binding_Rem_Info, "S_phone");
                Txts_nat.DataBindings.Add("Text", binding_Rem_Info, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                TxtScity.DataBindings.Add("Text", binding_Rem_Info, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                Txt_SBirth.DataBindings.Add("Text", binding_Rem_Info, "sbirth_date");
                // Txt_S_Birth_Place.DataBindings.Add("Text", Bs_rem, "SBirth_Place");
                Txt_s_job.DataBindings.Add("Text", binding_Rem_Info, "s_job");
                TxtS_address.DataBindings.Add("Text", binding_Rem_Info, "S_address");
                Txt_S_doc_type.DataBindings.Add("Text", binding_Rem_Info, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_eDOC_NA");
                Txt_S_doc_no.DataBindings.Add("Text", binding_Rem_Info, "S_doc_no");
                Txt_s_docdate.DataBindings.Add("Text", binding_Rem_Info, "S_doc_ida");
                Txt_S_doc_issue.DataBindings.Add("Text", binding_Rem_Info, "S_doc_issue");
                Txt_Relionship.DataBindings.Add("Text", binding_Rem_Info, "Relation_S_R");
                Txt_Soruce_money.DataBindings.Add("Text", binding_Rem_Info, "Source_money");
                Txt_R_Name.DataBindings.Add("Text", binding_Rem_Info, "r_name");
                Txt_R_Phone.DataBindings.Add("Text", binding_Rem_Info, "R_phone");
                Txt_R_Nat.DataBindings.Add("Text", binding_Rem_Info, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                Txt_R_City.DataBindings.Add("Text", binding_Rem_Info, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                Txt_RBirth.DataBindings.Add("Text", binding_Rem_Info, "rbirth_date");
                Txt_r_job.DataBindings.Add("Text", binding_Rem_Info, "r_job");
                Txt_R_address.DataBindings.Add("Text", binding_Rem_Info, "r_address");
                Txt_Purpose.DataBindings.Add("Text", binding_Rem_Info, "T_purpose");

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable DT = new DataTable();

            try
            {
                DT = connection.SQLDS.Tables["remittences_offline_tbl"].DefaultView.ToTable(false, "chk", "Rem_id", "rem_no", "R_amount", "R_Cur_Id", "R_ACUR_NAME", "R_ECUR_NAME", "PR_cur_id", "PR_ACUR_NAME",
                      "PR_ECUR_NAME", "S_name", "S_phone", "S_city_id", "S_ACITY_NAME", "S_ECITY_NAME", "s_coun_id", "S_ACOUN_NAME",
                      "S_ECOUN_NAME", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_doc_no", "S_doc_ida",
                      "S_doc_eda", "S_doc_issue", "s_Email", "sFrm_doc_id", "sFRM_ADOC_NA", "sFRM_EDOC_NA", "snat_Id", "s_A_NAT_NAME",
                      "s_E_NAT_NAME", "s_notes", "r_name", "R_phone", "r_city_id", "r_ACITY_NAME", "r_ECITY_NAME", "r_coun_id",
                      "r_ACOUN_NAME", "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "r_doc_no",
                      "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "rFrm_doc_id", "rFRM_ADOC_NA", "rFRM_EDOC_NA", "rnat_Id",
                      "r_A_NAT_NAME", "r_E_NAT_NAME", "T_purpose", "r_notes", "case_id", "ACASE_NA", "ECASE_NA", "Case_Date", "vo_no",
                      "In_rec_date", "nrec_date", "rem_flag", "C_DATE", "U_DATE", "s_CUST_ID", "d_CUST_ID", "sbirth_date", "rbirth_date",

                      "update_flag", "s_ocomm", "s_acomm", "s_Cur_Id", "s_ACUR_NAME", "s_ECUR_NAME", "c_ocomm", "c_acomm", "c_Cur_Id",
                      "c_ACUR_NAME", "c_ECUR_NAME", "i_ocomm", "i_acomm", "i_Cur_Id", "i_ACUR_NAME", "i_ECUR_NAME", "r_ocomm", "r_acomm",
                      "r_CCur_Id", "r_CCur_ACUR_NAME", "r_CCur_ECUR_NAME", "tot_comm", "sq_rem_no", "user_update", "can_COMM_PER",
                      "can_comm", "recourd_flag", "rem_amount_cust", "cur_rem_amount_cust", "com_amount_cust", "cur_com_amount_cust",
                      "Fax_No", "Test_Key", "notes_stop", "rate_online", "S_Ver_Flag", "D_Ver_Flag", "SBirth_Place", "TBirth_Place", "Code_Rem",
                      "t_city_id", "t_ACITY_NAME", "t_ECITY_NAME", "s_per_id", "s_seq_doc", "r_seq_doc", "r_per_id", "Sa_City_Id", "Sa_ACITY_NAME",
                      "Sa_ECITY_NAME", "Sa_Coun_Id", "Sa_ACOUN_NAME", "Sa_ECOUN_NAME", "t_Coun_Id", "t_ACOUN_NAME", "t_ECOUN_NAME",
                      "s_job", "r_job", "Cancel_Stop_Date", "paid_Rate", "Source_money", "Relation_S_R", "Case_purpose_id", "Case_purpose_Aname",
                      "Case_purpose_Ename", "login_Sys_Id", "Chk_Bl", "login_Name", "User_id", "User_Name", "Sender_Code_phone", "Receiver_Code_phone",
                      "out_rem_loc_amount", "out_comm_loc_amount", "in_rem_loc_amount", "in_comm_loc_amount", "send_rem_flag", "local_international",
                      "cust_paid_id", "cust_paid_aname", "cust_paid_ename", "paid_comm_type", "chk_Trans").Select("chk > 0").CopyToDataTable();




            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الحوالة  " : "Choose the Rimmetance Please", MyGeneral_Lib.LblCap);
                return;
            }

            //foreach (DataRow row in connection.SQLDS.Tables["remittences_offline_tbl"].Rows)
            //{


            //    for (int i = 0; i < GrdRem_Details.Rows.Count; i++)
            //    {
            //        if (row["Chk"].ToString() != "0")
            //        {
                        
            //            row["i_Cur_Id"] = GrdRem_Details.Rows[i].Cells[7].Value;
                          
            //        }
            //    }
            //}

            if (DT.DefaultView.ToTable().Select(" i_cur_id = 0 ").Count() > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق عملة العمولة" : "check the records", MyGeneral_Lib.LblCap);
                return;
            }

             
            try
            {
                int Rec_No = (from DataRow row in DT.Rows
                              where
                                  (decimal)row["i_ocomm"] != 0 && ((string)row["i_ACUR_NAME"] == string.Empty || (int)row["paid_comm_type"] == 0 || row["paid_comm_type"] == DBNull.Value || (int)row["i_cur_id"] == 0 || row["i_cur_id"] == DBNull.Value)
                              select row).Count();
                if (Rec_No > 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "دقق عملة العمولة" : "check the records", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            catch
            { }

            Rem_info = DT.DefaultView.ToTable(false, "Rem_id", "rem_no", "R_amount", "R_Cur_Id", "R_ACUR_NAME", "R_ECUR_NAME", "PR_cur_id", "PR_ACUR_NAME",
                   "PR_ECUR_NAME", "S_name", "S_phone", "S_city_id", "S_ACITY_NAME", "S_ECITY_NAME", "s_coun_id", "S_ACOUN_NAME",
                   "S_ECOUN_NAME", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_doc_no", "S_doc_ida",
                   "S_doc_eda", "S_doc_issue", "s_Email", "sFrm_doc_id", "sFRM_ADOC_NA", "sFRM_EDOC_NA", "snat_Id", "s_A_NAT_NAME",
                   "s_E_NAT_NAME", "s_notes", "r_name", "R_phone", "r_city_id", "r_ACITY_NAME", "r_ECITY_NAME", "r_coun_id",
                   "r_ACOUN_NAME", "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "r_doc_no",
                   "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "rFrm_doc_id", "rFRM_ADOC_NA", "rFRM_EDOC_NA", "rnat_Id",
                   "r_A_NAT_NAME", "r_E_NAT_NAME", "T_purpose", "r_notes", "case_id", "ACASE_NA", "ECASE_NA", "Case_Date", "vo_no",
                   "In_rec_date", "nrec_date", "rem_flag", "C_DATE", "U_DATE", "s_CUST_ID", "d_CUST_ID", "sbirth_date", "rbirth_date",
                   "update_flag", "s_ocomm", "s_acomm", "s_Cur_Id", "s_ACUR_NAME", "s_ECUR_NAME", "c_ocomm", "c_acomm", "c_Cur_Id",
                   "c_ACUR_NAME", "c_ECUR_NAME", "i_ocomm", "i_acomm", "i_Cur_Id", "i_ACUR_NAME", "i_ECUR_NAME", "r_ocomm", "r_acomm",
                   "r_CCur_Id", "r_CCur_ACUR_NAME", "r_CCur_ECUR_NAME", "tot_comm", "sq_rem_no", "user_update", "can_COMM_PER",
                   "can_comm", "recourd_flag", "rem_amount_cust", "cur_rem_amount_cust", "com_amount_cust", "cur_com_amount_cust",
                   "Fax_No", "Test_Key", "notes_stop", "rate_online", "S_Ver_Flag", "D_Ver_Flag", "SBirth_Place", "TBirth_Place", "Code_Rem",
                   "t_city_id", "t_ACITY_NAME", "t_ECITY_NAME", "s_per_id", "s_seq_doc", "r_seq_doc", "r_per_id", "Sa_City_Id", "Sa_ACITY_NAME",
                   "Sa_ECITY_NAME", "Sa_Coun_Id", "Sa_ACOUN_NAME", "Sa_ECOUN_NAME", "t_Coun_Id", "t_ACOUN_NAME", "t_ECOUN_NAME",
                   "s_job", "r_job", "Cancel_Stop_Date", "paid_Rate", "Source_money", "Relation_S_R", "Case_purpose_id", "Case_purpose_Aname",
                   "Case_purpose_Ename", "login_Sys_Id", "Chk_Bl", "login_Name", "User_id", "User_Name", "Sender_Code_phone", "Receiver_Code_phone",
                   "out_rem_loc_amount", "out_comm_loc_amount", "in_rem_loc_amount", "in_comm_loc_amount", "send_rem_flag", "local_international",
                   "cust_paid_id", "cust_paid_aname", "cust_paid_ename", "paid_comm_type", "chk_Trans").Select().CopyToDataTable();

            cust_paid_name = ((DataRowView)_BS_sub_agent.Current).Row["ASub_CustName"].ToString();
            cust_paid_ename = ((DataRowView)_BS_sub_agent.Current).Row["ESub_CustNmae"].ToString();
            int cust_paid_id = Convert.ToInt16(((DataRowView)_BS_sub_agent.Current).Row["Sub_Cust_ID"]);
            int int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);

            connection.SQLCMD.Parameters.AddWithValue("@remittences_online_type", Rem_info);
            connection.SQLCMD.Parameters.AddWithValue("@cust_paid_name", cust_paid_name);
            connection.SQLCMD.Parameters.AddWithValue("@cust_paid_ename", cust_paid_ename);
            connection.SQLCMD.Parameters.AddWithValue("@cust_paid_id", cust_paid_id);
            connection.SQLCMD.Parameters.AddWithValue("@nrec_date", int_nrecdate);
            connection.SQLCMD.Parameters.AddWithValue("@lang_Id", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_Fax", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_Fax"].Direction = ParameterDirection.Output;
            connection.SqlExec("send_out_rem_difoffline", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            fax_no = connection.SQLCMD.Parameters["@Param_Result_Fax"].Value.ToString();
            connection.SQLCMD.Parameters.Clear();
            //  MessageBox.Show(connection.Lang_id == 2 ? "Send completed successfully" : "أرسلت الحوالة بنجاح", MyGeneral_Lib.LblCap);

            DataTable DT_export = new DataTable();
            DT_export = Rem_info;
            DT_export.TableName = "DT_export";
            connection.SQLDS.Tables.Add(DT_export);

            RptLang_MsgBox2 RptLang_MsgBox2 = new RptLang_MsgBox2();
            this.Visible = false;
            RptLang_MsgBox2.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("DT_export");
            this.Close();
            //clear_all();
            // string[] Str = { "remittences_offline_tbl" };

        }


        private void cbo_Paid_comm_cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbo1)//if (X)
            //{
            ComboBox CboBox = (ComboBox)sender;
            if (CboBox.Text != "System.Data.DataRowView" && CboBox.Text != string.Empty && CboBox.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                int MCRow = 0;
                //=========================
                try
                {
                    DataRow[] Row = connection.SQLDS.Tables["Paid_cur_tbl"].Select("Cur_Id = " + Convert.ToInt16(CboBox.SelectedValue));
                    MCRow = GrdRem_Details.CurrentRow.Index;
                    GrdRem_Details.CurrentRow.Cells[cbo_Paid_comm_cur.Index].Value = connection.Lang_id == 1 ? Row[0]["Cur_Aname"] : Row[0]["Cur_Ename"];
                    GrdRem_Details.Refresh();
                    connection.SQLDS.Tables["remittences_offline_tbl"].Rows[MCRow].SetField("i_cur_id", Row[0]["Cur_Id"]);
                    connection.SQLDS.Tables["remittences_offline_tbl"].Rows[MCRow].SetField("i_ACUR_NAME", Row[0]["Cur_ANAME"]);
                    connection.SQLDS.Tables["remittences_offline_tbl"].Rows[MCRow].SetField("i_ECUR_NAME", Row[0]["Cur_ENAME"]);
                  //  ((DataRowView)binding_Rem_Info.Current).Row["i_cur_id"] = ((DataRowView)binding_Paid_cur.Current).Row["cur_id"];
                    //cbo1 = false;
                }
                catch { }
                ///////


                // }
            }

        }
        private void cbo_paid_data_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbo2)  // if (y)
            //   {

            ComboBox combo_data = (ComboBox)sender;
            if (combo_data.Text != "System.Data.DataRowView" && combo_data.Text != string.Empty && combo_data.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                int MCRow = 0;
                //=========================
                try
                {
                    DataRow[] Row = connection.SQLDS.Tables["TBL_REM_TYPE"].Select(" type_rem_ID = " + Convert.ToInt16(combo_data.SelectedValue));
                    MCRow = GrdRem_Details.CurrentRow.Index;
                    GrdRem_Details.CurrentRow.Cells[cbo_paid_data.Index].Value = connection.Lang_id == 1 ? Row[0]["Type_rem_ANAME"] : Row[0]["Type_rem_ENAME"];
                    GrdRem_Details.Refresh();
                    connection.SQLDS.Tables["remittences_offline_tbl"].Rows[MCRow].SetField("paid_comm_type", Row[0]["type_rem_ID"]);
                    //cbo1 = false;
                }
                catch { }
                ///////
                //  ComboBox combo_data = (ComboBox)sender;

                //try
                //{

                //    if (combo_data.Text != "System.Data.DataRowView" && combo_data.Text != string.Empty )
                //    {
                //    int MCRow = 0;
                //    //=========================
                //    // DataRow[] Row = connection.SQLDS.Tables["paid_tbl"].Select("paid_id = " + Convert.ToInt16(combo_data.SelectedValue));
                //    //  DataRow[] Row =  (cbo_data.Index);
                //    //var comboBox = (DataGridViewComboBoxEditingControl)sender;
                //    //int rowIndex = comboBox.SelectedIndex;
                //    int selectedIndex = combo_data.SelectedIndex;
                //    if (selectedIndex == 0)//مدفوعة
                //    {
                //        cust_paid_Type = -1;
                //        combo_data.Text = "مدفوعة";
                //    }
                //    else//مقبوضة
                //    { cust_paid_Type = 1;
                //    combo_data.Text = "مقبوضة";
                //    }


                //   //  y = false;
                //    MCRow = GrdRem_Details.CurrentRow.Index;
                //    connection.SQLDS.Tables["remittences_offline_tbl"].Rows[MCRow].SetField("paid_comm_type", cust_paid_Type);                    
                //     }

            }
            //catch { }
            // }
        }

        private void GrdRem_Details_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //X = false;
            //y = false;
            if (Txt_Agent.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الوكيل  " : "Choose the agent Please", MyGeneral_Lib.LblCap);
                return;
            }
            else
            {
                cbo1 = false;
                cbo2 = false;
                txt = false;
                if (e.Control is ComboBox && GrdRem_Details.CurrentCell.ColumnIndex == cbo_Paid_comm_cur.Index)
                {

                    ComboBox combo = e.Control as ComboBox;
                    //if (combo != null)
                    //{
                    combo.SelectedIndexChanged -= new EventHandler(cbo_Paid_comm_cur_SelectedIndexChanged);

                    //  Add the event handler. 
                    combo.SelectedIndexChanged += new System.EventHandler(cbo_Paid_comm_cur_SelectedIndexChanged);

                    // X = true;
                    cbo1 = true;
                }
                // cbo_paid_data_SelectedIndexChanged(null, null);
                if (e.Control is ComboBox && GrdRem_Details.CurrentCell.ColumnIndex == cbo_paid_data.Index)
                {
                    ComboBox combo = e.Control as ComboBox;
                    //if (combo != null)
                    //{
                    combo.SelectedIndexChanged -= new EventHandler(cbo_paid_data_SelectedIndexChanged);

                    //  Add the event handler. 
                    combo.SelectedIndexChanged += new System.EventHandler(cbo_paid_data_SelectedIndexChanged);
                    // y = true;
                    cbo2 = true;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void send_rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            cbo1 = false;
            cbo2 = false;
            string[] Used_Tbl = { "t_city_tbl" ,"R_cur_tbl" ,"pr_cur_tbl" ,"cases_tbl" ,"Sub_agent_tbl" ,"Paid_cur_tbl" , 
                                     "remittences_offline_tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        private void clear_all()
        {
            GrdRem_Details.DataSource = new BindingSource();
            TxtS_name.Text = "";
            TxtS_phone.Text = "";
            Txts_nat.Text = "";
            TxtScity.Text = "";
            Txt_SBirth.Text = "0000/00/00";
            Txt_s_job.Text = "";
            TxtS_address.Text = "";
            Txt_S_doc_type.Text = "";
            Txt_S_doc_no.Text = "";
            Txt_s_docdate.Text = "0000/00/00";
            Txt_S_doc_issue.Text = "";
            Txt_Relionship.Text = "";
            Txt_Soruce_money.Text = "";
            Txt_R_Name.Text = "";
            Txt_R_Phone.Text = "";
            Txt_R_Nat.Text = "";
            Txt_R_City.Text = "";
            Txt_RBirth.Text = "0000/00/00";
            Txt_r_job.Text = "";
            Txt_R_address.Text = "";
            Txt_Purpose.Text = "";
            Txt_Discreption.Text = "";
            Txt_Agent.Text = "";
            Txt_Accagent.Text = "";
            Grd_agent.DataSource = new DataTable();
        }
        private void GrdRem_Details_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void label80_Click(object sender, EventArgs e)
        {
            try
            {

                label80.Enabled = false;
                DataTable Per_blacklist_tbl = new DataTable();
                string[] Column4 = { "Per_AName", "Per_EName" };
                string[] DType4 = { "System.String", "System.String" };

                Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                Per_blacklist_tbl.Rows.Clear();


                Per_blacklist_tbl.Rows.Add(((DataRowView)binding_Rem_Info.Current).Row["S_name"].ToString(), ((DataRowView)binding_Rem_Info.Current).Row["S_Ename"].ToString());


                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label80.Enabled = true;
                }
                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label80.Enabled = true;
                }
                if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
                {
                    Rem_Blacklist AddFrm = new Rem_Blacklist();
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label80.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                    return;

                }
            }
            catch { label80.Enabled = true; }
        }

        private void label77_Click(object sender, EventArgs e)
        {
            try
            {

                label77.Enabled = false;
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@name", ((DataRowView)binding_Rem_Info.Current).Row["S_name"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@S_doc_no", ((DataRowView)binding_Rem_Info.Current).Row["S_doc_no"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@S_Social_No", ((DataRowView)binding_Rem_Info.Current).Row["S_Social_No"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)binding_Rem_Info.Current).Row["sFrm_doc_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", "");
                connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", "");
                connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 2);
                connection.SQLCMD.Parameters.AddWithValue("@sbirth_date", ((DataRowView)binding_Rem_Info.Current).Row["sbirth_date"].ToString());
                connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", "");


                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                label77.Enabled = true;
                if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
                {

                    Rem_Count AddFrm = new Rem_Count(1);
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label77.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            catch
            {
                label77.Enabled = true;
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
        }

        //private void GrdRem_Details_Leave(object sender, EventArgs e)
        //{
          
        //}
    }
}