﻿namespace Integration_Accounting_Sys
{
    partial class RptLang_MsgBox2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.CboLang_Rpt = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Prt_Btn = new System.Windows.Forms.Button();
            this.Btn_Pdf = new System.Windows.Forms.Button();
            this.Btn_Excel = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.Cbo_print_typ = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(3, 42);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(126, 19);
            this.label1.TabIndex = 190;
            this.label1.Text = "لغة الطباعة /التصدير";
            // 
            // CboLang_Rpt
            // 
            this.CboLang_Rpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.CboLang_Rpt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboLang_Rpt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboLang_Rpt.FormattingEnabled = true;
            this.CboLang_Rpt.Items.AddRange(new object[] {
            "Arabic / عربي",
            "English / انكليزي"});
            this.CboLang_Rpt.Location = new System.Drawing.Point(130, 42);
            this.CboLang_Rpt.Name = "CboLang_Rpt";
            this.CboLang_Rpt.Size = new System.Drawing.Size(232, 24);
            this.CboLang_Rpt.TabIndex = 191;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-2, 69);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(413, 1);
            this.flowLayoutPanel1.TabIndex = 194;
            // 
            // Prt_Btn
            // 
            this.Prt_Btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prt_Btn.ForeColor = System.Drawing.Color.Navy;
            this.Prt_Btn.Location = new System.Drawing.Point(34, 74);
            this.Prt_Btn.Name = "Prt_Btn";
            this.Prt_Btn.Size = new System.Drawing.Size(103, 26);
            this.Prt_Btn.TabIndex = 193;
            this.Prt_Btn.Text = "طباعــــة ";
            this.Prt_Btn.UseVisualStyleBackColor = true;
            this.Prt_Btn.Click += new System.EventHandler(this.Prt_Btn_Click);
            // 
            // Btn_Pdf
            // 
            this.Btn_Pdf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Pdf.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Pdf.Location = new System.Drawing.Point(136, 74);
            this.Btn_Pdf.Name = "Btn_Pdf";
            this.Btn_Pdf.Size = new System.Drawing.Size(100, 26);
            this.Btn_Pdf.TabIndex = 195;
            this.Btn_Pdf.Text = "تصدير pdf";
            this.Btn_Pdf.UseVisualStyleBackColor = true;
            this.Btn_Pdf.Click += new System.EventHandler(this.Btn_Pdf_Click);
            // 
            // Btn_Excel
            // 
            this.Btn_Excel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Excel.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Excel.Location = new System.Drawing.Point(236, 74);
            this.Btn_Excel.Name = "Btn_Excel";
            this.Btn_Excel.Size = new System.Drawing.Size(100, 26);
            this.Btn_Excel.TabIndex = 196;
            this.Btn_Excel.Text = "تصدير Excel";
            this.Btn_Excel.UseVisualStyleBackColor = true;
            this.Btn_Excel.Click += new System.EventHandler(this.Btn_Excel_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(-5, 17);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(134, 14);
            this.label12.TabIndex = 1294;
            this.label12.Text = " نوع الطباعة / التصدير";
            // 
            // Cbo_print_typ
            // 
            this.Cbo_print_typ.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_print_typ.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_print_typ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_print_typ.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_print_typ.FormattingEnabled = true;
            this.Cbo_print_typ.Items.AddRange(new object[] {
            "اختر نوع الطباعـــة/ التصدير",
            "تفصيلـــي",
            "اجمالــــي"});
            this.Cbo_print_typ.Location = new System.Drawing.Point(130, 12);
            this.Cbo_print_typ.Name = "Cbo_print_typ";
            this.Cbo_print_typ.Size = new System.Drawing.Size(232, 24);
            this.Cbo_print_typ.TabIndex = 1293;
            // 
            // RptLang_MsgBox2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 109);
            this.ControlBox = false;
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Cbo_print_typ);
            this.Controls.Add(this.Btn_Excel);
            this.Controls.Add(this.Btn_Pdf);
            this.Controls.Add(this.Prt_Btn);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.CboLang_Rpt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RptLang_MsgBox2";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "158";
            this.Text = "لغة الطباعة";
            this.Load += new System.EventHandler(this.RptLang_MsgBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboLang_Rpt;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button Prt_Btn;
        private System.Windows.Forms.Button Btn_Pdf;
        private System.Windows.Forms.Button Btn_Excel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Cbo_print_typ;
    }
}