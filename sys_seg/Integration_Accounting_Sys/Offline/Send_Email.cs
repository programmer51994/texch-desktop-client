﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.IO;
using System.Net;

namespace Integration_Accounting_Sys
{
    public partial class Send_Email : Form
    {
        string file_path;
        string path_attached = "";
        public Send_Email(string Path)
        {
            InitializeComponent();
            path_attached = Path;
        }

        private void Send_Email_Load(object sender, EventArgs e)
        {

        }
        //----------------------------------------------------------------------
        private void send_btn_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage mail = new MailMessage();
                // SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpClient SmtpServer = new SmtpClient("smtp.mail.yahoo.com");


                mail.From = new MailAddress(Txt_sender_mail.Text.Trim());//"Sundus.k@taifco.net"
                mail.To.Add(Txt_rec_mail.Text.Trim());//"programmer5@taifco.net"
                mail.CC.Add(TXT_CC.Text.Trim());//cc
                mail.Subject = "FW:" + Txt_subject.Text.Trim();//"Test Mail"
                mail.Body = Txt_note.Text.Trim();//"mail with attachment"

                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(path_attached);
                mail.Attachments.Add(attachment);

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(Txt_sender_mail.Text.Trim(), Txt_password.Text.Trim());
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("mail Send");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            //try
            //{
                

            //    string smtpAddress = "smtp.mail.yahoo.com";
            //    int portNumber = 465;
            //    bool enableSSL = true;

            //    //smtp.Port = 465;
            //    //smtp.EnableSsl = true;

            //    string emailFrom = "fadiatfm@yahoo.com";
            //    string password = "fofofadia";
            //    string emailTo = "programmer7@taifco.net";
            //    string subject = "Hello";
            //    string body = "Hello, I'm just writing this to say Hi!";
                
            //    using (MailMessage mail = new MailMessage())
            //    {
            //        mail.From = new MailAddress(emailFrom);
            //        mail.To.Add(emailTo);
            //        mail.Subject = subject;
            //        mail.Body = body;
            //        mail.IsBodyHtml = true;
            //        // Can set to false, if you are sending pure text.

            //        //mail.Attachments.Add(new Attachment("C:\\SomeFile.txt"));
            //        //mail.Attachments.Add(new Attachment("C:\\SomeZip.zip"));

            //        using (SmtpClient smtp = new SmtpClient(smtpAddress, 25))
            //        {
            //           // smtp.UseDefaultCredentials = false;
            //            smtp.Credentials = new NetworkCredential(emailFrom, password);
            //            smtp.EnableSsl = enableSSL;
            //            smtp.Send(mail);
            //        }
            //    }
            //}
            //catch
            //{ }
        }
    }
}