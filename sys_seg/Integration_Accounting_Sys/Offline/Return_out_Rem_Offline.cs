﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
namespace Integration_Accounting_Sys
{
    public partial class Return_out_Rem_Offline : Form
    {
        BindingSource _Bs_Returen = new BindingSource();
        BindingSource binding_cbo_city = new BindingSource();
        BindingSource binding_rcur = new BindingSource();
        BindingSource binding_Customers = new BindingSource();
        DataTable _Dt = new DataTable();
        int     nrec_date = 0;
        int     Vo_No = 0;
        string  _Date = "";
        string  rem_no = "";
        string  local_Cur = "";
        string  forgin_Cur = "";
        string  forgin_Cur1 = "";
        int     Oper_Id = 0;
        string  User = "";
        double  Rem_Amount = 0;
        string  term = "";
        string  com_Cur = "";
        Int16   online_cust_id = 0;
        int     S_cust_id = 0;
        int     int_nrecdate = 0;
        int     procker = 0;
        string  prockr_name = "";
        string  procker_ename = "";
        string s_Ejob = "";
        string r_Ejob = "";
        string S_Social_No = "";
        string R_Social_No = "";

        public Return_out_Rem_Offline()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
             connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, new TextBox());
            Grd_Return.AutoGenerateColumns = false;
        }
        private void Returen_Delivery_Rem_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()
            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            #endregion

            string Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as Sub_Cust_ID  "
                + "  From CUSTOMERS A, TERMINALS B   "
                + "  Where A.CUST_ID = B.CUST_ID "
                + "  And A.Cust_Flag <>0  "
                + "  and B.t_id =  " + connection.T_ID
                + " union "
                + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID "
                + " From  Sub_CUSTOMERS_online A    "
                + " Where A.Cust_state  = 1 "
                + " and A.t_id =  " + connection.T_ID
                + " order by cust_id ";

                    
            binding_Customers.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
            CboCust_Id.DataSource = binding_Customers;
            CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            CboCust_Id.ValueMember = "Sub_Cust_ID";
           

            string Sqltxt = " select cust_id from TERMINALS where t_id =  "+ connection.T_ID;
            connection.SqlExec(Sqltxt, "S_Cust_tbl");
            S_cust_id = Convert.ToInt16(connection.SQLDS.Tables["S_Cust_tbl"].Rows[0]["cust_id"]);
            string Sqltxt1 = " select cust_online_id from TERMINALS where t_id =  " + connection.T_ID;
            connection.SqlExec(Sqltxt1, "online_Cust_tbl");
            online_cust_id = Convert.ToInt16(connection.SQLDS.Tables["online_Cust_tbl"].Rows[0]["cust_online_id"]);
            //-----مدينة الاصدار
            string SqlTxt1 = "";
            int cust_id_online = 0;
            string SqlTxt = " Select  CUST_ID From  TERMINALS where t_id = " + connection.T_ID;
            connection.SqlExec(SqlTxt, "Cust_tbl");
            cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);
            SqlTxt1 = " exec Full_information_Web_offline " + connection.Lang_id + "," + 0 + "," + cust_id_online;
            connection.SqlExec(SqlTxt1, "Full_information_tab");
            
            Cmb_oper_type.SelectedIndex = 0;
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "R_ECUR_NAME";
                Column7.DataPropertyName = "S_ECITY_NAME";
                Column8.DataPropertyName = "t_ECITY_NAME";
                Column5.DataPropertyName = "ECASE_NA";
                Column10.DataPropertyName = "r_CCur_ECUR_NAME";

                Cmb_oper_type.Items[0] = "Select the type of operation";
                Cmb_oper_type.Items[1] = "Return out rem.";
                Cmb_oper_type.Items[2] = "Return in rem.";
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            if (Cmb_oper_type.SelectedIndex == 0)
            {
                MessageBox.Show("الرجاء تحديد نوع العملية " + (Char)Keys.Enter + "Plase Choose Type or Rem.");
                return;
            }
            try
            {
                if (connection.SQLDS.Tables.Contains("Return_Delivery_Tbl"))
                {

                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Return_Delivery_Tbl"]);
                }
                try
                {
                    Int16 type_rem =  0;
                    Int16 Case_id =  0;

                    if (Cmb_oper_type.SelectedIndex == 1)
                    { type_rem = 2;
                      Case_id = 101 ;
                    }
                    if (Cmb_oper_type.SelectedIndex == 2 || Cmb_oper_type.SelectedIndex == 3)
                    {
                        type_rem = 2;
                        Case_id = 103;
                    }
                    if (Cmb_oper_type.SelectedIndex == 4 )// رد الوارد
                    {
                      type_rem = 1;
                       Case_id = 1;
                    }

                    if (Cmb_oper_type.SelectedIndex == 5)//بعد رد الدفع رد الوارد
                    {
                        type_rem = 1;
                        Case_id = 52;
                    }


                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[get_return_send_delivery_offline]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@rem_no", Txt_Rem_No.Text.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@Lang_Id", connection.Lang_id);
                    connection.SQLCMD.Parameters.AddWithValue("@rem_type", type_rem);
                    connection.SQLCMD.Parameters.AddWithValue("@case_id", Case_id);
                   connection.SQLCMD.Parameters.AddWithValue("@Cust_ID", ((DataRowView)binding_Customers.Current).Row["cust_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@broker_cust_id", ((DataRowView)binding_Customers.Current).Row["sub_cust_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@ID_Chk_State", Cmb_oper_type.SelectedIndex);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                    //string sql = "";
                    //sql = "exec get_return_send_delivery_offline '" + Txt_Rem_No.Text.Trim() + "'," + connection.Lang_id + "," + type_rem + "," + Case_id + ","
                    //     + Convert.ToInt16(((DataRowView)binding_Customers.Current).Row["cust_id"]) + "," + Convert.ToInt16(((DataRowView)binding_Customers.Current).Row["sub_cust_id"])
                    //    + ",''";
                    //connection.SqlExec(sql, "Return_Delivery_Tbl");
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Return_Delivery_Tbl");
                    obj.Close();

                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Dispose();
                        Clear_txt();
                        return;
                    }
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch (Exception _Err)
                {
                    MessageBox.Show("الحواله لا تحقق الشروط " + (Char)Keys.Enter + "Plase Choosen remittance not in the same conditions");
                    connection.SQLCMD.Parameters.Clear();
                    MyErrorHandler.ExceptionHandler(_Err);
                    return;
                }

                if (connection.SQLDS.Tables["Return_Delivery_Tbl"].Rows.Count > 0)
                {
                    _Bs_Returen.DataSource = connection.SQLDS.Tables["Return_Delivery_Tbl"];
                    Grd_Return.DataSource = _Bs_Returen;
                    Txt_S_Name.DataBindings.Clear();
                    Txt_S_Phone.DataBindings.Clear();
                    Txt_Sbirth_Date.DataBindings.Clear();
                    Txt_S_Doc_No.DataBindings.Clear();
                    Txt_Doc_S_Issue.DataBindings.Clear();
                    Txt_S_job.DataBindings.Clear();
                    Txt_source_money.DataBindings.Clear();
                    Txt_S_Nat_name.DataBindings.Clear();
                    Txt_S_City.DataBindings.Clear();
                    Txt_S_Doc_type.DataBindings.Clear();
                    Txt_Doc_S_Date.DataBindings.Clear();
                    Txt_S_Addres.DataBindings.Clear();
                    Txt_Relship.DataBindings.Clear();
                    Txt_R_Name.DataBindings.Clear();
                    Txt_R_Phone.DataBindings.Clear();
                    Txt_Rbirth_Date.DataBindings.Clear();
                    Txt_R_job.DataBindings.Clear();
                    Txt_R_Nat.DataBindings.Clear();
                    Txt_R_City.DataBindings.Clear();
                    Txt_R_Addres.DataBindings.Clear();
                    Txt_T_Purpose.DataBindings.Clear();
                    Txt_Notes.DataBindings.Clear();

                    Txt_S_Name.DataBindings.Add("Text", _Bs_Returen, "S_name");
                    Txt_S_Phone.DataBindings.Add("Text", _Bs_Returen, "S_phone");
                    Txt_Sbirth_Date.DataBindings.Add("Text", _Bs_Returen, "sbirth_date");
                    Txt_S_Doc_No.DataBindings.Add("Text", _Bs_Returen, "S_doc_no");
                    Txt_Doc_S_Issue.DataBindings.Add("Text", _Bs_Returen, "S_doc_issue");
                    Txt_S_job.DataBindings.Add("Text", _Bs_Returen, "s_job");
                    Txt_source_money.DataBindings.Add("Text", _Bs_Returen, "Source_money");
                    Txt_S_Nat_name.DataBindings.Add("Text", _Bs_Returen, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                    Txt_S_City.DataBindings.Add("Text", _Bs_Returen, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                    Txt_S_Doc_type.DataBindings.Add("Text", _Bs_Returen, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                    Txt_Doc_S_Date.DataBindings.Add("Text", _Bs_Returen, "S_doc_ida");
                    Txt_S_Addres.DataBindings.Add("Text", _Bs_Returen, "S_address");
                    Txt_Relship.DataBindings.Add("Text", _Bs_Returen, "Relation_S_R");
                    Txt_R_Name.DataBindings.Add("Text", _Bs_Returen, "r_name");
                    Txt_R_Phone.DataBindings.Add("Text", _Bs_Returen, "R_phone");
                    Txt_Rbirth_Date.DataBindings.Add("Text", _Bs_Returen, "rbirth_date");
                    Txt_R_job.DataBindings.Add("Text", _Bs_Returen, "r_job");
                    Txt_R_Nat.DataBindings.Add("Text", _Bs_Returen, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                    Txt_R_City.DataBindings.Add("Text", _Bs_Returen, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                    Txt_R_Addres.DataBindings.Add("Text", _Bs_Returen, "r_address");
                    Txt_T_Purpose.DataBindings.Add("Text", _Bs_Returen, "T_purpose");
                }
                else
                {
                    MessageBox.Show("لا توجد حوالات تحقق الشروط" + (Char)Keys.Enter + "No Rem. for this condintion");
                    Grd_Return.DataSource = new BindingSource();
                    Clear_txt();
                    return;
                }
            }
            catch
            {
                MessageBox.Show("الرجاء التأكد من نوع الحوالة أو رقمها " + (Char)Keys.Enter + "Plase Enter Correct Type or Rem. NO");
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                return;
            }
        }
        private void Btn_Ok1_Click(object sender, EventArgs e)
        {
            if (Grd_Return.RowCount <= 0 )
            { MessageBox.Show("لا توجد حوالة للرد " + (Char)Keys.Enter + "there is no Rem For return");
            return;
            }
            if (Cmb_oper_type.SelectedIndex == 3) //شامل 
            {
                nrec_date = Convert.ToInt32(((DataRowView)_Bs_Returen.Current).Row["nrec_date"]);

               
                BtnOk.Enabled = false;
                connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_No.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
                connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@Login_Name", connection.User_Name);
                connection.SQLCMD.Parameters.AddWithValue("@cust_online", CboCust_Id.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@nrec_date", TxtIn_Rec_Date.Text);
                connection.SQLCMD.Parameters.AddWithValue("@Lang_Id", connection.Lang_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.Add("@Param_vo_no", SqlDbType.Int).Value = "";
                connection.SQLCMD.Parameters["@Param_vo_no"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.Add("@Param_case_date", SqlDbType.VarChar, 25).Value = "";
                connection.SQLCMD.Parameters["@Param_case_date"].Direction = ParameterDirection.Output;
                connection.SqlExec("Return_Rem_Online", connection.SQLCMD);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Clear_txt();
                    BtnOk.Enabled = true;
                    return;
                }
            }

                 if (Cmb_oper_type.SelectedIndex == 1 || Cmb_oper_type.SelectedIndex == 2  ) //شامل 
            {

                      
                   Int16 Case_id =  0;
                    if (Cmb_oper_type.SelectedIndex == 1)
                    { Case_id = 101 ; }
                    if (Cmb_oper_type.SelectedIndex == 2 )
                    { Case_id = 103;}

                nrec_date = Convert.ToInt32(((DataRowView)_Bs_Returen.Current).Row["nrec_date"]);
                BtnOk.Enabled = false;
                connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_No.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
                connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@Login_Name", connection.User_Name);
                connection.SQLCMD.Parameters.AddWithValue("@cust_brocker", CboCust_Id.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@nrec_date", TxtIn_Rec_Date.Text);
                connection.SQLCMD.Parameters.AddWithValue("@Lang_Id", connection.Lang_id);
                connection.SQLCMD.Parameters.AddWithValue("@case_id", Case_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.Add("@Param_vo_no", SqlDbType.Int).Value = "";
                connection.SQLCMD.Parameters["@Param_vo_no"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.Add("@Param_case_date", SqlDbType.VarChar, 25).Value = "";
                connection.SQLCMD.Parameters["@Param_case_date"].Direction = ParameterDirection.Output;
                connection.SqlExec("Return_Rem_Offline", connection.SQLCMD);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Clear_txt();
                    BtnOk.Enabled = true;
                    return;
                }
            }

           // -----------------wared
                 if (Cmb_oper_type.SelectedIndex == 4 || Cmb_oper_type.SelectedIndex == 5) //وارد 
                 {

                     nrec_date = Convert.ToInt32(((DataRowView)_Bs_Returen.Current).Row["nrec_date"]);
                     BtnOk.Enabled = false;
                     connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_No.Text.Trim());
                     connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
                     connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                     connection.SQLCMD.Parameters.AddWithValue("@Login_Name", connection.User_Name);
                     connection.SQLCMD.Parameters.AddWithValue("@cust_brocker", CboCust_Id.SelectedValue);
                     connection.SQLCMD.Parameters.AddWithValue("@nrec_date", TxtIn_Rec_Date.Text);
                     connection.SQLCMD.Parameters.AddWithValue("@Lang_Id", connection.Lang_id);
                     connection.SQLCMD.Parameters.AddWithValue("@case_id", Cmb_oper_type.SelectedIndex == 4 ? 1:52 );//هنا التغيير
                     connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                     connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                     connection.SQLCMD.Parameters.Add("@Param_vo_no", SqlDbType.Int).Value = "";
                     connection.SQLCMD.Parameters["@Param_vo_no"].Direction = ParameterDirection.Output;
                     connection.SQLCMD.Parameters.Add("@Param_case_date", SqlDbType.VarChar, 25).Value = "";
                     connection.SQLCMD.Parameters["@Param_case_date"].Direction = ParameterDirection.Output;
                     connection.SqlExec("Return_INRem_Offline", connection.SQLCMD);
                     if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                     {
                         MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                         connection.SQLCMD.Parameters.Clear();
                         Clear_txt();
                         BtnOk.Enabled = true;
                         return;
                     }
                 }
            //if (Cmb_oper_type.SelectedIndex == 2)
            //{
            //    nrec_date = Convert.ToInt32(((DataRowView)_Bs_Returen.Current).Row["nrec_date"]);

            //    connection.SQLCMD.Parameters.Clear();
            //    connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_No.Text.Trim());
            //    connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            //    connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            //    connection.SQLCMD.Parameters.AddWithValue("@Login_Name", connection.User_Name);
            //    connection.SQLCMD.Parameters.AddWithValue("@nrec_date", TxtIn_Rec_Date.Text);
            //    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            //    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            //    connection.SQLCMD.Parameters.Add("@Param_Rem_no", SqlDbType.VarChar, 200).Value = "";
            //    connection.SQLCMD.Parameters["@Param_Rem_no"].Direction = ParameterDirection.Output;
            //    connection.SQLCMD.Parameters.AddWithValue("@Lang_Id", connection.Lang_id);
            //    connection.SQLCMD.Parameters.Add("@Param_vo_no", SqlDbType.Int).Value = "";
            //    connection.SQLCMD.Parameters["@Param_vo_no"].Direction = ParameterDirection.Output;
            //    connection.SQLCMD.Parameters.Add("@Param_case_date", SqlDbType.VarChar, 25).Value = "";
            //    connection.SQLCMD.Parameters["@Param_case_date"].Direction = ParameterDirection.Output;
            //    connection.SqlExec("Return_Rem_Online_2", connection.SQLCMD);
            //    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            //    {
            //        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
            //        connection.SQLCMD.Parameters.Clear();
            //        Clear_txt();
            //        BtnOk.Enabled = true;
            //        return;
                    
            //    }

            //}
            BtnOk.Enabled = true;
            connection.SQLCMD.Parameters.Clear();
                print_Rpt_Pdf_1();
                Clear_txt();
            }
        //------------------------------------
        private void print_Rpt_Pdf_1()
        {

            string phone = "";

            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            string Com_ToWord = "";
            string Com_ToEWord = "";
            string Frm_id = "";
            double com_amount = 0;
            string local_Cur1 = "";
            string local_ECur1 = "";
            Int16 cmb_op = 0;
            string U_Date = "";
            string forgin_ECur1 = "";
            User = connection.User_Name;
            rem_no = Txt_Rem_No.Text;
            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);
            string SqlTxt = "Exec Report_Rem_Information_new " + "'" + rem_no + "'," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Rem_Info");
            Vo_No = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString());
            U_Date = connection.SQLDS.Tables["Rem_Info"].Rows[0]["U_DATE"].ToString();

            if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0)
            {
                procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
                prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
                procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
            }
            if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]) != 0)
            {
                procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]);
                prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_aname"].ToString();
                procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_ename"].ToString();
            }

            if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)
            {
                procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]);
                prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_Aname"].ToString();
                procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_ename"].ToString();
            }

           

            local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
            local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

            forgin_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Cur_Code"].ToString();
            forgin_Cur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ACUR_NAME"].ToString();
            forgin_ECur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ECUR_NAME"].ToString();



            s_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_Ejob"].ToString();
            r_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_Ejob"].ToString();
            S_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["S_Social_No"].ToString();
            R_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Social_No"].ToString();


            if (Cmb_oper_type.SelectedIndex == 1)
            {
                 //في حال رد الصادر

                cmb_op = 1;

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                    {
                        com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]);
                        com_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_code"].ToString();
                        ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                        Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                    }
                }
                else// عملة محلية
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                    {
                        com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]);
                        com_Cur = local_Cur;
                        ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                        Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                    }
                }
            }
            else//وارد
            {
                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";
                    com_amount = 0; 
                    
                }
                else// عملة محلية
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                    com_amount = 0; 
                   
                }
            }


            //if (Cmb_oper_type.SelectedIndex != 4)
            //{
            //    if (Cmb_oper_type.SelectedIndex == 1 &&
            //        ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) == 0) ||
            //         (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) == 0)))
            //    {
            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
            //        {
            //            Oper_Id = 9;

            //        }
            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
            //        {
            //            Oper_Id = 11;
            //        }
            //    }

            //    if (Cmb_oper_type.SelectedIndex == 1 &&
            //       ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0) ||
            //        (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)))
            //    {
            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
            //        {
            //            Oper_Id = 32;

            //        }
            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
            //        {
            //            Oper_Id = 34;
            //        }
            //    }


            //    if (Cmb_oper_type.SelectedIndex == 2 &&
            //  ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) == 0) ||
            //   (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) == 0)))
            //    {

            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
            //        {
            //            Oper_Id = 5;
            //        }

            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
            //        {

            //            Oper_Id = 4;
            //        }


            //    }


            //    if (Cmb_oper_type.SelectedIndex == 2 &&
            //((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0) ||
            // (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)))
            //    {

            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
            //        {
            //            Oper_Id = 36;
            //        }

            //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
            //        {

            //            Oper_Id = 29;
            //        }


            //    }
            //}
            //else
            //{ 
            Oper_Id = Convert.ToInt16(connection.SQLDS.Tables["Rem_Info"].Rows[0]["oper_id"]); //}

            return_delivery_rem_1 ObjRpt = new return_delivery_rem_1();
            return_delivery_rem_eng_1 ObjRptEng = new return_delivery_rem_eng_1();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("local_Cur", local_Cur);
            Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", Oper_Id);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("User", User);
            Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
            Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
            Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
            Dt_Param.Rows.Add("term", term);
            Dt_Param.Rows.Add("Frm_id", Frm_id);
            Dt_Param.Rows.Add("cmb_op", cmb_op);
            Dt_Param.Rows.Add("phone", phone);
            Dt_Param.Rows.Add("com_Cur", com_Cur);
            Dt_Param.Rows.Add("U_Date", U_Date);
            Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
            Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
            Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
            Dt_Param.Rows.Add("procker", procker);
            Dt_Param.Rows.Add("procker_ename", procker_ename);
            Dt_Param.Rows.Add("prockr_name", prockr_name);
            Dt_Param.Rows.Add("s_Ejob", s_Ejob);
            Dt_Param.Rows.Add("r_Ejob", r_Ejob);
            Dt_Param.Rows.Add("S_Social_No", S_Social_No);
            Dt_Param.Rows.Add("R_Social_No", R_Social_No);
            Dt_Param.Rows.Add("Anote_report", Anote_report);
            Dt_Param.Rows.Add("Enote_report", Enote_report);

            connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);
        }
        //-------------------------------
        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-------------------------------
        private void Clear_txt()
        {
            Txt_S_Name.Text = "";
            Txt_S_Phone.Text = "";
            Txt_S_Nat_name.Text = "";
            Txt_S_City.Text = "";
            Txt_Sbirth_Date.Text = "0000/00/00" ;
            Txt_S_Doc_type.Text = "";
            Txt_S_Doc_No.Text = "";
            Txt_Doc_S_Date.Text = "0000/00/00";
            Txt_Doc_S_Issue.Text = "";
            Txt_S_job.Text = "";
            Txt_S_Addres.Text = "";
            Txt_R_Name.Text = "";
            Txt_R_Nat.Text = "";
            Txt_R_Phone.Text = "";
            Txt_R_City.Text = "";
            Txt_Rbirth_Date.Text = "0000/00/00";
            Txt_R_job.Text = "";
            Txt_R_Addres.Text = "";
            Txt_source_money.Text = "";
            Txt_Relship.Text = "";
            Txt_T_Purpose.Text = "";
            Txt_Notes.Text = "";
            Grd_Return.DataSource = new BindingSource();
        }
        //-------------------------------
        private void Txt_Rem_No_TextChanged(object sender, EventArgs e)
        {
            clear_all();
        }
        //-------------------------------
        private void clear_all()
        {
            _Bs_Returen.DataSource = new BindingSource();
            Grd_Return.DataSource = _Bs_Returen;
            Txt_S_Name.Text = "";
            Txt_S_Phone.Text = "";
            Txt_Sbirth_Date.Text = "";
            Txt_S_Doc_No.Text = "";
            Txt_Doc_S_Issue.Text = "";
            Txt_S_job.Text = "";
            Txt_source_money.Text = "";
            Txt_S_Nat_name.Text = "";
            Txt_S_City.Text = "";
            Txt_S_Doc_type.Text = "";
            Txt_Doc_S_Date.Text = "";
            Txt_S_Addres.Text = "";
            Txt_Relship.Text = "";
            Txt_R_Name.Text = "";
            Txt_R_Phone.Text = "";
            Txt_Rbirth_Date.Text = "";
            Txt_R_job.Text = "";
            Txt_R_Nat.Text = "";
            Txt_R_City.Text = "";
            Txt_R_Addres.Text = "";
            Txt_T_Purpose.Text = "";
            Txt_Notes.Text = "";
        
        
        }

        private void Cmb_oper_type_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (Cmb_oper_type.SelectedIndex == 1 )
            {

                string Sql_Text = " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,Cust_Online_Main_Id as  cust_id  ,A.Sub_Cust_ID "
                        + "   From  Sub_CUSTOMERS_online A  , remittences_online B  "
                        + "  Where A.Cust_state  =  1  "
                        + "  And B.s_cust_id IN( Select Cust_id from Terminals where t_id = " + connection.T_ID + ")"
                        + "  And A.t_id = " + connection.T_ID
                        + "  And A.Sub_Cust_ID = B.cust_broker_id  "
                        + " union "
                        + " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as Sub_Cust_ID  "
                        + "  From CUSTOMERS A, TERMINALS B   "
                        + "  Where A.CUST_ID = B.CUST_ID "
                        + "  And A.Cust_Flag <> 0  "
                        + "  and B.t_id =  " + connection.T_ID
                         + "  order by A.Sub_Cust_ID ";
                

                binding_Customers.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
                CboCust_Id.DataSource = binding_Customers;
                CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                CboCust_Id.ValueMember = "Sub_Cust_ID";

            }

            if (Cmb_oper_type.SelectedIndex == 2)
            {
                string Sql_Text = " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,Cust_Online_Main_Id as  cust_id  ,A.Sub_Cust_ID "
                          + "   From  Sub_CUSTOMERS_online A  , remittences_online B  "
                          + "  Where A.Cust_state  =  1  "
                          + "  And B.s_cust_id IN( Select Cust_id from Terminals where t_id = " + connection.T_ID + ")"
                          + "  And A.t_id = " + connection.T_ID
                          + "  And A.Sub_Cust_ID =  B.cust_paid_id " 
                          + "  order by A.Sub_Cust_ID ";

                binding_Customers.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
                CboCust_Id.DataSource = binding_Customers;
                CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                CboCust_Id.ValueMember = "Sub_Cust_ID";

            }

            if (Cmb_oper_type.SelectedIndex == 3 || Cmb_oper_type.SelectedIndex == 4)
            {
                string Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as Sub_Cust_ID  "
                + "  From CUSTOMERS A, TERMINALS B   "
                + "  Where A.CUST_ID = B.CUST_ID "
                + "  And A.Cust_Flag <> 0  "
                + "  and B.t_id =  " + connection.T_ID
                + " union "
                + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID "
                + " From  Sub_CUSTOMERS_online A    "
                + " Where A.Cust_state  =  1 "
                + " and A.t_id =  " + connection.T_ID
                + " order by cust_id ";


                binding_Customers.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
                CboCust_Id.DataSource = binding_Customers;
                CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                CboCust_Id.ValueMember = "Sub_Cust_ID";
            }
            Clear_txt();
            
        }

        private void CboCust_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            Clear_txt();
        }
    }
}