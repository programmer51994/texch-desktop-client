﻿namespace Integration_Accounting_Sys
{
    partial class Return_out_Rem_Offline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.Cmb_oper_type = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Grd_Return = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.BtnOk = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Txt_Rem_No = new System.Windows.Forms.TextBox();
            this.Txt_S_Name = new System.Windows.Forms.TextBox();
            this.Txt_S_Phone = new System.Windows.Forms.TextBox();
            this.Txt_S_Doc_No = new System.Windows.Forms.TextBox();
            this.Txt_Doc_S_Issue = new System.Windows.Forms.TextBox();
            this.Txt_S_job = new System.Windows.Forms.TextBox();
            this.Txt_source_money = new System.Windows.Forms.TextBox();
            this.Txt_S_Addres = new System.Windows.Forms.TextBox();
            this.Txt_Relship = new System.Windows.Forms.TextBox();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_job = new System.Windows.Forms.TextBox();
            this.Txt_R_Addres = new System.Windows.Forms.TextBox();
            this.Txt_T_Purpose = new System.Windows.Forms.TextBox();
            this.Txt_Notes = new System.Windows.Forms.TextBox();
            this.Txt_S_Nat_name = new System.Windows.Forms.TextBox();
            this.Txt_S_City = new System.Windows.Forms.TextBox();
            this.Txt_S_Doc_type = new System.Windows.Forms.TextBox();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CboCust_Id = new System.Windows.Forms.ComboBox();
            this.Txt_Rbirth_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Doc_S_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Sbirth_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Return)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-78, 30);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel9.TabIndex = 639;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(688, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 633;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(741, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(129, 23);
            this.TxtIn_Rec_Date.TabIndex = 9;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // Cmb_oper_type
            // 
            this.Cmb_oper_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_oper_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_oper_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_oper_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_oper_type.FormattingEnabled = true;
            this.Cmb_oper_type.Items.AddRange(new object[] {
            "اختر نوع العملية",
            "رد حوالـة صادرة منشأة",
            "رد حوالـة صادرة مرسلة فقط",
            "رد حوالـة صادرة _(منشأة,مرسلة)_",
            "رد اشعار مقبول",
            "رد اشعار مقبول بعد رد الدفع"});
            this.Cmb_oper_type.Location = new System.Drawing.Point(288, 44);
            this.Cmb_oper_type.Name = "Cmb_oper_type";
            this.Cmb_oper_type.Size = new System.Drawing.Size(208, 24);
            this.Cmb_oper_type.TabIndex = 1;
            this.Cmb_oper_type.SelectedIndexChanged += new System.EventHandler(this.Cmb_oper_type_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(221, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 645;
            this.label3.Text = "نوع العملية :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(3, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 647;
            this.label5.Text = "رقم الحوالة:";
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_OK.ForeColor = System.Drawing.Color.Navy;
            this.Btn_OK.Location = new System.Drawing.Point(798, 44);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(76, 25);
            this.Btn_OK.TabIndex = 4;
            this.Btn_OK.Text = "بحـــث";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_Search_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(3, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 16);
            this.label6.TabIndex = 789;
            this.label6.Text = "معلومات الحوالــــة...";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(500, 234);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(93, 14);
            this.label27.TabIndex = 855;
            this.label27.Text = "الجنسيـــــــــــة:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(3, 311);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(94, 14);
            this.label26.TabIndex = 848;
            this.label26.Text = "رقم الوثيقــــــة:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(500, 339);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(96, 14);
            this.label25.TabIndex = 868;
            this.label25.Text = "مهنــة المرسـل:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(465, 523);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(138, 14);
            this.label8.TabIndex = 866;
            this.label8.Text = "علاقة مرسل/ مستلم :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(-1, 523);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(98, 14);
            this.label13.TabIndex = 870;
            this.label13.Text = "مصـــدر المـــال :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(28, 234);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(69, 14);
            this.label11.TabIndex = 842;
            this.label11.Text = "الاســــــــم:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(500, 260);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(95, 14);
            this.label10.TabIndex = 858;
            this.label10.Text = "مدينة المرسل :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(0, 363);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(97, 14);
            this.label22.TabIndex = 864;
            this.label22.Text = "عنوان المرسل :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(3, 339);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(94, 14);
            this.label20.TabIndex = 853;
            this.label20.Text = "جهــة الاصـــدار:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(776, 311);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 863;
            this.label34.Text = "yyyy/mm/dd";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-233, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel3.TabIndex = 630;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(144, 219);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(791, 1);
            this.flowLayoutPanel2.TabIndex = 792;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(500, 311);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(97, 14);
            this.label18.TabIndex = 861;
            this.label18.Text = "تاريخ الوثيقــــة :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(500, 284);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 860;
            this.label9.Text = "نوع الوثيقـــــــة:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Maroon;
            this.label33.Location = new System.Drawing.Point(278, 284);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(87, 14);
            this.label33.TabIndex = 17;
            this.label33.Text = "yyyy/mm/dd";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(25, 284);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(72, 14);
            this.label24.TabIndex = 845;
            this.label24.Text = "التولــــــــــد:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(1, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 14);
            this.label7.TabIndex = 791;
            this.label7.Text = "معلومـــات المرســــل ....";
            // 
            // Grd_Return
            // 
            this.Grd_Return.AllowUserToAddRows = false;
            this.Grd_Return.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Return.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Return.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Return.ColumnHeadersHeight = 24;
            this.Grd_Return.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2,
            this.Column7,
            this.Column8,
            this.Column5,
            this.Column6,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Return.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Return.Location = new System.Drawing.Point(3, 91);
            this.Grd_Return.Name = "Grd_Return";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Return.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Return.RowHeadersVisible = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Return.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Return.Size = new System.Drawing.Size(875, 118);
            this.Grd_Return.TabIndex = 5;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحولة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "R_amount";
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "S_ACITY_NAME";
            this.Column7.HeaderText = "مدينة الارسال";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 120;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "t_ACITY_NAME";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 120;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 130;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Case_Date";
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 110;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "r_ocomm";
            this.Column9.HeaderText = "العمولة";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "r_CCur_ACUR_NAME";
            this.Column10.HeaderText = "عملة العمولة";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(23, 260);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(74, 14);
            this.label16.TabIndex = 843;
            this.label16.Text = "الهاتـــــــــف:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(503, 431);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(99, 14);
            this.label14.TabIndex = 881;
            this.label14.Text = "مدينة المستلم :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(503, 406);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(90, 14);
            this.label15.TabIndex = 878;
            this.label15.Text = "الجنسيــــــــــة:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(148, 394);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(778, 1);
            this.flowLayoutPanel4.TabIndex = 873;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-246, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel5.TabIndex = 630;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Maroon;
            this.label21.Location = new System.Drawing.Point(2, 384);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(149, 14);
            this.label21.TabIndex = 872;
            this.label21.Text = "معلومـــات المستلــــم ....";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Maroon;
            this.label23.Location = new System.Drawing.Point(274, 458);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(87, 14);
            this.label23.TabIndex = 884;
            this.label23.Text = "yyyy/mm/dd";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(25, 457);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(72, 14);
            this.label28.TabIndex = 882;
            this.label28.Text = "التولــــــــــد:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(503, 457);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(98, 14);
            this.label29.TabIndex = 885;
            this.label29.Text = "مهنـة المستلم :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(0, 483);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(97, 14);
            this.label30.TabIndex = 887;
            this.label30.Text = "عنوان المستلم:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(4, 550);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(93, 14);
            this.label32.TabIndex = 892;
            this.label32.Text = "غرض التحويل :";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(147, 514);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(763, 1);
            this.flowLayoutPanel6.TabIndex = 890;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-261, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Maroon;
            this.label35.Location = new System.Drawing.Point(0, 503);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(149, 14);
            this.label35.TabIndex = 889;
            this.label35.Text = "معلومـــات الحوالـــــــة ....";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(7, 576);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(92, 14);
            this.label31.TabIndex = 894;
            this.label31.Text = "الملاحظــــــات :";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-74, 598);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel8.TabIndex = 895;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(360, 602);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(81, 29);
            this.BtnOk.TabIndex = 6;
            this.BtnOk.Text = "موافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.Btn_Ok1_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(440, 602);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(81, 29);
            this.Btn_Ext.TabIndex = 7;
            this.Btn_Ext.Text = "انهـــاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // Txt_Rem_No
            // 
            this.Txt_Rem_No.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_No.Location = new System.Drawing.Point(66, 44);
            this.Txt_Rem_No.MaxLength = 14;
            this.Txt_Rem_No.Name = "Txt_Rem_No";
            this.Txt_Rem_No.Size = new System.Drawing.Size(149, 22);
            this.Txt_Rem_No.TabIndex = 2;
            this.Txt_Rem_No.TextChanged += new System.EventHandler(this.Txt_Rem_No_TextChanged);
            // 
            // Txt_S_Name
            // 
            this.Txt_S_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Name.Location = new System.Drawing.Point(105, 229);
            this.Txt_S_Name.MaxLength = 49;
            this.Txt_S_Name.Name = "Txt_S_Name";
            this.Txt_S_Name.ReadOnly = true;
            this.Txt_S_Name.Size = new System.Drawing.Size(389, 23);
            this.Txt_S_Name.TabIndex = 10;
            // 
            // Txt_S_Phone
            // 
            this.Txt_S_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Phone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Phone.Location = new System.Drawing.Point(105, 255);
            this.Txt_S_Phone.MaxLength = 19;
            this.Txt_S_Phone.Name = "Txt_S_Phone";
            this.Txt_S_Phone.ReadOnly = true;
            this.Txt_S_Phone.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Phone.TabIndex = 16;
            // 
            // Txt_S_Doc_No
            // 
            this.Txt_S_Doc_No.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Doc_No.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Doc_No.Location = new System.Drawing.Point(105, 306);
            this.Txt_S_Doc_No.MaxLength = 49;
            this.Txt_S_Doc_No.Name = "Txt_S_Doc_No";
            this.Txt_S_Doc_No.ReadOnly = true;
            this.Txt_S_Doc_No.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Doc_No.TabIndex = 14;
            // 
            // Txt_Doc_S_Issue
            // 
            this.Txt_Doc_S_Issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Doc_S_Issue.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_S_Issue.Location = new System.Drawing.Point(105, 334);
            this.Txt_Doc_S_Issue.MaxLength = 49;
            this.Txt_Doc_S_Issue.Name = "Txt_Doc_S_Issue";
            this.Txt_Doc_S_Issue.ReadOnly = true;
            this.Txt_Doc_S_Issue.Size = new System.Drawing.Size(389, 23);
            this.Txt_Doc_S_Issue.TabIndex = 13;
            // 
            // Txt_S_job
            // 
            this.Txt_S_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_job.Location = new System.Drawing.Point(608, 334);
            this.Txt_S_job.MaxLength = 49;
            this.Txt_S_job.Name = "Txt_S_job";
            this.Txt_S_job.ReadOnly = true;
            this.Txt_S_job.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_job.TabIndex = 23;
            // 
            // Txt_source_money
            // 
            this.Txt_source_money.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_source_money.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_source_money.Location = new System.Drawing.Point(105, 519);
            this.Txt_source_money.MaxLength = 99;
            this.Txt_source_money.Name = "Txt_source_money";
            this.Txt_source_money.ReadOnly = true;
            this.Txt_source_money.Size = new System.Drawing.Size(354, 23);
            this.Txt_source_money.TabIndex = 922;
            // 
            // Txt_S_Addres
            // 
            this.Txt_S_Addres.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Addres.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Addres.Location = new System.Drawing.Point(105, 359);
            this.Txt_S_Addres.MaxLength = 99;
            this.Txt_S_Addres.Name = "Txt_S_Addres";
            this.Txt_S_Addres.ReadOnly = true;
            this.Txt_S_Addres.Size = new System.Drawing.Size(762, 23);
            this.Txt_S_Addres.TabIndex = 12;
            // 
            // Txt_Relship
            // 
            this.Txt_Relship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relship.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Relship.Location = new System.Drawing.Point(614, 519);
            this.Txt_Relship.MaxLength = 99;
            this.Txt_Relship.Name = "Txt_Relship";
            this.Txt_Relship.ReadOnly = true;
            this.Txt_Relship.Size = new System.Drawing.Size(255, 23);
            this.Txt_Relship.TabIndex = 924;
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Name.Location = new System.Drawing.Point(105, 402);
            this.Txt_R_Name.MaxLength = 49;
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(389, 23);
            this.Txt_R_Name.TabIndex = 925;
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Phone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Phone.Location = new System.Drawing.Point(105, 427);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_Phone.TabIndex = 926;
            // 
            // Txt_R_job
            // 
            this.Txt_R_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_job.Location = new System.Drawing.Point(611, 453);
            this.Txt_R_job.MaxLength = 49;
            this.Txt_R_job.Name = "Txt_R_job";
            this.Txt_R_job.ReadOnly = true;
            this.Txt_R_job.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_job.TabIndex = 927;
            // 
            // Txt_R_Addres
            // 
            this.Txt_R_Addres.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Addres.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Addres.Location = new System.Drawing.Point(105, 480);
            this.Txt_R_Addres.MaxLength = 99;
            this.Txt_R_Addres.Name = "Txt_R_Addres";
            this.Txt_R_Addres.ReadOnly = true;
            this.Txt_R_Addres.Size = new System.Drawing.Size(765, 23);
            this.Txt_R_Addres.TabIndex = 928;
            // 
            // Txt_T_Purpose
            // 
            this.Txt_T_Purpose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_T_Purpose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_T_Purpose.Location = new System.Drawing.Point(105, 545);
            this.Txt_T_Purpose.MaxLength = 199;
            this.Txt_T_Purpose.Name = "Txt_T_Purpose";
            this.Txt_T_Purpose.ReadOnly = true;
            this.Txt_T_Purpose.Size = new System.Drawing.Size(763, 23);
            this.Txt_T_Purpose.TabIndex = 929;
            // 
            // Txt_Notes
            // 
            this.Txt_Notes.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Notes.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Notes.Location = new System.Drawing.Point(105, 571);
            this.Txt_Notes.MaxLength = 199;
            this.Txt_Notes.Name = "Txt_Notes";
            this.Txt_Notes.ReadOnly = true;
            this.Txt_Notes.Size = new System.Drawing.Size(763, 23);
            this.Txt_Notes.TabIndex = 930;
            // 
            // Txt_S_Nat_name
            // 
            this.Txt_S_Nat_name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Nat_name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Nat_name.Location = new System.Drawing.Point(608, 229);
            this.Txt_S_Nat_name.MaxLength = 49;
            this.Txt_S_Nat_name.Name = "Txt_S_Nat_name";
            this.Txt_S_Nat_name.ReadOnly = true;
            this.Txt_S_Nat_name.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Nat_name.TabIndex = 22;
            // 
            // Txt_S_City
            // 
            this.Txt_S_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_City.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_City.Location = new System.Drawing.Point(608, 255);
            this.Txt_S_City.MaxLength = 49;
            this.Txt_S_City.Name = "Txt_S_City";
            this.Txt_S_City.ReadOnly = true;
            this.Txt_S_City.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_City.TabIndex = 20;
            // 
            // Txt_S_Doc_type
            // 
            this.Txt_S_Doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Doc_type.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Doc_type.Location = new System.Drawing.Point(608, 279);
            this.Txt_S_Doc_type.MaxLength = 49;
            this.Txt_S_Doc_type.Name = "Txt_S_Doc_type";
            this.Txt_S_Doc_type.ReadOnly = true;
            this.Txt_S_Doc_type.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Doc_type.TabIndex = 19;
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Nat.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Nat.Location = new System.Drawing.Point(611, 402);
            this.Txt_R_Nat.MaxLength = 49;
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_Nat.TabIndex = 937;
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_City.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_City.Location = new System.Drawing.Point(611, 427);
            this.Txt_R_City.MaxLength = 49;
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_City.TabIndex = 938;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(881, 632);
            this.shapeContainer1.TabIndex = 642;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.Location = new System.Drawing.Point(3, 82);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(871, 0);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(28, 408);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(69, 14);
            this.label19.TabIndex = 939;
            this.label19.Text = "الاســــــــم:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(23, 431);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(74, 14);
            this.label17.TabIndex = 940;
            this.label17.Text = "الهاتـــــــــف:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(1, 8);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 14);
            this.label36.TabIndex = 942;
            this.label36.Text = "المستخدم:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(71, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(168, 23);
            this.TxtUser.TabIndex = 6;
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(556, 4);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(134, 23);
            this.TxtBox_User.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(496, 8);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(62, 14);
            this.label12.TabIndex = 946;
            this.label12.Text = "الصندوق:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(241, 8);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(94, 14);
            this.label37.TabIndex = 944;
            this.label37.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(337, 3);
            this.Txt_Loc_Cur.Multiline = true;
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(159, 25);
            this.Txt_Loc_Cur.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(497, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 951;
            this.label2.Text = "الوكيل:";
            // 
            // CboCust_Id
            // 
            this.CboCust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCust_Id.FormattingEnabled = true;
            this.CboCust_Id.Location = new System.Drawing.Point(540, 44);
            this.CboCust_Id.Name = "CboCust_Id";
            this.CboCust_Id.Size = new System.Drawing.Size(252, 24);
            this.CboCust_Id.TabIndex = 3;
            this.CboCust_Id.SelectedIndexChanged += new System.EventHandler(this.CboCust_Id_SelectedIndexChanged);
            // 
            // Txt_Rbirth_Date
            // 
            this.Txt_Rbirth_Date.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Rbirth_Date.DateSeperator = '/';
            this.Txt_Rbirth_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rbirth_Date.Location = new System.Drawing.Point(105, 454);
            this.Txt_Rbirth_Date.Mask = "0000/00/00";
            this.Txt_Rbirth_Date.Name = "Txt_Rbirth_Date";
            this.Txt_Rbirth_Date.PromptChar = ' ';
            this.Txt_Rbirth_Date.ReadOnly = true;
            this.Txt_Rbirth_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Rbirth_Date.Size = new System.Drawing.Size(165, 23);
            this.Txt_Rbirth_Date.TabIndex = 933;
            this.Txt_Rbirth_Date.Text = "00000000";
            this.Txt_Rbirth_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txt_Rbirth_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Doc_S_Date
            // 
            this.Txt_Doc_S_Date.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Doc_S_Date.DateSeperator = '/';
            this.Txt_Doc_S_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_S_Date.Location = new System.Drawing.Point(609, 307);
            this.Txt_Doc_S_Date.Mask = "0000/00/00";
            this.Txt_Doc_S_Date.Name = "Txt_Doc_S_Date";
            this.Txt_Doc_S_Date.PromptChar = ' ';
            this.Txt_Doc_S_Date.ReadOnly = true;
            this.Txt_Doc_S_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Doc_S_Date.Size = new System.Drawing.Size(164, 23);
            this.Txt_Doc_S_Date.TabIndex = 18;
            this.Txt_Doc_S_Date.Text = "00000000";
            this.Txt_Doc_S_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txt_Doc_S_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Sbirth_Date
            // 
            this.Txt_Sbirth_Date.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Sbirth_Date.DateSeperator = '/';
            this.Txt_Sbirth_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sbirth_Date.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Txt_Sbirth_Date.Location = new System.Drawing.Point(105, 280);
            this.Txt_Sbirth_Date.Mask = "0000/00/00";
            this.Txt_Sbirth_Date.Name = "Txt_Sbirth_Date";
            this.Txt_Sbirth_Date.PromptChar = ' ';
            this.Txt_Sbirth_Date.ReadOnly = true;
            this.Txt_Sbirth_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Sbirth_Date.Size = new System.Drawing.Size(167, 23);
            this.Txt_Sbirth_Date.TabIndex = 15;
            this.Txt_Sbirth_Date.Text = "00000000";
            this.Txt_Sbirth_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txt_Sbirth_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Return_out_Rem_Offline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 632);
            this.Controls.Add(this.CboCust_Id);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.Txt_S_Doc_type);
            this.Controls.Add(this.Txt_S_City);
            this.Controls.Add(this.Txt_S_Nat_name);
            this.Controls.Add(this.Txt_Rbirth_Date);
            this.Controls.Add(this.Txt_Doc_S_Date);
            this.Controls.Add(this.Txt_Sbirth_Date);
            this.Controls.Add(this.Txt_Notes);
            this.Controls.Add(this.Txt_T_Purpose);
            this.Controls.Add(this.Txt_R_Addres);
            this.Controls.Add(this.Txt_R_job);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.Txt_Relship);
            this.Controls.Add(this.Txt_S_Addres);
            this.Controls.Add(this.Txt_source_money);
            this.Controls.Add(this.Txt_S_job);
            this.Controls.Add(this.Txt_Doc_S_Issue);
            this.Controls.Add(this.Txt_S_Doc_No);
            this.Controls.Add(this.Txt_S_Phone);
            this.Controls.Add(this.Txt_S_Name);
            this.Controls.Add(this.Txt_Rem_No);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Grd_Return);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cmb_oper_type);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.shapeContainer1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Return_out_Rem_Offline";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "503";
            this.Text = "رد الحوالات";
            this.Load += new System.EventHandler(this.Returen_Delivery_Rem_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Return)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.ComboBox Cmb_oper_type;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView Grd_Return;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.TextBox Txt_Rem_No;
        private System.Windows.Forms.TextBox Txt_S_Name;
        private System.Windows.Forms.TextBox Txt_S_Phone;
        private System.Windows.Forms.TextBox Txt_S_Doc_No;
        private System.Windows.Forms.TextBox Txt_Doc_S_Issue;
        private System.Windows.Forms.TextBox Txt_S_job;
        private System.Windows.Forms.TextBox Txt_source_money;
        private System.Windows.Forms.TextBox Txt_S_Addres;
        private System.Windows.Forms.TextBox Txt_Relship;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_R_job;
        private System.Windows.Forms.TextBox Txt_R_Addres;
        private System.Windows.Forms.TextBox Txt_T_Purpose;
        private System.Windows.Forms.TextBox Txt_Notes;
        private MyDateTextBox Txt_Sbirth_Date;
        private MyDateTextBox Txt_Doc_S_Date;
        private MyDateTextBox Txt_Rbirth_Date;
        private System.Windows.Forms.TextBox Txt_S_Nat_name;
        private System.Windows.Forms.TextBox Txt_S_City;
        private System.Windows.Forms.TextBox Txt_S_Doc_type;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.TextBox Txt_R_City;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CboCust_Id;
    }
}