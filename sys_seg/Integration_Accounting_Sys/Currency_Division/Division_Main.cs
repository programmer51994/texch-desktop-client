﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Division_Main : Form
    {
        #region Defintion
            string Sql_Text = "";
            int MCur_id = 0;
            bool Cbo_Change = false;
            bool Change = false;
            string cur_id = "";
            Byte[] img_data = new Byte[0];
          public static   BindingSource Bs_Division_Main = new BindingSource();
          public static  BindingSource Bs_Division = new BindingSource();
            string Division;
            string Cond_Str = "";
        #endregion

        public Division_Main()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            Page_Setting.Header_Page((TextBox)TxtUser.Control, TxtIn_Rec_Date);
            
            Grd_Cur_div.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Cur_div.Columns["Column2"].DataPropertyName = "Cur_Ename";
            }
                      
        }
       //---------------------------
        private void Display_Pic()
        {
            img_data = (Byte[])((DataRowView)Bs_Division_Main.Current).Row["cur_pic"];
            MemoryStream mem = new MemoryStream(img_data);
            pictureBox1.Image = Image.FromStream(mem);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        //-------------------------
        private void Division_Main_Load(object sender, EventArgs e)
        {
            Cbo_Change = false;

            //TxtCur_TextChanged(sender, e);
            string Sql_Text = "Select cur_aname , cur_ename  ,cur_id  from cur_tbl where cur_id != 0"
            + " union "
            + " Select 'جميع العملات' As cur_aname , 'All Curencies ' AS cur_ename , -1 as cur_id from cur_tbl order by Cur_ID ";
            Cbo_Cur_ID.DataSource = connection.SqlExec(Sql_Text, "Cur_TBL");
            Cbo_Cur_ID.DisplayMember = connection.Lang_id == 1 ? "cur_aname" : "cur_ename";
            Cbo_Cur_ID.ValueMember = "cur_id";
            if (connection.SQLDS.Tables["Cur_TBL"].Rows.Count > 0)
            {
                Cbo_Change = true;
                Cbo_Cur_ID_SelectedIndexChanged(null, null);
            }

        }
        //-------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            Bs_Division_Main.MoveNext();
            Display_Pic();
        }
        //-------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            Bs_Division_Main.MovePrevious();
            Display_Pic();
        }
        //-------------------------
        private void TxtCur_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Cbo_Change = true;
                Cbo_Cur_ID_SelectedIndexChanged(null, null);
            }
            catch { }
        }
        //-------------------------
        private void Grd_Cur_div_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {

                Division = ((DataRowView)Bs_Division.Current).Row["division"].ToString();
                cur_id = ((DataRowView)Bs_Division.Current).Row["cur_id"].ToString();
                Sql_Text = "SELECT  id_identity_cur ,name_pic,CAST(CAST(cur_pic as varchar(max)) as varbinary(max)) as cur_pic "
                        + " FROM cur_pic "
                        + " where cur_id =  " + cur_id
                        + " and division_cur = " + Division;
                Bs_Division_Main.DataSource = connection.SqlExec(Sql_Text, "Pic_Tbl");

                try
                {
                    Display_Pic();
                    if (Bs_Division_Main.List.Count == 1)
                    {
                        AddBtn.Enabled = false;
                        Btn_Excel.Enabled = false;
                    }
                    else
                    {
                        Btn_Excel.Enabled = true;
                        AddBtn.Enabled = true;
                    }
                }
                catch
                {
                    pictureBox1.Image = null;
                    AddBtn.Enabled = false;
                    Btn_Excel.Enabled = false;
                }
            }
        }
        //-------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Division_Add AddFrm = new Division_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Division_Main_Load(null, null);
            this.Visible = true;
        }
        //-------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
           
            Division_Upd UpdFrm = new Division_Upd();
            this.Visible = false;
            UpdFrm.ShowDialog();
            Division_Main_Load(null, null);
            this.Visible = true;
        }
        //-------------------------
        private void Division_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;

            Cbo_Change = false;

 
            string[] Used_Tbl = { "cur_Tbl", "Pic_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-----------------------------------------------------------------
        private void Cbo_Cur_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_Change)
            {
                if (Cbo_Cur_ID.SelectedIndex == 0)
                {
                    Cond_Str = "";
                       
                }
                else
                {
                    Cond_Str = " And A.cur_id =" + Cbo_Cur_ID.SelectedValue;  
                }

                Sql_Text = "SELECT Cast(A.cur_id as Varchar(5))As cur_id1,A.cur_id , 'division1' = case  when CAST(division as int ) > 0 then "
                 + " cast(CAST(division as int )AS varchar(40)) when  CAST(division as int )  <= 0  then "
                 + " cast(division as varchar(50))  end,division, B.Cur_ANAME ,B.Cur_ENAME ,B.Cur_ANAME as Acur_name,B.Cur_ENAME as Ecur_name "
                 + " FROM  currency_division A, Cur_Tbl B "
                 + " Where A.cur_id = B.Cur_ID "
                 + " and (B.Cur_Aname like '%" + TxtCur.Text + "%'"
                 + " or B.Cur_ename like  '%" + TxtCur.Text + "%'"
                 + " or B.Cur_id like '" + TxtCur.Text + "')"  
                 + Cond_Str;
                connection.SqlExec(Sql_Text, "cur_Tbl");

                #region Grouping
                string strCurrentValue = string.Empty;
                string strPreviousValue = string.Empty;
                string cur_id = string.Empty;

                DataTable DT = new DataTable();
                DT = connection.SQLDS.Tables["cur_Tbl"];
                for (int m = 0; m < DT.Rows.Count; m++)
                {
                    object cellValue = DT.Rows[m]["Cur_Aname"]; // Catch the value form current column
                    object cellValue2 = DT.Rows[m]["Cur_id1"]; // Catch the value form current column

                    strCurrentValue = cellValue.ToString().Trim(); // Assign the above value to 'strCurrentValue'
                    cur_id = cellValue2.ToString().Trim();
                    if (strCurrentValue != strPreviousValue) // Now compare the current value with previous value
                    {
                        DT.Rows[m]["Cur_Aname"] = strCurrentValue;	// If current value is not equal to previous value 
                        DT.Rows[m]["Cur_id1"] = cur_id; ;

                        // the column will display current value
                    }
                    else
                    {
                        DT.Rows[m]["Cur_Aname"] = string.Empty;	// If current value is equal to previous value 
                        DT.Rows[m]["Cur_id1"] = string.Empty;

                        // the column will be empty	
                    }
                    strPreviousValue = strCurrentValue;	        // Assign current value to previous value
                }
                strCurrentValue = string.Empty;					// Reset Current and Previous Value
                strPreviousValue = string.Empty;
                cur_id = string.Empty;
                #endregion
                Bs_Division.DataSource = DT;
                Grd_Cur_div.DataSource = Bs_Division;
                if (connection.SQLDS.Tables["cur_Tbl"].Rows.Count > 0)
                {
                    Change = true;
                    Grd_Cur_div_SelectionChanged(sender, e);
                }

            }
        }

        private void Division_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Division_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        
    }
}