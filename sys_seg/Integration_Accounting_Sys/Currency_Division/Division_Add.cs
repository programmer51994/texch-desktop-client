﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace Integration_Accounting_Sys
{

    public partial class Division_Add : Form
    {
        #region Defintion
            string Sql_Text = "";
            string Pic_Name = "";
            //string cur_pic = "";
            int Frm_Id = 1;
            ArrayList ItemList = new ArrayList();
            DataTable Pic_Tbl = new DataTable();
            BindingSource _BS_add = new BindingSource();
        #endregion
        public Division_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Lst_Cur_id.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Lst_Cur_id.Columns["Column2"].DataPropertyName = "cur_ename";
            }
            Create_Pic_Tbl();
        }
        //--------------------------------
        private void Create_Pic_Tbl()
        {
            string[] Column = { "Id_identity_cur", "Name_pic", "Cur_pic" };
            string[] DType = {" System.Int32","System.String", "System.Byte[]" };
            Pic_Tbl = CustomControls.Custom_DataTable("Pic_Tbl", Column, DType);
        }
     
        //--------------------------------
        private void Division_Add_Load(object sender, EventArgs e)
        {
            TxtCur_Name_TextChanged(sender, e);
        }
        //----------------------------------
        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {
            Sql_Text = "SELECT  Cur_ID, Cur_ANAME, Cur_ENAME FROM Cur_Tbl"
                + " Where cur_id <> 0 And (cur_aname like '%" + TxtCur_Name.Text + "%'"
                + " Or cur_ename like '%" + TxtCur_Name.Text + "%'"
                + " Or cur_id like '%" + TxtCur_Name.Text + "%')"
                + " Order By " + (connection.Lang_id ==1? "Cur_ANAME":"Cur_ENAME");
            _BS_add.DataSource = connection.SqlExec(Sql_Text, "cur_Tbl");
            Lst_Cur_id.DataSource = _BS_add;

        }
        //----------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }
        //----------------------------------
       private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtDiv.Text == "0.000")
            {
                MessageBox.Show(connection.Lang_id==1 ? "حدد الفئة":"Select the division please",MyGeneral_Lib.LblCap);
                TxtDiv.Focus();
                return;
            }
            if (Lst_Cur_id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد العملــة" : "Select the currency please", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion

           
           DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? " هل تريد اضافة صورة":
            "Do you want to add picture for this Division", MyGeneral_Lib.LblCap,
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Dr == DialogResult.Yes)
            {
                OpenFileDialog Open_Pic = new OpenFileDialog();
                Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|" + "All files (*.*)|*.*";
                Open_Pic.Multiselect = true;
                Open_Pic.Title = "My Image Browser";
                if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
                {
                    foreach (String file in Open_Pic.FileNames)
                    {

                        FileStream FS = new FileStream(file, FileMode.Open, FileAccess.Read);
                        byte[] img = new byte[FS.Length];
                        FS.Read(img, 0, Convert.ToInt32(FS.Length));
                        FS.Close();
                        Pic_Name = Path.GetFileName(file);
                     //   cur_pic = System.Text.Encoding.Default.GetString(img);
                        DataRow row = Pic_Tbl.NewRow();
                        row.SetField("Name_pic", Pic_Name);
                        row.SetField("Cur_Pic", img);
                        Pic_Tbl.Rows.Add(row);
                     }
                }
            }
            connection.SQLCMD.Parameters.AddWithValue("@Cur_id", ((DataRowView)_BS_add.Current).Row["cur_id"]);
            connection.SQLCMD.Parameters.AddWithValue("@Division_cur", Convert.ToDecimal(TxtDiv.Text));
            connection.SQLCMD.Parameters.AddWithValue("@Div_Pic_Tbl", Pic_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Frm_id", Frm_Id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Division_Add_Upd", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
         
            
        }
        //----------------------------------
        private void Division_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
       
            string[] Used_Tbl = { "cur_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
       
    }
}