﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

namespace Integration_Accounting_Sys
{

    public partial class Division_Upd : Form
    {
        #region Defintion
        string Sql_Text = "";
        string cur_pic = "";
        string Pic_Name = "";
        int Frm_Id = 2;
        int Flag = 0;
        ArrayList ItemList = new ArrayList();
        Byte[] img_data = new Byte[0];
        byte[] img;
        OpenFileDialog Open_Pic;
        DataTable Pic_Tbl = new DataTable();
        BindingSource _BS = new BindingSource();
        #endregion
        public Division_Upd()
        {
            InitializeComponent(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
           

        }
        //-------------------------------
        //private void Create_Pic_Tbl()
        //{
        //    string[] Column = { "Id_identity_cur", "Name_pic", "Cur_pic" };
        //    string[] DType = { " System.Int32", "System.String", "System.String" };
        //    Pic_Tbl = CustomControls.Custom_DataTable("Pic_Tbl", Column, DType);
        //}
        //-------------------------
        private void Display_Pic()
        {
            img_data = (Byte[])((DataRowView)_BS.Current).Row["cur_pic"];
            MemoryStream mem = new MemoryStream(img_data);
            pictureBox1.Image = Image.FromStream(mem);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        //-------------------------
        private void Division_Upd_Load(object sender, EventArgs e)
        {

            Pic_Tbl = ((DataTable)Division_Main.Bs_Division_Main.DataSource).DefaultView.ToTable(false, "Id_identity_cur", "Name_pic", "Cur_pic");
            _BS.DataSource = Pic_Tbl ;
            TxtCur_Name.Text = ((DataRowView)Division_Main.Bs_Division.Current).Row[connection.Lang_id == 1 ? "Acur_name" : "Ecur_name"].ToString();
            TxtCur_Name.Tag = ((DataRowView)Division_Main.Bs_Division.Current).Row["cur_id"];
            TxtDiv.Text = ((DataRowView)Division_Main.Bs_Division.Current).Row["division"].ToString();
            try
            {
                Display_Pic();
                NxtBtn.Enabled = true ;
                PerBtn.Enabled = true ;
            }
            catch
            {
                pictureBox1.Image = null;
                NxtBtn.Enabled = false;
                PerBtn.Enabled = false;
                Btn_Browser.Enabled = false;
            }

        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //---------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {


            connection.SQLCMD.Parameters.AddWithValue("@Cur_id", ((DataRowView)Division_Main.Bs_Division.Current).Row["cur_id"]);
            connection.SQLCMD.Parameters.AddWithValue("@Division_cur", ((DataRowView)Division_Main.Bs_Division.Current).Row["division"]);
            connection.SQLCMD.Parameters.AddWithValue("@Div_Pic_Tbl", Pic_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Frm_id", Frm_Id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Division_Add_Upd", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            Pic_Tbl.Rows.Clear();
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
                
        private void Division_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
           

            string[] Used_Tbl = { "cur_Tbl", "Pic_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {

            OpenFileDialog Open_Pic = new OpenFileDialog();
            Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|" + "All files (*.*)|*.*";
            Open_Pic.Multiselect = true;
            Open_Pic.Title = "My Image Browser";
            if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
            {
                foreach (String file in Open_Pic.FileNames)
                {

                    FileStream FS = new FileStream(file, FileMode.Open, FileAccess.Read);
                    byte[] img = new byte[FS.Length];
                    FS.Read(img, 0, Convert.ToInt32(FS.Length));
                    FS.Close();
                    Pic_Name = Path.GetFileName(file);
                   // cur_pic = System.Text.Encoding.Default.GetString(img);
                    DataRow row = Pic_Tbl.NewRow();
                    row.SetField("Name_pic", Pic_Name);
                    row.SetField("Cur_Pic", img);
                    //row.SetField("Cur_pic1", img);
                    Pic_Tbl.Rows.Add(row);
                    NxtBtn_Click(sender, e);
                  
                    
                }
            }
        }

        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            OpenFileDialog Open_Pic = new OpenFileDialog();
            Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|" + "All files (*.*)|*.*";
            Open_Pic.Title = "My Image Browser";
            if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
            {
                FileStream FS = new FileStream(Open_Pic.FileName, FileMode.Open, FileAccess.Read);
                img = new byte[FS.Length];
                FS.Read(img, 0, Convert.ToInt32(FS.Length));
                FS.Close();
                pictureBox1.Image = new Bitmap(Open_Pic.FileName);
                Pic_Name = Path.GetFileName(Open_Pic.FileName);
                //cur_pic = System.Text.Encoding.Default.GetString(img);
                Pic_Tbl.Rows[_BS.Position]["Name_pic"] = Pic_Name;
                Pic_Tbl.Rows[_BS.Position]["Cur_Pic"] = img;
              
            }
        }

        private void PerBtn_Click(object sender, EventArgs e)
        {
            _BS.MovePrevious();
            Display_Pic();
        }

        private void NxtBtn_Click(object sender, EventArgs e)
        {
            _BS.MoveNext();
            Display_Pic();
        }
    }
}