﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Frm_Cities : Form
    {
        bool change = false;
        string ChildTbl = "";
        public static string Col = "";
        public Frm_Cities()
        {
            InitializeComponent();
            connection.SQLBS.DataSource = new BindingSource();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #endregion
            
            int Frm_Id_Main = connection.Frm_Id_Main;

            #region Caption
            if (connection.Lang_id == 1)
            {
                if (connection.Frm_Id == 1) this.Text = "الـمـدن";
                else if (connection.Frm_Id == 31) this.Text = "المـنـاطـــق";
                if (connection.Frm_Id_Main == 4) label1.Text = "اســـــــــم البــــــلـــــــد:";
                else if (connection.Frm_Id_Main == 1) label1.Text = "اســـــــــم المدينـــــــة:";
            }
            else
            {
                if (connection.Frm_Id == 1) this.Text = "Cities";
                else if (connection.Frm_Id == 31) this.Text = "Zones";
                if (connection.Frm_Id_Main == 4) label1.Text = "Country Name : ";
                else if (connection.Frm_Id_Main == 1) label1.Text = "City Name : ";
            }
            #endregion
        }
        
        private void Frm_Main_Cities_Load(object sender, EventArgs e)
        {
            change = false;
            object[] Sparams = { connection.Frm_Id, connection.Frm_Id_Main, "" };
            CboCoun_Id.DataSource = connection.SqlExec("Get_Data", "Cbo_Tbl",Sparams);
            int CC = connection.Col_Name.IndexOf(',');
             Col = connection.Col_Name.Substring(0,CC);
            ChildTbl = connection.Col_Name.Substring(Col.Length + 1);
            CboCoun_Id.DisplayMember = connection.Lang_id == 1 ? Col + "_aname" : Col + "_Ename";
            CboCoun_Id.ValueMember = Col + "_id";
            change = true;
            CboCoun_Id_SelectedIndexChanged(sender, e) ;
        }

        private void CboCoun_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change)
            {
                object[] parameters = {connection.Frm_Id,connection.Frm_Id_Main, Convert.ToInt16(CboCoun_Id.SelectedValue) };
                connection.SQLBS.DataSource = connection.SqlExec("Main_City", "City_Tbl", parameters);
                GrdCit_Tbl.DataSource = connection.SQLBS;
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            add_upd_Cit AddFrm = new add_upd_Cit(1, "", "", "", 1, Convert.ToInt16(CboCoun_Id.SelectedValue));
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Frm_Main_Cities_Load( sender,  e);
            this.Visible = true;
        }

        private void Btn_upd_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(GrdCit_Tbl.CurrentRow.Cells["Column1"].Value.ToString());
            string AName = GrdCit_Tbl.CurrentRow.Cells["Column2"].Value.ToString();
            string EName = GrdCit_Tbl.CurrentRow.Cells["Column3"].Value.ToString();
            string Code = GrdCit_Tbl.CurrentRow.Cells["Column4"].Value.ToString();


            add_upd_Cit frm = new add_upd_Cit(id, AName, EName, Code, 2, Convert.ToInt16(CboCoun_Id.SelectedValue));
            this.Visible = false;
            frm.ShowDialog(this);
            this.Visible = true;
            Frm_Main_Cities_Load(sender, e);
          
        }

        private void GrdCit_Tbl_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records();
        }

        private void Frm_Cities_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }

        private void Frm_Cities_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Add_Click(sender, e);
                    break;
                case Keys.F7:
                    Btn_upd_Click(sender, e);
                    break;
                //case Keys.F5:
                //    Btn_Search_Click(sender, e);
                //    break;
                //case Keys.F4:
                //    Btn_ShowAll_Click(sender, e);
                //    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Frm_Cities_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
            if (connection.SQLDS.Tables.Contains("Cbo_Tbl"))
                connection.SQLDS.Tables.Remove("Cbo_Tbl");
            if (connection.SQLDS.Tables.Contains("City_Tbl"))
                connection.SQLDS.Tables.Remove("City_Tbl");
        }

        
    }
}
