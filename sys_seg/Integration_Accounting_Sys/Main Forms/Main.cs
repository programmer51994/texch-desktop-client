﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Management;

namespace Integration_Accounting_Sys
{
    public partial class Main : Form
    {
        #region Declaration
        bool isLogin = false;
        bool isCompChange = false;
        string PEXP_DATE = "";
        int period_pass = 0;
        string sqltxt_user = "";
       
        public static Int32 current_user_id = 0;
        #endregion

        public Main()
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #endregion
            CboLang_id.SelectedIndex = 0;
            Cbo_Term.Enabled = false;
           // LblRec.Text = "Copyright © AlTaif Consulting Co Ltd. All Rights Reserved.";

            #region   Client Background

            DateTime Day = Convert.ToDateTime(connection.SQLDS.Tables["TblObj"].Rows[0]["CurrentDate"]);
            switch (Day.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    Pic_Backgrnd.Image = Integration_Accounting_Sys.Properties.Resources.dollar_1;
                    break;
                case DayOfWeek.Sunday:
                    Pic_Backgrnd.Image = Integration_Accounting_Sys.Properties.Resources.dollar_1;
                    break;
                case DayOfWeek.Monday:
                    Pic_Backgrnd.Image = Integration_Accounting_Sys.Properties.Resources.dollar_1;
                    break;
                case DayOfWeek.Tuesday:
                    Pic_Backgrnd.Image = Integration_Accounting_Sys.Properties.Resources.dollar_1;
                    break;
                case DayOfWeek.Wednesday:
                    Pic_Backgrnd.Image = Integration_Accounting_Sys.Properties.Resources.dollar_1;
                    break;
                case DayOfWeek.Thursday:
                    Pic_Backgrnd.Image = Integration_Accounting_Sys.Properties.Resources.dollar_1;
                    break;
            }
            #endregion
        }
        //-------------------
        #region Single Instance at Time
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == NativeMethods.WM_SHOWME)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Maximized;
                }
                // get our current "TopMost" value (ours will always be false though)
                bool top = this.TopMost;
                // make our form jump to the top of everything
                TopMost = true;
                // set it back to whatever it was
                this.TopMost = top;
            }
            base.WndProc(ref m);
        }
        #endregion
        //-------------------
        private void MessageBoxButtoneCaption()
        {
            MessageBoxManager.Yes = "نعم";
            MessageBoxManager.No = "كلا";
            MessageBoxManager.OK = "حسنا";
            MessageBoxManager.Ignore = "تجاهل";
            MessageBoxManager.Abort = "إحباط";
            MessageBoxManager.Cancel = "إلغاء";
            MessageBoxManager.Retry = "إعادة المحاولة";
            MessageBoxManager.Register();
        }
        //-------------------
        private void Main_Load(object sender, EventArgs e)
        {
            TxtUser_Name.Focus();
            Ver_No.Text = "Version No. : 1.5";
        }
        //------------------------
        private void trV_Main_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Name.Length > 0)
            {
                // Get the Assembly that defines the Form [class] 
                Assembly a = Assembly.GetEntryAssembly();

                // Load the RunTime Type for the Form
                System.Type t = a.GetType("Integration_Accounting_Sys." + e.Node.Name);
                if (!string.IsNullOrEmpty(e.Node.Tag.ToString()))
                {
                    connection.Frm_Id = Convert.ToInt16(e.Node.Tag);
                    MyGeneral_Lib.Tag_Edit = Convert.ToInt16(e.Node.Tag);
                    MyGeneral_Lib.Emp_Out = Convert.ToInt16(e.Node.Tag);

                    if (!string.IsNullOrEmpty(e.Node.ImageKey.ToString()))
                    {
                        connection.Frm_Id_Main = Convert.ToInt16(e.Node.ImageKey);
                        connection.Node_id = Convert.ToInt32(e.Node.ToolTipText);
                    }
                }
                try
                {
                    // Create an instance of the Form ... 
                    object o = Activator.CreateInstance(t);
                    Form frm = (Form)o;

                    // ... and show it 
                    frm.ShowDialog(this);
                    trV_Main.SelectedNode = e.Node.Parent;
                }
                catch { }
            }
        }
        //-------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Valdation
            if (TxtUser_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل اسم المستخدم" : "Please Enter User Name");
                TxtUser_Name.Focus();
                return;
            }

            if (TxtPwd.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل كلمة المرور" : "Please Enter Password");
                TxtPwd.Focus();
                return;
            }
            #endregion
            isLogin = false;
            string CPU_ID = GetProcessorID();
            string BaseBordID = GetBaseBordID(); 
            object[] Sparams = { TxtUser_Name.Text.Trim(), TxtPwd.Text.Trim(), CPU_ID,BaseBordID, "", "" };



            //MyGeneral_Lib.Copytocliptext("Logins_Main", Sparams);

            connection.SqlExec("Logins_Main", "User_Tbl", Sparams);
            if (connection.Col_Name != "" && connection.Col_Name != "1" )
            {
                MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                return;
            }
            //if (connection.Col_Name == "1")
            //{
            //    Active_users Frm = new Active_users();
            //   // this.Visible = false;
            //    Frm.ShowDialog();
            //   // this.Visible = true;
            //}


            DataTable table_user = new DataTable();
            table_user = connection.SQLDS.Tables["User_Tbl"];
            current_user_id = 0;
            
           //current_user_id = Convert.ToInt32(connection.SQLDS.Tables["User_Tbl"].Rows[0]["User_Id"].ToString());
            if (connection.Col_Name != "")
            {
                //MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);

                DataTable Nodes = connection.SqlExec("select Node_Id from Terminal_Permissions "
                    + "where Frm_Name like 'log_out_users' ", "node_tbl");

                int the_node = Convert.ToInt32(connection.SQLDS.Tables["node_tbl"].Rows[0]["Node_Id"].ToString());

                DataTable user_form_id = connection.SqlExec("select Per_User_Id from terminal_User_Permission "
                    + "where Node_Id = " + the_node, "node_tbl1");

               
                //MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap,
                //    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                
                try
                {
                    Int32 user_form_id1 = Convert.ToInt32(connection.SQLDS.Tables["node_tbl1"].Rows[0]["Per_User_Id"].ToString());
                    current_user_id = Convert.ToInt32(connection.SQLDS.Tables["User_Tbl"].Rows[0]["User_Id"].ToString());

                    if (current_user_id == user_form_id1)
                    {
                        DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد الخروج من البرنامج  ؟" :
                        "Do you want to logout of the system", MyGeneral_Lib.LblCap,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (Dr == DialogResult.Yes)
                        {
                            log_out_users Frm = new log_out_users();
                            this.Visible = false;
                            Frm.ShowDialog();
                            this.Visible = true;
                        }

                    }


                }
                catch
                {
                }
                return;
            }


            //if (connection.Col_Name != "")
            //{
            //    MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
            //    return;
            //}

            connection.user_id = Convert.ToInt16(connection.SQLDS.Tables["User_Tbl"].Rows[0]["User_Id"]);
            connection.User_Name = Convert.ToString(connection.SQLDS.Tables["User_Tbl"].Rows[0]["User_name"]);

            connection.Lang_id = CboLang_id.SelectedIndex == 0 ? 1 : 2;
            ExtBtn.Enabled = true;
            isLogin = true;
            BtnOk.Enabled = false;
            TxtUser_Name.Enabled = false;
            TxtPwd.Enabled = false;
            CboLang_id.Enabled = false;
            Cbo_Term.Enabled = true;
            connection.User_Name = TxtUser_Name.Text.Trim();

            #region Get Company(s)

            if (connection.user_id == 1)
            {


                Cbo_Term.Enabled = false;
            }
            //if (connection.user_id != 1)
            //{
            //    string StrCond = " Where Comp_ID In (Select Comp_ID From User_Companies Where Comp_User_Id = " + connection.user_id + ")";

            //    SqlTxt += (StrCond);
            //}
            if (connection.user_id != 1)
            {
                string SqlTxt = " Select  ACUST_NAME, ECUST_NAME , T_ID  , b.loc_cur_id,c.CIT_ID,Cit_AName ,isnull(cust_online_id,0) as cust_online_id,isnull(A.cust_id,0) as Term_cust_id , comm_rem_flag,period_password  "
                    + " From CUSTOMERS A, TERMINALS B, Cit_Tbl c "
                    + " where A.CUST_ID = B.CUST_ID "
                    + " and a.CIT_ID = c.CIT_ID"
                    + " AND  B.T_ID in(select t_id from User_Terminals where User_State not in(2,3) and t_user_id= " + connection.user_id + ")";

                Cbo_Term.DataSource = connection.SqlExec(SqlTxt, "MCompUser_Tbl");
                Cbo_Term.ValueMember = "T_ID";
                Cbo_Term.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";

                if (connection.SQLDS.Tables["MCompUser_Tbl"].Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " المستخدم غير معرف او غير مفعل على الفروع " : " the user not define  or not active for terminals", MyGeneral_Lib.LblCap);
                    return;
                }




                if (connection.SQLDS.Tables["MCompUser_Tbl"].Rows.Count > 0)
                {
                    isCompChange = true;
                    Cbo_Term_SelectedIndexChanged(sender, e);

                }
                else

                { isCompChange = false; }
            }
            //if (connection.SQLDS.Tables["MCompUser_Tbl"].Rows.Count > 1)
            //{
            //    CboComp_Id.Enabled = true;
            //}
            //else
            //{
            //    CboComp_Id.Enabled = false;
            //}

            
            //}
           

            #endregion
            if (connection.SQLDS.Tables["User_Tbl"].Rows.Count > 0)
            {

                PEXP_DATE = connection.SQLDS.Tables["User_Tbl"].Rows[0]["PEXP_DATE"].ToString();
            }

            if (connection.SQLDS.Tables["MCompUser_Tbl"].Rows.Count > 0)
            {

                period_pass = Convert.ToInt16(connection.SQLDS.Tables["MCompUser_Tbl"].Rows[0]["period_password"]);
                if (period_pass > 0)
                {
                    DateTime date = Convert.ToDateTime(PEXP_DATE);
                    int diff_expdate = date.Day + date.Month * 100 + date.Year * 10000;
                    DateTime date_now = DateTime.Now;
                    int diff_datenow = date_now.Day + date_now.Month * 100 + date_now.Year * 10000;
                    int result_diff = diff_datenow - diff_expdate;
                    if (diff_datenow > diff_expdate)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " يرجى اعادة تعيين كلمة المرور " : " please update your password", MyGeneral_Lib.LblCap);

                        Update_User_PWD AddFrm = new Update_User_PWD();
                        this.Visible = false;
                        AddFrm.ShowDialog(this);
                        this.Visible = true;
                        ExtBtn_Click(null, null);
                        //  return;
                     

                    }
                    else 
                    { Get_Data(); }
                }
                 if (period_pass <= 0)
                {
                    Get_Data();
                }
            }
            
        }
        //-------------------
        private void Get_Data()
        {
            string sqlstring = "";
            if (connection.user_id == 1)
            {
                sqlstring = "select distinct(Parent) From dbo.Terminal_Permissions"
                    + " Where  Parent <> 0 ";
            }
            else
            {
                sqlstring = "select distinct(A.Parent) From dbo.Terminal_Permissions AS A,dbo.Terminal_User_Permission AS B " +
                                  " Where A.Node_Id=B.Node_Id " +
                                   " And A.Web_Flag = 0" +
                                  " And B.Per_user_id = " + connection.user_id;

            }
            DataTable Parent_Id_Table = connection.SqlExec(sqlstring, "Parent_Table");


            foreach (DataRow Prow in Parent_Id_Table.Rows)
            {
                sqlstring = " Select Node_Aname,Node_Ename From Terminal_Permissions " +
                            " where Node_Id = " + Prow[0].ToString();

                DataTable Parent_Name_Table = connection.SqlExec(sqlstring, "Parent_N_Table");
                TreeNode ParentNode = new TreeNode();
                ParentNode.Text = Parent_Name_Table.Rows[0][connection.Lang_id == 1 ? "Node_Aname" : "Node_Ename"].ToString();
                trV_Main.Nodes.Add(ParentNode);
                if (connection.user_id == 1)
                {
                    sqlstring = " select * from Terminal_Permissions where Parent = " + Prow[0].ToString();
                }
                else
                {
                    sqlstring = " select * from Terminal_Permissions where Parent = " + Prow[0].ToString() +
                               " And node_id in (select Node_Id from dbo.Terminal_User_Permission Where Per_User_Id = " + connection.user_id + ")";
                }
                DataTable Child_Node_Table = connection.SqlExec(sqlstring, "Child_Node_Table");
                foreach (DataRow Crow in Child_Node_Table.Rows)
                {
                    TreeNode NextChildNode = new TreeNode();
                    NextChildNode.Text = Crow[connection.Lang_id == 1 ? "Node_Aname" : "Node_Ename"].ToString();
                    NextChildNode.Name = Crow["Frm_Name"].ToString();
                    NextChildNode.Tag = Crow["Frm_Id"].ToString();
                    NextChildNode.ImageKey = Crow["Frm_Id_Main"].ToString();
                    NextChildNode.ToolTipText = Crow["Node_Id"].ToString();
                    ParentNode.Nodes.Add(NextChildNode);
                }
            }
        }
        //-------------------
        private void CboLang_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            connection.Lang_id = CboLang_id.SelectedIndex + 1;
            //--------------MessageBoxManager
            if (connection.Lang_id == 1)
            {
                MessageBoxButtoneCaption();
            }
            else
            {
                MessageBoxManager.Unregister();
            }
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }
        //-------------------
        private void CboComp_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (isCompChange)
            //{
            //    connection.T_ID = Convert.ToInt16(Cbo_Term.SelectedValue);
            //DataTable Dt_loc =    connection.SQLDS.Tables["MCompUser_Tbl"].DefaultView.ToTable(false, "T_id", "loc_cur_id").Select("T_ID = " +  connection.T_ID).CopyToDataTable();
            // connection.Loc_Cur_Id = Convert.ToInt16( Dt_loc.Rows[0]["loc_cur_id"]);
            //}
        }
        //-------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            isCompChange = false;
            //Application.Restart();
            //object[] Sparams = { connection.Comp_Id, connection.user_id, "" };
            //connection.SqlExec("User_LogOut", "LogOut_User_Tbl", Sparams);
            //if (connection.Col_Name != "")
            //{
            //    MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
            //    return;
            //}
            //connection.SQLDS.Clear();
            trV_Main.Nodes.Clear();
            TxtUser_Name.Text = "";
            TxtPwd.Text = "";
            //  CboLang_id.SelectedIndex = 0;

            BtnOk.Enabled = true;
            TxtUser_Name.Enabled = true;
            TxtPwd.Enabled = true;
            CboLang_id.Enabled = true;
            Cbo_Term.Enabled = true;
            ExtBtn.Enabled = false;
            TxtUser_Name.Focus();
            //connection.SQLDS = new DataSet();
        }
        //------------------------
        /// <summary>
        ///  
        /// </summary>
        /// <returns> Processor ID </returns>
        private string GetProcessorID()
        {
            string sProcessorID = "";
            string sQuery = "SELECT ProcessorId FROM Win32_Processor";
            ManagementObjectSearcher oManagementObjectSearcher = new ManagementObjectSearcher(sQuery);
            ManagementObjectCollection oCollection = oManagementObjectSearcher.Get();
            foreach (ManagementObject oManagementObject in oCollection)
            {
                sProcessorID = (string)oManagementObject["ProcessorId"];
            }

            return (sProcessorID);
        }
        //------------------
        private void Main_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) //-----13 = Enter Key
            {
                SendKeys.Send("{tab}");
            }
        }
        //------------------------
        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (isLogin)
            {
                //object[] Sparams = { connection.Comp_Id, connection.user_id, "" };
                //connection.SqlExec("User_LogOut", "LogOut_User_Tbl", Sparams);
                //if (connection.Col_Name != "")
                //{
                //    MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                //    return;
                //}
            }
            MessageBoxManager.Unregister();
        }

        private void Cbo_Term_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isCompChange)
            {
                //string SqlTxt1 = " Select   from TERMINALS where T_id =" + Convert.ToInt32(Cbo_Term.SelectedValue);
                //connection.SqlExec(SqlTxt1, "Trm_Tbl");
                

                connection.T_ID = Convert.ToInt16(Cbo_Term.SelectedValue);
                DataTable Dt_loc = connection.SQLDS.Tables["MCompUser_Tbl"].DefaultView.ToTable(false, "T_id", "loc_cur_id", "CIT_ID", "cust_online_id", "Term_cust_id", "comm_rem_flag").Select("T_ID = " + connection.T_ID).CopyToDataTable();
                connection.Loc_Cur_Id = Convert.ToInt16(Dt_loc.Rows[0]["loc_cur_id"]);
                connection.city_id_online = Convert.ToInt32(Dt_loc.Rows[0]["CIT_ID"]);
                connection.Cust_online_Id = Convert.ToInt16(Dt_loc.Rows[0]["cust_online_id"]);
                connection.Term_cust_ID = Convert.ToInt16(Dt_loc.Rows[0]["Term_cust_id"]);
                connection.comm_rem_flag = Convert.ToByte(Dt_loc.Rows[0]["comm_rem_flag"]);

            }
        }

        private string GetBaseBordID()
        {
            string BaseBordID = "";
            string sQuery = "Select SerialNumber From Win32_BaseBoard";
            ManagementObjectSearcher oManagementObjectSearcher = new ManagementObjectSearcher(sQuery);
            ManagementObjectCollection oCollection = oManagementObjectSearcher.Get();
            foreach (ManagementObject oManagementObject in oCollection)
            {
                BaseBordID = (string)oManagementObject["SerialNumber"];
            }

            return (BaseBordID);
        }


    }
}