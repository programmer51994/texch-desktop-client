﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

namespace Integration_Accounting_Sys
{
    public partial class add_upd_Cit : Form
    {
        int Frm_Id = 0;
        public add_upd_Cit(int Id, string AName, string EName, string Code, int Form_Id,int Con_Id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Frm_Id = Form_Id;
            #region Caption
            if (connection.Lang_id == 1)
            {
                if (connection.Frm_Id == 1) this.Text = "الـمـدن";
                else if (connection.Frm_Id == 31) this.Text = "المـنـاطـــق";
                if (connection.Frm_Id_Main == 4) label4.Text = "اســـــــــم البــــــلـــــــد:";
                else if (connection.Frm_Id_Main == 1) label4.Text = "اســـــــــم المدينـــــــة:";
            }
            else
            {
                if (connection.Frm_Id == 1) this.Text = "Cities";
                else if (connection.Frm_Id == 31) this.Text = "Zones";
                if (connection.Frm_Id_Main == 4) label4.Text = "Country Name : ";
                else if (connection.Frm_Id_Main == 1) label4.Text = "City Name : ";
            }
            #endregion
            
            Get_Data();
            if (Form_Id == 2)
            {
                Txt_AName.Text = AName;
                Txt_EName.Text = EName;
                Txt_Code.Text = Code;
                Cbo_Id.SelectedValue = Con_Id;
                Txt_AName.Tag = Form_Id;
                Txt_Code.Tag = Id;
            }
            Internet_connect.Visible = false;
        }
        private void Get_Data()
        {
            Cbo_Id.DataSource = connection.SqlExec("Get_Data_Add_Upd", "Country_Tbl",new object [] {connection.Frm_Id_Main});//, parameters
            Cbo_Id.DisplayMember = connection.Lang_id == 1 ? "aname" : "Ename";
            Cbo_Id.ValueMember = "id";
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            #region Validation
            //string lblmessage = connection.Lang_id == 1 ? "رسالة تحذير" : "Warning Message";
            if (Convert.ToInt16(Cbo_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد البلد او المدينة اولا" : "Please select country or city", MyGeneral_Lib.LblCap);
                Txt_AName.Focus();
                return;
            }
            if (Txt_AName.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ?"ادخل الاسم العربي":"Please Insert Arabic Name", MyGeneral_Lib.LblCap);
                Txt_AName.Focus();
                return;
            }
            if (Txt_EName.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاسم الاجنبي" : "Please Insert English Name", MyGeneral_Lib.LblCap);
                Txt_EName.Focus();
                return;
            }
            if (Txt_Code.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاختصار" : "Please Insert Code", MyGeneral_Lib.LblCap);
                Txt_Code.Focus();
                return;
            }
            #endregion

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Txt_AName.Text.Trim());
            ItemList.Insert(1, Txt_EName.Text.Trim().ToUpper());
            ItemList.Insert(2, Txt_Code.Text.Trim().ToUpper());
            ItemList.Insert(3, connection.user_id);
            ItemList.Insert(4, Frm_Id);   // Form_Id
            ItemList.Insert(5, Convert.ToInt32(Txt_Code.Tag));    // Id 
            ItemList.Insert(6, Convert.ToInt32(Cbo_Id.SelectedValue));
            ItemList.Insert(7, connection.Frm_Id);    // Id 
            ItemList.Insert(8, connection.Frm_Id_Main);
            //ItemList.Insert(9, connection.Frm_Id);
            ItemList.Insert(9, "");
            connection.scalar("Add_Upd_City", ItemList);
            if (connection.SQLCMD.Parameters["@param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Frm_Id == 1)
            {
                Txt_AName.Text = "";
                Txt_EName.Text = "";
                Txt_Code.Text = "";
                Txt_AName.Focus();
            }
            else
            this.Close();

        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Add_Upd_Cit_Load(object sender, EventArgs e)
        {

        }

        private void Frm_Add_Upd_Cit_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection.SQLDS.Tables.Contains("Country_Tbl"))
                connection.SQLDS.Tables.Remove("Country_Tbl");
        }

        private void add_upd_Cit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }

        private void add_upd_Cit_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Ok_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            } 
        }

        private void Txt_AName_Leave(object sender, EventArgs e)
        {
            Txt_EName.Text = Txt_AName.Text != "" ? MyGeneral_Lib.TranslateText(Txt_AName.Text, "ar", "en") : "";
            if (Txt_EName.Text != "-1")
                Internet_connect.Visible = false;
            else
            {
                Internet_connect.Visible = true;
                Txt_EName.Text = "";
            }
        }
    }
}
