﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Add_Favorite : Form
    {
        #region MyRegion
        string SqlTxt = "";
        bool CboChange = false;
        bool GrdChange = false;

        DataTable DT = new DataTable();
        BindingSource BS_Grd = new BindingSource();
        BindingSource _Bs_Grd_fav = new BindingSource();
        DataTable TBL = new DataTable();
        #endregion

        public Add_Favorite()
        {
            InitializeComponent();
            connection.SQLBSGrd.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Fav.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                //    Column1.DataPropertyName = "Chk";
                //    Column2.DataPropertyName = "Acc_Ename";
                //    Column3.DataPropertyName = "Acc_Ename";
                //    Column4.DataPropertyName = "ECust_name";

                Cbo_Fav.Items[0] = "Nationality";
                Cbo_Fav.Items[1] = "Currency";
                Cbo_Fav.Items[2] = "Countries and cities";
            }
        }
        //------------------------
        private void Acc_Cust_Operations_Load(object sender, EventArgs e)
        {
            Cbo_Fav.SelectedIndex =0;
        }
        //----------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {


            CboChange = true;
            string Tbl_name = "";
            if (Cbo_Fav.SelectedIndex == 0)
            {
                Tbl_name = "Nat";
                Grd_Fav.Columns["Column2"].DataPropertyName = "Aname_1";
                Grd_Fav.Columns["Column3"].DataPropertyName = "Ename_1";
                Grd_Fav.Columns["Column4"].DataPropertyName = "symbol_1";
            }
            if (Cbo_Fav.SelectedIndex == 1)
            {
                Tbl_name = "cur";
                Grd_Fav.Columns["Column2"].DataPropertyName = "Aname_1";
                Grd_Fav.Columns["Column3"].DataPropertyName = "Ename_1";
                Grd_Fav.Columns["Column4"].DataPropertyName = "symbol_1";
            }
            if (Cbo_Fav.SelectedIndex == 2)
            {
                Tbl_name = "cit";
                Grd_Fav.Columns["Column2"].DataPropertyName = "Cit_Con_aname";
                Grd_Fav.Columns["Column3"].DataPropertyName = "Cit_Con_ename";
                Grd_Fav.Columns["Column4"].DataPropertyName = "Cit_Con_symbol";
           
            }

            try
            {

                SqlTxt = " Exec Main_favorit_tbl1 " + Tbl_name + ",'" + Txt_SearchName.Text + "'";
                _Bs_Grd_fav.DataSource = connection.SqlExec(SqlTxt, "Tbl_Favorite_2");
                Grd_Fav.DataSource = _Bs_Grd_fav;
            }

            catch
            {
            }
        }
        //----------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {

            try
            {

                DT = connection.SQLDS.Tables["Tbl_Favorite_2"].DefaultView.ToTable(false, "Chk", "ID_1", "ID_2").Select().CopyToDataTable();
            }
            catch {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجا اختيار القائمة المفضلة لديك " : "check your favorite list", MyGeneral_Lib.LblCap);
                    return;
            
            }


    connection.SQLCMD.Parameters.AddWithValue("@Cbo_Fav", Cbo_Fav.SelectedIndex);
    connection.SQLCMD.Parameters.AddWithValue("@Tbl_Favorite_2", DT);
    connection.SQLCMD.Parameters.AddWithValue("@User_Id",  connection.user_id);   
    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
    connection.SqlExec("ADD_Favorite", connection.SQLCMD);

    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" )
    {
        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
        connection.SQLCMD.Parameters.Clear();
        return;
    }
    connection.SQLCMD.Parameters.Clear();


    connection.SQLCMD.Parameters.Clear();
    MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);

    Grd_Fav.DataSource = BS_Grd;

        }
        //----------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
             "Chk", "ID_1", "ID_2"     
            };

            string[] DType =
            {
               "System.byte", "System.Int32","System.Int32"
            };


            TBL = CustomControls.Custom_DataTable("TBL", Column, DType);
        }
        //----------------------------------------------------------------
        private void Cbo_Fav_SelectedIndexChanged(object sender, EventArgs e)
        {

            button1_Click(null, null);
        }
        //----------------------------------------------------------------
        private void Grd_Fav_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Grd_fav);
        }

        private void Add_Favorite_FormClosed(object sender, FormClosedEventArgs e)
        {
         CboChange = false;
           GrdChange = false;
        }
    }
}