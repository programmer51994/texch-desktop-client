﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class backup_db : Form
    {
        public backup_db()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            connection.SqlExec("Exec Backup_database_all");

            MessageBox.Show(connection.Lang_id == 1 ? " تم أخذ نسخة من قاعدة البيانات في التوقيت  " + DateTime.Now : " A copy of the database has been taked at time " + DateTime.Now, MyGeneral_Lib.LblCap);

        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backup_db_Load(object sender, EventArgs e)
        {

        }

    }
}
