﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

namespace Integration_Accounting_Sys
{
    public partial class Frm_Master : Form
    {
        public static BindingSource _BS_Master = new BindingSource();
        public Frm_Master()
        {
            InitializeComponent();
            
            //MyGeneral_Lib.Form_Orientation(this);
            //connection.Control_Cap(this, Convert.ToInt16(connection.Frm_Id));
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            #region Caption
            if (connection.Lang_id == 1)
            {
                if (connection.Frm_Id == 4) this.Text = "الــبــلــدان";
                else if (connection.Frm_Id == 31) this.Text = "الـمـنــاطـــق";
                else if (connection.Frm_Id == 7) this.Text = "الـــوثــائــق";
                else if (connection.Frm_Id == 8) this.Text = "نــوع الــزبــون";
                else if (connection.Frm_Id == 5) this.Text = "الــجــنــســيــات";
                else if (connection.Frm_Id == 32) this.Text = "الــعــمــلات";
                else if (connection.Frm_Id == 100) this.Text = "العناويــن الوظـفـية";
                else if (connection.Frm_Id == 101) this.Text = "المنــاصــب الاداريـة";
                else if (connection.Frm_Id == 3) this.Text = "التخصص الاكاديمي الدقيق";
                else if (connection.Frm_Id == 102) this.Text = "المواقـع الادارية";
                else if (connection.Frm_Id == 103) this.Text = "معايير التقييم الاساسية";
                else if (connection.Frm_Id == 104) this.Text = "مسؤولـي التقييـم";
                else if (connection.Frm_Id == 176) this.Text = "الاقـــارب";
                else if (connection.Frm_Id == 207) this.Text = "المجموعــــات";
                else if (connection.Frm_Id == 207) this.Text = "المجموعــــات";
                else if (connection.Frm_Id == 500) this.Text = " الوثائق المعنوية";
            
            }
            else
            {
                if (connection.Frm_Id == 4) this.Text = "COUNTRIES";
                else if (connection.Frm_Id == 31) this.Text = "ZONES";
                else if (connection.Frm_Id == 7) this.Text = "FORMAL DOCUMENT";
                else if (connection.Frm_Id == 8) this.Text = "CUSTOMER TYPE";
                else if (connection.Frm_Id == 5) this.Text = "NATIONALITY";
                else if (connection.Frm_Id == 32) this.Text = "CURENCIES";
                else if (connection.Frm_Id == 100) this.Text = "Jobs Address";
                else if (connection.Frm_Id == 101) this.Text = "Positions";
                else if (connection.Frm_Id == 3) this.Text = "Acadimic Specialization";
                else if (connection.Frm_Id == 102) this.Text = "Admin Location";
                else if (connection.Frm_Id == 103) this.Text = "Standard Evaluation";
                else if (connection.Frm_Id == 104) this.Text = "Evaluation Responsiple";
                else if (connection.Frm_Id == 176) this.Text = "Relatives";
                else if (connection.Frm_Id == 207) this.Text = "Groups";
                else if (connection.Frm_Id == 500) this.Text = "FORMAL DOCUMENT Special";
               
               
            }
            #endregion
        }
        //------------------------------------------
        private void Frm_Master_Load_1(object sender, EventArgs e)
        {
            this.Tag = connection.Frm_Id;
            Txt_AName.Text = "";
            Txt_EName.Text = "";
            GetData();
        }
        //------------------------------------------
        private void GetData()
        {
            string[] parameters = { Txt_AName.Text.Trim(), Txt_EName.Text.Trim(), this.Tag.ToString() };
            _BS_Master.DataSource = connection.SqlExec("Main_Master", "Master_Tbl", parameters);
            Grd_Tbl.DataSource = _BS_Master;

            if (_BS_Master.List.Count <= 0)
                UpdBtn.Enabled = false;
            else
                UpdBtn.Enabled = true;
        }
        //------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Frm_Add_Upd_Master AddFrm = new Frm_Add_Upd_Master(1, "", "", "", 1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            GetData();
            this.Visible = true;


        }
        //------------------------------------------
        private void Btn_upd_Click(object sender, EventArgs e)
        {

            int id = Convert.ToInt32(Grd_Tbl.CurrentRow.Cells["Column1"].Value.ToString());
            string AName = Grd_Tbl.CurrentRow.Cells["Column2"].Value.ToString();
            string EName = Grd_Tbl.CurrentRow.Cells["Column3"].Value.ToString();
            string Code = Grd_Tbl.CurrentRow.Cells["Column4"].Value.ToString();


            Frm_Add_Upd_Master frm = new Frm_Add_Upd_Master(id, AName, EName, Code, 2);
            this.Visible = false;
            frm.ShowDialog(this);
            this.Visible = true;
            GetData();
        }
        //------------------------------------------
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GetData();
        }
        //------------------------------------------
        private void Btn_ShowAll_Click(object sender, EventArgs e)
        {
            Txt_AName.Text = "";
            Txt_EName.Text = "";
            GetData();
        }
        //------------------------------------------
        private void Grd_Tbl_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records();
        }
        //------------------------------------------
        private void Frm_Master_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            if (connection.SQLDS.Tables.Contains("Master_Tbl"))
                connection.SQLDS.Tables["Master_Tbl"].Clear();
        }

        private void Frm_Master_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }

        private void Frm_Master_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Add_Click(sender, e);
                    break;
                case Keys.F7:
                    Btn_upd_Click(sender, e);
                    break;
                case Keys.F5:
                    Btn_Search_Click(sender, e);
                    break;
                case Keys.F4:
                    Btn_ShowAll_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
    }
}
