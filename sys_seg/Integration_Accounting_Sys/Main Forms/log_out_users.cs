﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class log_out_users : Form
    {
        #region MyRegion
        string @format = "dd/MM/yyyy HH:mm";
        string @format_null = " ";
        DataTable user_tbl = new DataTable();
        string from_Date = "";
        bool change_1 = false;
        bool Change = false;
        BindingSource _bs_cbo_term = new BindingSource();
        #endregion
        public log_out_users()
        {
            InitializeComponent();
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }
        //-----------------------
        private void log_out_users_Load(object sender, EventArgs e)
        {
            Cmb_permission.SelectedIndex = 0;


        }
        //-----------------------------------------------------
        private void Txt_From_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_From.Checked == true)
            {
                Txt_From.Format = DateTimePickerFormat.Custom;
                Txt_From.CustomFormat = @format;
            }
            else
            {
                Txt_From.Format = DateTimePickerFormat.Custom;
                Txt_From.CustomFormat = " ";
            }

            from_Date = Txt_From.Value.ToString("yyyy/MM/dd HH:mm");
        }
        //-----------------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {

           
            if (Convert.ToInt16(Cmb_permission.SelectedIndex) == 0)
            {
                if (cmb_users_names.SelectedIndex == 0 && Main.current_user_id == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " إخترالموظف رجاءاً " : "choose the user please ", MyGeneral_Lib.LblCap);
                    cmb_users_names.Focus();
                    return;
                }

                if (Main.current_user_id != 0)
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Comp_Id", connection.Comp_Id);
                    connection.SQLCMD.Parameters.AddWithValue("@User_ID", Main.current_user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@log_out_time", from_Date);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SqlExec("log_out_users", connection.SQLCMD);
                }
                else
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Comp_Id", connection.Comp_Id);
                    connection.SQLCMD.Parameters.AddWithValue("@User_ID", cmb_users_names.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@log_out_time", from_Date);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SqlExec("log_out_users", connection.SQLCMD);
                }
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;

                }
                connection.SQLCMD.Parameters.Clear();

            }
            else
            {
                connection.SQLCMD.Parameters.AddWithValue("@login_id",Convert.ToInt16( cmb_users_names.SelectedValue));
                connection.SQLCMD.Parameters.AddWithValue("@Cust_id", Convert.ToInt16(Cbo_Terminals.SelectedValue));
                connection.SqlExec("reset_login web", connection.SQLCMD);
                  connection.SQLCMD.Parameters.Clear();
            
            }
            MessageBox.Show(connection.Lang_id == 1 ? " تم إخراج الموظف من النظام " : "The user has been loggined out of the system now ", MyGeneral_Lib.LblCap);
            connection.SQLDS.Tables.Clear();
            Main.current_user_id = 0;
            button1_Click(null, null);
        }
        //-----------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmb_permission_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_permission.SelectedIndex == 0)
            {
                Change = false;
               string SqlTxt = " Select  ACUST_NAME , ECUST_NAME ,T_ID , A.cust_id "
                    + " from TERMINALS  A , CUSTOMERS  B "
                    + " where  a.CUST_ID = b.CUST_ID  "
                    + " And TERM_STATE_ID = 1 ";

                _bs_cbo_term.DataSource = connection.SqlExec(SqlTxt, "Term_TBL");
                Cbo_Terminals.DataSource = _bs_cbo_term;
                Cbo_Terminals.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
                Cbo_Terminals.ValueMember = "cust_id";
                //if (connection.Lang_id == 1)
                //{
                //    label4.Text = "فروع الشركة";
                //}
                //else
                //{
                //    label4.Text = "Company branch";
                //}
                Change = true;
                Cbo_Terminals_SelectedIndexChanged(null, null);
            }
            else
            {

                Change = false;
                string SqlTxt1 = " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name , Sub_Cust_ID as cust_id "
                  + "  From  Sub_CUSTOMERS_online A   "
                  + "  Where A.Cust_state  =  1  "
                  //+ "  and A.t_id =  " + connection.T_ID
                  + "  and Sub_Cust_ID in (select cust_id from Login_Cust_Online) "
                  + "  order by cust_id  ";

                _bs_cbo_term.DataSource = connection.SqlExec(SqlTxt1, "Term_TBL");
                Cbo_Terminals.DataSource = _bs_cbo_term;
                Cbo_Terminals.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                Cbo_Terminals.ValueMember = "cust_id";
               // label4.Text = "الوكالات الثانوية:";
                if (connection.SQLDS.Tables["Term_TBL"].Rows.Count > 0)
                {
                    Change = true;
                    Cbo_Terminals_SelectedIndexChanged(null, null);
                }
                else
                {
                    //LstUser_Id.DataSource = new BindingSource();
                    //Grd_TermPermission.DataSource = new BindingSource();
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد وكالات معرفة" : "there is no defined customers", MyGeneral_Lib.LblCap);

                    return;
                }
            }
        }

        private void Cbo_Terminals_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                if (Cmb_permission.SelectedIndex == 0)
                {

                Txt_From.CustomFormat = " ";
                connection.SqlExec("Exec get_user_name", "tbl");
                user_tbl = connection.SQLDS.Tables["tbl"];

                if (Main.current_user_id != 0)
                {
                    DataTable Dt = new DataTable();
                    DataTable new_user_tbl = new DataTable();
                    Dt = connection.SQLDS.Tables["user_tbl"];
                    new_user_tbl = Dt.DefaultView.ToTable().Select("user_id = " + Main.current_user_id).CopyToDataTable();

                    cmb_users_names.DataSource = new_user_tbl;
                    cmb_users_names.ValueMember = "User_Id";
                    cmb_users_names.DisplayMember = connection.Lang_id == 1 ? "User_Name" : "User_Name";
                    cmb_users_names.Enabled = false;

                }
                else
                {
                    cmb_users_names.DataSource = user_tbl;
                    cmb_users_names.ValueMember = "current_user_id";
                    cmb_users_names.DisplayMember = connection.Lang_id == 1 ? "Auser_name" : "Euser_name";
                }



                Txt_From_ValueChanged(null, null);
                
                }
                else
                {
                string SqlTxt = " SELECT distinct login_id, C.T_id  , login_Aname , C.cust_id , A.USER_ID "
                                    + " FROM   Login_Cust_Online A , USERS B , CUSTOMERS_ACCOUNTS C "
                                    + " where  A.CUST_ID = " + Cbo_Terminals.SelectedValue
                                    + "  And C.cust_id = A.cust_id "
                                    + "  And  B.User_State = 1 "
                                    + "  And A.user_id = B.USER_ID ";
                             //+ "  And B.user_name like '%" + TxtUser_Id.Text + "%' ";


         cmb_users_names.DataSource = connection.SqlExec(SqlTxt, "User_Term");
         cmb_users_names.DisplayMember = "login_Aname";
         cmb_users_names.ValueMember = "login_id";
                }
            }
        }

        private void log_out_users_FormClosed(object sender, FormClosedEventArgs e)
        {
            change_1 = false;
            Change = false;
        }

      
    }
}