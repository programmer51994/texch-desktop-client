﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Web;

namespace Integration_Accounting_Sys
{
    public partial class Frm_Add_Upd_Master : Form
    {

        public Frm_Add_Upd_Master(int Id, string AName, string EName, string Code, int Form_Id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Txt_AName.Text = AName;
            Txt_EName.Text = EName;
            Txt_Code.Text = Code;
            Txt_AName.Tag = Form_Id;
            Txt_Code.Tag = Id;
            Internet_connect.Visible = false;
        }
        //---------------------------------
        private void add_upd_con_Load(object sender, EventArgs e)
        {
            Control_Txt();
        }
        //---------------------------------
        private void Control_Txt()
        {
            switch (connection.Frm_Id)
            {
                case 4:
                    this.Text = connection.Lang_id == 1 ? "اضـافـة وتــعديــل بـلـدان" : "ADD UPDATE COUNTRIES";
                    break;
                case 5:
                    this.Text = connection.Lang_id == 1 ? "اضـافــة وتــعديــل جنسيات" : "ADD UPDATE NATIONALITY";
                    break;
                case 7:
                    this.Text = connection.Lang_id == 1 ? "اضـافـة وتــعديــل وثـائــق" : "ADD UPDATE FORMAL DOCUMENT";
                    break;
                case 8:
                    this.Text = connection.Lang_id == 1 ? "اضـافــة وتــعديــل نوع الزبون" : "ADD UPDATE CUSTOMER TYPE";
                    break;
                case 31:
                    this.Text = connection.Lang_id == 1 ? "اضـافـة وتــعديــل مــناطـق" : "ADD UPDATE ZONES";
                    break;
                case 32:
                    this.Text = connection.Lang_id == 1 ? "اضـافــة وتــعديــل عملات" : "ADD UPDATE CURENCIES";
                    break;
                case 100:
                    this.Text = connection.Lang_id == 1 ? "اضـافــة وتــعديــل العناويــن الوظـفـية" : "ADD UPDATE Jobs Address";
                    break;
                case 101:
                    this.Text = connection.Lang_id == 1 ? "اضـافــة وتــعديــل المنــاصــب الاداريـة" : "ADD UPDATE Positions";
                    break;
                case 3:
                    this.Text = connection.Lang_id == 1 ? "التخصص الاكاديمي الدقيق" : "Add Update Acadimic Specialization";
                    break;
                case 102:
                    this.Text = connection.Lang_id == 1 ? "المواقـع الادارية" : "ADD UPDATE Admin Location";
                    break;
                case 103:
                    this.Text = connection.Lang_id == 1 ? "معايير التقييم الاساسية" : "ADD UPDATE Standard Evaluation";
                    break;
                case 104:
                    this.Text = connection.Lang_id == 1 ? "مسؤولـي التقييـم" : "ADD UPDATE Evaluation Responsiple";
                    break;
                case 176:
                    this.Text = connection.Lang_id == 1 ? "أضافــة الاقارب" : "ADD UPDATE Relatives";
                    break;
                case 207:
                    this.Text = connection.Lang_id == 1 ? "أضافــة مجموعة" : "ADD UPDATE Groups";
                    break;
                case 500:
                    this.Text = connection.Lang_id == 1 ? " اضـافـة وتــعديــل الوثـائــق المعنوية" : "ADD UPDATE FORMAL DOCUMENT Special";
                    break;
              
            }
        }
        //---------------------------------
        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Txt_AName.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please Insert Arabic Name", MyGeneral_Lib.LblCap);
                Txt_AName.Focus();
                return;
            }
            if (Txt_EName.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم الاجنبي" : "Please Insert English Name", MyGeneral_Lib.LblCap);
                Txt_EName.Focus();
                return;
            }
            if (Txt_Code.Text == "")
            {
                if ((connection.Frm_Id != 176 ) &&(connection.Frm_Id != 207 ))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاختصار" : "Please Insert Code", MyGeneral_Lib.LblCap);
                    Txt_Code.Focus();
                    return;
            #endregion
                }
                else
                { 
                    Txt_Code.Text = " ";
                }
            }

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Txt_AName.Text.Trim());
            ItemList.Insert(1, Txt_EName.Text.Trim().ToUpper());
            ItemList.Insert(2, Txt_Code.Text.Trim().ToUpper());
            ItemList.Insert(3, connection.user_id);
            ItemList.Insert(4, Convert.ToInt16(Txt_AName.Tag));   // Form_Id
            ItemList.Insert(5, Convert.ToInt32(Txt_Code.Tag));    // Id 
            ItemList.Insert(6, connection.Frm_Id);
            ItemList.Insert(7, "");
            connection.scalar("add_upd_Master", ItemList);
            if (connection.SQLCMD.Parameters["@param_Result"].Value.ToString() != "")
            {
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            if (Convert.ToByte(Txt_AName.Tag) == 1)
            {
                Txt_AName.Text = "";
                Txt_EName.Text = "";
                Txt_Code.Text = "";
                Txt_AName.Focus();
                return;
            }

            this.Close();
        }
        //---------------------------------
        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------
        private void Frm_Add_Upd_Master_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{tab}");
            }
        }
        //---------------------------------
        private void Frm_Add_Upd_Master_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Ok_Click(sender, e);
                    break;
                    case Keys.Escape:
                    this.Close();
                    break;

            } 
        }
        //---------------------------------
        private void Txt_AName_Leave(object sender, EventArgs e)
        {
            Txt_EName.Text = Txt_AName.Text != "" ? MyGeneral_Lib.TranslateText(Txt_AName.Text, "ar", "en") : "";
            if (Txt_EName.Text != "-1")
                Internet_connect.Visible = false;
            else
            {
                Internet_connect.Visible = true;
                Txt_EName.Text = "";
            }
        }

       
    }
}