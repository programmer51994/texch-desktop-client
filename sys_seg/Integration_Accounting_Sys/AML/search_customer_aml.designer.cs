﻿namespace Integration_Accounting_Sys
{
    partial class search_customer_aml
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_RemIn = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_RemOut = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.Btn_Export_Details = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_doc_no = new System.Windows.Forms.TextBox();
            this.Txt_rem_no = new System.Windows.Forms.TextBox();
            this.Txt_phone = new System.Windows.Forms.TextBox();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Cbo_doc_type = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Cbo_sing = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Cbo_Term = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Nrec_date_From = new System.Windows.Forms.DateTimePicker();
            this.Txt_Nrec_date_to = new System.Windows.Forms.DateTimePicker();
            this.Txt_RCount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Cbo_RCur = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Cbo_Rcur2 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Btn_Excel = new System.Windows.Forms.Button();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Person_Info = new System.Windows.Forms.DataGridView();
            this.Txt_SSum_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_RSum_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_RemIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_RemOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Person_Info)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(5, 30);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(988, 1);
            this.flowLayoutPanel9.TabIndex = 533;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(83, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(252, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(817, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(170, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(754, 8);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 530;
            this.label1.Text = "التاريـــخ:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 638);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(999, 22);
            this.statusStrip1.TabIndex = 537;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(56, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 600);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(989, 1);
            this.flowLayoutPanel1.TabIndex = 542;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1, 582);
            this.flowLayoutPanel3.TabIndex = 544;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(993, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 582);
            this.flowLayoutPanel4.TabIndex = 545;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(5, 269);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(989, 1);
            this.flowLayoutPanel5.TabIndex = 546;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(5, 436);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(989, 1);
            this.flowLayoutPanel2.TabIndex = 548;
            // 
            // Grd_RemIn
            // 
            this.Grd_RemIn.AllowUserToAddRows = false;
            this.Grd_RemIn.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_RemIn.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_RemIn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_RemIn.ColumnHeadersHeight = 30;
            this.Grd_RemIn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column26,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column37,
            this.Column38});
            this.Grd_RemIn.Location = new System.Drawing.Point(12, 280);
            this.Grd_RemIn.Name = "Grd_RemIn";
            this.Grd_RemIn.ReadOnly = true;
            this.Grd_RemIn.RowHeadersWidth = 15;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_RemIn.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_RemIn.Size = new System.Drawing.Size(975, 119);
            this.Grd_RemIn.TabIndex = 547;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "rem_no";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحوالة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "r_name";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم المستلم";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "s_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "اسم المرسل";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 200;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "R_amount";
            this.Column5.HeaderText = "مبلغ الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "R_ACUR_NAME";
            this.Column6.HeaderText = "عملة الحوالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "rFRM_ADOC_NA";
            this.Column7.HeaderText = "نوع وثيقة المستلم";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "r_doc_no";
            this.Column8.HeaderText = "رقم وثيقة المستلم";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "r_doc_issue";
            this.Column9.HeaderText = "جهة اصدار وثيقة المستلم";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "r_doc_ida";
            this.Column10.HeaderText = "تاريخ اصدار هوية المستلم";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "r_doc_eda";
            this.Column11.HeaderText = "تاريخ الانتهاء";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "r_A_NAT_NAME";
            this.Column26.HeaderText = "جنسية المستلم";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "r_address";
            this.Column29.HeaderText = "عنوان المستلم";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "R_phone";
            this.Column30.HeaderText = "هاتف المستلم";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "T_purpose";
            this.Column31.HeaderText = "الغرض من الحوالة";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "S_phone";
            this.Column32.HeaderText = "هاتف المرسل";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "ACASE_NA";
            this.Column37.HeaderText = "حالة الحوالة";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "nrec_date";
            this.Column38.HeaderText = "وقت وتاريخ الانشاء";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            // 
            // Grd_RemOut
            // 
            this.Grd_RemOut.AllowUserToAddRows = false;
            this.Grd_RemOut.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_RemOut.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_RemOut.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_RemOut.ColumnHeadersHeight = 30;
            this.Grd_RemOut.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn7,
            this.Column20,
            this.Column21,
            this.Column25,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column22,
            this.Column23});
            this.Grd_RemOut.Location = new System.Drawing.Point(12, 447);
            this.Grd_RemOut.Name = "Grd_RemOut";
            this.Grd_RemOut.ReadOnly = true;
            this.Grd_RemOut.RowHeadersWidth = 15;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_RemOut.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_RemOut.Size = new System.Drawing.Size(975, 119);
            this.Grd_RemOut.TabIndex = 549;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "rem_no";
            this.dataGridViewTextBoxColumn10.HeaderText = "رقم الحوالة";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "S_name";
            this.dataGridViewTextBoxColumn11.HeaderText = "اسم المرسل";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "r_name";
            this.dataGridViewTextBoxColumn14.HeaderText = "اسم المستلم";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "r_amount";
            this.dataGridViewTextBoxColumn15.HeaderText = "مبلغ الحوالة";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "R_ACUR_NAME";
            this.dataGridViewTextBoxColumn9.HeaderText = "عملة الحوالة";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "sFRM_ADOC_NA";
            this.dataGridViewTextBoxColumn17.HeaderText = "نوع وثيقة المرسل";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "S_doc_no";
            this.dataGridViewTextBoxColumn8.HeaderText = "رقم وثيقة المرسل";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "S_doc_issue";
            this.dataGridViewTextBoxColumn7.HeaderText = "جهة اصداروثيقة المرسل";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "S_doc_ida";
            this.Column20.HeaderText = "تاريخ اصدار وثيقة المرسل";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "S_doc_eda";
            this.Column21.HeaderText = "تاريخ الانتهاء";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "s_A_NAT_NAME";
            this.Column25.HeaderText = "جنسية المرسل";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "S_address";
            this.Column14.HeaderText = "عنوان المرسل";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "S_phone";
            this.Column15.HeaderText = "هاتف المرسل";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "T_purpose";
            this.Column16.HeaderText = "الغرض من الحوالة";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "R_phone";
            this.Column17.HeaderText = "هاتف المستلم";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "ACASE_NA";
            this.Column22.HeaderText = "حالة الحوالة";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Width = 150;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "nrec_date";
            this.Column23.HeaderText = "وقت وتاريخ الانشاء";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(562, 605);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 26);
            this.button1.TabIndex = 551;
            this.button1.Text = "إنهــــــــــــــاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Btn_Export_Details
            // 
            this.Btn_Export_Details.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Export_Details.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Export_Details.Location = new System.Drawing.Point(464, 605);
            this.Btn_Export_Details.Name = "Btn_Export_Details";
            this.Btn_Export_Details.Size = new System.Drawing.Size(99, 26);
            this.Btn_Export_Details.TabIndex = 550;
            this.Btn_Export_Details.Text = "تصدير تفصيلي";
            this.Btn_Export_Details.UseVisualStyleBackColor = true;
            this.Btn_Export_Details.Click += new System.EventHandler(this.button2_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(17, 426);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(170, 14);
            this.label12.TabIndex = 765;
            this.label12.Text = "معلومات الحوالات الصادرة ...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(9, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 14);
            this.label2.TabIndex = 767;
            this.label2.Text = "الاســـــــــم:";
            // 
            // txt_name
            // 
            this.txt_name.BackColor = System.Drawing.Color.White;
            this.txt_name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.Location = new System.Drawing.Point(83, 37);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(122, 22);
            this.txt_name.TabIndex = 768;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(524, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 769;
            this.label3.Text = "رقم الحوالة:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(336, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 770;
            this.label4.Text = "الهــاتـف:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 14);
            this.label5.TabIndex = 771;
            this.label5.Text = "نوع الوثيقـة:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(316, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 14);
            this.label6.TabIndex = 772;
            this.label6.Text = "رقم الوثيقة:";
            // 
            // Txt_doc_no
            // 
            this.Txt_doc_no.BackColor = System.Drawing.Color.White;
            this.Txt_doc_no.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_doc_no.Location = new System.Drawing.Point(394, 63);
            this.Txt_doc_no.Name = "Txt_doc_no";
            this.Txt_doc_no.Size = new System.Drawing.Size(128, 22);
            this.Txt_doc_no.TabIndex = 776;
            // 
            // Txt_rem_no
            // 
            this.Txt_rem_no.BackColor = System.Drawing.Color.White;
            this.Txt_rem_no.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_rem_no.Location = new System.Drawing.Point(598, 63);
            this.Txt_rem_no.Name = "Txt_rem_no";
            this.Txt_rem_no.Size = new System.Drawing.Size(146, 22);
            this.Txt_rem_no.TabIndex = 779;
            // 
            // Txt_phone
            // 
            this.Txt_phone.BackColor = System.Drawing.Color.White;
            this.Txt_phone.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_phone.Location = new System.Drawing.Point(394, 37);
            this.Txt_phone.Name = "Txt_phone";
            this.Txt_phone.Size = new System.Drawing.Size(128, 22);
            this.Txt_phone.TabIndex = 778;
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.BtnAdd.Location = new System.Drawing.Point(933, 61);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(58, 26);
            this.BtnAdd.TabIndex = 780;
            this.BtnAdd.Text = "بحث";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.button3_Click);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 105);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(989, 1);
            this.flowLayoutPanel6.TabIndex = 781;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(17, 258);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 14);
            this.label7.TabIndex = 782;
            this.label7.Text = "معلومات الحوالات الواردة ...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(17, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 14);
            this.label8.TabIndex = 783;
            this.label8.Text = "معلومات الزبون ...";
            // 
            // Cbo_doc_type
            // 
            this.Cbo_doc_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_doc_type.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_doc_type.FormattingEnabled = true;
            this.Cbo_doc_type.Location = new System.Drawing.Point(84, 63);
            this.Cbo_doc_type.Name = "Cbo_doc_type";
            this.Cbo_doc_type.Size = new System.Drawing.Size(230, 22);
            this.Cbo_doc_type.TabIndex = 786;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(522, 41);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(102, 14);
            this.label26.TabIndex = 1089;
            this.label26.Text = "تاريخ الانشاء من:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(751, 41);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(38, 14);
            this.label10.TabIndex = 1091;
            this.label10.Text = "الــى:";
            // 
            // Cbo_sing
            // 
            this.Cbo_sing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing.FormattingEnabled = true;
            this.Cbo_sing.Location = new System.Drawing.Point(206, 37);
            this.Cbo_sing.Name = "Cbo_sing";
            this.Cbo_sing.Size = new System.Drawing.Size(110, 22);
            this.Cbo_sing.TabIndex = 1093;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(7, 8);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(76, 14);
            this.label11.TabIndex = 1094;
            this.label11.Text = "المستخــدم:";
            // 
            // Cbo_Term
            // 
            this.Cbo_Term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Term.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Term.FormattingEnabled = true;
            this.Cbo_Term.Location = new System.Drawing.Point(790, 63);
            this.Cbo_Term.Name = "Cbo_Term";
            this.Cbo_Term.Size = new System.Drawing.Size(142, 22);
            this.Cbo_Term.TabIndex = 1096;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(745, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 14);
            this.label13.TabIndex = 1095;
            this.label13.Text = "الفــرع:";
            // 
            // Txt_Nrec_date_From
            // 
            this.Txt_Nrec_date_From.Checked = false;
            this.Txt_Nrec_date_From.CustomFormat = "dd/MM/yyyy";
            this.Txt_Nrec_date_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Nrec_date_From.Location = new System.Drawing.Point(622, 38);
            this.Txt_Nrec_date_From.Name = "Txt_Nrec_date_From";
            this.Txt_Nrec_date_From.ShowCheckBox = true;
            this.Txt_Nrec_date_From.Size = new System.Drawing.Size(121, 20);
            this.Txt_Nrec_date_From.TabIndex = 1099;
            this.Txt_Nrec_date_From.ValueChanged += new System.EventHandler(this.Txt_Nrec_date_From_ValueChanged);
            // 
            // Txt_Nrec_date_to
            // 
            this.Txt_Nrec_date_to.Checked = false;
            this.Txt_Nrec_date_to.CustomFormat = "dd/mm/yyyy";
            this.Txt_Nrec_date_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Nrec_date_to.Location = new System.Drawing.Point(790, 38);
            this.Txt_Nrec_date_to.Name = "Txt_Nrec_date_to";
            this.Txt_Nrec_date_to.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Nrec_date_to.ShowCheckBox = true;
            this.Txt_Nrec_date_to.Size = new System.Drawing.Size(138, 20);
            this.Txt_Nrec_date_to.TabIndex = 1100;
            this.Txt_Nrec_date_to.ValueChanged += new System.EventHandler(this.Txt_Nrec_date_to_ValueChanged);
            // 
            // Txt_RCount
            // 
            this.Txt_RCount.BackColor = System.Drawing.Color.White;
            this.Txt_RCount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_RCount.Location = new System.Drawing.Point(130, 402);
            this.Txt_RCount.Name = "Txt_RCount";
            this.Txt_RCount.ReadOnly = true;
            this.Txt_RCount.Size = new System.Drawing.Size(126, 22);
            this.Txt_RCount.TabIndex = 1101;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(35, 406);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 14);
            this.label14.TabIndex = 1102;
            this.label14.Text = "عدد الحوالات:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(39, 575);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 14);
            this.label15.TabIndex = 1104;
            this.label15.Text = "عدد الحوالات:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(130, 571);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(126, 22);
            this.textBox1.TabIndex = 1103;
            // 
            // Cbo_RCur
            // 
            this.Cbo_RCur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_RCur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_RCur.FormattingEnabled = true;
            this.Cbo_RCur.Location = new System.Drawing.Point(417, 571);
            this.Cbo_RCur.Name = "Cbo_RCur";
            this.Cbo_RCur.Size = new System.Drawing.Size(186, 22);
            this.Cbo_RCur.TabIndex = 1106;
            this.Cbo_RCur.SelectedValueChanged += new System.EventHandler(this.Cbo_RCur_SelectedValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(322, 575);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 14);
            this.label16.TabIndex = 1105;
            this.label16.Text = "عملة الحوالة:";
            // 
            // Cbo_Rcur2
            // 
            this.Cbo_Rcur2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rcur2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rcur2.FormattingEnabled = true;
            this.Cbo_Rcur2.Location = new System.Drawing.Point(417, 402);
            this.Cbo_Rcur2.Name = "Cbo_Rcur2";
            this.Cbo_Rcur2.Size = new System.Drawing.Size(186, 22);
            this.Cbo_Rcur2.TabIndex = 1108;
            this.Cbo_Rcur2.SelectedValueChanged += new System.EventHandler(this.Cbo_Rcur2_SelectedValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(326, 406);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 14);
            this.label17.TabIndex = 1107;
            this.label17.Text = "عملة الحوالة:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(675, 575);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(148, 14);
            this.label18.TabIndex = 1110;
            this.label18.Text = "المجموع بالعملة الاصلية:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(674, 406);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(148, 14);
            this.label19.TabIndex = 1112;
            this.label19.Text = "المجموع بالعملة الاصلية:";
            // 
            // Btn_Excel
            // 
            this.Btn_Excel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Excel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Excel.Location = new System.Drawing.Point(366, 605);
            this.Btn_Excel.Name = "Btn_Excel";
            this.Btn_Excel.Size = new System.Drawing.Size(98, 26);
            this.Btn_Excel.TabIndex = 1115;
            this.Btn_Excel.Text = "تصدير اجمالي";
            this.Btn_Excel.UseVisualStyleBackColor = true;
            this.Btn_Excel.Click += new System.EventHandler(this.button4_Click);
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "doc_issue";
            this.Column4.HeaderText = "جهة الاصدار";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 250;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "doc_no";
            this.Column3.HeaderText = "رقم الوثيقة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "FRM_ADOC_NA";
            this.Column2.HeaderText = "نوع الوثيقة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 250;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "name";
            this.Column1.HeaderText = "الاســـــم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 265;
            // 
            // Grd_Person_Info
            // 
            this.Grd_Person_Info.AllowUserToAddRows = false;
            this.Grd_Person_Info.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Person_Info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Person_Info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_Person_Info.ColumnHeadersHeight = 30;
            this.Grd_Person_Info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.Grd_Person_Info.Location = new System.Drawing.Point(12, 112);
            this.Grd_Person_Info.Name = "Grd_Person_Info";
            this.Grd_Person_Info.ReadOnly = true;
            this.Grd_Person_Info.RowHeadersWidth = 15;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Person_Info.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_Person_Info.Size = new System.Drawing.Size(975, 141);
            this.Grd_Person_Info.TabIndex = 534;
            this.Grd_Person_Info.SelectionChanged += new System.EventHandler(this.Grd_Person_Info_SelectionChanged);
            // 
            // Txt_SSum_Amount
            // 
            this.Txt_SSum_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_SSum_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SSum_Amount.Location = new System.Drawing.Point(827, 571);
            this.Txt_SSum_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SSum_Amount.Name = "Txt_SSum_Amount";
            this.Txt_SSum_Amount.NumberDecimalDigits = 3;
            this.Txt_SSum_Amount.NumberDecimalSeparator = ".";
            this.Txt_SSum_Amount.NumberGroupSeparator = ",";
            this.Txt_SSum_Amount.ReadOnly = true;
            this.Txt_SSum_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_SSum_Amount.Size = new System.Drawing.Size(136, 23);
            this.Txt_SSum_Amount.TabIndex = 1114;
            this.Txt_SSum_Amount.Text = "0.000";
            // 
            // Txt_RSum_Amount
            // 
            this.Txt_RSum_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_RSum_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_RSum_Amount.Location = new System.Drawing.Point(827, 402);
            this.Txt_RSum_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_RSum_Amount.Name = "Txt_RSum_Amount";
            this.Txt_RSum_Amount.NumberDecimalDigits = 3;
            this.Txt_RSum_Amount.NumberDecimalSeparator = ".";
            this.Txt_RSum_Amount.NumberGroupSeparator = ",";
            this.Txt_RSum_Amount.ReadOnly = true;
            this.Txt_RSum_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_RSum_Amount.Size = new System.Drawing.Size(136, 23);
            this.Txt_RSum_Amount.TabIndex = 1113;
            this.Txt_RSum_Amount.Text = "0.000";
            // 
            // search_customer_aml
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 660);
            this.Controls.Add(this.Btn_Excel);
            this.Controls.Add(this.Txt_SSum_Amount);
            this.Controls.Add(this.Txt_RSum_Amount);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Cbo_Rcur2);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Cbo_RCur);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_RCount);
            this.Controls.Add(this.Txt_Nrec_date_to);
            this.Controls.Add(this.Txt_Nrec_date_From);
            this.Controls.Add(this.Cbo_Term);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Cbo_sing);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.Cbo_doc_type);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Txt_rem_no);
            this.Controls.Add(this.Txt_phone);
            this.Controls.Add(this.Txt_doc_no);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Btn_Export_Details);
            this.Controls.Add(this.Grd_RemOut);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Grd_RemIn);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.Grd_Person_Info);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "search_customer_aml";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "479";
            this.Text = "إستعلام عن حوالات زبون";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.search_customer_aml_FormClosed);
            this.Load += new System.EventHandler(this.Acc_Cust_Operations_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_RemIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_RemOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Person_Info)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridView Grd_RemIn;
        private System.Windows.Forms.DataGridView Grd_RemOut;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Btn_Export_Details;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_doc_no;
        private System.Windows.Forms.TextBox Txt_rem_no;
        private System.Windows.Forms.TextBox Txt_phone;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox Cbo_doc_type;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Cbo_sing;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Cbo_Term;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker Txt_Nrec_date_From;
        private System.Windows.Forms.DateTimePicker Txt_Nrec_date_to;
        private System.Windows.Forms.TextBox Txt_RCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox Cbo_RCur;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox Cbo_Rcur2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_RSum_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SSum_Amount;
        private System.Windows.Forms.Button Btn_Excel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView Grd_Person_Info;
    }
}