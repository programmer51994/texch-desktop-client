﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class search_reveal_per_info : Form
    {
        #region Defintion

        string @format = "dd/MM/yyyy";
        DataTable DT_per = new DataTable();
        DataTable DT_per_TBL = new DataTable();

        BindingSource BS_CITY = new BindingSource();
        BindingSource BS_NAT = new BindingSource();
        BindingSource BS_GENDER = new BindingSource();
        BindingSource BS_DOC = new BindingSource();
        BindingSource BS_JOB = new BindingSource();
        BindingSource BS_T_id = new BindingSource();
        BindingSource BS_resd = new BindingSource();


        Int64 from_nrec_date = 0;
        Int64 to_nrec_date = 0;
        string Birth_Da = "";
        string Doc_Da = "";
        string Exp_Da = "";
        string Sql_Text = "";
        string Sql_Text1 = "";
        int per_id = 0;
        string Filter = "";

        string name_mother = "";
        string name_birth_place = "";
        string name_phone = "";
        string name_other_phone = "";
        string name_email = "";
        string name_DO_NO = "";
        string name_DO_IS="";
        string name_social = "";
        string name_state = "";
        string name_sub = "";
        string name_street="";
        string name_post="";


        public static Int64 frm_date = 0;
        public static Int64 to_date = 0;
         public static Int64 t_id_new = 0;





        #endregion

        public search_reveal_per_info()
        {
            InitializeComponent(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_per.AutoGenerateColumns = false;
            Grd_per_id.AutoGenerateColumns = false;
            Create_Tbl();

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                //Grd_per_id.Columns["Column8"].DataPropertyName = "Ecust_name";
                //Grd_per.Columns["Column11"].DataPropertyName = "Ecust_name";
            }
            #endregion
        }

        private void Create_Tbl()

           
        {
            string[] Column1 = { "Per_ID", "Per_AName", "Per_EName" };
            string[] DType1 = { "System.Int16", "System.String", "System.String" };
            DT_per = CustomControls.Custom_DataTable("DT_per", Column1, DType1);
            Grd_per.DataSource = DT_per;


        }

        private void Search_Trial_Balance_Load(object sender, EventArgs e)
        {
            
            if (connection.Lang_id == 2)
            {
                Cbo_info_desplay.Items.Clear();
                Cbo_info_desplay.Items.Add("Detailed");
                Cbo_info_desplay.Items.Add("Total");
            }

            Cbo_info_desplay.SelectedIndex = 0;

            Sql_Text = " Exec all_master ";

            Sql_Text1 = "select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name  , A.cust_id as cust_id "
                       + "  From CUSTOMERS A, TERMINALS B   "
                       + "  Where A.CUST_ID = B.CUST_ID "
                       + "  And A.Cust_Flag <>  0 ";

            BS_T_id.DataSource = connection.SqlExec(Sql_Text1, "t_id_tbl");
            Cbo_t_id.DataSource = BS_T_id;
            Cbo_t_id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            Cbo_t_id.ValueMember = "T_id";
                       
            connection.SqlExec(Sql_Text, "all_master");
            BS_CITY.DataSource = connection.SQLDS.Tables["all_master6"];
            CboCit_Id.DataSource = BS_CITY;
            CboCit_Id.ValueMember = "Cit_Id";
            CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "cit_Con_Aname" : "cit_Con_Ename";

            BS_NAT.DataSource = connection.SQLDS.Tables["all_master1"];
            CboNat_id.DataSource = BS_NAT;
            CboNat_id.ValueMember = "NAt_Id";
            CboNat_id.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_eNAME";

            BS_DOC.DataSource = connection.SQLDS.Tables["all_master2"];
            CboDoc_id.DataSource = BS_DOC;
            CboDoc_id.ValueMember = "Fmd_id";
            CboDoc_id.DisplayMember = connection.Lang_id == 1 ? "Fmd_aname" : "Fmd_Ename";

            BS_GENDER.DataSource = connection.SQLDS.Tables["all_master7"];
            Cbo_Gender.DataSource = BS_GENDER;
            Cbo_Gender.ValueMember = "Gender_id";
            Cbo_Gender.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";

            BS_JOB.DataSource = connection.SQLDS.Tables["all_master11"];
            cmb_job.DataSource = BS_JOB;
            cmb_job.ValueMember = "Job_ID";
            cmb_job.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";

            BS_resd.DataSource = connection.SQLDS.Tables["all_master13"];
            resd_rec_cmb.DataSource = BS_resd;
            resd_rec_cmb.ValueMember = "resd_flag";
            resd_rec_cmb.DisplayMember = connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename";


            TxtFromDate.CustomFormat = "00/00/0000";
            TxtToDate.CustomFormat = "00/00/0000";
            TxtBirth_Da.CustomFormat = "00/00/0000";
            TxtDoc_Da.CustomFormat = "00/00/0000";
            TxtExp_Da.CustomFormat = "00/00/0000";
            /////////////////////////////////////////////
            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();
            connection.SqlExec("Exec Search_Account_Balance_new ", "Account_Balance_new_Tbl");
            ////////////////////////////////////////////
            if (connection.Lang_id == 2)
            {
                Cbo_sing_mother.Items.Add("matching"); Cbo_sing_mother.Items.Add("starts with"); Cbo_sing_mother.Items.Add("Wherever found");
                Cbo_sing_birth_place.Items.Add("matching"); Cbo_sing_birth_place.Items.Add("starts with"); Cbo_sing_birth_place.Items.Add("Wherever found");
                Cbo_sing_phone.Items.Add("matching"); Cbo_sing_phone.Items.Add("starts with"); Cbo_sing_phone.Items.Add("Wherever found");
                Cbo_sing_other_phone.Items.Add("matching"); Cbo_sing_other_phone.Items.Add("starts with"); Cbo_sing_other_phone.Items.Add("Wherever found");
                Cbo_sing_email.Items.Add("matching"); Cbo_sing_email.Items.Add("starts with"); Cbo_sing_email.Items.Add("Wherever found");
                Cbo_sing_Do_NO.Items.Add("matching"); Cbo_sing_Do_NO.Items.Add("starts with"); Cbo_sing_Do_NO.Items.Add("Wherever found");
                Cbo_sing_DO_IS.Items.Add("matching"); Cbo_sing_DO_IS.Items.Add("starts with"); Cbo_sing_DO_IS.Items.Add("Wherever found");
                Cbo_sing_social_no.Items.Add("matching"); Cbo_sing_social_no.Items.Add("starts with"); Cbo_sing_social_no.Items.Add("Wherever found");
                Cbo_sing_state.Items.Add("matching"); Cbo_sing_state.Items.Add("starts with"); Cbo_sing_state.Items.Add("Wherever found");
                Cbo_sing_sub.Items.Add("matching"); Cbo_sing_sub.Items.Add("starts with"); Cbo_sing_sub.Items.Add("Wherever found");
                Cbo_sing_street.Items.Add("matching"); Cbo_sing_street.Items.Add("starts with"); Cbo_sing_street.Items.Add("Wherever found");
                Cbo_sing_post.Items.Add("matching"); Cbo_sing_post.Items.Add("starts with"); Cbo_sing_post.Items.Add("Wherever found");
               

            }
            else
            {
                Cbo_sing_mother.Items.Add("مطابق"); Cbo_sing_mother.Items.Add("يبدأ ب"); Cbo_sing_mother.Items.Add("اينما وجد");
                Cbo_sing_birth_place.Items.Add("مطابق"); Cbo_sing_birth_place.Items.Add("يبدأ ب"); Cbo_sing_birth_place.Items.Add("اينما وجد");
                Cbo_sing_phone.Items.Add("مطابق"); Cbo_sing_phone.Items.Add("يبدأ ب"); Cbo_sing_phone.Items.Add("اينما وجد");
                Cbo_sing_other_phone.Items.Add("مطابق"); Cbo_sing_other_phone.Items.Add("يبدأ ب"); Cbo_sing_other_phone.Items.Add("اينما وجد");
                Cbo_sing_email.Items.Add("مطابق"); Cbo_sing_email.Items.Add("يبدأ ب"); Cbo_sing_email.Items.Add("اينما وجد");
                Cbo_sing_Do_NO.Items.Add("مطابق"); Cbo_sing_Do_NO.Items.Add("يبدأ ب"); Cbo_sing_Do_NO.Items.Add("اينما وجد");
                Cbo_sing_DO_IS.Items.Add("مطابق"); Cbo_sing_DO_IS.Items.Add("يبدأ ب"); Cbo_sing_DO_IS.Items.Add("اينما وجد");
                Cbo_sing_social_no.Items.Add("مطابق"); Cbo_sing_social_no.Items.Add("يبدأ ب"); Cbo_sing_social_no.Items.Add("اينما وجد");
                Cbo_sing_state.Items.Add("مطابق"); Cbo_sing_state.Items.Add("يبدأ ب"); Cbo_sing_state.Items.Add("اينما وجد");
                Cbo_sing_sub.Items.Add("مطابق"); Cbo_sing_sub.Items.Add("يبدأ ب"); Cbo_sing_sub.Items.Add("اينما وجد");
                Cbo_sing_street.Items.Add("مطابق"); Cbo_sing_street.Items.Add("يبدأ ب"); Cbo_sing_street.Items.Add("اينما وجد");
                Cbo_sing_post.Items.Add("مطابق"); Cbo_sing_post.Items.Add("يبدأ ب"); Cbo_sing_post.Items.Add("اينما وجد");
              
               
            }
            Cbo_sing_mother.SelectedIndex = 0;
            Cbo_sing_birth_place.SelectedIndex = 0;
            Cbo_sing_phone.SelectedIndex = 0;
            Cbo_sing_other_phone.SelectedIndex = 0;
            Cbo_sing_email.SelectedIndex = 0;
            Cbo_sing_Do_NO.SelectedIndex = 0;
            Cbo_sing_DO_IS.SelectedIndex = 0;
            Cbo_sing_social_no.SelectedIndex = 0;
            Cbo_sing_state.SelectedIndex = 0;
            Cbo_sing_sub.SelectedIndex = 0;
            Cbo_sing_street.SelectedIndex = 0;
            Cbo_sing_post.SelectedIndex = 0;

        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {

                if (connection.SQLDS.Tables["all_reveal_person_info_tbl"].Rows.Count > 0)
                {
                    DT_per_TBL = connection.SQLDS.Tables["all_reveal_person_info_tbl"];
                    per_id = 0;
                    string Cond_Str_per_id = "";
                    string Con_Str = "";
                    if (DT_per.Rows.Count > 0)
                    {
                        MyGeneral_Lib.ColumnToString(DT_per, "per_id", out Cond_Str_per_id);
                        Con_Str = " And per_id not in(" + Cond_Str_per_id + ")";
                    }
                    int.TryParse(TxtCust_Name.Text, out per_id);


                 

                    Filter = " (Per_AName like '" + TxtCust_Name.Text + "%' or  Per_EName like '" + TxtCust_Name.Text + "%'"
                              + " Or per_id = " + per_id + ")" + Con_Str;


                    DT_per_TBL = connection.SQLDS.Tables["all_reveal_person_info_tbl"].DefaultView.ToTable(true, "per_id", "Per_AName", "Per_EName").Select(Filter).CopyToDataTable();
                    Grd_per_id.DataSource = DT_per_TBL;
                    
                }
            }

            catch
            { Grd_per_id.DataSource = new DataTable(); }

          
            
            if (Grd_per_id.Rows.Count <= 0)
            {

                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }

        private void Search_Trial_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "search_reveal_person_info_tbl", "all_reveal_person_info_tbl", "search_reveal_person_info_tbl1", "search_reveal_person_info_tbl2" 
                                ,"search_reveal_person_info_tbl3","search_reveal_person_info_tbl4","search_reveal_person_info_tbl5","all_master13","all_master1","all_master2",
                                "all_master6","all_master7","all_master11"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_per.NewRow();

            row["Per_ID"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_ID"];
            row["Per_AName"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_AName"];
            row["Per_EName"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_EName"];
            DT_per.Rows.Add(row);
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);

            button3.Enabled = true;
            button4.Enabled = true;
            if (Grd_per_id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_per_TBL.Rows.Count; i++)
            {
                DataRow row = DT_per.NewRow();
                row["Per_ID"] = DT_per_TBL.Rows[i]["Per_ID"];
                row["Per_AName"] = DT_per_TBL.Rows[i]["Per_AName"];
                row["Per_EName"] = DT_per_TBL.Rows[i]["Per_EName"];
                DT_per.Rows.Add(row);
            }
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DT_per.Rows[Grd_per.CurrentRow.Index].Delete();
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            if (Grd_per.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DT_per.Rows.Clear();
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {

              getdata();

            //-----------------------------------------------------------

            string Condition = "";
            if (DT_per.Rows.Count > 0)
            {
                string per_id_Str = "";
                MyGeneral_Lib.ColumnToString(DT_per, "per_id", out  per_id_Str);
                string[] values = per_id_Str.Split(',');
                for (int i = 0; i < values.Length; i++)
                {
                    Condition += values[i].Trim() + ",";
                }
                Condition = Condition.Remove(Condition.Length - 1);
            }

            if (Condition.Length > 8000)
            { Condition = ""; }
            //-----------------------------------------------------------   
            //string Sqltexe = "EXec search_reveal_person_info " + "'" + from_nrec_date + "'" + "," + "'" + to_nrec_date + "'" + "," + CboCit_Id.SelectedValue + "," + "'" + name_birth_place + "'" + "," + CboNat_id.SelectedValue
            //    + "," + "'" + Birth_Da + "'" + "," + Cbo_Gender.SelectedValue + "," + "'" + name_phone + "'" + "," + "'" + name_mother + "'" + "," + "'" + name_other_phone + "'"
            //    + "," + cmb_job.SelectedValue + "," + resd_rec_cmb.SelectedValue + ","+ "'" + name_email + "'" + "," + CboDoc_id.SelectedValue + "," + "'" + name_DO_NO + "'" + "," + "'" + Doc_Da + "'"
            //    + "," + "'" + Exp_Da + "'" + "," + "'" + name_DO_IS + "'" + "," + "'" + name_social + "'" + "," + "'" + name_state + "'" + "," + "'" + name_street + "'"
            //    + "," + "'" + name_sub + "'" + "," + "'" + name_post + "'" + "," + "'" + Condition + "'" + "," + Cbo_t_id.SelectedValue;

            try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "search_reveal_person_info" : "search_reveal_person_info_PHst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                if (cbo_year.SelectedIndex == 0)//cureent
                {
                    connection.SQLCMD.Parameters.AddWithValue("@from_date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@to_date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@per_City_ID", CboCit_Id.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Birth_place", name_birth_place);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Nationalty_ID", CboNat_id.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Birth_day", Birth_Da);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Gender_ID", Cbo_Gender.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Phone", name_phone);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Mother_name", name_mother);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Another_Phone", name_other_phone);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Occupation_id", cmb_job.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@resd_flag", resd_rec_cmb.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Email", name_email);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_ID", CboDoc_id.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_NO", name_DO_NO);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_Date", Doc_Da);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_EDate", Exp_Da);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_IS", name_DO_IS);
                    connection.SQLCMD.Parameters.AddWithValue("@Social_No", name_social);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_State", name_state);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Street", name_street);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Suburb", name_sub);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Post_Code", name_post);
                    connection.SQLCMD.Parameters.AddWithValue("@per_id", Condition);
                    connection.SQLCMD.Parameters.AddWithValue("@t_id", Cbo_t_id.SelectedValue);
                    connection.SQLCMD.Parameters.Add("@tot_count", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@tot_count"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_buy", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_buy"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_sale", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_sale"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_rem_out", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_rem_out"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_rem_in", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_rem_in"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_tot_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_tot_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_buy_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_buy_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_sal_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_sal_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_out_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_out_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_in_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_in_per"].Direction = ParameterDirection.Output;
                }
                else //old
                {
                    connection.SQLCMD.Parameters.AddWithValue("@from_date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@to_date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@per_City_ID", CboCit_Id.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Birth_place", name_birth_place);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Nationalty_ID", CboNat_id.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Birth_day", Birth_Da);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Gender_ID", Cbo_Gender.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Phone", name_phone);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Mother_name", name_mother);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Another_Phone", name_other_phone);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Occupation_id", cmb_job.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@resd_flag", resd_rec_cmb.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Email", name_email);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_ID", CboDoc_id.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_NO", name_DO_NO);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_Date", Doc_Da);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_EDate", Exp_Da);
                    connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_IS", name_DO_IS);
                    connection.SQLCMD.Parameters.AddWithValue("@Social_No", name_social);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_State", name_state);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Street", name_street);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Suburb", name_sub);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Post_Code", name_post);
                    connection.SQLCMD.Parameters.AddWithValue("@per_id", Condition);
                    connection.SQLCMD.Parameters.AddWithValue("@t_id", Cbo_t_id.SelectedValue);
                    connection.SQLCMD.Parameters.Add("@tot_count", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@tot_count"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_buy", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_buy"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_sale", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_sale"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_rem_out", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_rem_out"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_rem_in", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_rem_in"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_tot_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_tot_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_buy_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_buy_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_sal_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_sal_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_out_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_out_per"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@count_in_per", SqlDbType.Float).Value = 0;
                    connection.SQLCMD.Parameters["@count_in_per"].Direction = ParameterDirection.Output;
                }

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "search_reveal_person_info_tbl");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "search_reveal_person_info_tbl1");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "search_reveal_person_info_tbl2");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "search_reveal_person_info_tbl3");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "search_reveal_person_info_tbl4");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "search_reveal_person_info_tbl5");
                obj.Close();
                connection.SQLCS.Close();

                //if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                //{
                //    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                //    connection.SQLCMD.Parameters.Clear();
                //    return;
                //}
                if ((connection.SQLDS.Tables["search_reveal_person_info_tbl4"].Rows.Count <= 0) && (connection.SQLDS.Tables["search_reveal_person_info_tbl"].Rows.Count <= 0) &&
                (connection.SQLDS.Tables["search_reveal_person_info_tbl1"].Rows.Count <= 0) && (connection.SQLDS.Tables["search_reveal_person_info_tbl2"].Rows.Count <= 0) &&
                (connection.SQLDS.Tables["search_reveal_person_info_tbl3"].Rows.Count <= 0))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط " : "No data For this cases", MyGeneral_Lib.LblCap);
                    return;
                }
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();

                Int64 t_id = Convert.ToInt64(Cbo_t_id.SelectedValue);
                frm_date = from_nrec_date;
                to_date = to_nrec_date;
                t_id_new = t_id;
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }

            
            //string Sqltexe = "EXec search_reveal_person_info " + "'" + from_nrec_date + "'" + "," + "'" + to_nrec_date + "'" + "," + CboCit_Id.SelectedValue + "," + "'" + name_birth_place + "'" + "," + CboNat_id.SelectedValue
            //    + "," + "'" + Birth_Da + "'" + "," + Cbo_Gender.SelectedValue + "," + "'" + name_phone + "'" + "," + "'" + name_mother + "'" + "," + "'" + name_other_phone + "'"
            //    + "," + cmb_job.SelectedValue + "," + resd_rec_cmb.SelectedValue + ","+ "'" + name_email + "'" + "," + CboDoc_id.SelectedValue + "," + "'" + name_DO_NO + "'" + "," + "'" + Doc_Da + "'"
            //    + "," + "'" + Exp_Da + "'" + "," + "'" + name_DO_IS + "'" + "," + "'" + name_social + "'" + "," + "'" + name_state + "'" + "," + "'" + name_street + "'"
            //    + "," + "'" + name_sub + "'" + "," + "'" + name_post + "'" + "," + "'" + Condition + "'" + "," + Cbo_t_id.SelectedValue;
            //    connection.SqlExec(Sqltexe, "search_reveal_person_info_tbl");

            //if ((connection.SQLDS.Tables["search_reveal_person_info_tbl4"].Rows.Count <= 0) && (connection.SQLDS.Tables["search_reveal_person_info_tbl"].Rows.Count <= 0)&&
            //    (connection.SQLDS.Tables["search_reveal_person_info_tbl1"].Rows.Count <= 0) && (connection.SQLDS.Tables["search_reveal_person_info_tbl2"].Rows.Count <= 0)&&
            //    (connection.SQLDS.Tables["search_reveal_person_info_tbl3"].Rows.Count <= 0) )
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط " : "No data For this cases", MyGeneral_Lib.LblCap);
            //    return;
            //}

            //Int64 t_id = Convert.ToInt64( Cbo_t_id.SelectedValue);

            //frm_date = from_nrec_date;
            //to_date = to_nrec_date;
            //t_id_new = t_id;


            if (Cbo_info_desplay.SelectedIndex == 0)
            {
                Reveal_Per_Info Frm = new Reveal_Per_Info();
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }
            if (Cbo_info_desplay.SelectedIndex == 1)
            {
                Reveal_Per_Info_ALL Frm = new Reveal_Per_Info_ALL();
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }

            if (connection.SQLDS.Tables.Contains("all_reveal_person_info_tbl"))
            { connection.SQLDS.Tables.Remove("all_reveal_person_info_tbl"); }
            Cbo_t_id.SelectedIndex = 0;
            CboCit_Id.SelectedIndex = 0;
            CboNat_id.SelectedIndex = 0;
            Cbo_Gender.SelectedIndex = 0;
            Cbo_sing_mother.SelectedIndex = 0;
            cmb_job.SelectedIndex = 0;
            resd_rec_cmb.SelectedIndex = 0;
            Cbo_sing_birth_place.SelectedIndex = 0;
            Cbo_sing_phone.SelectedIndex = 0;
            Cbo_sing_other_phone.SelectedIndex = 0;
            Cbo_sing_email.SelectedIndex = 0;
            CboDoc_id.SelectedIndex = 0;
            Cbo_sing_Do_NO.SelectedIndex = 0;
            Cbo_sing_DO_IS.SelectedIndex = 0;
            Cbo_sing_social_no.SelectedIndex = 0;
            Cbo_sing_state.SelectedIndex = 0;
            Cbo_sing_sub.SelectedIndex = 0;
            Cbo_sing_street.SelectedIndex = 0;
            Cbo_sing_post.SelectedIndex = 0;
            TxtBirth_Da.Checked = false;
            TxtDoc_Da.Checked = false;
            TxtExp_Da.Checked = false;
            TxtFromDate.Checked = false;
            TxtToDate.Checked = false;
            Txt_Mother_name.Text = "";
            Txt_birth_place.Text = "";
            TxtPhone.Text = "";
            Txt_Another_Phone.Text = "";
            TxtEmail.Text = "";
            TxtNo_Doc.Text = "";
            TxtIss_Doc.Text = "";
            Txt_Social_ID.Text = "";
            Txt_State.Text = "";
            Txt_Suburb.Text = "";
            Txt_Street.Text = "";
            Txt_Post_code.Text = "";
            TxtCust_Name.Text = "";
            Grd_per_id.DataSource = new BindingSource();
            DT_per.Clear();
            DT_per_TBL.Clear();
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;

        }

      

        private void Search_Reveal_pl_Acc_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }


        private void TxtFromDate_ValueChanged_1(object sender, EventArgs e)
        {
            if (TxtFromDate.Checked == true)
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = @format;
            }
            else
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = "00/00/0000";
            }
        }

        private void TxtToDate_ValueChanged_1(object sender, EventArgs e)
        {
            if (TxtToDate.Checked == true)
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = @format;
            }
            else
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = "00/00/0000";
            }
        }

        private void TxtBirth_Da_ValueChanged_1(object sender, EventArgs e)
        {
            if (TxtBirth_Da.Checked == true)
            {
                TxtBirth_Da.Format = DateTimePickerFormat.Custom;
                TxtBirth_Da.CustomFormat = @format;
            }
            else
            {
                TxtBirth_Da.Format = DateTimePickerFormat.Custom;
                TxtBirth_Da.CustomFormat = "00/00/0000";
            }
        }

        private void TxtDoc_Da_ValueChanged_1(object sender, EventArgs e)
        {
            if (TxtDoc_Da.Checked == true)
            {
                TxtDoc_Da.Format = DateTimePickerFormat.Custom;
                TxtDoc_Da.CustomFormat = @format;
            }
            else
            {
                TxtDoc_Da.Format = DateTimePickerFormat.Custom;
                TxtDoc_Da.CustomFormat = "00/00/0000";
            }
        }

        private void TxtExp_Da_ValueChanged_1(object sender, EventArgs e)
        {
            if (TxtExp_Da.Checked == true)
            {
                TxtExp_Da.Format = DateTimePickerFormat.Custom;
                TxtExp_Da.CustomFormat = @format;
            }
            else
            {
                TxtExp_Da.Format = DateTimePickerFormat.Custom;
                TxtExp_Da.CustomFormat = "00/00/0000";
            }
        }


        private void getdata()
        {

            #region text_time


           
            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //from_nrec_date = TxtFromDate.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                from_nrec_date = 0;
            }
            //-------------------------
            if (TxtToDate.Checked == true)
            {


                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //  to_nrec_date = TxtToDate.Value.ToString("yyyy/MM/dd");
            }
            else
            {
                to_nrec_date = 0;
            }
            //---------------------------------
            if (TxtBirth_Da.Checked == true)
            {

                Birth_Da = TxtBirth_Da.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                Birth_Da = "0";
            }
            //-----------------------------------------------------

            if (TxtDoc_Da.Checked == true)
            {

                Doc_Da = TxtDoc_Da.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                Doc_Da = "0";
            }
            //--------------------------

            if (TxtExp_Da.Checked == true)
            {

                Exp_Da = TxtExp_Da.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                Exp_Da = "0";
            }

            #endregion

            #region cbo_sign

            if (Cbo_sing_mother.SelectedIndex == 0)
            {
                name_mother = Txt_Mother_name.Text;
            }
            if (Cbo_sing_mother.SelectedIndex == 1)
            {
                name_mother = Txt_Mother_name.Text + "%";
            }
            if (Cbo_sing_mother.SelectedIndex == 2)
            {
                name_mother = "%" + Txt_Mother_name.Text + "%";
            }

            //--------------------------------------


            if (Cbo_sing_birth_place.SelectedIndex == 0)
            {
                name_birth_place = Txt_birth_place.Text;
            }
            if (Cbo_sing_birth_place.SelectedIndex == 1)
            {
                name_birth_place = Txt_birth_place.Text + "%";
            }
            if (Cbo_sing_birth_place.SelectedIndex == 2)
            {
                name_birth_place = "%" + Txt_birth_place.Text + "%";
            }
            //------------------------------------------------

            if (Cbo_sing_phone.SelectedIndex == 0)
            {
                name_phone = TxtPhone.Text;
            }
            if (Cbo_sing_phone.SelectedIndex == 1)
            {
                name_phone = TxtPhone.Text + "%";
            }
            if (Cbo_sing_phone.SelectedIndex == 2)
            {
                name_phone = "%" + TxtPhone.Text + "%";
            }
            //---------------------------------
            if (Cbo_sing_other_phone.SelectedIndex == 0)
            {
                name_other_phone = Txt_Another_Phone.Text;
            }
            if (Cbo_sing_other_phone.SelectedIndex == 1)
            {
                name_other_phone = Txt_Another_Phone.Text + "%";
            }
            if (Cbo_sing_other_phone.SelectedIndex == 2)
            {
                name_other_phone = "%" + Txt_Another_Phone.Text + "%";
            }
            //--------------------------------------

            if (Cbo_sing_email.SelectedIndex == 0)
            {
                name_email = TxtEmail.Text;
            }
            if (Cbo_sing_email.SelectedIndex == 1)
            {
                name_email = TxtEmail.Text + "%";
            }
            if (Cbo_sing_email.SelectedIndex == 2)
            {
                name_email = "%" + TxtEmail.Text + "%";
            }
            //---------------------------------------

            if (Cbo_sing_Do_NO.SelectedIndex == 0)
            {
                name_DO_NO = TxtNo_Doc.Text;
            }
            if (Cbo_sing_Do_NO.SelectedIndex == 1)
            {
                name_DO_NO = TxtNo_Doc.Text + "%";
            }
            if (Cbo_sing_Do_NO.SelectedIndex == 2)
            {
                name_DO_NO = "%" + TxtNo_Doc.Text + "%";
            }
            //-------------------------------------------

            if (Cbo_sing_DO_IS.SelectedIndex == 0)
            {
                name_DO_IS = TxtIss_Doc.Text;
            }
            if (Cbo_sing_DO_IS.SelectedIndex == 1)
            {
                name_DO_IS = TxtIss_Doc.Text + "%";
            }
            if (Cbo_sing_DO_IS.SelectedIndex == 2)
            {
                name_DO_IS = "%" + TxtIss_Doc.Text + "%";
            }

            //-------------------------------

            if (Cbo_sing_social_no.SelectedIndex == 0)
            {
                name_social = Txt_Social_ID.Text;
            }
            if (Cbo_sing_social_no.SelectedIndex == 1)
            {
                name_social = Txt_Social_ID.Text + "%";
            }
            if (Cbo_sing_social_no.SelectedIndex == 2)
            {
                name_social = "%" + Txt_Social_ID.Text + "%";
            }

            //----------------------------------------------

            if (Cbo_sing_state.SelectedIndex == 0)
            {
                name_state = Txt_State.Text;
            }
            if (Cbo_sing_state.SelectedIndex == 1)
            {
                name_state = Txt_State.Text + "%";
            }
            if (Cbo_sing_state.SelectedIndex == 2)
            {
                name_state = "%" + Txt_State.Text + "%";
            }

            //--------------------------------

            if (Cbo_sing_sub.SelectedIndex == 0)
            {
                name_sub = Txt_Suburb.Text;
            }
            if (Cbo_sing_sub.SelectedIndex == 1)
            {
                name_sub = Txt_Suburb.Text + "%";
            }
            if (Cbo_sing_sub.SelectedIndex == 2)
            {
                name_sub = "%" + Txt_Suburb.Text + "%";
            }

            //----------------------------------------

            if (Cbo_sing_street.SelectedIndex == 0)
            {
                name_street = Txt_Street.Text;
            }
            if (Cbo_sing_street.SelectedIndex == 1)
            {
                name_street = Txt_Street.Text + "%";
            }
            if (Cbo_sing_street.SelectedIndex == 2)
            {
                name_street = "%" + Txt_Street.Text + "%";
            }
            //-----------------------------------

            if (Cbo_sing_post.SelectedIndex == 0)
            {
                name_post = Txt_Post_code.Text;
            }
            if (Cbo_sing_post.SelectedIndex == 1)
            {
                name_post = Txt_Post_code.Text + "%";
            }
            if (Cbo_sing_post.SelectedIndex == 2)
            {
                name_post = "%" + Txt_Post_code.Text + "%";
            }

            //--------------------------------------------

            #endregion
     
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {


            getdata();

           if( connection.SQLDS.Tables.Contains("all_reveal_person_info_tbl"))
           { connection.SQLDS.Tables.Remove("all_reveal_person_info_tbl");}

           if (DT_per.Rows.Count > 0)
           { DT_per.Clear();}


           if (DT_per_TBL.Rows.Count > 0)
           { DT_per_TBL.Clear(); }

             int per_id_serach=0;
             int.TryParse(TxtCust_Name.Text, out per_id_serach);


            string Sqltexe = "EXec all_reveal_person_info " + CboCit_Id.SelectedValue + "," + "'" + name_birth_place + "'" + "," + CboNat_id.SelectedValue
            + "," + "'" + Birth_Da + "'" + "," + Cbo_Gender.SelectedValue + "," + "'" + name_phone + "'" + "," + "'" + name_mother + "'" + "," + "'" + name_other_phone + "'"
            + "," + cmb_job.SelectedValue + "," + resd_rec_cmb.SelectedValue +"," + "'" + name_email + "'" + "," + CboDoc_id.SelectedValue + "," + "'" + name_DO_NO + "'" + "," + "'" + Doc_Da + "'"
            + "," + "'" + Exp_Da + "'" + "," + "'" + name_DO_IS + "'" + "," + "'" + name_social + "'" + "," + "'" + name_state + "'" + "," + "'" + name_street + "'"
            + "," + "'" + name_sub + "'" + "," + "'" + name_post + "'" + "," + per_id_serach + "," + "'" + TxtCust_Name.Text.Trim() + "'";

               connection.SqlExec(Sqltexe, "all_reveal_person_info_tbl");

                if (connection.SQLDS.Tables["all_reveal_person_info_tbl"].Rows.Count > 0)
                {  TxtCust_Name_TextChanged(null, null); }

                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط " : "No data For this cases", MyGeneral_Lib.LblCap);
                    return;
                    TxtCust_Name.Text = "";
                }
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();
                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

                TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();
                TxtToDate.Checked = false;
                //checkBox1.Visible = false;

            }
            if (cbo_year.SelectedIndex == 1)// old 
            {
                try
                {
                    Grd_years.Enabled = true;
                    //_bsyears.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl3"];
                    //Grd_years.DataSource = _bsyears;

                    if (connection.SQLDS.Tables["Account_Balance_new_Tbl3"].Rows.Count > 0)
                    {
                        Grd_years.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl3"].Select("t_id =" + connection.T_ID).CopyToDataTable();

                        TxtFromDate.Text = connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["min_nrec_date"].ToString();
                        TxtToDate.Text = connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["max_nrec_date"].ToString();
                    }

                    //Change_grd = true;
                    //Grd_years_SelectionChanged(null, null);
                    // checkBox1.Visible = true;
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                        cbo_year.SelectedIndex = 0;
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }


            }
        }

    }
}
