﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys.remittences_online
{
    public partial class Inquery_aml_Remittances : Form
    {
        DataTable Q_DT = new DataTable();
        BindingSource _BS_GRD_rem = new BindingSource();
        BindingSource Bs_Viewrem = new BindingSource();
        bool Change = false;

        public Inquery_aml_Remittances(DataTable Query_Dt)
        {
            InitializeComponent();

            Q_DT = Query_Dt;

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            
            Grdrec_rem.AutoGenerateColumns = false;
        }




        private void Inquery_aml_Remittances_Load(object sender, EventArgs e)
        {

            Bs_Viewrem.DataSource = Q_DT;
            Grdrec_rem.DataSource = Bs_Viewrem;

            Change = true;
            Grdrec_rem_SelectionChanged(null, null);


            //-----------


            if (connection.Lang_id == 2)
            {

                //---------Engilish dataproperty
                //Normal Rem:

                Column2.DataPropertyName = "R_ECur_name";
                Column4.DataPropertyName = "PR_ECUR_name";
                Column8.DataPropertyName = "t_Ecity_name";
                Column9.DataPropertyName = "s_Ecity_name";
                //Column9.DataPropertyName = "ACASE_NA";
                Column5.DataPropertyName = "ECASE_NA";
                Column15.DataPropertyName = "s_E_NAT_NAME";
                Column17.DataPropertyName = "Sa_ECITY_NAME";
                Column23.DataPropertyName = "sFRM_EDOC_NA";
                Column18.DataPropertyName = "R_E_NAT_NAME";
                Column24.DataPropertyName = "r_ECITY_NAME";
                Column33.DataPropertyName = "RFRM_EDOC_NA";
           

            }

            //-----------------------------------
            Grdrec_rem.Columns["Column38"].HeaderText = connection.Lang_id == 1 ? "غرض التحويل" : "T Purpose";
            Grdrec_rem.Columns["Column27"].HeaderText = connection.Lang_id == 1 ? "علاقة مر. مس." : "S. R. Realation";
            Grdrec_rem.Columns["Column42"].HeaderText = connection.Lang_id == 1 ? "ملاحظات المستلم" : "R. Notes";
            Grdrec_rem.Columns["Column39"].HeaderText = connection.Lang_id == 1 ? "ملاحظات المرسل" : "S. Notes";
            Grdrec_rem.Columns["Column37"].HeaderText = connection.Lang_id == 1 ? "محل اصدار الوثيقة للمستلم" : "Doc. Issue";
            Grdrec_rem.Columns["Column36"].HeaderText = connection.Lang_id == 1 ? "تاريخ انشاء الوثيقة للمستلم" : "R. Doc. ida";
            Grdrec_rem.Columns["Column34"].HeaderText = connection.Lang_id == 1 ? " رقم وثيقة المستلم" : "R. Doc. Num.";
            Grdrec_rem.Columns["Column33"].HeaderText = connection.Lang_id == 1 ? "وثيقة المستلم" : "R. Doc.";
            Grdrec_rem.Columns["Column32"].HeaderText = connection.Lang_id == 1 ? "عنوان المستلم" : "R. Address";
            Grdrec_rem.Columns["Column41"].HeaderText = connection.Lang_id == 1 ? "تفاصيل عمل المستلم" : "R. Job. details";
            Grdrec_rem.Columns["Column31"].HeaderText = connection.Lang_id == 1 ? "مهنة المستلم" : "R. Job";
            Grdrec_rem.Columns["Column30"].HeaderText = connection.Lang_id == 1 ? "محل ولادة المستلم" : "R. Birth Place";
            Grdrec_rem.Columns["Column29"].HeaderText = connection.Lang_id == 1 ? "ميلاد المستلم" : "R Birthday";
            Grdrec_rem.Columns["Column24"].HeaderText = connection.Lang_id == 1 ? "مدينة المستلم" : "R. City";
            Grdrec_rem.Columns["Column18"].HeaderText = connection.Lang_id == 1 ? "جنسية المستلم" : "R. Nation.";
            Grdrec_rem.Columns["Column16"].HeaderText = connection.Lang_id == 1 ? "هاتف المستلم" : "R. Phone";
            Grdrec_rem.Columns["Column28"].HeaderText = connection.Lang_id == 1 ? "مصدر المال" : "Money source";
            Grdrec_rem.Columns["Column26"].HeaderText = connection.Lang_id == 1 ? "جهة اصدار وثيقة المرسل" : "S. Doc. Issue.";
            Grdrec_rem.Columns["Column35"].HeaderText = connection.Lang_id == 1 ? "تاريخ انشاء الوثيقة للمرسل" : "S. Doc. ida";
            Grdrec_rem.Columns["Column25"].HeaderText = connection.Lang_id == 1 ? "رقم وثيقة المرسل" : "S. Doc. Num.";
            Grdrec_rem.Columns["Column23"].HeaderText = connection.Lang_id == 1 ? "وثيقة المرسل" : "S. Doc.";
            Grdrec_rem.Columns["Column22"].HeaderText = connection.Lang_id == 1 ? "عنوان المرسل" : "S. Address";
            Grdrec_rem.Columns["Column40"].HeaderText = connection.Lang_id == 1 ? "تفاصيل عمل المرسل" : "S. Job details";
            Grdrec_rem.Columns["Column21"].HeaderText = connection.Lang_id == 1 ? "مهنة المرسل" : "S. Job";
            Grdrec_rem.Columns["Column20"].HeaderText = connection.Lang_id == 1 ? "مكان ولادة المرسل" : "S. Birth Place";
            Grdrec_rem.Columns["Column19"].HeaderText = connection.Lang_id == 1 ? "تاريخ ميلاد المرسل" : "S Birthdate";
            Grdrec_rem.Columns["Column17"].HeaderText = connection.Lang_id == 1 ? "مدينة المرسل" : "S. city";
            Grdrec_rem.Columns["Column15"].HeaderText = connection.Lang_id == 1 ? "جنسية المرسل" : "S. Nation.";
            Grdrec_rem.Columns["Column14"].HeaderText = connection.Lang_id == 1 ? "هاتف المرسل" : "S. Phone";
            Grdrec_rem.Columns["Column13"].HeaderText = connection.Lang_id == 1 ? "اسم المستخدم" : "User Name";
            Grdrec_rem.Columns["Column11"].HeaderText = connection.Lang_id == 1 ? "تأريخ ووقت الانشاء" : "T Purpose";
            Grdrec_rem.Columns["Column10"].HeaderText = connection.Lang_id == 1 ? "اسم المستــلم" : "R. Name";
            //Column10
           

        }

        private void Btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Inquery_aml_Remittances_FormClosed(object sender, FormClosedEventArgs e)
        {

            Change = false;
            
            //string[] Str = { "", "" };
            //foreach (string Tbl in Str)
            //{
            //    if (connection.SQLDS.Tables.Contains(Tbl))
            //        connection.SQLDS.Tables.Remove(Tbl);
            //}

        }

        private void Grdrec_rem_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {

                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtScity.DataBindings.Clear();
                Txt_S_Birth.DataBindings.Clear();
                //Txt_S_Birth_Place.DataBindings.Clear();
                Txt_s_job.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                Txt_S_doc_type.DataBindings.Clear();
                Txt_S_doc_no.DataBindings.Clear();
                Txt_S_doc_Date.DataBindings.Clear();
                Txt_S_doc_issue.DataBindings.Clear();
                Txt_Relionship.DataBindings.Clear();
                Txt_Soruce_money.DataBindings.Clear();
                Txt_R_Name.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                Txt_R_Nat.DataBindings.Clear();
                Txt_R_City.DataBindings.Clear();
                Txt_r_birthdate.DataBindings.Clear();
                // Txt_R_Birth_Place.DataBindings.Clear();
                Txt_r_job.DataBindings.Clear();
                Txt_R_address.DataBindings.Clear();
                Txt_R_doc_type.DataBindings.Clear();
                Txt_R_doc_no.DataBindings.Clear();
                Txt_R_doc_date.DataBindings.Clear();
                Txt_r_doc_issue.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                Txt_Discreption.DataBindings.Clear();

                Txt_S_details_job.DataBindings.Clear();
                Txt_R_details_job.DataBindings.Clear();
                Txt_R_notes.DataBindings.Clear();
                txt_purpose_rec.DataBindings.Clear();
                Txt_R_Relation.DataBindings.Clear();




                //CboR_cur_name.DataBindings.Clear();
                //Txtreccount.DataBindings.Clear();
                //Txttotal_amuont.DataBindings.Clear();
                TxtS_name.DataBindings.Add("Text", Bs_Viewrem, "S_name");
                TxtS_phone.DataBindings.Add("Text", Bs_Viewrem, "S_phone");
                Txts_nat.DataBindings.Add("Text", Bs_Viewrem,  connection.Lang_id == 1 ?"s_A_NAT_NAME":"s_E_NAT_NAME");
                TxtScity.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                Txt_S_Birth.DataBindings.Add("Text", Bs_Viewrem, "sbirth_date");
                // Txt_S_Birth_Place.DataBindings.Add("Text", Bs_rem, "SBirth_Place");
                Txt_s_job.DataBindings.Add("Text", Bs_Viewrem, "s_job");
                TxtS_address.DataBindings.Add("Text", Bs_Viewrem, "S_address");
                Txt_S_doc_type.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                Txt_S_doc_no.DataBindings.Add("Text", Bs_Viewrem, "S_doc_no");
                Txt_S_doc_Date.DataBindings.Add("Text", Bs_Viewrem, "S_doc_ida");
                Txt_S_doc_issue.DataBindings.Add("Text", Bs_Viewrem, "S_doc_issue");
                Txt_Relionship.DataBindings.Add("Text", Bs_Viewrem, "Relation_S_R");
                Txt_Soruce_money.DataBindings.Add("Text", Bs_Viewrem, "Source_money");
                Txt_R_Name.DataBindings.Add("Text", Bs_Viewrem, "r_name");
                Txt_R_Phone.DataBindings.Add("Text", Bs_Viewrem, "R_phone");
                Txt_R_Nat.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "R_A_NAT_NAME" : "R_E_NAT_NAME");
                Txt_R_City.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                Txt_r_birthdate.DataBindings.Add("Text", Bs_Viewrem, "rbirth_date");
                // Txt_R_Birth_Place.DataBindings.Add("Text", Bs_rem, "TBirth_Place");
                Txt_r_job.DataBindings.Add("Text", Bs_Viewrem, "r_job");
                Txt_R_address.DataBindings.Add("Text", Bs_Viewrem, "r_address");
                Txt_R_doc_type.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "RFRM_ADOC_NA" : "RFRM_EDOC_NA");
                Txt_R_doc_no.DataBindings.Add("Text", Bs_Viewrem, "r_doc_no");
                Txt_R_doc_date.DataBindings.Add("Text", Bs_Viewrem, "r_doc_ida");
                Txt_r_doc_issue.DataBindings.Add("Text", Bs_Viewrem, "r_doc_issue");
                Txt_Purpose.DataBindings.Add("Text", Bs_Viewrem, "T_purpose");
                Txt_Discreption.DataBindings.Add("Text", Bs_Viewrem, "s_notes");

                Txt_S_details_job.DataBindings.Add("Text", Bs_Viewrem, "s_details_job");
                Txt_R_details_job.DataBindings.Add("Text", Bs_Viewrem, "r_details_job");
                Txt_R_notes.DataBindings.Add("Text", Bs_Viewrem, "r_notes");
                txt_purpose_rec.DataBindings.Add("Text", Bs_Viewrem, "T_purpose");
                Txt_R_Relation.DataBindings.Add("Text", Bs_Viewrem, "Relation_S_R");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {


            if (connection.Lang_id == 1)
            {

                DataTable ExpDt = Q_DT.DefaultView.ToTable(false, "rem_no", "S_name", "R_amount", "R_aCur_name", "PR_aCUR_name", "t_acity_name", "s_acity_name", "r_name", "ACASE_NA", "Case_Date", "C_DATE", "User_name", "S_phone", "s_A_NAT_NAME", "Sa_ACITY_NAME", "sbirth_date", "SBirth_Place", "s_job", "s_details_job", "S_address", "sFRM_ADOC_NA", "S_doc_no", "S_doc_ida", "S_doc_issue", "Source_money", "R_phone", "R_A_NAT_NAME", "r_ACITY_NAME", "rbirth_date", "TBirth_Place", "r_job", "r_details_job", "r_address", "RFRM_ADOC_NA", "r_doc_no", "r_doc_ida", "r_doc_issue", "s_notes", "r_notes", "Relation_S_R", "T_purpose").Select().CopyToDataTable();
                DataTable[] _EXP_DT = { ExpDt };
                DataGridView[] Export_GRD = { Grdrec_rem };
                MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
                // Inquery_aml_Remittances_Load(null,null);
            }
            else
            {
                DataTable ExpDt = Q_DT.DefaultView.ToTable(false, "rem_no", "S_name", "R_amount", "R_eCur_name", "PR_eCUR_name", "t_ecity_name", "s_ecity_name", "r_name", "eCASE_NA", "Case_Date", "C_DATE", "User_name", "S_phone", "s_E_NAT_NAME", "Sa_ECITY_NAME", "sbirth_date", "SBirth_Place", "s_job", "s_details_job", "S_address", "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_issue", "Source_money", "R_phone", "R_E_NAT_NAME", "r_ECITY_NAME", "rbirth_date", "TBirth_Place", "r_job", "r_details_job", "r_address", "RFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_issue", "s_notes", "r_notes", "Relation_S_R", "T_purpose").Select().CopyToDataTable();
                DataTable[] _EXP_DT = { ExpDt };
                DataGridView[] Export_GRD = { Grdrec_rem };
                MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
                // Inquery_aml_Remittances_Load(null,null);
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void txt_purpose_rec_TextChanged(object sender, EventArgs e)
        {

        }

        //private void Grdrec_rem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{

        //}

        
    }
}