﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Per_Info : Form
    {
        #region Defintion
        bool perchange = false;
        //bool CurChange = false;
        //bool AccChange = false;
        BindingSource BS_per_name = new BindingSource();
        BindingSource BS_SALE = new BindingSource();
        BindingSource BS_BUY = new BindingSource();
        BindingSource BS_REM_OUT = new BindingSource();
        BindingSource BS_REM_IN = new BindingSource();
        DataGridView Date_Grd_per_name = new DataGridView();
        string Sql_Text1 = "";

        //string frm_date = "";
        //string to_date = "";
        //string t_id_new = "";

        DataTable Aml_cust_count_buy = new DataTable();
        DataTable Aml_cust_count_sale = new DataTable();
        DataTable Aml_cust_count_out = new DataTable();
        DataTable Aml_cust_count_in = new DataTable();
        DataTable Aml_cust_count_oper = new DataTable();

        
       

        #endregion
        //---------------------------
        public Reveal_Per_Info()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

          

            Grd_buy.AutoGenerateColumns = false;
            Grd_sale.AutoGenerateColumns = false;
            Grd_rem_out.AutoGenerateColumns = false;
            Grd_rem_in.AutoGenerateColumns = false;
            #region Arabic/English
            if (connection.Lang_id != 1)
            {
              Column5.DataPropertyName = "EOPER_NAME";
                Column3.DataPropertyName = "Cur_ENAME";
                Column10.DataPropertyName = "EOPER_NAME";
                Column8.DataPropertyName = "Cur_ENAME";
                Column13.DataPropertyName = "R_ECUR_NAME";
                Column19.DataPropertyName = "R_ECUR_NAME";

                Column15.DataPropertyName = "EOPER_NAME";
                Column16.DataPropertyName = "EOPER_NAME";




            }
            #endregion
        }
        //---------------------------
        private void gettable()
        {
            string[] Column3 = { "Per_ID", "AOPER_NAME", "EOPER_NAME", "count_OPERATIONS", "Cur_ANAME", "Cur_ENAME", "for_amount", "loc_amount" };
            string[] DType3 = { "System.Int32", "System.String","System.String", "System.Int32", "System.String","System.String", "System.Int32", "System.Int32" };

            Aml_cust_count_oper = CustomControls.Custom_DataTable("Aml_cust_count_oper", Column3, DType3);

 
        
        }
        private void Reveal_Acc_Main2_Load(object sender, EventArgs e)
        {
            perchange = false;
            if (connection.Lang_id !=1)
            {
                Btn_Pdf.Text = "Print";
            }

            BS_per_name.DataSource = connection.SQLDS.Tables["search_reveal_person_info_tbl4"];
            Grd_per_name.DataSource = BS_per_name;

            if (connection.SQLDS.Tables["search_reveal_person_info_tbl4"].Rows.Count > 0)
            {
                perchange = true;
                Grd_per_name_SelectionChanged(null, null);
            }
        }
      
        //---------------------------
       
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Reveal_Acc_Main2_FormClosed(object sender, FormClosedEventArgs e)
        {
          perchange = false;
           
            string[] Used_Tbl = { "search_reveal_person_info_tbl", "search_reveal_person_info_tbl1", 
                                     "search_reveal_person_info_tbl2", "search_reveal_person_info_tbl3", 
                                     "search_reveal_person_info_tbl4","search_reveal_person_info_tbl5" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
       

        private void Grd_per_name_SelectionChanged(object sender, EventArgs e)
        {
 

            if (perchange)
            {
                Int64 per_id = 0;
                per_id = Convert.ToInt64(((DataRowView)BS_per_name.Current).Row["per_id"]);

                try
                {
                    Aml_cust_count_buy = new DataTable();
                    Aml_cust_count_buy = connection.SQLDS.Tables["search_reveal_person_info_tbl"].Select("per_id = " + per_id).CopyToDataTable();
                    BS_BUY.DataSource = Aml_cust_count_buy;
                   Grd_buy.DataSource = BS_BUY;
                }
             
                catch { Grd_buy.DataSource = new BindingSource(); }

                try{
                    Aml_cust_count_sale = new DataTable();
                    Aml_cust_count_sale = connection.SQLDS.Tables["search_reveal_person_info_tbl1"].Select("per_id = " + per_id).CopyToDataTable();
                    BS_SALE.DataSource = Aml_cust_count_sale;
                    Grd_sale.DataSource = BS_SALE;
                }

               
                catch { Grd_sale.DataSource = new BindingSource(); }

                try{
                    Aml_cust_count_out = new DataTable();
                    Aml_cust_count_out = connection.SQLDS.Tables["search_reveal_person_info_tbl2"].Select("per_id = " + per_id).CopyToDataTable();
                    BS_REM_OUT.DataSource = Aml_cust_count_out;
                    Grd_rem_out.DataSource = BS_REM_OUT;

                }

               

                catch { Grd_rem_out.DataSource = new BindingSource(); }

                try
                {
                    Aml_cust_count_in = new DataTable();
                    Aml_cust_count_in = connection.SQLDS.Tables["search_reveal_person_info_tbl3"].Select("per_id = " + per_id).CopyToDataTable();
                    BS_REM_IN.DataSource = Aml_cust_count_in;
                    Grd_rem_in.DataSource = BS_REM_IN;

                }

              
                catch { Grd_rem_in.DataSource = new BindingSource(); }
            }

        }


        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {
           
                Date_Grd_per_name = new DataGridView();
                Date_Grd_per_name.Columns.Add("Column1", connection.Lang_id == 1 ? "رمزالزبون" : "Person ID:");
                Date_Grd_per_name.Columns.Add("Column2", connection.Lang_id == 1 ? " اسم الزبون عربي" : "Person Name AR ");
                Date_Grd_per_name.Columns.Add("Column3", connection.Lang_id == 1 ? " اسم الزبون اجنبي" : "Person Name EN ");

            

                Int64 person_id = 0;
                person_id = Convert.ToInt64(((DataRowView)BS_per_name.Current).Row["per_id"]);

                DataTable Dt_buy = new DataTable();
                DataTable Dt_buy_exp = new DataTable();
                DataTable Dt_sale = new DataTable();
                DataTable Dt_sale_exp = new DataTable();
                DataTable Dt_rem_out = new DataTable();
                DataTable Dt_rem_out_exp = new DataTable();
                DataTable Dt_rem_in = new DataTable();
                DataTable Dt_rem_in_exp = new DataTable();
                DataTable dt_per_name = new DataTable();
               
                 dt_per_name = connection.SQLDS.Tables["search_reveal_person_info_tbl4"].DefaultView.ToTable(false, "per_id",  "per_aname" , "per_ename").Select(" per_id = " + person_id).CopyToDataTable();
              try
                 {
                if (connection.SQLDS.Tables["search_reveal_person_info_tbl"].Rows.Count > 0)
                {
                    Dt_buy = connection.SQLDS.Tables["search_reveal_person_info_tbl"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount", "per_id").Select("per_id = " + person_id).CopyToDataTable();
                    Dt_buy_exp = Dt_buy.DefaultView.ToTable(false,"per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                }
                 }
               catch{}
              try
              {
                if (connection.SQLDS.Tables["search_reveal_person_info_tbl1"].Rows.Count > 0)
                {

                    Dt_sale = connection.SQLDS.Tables["search_reveal_person_info_tbl1"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount", "per_id").Select("per_id = " + person_id).CopyToDataTable();
                    Dt_sale_exp = Dt_sale.DefaultView.ToTable(false,"per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();

                }
              }
            catch{}

            try{
                if (connection.SQLDS.Tables["search_reveal_person_info_tbl2"].Rows.Count > 0)
                {

                    Dt_rem_out = connection.SQLDS.Tables["search_reveal_person_info_tbl2"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount", "per_id").Select("per_id = " + person_id).CopyToDataTable();
                    Dt_rem_out_exp = Dt_rem_out.DefaultView.ToTable(false,"per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                }
            }
            catch{}


            try
            {
                if (connection.SQLDS.Tables["search_reveal_person_info_tbl3"].Rows.Count > 0)
                {

                    Dt_rem_in = connection.SQLDS.Tables["search_reveal_person_info_tbl3"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount", "per_id").Select("per_id = " + person_id).CopyToDataTable();
                    Dt_rem_in_exp = Dt_rem_in.DefaultView.ToTable(false,"per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();

                }
            }
            catch{}

                DataGridView[] Export_GRD = { Date_Grd_per_name, Grd_buy, Grd_sale, Grd_rem_out, Grd_rem_in };
                DataTable[] Export_DT = { dt_per_name, Dt_buy_exp, Dt_sale_exp, Dt_rem_out_exp, Dt_rem_in_exp };

                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        
    
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
           // perchange = false;
            try
            {
                DataRowView DRV = BS_per_name.Current as DataRowView;
                DataRow DR = DRV.Row;
                Int64 Per_ID = DR.Field<Int64>("Per_ID");



                details_Reveal_Per_Info HstFrm = new details_Reveal_Per_Info(Per_ID);
                this.Visible = false;
                HstFrm.ShowDialog(this);
                this.Visible = true;
            }
            catch
            { }
        }

        private void Btn_Export_all_Click(object sender, EventArgs e)
        {
            try
            {
                //DataGridView Date_Grd_per_name = new DataGridView();
                //Date_Grd_per_name.Columns.Add("Column1", connection.Lang_id == 1 ? "رمزالزبون" : "Person ID");
                //Date_Grd_per_name.Columns.Add("Column2", connection.Lang_id == 1 ? " اسم الزبون عربي" : "Person Name AR ");
                //Date_Grd_per_name.Columns.Add("Column3", connection.Lang_id == 1 ? " اسم الزبون اجنبي" : "Person Name EN ");
                             
                DataTable Dt_buy_exp = new DataTable();
                DataTable Dt_sale_exp = new DataTable();
                DataTable Dt_rem_out_exp = new DataTable();
                DataTable Dt_rem_in_exp = new DataTable();
                DataTable dt_per_name = new DataTable();

                dt_per_name = connection.SQLDS.Tables["search_reveal_person_info_tbl4"].DefaultView.ToTable(false, "per_id",  "per_aname" , "per_ename").Select().CopyToDataTable();
               
                if (connection.SQLDS.Tables["search_reveal_person_info_tbl"].Rows.Count > 0)
                {

                    Dt_buy_exp = connection.SQLDS.Tables["search_reveal_person_info_tbl"].DefaultView.ToTable(false, "per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                }

                if (connection.SQLDS.Tables["search_reveal_person_info_tbl1"].Rows.Count > 0)
                {


                    Dt_sale_exp = connection.SQLDS.Tables["search_reveal_person_info_tbl1"].DefaultView.ToTable(false, "per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();

                }

                if (connection.SQLDS.Tables["search_reveal_person_info_tbl2"].Rows.Count > 0)
                {


                    Dt_rem_out_exp = connection.SQLDS.Tables["search_reveal_person_info_tbl2"].DefaultView.ToTable(false, "per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                }


                if (connection.SQLDS.Tables["search_reveal_person_info_tbl3"].Rows.Count > 0)
                {


                    Dt_rem_in_exp = connection.SQLDS.Tables["search_reveal_person_info_tbl3"].DefaultView.ToTable(false, "per_id", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();

                }

                DataGridView[] Export_GRD = { Grd_per_name, Grd_buy, Grd_sale, Grd_rem_out, Grd_rem_in };
                DataTable[] Export_DT = { dt_per_name, Dt_buy_exp, Dt_sale_exp, Dt_rem_out_exp, Dt_rem_in_exp };

                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            catch
            { }
        }

        private void Btn_Pdf_Click(object sender, EventArgs e)
        {

            gettable();
            string person_info = ((DataRowView)BS_per_name.Current).Row["per_aname"].ToString();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Person_Info", person_info);
            Aml_cust_count_buy.TableName = "Aml_cust_count_buy";
            Aml_cust_count_sale.TableName = "Aml_cust_count_sale";
            Aml_cust_count_out.TableName = "Aml_cust_count_out";
            Aml_cust_count_in.TableName = "Aml_cust_count_in";

            try
            {
                for (int i = 0; i < Aml_cust_count_buy.Rows.Count; i++)
                {
                    DataRow DRow = Aml_cust_count_oper.NewRow();
                    DRow["per_id"] = Aml_cust_count_buy.Rows[i]["per_id"];
                    //if (connection.Lang_id != 1)
                    //{
                        DRow["EOPER_NAME"] = Aml_cust_count_buy.Rows[i]["EOPER_NAME"];
                    //}
                    //else
                    DRow["AOPER_NAME"] = Aml_cust_count_buy.Rows[i]["AOPER_NAME"];
                    DRow["count_OPERATIONS"] = Aml_cust_count_buy.Rows[i]["count_OPERATIONS"];
                    //if (connection.Lang_id != 1)
                    //{
                        DRow["Cur_ENAME"] = Aml_cust_count_buy.Rows[i]["Cur_ENAME"];
                    //}
                    //else
                    DRow["Cur_ANAME"] = Aml_cust_count_buy.Rows[i]["Cur_ANAME"];
                    DRow["for_amount"] = Aml_cust_count_buy.Rows[i]["for_amount"];
                    DRow["loc_amount"] = Aml_cust_count_buy.Rows[i]["loc_amount"];
                    Aml_cust_count_oper.Rows.Add(DRow);
                }
            }
            catch
            { }
            try
            {
                for (int i = 0; i < Aml_cust_count_sale.Rows.Count; i++)
                //for (int i = Aml_cust_count_buy.Rows.Count + 1; i < Aml_cust_count_sale.Rows.Count; i++)
                {
                    DataRow DRow = Aml_cust_count_oper.NewRow();
                    DRow["per_id"] = Aml_cust_count_sale.Rows[i]["per_id"];
                    //if (connection.Lang_id != 1)
                    //{
                        DRow["EOPER_NAME"] = Aml_cust_count_sale.Rows[i]["EOPER_NAME"];
                    //}
                    //else
                    DRow["AOPER_NAME"] = Aml_cust_count_sale.Rows[i]["AOPER_NAME"];
                    DRow["count_OPERATIONS"] = Aml_cust_count_sale.Rows[i]["count_OPERATIONS"];
                    //if (connection.Lang_id != 1)
                    //{
                        DRow["Cur_ENAME"] = Aml_cust_count_sale.Rows[i]["Cur_ENAME"];
                    //}
                    //else
                    DRow["Cur_ANAME"] = Aml_cust_count_sale.Rows[i]["Cur_ANAME"];
                    DRow["for_amount"] = Aml_cust_count_sale.Rows[i]["for_amount"];
                    DRow["loc_amount"] = Aml_cust_count_sale.Rows[i]["loc_amount"];
                    Aml_cust_count_oper.Rows.Add(DRow);
                }
            }
            catch
            { }
            for (int i =0; i < Aml_cust_count_out.Rows.Count; i++)
            {
                DataRow DRow = Aml_cust_count_oper.NewRow();
                DRow["per_id"] = Aml_cust_count_out.Rows[i]["per_id"];
                //if (connection.Lang_id != 1)
                //{
                    DRow["EOPER_NAME"] = Aml_cust_count_out.Rows[i]["EOPER_NAME"];
                //}
                //else
                DRow["AOPER_NAME"] = Aml_cust_count_out.Rows[i]["AOPER_NAME"];
                DRow["count_OPERATIONS"] = Aml_cust_count_out.Rows[i]["count_OPERATIONS"];
                //if (connection.Lang_id != 1)
                //{
                    DRow["Cur_ENAME"] = Aml_cust_count_out.Rows[i]["Cur_ENAME"];
                //}
                //else
                DRow["Cur_ANAME"] = Aml_cust_count_out.Rows[i]["Cur_ANAME"];
                DRow["for_amount"] = Aml_cust_count_out.Rows[i]["for_amount"];
                DRow["loc_amount"] = Aml_cust_count_out.Rows[i]["loc_amount"];
                Aml_cust_count_oper.Rows.Add(DRow);
            }
            for (int i = 0; i < Aml_cust_count_in.Rows.Count; i++)
            {
                DataRow DRow = Aml_cust_count_oper.NewRow();
                DRow["per_id"] = Aml_cust_count_in.Rows[i]["per_id"];
                //if (connection.Lang_id != 1)
                //{
                    DRow["EOPER_NAME"] = Aml_cust_count_in.Rows[i]["EOPER_NAME"];
                //}
                //else
                DRow["AOPER_NAME"] = Aml_cust_count_in.Rows[i]["AOPER_NAME"];
                DRow["count_OPERATIONS"] = Aml_cust_count_in.Rows[i]["count_OPERATIONS"];
                //if (connection.Lang_id != 1)
                //{
                    DRow["Cur_ENAME"] = Aml_cust_count_in.Rows[i]["Cur_ENAME"];
                //}
                //else
                DRow["Cur_ANAME"] = Aml_cust_count_in.Rows[i]["Cur_ANAME"];
                DRow["for_amount"] = Aml_cust_count_in.Rows[i]["for_amount"];
                DRow["loc_amount"] = Aml_cust_count_in.Rows[i]["loc_amount"];
                Aml_cust_count_oper.Rows.Add(DRow);
            }
            


            //connection.SQLDS.Tables.Add(Aml_cust_count_buy);
            //connection.SQLDS.Tables.Add(Aml_cust_count_sale);
            //connection.SQLDS.Tables.Add(Aml_cust_count_out);
            connection.SQLDS.Tables.Add(Aml_cust_count_oper);

            Reveal_per_info_Ar ObjRpt = new Reveal_per_info_Ar();
            Reveal_Per_Info_eng ObjRptEng = new Reveal_Per_Info_eng();

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;



            string[] Used_Tbl = { "Aml_cust_count_oper" };
                foreach (string Tbl in Used_Tbl)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                    {
                        connection.SQLDS.Tables.Remove(Tbl);
                    }
                }

               
            

        }

        private void Grd_rem_in_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}