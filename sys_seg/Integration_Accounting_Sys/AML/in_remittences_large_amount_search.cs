﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class in_remittences_large_amount_search : Form
    {
        #region MyRegion
        string @format = "dd/MM/yyyy";
        string @format_null = " ";
        string SqlTxt = "";
        DataTable cur_tbl = new DataTable();
        DataTable coun_Cit_tbl = new DataTable();
        DataTable s_coun_tbl = new DataTable();
        DataTable r_coun_tbl = new DataTable();
        DataTable GRD1_tbl = new DataTable();
        DataTable GRD2_tbl = new DataTable();
        DataTable term_tbl = new DataTable();
        BindingSource cur_bs = new BindingSource();
        BindingSource r_coun = new BindingSource();
        BindingSource res_BS = new BindingSource();
        BindingSource det_BS = new BindingSource();
        BindingSource s_coun = new BindingSource();
        DataGridView Grd_Result_1 = new DataGridView();
        DataGridView Grd_Result_2 = new DataGridView();
        DataGridView Grd_Result_3 = new DataGridView();
        DataGridView Grd_Result_4 = new DataGridView();
        DataGridView Grd_Result_5 = new DataGridView();
        DataGridView Grd_Result_6 = new DataGridView();
        DataGridView Grd_Result_7 = new DataGridView();
        Boolean Change = false;
        DataTable Dt = new DataTable();
        DataTable Dt1 = new DataTable();
        DataTable Rpt_dt = new DataTable();
        string r_name = "";
        DataTable main_tbl = new DataTable();
        DataTable main_tbl2 = new DataTable();
        string number = "";
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        #endregion


        public in_remittences_large_amount_search()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_result.AutoGenerateColumns = false;
            Grd_details.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
              //  Column25.DataPropertyName = "rFRM_EDOC_NA";
               // Column27.DataPropertyName = "s_ECITY_NAME";
                Column5.DataPropertyName = "R_ECUR_NAME";
                Column7.DataPropertyName = "Case_purpose_Ename";
                Column10.DataPropertyName = "rFRM_EDOC_NA";
                Column18.DataPropertyName = "r_E_NAT_NAME";
                Column19.DataPropertyName = "r_ECOUN_NAME";
                Column20.DataPropertyName = "r_ECITY_NAME";
                Column24.DataPropertyName = "S_ECOUN_NAME";
                Column28.DataPropertyName = "s_ECITY_NAME";
                Column29.DataPropertyName = "ECASE_NA";
            }
        }

        private void in_remittences_large_amount_search_Load(object sender, EventArgs e)
        {

            Txt_To.CustomFormat = " ";
            Txt_From.CustomFormat = " ";
            Txt_top_sec.Text = "0";
            if (connection.Lang_id == 2)
            {
                Cmb_no.Items[0] = "Smaller than";
                Cmb_no.Items[1] = "Smaller or equal to";
                Cmb_no.Items[2] = "Greater than";
                Cmb_no.Items[3] = "Greater or equal";
                Cmb_no.Items[4] = "Equal";
            }
            connection.SqlExec("Exec get_large_amount", "tbl");
            try
            {
                coun_Cit_tbl = connection.SQLDS.Tables["tbl"];
                CMB_cit_coun.DataSource = coun_Cit_tbl;
                CMB_cit_coun.DisplayMember = connection.Lang_id == 1 ? "ACIT_COUN_name" : "ECIT_COUN_name";
                CMB_cit_coun.ValueMember = "cit_coun_id";
                CMB_cit_coun.SelectedValue = 0;
               
            }
            catch { }
            

           cur_tbl= connection.SQLDS.Tables["tbl1"];
            CMB_CUR.DataSource = cur_tbl;
            CMB_CUR.ValueMember = "cur_id";
            CMB_CUR.DisplayMember = connection.Lang_id == 1 ? "Acur_name" : "Ecur_name";
            cur_bs.DataSource = cur_tbl;

            s_coun_tbl = connection.SQLDS.Tables["tbl2"];
            s_coun.DataSource = s_coun_tbl;
            r_coun_tbl = connection.SQLDS.Tables["tbl3"];
            r_coun.DataSource = r_coun_tbl;



            Cmb_no.SelectedIndex = 0;

            term_tbl = connection.SQLDS.Tables["tbl7"];
            cbo_term.DataSource = term_tbl;
            cbo_term.ValueMember = "term_id";
            cbo_term.DisplayMember = connection.Lang_id == 1 ? "Term_name" : "ETerm_name";

       

        }
        //---------------------------البحث
        private void button1_Click(object sender, EventArgs e)
        {
            string[] Used_Tbl = { "rem_tbl", "rem_tbl1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

             #region Validation
            string Ord_Strt1 =Txt_From.Text;
            if (Ord_Strt1 == " ")
            {
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "please enter the start date of search" : "الرجاء إدخال بداية مدة البحث", MyGeneral_Lib.LblCap);
                    Txt_From.Focus();
                    return;
                }
            }
            string Ord_Strt = Txt_To.Text;
            if (Ord_Strt == " ")
            {

                {
                    MessageBox.Show(connection.Lang_id == 2 ? "please enter the end date of search" : "الرجاء إدخال نهاية مدة البحث", MyGeneral_Lib.LblCap);
                    Txt_To.Focus();
                    return;

                }
            }
         

            DateTime date = Convert.ToDateTime(DateTime.Now.ToString("d"));
            DateTime text_from = Convert.ToDateTime(Txt_From.Value.Date.ToString("d"));
            if (text_from > date)
            {

                {
                    MessageBox.Show(connection.Lang_id == 2 ? "the date is higher thean current date" : " التاريخ أعلى من التاريخ الحالي", MyGeneral_Lib.LblCap);
                    Txt_From.Focus();
                    return;

                }
            }
            if ((Convert.ToInt32(Txt_top_sec.Text) != 0) && (Cmb_no.SelectedIndex == -1))
            {

                MessageBox.Show(connection.Lang_id == 2 ? "please choose the compareter operand with the enterd value" : "الرجاء إختيار شرط المقارنة نسبة للرقم المكتوب", MyGeneral_Lib.LblCap);
                Cmb_no.Focus();
                return;

            }

            DateTime text_to = Convert.ToDateTime(Txt_To.Value.Date.ToString("d"));
            if (text_to > date)
            {

                {
                    MessageBox.Show(connection.Lang_id == 2 ? "the date is higher thean current date" : " التاريخ أعلى من التاريخ الحالي", MyGeneral_Lib.LblCap);
                    Txt_To.Focus();
                    return;

                }
            }

            connection.SQLDS.Tables.Clear();
             #endregion
            //--------------------------------------

            res_BS= new BindingSource();
            det_BS= new BindingSource();
            if (checkBox_sender_cit.Checked == false)
            {
                Grd_result.Columns[5].Visible=false;
            
            }
            else
                Grd_result.Columns[5].Visible =true;

            if (checkBox_doc_type.Checked == false)
            {
                Grd_result.Columns[3].Visible = false;

            }
            else
                Grd_result.Columns[3].Visible = true;

            if (checkBox_doc_no.Checked == false)
            {
                Grd_result.Columns[4].Visible = false;

            }
            else
                Grd_result.Columns[4].Visible = true;

            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == false)
            {
                Grd_result.Columns[0].Width = 400;
                Grd_result.Columns[1].Width = 150;
                Grd_result.Columns[2].Width = 400;

            }
            else
            {
                Grd_result.Columns[0].Width = 300;
                Grd_result.Columns[1].Width = 150;
                Grd_result.Columns[2].Width = 200;
            }
            if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == false)
            {
                Grd_result.Columns[0].Width = 400;
                Grd_result.Columns[1].Width = 150;
                Grd_result.Columns[2].Width = 200;
                Grd_result.Columns[5].Width = 200;
            }
            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == false)
            {
                Grd_result.Columns[0].Width = 400;
                Grd_result.Columns[1].Width = 150;
                Grd_result.Columns[2].Width = 200;
                Grd_result.Columns[3].Width = 200;
            }
            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == true)
            {
                Grd_result.Columns[0].Width = 400;
                Grd_result.Columns[1].Width = 150;
                Grd_result.Columns[2].Width = 200;
                Grd_result.Columns[4].Width = 200;
            }

            try
            {
                Cmb_no_SelectedIndexChanged(null, null);
                main_tbl = new DataTable();
                main_tbl2 = new DataTable();
                Rpt_dt = new DataTable();
                Grd_details.DataSource = new BindingSource();
                Grd_result.DataSource = new BindingSource();
                string from_Date = Txt_From.Value.ToString("yyyy/MM/dd");
                string to_Date = Txt_To.Value.ToString("yyyy/MM/dd");
                Int16 cit = Convert.ToInt16(((DataRowView)s_coun.Current).Row["city_id"]);
                Int16 cit_ = Convert.ToInt16(CMB_cit_coun.SelectedValue);
                Int16 cur = Convert.ToInt16(CMB_CUR.SelectedValue);
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "LV_In_OUT_comming_Rem";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Sa_city_id", (((DataRowView)s_coun.Current).Row["city_id"]));
                connection.SQLCMD.Parameters.AddWithValue("@r_city_id", Convert.ToInt16(CMB_cit_coun.SelectedValue));
                connection.SQLCMD.Parameters.AddWithValue("@r_Cur_Id", Convert.ToInt16(CMB_CUR.SelectedValue));
                connection.SQLCMD.Parameters.AddWithValue("@from_nrec_date", from_Date);
                connection.SQLCMD.Parameters.AddWithValue("@to_nrec_date", to_Date);
                connection.SQLCMD.Parameters.AddWithValue("@top", Txt_top_first.Text);
                connection.SQLCMD.Parameters.AddWithValue("@in_rem_loc_amount", (Convert.ToInt64(Txt_top_sec.Text)));
                connection.SQLCMD.Parameters.AddWithValue("@out_rem_loc_amount", 0);
                connection.SQLCMD.Parameters.AddWithValue("@vale", number);
                connection.SQLCMD.Parameters.AddWithValue("@chk_cancel", checkBox_Cancel.Checked == true ? 1 : 0);
                connection.SQLCMD.Parameters.AddWithValue("@chk_r_doc_no", checkBox_doc_no.Checked == true ? 1 : 0);
                connection.SQLCMD.Parameters.AddWithValue("@chk_s_doc_no", 0);
                connection.SQLCMD.Parameters.AddWithValue("@chk_FRM_ADOC_NA", checkBox_doc_type.Checked == true ? 1 : 0);
                connection.SQLCMD.Parameters.AddWithValue("@chk_sFRM_ADOC_NA", 0);
                connection.SQLCMD.Parameters.AddWithValue("@chk_s_ACITY_NAME", checkBox_sender_cit.Checked == true ? 1 : 0);
                connection.SQLCMD.Parameters.AddWithValue("@chk_r_ACITY_NAME", 0);
                connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 1);
                connection.SQLCMD.Parameters.AddWithValue("@s_cust_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@d_cust_id", Convert.ToInt16(cbo_term.SelectedValue));
                connection.SQLCMD.Parameters.AddWithValue("@lang_id", connection.Lang_id);

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "rem_tbl");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "rem_tbl1");
                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();

                main_tbl2 = connection.SQLDS.Tables["rem_tbl1"];
                main_tbl = connection.SQLDS.Tables["rem_tbl"];

                if (main_tbl2.Rows.Count > 0)
                {
                    if (connection.Lang_id == 2)
                    {
                        Grd_result.Columns["Column25"].DataPropertyName = "rFRM_EDOC_NA";
                        Grd_result.Columns["Column27"].DataPropertyName = "s_ECITY_NAME";
                    }

                    res_BS.DataSource = main_tbl2;
                    Grd_result.DataSource = res_BS;

                    //LblRec.Text = connection.Records(res_BS);
                    //LblRec.Text = connection.Records(det_BS);
                    Change = true;
                    Grd_result_SelectionChanged(null, null);

                }
                else
                { MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد حوالات تطابق الشروط" : "No Remittences matched the condition", MyGeneral_Lib.LblCap); }
            }
            catch { }
         
        }
        //---------------------------تغير الجرد الثانية عند تغيير الاولى
        private void Grd_result_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                try
                {
                    string r_name = ((DataRowView)res_BS.Current).Row["r_name"].ToString();
                    try
                    {
                        Rpt_dt = new DataTable();
                        Rpt_dt = main_tbl.DefaultView.ToTable().Select("r_name = '" + r_name + "'").CopyToDataTable();
                    }
                    catch
                    {
                    }

                    det_BS.DataSource = Rpt_dt;
                    Grd_details.DataSource = det_BS;
                }
                catch
                { Grd_details.DataSource = new DataTable(); }
            }
          //  LblRec.Text = connection.Records(det_BS);
        }
        private void Get_Grd()
        {
            Grd_Result_1.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_1.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_1.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");

            Grd_Result_2.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_2.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_2.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
            Grd_Result_2.Columns.Add("Column4", connection.Lang_id == 1 ? "مدينة المرسـل" : "Sender City");

            Grd_Result_3.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_3.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_3.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
            Grd_Result_3.Columns.Add("Column4", connection.Lang_id == 1 ? " نوع الوثيقة" : "Doc. Type");

            Grd_Result_4.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_4.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_4.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
            Grd_Result_4.Columns.Add("Column4", connection.Lang_id == 1 ? " رقم الوثيقة" : "Doc. No");

            Grd_Result_5.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_5.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_5.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
            Grd_Result_5.Columns.Add("Column4", connection.Lang_id == 1 ? " نوع الوثيقة" : "Doc. Type");
            Grd_Result_5.Columns.Add("Column5", connection.Lang_id == 1 ? "مدينة المرسـل" : "Sender City");

            Grd_Result_6.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_6.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_6.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
            Grd_Result_6.Columns.Add("Column4", connection.Lang_id == 1 ? " رقم الوثيقة" : "Doc. No");
            Grd_Result_6.Columns.Add("Column5", connection.Lang_id == 1 ? "مدينة المرسـل" : "Sender City");

            Grd_Result_7.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
            Grd_Result_7.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
            Grd_Result_7.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
            Grd_Result_7.Columns.Add("Column4", connection.Lang_id == 1 ? " نوع الوثيقة" : "Doc. Type");
            Grd_Result_7.Columns.Add("Column5", connection.Lang_id == 1 ? " رقم الوثيقة" : "Doc. No");


        }
        //-----------------------------تصدير إجمالي
        private void button3_Click(object sender, EventArgs e)
        {
            if (Grd_result.RowCount > 0)
            {
                try
                {
                    Date();
                    DataTable Dt = new DataTable();

                    Dt = connection.SQLDS.Tables["rem_tbl1"];

                    // Rpt_dt = connection.SQLDS.Tables["rem_tbl1"];
                    if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == true)
                    {
                        DataGridView[] Export_GRD = { Date_Grd ,Grd_result };
                        DataTable[] Export_DT = { Date_Tbl ,connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount",
           connection.Lang_id==1?"rFRM_ADOC_NA" : "rFRM_EDOC_NA","r_doc_no",connection.Lang_id==1? "s_ACITY_NAME" : "s_ECITY_NAME").Select().CopyToDataTable()};
                        MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                    }
                    if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == false)
                    {
                        if (Grd_Result_1.ColumnCount < 3)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd , Grd_Result_1 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ).Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_1 };
                            DataTable[] Export_DT = { Date_Tbl ,connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ).Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                    if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == false)
                    {
                        if (Grd_Result_2.ColumnCount < 4)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_2 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1?  "s_ACITY_NAME" :"s_ECITY_NAME").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_2 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1?  "s_ACITY_NAME" :"s_ECITY_NAME").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                    if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == false)
                    {
                        if (Grd_Result_3.ColumnCount < 4)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd, Grd_Result_3 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1? "rFRM_ADOC_NA" :"rFRM_EDOC_NA").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd, Grd_Result_3 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1? "rFRM_ADOC_NA" :"rFRM_EDOC_NA").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                    if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == true)
                    {
                        if (Grd_Result_4.ColumnCount < 4)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_4 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,"r_doc_no").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_4 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,"r_doc_no").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                    if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == false)
                    {
                        if (Grd_Result_5.ColumnCount < 5)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_5 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1?"rFRM_ADOC_NA":"rFRM_EDOC_NA",connection.Lang_id==1?"s_ACITY_NAME" : "s_ACITY_NAME").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd, Grd_Result_5 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1?"rFRM_ADOC_NA":"rFRM_EDOC_NA",connection.Lang_id==1?"s_ACITY_NAME" : "s_ACITY_NAME").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                    if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == true)
                    {
                        if (Grd_Result_6.ColumnCount < 5)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd,  Grd_Result_6 };
                            DataTable[] Export_DT = {  Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,"r_doc_no",connection.Lang_id==1?"s_ACITY_NAME" : "s_ECITY_NAME").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd, Grd_Result_6 };
                            DataTable[] Export_DT = { Date_Tbl ,connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,"r_doc_no",connection.Lang_id==1?"s_ACITY_NAME" : "s_ECITY_NAME").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                    if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == true)
                    {
                        if (Grd_Result_7.ColumnCount < 5)
                        {
                            Get_Grd();
                            DataGridView[] Export_GRD = { Date_Grd, Grd_Result_7 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1?"rFRM_ADOC_NA":"rFRM_EDOC_NA","r_doc_no").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                        else
                        {
                            DataGridView[] Export_GRD = { Date_Grd , Grd_Result_7 };
                            DataTable[] Export_DT = { Date_Tbl , connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false ,"r_name","count","in_rem_loc_amount"
            ,connection.Lang_id==1?"rFRM_ADOC_NA":"rFRM_EDOC_NA","r_doc_no").Select().CopyToDataTable()};
                            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                        }
                    }
                }
                catch
                { }
            }
            else
            { MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للطباعة" : "No Records to print", MyGeneral_Lib.LblCap); }
        }
        //----------------------------تصدير تفصيلي
        private void button4_Click(object sender, EventArgs e)
        {
             if (Grd_result.RowCount > 0)
            {

                Date();
                 
            DataTable Dt = new DataTable();
            DataTable Dt_details = new DataTable();
            DataTable Rpt_dt_details = new DataTable();

            DataTable Export_DT_main = new DataTable();
            DataTable Export_DT_details = new DataTable();

            string r_name = ((DataRowView)res_BS.Current).Row["r_name"].ToString();
            Dt = connection.SQLDS.Tables["rem_tbl1"];
            Dt_details = connection.SQLDS.Tables["rem_tbl"];
            Rpt_dt_details = Rpt_dt;
            if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == true)
            {
                Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount",
                    connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                    connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Aname", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                    "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ACOUN_NAME", "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { Date_Grd , Grd_result, Grd_details };
                DataTable[] Export_DT = { Date_Tbl ,Export_DT_main, Export_DT_details };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == false)
            {
                if (Grd_Result_1.ColumnCount < 3)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                    ).Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_1, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                   ).Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_1, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == false)
            {
                if (Grd_Result_2.ColumnCount < 4)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                        , connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_2, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                      , connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_2, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == false)
            {

                if (Grd_Result_3.ColumnCount < 4)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                        , connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NA").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_3, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl,  Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                      , connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NA").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_3, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == true)
            {

                if (Grd_Result_4.ColumnCount < 4)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                    , "r_doc_no").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ACOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ACITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_4, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                  , "r_doc_no").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ACOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ACITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_4, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == false)
            {
                if (Grd_Result_5.ColumnCount < 5)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                        , connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_5, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                       , connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_5, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            if (checkBox_sender_cit.Checked == true && checkBox_doc_type.Checked == false && checkBox_doc_no.Checked == true)
            {
                if (Grd_Result_6.ColumnCount < 5)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                        , "r_doc_no", connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_6, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                       , "r_doc_no", connection.Lang_id == 1 ? "s_ACITY_NAME" : "s_ECITY_NAME").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_6, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            if (checkBox_sender_cit.Checked == false && checkBox_doc_type.Checked == true && checkBox_doc_no.Checked == true)
            {
                if (Grd_Result_7.ColumnCount < 5)
                {
                    Get_Grd();
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                        , connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_7, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    Export_DT_main = connection.SQLDS.Tables["rem_tbl1"].DefaultView.ToTable(false, "r_name", "count", "in_rem_loc_amount"
                       , connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no").Select("r_name = '" + r_name + "'").CopyToDataTable();

                    Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "Case_Date", "rem_no", "r_amount",
                        connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "in_rem_loc_amount", connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename", "s_name", "r_name", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_issue", "r_doc_ida",
                        "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", "r_address", "R_phone", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "r_job").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Grd_Result_7, Grd_details };
                    DataTable[] Export_DT = { Date_Tbl, Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
            }
            //DataGridView[] Export_GRD = { Grd_result, Grd_details };
            //DataTable[] Export_DT = { Export_DT_main, Export_DT_details };
            //MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

        }
 
            else
            { MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للطباعة" : "No Records to print", MyGeneral_Lib.LblCap); }
        }
        //----------------------------time picker(to)
        private void Txt_To_ValueChanged(object sender, EventArgs e)
        {
          
            if (Txt_To.Checked == true)
            {
                Txt_To.Format = DateTimePickerFormat.Custom;
                Txt_To.CustomFormat = @format;
            }
            else
            {
                Txt_To.Format = DateTimePickerFormat.Custom;
                Txt_To.CustomFormat = " ";
            }
        }
        //----------------------------time picker(from)
        private void Txt_From_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_From.Checked == true)
            {
                Txt_From.Format = DateTimePickerFormat.Custom;
                Txt_From.CustomFormat = @format;
            }
            else
            {
                Txt_From.Format = DateTimePickerFormat.Custom;
                Txt_From.CustomFormat = " ";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            main_tbl.Clear();
            main_tbl2 .Clear();
            Rpt_dt.Clear();
            Change = false;
            this.Close();
        }
        //--------------------------------------
        private void Grd_details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(det_BS);
        }
        //--------------------------------------
        private void Cmb_no_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_no.SelectedIndex == 0)
            {
                number = "<";
            }
            if (Cmb_no.SelectedIndex == 1)
            {
                number = "<=";
            }
            if (Cmb_no.SelectedIndex == 2)
            {
                number = ">";
            }
            if (Cmb_no.SelectedIndex == 3)
            {
                number = ">=";
            }
            if (Cmb_no.SelectedIndex == 4)
            {
                number = "=";
            }
            //if (Cmb_no.SelectedIndex == 5)
            //{
            //    number = "=";
            //}


        }
        //--------------------------------------
        private void in_remittences_large_amount_search_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "rem_tbl", "rem_tbl1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //--------------------------------------
        private void button2_Click_1(object sender, EventArgs e)
        {
            if (Grd_result.RowCount > 0)
            {
                DataGridView Rpt_IN_GRD = new DataGridView();
                Rpt_IN_GRD.Columns.Add("Column1", connection.Lang_id == 1 ? "اسم المستلــم" : "Receiver name");
                Rpt_IN_GRD.Columns.Add("Column2", connection.Lang_id == 1 ? "عدد الحوالات" : "Remittances Count");
                Rpt_IN_GRD.Columns.Add("Column3", connection.Lang_id == 1 ? "(مجموع المبالغ (ع.م" : "Total L.C");
                Rpt_IN_GRD.Columns.Add("Column4", connection.Lang_id == 1 ? "الفرع" : "Branch");
                Rpt_IN_GRD.Columns.Add("Column5", connection.Lang_id == 1 ? "تاريخ الحالة" : "Case Date");
                Rpt_IN_GRD.Columns.Add("Column6", connection.Lang_id == 1 ? "رقم الحوالة" : "Rem No.");
                Rpt_IN_GRD.Columns.Add("Column7", connection.Lang_id == 1 ? "المبلغ ع.ص" : "Amount (F.C.)");
                Rpt_IN_GRD.Columns.Add("Column8", connection.Lang_id == 1 ? "علمة الحوالة" : "Rem Currency");
                Rpt_IN_GRD.Columns.Add("Column8", connection.Lang_id == 1 ? "المبلغ ع.م" : "Amount (L.C.)");
                Rpt_IN_GRD.Columns.Add("Column9", connection.Lang_id == 1 ? "الغرض من التحويل" : "Rem. Purpose");
                Rpt_IN_GRD.Columns.Add("Column10", connection.Lang_id == 1 ? "اسم المرسل" : "Sender Name");
                Rpt_IN_GRD.Columns.Add("Column11", connection.Lang_id == 1 ? "نوع الوثيقة" : "Rec. ID type");
                Rpt_IN_GRD.Columns.Add("Column12", connection.Lang_id == 1 ? "رقم الوثيقة" : "Rec. ID No.");
                Rpt_IN_GRD.Columns.Add("Column13", connection.Lang_id == 1 ? "جهة الاصدار" : "doc_issue");

                Rpt_IN_GRD.Columns.Add("Column14", connection.Lang_id == 1 ? "تاريخ الوثيقة" : "Rec. ID Date");
                Rpt_IN_GRD.Columns.Add("Column15", connection.Lang_id == 1 ? "تاريخ انتهاء الوثيقة" : "Rec. Exp Date");
                Rpt_IN_GRD.Columns.Add("Column16", connection.Lang_id == 1 ? "الجنسية" : "Rec. Nationality");

                Rpt_IN_GRD.Columns.Add("Column17", connection.Lang_id == 1 ? "بلد المستلم" : "Rec. Country");
                Rpt_IN_GRD.Columns.Add("Column18", connection.Lang_id == 1 ? "مدينة المستلم" : "Rec. City");
                Rpt_IN_GRD.Columns.Add("Column19", connection.Lang_id == 1 ? " العنوان" : "Rec. Address");

                Rpt_IN_GRD.Columns.Add("Column20", connection.Lang_id == 1 ? " رقم هاتف المرسل" : "Sen. phone");
                Rpt_IN_GRD.Columns.Add("Column21", connection.Lang_id == 1 ? " رقم هاتف المستلم" : "Rec. phone");

                Rpt_IN_GRD.Columns.Add("Column22", connection.Lang_id == 1 ? "بلد المرسل" : "sen. Country");
                Rpt_IN_GRD.Columns.Add("Column23", connection.Lang_id == 1 ? " مدينة المرسل" : "sen. City");
                Rpt_IN_GRD.Columns.Add("Column24", connection.Lang_id == 1 ? " حالة الحوالة " : "Rem. Case");
                Rpt_IN_GRD.Columns.Add("Column25", connection.Lang_id == 1 ? " مهنة المرسل " : " Sender job");
                Rpt_IN_GRD.Columns.Add("Column26", connection.Lang_id == 1 ? " مهنة المستلم " : " rec. job");
                Rpt_IN_GRD.Columns.Add("Column27", connection.Lang_id == 1 ? " مصدر المال " : " source money ");
                Rpt_IN_GRD.Columns.Add("Column28", connection.Lang_id == 1 ? " علاقة المرسل بالمستلم " : "  relation S/R ");




                DataTable Rpt_IN_All = new DataTable();
                string[] Column =
            {
                        "receiver_name" , "No. of Rem" , "Total Amount (L.C.)" ,"Branch" ,"Case_Date" ,"Rem No." , "r_amount",
                        "currency" ,"in_rem_loc_amount","Case_purpose" , "s_name"  ,"rRM_ADOC_NA" , "Rec_ID_No" , "R_doc_issue"
                       , "r_doc_ida" , "r_doc_eda" ,"r_NAT_NAME" ,"r_COUN_NAME" ,"r_CITY_NAME","r_address","S_phone" ,"R_phone",
                        "S_COUN_NAME" , "s_CITY_NAME","ACASE_NA","r_job","S_job","Source_money","Relation_S_R"

            };

                string[] DType =
            {
                "System.String" ,"System.String",  "System.String",  "System.String",  "System.String",  "System.String",  "System.String",
                "System.String",  "System.String",  "System.String",  "System.String",  "System.String",  "System.String",  "System.String"
                , "System.String",  "System.String",  "System.String", "System.String", "System.String", "System.String", "System.String", 
                "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", 
            };


                Rpt_IN_All = CustomControls.Custom_DataTable("Rpt_IN_All", Column, DType);


                foreach (DataRow row in main_tbl2.Rows)
                {
                    DataRow DRow = Rpt_IN_All.NewRow();
                    DRow["receiver_name"] = row["r_name"];
                    DRow["No. of Rem"] = row["count"];
                    DRow["Total Amount (L.C.)"] = row["in_rem_loc_amount"];
                    DRow["Branch"] = "";
                    DRow["Case_Date"] = "";
                    DRow["Rem No."] = "";
                    DRow["r_amount"] = "";
                    DRow["currency"] = "";
                    DRow["in_rem_loc_amount"] = "";
                    DRow["Case_purpose"] = "";
                    DRow["s_name"] = "";
                    DRow["rRM_ADOC_NA"] = "";
                    DRow["Rec_ID_No"] = "";
                    DRow["R_doc_issue"] = "";
                    DRow["r_doc_ida"] = "";
                    DRow["r_doc_eda"] = "";
                    DRow["r_NAT_NAME"] = "";
                    DRow["r_COUN_NAME"] = "";
                    DRow["r_CITY_NAME"] = "";
                    DRow["r_address"] = "";
                    DRow["S_phone"] = "";
                    DRow["R_phone"] = "";
                    DRow["S_COUN_NAME"] = "";
                    DRow["s_CITY_NAME"] = "";
                    DRow["ACASE_NA"] = "";
                    DRow["r_job"] = "";
                    DRow["S_job"] = "";
                    DRow["Source_money"] = "";
                    DRow["Relation_S_R"] = "";



                    Rpt_IN_All.Rows.Add(DRow);
                    foreach (DataRow row1 in main_tbl.Rows)
                    {
                        DataRow DRow1 = Rpt_IN_All.NewRow();
                        string rr = row["r_name"].ToString();
                        string rr2 = row1["r_name"].ToString();
                        if (rr == rr2)
                        {
                            DRow1["receiver_name"] = "";
                            DRow1["No. of Rem"] = "";
                            DRow1["Total Amount (L.C.)"] = "";
                            DRow1["Branch"] = connection.Lang_id == 1 ? row1["D_Acust_name"] : row1["D_Ecust_name"];
                            DRow1["Case_Date"] = row1["Case_Date"];
                            DRow1["Rem No."] = row1["rem_no"];
                            DRow1["r_amount"] = row1["r_amount"];
                            DRow1["currency"] = connection.Lang_id == 1 ? row1["R_ACUR_NAME"] : row1["R_ECUR_NAME"];
                            DRow1["in_rem_loc_amount"] = row1["in_rem_loc_amount"];
                            DRow1["Case_purpose"] = connection.Lang_id == 1 ? row1["Case_purpose_Aname"] : row1["Case_purpose_Ename"];
                            DRow1["s_name"] = row1["s_name"];
                            DRow1["rRM_ADOC_NA"] = connection.Lang_id == 1 ? row1["rFRM_ADOC_NA"] : row1["rFRM_EDOC_NA"];
                            DRow1["Rec_ID_No"] = row1["r_doc_no"];
                            DRow1["R_doc_issue"] = row1["R_doc_issue"];
                            DRow1["r_doc_ida"] = row1["r_doc_ida"];
                            DRow1["r_doc_eda"] = row1["r_doc_eda"];
                            DRow1["r_NAT_NAME"] = connection.Lang_id == 1 ? row1["r_A_NAT_NAME"] : row1["r_E_NAT_NAME"];
                            DRow1["r_COUN_NAME"] = connection.Lang_id == 1 ? row1["r_ACOUN_NAME"] : row1["r_ECOUN_NAME"];
                            DRow1["r_CITY_NAME"] = connection.Lang_id == 1 ? row1["r_ACITY_NAME"] : row1["r_ECITY_NAME"];
                            DRow1["r_address"] = row1["r_address"];
                            DRow1["S_phone"] = row1["S_phone"];
                            DRow1["R_phone"] = row1["R_phone"];
                            DRow1["S_COUN_NAME"] = connection.Lang_id == 1 ? row1["S_ACOUN_NAME"] : row1["S_ECOUN_NAME"];
                            DRow1["s_CITY_NAME"] = connection.Lang_id == 1 ? row1["s_ACITY_NAME"] : row1["s_ECITY_NAME"];
                            DRow1["ACASE_NA"] = connection.Lang_id == 1 ? row1["ACASE_NA"] : row1["ECASE_NA"];
                            DRow1["r_job"] = row1["r_job"];
                            DRow1["S_job"] = row1["S_job"];
                            DRow1["Source_money"] = row1["Source_money"];
                            DRow1["Relation_S_R"] = row1["Relation_S_R"];

                            Rpt_IN_All.Rows.Add(DRow1);
                        }
                    }

                }
                Date();
                DataGridView[] Export_GRD = { Date_Grd, Rpt_IN_GRD };
                DataTable[] Export_DT = {Date_Tbl , Rpt_IN_All };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

            }
            else
            { MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للطباعة" : "No Records to print", MyGeneral_Lib.LblCap); }

        }
        //--------------------------------------
        private void Date()
        {

            Date_Grd = new DataGridView();
            Date_Tbl = new DataTable();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            Date_Tbl.Rows.Add(new object[] { Txt_From.Value.ToString("dd/MM/yyyy"), Txt_To.Value.ToString("dd/MM/yyyy") });
        }

    }

}
