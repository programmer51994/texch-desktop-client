﻿namespace Integration_Accounting_Sys.remittences_online
{
    partial class Inquery_aml_Remittances
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grdrec_rem = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtScity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_S_doc_issue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_S_doc_type = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Txt_S_details_job = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_s_job = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.Txt_Discreption = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.Txt_R_notes = new System.Windows.Forms.TextBox();
            this.Txt_R_Relation = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txt_purpose_rec = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Txt_R_details_job = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_address = new System.Windows.Forms.TextBox();
            this.Txt_r_job = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.Txt_r_doc_issue = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Txt_R_doc_type = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.Txt_R_doc_no = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txt_R_doc_date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_r_birthdate = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_doc_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_Birth = new Integration_Accounting_Sys.MyDateTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(307, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 906;
            this.label1.Text = "المستخـــــدم:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(682, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(71, 14);
            this.label4.TabIndex = 904;
            this.label4.Text = "التاريــــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(761, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(184, 23);
            this.TxtIn_Rec_Date.TabIndex = 905;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(394, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(250, 23);
            this.TxtUser.TabIndex = 903;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(15, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(265, 23);
            this.TxtTerm_Name.TabIndex = 902;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1, 29);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(969, 1);
            this.flowLayoutPanel9.TabIndex = 907;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-55, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(13, 32);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(182, 14);
            this.label2.TabIndex = 909;
            this.label2.Text = "معلومــــات الحوالـــة الحالـي....";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(639, 402);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(0, 14);
            this.label13.TabIndex = 943;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(193, 40);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(777, 1);
            this.flowLayoutPanel6.TabIndex = 994;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 608);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(980, 1);
            this.flowLayoutPanel4.TabIndex = 986;
            // 
            // Grdrec_rem
            // 
            this.Grdrec_rem.AllowUserToAddRows = false;
            this.Grdrec_rem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.Grdrec_rem.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grdrec_rem.ColumnHeadersHeight = 24;
            this.Grdrec_rem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column7,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column5,
            this.Column6,
            this.Column11,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column17,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column40,
            this.Column22,
            this.Column23,
            this.Column25,
            this.Column35,
            this.Column26,
            this.Column28,
            this.Column16,
            this.Column18,
            this.Column24,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column41,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column36,
            this.Column37,
            this.Column39,
            this.Column42,
            this.Column27,
            this.Column38});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdrec_rem.DefaultCellStyle = dataGridViewCellStyle18;
            this.Grdrec_rem.Location = new System.Drawing.Point(1, 49);
            this.Grdrec_rem.Name = "Grdrec_rem";
            this.Grdrec_rem.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.Grdrec_rem.RowHeadersVisible = false;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.Grdrec_rem.Size = new System.Drawing.Size(952, 172);
            this.Grdrec_rem.TabIndex = 995;
            this.Grdrec_rem.VirtualMode = true;
            this.Grdrec_rem.SelectionChanged += new System.EventHandler(this.Grdrec_rem_SelectionChanged);
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحولة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 81;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "اسم المرسـل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 90;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "R_amount";
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 88;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_aCur_name";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 88;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PR_aCUR_name";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 88;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "t_acity_name";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 96;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "s_acity_name";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 97;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "r_name";
            this.Column10.HeaderText = "اسم المستلم";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 86;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 88;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Case_Date";
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 91;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "C_DATE";
            this.Column11.HeaderText = "تاريخ ووقت الانشاء";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 127;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "User_name";
            this.Column13.HeaderText = "اسم المستخدم";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "S_phone";
            this.Column14.HeaderText = "هاتف المرسل";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Visible = false;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "s_A_NAT_NAME";
            this.Column15.HeaderText = "جنسية المرسل";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Visible = false;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Sa_ACITY_NAME";
            this.Column17.HeaderText = "مدينة المرسل";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Visible = false;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "sbirth_date";
            this.Column19.HeaderText = "تاريخ ميلاد المرسل";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Visible = false;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "SBirth_Place";
            this.Column20.HeaderText = "مكان ولادة المرسل";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Visible = false;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "s_job";
            this.Column21.HeaderText = "مهنة المرسل";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Visible = false;
            // 
            // Column40
            // 
            this.Column40.DataPropertyName = "s_details_job";
            this.Column40.HeaderText = "تفاصيل عمل المرسل";
            this.Column40.Name = "Column40";
            this.Column40.ReadOnly = true;
            this.Column40.Visible = false;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "S_address";
            this.Column22.HeaderText = "عنوان المرسل";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Visible = false;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "sFRM_ADOC_NA";
            this.Column23.HeaderText = "وثيقة المرسل";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.Visible = false;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "S_doc_no";
            this.Column25.HeaderText = "رقم وثيقة المرسل";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.Visible = false;
            // 
            // Column35
            // 
            this.Column35.DataPropertyName = "S_doc_ida";
            this.Column35.HeaderText = "تاريخ انشاء الوثيقة للمرسل";
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            this.Column35.Visible = false;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "S_doc_issue";
            this.Column26.HeaderText = "جهة اصدار وثيقة المرسل";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.Visible = false;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "Source_money";
            this.Column28.HeaderText = "مصدر المال";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.Visible = false;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "R_phone";
            this.Column16.HeaderText = "هاتف المستلم";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Visible = false;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "R_A_NAT_NAME";
            this.Column18.HeaderText = "جنسية المستلم";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Visible = false;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "r_ACITY_NAME";
            this.Column24.HeaderText = "مدينة المستلم";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.Visible = false;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "rbirth_date";
            this.Column29.HeaderText = "ميلاد المستلم";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.Visible = false;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "TBirth_Place";
            this.Column30.HeaderText = "محل ولادة المستلم";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Visible = false;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "r_job";
            this.Column31.HeaderText = "مهنة المستلم";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.Visible = false;
            // 
            // Column41
            // 
            this.Column41.DataPropertyName = "r_details_job";
            this.Column41.HeaderText = "تفاصيل عمل المستلم";
            this.Column41.Name = "Column41";
            this.Column41.ReadOnly = true;
            this.Column41.Visible = false;
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "r_address";
            this.Column32.HeaderText = "عنوان المستلم";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.Visible = false;
            // 
            // Column33
            // 
            this.Column33.DataPropertyName = "RFRM_ADOC_NA";
            this.Column33.HeaderText = "وثيقة المستلم";
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            this.Column33.Visible = false;
            // 
            // Column34
            // 
            this.Column34.DataPropertyName = "r_doc_no";
            this.Column34.HeaderText = "رقم وثيقة المستلم";
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            this.Column34.Visible = false;
            // 
            // Column36
            // 
            this.Column36.DataPropertyName = "R_doc_ida";
            this.Column36.HeaderText = "تاريخ انشاء الوثيقة للمستلم";
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            this.Column36.Visible = false;
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "r_doc_issue";
            this.Column37.HeaderText = "محل اصدار الوثيقة للمستلم";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            this.Column37.Visible = false;
            // 
            // Column39
            // 
            this.Column39.DataPropertyName = "s_notes";
            this.Column39.HeaderText = "ملاحظات المرسل";
            this.Column39.Name = "Column39";
            this.Column39.ReadOnly = true;
            this.Column39.Visible = false;
            // 
            // Column42
            // 
            this.Column42.DataPropertyName = "r_notes";
            this.Column42.HeaderText = "ملاحظات المستلم";
            this.Column42.Name = "Column42";
            this.Column42.ReadOnly = true;
            this.Column42.Visible = false;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "Relation_S_R";
            this.Column27.HeaderText = "علاقة مر. مس.";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            this.Column27.Visible = false;
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "T_purpose";
            this.Column38.HeaderText = "غرض التحويل";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            this.Column38.Visible = false;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(117, 237);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(854, 1);
            this.flowLayoutPanel17.TabIndex = 1041;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(3, 226);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(116, 14);
            this.label14.TabIndex = 1040;
            this.label14.Text = "معلومات المرسل...";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txts_nat.Location = new System.Drawing.Point(716, 244);
            this.Txts_nat.Multiline = true;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(217, 25);
            this.Txts_nat.TabIndex = 1046;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(638, 248);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 1047;
            this.label7.Text = "جنسية المرسل:";
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_name.Location = new System.Drawing.Point(76, 244);
            this.TxtS_name.Multiline = true;
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(247, 25);
            this.TxtS_name.TabIndex = 1044;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(3, 248);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 1045;
            this.label5.Text = "اسم المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_phone.Location = new System.Drawing.Point(412, 244);
            this.TxtS_phone.Multiline = true;
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(218, 25);
            this.TxtS_phone.TabIndex = 1043;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(329, 248);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 16);
            this.label19.TabIndex = 1042;
            this.label19.Text = "هاتف المرسل:";
            // 
            // TxtScity
            // 
            this.TxtScity.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtScity.Location = new System.Drawing.Point(75, 270);
            this.TxtScity.Multiline = true;
            this.TxtScity.Name = "TxtScity";
            this.TxtScity.ReadOnly = true;
            this.TxtScity.Size = new System.Drawing.Size(247, 25);
            this.TxtScity.TabIndex = 1050;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(1, 274);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 1051;
            this.label8.Text = "مدينة المرسل:";
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_address.Location = new System.Drawing.Point(412, 270);
            this.TxtS_address.Multiline = true;
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(521, 25);
            this.TxtS_address.TabIndex = 1048;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(330, 274);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(75, 16);
            this.label23.TabIndex = 1049;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(851, 303);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(87, 14);
            this.label3.TabIndex = 1058;
            this.label3.Text = "yyyy/mm/dd";
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relionship.Location = new System.Drawing.Point(411, 297);
            this.Txt_Relionship.Multiline = true;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(218, 25);
            this.Txt_Relionship.TabIndex = 1055;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(326, 301);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(85, 16);
            this.label6.TabIndex = 1056;
            this.label6.Text = "علاقة مر /  مس:";
            // 
            // Txt_S_doc_issue
            // 
            this.Txt_S_doc_issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_issue.Location = new System.Drawing.Point(75, 297);
            this.Txt_S_doc_issue.Multiline = true;
            this.Txt_S_doc_issue.Name = "Txt_S_doc_issue";
            this.Txt_S_doc_issue.ReadOnly = true;
            this.Txt_S_doc_issue.Size = new System.Drawing.Size(247, 25);
            this.Txt_S_doc_issue.TabIndex = 1053;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(0, 301);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 1054;
            this.label10.Text = "محل اصدارها:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(642, 303);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(65, 16);
            this.label20.TabIndex = 1052;
            this.label20.Text = "التولــــــــــد:";
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_no.Location = new System.Drawing.Point(411, 323);
            this.Txt_S_doc_no.Multiline = true;
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.ReadOnly = true;
            this.Txt_S_doc_no.Size = new System.Drawing.Size(218, 25);
            this.Txt_S_doc_no.TabIndex = 1064;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(329, 327);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 1065;
            this.label9.Text = "رقم الوثيقـــــة:";
            // 
            // Txt_S_doc_type
            // 
            this.Txt_S_doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_type.Location = new System.Drawing.Point(75, 323);
            this.Txt_S_doc_type.Multiline = true;
            this.Txt_S_doc_type.Name = "Txt_S_doc_type";
            this.Txt_S_doc_type.ReadOnly = true;
            this.Txt_S_doc_type.Size = new System.Drawing.Size(247, 25);
            this.Txt_S_doc_type.TabIndex = 1063;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(1, 326);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 16);
            this.label11.TabIndex = 1062;
            this.label11.Text = "نوع الوثيقة:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(851, 329);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 1061;
            this.label34.Text = "yyyy/mm/dd";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(639, 327);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(73, 16);
            this.label18.TabIndex = 1060;
            this.label18.Text = "تاريخهــــــــا:";
            // 
            // Txt_S_details_job
            // 
            this.Txt_S_details_job.BackColor = System.Drawing.Color.White;
            this.Txt_S_details_job.Location = new System.Drawing.Point(346, 350);
            this.Txt_S_details_job.MaxLength = 99;
            this.Txt_S_details_job.Multiline = true;
            this.Txt_S_details_job.Name = "Txt_S_details_job";
            this.Txt_S_details_job.ReadOnly = true;
            this.Txt_S_details_job.Size = new System.Drawing.Size(283, 25);
            this.Txt_S_details_job.TabIndex = 1155;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(326, 356);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(16, 20);
            this.label45.TabIndex = 1154;
            this.label45.Text = "/";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Purpose.Location = new System.Drawing.Point(715, 350);
            this.Txt_Purpose.Multiline = true;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(217, 25);
            this.Txt_Purpose.TabIndex = 1152;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(638, 354);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 1153;
            this.label17.Text = "غرض التحويل:";
            // 
            // Txt_s_job
            // 
            this.Txt_s_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_s_job.Location = new System.Drawing.Point(75, 350);
            this.Txt_s_job.Multiline = true;
            this.Txt_s_job.Name = "Txt_s_job";
            this.Txt_s_job.ReadOnly = true;
            this.Txt_s_job.Size = new System.Drawing.Size(247, 25);
            this.Txt_s_job.TabIndex = 1150;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(1, 353);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(69, 16);
            this.label22.TabIndex = 1151;
            this.label22.Text = "مهنة المرسل:";
            // 
            // Txt_Discreption
            // 
            this.Txt_Discreption.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Discreption.Location = new System.Drawing.Point(75, 403);
            this.Txt_Discreption.Multiline = true;
            this.Txt_Discreption.Name = "Txt_Discreption";
            this.Txt_Discreption.ReadOnly = true;
            this.Txt_Discreption.Size = new System.Drawing.Size(858, 25);
            this.Txt_Discreption.TabIndex = 1158;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(-1, 407);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 1159;
            this.label33.Text = "التفاصيـــــــل:";
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(74, 376);
            this.Txt_Soruce_money.Multiline = true;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(859, 25);
            this.Txt_Soruce_money.TabIndex = 1156;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(-1, 381);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(103, 16);
            this.label21.TabIndex = 1157;
            this.label21.Text = "مصـــدر المــال:";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(119, 438);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel3.TabIndex = 1161;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(1, 428);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(120, 14);
            this.label15.TabIndex = 1160;
            this.label15.Text = "معلومات المستلم...";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(4, 588);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(73, 16);
            this.label47.TabIndex = 1183;
            this.label47.Text = "التفاصيـــــــل:";
            // 
            // Txt_R_notes
            // 
            this.Txt_R_notes.BackColor = System.Drawing.Color.White;
            this.Txt_R_notes.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_notes.Location = new System.Drawing.Point(77, 584);
            this.Txt_R_notes.MaxLength = 199;
            this.Txt_R_notes.Name = "Txt_R_notes";
            this.Txt_R_notes.ReadOnly = true;
            this.Txt_R_notes.Size = new System.Drawing.Size(865, 23);
            this.Txt_R_notes.TabIndex = 1182;
            // 
            // Txt_R_Relation
            // 
            this.Txt_R_Relation.BackColor = System.Drawing.Color.White;
            this.Txt_R_Relation.Location = new System.Drawing.Point(422, 559);
            this.Txt_R_Relation.MaxLength = 200;
            this.Txt_R_Relation.Multiline = true;
            this.Txt_R_Relation.Name = "Txt_R_Relation";
            this.Txt_R_Relation.ReadOnly = true;
            this.Txt_R_Relation.Size = new System.Drawing.Size(218, 25);
            this.Txt_R_Relation.TabIndex = 1180;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(334, 563);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(96, 25);
            this.label58.TabIndex = 1181;
            this.label58.Text = "علاقة مس /  مر:";
            // 
            // txt_purpose_rec
            // 
            this.txt_purpose_rec.BackColor = System.Drawing.Color.White;
            this.txt_purpose_rec.Location = new System.Drawing.Point(719, 558);
            this.txt_purpose_rec.Multiline = true;
            this.txt_purpose_rec.Name = "txt_purpose_rec";
            this.txt_purpose_rec.ReadOnly = true;
            this.txt_purpose_rec.Size = new System.Drawing.Size(223, 25);
            this.txt_purpose_rec.TabIndex = 1178;
            this.txt_purpose_rec.TextChanged += new System.EventHandler(this.txt_purpose_rec_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(641, 564);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(77, 16);
            this.label44.TabIndex = 1179;
            this.label44.Text = "غرض التحويل:";
            // 
            // Txt_R_details_job
            // 
            this.Txt_R_details_job.BackColor = System.Drawing.Color.White;
            this.Txt_R_details_job.Location = new System.Drawing.Point(608, 529);
            this.Txt_R_details_job.MaxLength = 99;
            this.Txt_R_details_job.Multiline = true;
            this.Txt_R_details_job.Name = "Txt_R_details_job";
            this.Txt_R_details_job.ReadOnly = true;
            this.Txt_R_details_job.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_details_job.Size = new System.Drawing.Size(333, 25);
            this.Txt_R_details_job.TabIndex = 1177;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(244, 564);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(87, 14);
            this.label16.TabIndex = 1176;
            this.label16.Text = "yyyy/mm/dd";
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_City.Location = new System.Drawing.Point(85, 474);
            this.Txt_R_City.Multiline = true;
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_City.TabIndex = 1173;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(5, 476);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(71, 16);
            this.label27.TabIndex = 1174;
            this.label27.Text = "مدينة المستلم:";
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Nat.Location = new System.Drawing.Point(722, 448);
            this.Txt_R_Nat.Multiline = true;
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(219, 25);
            this.Txt_R_Nat.TabIndex = 1171;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(644, 452);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(77, 16);
            this.label28.TabIndex = 1172;
            this.label28.Text = "جنسية المستلم:";
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Name.Location = new System.Drawing.Point(85, 448);
            this.Txt_R_Name.Multiline = true;
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_Name.TabIndex = 1169;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(5, 451);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(70, 16);
            this.label30.TabIndex = 1170;
            this.label30.Text = "اسم المستلــم:";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Phone.Location = new System.Drawing.Point(421, 448);
            this.Txt_R_Phone.Multiline = true;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(218, 25);
            this.Txt_R_Phone.TabIndex = 1168;
            // 
            // Txt_R_address
            // 
            this.Txt_R_address.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_address.Location = new System.Drawing.Point(422, 474);
            this.Txt_R_address.Multiline = true;
            this.Txt_R_address.Name = "Txt_R_address";
            this.Txt_R_address.ReadOnly = true;
            this.Txt_R_address.Size = new System.Drawing.Size(519, 25);
            this.Txt_R_address.TabIndex = 1163;
            // 
            // Txt_r_job
            // 
            this.Txt_r_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_r_job.Location = new System.Drawing.Point(421, 530);
            this.Txt_r_job.Multiline = true;
            this.Txt_r_job.Name = "Txt_r_job";
            this.Txt_r_job.ReadOnly = true;
            this.Txt_r_job.Size = new System.Drawing.Size(159, 25);
            this.Txt_r_job.TabIndex = 1162;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(340, 479);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(74, 16);
            this.label31.TabIndex = 1167;
            this.label31.Text = "عنوان المستلم:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(342, 536);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(68, 16);
            this.label32.TabIndex = 1166;
            this.label32.Text = "مهنة المستلم:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(5, 563);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(71, 16);
            this.label35.TabIndex = 1165;
            this.label35.Text = "التولــــــــــــد:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(341, 451);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 16);
            this.label38.TabIndex = 1164;
            this.label38.Text = "هاتف المستلم:";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(395, 615);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 25);
            this.button2.TabIndex = 1185;
            this.button2.Text = "تصديـــر";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(476, 615);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 25);
            this.button1.TabIndex = 1186;
            this.button1.Text = "انهـــاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(586, 531);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(16, 20);
            this.label46.TabIndex = 1187;
            this.label46.Text = "/";
            // 
            // Txt_r_doc_issue
            // 
            this.Txt_r_doc_issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_r_doc_issue.Location = new System.Drawing.Point(421, 502);
            this.Txt_r_doc_issue.Multiline = true;
            this.Txt_r_doc_issue.Name = "Txt_r_doc_issue";
            this.Txt_r_doc_issue.ReadOnly = true;
            this.Txt_r_doc_issue.Size = new System.Drawing.Size(220, 25);
            this.Txt_r_doc_issue.TabIndex = 1189;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(337, 508);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(75, 16);
            this.label25.TabIndex = 1190;
            this.label25.Text = "محل اصدارها:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(853, 507);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(87, 14);
            this.label12.TabIndex = 1193;
            this.label12.Text = "yyyy/mm/dd";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(648, 506);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(73, 16);
            this.label24.TabIndex = 1192;
            this.label24.Text = "تاريخهــــــــا:";
            // 
            // Txt_R_doc_type
            // 
            this.Txt_R_doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_doc_type.Location = new System.Drawing.Point(85, 502);
            this.Txt_R_doc_type.Multiline = true;
            this.Txt_R_doc_type.Name = "Txt_R_doc_type";
            this.Txt_R_doc_type.ReadOnly = true;
            this.Txt_R_doc_type.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_doc_type.TabIndex = 1195;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(5, 505);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 16);
            this.label26.TabIndex = 1194;
            this.label26.Text = "نوع الوثيقــة:";
            // 
            // Txt_R_doc_no
            // 
            this.Txt_R_doc_no.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_doc_no.Location = new System.Drawing.Point(85, 530);
            this.Txt_R_doc_no.Multiline = true;
            this.Txt_R_doc_no.Name = "Txt_R_doc_no";
            this.Txt_R_doc_no.ReadOnly = true;
            this.Txt_R_doc_no.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_doc_no.TabIndex = 1196;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(5, 534);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(73, 16);
            this.label29.TabIndex = 1197;
            this.label29.Text = "رقم الوثيقــــة:";
            // 
            // Txt_R_doc_date
            // 
            this.Txt_R_doc_date.BackColor = System.Drawing.Color.White;
            this.Txt_R_doc_date.DateSeperator = '/';
            this.Txt_R_doc_date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_doc_date.Location = new System.Drawing.Point(723, 502);
            this.Txt_R_doc_date.Mask = "0000/00/00";
            this.Txt_R_doc_date.Name = "Txt_R_doc_date";
            this.Txt_R_doc_date.PromptChar = ' ';
            this.Txt_R_doc_date.ReadOnly = true;
            this.Txt_R_doc_date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_doc_date.Size = new System.Drawing.Size(128, 22);
            this.Txt_R_doc_date.TabIndex = 1191;
            this.Txt_R_doc_date.Text = "00000000";
            this.Txt_R_doc_date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_r_birthdate
            // 
            this.Txt_r_birthdate.BackColor = System.Drawing.Color.White;
            this.Txt_r_birthdate.DateSeperator = '/';
            this.Txt_r_birthdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_birthdate.Location = new System.Drawing.Point(85, 561);
            this.Txt_r_birthdate.Mask = "0000/00/00";
            this.Txt_r_birthdate.Name = "Txt_r_birthdate";
            this.Txt_r_birthdate.PromptChar = ' ';
            this.Txt_r_birthdate.ReadOnly = true;
            this.Txt_r_birthdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_birthdate.Size = new System.Drawing.Size(145, 22);
            this.Txt_r_birthdate.TabIndex = 1175;
            this.Txt_r_birthdate.Text = "00000000";
            this.Txt_r_birthdate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_S_doc_Date
            // 
            this.Txt_S_doc_Date.BackColor = System.Drawing.Color.White;
            this.Txt_S_doc_Date.DateSeperator = '/';
            this.Txt_S_doc_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_doc_Date.Location = new System.Drawing.Point(721, 324);
            this.Txt_S_doc_Date.Mask = "0000/00/00";
            this.Txt_S_doc_Date.Name = "Txt_S_doc_Date";
            this.Txt_S_doc_Date.PromptChar = ' ';
            this.Txt_S_doc_Date.ReadOnly = true;
            this.Txt_S_doc_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_doc_Date.Size = new System.Drawing.Size(128, 22);
            this.Txt_S_doc_Date.TabIndex = 1059;
            this.Txt_S_doc_Date.Text = "00000000";
            this.Txt_S_doc_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_S_Birth
            // 
            this.Txt_S_Birth.BackColor = System.Drawing.Color.White;
            this.Txt_S_Birth.DateSeperator = '/';
            this.Txt_S_Birth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Birth.Location = new System.Drawing.Point(719, 298);
            this.Txt_S_Birth.Mask = "0000/00/00";
            this.Txt_S_Birth.Name = "Txt_S_Birth";
            this.Txt_S_Birth.PromptChar = ' ';
            this.Txt_S_Birth.ReadOnly = true;
            this.Txt_S_Birth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_Birth.Size = new System.Drawing.Size(128, 22);
            this.Txt_S_Birth.TabIndex = 1057;
            this.Txt_S_Birth.Text = "00000000";
            this.Txt_S_Birth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Inquery_aml_Remittances
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 642);
            this.Controls.Add(this.Txt_R_doc_no);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Txt_R_doc_type);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.Txt_R_doc_date);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Txt_r_doc_issue);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.Txt_R_notes);
            this.Controls.Add(this.Txt_R_Relation);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.txt_purpose_rec);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.Txt_R_details_job);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_r_birthdate);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_address);
            this.Controls.Add(this.Txt_r_job);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_Discreption);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.Txt_Soruce_money);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Txt_S_details_job);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_s_job);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.Txt_S_doc_no);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Txt_S_doc_type);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_S_doc_Date);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_S_Birth);
            this.Controls.Add(this.Txt_Relionship);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_S_doc_issue);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.TxtScity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtS_address);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Grdrec_rem);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Inquery_aml_Remittances";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "556";
            this.Text = "كشف بتفاصيل الحوالات";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Inquery_aml_Remittances_FormClosed);
            this.Load += new System.EventHandler(this.Inquery_aml_Remittances_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.DataGridView Grdrec_rem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TxtScity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label3;
        private MyDateTextBox Txt_S_Birth;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_S_doc_issue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txt_S_doc_type;
        private System.Windows.Forms.Label label11;
        private MyDateTextBox Txt_S_doc_Date;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Txt_S_details_job;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_s_job;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Txt_Discreption;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox Txt_R_notes;
        private System.Windows.Forms.TextBox Txt_R_Relation;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txt_purpose_rec;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox Txt_R_details_job;
        private System.Windows.Forms.Label label16;
        private MyDateTextBox Txt_r_birthdate;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_R_address;
        private System.Windows.Forms.TextBox Txt_r_job;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox Txt_r_doc_issue;
        private System.Windows.Forms.Label label25;
        private MyDateTextBox Txt_R_doc_date;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Txt_R_doc_type;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox Txt_R_doc_no;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
    }
}