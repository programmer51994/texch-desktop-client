﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Setting_control_online_remittannce : Form
    {
        BindingSource _Bs_Grd_nat = new BindingSource();
        BindingSource _Bs_Grd_con = new BindingSource();
        BindingSource _Bs_Grd_job = new BindingSource();
        BindingSource _Bs_Grd_Doc = new BindingSource();
        BindingSource _Bs_Grd_nat_R = new BindingSource();
        BindingSource _Bs_Grd_con_R = new BindingSource();
        BindingSource _Bs_Grd_job_R = new BindingSource();
        BindingSource _Bs_Grd_Doc_R = new BindingSource();
        BindingSource _BS_Users = new BindingSource();
        BindingSource _Bs_Cur = new BindingSource();
        BindingSource _Bs_Purpose = new BindingSource();
        BindingSource _Bs_cbo = new BindingSource();

        DataTable Temp_nat_Tbl = new DataTable();
        DataTable Temp_Con_Tbl = new DataTable();
        DataTable Temp_Job_Tbl = new DataTable();
        DataTable Temp_Doc_Tbl = new DataTable();
        DataTable Temp_nat_R_Tbl = new DataTable();
        DataTable Temp_Con_R_Tbl = new DataTable();
        DataTable Temp_Job_R_Tbl = new DataTable();
        DataTable Temp_Doc_R_Tbl = new DataTable();
        DataTable Temp_USERS_Tbl = new DataTable();
        DataTable Temp_Cur_tbl = new DataTable();
        DataTable Temp_purpose_tbl = new DataTable();

        DataTable struct_Dt_Tbl = new DataTable();
        DataTable struct_cur_Tbl = new DataTable();
        //struct_cur_Tbl

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();
        DataTable DT3 = new DataTable();
        DataTable DT4 = new DataTable();
        DataTable DT5 = new DataTable();
        DataTable DT6 = new DataTable();
        DataTable DT7 = new DataTable();
        DataTable DT8 = new DataTable();
        DataTable DT9 = new DataTable();
        DataTable DT10 = new DataTable();
        DataTable DT11 = new DataTable();
        DataTable Bill = new DataTable();
        bool Change = false;
        bool Change_filter_nat = false;
        bool Change_filter_con = false;
        bool Change_filter_job = false;
        bool Change_filter_Doc = false;




        bool Change_filter_nat_R = false;
        bool Change_filter_con_R = false;
        bool Change_filter_job_R = false;
        bool Change_filter_Doc_R = false;
        //bool Change_filter_Users = false;
        //bool Change_filter_cur = false;
        //bool Change_filter_Purpose = false;
        //bool change_R_Type_Id = false;
        //bool change_from = false;
        bool change1 = false;


        Int16 nat_id = 0;
        Int16 con_id = 0;
        Int16 job_id = 0;
        Int16 fmd_id = 0;
        //**
        Int64 from_nrec_date = 0;
        Int64 to_nrec_date = 0;
        //   int R_type_id;

        //string R_Con_str = "";
        //string S_Con_str = "";
        //string S_nat_str = "";
        //string R_nat_str = "";
        //string S_job_str = "";
        //string R_job_str = "";
        //string S_Doc_str = "";
        //string R_Doc_str = "";
        //string User_str = "";
        //string purpose_str = "";
        //string cur_str = "";
        //--------------------------------------------------------------------
        public Setting_control_online_remittannce()
        {
            InitializeComponent();
            Create_Tbl();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_nat.AutoGenerateColumns = false;
            Grd_con.AutoGenerateColumns = false;
            Grd_job.AutoGenerateColumns = false;
            Grd_doc.AutoGenerateColumns = false;

            Grd_nat_R.AutoGenerateColumns = false;
            Grd_con_R.AutoGenerateColumns = false;
            Grd_job_R.AutoGenerateColumns = false;
            Grd_doc_R.AutoGenerateColumns = false;
            if (connection.login_Id != 0)
            {
                Cbo_Term.Items.Clear();
                Cbo_Term.Items.Add("Individual Conditions");
                Cbo_Term.Items.Add("Combined Conditions");
            }

        }
        //---------------------------------------------------------------------
        private void Setting_control_online_rem_Load(object sender, EventArgs e)
        {

            Cbo_Term.SelectedIndex = 0;


            nat_id = 0;
            con_id = 0;
            job_id = 0;
            fmd_id = 0;

            Int16 Flag = 0;
            change1 = false;
            try
            {
                Flag = Convert.ToInt16(connection.SqlExec("SELECT        distinct isnull(Search_Aml_flag,0) as Search_Aml_flag  FROM    Control_Conf_Rem ", "Tbl_aml_flag").Rows[0]["Search_Aml_flag"].ToString());

                if (Flag == 0)
                {
                    Cbo_Term.SelectedIndex = 0;

                }
                else
                { Cbo_Term.SelectedIndex = 1; }


                change1 = true;
                Cbo_Term_SelectedIndexChanged(null, null);
            }
            catch
            { Flag = 0;
            change1 = true;
            Cbo_Term_SelectedIndexChanged(null, null);
            }
        }
        //--------------------------------------------------------------------
        private void Create_Tbl()
        {
            string[] Column1 = { "ID" };
            string[] DType1 = { "System.Int16" };
            struct_Dt_Tbl = CustomControls.Custom_DataTable("struct_Dt_Tbl", Column1, DType1);
        }
        //--------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------------------------
        private void Setting_control_online_rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            Change_filter_nat = false;
            Change_filter_con = false;
            Change_filter_job = false;
            Change_filter_Doc = false;
            Change_filter_nat_R = false;
            Change_filter_con_R = false;
            Change_filter_job_R = false;
            Change_filter_Doc_R = false;

            string[] Str1 = { "Reveal_Details_GetData_tbl", "Reveal_Details_GetData_tbl1", "Reveal_Details_GetData_tbl2", "Reveal_Details_GetData_tbl3", "Reveal_Details_GetData_tbl4", "Reveal_Details_GetData_tbl8", "Reveal_Details_GetData_tbl6", "Reveal_Details_GetData_tbl7", "Reveal_Details_GetData_tbl9", "Reveal_Details_GetData_tbl10", "Reveal_Details_GetData_tbl11", "Qurey_Rem_tbl1", "Qurey_Rem_tbl2", "aml_Remittances_Query_Tbl" };
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //--------------------------------------------------------------------
        private void Txt_Nat_TextChanged(object sender, EventArgs e)
        {
            Change_filter_nat = true;
            Get_Nat_Data();
        }
        //--------------------------------------------------------------------
        private void Txt_Con_TextChanged(object sender, EventArgs e)
        {
            Change_filter_con = true;
            Get_Con_Data();
        }
        //--------------------------------------------------------------------
        private void Txt_Job_TextChanged(object sender, EventArgs e)
        {
            Change_filter_job = true;
            Get_Job_Data();
        }
        //--------------------------------------------------------------------
        private void Txt_Fmd_TextChanged(object sender, EventArgs e)
        {
            Change_filter_Doc = true;
            //func
            Get_Doc_Data();
        }
        //--------------------------------------------------------------------
        private void Get_Nat_Data()
        {
            if (Change_filter_nat)
            {
                int Nat_id_int = 0;
                int.TryParse(Txt_Nat.Text, out Nat_id_int);
                if (Txt_Nat.Text != "")
                {

                    DT1 = Temp_nat_Tbl;
                    DT1.DefaultView.RowFilter = "( Nat_id  = " + Nat_id_int + " OR ( Nat_AName like '%" + Txt_Nat.Text.Trim() + "%' or Nat_EName like '%" + Txt_Nat.Text.Trim() + "%'))";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Temp_nat_Tbl;
                    DT1.DefaultView.RowFilter = " Nat_id > " + Nat_id_int;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
        }
        //--------------------------------------------------------------------
        private void Get_Con_Data()
        {
            if (Change_filter_con)
            {
                int con_id_int = 0;
                int.TryParse(Txt_Con.Text, out con_id_int);


                if (Txt_Con.Text != "")
                {
                    DT2 = Temp_Con_Tbl;
                    DT2.DefaultView.RowFilter = "( Con_ID  = " + con_id_int + " OR (Con_AName like '%" + Txt_Con.Text.Trim() + "%' or Con_EName like '%" + Txt_Con.Text.Trim() + "%') )";
                    DT2 = DT2.DefaultView.ToTable();
                }
                else
                {
                    DT2 = Temp_Con_Tbl;
                    DT2.DefaultView.RowFilter = " Con_ID > " + con_id_int;
                    DT2 = DT2.DefaultView.ToTable();
                }
            }
        }
        //--------------------------------------------------------------------
        private void Get_Job_Data()
        {
            if (Change_filter_job)
            {
                int Job_id_int = 0;
                int.TryParse(Txt_Job.Text, out Job_id_int);
                if (Txt_Job.Text != "")
                {

                    DT3 = Temp_Job_Tbl;
                    DT3.DefaultView.RowFilter = "( Job_ID  = " + Job_id_int + " OR (Job_AName like '%" + Txt_Job.Text.Trim() + "%'  or Job_EName like '%" + Txt_Job.Text.Trim() + "%') )";
                    DT3 = DT3.DefaultView.ToTable();
                }
                else
                {
                    DT3 = Temp_Job_Tbl;
                    DT3.DefaultView.RowFilter = " Job_ID > " + Job_id_int;
                    DT3 = DT3.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------
        private void Get_Doc_Data()
        {
            if (Change_filter_Doc)
            {
                int Doc_id_int = 0;
                int.TryParse(Txt_Fmd.Text, out Doc_id_int);


                if (Txt_Fmd.Text != "")
                {
                    DT4 = Temp_Doc_Tbl;
                    DT4.DefaultView.RowFilter = "( Fmd_ID  = " + Doc_id_int + " OR ( Fmd_AName like '%" + Txt_Fmd.Text.Trim() + "%' or Fmd_EName like '%" + Txt_Fmd.Text.Trim() + "%')  )";
                    DT4 = DT4.DefaultView.ToTable();
                }
                else
                {
                    DT4 = Temp_Doc_Tbl;
                    DT4.DefaultView.RowFilter = " Fmd_ID > " + Doc_id_int;
                    DT4 = DT4.DefaultView.ToTable();
                }
            }
        }
        //--------------------------------------------------------------------
        private void Txt_Fmd_R_TextChanged(object sender, EventArgs e)
        {
            Change_filter_Doc_R = true;
            Get_Doc_Data_R();
        }
        //-------------------------------------------------------------------- 
        private void Get_Nat_Data_R()
        {
            if (Change_filter_nat_R)
            {
                int Nat_id_int_R = 0;
                int.TryParse(Txt_Nat_R.Text, out Nat_id_int_R);
                if (Txt_Nat_R.Text != "")
                {
                    DT5 = Temp_nat_R_Tbl;
                    DT5.DefaultView.RowFilter = "( Nat_id  = " + Nat_id_int_R + " OR( Nat_AName like '%" + Txt_Nat_R.Text.Trim() + "%' or Nat_EName like '%" + Txt_Nat_R.Text.Trim() + "%') )";
                    DT5 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT5 = Temp_nat_R_Tbl;
                    DT5.DefaultView.RowFilter = " Nat_id > " + Nat_id_int_R;
                    DT5 = DT5.DefaultView.ToTable();
                }
            }
        }

        //--------------------------------------------------------------------
        private void Get_Con_Data_R()
        {
            if (Change_filter_con_R)
            {
                int con_id_int = 0;
                int.TryParse(Txt_Con_R.Text, out con_id_int);


                if (Txt_Con_R.Text != "")
                {

                    DT6 = Temp_Con_R_Tbl;
                    DT6.DefaultView.RowFilter = "( Con_ID  = " + con_id_int + " OR Con_AName like '%" + Txt_Con_R.Text.Trim() + "%'  )";
                    DT6 = DT6.DefaultView.ToTable();


                }
                else
                {
                    DT6 = Temp_Con_R_Tbl;
                    DT6.DefaultView.RowFilter = " Con_ID > " + con_id_int;
                    DT6 = DT6.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------
        private void Get_Job_Data_R()
        {
            if (Change_filter_job_R)
            {
                int Job_id_int = 0;
                int.TryParse(Txt_Job_R.Text, out Job_id_int);


                if (Txt_Job_R.Text != "")
                {

                    DT7 = Temp_Job_R_Tbl;
                    DT7.DefaultView.RowFilter = "( Job_ID  = " + Job_id_int + " OR (Job_AName like '%" + Txt_Job_R.Text.Trim() + "%' or Job_EName like '%" + Txt_Job_R.Text.Trim() + "%')  )";
                    DT7 = DT7.DefaultView.ToTable();


                }
                else
                {
                    DT7 = Temp_Job_R_Tbl;
                    DT7.DefaultView.RowFilter = " Job_ID > " + Job_id_int;
                    DT7 = DT7.DefaultView.ToTable();

                }
            }
        }
        //----------------------------------------------------------
        private void Get_Doc_Data_R()
        {
            if (Change_filter_Doc_R)
            {
                int Doc_id_int = 0;
                int.TryParse(Txt_Fmd_R.Text, out Doc_id_int);


                if (Txt_Fmd_R.Text != "")
                {

                    DT8 = Temp_Doc_R_Tbl;
                    DT8.DefaultView.RowFilter = "( Fmd_ID  = " + Doc_id_int + " OR ( Fmd_AName like '%" + Txt_Fmd_R.Text.Trim() + "%'  or Fmd_EName like '%" + Txt_Fmd_R.Text.Trim() + "%') )";
                    DT8 = DT8.DefaultView.ToTable();


                }
                else
                {
                    DT8 = Temp_Doc_R_Tbl;
                    DT8.DefaultView.RowFilter = " Fmd_ID > " + Doc_id_int;
                    DT8 = DT8.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------
        private void Txt_Nat_R_TextChanged_1(object sender, EventArgs e)
        {
            Change_filter_nat_R = true;
            Get_Nat_Data_R();
        }
        //--------------------------------------------------------------------
        private void Txt_Job_R_TextChanged(object sender, EventArgs e)
        {
            Change_filter_job_R = true;
            Get_Job_Data_R();
        }
        //--------------------------------------------------------------------
        private void Txt_Con_R_TextChanged(object sender, EventArgs e)
        {
            Change_filter_con_R = true;
            Get_Con_Data_R();
        }
        //--------------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column = { "Id" };

            string[] DType =
            {
                "System.Int32"
                
            };

            Bill = CustomControls.Custom_DataTable("Bill", Column, DType);
        }

        private void Add_Btn_Click(object sender, EventArgs e)
        {
            Create_Table();

            string[] Str1 = { "Reveal_Details_GetData_tbl", "Reveal_Details_GetData_tbl1", "Reveal_Details_GetData_tbl2", "Reveal_Details_GetData_tbl3", "Reveal_Details_GetData_tbl4", "Reveal_Details_GetData_tbl8", "Reveal_Details_GetData_tbl6", "Reveal_Details_GetData_tbl7", "Reveal_Details_GetData_tbl9", "Reveal_Details_GetData_tbl10", "Reveal_Details_GetData_tbl11", "Qurey_Rem_tbl1", "Qurey_Rem_tbl2", "aml_Remittances_Query_Tbl" };
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }

            DataTable DT_Tbl_Nat = new DataTable();
            DataTable DT_TbL_Con = new DataTable();
            DataTable DT_TbL_Job = new DataTable();
            DataTable DT_TbL_Doc = new DataTable();
            DataTable DT_Tbl_Nat_R = new DataTable();
            DataTable DT_TbL_Con_R = new DataTable();
            DataTable DT_TbL_Job_R = new DataTable();
            DataTable DT_TbL_Doc_R = new DataTable();
            DataTable DT_TbL_Pur = new DataTable();
            DataTable DT_TbL_Users = new DataTable();
            DataTable DT_TbL_Cur = new DataTable();

            DataTable aml_Rem_Q_DT = new DataTable();
            if (from_nrec_date > to_nrec_date)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره الادنى اصغر من تاريخ الفتره الاعلى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                return;
            }


            try
            {
                DT_Tbl_Nat_R = Temp_nat_R_Tbl.Select("chk > 0").CopyToDataTable();
                DT_Tbl_Nat_R = DT_Tbl_Nat_R.DefaultView.ToTable(false, "Nat_id").Select().CopyToDataTable();
            }
            catch
            { }

            try
            {

                DT_Tbl_Nat = Temp_nat_Tbl.Select("chk > 0").CopyToDataTable();
                DT_Tbl_Nat = DT_Tbl_Nat.DefaultView.ToTable(false, "Nat_id").Select().CopyToDataTable();
            }
            catch
            { }
            try
            {
                DT_TbL_Con = Temp_Con_Tbl.Select("Chk > 0").CopyToDataTable();
                DT_TbL_Con = DT_TbL_Con.DefaultView.ToTable(false, "Con_ID").Select().CopyToDataTable();
            }
            catch
            { }
            try
            {
                DT_TbL_Con_R = Temp_Con_R_Tbl.Select("Chk > 0").CopyToDataTable();
                DT_TbL_Con_R = DT_TbL_Con_R.DefaultView.ToTable(false, "Con_ID").Select().CopyToDataTable();
            }
            catch
            { }


            try
            {
                DT_TbL_Job = Temp_Job_Tbl.Select("Chk > 0").CopyToDataTable();
                DT_TbL_Job = DT_TbL_Job.DefaultView.ToTable(false, "Job_ID").Select().CopyToDataTable();
            }
            catch
            { }
            try
            {
                DT_TbL_Job_R = Temp_Job_R_Tbl.Select("Chk > 0").CopyToDataTable();
                DT_TbL_Job_R = DT_TbL_Job_R.DefaultView.ToTable(false, "Job_ID").Select().CopyToDataTable();
            }
            catch
            { }

            try
            {
                DT_TbL_Doc = Temp_Doc_Tbl.Select("Chk > 0").CopyToDataTable();
                DT_TbL_Doc = DT_TbL_Doc.DefaultView.ToTable(false, "Fmd_ID").Select().CopyToDataTable();
            }
            catch
            { }
            try
            {
                DT_TbL_Doc_R = Temp_Doc_R_Tbl.Select("Chk > 0").CopyToDataTable();
                DT_TbL_Doc_R = DT_TbL_Doc_R.DefaultView.ToTable(false, "Fmd_ID").Select().CopyToDataTable();
            }
            catch
            { }

            if (Cbo_Term.SelectedIndex == 1)// الشروط مجتمعة
            {
                if (Convert.ToInt16(DT_Tbl_Nat_R.Rows.Count )!= 0)
                {
                    if (Convert.ToInt16(DT_Tbl_Nat_R.Rows.Count) > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "  لايمكن اضافة اكثر من اختيار ضمن الشروط المجتمعة لجنسية المستلم " : "you can't chose more than tow chose for rec. nationality ", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                if (Convert.ToInt16(DT_Tbl_Nat.Rows.Count) != 0)
                {
                    if (Convert.ToInt16(DT_Tbl_Nat.Rows.Count) > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "   لايمكن اضافة اكثر من اختيار ضمن الشروط المجتمعة لجنسية المرسل" : "you can't chose more than tow chose for sender nationality ", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                if (Convert.ToInt16(DT_TbL_Con.Rows.Count) != 0)
                {
                    if (Convert.ToInt16(DT_TbL_Con.Rows.Count) > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "    لايمكن اضافة اكثر من اختيار ضمن الشروط المجتمعة لبلد المرسل" : "you can't chose more than tow chose for Sender Contury", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                if (Convert.ToInt16(DT_TbL_Con_R.Rows.Count) != 0)
                {
                    if (Convert.ToInt16(DT_TbL_Con_R.Rows.Count) > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "   لايمكن اضافة اكثر من اختيار ضمن الشروط المجتمعة لبلد المستلم " : "you can't chose more than tow chose for rec. Contury", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                if (Convert.ToInt16(DT_TbL_Doc.Rows.Count) != 0)
                {
                    if (Convert.ToInt16(DT_TbL_Doc.Rows.Count) > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "   لايمكن اضافة اكثر من اختيار ضمن الشروط المجتمعة لوثيقة المرسل " : "you can't chose more than tow chose for sender  Document", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                if (Convert.ToInt16(DT_TbL_Doc_R.Rows.Count) != 0)
                {
                    if (Convert.ToInt16(DT_TbL_Doc_R.Rows.Count) > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "   لايمكن اضافة اكثر من اختيار ضمن الشروط المجتمعة لوثيقة المستلم " : "you can't chose more than tow chose for rec. Document", MyGeneral_Lib.LblCap);
                        return;
                    }
                }



                if (Convert.ToInt16(DT_Tbl_Nat.Rows.Count) == 0 && Convert.ToInt16(DT_Tbl_Nat_R.Rows.Count) == 1 || Convert.ToInt16(DT_Tbl_Nat.Rows.Count) == 1 && Convert.ToInt16(DT_Tbl_Nat_R.Rows.Count) == 0 )
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الشروط مجتمعة" : "you must choose the condition together", MyGeneral_Lib.LblCap);
                        return; 
                }

                if (Convert.ToInt16(DT_TbL_Doc.Rows.Count) == 0 && Convert.ToInt16(DT_TbL_Doc_R.Rows.Count) == 1 || Convert.ToInt16(DT_TbL_Doc.Rows.Count) == 1 && Convert.ToInt16(DT_TbL_Doc_R.Rows.Count) == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الشروط مجتمعة" : "you must choose the condition together", MyGeneral_Lib.LblCap);
                    return;
                }

                if (Convert.ToInt16(DT_TbL_Con.Rows.Count) == 0 && Convert.ToInt16(DT_TbL_Con_R.Rows.Count) == 1 || Convert.ToInt16(DT_TbL_Con.Rows.Count) == 1 && Convert.ToInt16(DT_TbL_Con_R.Rows.Count) == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الشروط مجتمعة" : "you must choose the condition together", MyGeneral_Lib.LblCap);
                    return;
                }
                

            }


            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Setting_Control_Online_Rem";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Nat_tbl", DT_Tbl_Nat.Rows.Count > 0 ? DT_Tbl_Nat :Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Nat_tbl_rec", DT_Tbl_Nat_R.Rows.Count > 0 ? DT_Tbl_Nat_R : Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Con_tbl", DT_TbL_Con.Rows.Count > 0 ? DT_TbL_Con : Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Con_tbl_rec", DT_TbL_Con_R.Rows.Count > 0 ? DT_TbL_Con_R : Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Job_tbl",DT_TbL_Job.Rows.Count > 0 ?  DT_TbL_Job :Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Job_tbl_rec", DT_TbL_Job_R.Rows.Count > 0 ? DT_TbL_Job_R :Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Fmd_tbl",DT_TbL_Doc.Rows.Count > 0 ?  DT_TbL_Doc :Bill);
                connection.SQLCMD.Parameters.AddWithValue("@Fmd_tbl_rec",DT_TbL_Doc_R.Rows.Count > 0 ?  DT_TbL_Doc_R :Bill);
                connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@Search_Aml_flag", Cbo_Term.SelectedIndex);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                IDataReader obj = connection.SQLCMD.ExecuteReader();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();

                }
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }


            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
            connection.SQLCMD.Parameters.Clear();

            this.Close();
        }

        private void Cbo_Term_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change1)
            {
                connection.SqlExec("exec setting_control_get_data " + Cbo_Term.SelectedIndex, "Reveal_Details_GetData_tbl");

                Temp_Job_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl"];
                _Bs_Grd_job.DataSource = Temp_Job_Tbl;
                Grd_job.DataSource = _Bs_Grd_job;

                Temp_nat_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl1"];
              
                _Bs_Grd_nat.DataSource = Temp_nat_Tbl;
                Grd_nat.DataSource = _Bs_Grd_nat;

                Temp_Doc_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl2"];
                _Bs_Grd_Doc.DataSource = Temp_Doc_Tbl;
                Grd_doc.DataSource = _Bs_Grd_Doc;

                Temp_Con_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl3"];
                _Bs_Grd_con.DataSource = Temp_Con_Tbl;
                Grd_con.DataSource = _Bs_Grd_con;

                //Reciever:
                Temp_Job_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl4"];
                _Bs_Grd_job_R.DataSource = Temp_Job_R_Tbl;
                Grd_job_R.DataSource = _Bs_Grd_job_R;


                Temp_nat_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl5"];
                _Bs_Grd_nat_R.DataSource = Temp_nat_R_Tbl;
                Grd_nat_R.DataSource = _Bs_Grd_nat_R;

                Temp_Doc_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl6"];
                _Bs_Grd_Doc_R.DataSource = Temp_Doc_R_Tbl;
                Grd_doc_R.DataSource = _Bs_Grd_Doc_R;

                Temp_Con_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl7"];
                _Bs_Grd_con_R.DataSource = Temp_Con_R_Tbl;
                Grd_con_R.DataSource = _Bs_Grd_con_R;

            }
        }
    }
}