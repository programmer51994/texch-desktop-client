﻿namespace Integration_Accounting_Sys
{
    partial class search_reveal_per_info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Grd_per = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_per_id = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.TxtCust_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.Txt_Social_ID = new System.Windows.Forms.TextBox();
            this.Txt_birth_place = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_State = new System.Windows.Forms.TextBox();
            this.Txt_Post_code = new System.Windows.Forms.TextBox();
            this.Txt_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Street = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Cbo_Gender = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CboDoc_id = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CboNat_id = new System.Windows.Forms.ComboBox();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.CboCit_Id = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.Txt_Mother_name = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.cmb_job = new System.Windows.Forms.ComboBox();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.TxtBirth_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtDoc_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtExp_Da = new System.Windows.Forms.DateTimePicker();
            this.Cbo_sing_mother = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_birth_place = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_phone = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_email = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_social_no = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_state = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_sub = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_street = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_post = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_Do_NO = new System.Windows.Forms.ComboBox();
            this.Cbo_sing_DO_IS = new System.Windows.Forms.ComboBox();
            this.Cbo_t_id = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.Cbo_info_desplay = new System.Windows.Forms.ComboBox();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.resd_rec_cmb = new System.Windows.Forms.ComboBox();
            this.Txt_Another_Phone = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Cbo_sing_other_phone = new System.Windows.Forms.ComboBox();
            this.Grd_years = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_year = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(10, 33);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(878, 1);
            this.flowLayoutPanel2.TabIndex = 530;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(124, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(247, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(762, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(120, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(706, 7);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 527;
            this.label2.Text = "التاريـخ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 14);
            this.label1.TabIndex = 526;
            this.label1.Text = "اسم المستخــدم:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(17, 118);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 569;
            this.label3.Text = "التــاريــخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(196, 118);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 568;
            this.label5.Text = "إلــــى :";
            // 
            // Grd_per
            // 
            this.Grd_per.AllowUserToAddRows = false;
            this.Grd_per.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_per.BackgroundColor = System.Drawing.Color.White;
            this.Grd_per.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_per.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_per.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_per.ColumnHeadersHeight = 45;
            this.Grd_per.ColumnHeadersVisible = false;
            this.Grd_per.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column6});
            this.Grd_per.Location = new System.Drawing.Point(486, 517);
            this.Grd_per.Name = "Grd_per";
            this.Grd_per.ReadOnly = true;
            this.Grd_per.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_per.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_per.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_per.Size = new System.Drawing.Size(397, 105);
            this.Grd_per.TabIndex = 12;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "Per_ID";
            this.Column4.HeaderText = "رمز الزبون";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 5;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "Per_AName";
            this.Column5.HeaderText = "اسم الزبون عربي";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 5;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Per_EName";
            this.Column6.HeaderText = "اسم الزبون اجنبي";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 5;
            // 
            // Grd_per_id
            // 
            this.Grd_per_id.AllowUserToAddRows = false;
            this.Grd_per_id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per_id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_per_id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_per_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_per_id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_per_id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_per_id.ColumnHeadersHeight = 45;
            this.Grd_per_id.ColumnHeadersVisible = false;
            this.Grd_per_id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_per_id.DefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_per_id.Location = new System.Drawing.Point(13, 517);
            this.Grd_per_id.Name = "Grd_per_id";
            this.Grd_per_id.ReadOnly = true;
            this.Grd_per_id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_per_id.RowHeadersVisible = false;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per_id.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_per_id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_per_id.Size = new System.Drawing.Size(397, 104);
            this.Grd_per_id.TabIndex = 7;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "Per_ID";
            this.Column1.HeaderText = "رمز الزبون";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 5;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "Per_AName";
            this.Column2.HeaderText = "اسم الزبون عربي";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 5;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "Per_EName";
            this.Column3.HeaderText = "اسم الزبون اجنبي";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 5;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button4.Location = new System.Drawing.Point(415, 597);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(67, 22);
            this.button4.TabIndex = 11;
            this.button4.Text = "<<";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button3.Location = new System.Drawing.Point(415, 576);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(67, 22);
            this.button3.TabIndex = 10;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(415, 555);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(67, 22);
            this.button2.TabIndex = 9;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(415, 534);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 22);
            this.button1.TabIndex = 8;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.BackColor = System.Drawing.Color.White;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCust_Name.Location = new System.Drawing.Point(105, 494);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(305, 22);
            this.TxtCust_Name.TabIndex = 6;
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Name_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 33);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 624);
            this.flowLayoutPanel1.TabIndex = 585;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(315, 659);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(135, 27);
            this.AddBtn.TabIndex = 13;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(449, 659);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(135, 27);
            this.ExtBtn.TabIndex = 14;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-9, 28);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(918, 1);
            this.flowLayoutPanel3.TabIndex = 589;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(888, 33);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 624);
            this.flowLayoutPanel4.TabIndex = 590;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(9, 656);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(879, 1);
            this.flowLayoutPanel5.TabIndex = 591;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(525, 399);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 646;
            this.label33.Text = "الرقم الوطني :";
            // 
            // Txt_Social_ID
            // 
            this.Txt_Social_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Social_ID.Location = new System.Drawing.Point(608, 396);
            this.Txt_Social_ID.Name = "Txt_Social_ID";
            this.Txt_Social_ID.Size = new System.Drawing.Size(186, 23);
            this.Txt_Social_ID.TabIndex = 645;
            // 
            // Txt_birth_place
            // 
            this.Txt_birth_place.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_birth_place.Location = new System.Drawing.Point(607, 189);
            this.Txt_birth_place.MaxLength = 200;
            this.Txt_birth_place.Name = "Txt_birth_place";
            this.Txt_birth_place.Size = new System.Drawing.Size(188, 23);
            this.Txt_birth_place.TabIndex = 599;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(520, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 16);
            this.label4.TabIndex = 638;
            this.label4.Text = "مكــــان التولد :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(11, 324);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 14);
            this.label19.TabIndex = 636;
            this.label19.Text = "الوثــــــــــــــــيقة....";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(12, 334);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(877, 1);
            this.flowLayoutPanel6.TabIndex = 635;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(12, 417);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 14);
            this.label6.TabIndex = 634;
            this.label6.Text = "الــعنـــــــــــــــوان....";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(14, 428);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(875, 1);
            this.flowLayoutPanel7.TabIndex = 633;
            // 
            // Txt_State
            // 
            this.Txt_State.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_State.Location = new System.Drawing.Point(105, 436);
            this.Txt_State.MaxLength = 100;
            this.Txt_State.Name = "Txt_State";
            this.Txt_State.Size = new System.Drawing.Size(194, 23);
            this.Txt_State.TabIndex = 609;
            // 
            // Txt_Post_code
            // 
            this.Txt_Post_code.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Post_code.Location = new System.Drawing.Point(607, 461);
            this.Txt_Post_code.MaxLength = 100;
            this.Txt_Post_code.Name = "Txt_Post_code";
            this.Txt_Post_code.Size = new System.Drawing.Size(188, 23);
            this.Txt_Post_code.TabIndex = 615;
            // 
            // Txt_Suburb
            // 
            this.Txt_Suburb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Suburb.Location = new System.Drawing.Point(105, 461);
            this.Txt_Suburb.MaxLength = 100;
            this.Txt_Suburb.Name = "Txt_Suburb";
            this.Txt_Suburb.Size = new System.Drawing.Size(193, 23);
            this.Txt_Suburb.TabIndex = 613;
            // 
            // Txt_Street
            // 
            this.Txt_Street.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Street.Location = new System.Drawing.Point(607, 436);
            this.Txt_Street.MaxLength = 100;
            this.Txt_Street.Name = "Txt_Street";
            this.Txt_Street.Size = new System.Drawing.Size(188, 23);
            this.Txt_Street.TabIndex = 611;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(15, 439);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 16);
            this.label25.TabIndex = 632;
            this.label25.Text = "الولايــــــــــــــة:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(526, 464);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 16);
            this.label24.TabIndex = 631;
            this.label24.Text = "الرمز البريدي:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(17, 464);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 16);
            this.label23.TabIndex = 630;
            this.label23.Text = "الحــــــــــــــي :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(522, 439);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 16);
            this.label22.TabIndex = 629;
            this.label22.Text = "زقـــــــــــاق  :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(24, 274);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 627;
            this.label8.Text = "المهـــــــــنة :";
            // 
            // Cbo_Gender
            // 
            this.Cbo_Gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Gender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Gender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Gender.FormattingEnabled = true;
            this.Cbo_Gender.Location = new System.Drawing.Point(105, 216);
            this.Cbo_Gender.Name = "Cbo_Gender";
            this.Cbo_Gender.Size = new System.Drawing.Size(282, 24);
            this.Cbo_Gender.TabIndex = 596;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(19, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 16);
            this.label7.TabIndex = 626;
            this.label7.Text = "الجنـــــــــــس:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(14, 372);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 16);
            this.label16.TabIndex = 623;
            this.label16.Text = "تاريخ الاصدار:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(13, 399);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 16);
            this.label17.TabIndex = 622;
            this.label17.Text = "تاريخ النفـــــــاذ:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(12, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 625;
            this.label13.Text = "نوع الوثيــقــــة:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(515, 346);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 620;
            this.label14.Text = "رقم الوثيقـــــــــة:";
            // 
            // CboDoc_id
            // 
            this.CboDoc_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDoc_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDoc_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDoc_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDoc_id.FormattingEnabled = true;
            this.CboDoc_id.Location = new System.Drawing.Point(105, 342);
            this.CboDoc_id.Name = "CboDoc_id";
            this.CboDoc_id.Size = new System.Drawing.Size(282, 24);
            this.CboDoc_id.TabIndex = 603;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(513, 372);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 16);
            this.label15.TabIndex = 619;
            this.label15.Text = "جـهـة الاصــدار:";
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(607, 343);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(188, 23);
            this.TxtNo_Doc.TabIndex = 604;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(515, 165);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 621;
            this.label10.Text = "تاريخ التولــــد:";
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(608, 369);
            this.TxtIss_Doc.MaxLength = 50;
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(188, 23);
            this.TxtIss_Doc.TabIndex = 607;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(15, 192);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 16);
            this.label11.TabIndex = 624;
            this.label11.Text = "الجنسيــــــــــــة:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(502, 220);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 16);
            this.label12.TabIndex = 617;
            this.label12.Text = "رقــم الهــــاتــف 1:";
            // 
            // CboNat_id
            // 
            this.CboNat_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboNat_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboNat_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboNat_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNat_id.FormattingEnabled = true;
            this.CboNat_id.Location = new System.Drawing.Point(105, 188);
            this.CboNat_id.Name = "CboNat_id";
            this.CboNat_id.Size = new System.Drawing.Size(364, 24);
            this.CboNat_id.TabIndex = 594;
            // 
            // TxtPhone
            // 
            this.TxtPhone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(607, 217);
            this.TxtPhone.MaxLength = 20;
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(191, 23);
            this.TxtPhone.TabIndex = 597;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(512, 274);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 16);
            this.label9.TabIndex = 618;
            this.label9.Text = "البريد الالكتروني:";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(607, 271);
            this.TxtEmail.MaxLength = 30;
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(191, 23);
            this.TxtEmail.TabIndex = 593;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(15, 165);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 16);
            this.label18.TabIndex = 616;
            this.label18.Text = "المدينـة / البلـــد :";
            // 
            // CboCit_Id
            // 
            this.CboCit_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCit_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCit_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCit_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCit_Id.FormattingEnabled = true;
            this.CboCit_Id.Location = new System.Drawing.Point(105, 161);
            this.CboCit_Id.Name = "CboCit_Id";
            this.CboCit_Id.Size = new System.Drawing.Size(364, 24);
            this.CboCit_Id.TabIndex = 592;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Maroon;
            this.label26.Location = new System.Drawing.Point(14, 139);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(155, 14);
            this.label26.TabIndex = 648;
            this.label26.Text = "المعلومـــات العامــــــــة ....";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(14, 149);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(874, 1);
            this.flowLayoutPanel8.TabIndex = 647;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(10, 487);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(878, 1);
            this.flowLayoutPanel9.TabIndex = 649;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(17, 246);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 16);
            this.label20.TabIndex = 651;
            this.label20.Text = "اســـــــــــم الام:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Txt_Mother_name
            // 
            this.Txt_Mother_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Mother_name.Location = new System.Drawing.Point(104, 243);
            this.Txt_Mother_name.MaxLength = 150;
            this.Txt_Mother_name.Name = "Txt_Mother_name";
            this.Txt_Mother_name.Size = new System.Drawing.Size(192, 23);
            this.Txt_Mother_name.TabIndex = 650;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(13, 496);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 16);
            this.label27.TabIndex = 652;
            this.label27.Text = "اســــــــم الزبون:";
            // 
            // cmb_job
            // 
            this.cmb_job.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_job.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_job.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_job.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_job.FormattingEnabled = true;
            this.cmb_job.Location = new System.Drawing.Point(104, 270);
            this.cmb_job.Name = "cmb_job";
            this.cmb_job.Size = new System.Drawing.Size(283, 24);
            this.cmb_job.TabIndex = 878;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "yyyy/mm/dd";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(101, 115);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(94, 20);
            this.TxtFromDate.TabIndex = 967;
            this.TxtFromDate.ValueChanged += new System.EventHandler(this.TxtFromDate_ValueChanged_1);
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "yyyy/mm/dd";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(246, 115);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(94, 20);
            this.TxtToDate.TabIndex = 968;
            this.TxtToDate.ValueChanged += new System.EventHandler(this.TxtToDate_ValueChanged_1);
            // 
            // TxtBirth_Da
            // 
            this.TxtBirth_Da.Checked = false;
            this.TxtBirth_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtBirth_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtBirth_Da.Location = new System.Drawing.Point(607, 163);
            this.TxtBirth_Da.Name = "TxtBirth_Da";
            this.TxtBirth_Da.ShowCheckBox = true;
            this.TxtBirth_Da.Size = new System.Drawing.Size(159, 20);
            this.TxtBirth_Da.TabIndex = 969;
            this.TxtBirth_Da.ValueChanged += new System.EventHandler(this.TxtBirth_Da_ValueChanged_1);
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.Checked = false;
            this.TxtDoc_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtDoc_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtDoc_Da.Location = new System.Drawing.Point(104, 370);
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.ShowCheckBox = true;
            this.TxtDoc_Da.Size = new System.Drawing.Size(163, 20);
            this.TxtDoc_Da.TabIndex = 970;
            this.TxtDoc_Da.ValueChanged += new System.EventHandler(this.TxtDoc_Da_ValueChanged_1);
            // 
            // TxtExp_Da
            // 
            this.TxtExp_Da.Checked = false;
            this.TxtExp_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtExp_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtExp_Da.Location = new System.Drawing.Point(104, 397);
            this.TxtExp_Da.Name = "TxtExp_Da";
            this.TxtExp_Da.ShowCheckBox = true;
            this.TxtExp_Da.Size = new System.Drawing.Size(163, 20);
            this.TxtExp_Da.TabIndex = 971;
            this.TxtExp_Da.ValueChanged += new System.EventHandler(this.TxtExp_Da_ValueChanged_1);
            // 
            // Cbo_sing_mother
            // 
            this.Cbo_sing_mother.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_mother.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_mother.FormattingEnabled = true;
            this.Cbo_sing_mother.Location = new System.Drawing.Point(296, 243);
            this.Cbo_sing_mother.Name = "Cbo_sing_mother";
            this.Cbo_sing_mother.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_mother.TabIndex = 1094;
            // 
            // Cbo_sing_birth_place
            // 
            this.Cbo_sing_birth_place.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_birth_place.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_birth_place.FormattingEnabled = true;
            this.Cbo_sing_birth_place.Location = new System.Drawing.Point(794, 189);
            this.Cbo_sing_birth_place.Name = "Cbo_sing_birth_place";
            this.Cbo_sing_birth_place.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_birth_place.TabIndex = 1095;
            // 
            // Cbo_sing_phone
            // 
            this.Cbo_sing_phone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_phone.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_phone.FormattingEnabled = true;
            this.Cbo_sing_phone.Location = new System.Drawing.Point(794, 217);
            this.Cbo_sing_phone.Name = "Cbo_sing_phone";
            this.Cbo_sing_phone.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_phone.TabIndex = 1096;
            // 
            // Cbo_sing_email
            // 
            this.Cbo_sing_email.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_email.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_email.FormattingEnabled = true;
            this.Cbo_sing_email.Location = new System.Drawing.Point(794, 271);
            this.Cbo_sing_email.Name = "Cbo_sing_email";
            this.Cbo_sing_email.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_email.TabIndex = 1098;
            // 
            // Cbo_sing_social_no
            // 
            this.Cbo_sing_social_no.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_social_no.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_social_no.FormattingEnabled = true;
            this.Cbo_sing_social_no.Location = new System.Drawing.Point(794, 396);
            this.Cbo_sing_social_no.Name = "Cbo_sing_social_no";
            this.Cbo_sing_social_no.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_social_no.TabIndex = 1099;
            // 
            // Cbo_sing_state
            // 
            this.Cbo_sing_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_state.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_state.FormattingEnabled = true;
            this.Cbo_sing_state.Location = new System.Drawing.Point(298, 436);
            this.Cbo_sing_state.Name = "Cbo_sing_state";
            this.Cbo_sing_state.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_state.TabIndex = 1100;
            // 
            // Cbo_sing_sub
            // 
            this.Cbo_sing_sub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_sub.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_sub.FormattingEnabled = true;
            this.Cbo_sing_sub.Location = new System.Drawing.Point(298, 461);
            this.Cbo_sing_sub.Name = "Cbo_sing_sub";
            this.Cbo_sing_sub.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_sub.TabIndex = 1101;
            // 
            // Cbo_sing_street
            // 
            this.Cbo_sing_street.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_street.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_street.FormattingEnabled = true;
            this.Cbo_sing_street.Location = new System.Drawing.Point(794, 436);
            this.Cbo_sing_street.Name = "Cbo_sing_street";
            this.Cbo_sing_street.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_street.TabIndex = 1102;
            // 
            // Cbo_sing_post
            // 
            this.Cbo_sing_post.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_post.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_post.FormattingEnabled = true;
            this.Cbo_sing_post.Location = new System.Drawing.Point(794, 461);
            this.Cbo_sing_post.Name = "Cbo_sing_post";
            this.Cbo_sing_post.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_post.TabIndex = 1103;
            // 
            // Cbo_sing_Do_NO
            // 
            this.Cbo_sing_Do_NO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_Do_NO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_Do_NO.FormattingEnabled = true;
            this.Cbo_sing_Do_NO.Location = new System.Drawing.Point(794, 343);
            this.Cbo_sing_Do_NO.Name = "Cbo_sing_Do_NO";
            this.Cbo_sing_Do_NO.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_Do_NO.TabIndex = 1104;
            // 
            // Cbo_sing_DO_IS
            // 
            this.Cbo_sing_DO_IS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_DO_IS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_DO_IS.FormattingEnabled = true;
            this.Cbo_sing_DO_IS.Location = new System.Drawing.Point(794, 369);
            this.Cbo_sing_DO_IS.Name = "Cbo_sing_DO_IS";
            this.Cbo_sing_DO_IS.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_DO_IS.TabIndex = 1105;
            // 
            // Cbo_t_id
            // 
            this.Cbo_t_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_t_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_t_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_t_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_t_id.FormattingEnabled = true;
            this.Cbo_t_id.Location = new System.Drawing.Point(621, 118);
            this.Cbo_t_id.Name = "Cbo_t_id";
            this.Cbo_t_id.Size = new System.Drawing.Size(261, 24);
            this.Cbo_t_id.TabIndex = 1106;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(545, 122);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 16);
            this.label28.TabIndex = 1107;
            this.label28.Text = "الــــــــــفرع:";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(10, 624);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(878, 1);
            this.flowLayoutPanel10.TabIndex = 1108;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(16, 633);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(118, 16);
            this.label29.TabIndex = 1110;
            this.label29.Text = "عرض معلومات الزبائن:";
            // 
            // Cbo_info_desplay
            // 
            this.Cbo_info_desplay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_info_desplay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_info_desplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_info_desplay.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_info_desplay.FormattingEnabled = true;
            this.Cbo_info_desplay.Items.AddRange(new object[] {
            "تفصيلي",
            "أجمالي"});
            this.Cbo_info_desplay.Location = new System.Drawing.Point(136, 629);
            this.Cbo_info_desplay.Name = "Cbo_info_desplay";
            this.Cbo_info_desplay.Size = new System.Drawing.Size(274, 24);
            this.Cbo_info_desplay.TabIndex = 1109;
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.SearchBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.SearchBtn.Location = new System.Drawing.Point(792, 489);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(91, 27);
            this.SearchBtn.TabIndex = 1111;
            this.SearchBtn.Text = "بحــــــث";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(19, 303);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 14);
            this.label30.TabIndex = 1113;
            this.label30.Text = "نوع الاقامـة:";
            // 
            // resd_rec_cmb
            // 
            this.resd_rec_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_rec_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_rec_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_rec_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_rec_cmb.FormattingEnabled = true;
            this.resd_rec_cmb.Location = new System.Drawing.Point(104, 298);
            this.resd_rec_cmb.Name = "resd_rec_cmb";
            this.resd_rec_cmb.Size = new System.Drawing.Size(283, 24);
            this.resd_rec_cmb.TabIndex = 1112;
            // 
            // Txt_Another_Phone
            // 
            this.Txt_Another_Phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Another_Phone.Location = new System.Drawing.Point(607, 243);
            this.Txt_Another_Phone.MaxLength = 20;
            this.Txt_Another_Phone.Name = "Txt_Another_Phone";
            this.Txt_Another_Phone.Size = new System.Drawing.Size(191, 23);
            this.Txt_Another_Phone.TabIndex = 598;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(504, 246);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 16);
            this.label21.TabIndex = 628;
            this.label21.Text = "رقــم الهــــاتــف 2:";
            // 
            // Cbo_sing_other_phone
            // 
            this.Cbo_sing_other_phone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_sing_other_phone.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_sing_other_phone.FormattingEnabled = true;
            this.Cbo_sing_other_phone.Location = new System.Drawing.Point(794, 243);
            this.Cbo_sing_other_phone.Name = "Cbo_sing_other_phone";
            this.Cbo_sing_other_phone.Size = new System.Drawing.Size(91, 22);
            this.Cbo_sing_other_phone.TabIndex = 1097;
            // 
            // Grd_years
            // 
            this.Grd_years.AllowUserToAddRows = false;
            this.Grd_years.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_years.BackgroundColor = System.Drawing.Color.White;
            this.Grd_years.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_years.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_years.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_years.ColumnHeadersHeight = 45;
            this.Grd_years.ColumnHeadersVisible = false;
            this.Grd_years.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_years.DefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_years.Location = new System.Drawing.Point(17, 63);
            this.Grd_years.Name = "Grd_years";
            this.Grd_years.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_years.RowHeadersVisible = false;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_years.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_years.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_years.Size = new System.Drawing.Size(317, 48);
            this.Grd_years.TabIndex = 1116;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FromNrec_date";
            this.dataGridViewTextBoxColumn2.HeaderText = "التاريخ من";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 175;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TONrec_date";
            this.dataGridViewTextBoxColumn1.HeaderText = "التاريخ الى";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // cbo_year
            // 
            this.cbo_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_year.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_year.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_year.FormattingEnabled = true;
            this.cbo_year.Items.AddRange(new object[] {
            "البيانات الحالية",
            "البيانات المرحلة"});
            this.cbo_year.Location = new System.Drawing.Point(105, 38);
            this.cbo_year.Name = "cbo_year";
            this.cbo_year.Size = new System.Drawing.Size(230, 24);
            this.cbo_year.TabIndex = 1114;
            this.cbo_year.SelectedIndexChanged += new System.EventHandler(this.cbo_year_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(12, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(92, 14);
            this.label31.TabIndex = 1115;
            this.label31.Text = "نـــــوع البحـــث:";
            // 
            // search_reveal_per_info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 688);
            this.Controls.Add(this.Grd_years);
            this.Controls.Add(this.cbo_year);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.resd_rec_cmb);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Cbo_info_desplay);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Cbo_t_id);
            this.Controls.Add(this.Cbo_sing_DO_IS);
            this.Controls.Add(this.Cbo_sing_Do_NO);
            this.Controls.Add(this.Cbo_sing_post);
            this.Controls.Add(this.Cbo_sing_street);
            this.Controls.Add(this.Cbo_sing_sub);
            this.Controls.Add(this.Cbo_sing_state);
            this.Controls.Add(this.Cbo_sing_social_no);
            this.Controls.Add(this.Cbo_sing_email);
            this.Controls.Add(this.Cbo_sing_other_phone);
            this.Controls.Add(this.Cbo_sing_phone);
            this.Controls.Add(this.Cbo_sing_birth_place);
            this.Controls.Add(this.Cbo_sing_mother);
            this.Controls.Add(this.TxtExp_Da);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.TxtBirth_Da);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.cmb_job);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Txt_Mother_name);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.Txt_Social_ID);
            this.Controls.Add(this.Txt_birth_place);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.Txt_State);
            this.Controls.Add(this.Txt_Post_code);
            this.Controls.Add(this.Txt_Suburb);
            this.Controls.Add(this.Txt_Street);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Txt_Another_Phone);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Cbo_Gender);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.CboDoc_id);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CboNat_id);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.CboCit_Id);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Grd_per);
            this.Controls.Add(this.Grd_per_id);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TxtCust_Name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "search_reveal_per_info";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "534";
            this.Text = "search_reveal_per_info";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Search_Reveal_pl_Acc_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Search_Trial_Balance_FormClosed);
            this.Load += new System.EventHandler(this.Search_Trial_Balance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView Grd_per;
        private System.Windows.Forms.DataGridView Grd_per_id;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TxtCust_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox Txt_Social_ID;
        private System.Windows.Forms.TextBox Txt_birth_place;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox Txt_State;
        private System.Windows.Forms.TextBox Txt_Post_code;
        private System.Windows.Forms.TextBox Txt_Suburb;
        private System.Windows.Forms.TextBox Txt_Street;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox Cbo_Gender;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox CboDoc_id;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CboNat_id;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox CboCit_Id;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Txt_Mother_name;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmb_job;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.DateTimePicker TxtBirth_Da;
        private System.Windows.Forms.DateTimePicker TxtDoc_Da;
        private System.Windows.Forms.DateTimePicker TxtExp_Da;
        private System.Windows.Forms.ComboBox Cbo_sing_mother;
        private System.Windows.Forms.ComboBox Cbo_sing_birth_place;
        private System.Windows.Forms.ComboBox Cbo_sing_phone;
        private System.Windows.Forms.ComboBox Cbo_sing_email;
        private System.Windows.Forms.ComboBox Cbo_sing_social_no;
        private System.Windows.Forms.ComboBox Cbo_sing_state;
        private System.Windows.Forms.ComboBox Cbo_sing_sub;
        private System.Windows.Forms.ComboBox Cbo_sing_street;
        private System.Windows.Forms.ComboBox Cbo_sing_post;
        private System.Windows.Forms.ComboBox Cbo_sing_Do_NO;
        private System.Windows.Forms.ComboBox Cbo_sing_DO_IS;
        private System.Windows.Forms.ComboBox Cbo_t_id;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox Cbo_info_desplay;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox resd_rec_cmb;
        private System.Windows.Forms.TextBox Txt_Another_Phone;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox Cbo_sing_other_phone;
        private System.Windows.Forms.DataGridView Grd_years;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.ComboBox cbo_year;
        private System.Windows.Forms.Label label31;
    }
}