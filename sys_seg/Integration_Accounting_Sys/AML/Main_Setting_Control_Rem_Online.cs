﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class Main_Setting_Control_Rem_Online : Form
    {

        BindingSource binding_Setting_Control_Rem = new BindingSource();
        string Filter = "";


        public Main_Setting_Control_Rem_Online()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_Setting_Control_Rem.AutoGenerateColumns = false;
            Grd_Setting_Control_Rem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Setting_Control_Rem.MultiSelect = false;

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Setting_Control_Rem.Columns["Column2"].DataPropertyName = "table_ename";
                Grd_Setting_Control_Rem.Columns["Column3"].DataPropertyName = "S_EName";
                Grd_Setting_Control_Rem.Columns["Column4"].DataPropertyName = "R_EName";

            }
            #endregion

        }

        private void Main_Setting_Control_Rem_Online_Load(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables.Contains("Main_Setting_Control_Rem_online_Tbl"))
            {
                connection.SQLDS.Tables.Remove("Main_Setting_Control_Rem_online_Tbl");
            }

            connection.SqlExec("exec Main_Setting_Control_Rem_online", "Main_Setting_Control_Rem_online_Tbl");

            if (connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"].Rows.Count > 0)
            {

                binding_Setting_Control_Rem.DataSource = connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"];
                Grd_Setting_Control_Rem.DataSource = binding_Setting_Control_Rem;
                Txt_Setting_Control_Rem.Text = connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"].Rows[0][connection.Lang_id == 1 ? "Search_Aml_flag_Aname" : "Search_Aml_flag_Ename"].ToString();

            }

            else { Grd_Setting_Control_Rem.DataSource = new DataTable(); }
        }


        private void SearchBtn_Click(object sender, EventArgs e)
        {
            if (Txt_Type_Setting.Text.Trim() != "")
            {
                try
                {

                    Filter = " (table_aname like '%" + Txt_Type_Setting.Text.Trim() + "%' or  table_ename like '%" + Txt_Type_Setting.Text.Trim() + "%'" + ")";
                    binding_Setting_Control_Rem.DataSource = connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"].Select(Filter).CopyToDataTable();


                    if (connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"].Rows.Count > 0)
                    {
                        Grd_Setting_Control_Rem.DataSource = binding_Setting_Control_Rem;

                    }

                    else { Grd_Setting_Control_Rem.DataSource = new DataTable(); }
                }
                catch { Grd_Setting_Control_Rem.DataSource = new DataTable(); }
            }
        }

        private void AllBtn_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"].Rows.Count > 0)
            {
                Txt_Type_Setting.Text = "";
                binding_Setting_Control_Rem.DataSource = connection.SQLDS.Tables["Main_Setting_Control_Rem_online_Tbl"];
                Grd_Setting_Control_Rem.DataSource = binding_Setting_Control_Rem;
            }
            else { Grd_Setting_Control_Rem.DataSource = new DataTable(); }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Setting_control_online_remittannce AddFrm = new Setting_control_online_remittannce();
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Txt_Type_Setting.Text = "";
            Main_Setting_Control_Rem_Online_Load(sender, e);
            this.Visible = true;
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Grd_Setting_Control_Rem.RowCount > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد حذف هذه الاعدادات؟" :
                  "Do you want to delete this setting", MyGeneral_Lib.LblCap,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Dr == DialogResult.Yes)
                {


                    connection.SQLCMD.Parameters.AddWithValue("@s_id", Convert.ToInt16(((DataRowView)binding_Setting_Control_Rem.Current).Row["s_id"]));
                    connection.SQLCMD.Parameters.AddWithValue("@r_id", Convert.ToInt16(((DataRowView)binding_Setting_Control_Rem.Current).Row["r_id"]));
                    connection.SQLCMD.Parameters.AddWithValue("@table_name",((DataRowView)binding_Setting_Control_Rem.Current).Row["table_name"]);
                    connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SqlExec("Delete_Setting_Control_Rem_online", connection.SQLCMD);



                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                    }
                    connection.SQLCMD.Parameters.Clear();
                    Main_Setting_Control_Rem_Online_Load(null, null);
                }
            }
        }

        private void Main_Setting_Control_Rem_Online_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Main_Setting_Control_Rem_online_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Grd_Setting_Control_Rem_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(binding_Setting_Control_Rem);
        }
    }
}
