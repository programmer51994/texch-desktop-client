﻿namespace Integration_Accounting_Sys
{
    partial class Setting_control_online_remittannce
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Txt_Nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Grd_nat = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Con = new System.Windows.Forms.TextBox();
            this.Add_Btn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Grd_con = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.Grd_doc = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Fmd = new System.Windows.Forms.TextBox();
            this.Grd_job = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Job = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_nat_R = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_Nat_R = new System.Windows.Forms.TextBox();
            this.Grd_job_R = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_Job_R = new System.Windows.Forms.TextBox();
            this.Grd_con_R = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_Con_R = new System.Windows.Forms.TextBox();
            this.Grd_doc_R = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_Fmd_R = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Cbo_Term = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_nat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_con)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_doc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_job)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_nat_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_job_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_con_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_doc_R)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 603);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(742, 22);
            this.statusStrip1.TabIndex = 537;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Txt_Nat
            // 
            this.Txt_Nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Nat.Location = new System.Drawing.Point(104, 40);
            this.Txt_Nat.MaxLength = 100;
            this.Txt_Nat.Name = "Txt_Nat";
            this.Txt_Nat.Size = new System.Drawing.Size(264, 23);
            this.Txt_Nat.TabIndex = 1189;
            this.Txt_Nat.TextChanged += new System.EventHandler(this.Txt_Nat_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(22, 42);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(81, 16);
            this.label7.TabIndex = 1190;
            this.label7.Text = "جنسية المرسل :";
            // 
            // Grd_nat
            // 
            this.Grd_nat.AllowUserToAddRows = false;
            this.Grd_nat.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_nat.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_nat.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_nat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_nat.ColumnHeadersHeight = 40;
            this.Grd_nat.ColumnHeadersVisible = false;
            this.Grd_nat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column4,
            this.Column12,
            this.Column1});
            this.Grd_nat.Location = new System.Drawing.Point(19, 64);
            this.Grd_nat.Name = "Grd_nat";
            this.Grd_nat.RowHeadersVisible = false;
            this.Grd_nat.RowHeadersWidth = 15;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_nat.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_nat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_nat.Size = new System.Drawing.Size(349, 94);
            this.Grd_nat.TabIndex = 1192;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Chk";
            this.Column5.HeaderText = "تأشير";
            this.Column5.Name = "Column5";
            this.Column5.Width = 50;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Nat_ID";
            this.Column4.HeaderText = "الرمز";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Nat_AName";
            this.Column12.HeaderText = "الجنسيه";
            this.Column12.Name = "Column12";
            this.Column12.Width = 130;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Nat_Ename";
            this.Column1.HeaderText = "الجنسية اجنبي";
            this.Column1.Name = "Column1";
            this.Column1.Width = 150;
            // 
            // Txt_Con
            // 
            this.Txt_Con.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Con.Location = new System.Drawing.Point(100, 290);
            this.Txt_Con.MaxLength = 100;
            this.Txt_Con.Name = "Txt_Con";
            this.Txt_Con.Size = new System.Drawing.Size(268, 23);
            this.Txt_Con.TabIndex = 1194;
            this.Txt_Con.TextChanged += new System.EventHandler(this.Txt_Con_TextChanged);
            // 
            // Add_Btn
            // 
            this.Add_Btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Add_Btn.ForeColor = System.Drawing.Color.Navy;
            this.Add_Btn.Location = new System.Drawing.Point(282, 576);
            this.Add_Btn.Name = "Add_Btn";
            this.Add_Btn.Size = new System.Drawing.Size(89, 26);
            this.Add_Btn.TabIndex = 1196;
            this.Add_Btn.Text = "مـوافـق";
            this.Add_Btn.UseVisualStyleBackColor = true;
            this.Add_Btn.Click += new System.EventHandler(this.Add_Btn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(371, 576);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 1197;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Grd_con
            // 
            this.Grd_con.AllowUserToAddRows = false;
            this.Grd_con.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_con.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_con.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_con.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_con.ColumnHeadersHeight = 40;
            this.Grd_con.ColumnHeadersVisible = false;
            this.Grd_con.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column10,
            this.dataGridViewTextBoxColumn2,
            this.Column2});
            this.Grd_con.Location = new System.Drawing.Point(19, 314);
            this.Grd_con.Name = "Grd_con";
            this.Grd_con.RowHeadersVisible = false;
            this.Grd_con.RowHeadersWidth = 15;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_con.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_con.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_con.Size = new System.Drawing.Size(349, 94);
            this.Grd_con.TabIndex = 1198;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Chk";
            this.Column3.FalseValue = "0";
            this.Column3.HeaderText = "تأشير";
            this.Column3.Name = "Column3";
            this.Column3.Width = 50;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Con_ID";
            this.Column10.HeaderText = "الرمز";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Con_Aname";
            this.dataGridViewTextBoxColumn2.HeaderText = "البلد";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 130;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "Con_Ename";
            this.Column2.HeaderText = "الاسم اجنبي";
            this.Column2.Name = "Column2";
            this.Column2.Width = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(22, 293);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 1205;
            this.label2.Text = "بلــد الارسال :";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(12, 572);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(724, 1);
            this.flowLayoutPanel6.TabIndex = 1256;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-229, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(948, 1);
            this.flowLayoutPanel3.TabIndex = 1257;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(16, 167);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(78, 16);
            this.label1.TabIndex = 1262;
            this.label1.Text = "وثائق المرسل :";
            // 
            // Grd_doc
            // 
            this.Grd_doc.AllowUserToAddRows = false;
            this.Grd_doc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_doc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_doc.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_doc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_doc.ColumnHeadersHeight = 40;
            this.Grd_doc.ColumnHeadersVisible = false;
            this.Grd_doc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.Column14,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3});
            this.Grd_doc.Location = new System.Drawing.Point(19, 189);
            this.Grd_doc.Name = "Grd_doc";
            this.Grd_doc.RowHeadersVisible = false;
            this.Grd_doc.RowHeadersWidth = 15;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_doc.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_doc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_doc.Size = new System.Drawing.Size(349, 94);
            this.Grd_doc.TabIndex = 1261;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn1.FalseValue = "0";
            this.dataGridViewCheckBoxColumn1.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Fmd_ID";
            this.Column14.HeaderText = "الرمز";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Fmd_Aname";
            this.dataGridViewTextBoxColumn1.HeaderText = "الوثيقه";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Fmd_Ename";
            this.dataGridViewTextBoxColumn3.HeaderText = "الاسم اجنبي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // Txt_Fmd
            // 
            this.Txt_Fmd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Fmd.Location = new System.Drawing.Point(97, 165);
            this.Txt_Fmd.MaxLength = 100;
            this.Txt_Fmd.Name = "Txt_Fmd";
            this.Txt_Fmd.Size = new System.Drawing.Size(271, 23);
            this.Txt_Fmd.TabIndex = 1260;
            this.Txt_Fmd.TextChanged += new System.EventHandler(this.Txt_Fmd_TextChanged);
            // 
            // Grd_job
            // 
            this.Grd_job.AllowUserToAddRows = false;
            this.Grd_job.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_job.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_job.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_job.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_job.ColumnHeadersHeight = 40;
            this.Grd_job.ColumnHeadersVisible = false;
            this.Grd_job.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2,
            this.Column7,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.Grd_job.Location = new System.Drawing.Point(20, 459);
            this.Grd_job.Name = "Grd_job";
            this.Grd_job.RowHeadersVisible = false;
            this.Grd_job.RowHeadersWidth = 15;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_job.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_job.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_job.Size = new System.Drawing.Size(349, 94);
            this.Grd_job.TabIndex = 1259;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn2.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 50;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Job_ID";
            this.Column7.HeaderText = "الرمز";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Job_Aname";
            this.dataGridViewTextBoxColumn4.HeaderText = "المهنة";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 130;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Job_Ename";
            this.dataGridViewTextBoxColumn5.HeaderText = "المهنة اجنبي";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(24, 437);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 1258;
            this.label3.Text = "مهنة المرسل :";
            // 
            // Txt_Job
            // 
            this.Txt_Job.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Job.Location = new System.Drawing.Point(99, 434);
            this.Txt_Job.MaxLength = 100;
            this.Txt_Job.Name = "Txt_Job";
            this.Txt_Job.Size = new System.Drawing.Size(270, 23);
            this.Txt_Job.TabIndex = 1257;
            this.Txt_Job.TextChanged += new System.EventHandler(this.Txt_Job_TextChanged);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(13, 5);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(723, 1);
            this.flowLayoutPanel5.TabIndex = 1265;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-230, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(948, 1);
            this.flowLayoutPanel7.TabIndex = 1257;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-230, 10);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(948, 1);
            this.flowLayoutPanel4.TabIndex = 1266;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(948, 1);
            this.flowLayoutPanel8.TabIndex = 1257;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(12, 5);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1, 568);
            this.flowLayoutPanel2.TabIndex = 1267;
            // 
            // Grd_nat_R
            // 
            this.Grd_nat_R.AllowUserToAddRows = false;
            this.Grd_nat_R.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_nat_R.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_nat_R.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_nat_R.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_nat_R.ColumnHeadersHeight = 40;
            this.Grd_nat_R.ColumnHeadersVisible = false;
            this.Grd_nat_R.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn3,
            this.Column6,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.Grd_nat_R.Location = new System.Drawing.Point(382, 63);
            this.Grd_nat_R.Name = "Grd_nat_R";
            this.Grd_nat_R.RowHeadersVisible = false;
            this.Grd_nat_R.RowHeadersWidth = 15;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_nat_R.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_nat_R.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_nat_R.Size = new System.Drawing.Size(349, 94);
            this.Grd_nat_R.TabIndex = 1279;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn3.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 50;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Fmd_ID";
            this.Column6.HeaderText = "الرمز";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Nat_Aname";
            this.dataGridViewTextBoxColumn6.HeaderText = "الجنسيه";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 130;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NAt_Ename";
            this.dataGridViewTextBoxColumn7.HeaderText = "الجنسية اجنبي";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(380, 43);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 1278;
            this.label8.Text = "جنسية المستلم :";
            // 
            // Txt_Nat_R
            // 
            this.Txt_Nat_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Nat_R.Location = new System.Drawing.Point(460, 39);
            this.Txt_Nat_R.MaxLength = 100;
            this.Txt_Nat_R.Name = "Txt_Nat_R";
            this.Txt_Nat_R.Size = new System.Drawing.Size(271, 23);
            this.Txt_Nat_R.TabIndex = 1277;
            this.Txt_Nat_R.TextChanged += new System.EventHandler(this.Txt_Nat_R_TextChanged_1);
            // 
            // Grd_job_R
            // 
            this.Grd_job_R.AllowUserToAddRows = false;
            this.Grd_job_R.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_job_R.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_job_R.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_job_R.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_job_R.ColumnHeadersHeight = 40;
            this.Grd_job_R.ColumnHeadersVisible = false;
            this.Grd_job_R.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn4,
            this.Column8,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.Grd_job_R.Location = new System.Drawing.Point(383, 459);
            this.Grd_job_R.Name = "Grd_job_R";
            this.Grd_job_R.RowHeadersVisible = false;
            this.Grd_job_R.RowHeadersWidth = 15;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_job_R.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_job_R.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_job_R.Size = new System.Drawing.Size(349, 94);
            this.Grd_job_R.TabIndex = 1282;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn4.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 50;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Job_ID";
            this.Column8.HeaderText = "الرمز";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Job_Aname";
            this.dataGridViewTextBoxColumn8.HeaderText = "المهنة";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 130;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Job_Ename";
            this.dataGridViewTextBoxColumn9.HeaderText = "المهنة اجنبي";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(380, 437);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(71, 16);
            this.label9.TabIndex = 1281;
            this.label9.Text = "مهنة المستلم :";
            // 
            // Txt_Job_R
            // 
            this.Txt_Job_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Job_R.Location = new System.Drawing.Point(454, 434);
            this.Txt_Job_R.MaxLength = 100;
            this.Txt_Job_R.Name = "Txt_Job_R";
            this.Txt_Job_R.Size = new System.Drawing.Size(277, 23);
            this.Txt_Job_R.TabIndex = 1280;
            this.Txt_Job_R.TextChanged += new System.EventHandler(this.Txt_Job_R_TextChanged);
            // 
            // Grd_con_R
            // 
            this.Grd_con_R.AllowUserToAddRows = false;
            this.Grd_con_R.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_con_R.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.Grd_con_R.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_con_R.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_con_R.ColumnHeadersHeight = 40;
            this.Grd_con_R.ColumnHeadersVisible = false;
            this.Grd_con_R.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn5,
            this.Column11,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.Grd_con_R.Location = new System.Drawing.Point(382, 314);
            this.Grd_con_R.Name = "Grd_con_R";
            this.Grd_con_R.RowHeadersVisible = false;
            this.Grd_con_R.RowHeadersWidth = 15;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_con_R.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_con_R.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_con_R.Size = new System.Drawing.Size(349, 94);
            this.Grd_con_R.TabIndex = 1285;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn5.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 50;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Con_ID";
            this.Column11.HeaderText = "الرمز";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Con_Aname";
            this.dataGridViewTextBoxColumn10.HeaderText = "البلد";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 130;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Con_Ename";
            this.dataGridViewTextBoxColumn11.HeaderText = "الاسم اجنبي";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(391, 293);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(65, 16);
            this.label10.TabIndex = 1284;
            this.label10.Text = "بلد الاستلام :";
            // 
            // Txt_Con_R
            // 
            this.Txt_Con_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Con_R.Location = new System.Drawing.Point(462, 290);
            this.Txt_Con_R.MaxLength = 100;
            this.Txt_Con_R.Name = "Txt_Con_R";
            this.Txt_Con_R.Size = new System.Drawing.Size(268, 23);
            this.Txt_Con_R.TabIndex = 1283;
            this.Txt_Con_R.TextChanged += new System.EventHandler(this.Txt_Con_R_TextChanged);
            // 
            // Grd_doc_R
            // 
            this.Grd_doc_R.AllowUserToAddRows = false;
            this.Grd_doc_R.AllowUserToDeleteRows = false;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_doc_R.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_doc_R.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_doc_R.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_doc_R.ColumnHeadersHeight = 40;
            this.Grd_doc_R.ColumnHeadersVisible = false;
            this.Grd_doc_R.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn6,
            this.Column15,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.Grd_doc_R.Location = new System.Drawing.Point(382, 189);
            this.Grd_doc_R.Name = "Grd_doc_R";
            this.Grd_doc_R.RowHeadersVisible = false;
            this.Grd_doc_R.RowHeadersWidth = 15;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_doc_R.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.Grd_doc_R.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_doc_R.Size = new System.Drawing.Size(349, 94);
            this.Grd_doc_R.TabIndex = 1288;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn6.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Width = 50;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Fmd_ID";
            this.Column15.HeaderText = "الرمز";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Fmd_Aname";
            this.dataGridViewTextBoxColumn12.HeaderText = "الوثائق";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Fmd_Ename";
            this.dataGridViewTextBoxColumn13.HeaderText = "الاسم اجنبي";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(383, 167);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(77, 16);
            this.label11.TabIndex = 1287;
            this.label11.Text = "وثائق المستلم :";
            // 
            // Txt_Fmd_R
            // 
            this.Txt_Fmd_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Fmd_R.Location = new System.Drawing.Point(461, 165);
            this.Txt_Fmd_R.MaxLength = 100;
            this.Txt_Fmd_R.Name = "Txt_Fmd_R";
            this.Txt_Fmd_R.Size = new System.Drawing.Size(269, 23);
            this.Txt_Fmd_R.TabIndex = 1286;
            this.Txt_Fmd_R.TextChanged += new System.EventHandler(this.Txt_Fmd_R_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(735, 5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 568);
            this.flowLayoutPanel1.TabIndex = 1268;
            // 
            // Cbo_Term
            // 
            this.Cbo_Term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Term.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Term.FormattingEnabled = true;
            this.Cbo_Term.Items.AddRange(new object[] {
            "الشروط منفردة",
            "الشروط مجتمعة "});
            this.Cbo_Term.Location = new System.Drawing.Point(104, 12);
            this.Cbo_Term.Name = "Cbo_Term";
            this.Cbo_Term.Size = new System.Drawing.Size(264, 22);
            this.Cbo_Term.TabIndex = 1290;
            this.Cbo_Term.SelectedIndexChanged += new System.EventHandler(this.Cbo_Term_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(23, 15);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(78, 16);
            this.label6.TabIndex = 1289;
            this.label6.Text = "شروط البحـــث:";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(13, 424);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(722, 1);
            this.flowLayoutPanel9.TabIndex = 1258;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-231, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(948, 1);
            this.flowLayoutPanel10.TabIndex = 1257;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(17, 413);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(120, 16);
            this.label12.TabIndex = 1295;
            this.label12.Text = "شروط البحث في الصادر";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(379, 413);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(114, 16);
            this.label13.TabIndex = 1296;
            this.label13.Text = "شروط البحث في الوارد";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(22, 554);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(255, 16);
            this.label4.TabIndex = 1297;
            this.label4.Text = "ملاحظة( شروط بحث المهن تكون منفردة في الحالتين)";
            // 
            // Setting_control_online_remittannce
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 625);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Cbo_Term);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Grd_doc_R);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_Fmd_R);
            this.Controls.Add(this.Grd_con_R);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_Con_R);
            this.Controls.Add(this.Grd_job_R);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Txt_Job_R);
            this.Controls.Add(this.Grd_nat_R);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txt_Nat_R);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Grd_doc);
            this.Controls.Add(this.Txt_Fmd);
            this.Controls.Add(this.Grd_job);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Job);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Grd_con);
            this.Controls.Add(this.Add_Btn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.Txt_Con);
            this.Controls.Add(this.Grd_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_Nat);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Setting_control_online_remittannce";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "549";
            this.Text = "الكشف بتفاصيل الحوالات - البحث";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Setting_control_online_rem_FormClosed);
            this.Load += new System.EventHandler(this.Setting_control_online_rem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_nat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_con)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_doc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_job)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_nat_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_job_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_con_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_doc_R)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button OK_button;
        private System.Windows.Forms.TextBox Txt_Nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView Grd_nat;
        private System.Windows.Forms.TextBox Txt_Con;
        private System.Windows.Forms.Button Add_Btn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.DataGridView Grd_con;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView Grd_doc;
        private System.Windows.Forms.TextBox Txt_Fmd;
        private System.Windows.Forms.DataGridView Grd_job;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Job;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridView Grd_nat_R;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_Nat_R;
        private System.Windows.Forms.DataGridView Grd_job_R;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txt_Job_R;
        private System.Windows.Forms.DataGridView Grd_con_R;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_Con_R;
        private System.Windows.Forms.DataGridView Grd_doc_R;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_Fmd_R;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Amonut;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ComboBox Cbo_Term;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Label label4;
    }
}