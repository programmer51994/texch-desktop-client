﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Rem_check_processes : Form
    {
        string @format = "dd/MM/yyyy";
        Int32 from_nrec_date = 0;
        Int32 to_nrec_date = 0;
       

        DataTable R_quer_DT = new DataTable();
        BindingSource R_que_Bs = new BindingSource();
        BindingSource R_que_Bs2 = new BindingSource();
        public Rem_check_processes()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
           Grd_rem_vo2.AutoGenerateColumns = false;
           Grd_rem_vo.AutoGenerateColumns = false;
           if (connection.Lang_id != 1)
           {
               Cbo_Type.Items.Clear();
               Cbo_Type.Items.Add("Accepted Operations");
               Cbo_Type.Items.Add("Refused Operations");
           }
        }
        //******************************************************************************************************
        private void Remittances_Query_search_Load(object sender, EventArgs e)
        {
            Cbo_Type.SelectedIndex = 0;
            TxtFromDate.CustomFormat = "00/00/0000";
            TxtToDate.CustomFormat = "00/00/0000";
       
            Grd_rem_vo.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
              
                dataGridViewTextBoxColumn8.DataPropertyName = "Acc_EName";
                dataGridViewTextBoxColumn9.DataPropertyName = "Cur_ENAME";
                Column6.DataPropertyName = "Ecase_na";
                Column8.DataPropertyName = "ECUST_NAME";
                Column2.DataPropertyName = "threshold_eng";
            }
        }
        //******************************************************************************************************
        private void getdata()
        {

            #region text_time



            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //from_nrec_date = TxtFromDate.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                from_nrec_date = 0;
            }
            //-------------------------
            if (TxtToDate.Checked == true)
            {


                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //  to_nrec_date = TxtToDate.Value.ToString("yyyy/MM/dd");
            }
            else
            {
                to_nrec_date = 0;
            }

            #endregion

            
        }

        //******************************************************************************************************

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //******************************************************************************************************


        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtFromDate.Checked == true)
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = @format;
            }
            else
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = "00/00/0000";
            }
            Grd_rem_vo.DataSource = new BindingSource();
            Grd_rem_vo2.DataSource = new BindingSource();

        }

        //******************************************************************************************************

        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtToDate.Checked == true)
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = @format;
            }
            else
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = "00/00/0000";
            }
            Grd_rem_vo.DataSource = new BindingSource();
            Grd_rem_vo2.DataSource = new BindingSource();
        }

        //******************************************************************************************************

        private void Remittances_Query_search_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "Rem_Query_tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        //******************************************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
            string[] Used_Tbl = { "Rem_Query_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
            Grd_rem_vo.DataSource = new DataTable();

            if (from_nrec_date > to_nrec_date)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                return;
            }
            if (TxtFromDate.Checked == false )
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الفتره من " : "Chosse From Date Please", MyGeneral_Lib.LblCap);
                return;
            }
            if (TxtToDate.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الفتره الى" : "Chosse To Date Please ", MyGeneral_Lib.LblCap);
                return;
            }
            getdata();

            
            //if (from_nrec_date == to_nrec_date)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
            //    return;
            //}

           // getdata();
            connection.SqlExec("Exec Reveal_note_suspend " + "'" + from_nrec_date + "'," + "'" + to_nrec_date + "'," + Cbo_Type.SelectedIndex, "Rem_Query_tbl");

      
            if (connection.SQLDS.Tables["Rem_Query_tbl"].Rows.Count > 0)
            {
                R_que_Bs.DataSource = connection.SQLDS.Tables["Rem_Query_tbl"];
                Grd_rem_vo.DataSource = R_que_Bs;

            }
            if (connection.SQLDS.Tables["Rem_Query_tbl1"].Rows.Count > 0)
            {
                R_que_Bs2.DataSource = connection.SQLDS.Tables["Rem_Query_tbl1"];
                Grd_rem_vo2.DataSource = R_que_Bs2;

            }
            try
            {
                if (connection.SQLDS.Tables["Rem_Query_tbl"].Rows.Count <= 0)
                {
                  
                }
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا يوجد بيانات تحقق هذا التاريخ" : "No Result For this Date", MyGeneral_Lib.LblCap);
                return;
            }


        }
        //******************************************************************************************************
      

        private void button2_Click(object sender, EventArgs e)
        {

            //if (Grd_rem_vo.Rows.Count == 0)
            //{

            //    MessageBox.Show(connection.Lang_id == 1 ? " لا يوجد بيانات ليتم تصديرها" : "there is no data to be exported", MyGeneral_Lib.LblCap);
            //    return;
            //}
            //else
            //{
            //    if (connection.Lang_id == 1)
            //    {
            //        DataTable ExpDt = connection.SQLDS.Tables["Rem_Query_tbl"].DefaultView.ToTable(false, "rem_no", "threshold", "DFor_Amount", "CFor_Amount", "Acc_id", "Acc_AName", "Cur_ANAME", "Exch_Price", "REAL_PRICE", "Dloc_Amount", "Cloc_Amount", "Vo_no", "NREC_DATE1", "Acase_na", "C_date", "ACUST_NAME", "User_name");
            //        DataTable[] _EXP_DT = { ExpDt };
            //        DataGridView[] Export_GRD = { Grd_rem_vo };
            //        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
            //    }
            //    else
            //    {

            //        DataTable ExpDt = connection.SQLDS.Tables["Rem_Query_tbl"].DefaultView.ToTable(false, "rem_no", "threshold_eng", "DFor_Amount", "CFor_Amount", "Acc_id", "Acc_EName", "Cur_ENAME", "Exch_Price", "REAL_PRICE", "Dloc_Amount", "Cloc_Amount", "Vo_no", "NREC_DATE1", "Ecase_na", "C_date", "ECUST_NAME", "User_name");
            //        DataTable[] _EXP_DT = { ExpDt };
            //        DataGridView[] Export_GRD = { Grd_rem_vo };
            //        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
            //    }
            //}
            DataTable Dt_G1 = new DataTable();
            DataTable DT_G2= new DataTable();
            Dt_G1 = connection.SQLDS.Tables["Rem_Query_tbl"].DefaultView.ToTable(false,"rem_no","threshold","note_user","Acase_na","Aoper_name","Vo_no","Cur_ANAME","per_id","per_Name","Bill_Amount","User_name","C_date");
            DT_G2 = connection.SQLDS.Tables["Rem_Query_tbl1"].DefaultView.ToTable(false, "Vo_no", "Aoper_name", "threshold", "note_user", "Cur_ANAME", "per_id", "per_Name", "Bill_Amount", "User_name", "C_date");
            DataGridView[] Export_GRD = { Grd_rem_vo, Grd_rem_vo2};
            DataTable[] Export_DT = { Dt_G1, DT_G2};
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void Cbo_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grd_rem_vo.DataSource = new BindingSource();
            Grd_rem_vo2.DataSource = new BindingSource();
        }

    }
}
//******************************************************************************************************.