﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;

namespace Integration_Accounting_Sys
{
    public partial class details_Reveal_Per_Info : Form
    {
        #region Defintion
       

        BindingSource BS_per_name = new BindingSource();
        BindingSource BS_SALE = new BindingSource();
        BindingSource BS_BUY = new BindingSource();
        BindingSource BS_REM_OUT = new BindingSource();
        BindingSource BS_REM_IN = new BindingSource();
        DataGridView Date_Grd_per_name = new DataGridView();
        string Sql_Text1 = "";
        Int64 det_per_id;
        Int64 frm_date_new = 0;
        Int64 to_date_new = 0;
        Int64 t_id_new = 0;
        #endregion
        //---------------------------
        public details_Reveal_Per_Info(Int64 per_id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

            det_per_id = per_id;

            Grd_buy.AutoGenerateColumns = false;
            Grd_sale.AutoGenerateColumns = false;
            Grd_rem_out.AutoGenerateColumns = false;
            Grd_rem_in.AutoGenerateColumns = false;
            #region Arabic/English
            if (connection.Lang_id != 1)
            {
                Column5.DataPropertyName = "EOPER_NAME";
                Column3.DataPropertyName = "Cur_ENAME";
                Column10.DataPropertyName = "EOPER_NAME";
                Column8.DataPropertyName = "Cur_ENAME";
                Column13.DataPropertyName = "R_ECUR_NAME";
                Column19.DataPropertyName = "R_ECUR_NAME";  
               
            }
            #endregion
        }
        //---------------------------
        private void Reveal_Acc_Main2_Load(object sender, EventArgs e)
        {
            frm_date_new = search_reveal_per_info.frm_date;
            to_date_new = search_reveal_per_info.to_date;
            t_id_new = search_reveal_per_info.t_id_new;


            BS_per_name.DataSource = connection.SQLDS.Tables["search_reveal_person_info_tbl4"].Select("per_id = " + det_per_id).CopyToDataTable();
            Grd_per_name.DataSource = BS_per_name;


            string Sqltexe = "EXec count_OPERATIONS_per_info " + det_per_id + "," + "'" + frm_date_new + "'" + "," + "'" + to_date_new + "'" + "," + t_id_new;


            connection.SqlExec(Sqltexe, "count_OPERATIONS_per_info_tbl");



            if (connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl1"].Rows.Count > 0 ||
                connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl2"].Rows.Count > 0 ||connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl3"].Rows.Count > 0)
            {

                    BS_BUY.DataSource = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl"];
                    Grd_buy.DataSource = BS_BUY;
               
                    BS_SALE.DataSource = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl1"];
                    Grd_sale.DataSource = BS_SALE;
            
                    BS_REM_OUT.DataSource = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl2"];
                    Grd_rem_out.DataSource = BS_REM_OUT;

                    BS_REM_IN.DataSource = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl3"];
                    Grd_rem_in.DataSource = BS_REM_IN;
                
            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات تاريخية " : "No Information", MyGeneral_Lib.LblCap);
                return;
            }
        }
      
        //---------------------------
       
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Reveal_Acc_Main2_FormClosed(object sender, FormClosedEventArgs e)
        {
        

          string[] Used_Tbl = { "count_OPERATIONS_per_info_tbl", "count_OPERATIONS_per_info_tbl1", 
                                "count_OPERATIONS_per_info_tbl2", "count_OPERATIONS_per_info_tbl3"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            try
            {


                Date_Grd_per_name = new DataGridView();
                Date_Grd_per_name.Columns.Add("Column1", connection.Lang_id == 1 ? "رمزالزبون" : "Person ID:");
                Date_Grd_per_name.Columns.Add("Column2", connection.Lang_id == 1 ? " اسم الزبون عربي" : "Person Name AR ");
                Date_Grd_per_name.Columns.Add("Column3", connection.Lang_id == 1 ? " اسم الزبون اجنبي" : "Person Name EN ");

                DataTable Dt_buy_det_exp = new DataTable();
                DataTable Dt_sale_det_exp = new DataTable();
                DataTable Dt_rem_out_det_exp = new DataTable();
                DataTable Dt_rem_in_det_exp = new DataTable();
                DataTable dt_per_det_name = new DataTable();

                dt_per_det_name = connection.SQLDS.Tables["search_reveal_person_info_tbl4"].DefaultView.ToTable(false, "per_id","per_aname" , "per_ename").Select("per_id = " + det_per_id).CopyToDataTable();

                if (connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl"].Rows.Count > 0)
                {

                    Dt_buy_det_exp = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl"].DefaultView.ToTable(false, "vo_no", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                }

                if (connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl1"].Rows.Count > 0)
                {

                    Dt_sale_det_exp = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl1"].DefaultView.ToTable(false, "vo_no", connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                }

                if (connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl2"].Rows.Count > 0)
                {

                    Dt_rem_out_det_exp = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl2"].DefaultView.ToTable(false, "rem_no", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "r_amount", "out_rem_loc_amount").Select().CopyToDataTable();
                }


                if (connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl3"].Rows.Count > 0)
                {

                    Dt_rem_in_det_exp = connection.SQLDS.Tables["count_OPERATIONS_per_info_tbl3"].DefaultView.ToTable(false, "rem_no", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "r_amount", "in_rem_loc_amount").Select().CopyToDataTable();
                }

                DataGridView[] Export_GRD = { Date_Grd_per_name, Grd_buy, Grd_sale, Grd_rem_out, Grd_rem_in };
                DataTable[] Export_DT = { dt_per_det_name, Dt_buy_det_exp, Dt_sale_det_exp, Dt_rem_out_det_exp, Dt_rem_in_det_exp };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            catch
            { }
        }

        private void Btn_end_Click(object sender, EventArgs e)
        {
            this.Close();
        }  
    }
}