﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Per_Info_ALL : Form
    {
        #region Defintion
        string Sql_Text1 = "";

        BindingSource BS_per_name = new BindingSource();
        BindingSource BS_SALE = new BindingSource();
        BindingSource BS_BUY = new BindingSource();
        BindingSource BS_REM_OUT = new BindingSource();
        BindingSource BS_REM_IN = new BindingSource();

        DataGridView Date_Grd_per_name = new DataGridView();

        DataGridView Date_Grd_oper_per_all = new DataGridView();

        DataTable Dt_Buy_New = new DataTable();
        DataTable Dt_Sale_New = new DataTable();
        DataTable Dt_REM_OUT_New = new DataTable();
        DataTable Dt_REM_IN_New = new DataTable();

        Int64 count_oper_buy = 0;
        Int64 count_oper_sale = 0;
        Int64 count_oper_out = 0;
        Int64 count_oper_in = 0;
        Int64 count_per_buy = 0;
        Int64 count_per_sale = 0;
        Int64 count_per_out = 0;
        Int64 count_per_in = 0;

        //Int64 count_oper_all = 0;
        //Int64 count_per_all = 0;


        #endregion
        //---------------------------
        public Reveal_Per_Info_ALL()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

            Grd_buy.AutoGenerateColumns = false;
            Grd_sale.AutoGenerateColumns = false;
            Grd_rem_out.AutoGenerateColumns = false;
            Grd_rem_in.AutoGenerateColumns = false;
            #region Arabic/English
            if (connection.Lang_id != 1)
            {
              Column5.DataPropertyName = "EOPER_NAME";
                Column3.DataPropertyName = "Cur_ENAME";
                Column10.DataPropertyName = "EOPER_NAME";
                Column8.DataPropertyName = "Cur_ENAME";
                Column13.DataPropertyName = "R_ECUR_NAME";
                Column19.DataPropertyName = "R_ECUR_NAME";


                Column15.DataPropertyName = "EOPER_NAME";
                Column16.DataPropertyName = "EOPER_NAME";
               
            }
            #endregion
        }
  

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {
            try
            {
                //DataGridView Date_Grd_per_name = new DataGridView();
                //Date_Grd_per_name.Columns.Add("Column1", connection.Lang_id == 1 ? "رمزالزبون" : "Person ID");
                //Date_Grd_per_name.Columns.Add("Column2", connection.Lang_id == 1 ? " اسم الزبون عربي" : "Person Name AR ");
                //Date_Grd_per_name.Columns.Add("Column3", connection.Lang_id == 1 ? " اسم الزبون اجنبي" : "Person Name EN ");

                DataGridView Date_Grd_oper_per_all = new DataGridView();
                Date_Grd_oper_per_all.Columns.Add("Column1", connection.Lang_id == 1 ? "المجموع الكلي للزبائن" : "Total Customers");
                Date_Grd_oper_per_all.Columns.Add("Column2", connection.Lang_id == 1 ? " المجموع الكلي للعمليات" : "Total operations");


                DataTable Dt_buy_all_exp = new DataTable();             
                DataTable Dt_sale_all_exp = new DataTable();      
                DataTable Dt_rem_out_all_exp = new DataTable();
                DataTable Dt_rem_in_all_exp = new DataTable();
                DataTable dt_per_name_all = new DataTable();
                DataTable dt_per_oper_all = new DataTable();

                dt_per_name_all = connection.SQLDS.Tables["search_reveal_person_info_tbl4"].DefaultView.ToTable(false, "per_id", "per_aname", "per_ename").Select().CopyToDataTable();
                 //count_oper_all = Convert.ToInt64(txt_count_oper_all.Text);
                 //count_per_all = Convert.ToInt64(txt_count_per.Text);

                if( Dt_Buy_New.Rows.Count > 0)
                {

                    Dt_buy_all_exp = Dt_Buy_New.DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();

                    count_oper_buy=Convert.ToInt64(txt_count_oper_buy.Text);
                    count_per_buy = Convert.ToInt64(txt_count_per_buy.Text);

                    Dt_buy_all_exp.Rows.Add(new Object[] { "مجمــوع العـملـيـات " , count_oper_buy," مجمـوع الزبائـن لعمليـات الشـراء " ,
                                              count_per_buy, DBNull.Value });


                }

                if (Dt_Sale_New.Rows.Count > 0)
                {

                    Dt_sale_all_exp = Dt_Sale_New.DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();
                    count_oper_sale=Convert.ToInt64(txt_count_oper_sale.Text);
                    count_per_sale = Convert.ToInt64(txt_count_per_sale.Text);


                    Dt_sale_all_exp.Rows.Add(new Object[] { "مجمــوع العـملـيـات " , count_oper_sale," مجمـوع الزبائـن لعمليـات البيـــع " ,
                       count_per_sale, DBNull.Value });


                }

                if (Dt_REM_OUT_New.Rows.Count > 0)
                {

                    Dt_rem_out_all_exp = Dt_REM_OUT_New.DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();

                    count_oper_out=Convert.ToInt64(txt_count_oper_out.Text);
                    count_per_out = Convert.ToInt64(txt_count_per_out.Text);

                    Dt_rem_out_all_exp.Rows.Add(new Object[] { "مجمــوع العـملـيـات " , count_oper_out," مجمـوع الزبائـن للحـــوالات الصــادرة " ,
                       count_per_out, DBNull.Value });
                }

                if (Dt_REM_IN_New.Rows.Count > 0)
                {

                    Dt_rem_in_all_exp = Dt_REM_IN_New.DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "count_OPERATIONS", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "for_amount", "loc_amount").Select().CopyToDataTable();


                    count_oper_in = Convert.ToInt64(txt_count_oper_in.Text);
                    count_per_in = Convert.ToInt64(txt_count_per_in.Text);

                    Dt_rem_in_all_exp.Rows.Add(new Object[] { "مجمــوع العـملـيـات " , count_oper_in," مجمـوع الزبائـن للحـــوالات الـــواردة " ,
                    count_per_in, DBNull.Value });

                }

                if (connection.SQLDS.Tables["search_reveal_person_info_tbl5"].Rows.Count > 0)
                {

                    dt_per_oper_all = connection.SQLDS.Tables["search_reveal_person_info_tbl5"].DefaultView.ToTable(false, "count_tot_per", "tot_count").Select().CopyToDataTable();
                }


                DataGridView[] Export_GRD = { Grd_per_name, Grd_buy, Grd_sale, Grd_rem_out, Grd_rem_in, Date_Grd_oper_per_all };
                DataTable[] Export_DT = { dt_per_name_all, Dt_buy_all_exp, Dt_sale_all_exp, Dt_rem_out_all_exp, Dt_rem_in_all_exp, dt_per_oper_all };

                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            catch
            { }
          
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
          
            try
            {
                DataRowView DRV = BS_per_name.Current as DataRowView;
                DataRow DR = DRV.Row;
                int Per_ID = DR.Field<int>("Per_ID");



                details_Reveal_Per_Info HstFrm = new details_Reveal_Per_Info(Per_ID);
                this.Visible = false;
                HstFrm.ShowDialog(this);
                this.Visible = true;
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات تاريخية " : "No Information", MyGeneral_Lib.LblCap);

                return;
            }
        }

        private void Reveal_Per_Info_ALL_Load(object sender, EventArgs e)
        {

              
            if (connection.SQLDS.Tables["search_reveal_person_info_tbl5"].Rows.Count > 0)
            {
                 DataTable count_tbl =connection.SQLDS.Tables["search_reveal_person_info_tbl5"];

                 txt_count_oper_buy.Text = count_tbl.Rows[0]["count_buy"].ToString();
                 txt_count_oper_sale.Text = count_tbl.Rows[0]["count_sale"].ToString();
                 txt_count_oper_out.Text = count_tbl.Rows[0]["count_rem_out"].ToString();
                 txt_count_oper_in.Text = count_tbl.Rows[0]["count_rem_in"].ToString();
                 txt_count_per_sale.Text = count_tbl.Rows[0]["count_sal_per"].ToString();
                 txt_count_per_buy.Text = count_tbl.Rows[0]["count_buy_per"].ToString();
                 txt_count_per_out.Text = count_tbl.Rows[0]["count_out_per"].ToString();
                 txt_count_per_in.Text = count_tbl.Rows[0]["count_in_per"].ToString();
                 txt_count_oper_all.Text = count_tbl.Rows[0]["tot_count"].ToString();
                 txt_count_per.Text = count_tbl.Rows[0]["count_tot_per"].ToString();

            }

            ///-----------------------------
            ///



            if (connection.SQLDS.Tables["search_reveal_person_info_tbl4"].Rows.Count > 0)
            {
                BS_per_name.DataSource = connection.SQLDS.Tables["search_reveal_person_info_tbl4"];
                Grd_per_name.DataSource = BS_per_name;



                if (connection.SQLDS.Tables["search_reveal_person_info_tbl"].Rows.Count > 0)
                {
                    DataTable Dt_Buy = connection.SQLDS.Tables["search_reveal_person_info_tbl"];
                    var Buy_sum_query = from row in Dt_Buy.AsEnumerable()
                                        group row by new
                                        {

                                            EOPER_NAME = row.Field<string>("EOPER_NAME"),
                                            AOPER_NAME = row.Field<string>("AOPER_NAME"),
                                            Cur_Aname = row.Field<string>("cur_aname"),
                                            Cur_Ename = row.Field<string>("cur_ename"),
                                            for_cur_id = row.Field<Int16>("for_cur_id")

                                        } into Currency_grp

                                        select new
                                        {

                                            Cur_Aname = Currency_grp.Key.Cur_Aname,
                                            Cur_Ename = Currency_grp.Key.Cur_Ename,
                                            for_cur_id = Currency_grp.Key.for_cur_id,

                                            AOPER_NAME = Currency_grp.Key.AOPER_NAME,
                                            EOPER_NAME = Currency_grp.Key.EOPER_NAME,

                                            count_OPERATIONS = Math.Abs(Currency_grp.Sum(r => r.Field<Int16>("count_OPERATIONS"))),
                                            For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount"))),
                                            loc_amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("loc_amount")))
                                        };

                    Dt_Buy_New = CustomControls.IEnumerableToDataTable(Buy_sum_query);
                    BS_BUY.DataSource = Dt_Buy_New;
                    Grd_buy.DataSource = BS_BUY;

                }

                if (connection.SQLDS.Tables["search_reveal_person_info_tbl1"].Rows.Count > 0)
                {
                    DataTable Dt_Sale = connection.SQLDS.Tables["search_reveal_person_info_tbl1"];
                    var Buy_sum_query = from row in Dt_Sale.AsEnumerable()
                                        group row by new
                                        {

                                            EOPER_NAME = row.Field<string>("EOPER_NAME"),
                                            AOPER_NAME = row.Field<string>("AOPER_NAME"),
                                            Cur_Aname = row.Field<string>("cur_aname"),
                                            Cur_Ename = row.Field<string>("cur_ename"),
                                            for_cur_id = row.Field<Int16>("for_cur_id")

                                        } into Currency_grp

                                        select new
                                        {

                                            Cur_Aname = Currency_grp.Key.Cur_Aname,
                                            Cur_Ename = Currency_grp.Key.Cur_Ename,
                                            for_cur_id = Currency_grp.Key.for_cur_id,


                                            AOPER_NAME = Currency_grp.Key.AOPER_NAME,
                                            EOPER_NAME = Currency_grp.Key.EOPER_NAME,

                                            count_OPERATIONS = Math.Abs(Currency_grp.Sum(r => r.Field<Int16>("count_OPERATIONS"))),
                                            For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount"))),
                                            loc_amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("loc_amount")))
                                        };

                    Dt_Sale_New = CustomControls.IEnumerableToDataTable(Buy_sum_query);
                    BS_SALE.DataSource = Dt_Sale_New;
                    Grd_sale.DataSource = BS_SALE;

                }

                if (connection.SQLDS.Tables["search_reveal_person_info_tbl2"].Rows.Count > 0)
                {
                    DataTable Dt_REM_OUT = connection.SQLDS.Tables["search_reveal_person_info_tbl2"];
                    var Buy_sum_query = from row in Dt_REM_OUT.AsEnumerable()
                                        group row by new
                                        {

                                            EOPER_NAME = row.Field<string>("EOPER_NAME"),
                                            AOPER_NAME = row.Field<string>("AOPER_NAME"),
                                            Cur_Aname = row.Field<string>("cur_aname"),
                                            Cur_Ename = row.Field<string>("cur_ename"),
                                            for_cur_id = row.Field<Int16>("for_cur_id")

                                        } into Currency_grp

                                        select new
                                        {

                                            Cur_Aname = Currency_grp.Key.Cur_Aname,
                                            Cur_Ename = Currency_grp.Key.Cur_Ename,
                                            for_cur_id = Currency_grp.Key.for_cur_id,


                                            AOPER_NAME = Currency_grp.Key.AOPER_NAME,
                                            EOPER_NAME = Currency_grp.Key.EOPER_NAME,

                                            count_OPERATIONS = Math.Abs(Currency_grp.Sum(r => r.Field<Int16>("count_OPERATIONS"))),
                                            For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount"))),
                                            loc_amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("loc_amount")))
                                        };

                    Dt_REM_OUT_New = CustomControls.IEnumerableToDataTable(Buy_sum_query);
                    BS_REM_OUT.DataSource = Dt_REM_OUT_New;
                    Grd_rem_out.DataSource = BS_REM_OUT;

                }


                if (connection.SQLDS.Tables["search_reveal_person_info_tbl3"].Rows.Count > 0)
                {
                    DataTable Dt_REM_IN = connection.SQLDS.Tables["search_reveal_person_info_tbl3"];
                    var Buy_sum_query = from row in Dt_REM_IN.AsEnumerable()
                                        group row by new
                                        {

                                            EOPER_NAME = row.Field<string>("EOPER_NAME"),
                                            AOPER_NAME = row.Field<string>("AOPER_NAME"),
                                            Cur_Aname = row.Field<string>("cur_aname"),
                                            Cur_Ename = row.Field<string>("cur_ename"),
                                            for_cur_id = row.Field<Int16>("for_cur_id")

                                        } into Currency_grp

                                        select new
                                        {

                                            Cur_Aname = Currency_grp.Key.Cur_Aname,
                                            Cur_Ename = Currency_grp.Key.Cur_Ename,
                                            for_cur_id = Currency_grp.Key.for_cur_id,


                                            AOPER_NAME = Currency_grp.Key.AOPER_NAME,
                                            EOPER_NAME = Currency_grp.Key.EOPER_NAME,


                                            count_OPERATIONS = Math.Abs(Currency_grp.Sum(r => r.Field<Int16>("count_OPERATIONS"))),
                                            For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount"))),
                                            loc_amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("loc_amount")))
                                        };

                    Dt_REM_IN_New = CustomControls.IEnumerableToDataTable(Buy_sum_query);
                    BS_REM_IN.DataSource = Dt_REM_IN_New;
                    Grd_rem_in.DataSource = BS_REM_IN;

                }

            }
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Reveal_Per_Info_ALL_FormClosed(object sender, FormClosedEventArgs e)
        {
          

            string[] Used_Tbl = { "search_reveal_person_info_tbl", "search_reveal_person_info_tbl1", 
                                         "search_reveal_person_info_tbl2", "search_reveal_person_info_tbl3", 
                                         "search_reveal_person_info_tbl4","search_reveal_person_info_tbl5" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
    }
}