﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Inquery_aml_Remittances_search : Form
    {

        //***
        string @format = "dd/MM/yyyy";
        //**
        BindingSource _Bs_Grd_nat = new BindingSource();
        BindingSource _Bs_Grd_con = new BindingSource();
        BindingSource _Bs_Grd_job = new BindingSource();
        BindingSource _Bs_Grd_Doc = new BindingSource();
        BindingSource _Bs_Grd_nat_R = new BindingSource();
        BindingSource _Bs_Grd_con_R = new BindingSource();
        BindingSource _Bs_Grd_job_R = new BindingSource();
        BindingSource _Bs_Grd_Doc_R = new BindingSource();
        BindingSource _BS_Users = new BindingSource();
        BindingSource _Bs_Cur = new BindingSource();
        BindingSource _Bs_Purpose = new BindingSource();
        BindingSource _Bs_cbo = new BindingSource();
     
        DataTable Temp_nat_Tbl = new DataTable();
        DataTable Temp_Con_Tbl = new DataTable();
        DataTable Temp_Job_Tbl = new DataTable();
        DataTable Temp_Doc_Tbl = new DataTable();
        DataTable Temp_nat_R_Tbl = new DataTable();
        DataTable Temp_Con_R_Tbl = new DataTable();
        DataTable Temp_Job_R_Tbl = new DataTable();
        DataTable Temp_Doc_R_Tbl = new DataTable();
        DataTable Temp_USERS_Tbl = new DataTable();
        DataTable Temp_Cur_tbl = new DataTable();
        DataTable Temp_purpose_tbl = new DataTable();

        //DataTable DT_str_SCon = new DataTable();
        //DataTable DT_str_RCon = new DataTable();


      //DataTable aml_Rem_Q_DT = new DataTable();

        DataTable struct_Dt_Tbl = new DataTable();
        DataTable struct_cur_Tbl = new DataTable();
        //struct_cur_Tbl

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();
        DataTable DT3 = new DataTable();
        DataTable DT4 = new DataTable();
        DataTable DT5 = new DataTable();
        DataTable DT6 = new DataTable();
        DataTable DT7 = new DataTable();
        DataTable DT8 = new DataTable();
        DataTable DT9= new DataTable();
        DataTable DT10 = new DataTable();
        DataTable DT11 = new DataTable();
        //DataTable Cur_Term_Tbl = new DataTable();
       
        bool Change = false;
        bool Change_filter_nat = false;
        bool Change_filter_con = false;
        bool Change_filter_job = false;
        bool Change_filter_Doc = false;



      
        bool Change_filter_nat_R = false;
        bool Change_filter_con_R = false;
        bool Change_filter_job_R = false;
        bool Change_filter_Doc_R = false;
        bool Change_filter_Users = false;
        bool Change_filter_cur = false;
        bool Change_filter_Purpose = false;
        bool change_R_Type_Id = false;
        bool change_from = false;
        bool change_to = false;

      
        Int16 nat_id = 0;
        Int16 con_id = 0;
        Int16 job_id = 0;
        Int16 fmd_id = 0;
        //**
        Int64 from_nrec_date = 0;
        Int64 to_nrec_date = 0;
        int R_type_id;

        string R_Con_str = "";
        string S_Con_str = "";
        string S_nat_str = "";
        string R_nat_str = "";
        string S_job_str = "";
        string R_job_str = "";
        string S_Doc_str = "";
        string R_Doc_str = "";
        string User_str = "";
        string purpose_str = "";
        string cur_str = "";
  
        //**

        //--------------------------------------------------------------------
        public Inquery_aml_Remittances_search()
        {
            InitializeComponent();

            Create_Tbl();
            //Create_Tbl3();
                //Create_Tbl4();
          //  Create_Tbl2();
            //Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
                MyGeneral_Lib.Form_Orientation(this);
                connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }

        //---------------------------------------------------------------------

        private void Setting_control_online_rem_Load(object sender, EventArgs e)
        {
            change_R_Type_Id = false;
            Txt_Amount.Text = "0.000";
           // TxtFromDate.Checked = false;
           // TxtToDate.Checked=false;
            cmb_range.SelectedIndex = 1;
            //**
            
        //TxtFromDate.Text="00/00/0000";
        //    TxtFromDate.Text="00/00/0000";
          
            TxtFromDate.CustomFormat = "00/00/0000";
            TxtToDate.CustomFormat = "00/00/0000";
            //**
            //TxtFromDate.Format = DateTimePickerFormat.Custom;
            //TxtFromDate.CustomFormat = @format;

            ////if (TxtToDate.Checked == true)
            ////{

            //    TxtToDate.Format = DateTimePickerFormat.Custom;
            //    TxtToDate.CustomFormat = @format;
            ////}
           
       
            if (connection.Lang_id == 2)
            {
                Cbo_R_Type_Id.Items[0] = "Out";
                Cbo_R_Type_Id.Items[1] = "In";
                //RemType.Items[2] = "Offline";
            }

            Grd_nat.AutoGenerateColumns = false;
            Grd_con.AutoGenerateColumns = false;
            Grd_job.AutoGenerateColumns = false;
            Grd_doc.AutoGenerateColumns = false;

            Grd_nat_R.AutoGenerateColumns = false;
            Grd_con_R.AutoGenerateColumns = false;
            Grd_job_R.AutoGenerateColumns = false;
            Grd_doc_R.AutoGenerateColumns = false;
            Grd_Users.AutoGenerateColumns = false;
            Grd_cur.AutoGenerateColumns = false;

            nat_id = 0;
            con_id = 0;
            job_id = 0;
            fmd_id = 0;

            connection.SqlExec("exec Reveal_Details_GetData ", "Reveal_Details_GetData_tbl");

            Temp_nat_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl1"];
            _Bs_Grd_nat.DataSource = Temp_nat_Tbl;
            Grd_nat.DataSource = _Bs_Grd_nat;

            Temp_Con_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl3"];
            _Bs_Grd_con.DataSource = Temp_Con_Tbl;
            Grd_con.DataSource = _Bs_Grd_con;

            Temp_Job_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl"];
            _Bs_Grd_job.DataSource = Temp_Job_Tbl;
            Grd_job.DataSource = _Bs_Grd_job;

            Temp_Doc_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl2"];
            _Bs_Grd_Doc.DataSource = Temp_Doc_Tbl;
            Grd_doc.DataSource = _Bs_Grd_Doc;

            //Reciever:

            Temp_nat_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl7"];
            _Bs_Grd_nat_R.DataSource = Temp_nat_R_Tbl;
            Grd_nat_R.DataSource = _Bs_Grd_nat_R;


            Temp_Con_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl9"];
            _Bs_Grd_con_R.DataSource = Temp_Con_R_Tbl;
            Grd_con_R.DataSource = _Bs_Grd_con_R;

            Temp_Job_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl6"];
            _Bs_Grd_job_R.DataSource = Temp_Job_R_Tbl;
            Grd_job_R.DataSource = _Bs_Grd_job_R;


            Temp_Doc_R_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl8"];
            _Bs_Grd_Doc_R.DataSource = Temp_Doc_R_Tbl;
            Grd_doc_R.DataSource = _Bs_Grd_Doc_R;

            Temp_USERS_Tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl4"];
            _BS_Users.DataSource = Temp_USERS_Tbl;
            Grd_Users.DataSource = _BS_Users;


            Temp_purpose_tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl10"];
            _Bs_Purpose.DataSource = Temp_purpose_tbl;
            Grd_purpose.DataSource = _Bs_Purpose;

            Temp_Cur_tbl = connection.SQLDS.Tables["Reveal_Details_GetData_tbl11"];
            _Bs_Cur.DataSource = Temp_Cur_tbl;
            Grd_cur.DataSource = _Bs_Cur;

            Cbo_R_Type_Id.SelectedIndex = 0;


            change_R_Type_Id = true;
            Cbo_R_Type_Id_SelectedIndexChanged_1(null,null);
         
        }

        //--------------------------------------------------------------------

        private void getdata()
        {

            #region text_time



            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //from_nrec_date = TxtFromDate.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                from_nrec_date = 0;
            }
            //-------------------------
            if (TxtToDate.Checked == true)
            {


                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //  to_nrec_date = TxtToDate.Value.ToString("yyyy/MM/dd");
            }
            else
            {
                to_nrec_date = 0;
            }
           

            #endregion
        }
        //--------------------------------------------------------------------
        private void Create_Tbl()
        {

            string[] Column1 = { "ID"};
            string[] DType1 = { "System.Int16"};
            struct_Dt_Tbl = CustomControls.Custom_DataTable("struct_Dt_Tbl", Column1, DType1);
            //Grd_per.DataSource = DT_per;
        }

        //private void Create_Tbl3()
        //{

        //    string[] Column1 = { "SCOn_ID" };
        //    string[] DType1 = { "System.Int16" };
        //    DT_str_SCon = CustomControls.Custom_DataTable("DT_str_SCon", Column1, DType1);
        //    //Grd_per.DataSource = DT_per;
        //}

        //private void Create_Tbl4()
        //{

        //    string[] Column1 = { "RCOn_ID" };
        //    string[] DType1 = { "System.Int16" };
        //    DT_str_RCon = CustomControls.Custom_DataTable("DT_str_RCon", Column1, DType1);
        //    //Grd_per.DataSource = DT_per;
        //}
        //private void Create_Tbl2()
        //{

        //    string[] Column1 = { "ID","Name","EName" };
        //    string[] DType1 = { "System.Int16", "System.String", "System.String" };
        //    struct_cur_Tbl = CustomControls.Custom_DataTable("struct_cur_Tbl", Column1, DType1);
        //    //Grd_per.DataSource = DT_per;
        //}
   
        //--------------------------------------------------------------------

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        //--------------------------------------------------------------------

        private void Setting_control_online_rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            
                Change = false;
                Change_filter_nat = false;
                Change_filter_con = false;
                Change_filter_job = false;
                Change_filter_Doc = false;
                Change_filter_nat_R = false;
                Change_filter_con_R = false;
                Change_filter_job_R = false;
                Change_filter_Doc_R = false;
                Change_filter_Users = false;
                Change_filter_cur = false;
                Change_filter_Purpose = false;
                change_R_Type_Id = false;

                string[] Str1 = { "Reveal_Details_GetData_tbl", "Reveal_Details_GetData_tbl1", "Reveal_Details_GetData_tbl2", "Reveal_Details_GetData_tbl3","Reveal_Details_GetData_tbl4","Reveal_Details_GetData_tbl8","Reveal_Details_GetData_tbl6","Reveal_Details_GetData_tbl7","Reveal_Details_GetData_tbl9","Reveal_Details_GetData_tbl10","Reveal_Details_GetData_tbl11","Qurey_Rem_tbl1","Qurey_Rem_tbl2","aml_Remittances_Query_Tbl"};
                foreach (string Tbl in Str1)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(Tbl);
                }
        }
        //--------------------------------------------------------------------

        private void Txt_Nat_TextChanged(object sender, EventArgs e)
        {
            Change_filter_nat = true;
            //func
            Get_Nat_Data();
        }
        //--------------------------------------------------------------------
        private void Txt_Con_TextChanged(object sender, EventArgs e)
        {
            Change_filter_con = true;
            //func
            Get_Con_Data();
        }
        //--------------------------------------------------------------------
        private void Txt_Job_TextChanged(object sender, EventArgs e)
        {
            Change_filter_job = true;
            //func
            Get_Job_Data();
        }
        //--------------------------------------------------------------------
        private void Txt_Fmd_TextChanged(object sender, EventArgs e)
        {
            Change_filter_Doc = true;
            //func
            Get_Doc_Data();
        }

        //--------------------------------------------------------------------
        private void Get_Nat_Data()
        {
            if (Change_filter_nat)
            {
                int Nat_id_int = 0;
                int.TryParse(Txt_Nat.Text, out Nat_id_int);


                if (Txt_Nat.Text != "")
                {
                    
                    DT1 = Temp_nat_Tbl;
                    DT1.DefaultView.RowFilter = "( Nat_id  = " + Nat_id_int + " OR ( Nat_AName like '%" + Txt_Nat.Text.Trim() + "%' or Nat_EName like '%" + Txt_Nat.Text.Trim() + "%'))";
                    DT1 = DT1.DefaultView.ToTable();
                  

                }
                else
                {
                    DT1 = Temp_nat_Tbl;
                    DT1.DefaultView.RowFilter = " Nat_id > " + Nat_id_int;
                    DT1 = DT1.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------

        private void Get_Con_Data()
        {
            if (Change_filter_con)
            {
                int con_id_int = 0;
                int.TryParse(Txt_Con.Text, out con_id_int);


                if (Txt_Con.Text != "")
                {
                    
                    DT2 = Temp_Con_Tbl;
                    DT2.DefaultView.RowFilter = "( Con_ID  = " + con_id_int + " OR (Con_AName like '%" + Txt_Con.Text.Trim() + "%' or Con_EName like '%" + Txt_Con.Text.Trim() + "%') )";
                    DT2 = DT2.DefaultView.ToTable();
                  

                }
                else
                {
                    DT2 = Temp_Con_Tbl;
                    DT2.DefaultView.RowFilter = " Con_ID > " + con_id_int;
                    DT2 = DT2.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------

        private void Get_Job_Data()
        {
            if (Change_filter_job)
            {
                int Job_id_int = 0;
                int.TryParse(Txt_Job.Text, out Job_id_int);


                if (Txt_Job.Text != "")
                {
                    
                    DT3 = Temp_Job_Tbl;
                    DT3.DefaultView.RowFilter = "( Job_ID  = " + Job_id_int + " OR (Job_AName like '%" + Txt_Job.Text.Trim() + "%'  or Job_EName like '%" + Txt_Job.Text.Trim() + "%') )";
                    DT3 = DT3.DefaultView.ToTable();
                  

                }
                else
                {
                    DT3 = Temp_Job_Tbl;
                    DT3.DefaultView.RowFilter = " Job_ID > " + Job_id_int;
                    DT3 = DT3.DefaultView.ToTable();

                }
            }
        }

        //--------------------------------------------------------------------
        private void Get_Doc_Data()
        {
            if (Change_filter_Doc)
            {
                int Doc_id_int = 0;
                int.TryParse(Txt_Fmd.Text, out Doc_id_int);


                if (Txt_Fmd.Text != "")
                {
                   
                    DT4 = Temp_Doc_Tbl;
                    DT4.DefaultView.RowFilter = "( Fmd_ID  = " + Doc_id_int + " OR ( Fmd_AName like '%" + Txt_Fmd.Text.Trim() + "%' or Fmd_EName like '%" + Txt_Fmd.Text.Trim() + "%')  )";
                    DT4 = DT4.DefaultView.ToTable();
                   

                }
                else
                {
                    DT4 = Temp_Doc_Tbl;
                    DT4.DefaultView.RowFilter = " Fmd_ID > " + Doc_id_int;
                    DT4 = DT4.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------

        
        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtFromDate.Checked == true)
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = @format;
            }
            else
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = "00/00/0000";
            }
        }
        //--------------------------------------------------------------------
        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtToDate.Checked == true)
            {
                
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = @format;
            }
            else
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = "00/00/0000";
            }
        }
        //--------------------------------------------------------------------
     
        //**Txt_fmd_R
        private void Txt_Fmd_R_TextChanged(object sender, EventArgs e)
        {

            Change_filter_Doc_R = true;
            //func
           Get_Doc_Data_R();

        }
        //-------------------------------------------------------------------- 

        private void Get_Nat_Data_R()
        {
            if (Change_filter_nat_R)
            {
                int Nat_id_int_R = 0;
                int.TryParse(Txt_Nat_R.Text, out Nat_id_int_R);


                if (Txt_Nat_R.Text != "")
                {
                    
                    DT5 = Temp_nat_R_Tbl;
                    DT5.DefaultView.RowFilter = "( Nat_id  = " + Nat_id_int_R + " OR( Nat_AName like '%" + Txt_Nat_R.Text.Trim() + "%' or Nat_EName like '%" + Txt_Nat_R.Text.Trim() + "%') )";
                    DT5 = DT1.DefaultView.ToTable();
                    

                }
                else
                {
                    DT5 = Temp_nat_R_Tbl;
                    DT5.DefaultView.RowFilter = " Nat_id > " + Nat_id_int_R;
                    DT5 = DT5.DefaultView.ToTable();

                }
            }
        }

        //--------------------------------------------------------------------
        private void Get_Con_Data_R()
        {
            if (Change_filter_con_R)
            {
                int con_id_int = 0;
                int.TryParse(Txt_Con_R.Text, out con_id_int);


                if (Txt_Con_R.Text != "")
                {
                    
                    DT6 = Temp_Con_R_Tbl;
                    DT6.DefaultView.RowFilter = "( Con_ID  = " + con_id_int + " OR Con_AName like '%" + Txt_Con_R.Text.Trim() + "%'  )";
                    DT6 = DT6.DefaultView.ToTable();
                   

                }
                else
                {
                    DT6 = Temp_Con_R_Tbl;
                    DT6.DefaultView.RowFilter = " Con_ID > " + con_id_int;
                    DT6 = DT6.DefaultView.ToTable();

                }
            }
        }

        //--------------------------------------------------------------------

        private void Get_Job_Data_R()
        {
            if (Change_filter_job_R)
            {
                int Job_id_int = 0;
                int.TryParse(Txt_Job_R.Text, out Job_id_int);


                if (Txt_Job_R.Text != "")
                {
                   
                    DT7 = Temp_Job_R_Tbl;
                    DT7.DefaultView.RowFilter = "( Job_ID  = " + Job_id_int + " OR (Job_AName like '%" + Txt_Job_R.Text.Trim() + "%' or Job_EName like '%" + Txt_Job_R.Text.Trim() + "%')  )";
                    DT7 = DT7.DefaultView.ToTable();
                  

                }
                else
                {
                    DT7 = Temp_Job_R_Tbl;
                    DT7.DefaultView.RowFilter = " Job_ID > " + Job_id_int;
                    DT7 = DT7.DefaultView.ToTable();

                }
            }
        }

        //--------------------------------------------------------------------
        private void Get_Users()
        {
            if (Change_filter_Users)
            {
                int user_id_int = 0;
                int.TryParse(Txt_User.Text, out user_id_int);


                if (Txt_User.Text != "")
                {
                    
                    DT9 = Temp_USERS_Tbl;
                    DT9.DefaultView.RowFilter = "( User_id  = " + user_id_int + " OR user_name like '%" + Txt_User.Text.Trim() + "%'  )";
                    DT9 = DT9.DefaultView.ToTable();
                    

                }
                else
                {
                    DT9 = Temp_USERS_Tbl;
                    DT9.DefaultView.RowFilter = " User_id > " + user_id_int;
                    DT9 = DT9.DefaultView.ToTable();

                }
            }
        }

        //--------------------------------------------------------------------

        private void Get_Curs()
        {
            if (Change_filter_cur)
            {
                int cur_id_int = 0;
                int.TryParse(txt_cur.Text, out cur_id_int);


                if (txt_cur.Text != "")
                {
                    
                    DT10 = Temp_Cur_tbl;
                    DT10.DefaultView.RowFilter = "( cur_id  = " + cur_id_int + " OR (cur_Aname like '%" + txt_cur.Text.Trim() + "%'  or cur_Ename like '%" + txt_cur.Text.Trim() + "%' ))";
                    DT10 = DT10.DefaultView.ToTable();
                 

                }
                else
                {
                    DT10 = Temp_Cur_tbl;
                    DT10.DefaultView.RowFilter = " cur_id > " + cur_id_int;
                    DT10 = DT10.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------
        //Get_Purposes
        private void Get_Purposes()
        {
            if (Change_filter_Purpose)
            {
                int pur_id_int = 0;
                int.TryParse(Txt_pur.Text, out pur_id_int);


                if (Txt_pur.Text != "")
                {
                    
                    DT11 = Temp_purpose_tbl;
                    DT11.DefaultView.RowFilter = "( Case_purpose_id  = " + pur_id_int + " OR ( Case_purpose_Aname like '%" + Txt_pur.Text.Trim() + "%' or Case_purpose_Ename like '%" + Txt_pur.Text.Trim() + "%' )  )";
                    DT11 = DT11.DefaultView.ToTable();
                  

                }
                else
                {
                    DT11 = Temp_purpose_tbl;
                    DT11.DefaultView.RowFilter = " Case_purpose_id > " + pur_id_int;
                    DT11 = DT11.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------

        private void Get_Doc_Data_R()
        {
            if (Change_filter_Doc_R)
            {
                int Doc_id_int = 0;
                int.TryParse(Txt_Fmd_R.Text, out Doc_id_int);


                if (Txt_Fmd_R.Text != "")
                {
                   
                    DT8 = Temp_Doc_R_Tbl;
                    DT8.DefaultView.RowFilter = "( Fmd_ID  = " + Doc_id_int + " OR ( Fmd_AName like '%" + Txt_Fmd_R.Text.Trim() + "%'  or Fmd_EName like '%" + Txt_Fmd_R.Text.Trim() + "%') )";
                    DT8 = DT8.DefaultView.ToTable();
                   

                }
                else
                {
                    DT8 = Temp_Doc_R_Tbl;
                    DT8.DefaultView.RowFilter = " Fmd_ID > " + Doc_id_int;
                    DT8 = DT8.DefaultView.ToTable();

                }
            }
        }
        //--------------------------------------------------------------------
        private void Txt_Nat_R_TextChanged_1(object sender, EventArgs e)
        {
            Change_filter_nat_R = true;
            //func
            Get_Nat_Data_R();

        }
        //--------------------------------------------------------------------
        private void Txt_Job_R_TextChanged(object sender, EventArgs e)
        {
            Change_filter_job_R = true;
            //func
            Get_Job_Data_R();
        }
        //--------------------------------------------------------------------
        private void Txt_Con_R_TextChanged(object sender, EventArgs e)
        {
            Change_filter_con_R = true;
            //func
            Get_Con_Data_R();
        }

        //--------------------------------------------------------------------
       
        private void Txt_User_TextChanged(object sender, EventArgs e)
        {
            Change_filter_Users = true;
            //func
            Get_Users();
        }
        //--------------------------------------------------------------------
        private void txt_cur_TextChanged(object sender, EventArgs e)
        {
            Change_filter_cur = true;
            //func
            Get_Curs();
        }

        //--------------------------------------------------------------------

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Change_filter_Purpose = true;
            //func
            Get_Purposes();
        }

        //--------------------------------------------------------------------

        private void Cbo_R_Type_Id_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            if (change_R_Type_Id)
            {
                try
                {
                    if (Cbo_R_Type_Id.SelectedIndex == 0) //صادر
                    {
                        Cmb_Case_id.DataSource = new DataTable();

                        string sql_text = "select  0 as  CASE_ID, 'جميع الحالات ' ACASE_NA, 'all cases ' as ECASE_NA "
                                          + " union "
                                          + "select CASE_ID, ACASE_NA, ECASE_NA from REMITTENCE_CASES where r_type_id=2";
                        _Bs_cbo.DataSource = connection.SqlExec(sql_text, "Qurey_Rem_tbl1");

                        Cmb_Case_id.DataSource = _Bs_cbo;
                        Cmb_Case_id.DisplayMember = connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA";
                        Cmb_Case_id.ValueMember = "CASE_ID";

                        // Cbo_purpos.Enabled = true;

                    }
                    else
                        if (Cbo_R_Type_Id.SelectedIndex == 1) //وارد
                        {


                            
                        string sql_text = "select  0 as  CASE_ID, 'جميع الحالات ' ACASE_NA, 'all cases ' as ECASE_NA "
                                          + " union "
                                          + "select distinct CASE_ID, ACASE_NA, ECASE_NA  from REMITTENCE_CASES where r_type_id=1";
                        _Bs_cbo.DataSource = connection.SqlExec(sql_text, "Qurey_Rem_tbl2");
                       // _Bs_cbo.DataSource = connection.SqlExec(sql_text, "Qurey_Rem_tbl4");


                            Cmb_Case_id.DataSource = new DataTable();
                            Cmb_Case_id.DataSource = _Bs_cbo;
                            Cmb_Case_id.DisplayMember = connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA";
                            Cmb_Case_id.ValueMember = "CASE_ID";

                            //  Cbo_purpos.Enabled = false;

                        }
                }
                catch { }
            }
        }

        private void Add_Btn_Click(object sender, EventArgs e)
        {

            string[] Str1 = { "Reveal_Details_GetData_tbl", "Reveal_Details_GetData_tbl1", "Reveal_Details_GetData_tbl2", "Reveal_Details_GetData_tbl3", "Reveal_Details_GetData_tbl4", "Reveal_Details_GetData_tbl8", "Reveal_Details_GetData_tbl6", "Reveal_Details_GetData_tbl7", "Reveal_Details_GetData_tbl9", "Reveal_Details_GetData_tbl10", "Reveal_Details_GetData_tbl11", "Qurey_Rem_tbl1", "Qurey_Rem_tbl2", "aml_Remittances_Query_Tbl" };
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }



            DataTable DT_Tbl_Nat = new DataTable();
            DataTable DT_TbL_Con = new DataTable();
            DataTable DT_TbL_Job = new DataTable();
            DataTable DT_TbL_Doc = new DataTable();

            DataTable DT_Tbl_Nat_R = new DataTable();
            DataTable DT_TbL_Con_R = new DataTable();
            DataTable DT_TbL_Job_R = new DataTable();
            DataTable DT_TbL_Doc_R = new DataTable();

            DataTable DT_TbL_Pur = new DataTable();
            DataTable DT_TbL_Users = new DataTable();
            DataTable DT_TbL_Cur = new DataTable();

            DataTable aml_Rem_Q_DT = new DataTable();
            
            

            if (TxtFromDate.Checked == false)
            {

                MessageBox.Show(connection.Lang_id == 1 ? "يرجى تأكيد الحد الادنى من التاريخ " : "SELECT THE MINIMUM DATE", MyGeneral_Lib.LblCap);
                return;

            }

            //-------------------------
            if (TxtToDate.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى تأكيد الحد الاعلى من التاريخ " : "SELECT THE MAXIMUM DATE", MyGeneral_Lib.LblCap);
                return;

            }
            getdata();
            if (from_nrec_date > to_nrec_date)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره الادنى اصغر من تاريخ الفتره الاعلى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                return;
            }
            //if (from_nrec_date == to_nrec_date)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفترهالادنى اصغر من تاريخ الفتره الاعلى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
            //    return;
            //}

                try
                {
                    DT_Tbl_Nat_R = Temp_nat_R_Tbl.DefaultView.ToTable(false, "Chk", "Nat_ID").Select("Chk > 0").CopyToDataTable();
                    DT_Tbl_Nat_R = DT_Tbl_Nat_R.DefaultView.ToTable(false, "Nat_id").Select().CopyToDataTable();
                    R_nat_str = "";
                MyGeneral_Lib.ColumnToString(DT_Tbl_Nat_R,"Nat_ID", out R_nat_str);
                }
                catch
                {
                    R_nat_str = "";
                    //DT_Tbl_Nat_R = struct_Dt_Tbl;
                }

                try
                {
                    DT_Tbl_Nat = Temp_nat_Tbl.DefaultView.ToTable(false, "Chk", "Nat_ID").Select("Chk > 0").CopyToDataTable();
                    DT_Tbl_Nat = DT_Tbl_Nat.DefaultView.ToTable(false, "Nat_id").Select().CopyToDataTable();
                    S_nat_str = "";
                    MyGeneral_Lib.ColumnToString(DT_Tbl_Nat, "Nat_ID", out S_nat_str);
                }
                catch
                {

                    //DT_Tbl_Nat = struct_Dt_Tbl;
                    S_nat_str = "";
                }


                try
                {
                    DT_TbL_Con = Temp_Con_Tbl.DefaultView.ToTable(false, "Chk", "Con_ID").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Con = DT_TbL_Con.DefaultView.ToTable(false, "Con_ID").Select().CopyToDataTable();
                    //**
                    //for (int i = 0; i < DT_TbL_Con.Rows.Count; i++)
                    //{
                    //    DataRow row = DT_str_SCon.NewRow();
                    //    row["SCON_ID"] = DT_TbL_Con.Rows[i]["Con_ID"];

                    //    DT_str_SCon.Rows.Add(row);

                        S_Con_str = "";
                        MyGeneral_Lib.ColumnToString(DT_TbL_Con,"Con_ID", out S_Con_str);

                    //}
                    //**
                }
                catch
                {
                    S_Con_str = "";
                    //DT_TbL_Con = struct_Dt_Tbl;
                }
                try
                {
                    DT_TbL_Con_R = Temp_Con_R_Tbl.DefaultView.ToTable(false, "Chk", "Con_ID").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Con_R = DT_TbL_Con_R.DefaultView.ToTable(false, "Con_ID").Select().CopyToDataTable();

                    //for (int i = 0; i < DT_TbL_Con_R.Rows.Count; i++)
                    //{
                    //    DataRow row = DT_str_RCon.NewRow();
                    //    row["RCON_ID"] = DT_TbL_Con.Rows[i]["Con_ID"];

                    //    DT_str_RCon.Rows.Add(row);

                       R_Con_str = "";
                       MyGeneral_Lib.ColumnToString(DT_TbL_Con_R,"Con_ID", out R_Con_str);
                    //}





                }
                catch
                {
                    R_Con_str = "";
                    //DT_TbL_Con_R = struct_Dt_Tbl;
                }


                try
                {
                    DT_TbL_Job = Temp_Job_Tbl.DefaultView.ToTable(false, "Chk", "Job_ID").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Job = DT_TbL_Job.DefaultView.ToTable(false, "Job_ID").Select().CopyToDataTable();
                    S_job_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Job, "Job_ID", out S_job_str);
                }
                catch
                {
                    S_job_str = "";
                    //DT_TbL_Job = struct_Dt_Tbl;
                }
                try
                {
                    DT_TbL_Job_R = Temp_Job_R_Tbl.DefaultView.ToTable(false, "Chk", "Job_ID").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Job_R = DT_TbL_Job_R.DefaultView.ToTable(false, "Job_ID").Select().CopyToDataTable();
                    R_job_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Job_R, "Job_ID", out R_job_str);
                }
                catch
                {
                    R_job_str = "";
                    //DT_TbL_Job_R = struct_Dt_Tbl;
                }

                try
                {
                    DT_TbL_Doc = Temp_Doc_Tbl.DefaultView.ToTable(false, "Chk", "Fmd_ID").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Doc = DT_TbL_Doc.DefaultView.ToTable(false, "Fmd_ID").Select().CopyToDataTable();
                    S_Doc_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Doc, "Fmd_ID", out S_Doc_str);
                }
                catch
                {
                    S_Doc_str = "";
                    //DT_TbL_Doc = struct_Dt_Tbl;
                }
                try
                {
                    DT_TbL_Doc_R = Temp_Doc_R_Tbl.DefaultView.ToTable(false, "Chk", "Fmd_ID").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Doc_R = DT_TbL_Doc_R.DefaultView.ToTable(false, "Fmd_ID").Select().CopyToDataTable();
                    R_Doc_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Doc_R, "Fmd_ID", out R_Doc_str);
                }
                catch
                {
                     R_Doc_str = "";
                    //DT_TbL_Doc_R = struct_Dt_Tbl;
                }

                try
                {
                    DT_TbL_Users = Temp_USERS_Tbl.DefaultView.ToTable(false, "Chk", "User_id").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Users = DT_TbL_Users.DefaultView.ToTable(false, "User_id").Select().CopyToDataTable();
                     User_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Users, "User_id", out User_str);
                }
                catch
                {
                    User_str = "";
                    //DT_TbL_Users = struct_Dt_Tbl;
                }
                //DT_TbL_Pur


                try
                {
                    DT_TbL_Pur = Temp_purpose_tbl.DefaultView.ToTable(false, "Chk", "Case_purpose_id").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Pur = DT_TbL_Pur.DefaultView.ToTable(false, "Case_purpose_id").Select().CopyToDataTable();
                    purpose_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Pur, "Case_purpose_id", out purpose_str);
                }
                catch
                {
                     purpose_str = "";
                    //DT_TbL_Pur = struct_Dt_Tbl;
                }
                //DT_TbL_Cur
                try
                {
                    DT_TbL_Cur = Temp_Cur_tbl.DefaultView.ToTable(false, "Chk","cur_id").Select("Chk > 0").CopyToDataTable();
                    DT_TbL_Cur = DT_TbL_Cur.DefaultView.ToTable(false, "cur_id").Select().CopyToDataTable();
                    cur_str = "";
                    MyGeneral_Lib.ColumnToString(DT_TbL_Cur,"cur_id", out cur_str);
                }
                catch
                {
                    cur_str = "";
                    //DT_TbL_Cur = struct_Dt_Tbl;
                }
                //***//DT_Tbl_Nat_R
                //DataTable DT_Rcon_ = new DataTable();
                //DataTable DT_Scon_ = new DataTable();
               
            

            if(Cbo_R_Type_Id.SelectedIndex==0)
            {
                R_type_id = 2;
            }
            if(Cbo_R_Type_Id.SelectedIndex==1)
            {
                R_type_id =1;
            }

            //getdata();

            try{

//                @from_Casedate		varchar(12) ='20160402',
//@to_Casedate		varchar(12)='20160503' ,
//@R_Type_id			tinyint  =2 , 
//@case_id			tinyint  = 103,
//@R_amount           decimal(20,3)=100,
//@Rang               char(5)='<',
//@sCon_Tbl           Varchar(max),
//@sFmd_Tbl           Varchar(max),
//@snat_Tbl           Varchar(max),
//@sjob_Tbl           Varchar(max),
//@rCon_Tbl           Varchar(max),
//@rFmd_Tbl           Varchar(max),
//@rnat_Tbl           Varchar(max),
//@rjob_Tbl           Varchar(max),
//@Cur_Tbl            Varchar(max),
//@Case_purpose_Rem   Varchar(max),
//@login_tbl           Varchar(max)

            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "aml_Remittances_Query";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;

            connection.SQLCMD.Parameters.AddWithValue("@from_Casedate",from_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@to_Casedate",to_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@R_Type_id",R_type_id);
            connection.SQLCMD.Parameters.AddWithValue("@case_id",Cmb_Case_id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@R_amount",Convert.ToDecimal(Txt_Amount.Text.Trim()));
            connection.SQLCMD.Parameters.AddWithValue("@Rang",cmb_range.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@sCon_Tbl",S_Con_str);
            connection.SQLCMD.Parameters.AddWithValue("@sFmd_Tbl",S_Doc_str);

            connection.SQLCMD.Parameters.AddWithValue("@snat_Tbl",S_nat_str);
            connection.SQLCMD.Parameters.AddWithValue("@sjob_Tbl",S_job_str);
            connection.SQLCMD.Parameters.AddWithValue("@rCon_Tbl",R_Con_str);
  //****
            connection.SQLCMD.Parameters.AddWithValue("@rFmd_Tbl",R_Doc_str);
            connection.SQLCMD.Parameters.AddWithValue("@rnat_Tbl",R_nat_str);
            connection.SQLCMD.Parameters.AddWithValue("@rjob_Tbl",R_job_str);
            connection.SQLCMD.Parameters.AddWithValue("@Cur_Tbl",cur_str);
            connection.SQLCMD.Parameters.AddWithValue("@Case_purpose_Rem",purpose_str);
            connection.SQLCMD.Parameters.AddWithValue("@login_tbl",User_str);

              IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "aml_Remittances_Query_Tbl");
          //  connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl1");
          //  connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl2");
         //   connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Account_Balancing_new_tbl3");
            
    
        obj.Close();
        connection.SQLCS.Close();
        connection.SQLCMD.Parameters.Clear();
        connection.SQLCMD.Dispose();
    }
                

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }

            //TxtFromDate.Text="00/00/0000";
            //TxtToDate.Text = "00/00/0000";
            //TxtFromDate.Checked = false;
            //TxtToDate.Checked = false;

            connection.SQLCMD.Parameters.Clear();
            aml_Rem_Q_DT = connection.SQLDS.Tables["aml_Remittances_Query_Tbl"];
            Integration_Accounting_Sys.remittences_online.Inquery_aml_Remittances Frm = new Integration_Accounting_Sys.remittences_online.Inquery_aml_Remittances(aml_Rem_Q_DT);
            this.Visible = false;
            Frm.ShowDialog(this);
           // Setting_control_online_rem_Load(null, null);
            this.Visible = true;
            this.Enabled = true;
              


              
              

                //Setting_control_online_rem_Load(null, null);
                //this.Close();

        }




    }
}


//-------------------------------------------------------------------- .*