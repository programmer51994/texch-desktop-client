﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class search_customer_aml : Form
    {
        #region MyRegion
        string SqlTxt = "";
        string SqlTxt1 = "";        
        string Rem_No = "";
        string Rem_No2 = "";       
        string @format = "dd/MM/yyyy";
        string @null = " ";
        string Name = "";
        string per_Name = "";          
        bool Change = false;       
        BindingSource PerBs_S = new BindingSource();
        BindingSource Cur_Bs = new BindingSource();
        BindingSource Cur_Bs2 = new BindingSource();
        BindingSource Change_cbo = new BindingSource();
        BindingSource Change_cbo2 = new BindingSource();
        DataTable ExpDt_Term_Acc = new DataTable();
       
        #endregion

        public search_customer_aml()
        {
            InitializeComponent();          
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);          
           Grd_RemIn.AutoGenerateColumns = false;
           Grd_RemOut.AutoGenerateColumns = false;
           Grd_Person_Info.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "FRM_EDOC_NA";
                Column7.DataPropertyName = "rFRM_EDOC_NA";
                Column26.DataPropertyName = "r_E_NAT_NAME";
                Column37.DataPropertyName = "ECASE_NA";
                Column22.DataPropertyName = "ECASE_NA";
                dataGridViewTextBoxColumn17.DataPropertyName = "sFRM_EDOC_NA";
                Column25.DataPropertyName = "s_E_NAT_NAME";
                Column6.DataPropertyName = "R_ECUR_NAME";
                dataGridViewTextBoxColumn9.DataPropertyName = "R_ECUR_NAME";
               
                
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {

        }

        private void Acc_Cust_Operations_Load(object sender, EventArgs e)
        {
            Txt_Nrec_date_From.CustomFormat = @null;
            Txt_Nrec_date_to.CustomFormat = @null;
            try
            {
         
           
                if (connection.Lang_id == 2)
                {
                    Cbo_sing.Items.Add("matching");
                    Cbo_sing.Items.Add("starts with");
                    Cbo_sing.Items.Add ( "Wherever found");
                   
                }
                else
                {
                    Cbo_sing.Items.Add ("مطابق");
                    Cbo_sing.Items.Add( "يبدأ ب");
                    Cbo_sing.Items.Add("اينما وجد");
                }
                Cbo_sing.SelectedIndex = 0;
            }
            catch
            {
            }
            SqlTxt = "SELECT Fmd_ID, Fmd_AName, Fmd_EName, Fmd_Code FROM Fmd_Tbl";
            connection.SqlExec(SqlTxt, "Fmd_Tbl");
            Cbo_doc_type.DataSource = connection.SQLDS.Tables["Fmd_Tbl"];
            Cbo_doc_type.ValueMember = "Fmd_ID";
            Cbo_doc_type.DisplayMember = connection.Lang_id == 1 ? "Fmd_AName" : "Fmd_EName";


            SqlTxt1 = "select ACUST_NAME ,ECUST_NAME,b.cust_id from TERMINALS a ,CUSTOMERS b  where a.cust_id= b.CUST_ID";
            connection.SqlExec(SqlTxt1, "Term_Tbl");
            Cbo_Term.DataSource = connection.SQLDS.Tables["Term_Tbl"];
            Cbo_Term.ValueMember = "cust_id";
            Cbo_Term.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";

        }

        private void Grd_Person_Info_SelectionChanged(object sender, EventArgs e)
        {

            if (Change)
            {
                per_Name = ((DataRowView)PerBs_S.Current).Row["name"].ToString();
               
              
                try
                {
                    Cur_Bs.DataSource = connection.SQLDS.Tables["Dt_Get1"].Select("r_name like " + "'" + per_Name + "'").CopyToDataTable();
                    Grd_RemIn.DataSource = Cur_Bs;
                }
                catch
                {
                    Grd_RemIn.DataSource = new BindingSource();
                }


                try
                {
                    Cur_Bs2.DataSource = connection.SQLDS.Tables["Dt_Get"].Select("s_name like " + "'" + per_Name + "'").CopyToDataTable();
                    Grd_RemOut.DataSource = Cur_Bs2;
                }
                catch 
                { 
                    Grd_RemOut.DataSource = new BindingSource(); 
                }

                Txt_RCount.Text = Grd_RemIn.Rows.Count.ToString();
                textBox1.Text = Grd_RemOut.Rows.Count.ToString();

                try
                {
                    Cbo_Rcur2.Enabled = true;
                    Cbo_Rcur2.DataSource = connection.SQLDS.Tables["Dt_Get1"].DefaultView.ToTable(true, connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "R_Cur_Id", "r_name").Select("r_name like " + "'" + per_Name + "'").CopyToDataTable();
                    Cbo_Rcur2.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
                    Cbo_Rcur2.ValueMember = "R_Cur_Id";
                }
                catch
                { 
                    Cbo_Rcur2.DataSource = null;
                    Cbo_Rcur2.Enabled = false;
                }
                try
                {
                    Cbo_RCur.Enabled = true;
                    Cbo_RCur.DataSource = connection.SQLDS.Tables["Dt_Get"].DefaultView.ToTable(true, connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "R_Cur_Id", "s_name").Select("s_name like " + "'" + per_Name + "'").CopyToDataTable();
                    Cbo_RCur.ValueMember = "R_Cur_Id";
                    Cbo_RCur.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME"; 
                   
                  
                }
                catch
                {
                    Cbo_RCur.DataSource = null;
                    Cbo_RCur.Enabled = false;
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            Change = false;
            string[] Tbl = { "Dt_Get", "Dt_Get1" ,"Dt_Get2"};
            foreach (string str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(str))
                {
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[str]);
                }
            }
           
            if (Cbo_sing.SelectedIndex == 0)
            {
                Name =  txt_name.Text ;
            }
            if (Cbo_sing.SelectedIndex == 1)
            {
                Name =  txt_name.Text + "%";
            }
            if (Cbo_sing.SelectedIndex == 2)
            {
                Name =  "%" + txt_name.Text + "%" ;
            }
          

            string from_Date = Txt_Nrec_date_From.Value.ToString("yyyy/MM/dd");
            string to_Date = Txt_Nrec_date_to.Value.ToString("yyyy/MM/dd");
            if (Txt_Nrec_date_From.Checked == false && Txt_Nrec_date_to.Checked == false)
            {
                from_Date = "";
                to_Date = "";
            }
            try
            {			
		
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[search_customer]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@cust_name", Name.Trim());                                
                connection.SQLCMD.Parameters.AddWithValue("@phon_no", Txt_phone.Text);
                connection.SQLCMD.Parameters.AddWithValue("@doc_id", Cbo_doc_type.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@doc_no", Txt_doc_no.Text);
                connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_rem_no.Text);
                connection.SQLCMD.Parameters.AddWithValue("@from_nrec_date", from_Date);
                connection.SQLCMD.Parameters.AddWithValue("@to_nrec_date", to_Date);
                connection.SQLCMD.Parameters.AddWithValue("@cust_id", Cbo_Term.SelectedValue);
                //connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 50).Value = "";
                //connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Dt_Get");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Dt_Get1");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Dt_Get2");
                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
            PerBs_S.DataSource = connection.SQLDS.Tables["Dt_Get2"];
            Cur_Bs.DataSource = connection.SQLDS.Tables["Dt_Get1"];
            Cur_Bs2.DataSource = connection.SQLDS.Tables["Dt_Get"];
           // Grd_Person_Info.DataSource = PerBs_S;
            if (connection.SQLDS.Tables["Dt_Get2"].Rows.Count > 0)
            {
              Grd_Person_Info.DataSource = PerBs_S;
                Change = true;
                Grd_Person_Info_SelectionChanged(null, null);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد حوالة تطابق الشروط" : "No Remittences matched the condition", MyGeneral_Lib.LblCap);
                
              
               Grd_RemIn.DataSource = new BindingSource();
               Grd_RemOut.DataSource = new BindingSource();
              // Grd_Person_Info.DataSource = new BindingSource();
               Txt_RCount.Text = "";
               Txt_RSum_Amount.Text = "0.000";
               Cbo_Rcur2.DataSource = new BindingSource();
               textBox1.Text = "";
               Txt_SSum_Amount.Text = "0.000";
               Cbo_RCur.DataSource = new BindingSource();
               return;
            }
          
           
            
        }

        private void Txt_Nrec_date_From_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_Nrec_date_From.Checked == true)
            {
                Txt_Nrec_date_From.Format = DateTimePickerFormat.Custom;
                Txt_Nrec_date_From.CustomFormat = @format;
            }
            else 
            {
                Txt_Nrec_date_From.Format = DateTimePickerFormat.Custom;
                Txt_Nrec_date_From.CustomFormat = @null;
            }

        }

        private void Txt_Nrec_date_to_ValueChanged(object sender, EventArgs e)
        {

            if (Txt_Nrec_date_to.Checked == true)
            {
                Txt_Nrec_date_to.Format = DateTimePickerFormat.Custom;
                Txt_Nrec_date_to.CustomFormat = @format;
            }
            else
            {
                Txt_Nrec_date_to.Format = DateTimePickerFormat.Custom;
                Txt_Nrec_date_to.CustomFormat = @null;
            }

        }

      

        private void Cbo_RCur_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (Cbo_RCur.DataSource != null)
                {
                    Txt_SSum_Amount.Text = connection.SQLDS.Tables["Dt_Get"].Compute("Sum(R_amount)", "s_name like " + "'" + per_Name + "'" + "and" + " R_Cur_Id= " + Cbo_RCur.SelectedValue).ToString();
                }
                 else
                {
                    Txt_SSum_Amount.Text = "0.000";
                }
            }
            catch { }
        }

      

        private void Cbo_Rcur2_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (Cbo_Rcur2.DataSource != null)
                {
                    Txt_RSum_Amount.Text = connection.SQLDS.Tables["Dt_Get1"].Compute("Sum(R_amount)", "r_name like " + "'" + per_Name + "'" + "and" + " R_Cur_Id= " + Cbo_Rcur2.SelectedValue).ToString();
                }
                else
                {
                    Txt_RSum_Amount.Text = "0.000";
                }
            }
            catch { }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Dt_Get2"].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DataTable Exp_Dt = connection.SQLDS.Tables["Dt_Get2"].DefaultView.ToTable(false, "name", connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_EDOC_NA", "doc_no", "doc_issue").Select().CopyToDataTable();

                DataTable[] _EXP_DT = { Exp_Dt };
                DataGridView[] Export_GRD = { Grd_Person_Info };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, DT, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف معلومات الزبون" : "Person information Reveal ");
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            else
            {

                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات" : " No remittences", MyGeneral_Lib.LblCap);
                return;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
            if (Grd_RemIn.RowCount == 0 && Grd_RemOut.RowCount != 0)
            {
                Rem_No2 = ((DataRowView)Cur_Bs2.Current).Row["Rem_No"].ToString();
                DataTable Date_Tbl = new DataTable();
                DataTable ExpDt_Term = connection.SQLDS.Tables["Dt_Get2"].DefaultView.ToTable(true, "name", connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_EDOC_NA", "doc_no", "doc_issue").Select("name like " + "'" + per_Name + "'").CopyToDataTable();

                DataTable ExpDt_Term_Acc = new DataTable();
                DataTable ExpDt_Term_Acc2 = connection.SQLDS.Tables["Dt_Get"].DefaultView.ToTable(true, "rem_no", "s_name", "r_name", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA", "s_doc_no",
                    "s_doc_issue", "s_doc_ida", "s_doc_eda", connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME", "s_address", "s_phone", "T_purpose", "r_phone", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "nrec_date").Select("s_name like " + "'" + per_Name + "'" + " and " + " Rem_No like " + "'" + Rem_No2 + "'").CopyToDataTable();


                DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc, ExpDt_Term_Acc2 };
                DataGridView[] Export_GRD = { Grd_Person_Info, Grd_RemIn, Grd_RemOut };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, Date_Tbl, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف معلومات الزبون" : "Person information Reveal ");
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }

            if (Grd_RemOut.RowCount == 0 && Grd_RemIn.RowCount != 0)
            {
                Rem_No = ((DataRowView)Cur_Bs.Current).Row["Rem_No"].ToString();
                DataTable Date_Tbl = new DataTable();
                DataTable ExpDt_Term = connection.SQLDS.Tables["Dt_Get2"].DefaultView.ToTable(true, "name", connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_EDOC_NA", "doc_no", "doc_issue").Select("name like " + "'" + per_Name + "'").CopyToDataTable();

                DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Dt_Get1"].DefaultView.ToTable(true, "rem_no", "r_name", "s_name", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ACUR_NAME", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no",
                    "r_doc_issue", "r_doc_ida", "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_A_NAT_NAME", "r_address", "R_phone", "T_purpose", "S_phone", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "nrec_date").Select("r_name like " + "'" + per_Name + "'" + " and " + " Rem_No like " + "'" + Rem_No + "'").CopyToDataTable();
                 
                DataTable ExpDt_Term_Acc2 = new DataTable();
                

                DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc, ExpDt_Term_Acc2 };
                DataGridView[] Export_GRD = { Grd_Person_Info, Grd_RemIn, Grd_RemOut };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, Date_Tbl, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف معلومات حوالات الزبون" : "Person Remittences Information Reveal ");
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }

            if (Grd_RemIn.RowCount != 0 && Grd_RemOut.RowCount != 0)
            {
                Rem_No = ((DataRowView)Cur_Bs.Current).Row["Rem_No"].ToString();
                Rem_No2 = ((DataRowView)Cur_Bs2.Current).Row["Rem_No"].ToString();
                DataTable Date_Tbl = new DataTable();
                DataTable ExpDt_Term = connection.SQLDS.Tables["Dt_Get2"].DefaultView.ToTable(true, "name", connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_EDOC_NA", "doc_no", "doc_issue").Select("name like " + "'" + per_Name + "'").CopyToDataTable();

                DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Dt_Get1"].DefaultView.ToTable(true, "rem_no", "r_name", "s_name", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NA", "r_doc_no",
                    "r_doc_issue", "r_doc_ida", "r_doc_eda", connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME", "r_address", "R_phone", "T_purpose", "S_phone", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "nrec_date").Select("r_name like " + "'" + per_Name + "'" + " and " + " Rem_No like " + "'" + Rem_No + "'").CopyToDataTable();

                DataTable ExpDt_Term_Acc2 = connection.SQLDS.Tables["Dt_Get"].DefaultView.ToTable(true, "rem_no", "s_name", "r_name", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA", "s_doc_no",
                    "s_doc_issue", "s_doc_ida", "s_doc_eda", connection.Lang_id == 1 ? "s_A_NAT_NAME" : "sFRM_EDOC_NA", "s_address", "s_phone", "T_purpose", "r_phone", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "nrec_date").Select("s_name like " + "'" + per_Name + "'" + " and " + " Rem_No like " + "'" + Rem_No2 + "'").CopyToDataTable();

                
                DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc, ExpDt_Term_Acc2 };
                DataGridView[] Export_GRD = { Grd_Person_Info, Grd_RemIn, Grd_RemOut };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, Date_Tbl, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف معلومات حوالات الزبون" : " Person Remittences Information Reveal ");
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            if (Grd_RemIn.RowCount == 0 &&  Grd_RemOut.RowCount == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات" : " No Remittences", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void search_customer_aml_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Str = { "Dt_Get", "Dt_Get1","Dt_Get2" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private void Txt_Nrec_date_From_ValueChanged(object sender, EventArgs e)
        //{

        //}
        //------------------------
        //------------------------
       
        
    }
}