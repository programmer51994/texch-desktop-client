﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Printing;

namespace Integration_Accounting_Sys
{
    public partial class  Person_Document_main : Form
    {
        Byte[] img_data = new Byte[0];
        byte[] img = new byte[0];
        BindingSource _Bs_Grd_Img = new BindingSource();
        BindingSource _Bs_Img_Show = new BindingSource();
        bool Chang_Grd = false;
        DataTable Pic_Tbl = new DataTable();
        string fileName = "";
        string Person_AName = "";
        string Person_EName = "";
        int Person_id = 0;
        public Person_Document_main(string Per_AName, int Per_id, string Per_EName)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Person_AName = Per_AName;
            Person_EName = Per_EName;
            Person_id = Per_id;
         
        }
        private void Create_Pic_Tbl()
        {
            string[] Column = { "Fmd_ID", "Fmd_AName", "Fmd_EName", "Doc_Pic", "Doc_pic_name", "Flag", "path" };
            string[] DType = { "System.Int16", "System.String", "System.String", "System.Byte[]", "System.String", "System.Int16", "System.String" };
            Pic_Tbl = CustomControls.Custom_DataTable("Pic_Tbl", Column, DType);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            if (ADD_REM_CASH_DAILY.Del_Btn == 1 || Delivery_Add.Del_Btn == 1 || Rem_Confirmation.Del_Btn == 1 || Document_person_view.Del_Btn == 1)
            { BtnDel.Visible = false; }
            if (Convert.ToByte(connection.SqlExec("Select upload_flag from person_info  where per_id = " + Person_id, "Tbl_Person_Flag").Rows[0]["upload_flag"]) == 1)
            {
                try
                {
                    Chang_Grd = false;
                    Grd_Doc.ClearSelection();
                    Create_Pic_Tbl();
                    Grd_Doc.AutoGenerateColumns = false;
                    string Sql_text = "SELECT  top(1)  server_name, UID, PWD, db_name FROM Agencies" ;
                    connection.SqlExec(Sql_text, "server_Tbl");

                    string connStr = @"server = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["server_name"].ToString() + " ; database =" + connection.SQLDS.Tables["server_Tbl"].Rows[0]["db_name"].ToString()
                        + " ; user id = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["UID"].ToString() + " ; password = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["PWD"].ToString() + " ; connection timeout = 0";

                    SqlConnection SqlCon = new SqlConnection(connStr);

                    SqlCon.Open();
                    connection.SQLCMD.CommandText = "[dbo].[Main_person_Doc]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = SqlCon;
                    connection.SQLCMD.Parameters.AddWithValue("@Per_id", Person_id);
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "TBL_IMG_DOc_Cust");

                    obj.Close();
                    //connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    SqlCon.Close();

                    if (connection.SQLDS.Tables["TBL_IMG_DOc_Cust"].Rows.Count > 0)
                    {
                        Txt_per_name.Text = connection.SQLDS.Tables["TBL_IMG_DOc_Cust"].Rows[0][connection.Lang_id ==1?"Per_AName":"Per_EName"].ToString();
                        Chang_Grd = false;
                        _Bs_Grd_Img.DataSource = connection.SQLDS.Tables["TBL_IMG_DOc_Cust"].DefaultView.ToTable(true, "Flag_name","Flag_Ename", "Fmd_AName", "Fmd_EName", "Flag", "Count_rec", "Fmd_id").Select().CopyToDataTable();
                        Grd_Doc.DataSource = _Bs_Grd_Img;
                        if(connection.Lang_id==2)
                            {
                                Column2.DataPropertyName = "Fmd_eName";
                                Column1.DataPropertyName = "Flag_Ename";
                            }
                        Chang_Grd = true;
                        Grd_Doc_SelectionChanged(null, null);
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات للعرض لهذا الثانوي" : "No  info. for this customer", MyGeneral_Lib.LblCap);
                        this.Close();
                    }
                }

                catch (Exception _Err)
                {
                    connection.SQLCMD.Parameters.Clear();
                    MyErrorHandler.ExceptionHandler(_Err);

                }
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات للعرض لهذا الثانوي" : "No  info. for this customer", MyGeneral_Lib.LblCap);
                this.Close();
            }

            
        }
   


        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream mStream = new MemoryStream(byteArrayIn))
            {
                return Image.FromStream(mStream);
            }
        }

       

        private void Grd_Doc_SelectionChanged(object sender, EventArgs e)
        {
            if (Chang_Grd)
            {
                try
                {
                    pictureBox1.Show();
                    axAcroPDF2.Hide();
                    AddBtn.Show();
                    _Bs_Img_Show.DataSource = connection.SQLDS.Tables["TBL_IMG_DOc_Cust"].DefaultView.ToTable().Select("Flag = " + ((DataRowView)_Bs_Grd_Img.Current).Row["Flag"] + "and Fmd_AName like'" + ((DataRowView)_Bs_Grd_Img.Current).Row["Fmd_AName"] + "'").CopyToDataTable(); ;
                    img = (Byte[])((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"];
                    MemoryStream mem = new MemoryStream(img);
                    pictureBox1.Image = Image.FromStream(mem);
                }
                catch
                {
                    AddBtn.Hide();
                    pictureBox1.Hide();
                    axAcroPDF2.Show();
                     fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
                    _Bs_Img_Show.DataSource = connection.SQLDS.Tables["TBL_IMG_DOc_Cust"].DefaultView.ToTable().Select("Flag = " + ((DataRowView)_Bs_Grd_Img.Current).Row["Flag"] + "and Fmd_AName like'" + ((DataRowView)_Bs_Grd_Img.Current).Row["Fmd_AName"] + "'").CopyToDataTable();
                    byte[] fileData = (byte[])((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"];
                    File.WriteAllBytes(fileName, fileData);
                    this.axAcroPDF2.LoadFile(fileName);
                    this.axAcroPDF2.src = fileName;
                    this.axAcroPDF2.setShowToolbar(false);
                    this.axAcroPDF2.setView("FitH");
                   this.axAcroPDF2.setLayoutMode("SinglePage");
                    this.axAcroPDF2.Show();
                }
                if (_Bs_Img_Show.Count == 1)
                {
                    Btn_Previous.Enabled = false;
                    Btn_Next.Enabled = false;
                }
                else
                {
                    Btn_Previous.Enabled = true;
                    Btn_Next.Enabled = true;
                }
            }

        }

        private void Btn_Next_Click(object sender, EventArgs e)
        {

            _Bs_Img_Show.MovePrevious();
            try
            {
               
                img = (Byte[])((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"];
                MemoryStream mem = new MemoryStream(img);
                pictureBox1.Image = Image.FromStream(mem);
                pictureBox1.Show();
                axAcroPDF2.Hide();
            }
            catch
            {
               
                 fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
                byte[] fileData = (byte[])((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"];
                File.WriteAllBytes(fileName, fileData);
                this.axAcroPDF2.LoadFile(fileName);
                this.axAcroPDF2.src = fileName;
                this.axAcroPDF2.setShowToolbar(false);
                this.axAcroPDF2.setView("FitH");
                this.axAcroPDF2.setLayoutMode("SinglePage");
                this.axAcroPDF2.Show();
                pictureBox1.Hide();
            }


        }

        private void Btn_Previous_Click(object sender, EventArgs e)
        {
            _Bs_Img_Show.MoveNext();
            try
            {
                img = (Byte[])((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"];
                MemoryStream mem = new MemoryStream(img);
                pictureBox1.Image = Image.FromStream(mem);
                pictureBox1.Show();
                axAcroPDF2.Hide();
            }
            catch
            {
                 fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
                byte[] fileData = (byte[])((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"];
                File.WriteAllBytes(fileName, fileData);
                this.axAcroPDF2.LoadFile(fileName);
                this.axAcroPDF2.src = fileName;
                this.axAcroPDF2.setShowToolbar(false);
                this.axAcroPDF2.setView("FitH");
                this.axAcroPDF2.setLayoutMode("SinglePage");
                this.axAcroPDF2.Show();
                pictureBox1.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Sql_text = "SELECT top(1)  server_name, UID, PWD, db_name FROM Agencies  ";
            connection.SqlExec(Sql_text, "server_Tbl");

            string connStr = @"server = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["server_name"].ToString() + " ; database =" + connection.SQLDS.Tables["server_Tbl"].Rows[0]["db_name"].ToString()
                + " ; user id = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["UID"].ToString() + " ; password = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["PWD"].ToString() + " ; connection timeout = 100000";

            DataRow DRow1 = Pic_Tbl.NewRow();
            DRow1.SetField("fmd_id", ((DataRowView)_Bs_Grd_Img.Current).Row["fmd_id"]);
            DRow1.SetField("Fmd_AName", "");
            DRow1.SetField("Fmd_EName", "");
            DRow1.SetField("Doc_Pic", ((DataRowView)_Bs_Img_Show.Current).Row["Doc_Pic"]);
            DRow1.SetField("Flag", ((DataRowView)_Bs_Grd_Img.Current).Row["Flag"]);
            DRow1.SetField("path", "");
            DRow1.SetField("Doc_pic_name", "");

            Pic_Tbl.Rows.Add(DRow1);

            DataTable DT_pic = new DataTable();

            DT_pic = Pic_Tbl.DefaultView.ToTable(false, "Fmd_ID", "Fmd_AName", "Fmd_EName", "Doc_Pic", "Doc_pic_name", "Flag").Select().CopyToDataTable();
            connection.SQLCMD.Parameters.AddWithValue("@Doc_Pic_Tbl", DT_pic);
            //connection.SQLCMD.Parameters.AddWithValue("@T_id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@User_id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Per_id", Person_id);
            connection.SQLCMD.Parameters.AddWithValue("@Per_AName", Txt_per_name.Text);
            connection.SQLCMD.Parameters.AddWithValue("@Per_EName", Txt_per_name.Text);
            connection.SQLCMD.Parameters.AddWithValue("@User_Name", connection.User_Name);
            connection.SQLCMD.Parameters.AddWithValue("@From_id", 2);//اضافة
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Count", SqlDbType.TinyInt).Value = 0;
            connection.SQLCMD.Parameters["@Param_Count"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Person_Doc", connection.SQLCMD, connStr);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                //this.Enabled = true;
                return;
            }
            
            if (Convert.ToInt32(connection.SQLCMD.Parameters["@Param_Count"].Value ) == 0)
            {
                string Sqltext = "Update person_info set upload_flag = 0 where per_id = " + Person_id;
                connection.SqlExec(Sqltext, "TBL_Upload_Chk");
            }
            Pic_Tbl.Clear();
            connection.SQLCMD.Parameters.Clear();


            string[] Used_Tbl = { "TBL_IMG_DOc_Cust" };
            foreach (string Tbl in Used_Tbl)
            {
                connection.SQLDS.Tables[Tbl].Dispose();
                connection.SQLDS.Tables[Tbl].Clear();
                connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
            Form1_Load(null, null);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
           
            Chang_Grd = false;
            string machineName = System.Environment.GetEnvironmentVariable("TEMP");
            DirectoryInfo dir = new DirectoryInfo(machineName + "/");

            foreach (FileInfo files in dir.GetFiles())
            {
                try
                {
                    files.Delete();
                }
                catch { };
            }

            foreach (DirectoryInfo dirs in dir.GetDirectories())
            {
                try
                {
                    dirs.Delete(true);
                }
                catch { };
            }


            string[] Used_Tbl = { "TBL_IMG_DOc_Cust" };
            foreach (string Tbl in Used_Tbl)
            {
                //connection.SQLDS.Tables[Tbl].Dispose();
                //connection.SQLDS.Tables[Tbl].Clear();
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }
            }

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
                PrintDocument pd = new PrintDocument();
                pd.PrintPage += new PrintPageEventHandler(pqr);
                pd.Print();     
        }
        void pqr(object o, PrintPageEventArgs e)
        {
            Bitmap myBitmap1 = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(myBitmap1, new Rectangle(0, 0, pictureBox1.Width, pictureBox1.Height));
            e.Graphics.DrawImage(myBitmap1, 0, 0);
            myBitmap1.Dispose();
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Txt_per_name_TextChanged(object sender, EventArgs e)
        {

        }
    }
}