﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Person_Info_Main : Form
    {
        bool change = false;
        string Sql_Text = "";
        Int16 Con_Id = 0;
      
        Int16 Cit_id = 0;
        string Birth_date = "0000/00/00";
        string Doc_date = "0000/00/00";
        string Doc_Edate = "0000/00/00";
        DataTable Per_inf_TBL = new DataTable();
        public static BindingSource _Bs_Person_Info = new BindingSource();
        public Person_Info_Main()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, new MaskedTextBox());
            Grd_Per.AutoGenerateColumns = false;
            if (Customer_Main.ISAuthorized == 1)
            { BND.Visible = false; }
            //if (connection.Lang_id == 2)
            //{
            //    Column4.DataPropertyName = "PerFRM_EDOC_NA";
            //    Column1.DataPropertyName = "Per_EName";
            //}

        }

        private void Grd_Bind()
        {
            change = false;
            if (Customer_Main.ISAuthorized != 1)// person_info Table 
            {

                _Bs_Person_Info.DataSource = connection.SqlExec("main_person_info '"
                                                                + TxtPer_Name.Text.Trim() + "',"
                                                                //+ Con_Id + ","
                                                                //+ Cit_id + ","0
                                                                + "'person_info'", "per_Tbl");




                if (connection.SQLDS.Tables["per_Tbl"].Rows.Count > 0)
                {
                    Grd_Per.DataSource = _Bs_Person_Info;
                    change = true;
                    Grd_Per_SelectionChanged(null, null);

                }
            }
            else // From Customers main has authorized Person
            {
                if (connection.SQLDS.Tables["per_Tbl"].Rows.Count > 0)
                {
                    _Bs_Person_Info.DataSource = connection.SQLDS.Tables["per_Tbl"];
                    Grd_Per.DataSource = _Bs_Person_Info;
                    change = true;
                    Grd_Per_SelectionChanged(null, null);

                }

            }
            TxtCityAR.DataBindings.Clear();
            TxtNat_NameAR.DataBindings.Clear();
            TxtPhoneAR.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            Txt_Another_phone.DataBindings.Clear();
            Txt_GenderAR.DataBindings.Clear();
            Txt_OccupationAR.DataBindings.Clear();
            Txt_Post_code.DataBindings.Clear();
            Txt_StateAR.DataBindings.Clear();
            Txt_StreetAR.DataBindings.Clear();
            Txt_SuburbAR.DataBindings.Clear();
            txt_resd_flag.DataBindings.Clear();

            Txt_OccupationEN.DataBindings.Clear();
            Txt_GenderEN.DataBindings.Clear();
            TxtNat_NameEN.DataBindings.Clear();
            TxtCityEN.DataBindings.Clear();
            Txt_StateEN.DataBindings.Clear();
            Txt_StreetEN.DataBindings.Clear();
            Txt_SuburbEN.DataBindings.Clear();
            SOCIAL_NO.DataBindings.Clear();

            


            TxtCityAR.DataBindings.Add("Text", _Bs_Person_Info,"Per_ACITY_NAME" );
            TxtNat_NameAR.DataBindings.Add("Text", _Bs_Person_Info, "Per_A_NAT_NAME" );
            Txt_GenderAR.DataBindings.Add("Text", _Bs_Person_Info,  "Per_Gender_Aname");
            TxtPhoneAR.DataBindings.Add("Text", _Bs_Person_Info, "per_Phone");
            TxtEmail.DataBindings.Add("Text", _Bs_Person_Info, "per_Email");
            Txt_Another_phone.DataBindings.Add("Text", _Bs_Person_Info, "per_Another_Phone");
            Txt_OccupationAR.DataBindings.Add("Text", _Bs_Person_Info, "per_Occupation");
            Txt_Post_code.DataBindings.Add("Text", _Bs_Person_Info, "Per_Post_Code");
            Txt_StateAR.DataBindings.Add("Text", _Bs_Person_Info, "Per_State");
            Txt_StreetAR.DataBindings.Add("Text", _Bs_Person_Info, "Per_Street");
            Txt_SuburbAR.DataBindings.Add("Text", _Bs_Person_Info, "Per_Suburb");
            if (connection.Lang_id == 1)
            {
                txt_resd_flag.DataBindings.Add("Text", _Bs_Person_Info, "resd_flag_Aname");
            }
            else
            {
                txt_resd_flag.DataBindings.Add("Text", _Bs_Person_Info, "resd_flag_Ename");
            }
            TxtCityEN.DataBindings.Add("Text", _Bs_Person_Info,"Per_ECITY_NAME");
            TxtNat_NameEN.DataBindings.Add("Text", _Bs_Person_Info,"Per_E_NAT_NAME");
            Txt_GenderEN.DataBindings.Add("Text", _Bs_Person_Info, "Per_Gender_Ename");
            Txt_StateEN.DataBindings.Add("Text", _Bs_Person_Info, "Per_EState");
            Txt_StreetEN.DataBindings.Add("Text", _Bs_Person_Info, "Per_EStreet");
            Txt_SuburbEN.DataBindings.Add("Text", _Bs_Person_Info, "Per_ESuburb");
            Txt_OccupationEN.DataBindings.Add("Text", _Bs_Person_Info, "EOccupation");
            SOCIAL_NO.DataBindings.Add("Text", _Bs_Person_Info, "Social_No");
        }



        private void Person__Info_Main_Load(object sender, EventArgs e)
        {





            change = false;

            //Sql_Text = " select distinct per_City_ID,per_Coun_ID,Per_ACITY_NAME + '_' + Per_ACOUN_NAME as Per_ACITY_NAME,Per_ECITY_NAME + '_' + Per_ECOUN_NAME as Per_ECITY_NAME from Person_Info  ";
            //CboCountry.ComboBox.DataSource = connection.SqlExec(Sql_Text, "Country_Tbl");
            //CboCountry.ComboBox.DisplayMember = connection.Lang_id == 1 ? "Per_ACITY_NAME" : "Per_ECITY_NAME";
            //CboCountry.ComboBox.ValueMember = "per_City_ID";

            Grd_Bind();

            if (connection.SQLDS.Tables["per_Tbl"].Rows.Count > 0 )
            {
            UpdBtn.Enabled=true;
             BtnDel.Enabled=true;
              label39.Enabled=true;
            }

            else
            {
             UpdBtn.Enabled=false;
             BtnDel.Enabled=false;
              label39.Enabled=false;
            
            }
            change = true;

        }


   


        private void SearchBtn_Click(object sender, EventArgs e)
        {
            //Con_Id = Convert.ToInt16(connection.SQLDS.Tables["Country_Tbl"].Rows[CboCountry.SelectedIndex]["per_Coun_ID"]);
            //Cit_id = Convert.ToInt16(CboCountry.ComboBox.SelectedValue);
            Grd_Bind();
        }

        private void Grd_Per_SelectionChanged(object sender, EventArgs e)
        {
            if (change)
            {


                Int32 upload_flag = 0;
                upload_flag = Convert.ToInt32(((DataRowView)_Bs_Person_Info.Current).Row["upload_flag"]);
                if (upload_flag == 1)
                {
                    label38.Enabled = true;

                }
                else
                {
                    label38.Enabled = false;
                }

                LblRec.Text = connection.Records(_Bs_Person_Info);

            
            }
        }

        private void Btn_Hst_Click(object sender, EventArgs e)
        {
            change = false;
            try
            {
                DataRowView DRV = _Bs_Person_Info.Current as DataRowView;
                DataRow DR = DRV.Row;
                int Per_ID = DR.Field<int>("Per_ID");
                Hst_Person_Info_main HstFrm = new Hst_Person_Info_main(Per_ID);
                this.Visible = false;
                HstFrm.ShowDialog(this);
                this.Visible = true;
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات تاريخية " : "No Information", MyGeneral_Lib.LblCap);

                return;
            }



        }

        private void Person__Info_Main_FormClosed(object sender, FormClosedEventArgs e)
        {

            change = false;

            string[] Used_Tbl = { "Country_Tbl", "per_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {

            change = false;
            Person_Info_Add_Upd AddFrm = new Person_Info_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            TxtPer_Name.Text = "";
            Person__Info_Main_Load(sender, e);
            this.Visible = true;
        }

        private void UpdBtn_Click(object sender, EventArgs e)
        {

            change = false;
            Person_Info_Add_Upd AddFrm = new Person_Info_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Person__Info_Main_Load(sender, e);
            this.Visible = true;
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
             DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد حذف الزبون المعرف؟" :
                "Do you want to delete this customer", MyGeneral_Lib.LblCap,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
             if (Dr == DialogResult.Yes)
             {
                 Int64 Per_id = 0;
                 Per_id = Convert.ToInt64(((DataRowView)_Bs_Person_Info.Current).Row["Per_id"]);

                 connection.SQLCMD.Parameters.AddWithValue("@per_id ", Per_id);
                 connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                 connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                 connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                 connection.SqlExec("Person_info_delete", connection.SQLCMD);



                 if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                         !(Int64.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Per_id)))
                 {
                     MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                     connection.SQLCMD.Parameters.Clear();
                     return;
                 }
                 connection.SQLCMD.Parameters.Clear();

                 //connection.SQLCMD.Parameters.AddWithValue("@per_id", Convert.ToInt16(((DataRowView)_Bs_Person_Info.Current).Row["per_id"]));
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_AName", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_EName", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_City_ID", 0);
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Coun_ID", 0);
                 ////connection.SQLCMD.Parameters.AddWithValue("@per_Address", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_Birth_place", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Phone", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Another_Phone", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Email", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_ID", 0);
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_NO", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_Date", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_EDate", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Frm_Doc_IS", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Nationalty_ID", 0);
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Birth_day", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Gender_ID", 0);
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Occupation", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@per_Mother_name", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@User_ID", connection.user_id);
                 //connection.SQLCMD.Parameters.AddWithValue("@COUN_K_PH", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_ACITY_NAME", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_ECITY_NAME", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_ACOUN_NAME ", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_ECOUN_NAME", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_Street", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_Suburb", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_Post_Code", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_State", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@PerFRM_ADOC_NA", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@PerFRM_EDOC_NA ", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_A_NAT_NAME", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_E_NAT_NAME ", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_Gender_Aname", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@Per_Gender_Ename", "");
                 //connection.SQLCMD.Parameters.AddWithValue("@FORM_Id ", 3);

                 //connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                 //connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                 //connection.SqlExec("Person_info_Add_Upd_1", connection.SQLCMD);


                 ////MyGeneral_Lib.Copytocliptext("Person_info_Add_Upd", connection.SQLCMD);
                 ////connection.scalar("Person_info_Add_Upd", connection.SQLCMD);

                 //if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                 //{
                 //    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                 //    connection.SQLCMD.Parameters.Clear();
                 //    return;
                 //}

                 Person__Info_Main_Load(null, null);


             }

        }

        private void AllBtn_Click(object sender, EventArgs e)
        {
            TxtPer_Name.Text = "";
          //  CboCountry.ComboBox.SelectedIndex = 0;
            Grd_Bind();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {


            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1)// يملك خاصية رفع الوثائق
            {

                int Per_id = Convert.ToInt32(((DataRowView)_Bs_Person_Info.Current).Row["Per_id"]);
                string Per_AName = ((DataRowView)_Bs_Person_Info.Current).Row["Per_AName"].ToString();
                string Per_EName = ((DataRowView)_Bs_Person_Info.Current).Row["Per_EName"].ToString();


                Person_Document_main UpdFrm = new Person_Document_main(Per_AName, Per_id, Per_EName);
                this.Visible = false;
                UpdFrm.ShowDialog(this);
                Person__Info_Main_Load(sender, e);
                this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Btn_Add_Doc_Img_Click(object sender, EventArgs e)
        {
             
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1  )// يملك خاصية رفع الوثائق
            {


                int Per_id = Convert.ToInt32(((DataRowView)_Bs_Person_Info.Current).Row["Per_id"]);
                string Per_AName = ((DataRowView)_Bs_Person_Info.Current).Row["Per_AName"].ToString();
                string Per_EName = ((DataRowView)_Bs_Person_Info.Current).Row["Per_EName"].ToString();


                Person_Document_Add_Upd UpdFrm = new Person_Document_Add_Upd(Per_AName, Per_id, Per_EName);
                this.Visible = false;
                UpdFrm.ShowDialog(this);
                Person__Info_Main_Load(sender, e);
                this.Visible = true;

            }
            else
            {

               
                    MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " ", MyGeneral_Lib.LblCap);
                    return;
                

            }
        }
    }
}