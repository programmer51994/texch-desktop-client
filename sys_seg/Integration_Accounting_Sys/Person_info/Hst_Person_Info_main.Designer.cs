﻿namespace Integration_Accounting_Sys
{
    partial class Hst_Person_Info_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SOCIAL_NO = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_GenderEN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_OccupationEN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtNat_NameEN = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_GenderAR = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_Post_code = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Txt_OccupationAR = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_Another_phone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPhoneAR = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtNat_NameAR = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_StreetEN = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_SuburbEN = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_StateEN = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCityEN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_StateAR = new System.Windows.Forms.TextBox();
            this.Txt_SuburbAR = new System.Windows.Forms.TextBox();
            this.Txt_StreetAR = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtCityAR = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Hst_Grd_Per = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Hst_Grd_Per)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(1088, 384);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(2, 158);
            this.flowLayoutPanel8.TabIndex = 601;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(18, 349);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(2, 225);
            this.flowLayoutPanel5.TabIndex = 600;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 592);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1084, 22);
            this.statusStrip1.TabIndex = 599;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(226, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel2.TabIndex = 86;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(1097, 387);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(2, 155);
            this.flowLayoutPanel6.TabIndex = 579;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(18, 571);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1056, 2);
            this.flowLayoutPanel1.TabIndex = 578;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(18, 347);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1056, 2);
            this.flowLayoutPanel3.TabIndex = 572;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(226, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel4.TabIndex = 86;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-14, 40);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1110, 2);
            this.flowLayoutPanel7.TabIndex = 569;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(15, 13);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 568;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(101, 10);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(279, 23);
            this.TxtUser.TabIndex = 567;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1072, 347);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(2, 225);
            this.flowLayoutPanel9.TabIndex = 602;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(658, 349);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(2, 223);
            this.flowLayoutPanel10.TabIndex = 601;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Maroon;
            this.label26.Location = new System.Drawing.Point(662, 335);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 14);
            this.label26.TabIndex = 605;
            this.label26.Text = "العنــــــــــوان...";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(21, 335);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 14);
            this.label10.TabIndex = 606;
            this.label10.Text = "معلومات شخصية .....";
            // 
            // SOCIAL_NO
            // 
            this.SOCIAL_NO.BackColor = System.Drawing.Color.White;
            this.SOCIAL_NO.Enabled = false;
            this.SOCIAL_NO.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCIAL_NO.Location = new System.Drawing.Point(101, 516);
            this.SOCIAL_NO.Name = "SOCIAL_NO";
            this.SOCIAL_NO.Size = new System.Drawing.Size(229, 23);
            this.SOCIAL_NO.TabIndex = 681;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(28, 519);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 16);
            this.label17.TabIndex = 680;
            this.label17.Text = "الرقم الوطني:";
            // 
            // Txt_GenderEN
            // 
            this.Txt_GenderEN.BackColor = System.Drawing.Color.White;
            this.Txt_GenderEN.Enabled = false;
            this.Txt_GenderEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_GenderEN.Location = new System.Drawing.Point(422, 407);
            this.Txt_GenderEN.Name = "Txt_GenderEN";
            this.Txt_GenderEN.Size = new System.Drawing.Size(229, 23);
            this.Txt_GenderEN.TabIndex = 679;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(337, 410);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 678;
            this.label1.Text = "الجنس انكليزي:";
            // 
            // Txt_OccupationEN
            // 
            this.Txt_OccupationEN.BackColor = System.Drawing.Color.White;
            this.Txt_OccupationEN.Enabled = false;
            this.Txt_OccupationEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_OccupationEN.Location = new System.Drawing.Point(422, 434);
            this.Txt_OccupationEN.Name = "Txt_OccupationEN";
            this.Txt_OccupationEN.Size = new System.Drawing.Size(229, 23);
            this.Txt_OccupationEN.TabIndex = 676;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(342, 437);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 16);
            this.label3.TabIndex = 677;
            this.label3.Text = "المهنة انكليزي:";
            // 
            // TxtNat_NameEN
            // 
            this.TxtNat_NameEN.BackColor = System.Drawing.Color.White;
            this.TxtNat_NameEN.Enabled = false;
            this.TxtNat_NameEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNat_NameEN.Location = new System.Drawing.Point(422, 381);
            this.TxtNat_NameEN.Name = "TxtNat_NameEN";
            this.TxtNat_NameEN.Size = new System.Drawing.Size(229, 23);
            this.TxtNat_NameEN.TabIndex = 674;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(334, 384);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 675;
            this.label11.Text = "الجنسية انكليزي:";
            // 
            // Txt_GenderAR
            // 
            this.Txt_GenderAR.BackColor = System.Drawing.Color.White;
            this.Txt_GenderAR.Enabled = false;
            this.Txt_GenderAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_GenderAR.Location = new System.Drawing.Point(103, 407);
            this.Txt_GenderAR.Name = "Txt_GenderAR";
            this.Txt_GenderAR.Size = new System.Drawing.Size(229, 23);
            this.Txt_GenderAR.TabIndex = 673;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(25, 410);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 16);
            this.label15.TabIndex = 672;
            this.label15.Text = "الجنس عربي:";
            // 
            // Txt_Post_code
            // 
            this.Txt_Post_code.BackColor = System.Drawing.Color.White;
            this.Txt_Post_code.Enabled = false;
            this.Txt_Post_code.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Post_code.Location = new System.Drawing.Point(422, 487);
            this.Txt_Post_code.Name = "Txt_Post_code";
            this.Txt_Post_code.Size = new System.Drawing.Size(229, 23);
            this.Txt_Post_code.TabIndex = 671;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(349, 490);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 16);
            this.label24.TabIndex = 670;
            this.label24.Text = "الرمز البريدي:";
            // 
            // Txt_OccupationAR
            // 
            this.Txt_OccupationAR.BackColor = System.Drawing.Color.White;
            this.Txt_OccupationAR.Enabled = false;
            this.Txt_OccupationAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_OccupationAR.Location = new System.Drawing.Point(103, 434);
            this.Txt_OccupationAR.Name = "Txt_OccupationAR";
            this.Txt_OccupationAR.Size = new System.Drawing.Size(229, 23);
            this.Txt_OccupationAR.TabIndex = 668;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(30, 437);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 16);
            this.label14.TabIndex = 669;
            this.label14.Text = "المهنة عربي:";
            // 
            // Txt_Another_phone
            // 
            this.Txt_Another_phone.BackColor = System.Drawing.Color.White;
            this.Txt_Another_phone.Enabled = false;
            this.Txt_Another_phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Another_phone.Location = new System.Drawing.Point(422, 460);
            this.Txt_Another_phone.Name = "Txt_Another_phone";
            this.Txt_Another_phone.Size = new System.Drawing.Size(229, 23);
            this.Txt_Another_phone.TabIndex = 666;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(341, 463);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 16);
            this.label5.TabIndex = 667;
            this.label5.Text = "رقم الهاتف2:";
            // 
            // TxtPhoneAR
            // 
            this.TxtPhoneAR.BackColor = System.Drawing.Color.White;
            this.TxtPhoneAR.Enabled = false;
            this.TxtPhoneAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhoneAR.Location = new System.Drawing.Point(103, 460);
            this.TxtPhoneAR.Name = "TxtPhoneAR";
            this.TxtPhoneAR.Size = new System.Drawing.Size(229, 23);
            this.TxtPhoneAR.TabIndex = 664;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(24, 463);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 665;
            this.label9.Text = "رقم الهـاتــــف:";
            // 
            // TxtNat_NameAR
            // 
            this.TxtNat_NameAR.BackColor = System.Drawing.Color.White;
            this.TxtNat_NameAR.Enabled = false;
            this.TxtNat_NameAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNat_NameAR.Location = new System.Drawing.Point(103, 381);
            this.TxtNat_NameAR.Name = "TxtNat_NameAR";
            this.TxtNat_NameAR.Size = new System.Drawing.Size(229, 23);
            this.TxtNat_NameAR.TabIndex = 660;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(24, 384);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 16);
            this.label8.TabIndex = 663;
            this.label8.Text = "الجنسية عربي:";
            // 
            // TxtEmail
            // 
            this.TxtEmail.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Enabled = false;
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(103, 487);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(229, 23);
            this.TxtEmail.TabIndex = 661;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(29, 490);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 662;
            this.label13.Text = "الايميــــــــــل:";
            // 
            // Txt_StreetEN
            // 
            this.Txt_StreetEN.BackColor = System.Drawing.Color.White;
            this.Txt_StreetEN.Enabled = false;
            this.Txt_StreetEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_StreetEN.Location = new System.Drawing.Point(747, 542);
            this.Txt_StreetEN.Name = "Txt_StreetEN";
            this.Txt_StreetEN.Size = new System.Drawing.Size(299, 23);
            this.Txt_StreetEN.TabIndex = 697;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(663, 546);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 16);
            this.label16.TabIndex = 696;
            this.label16.Text = "زقاق انكليزي:";
            // 
            // Txt_SuburbEN
            // 
            this.Txt_SuburbEN.BackColor = System.Drawing.Color.White;
            this.Txt_SuburbEN.Enabled = false;
            this.Txt_SuburbEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SuburbEN.Location = new System.Drawing.Point(747, 487);
            this.Txt_SuburbEN.Name = "Txt_SuburbEN";
            this.Txt_SuburbEN.Size = new System.Drawing.Size(299, 23);
            this.Txt_SuburbEN.TabIndex = 695;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(664, 490);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 16);
            this.label12.TabIndex = 694;
            this.label12.Text = "الحي انكليزي:";
            // 
            // Txt_StateEN
            // 
            this.Txt_StateEN.BackColor = System.Drawing.Color.White;
            this.Txt_StateEN.Enabled = false;
            this.Txt_StateEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_StateEN.Location = new System.Drawing.Point(747, 434);
            this.Txt_StateEN.Name = "Txt_StateEN";
            this.Txt_StateEN.Size = new System.Drawing.Size(299, 23);
            this.Txt_StateEN.TabIndex = 693;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(661, 437);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 692;
            this.label6.Text = "الولاية انكليزي:";
            // 
            // TxtCityEN
            // 
            this.TxtCityEN.BackColor = System.Drawing.Color.White;
            this.TxtCityEN.Enabled = false;
            this.TxtCityEN.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityEN.Location = new System.Drawing.Point(747, 381);
            this.TxtCityEN.Name = "TxtCityEN";
            this.TxtCityEN.Size = new System.Drawing.Size(299, 23);
            this.TxtCityEN.TabIndex = 690;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(663, 384);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 691;
            this.label4.Text = "مدينة انكليزي:";
            // 
            // Txt_StateAR
            // 
            this.Txt_StateAR.BackColor = System.Drawing.Color.White;
            this.Txt_StateAR.Enabled = false;
            this.Txt_StateAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_StateAR.Location = new System.Drawing.Point(747, 407);
            this.Txt_StateAR.Name = "Txt_StateAR";
            this.Txt_StateAR.Size = new System.Drawing.Size(299, 23);
            this.Txt_StateAR.TabIndex = 689;
            // 
            // Txt_SuburbAR
            // 
            this.Txt_SuburbAR.BackColor = System.Drawing.Color.White;
            this.Txt_SuburbAR.Enabled = false;
            this.Txt_SuburbAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SuburbAR.Location = new System.Drawing.Point(747, 460);
            this.Txt_SuburbAR.Name = "Txt_SuburbAR";
            this.Txt_SuburbAR.Size = new System.Drawing.Size(299, 23);
            this.Txt_SuburbAR.TabIndex = 688;
            // 
            // Txt_StreetAR
            // 
            this.Txt_StreetAR.BackColor = System.Drawing.Color.White;
            this.Txt_StreetAR.Enabled = false;
            this.Txt_StreetAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_StreetAR.Location = new System.Drawing.Point(747, 516);
            this.Txt_StreetAR.Name = "Txt_StreetAR";
            this.Txt_StreetAR.Size = new System.Drawing.Size(299, 23);
            this.Txt_StreetAR.TabIndex = 687;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(668, 410);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(72, 16);
            this.label25.TabIndex = 686;
            this.label25.Text = "الولاية عربي :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(675, 463);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 16);
            this.label23.TabIndex = 685;
            this.label23.Text = "الحي عربي:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(674, 519);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 16);
            this.label22.TabIndex = 684;
            this.label22.Text = "زقاق عربي:";
            // 
            // TxtCityAR
            // 
            this.TxtCityAR.BackColor = System.Drawing.Color.White;
            this.TxtCityAR.Enabled = false;
            this.TxtCityAR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityAR.Location = new System.Drawing.Point(747, 355);
            this.TxtCityAR.Name = "TxtCityAR";
            this.TxtCityAR.Size = new System.Drawing.Size(299, 23);
            this.TxtCityAR.TabIndex = 682;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(672, 358);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 16);
            this.label7.TabIndex = 683;
            this.label7.Text = "مدينة عربي:";
            // 
            // Hst_Grd_Per
            // 
            this.Hst_Grd_Per.AllowUserToAddRows = false;
            this.Hst_Grd_Per.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hst_Grd_Per.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Hst_Grd_Per.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Hst_Grd_Per.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Hst_Grd_Per.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Hst_Grd_Per.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1,
            this.Column13,
            this.Column10,
            this.Column12,
            this.Column14,
            this.Column4,
            this.Column17,
            this.Column5,
            this.Column6,
            this.Column15,
            this.Column7,
            this.Column8,
            this.Column11,
            this.Column16,
            this.Column9,
            this.Column3});
            this.Hst_Grd_Per.GridColor = System.Drawing.SystemColors.Control;
            this.Hst_Grd_Per.Location = new System.Drawing.Point(5, 46);
            this.Hst_Grd_Per.Name = "Hst_Grd_Per";
            this.Hst_Grd_Per.ReadOnly = true;
            this.Hst_Grd_Per.RowHeadersWidth = 20;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hst_Grd_Per.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Hst_Grd_Per.Size = new System.Drawing.Size(1074, 280);
            this.Hst_Grd_Per.TabIndex = 698;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "Per_ID";
            this.Column2.HeaderText = "الرقم";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 56;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "Per_AName";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "الاســـم العربي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 66;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "Per_EName";
            this.Column13.HeaderText = "الاسم الاجنبي";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 88;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "per_Birth_day";
            this.Column10.HeaderText = "تارخ التولد";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 58;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "Per_Birth_place";
            this.Column12.HeaderText = "مكان التولد العربي";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 111;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "Per_EBirth_place";
            this.Column14.HeaderText = "مكان التولد الاجنبي";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 114;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PerFRM_ADOC_NA";
            this.Column4.HeaderText = "نوع الوثيقة العربي";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 109;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "PerFRM_EDOC_NA";
            this.Column17.HeaderText = "نوع الوثيقة الاجنبي";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 113;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "per_Frm_Doc_NO";
            this.Column5.HeaderText = "رقم الوثيقة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 77;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "per_Frm_Doc_IS";
            this.Column6.HeaderText = "جهة الاصدار العربي";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 114;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column15.DataPropertyName = "per_EFrm_Doc_IS";
            this.Column15.HeaderText = "جهة الاصدار الاجنبي";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 117;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "per_Frm_Doc_Date";
            this.Column7.HeaderText = "تاريخ الاصدار";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 90;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "per_Frm_Doc_EDate";
            this.Column8.HeaderText = "تاريخ النفاذ";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 61;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "per_Mother_name";
            this.Column11.HeaderText = "اسم الام العربي";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 68;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "mother_Ename";
            this.Column16.HeaderText = "اسم الام الاجنبي";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 98;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "C_Date";
            this.Column9.HeaderText = "تاريخ الانشاء";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 89;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "U_Date";
            this.Column3.HeaderText = "تاريخ اخر تحديث";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 105;
            // 
            // Hst_Person_Info_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 614);
            this.Controls.Add(this.Hst_Grd_Per);
            this.Controls.Add(this.Txt_StreetEN);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_SuburbEN);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_StateEN);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtCityEN);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Txt_StateAR);
            this.Controls.Add(this.Txt_SuburbAR);
            this.Controls.Add(this.Txt_StreetAR);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.TxtCityAR);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.SOCIAL_NO);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_GenderEN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_OccupationEN);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtNat_NameEN);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_GenderAR);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_Post_code);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Txt_OccupationAR);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_Another_phone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtPhoneAR);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtNat_NameAR);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtUser);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Hst_Person_Info_main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "476";
            this.Text = "Hst_Person_Info_main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Hst_Person_Info_main_FormClosed);
            this.Load += new System.EventHandler(this.Hst_Person_Info_main_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Hst_Grd_Per)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox SOCIAL_NO;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_GenderEN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_OccupationEN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtNat_NameEN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_GenderAR;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txt_Post_code;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Txt_OccupationAR;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_Another_phone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtPhoneAR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtNat_NameAR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_StreetEN;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txt_SuburbEN;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_StateEN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtCityEN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_StateAR;
        private System.Windows.Forms.TextBox Txt_SuburbAR;
        private System.Windows.Forms.TextBox Txt_StreetAR;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TxtCityAR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView Hst_Grd_Per;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}