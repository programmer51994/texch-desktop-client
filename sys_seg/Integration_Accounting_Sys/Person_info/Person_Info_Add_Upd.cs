﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Person_Info_Add_Upd : Form
    {
        #region Defintion
        bool Change = false;
        string Sql_Text = "";
        byte Frm_Id;
        public static int Per_id = 0;
        Byte IS_Autorized = 0;
        string @format = "dd/MM/yyyy";
        string @format_doc = "dd/MM/yyyy";
        string @format_null = "";
        string S_doc_exp_null = "0000/00/00";
        DataTable Per_inf_TBL = new DataTable();
        BindingSource Per_Bs = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        BindingSource _Bs1 = new BindingSource();
        BindingSource _Bs_2 = new BindingSource();
        BindingSource _Bs_4 = new BindingSource();
        BindingSource _Bs_5 = new BindingSource();
        Int16 Con_Id = 0;
        string COUN_K_PH = "";
        string Per_Occupation_Aname = "";
        string Per_Occupation_Ename = "";
        string Birth_date = "0000/00/00";
        string Doc_date = "0000/00/00";
        string Doc_Edate = "0000/00/00";
        #endregion
        //---------------------------------------------------------------------------------------------------------
        public Person_Info_Add_Upd(byte Form_Id =1 , byte ISAutorized = 1)
        {
            InitializeComponent();
            #region Form_Orientation
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser,new MaskedTextBox());
            #endregion
            Frm_Id = Form_Id;
            IS_Autorized =ISAutorized;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
              
            }

            #endregion
        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Add_Upd_Load(object sender, EventArgs e)
        {
            TxtBirth_Da.Format = DateTimePickerFormat.Custom;
            TxtBirth_Da.CustomFormat = @format;

            TxtExp_Da.Format = DateTimePickerFormat.Custom;
            TxtExp_Da.CustomFormat = @format;

            TxtDoc_Da.Format = DateTimePickerFormat.Custom;
            TxtDoc_Da.CustomFormat = @format;
            GetData();
         
            if (Frm_Id == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديــل الثانوي" : "Update Customer"; 
                Change = true;
                EditRecord();
            }
   
        }
        //----------------------
        private void GetData()
        {
            Change = false;
            Sql_Text = " Exec all_master ";
            connection.SqlExec(Sql_Text, "all_master");
             Per_Bs.DataSource=connection.SQLDS.Tables["all_master6"];
            CboCit_Id.DataSource = Per_Bs;
            CboCit_Id.ValueMember = "Cit_Id";
            CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "cit_Con_Aname" : "cit_Con_Ename";

            _Bs1.DataSource = connection.SQLDS.Tables["all_master1"];
            CboNat_id.DataSource = _Bs1;
            CboNat_id.ValueMember = "NAt_Id";
            CboNat_id.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_eNAME";

            _Bs_2.DataSource = connection.SQLDS.Tables["all_master2"];
            CboDoc_id.DataSource = _Bs_2;
           CboDoc_id.ValueMember = "Fmd_id";
           CboDoc_id.DisplayMember = connection.Lang_id == 1 ? "Fmd_aname" : "Fmd_Ename";

           _Bs_4.DataSource = connection.SQLDS.Tables["all_master7"];
           Cbo_Gender.DataSource = _Bs_4;
           Cbo_Gender.ValueMember = "Gender_id";
           Cbo_Gender.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";

           _Bs_5.DataSource = connection.SQLDS.Tables["all_master8"].Select("Job_ID not in (-1)").CopyToDataTable();
           Cbo_Occupation.DataSource = _Bs_5;
           Cbo_Occupation.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";
           Cbo_Occupation.ValueMember = "Job_ID";


            Change = true;
            if (connection.Lang_id == 1)
            {
                resd_cmb.Items.Clear();
                resd_cmb.Items.Add("مقيم");
                resd_cmb.Items.Add("غير مقيم");
                resd_cmb.SelectedIndex = 0;
            }
            else
            {
                resd_cmb.Items.Clear();
                resd_cmb.Items.Add("Resident");
                resd_cmb.Items.Add("Non-Resident");
                resd_cmb.SelectedIndex = 0;
            }
          
        }


        //---------------------------------------------------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column=
            {
                        "Per_ID", "Per_AName", "Per_EName", "per_City_ID", "per_Coun_ID", "Per_Birth_place", "per_Phone", "per_Another_Phone",
                        "per_Email", "per_Frm_Doc_ID","per_Frm_Doc_NO", "per_Frm_Doc_Date", "per_Frm_Doc_EDate", "per_Frm_Doc_IS", "per_Nationalty_ID", "per_Birth_day", "per_Gender_ID", "per_Occupation",
                        "per_Mother_name", "cust_ID", "User_ID", "registerd_ID","COUN_K_PH", "Per_ACITY_NAME", "Per_ECITY_NAME","Per_ACOUN_NAME","Per_ECOUN_NAME","Per_Street",
                        "Per_Suburb","Per_Post_Code","Per_State" ,"PerFRM_ADOC_NA","PerFRM_EDOC_NA","Per_A_NAT_NAME" ,"Per_E_NAT_NAME","Per_Gender_Aname","Per_Gender_Ename","Login_id",
                        "per_EFrm_Doc_IS","EOccupation","mother_Ename","Per_EBirth_place","Per_EStreet","Per_ESuburb","Per_EState","Social_No","resd_flag","Per_Occupation_id","details_job"
                       
            };
            string[] DType =
            {
                "System.Int32","System.String","System.String","System.Int16",  "System.Int16", "System.String", "System.String","System.String",
                "System.String", "System.Int16", "System.String","System.String" ,"System.String","System.String","System.Int16", "System.String", "System.Byte", "System.String",
                "System.String","System.Int16","System.Int16", "System.Int16", "System.String", "System.String", "System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String", "System.String","System.String","System.String","System.String","System.String","System.String","System.Int16",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.Int16","System.Int16", "System.String"
            };

            Per_inf_TBL = CustomControls.Custom_DataTable("Per_inf_TBL", Column, DType);
        }



        //-----------------------------------------------------------------------------------------------------

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Create_Table();

            try
            {
                DataRowView DRV = Person_Info_Main._Bs_Person_Info.Current as DataRowView;
                DataRow DR = DRV.Row;

                Per_id = DR.Field<int>("Per_id");
                 }

            catch { }
                Con_Id = Convert.ToInt16(((DataRowView)Per_Bs.Current).Row["Con_ID"]);
                COUN_K_PH = ((DataRowView)Per_Bs.Current).Row["COUN_K_PH"].ToString();

                Doc_date = TxtDoc_Da.Value.ToString("yyyy/MM/dd");
                Doc_Edate = TxtExp_Da.Value.ToString("yyyy/MM/dd");
                Birth_date = TxtBirth_Da.Value.ToString("yyyy/MM/dd");


            

            #region Validation
            if (TxtCust_Aname.Text.Trim() == "" && TxtCust_Ename.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي او الاجنبي" : "Please enter the Arabic or English Name", MyGeneral_Lib.LblCap);
               TxtCust_Aname.Focus();
                return;
            }
            //if (TxtCust_Ename.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاسم الاجنبي" : "Please enter the English Name", MyGeneral_Lib.LblCap);
            //    TxtCust_Ename.Focus();
            //    return;
            //}



            if (CboDoc_id.SelectedIndex == 0 || CboDoc_id.SelectedIndex == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يرجى تحديد الوثيقة" : "Please select the type of Doc.", MyGeneral_Lib.LblCap);
                CboDoc_id.Focus();
                return;
            }

            if (TxtNo_Doc.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل رقم الوثيقة" : "Please enter Doc. No.", MyGeneral_Lib.LblCap);
                TxtNo_Doc.Focus();
                return;
            }


            if (TxtDoc_Da.Text == "0000/00/00")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل تاريخ اصدار الوثيقة" : "Please enter Issuer Date", MyGeneral_Lib.LblCap);
                TxtDoc_Da.Focus();
                return;
            }

        



            
            //if (IS_Autorized == 1)
            //{
            //    if (Convert.ToInt16(CboCit_Id.SelectedValue) <= 0 && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " حدد المدينة" : "Please select the Country", MyGeneral_Lib.LblCap);
            //        CboCit_Id.Focus();
            //        return;
            //    }

            //    if (Convert.ToInt16(CboNat_id.SelectedValue) <= 0 && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " حدد الجنسية" : "Please select the Nationality", MyGeneral_Lib.LblCap);
            //        CboNat_id.Focus();
            //        return;
            //    }
            //    if (Txt_Occupation.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " ادخل المهنة " : "Please enter the Occupation", MyGeneral_Lib.LblCap);
            //        Txt_Occupation.Focus();
            //        return;
            //    }

            //if (Convert.ToInt16(Cbo_Gender.SelectedValue) <= 0 && IS_Autorized == 1)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " حدد الجنس" : "Please select the Gender", MyGeneral_Lib.LblCap);
            //    Cbo_Gender.Focus();
            //    return;
            //}


            //    if (Convert.ToInt16(CboDoc_id.SelectedValue) < 0 && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثيقة" : "Please select the type of Doc.", MyGeneral_Lib.LblCap);
            //        CboDoc_id.Focus();
            //        return;
            //    }

            //    if (TxtNo_Doc.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الوثيقة" : "Please enter Doc. No.", MyGeneral_Lib.LblCap);
            //        TxtNo_Doc.Focus();
            //        return;
            //    }
            //    if (TxtIss_Doc.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "ادخل جهة الاصدار" : "Please enter Issuer", MyGeneral_Lib.LblCap);
            //        TxtIss_Doc.Focus();
            //        return;
            //    }
            //    Doc_Date = TxtDoc_Da.Text;
            //    string Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
            //    if (Ord_Strt == "0" || Ord_Strt == "-1" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "ادخل تأريخ الاصدار" : "Please enter Issuer Date", MyGeneral_Lib.LblCap);
            //        TxtDoc_Da.Focus();
            //        return;
            //    }

            //    Doc_Edate = TxtExp_Da.Text;
            //    Ord_Strt = MyGeneral_Lib.DateChecking(TxtExp_Da.Text);
            //    if (Ord_Strt == "-1")
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ نفاذ الوثيقة" : "Please make sure of expire date of formal document", MyGeneral_Lib.LblCap);
            //        TxtExp_Da.Focus();
            //        return;
            //    }
            //    else if (Ord_Strt == "0")
            //    {
            //        Doc_Edate = "";
            //    }

            //    Birth = TxtBirth_Da.Text;
            //    string Ord_Strt1 = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
            //    if (Ord_Strt1 == "-1" || Ord_Strt1 == "0" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ التولد" : "Please make sure of birth date", MyGeneral_Lib.LblCap);
            //        TxtBirth_Da.Focus();
            //        return;
            //    }

            //    if (TxtPhone.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " ادخل الهاتف " : "Please enter the Phone NO.", MyGeneral_Lib.LblCap);
            //        TxtPhone.Focus();
            //        return;
            //    }
            //    if (Txt_State.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " ادخل الولاية " : "Please enter the State", MyGeneral_Lib.LblCap);
            //        Txt_State.Focus();
            //        return;
            //    }
            //    if (Txt_Street.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " ادخل الزقاق " : "Please enter the Street", MyGeneral_Lib.LblCap);
            //        Txt_Street.Focus();
            //        return;
            //    }

            //    if (Txt_Suburb.Text == "" && IS_Autorized == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " ادخل الحي " : "Please enter the subrub", MyGeneral_Lib.LblCap);
            //        Txt_Suburb.Focus();
            //        return;
            //    }

            if (Convert.ToInt16(CboDoc_id.SelectedValue) < 0)
            {
                CboDoc_id.SelectedValue = 0;
            }

            if (Convert.ToInt16(CboNat_id.SelectedValue) < 0)
            {
                CboNat_id.SelectedValue = 0;
            }
            if (Convert.ToInt16(Cbo_Gender.SelectedValue) < 0)
            {
                Cbo_Gender.SelectedValue = 0;
            }
            #endregion
            Per_Occupation_Aname = ((DataRowView)_Bs_5.Current).Row["Job_AName"].ToString();
            Per_Occupation_Ename = ((DataRowView)_Bs_5.Current).Row["Job_EName"].ToString();

            Per_inf_TBL.Rows.Add(new object[] { Per_id, TxtCust_Aname.Text, TxtCust_Ename.Text.Trim(), CboCit_Id.SelectedValue, Con_Id ,Txt_birth_place.Text.Trim() ,TxtPhone.Text.Trim(),Txt_Another_Phone.Text ,
                                                    TxtEmail.Text, CboDoc_id.SelectedValue,TxtNo_Doc.Text,Doc_date,Doc_Edate,TxtIss_Doc.Text, CboNat_id.SelectedValue,Birth_date,Cbo_Gender.SelectedValue,
                                                     Per_Occupation_Aname, Txt_Mother_name.Text,0,0,0,COUN_K_PH ,((DataRowView)Per_Bs.Current).Row["cit_Aname"].ToString(),((DataRowView)Per_Bs.Current).Row["cit_Ename"].ToString(),
                                                     ((DataRowView)Per_Bs.Current).Row["Con_Aname"].ToString(),((DataRowView)Per_Bs.Current).Row["Con_Ename"].ToString(), 
                                                     Txt_Street.Text.Trim(),Txt_Suburb.Text.Trim(),Txt_Post_code.Text.Trim(),Txt_State.Text.Trim(),((DataRowView)_Bs_2.Current).Row["Fmd_Aname"].ToString(),
                                                     ((DataRowView)_Bs_2.Current).Row["Fmd_Ename"].ToString(),((DataRowView)_Bs1.Current).Row["Nat_ANAME"].ToString(),
                                                    ((DataRowView)_Bs1.Current).Row["Nat_ENAME"].ToString(),((DataRowView)_Bs_4.Current).Row["Gender_Aname"].ToString(),((DataRowView)_Bs_4.Current).Row["Gender_Ename"].ToString(),0,
                                                    TxtIss_EDoc.Text.Trim(),Per_Occupation_Ename,Txt_Mother_Ename.Text.Trim(),
                                                     Txt_Ebirth_place.Text.Trim(),Txt_EStreet.Text.Trim(),Txt_ESuburb.Text.Trim(),Txt_EState.Text.Trim(),Txt_Social_ID.Text.Trim(),resd_cmb.SelectedIndex,Cbo_Occupation.SelectedValue,Txt_details_job.Text.Trim()});
                                                  
                                                    
           

            connection.SQLCMD.Parameters.AddWithValue("@Person_Info_Type", Per_inf_TBL);
            connection.SQLCMD.Parameters.AddWithValue("@registerd_ID", 1);
            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Form_id", Frm_Id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Person_info_Add_Upd_2", connection.SQLCMD);

               
              
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                        !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Per_id)))
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                connection.SQLCMD.Parameters.Clear();



               

            if (Frm_Id == 1)
            {
                TxtCust_Aname.Text = "";
                TxtCust_Ename.Text = "";
                TxtEmail.Text = ""; 
                TxtDoc_Da.Text = "";
                //TxtE_Address.Text = "";
                TxtExp_Da.Text = ""; 
                TxtIss_Doc.Text = "";
                TxtNo_Doc.Text = "";
                TxtPhone.Text = "";
                //TxtA_Address.Text = "";
                TxtBirth_Da.Text = "";
                CboCit_Id.SelectedValue = 0;
                //CboCls_Cust.SelectedIndex = 0;     
                CboDoc_id.SelectedValue = -1;
                CboNat_id.SelectedValue = -1; 
                Txt_Another_Phone.Text = "";
                Cbo_Occupation.SelectedValue = 0;
                Txt_Post_code.Text = "";
                Txt_State.Text = "";
                Txt_Street.Text = "";
                Txt_Suburb.Text = "";
                Cbo_Gender.SelectedValue = 0;
                resd_cmb.SelectedIndex = 0;
                Txt_birth_place.Text = "";
                Txt_Mother_name.Text = "";
                Txt_Mother_Ename.Text="";
                Txt_Ebirth_place.Text="";
                //Txt_EOccupation.Text="";
                Txt_EState.Text="";
                Txt_EStreet.Text = "";
                Txt_ESuburb.Text="";
                TxtIss_EDoc.Text = "";
                Txt_Social_ID.Text = "";
                Txt_details_job.Text = "";



                if (Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]) == 1)// يملك خاصية رفع الوثائق
                {

                    Sql_Text = "Select  Per_ID, Per_AName, Per_EName from Person_Info where Per_ID = " + Per_id;
                    connection.SqlExec(Sql_Text, "Per_Info_tbl");
                    Person_Document_Add_Upd UpdFrm = new Person_Document_Add_Upd(connection.SQLDS.Tables["Per_Info_tbl"].Rows[0]["Per_AName"].ToString(), Per_id, connection.SQLDS.Tables["Per_Info_tbl"].Rows[0]["Per_EName"].ToString());
                    this.Visible = false;
                    UpdFrm.ShowDialog(this);
                    Customer_Add_Upd_Load(sender, e);
                    this.Visible = true;

                }

            }
            else
                this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------------------------------------------------------------
        private void EditRecord()
        {
            DataRowView DRV = Person_Info_Main._Bs_Person_Info.Current as DataRowView;
            DataRow DR = DRV.Row;

            Per_id = DR.Field<int>("Per_id");
            //--------------------------------------------
            TxtCust_Aname.DataBindings.Clear();
            TxtCust_Ename.DataBindings.Clear();
            CboCit_Id.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            TxtBirth_Da.DataBindings.Clear();
            CboNat_id.DataBindings.Clear();
            TxtPhone.DataBindings.Clear();
            CboDoc_id.DataBindings.Clear();
            TxtNo_Doc.DataBindings.Clear();
            TxtDoc_Da.DataBindings.Clear();
            TxtIss_Doc.DataBindings.Clear();
            TxtExp_Da.DataBindings.Clear();
            //TxtA_Address.DataBindings.Clear();
            //TxtE_Address.DataBindings.Clear();
            //CboCls_Cust.DataBindings.Clear();

           // CboCls_Cust.Enabled = false;
            Txt_Another_Phone.DataBindings.Clear();
            Txt_Mother_name.DataBindings.Clear();
            //Chk_Authorized.DataBindings.Clear();
            Cbo_Gender.DataBindings.Clear();
            resd_cmb.DataBindings.Clear();
            Cbo_Occupation.DataBindings.Clear();
            Txt_Post_code.DataBindings.Clear();
            Txt_State.DataBindings.Clear();
            Txt_Street.DataBindings.Clear();
            Txt_Suburb.DataBindings.Clear();
            //TxtBirth_Da.DataBindings.Clear();
            //TxtDoc_Da.DataBindings.Clear();
            //TxtExp_Da.DataBindings.Clear();
            //Per_Check.DataBindings.Clear();
            Txt_Mother_Ename.DataBindings.Clear();
            Txt_Ebirth_place.DataBindings.Clear();
           // Txt_EOccupation.DataBindings.Clear();
            Txt_EState.DataBindings.Clear();
            Txt_EStreet.DataBindings.Clear();
            Txt_ESuburb.DataBindings.Clear();
            TxtIss_EDoc.DataBindings.Clear();
            Txt_Social_ID.DataBindings.Clear();
            Txt_details_job.DataBindings.Clear();



            TxtCust_Aname.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_AName");
            TxtCust_Ename.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_EName");
            CboCit_Id.DataBindings.Add("SelectedValue", Person_Info_Main._Bs_Person_Info, "per_City_ID");
            TxtEmail.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Email");
            TxtBirth_Da.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Birth_day");
            CboNat_id.DataBindings.Add("SelectedValue", Person_Info_Main._Bs_Person_Info, "per_Nationalty_ID");

            Txt_birth_place.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_Birth_place");
            TxtPhone.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Phone");
            CboDoc_id.DataBindings.Add("SelectedValue", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_ID");
            TxtNo_Doc.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_NO");
            TxtDoc_Da.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_Date");
            TxtIss_Doc.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_IS");
            TxtExp_Da.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_EDate");
            //TxtA_Address.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "A_Address");
           // TxtE_Address.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "E_Address");
           // CboCls_Cust.DataBindings.Add("SelectedIndex", Person_Info_Main._Bs_Person_Info, "CUS_Cls_ID");
           // CboType_cust.DataBindings.Add("SelectedValue", Person_Info_Main._Bs_Person_Info, "Cust_Type_Id");
            Txt_Another_Phone.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Another_Phone");
            Txt_Mother_name.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Mother_name");
            //Chk_Authorized.DataBindings.Add("Checked", Person_Info_Main._Bs_Person_Info, "Authorized_Flag");
            Cbo_Gender.DataBindings.Add("SelectedValue", Person_Info_Main._Bs_Person_Info, "per_Gender_ID");
            Cbo_Occupation.DataBindings.Add("SelectedValue", Person_Info_Main._Bs_Person_Info, "per_Occupation_id");
            Txt_Post_code.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_Post_Code");
            //TxtBirth_Da.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Birth_day");
            //TxtDoc_Da.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_Date");
            //TxtExp_Da.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_Frm_Doc_EDate");

            
            //Txt_Occupation.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Occupation");
            //Txt_Post_code.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_Post_Code");

            Txt_State.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_State");
            Txt_Street.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_Street");
            Txt_Suburb.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_Suburb");


            Txt_Mother_Ename.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "mother_Ename");
            Txt_Ebirth_place.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_EBirth_place");
          //  Txt_EOccupation.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "EOccupation");
            Txt_EState.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_EState");
            Txt_EStreet.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_EStreet");
            Txt_ESuburb.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Per_ESuburb");
            TxtIss_EDoc.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "per_EFrm_Doc_IS");
            Txt_Social_ID.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "Social_No");
            resd_cmb.DataBindings.Add("SelectedIndex", Person_Info_Main._Bs_Person_Info, "resd_flag");
            Txt_details_job.DataBindings.Add("Text", Person_Info_Main._Bs_Person_Info, "details_job");






           // Per_Check.DataBindings.Add("Checked", Person_Info_Main._Bs_Person_Info, "Chk_person");

            //if (Per_Check.Checked == true )
            //{ Per_Check.Enabled = false;}
            //if (Chk_Authorized.Checked == true)
            //{ Chk_Authorized.Enabled = false;}

            Per_id = Convert.ToInt32(((DataRowView)Person_Info_Main._Bs_Person_Info.Current).Row["per_id"]);
            //Cust_Id = Convert.ToInt32(((DataRowView)Person_Info_Main._Bs_Person_Info.Current).Row["Cust_id"]);

         //   string Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
            if (TxtDoc_Da.Text == "0" || TxtDoc_Da.Text == "" || TxtDoc_Da.Text == "0000/00/00" || TxtDoc_Da.Text == "00/00/0000")//(Ord_Strt == "0" || Ord_Strt == "-1")
            {
                TxtDoc_Da.Text = "";
            }
          //  Ord_Strt = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
            if(TxtBirth_Da.Text == "0" || TxtBirth_Da.Text == "" || TxtBirth_Da.Text == "0000/00/00" || TxtBirth_Da.Text == "00/00/0000")// (Ord_Strt == "0" || Ord_Strt == "-1")
            {
                TxtBirth_Da.Text = "";
            }
           // Ord_Strt = MyGeneral_Lib.DateChecking(TxtExp_Da.Text);
            if (TxtExp_Da.Text == "0" || TxtExp_Da.Text == "" || TxtExp_Da.Text == "0000/00/00" || TxtExp_Da.Text == "00/00/0000")//(Ord_Strt == "0" || Ord_Strt == "-1")
            {
                TxtExp_Da.Text = "";
            }

        }
        //---------------------------------------------------------------------------------------------------------
        private void Customer_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Used_Tbl = { "all_master1", "all_master", "all_master2",
                                   "all_master6","all_master7","Type_cust" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void CboDoc_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboDoc_id.SelectedIndex == 1)
            {
                TxtNo_Doc.Enabled = false; TxtNo_Doc.Text = "";
                TxtIss_Doc.Enabled = false;TxtIss_Doc.Text = "";
                TxtDoc_Da.Enabled = false; TxtDoc_Da.Text = "";
                TxtExp_Da.Enabled = false; TxtExp_Da.Text = "";

            }
            else
            {
                TxtNo_Doc.Enabled = true ;
                TxtIss_Doc.Enabled = true;
                TxtDoc_Da.Enabled = true;
                TxtExp_Da.Enabled = true;
            }
        }
        //---------------------------------------------------------------------------------------------------------
        private void CboCls_Cust_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            if (Convert.ToInt16(CboCit_Id.SelectedValue) < 0)
            {
                CboCit_Id.SelectedValue = 0;
            }

            if (Convert.ToInt16(CboDoc_id.SelectedValue) < 0)
            {
                CboDoc_id.SelectedValue = 0;
            }

            if (Convert.ToInt16(CboNat_id.SelectedValue) < 0)
            {
                CboNat_id.SelectedValue = 0;
            }

       

                if (Convert.ToInt16(CboCit_Id.SelectedValue) <= 0)
                {
                    CboCit_Id.SelectedValue = 0;
                }
                if (Convert.ToInt16(CboDoc_id.SelectedValue) <= 0 && Frm_Id == 1)
                {
                    CboDoc_id.SelectedValue = -1;
                }
                if (Convert.ToInt16(CboNat_id.SelectedValue) <= 0)
                {
                    CboNat_id.SelectedValue = -1;
                }

            
        }



        private void Customer_Add_Upd_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 2;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

   

    }
}