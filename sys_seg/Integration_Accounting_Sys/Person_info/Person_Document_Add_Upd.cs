﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;

  //Pic_Name = Path.GetFileName(file);

namespace Integration_Accounting_Sys
{
    public partial class Person_Document_Add_Upd : Form
    {
        bool change = false;
        bool change_SPC = false;
        string Sql_Text = "";
        DataTable Pic_Tbl = new DataTable();
        DataTable DT_Tbl = new DataTable();
        byte[] img = new byte[0];
        int X = 0;
        int Y = 0;
        int ISDoc_ORSpec = 0;
        int ISSpec = 0;
        int rowindex = 0;
        int rowindex_Spc = 0;
        public static BindingSource _Bs_Doc_Info = new BindingSource();
        public static BindingSource _Bs_Doc_Spc = new BindingSource();
        BindingSource _Bs_Pic = new BindingSource();
        DataTable DT = new DataTable();
        string Path_ex = "";
        string Path_Spc_ex = "";
        string Person_AName = "";
        string Person_EName = "";
        int Person_id = 0;


        public Person_Document_Add_Upd(string Per_AName, int Per_ID, string Per_EName)
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Doc.AutoGenerateColumns = false;
            Grd_Doc_Spc.AutoGenerateColumns = false;
            Person_AName = Per_AName;
            Person_EName = Per_EName;
            Person_id = Per_ID;
      
        }
        //-----------------------------------------
        private void Create_Pic_Tbl()
        {
            string[] Column = { "Fmd_ID", "Fmd_AName", "Fmd_EName", "Doc_Pic", "Doc_pic_name", "Flag", "path", "extension" };
            string[] DType = { "System.Int16", "System.String", "System.String", "System.Byte[]", "System.String", "System.Int16", "System.String", "System.String" };
            Pic_Tbl = CustomControls.Custom_DataTable("Pic_Tbl", Column, DType);
        }
        //-----------------------------------------
        private void add_Upd_Document_Load(object sender, EventArgs e)
        {

            change = false;
            Create_Pic_Tbl();
            Txt_per_name.Text = Person_AName;

         
            Sql_Text = " SELECT Fmd_ID, Fmd_AName, Fmd_EName    FROM Fmd_Tbl where Fmd_ID <> 0 "
            + " SELECT Special_Fmd_ID, Special_Fmd_AName, Special_Fmd_EName FROM Special_Fmd_Tbl ";


            _Bs_Doc_Info.DataSource = connection.SqlExec(Sql_Text, "Doc_TBL");
            Grd_Doc.DataSource = _Bs_Doc_Info;
            _Bs_Doc_Spc.DataSource = connection.SQLDS.Tables["Doc_TBL1"];
            Grd_Doc_Spc.DataSource = _Bs_Doc_Spc;
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "Fmd_EName";
                dataGridViewTextBoxColumn3.DataPropertyName = "Special_Fmd_EName";
            }
            Btn_Next.Enabled = false;
            Btn_Previous.Enabled = false;
            Grd_Doc.ClearSelection();
            Grd_Doc_Spc.ClearSelection();


        }


        private void Del_Doc_Click(object sender, EventArgs e)
        {
            if (ISDoc_ORSpec == 1)
            {
                try
                {
                    Int16 ID = Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"]);
                    //DataTable DT = new DataTable();
                    //DT = Pic_Tbl.DefaultView.ToTable(false, "fmd_id", "Doc_Pic").Select(" fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])).CopyToDataTable();
                    DataRow[] drr = Pic_Tbl.Select(" flag = 1 and  fmd_id =" + ID);
                    drr[0].Delete();
                    Pic_Tbl.AcceptChanges();
                    Grd_Doc.Rows[rowindex].DefaultCellStyle.BackColor = Color.White;
                }
                catch
                { }
            }
            else
            {
                try
                {
                    Int16 ID = Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_fmd_id"]);
                    //DataTable DT = new DataTable();
                    //DT = Pic_Tbl.DefaultView.ToTable(false, "fmd_id", "Doc_Pic").Select(" fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])).CopyToDataTable();
                    DataRow[] drr = Pic_Tbl.Select(" flag = 2 and  fmd_id =" + ID);
                    drr[0].Delete();
                    Pic_Tbl.AcceptChanges();
                    Grd_Doc_Spc.Rows[rowindex_Spc].DefaultCellStyle.BackColor = Color.White;
                }
                catch
                { }
            }
            Grd_Doc_SelectionChanged(null, null);
            Grd_Doc_Spc_SelectionChanged(null, null);

        }
        //-----------------------------------------
        private void Add_Doc_Click(object sender, EventArgs e)
        {
            Int16 Frm_id = 0;
            string Fmd_AName = "";
            string Fmd_EName = "";

            Grd_Doc.Refresh();
            Grd_Doc_Spc.Refresh();
            if (ISDoc_ORSpec == 1)
            {
                Frm_id = Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"]);
                Fmd_AName = Convert.ToString(((DataRowView)_Bs_Doc_Info.Current).Row["Fmd_AName"]);
                Fmd_EName = Convert.ToString(((DataRowView)_Bs_Doc_Info.Current).Row["Fmd_EName"]);

            }
            else
            {
                Frm_id = Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"]);
                Fmd_AName = Convert.ToString(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_AName"]);
                Fmd_EName = Convert.ToString(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_EName"]);
            }
            OpenFileDialog Open_Pic = new OpenFileDialog();
            Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PDF)|*.BMP;*.JPG;*.GIF;*.PDF|" + "All files (*.*)|*.*";
            Open_Pic.Title = connection.Lang_id == 1 ? "اختر صورة " : "Select  Photo";
            Open_Pic.Multiselect = true;
           
            if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
            {
                foreach (String file in Open_Pic.FileNames)
                {
                    string Pic_Name = "" ;
                    string file_Name = Path.GetFileName(file);

                    if (file_Name.Contains("."))
                    {
                         Pic_Name = Path.GetFileName(file.Substring(0, file.Length - 4));
                    }
                    else
                    { Pic_Name = file_Name; }
                    FileStream FS = new FileStream(file, FileMode.Open, FileAccess.Read);
                    img = new byte[FS.Length];
                    string extension = System.IO.Path.GetExtension(file);
                    extension = extension.ToLower();
                    FS.Read(img, 0, System.Convert.ToInt32(FS.Length));
                    FS.Close();
                    DataRow DRow1 = Pic_Tbl.NewRow();
                    DRow1.SetField("fmd_id", Frm_id);
                    DRow1.SetField("Fmd_AName", Fmd_AName);
                    DRow1.SetField("Fmd_EName", Fmd_EName);
                    DRow1.SetField("Doc_Pic", img);
                    DRow1.SetField("Flag", ISDoc_ORSpec);
                    DRow1.SetField("path", file);
                    DRow1.SetField("Doc_pic_name", Pic_Name);
                    DRow1.SetField("extension", extension);

                    Pic_Tbl.Rows.Add(DRow1);
                    _Bs_Pic.DataSource = Pic_Tbl;
                }

                if (Pic_Tbl.Rows.Count > 0 && ISDoc_ORSpec == 1)
                {
                    change = true;
                    Grd_Doc_SelectionChanged(null, null);
                    change = false;
                    Grd_Doc.Rows[rowindex].DefaultCellStyle.BackColor = Color.LightGreen;
                }
                else
                {
                    change_SPC = true;
                    Grd_Doc_Spc_SelectionChanged(null, null);
                    change_SPC = false;
                    Grd_Doc_Spc.Rows[rowindex_Spc].DefaultCellStyle.BackColor = Color.LightGreen;
                }

            }

        }
        //-----------------------------------------
        private void Grd_Doc_SelectionChanged(object sender, EventArgs e)
        {

            if (change)
            {

                //MessageBox.Show(rowindex.ToString());
                try
                {

                    ISSpec = 0;
                    X = 0;
                    DataTable DT = new DataTable();
                    DT = Pic_Tbl.DefaultView.ToTable(false, "fmd_id", "Flag", "Doc_Pic", "path", "extension").Select(" Flag = 1 and fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])).CopyToDataTable();
                   // Path_ex = DT.Rows[0]["path"].ToString().Substring(DT.Rows[0]["path"].ToString().Length - 3);
                    Path_ex = DT.Rows[0]["extension"].ToString();
                    if (Path_ex != ".pdf")
                    {
                        pictureBox1.Visible = true;
                        axAcroPDF1.Visible = false;
                        img = (Byte[])(DT.Rows[0]["Doc_Pic"]);
                        MemoryStream mem = new MemoryStream(img);
                        pictureBox1.Image = Image.FromStream(mem);

                    }
                    else
                    {
                        axAcroPDF1.Visible = true;
                        pictureBox1.Visible = false;
                        axAcroPDF1.LoadFile(DT.Rows[0]["path"].ToString());
                        axAcroPDF1.src = DT.Rows[0]["path"].ToString();
                        axAcroPDF1.setShowToolbar(false);
                        axAcroPDF1.setView("FitH");
                        axAcroPDF1.setLayoutMode("SinglePage");
                        axAcroPDF1.Show();
                    }
                }
                catch
                {

                    pictureBox1.Image = null;
                    axAcroPDF1.Visible = false;

                }

                if (Path_ex != ".pdf")
                {
                    Btn_Next.Visible = true ;
                    Btn_Previous.Visible = true;
                    int Rec_No = (from DataRow row in Pic_Tbl.Rows
                                  where
                                      (Int16)row["Fmd_id"] == Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])
                                      && (Int16)row["Flag"] == 1
                                  select row).Count();
                    if (Rec_No > 1)
                    { Btn_Next.Enabled = true; }
                    else
                    {
                        Btn_Next.Enabled = false;
                        Btn_Previous.Enabled = false;
                    }
                }
                else
                {
                    Btn_Next.Visible = false;
                    Btn_Previous.Visible = false;
                }
            }
            else
            { pictureBox1.Image = null; }
            rowindex = Grd_Doc.CurrentRow.Index;
        }
        //-----------------------------------------
        private void Grd_Doc_Spc_SelectionChanged(object sender, EventArgs e)
        {
            if (change_SPC)
            {
                try
                {
                    ISSpec = 1;
                    Y = 0;
                    DataTable DT = new DataTable();
                    
                    DT = Pic_Tbl.DefaultView.ToTable(false, "Fmd_ID", "Flag", "Doc_Pic", "path", "extension").Select(" Flag = 2 and Fmd_ID = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"])).CopyToDataTable();
                   // Path_Spc_ex = DT.Rows[0]["path"].ToString().Substring(DT.Rows[0]["path"].ToString().Length - 3);
                    Path_Spc_ex = DT.Rows[0]["extension"].ToString();
                    if (Path_Spc_ex != ".pdf" )
                    {
                        pictureBox1.Visible = true;
                        axAcroPDF1.Visible = false;
                        img = (Byte[])(DT.Rows[0]["Doc_Pic"]);
                        MemoryStream mem = new MemoryStream(img);
                        pictureBox1.Image = Image.FromStream(mem);
                    }
                    else
                    {
                        axAcroPDF1.Visible = true;
                        pictureBox1.Visible = false;
                        axAcroPDF1.LoadFile(DT.Rows[0]["path"].ToString());
                        axAcroPDF1.src = DT.Rows[0]["path"].ToString();
                        axAcroPDF1.setShowToolbar(false);
                        axAcroPDF1.setView("FitH");
                        axAcroPDF1.setLayoutMode("SinglePage");
                        axAcroPDF1.Show();
                    }
                }
                catch
                {
                    pictureBox1.Image = null;
                    axAcroPDF1.Visible = false;
                }

                if (Path_Spc_ex != ".pdf" )
                {
                    Btn_Next.Visible = true;
                    Btn_Previous.Visible = true;
                    int Rec_No_Spc = (from DataRow row in Pic_Tbl.Rows
                                      where
                                          (Int16)row["Fmd_ID"] == Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"])
                                          && (Int16)row["Flag"] == 2
                                      select row).Count();
                    if (Rec_No_Spc > 1)
                    { Btn_Next.Enabled = true; }
                    else
                    {
                        Btn_Next.Enabled = false;
                        Btn_Previous.Enabled = false;
                    }
                }
                else
                {
                    Btn_Next.Visible = false;
                    Btn_Previous.Visible = false;
                }
            }
            else
            { pictureBox1.Image = null; }
            try
            {
                rowindex_Spc = Grd_Doc_Spc.CurrentRow.Index;
            }
            catch
            {}
        }
        //-----------------------------------------
        private void Btn_Previous_Click(object sender, EventArgs e)
        {
            if (ISSpec == 0)
            {
                int Rec_No = (from DataRow row in Pic_Tbl.Rows
                              where
                                  (Int16)row["Fmd_id"] == Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])
                                   && (Int16)row["Flag"] == 1
                              select row).Count();
                if (Rec_No > 0)
                {

                    try
                    {

                        Btn_Next.Enabled = true;
                        X = X - 1;
                        DataTable DT = new DataTable();
                        DT = Pic_Tbl.DefaultView.ToTable(false, "Fmd_id", "Flag", "Doc_Pic").Select(" Flag = 1 and Fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])).CopyToDataTable();
                        img = (Byte[])(DT.Rows[X]["Doc_Pic"]);
                        MemoryStream mem = new MemoryStream(img);
                        pictureBox1.Image = Image.FromStream(mem);
                    }
                    catch
                    {
                        Btn_Previous.Enabled = false;
                        X = X + 1;
                    }

                }
            }
            else
            {
                int Rec_No = (from DataRow row in Pic_Tbl.Rows
                              where
                                  (Int16)row["Fmd_id"] == Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"])
                                   && (Int16)row["Flag"] == 2
                              select row).Count();
                if (Rec_No > 0)
                {

                    try
                    {

                        Btn_Next.Enabled = true;
                        Y = Y - 1;
                        DataTable DT = new DataTable();
                        DT = Pic_Tbl.DefaultView.ToTable(false, "Fmd_id", "Flag", "Doc_Pic").Select(" Flag = 2 and Fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"])).CopyToDataTable();
                        img = (Byte[])(DT.Rows[Y]["Doc_Pic"]);
                        MemoryStream mem = new MemoryStream(img);
                        pictureBox1.Image = Image.FromStream(mem);
                    }
                    catch
                    {
                        Btn_Previous.Enabled = false;
                        Y = Y + 1;
                    }
                }
            }
        }
        //----------------------------------
        private void Btn_Next_Click(object sender, EventArgs e)
        {
            if (ISSpec == 0)
            {
                int Rec_No = (from DataRow row in Pic_Tbl.Rows
                              where
                                  (Int16)row["Fmd_id"] == Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])
                                   && (Int16)row["Flag"] == 1
                              select row).Count();
                if (Rec_No > 0)
                {
                    try
                    {

                        Btn_Previous.Enabled = true;
                        X = X + 1;
                        DataTable DT = new DataTable();
                        DT = Pic_Tbl.DefaultView.ToTable(false, "Fmd_id", "Flag", "Doc_Pic").Select("Flag = 1 and   Fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Info.Current).Row["fmd_id"])).CopyToDataTable();
                        img = (Byte[])(DT.Rows[X]["Doc_Pic"]);
                        MemoryStream mem = new MemoryStream(img);
                        pictureBox1.Image = Image.FromStream(mem);

                    }
                    catch
                    {
                        Btn_Next.Enabled = false;
                        X = X - 1;
                    }
                }
            }
            else
            {
                int Rec_No = (from DataRow row in Pic_Tbl.Rows
                              where
                                  (Int16)row["Fmd_id"] == Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"])
                                   && (Int16)row["Flag"] == 2
                              select row).Count();
                if (Rec_No > 0)
                {
                    try
                    {

                        Btn_Previous.Enabled = true;
                        Y = Y + 1;
                        DataTable DT = new DataTable();
                        DT = Pic_Tbl.DefaultView.ToTable(false, "Fmd_id", "Flag", "Doc_Pic").Select(" Flag = 2 and  Fmd_id = " + Convert.ToInt16(((DataRowView)_Bs_Doc_Spc.Current).Row["Special_Fmd_ID"])).CopyToDataTable();
                        img = (Byte[])(DT.Rows[Y]["Doc_Pic"]);

                        MemoryStream mem = new MemoryStream(img);
                        pictureBox1.Image = Image.FromStream(mem);

                    }
                    catch
                    {
                        Btn_Next.Enabled = false;
                        Y = Y - 1;
                    }
                }
            }
        }
        //----------------------------------
        private void Grd_Doc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                change = true;
                Grd_Doc_SelectionChanged(null, null);
                change = false;

            }
            if (e.ColumnIndex == 1)
            {

                ISDoc_ORSpec = 1;// عند اضافة وثائق شخصية
                Add_Doc_Click(sender, e);

            }
            if (e.ColumnIndex == 2)
            {
                ISDoc_ORSpec = 1;// عند حذف وثائق شخصية
                Del_Doc_Click(sender, e);
                change = true;
                Grd_Doc_SelectionChanged(null, null);
                change = false;
            }
          
        }
        //----------------------------------
        private void Grd_Doc_Spc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                change_SPC = true;
                Grd_Doc_Spc_SelectionChanged(null, null);
                change_SPC = false;

            }
            if (e.ColumnIndex == 1)
            {
                ISDoc_ORSpec = 2;// وئاثق معنوية
                Add_Doc_Click(sender, e);
            }
            if (e.ColumnIndex == 2)
            {
                ISDoc_ORSpec = 2;// وئاثق معنوية حذف
                Del_Doc_Click(sender, e);
                change_SPC = true;
                Grd_Doc_Spc_SelectionChanged(null, null);
                change_SPC = false;
            }
        }
        //----------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            string Sql_text = "SELECT top(1)  server_name, UID, PWD, db_name FROM Agencies   " ;
            connection.SqlExec(Sql_text, "server_Tbl");

            string connStr = @"server = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["server_name"].ToString() + " ; database =" + connection.SQLDS.Tables["server_Tbl"].Rows[0]["db_name"].ToString()
                + " ; user id = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["UID"].ToString() + " ; password = " + connection.SQLDS.Tables["server_Tbl"].Rows[0]["PWD"].ToString() + " ; connection timeout = 0";
            DataTable DT_pic = new DataTable();
            DT_pic = Pic_Tbl.DefaultView.ToTable(false, "Fmd_ID", "Fmd_AName", "Fmd_EName", "Doc_Pic", "Doc_pic_name", "Flag").Select().CopyToDataTable();
            connection.SQLCMD.Parameters.AddWithValue("@Doc_Pic_Tbl", DT_pic);
            //connection.SQLCMD.Parameters.AddWithValue("@T_id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@User_id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Per_id", Person_id);
            connection.SQLCMD.Parameters.AddWithValue("@Per_AName", Person_AName);
            connection.SQLCMD.Parameters.AddWithValue("@Per_EName", Person_EName);
            connection.SQLCMD.Parameters.AddWithValue("@User_Name", connection.User_Name  );
            connection.SQLCMD.Parameters.AddWithValue("@From_id", 1);//اضافة
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Count", SqlDbType.TinyInt).Value = 0;
            connection.SQLCMD.Parameters["@Param_Count"].Direction = ParameterDirection.Output;

            connection.SqlExec("Add_Person_Doc", connection.SQLCMD, connStr);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                //this.Enabled = true;
                return;
            }
            string Sqltext = "Update person_info set upload_flag = 1 where per_id = " + Person_id;
            connection.SqlExec(Sqltext, "TBL_Upload_Chk");


            this.Close();
            connection.SQLCMD.Parameters.Clear();
        }
        //----------------------------------
        private void Grd_Doc_KeyDown(object sender, KeyEventArgs e)
        {
            change = true;
            Grd_Doc_SelectionChanged(null, null);
            change = false;
        }
        //----------------------------------
        private void Grd_Doc_KeyUp(object sender, KeyEventArgs e)
        {
            change = true;
            Grd_Doc_SelectionChanged(null, null);
            change = false;
        }
        //----------------------------------
        private void Grd_Doc_Spc_KeyUp(object sender, KeyEventArgs e)
        {
            change_SPC = true;
            Grd_Doc_Spc_SelectionChanged(null, null);
            change_SPC = false;
        }
        //----------------------------------
        private void Grd_Doc_Spc_KeyDown(object sender, KeyEventArgs e)
        {
            change_SPC = true;
            Grd_Doc_Spc_SelectionChanged(null, null);
            change_SPC = false;
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Person_Document_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
            change_SPC = false;
            string machineName = System.Environment.GetEnvironmentVariable("TEMP");
            DirectoryInfo dir = new DirectoryInfo(machineName + "/");

            foreach (FileInfo files in dir.GetFiles())
            {
                try
                {
                    files.Delete();
                }
                catch { };
            }

            foreach (DirectoryInfo dirs in dir.GetDirectories())
            {
                try
                {
                    dirs.Delete(true);
                }
                catch { };
            }


            string[] Used_Tbl = { "Doc_TBL", "Doc_TBL1", "TBL_Upload_Chk" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                   
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                  
                }
            }
        }
    }
}