﻿namespace Integration_Accounting_Sys
{
    partial class Person_Info_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtCust_Aname = new System.Windows.Forms.TextBox();
            this.TxtCust_Ename = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.CboDoc_id = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CboNat_id = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CboCit_Id = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Cbo_Gender = new System.Windows.Forms.ComboBox();
            this.Txt_Another_Phone = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_Street = new System.Windows.Forms.TextBox();
            this.Txt_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Post_code = new System.Windows.Forms.TextBox();
            this.Txt_State = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.Txt_Mother_name = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_birth_place = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Mother_Ename = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Txt_Ebirth_place = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtIss_EDoc = new System.Windows.Forms.TextBox();
            this.Txt_EState = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_EStreet = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.Txt_ESuburb = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.Txt_Social_ID = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.resd_cmb = new System.Windows.Forms.ComboBox();
            this.Cbo_Occupation = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtBirth_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtDoc_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtExp_Da = new System.Windows.Forms.DateTimePicker();
            this.Txt_details_job = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxtCust_Aname
            // 
            this.TxtCust_Aname.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Aname.Location = new System.Drawing.Point(96, 39);
            this.TxtCust_Aname.MaxLength = 200;
            this.TxtCust_Aname.Name = "TxtCust_Aname";
            this.TxtCust_Aname.Size = new System.Drawing.Size(376, 23);
            this.TxtCust_Aname.TabIndex = 0;
            // 
            // TxtCust_Ename
            // 
            this.TxtCust_Ename.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCust_Ename.Location = new System.Drawing.Point(577, 39);
            this.TxtCust_Ename.MaxLength = 200;
            this.TxtCust_Ename.Name = "TxtCust_Ename";
            this.TxtCust_Ename.Size = new System.Drawing.Size(376, 23);
            this.TxtCust_Ename.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(3, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 16);
            this.label4.TabIndex = 53;
            this.label4.Text = "الاســـم العربــي:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(483, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 54;
            this.label5.Text = "الاســم الاجنبـــي:";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(479, 529);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 30;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(390, 529);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 26);
            this.AddBtn.TabIndex = 29;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(8, 341);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 16);
            this.label16.TabIndex = 64;
            this.label16.Text = "تاريخ الاصدار:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(513, 341);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 16);
            this.label17.TabIndex = 63;
            this.label17.Text = "تاريخ النفـــــــاذ:";
            // 
            // CboDoc_id
            // 
            this.CboDoc_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDoc_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDoc_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDoc_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDoc_id.FormattingEnabled = true;
            this.CboDoc_id.Location = new System.Drawing.Point(108, 308);
            this.CboDoc_id.Name = "CboDoc_id";
            this.CboDoc_id.Size = new System.Drawing.Size(252, 24);
            this.CboDoc_id.TabIndex = 15;
            this.CboDoc_id.SelectedIndexChanged += new System.EventHandler(this.CboDoc_id_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(5, 316);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 84;
            this.label13.Text = "نوع الوثيــقــــة:";
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(606, 311);
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(252, 23);
            this.TxtNo_Doc.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(515, 314);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 60;
            this.label14.Text = "رقم الوثيقـــــــــة:";
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(111, 366);
            this.TxtIss_Doc.MaxLength = 50;
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(252, 23);
            this.TxtIss_Doc.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(-1, 369);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 16);
            this.label15.TabIndex = 59;
            this.label15.Text = "جـهـة الاصــدار العربي:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(526, 151);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 62;
            this.label10.Text = "تاريخ التولــــد:";
            // 
            // CboNat_id
            // 
            this.CboNat_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboNat_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboNat_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboNat_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNat_id.FormattingEnabled = true;
            this.CboNat_id.Location = new System.Drawing.Point(101, 147);
            this.CboNat_id.Name = "CboNat_id";
            this.CboNat_id.Size = new System.Drawing.Size(364, 24);
            this.CboNat_id.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(5, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 16);
            this.label11.TabIndex = 66;
            this.label11.Text = "الجنسيــــــــــــة:";
            // 
            // TxtPhone
            // 
            this.TxtPhone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(608, 176);
            this.TxtPhone.MaxLength = 20;
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(252, 23);
            this.TxtPhone.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(503, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 16);
            this.label12.TabIndex = 47;
            this.label12.Text = "رقــم الهــــاتــف 1:";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(608, 121);
            this.TxtEmail.MaxLength = 30;
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(252, 23);
            this.TxtEmail.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(518, 124);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 16);
            this.label9.TabIndex = 48;
            this.label9.Text = "البريد الالكتروني:";
            // 
            // CboCit_Id
            // 
            this.CboCit_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCit_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCit_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCit_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCit_Id.FormattingEnabled = true;
            this.CboCit_Id.Location = new System.Drawing.Point(101, 120);
            this.CboCit_Id.Name = "CboCit_Id";
            this.CboCit_Id.Size = new System.Drawing.Size(364, 24);
            this.CboCit_Id.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(5, 124);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 16);
            this.label7.TabIndex = 37;
            this.label7.Text = "المدينـة / البلـــد :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 521);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(977, 2);
            this.flowLayoutPanel1.TabIndex = 85;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(0, 31);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(970, 2);
            this.flowLayoutPanel7.TabIndex = 497;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(96, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(250, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(8, 7);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 492;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(9, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 502;
            this.label1.Text = "الجنـــــــــــس:";
            // 
            // Cbo_Gender
            // 
            this.Cbo_Gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Gender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Gender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Gender.FormattingEnabled = true;
            this.Cbo_Gender.Location = new System.Drawing.Point(101, 175);
            this.Cbo_Gender.Name = "Cbo_Gender";
            this.Cbo_Gender.Size = new System.Drawing.Size(252, 24);
            this.Cbo_Gender.TabIndex = 8;
            // 
            // Txt_Another_Phone
            // 
            this.Txt_Another_Phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Another_Phone.Location = new System.Drawing.Point(608, 205);
            this.Txt_Another_Phone.MaxLength = 20;
            this.Txt_Another_Phone.Name = "Txt_Another_Phone";
            this.Txt_Another_Phone.Size = new System.Drawing.Size(250, 23);
            this.Txt_Another_Phone.TabIndex = 11;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(504, 208);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 16);
            this.label21.TabIndex = 508;
            this.label21.Text = "رقــم الهــــاتــف 2:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(8, 437);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 16);
            this.label22.TabIndex = 509;
            this.label22.Text = "زقـــــــــــاق العربي :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(4, 465);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 16);
            this.label23.TabIndex = 510;
            this.label23.Text = "الحــــــــــــــي العربي:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(35, 492);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 16);
            this.label24.TabIndex = 511;
            this.label24.Text = "الرمز البريدي:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(0, 414);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(116, 16);
            this.label25.TabIndex = 512;
            this.label25.Text = "الولايــــــــــــــة العربي:";
            // 
            // Txt_Street
            // 
            this.Txt_Street.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Street.Location = new System.Drawing.Point(116, 434);
            this.Txt_Street.MaxLength = 100;
            this.Txt_Street.Name = "Txt_Street";
            this.Txt_Street.Size = new System.Drawing.Size(252, 23);
            this.Txt_Street.TabIndex = 23;
            // 
            // Txt_Suburb
            // 
            this.Txt_Suburb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Suburb.Location = new System.Drawing.Point(116, 462);
            this.Txt_Suburb.MaxLength = 100;
            this.Txt_Suburb.Name = "Txt_Suburb";
            this.Txt_Suburb.Size = new System.Drawing.Size(252, 23);
            this.Txt_Suburb.TabIndex = 25;
            // 
            // Txt_Post_code
            // 
            this.Txt_Post_code.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Post_code.Location = new System.Drawing.Point(116, 489);
            this.Txt_Post_code.MaxLength = 100;
            this.Txt_Post_code.Name = "Txt_Post_code";
            this.Txt_Post_code.Size = new System.Drawing.Size(252, 23);
            this.Txt_Post_code.TabIndex = 27;
            // 
            // Txt_State
            // 
            this.Txt_State.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_State.Location = new System.Drawing.Point(116, 411);
            this.Txt_State.MaxLength = 100;
            this.Txt_State.Name = "Txt_State";
            this.Txt_State.Size = new System.Drawing.Size(252, 23);
            this.Txt_State.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(1, 390);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 14);
            this.label6.TabIndex = 531;
            this.label6.Text = "الــعنـــــــــــــــوان....";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1, 401);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(972, 1);
            this.flowLayoutPanel2.TabIndex = 530;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(5, 288);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 14);
            this.label19.TabIndex = 533;
            this.label19.Text = "الوثــــــــــــــــيقة....";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-2, 300);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(971, 1);
            this.flowLayoutPanel3.TabIndex = 532;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(3, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 16);
            this.label20.TabIndex = 535;
            this.label20.Text = "أسـم الام العربي";
            // 
            // Txt_Mother_name
            // 
            this.Txt_Mother_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Mother_name.Location = new System.Drawing.Point(96, 67);
            this.Txt_Mother_name.MaxLength = 150;
            this.Txt_Mother_name.Name = "Txt_Mother_name";
            this.Txt_Mother_name.Size = new System.Drawing.Size(376, 23);
            this.Txt_Mother_name.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Maroon;
            this.label26.Location = new System.Drawing.Point(-1, 93);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(155, 14);
            this.label26.TabIndex = 537;
            this.label26.Text = "المعلومـــات العامــــــــة ....";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-2, 102);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(967, 1);
            this.flowLayoutPanel4.TabIndex = 536;
            // 
            // Txt_birth_place
            // 
            this.Txt_birth_place.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_birth_place.Location = new System.Drawing.Point(101, 233);
            this.Txt_birth_place.MaxLength = 200;
            this.Txt_birth_place.Name = "Txt_birth_place";
            this.Txt_birth_place.Size = new System.Drawing.Size(252, 23);
            this.Txt_birth_place.TabIndex = 12;
            //this.Txt_birth_place.TextChanged += new System.EventHandler(this.Txt_birth_place_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(3, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 16);
            this.label3.TabIndex = 538;
            this.label3.Text = "مكان التولد العربي:";
            //this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Txt_Mother_Ename
            // 
            this.Txt_Mother_Ename.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Mother_Ename.Location = new System.Drawing.Point(577, 68);
            this.Txt_Mother_Ename.MaxLength = 200;
            this.Txt_Mother_Ename.Name = "Txt_Mother_Ename";
            this.Txt_Mother_Ename.Size = new System.Drawing.Size(376, 23);
            this.Txt_Mother_Ename.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(488, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 16);
            this.label18.TabIndex = 541;
            this.label18.Text = "أسم الام الاجنبي";
            // 
            // Txt_Ebirth_place
            // 
            this.Txt_Ebirth_place.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Ebirth_place.Location = new System.Drawing.Point(606, 233);
            this.Txt_Ebirth_place.MaxLength = 200;
            this.Txt_Ebirth_place.Name = "Txt_Ebirth_place";
            this.Txt_Ebirth_place.Size = new System.Drawing.Size(252, 23);
            this.Txt_Ebirth_place.TabIndex = 13;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(501, 236);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(103, 16);
            this.label27.TabIndex = 542;
            this.label27.Text = "مكان التولد الاجنبي:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(486, 369);
            this.label29.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(118, 16);
            this.label29.TabIndex = 547;
            this.label29.Text = "جـهـة الاصــدار الاجنبي:";
            // 
            // TxtIss_EDoc
            // 
            this.TxtIss_EDoc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_EDoc.Location = new System.Drawing.Point(606, 366);
            this.TxtIss_EDoc.MaxLength = 200;
            this.TxtIss_EDoc.Name = "TxtIss_EDoc";
            this.TxtIss_EDoc.Size = new System.Drawing.Size(252, 23);
            this.TxtIss_EDoc.TabIndex = 20;
            // 
            // Txt_EState
            // 
            this.Txt_EState.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EState.Location = new System.Drawing.Point(599, 407);
            this.Txt_EState.MaxLength = 100;
            this.Txt_EState.Name = "Txt_EState";
            this.Txt_EState.Size = new System.Drawing.Size(252, 23);
            this.Txt_EState.TabIndex = 22;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(472, 414);
            this.label30.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(120, 16);
            this.label30.TabIndex = 548;
            this.label30.Text = "الولايــــــــــــــة الاجنبي:";
            // 
            // Txt_EStreet
            // 
            this.Txt_EStreet.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EStreet.Location = new System.Drawing.Point(596, 434);
            this.Txt_EStreet.MaxLength = 100;
            this.Txt_EStreet.Name = "Txt_EStreet";
            this.Txt_EStreet.Size = new System.Drawing.Size(252, 23);
            this.Txt_EStreet.TabIndex = 24;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(481, 437);
            this.label31.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(111, 16);
            this.label31.TabIndex = 550;
            this.label31.Text = "زقـــــــــــاق الاجنبي :";
            // 
            // Txt_ESuburb
            // 
            this.Txt_ESuburb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ESuburb.Location = new System.Drawing.Point(596, 462);
            this.Txt_ESuburb.MaxLength = 100;
            this.Txt_ESuburb.Name = "Txt_ESuburb";
            this.Txt_ESuburb.Size = new System.Drawing.Size(252, 23);
            this.Txt_ESuburb.TabIndex = 26;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(479, 465);
            this.label32.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(116, 16);
            this.label32.TabIndex = 552;
            this.label32.Text = "الحــــــــــــــي الاجنبي:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(15, 208);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 554;
            this.label33.Text = "الرقم الوطني :";
            // 
            // Txt_Social_ID
            // 
            this.Txt_Social_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Social_ID.Location = new System.Drawing.Point(102, 205);
            this.Txt_Social_ID.Name = "Txt_Social_ID";
            this.Txt_Social_ID.Size = new System.Drawing.Size(252, 23);
            this.Txt_Social_ID.TabIndex = 10;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(494, 492);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(94, 16);
            this.label50.TabIndex = 917;
            this.label50.Text = "نوع الاقامــــــــــة:";
            // 
            // resd_cmb
            // 
            this.resd_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_cmb.FormattingEnabled = true;
            this.resd_cmb.Location = new System.Drawing.Point(596, 489);
            this.resd_cmb.Name = "resd_cmb";
            this.resd_cmb.Size = new System.Drawing.Size(252, 24);
            this.resd_cmb.TabIndex = 28;
            // 
            // Cbo_Occupation
            // 
            this.Cbo_Occupation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Occupation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Occupation.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Occupation.FormattingEnabled = true;
            this.Cbo_Occupation.Location = new System.Drawing.Point(101, 262);
            this.Cbo_Occupation.Name = "Cbo_Occupation";
            this.Cbo_Occupation.Size = new System.Drawing.Size(253, 24);
            this.Cbo_Occupation.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(6, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 14);
            this.label8.TabIndex = 918;
            this.label8.Text = "المـهنــــــــــــة:";
            // 
            // TxtBirth_Da
            // 
            this.TxtBirth_Da.CustomFormat = "dd/mm/yyyy";
            this.TxtBirth_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtBirth_Da.Location = new System.Drawing.Point(610, 151);
            this.TxtBirth_Da.Name = "TxtBirth_Da";
            this.TxtBirth_Da.Size = new System.Drawing.Size(248, 20);
            this.TxtBirth_Da.TabIndex = 7;
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.CustomFormat = "dd/mm/yyyy";
            this.TxtDoc_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtDoc_Da.Location = new System.Drawing.Point(108, 339);
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.Size = new System.Drawing.Size(252, 20);
            this.TxtDoc_Da.TabIndex = 17;
            // 
            // TxtExp_Da
            // 
            this.TxtExp_Da.CustomFormat = "dd/mm/yyyy";
            this.TxtExp_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtExp_Da.Location = new System.Drawing.Point(606, 339);
            this.TxtExp_Da.Name = "TxtExp_Da";
            this.TxtExp_Da.Size = new System.Drawing.Size(252, 20);
            this.TxtExp_Da.TabIndex = 18;
            // 
            // Txt_details_job
            // 
            this.Txt_details_job.Location = new System.Drawing.Point(606, 263);
            this.Txt_details_job.MaxLength = 99;
            this.Txt_details_job.Name = "Txt_details_job";
            this.Txt_details_job.Size = new System.Drawing.Size(252, 20);
            this.Txt_details_job.TabIndex = 919;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(504, 265);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(77, 16);
            this.label35.TabIndex = 920;
            this.label35.Text = "تفاصيل العمل :";
            // 
            // Person_Info_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 558);
            this.Controls.Add(this.Txt_details_job);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.TxtExp_Da);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.TxtBirth_Da);
            this.Controls.Add(this.Cbo_Occupation);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.resd_cmb);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.Txt_Social_ID);
            this.Controls.Add(this.Txt_ESuburb);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.Txt_EStreet);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.Txt_EState);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.TxtIss_EDoc);
            this.Controls.Add(this.Txt_Ebirth_place);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Txt_Mother_Ename);
            this.Controls.Add(this.Txt_birth_place);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Txt_Mother_name);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Txt_State);
            this.Controls.Add(this.Txt_Post_code);
            this.Controls.Add(this.Txt_Suburb);
            this.Controls.Add(this.Txt_Street);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Txt_Another_Phone);
            this.Controls.Add(this.Cbo_Gender);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.CboDoc_id);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CboNat_id);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CboCit_Id);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtCust_Ename);
            this.Controls.Add(this.TxtCust_Aname);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Person_Info_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "475";
            this.Text = "Person_Info_Add_Upd";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Customer_Add_Upd_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Customer_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.Customer_Add_Upd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtCust_Aname;
        private System.Windows.Forms.TextBox TxtCust_Ename;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox CboDoc_id;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CboNat_id;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CboCit_Id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Cbo_Gender;
        private System.Windows.Forms.TextBox Txt_Another_Phone;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_Street;
        private System.Windows.Forms.TextBox Txt_Suburb;
        private System.Windows.Forms.TextBox Txt_Post_code;
        private System.Windows.Forms.TextBox Txt_State;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Txt_Mother_name;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.TextBox Txt_birth_place;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Mother_Ename;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Txt_Ebirth_place;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox TxtIss_EDoc;
        private System.Windows.Forms.TextBox Txt_EState;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_EStreet;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txt_ESuburb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox Txt_Social_ID;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox resd_cmb;
        private System.Windows.Forms.ComboBox Cbo_Occupation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker TxtBirth_Da;
        private System.Windows.Forms.DateTimePicker TxtDoc_Da;
        private System.Windows.Forms.DateTimePicker TxtExp_Da;
        private System.Windows.Forms.TextBox Txt_details_job;
        private System.Windows.Forms.Label label35;
    }
}