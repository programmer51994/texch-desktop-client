﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Hst_Person_Info_main : Form
    {
        bool change = false;
        BindingSource Bs_Hst = new BindingSource();
        int hst_per_id ;
        public Hst_Person_Info_main(int per_id )
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, new MaskedTextBox() );

             hst_per_id = per_id;
             Hst_Grd_Per.AutoGenerateColumns = false;
        }

        private void Hst_Person_Info_main_Load(object sender, EventArgs e)
        {

            change = false;
            Bs_Hst.DataSource = connection.SqlExec("main_person_info "
                                                            + hst_per_id + ","
                                                            + "'Hst_Person_Info'", "Hst_per_Tbl");




            if (connection.SQLDS.Tables["Hst_per_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات تاريخية لهذا الشخص" : "No History Info. for this Person");
                this.Close();
            }



            //TxtCity.DataBindings.Clear();
            //TxtNat_Name.DataBindings.Clear();
            //TxtPhone.DataBindings.Clear();
            //TxtEmail.DataBindings.Clear();
            //Txt_Another_phone.DataBindings.Clear();           
            //Txt_Gender.DataBindings.Clear();
            //Txt_Occupation.DataBindings.Clear();
            //Txt_Post_code.DataBindings.Clear();
            //Txt_State.DataBindings.Clear();
            //Txt_Street.DataBindings.Clear();
            //Txt_Suburb.DataBindings.Clear();
           

            //TxtCity.DataBindings.Add("Text", Bs_Hst, connection.Lang_id == 1 ? "Per_ACITY_NAME" : "Per_ECITY_NAME");
            //TxtNat_Name.DataBindings.Add("Text", Bs_Hst, connection.Lang_id == 1 ? "Per_A_NAT_NAME" : "Per_E_NAT_NAME");
            //TxtPhone.DataBindings.Add("Text", Bs_Hst, "per_Phone");
            //TxtEmail.DataBindings.Add("Text", Bs_Hst, "per_Email");
            //Txt_Another_phone.DataBindings.Add("Text", Bs_Hst, "per_Another_Phone");
            //Txt_Gender.DataBindings.Add("Text", Bs_Hst, connection.Lang_id == 1 ? "Per_Gender_Aname" : "Per_Gender_Ename");
            //Txt_Occupation.DataBindings.Add("Text", Bs_Hst, "per_Occupation");
            //Txt_Post_code.DataBindings.Add("Text", Bs_Hst, "Per_Post_Code");
            //Txt_State.DataBindings.Add("Text", Bs_Hst, "Per_State");
            //Txt_Street.DataBindings.Add("Text", Bs_Hst, "Per_Street");
            //Txt_Suburb.DataBindings.Add("Text", Bs_Hst, "Per_Suburb");

            TxtCityAR.DataBindings.Clear();
            TxtNat_NameAR.DataBindings.Clear();
            TxtPhoneAR.DataBindings.Clear();
            TxtEmail.DataBindings.Clear();
            Txt_Another_phone.DataBindings.Clear();
            Txt_GenderAR.DataBindings.Clear();
            Txt_OccupationAR.DataBindings.Clear();
            Txt_Post_code.DataBindings.Clear();
            Txt_StateAR.DataBindings.Clear();
            Txt_StreetAR.DataBindings.Clear();
            Txt_SuburbAR.DataBindings.Clear();

            Txt_OccupationEN.DataBindings.Clear();
            Txt_GenderEN.DataBindings.Clear();
            TxtNat_NameEN.DataBindings.Clear();
            TxtCityEN.DataBindings.Clear();
            Txt_StateEN.DataBindings.Clear();
            Txt_StreetEN.DataBindings.Clear();
            Txt_SuburbEN.DataBindings.Clear();
            SOCIAL_NO.DataBindings.Clear();




            TxtCityAR.DataBindings.Add("Text", Bs_Hst, "Per_ACITY_NAME");
            TxtNat_NameAR.DataBindings.Add("Text", Bs_Hst, "Per_A_NAT_NAME");
            Txt_GenderAR.DataBindings.Add("Text", Bs_Hst, "Per_Gender_Aname");
            TxtPhoneAR.DataBindings.Add("Text", Bs_Hst, "per_Phone");
            TxtEmail.DataBindings.Add("Text", Bs_Hst, "per_Email");
            Txt_Another_phone.DataBindings.Add("Text", Bs_Hst, "per_Another_Phone");
            Txt_OccupationAR.DataBindings.Add("Text", Bs_Hst, "per_Occupation");
            Txt_Post_code.DataBindings.Add("Text", Bs_Hst, "Per_Post_Code");
            Txt_StateAR.DataBindings.Add("Text", Bs_Hst, "Per_State");
            Txt_StreetAR.DataBindings.Add("Text", Bs_Hst, "Per_Street");
            Txt_SuburbAR.DataBindings.Add("Text", Bs_Hst, "Per_Suburb");

            TxtCityEN.DataBindings.Add("Text", Bs_Hst, "Per_ECITY_NAME");
            TxtNat_NameEN.DataBindings.Add("Text", Bs_Hst, "Per_E_NAT_NAME");
            Txt_GenderEN.DataBindings.Add("Text", Bs_Hst, "Per_Gender_Ename");
            Txt_StateEN.DataBindings.Add("Text", Bs_Hst, "Per_EState");
            Txt_StreetEN.DataBindings.Add("Text", Bs_Hst, "Per_EStreet");
            Txt_SuburbEN.DataBindings.Add("Text", Bs_Hst, "Per_ESuburb");
            Txt_OccupationEN.DataBindings.Add("Text", Bs_Hst, "EOccupation");
            SOCIAL_NO.DataBindings.Add("Text", Bs_Hst, "Social_No");
          



            if (connection.SQLDS.Tables["Hst_per_Tbl"].Rows.Count > 0)
            {
                Hst_Grd_Per.DataSource = Bs_Hst;
                change = true;
                Hst_Grd_Per_SelectionChanged(null, null);

            }


        }

        private void Hst_Grd_Per_SelectionChanged(object sender, EventArgs e)
        {
            if (change)
            {
                LblRec.Text = connection.Records(Bs_Hst);
            }
        }

        private void Hst_Person_Info_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;

            string[] Used_Tbl = { "Hst_per_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

  
    }
}
