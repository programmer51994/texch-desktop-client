﻿namespace Integration_Accounting_Sys
{
    partial class Add_First_T_Balance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.Grd_First_Acc = new System.Windows.Forms.DataGridView();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.cbot_id = new System.Windows.Forms.ComboBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.TXT_SDebit = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TXT_SCredit = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_First_Acc)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(391, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 14);
            this.label2.TabIndex = 625;
            this.label2.Text = "المستـخدم:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_UsrName.Location = new System.Drawing.Point(470, 6);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.Size = new System.Drawing.Size(185, 23);
            this.Txt_UsrName.TabIndex = 624;
            // 
            // Grd_First_Acc
            // 
            this.Grd_First_Acc.AllowUserToAddRows = false;
            this.Grd_First_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_First_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_First_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_First_Acc.ColumnHeadersHeight = 30;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_First_Acc.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_First_Acc.Location = new System.Drawing.Point(10, 42);
            this.Grd_First_Acc.Name = "Grd_First_Acc";
            this.Grd_First_Acc.RowTemplate.DefaultCellStyle.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.Grd_First_Acc.Size = new System.Drawing.Size(986, 437);
            this.Grd_First_Acc.TabIndex = 629;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(415, 531);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(90, 29);
            this.Btn_Add.TabIndex = 630;
            this.Btn_Add.Text = "الحاق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_OK.ForeColor = System.Drawing.Color.Navy;
            this.Btn_OK.Location = new System.Drawing.Point(504, 531);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(90, 29);
            this.Btn_OK.TabIndex = 631;
            this.Btn_OK.Text = "موافق";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbot_id
            // 
            this.cbot_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbot_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbot_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbot_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbot_id.FormattingEnabled = true;
            this.cbot_id.Location = new System.Drawing.Point(102, 5);
            this.cbot_id.Name = "cbot_id";
            this.cbot_id.Size = new System.Drawing.Size(217, 24);
            this.cbot_id.TabIndex = 633;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.Color.Navy;
            this.Label6.Location = new System.Drawing.Point(23, 10);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label6.Size = new System.Drawing.Size(76, 14);
            this.Label6.TabIndex = 632;
            this.Label6.Text = "الفــــــــــــرع:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(54, 497);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(188, 14);
            this.label5.TabIndex = 635;
            this.label5.Text = "المجموع بالعملة المحليــة مدين:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(577, 497);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(185, 14);
            this.label3.TabIndex = 634;
            this.label3.Text = "المجموع بالعملة المحليــة دائن:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(760, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 14);
            this.label4.TabIndex = 638;
            this.label4.Text = "تـــأريـخ :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-2, 34);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1010, 2);
            this.flowLayoutPanel1.TabIndex = 639;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-2, 485);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1010, 2);
            this.flowLayoutPanel2.TabIndex = 640;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-2, 523);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1010, 2);
            this.flowLayoutPanel3.TabIndex = 640;
            // 
            // TXT_SDebit
            // 
            this.TXT_SDebit.BackColor = System.Drawing.Color.White;
            this.TXT_SDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_SDebit.Enabled = false;
            this.TXT_SDebit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_SDebit.ForeColor = System.Drawing.Color.Black;
            this.TXT_SDebit.Location = new System.Drawing.Point(245, 493);
            this.TXT_SDebit.Name = "TXT_SDebit";
            this.TXT_SDebit.NumberDecimalDigits = 3;
            this.TXT_SDebit.NumberDecimalSeparator = ".";
            this.TXT_SDebit.NumberGroupSeparator = ",";
            this.TXT_SDebit.Size = new System.Drawing.Size(203, 23);
            this.TXT_SDebit.TabIndex = 637;
            this.TXT_SDebit.Text = "0.000";
            // 
            // TXT_SCredit
            // 
            this.TXT_SCredit.BackColor = System.Drawing.Color.White;
            this.TXT_SCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_SCredit.Enabled = false;
            this.TXT_SCredit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_SCredit.ForeColor = System.Drawing.Color.Black;
            this.TXT_SCredit.Location = new System.Drawing.Point(765, 493);
            this.TXT_SCredit.Name = "TXT_SCredit";
            this.TXT_SCredit.NumberDecimalDigits = 3;
            this.TXT_SCredit.NumberDecimalSeparator = ".";
            this.TXT_SCredit.NumberGroupSeparator = ",";
            this.TXT_SCredit.Size = new System.Drawing.Size(188, 23);
            this.TXT_SCredit.TabIndex = 636;
            this.TXT_SCredit.Text = "0.000";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(820, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(176, 23);
            this.TxtIn_Rec_Date.TabIndex = 709;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // Add_First_T_Balance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 565);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TXT_SDebit);
            this.Controls.Add(this.TXT_SCredit);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbot_id);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Grd_First_Acc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_UsrName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Add_First_T_Balance";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "438";
            this.Text = "Add_First_T_Balance";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Add_First_T_Balance_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Add_First_T_Balance_FormClosed);
            this.Load += new System.EventHandler(this.Add_First_T_Balance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_First_Acc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.DataGridView Grd_First_Acc;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.ComboBox cbot_id;
        private System.Windows.Forms.Label Label6;
        private System.Windows.Forms.Sample.DecimalTextBox TXT_SDebit;
        private System.Windows.Forms.Sample.DecimalTextBox TXT_SCredit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private MyDateTextBox TxtIn_Rec_Date;
    }
}