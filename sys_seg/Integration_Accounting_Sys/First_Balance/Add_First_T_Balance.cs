﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Add_First_T_Balance : Form
    {
        int Rec_Id = 1;
        string Sql_Text = "";
        string conUserID = "";
        string conPassword = "";
        string conServerIP = "";
        string conData_Base = "";
        Int16  Flag = 0;
        public Add_First_T_Balance()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            Grd_First_Acc.AutoGenerateColumns = true;
        }

        private void Add_First_T_Balance_Load(object sender, EventArgs e)
        {
            MaskedTextBox Txt = new MaskedTextBox();
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);

            String Sql_Text = " Select convert(varchar(12), STARTDATE ,111) As STARTDATE From  TERMINALS  where t_id = " + connection.T_ID;
            TxtIn_Rec_Date.Text = connection.SqlExec(Sql_Text, "Date_TbL").Rows[0]["STARTDATE"].ToString();

        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            MyExports.ImportExcels("", false);
            Grd_First_Acc.DataSource = MyExports.output.Tables[0];

             foreach (DataRow Row in MyExports.output.Tables[0].Rows)
            {
                Row["تسلسل"] = Rec_Id++;
                Row["المبلغ الاصلي"] = decimal.Round(Convert.ToDecimal(Row["المبلغ الاصلي"]), 3);
                Row["المبلغ المحلي"] = decimal.Round(Convert.ToDecimal(Row["المبلغ المحلي"]), 3);
            }

             MyExports.output.Tables[0].Columns["المبلغ المحلي"].ColumnName = "oloc_amount";
            TXT_SCredit.ResetText(); TXT_SDebit.ResetText();
            TXT_SDebit.Text = MyExports.output.Tables[0].Compute("Sum(oloc_amount)", "oloc_amount > 0").ToString();
            TXT_SCredit.Text = MyExports.output.Tables[0].Compute("Sum(oloc_amount)", "oloc_amount < 0").ToString();
            MyExports.output.Tables[0].Columns["oloc_amount"].ColumnName = "المبلغ المحلي";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            connection.SqlExec("Select Count(*)  as count from Daily_Opening where t_id =" + connection.T_ID, "Check_tbl");
            if (Convert.ToInt16(connection.SQLDS.Tables["Check_tbl"].Rows[0]["count"]) == 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد اغلاق ارصدة الافتتاح ؟" :
                  "Do you want to close open fixed balance", MyGeneral_Lib.LblCap,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Dr == DialogResult.Yes)
                {
                    Flag = 1;//اغلاق   
                }
                else
                {
                    Flag = 2;//افتتاح

                }
            }
            if (Math.Abs(Convert.ToDecimal(TXT_SCredit.Text)) != Math.Abs(Convert.ToDecimal(TXT_SDebit.Text)))
            {
                MessageBox.Show(connection.Lang_id==1?"لا يمكن الاضافة لان القيد غير متوازن":"Can not add the record unbalanced",MyGeneral_Lib.LblCap);
                TXT_SDebit.Focus();
                return;
            }

            MyExports.output.Tables[0].Columns["تسلسل"].ColumnName = "rec_id";
            MyExports.output.Tables[0].Columns["الحساب"].ColumnName = "acc_id";
            MyExports.output.Tables[0].Columns["العملة الاصلية"].ColumnName = "for_cur_id";
            MyExports.output.Tables[0].Columns["المبلغ الاصلي"].ColumnName = "ofor_amount";
            MyExports.output.Tables[0].Columns["التعادل"].ColumnName = "EXCH_RATE";
            MyExports.output.Tables[0].Columns["المبلغ المحلي"].ColumnName = "oloc_amount";
            MyExports.output.Tables[0].Columns["العملة المحلية"].ColumnName = "loc_cur_id";
            MyExports.output.Tables[0].Columns["رمز العميل"].ColumnName = "cust_id";
            MyExports.output.Tables[0].Columns["رمز الفرع"].ColumnName = "t_id";
            MyExports.output.Tables[0].Columns["التاريخ"].ColumnName = "nrec_date";

            DataTable Dt = MyExports.output.Tables[0];

            connection.SQLCMD.Parameters.AddWithValue("@Ftb_Tbl_Type", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_ID", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Flag", Flag);//  افتتاح(1) ---------------------   (2) اغلاق
            connection.SQLCMD.Parameters.AddWithValue("@Start_Date",Convert.ToInt32( TxtIn_Rec_Date.Text));// تاريخ الافتتاح من terminals
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

            connection.SqlExec("Add_Ftb_Tbl", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();

         
            
        }

        private void Add_First_T_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "Check_tbl", "Date_TbL" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void Add_First_T_Balance_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        
    }
}
