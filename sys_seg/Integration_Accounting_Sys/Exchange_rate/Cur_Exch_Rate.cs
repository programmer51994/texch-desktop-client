﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;


namespace Integration_Accounting_Sys
{
    public partial class Cur_Exch_Rate : Form
    {
        string Sql_Text = "";
        string cur_text = "";
        string name = "";
         BindingSource _Bs_Cur_Exch = new BindingSource();
        public Cur_Exch_Rate()
        {
            InitializeComponent();
           
            Grdclose_price.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
        }
        //----------------------------------------------------------------------------
        private void Cur_Exch_Rate_Load(object sender, EventArgs e)
        {
            Get_cur();
            name = connection.Lang_id == 1 ? "Cur_aNAME" : "Cur_ENAME";

            Sql_Text = " SELECT  " + name + " as cur_name ,a.Cur_Id, "
                     + " Exch_Rate, Buy_Rem_Rate, Sale_Rem_Rate,User_Name,isnull((a.u_date),a.c_date) as date "
                     + " FROM  Cur_Exch_Rate a, Cur_Tbl b,USERS c "
                     + " where a.cur_id = b.cur_id "
                     + " and  a.user_id = c.user_id"
                     + " Order By " + (connection.Lang_id == 1 ? "Cur_aname" : "Cur_ename");


            _Bs_Cur_Exch.DataSource = connection.SqlExec(Sql_Text, "Exch_Rate");
            Grdclose_price.DataSource = _Bs_Cur_Exch;
        }
        //----------------------------------------------------------------------------
        private void Get_cur()
        {
            Sql_Text = " Select * from Cur_Tbl "
                     + " where cur_id not in (select cur_id from Cur_Exch_Rate)"
                     + " And cur_id <> 0 "
                     + " Order By Cur_Aname";

            cbo_cue_id.DataSource = connection.SqlExec(Sql_Text, "cur_tbl");
            cbo_cue_id.ValueMember = "cur_id";
            cbo_cue_id.DisplayMember = (connection.Lang_id) == 1 ? "Cur_ANAME" : "Cur_ENAME";

        }
        //----------------------------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            DataRow DRow = connection.SQLDS.Tables["Exch_Rate"].NewRow();
            DRow["cur_name"] = cbo_cue_id.Text.Trim();
            DRow["Exch_Rate"] = 0.000;
            DRow["Buy_Rem_Rate"] = 0.000;
            DRow["Sale_Rem_Rate"] = 0.000;
            DRow["Cur_id"] = cbo_cue_id.SelectedValue;
            DRow["date"] = DateTime.Now;
            DRow["User_Name"] = "";
            connection.SQLDS.Tables["Exch_Rate"].Rows.Add(DRow);

            foreach (DataGridViewRow DRow1 in Grdclose_price.Rows)
            {

                string Mcur_id = DRow1.Cells["Column5"].Value.ToString();
                cur_text = cur_text + Mcur_id + ',';
            }
            cur_text = cur_text.Remove(cur_text.ToString().LastIndexOf(','), 1);

            Sql_Text = " select * from Cur_Tbl "
                      + " where cur_id not in (select cur_id from Cur_Exch_Rate)"
                      + " and cur_id <> 0 "
                    + " and cur_id not in  (" + cur_text + ")";

            cbo_cue_id.DataSource = connection.SqlExec(Sql_Text, "cur_tbl1");
            cbo_cue_id.ValueMember = "cur_id";
            cbo_cue_id.DisplayMember = (connection.Lang_id) == 1 ? "Cur_ANAME" : "Cur_ENAME";
            cur_text = "";
        }
        //----------------------------------------------------------------------------
        private void Btn_OK_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow DRow in Grdclose_price.Rows)
            {

                string Mcur_id = DRow.Cells["Column5"].Value.ToString();
                cur_text = cur_text + Mcur_id + ',';
            }
            connection.SQLCMD.Parameters.AddWithValue("@Exch_Rate_Tbl", connection.SQLDS.Tables["Exch_Rate"]);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@cur_text", cur_text);
            connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Update_Exch_Rate", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Cur_Exch_Rate_Load(null, null);
        }
        //----------------------------------------------------------------------------
        private void Btn_Cancel_Click(object sender, EventArgs e)
        {

            DataTable Dt = connection.SQLDS.Tables["Exch_Rate"].DefaultView.ToTable(false, "cur_id", "Cur_name",
                                                                                             "Exch_Rate", "Buy_Rem_Rate", "Sale_Rem_Rate","Date","User_Name").Select().CopyToDataTable();
            DataTable[] _EXP_DT = { Dt };
            DataGridView[] Export_GRD = { Grdclose_price };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }
        //----------------------------------------------------------------------------
        private void Cur_Exch_Rate_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Used_Tbl = { "Exch_Rate", "cur_tbl", "cur_tbl1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
        //----------------------------------------------------------------------------
        private void Grdclose_price_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال القيمة بشـكل صحيـح" : "Please Enter The Value Correctly");
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Grdclose_price_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Cur_Exch);
            if (((DataRowView)_Bs_Cur_Exch.Current).Row["User_Name"].ToString() == "")
            {
                DelBtn.Enabled = true;
            }
            else
            {
                DelBtn.Enabled = false;
            }
        }
        //-----------------------------------
        private void DelBtn_Click(object sender, EventArgs e)
        {
            connection.SQLDS.Tables["Exch_Rate"].Rows[Grdclose_price.CurrentRow.Index].Delete();
            Get_cur();

        }
    }
}