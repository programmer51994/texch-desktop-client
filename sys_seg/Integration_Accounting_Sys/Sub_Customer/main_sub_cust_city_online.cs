﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class main_sub_cust_city_online : Form
    {

        string sql_txt = "";
        bool grd_sub_cust_change = false;
        BindingSource cust_bs = new BindingSource();
        BindingSource cit_bs = new BindingSource();
        bool txt_change = false;

        public main_sub_cust_city_online()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), txt_User, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_sub_cust.AutoGenerateColumns = false;
            grd_sub_cust_city.AutoGenerateColumns = false;

        }

        private void main_sub_cust_city_online_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "ESub_CustNmae";
                Column5.DataPropertyName = "Nat_EName";
                Column6.DataPropertyName = "Fmd_EName";
                Column8.DataPropertyName = "FRM_DOC_DA";

                Column15.DataPropertyName = "Con_EName";
                Column14.DataPropertyName = "Cit_EName";
            }

            Txt_sub_cust.Text = "";
            sql_txt = " select distinct  a.CUST_ID,b.ASub_CustName,b.ESub_CustNmae,b.PHONE,b.EMAIL,b.A_ADDRESS, "
                    + " b.FRM_DOC_ID,b.NAT_ID,b.Fmd_AName,b.Fmd_EName, "
                    + " b.FRM_DOC_NO,b.FRM_DOC_DA,b.DOC_EDA,b.FRM_DOC_IS,b.Nat_AName,b.Nat_EName "
                    + " from Sub_CUST_CITY_ONLINE a, Sub_CUSTOMERS_online b "
                    + " where a.CUST_ID = b.Sub_Cust_ID ";

            connection.SqlExec(sql_txt, "customer_sub_cust_city");
            if (connection.SQLDS.Tables["customer_sub_cust_city"].Rows.Count > 0)
            {
                cust_bs.DataSource = connection.SQLDS.Tables["customer_sub_cust_city"];
                Grd_sub_cust.DataSource = cust_bs;
                upd.Enabled = true;
                grd_sub_cust_change = true;
                Grd_sub_cust_SelectionChanged(null, null);

            }
            else
            {
                Grd_sub_cust.DataSource = null;
                upd.Enabled = false;
                
            }
            txt_change = true;

        }

        private void Grd_sub_cust_SelectionChanged(object sender, EventArgs e)
        {

            if (grd_sub_cust_change)
            {

                DataRowView DRV = cust_bs.Current as DataRowView;
                DataRow DR = DRV.Row;
                int CUST_ID = DR.Field<int>("CUST_ID");

                sql_txt = " select a.CITY_ID ,a.COUN_ID,b.Cit_AName,b.Cit_EName,c.Con_AName,c.Con_EName "
                        + " from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b,Con_Tbl c "
                        + " where a.CITY_ID = b.Cit_ID "
                        + " and b.Con_ID = c.Con_ID "
                        + " and a.CUST_ID = " + CUST_ID;
                connection.SqlExec(sql_txt, "cust_city_tbl");
                cit_bs.DataSource = connection.SQLDS.Tables["cust_city_tbl"];
                grd_sub_cust_city.DataSource = cit_bs;





                sql_txt = " select * from sub_cust_city_online"
                   + " where cust_id = " + CUST_ID;

                connection.SqlExec(sql_txt, "cust_count_tbl");
                if (connection.SQLDS.Tables["cust_count_tbl"].Rows.Count >= 0)
                {
                   // BtnAdd.Enabled = false;
                    upd.Enabled = true;
                }
                else
                {
                    //BtnAdd.Enabled = true;
                    upd.Enabled =false ;
                }


            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Sub_Cust_city_online_add_upd AddFrm = new Sub_Cust_city_online_add_upd(1,0,0);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            main_sub_cust_city_online_Load(sender, e);
            this.Visible = true;
        }

        //private void UpdBtn_Click(object sender, EventArgs e)
        //{

        //    DataRowView DRV = cust_bs.Current as DataRowView;
        //    DataRow DR = DRV.Row;
        //    int CUST_ID = DR.Field<int>("CUST_ID");


        //    DataRowView DRV1 = cit_bs.Current as DataRowView;
        //    DataRow DR1 = DRV1.Row;
        //    Int16 cit_id = DR1.Field<Int16>("CITY_ID");


        //    try
        //    {
        //        connection.SQLCS.Open();
        //        connection.SQLCMD.CommandText = "Add_Upd_Sub_Cust_City";
        //        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
        //        connection.SQLCMD.Connection = connection.SQLCS;
        //        connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_ID",CUST_ID );
        //        connection.SQLCMD.Parameters.AddWithValue("@CITY_ID", cit_id);
        //        connection.SQLCMD.Parameters.AddWithValue("@User_id", connection.user_id);
        //        connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", 3);
        //        connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
        //        connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
        //        IDataReader obj = connection.SQLCMD.ExecuteReader();
        //        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Add_Upd_Sub_Cust_City");


        //        if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
        //        {
        //            MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
        //            connection.SQLCMD.Parameters.Clear();
        //            return;
        //        }

        //        obj.Close();
        //        connection.SQLCS.Close();
        //        connection.SQLCMD.Parameters.Clear();
        //        connection.SQLCMD.Dispose();
        //    }

        //    catch (Exception _Err)
        //    {
        //        connection.SQLCMD.Parameters.Clear();
        //        MyErrorHandler.ExceptionHandler(_Err);
        //    }
        //    main_sub_cust_city_online_Load(null, null);

        //}

        private void upd_Click(object sender, EventArgs e)
        {




            DataRowView DRV = cust_bs.Current as DataRowView;
            DataRow DR = DRV.Row;
            int CUST_ID = DR.Field<int>("CUST_ID");

            DataRowView DRV1 = cit_bs.Current as DataRowView;
            DataRow DR1 = DRV1.Row;
            Int16 cit_id = DR1.Field<Int16>("CITY_ID");

            Sub_Cust_city_online_add_upd AddFrm = new Sub_Cust_city_online_add_upd(2, CUST_ID, cit_id);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            main_sub_cust_city_online_Load(sender, e);
            this.Visible = true;




        }

        private void Txt_sub_cust_TextChanged(object sender, EventArgs e)
        {

            if (txt_change)
            {
                sql_txt = " select distinct  a.CUST_ID,b.ASub_CustName,b.ESub_CustNmae,b.PHONE,b.EMAIL,b.A_ADDRESS, "
                       + " b.FRM_DOC_ID,b.NAT_ID,b.Fmd_AName,b.Fmd_EName, "
                       + " b.FRM_DOC_NO,b.FRM_DOC_DA,b.DOC_EDA,b.FRM_DOC_IS,b.Nat_AName,b.Nat_EName "
                       + " from Sub_CUST_CITY_ONLINE a, Sub_CUSTOMERS_online b "
                       + " where a.CUST_ID = b.Sub_Cust_ID "
                       + " and b.ASub_CustName like '" + Txt_sub_cust.Text + "%'";

                connection.SqlExec(sql_txt, "customer_sub_cust_city");
                if (connection.SQLDS.Tables["customer_sub_cust_city"].Rows.Count > 0)
                {
                    cust_bs.DataSource = connection.SQLDS.Tables["customer_sub_cust_city"];
                    Grd_sub_cust.DataSource = cust_bs;
                    upd.Enabled = true;
                    //grd_sub_cust_change = true;
                    //Grd_sub_cust_SelectionChanged(null, null);
                }
                else
                {
                    Grd_sub_cust.DataSource = null;
                    upd.Enabled = false;

                }

            }

        }
    }
}
