﻿namespace Integration_Accounting_Sys
{
    partial class Customer_Online_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_Cust_Name = new System.Windows.Forms.TextBox();
            this.Grd_Cust_Online = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_Currency = new System.Windows.Forms.DataGridView();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Btn_Exist = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.Chk_Cur = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CboType_sub_cust = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_Cust_Code = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbo_term = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CHK1 = new System.Windows.Forms.CheckBox();
            this.cbo_deal_nature = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.CHK2 = new System.Windows.Forms.CheckBox();
            this.grd_cur_oper = new System.Windows.Forms.DataGridView();
            this.Column30 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cur_oper_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_sell_rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_buy_rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_per_tran_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_daily_uper_limt = new System.Windows.Forms.Sample.DecimalTextBox();
            this.cbo_time_zone = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Online)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Currency)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel14.SuspendLayout();
            this.flowLayoutPanel48.SuspendLayout();
            this.flowLayoutPanel50.SuspendLayout();
            this.flowLayoutPanel52.SuspendLayout();
            this.flowLayoutPanel54.SuspendLayout();
            this.flowLayoutPanel56.SuspendLayout();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel33.SuspendLayout();
            this.flowLayoutPanel35.SuspendLayout();
            this.flowLayoutPanel37.SuspendLayout();
            this.flowLayoutPanel39.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel43.SuspendLayout();
            this.flowLayoutPanel45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cur_oper)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(257, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 905;
            this.label1.Text = "المستخـــــدم:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(710, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(110, 23);
            this.TxtIn_Rec_Date.TabIndex = 904;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(348, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(259, 23);
            this.TxtUser.TabIndex = 903;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 6);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(246, 23);
            this.TxtTerm_Name.TabIndex = 902;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(222, 43);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(35, 16);
            this.label7.TabIndex = 908;
            this.label7.Text = "الاسم:";
            // 
            // Txt_Cust_Name
            // 
            this.Txt_Cust_Name.BackColor = System.Drawing.Color.White;
            this.Txt_Cust_Name.Location = new System.Drawing.Point(277, 40);
            this.Txt_Cust_Name.MaxLength = 49;
            this.Txt_Cust_Name.Name = "Txt_Cust_Name";
            this.Txt_Cust_Name.Size = new System.Drawing.Size(163, 20);
            this.Txt_Cust_Name.TabIndex = 909;
            this.Txt_Cust_Name.TextChanged += new System.EventHandler(this.Txt_Cust_Name_TextChanged);
            // 
            // Grd_Cust_Online
            // 
            this.Grd_Cust_Online.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Online.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Online.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust_Online.ColumnHeadersHeight = 25;
            this.Grd_Cust_Online.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column13,
            this.Column3,
            this.Column11,
            this.Column12,
            this.Column4,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column20,
            this.Column5,
            this.Column21,
            this.Column10,
            this.Column23,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column22});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust_Online.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cust_Online.Location = new System.Drawing.Point(7, 92);
            this.Grd_Cust_Online.Name = "Grd_Cust_Online";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = "n3";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Online.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cust_Online.RowHeadersWidth = 15;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Online.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cust_Online.Size = new System.Drawing.Size(815, 120);
            this.Grd_Cust_Online.TabIndex = 910;
            this.Grd_Cust_Online.SelectionChanged += new System.EventHandler(this.Grd_Cust_Online_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cust_Id";
            this.Column1.HeaderText = "الرقم";
            this.Column1.Name = "Column1";
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ACust_Name";
            this.Column2.HeaderText = "الاســــــــــــــــم";
            this.Column2.Name = "Column2";
            this.Column2.Width = 250;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "A_Address";
            this.Column13.HeaderText = "العنــــوان";
            this.Column13.Name = "Column13";
            this.Column13.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Cur_ID";
            this.Column3.HeaderText = "رقم العملة";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Cit_AName";
            this.Column11.HeaderText = "المدينـــة";
            this.Column11.Name = "Column11";
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Con_AName";
            this.Column12.HeaderText = "البلــــد";
            this.Column12.Name = "Column12";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Fmd_AName";
            this.Column4.HeaderText = "نــــوع الوثيقة";
            this.Column4.Name = "Column4";
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Phone";
            this.Column14.HeaderText = "رقـــــم الهـــاتف";
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Email";
            this.Column15.HeaderText = "البريـــد الالكتروني";
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Nat_Aname";
            this.Column16.HeaderText = "الجنسيـــة";
            this.Column16.Name = "Column16";
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "Cust_Type_AName";
            this.Column20.HeaderText = "اسم الصنف";
            this.Column20.Name = "Column20";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "FMD_ID";
            this.Column5.HeaderText = "رقم الوثيقي التسلسلي";
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Gender_aname";
            this.Column21.HeaderText = "الجنـــس";
            this.Column21.Name = "Column21";
            this.Column21.Visible = false;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "birth_date";
            this.Column10.HeaderText = "تاريــخ التولــــد";
            this.Column10.Name = "Column10";
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "Occupation";
            this.Column23.HeaderText = "المهنة";
            this.Column23.Name = "Column23";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "frm_doc_no";
            this.Column6.HeaderText = "رقــــم الوثيقة";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "frm_doc_Da";
            this.Column7.HeaderText = "تاريخ الاصدار";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "doc_eda";
            this.Column8.HeaderText = "تاريخ الانتهاء";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "frm_doc_Is";
            this.Column9.HeaderText = "جهــة الاصـــدار";
            this.Column9.Name = "Column9";
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "con_id";
            this.Column17.HeaderText = "رقم البلد";
            this.Column17.Name = "Column17";
            this.Column17.Visible = false;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "Cit_Id";
            this.Column18.HeaderText = "رقم المدينة";
            this.Column18.Name = "Column18";
            this.Column18.Visible = false;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "nat_id";
            this.Column19.HeaderText = "رقم الجنسية";
            this.Column19.Name = "Column19";
            this.Column19.Visible = false;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "Gender_id";
            this.Column22.HeaderText = "رقم الجنس";
            this.Column22.Name = "Column22";
            this.Column22.Visible = false;
            // 
            // Grd_Cust_Currency
            // 
            this.Grd_Cust_Currency.AllowUserToAddRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Currency.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Currency.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Cust_Currency.ColumnHeadersHeight = 25;
            this.Grd_Cust_Currency.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column25,
            this.Column24,
            this.Column28,
            this.Column26,
            this.Column29,
            this.Column27});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust_Currency.DefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust_Currency.Location = new System.Drawing.Point(9, 230);
            this.Grd_Cust_Currency.Name = "Grd_Cust_Currency";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.Format = "N3";
            dataGridViewCellStyle10.NullValue = null;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Currency.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Cust_Currency.RowHeadersWidth = 15;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Currency.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Cust_Currency.Size = new System.Drawing.Size(816, 124);
            this.Grd_Cust_Currency.TabIndex = 911;
            this.Grd_Cust_Currency.SelectionChanged += new System.EventHandler(this.Grd_Cust_Currency_SelectionChanged);
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "Cur_ID";
            this.Column25.HeaderText = "رقم العملة";
            this.Column25.Name = "Column25";
            this.Column25.Visible = false;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "Cur_ANAME";
            this.Column24.HeaderText = "العملة";
            this.Column24.Name = "Column24";
            this.Column24.Width = 205;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "Acc_AName";
            this.Column28.HeaderText = "اســم الحساب";
            this.Column28.Name = "Column28";
            this.Column28.Width = 250;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "UPPER_LIMIT";
            dataGridViewCellStyle8.Format = "N3";
            this.Column26.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column26.HeaderText = "الحد الاعلى للتحويل";
            this.Column26.Name = "Column26";
            this.Column26.Width = 295;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "Acc_Id";
            this.Column29.HeaderText = "Acc_Id";
            this.Column29.Name = "Column29";
            this.Column29.Visible = false;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "DC_Aname";
            this.Column27.HeaderText = "طبيعة الحساب";
            this.Column27.Name = "Column27";
            this.Column27.Width = 260;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rectangleShape1.Location = new System.Drawing.Point(433, 511);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(392, 85);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape3,
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(829, 636);
            this.shapeContainer1.TabIndex = 912;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape3
            // 
            this.rectangleShape3.BackColor = System.Drawing.SystemColors.Control;
            this.rectangleShape3.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rectangleShape3.Cursor = System.Windows.Forms.Cursors.Default;
            this.rectangleShape3.Location = new System.Drawing.Point(6, 510);
            this.rectangleShape3.Name = "rectangleShape2";
            this.rectangleShape3.Size = new System.Drawing.Size(410, 87);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(12, 534);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(174, 14);
            this.label2.TabIndex = 913;
            this.label2.Text = "الحد الاعلى للتحويل اليومي :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(12, 560);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(176, 14);
            this.label3.TabIndex = 914;
            this.label3.Text = "حد المعاملــــــة الواحــــــــــدة :";
            // 
            // Btn_Exist
            // 
            this.Btn_Exist.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Exist.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Exist.Location = new System.Drawing.Point(413, 603);
            this.Btn_Exist.Name = "Btn_Exist";
            this.Btn_Exist.Size = new System.Drawing.Size(83, 29);
            this.Btn_Exist.TabIndex = 918;
            this.Btn_Exist.Text = "انهـــــاء";
            this.Btn_Exist.UseVisualStyleBackColor = true;
            this.Btn_Exist.Click += new System.EventHandler(this.button2_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(333, 603);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(81, 29);
            this.Btn_Add.TabIndex = 917;
            this.Btn_Add.Text = "موافـــق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(441, 535);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(0, 14);
            this.label4.TabIndex = 919;
            this.label4.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Maroon;
            this.label21.Location = new System.Drawing.Point(548, 480);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 13);
            this.label21.TabIndex = 922;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-4, 507);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(956, 1);
            this.flowLayoutPanel2.TabIndex = 923;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel3.TabIndex = 630;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-5, 10);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel6.TabIndex = 924;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-5, 18);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel8.TabIndex = 925;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel11.TabIndex = 924;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel12.TabIndex = 630;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(146, 462);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 925;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-14, 599);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(956, 1);
            this.flowLayoutPanel4.TabIndex = 924;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel5.TabIndex = 630;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-5, 10);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel9.TabIndex = 925;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel13.TabIndex = 630;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel14.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-5, 18);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(956, 10);
            this.flowLayoutPanel14.TabIndex = 926;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel47.TabIndex = 630;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Controls.Add(this.flowLayoutPanel49);
            this.flowLayoutPanel48.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel48.Location = new System.Drawing.Point(-5, 10);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel48.TabIndex = 925;
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel49.TabIndex = 630;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel50.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel50.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel50.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(4, 34);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(947, 10);
            this.flowLayoutPanel50.TabIndex = 927;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel51.Location = new System.Drawing.Point(-77, 3);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel51.TabIndex = 630;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel52.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(-14, 10);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel52.TabIndex = 925;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel53.TabIndex = 630;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel54.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel54.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(-14, 18);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(956, 10);
            this.flowLayoutPanel54.TabIndex = 926;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel55.TabIndex = 630;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel56.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-5, 10);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel56.TabIndex = 925;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(-68, 3);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel57.TabIndex = 630;
            // 
            // Chk_Cur
            // 
            this.Chk_Cur.AutoSize = true;
            this.Chk_Cur.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Cur.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Cur.Location = new System.Drawing.Point(15, 514);
            this.Chk_Cur.Name = "Chk_Cur";
            this.Chk_Cur.Size = new System.Drawing.Size(150, 17);
            this.Chk_Cur.TabIndex = 926;
            this.Chk_Cur.Text = "الحـــــــــدود اليوميــــة....";
            this.Chk_Cur.UseVisualStyleBackColor = true;
            this.Chk_Cur.CheckedChanged += new System.EventHandler(this.chk_daily_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(10, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 14);
            this.label8.TabIndex = 928;
            this.label8.Text = "التفاصيــــــــل......";
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel31.Location = new System.Drawing.Point(0, 33);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(828, 1);
            this.flowLayoutPanel31.TabIndex = 931;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(-194, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel32.TabIndex = 630;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(-131, 10);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel33.TabIndex = 924;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel34.TabIndex = 630;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(-131, 18);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel35.TabIndex = 925;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel36.TabIndex = 630;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel37.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel37.TabIndex = 924;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel38.TabIndex = 630;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(81, 26);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel39.TabIndex = 931;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel40.TabIndex = 630;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel41.TabIndex = 924;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel42.TabIndex = 630;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel43.TabIndex = 925;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel44.TabIndex = 630;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel45.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel45.TabIndex = 924;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel46.TabIndex = 630;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(652, 9);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.TabIndex = 936;
            this.label9.Text = "التاريـخ:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(443, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 16);
            this.label10.TabIndex = 962;
            this.label10.Text = "حــالة الثانوي :";
            // 
            // CboType_sub_cust
            // 
            this.CboType_sub_cust.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboType_sub_cust.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType_sub_cust.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboType_sub_cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboType_sub_cust.FormattingEnabled = true;
            this.CboType_sub_cust.Items.AddRange(new object[] {
            "غير فعال",
            "فعال",
            "",
            "",
            ""});
            this.CboType_sub_cust.Location = new System.Drawing.Point(527, 38);
            this.CboType_sub_cust.Name = "CboType_sub_cust";
            this.CboType_sub_cust.Size = new System.Drawing.Size(110, 24);
            this.CboType_sub_cust.TabIndex = 961;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(659, 43);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(105, 14);
            this.label11.TabIndex = 981;
            this.label11.Text = "رمــــز الثانـــــوي :";
            // 
            // txt_Cust_Code
            // 
            this.txt_Cust_Code.BackColor = System.Drawing.Color.White;
            this.txt_Cust_Code.Enabled = false;
            this.txt_Cust_Code.Location = new System.Drawing.Point(763, 40);
            this.txt_Cust_Code.MaxLength = 49;
            this.txt_Cust_Code.Name = "txt_Cust_Code";
            this.txt_Cust_Code.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_Cust_Code.Size = new System.Drawing.Size(57, 20);
            this.txt_Cust_Code.TabIndex = 980;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(10, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 16);
            this.label12.TabIndex = 986;
            this.label12.Text = "الفرع :";
            // 
            // cbo_term
            // 
            this.cbo_term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_term.FormattingEnabled = true;
            this.cbo_term.Items.AddRange(new object[] {
            "فعـــــال",
            "غير فعـــــال"});
            this.cbo_term.Location = new System.Drawing.Point(61, 38);
            this.cbo_term.Name = "cbo_term";
            this.cbo_term.Size = new System.Drawing.Size(155, 24);
            this.cbo_term.TabIndex = 985;
            this.cbo_term.SelectedIndexChanged += new System.EventHandler(this.cbo_term_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(437, 549);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(141, 14);
            this.label15.TabIndex = 989;
            this.label15.Text = "مقــــدار العمولة للبيــع :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(437, 575);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(143, 14);
            this.label16.TabIndex = 991;
            this.label16.Text = "مقـــدار العمولة للشراء :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Crimson;
            this.label13.Location = new System.Drawing.Point(753, 549);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(21, 14);
            this.label13.TabIndex = 993;
            this.label13.Text = "%";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Crimson;
            this.label14.Location = new System.Drawing.Point(753, 575);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(21, 14);
            this.label14.TabIndex = 994;
            this.label14.Text = "%";
            // 
            // CHK1
            // 
            this.CHK1.AutoSize = true;
            this.CHK1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK1.ForeColor = System.Drawing.Color.Maroon;
            this.CHK1.Location = new System.Drawing.Point(430, 511);
            this.CHK1.Name = "CHK1";
            this.CHK1.Size = new System.Drawing.Size(183, 17);
            this.CHK1.TabIndex = 995;
            this.CHK1.Text = "اعتماد اسعار الصرف الشركة...";
            this.CHK1.UseVisualStyleBackColor = true;
            this.CHK1.CheckedChanged += new System.EventHandler(this.Chk_Exch_Price_CheckedChanged);
            // 
            // cbo_deal_nature
            // 
            this.cbo_deal_nature.Cursor = System.Windows.Forms.Cursors.No;
            this.cbo_deal_nature.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_deal_nature.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_deal_nature.FormattingEnabled = true;
            this.cbo_deal_nature.Items.AddRange(new object[] {
            "حالة الحســــاب",
            "حساب ثانوي خاص",
            "حساب ثانوي عام"});
            this.cbo_deal_nature.Location = new System.Drawing.Point(551, 65);
            this.cbo_deal_nature.Name = "cbo_deal_nature";
            this.cbo_deal_nature.Size = new System.Drawing.Size(249, 24);
            this.cbo_deal_nature.TabIndex = 997;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(456, 69);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 16);
            this.label28.TabIndex = 996;
            this.label28.Text = "طبـيـعة التعامـــل:";
            // 
            // CHK2
            // 
            this.CHK2.AutoSize = true;
            this.CHK2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK2.ForeColor = System.Drawing.Color.Maroon;
            this.CHK2.Location = new System.Drawing.Point(430, 528);
            this.CHK2.Name = "CHK2";
            this.CHK2.Size = new System.Drawing.Size(177, 17);
            this.CHK2.TabIndex = 998;
            this.CHK2.Text = "اعتماد اسعار الصرف العميل...";
            this.CHK2.UseVisualStyleBackColor = true;
            this.CHK2.CheckedChanged += new System.EventHandler(this.CHK2_CheckedChanged);
            // 
            // grd_cur_oper
            // 
            this.grd_cur_oper.AllowUserToAddRows = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grd_cur_oper.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_cur_oper.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.grd_cur_oper.ColumnHeadersHeight = 25;
            this.grd_cur_oper.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column30,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.CUST_ID,
            this.CUR_ID,
            this.cur_oper_id});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.NullValue = null;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_cur_oper.DefaultCellStyle = dataGridViewCellStyle14;
            this.grd_cur_oper.Location = new System.Drawing.Point(4, 377);
            this.grd_cur_oper.Name = "grd_cur_oper";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.Format = "N3";
            dataGridViewCellStyle15.NullValue = null;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_cur_oper.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.grd_cur_oper.RowHeadersWidth = 15;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grd_cur_oper.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.grd_cur_oper.Size = new System.Drawing.Size(816, 124);
            this.grd_cur_oper.TabIndex = 999;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "cur_oper_chk";
            this.Column30.FalseValue = "0";
            this.Column30.HeaderText = "التاشير";
            this.Column30.Name = "Column30";
            this.Column30.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column30.TrueValue = "1";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ACUST_NAME";
            this.dataGridViewTextBoxColumn1.HeaderText = "العميل";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Cur_ANAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "العملة";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 205;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "cur_oper_aname";
            this.dataGridViewTextBoxColumn3.HeaderText = "نوع العملية";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 250;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // CUR_ID
            // 
            this.CUR_ID.DataPropertyName = "CUR_ID";
            this.CUR_ID.HeaderText = "CUR_ID";
            this.CUR_ID.Name = "CUR_ID";
            this.CUR_ID.ReadOnly = true;
            this.CUR_ID.Visible = false;
            // 
            // cur_oper_id
            // 
            this.cur_oper_id.DataPropertyName = "cur_oper_id";
            this.cur_oper_id.HeaderText = "cur_oper_id";
            this.cur_oper_id.Name = "cur_oper_id";
            this.cur_oper_id.ReadOnly = true;
            this.cur_oper_id.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(12, 359);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 14);
            this.label5.TabIndex = 1000;
            this.label5.Text = "عــــمــليات العــمـلات.....";
            // 
            // Txt_sell_rate
            // 
            this.Txt_sell_rate.BackColor = System.Drawing.Color.White;
            this.Txt_sell_rate.Enabled = false;
            this.Txt_sell_rate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sell_rate.Location = new System.Drawing.Point(584, 545);
            this.Txt_sell_rate.Name = "Txt_sell_rate";
            this.Txt_sell_rate.NumberDecimalDigits = 7;
            this.Txt_sell_rate.NumberDecimalSeparator = ".";
            this.Txt_sell_rate.NumberGroupSeparator = ",";
            this.Txt_sell_rate.Size = new System.Drawing.Size(163, 23);
            this.Txt_sell_rate.TabIndex = 992;
            this.Txt_sell_rate.Text = "0.0000000";
            // 
            // Txt_buy_rate
            // 
            this.Txt_buy_rate.BackColor = System.Drawing.Color.White;
            this.Txt_buy_rate.Enabled = false;
            this.Txt_buy_rate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_buy_rate.Location = new System.Drawing.Point(584, 571);
            this.Txt_buy_rate.Name = "Txt_buy_rate";
            this.Txt_buy_rate.NumberDecimalDigits = 7;
            this.Txt_buy_rate.NumberDecimalSeparator = ".";
            this.Txt_buy_rate.NumberGroupSeparator = ",";
            this.Txt_buy_rate.Size = new System.Drawing.Size(163, 23);
            this.Txt_buy_rate.TabIndex = 990;
            this.Txt_buy_rate.Text = "0.0000000";
            // 
            // txt_per_tran_rem
            // 
            this.txt_per_tran_rem.BackColor = System.Drawing.Color.White;
            this.txt_per_tran_rem.Enabled = false;
            this.txt_per_tran_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_per_tran_rem.Location = new System.Drawing.Point(200, 557);
            this.txt_per_tran_rem.Name = "txt_per_tran_rem";
            this.txt_per_tran_rem.NumberDecimalDigits = 3;
            this.txt_per_tran_rem.NumberDecimalSeparator = ".";
            this.txt_per_tran_rem.NumberGroupSeparator = ",";
            this.txt_per_tran_rem.Size = new System.Drawing.Size(163, 23);
            this.txt_per_tran_rem.TabIndex = 933;
            this.txt_per_tran_rem.Text = "0.000";
            // 
            // txt_daily_uper_limt
            // 
            this.txt_daily_uper_limt.BackColor = System.Drawing.Color.White;
            this.txt_daily_uper_limt.Enabled = false;
            this.txt_daily_uper_limt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_daily_uper_limt.Location = new System.Drawing.Point(200, 530);
            this.txt_daily_uper_limt.Name = "txt_daily_uper_limt";
            this.txt_daily_uper_limt.NumberDecimalDigits = 3;
            this.txt_daily_uper_limt.NumberDecimalSeparator = ".";
            this.txt_daily_uper_limt.NumberGroupSeparator = ",";
            this.txt_daily_uper_limt.Size = new System.Drawing.Size(163, 23);
            this.txt_daily_uper_limt.TabIndex = 932;
            this.txt_daily_uper_limt.Text = "0.000";
            // 
            // cbo_time_zone
            // 
            this.cbo_time_zone.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbo_time_zone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_time_zone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_time_zone.FormattingEnabled = true;
            this.cbo_time_zone.Location = new System.Drawing.Point(80, 65);
            this.cbo_time_zone.Name = "cbo_time_zone";
            this.cbo_time_zone.Size = new System.Drawing.Size(360, 24);
            this.cbo_time_zone.TabIndex = 1002;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(12, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 16);
            this.label17.TabIndex = 1001;
            this.label17.Text = "نظام الوقت:";
            // 
            // Customer_Online_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 636);
            this.Controls.Add(this.cbo_time_zone);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grd_cur_oper);
            this.Controls.Add(this.CHK2);
            this.Controls.Add(this.cbo_deal_nature);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.CHK1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_sell_rate);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_buy_rate);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbo_term);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_Cust_Code);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.CboType_sub_cust);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_per_tran_rem);
            this.Controls.Add(this.txt_daily_uper_limt);
            this.Controls.Add(this.flowLayoutPanel31);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Chk_Cur);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Btn_Exist);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Grd_Cust_Currency);
            this.Controls.Add(this.Grd_Cust_Online);
            this.Controls.Add(this.Txt_Cust_Name);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.shapeContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customer_Online_Add";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "516";
            this.Text = "Customer_Online_Add_Upd";
            this.Load += new System.EventHandler(this.Customer_Online_Add_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Online)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Currency)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel14.ResumeLayout(false);
            this.flowLayoutPanel48.ResumeLayout(false);
            this.flowLayoutPanel50.ResumeLayout(false);
            this.flowLayoutPanel52.ResumeLayout(false);
            this.flowLayoutPanel54.ResumeLayout(false);
            this.flowLayoutPanel56.ResumeLayout(false);
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel33.ResumeLayout(false);
            this.flowLayoutPanel35.ResumeLayout(false);
            this.flowLayoutPanel37.ResumeLayout(false);
            this.flowLayoutPanel39.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel43.ResumeLayout(false);
            this.flowLayoutPanel45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_cur_oper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_Cust_Name;
        private System.Windows.Forms.DataGridView Grd_Cust_Online;
        private System.Windows.Forms.DataGridView Grd_Cust_Currency;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Btn_Exist;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.CheckBox Chk_Cur;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.Sample.DecimalTextBox txt_daily_uper_limt;
        private System.Windows.Forms.Sample.DecimalTextBox txt_per_tran_rem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CboType_sub_cust;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_Cust_Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbo_term;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_buy_rate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_sell_rate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox CHK1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
        private System.Windows.Forms.ComboBox cbo_deal_nature;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox CHK2;
        private System.Windows.Forms.DataGridView grd_cur_oper;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cur_oper_id;
        private System.Windows.Forms.ComboBox cbo_time_zone;
        private System.Windows.Forms.Label label17;
    }
}