﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Main_Customer_Online : Form
    {
        string sql_txt = "";
        string sql_text1 = "";
        BindingSource Cust_Main_Onilne_bs = new BindingSource();
        BindingSource Cust_Main_Onilne_Curency_bs = new BindingSource();
        BindingSource finitial_Cust_Main_Onilne_Curency_bs = new BindingSource();
        bool Cust_Main_Onilne_change = false;
        //bool Grd_Cust_Main_Onilne_Curency_change = false;
        int Sub_cust_id = 0;
        Int16 Cur_ID = 0;
        int sub_cust_id = 0;
        string Sql_cust_typ_Text = "";
        public static decimal UP_limit = 0;
        string cust_typ_string = "";
        //  int Rec_cust_id = 0;
        public Main_Customer_Online()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), txt_User, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_Cust_Main_Onilne.AutoGenerateColumns = false;
            Grd_Cust_Main_Onilne_Curency.AutoGenerateColumns = false;
            grd_finitial_oper.AutoGenerateColumns = false;
            Grd_Cust_Main_Onilne.ReadOnly = true;
            Grd_Cust_Main_Onilne_Curency.ReadOnly = true;

            #region Enghlish
            if (connection.Lang_id != 1) //
            {
                Grd_Cust_Main_Onilne.Columns["Column4"].DataPropertyName = "ESub_CustNmae";
                Grd_Cust_Main_Onilne.Columns["Column6"].DataPropertyName = "City_Ename";
                Grd_Cust_Main_Onilne.Columns["Column8"].DataPropertyName = "Con_EName";
                Grd_Cust_Main_Onilne.Columns["Column13"].DataPropertyName = "Fmd_EName";
                Grd_Cust_Main_Onilne.Columns["Column19"].DataPropertyName = "Nat_EName";
                Grd_Cust_Main_Onilne.Columns["Column35"].DataPropertyName = "cust_typ_Ename";

                Grd_Cust_Main_Onilne_Curency.Columns["Column24"].DataPropertyName = "Cur_ENAME";
                Grd_Cust_Main_Onilne_Curency.Columns["Column33"].DataPropertyName = "Acc_EName";
                Grd_Cust_Main_Onilne_Curency.Columns["Column26"].DataPropertyName = "Cust_state_ename";
                Grd_Cust_Main_Onilne_Curency.Columns["Column20"].DataPropertyName = "ver_check_ename";
                Grd_Cust_Main_Onilne_Curency.Columns["Column29"].DataPropertyName = "CHK_exch_agent_Ename";
                Column31.DataPropertyName = "eCUST_NAMe";

            }
            #endregion

        }

        private void Main_Customer_Online_Load(object sender, EventArgs e)
        {
            Sql_cust_typ_Text = "SELECT cust_typ_id,cust_typ_Aname,cust_typ_Ename FROM cust_type";
            cbo_deal_nature.DataSource = connection.SqlExec(Sql_cust_typ_Text, "Type_cust_typ");
            cbo_deal_nature.ValueMember = "cust_typ_id";
            cbo_deal_nature.DisplayMember = connection.Lang_id == 1 ? "cust_typ_Aname" : "cust_typ_Ename";
             
            Cust_Main_Onilne_change = false;
            Txt_CUST_1.Text = "";

            Grd_Cust_Main_Onilne_Fill();

        }
        //---------------
        private void Grd_Cust_Main_Onilne_Fill()   //cust_typ_id
        {

            if(Convert.ToInt16(cbo_deal_nature.SelectedValue)==0 )
            {
               cust_typ_string  = "";
            
            }

            else 
            {

                cust_typ_string = "and a.cust_typ_id =  " + Convert.ToInt16(cbo_deal_nature.SelectedValue);
            }

            
            Cust_Main_Onilne_change = false;
            sql_txt = " select A.*, case Cust_state when 0 then 'غير فعال' when 1 then 'فعال' end as Cust_state_name,case Cust_state when 0 then 'Not Active' when 1 then 'Active' end as Cust_state_ename,case a.ver_check  when 0 then 'Does not have confirm transfers' when 1 then 'have confirm transfers' end as ver_check_ename,case a.ver_check  when 0 then 'لايمتلك خاصية تأكيد حوالات' when 1 then 'يمتلك خاصية تأكيد حوالات' end as ver_check_name , b.ACUST_NAMe,b.eCUST_NAMe , d.cust_typ_Aname , d.cust_typ_Ename, case CHK_exch_agent when 0 then 'لا يملك  خاصية اسعار صرف العميل' when 1 then ' يملك  خاصية اسعار صرف العميل' end as CHK_exch_agent_Aname, case CHK_exch_agent when 0 then 'Dose Have Agent exchange price' when 1 then 'Dose not Have Agent exchange price' end as CHK_exch_agent_Ename"
             + " from Sub_CUSTOMERS_Online a, CUSTOMERS b, TERMINALS c , cust_type d"
                    + " where b.CUST_ID=c.CUST_ID "
                    + " and a.T_id=c.T_ID "
                    + " and a.cust_typ_id=d.cust_typ_id "
                    + " and ASub_CustName like '" + Txt_CUST_1.Text.Trim() + "%' "
                    + cust_typ_string
                    + " order by a.T_id , c_date desc";
            connection.SqlExec(sql_txt, "Sub_CUSTOMERS_online_Tbl");
            if (connection.SQLDS.Tables["Sub_CUSTOMERS_online_Tbl"].Rows.Count > 0)
            {
                DataTable DT_Main_Onilne_Cust = new DataTable();
                DT_Main_Onilne_Cust = connection.SQLDS.Tables["Sub_CUSTOMERS_online_Tbl"].DefaultView.ToTable(true, "City_Aname", "City_Ename", "Con_AName", "Con_EName", "A_Address", "Fmd_AName", "Fmd_EName", "Phone", "Email", "ASub_CustName", "ESub_CustNmae", "Nat_Aname", "Nat_Ename", "frm_doc_no", "frm_doc_Da", "doc_eda", "frm_doc_Is", "Sub_Cust_ID", "FRM_DOC_ID", "con_id", "City_Id", "nat_id", "Cust_Online_Main_Id", "Cust_Code", "ACUST_NAMe", "eCUST_NAMe", "T_id", "cust_typ_Aname", "cust_typ_Ename", "CHK_exch_agent_Aname", "CHK_exch_agent_Ename", "SubCustTimeZone").Select().CopyToDataTable();
                Cust_Main_Onilne_bs.DataSource = DT_Main_Onilne_Cust;
                Grd_Cust_Main_Onilne.DataSource = Cust_Main_Onilne_bs;
                Cust_Main_Onilne_change = true;
                Grd_Cust_Main_Onilne_SelectionChanged(null, null);

            }
            else
            {
                Grd_Cust_Main_Onilne.DataSource = new BindingSource();
                Grd_Cust_Main_Onilne_Curency.DataSource = new BindingSource();
                MessageBox.Show(connection.Lang_id == 2 ? "There is no data" : "لاتوجد بيانات تحقق الشروط", MyGeneral_Lib.LblCap);
                return;

            }
        }

        private void Grd_Cust_Main_Onilne_SelectionChanged(object sender, EventArgs e)
        {
            if (Cust_Main_Onilne_change)
            {
            sub_cust_id = Convert.ToInt32(((DataRowView)Cust_Main_Onilne_bs.Current).Row["Sub_Cust_ID"]);
            Int16 T_id = Convert.ToInt16(((DataRowView)Cust_Main_Onilne_bs.Current).Row["T_ID"]);
            if (Cust_Main_Onilne_change)
            {

                try
                {

                    Cust_Main_Onilne_Curency_bs.DataSource = connection.SQLDS.Tables["Sub_CUSTOMERS_online_Tbl"].Select("Sub_Cust_ID = " + sub_cust_id + "and t_id = " + T_id).CopyToDataTable();
                    Grd_Cust_Main_Onilne_Curency.DataSource = Cust_Main_Onilne_Curency_bs;
                    //Grd_Cust_Main_Onilne_Curency_change = true;


                }
                catch
                {
                    Cust_Main_Onilne_Curency_bs.DataSource = null;  //تفريغ الجدول
                }

            }
            }


        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Customer_Online_Add AddFrm = new Customer_Online_Add(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Main_Customer_Online_Load(sender, e);
            this.Visible = true;
        }

        private void UpdBtn_Click(object sender, EventArgs e)
        {

            DataRowView DRV = Cust_Main_Onilne_bs.Current as DataRowView;
            DataRow DR = DRV.Row;
            Sub_cust_id = DR.Field<int>("Sub_Cust_ID");
            Int16 t_id = DR.Field< Int16>("T_ID");
            string acust_name = DR.Field<string>("ACUST_NAMe");
            string ecust_name = DR.Field<string>("eCUST_NAMe");
            // Rec_cust_id = DR.Field<int>("Rec_cust_id");


            DataRowView DRV1 = Cust_Main_Onilne_Curency_bs.Current as DataRowView;
            DataRow DR1 = DRV1.Row;
            Cur_ID = DR1.Field<Int16>("Cur_Id");



            Customer_Online_upd AddFrm = new Customer_Online_upd(2, Sub_cust_id, Cur_ID, UP_limit, t_id, acust_name, ecust_name);//Rec_cust_id
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Main_Customer_Online_Load(sender, e);
            this.Visible = true;


        }

        private void Grd_Cust_Main_Onilne_Curency_SelectionChanged(object sender, EventArgs e)
        {
           
            try
            {
                Int16 cur_id = Convert.ToInt16(((DataRowView)Cust_Main_Onilne_Curency_bs.Current).Row["CUR_ID"]);
                string sql_text = " select UPPER_LIMIT from CUSTOMERS_ACCOUNTS where CUST_ID = " + sub_cust_id
                                + " and cur_id = " + cur_id;
                connection.SqlExec(sql_text, "Up_Limt_Tbl");
                UP_limit = Convert.ToDecimal(connection.SQLDS.Tables["Up_Limt_Tbl"].Rows[0]["UPPER_LIMIT"]);


                string sql_text1 = " select  distinct CUST_ID,	a.cur_oper_id,	a.CUR_ID	, b.cur_oper_aname,b.cur_oper_ename, c.Cur_ANAME,c.Cur_ENAME       from Sub_CUST_Financial_cur_oper a, Financial_cur_oper b,Cur_Tbl c where  a.cur_oper_id = b.cur_oper_id  and a.CUR_ID=c.Cur_ID  " 
                                + " and a.cur_id = " + cur_id
                                 + " and CUST_ID = " + sub_cust_id;
                connection.SqlExec(sql_text1, "cur_oper1_Tbl");
                try
                {
                    DataTable DT_Main_Onilne_Cust1 = new DataTable();
                    DT_Main_Onilne_Cust1 = connection.SQLDS.Tables["cur_oper1_Tbl"].DefaultView.ToTable().Select().CopyToDataTable();
                    finitial_Cust_Main_Onilne_Curency_bs.DataSource = DT_Main_Onilne_Cust1;
                    grd_finitial_oper.DataSource = finitial_Cust_Main_Onilne_Curency_bs;
                }
                catch {grd_finitial_oper.DataSource= new BindingSource();};
            }
            catch {  };
           
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Grd_Cust_Main_Onilne_Fill();
        }

        private void Txt_CUST_1_TextChanged(object sender, EventArgs e)
        {
            if (Txt_CUST_1.Text == "")
            {
                Grd_Cust_Main_Onilne_Fill();
            }
        }

        
    }
}