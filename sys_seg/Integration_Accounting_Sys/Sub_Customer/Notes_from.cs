﻿using System;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{

    public partial class Notes_from : Form
    {
        public static string Notes_BL = "";
        public Notes_from()
        {
            InitializeComponent();
           
            MyGeneral_Lib.Form_Orientation(this);
           
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (Txt_Notes.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يرجى كتابة ملاحظات" : " Please write Notes", MyGeneral_Lib.LblCap);
                return;
            
            }
            Notes_BL = Txt_Notes.Text.Trim();
            this.Close();
        }
        
    }
}