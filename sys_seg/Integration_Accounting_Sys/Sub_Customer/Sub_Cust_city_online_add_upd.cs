﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Sub_Cust_city_online_add_upd : Form
    {

        string sql_txt = "";
        bool cust_change = false;
        bool Grd_subcust_change = false;
        int form_id = 0;
        int Sub_cust_id = 0;
        Int16 city_id = 0;
        BindingSource scut_Bs = new BindingSource();
        BindingSource city_Bs = new BindingSource();
        DataTable dt_city_sub_cust = new DataTable();


        public Sub_Cust_city_online_add_upd(int frm_id,int sub_cust_id,Int16 cit_id)
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), txt_User, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_subcust.AutoGenerateColumns = false;
            Grd_sub_city.AutoGenerateColumns = false;
            Sub_cust_id = sub_cust_id;
            city_id = cit_id;
            form_id = frm_id;
            Grd_subcust.ReadOnly = true;
            if (connection.Lang_id == 2)
            {
                Column10.DataPropertyName = "ESub_CustNmae";
                Column2.DataPropertyName = "ECity_Name";
            }

        }

   

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Sub_Cust_city_online_add_upd_Load(object sender, EventArgs e)
        {



            cust_change = true;
            Txt_sub_cust_TextChanged(null, null);



            if (form_id == 2)
            {

                Grd_sub_city.Columns["Column2"].ReadOnly = true;
                Txt_sub_cust.Enabled = false;
                edit_record();

            }



        }

        private void edit_record()
        {



            sql_txt = " select distinct Sub_Cust_ID,ASub_CustName,ESub_CustNmae "
                    + " from Sub_CUSTOMERS_Online "
                    + " where Cust_state = 1 "
                    + " and Sub_Cust_ID = " + Sub_cust_id;


            connection.SqlExec(sql_txt, "Sub_CUSTOMERS_online_Tbl");

            scut_Bs.DataSource = connection.SQLDS.Tables["Sub_CUSTOMERS_online_Tbl"];
            Grd_subcust.DataSource = scut_Bs;
            if (connection.SQLDS.Tables["Sub_CUSTOMERS_online_Tbl"].Rows.Count > 0)
            {
                Grd_subcust.DataSource = connection.SQLDS.Tables["Sub_CUSTOMERS_online_Tbl"];

            }
            else
            {
                Grd_subcust.DataSource = null;
            }


            sql_txt = " SELECT 0 as chk,a.Cit_AName ,Cit_eName  ,a.Cit_ID,b.Con_ID,Con_AName,"
                        + " Con_eName, Cit_AName + ' ـ ' + Con_AName + '  ('+ Con_Code + ')' as ACity_Name, "
                        + " Cit_eName + ' - ' + Con_eName + '  ('+ Con_Code + ')'as ECity_Name, "
                        + " Fav_Cit_id "
                        + " FROM  Cit_Tbl a ,Con_Tbl b "
                        + " where a.Con_ID = b.Con_ID "
                        + " and a.Cit_ID in (select CITY_ID from CUST_CITY_ONLINE) "
                        + " and a.Cit_ID not in (select CITY_ID from Sub_CUST_CITY_ONLINE where CUST_ID = " + Sub_cust_id
                        + " )"
                        + " union "
                        + " SELECT 1 as chk,a.Cit_AName ,Cit_eName  ,a.Cit_ID,b.Con_ID,Con_AName,"
                        + " Con_eName, Cit_AName + ' ـ ' + Con_AName + '  ('+ Con_Code + ')' as ACity_Name, "
                        + " Cit_eName + ' - ' + Con_eName + '  ('+ Con_Code + ')'as ECity_Name, "
                        + " Fav_Cit_id "
                        + " FROM  Cit_Tbl a ,Con_Tbl b "
                        + " where a.Con_ID = b.Con_ID "
                        + " and a.Cit_ID in (select CITY_ID from Sub_CUST_CITY_ONLINE where CUST_ID = " + Sub_cust_id
                        + " )"
                        + " order by chk desc ";
            connection.SqlExec(sql_txt, "cities_tbl");
            if (connection.SQLDS.Tables["cities_tbl"].Rows.Count > 0)
            {
                Grd_sub_city.DataSource = connection.SQLDS.Tables["cities_tbl"];
            }
            else
            {
                Grd_sub_city.DataSource = null;
            }

         
        }


        

        private void button3_Click(object sender, EventArgs e)
        {

            int  sub_cust_id = Convert.ToInt32(((DataRowView)scut_Bs.Current).Row["Sub_Cust_ID"]);

           
            #region Validations

            int Rec_No = connection.SQLDS.Tables["cities_tbl"].Select("Chk > 0").Length;
            if (Rec_No <= 0 && (form_id == 1 ||form_id == 2))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم تحدد مدينة الوكيل الفرعي  " : "There is no selection of the sub customer city", MyGeneral_Lib.LblCap);
                Grd_sub_city.Focus();
                return;
            }
            #endregion


            dt_city_sub_cust = connection.SQLDS.Tables["cities_tbl"].DefaultView.ToTable(false, "Chk", "Cit_ID", "Con_ID").Select("CHK > 0").CopyToDataTable();



          
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Add_Upd_Sub_Cust_City";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_ID", sub_cust_id);
                connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_City_Tbl", dt_city_sub_cust);
                connection.SQLCMD.Parameters.AddWithValue("@User_id",connection.user_id );
                connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", form_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Add_Upd_Sub_Cust_City");


                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }

            this.Close();

        }

       

        private void Txt_sub_cust_TextChanged(object sender, EventArgs e)
        {
            if (cust_change)
            {

                sql_txt = " select distinct Sub_Cust_ID,ASub_CustName,ESub_CustNmae "
                        + " from Sub_CUSTOMERS_Online "
                        + " where Cust_state = 1 "
                        + " and ASub_CustName like '" + Txt_sub_cust.Text + "%'"
                        + " and Sub_Cust_ID not in (select cust_id from Sub_CUST_CITY_ONLINE "
                        + ") ";
                connection.SqlExec(sql_txt, "subcust_tbl");

                if (connection.SQLDS.Tables["subcust_tbl"].Rows.Count > 0)
                {
                    scut_Bs.DataSource = connection.SQLDS.Tables["subcust_tbl"];
                    Grd_subcust.DataSource = scut_Bs;
                    Grd_subcust_change = true;
                    Grd_subcust_SelectionChanged(null, null);

                }

                else
                {
                  //  MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات تحقق الشروط" : "There is no data ", MyGeneral_Lib.LblCap);
                    Grd_subcust.DataSource = null;
                }

            }
        }

        private void Grd_subcust_SelectionChanged(object sender, EventArgs e)
        {

            DataRowView DRV1 = scut_Bs.Current as DataRowView;
            DataRow DR1 = DRV1.Row;
            int Sub_Cust_ID = DR1.Field<int>("Sub_Cust_ID");


            sql_txt = " SELECT  0 as Chk,a.Cit_AName ,Cit_eName  ,a.Cit_ID,b.Con_ID,Con_AName,"
                       + " Con_eName, Cit_AName + ' ـ ' + Con_AName + '  ('+ Con_Code + ')' as ACity_Name, "
                       + " Cit_eName + ' - ' + Con_eName + '  ('+ Con_Code + ')'as ECity_Name, "
                       + " Fav_Cit_id "
                       + " FROM  Cit_Tbl a ,Con_Tbl b "
                       + " where a.Con_ID = b.Con_ID "
                       + " and a.Cit_ID in (select CITY_ID from CUST_CITY_ONLINE) "
                       + " Order by Chk desc ";
            connection.SqlExec(sql_txt, "cities_tbl");


            if (connection.SQLDS.Tables["cities_tbl"].Rows.Count > 0)
            {
                city_Bs.DataSource = connection.SQLDS.Tables["cities_tbl"];
                Grd_sub_city.DataSource = city_Bs;
                
            }

            else
            {
                Grd_sub_city.DataSource = null;
            }


        }

    }
}
