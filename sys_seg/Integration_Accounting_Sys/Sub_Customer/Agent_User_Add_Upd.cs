﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Agent_User_Add_Upd : Form
    {

        string sql_txt = "";
        BindingSource SubCust_Onilne_bs = new BindingSource();
        BindingSource Sub_Cust_User_bs = new BindingSource();
        //bool Grd_Agent_Sub_Cust_change = false;
        DataTable Users_Agent_Tbl = new DataTable();
        DataTable DT = new DataTable();
        DataTable DT_Onilne_Cust = new DataTable();
        int Form_Id = 0;
        int SSub_Cust_id = 0;
        Int16 T_id_bran = 0;
        bool chan = false;
        DataTable Dt_user = new DataTable();
        DataTable DT1 = new DataTable();
        Boolean change = false;
        Boolean cbo_change = false;
        BindingSource binding_Cbo_CityUser_Id = new BindingSource();


        public Agent_User_Add_Upd(int frm_id, int Sub_Cust_Id, Int16 T_ID)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Agent_Sub_Cust.AutoGenerateColumns = false;
            Grd_Login_Cust_Online.AutoGenerateColumns = false;
            Form_Id = frm_id;
            SSub_Cust_id = Sub_Cust_Id;
            T_id_bran = T_ID ;
            
            if (connection.Lang_id != 1)
            {
                Column4.DataPropertyName = "eSub_CustNmae";
                Column6.DataPropertyName = "City_ename";
                Column8.DataPropertyName = "Con_eName";
                Column19.DataPropertyName = "Nat_eName";
                Column13.DataPropertyName = "Fmd_eName";
            }
        }

        private void Agent_User_Add_Upd_Load(object sender, EventArgs e)
        {
            if (Form_Id == 1)
            {
                //label5.Text = " بلد ومدينة الارســال :";
                //label6.Visible = false;
                //Cbo_upd_CityUser_Id.Visible = false;
                chan = false;

                connection.SqlExec("Exec agnet_user_search ", "agnet_user_search_Tbl");


                Dt_user = connection.SQLDS.Tables["agnet_user_search_Tbl2"];
                Sub_Cust_User_bs.DataSource = Dt_user;


                Grd_Login_Cust_Online.DataSource = Sub_Cust_User_bs;

                cbo_term.DataSource = connection.SQLDS.Tables["agnet_user_search_Tbl"];
                cbo_term.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "Ecust_name";
                cbo_term.ValueMember = "T_id";

                chan = true;
                cbo_term_SelectedIndexChanged(null, null);



                if (Grd_Agent_Sub_Cust.Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد وكالات لاضافة المستخدمين  " : "There is no sub customers to add users", MyGeneral_Lib.LblCap);
                    this.Close();
                }




                //  int User_Sub_Cust_id = Convert.ToInt32(((DataRowView)SubCust_Onilne_bs.Current).Row["Sub_Cust_Id"]);


                sql_txt = " select a.CITY_ID,a.COUN_ID,b.Cit_AName,b.Cit_EName from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b "
                        + "  where a.CITY_ID = b.Cit_ID "
                        + "  and cust_id = " + 0
                        + "  union "
                        + " select 0 as  city_id,0 as COUN_ID,'اختر' as Cit_AName ,'Select' as Cit_EName  from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b "
                        + " where a.CITY_ID = b.Cit_ID "
                        + " and cust_id = " + 0;

                connection.SqlExec(sql_txt, "Sub_CUST_CITY_ONLINE_Tbl");


                Cbo_CityUser_Id.DataSource = connection.SQLDS.Tables["Sub_CUST_CITY_ONLINE_Tbl"];
                Cbo_CityUser_Id.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                Cbo_CityUser_Id.ValueMember = "CITY_ID";

                change = true;
                Grd_Agent_Sub_Cust_SelectionChanged(null, null);


            }
            else
            {

                if (Form_Id == 2)
                {
                    //label5.Text = "بحث بلد ومدينة الارســال  :";
                    //label6.Visible = true;
                    //Cbo_upd_CityUser_Id.Visible = true;
                    sql_txt = " select a.CITY_ID,a.COUN_ID,b.Cit_AName,b.Cit_EName from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b "
                             + "  where a.CITY_ID = b.Cit_ID "
                             + "  and cust_id = " + SSub_Cust_id
                             + "  union "
                             + " select 0 as  city_id,0 as COUN_ID,'اختر' as Cit_AName ,'Select' as Cit_EName  from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b "
                             + " where a.CITY_ID = b.Cit_ID "
                             + " and cust_id = " + SSub_Cust_id;


                    connection.SqlExec(sql_txt, "Sub_CUST_CITY_ONLINE_Tbl");

                    binding_Cbo_CityUser_Id.DataSource = connection.SQLDS.Tables["Sub_CUST_CITY_ONLINE_Tbl"];
                    Cbo_CityUser_Id.DataSource = binding_Cbo_CityUser_Id;
                    Cbo_CityUser_Id.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                    Cbo_CityUser_Id.ValueMember = "CITY_ID";


                    //connection.SqlExec(sql_txt, "Sub_CUST_CITY_ONLINE_upd_Tbl");

                    //Cbo_upd_CityUser_Id.DataSource = connection.SQLDS.Tables["Sub_CUST_CITY_ONLINE_upd_Tbl"];
                    //Cbo_upd_CityUser_Id.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                    //Cbo_upd_CityUser_Id.ValueMember = "CITY_ID";


                    Grd_Login_Cust_Online.Columns["Column21"].ReadOnly = true;
                    Grd_Login_Cust_Online.Columns["Column23"].ReadOnly = true;
                    Grd_Login_Cust_Online.Columns["Column25"].ReadOnly = true;
                    Grd_Login_Cust_Online.Columns["Column26"].ReadOnly = true;

                    cbo_change = true;
                    Cbo_CityUser_Id_SelectedValueChanged(null, null);

                    // edit_record();
                }

                edit_record();
            }

        }
        //--------------------------
        private void cbo_term_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chan)
            {
                
                txt_Serch_sub_cust.Text = "";
                Txt_Sub_Cust_User.Text = "";
                Grd_Agent_Sub_Fill();
                Cbo_CityUser_Id_SelectedValueChanged(null, null);
                //Grd_Login_Cust_Online.DataSource = new BindingSource();
            }

        }
        //--------------------------
        private void Grd_Agent_Sub_Fill()
        {


            string condstr = "";
            if (txt_Serch_sub_cust.Text.ToString() != "")
            {
                
                condstr = "(ASub_CustName like '%" + txt_Serch_sub_cust.Text + "%' or ESub_CustNmae like '%" + txt_Serch_sub_cust.Text + "%' ) and t_id = " + cbo_term.SelectedValue;
            }
            else
            {
                condstr = " t_id = " + cbo_term.SelectedValue;
            }
            if (connection.SQLDS.Tables["agnet_user_search_Tbl1"].Rows.Count > 0)
            {
                try
                {
                    DataTable DT_Onilne_Cust = new DataTable();

                    DT_Onilne_Cust = connection.SQLDS.Tables["agnet_user_search_Tbl1"].DefaultView.ToTable(true, "City_Aname", "City_Ename", "Con_AName", "Con_EName", "A_Address", "Fmd_AName", "Fmd_EName", "Phone", "Email", "ASub_CustName", "ESub_CustNmae", "Nat_Aname", "Nat_Ename", "frm_doc_no", "frm_doc_Da", "doc_eda", "frm_doc_Is", "Sub_Cust_ID", "FRM_DOC_ID", "con_id", "City_Id", "nat_id", "Cust_Online_Main_Id", "Cust_state", "Cust_Code", "T_id").Select(condstr).CopyToDataTable();
                    DT_Onilne_Cust = DT_Onilne_Cust.DefaultView.ToTable(true, "City_Aname", "City_Ename", "Con_AName", "Con_EName", "A_Address", "Fmd_AName", "Fmd_EName", "Phone", "Email", "ASub_CustName", "ESub_CustNmae", "Nat_Aname", "Nat_Ename", "frm_doc_no", "frm_doc_Da", "doc_eda", "frm_doc_Is", "Sub_Cust_ID", "FRM_DOC_ID", "con_id", "City_Id", "nat_id", "Cust_state", "Cust_Code").Select().CopyToDataTable();
                    if (DT_Onilne_Cust.Rows.Count > 0)
                    {
                        SubCust_Onilne_bs.DataSource = DT_Onilne_Cust;
                        Grd_Agent_Sub_Cust.DataSource = SubCust_Onilne_bs;
                    }
                    else
                    {
                        Grd_Agent_Sub_Cust.DataSource = new BindingSource();


                    }
                }
                catch
                { Grd_Agent_Sub_Cust.DataSource = new BindingSource(); }
            }
            else
            {
                Grd_Agent_Sub_Cust.DataSource = new BindingSource();
            }

            get_user_filter();

        }
        //---------------------------------------
        private void get_user_filter()
        {
            if (Dt_user.Rows.Count > 0)
            {
                int user_id = 0;
                int.TryParse(Txt_Sub_Cust_User.Text, out user_id);


                if (Txt_Sub_Cust_User.Text != "")
                {
                    DT1 = Dt_user;
                    DT1.DefaultView.RowFilter = "( user_id  = " + user_id + " OR user_Name like '%" + Txt_Sub_Cust_User.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_user;
                    DT1.DefaultView.RowFilter = " user_id > " + user_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }

            else
            {
                Grd_Login_Cust_Online.DataSource = null;

            }
        }
        //---------------------------------------
        private void txt_Serch_sub_cust_TextChanged(object sender, EventArgs e)
        {
            Grd_Agent_Sub_Fill();
        }
        //---------------------------------------
        private void Txt_Sub_Cust_User_TextChanged(object sender, EventArgs e)
        {
            Grd_Agent_Sub_Fill();

        }
        //---------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if ((Convert.ToInt16(Cbo_CityUser_Id.SelectedValue) == 0) || (Convert.ToInt16(Cbo_CityUser_Id.SelectedValue) == null))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف مدينة المستخدم لهذا الوكيل" : "Please define city for users of this customer", MyGeneral_Lib.LblCap);
                Cbo_CityUser_Id.Focus();
                return;
            }

            if (Grd_Agent_Sub_Cust.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للاضافة" : "There is no data for adding", MyGeneral_Lib.LblCap);
                Grd_Agent_Sub_Cust.Focus();
                return;
            }

            if (connection.SQLDS.Tables["agnet_user_search_Tbl1"].Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للاضافة" : "There is no data for adding", MyGeneral_Lib.LblCap);
                Grd_Login_Cust_Online.Focus();
                return;
            }
            if (Form_Id == 1)
            {
                try
                {
                    if (connection.SQLDS.Tables["agnet_user_search_Tbl2"].DefaultView.ToTable().Select("Chk > 0").CopyToDataTable().Rows.Count == 0)
                    {

                    }
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للاضافة" : "There is no data for adding", MyGeneral_Lib.LblCap);
                    Grd_Login_Cust_Online.Focus();
                    return;
                }

            }
            DataRowView DRV = SubCust_Onilne_bs.Current as DataRowView;
            DataRow DR = DRV.Row;

         
            int Sub_Cust_ID = DR.Field<int>("Sub_Cust_ID");
            string ASub_CustName = DR.Field<string>("ASub_CustName");
            string ESub_CustNmae = ((DataRowView)SubCust_Onilne_bs.Current).Row["ESub_CustNmae"].ToString();


            Int32 Cust_Code = Convert.ToInt32(((DataRowView)SubCust_Onilne_bs.Current).Row["Cust_Code"]);

            #region Validations
            if (Form_Id == 1)
            {
                int Rec_No = Dt_user.Select("Chk > 0").Length;
                if (Rec_No <= 0 && Form_Id == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لم تحدداســـم المستخدم يجب تأشير الاســـم  " : "There is no user to add,Please select the users", MyGeneral_Lib.LblCap);
                    Grd_Login_Cust_Online.Focus();
                    return;
                }
            
                DT = Dt_user.DefaultView.ToTable(false, "Chk", "User_Id", "User_Name", "Pwd").Select(" Chk = 1 ").CopyToDataTable();
                DT = DT.DefaultView.ToTable(false, "Chk", "User_Id", "User_Name", "Pwd").Select().CopyToDataTable();
            }
            else
            {
                //if ((Convert.ToInt16(Cbo_upd_CityUser_Id.SelectedValue) == 0) || (Convert.ToInt16(Cbo_upd_CityUser_Id.SelectedValue) == null))
                //{
                //    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف مدينة المستخدم لهذا الوكيل" : "Please define city for users of this customer", MyGeneral_Lib.LblCap);
                //    Cbo_upd_CityUser_Id.Focus();
                //    return;
                //}

                DT = Dt_user.DefaultView.ToTable(false, "Chk", "User_Id", "User_Name", "Pwd").Select().CopyToDataTable();
            }
            #endregion
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Add_Upd_Users_Agent";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Agent_Users_Tbl", DT);
                connection.SQLCMD.Parameters.AddWithValue("@Cust_id", Sub_Cust_ID);
                connection.SQLCMD.Parameters.AddWithValue("@ASub_CustName", ASub_CustName);
                connection.SQLCMD.Parameters.AddWithValue("@ESub_CustNmae", ESub_CustNmae);
                connection.SQLCMD.Parameters.AddWithValue("@Cust_Code", Cust_Code);
                connection.SQLCMD.Parameters.AddWithValue("@Form_Id", Form_Id);
                connection.SQLCMD.Parameters.AddWithValue("@T_Id", cbo_term.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@City_Id", Convert.ToInt16(Cbo_CityUser_Id.SelectedValue));
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                //MyGeneral_Lib.Copytocliptext("Add_Upd_Users_Agent", connection.SQLCMD);

                IDataReader obj = connection.SQLCMD.ExecuteReader();
               // connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Add_Upd_Users_Agent");

                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }


                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }

            this.Close();
        }
        //---------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //--------------------------------
        private void edit_record()
        {

            connection.SqlExec("exec agnet_user_update " + T_id_bran + "," + SSub_Cust_id + "," + Convert.ToInt16(Cbo_CityUser_Id.SelectedValue), "agnet_user_search_Tbl");

            chan = false;
            cbo_term.DataSource = connection.SQLDS.Tables["agnet_user_search_Tbl"];
            cbo_term.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "Ecust_name";
            cbo_term.ValueMember = "T_id";
            cbo_term.Enabled = false;
            txt_Serch_sub_cust.Enabled = false;


            DataTable DT_Onilne_Cust = new DataTable();
            DT_Onilne_Cust = connection.SQLDS.Tables["agnet_user_search_Tbl1"];
            SubCust_Onilne_bs.DataSource = DT_Onilne_Cust;
            Grd_Agent_Sub_Cust.DataSource = SubCust_Onilne_bs;

            Dt_user = connection.SQLDS.Tables["agnet_user_search_Tbl2"];
            Sub_Cust_User_bs.DataSource = Dt_user;
            Grd_Login_Cust_Online.DataSource = Sub_Cust_User_bs;

            get_user_filter();
        }

        private void Agent_User_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            chan = false;

            string[] Used_Tbl = { "agnet_user_search_Tbl", "agnet_user_search_Tbl1", "agnet_user_search_Tbl2" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Grd_Agent_Sub_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (change)
            {
                int User_Sub_Cust_id = Convert.ToInt32(((DataRowView)SubCust_Onilne_bs.Current).Row["Sub_Cust_Id"]);

                sql_txt = " select a.CITY_ID,a.COUN_ID,b.Cit_AName,b.Cit_EName from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b "
                           + "  where a.CITY_ID = b.Cit_ID "
                           + "  and cust_id = " + User_Sub_Cust_id
                           + "  union "
                           + " select 0 as  city_id,0 as COUN_ID,'اختر' as Cit_AName ,'Select' as Cit_EName  from Sub_CUST_CITY_ONLINE a ,Cit_Tbl b "
                           + " where a.CITY_ID = b.Cit_ID "
                           + " and cust_id = " + User_Sub_Cust_id;

                connection.SqlExec(sql_txt, "Sub_CUST_CITY_ONLINE_Tbl");

                if (connection.SqlExec(sql_txt, "Sub_CUST_CITY_ONLINE_Tbl").Rows.Count > 0)
                {
                    Cbo_CityUser_Id.DataSource = connection.SQLDS.Tables["Sub_CUST_CITY_ONLINE_Tbl"];
                    Cbo_CityUser_Id.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                    Cbo_CityUser_Id.ValueMember = "CITY_ID";
                }
                //else
                //{
                //    Cbo_CityUser_Id.DataSource = new BindingSource();
                //    //MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف مدينة للوكيل" : "Please define city to sub customer", MyGeneral_Lib.LblCap);
                //    return;
                //}
            }
        }

        private void Cbo_CityUser_Id_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbo_change)
            {
                edit_record();
                //if (Form_Id == 2)
                //{
                //    Cbo_upd_CityUser_Id.DataBindings.Clear();
                //    Cbo_upd_CityUser_Id.DataBindings.Add("SelectedValue", binding_Cbo_CityUser_Id, "CITY_ID");
                //}
            }
        }
    }
}