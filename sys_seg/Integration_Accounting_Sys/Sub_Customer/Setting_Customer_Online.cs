﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Setting_Customer_Online : Form
    {
        BindingSource Grd_bs = new BindingSource();
        bool ChK = false;
        string SqlTxt = "";
        public Setting_Customer_Online()
        {

            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_Cust_Online.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust_Online.Columns["Column2"].DataPropertyName = "ESub_CustNmae";
                 Grd_Cust_Online.Columns["Column3"].DataPropertyName = "Acc_EName";
                 Grd_Cust_Online.Columns["Column5"].DataPropertyName = "ver_check_Ename";
                 Grd_Cust_Online.Columns["Column10"].DataPropertyName = "Chk_Search_Rem_Ename";
                 //Grd_Cust_Online.Columns["Column13"].DataPropertyName = "E_Address";
                 Grd_Cust_Online.Columns["Column11"].DataPropertyName = "City_Ename";
                 Grd_Cust_Online.Columns["Column12"].DataPropertyName = "Con_EName";
                 Grd_Cust_Online.Columns["Column16"].DataPropertyName = "Nat_Ename";
                 Grd_Cust_Online.Columns["Column4"].DataPropertyName = "Fmd_EName";
                 Grd_Cust_Online.Columns["Column18"].DataPropertyName = "CHK_Oper_rem_Ename";
                 //Grd_Cust_Online.Columns["Column17"].DataPropertyName = "Correspondence_Sys_Ename";
                 Grd_Cust_Online.Columns["Column19"].DataPropertyName = "comm_rem_flag_Ename";
                 Grd_Cust_Online.Columns["Column17"].DataPropertyName = "Loc_int_case_Ename";
                 Grd_Cust_Online.Columns["Column20"].DataPropertyName = "path_tcity_chk_Ename"; 
                
                
                
               
            }
            #endregion

        }
        //--------------------------------------------------
        private void Setting_Customer_Online_Load(object sender, EventArgs e)
        {
            Cbo_loc_int_case.Enabled = true;
            
           
                SqlTxt = "select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  "
                + " From CUSTOMERS A, TERMINALS B "
                + " Where A.CUST_ID = B.CUST_ID  "
                + " And A.Cust_Flag <> 0 ";

             cbo_term.ComboBox.DataSource = connection.SqlExec(SqlTxt, "Term_tbl");
             cbo_term.ComboBox.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "Ecust_name";
             cbo_term.ComboBox.ValueMember = "T_id";

             if (connection.Lang_id == 2)
             

             {
                 Cbo_loc_int_case.Items[0] = "All Cases";
                 Cbo_loc_int_case.Items[1] = "IN Only";
                 Cbo_loc_int_case.Items[2] = "Out Only";
             }

        }
        //--------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            ChK = false;
            Grd_bs.DataSource = connection.SqlExec(" exec Search_sub_Customer_Online_setting '" + Txt_Sub_Name.Text.Trim() + "'," + cbo_term.ComboBox.SelectedValue, "GRD_TBL");
            Grd_Cust_Online.DataSource = Grd_bs;
            ChK = true;
            Grd_Cust_Online_SelectionChanged(null, null);
        }
        //--------------------------------------------------
        private void Btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------------------------------
        private void Grd_Cust_Online_SelectionChanged(object sender, EventArgs e)
        {
            if (ChK)
            {
                CHK1.DataBindings.Clear();
                CHK2.DataBindings.Clear();
                CHK4.DataBindings.Clear();
                CHK5.DataBindings.Clear();
                Chk_Cust.DataBindings.Clear();
                chk_trans.DataBindings.Clear();
                Cbo_loc_int_case.DataBindings.Clear();
                CHK1.DataBindings.Add("Checked", Grd_bs, "ver_check");
                CHK2.DataBindings.Add("Checked", Grd_bs, "Chk_Search_Rem");
              
                CHK4.DataBindings.Add("Checked", Grd_bs, "CHK_Oper_rem");
                CHK5.DataBindings.Add("Checked", Grd_bs, "comm_rem_flag");
                Chk_Cust.DataBindings.Add("Checked", Grd_bs, "chk_comm_preview");
                chk_trans.DataBindings.Add("Checked", Grd_bs, "path_tcity_chk");
                Cbo_loc_int_case.DataBindings.Add("SelectedIndex", Grd_bs, "Loc_int_case");
                
            }
        }
        //--------------------------------------------------
        private void Txt_Sub_Name_TextChanged(object sender, EventArgs e)
        {
            ChK = false;
            Grd_Cust_Online.DataSource = new BindingSource();
            CHK1.Checked = false;
            CHK2.Checked = false;
            //CHK3.Checked = false;
            CHK4.Checked = false;
            CHK5.Checked = false;
            Chk_Cust.Checked = false;
            chk_trans.Checked = false;
            Cbo_loc_int_case.SelectedIndex=0;

        }
        //--------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {

            if (Grd_Cust_Online.RowCount > 0)
            {

                connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_Id", Convert.ToInt32(((DataRowView)Grd_bs.Current).Row["Sub_cust_id"]));
                connection.SQLCMD.Parameters.AddWithValue("@T_Id", Convert.ToInt32(((DataRowView)Grd_bs.Current).Row["T_id"]));
                connection.SQLCMD.Parameters.AddWithValue("@ver_check", Convert.ToByte(CHK1.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@Chk_Search_Rem", Convert.ToByte(CHK2.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@CHK_Oper_rem", Convert.ToByte(CHK4.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@CHK_comm_rem_flag", Convert.ToByte(CHK5.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@User_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@chk_comm_preview ", Convert.ToByte(Chk_Cust.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@Loc_int_case ", Cbo_loc_int_case.SelectedIndex);
                connection.SQLCMD.Parameters.AddWithValue("@path_tcity_chk ", Convert.ToByte(chk_trans.Checked));
                connection.SqlExec("Add_upd_sub_Customer_online_setting", connection.SQLCMD);
                connection.SQLCMD.Parameters.Clear();

                MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
                Txt_Sub_Name_TextChanged(null, null);
            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب تحديد الوكيل أولا " : "You must select the agent first", MyGeneral_Lib.LblCap);
                return;
            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Txt_Sub_Name.Text = "";
            ChK = false;
            Grd_bs.DataSource = connection.SqlExec(" exec Search_sub_Customer_Online_setting ' '," + cbo_term.ComboBox.SelectedValue, "GRD_TBL");
            Grd_Cust_Online.DataSource = Grd_bs;
            ChK = true;
            Grd_Cust_Online_SelectionChanged(null, null);
        }

        private void Setting_Customer_Online_FormClosed(object sender, FormClosedEventArgs e)
        {
            ChK = false;
           
            string[] Str = { "Term_tbl", "GRD_TBL" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void CHK5_CheckedChanged(object sender, EventArgs e)
        {
            if (CHK5.Checked == true)
            {
                Chk_Cust.Enabled = true;
            }
            else
            { 
                Chk_Cust.Checked = false;
                Chk_Cust.Enabled = false;
            }
        }

        

        private void cbo_term_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChK = false;
            Grd_Cust_Online.DataSource = new BindingSource();
            CHK1.Checked = false;
            CHK2.Checked = false;
            CHK4.Checked = false;
            CHK5.Checked = false;
            Chk_Cust.Checked = false;
            chk_trans.Checked = false;
          //  Cbo_loc_int_case.Enabled = false;
        }
    }
}