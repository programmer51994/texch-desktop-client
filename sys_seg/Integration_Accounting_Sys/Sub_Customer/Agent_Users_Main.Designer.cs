﻿namespace Integration_Accounting_Sys
{
    partial class Agent_Users_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Agent_Users_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.UpdBtn = new System.Windows.Forms.ToolStripButton();
            this.txt_Agent_Login = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_User = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel65 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel66 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel67 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel68 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel69 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel70 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel71 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel72 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel73 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel74 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel75 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel76 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel77 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel78 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel79 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel80 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel81 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel82 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel83 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel84 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel85 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel86 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel87 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel88 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel89 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel90 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel91 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel92 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel93 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel94 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel95 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel96 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel97 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel98 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel99 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel100 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel101 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel102 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel103 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel104 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel105 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel106 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel107 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel108 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel109 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel110 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel111 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel112 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel58 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel59 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel60 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel61 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel62 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel63 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel64 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel113 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel114 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel115 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel116 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel117 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel118 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel119 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel120 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel121 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel122 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel123 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel124 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel125 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel126 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel127 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel128 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel129 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel130 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel131 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel132 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel133 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel134 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel135 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel136 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel137 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel138 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel139 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel140 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel141 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel142 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel143 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel144 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel145 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel146 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel147 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel148 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel149 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel150 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel151 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel152 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel153 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel154 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel155 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel156 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel157 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel158 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel159 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel160 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Agent_Main = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.Cbo_Term_Main_sub = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Grd_User = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel33.SuspendLayout();
            this.flowLayoutPanel35.SuspendLayout();
            this.flowLayoutPanel37.SuspendLayout();
            this.flowLayoutPanel39.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel43.SuspendLayout();
            this.flowLayoutPanel45.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            this.flowLayoutPanel17.SuspendLayout();
            this.flowLayoutPanel21.SuspendLayout();
            this.flowLayoutPanel23.SuspendLayout();
            this.flowLayoutPanel25.SuspendLayout();
            this.flowLayoutPanel27.SuspendLayout();
            this.flowLayoutPanel29.SuspendLayout();
            this.flowLayoutPanel47.SuspendLayout();
            this.flowLayoutPanel65.SuspendLayout();
            this.flowLayoutPanel67.SuspendLayout();
            this.flowLayoutPanel69.SuspendLayout();
            this.flowLayoutPanel71.SuspendLayout();
            this.flowLayoutPanel73.SuspendLayout();
            this.flowLayoutPanel75.SuspendLayout();
            this.flowLayoutPanel77.SuspendLayout();
            this.flowLayoutPanel79.SuspendLayout();
            this.flowLayoutPanel81.SuspendLayout();
            this.flowLayoutPanel83.SuspendLayout();
            this.flowLayoutPanel85.SuspendLayout();
            this.flowLayoutPanel87.SuspendLayout();
            this.flowLayoutPanel89.SuspendLayout();
            this.flowLayoutPanel91.SuspendLayout();
            this.flowLayoutPanel93.SuspendLayout();
            this.flowLayoutPanel95.SuspendLayout();
            this.flowLayoutPanel97.SuspendLayout();
            this.flowLayoutPanel99.SuspendLayout();
            this.flowLayoutPanel101.SuspendLayout();
            this.flowLayoutPanel103.SuspendLayout();
            this.flowLayoutPanel105.SuspendLayout();
            this.flowLayoutPanel107.SuspendLayout();
            this.flowLayoutPanel109.SuspendLayout();
            this.flowLayoutPanel111.SuspendLayout();
            this.flowLayoutPanel49.SuspendLayout();
            this.flowLayoutPanel51.SuspendLayout();
            this.flowLayoutPanel53.SuspendLayout();
            this.flowLayoutPanel55.SuspendLayout();
            this.flowLayoutPanel57.SuspendLayout();
            this.flowLayoutPanel59.SuspendLayout();
            this.flowLayoutPanel61.SuspendLayout();
            this.flowLayoutPanel63.SuspendLayout();
            this.flowLayoutPanel113.SuspendLayout();
            this.flowLayoutPanel115.SuspendLayout();
            this.flowLayoutPanel117.SuspendLayout();
            this.flowLayoutPanel119.SuspendLayout();
            this.flowLayoutPanel121.SuspendLayout();
            this.flowLayoutPanel123.SuspendLayout();
            this.flowLayoutPanel125.SuspendLayout();
            this.flowLayoutPanel127.SuspendLayout();
            this.flowLayoutPanel129.SuspendLayout();
            this.flowLayoutPanel131.SuspendLayout();
            this.flowLayoutPanel133.SuspendLayout();
            this.flowLayoutPanel135.SuspendLayout();
            this.flowLayoutPanel137.SuspendLayout();
            this.flowLayoutPanel139.SuspendLayout();
            this.flowLayoutPanel141.SuspendLayout();
            this.flowLayoutPanel143.SuspendLayout();
            this.flowLayoutPanel145.SuspendLayout();
            this.flowLayoutPanel147.SuspendLayout();
            this.flowLayoutPanel149.SuspendLayout();
            this.flowLayoutPanel151.SuspendLayout();
            this.flowLayoutPanel153.SuspendLayout();
            this.flowLayoutPanel155.SuspendLayout();
            this.flowLayoutPanel157.SuspendLayout();
            this.flowLayoutPanel159.SuspendLayout();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Agent_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_User)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel31.Controls.Add(this.bindingNavigator1);
            this.flowLayoutPanel31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel31.Location = new System.Drawing.Point(-66, 28);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(986, 1);
            this.flowLayoutPanel31.TabIndex = 934;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(-36, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel32.TabIndex = 630;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(27, 10);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel33.TabIndex = 924;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel34.TabIndex = 630;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(27, 18);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel35.TabIndex = 925;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel36.TabIndex = 630;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel37.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel37.TabIndex = 924;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel38.TabIndex = 630;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(239, 26);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel39.TabIndex = 931;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel40.TabIndex = 630;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel41.TabIndex = 924;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel42.TabIndex = 630;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel43.TabIndex = 925;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel44.TabIndex = 630;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel45.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel45.TabIndex = 924;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel46.TabIndex = 630;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(986, 2);
            this.flowLayoutPanel1.TabIndex = 934;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-36, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel2.TabIndex = 630;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(27, 10);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel3.TabIndex = 924;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(27, 18);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel5.TabIndex = 925;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel6.TabIndex = 630;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel7.TabIndex = 924;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel8.TabIndex = 630;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(239, 26);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel9.TabIndex = 931;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel11.TabIndex = 924;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel12.TabIndex = 630;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel13.TabIndex = 925;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel14.TabIndex = 630;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel15.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel15.TabIndex = 924;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel16.TabIndex = 630;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.AllowItemReorder = true;
            this.bindingNavigator1.AllowMerge = false;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.CountItem = null;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator2,
            this.toolStripButton2,
            this.toolStripSeparator4,
            this.toolStripButton3,
            this.toolStripSeparator5});
            this.bindingNavigator1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.bindingNavigator1.Location = new System.Drawing.Point(1, 39);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = null;
            this.bindingNavigator1.Size = new System.Drawing.Size(985, 30);
            this.bindingNavigator1.TabIndex = 933;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(92, 27);
            this.toolStripButton1.Tag = "2";
            this.toolStripButton1.Text = "&اضـافـة جـديـد";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(60, 27);
            this.toolStripButton2.Tag = "3";
            this.toolStripButton2.Text = "تعـديــل";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButton3.Image = global::Integration_Accounting_Sys.Properties.Resources._12;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(64, 27);
            this.toolStripButton3.Tag = "4";
            this.toolStripButton3.Text = "حـــذف";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.UpdBtn});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(753, 30);
            this.BND.TabIndex = 933;
            this.BND.Text = "bindingNavigator1";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(92, 27);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "&اضـافـة جـديـد";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // UpdBtn
            // 
            this.UpdBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.UpdBtn.Image = ((System.Drawing.Image)(resources.GetObject("UpdBtn.Image")));
            this.UpdBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdBtn.Name = "UpdBtn";
            this.UpdBtn.Size = new System.Drawing.Size(60, 27);
            this.UpdBtn.Tag = "3";
            this.UpdBtn.Text = "تعـديــل";
            this.UpdBtn.Click += new System.EventHandler(this.UpdBtn_Click_1);
            // 
            // txt_Agent_Login
            // 
            this.txt_Agent_Login.BackColor = System.Drawing.Color.White;
            this.txt_Agent_Login.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Agent_Login.Location = new System.Drawing.Point(496, 62);
            this.txt_Agent_Login.Multiline = true;
            this.txt_Agent_Login.Name = "txt_Agent_Login";
            this.txt_Agent_Login.Size = new System.Drawing.Size(248, 22);
            this.txt_Agent_Login.TabIndex = 935;
            this.txt_Agent_Login.TextChanged += new System.EventHandler(this.txt_Agent_Login_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(262, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 942;
            this.label1.Text = "المستخـــــدم:";
            // 
            // txt_User
            // 
            this.txt_User.BackColor = System.Drawing.Color.White;
            this.txt_User.Enabled = false;
            this.txt_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_User.Location = new System.Drawing.Point(333, 34);
            this.txt_User.Name = "txt_User";
            this.txt_User.ReadOnly = true;
            this.txt_User.Size = new System.Drawing.Size(226, 23);
            this.txt_User.TabIndex = 941;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 32);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(258, 23);
            this.TxtTerm_Name.TabIndex = 940;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(619, 33);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(127, 23);
            this.TxtIn_Rec_Date.TabIndex = 943;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(565, 36);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 944;
            this.label3.Text = "التاريـخ:";
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel25);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel65);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel81);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel49);
            this.flowLayoutPanel17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(-67, 58);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(988, 1);
            this.flowLayoutPanel17.TabIndex = 945;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel22);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel23);
            this.flowLayoutPanel21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel21.Location = new System.Drawing.Point(29, 3);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel21.TabIndex = 925;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel22.TabIndex = 630;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel23.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel23.TabIndex = 924;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel24.TabIndex = 630;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel25.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel25.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel25.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel25.Location = new System.Drawing.Point(241, 11);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel25.TabIndex = 931;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel26.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel26.TabIndex = 630;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel28);
            this.flowLayoutPanel27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel27.TabIndex = 924;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel28.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel28.TabIndex = 630;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel29.Controls.Add(this.flowLayoutPanel30);
            this.flowLayoutPanel29.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel29.TabIndex = 925;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel30.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel30.TabIndex = 630;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel47.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel47.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel47.TabIndex = 924;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel48.TabIndex = 630;
            // 
            // flowLayoutPanel65
            // 
            this.flowLayoutPanel65.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel65.Controls.Add(this.flowLayoutPanel66);
            this.flowLayoutPanel65.Controls.Add(this.flowLayoutPanel67);
            this.flowLayoutPanel65.Controls.Add(this.flowLayoutPanel69);
            this.flowLayoutPanel65.Controls.Add(this.flowLayoutPanel73);
            this.flowLayoutPanel65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel65.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel65.Location = new System.Drawing.Point(-3, 19);
            this.flowLayoutPanel65.Name = "flowLayoutPanel65";
            this.flowLayoutPanel65.Size = new System.Drawing.Size(988, 2);
            this.flowLayoutPanel65.TabIndex = 946;
            // 
            // flowLayoutPanel66
            // 
            this.flowLayoutPanel66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel66.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel66.Name = "flowLayoutPanel66";
            this.flowLayoutPanel66.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel66.TabIndex = 630;
            // 
            // flowLayoutPanel67
            // 
            this.flowLayoutPanel67.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel67.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel67.Controls.Add(this.flowLayoutPanel68);
            this.flowLayoutPanel67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel67.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel67.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel67.Name = "flowLayoutPanel67";
            this.flowLayoutPanel67.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel67.TabIndex = 924;
            // 
            // flowLayoutPanel68
            // 
            this.flowLayoutPanel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel68.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel68.Name = "flowLayoutPanel68";
            this.flowLayoutPanel68.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel68.TabIndex = 630;
            // 
            // flowLayoutPanel69
            // 
            this.flowLayoutPanel69.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel69.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel69.Controls.Add(this.flowLayoutPanel70);
            this.flowLayoutPanel69.Controls.Add(this.flowLayoutPanel71);
            this.flowLayoutPanel69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel69.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel69.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel69.Name = "flowLayoutPanel69";
            this.flowLayoutPanel69.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel69.TabIndex = 925;
            // 
            // flowLayoutPanel70
            // 
            this.flowLayoutPanel70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel70.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel70.Name = "flowLayoutPanel70";
            this.flowLayoutPanel70.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel70.TabIndex = 630;
            // 
            // flowLayoutPanel71
            // 
            this.flowLayoutPanel71.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel71.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel71.Controls.Add(this.flowLayoutPanel72);
            this.flowLayoutPanel71.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel71.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel71.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel71.Name = "flowLayoutPanel71";
            this.flowLayoutPanel71.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel71.TabIndex = 924;
            // 
            // flowLayoutPanel72
            // 
            this.flowLayoutPanel72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel72.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel72.Name = "flowLayoutPanel72";
            this.flowLayoutPanel72.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel72.TabIndex = 630;
            // 
            // flowLayoutPanel73
            // 
            this.flowLayoutPanel73.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel73.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel73.Controls.Add(this.flowLayoutPanel74);
            this.flowLayoutPanel73.Controls.Add(this.flowLayoutPanel75);
            this.flowLayoutPanel73.Controls.Add(this.flowLayoutPanel77);
            this.flowLayoutPanel73.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel73.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel73.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel73.Name = "flowLayoutPanel73";
            this.flowLayoutPanel73.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel73.TabIndex = 931;
            // 
            // flowLayoutPanel74
            // 
            this.flowLayoutPanel74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel74.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel74.Name = "flowLayoutPanel74";
            this.flowLayoutPanel74.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel74.TabIndex = 630;
            // 
            // flowLayoutPanel75
            // 
            this.flowLayoutPanel75.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel75.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel75.Controls.Add(this.flowLayoutPanel76);
            this.flowLayoutPanel75.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel75.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel75.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel75.Name = "flowLayoutPanel75";
            this.flowLayoutPanel75.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel75.TabIndex = 924;
            // 
            // flowLayoutPanel76
            // 
            this.flowLayoutPanel76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel76.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel76.Name = "flowLayoutPanel76";
            this.flowLayoutPanel76.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel76.TabIndex = 630;
            // 
            // flowLayoutPanel77
            // 
            this.flowLayoutPanel77.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel77.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel77.Controls.Add(this.flowLayoutPanel78);
            this.flowLayoutPanel77.Controls.Add(this.flowLayoutPanel79);
            this.flowLayoutPanel77.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel77.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel77.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel77.Name = "flowLayoutPanel77";
            this.flowLayoutPanel77.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel77.TabIndex = 925;
            // 
            // flowLayoutPanel78
            // 
            this.flowLayoutPanel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel78.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel78.Name = "flowLayoutPanel78";
            this.flowLayoutPanel78.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel78.TabIndex = 630;
            // 
            // flowLayoutPanel79
            // 
            this.flowLayoutPanel79.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel79.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel79.Controls.Add(this.flowLayoutPanel80);
            this.flowLayoutPanel79.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel79.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel79.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel79.Name = "flowLayoutPanel79";
            this.flowLayoutPanel79.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel79.TabIndex = 924;
            // 
            // flowLayoutPanel80
            // 
            this.flowLayoutPanel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel80.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel80.Name = "flowLayoutPanel80";
            this.flowLayoutPanel80.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel80.TabIndex = 630;
            // 
            // flowLayoutPanel81
            // 
            this.flowLayoutPanel81.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel81.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel82);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel83);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel85);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel89);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel97);
            this.flowLayoutPanel81.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel81.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel81.Location = new System.Drawing.Point(-3, 27);
            this.flowLayoutPanel81.Name = "flowLayoutPanel81";
            this.flowLayoutPanel81.Size = new System.Drawing.Size(988, 2);
            this.flowLayoutPanel81.TabIndex = 947;
            // 
            // flowLayoutPanel82
            // 
            this.flowLayoutPanel82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel82.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel82.Name = "flowLayoutPanel82";
            this.flowLayoutPanel82.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel82.TabIndex = 630;
            // 
            // flowLayoutPanel83
            // 
            this.flowLayoutPanel83.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel83.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel83.Controls.Add(this.flowLayoutPanel84);
            this.flowLayoutPanel83.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel83.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel83.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel83.Name = "flowLayoutPanel83";
            this.flowLayoutPanel83.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel83.TabIndex = 924;
            // 
            // flowLayoutPanel84
            // 
            this.flowLayoutPanel84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel84.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel84.Name = "flowLayoutPanel84";
            this.flowLayoutPanel84.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel84.TabIndex = 630;
            // 
            // flowLayoutPanel85
            // 
            this.flowLayoutPanel85.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel85.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel85.Controls.Add(this.flowLayoutPanel86);
            this.flowLayoutPanel85.Controls.Add(this.flowLayoutPanel87);
            this.flowLayoutPanel85.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel85.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel85.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel85.Name = "flowLayoutPanel85";
            this.flowLayoutPanel85.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel85.TabIndex = 925;
            // 
            // flowLayoutPanel86
            // 
            this.flowLayoutPanel86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel86.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel86.Name = "flowLayoutPanel86";
            this.flowLayoutPanel86.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel86.TabIndex = 630;
            // 
            // flowLayoutPanel87
            // 
            this.flowLayoutPanel87.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel87.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel87.Controls.Add(this.flowLayoutPanel88);
            this.flowLayoutPanel87.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel87.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel87.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel87.Name = "flowLayoutPanel87";
            this.flowLayoutPanel87.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel87.TabIndex = 924;
            // 
            // flowLayoutPanel88
            // 
            this.flowLayoutPanel88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel88.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel88.Name = "flowLayoutPanel88";
            this.flowLayoutPanel88.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel88.TabIndex = 630;
            // 
            // flowLayoutPanel89
            // 
            this.flowLayoutPanel89.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel89.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel89.Controls.Add(this.flowLayoutPanel90);
            this.flowLayoutPanel89.Controls.Add(this.flowLayoutPanel91);
            this.flowLayoutPanel89.Controls.Add(this.flowLayoutPanel93);
            this.flowLayoutPanel89.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel89.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel89.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel89.Name = "flowLayoutPanel89";
            this.flowLayoutPanel89.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel89.TabIndex = 931;
            // 
            // flowLayoutPanel90
            // 
            this.flowLayoutPanel90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel90.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel90.Name = "flowLayoutPanel90";
            this.flowLayoutPanel90.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel90.TabIndex = 630;
            // 
            // flowLayoutPanel91
            // 
            this.flowLayoutPanel91.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel91.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel92);
            this.flowLayoutPanel91.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel91.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel91.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel91.Name = "flowLayoutPanel91";
            this.flowLayoutPanel91.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel91.TabIndex = 924;
            // 
            // flowLayoutPanel92
            // 
            this.flowLayoutPanel92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel92.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel92.Name = "flowLayoutPanel92";
            this.flowLayoutPanel92.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel92.TabIndex = 630;
            // 
            // flowLayoutPanel93
            // 
            this.flowLayoutPanel93.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel93.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel93.Controls.Add(this.flowLayoutPanel94);
            this.flowLayoutPanel93.Controls.Add(this.flowLayoutPanel95);
            this.flowLayoutPanel93.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel93.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel93.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel93.Name = "flowLayoutPanel93";
            this.flowLayoutPanel93.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel93.TabIndex = 925;
            // 
            // flowLayoutPanel94
            // 
            this.flowLayoutPanel94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel94.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel94.Name = "flowLayoutPanel94";
            this.flowLayoutPanel94.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel94.TabIndex = 630;
            // 
            // flowLayoutPanel95
            // 
            this.flowLayoutPanel95.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel95.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel95.Controls.Add(this.flowLayoutPanel96);
            this.flowLayoutPanel95.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel95.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel95.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel95.Name = "flowLayoutPanel95";
            this.flowLayoutPanel95.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel95.TabIndex = 924;
            // 
            // flowLayoutPanel96
            // 
            this.flowLayoutPanel96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel96.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel96.Name = "flowLayoutPanel96";
            this.flowLayoutPanel96.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel96.TabIndex = 630;
            // 
            // flowLayoutPanel97
            // 
            this.flowLayoutPanel97.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel97.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel98);
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel99);
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel101);
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel105);
            this.flowLayoutPanel97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel97.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel97.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel97.Name = "flowLayoutPanel97";
            this.flowLayoutPanel97.Size = new System.Drawing.Size(988, 2);
            this.flowLayoutPanel97.TabIndex = 946;
            // 
            // flowLayoutPanel98
            // 
            this.flowLayoutPanel98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel98.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel98.Name = "flowLayoutPanel98";
            this.flowLayoutPanel98.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel98.TabIndex = 630;
            // 
            // flowLayoutPanel99
            // 
            this.flowLayoutPanel99.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel99.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel100);
            this.flowLayoutPanel99.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel99.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel99.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel99.Name = "flowLayoutPanel99";
            this.flowLayoutPanel99.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel99.TabIndex = 924;
            // 
            // flowLayoutPanel100
            // 
            this.flowLayoutPanel100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel100.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel100.Name = "flowLayoutPanel100";
            this.flowLayoutPanel100.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel100.TabIndex = 630;
            // 
            // flowLayoutPanel101
            // 
            this.flowLayoutPanel101.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel101.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel102);
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel103);
            this.flowLayoutPanel101.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel101.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel101.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel101.Name = "flowLayoutPanel101";
            this.flowLayoutPanel101.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel101.TabIndex = 925;
            // 
            // flowLayoutPanel102
            // 
            this.flowLayoutPanel102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel102.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel102.Name = "flowLayoutPanel102";
            this.flowLayoutPanel102.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel102.TabIndex = 630;
            // 
            // flowLayoutPanel103
            // 
            this.flowLayoutPanel103.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel103.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel103.Controls.Add(this.flowLayoutPanel104);
            this.flowLayoutPanel103.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel103.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel103.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel103.Name = "flowLayoutPanel103";
            this.flowLayoutPanel103.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel103.TabIndex = 924;
            // 
            // flowLayoutPanel104
            // 
            this.flowLayoutPanel104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel104.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel104.Name = "flowLayoutPanel104";
            this.flowLayoutPanel104.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel104.TabIndex = 630;
            // 
            // flowLayoutPanel105
            // 
            this.flowLayoutPanel105.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel105.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel106);
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel107);
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel109);
            this.flowLayoutPanel105.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel105.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel105.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel105.Name = "flowLayoutPanel105";
            this.flowLayoutPanel105.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel105.TabIndex = 931;
            // 
            // flowLayoutPanel106
            // 
            this.flowLayoutPanel106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel106.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel106.Name = "flowLayoutPanel106";
            this.flowLayoutPanel106.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel106.TabIndex = 630;
            // 
            // flowLayoutPanel107
            // 
            this.flowLayoutPanel107.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel107.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel107.Controls.Add(this.flowLayoutPanel108);
            this.flowLayoutPanel107.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel107.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel107.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel107.Name = "flowLayoutPanel107";
            this.flowLayoutPanel107.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel107.TabIndex = 924;
            // 
            // flowLayoutPanel108
            // 
            this.flowLayoutPanel108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel108.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel108.Name = "flowLayoutPanel108";
            this.flowLayoutPanel108.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel108.TabIndex = 630;
            // 
            // flowLayoutPanel109
            // 
            this.flowLayoutPanel109.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel109.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel109.Controls.Add(this.flowLayoutPanel110);
            this.flowLayoutPanel109.Controls.Add(this.flowLayoutPanel111);
            this.flowLayoutPanel109.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel109.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel109.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel109.Name = "flowLayoutPanel109";
            this.flowLayoutPanel109.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel109.TabIndex = 925;
            // 
            // flowLayoutPanel110
            // 
            this.flowLayoutPanel110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel110.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel110.Name = "flowLayoutPanel110";
            this.flowLayoutPanel110.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel110.TabIndex = 630;
            // 
            // flowLayoutPanel111
            // 
            this.flowLayoutPanel111.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel111.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel112);
            this.flowLayoutPanel111.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel111.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel111.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel111.Name = "flowLayoutPanel111";
            this.flowLayoutPanel111.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel111.TabIndex = 924;
            // 
            // flowLayoutPanel112
            // 
            this.flowLayoutPanel112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel112.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel112.Name = "flowLayoutPanel112";
            this.flowLayoutPanel112.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel112.TabIndex = 630;
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel113);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel129);
            this.flowLayoutPanel49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(-3, 35);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(988, 1);
            this.flowLayoutPanel49.TabIndex = 948;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel50.TabIndex = 630;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel51.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel51.TabIndex = 924;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel52.TabIndex = 630;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel53.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel53.TabIndex = 925;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel54.TabIndex = 630;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel55.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel55.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel55.TabIndex = 924;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel56.TabIndex = 630;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel58);
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel59);
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel61);
            this.flowLayoutPanel57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel57.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel57.TabIndex = 931;
            // 
            // flowLayoutPanel58
            // 
            this.flowLayoutPanel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel58.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel58.Name = "flowLayoutPanel58";
            this.flowLayoutPanel58.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel58.TabIndex = 630;
            // 
            // flowLayoutPanel59
            // 
            this.flowLayoutPanel59.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel59.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel59.Controls.Add(this.flowLayoutPanel60);
            this.flowLayoutPanel59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel59.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel59.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel59.Name = "flowLayoutPanel59";
            this.flowLayoutPanel59.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel59.TabIndex = 924;
            // 
            // flowLayoutPanel60
            // 
            this.flowLayoutPanel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel60.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel60.Name = "flowLayoutPanel60";
            this.flowLayoutPanel60.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel60.TabIndex = 630;
            // 
            // flowLayoutPanel61
            // 
            this.flowLayoutPanel61.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel62);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel63);
            this.flowLayoutPanel61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel61.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel61.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel61.Name = "flowLayoutPanel61";
            this.flowLayoutPanel61.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel61.TabIndex = 925;
            // 
            // flowLayoutPanel62
            // 
            this.flowLayoutPanel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel62.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel62.Name = "flowLayoutPanel62";
            this.flowLayoutPanel62.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel62.TabIndex = 630;
            // 
            // flowLayoutPanel63
            // 
            this.flowLayoutPanel63.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel63.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel63.Controls.Add(this.flowLayoutPanel64);
            this.flowLayoutPanel63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel63.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel63.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel63.Name = "flowLayoutPanel63";
            this.flowLayoutPanel63.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel63.TabIndex = 924;
            // 
            // flowLayoutPanel64
            // 
            this.flowLayoutPanel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel64.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel64.Name = "flowLayoutPanel64";
            this.flowLayoutPanel64.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel64.TabIndex = 630;
            // 
            // flowLayoutPanel113
            // 
            this.flowLayoutPanel113.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel113.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel113.Controls.Add(this.flowLayoutPanel114);
            this.flowLayoutPanel113.Controls.Add(this.flowLayoutPanel115);
            this.flowLayoutPanel113.Controls.Add(this.flowLayoutPanel117);
            this.flowLayoutPanel113.Controls.Add(this.flowLayoutPanel121);
            this.flowLayoutPanel113.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel113.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel113.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel113.Name = "flowLayoutPanel113";
            this.flowLayoutPanel113.Size = new System.Drawing.Size(988, 2);
            this.flowLayoutPanel113.TabIndex = 946;
            // 
            // flowLayoutPanel114
            // 
            this.flowLayoutPanel114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel114.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel114.Name = "flowLayoutPanel114";
            this.flowLayoutPanel114.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel114.TabIndex = 630;
            // 
            // flowLayoutPanel115
            // 
            this.flowLayoutPanel115.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel115.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel115.Controls.Add(this.flowLayoutPanel116);
            this.flowLayoutPanel115.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel115.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel115.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel115.Name = "flowLayoutPanel115";
            this.flowLayoutPanel115.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel115.TabIndex = 924;
            // 
            // flowLayoutPanel116
            // 
            this.flowLayoutPanel116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel116.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel116.Name = "flowLayoutPanel116";
            this.flowLayoutPanel116.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel116.TabIndex = 630;
            // 
            // flowLayoutPanel117
            // 
            this.flowLayoutPanel117.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel117.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel117.Controls.Add(this.flowLayoutPanel118);
            this.flowLayoutPanel117.Controls.Add(this.flowLayoutPanel119);
            this.flowLayoutPanel117.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel117.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel117.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel117.Name = "flowLayoutPanel117";
            this.flowLayoutPanel117.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel117.TabIndex = 925;
            // 
            // flowLayoutPanel118
            // 
            this.flowLayoutPanel118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel118.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel118.Name = "flowLayoutPanel118";
            this.flowLayoutPanel118.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel118.TabIndex = 630;
            // 
            // flowLayoutPanel119
            // 
            this.flowLayoutPanel119.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel119.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel119.Controls.Add(this.flowLayoutPanel120);
            this.flowLayoutPanel119.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel119.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel119.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel119.Name = "flowLayoutPanel119";
            this.flowLayoutPanel119.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel119.TabIndex = 924;
            // 
            // flowLayoutPanel120
            // 
            this.flowLayoutPanel120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel120.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel120.Name = "flowLayoutPanel120";
            this.flowLayoutPanel120.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel120.TabIndex = 630;
            // 
            // flowLayoutPanel121
            // 
            this.flowLayoutPanel121.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel121.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel121.Controls.Add(this.flowLayoutPanel122);
            this.flowLayoutPanel121.Controls.Add(this.flowLayoutPanel123);
            this.flowLayoutPanel121.Controls.Add(this.flowLayoutPanel125);
            this.flowLayoutPanel121.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel121.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel121.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel121.Name = "flowLayoutPanel121";
            this.flowLayoutPanel121.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel121.TabIndex = 931;
            // 
            // flowLayoutPanel122
            // 
            this.flowLayoutPanel122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel122.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel122.Name = "flowLayoutPanel122";
            this.flowLayoutPanel122.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel122.TabIndex = 630;
            // 
            // flowLayoutPanel123
            // 
            this.flowLayoutPanel123.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel123.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel123.Controls.Add(this.flowLayoutPanel124);
            this.flowLayoutPanel123.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel123.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel123.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel123.Name = "flowLayoutPanel123";
            this.flowLayoutPanel123.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel123.TabIndex = 924;
            // 
            // flowLayoutPanel124
            // 
            this.flowLayoutPanel124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel124.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel124.Name = "flowLayoutPanel124";
            this.flowLayoutPanel124.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel124.TabIndex = 630;
            // 
            // flowLayoutPanel125
            // 
            this.flowLayoutPanel125.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel125.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel126);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel127);
            this.flowLayoutPanel125.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel125.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel125.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel125.Name = "flowLayoutPanel125";
            this.flowLayoutPanel125.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel125.TabIndex = 925;
            // 
            // flowLayoutPanel126
            // 
            this.flowLayoutPanel126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel126.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel126.Name = "flowLayoutPanel126";
            this.flowLayoutPanel126.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel126.TabIndex = 630;
            // 
            // flowLayoutPanel127
            // 
            this.flowLayoutPanel127.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel127.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel128);
            this.flowLayoutPanel127.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel127.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel127.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel127.Name = "flowLayoutPanel127";
            this.flowLayoutPanel127.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel127.TabIndex = 924;
            // 
            // flowLayoutPanel128
            // 
            this.flowLayoutPanel128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel128.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel128.Name = "flowLayoutPanel128";
            this.flowLayoutPanel128.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel128.TabIndex = 630;
            // 
            // flowLayoutPanel129
            // 
            this.flowLayoutPanel129.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel129.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel129.Controls.Add(this.flowLayoutPanel130);
            this.flowLayoutPanel129.Controls.Add(this.flowLayoutPanel131);
            this.flowLayoutPanel129.Controls.Add(this.flowLayoutPanel133);
            this.flowLayoutPanel129.Controls.Add(this.flowLayoutPanel137);
            this.flowLayoutPanel129.Controls.Add(this.flowLayoutPanel145);
            this.flowLayoutPanel129.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel129.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel129.Location = new System.Drawing.Point(-3, 42);
            this.flowLayoutPanel129.Name = "flowLayoutPanel129";
            this.flowLayoutPanel129.Size = new System.Drawing.Size(988, 2);
            this.flowLayoutPanel129.TabIndex = 947;
            // 
            // flowLayoutPanel130
            // 
            this.flowLayoutPanel130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel130.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel130.Name = "flowLayoutPanel130";
            this.flowLayoutPanel130.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel130.TabIndex = 630;
            // 
            // flowLayoutPanel131
            // 
            this.flowLayoutPanel131.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel131.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel131.Controls.Add(this.flowLayoutPanel132);
            this.flowLayoutPanel131.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel131.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel131.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel131.Name = "flowLayoutPanel131";
            this.flowLayoutPanel131.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel131.TabIndex = 924;
            // 
            // flowLayoutPanel132
            // 
            this.flowLayoutPanel132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel132.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel132.Name = "flowLayoutPanel132";
            this.flowLayoutPanel132.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel132.TabIndex = 630;
            // 
            // flowLayoutPanel133
            // 
            this.flowLayoutPanel133.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel133.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel133.Controls.Add(this.flowLayoutPanel134);
            this.flowLayoutPanel133.Controls.Add(this.flowLayoutPanel135);
            this.flowLayoutPanel133.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel133.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel133.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel133.Name = "flowLayoutPanel133";
            this.flowLayoutPanel133.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel133.TabIndex = 925;
            // 
            // flowLayoutPanel134
            // 
            this.flowLayoutPanel134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel134.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel134.Name = "flowLayoutPanel134";
            this.flowLayoutPanel134.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel134.TabIndex = 630;
            // 
            // flowLayoutPanel135
            // 
            this.flowLayoutPanel135.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel135.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel136);
            this.flowLayoutPanel135.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel135.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel135.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel135.Name = "flowLayoutPanel135";
            this.flowLayoutPanel135.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel135.TabIndex = 924;
            // 
            // flowLayoutPanel136
            // 
            this.flowLayoutPanel136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel136.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel136.Name = "flowLayoutPanel136";
            this.flowLayoutPanel136.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel136.TabIndex = 630;
            // 
            // flowLayoutPanel137
            // 
            this.flowLayoutPanel137.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel137.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel138);
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel139);
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel141);
            this.flowLayoutPanel137.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel137.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel137.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel137.Name = "flowLayoutPanel137";
            this.flowLayoutPanel137.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel137.TabIndex = 931;
            // 
            // flowLayoutPanel138
            // 
            this.flowLayoutPanel138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel138.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel138.Name = "flowLayoutPanel138";
            this.flowLayoutPanel138.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel138.TabIndex = 630;
            // 
            // flowLayoutPanel139
            // 
            this.flowLayoutPanel139.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel139.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel139.Controls.Add(this.flowLayoutPanel140);
            this.flowLayoutPanel139.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel139.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel139.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel139.Name = "flowLayoutPanel139";
            this.flowLayoutPanel139.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel139.TabIndex = 924;
            // 
            // flowLayoutPanel140
            // 
            this.flowLayoutPanel140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel140.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel140.Name = "flowLayoutPanel140";
            this.flowLayoutPanel140.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel140.TabIndex = 630;
            // 
            // flowLayoutPanel141
            // 
            this.flowLayoutPanel141.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel141.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel141.Controls.Add(this.flowLayoutPanel142);
            this.flowLayoutPanel141.Controls.Add(this.flowLayoutPanel143);
            this.flowLayoutPanel141.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel141.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel141.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel141.Name = "flowLayoutPanel141";
            this.flowLayoutPanel141.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel141.TabIndex = 925;
            // 
            // flowLayoutPanel142
            // 
            this.flowLayoutPanel142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel142.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel142.Name = "flowLayoutPanel142";
            this.flowLayoutPanel142.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel142.TabIndex = 630;
            // 
            // flowLayoutPanel143
            // 
            this.flowLayoutPanel143.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel143.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel143.Controls.Add(this.flowLayoutPanel144);
            this.flowLayoutPanel143.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel143.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel143.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel143.Name = "flowLayoutPanel143";
            this.flowLayoutPanel143.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel143.TabIndex = 924;
            // 
            // flowLayoutPanel144
            // 
            this.flowLayoutPanel144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel144.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel144.Name = "flowLayoutPanel144";
            this.flowLayoutPanel144.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel144.TabIndex = 630;
            // 
            // flowLayoutPanel145
            // 
            this.flowLayoutPanel145.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel145.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel146);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel147);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel149);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel153);
            this.flowLayoutPanel145.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel145.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel145.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel145.Name = "flowLayoutPanel145";
            this.flowLayoutPanel145.Size = new System.Drawing.Size(988, 2);
            this.flowLayoutPanel145.TabIndex = 946;
            // 
            // flowLayoutPanel146
            // 
            this.flowLayoutPanel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel146.Location = new System.Drawing.Point(-34, 3);
            this.flowLayoutPanel146.Name = "flowLayoutPanel146";
            this.flowLayoutPanel146.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel146.TabIndex = 630;
            // 
            // flowLayoutPanel147
            // 
            this.flowLayoutPanel147.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel147.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel148);
            this.flowLayoutPanel147.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel147.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel147.Location = new System.Drawing.Point(29, 10);
            this.flowLayoutPanel147.Name = "flowLayoutPanel147";
            this.flowLayoutPanel147.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel147.TabIndex = 924;
            // 
            // flowLayoutPanel148
            // 
            this.flowLayoutPanel148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel148.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel148.Name = "flowLayoutPanel148";
            this.flowLayoutPanel148.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel148.TabIndex = 630;
            // 
            // flowLayoutPanel149
            // 
            this.flowLayoutPanel149.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel149.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel149.Controls.Add(this.flowLayoutPanel150);
            this.flowLayoutPanel149.Controls.Add(this.flowLayoutPanel151);
            this.flowLayoutPanel149.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel149.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel149.Location = new System.Drawing.Point(29, 18);
            this.flowLayoutPanel149.Name = "flowLayoutPanel149";
            this.flowLayoutPanel149.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel149.TabIndex = 925;
            // 
            // flowLayoutPanel150
            // 
            this.flowLayoutPanel150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel150.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel150.Name = "flowLayoutPanel150";
            this.flowLayoutPanel150.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel150.TabIndex = 630;
            // 
            // flowLayoutPanel151
            // 
            this.flowLayoutPanel151.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel151.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel151.Controls.Add(this.flowLayoutPanel152);
            this.flowLayoutPanel151.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel151.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel151.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel151.Name = "flowLayoutPanel151";
            this.flowLayoutPanel151.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel151.TabIndex = 924;
            // 
            // flowLayoutPanel152
            // 
            this.flowLayoutPanel152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel152.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel152.Name = "flowLayoutPanel152";
            this.flowLayoutPanel152.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel152.TabIndex = 630;
            // 
            // flowLayoutPanel153
            // 
            this.flowLayoutPanel153.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel153.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel153.Controls.Add(this.flowLayoutPanel154);
            this.flowLayoutPanel153.Controls.Add(this.flowLayoutPanel155);
            this.flowLayoutPanel153.Controls.Add(this.flowLayoutPanel157);
            this.flowLayoutPanel153.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel153.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel153.Location = new System.Drawing.Point(241, 26);
            this.flowLayoutPanel153.Name = "flowLayoutPanel153";
            this.flowLayoutPanel153.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel153.TabIndex = 931;
            // 
            // flowLayoutPanel154
            // 
            this.flowLayoutPanel154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel154.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel154.Name = "flowLayoutPanel154";
            this.flowLayoutPanel154.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel154.TabIndex = 630;
            // 
            // flowLayoutPanel155
            // 
            this.flowLayoutPanel155.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel155.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel156);
            this.flowLayoutPanel155.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel155.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel155.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel155.Name = "flowLayoutPanel155";
            this.flowLayoutPanel155.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel155.TabIndex = 924;
            // 
            // flowLayoutPanel156
            // 
            this.flowLayoutPanel156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel156.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel156.Name = "flowLayoutPanel156";
            this.flowLayoutPanel156.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel156.TabIndex = 630;
            // 
            // flowLayoutPanel157
            // 
            this.flowLayoutPanel157.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel157.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel158);
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel159);
            this.flowLayoutPanel157.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel157.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel157.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel157.Name = "flowLayoutPanel157";
            this.flowLayoutPanel157.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel157.TabIndex = 925;
            // 
            // flowLayoutPanel158
            // 
            this.flowLayoutPanel158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel158.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel158.Name = "flowLayoutPanel158";
            this.flowLayoutPanel158.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel158.TabIndex = 630;
            // 
            // flowLayoutPanel159
            // 
            this.flowLayoutPanel159.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel159.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel159.Controls.Add(this.flowLayoutPanel160);
            this.flowLayoutPanel159.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel159.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel159.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel159.Name = "flowLayoutPanel159";
            this.flowLayoutPanel159.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel159.TabIndex = 924;
            // 
            // flowLayoutPanel160
            // 
            this.flowLayoutPanel160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel160.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel160.Name = "flowLayoutPanel160";
            this.flowLayoutPanel160.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel160.TabIndex = 630;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel20);
            this.flowLayoutPanel19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(-96, 483);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(956, 1);
            this.flowLayoutPanel19.TabIndex = 924;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel20.TabIndex = 630;
            // 
            // Grd_Agent_Main
            // 
            this.Grd_Agent_Main.AllowUserToAddRows = false;
            this.Grd_Agent_Main.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Agent_Main.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Agent_Main.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Agent_Main.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Agent_Main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grd_Agent_Main.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column7,
            this.Column2,
            this.Column1,
            this.Column6,
            this.Column3,
            this.Column4});
            this.Grd_Agent_Main.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Agent_Main.Location = new System.Drawing.Point(12, 87);
            this.Grd_Agent_Main.Name = "Grd_Agent_Main";
            this.Grd_Agent_Main.ReadOnly = true;
            this.Grd_Agent_Main.RowHeadersWidth = 20;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Agent_Main.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Agent_Main.Size = new System.Drawing.Size(735, 178);
            this.Grd_Agent_Main.TabIndex = 947;
            this.Grd_Agent_Main.SelectionChanged += new System.EventHandler(this.Grd_Agent_Main_SelectionChanged);
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ASub_CustName";
            this.Column5.HeaderText = "اســـم الوكيل";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column5.Width = 500;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Cust_Code";
            this.Column7.HeaderText = "تشفير الحساب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 210;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "login_Aname";
            this.Column2.HeaderText = "اســــم المستــخدم";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Visible = false;
            this.Column2.Width = 265;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "login_id";
            this.Column1.HeaderText = "login_id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Visible = false;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "User_Password";
            this.Column6.HeaderText = "كلمة المرور";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column6.Visible = false;
            this.Column6.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "user_id";
            this.Column3.HeaderText = "user_id";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Visible = false;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Cust_id";
            this.Column4.HeaderText = "Cust_id";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(16, 66);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 967;
            this.label4.Text = "الثــانويـــون :";
            // 
            // Cbo_Term_Main_sub
            // 
            this.Cbo_Term_Main_sub.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Term_Main_sub.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Term_Main_sub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Term_Main_sub.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Term_Main_sub.FormattingEnabled = true;
            this.Cbo_Term_Main_sub.Location = new System.Drawing.Point(100, 61);
            this.Cbo_Term_Main_sub.Name = "Cbo_Term_Main_sub";
            this.Cbo_Term_Main_sub.Size = new System.Drawing.Size(241, 24);
            this.Cbo_Term_Main_sub.TabIndex = 969;
            this.Cbo_Term_Main_sub.SelectedIndexChanged += new System.EventHandler(this.Cbo_Main_sub_cust_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(342, 66);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(122, 16);
            this.label5.TabIndex = 970;
            this.label5.Text = "بحـــث اســــم الوكيـــــل:";
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rectangleShape1.Location = new System.Drawing.Point(1, 277);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(846, 0);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(753, 509);
            this.shapeContainer1.TabIndex = 971;
            this.shapeContainer1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 487);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(753, 22);
            this.statusStrip1.TabIndex = 979;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Grd_User
            // 
            this.Grd_User.AllowUserToAddRows = false;
            this.Grd_User.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_User.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_User.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_User.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_User.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grd_User.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column9});
            this.Grd_User.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_User.Location = new System.Drawing.Point(12, 287);
            this.Grd_User.Name = "Grd_User";
            this.Grd_User.ReadOnly = true;
            this.Grd_User.RowHeadersWidth = 20;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_User.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_User.Size = new System.Drawing.Size(734, 193);
            this.Grd_User.TabIndex = 980;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(10, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 16);
            this.label2.TabIndex = 981;
            this.label2.Text = "المستخدمــــون ....";
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "login_Aname";
            this.Column8.HeaderText = "اســــم المستــخدم";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 500;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Cit_AName";
            this.Column9.HeaderText = "مدينة الإرسال";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 200;
            // 
            // Agent_Users_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 509);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Grd_User);
            this.Controls.Add(this.flowLayoutPanel19);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cbo_Term_Main_sub);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Grd_Agent_Main);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_User);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.txt_Agent_Login);
            this.Controls.Add(this.flowLayoutPanel31);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.shapeContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Agent_Users_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "512";
            this.Text = "Agent_Users_Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Agent_Users_Main_FormClosed);
            this.Load += new System.EventHandler(this.Agent_Users_Main_Load);
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel33.ResumeLayout(false);
            this.flowLayoutPanel35.ResumeLayout(false);
            this.flowLayoutPanel37.ResumeLayout(false);
            this.flowLayoutPanel39.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel43.ResumeLayout(false);
            this.flowLayoutPanel45.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            this.flowLayoutPanel17.ResumeLayout(false);
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel23.ResumeLayout(false);
            this.flowLayoutPanel25.ResumeLayout(false);
            this.flowLayoutPanel27.ResumeLayout(false);
            this.flowLayoutPanel29.ResumeLayout(false);
            this.flowLayoutPanel47.ResumeLayout(false);
            this.flowLayoutPanel65.ResumeLayout(false);
            this.flowLayoutPanel67.ResumeLayout(false);
            this.flowLayoutPanel69.ResumeLayout(false);
            this.flowLayoutPanel71.ResumeLayout(false);
            this.flowLayoutPanel73.ResumeLayout(false);
            this.flowLayoutPanel75.ResumeLayout(false);
            this.flowLayoutPanel77.ResumeLayout(false);
            this.flowLayoutPanel79.ResumeLayout(false);
            this.flowLayoutPanel81.ResumeLayout(false);
            this.flowLayoutPanel83.ResumeLayout(false);
            this.flowLayoutPanel85.ResumeLayout(false);
            this.flowLayoutPanel87.ResumeLayout(false);
            this.flowLayoutPanel89.ResumeLayout(false);
            this.flowLayoutPanel91.ResumeLayout(false);
            this.flowLayoutPanel93.ResumeLayout(false);
            this.flowLayoutPanel95.ResumeLayout(false);
            this.flowLayoutPanel97.ResumeLayout(false);
            this.flowLayoutPanel99.ResumeLayout(false);
            this.flowLayoutPanel101.ResumeLayout(false);
            this.flowLayoutPanel103.ResumeLayout(false);
            this.flowLayoutPanel105.ResumeLayout(false);
            this.flowLayoutPanel107.ResumeLayout(false);
            this.flowLayoutPanel109.ResumeLayout(false);
            this.flowLayoutPanel111.ResumeLayout(false);
            this.flowLayoutPanel49.ResumeLayout(false);
            this.flowLayoutPanel51.ResumeLayout(false);
            this.flowLayoutPanel53.ResumeLayout(false);
            this.flowLayoutPanel55.ResumeLayout(false);
            this.flowLayoutPanel57.ResumeLayout(false);
            this.flowLayoutPanel59.ResumeLayout(false);
            this.flowLayoutPanel61.ResumeLayout(false);
            this.flowLayoutPanel63.ResumeLayout(false);
            this.flowLayoutPanel113.ResumeLayout(false);
            this.flowLayoutPanel115.ResumeLayout(false);
            this.flowLayoutPanel117.ResumeLayout(false);
            this.flowLayoutPanel119.ResumeLayout(false);
            this.flowLayoutPanel121.ResumeLayout(false);
            this.flowLayoutPanel123.ResumeLayout(false);
            this.flowLayoutPanel125.ResumeLayout(false);
            this.flowLayoutPanel127.ResumeLayout(false);
            this.flowLayoutPanel129.ResumeLayout(false);
            this.flowLayoutPanel131.ResumeLayout(false);
            this.flowLayoutPanel133.ResumeLayout(false);
            this.flowLayoutPanel135.ResumeLayout(false);
            this.flowLayoutPanel137.ResumeLayout(false);
            this.flowLayoutPanel139.ResumeLayout(false);
            this.flowLayoutPanel141.ResumeLayout(false);
            this.flowLayoutPanel143.ResumeLayout(false);
            this.flowLayoutPanel145.ResumeLayout(false);
            this.flowLayoutPanel147.ResumeLayout(false);
            this.flowLayoutPanel149.ResumeLayout(false);
            this.flowLayoutPanel151.ResumeLayout(false);
            this.flowLayoutPanel153.ResumeLayout(false);
            this.flowLayoutPanel155.ResumeLayout(false);
            this.flowLayoutPanel157.ResumeLayout(false);
            this.flowLayoutPanel159.ResumeLayout(false);
            this.flowLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Agent_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_User)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton UpdBtn;
        private System.Windows.Forms.TextBox txt_Agent_Login;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_User;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.DataGridView Grd_Agent_Main;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Cbo_Term_Main_sub;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel65;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel66;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel67;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel68;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel69;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel70;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel71;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel72;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel73;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel74;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel75;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel76;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel77;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel78;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel79;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel80;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel81;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel82;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel83;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel84;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel85;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel86;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel87;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel88;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel89;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel90;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel91;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel92;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel93;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel94;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel95;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel96;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel97;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel98;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel99;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel100;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel101;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel102;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel103;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel104;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel105;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel106;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel107;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel108;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel109;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel110;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel111;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel112;
        private System.Windows.Forms.Label label5;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel58;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel59;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel61;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel62;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel63;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel64;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel113;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel114;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel115;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel116;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel117;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel118;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel119;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel120;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel121;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel122;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel123;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel124;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel125;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel126;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel127;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel128;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel129;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel130;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel131;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel132;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel133;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel134;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel135;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel136;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel137;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel138;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel139;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel140;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel141;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel142;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel143;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel144;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel145;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel146;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel147;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel148;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel149;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel150;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel151;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel152;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel153;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel154;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel155;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel156;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel157;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel158;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel159;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel160;
        private System.Windows.Forms.DataGridView Grd_User;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
    }
}