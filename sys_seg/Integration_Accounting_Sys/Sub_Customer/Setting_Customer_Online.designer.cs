﻿namespace Integration_Accounting_Sys
{
    partial class Setting_Customer_Online
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setting_Customer_Online));
            this.label1 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.Grd_Cust_Online = new System.Windows.Forms.DataGridView();
            this.Btn_Exist = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.CHK2 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel74 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel75 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel76 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel77 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel78 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel79 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel80 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel81 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel82 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel83 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel84 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel85 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel86 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel87 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel88 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel89 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel90 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel91 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel92 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel93 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel94 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel95 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel96 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel97 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel98 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel99 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel100 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel101 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel102 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel103 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel104 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel105 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel106 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel107 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel108 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel109 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel110 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel111 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel112 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel113 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel114 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel115 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel116 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel117 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel118 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel119 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel120 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel121 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel122 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel123 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel124 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel125 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel126 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel127 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel128 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel129 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel130 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel131 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel132 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel133 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel134 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel135 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel136 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel137 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHK1 = new System.Windows.Forms.CheckBox();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.label7 = new System.Windows.Forms.ToolStripLabel();
            this.cbo_term = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Txt_Sub_Name = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHK4 = new System.Windows.Forms.CheckBox();
            this.CHK5 = new System.Windows.Forms.CheckBox();
            this.Chk_Cust = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.ToolStripButton();
            this.SearchBtn = new System.Windows.Forms.ToolStripButton();
            this.label10 = new System.Windows.Forms.Label();
            this.Cbo_loc_int_case = new System.Windows.Forms.ComboBox();
            this.chk_trans = new System.Windows.Forms.CheckBox();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Online)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel33.SuspendLayout();
            this.flowLayoutPanel35.SuspendLayout();
            this.flowLayoutPanel37.SuspendLayout();
            this.flowLayoutPanel39.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel43.SuspendLayout();
            this.flowLayoutPanel45.SuspendLayout();
            this.flowLayoutPanel74.SuspendLayout();
            this.flowLayoutPanel76.SuspendLayout();
            this.flowLayoutPanel78.SuspendLayout();
            this.flowLayoutPanel80.SuspendLayout();
            this.flowLayoutPanel82.SuspendLayout();
            this.flowLayoutPanel84.SuspendLayout();
            this.flowLayoutPanel86.SuspendLayout();
            this.flowLayoutPanel88.SuspendLayout();
            this.flowLayoutPanel90.SuspendLayout();
            this.flowLayoutPanel92.SuspendLayout();
            this.flowLayoutPanel94.SuspendLayout();
            this.flowLayoutPanel96.SuspendLayout();
            this.flowLayoutPanel98.SuspendLayout();
            this.flowLayoutPanel100.SuspendLayout();
            this.flowLayoutPanel102.SuspendLayout();
            this.flowLayoutPanel104.SuspendLayout();
            this.flowLayoutPanel106.SuspendLayout();
            this.flowLayoutPanel108.SuspendLayout();
            this.flowLayoutPanel110.SuspendLayout();
            this.flowLayoutPanel112.SuspendLayout();
            this.flowLayoutPanel114.SuspendLayout();
            this.flowLayoutPanel116.SuspendLayout();
            this.flowLayoutPanel118.SuspendLayout();
            this.flowLayoutPanel120.SuspendLayout();
            this.flowLayoutPanel122.SuspendLayout();
            this.flowLayoutPanel124.SuspendLayout();
            this.flowLayoutPanel126.SuspendLayout();
            this.flowLayoutPanel128.SuspendLayout();
            this.flowLayoutPanel130.SuspendLayout();
            this.flowLayoutPanel132.SuspendLayout();
            this.flowLayoutPanel134.SuspendLayout();
            this.flowLayoutPanel136.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(257, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 905;
            this.label1.Text = "المستخـــــدم:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(710, 37);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(110, 23);
            this.TxtIn_Rec_Date.TabIndex = 904;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(348, 37);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(259, 23);
            this.TxtUser.TabIndex = 903;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 37);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(246, 23);
            this.TxtTerm_Name.TabIndex = 902;
            // 
            // Grd_Cust_Online
            // 
            this.Grd_Cust_Online.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Online.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Online.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust_Online.ColumnHeadersHeight = 40;
            this.Grd_Cust_Online.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column10,
            this.Column18,
            this.Column19,
            this.Column17,
            this.Column20,
            this.Column13,
            this.Column11,
            this.Column12,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column4,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust_Online.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cust_Online.Location = new System.Drawing.Point(7, 90);
            this.Grd_Cust_Online.Name = "Grd_Cust_Online";
            this.Grd_Cust_Online.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = "n3";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Online.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cust_Online.RowHeadersWidth = 15;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Online.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cust_Online.Size = new System.Drawing.Size(815, 350);
            this.Grd_Cust_Online.TabIndex = 910;
            this.Grd_Cust_Online.SelectionChanged += new System.EventHandler(this.Grd_Cust_Online_SelectionChanged);
            // 
            // Btn_Exist
            // 
            this.Btn_Exist.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Exist.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Exist.Location = new System.Drawing.Point(408, 551);
            this.Btn_Exist.Name = "Btn_Exist";
            this.Btn_Exist.Size = new System.Drawing.Size(83, 29);
            this.Btn_Exist.TabIndex = 918;
            this.Btn_Exist.Text = "انهـــــاء";
            this.Btn_Exist.UseVisualStyleBackColor = true;
            this.Btn_Exist.Click += new System.EventHandler(this.Btn_Exist_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(328, 551);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(81, 29);
            this.Btn_Add.TabIndex = 917;
            this.Btn_Add.Text = "موافـــق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Maroon;
            this.label21.Location = new System.Drawing.Point(548, 423);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 13);
            this.label21.TabIndex = 922;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.label5);
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-13, 457);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(956, 1);
            this.flowLayoutPanel4.TabIndex = 924;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(858, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(93, 14);
            this.label5.TabIndex = 928;
            this.label5.Text = "التفاصيــــــــل...";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-5, 17);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(956, 1);
            this.flowLayoutPanel1.TabIndex = 929;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(858, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(93, 14);
            this.label3.TabIndex = 928;
            this.label3.Text = "التفاصيــــــــل...";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.label4);
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-5, 24);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel2.TabIndex = 930;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(858, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(93, 14);
            this.label4.TabIndex = 928;
            this.label4.Text = "التفاصيــــــــل...";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.label6);
            this.flowLayoutPanel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-5, 17);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(956, 1);
            this.flowLayoutPanel3.TabIndex = 929;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(858, 0);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(93, 14);
            this.label6.TabIndex = 928;
            this.label6.Text = "التفاصيــــــــل...";
            // 
            // CHK2
            // 
            this.CHK2.AutoSize = true;
            this.CHK2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK2.ForeColor = System.Drawing.Color.Maroon;
            this.CHK2.Location = new System.Drawing.Point(20, 498);
            this.CHK2.Name = "CHK2";
            this.CHK2.Size = new System.Drawing.Size(333, 20);
            this.CHK2.TabIndex = 926;
            this.CHK2.Text = "يمتلك البحث عن جميع الحوالات الواردة( عند الدفع)";
            this.CHK2.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel31.Location = new System.Drawing.Point(0, 64);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(828, 1);
            this.flowLayoutPanel31.TabIndex = 931;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(-194, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel32.TabIndex = 630;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(-131, 10);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel33.TabIndex = 924;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel34.TabIndex = 630;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(-131, 18);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel35.TabIndex = 925;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel36.TabIndex = 630;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel37.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel37.TabIndex = 924;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel38.TabIndex = 630;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(81, 26);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel39.TabIndex = 931;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel40.TabIndex = 630;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel41.TabIndex = 924;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel42.TabIndex = 630;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel43.TabIndex = 925;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel44.TabIndex = 630;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel45.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel45.TabIndex = 924;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel46.TabIndex = 630;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(652, 40);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.TabIndex = 936;
            this.label9.Text = "التاريـخ:";
            // 
            // flowLayoutPanel74
            // 
            this.flowLayoutPanel74.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel74.Controls.Add(this.flowLayoutPanel75);
            this.flowLayoutPanel74.Controls.Add(this.flowLayoutPanel76);
            this.flowLayoutPanel74.Controls.Add(this.flowLayoutPanel78);
            this.flowLayoutPanel74.Controls.Add(this.flowLayoutPanel82);
            this.flowLayoutPanel74.Controls.Add(this.flowLayoutPanel90);
            this.flowLayoutPanel74.Controls.Add(this.flowLayoutPanel106);
            this.flowLayoutPanel74.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel74.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel74.Location = new System.Drawing.Point(-1, 548);
            this.flowLayoutPanel74.Name = "flowLayoutPanel74";
            this.flowLayoutPanel74.Size = new System.Drawing.Size(829, 1);
            this.flowLayoutPanel74.TabIndex = 982;
            // 
            // flowLayoutPanel75
            // 
            this.flowLayoutPanel75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel75.Location = new System.Drawing.Point(-195, 3);
            this.flowLayoutPanel75.Name = "flowLayoutPanel75";
            this.flowLayoutPanel75.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel75.TabIndex = 630;
            // 
            // flowLayoutPanel76
            // 
            this.flowLayoutPanel76.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel76.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel76.Controls.Add(this.flowLayoutPanel77);
            this.flowLayoutPanel76.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel76.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel76.Location = new System.Drawing.Point(-132, 10);
            this.flowLayoutPanel76.Name = "flowLayoutPanel76";
            this.flowLayoutPanel76.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel76.TabIndex = 924;
            // 
            // flowLayoutPanel77
            // 
            this.flowLayoutPanel77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel77.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel77.Name = "flowLayoutPanel77";
            this.flowLayoutPanel77.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel77.TabIndex = 630;
            // 
            // flowLayoutPanel78
            // 
            this.flowLayoutPanel78.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel78.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel78.Controls.Add(this.flowLayoutPanel79);
            this.flowLayoutPanel78.Controls.Add(this.flowLayoutPanel80);
            this.flowLayoutPanel78.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel78.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel78.Location = new System.Drawing.Point(-132, 18);
            this.flowLayoutPanel78.Name = "flowLayoutPanel78";
            this.flowLayoutPanel78.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel78.TabIndex = 925;
            // 
            // flowLayoutPanel79
            // 
            this.flowLayoutPanel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel79.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel79.Name = "flowLayoutPanel79";
            this.flowLayoutPanel79.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel79.TabIndex = 630;
            // 
            // flowLayoutPanel80
            // 
            this.flowLayoutPanel80.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel80.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel80.Controls.Add(this.flowLayoutPanel81);
            this.flowLayoutPanel80.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel80.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel80.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel80.Name = "flowLayoutPanel80";
            this.flowLayoutPanel80.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel80.TabIndex = 924;
            // 
            // flowLayoutPanel81
            // 
            this.flowLayoutPanel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel81.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel81.Name = "flowLayoutPanel81";
            this.flowLayoutPanel81.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel81.TabIndex = 630;
            // 
            // flowLayoutPanel82
            // 
            this.flowLayoutPanel82.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel82.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel82.Controls.Add(this.flowLayoutPanel83);
            this.flowLayoutPanel82.Controls.Add(this.flowLayoutPanel84);
            this.flowLayoutPanel82.Controls.Add(this.flowLayoutPanel86);
            this.flowLayoutPanel82.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel82.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel82.Location = new System.Drawing.Point(80, 26);
            this.flowLayoutPanel82.Name = "flowLayoutPanel82";
            this.flowLayoutPanel82.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel82.TabIndex = 931;
            // 
            // flowLayoutPanel83
            // 
            this.flowLayoutPanel83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel83.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel83.Name = "flowLayoutPanel83";
            this.flowLayoutPanel83.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel83.TabIndex = 630;
            // 
            // flowLayoutPanel84
            // 
            this.flowLayoutPanel84.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel84.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel84.Controls.Add(this.flowLayoutPanel85);
            this.flowLayoutPanel84.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel84.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel84.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel84.Name = "flowLayoutPanel84";
            this.flowLayoutPanel84.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel84.TabIndex = 924;
            // 
            // flowLayoutPanel85
            // 
            this.flowLayoutPanel85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel85.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel85.Name = "flowLayoutPanel85";
            this.flowLayoutPanel85.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel85.TabIndex = 630;
            // 
            // flowLayoutPanel86
            // 
            this.flowLayoutPanel86.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel86.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel86.Controls.Add(this.flowLayoutPanel87);
            this.flowLayoutPanel86.Controls.Add(this.flowLayoutPanel88);
            this.flowLayoutPanel86.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel86.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel86.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel86.Name = "flowLayoutPanel86";
            this.flowLayoutPanel86.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel86.TabIndex = 925;
            // 
            // flowLayoutPanel87
            // 
            this.flowLayoutPanel87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel87.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel87.Name = "flowLayoutPanel87";
            this.flowLayoutPanel87.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel87.TabIndex = 630;
            // 
            // flowLayoutPanel88
            // 
            this.flowLayoutPanel88.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel88.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel88.Controls.Add(this.flowLayoutPanel89);
            this.flowLayoutPanel88.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel88.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel88.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel88.Name = "flowLayoutPanel88";
            this.flowLayoutPanel88.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel88.TabIndex = 924;
            // 
            // flowLayoutPanel89
            // 
            this.flowLayoutPanel89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel89.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel89.Name = "flowLayoutPanel89";
            this.flowLayoutPanel89.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel89.TabIndex = 630;
            // 
            // flowLayoutPanel90
            // 
            this.flowLayoutPanel90.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel90.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel90.Controls.Add(this.flowLayoutPanel91);
            this.flowLayoutPanel90.Controls.Add(this.flowLayoutPanel92);
            this.flowLayoutPanel90.Controls.Add(this.flowLayoutPanel94);
            this.flowLayoutPanel90.Controls.Add(this.flowLayoutPanel98);
            this.flowLayoutPanel90.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel90.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel90.Location = new System.Drawing.Point(26, 34);
            this.flowLayoutPanel90.Name = "flowLayoutPanel90";
            this.flowLayoutPanel90.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel90.TabIndex = 973;
            // 
            // flowLayoutPanel91
            // 
            this.flowLayoutPanel91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel91.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel91.Name = "flowLayoutPanel91";
            this.flowLayoutPanel91.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel91.TabIndex = 630;
            // 
            // flowLayoutPanel92
            // 
            this.flowLayoutPanel92.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel92.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel92.Controls.Add(this.flowLayoutPanel93);
            this.flowLayoutPanel92.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel92.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel92.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel92.Name = "flowLayoutPanel92";
            this.flowLayoutPanel92.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel92.TabIndex = 924;
            // 
            // flowLayoutPanel93
            // 
            this.flowLayoutPanel93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel93.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel93.Name = "flowLayoutPanel93";
            this.flowLayoutPanel93.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel93.TabIndex = 630;
            // 
            // flowLayoutPanel94
            // 
            this.flowLayoutPanel94.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel94.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel94.Controls.Add(this.flowLayoutPanel95);
            this.flowLayoutPanel94.Controls.Add(this.flowLayoutPanel96);
            this.flowLayoutPanel94.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel94.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel94.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel94.Name = "flowLayoutPanel94";
            this.flowLayoutPanel94.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel94.TabIndex = 925;
            // 
            // flowLayoutPanel95
            // 
            this.flowLayoutPanel95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel95.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel95.Name = "flowLayoutPanel95";
            this.flowLayoutPanel95.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel95.TabIndex = 630;
            // 
            // flowLayoutPanel96
            // 
            this.flowLayoutPanel96.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel96.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel96.Controls.Add(this.flowLayoutPanel97);
            this.flowLayoutPanel96.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel96.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel96.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel96.Name = "flowLayoutPanel96";
            this.flowLayoutPanel96.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel96.TabIndex = 924;
            // 
            // flowLayoutPanel97
            // 
            this.flowLayoutPanel97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel97.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel97.Name = "flowLayoutPanel97";
            this.flowLayoutPanel97.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel97.TabIndex = 630;
            // 
            // flowLayoutPanel98
            // 
            this.flowLayoutPanel98.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel98.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel98.Controls.Add(this.flowLayoutPanel99);
            this.flowLayoutPanel98.Controls.Add(this.flowLayoutPanel100);
            this.flowLayoutPanel98.Controls.Add(this.flowLayoutPanel102);
            this.flowLayoutPanel98.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel98.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel98.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel98.Name = "flowLayoutPanel98";
            this.flowLayoutPanel98.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel98.TabIndex = 931;
            // 
            // flowLayoutPanel99
            // 
            this.flowLayoutPanel99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel99.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel99.Name = "flowLayoutPanel99";
            this.flowLayoutPanel99.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel99.TabIndex = 630;
            // 
            // flowLayoutPanel100
            // 
            this.flowLayoutPanel100.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel100.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel100.Controls.Add(this.flowLayoutPanel101);
            this.flowLayoutPanel100.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel100.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel100.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel100.Name = "flowLayoutPanel100";
            this.flowLayoutPanel100.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel100.TabIndex = 924;
            // 
            // flowLayoutPanel101
            // 
            this.flowLayoutPanel101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel101.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel101.Name = "flowLayoutPanel101";
            this.flowLayoutPanel101.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel101.TabIndex = 630;
            // 
            // flowLayoutPanel102
            // 
            this.flowLayoutPanel102.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel102.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel102.Controls.Add(this.flowLayoutPanel103);
            this.flowLayoutPanel102.Controls.Add(this.flowLayoutPanel104);
            this.flowLayoutPanel102.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel102.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel102.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel102.Name = "flowLayoutPanel102";
            this.flowLayoutPanel102.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel102.TabIndex = 925;
            // 
            // flowLayoutPanel103
            // 
            this.flowLayoutPanel103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel103.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel103.Name = "flowLayoutPanel103";
            this.flowLayoutPanel103.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel103.TabIndex = 630;
            // 
            // flowLayoutPanel104
            // 
            this.flowLayoutPanel104.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel104.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel104.Controls.Add(this.flowLayoutPanel105);
            this.flowLayoutPanel104.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel104.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel104.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel104.Name = "flowLayoutPanel104";
            this.flowLayoutPanel104.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel104.TabIndex = 924;
            // 
            // flowLayoutPanel105
            // 
            this.flowLayoutPanel105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel105.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel105.Name = "flowLayoutPanel105";
            this.flowLayoutPanel105.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel105.TabIndex = 630;
            // 
            // flowLayoutPanel106
            // 
            this.flowLayoutPanel106.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel106.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel107);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel108);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel110);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel114);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel122);
            this.flowLayoutPanel106.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel106.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel106.Location = new System.Drawing.Point(26, 42);
            this.flowLayoutPanel106.Name = "flowLayoutPanel106";
            this.flowLayoutPanel106.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel106.TabIndex = 974;
            // 
            // flowLayoutPanel107
            // 
            this.flowLayoutPanel107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel107.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel107.Name = "flowLayoutPanel107";
            this.flowLayoutPanel107.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel107.TabIndex = 630;
            // 
            // flowLayoutPanel108
            // 
            this.flowLayoutPanel108.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel108.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel108.Controls.Add(this.flowLayoutPanel109);
            this.flowLayoutPanel108.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel108.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel108.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel108.Name = "flowLayoutPanel108";
            this.flowLayoutPanel108.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel108.TabIndex = 924;
            // 
            // flowLayoutPanel109
            // 
            this.flowLayoutPanel109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel109.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel109.Name = "flowLayoutPanel109";
            this.flowLayoutPanel109.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel109.TabIndex = 630;
            // 
            // flowLayoutPanel110
            // 
            this.flowLayoutPanel110.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel110.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel110.Controls.Add(this.flowLayoutPanel111);
            this.flowLayoutPanel110.Controls.Add(this.flowLayoutPanel112);
            this.flowLayoutPanel110.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel110.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel110.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel110.Name = "flowLayoutPanel110";
            this.flowLayoutPanel110.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel110.TabIndex = 925;
            // 
            // flowLayoutPanel111
            // 
            this.flowLayoutPanel111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel111.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel111.Name = "flowLayoutPanel111";
            this.flowLayoutPanel111.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel111.TabIndex = 630;
            // 
            // flowLayoutPanel112
            // 
            this.flowLayoutPanel112.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel112.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel112.Controls.Add(this.flowLayoutPanel113);
            this.flowLayoutPanel112.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel112.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel112.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel112.Name = "flowLayoutPanel112";
            this.flowLayoutPanel112.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel112.TabIndex = 924;
            // 
            // flowLayoutPanel113
            // 
            this.flowLayoutPanel113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel113.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel113.Name = "flowLayoutPanel113";
            this.flowLayoutPanel113.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel113.TabIndex = 630;
            // 
            // flowLayoutPanel114
            // 
            this.flowLayoutPanel114.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel114.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel114.Controls.Add(this.flowLayoutPanel115);
            this.flowLayoutPanel114.Controls.Add(this.flowLayoutPanel116);
            this.flowLayoutPanel114.Controls.Add(this.flowLayoutPanel118);
            this.flowLayoutPanel114.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel114.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel114.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel114.Name = "flowLayoutPanel114";
            this.flowLayoutPanel114.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel114.TabIndex = 931;
            // 
            // flowLayoutPanel115
            // 
            this.flowLayoutPanel115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel115.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel115.Name = "flowLayoutPanel115";
            this.flowLayoutPanel115.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel115.TabIndex = 630;
            // 
            // flowLayoutPanel116
            // 
            this.flowLayoutPanel116.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel116.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel116.Controls.Add(this.flowLayoutPanel117);
            this.flowLayoutPanel116.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel116.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel116.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel116.Name = "flowLayoutPanel116";
            this.flowLayoutPanel116.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel116.TabIndex = 924;
            // 
            // flowLayoutPanel117
            // 
            this.flowLayoutPanel117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel117.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel117.Name = "flowLayoutPanel117";
            this.flowLayoutPanel117.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel117.TabIndex = 630;
            // 
            // flowLayoutPanel118
            // 
            this.flowLayoutPanel118.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel118.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel118.Controls.Add(this.flowLayoutPanel119);
            this.flowLayoutPanel118.Controls.Add(this.flowLayoutPanel120);
            this.flowLayoutPanel118.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel118.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel118.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel118.Name = "flowLayoutPanel118";
            this.flowLayoutPanel118.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel118.TabIndex = 925;
            // 
            // flowLayoutPanel119
            // 
            this.flowLayoutPanel119.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel119.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel119.Name = "flowLayoutPanel119";
            this.flowLayoutPanel119.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel119.TabIndex = 630;
            // 
            // flowLayoutPanel120
            // 
            this.flowLayoutPanel120.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel120.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel120.Controls.Add(this.flowLayoutPanel121);
            this.flowLayoutPanel120.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel120.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel120.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel120.Name = "flowLayoutPanel120";
            this.flowLayoutPanel120.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel120.TabIndex = 924;
            // 
            // flowLayoutPanel121
            // 
            this.flowLayoutPanel121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel121.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel121.Name = "flowLayoutPanel121";
            this.flowLayoutPanel121.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel121.TabIndex = 630;
            // 
            // flowLayoutPanel122
            // 
            this.flowLayoutPanel122.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel122.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel122.Controls.Add(this.flowLayoutPanel123);
            this.flowLayoutPanel122.Controls.Add(this.flowLayoutPanel124);
            this.flowLayoutPanel122.Controls.Add(this.flowLayoutPanel126);
            this.flowLayoutPanel122.Controls.Add(this.flowLayoutPanel130);
            this.flowLayoutPanel122.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel122.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel122.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel122.Name = "flowLayoutPanel122";
            this.flowLayoutPanel122.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel122.TabIndex = 973;
            // 
            // flowLayoutPanel123
            // 
            this.flowLayoutPanel123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel123.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel123.Name = "flowLayoutPanel123";
            this.flowLayoutPanel123.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel123.TabIndex = 630;
            // 
            // flowLayoutPanel124
            // 
            this.flowLayoutPanel124.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel124.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel124.Controls.Add(this.flowLayoutPanel125);
            this.flowLayoutPanel124.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel124.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel124.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel124.Name = "flowLayoutPanel124";
            this.flowLayoutPanel124.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel124.TabIndex = 924;
            // 
            // flowLayoutPanel125
            // 
            this.flowLayoutPanel125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel125.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel125.Name = "flowLayoutPanel125";
            this.flowLayoutPanel125.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel125.TabIndex = 630;
            // 
            // flowLayoutPanel126
            // 
            this.flowLayoutPanel126.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel126.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel126.Controls.Add(this.flowLayoutPanel127);
            this.flowLayoutPanel126.Controls.Add(this.flowLayoutPanel128);
            this.flowLayoutPanel126.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel126.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel126.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel126.Name = "flowLayoutPanel126";
            this.flowLayoutPanel126.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel126.TabIndex = 925;
            // 
            // flowLayoutPanel127
            // 
            this.flowLayoutPanel127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel127.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel127.Name = "flowLayoutPanel127";
            this.flowLayoutPanel127.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel127.TabIndex = 630;
            // 
            // flowLayoutPanel128
            // 
            this.flowLayoutPanel128.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel128.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel128.Controls.Add(this.flowLayoutPanel129);
            this.flowLayoutPanel128.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel128.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel128.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel128.Name = "flowLayoutPanel128";
            this.flowLayoutPanel128.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel128.TabIndex = 924;
            // 
            // flowLayoutPanel129
            // 
            this.flowLayoutPanel129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel129.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel129.Name = "flowLayoutPanel129";
            this.flowLayoutPanel129.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel129.TabIndex = 630;
            // 
            // flowLayoutPanel130
            // 
            this.flowLayoutPanel130.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel130.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel131);
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel132);
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel134);
            this.flowLayoutPanel130.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel130.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel130.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel130.Name = "flowLayoutPanel130";
            this.flowLayoutPanel130.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel130.TabIndex = 931;
            // 
            // flowLayoutPanel131
            // 
            this.flowLayoutPanel131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel131.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel131.Name = "flowLayoutPanel131";
            this.flowLayoutPanel131.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel131.TabIndex = 630;
            // 
            // flowLayoutPanel132
            // 
            this.flowLayoutPanel132.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel132.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel132.Controls.Add(this.flowLayoutPanel133);
            this.flowLayoutPanel132.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel132.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel132.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel132.Name = "flowLayoutPanel132";
            this.flowLayoutPanel132.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel132.TabIndex = 924;
            // 
            // flowLayoutPanel133
            // 
            this.flowLayoutPanel133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel133.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel133.Name = "flowLayoutPanel133";
            this.flowLayoutPanel133.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel133.TabIndex = 630;
            // 
            // flowLayoutPanel134
            // 
            this.flowLayoutPanel134.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel134.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel134.Controls.Add(this.flowLayoutPanel135);
            this.flowLayoutPanel134.Controls.Add(this.flowLayoutPanel136);
            this.flowLayoutPanel134.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel134.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel134.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel134.Name = "flowLayoutPanel134";
            this.flowLayoutPanel134.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel134.TabIndex = 925;
            // 
            // flowLayoutPanel135
            // 
            this.flowLayoutPanel135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel135.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel135.Name = "flowLayoutPanel135";
            this.flowLayoutPanel135.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel135.TabIndex = 630;
            // 
            // flowLayoutPanel136
            // 
            this.flowLayoutPanel136.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel136.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel136.Controls.Add(this.flowLayoutPanel137);
            this.flowLayoutPanel136.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel136.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel136.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel136.Name = "flowLayoutPanel136";
            this.flowLayoutPanel136.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel136.TabIndex = 924;
            // 
            // flowLayoutPanel137
            // 
            this.flowLayoutPanel137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel137.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel137.Name = "flowLayoutPanel137";
            this.flowLayoutPanel137.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel137.TabIndex = 630;
            // 
            // CHK1
            // 
            this.CHK1.AutoSize = true;
            this.CHK1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK1.ForeColor = System.Drawing.Color.Maroon;
            this.CHK1.Location = new System.Drawing.Point(20, 473);
            this.CHK1.Name = "CHK1";
            this.CHK1.Size = new System.Drawing.Size(191, 20);
            this.CHK1.TabIndex = 995;
            this.CHK1.Text = "يمتلك خاصية تأكيد الحوالات";
            this.CHK1.UseVisualStyleBackColor = true;
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.label7,
            this.cbo_term,
            this.label8,
            this.toolStripSeparator1,
            this.Txt_Sub_Name,
            this.toolStripSeparator2,
            this.SearchBtn});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(828, 30);
            this.BND.TabIndex = 996;
            this.BND.Text = "bindingNavigator1";
            // 
            // label7
            // 
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 27);
            this.label7.Text = "الثانــــوي :";
            // 
            // cbo_term
            // 
            this.cbo_term.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cbo_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_term.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cbo_term.Name = "cbo_term";
            this.cbo_term.Size = new System.Drawing.Size(250, 30);
            this.cbo_term.SelectedIndexChanged += new System.EventHandler(this.cbo_term_SelectedIndexChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // Txt_Sub_Name
            // 
            this.Txt_Sub_Name.Name = "Txt_Sub_Name";
            this.Txt_Sub_Name.Size = new System.Drawing.Size(250, 30);
            this.Txt_Sub_Name.TextChanged += new System.EventHandler(this.Txt_Sub_Name_TextChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(5, 447);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(120, 14);
            this.label2.TabIndex = 997;
            this.label2.Text = "معلومات الوكيل .....";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-90, 31);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1008, 1);
            this.flowLayoutPanel9.TabIndex = 998;
            // 
            // CHK4
            // 
            this.CHK4.AutoSize = true;
            this.CHK4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK4.ForeColor = System.Drawing.Color.Maroon;
            this.CHK4.Location = new System.Drawing.Point(586, 499);
            this.CHK4.Name = "CHK4";
            this.CHK4.Size = new System.Drawing.Size(230, 20);
            this.CHK4.TabIndex = 1000;
            this.CHK4.Text = "انطلاق الحوالـة بعملـة المقاصــــة";
            this.CHK4.ThreeState = true;
            this.CHK4.UseVisualStyleBackColor = true;
            // 
            // CHK5
            // 
            this.CHK5.AutoSize = true;
            this.CHK5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK5.ForeColor = System.Drawing.Color.Maroon;
            this.CHK5.Location = new System.Drawing.Point(353, 473);
            this.CHK5.Name = "CHK5";
            this.CHK5.Size = new System.Drawing.Size(158, 20);
            this.CHK5.TabIndex = 1001;
            this.CHK5.Text = "أعتماد نسب العمولات";
            this.CHK5.UseVisualStyleBackColor = true;
            this.CHK5.CheckedChanged += new System.EventHandler(this.CHK5_CheckedChanged);
            // 
            // Chk_Cust
            // 
            this.Chk_Cust.AutoSize = true;
            this.Chk_Cust.Enabled = false;
            this.Chk_Cust.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Cust.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Cust.Location = new System.Drawing.Point(353, 498);
            this.Chk_Cust.Name = "Chk_Cust";
            this.Chk_Cust.Size = new System.Drawing.Size(215, 20);
            this.Chk_Cust.TabIndex = 1002;
            this.Chk_Cust.Text = "اضهار العمولات لدى وكلاء الويب";
            this.Chk_Cust.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Image = ((System.Drawing.Image)(resources.GetObject("label8.Image")));
            this.label8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 27);
            this.label8.Tag = "8";
            this.label8.Text = "بـحــث الكل:";
            this.label8.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.ForeColor = System.Drawing.Color.Black;
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(61, 27);
            this.SearchBtn.Tag = "8";
            this.SearchBtn.Text = "بـحــث";
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(577, 475);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 16);
            this.label10.TabIndex = 1004;
            this.label10.Text = "مسارالحوالة:";
            // 
            // Cbo_loc_int_case
            // 
            this.Cbo_loc_int_case.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_loc_int_case.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_loc_int_case.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_loc_int_case.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_loc_int_case.FormattingEnabled = true;
            this.Cbo_loc_int_case.Items.AddRange(new object[] {
            "جميع الحالات",
            "داخلي فقط",
            "خارجي فقط"});
            this.Cbo_loc_int_case.Location = new System.Drawing.Point(668, 471);
            this.Cbo_loc_int_case.Name = "Cbo_loc_int_case";
            this.Cbo_loc_int_case.Size = new System.Drawing.Size(152, 24);
            this.Cbo_loc_int_case.TabIndex = 1003;
            // 
            // chk_trans
            // 
            this.chk_trans.AutoSize = true;
            this.chk_trans.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_trans.ForeColor = System.Drawing.Color.Maroon;
            this.chk_trans.Location = new System.Drawing.Point(20, 522);
            this.chk_trans.Name = "chk_trans";
            this.chk_trans.Size = new System.Drawing.Size(292, 20);
            this.chk_trans.TabIndex = 1005;
            this.chk_trans.Text = "تعديــــل مســـار الحوالــــــة ومدينتـهــــــــــا";
            this.chk_trans.UseVisualStyleBackColor = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Sub_Cust_ID";
            this.Column1.HeaderText = "رمز الوكيل";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 60;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "ASub_CustName";
            this.Column2.HeaderText = "الاســــــــــــــــم";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 105;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "Acc_AName";
            this.Column3.HeaderText = "اسم الحســــاب";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 94;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "ver_check_Aname";
            this.Column5.HeaderText = "يمتلك خاصية التأكيد";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 118;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "Chk_Search_Rem_Aname";
            this.Column10.HeaderText = "يمتلك خاصية البحث عن الحوالات";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 132;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "CHK_Oper_rem_Aname";
            this.Column18.HeaderText = "يمتلك خاصية المقاصة";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 122;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "comm_rem_flag_Aname";
            this.Column19.HeaderText = "أعتماد نسب العمولات";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 122;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Loc_int_case_Aname";
            this.Column17.HeaderText = "مسار الحوالة";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "path_tcity_chk_Aname";
            this.Column20.HeaderText = "تعديل مسار الحوالة ومدينتها";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "A_Address";
            this.Column13.HeaderText = "العنــــوان";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 77;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "Cit_AName";
            this.Column11.HeaderText = "المدينـــة";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 72;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "Con_AName";
            this.Column12.HeaderText = "البلــــد";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 63;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "Phone";
            this.Column14.HeaderText = "رقـــــم الهـــاتف";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 99;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column15.DataPropertyName = "Email";
            this.Column15.HeaderText = "البريـــد الالكتروني";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 111;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "Nat_Aname";
            this.Column16.HeaderText = "الجنسيـــة";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 78;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "Fmd_AName";
            this.Column4.HeaderText = "نــــوع الوثيقة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 63;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "frm_doc_no";
            this.Column6.HeaderText = "رقــــم الوثيقة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 87;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "frm_doc_Da";
            this.Column7.HeaderText = "تاريخ الاصدار";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 90;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "doc_eda";
            this.Column8.HeaderText = "تاريخ الانتهاء";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 90;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "frm_doc_Is";
            this.Column9.HeaderText = "جهــة الاصـــدار";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 96;
            // 
            // Setting_Customer_Online
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 584);
            this.Controls.Add(this.chk_trans);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Cbo_loc_int_case);
            this.Controls.Add(this.Chk_Cust);
            this.Controls.Add(this.CHK5);
            this.Controls.Add(this.CHK4);
            this.Controls.Add(this.Grd_Cust_Online);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.CHK1);
            this.Controls.Add(this.flowLayoutPanel74);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.flowLayoutPanel31);
            this.Controls.Add(this.CHK2);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Btn_Exist);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Setting_Customer_Online";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "542";
            this.Text = "Customer_Online_Add_Upd";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Setting_Customer_Online_FormClosed);
            this.Load += new System.EventHandler(this.Setting_Customer_Online_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Online)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel33.ResumeLayout(false);
            this.flowLayoutPanel35.ResumeLayout(false);
            this.flowLayoutPanel37.ResumeLayout(false);
            this.flowLayoutPanel39.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel43.ResumeLayout(false);
            this.flowLayoutPanel45.ResumeLayout(false);
            this.flowLayoutPanel74.ResumeLayout(false);
            this.flowLayoutPanel76.ResumeLayout(false);
            this.flowLayoutPanel78.ResumeLayout(false);
            this.flowLayoutPanel80.ResumeLayout(false);
            this.flowLayoutPanel82.ResumeLayout(false);
            this.flowLayoutPanel84.ResumeLayout(false);
            this.flowLayoutPanel86.ResumeLayout(false);
            this.flowLayoutPanel88.ResumeLayout(false);
            this.flowLayoutPanel90.ResumeLayout(false);
            this.flowLayoutPanel92.ResumeLayout(false);
            this.flowLayoutPanel94.ResumeLayout(false);
            this.flowLayoutPanel96.ResumeLayout(false);
            this.flowLayoutPanel98.ResumeLayout(false);
            this.flowLayoutPanel100.ResumeLayout(false);
            this.flowLayoutPanel102.ResumeLayout(false);
            this.flowLayoutPanel104.ResumeLayout(false);
            this.flowLayoutPanel106.ResumeLayout(false);
            this.flowLayoutPanel108.ResumeLayout(false);
            this.flowLayoutPanel110.ResumeLayout(false);
            this.flowLayoutPanel112.ResumeLayout(false);
            this.flowLayoutPanel114.ResumeLayout(false);
            this.flowLayoutPanel116.ResumeLayout(false);
            this.flowLayoutPanel118.ResumeLayout(false);
            this.flowLayoutPanel120.ResumeLayout(false);
            this.flowLayoutPanel122.ResumeLayout(false);
            this.flowLayoutPanel124.ResumeLayout(false);
            this.flowLayoutPanel126.ResumeLayout(false);
            this.flowLayoutPanel128.ResumeLayout(false);
            this.flowLayoutPanel130.ResumeLayout(false);
            this.flowLayoutPanel132.ResumeLayout(false);
            this.flowLayoutPanel134.ResumeLayout(false);
            this.flowLayoutPanel136.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.DataGridView Grd_Cust_Online;
        private System.Windows.Forms.Button Btn_Exist;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.CheckBox CHK2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel74;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel75;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel76;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel77;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel78;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel79;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel80;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel81;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel82;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel83;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel84;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel85;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel86;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel87;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel88;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel89;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel90;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel91;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel92;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel93;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel94;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel95;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel96;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel97;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel98;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel99;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel100;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel101;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel102;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel103;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel104;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel105;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel106;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel107;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel108;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel109;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel110;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel111;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel112;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel113;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel114;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel115;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel116;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel117;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel118;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel119;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel120;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel121;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel122;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel123;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel124;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel125;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel126;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel127;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel128;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel129;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel130;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel131;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel132;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel133;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel134;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel135;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel136;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel137;
        private System.Windows.Forms.CheckBox CHK1;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripLabel label7;
        private System.Windows.Forms.ToolStripComboBox cbo_term;
        private System.Windows.Forms.ToolStripTextBox Txt_Sub_Name;
        private System.Windows.Forms.ToolStripButton SearchBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.ToolStripButton label8;
        private System.Windows.Forms.CheckBox CHK4;
        private System.Windows.Forms.CheckBox CHK5;
        private System.Windows.Forms.CheckBox Chk_Cust;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Cbo_loc_int_case;
        private System.Windows.Forms.CheckBox chk_trans;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
    }
}