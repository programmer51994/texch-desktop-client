﻿namespace Integration_Accounting_Sys
{
    partial class Agent_User_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel65 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel66 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt_Serch_sub_cust = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Grd_Login_Cust_Online = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Agent_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Sub_Cust_User = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel58 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel59 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel60 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel61 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel62 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel63 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel64 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel67 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel68 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel69 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel70 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel71 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel72 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel73 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel74 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel75 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel76 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel77 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel78 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel79 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel80 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel81 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel82 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel83 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel84 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel85 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel86 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel87 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel88 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel89 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel90 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel91 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel92 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel93 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel94 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel95 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel96 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel97 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel98 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel99 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel100 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel101 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel102 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel103 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel104 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel105 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel106 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel107 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel108 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel109 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel110 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel111 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel112 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.cbo_term = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Cbo_CityUser_Id = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel33.SuspendLayout();
            this.flowLayoutPanel35.SuspendLayout();
            this.flowLayoutPanel37.SuspendLayout();
            this.flowLayoutPanel39.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel43.SuspendLayout();
            this.flowLayoutPanel45.SuspendLayout();
            this.flowLayoutPanel19.SuspendLayout();
            this.flowLayoutPanel21.SuspendLayout();
            this.flowLayoutPanel23.SuspendLayout();
            this.flowLayoutPanel25.SuspendLayout();
            this.flowLayoutPanel27.SuspendLayout();
            this.flowLayoutPanel29.SuspendLayout();
            this.flowLayoutPanel47.SuspendLayout();
            this.flowLayoutPanel65.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Login_Cust_Online)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Agent_Sub_Cust)).BeginInit();
            this.flowLayoutPanel49.SuspendLayout();
            this.flowLayoutPanel51.SuspendLayout();
            this.flowLayoutPanel53.SuspendLayout();
            this.flowLayoutPanel55.SuspendLayout();
            this.flowLayoutPanel57.SuspendLayout();
            this.flowLayoutPanel59.SuspendLayout();
            this.flowLayoutPanel61.SuspendLayout();
            this.flowLayoutPanel63.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel15.SuspendLayout();
            this.flowLayoutPanel17.SuspendLayout();
            this.flowLayoutPanel67.SuspendLayout();
            this.flowLayoutPanel69.SuspendLayout();
            this.flowLayoutPanel71.SuspendLayout();
            this.flowLayoutPanel73.SuspendLayout();
            this.flowLayoutPanel75.SuspendLayout();
            this.flowLayoutPanel77.SuspendLayout();
            this.flowLayoutPanel79.SuspendLayout();
            this.flowLayoutPanel81.SuspendLayout();
            this.flowLayoutPanel83.SuspendLayout();
            this.flowLayoutPanel85.SuspendLayout();
            this.flowLayoutPanel87.SuspendLayout();
            this.flowLayoutPanel89.SuspendLayout();
            this.flowLayoutPanel91.SuspendLayout();
            this.flowLayoutPanel93.SuspendLayout();
            this.flowLayoutPanel95.SuspendLayout();
            this.flowLayoutPanel97.SuspendLayout();
            this.flowLayoutPanel99.SuspendLayout();
            this.flowLayoutPanel101.SuspendLayout();
            this.flowLayoutPanel103.SuspendLayout();
            this.flowLayoutPanel105.SuspendLayout();
            this.flowLayoutPanel107.SuspendLayout();
            this.flowLayoutPanel109.SuspendLayout();
            this.flowLayoutPanel111.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(4, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(286, 23);
            this.TxtTerm_Name.TabIndex = 904;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(296, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 909;
            this.label1.Text = "المستخـــــدم:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(386, 2);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(235, 23);
            this.TxtUser.TabIndex = 908;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(688, 5);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(48, 16);
            this.label7.TabIndex = 963;
            this.label7.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(742, 2);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(160, 23);
            this.TxtIn_Rec_Date.TabIndex = 962;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel31.Location = new System.Drawing.Point(-10, 28);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(1030, 1);
            this.flowLayoutPanel31.TabIndex = 964;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(8, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel32.TabIndex = 630;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(71, 10);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel33.TabIndex = 924;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel34.TabIndex = 630;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(71, 18);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel35.TabIndex = 925;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel36.TabIndex = 630;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel37.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel37.TabIndex = 924;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel38.TabIndex = 630;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(283, 26);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel39.TabIndex = 931;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel40.TabIndex = 630;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel41.TabIndex = 924;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel42.TabIndex = 630;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel43.TabIndex = 925;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel44.TabIndex = 630;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel45.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel45.TabIndex = 924;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel46.TabIndex = 630;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel20);
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel23);
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(1030, 2);
            this.flowLayoutPanel19.TabIndex = 933;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(8, 3);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel20.TabIndex = 630;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel22);
            this.flowLayoutPanel21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel21.Location = new System.Drawing.Point(71, 10);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel21.TabIndex = 924;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel22.TabIndex = 630;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel25);
            this.flowLayoutPanel23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel23.Location = new System.Drawing.Point(71, 18);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel23.TabIndex = 925;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel24.TabIndex = 630;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel25.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel25.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel25.TabIndex = 924;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel26.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel26.TabIndex = 630;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel28);
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(283, 26);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel27.TabIndex = 931;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel28.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel28.TabIndex = 630;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel29.Controls.Add(this.flowLayoutPanel30);
            this.flowLayoutPanel29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel29.TabIndex = 924;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel30.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel30.TabIndex = 630;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel47.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel47.Controls.Add(this.flowLayoutPanel65);
            this.flowLayoutPanel47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel47.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel47.TabIndex = 925;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel48.TabIndex = 630;
            // 
            // flowLayoutPanel65
            // 
            this.flowLayoutPanel65.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel65.Controls.Add(this.flowLayoutPanel66);
            this.flowLayoutPanel65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel65.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel65.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel65.Name = "flowLayoutPanel65";
            this.flowLayoutPanel65.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel65.TabIndex = 924;
            // 
            // flowLayoutPanel66
            // 
            this.flowLayoutPanel66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel66.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel66.Name = "flowLayoutPanel66";
            this.flowLayoutPanel66.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel66.TabIndex = 630;
            // 
            // txt_Serch_sub_cust
            // 
            this.txt_Serch_sub_cust.BackColor = System.Drawing.Color.White;
            this.txt_Serch_sub_cust.Location = new System.Drawing.Point(73, 60);
            this.txt_Serch_sub_cust.MaxLength = 49;
            this.txt_Serch_sub_cust.Name = "txt_Serch_sub_cust";
            this.txt_Serch_sub_cust.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_Serch_sub_cust.Size = new System.Drawing.Size(311, 20);
            this.txt_Serch_sub_cust.TabIndex = 966;
            this.txt_Serch_sub_cust.TextChanged += new System.EventHandler(this.txt_Serch_sub_cust_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(2, 37);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 965;
            this.label2.Text = " الثانـــوي :";
            // 
            // Grd_Login_Cust_Online
            // 
            this.Grd_Login_Cust_Online.AllowUserToAddRows = false;
            this.Grd_Login_Cust_Online.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Login_Cust_Online.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Login_Cust_Online.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Login_Cust_Online.ColumnHeadersHeight = 30;
            this.Grd_Login_Cust_Online.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column20,
            this.Column21,
            this.Column23,
            this.Column22,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column31});
            this.Grd_Login_Cust_Online.Location = new System.Drawing.Point(4, 305);
            this.Grd_Login_Cust_Online.Name = "Grd_Login_Cust_Online";
            this.Grd_Login_Cust_Online.RowHeadersWidth = 25;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Login_Cust_Online.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Login_Cust_Online.Size = new System.Drawing.Size(897, 172);
            this.Grd_Login_Cust_Online.TabIndex = 968;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Chk";
            this.Column1.FalseValue = "0";
            this.Column1.HeaderText = "تأشير ";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.Width = 55;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "USER_ID";
            this.Column20.HeaderText = "USER_ID";
            this.Column20.Name = "Column20";
            this.Column20.Visible = false;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "User_Name";
            this.Column21.HeaderText = "اســـم المستخدم";
            this.Column21.Name = "Column21";
            this.Column21.Width = 220;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "Full_name";
            this.Column23.HeaderText = "الاســـم الكامل";
            this.Column23.Name = "Column23";
            this.Column23.Width = 250;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "Pwd";
            this.Column22.HeaderText = "كلمة المرور";
            this.Column22.Name = "Column22";
            this.Column22.Visible = false;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "PHONE_NO";
            this.Column25.HeaderText = "رقـــم الهاتــف";
            this.Column25.Name = "Column25";
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "A_Address";
            this.Column26.HeaderText = "العنــــوان";
            this.Column26.Name = "Column26";
            this.Column26.Width = 245;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "USER_State";
            this.Column27.HeaderText = "حالة المستخدم";
            this.Column27.Name = "Column27";
            this.Column27.Visible = false;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "STOP_Date";
            this.Column28.HeaderText = "تاريخ التوقيف";
            this.Column28.Name = "Column28";
            this.Column28.Visible = false;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "PEXP_Date";
            this.Column29.HeaderText = "تاريخ الانتهاء";
            this.Column29.Name = "Column29";
            this.Column29.Visible = false;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "CS_FLAG";
            this.Column30.HeaderText = "CS_FLAG";
            this.Column30.Name = "Column30";
            this.Column30.Visible = false;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "CU_USER_ID";
            this.Column31.HeaderText = "CU_USER_ID";
            this.Column31.Name = "Column31";
            this.Column31.Visible = false;
            // 
            // Grd_Agent_Sub_Cust
            // 
            this.Grd_Agent_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Agent_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Agent_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Agent_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Agent_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Agent_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Agent_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column24,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column19,
            this.Column12,
            this.Column9,
            this.Column11,
            this.Column10,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18});
            this.Grd_Agent_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Agent_Sub_Cust.Location = new System.Drawing.Point(4, 82);
            this.Grd_Agent_Sub_Cust.Name = "Grd_Agent_Sub_Cust";
            this.Grd_Agent_Sub_Cust.ReadOnly = true;
            this.Grd_Agent_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Agent_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Agent_Sub_Cust.Size = new System.Drawing.Size(897, 174);
            this.Grd_Agent_Sub_Cust.TabIndex = 969;
            this.Grd_Agent_Sub_Cust.SelectionChanged += new System.EventHandler(this.Grd_Agent_Sub_Cust_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Rec_cust_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Rec_cust_id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Cust_Online_Main_Id";
            this.Column2.HeaderText = "cust_online_main_id";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Sub_Cust_ID";
            this.Column3.HeaderText = "sub_cust_id";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "ASub_CustName";
            this.Column4.HeaderText = "الاســــم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 250;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "Cust_Code";
            this.Column24.HeaderText = "رمـــز الحســاب";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "City_Id";
            this.Column5.HeaderText = "city_id";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "City_Aname";
            this.Column6.HeaderText = "المديـــنة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Con_Id";
            this.Column7.HeaderText = "con_id";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Con_AName";
            this.Column8.HeaderText = "البلد";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Nat_AName";
            this.Column19.HeaderText = "الجنســـية";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "FRM_DOC_ID";
            this.Column12.HeaderText = "frm_doc_id";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Visible = false;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "A_ADDRESS";
            this.Column9.HeaderText = "العنـــــوان";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 245;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "EMAIL";
            this.Column11.HeaderText = "البريد الالكتروني";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "PHONE";
            this.Column10.HeaderText = "الهاتـــف";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Fmd_AName";
            this.Column13.HeaderText = "نــوع الوثيقة";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "FRM_DOC_NO";
            this.Column14.HeaderText = "رقـــم الوثيقة";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "FRM_DOC_DA";
            this.Column15.HeaderText = "تاريـــخ الوثيقة";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "DOC_EDA";
            this.Column16.HeaderText = "تاريخ انتهائها";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "FRM_DOC_IS";
            this.Column17.HeaderText = "جهـــة الاصدار";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "NAT_ID";
            this.Column18.HeaderText = "nat_id";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Visible = false;
            // 
            // Txt_Sub_Cust_User
            // 
            this.Txt_Sub_Cust_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sub_Cust_User.Location = new System.Drawing.Point(96, 278);
            this.Txt_Sub_Cust_User.Name = "Txt_Sub_Cust_User";
            this.Txt_Sub_Cust_User.Size = new System.Drawing.Size(313, 23);
            this.Txt_Sub_Cust_User.TabIndex = 971;
            this.Txt_Sub_Cust_User.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_User_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(5, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 16);
            this.label4.TabIndex = 970;
            this.label4.Text = "اســـم المستخدم :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(0, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 16);
            this.label8.TabIndex = 973;
            this.label8.Text = "المستخدمــــــــــون ....";
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel17);
            this.flowLayoutPanel49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(111, 267);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(798, 1);
            this.flowLayoutPanel49.TabIndex = 972;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel50.TabIndex = 630;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel51.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel51.TabIndex = 924;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel52.TabIndex = 630;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel53.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel53.TabIndex = 925;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel54.TabIndex = 630;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel55.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel55.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel55.TabIndex = 924;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel56.TabIndex = 630;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel58);
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel59);
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel61);
            this.flowLayoutPanel57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel57.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel57.TabIndex = 931;
            // 
            // flowLayoutPanel58
            // 
            this.flowLayoutPanel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel58.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel58.Name = "flowLayoutPanel58";
            this.flowLayoutPanel58.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel58.TabIndex = 630;
            // 
            // flowLayoutPanel59
            // 
            this.flowLayoutPanel59.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel59.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel59.Controls.Add(this.flowLayoutPanel60);
            this.flowLayoutPanel59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel59.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel59.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel59.Name = "flowLayoutPanel59";
            this.flowLayoutPanel59.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel59.TabIndex = 924;
            // 
            // flowLayoutPanel60
            // 
            this.flowLayoutPanel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel60.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel60.Name = "flowLayoutPanel60";
            this.flowLayoutPanel60.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel60.TabIndex = 630;
            // 
            // flowLayoutPanel61
            // 
            this.flowLayoutPanel61.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel62);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel63);
            this.flowLayoutPanel61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel61.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel61.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel61.Name = "flowLayoutPanel61";
            this.flowLayoutPanel61.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel61.TabIndex = 925;
            // 
            // flowLayoutPanel62
            // 
            this.flowLayoutPanel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel62.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel62.Name = "flowLayoutPanel62";
            this.flowLayoutPanel62.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel62.TabIndex = 630;
            // 
            // flowLayoutPanel63
            // 
            this.flowLayoutPanel63.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel63.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel63.Controls.Add(this.flowLayoutPanel64);
            this.flowLayoutPanel63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel63.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel63.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel63.Name = "flowLayoutPanel63";
            this.flowLayoutPanel63.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel63.TabIndex = 924;
            // 
            // flowLayoutPanel64
            // 
            this.flowLayoutPanel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel64.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel64.Name = "flowLayoutPanel64";
            this.flowLayoutPanel64.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel64.TabIndex = 630;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel1.TabIndex = 973;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel2.TabIndex = 630;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel3.TabIndex = 924;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel5.TabIndex = 925;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel6.TabIndex = 630;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel7.TabIndex = 924;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel8.TabIndex = 630;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel9.TabIndex = 931;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel11.TabIndex = 924;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel12.TabIndex = 630;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel13.TabIndex = 925;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel14.TabIndex = 630;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel15.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel15.TabIndex = 924;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel16.TabIndex = 630;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel18);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel67);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel69);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel73);
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel81);
            this.flowLayoutPanel17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(-3, 42);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel17.TabIndex = 974;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel18.TabIndex = 630;
            // 
            // flowLayoutPanel67
            // 
            this.flowLayoutPanel67.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel67.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel67.Controls.Add(this.flowLayoutPanel68);
            this.flowLayoutPanel67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel67.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel67.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel67.Name = "flowLayoutPanel67";
            this.flowLayoutPanel67.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel67.TabIndex = 924;
            // 
            // flowLayoutPanel68
            // 
            this.flowLayoutPanel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel68.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel68.Name = "flowLayoutPanel68";
            this.flowLayoutPanel68.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel68.TabIndex = 630;
            // 
            // flowLayoutPanel69
            // 
            this.flowLayoutPanel69.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel69.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel69.Controls.Add(this.flowLayoutPanel70);
            this.flowLayoutPanel69.Controls.Add(this.flowLayoutPanel71);
            this.flowLayoutPanel69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel69.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel69.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel69.Name = "flowLayoutPanel69";
            this.flowLayoutPanel69.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel69.TabIndex = 925;
            // 
            // flowLayoutPanel70
            // 
            this.flowLayoutPanel70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel70.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel70.Name = "flowLayoutPanel70";
            this.flowLayoutPanel70.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel70.TabIndex = 630;
            // 
            // flowLayoutPanel71
            // 
            this.flowLayoutPanel71.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel71.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel71.Controls.Add(this.flowLayoutPanel72);
            this.flowLayoutPanel71.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel71.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel71.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel71.Name = "flowLayoutPanel71";
            this.flowLayoutPanel71.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel71.TabIndex = 924;
            // 
            // flowLayoutPanel72
            // 
            this.flowLayoutPanel72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel72.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel72.Name = "flowLayoutPanel72";
            this.flowLayoutPanel72.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel72.TabIndex = 630;
            // 
            // flowLayoutPanel73
            // 
            this.flowLayoutPanel73.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel73.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel73.Controls.Add(this.flowLayoutPanel74);
            this.flowLayoutPanel73.Controls.Add(this.flowLayoutPanel75);
            this.flowLayoutPanel73.Controls.Add(this.flowLayoutPanel77);
            this.flowLayoutPanel73.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel73.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel73.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel73.Name = "flowLayoutPanel73";
            this.flowLayoutPanel73.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel73.TabIndex = 931;
            // 
            // flowLayoutPanel74
            // 
            this.flowLayoutPanel74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel74.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel74.Name = "flowLayoutPanel74";
            this.flowLayoutPanel74.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel74.TabIndex = 630;
            // 
            // flowLayoutPanel75
            // 
            this.flowLayoutPanel75.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel75.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel75.Controls.Add(this.flowLayoutPanel76);
            this.flowLayoutPanel75.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel75.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel75.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel75.Name = "flowLayoutPanel75";
            this.flowLayoutPanel75.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel75.TabIndex = 924;
            // 
            // flowLayoutPanel76
            // 
            this.flowLayoutPanel76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel76.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel76.Name = "flowLayoutPanel76";
            this.flowLayoutPanel76.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel76.TabIndex = 630;
            // 
            // flowLayoutPanel77
            // 
            this.flowLayoutPanel77.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel77.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel77.Controls.Add(this.flowLayoutPanel78);
            this.flowLayoutPanel77.Controls.Add(this.flowLayoutPanel79);
            this.flowLayoutPanel77.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel77.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel77.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel77.Name = "flowLayoutPanel77";
            this.flowLayoutPanel77.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel77.TabIndex = 925;
            // 
            // flowLayoutPanel78
            // 
            this.flowLayoutPanel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel78.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel78.Name = "flowLayoutPanel78";
            this.flowLayoutPanel78.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel78.TabIndex = 630;
            // 
            // flowLayoutPanel79
            // 
            this.flowLayoutPanel79.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel79.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel79.Controls.Add(this.flowLayoutPanel80);
            this.flowLayoutPanel79.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel79.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel79.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel79.Name = "flowLayoutPanel79";
            this.flowLayoutPanel79.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel79.TabIndex = 924;
            // 
            // flowLayoutPanel80
            // 
            this.flowLayoutPanel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel80.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel80.Name = "flowLayoutPanel80";
            this.flowLayoutPanel80.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel80.TabIndex = 630;
            // 
            // flowLayoutPanel81
            // 
            this.flowLayoutPanel81.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel81.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel82);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel83);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel85);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel89);
            this.flowLayoutPanel81.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel81.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel81.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel81.Name = "flowLayoutPanel81";
            this.flowLayoutPanel81.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel81.TabIndex = 973;
            // 
            // flowLayoutPanel82
            // 
            this.flowLayoutPanel82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel82.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel82.Name = "flowLayoutPanel82";
            this.flowLayoutPanel82.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel82.TabIndex = 630;
            // 
            // flowLayoutPanel83
            // 
            this.flowLayoutPanel83.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel83.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel83.Controls.Add(this.flowLayoutPanel84);
            this.flowLayoutPanel83.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel83.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel83.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel83.Name = "flowLayoutPanel83";
            this.flowLayoutPanel83.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel83.TabIndex = 924;
            // 
            // flowLayoutPanel84
            // 
            this.flowLayoutPanel84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel84.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel84.Name = "flowLayoutPanel84";
            this.flowLayoutPanel84.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel84.TabIndex = 630;
            // 
            // flowLayoutPanel85
            // 
            this.flowLayoutPanel85.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel85.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel85.Controls.Add(this.flowLayoutPanel86);
            this.flowLayoutPanel85.Controls.Add(this.flowLayoutPanel87);
            this.flowLayoutPanel85.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel85.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel85.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel85.Name = "flowLayoutPanel85";
            this.flowLayoutPanel85.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel85.TabIndex = 925;
            // 
            // flowLayoutPanel86
            // 
            this.flowLayoutPanel86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel86.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel86.Name = "flowLayoutPanel86";
            this.flowLayoutPanel86.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel86.TabIndex = 630;
            // 
            // flowLayoutPanel87
            // 
            this.flowLayoutPanel87.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel87.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel87.Controls.Add(this.flowLayoutPanel88);
            this.flowLayoutPanel87.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel87.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel87.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel87.Name = "flowLayoutPanel87";
            this.flowLayoutPanel87.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel87.TabIndex = 924;
            // 
            // flowLayoutPanel88
            // 
            this.flowLayoutPanel88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel88.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel88.Name = "flowLayoutPanel88";
            this.flowLayoutPanel88.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel88.TabIndex = 630;
            // 
            // flowLayoutPanel89
            // 
            this.flowLayoutPanel89.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel89.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel89.Controls.Add(this.flowLayoutPanel90);
            this.flowLayoutPanel89.Controls.Add(this.flowLayoutPanel91);
            this.flowLayoutPanel89.Controls.Add(this.flowLayoutPanel93);
            this.flowLayoutPanel89.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel89.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel89.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel89.Name = "flowLayoutPanel89";
            this.flowLayoutPanel89.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel89.TabIndex = 931;
            // 
            // flowLayoutPanel90
            // 
            this.flowLayoutPanel90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel90.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel90.Name = "flowLayoutPanel90";
            this.flowLayoutPanel90.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel90.TabIndex = 630;
            // 
            // flowLayoutPanel91
            // 
            this.flowLayoutPanel91.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel91.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel92);
            this.flowLayoutPanel91.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel91.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel91.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel91.Name = "flowLayoutPanel91";
            this.flowLayoutPanel91.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel91.TabIndex = 924;
            // 
            // flowLayoutPanel92
            // 
            this.flowLayoutPanel92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel92.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel92.Name = "flowLayoutPanel92";
            this.flowLayoutPanel92.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel92.TabIndex = 630;
            // 
            // flowLayoutPanel93
            // 
            this.flowLayoutPanel93.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel93.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel93.Controls.Add(this.flowLayoutPanel94);
            this.flowLayoutPanel93.Controls.Add(this.flowLayoutPanel95);
            this.flowLayoutPanel93.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel93.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel93.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel93.Name = "flowLayoutPanel93";
            this.flowLayoutPanel93.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel93.TabIndex = 925;
            // 
            // flowLayoutPanel94
            // 
            this.flowLayoutPanel94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel94.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel94.Name = "flowLayoutPanel94";
            this.flowLayoutPanel94.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel94.TabIndex = 630;
            // 
            // flowLayoutPanel95
            // 
            this.flowLayoutPanel95.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel95.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel95.Controls.Add(this.flowLayoutPanel96);
            this.flowLayoutPanel95.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel95.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel95.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel95.Name = "flowLayoutPanel95";
            this.flowLayoutPanel95.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel95.TabIndex = 924;
            // 
            // flowLayoutPanel96
            // 
            this.flowLayoutPanel96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel96.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel96.Name = "flowLayoutPanel96";
            this.flowLayoutPanel96.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel96.TabIndex = 630;
            // 
            // flowLayoutPanel97
            // 
            this.flowLayoutPanel97.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel97.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel98);
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel99);
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel101);
            this.flowLayoutPanel97.Controls.Add(this.flowLayoutPanel105);
            this.flowLayoutPanel97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel97.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel97.Location = new System.Drawing.Point(2, 482);
            this.flowLayoutPanel97.Name = "flowLayoutPanel97";
            this.flowLayoutPanel97.Size = new System.Drawing.Size(928, 1);
            this.flowLayoutPanel97.TabIndex = 974;
            // 
            // flowLayoutPanel98
            // 
            this.flowLayoutPanel98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel98.Location = new System.Drawing.Point(-94, 3);
            this.flowLayoutPanel98.Name = "flowLayoutPanel98";
            this.flowLayoutPanel98.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel98.TabIndex = 630;
            // 
            // flowLayoutPanel99
            // 
            this.flowLayoutPanel99.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel99.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel100);
            this.flowLayoutPanel99.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel99.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel99.Location = new System.Drawing.Point(-31, 10);
            this.flowLayoutPanel99.Name = "flowLayoutPanel99";
            this.flowLayoutPanel99.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel99.TabIndex = 924;
            // 
            // flowLayoutPanel100
            // 
            this.flowLayoutPanel100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel100.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel100.Name = "flowLayoutPanel100";
            this.flowLayoutPanel100.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel100.TabIndex = 630;
            // 
            // flowLayoutPanel101
            // 
            this.flowLayoutPanel101.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel101.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel102);
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel103);
            this.flowLayoutPanel101.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel101.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel101.Location = new System.Drawing.Point(-31, 18);
            this.flowLayoutPanel101.Name = "flowLayoutPanel101";
            this.flowLayoutPanel101.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel101.TabIndex = 925;
            // 
            // flowLayoutPanel102
            // 
            this.flowLayoutPanel102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel102.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel102.Name = "flowLayoutPanel102";
            this.flowLayoutPanel102.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel102.TabIndex = 630;
            // 
            // flowLayoutPanel103
            // 
            this.flowLayoutPanel103.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel103.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel103.Controls.Add(this.flowLayoutPanel104);
            this.flowLayoutPanel103.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel103.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel103.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel103.Name = "flowLayoutPanel103";
            this.flowLayoutPanel103.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel103.TabIndex = 924;
            // 
            // flowLayoutPanel104
            // 
            this.flowLayoutPanel104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel104.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel104.Name = "flowLayoutPanel104";
            this.flowLayoutPanel104.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel104.TabIndex = 630;
            // 
            // flowLayoutPanel105
            // 
            this.flowLayoutPanel105.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel105.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel106);
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel107);
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel109);
            this.flowLayoutPanel105.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel105.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel105.Location = new System.Drawing.Point(181, 26);
            this.flowLayoutPanel105.Name = "flowLayoutPanel105";
            this.flowLayoutPanel105.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel105.TabIndex = 931;
            // 
            // flowLayoutPanel106
            // 
            this.flowLayoutPanel106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel106.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel106.Name = "flowLayoutPanel106";
            this.flowLayoutPanel106.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel106.TabIndex = 630;
            // 
            // flowLayoutPanel107
            // 
            this.flowLayoutPanel107.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel107.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel107.Controls.Add(this.flowLayoutPanel108);
            this.flowLayoutPanel107.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel107.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel107.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel107.Name = "flowLayoutPanel107";
            this.flowLayoutPanel107.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel107.TabIndex = 924;
            // 
            // flowLayoutPanel108
            // 
            this.flowLayoutPanel108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel108.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel108.Name = "flowLayoutPanel108";
            this.flowLayoutPanel108.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel108.TabIndex = 630;
            // 
            // flowLayoutPanel109
            // 
            this.flowLayoutPanel109.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel109.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel109.Controls.Add(this.flowLayoutPanel110);
            this.flowLayoutPanel109.Controls.Add(this.flowLayoutPanel111);
            this.flowLayoutPanel109.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel109.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel109.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel109.Name = "flowLayoutPanel109";
            this.flowLayoutPanel109.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel109.TabIndex = 925;
            // 
            // flowLayoutPanel110
            // 
            this.flowLayoutPanel110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel110.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel110.Name = "flowLayoutPanel110";
            this.flowLayoutPanel110.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel110.TabIndex = 630;
            // 
            // flowLayoutPanel111
            // 
            this.flowLayoutPanel111.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel111.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel112);
            this.flowLayoutPanel111.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel111.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel111.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel111.Name = "flowLayoutPanel111";
            this.flowLayoutPanel111.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel111.TabIndex = 924;
            // 
            // flowLayoutPanel112
            // 
            this.flowLayoutPanel112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel112.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel112.Name = "flowLayoutPanel112";
            this.flowLayoutPanel112.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel112.TabIndex = 630;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(429, 486);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 29);
            this.button2.TabIndex = 977;
            this.button2.Text = "انهـــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(349, 486);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(81, 29);
            this.Btn_Add.TabIndex = 976;
            this.Btn_Add.Text = "موافـــق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 519);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(907, 22);
            this.statusStrip1.TabIndex = 978;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // cbo_term
            // 
            this.cbo_term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_term.FormattingEnabled = true;
            this.cbo_term.Location = new System.Drawing.Point(73, 32);
            this.cbo_term.Name = "cbo_term";
            this.cbo_term.Size = new System.Drawing.Size(311, 24);
            this.cbo_term.TabIndex = 987;
            this.cbo_term.SelectedIndexChanged += new System.EventHandler(this.cbo_term_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(2, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 988;
            this.label3.Text = "بحــــــــــث:";
            // 
            // Cbo_CityUser_Id
            // 
            this.Cbo_CityUser_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_CityUser_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_CityUser_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_CityUser_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_CityUser_Id.FormattingEnabled = true;
            this.Cbo_CityUser_Id.Location = new System.Drawing.Point(589, 32);
            this.Cbo_CityUser_Id.Name = "Cbo_CityUser_Id";
            this.Cbo_CityUser_Id.Size = new System.Drawing.Size(311, 24);
            this.Cbo_CityUser_Id.TabIndex = 990;
            this.Cbo_CityUser_Id.SelectedValueChanged += new System.EventHandler(this.Cbo_CityUser_Id_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(476, 36);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(87, 16);
            this.label5.TabIndex = 989;
            this.label5.Text = "مدينة الارســال  :";
            // 
            // Agent_User_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 541);
            this.Controls.Add(this.Cbo_CityUser_Id);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbo_term);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.flowLayoutPanel97);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel49);
            this.Controls.Add(this.Txt_Sub_Cust_User);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Grd_Agent_Sub_Cust);
            this.Controls.Add(this.Grd_Login_Cust_Online);
            this.Controls.Add(this.txt_Serch_sub_cust);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel31);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Agent_User_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "513";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Agent_User_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.Agent_User_Add_Upd_Load);
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel33.ResumeLayout(false);
            this.flowLayoutPanel35.ResumeLayout(false);
            this.flowLayoutPanel37.ResumeLayout(false);
            this.flowLayoutPanel39.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel43.ResumeLayout(false);
            this.flowLayoutPanel45.ResumeLayout(false);
            this.flowLayoutPanel19.ResumeLayout(false);
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel23.ResumeLayout(false);
            this.flowLayoutPanel25.ResumeLayout(false);
            this.flowLayoutPanel27.ResumeLayout(false);
            this.flowLayoutPanel29.ResumeLayout(false);
            this.flowLayoutPanel47.ResumeLayout(false);
            this.flowLayoutPanel65.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Login_Cust_Online)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Agent_Sub_Cust)).EndInit();
            this.flowLayoutPanel49.ResumeLayout(false);
            this.flowLayoutPanel51.ResumeLayout(false);
            this.flowLayoutPanel53.ResumeLayout(false);
            this.flowLayoutPanel55.ResumeLayout(false);
            this.flowLayoutPanel57.ResumeLayout(false);
            this.flowLayoutPanel59.ResumeLayout(false);
            this.flowLayoutPanel61.ResumeLayout(false);
            this.flowLayoutPanel63.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel15.ResumeLayout(false);
            this.flowLayoutPanel17.ResumeLayout(false);
            this.flowLayoutPanel67.ResumeLayout(false);
            this.flowLayoutPanel69.ResumeLayout(false);
            this.flowLayoutPanel71.ResumeLayout(false);
            this.flowLayoutPanel73.ResumeLayout(false);
            this.flowLayoutPanel75.ResumeLayout(false);
            this.flowLayoutPanel77.ResumeLayout(false);
            this.flowLayoutPanel79.ResumeLayout(false);
            this.flowLayoutPanel81.ResumeLayout(false);
            this.flowLayoutPanel83.ResumeLayout(false);
            this.flowLayoutPanel85.ResumeLayout(false);
            this.flowLayoutPanel87.ResumeLayout(false);
            this.flowLayoutPanel89.ResumeLayout(false);
            this.flowLayoutPanel91.ResumeLayout(false);
            this.flowLayoutPanel93.ResumeLayout(false);
            this.flowLayoutPanel95.ResumeLayout(false);
            this.flowLayoutPanel97.ResumeLayout(false);
            this.flowLayoutPanel99.ResumeLayout(false);
            this.flowLayoutPanel101.ResumeLayout(false);
            this.flowLayoutPanel103.ResumeLayout(false);
            this.flowLayoutPanel105.ResumeLayout(false);
            this.flowLayoutPanel107.ResumeLayout(false);
            this.flowLayoutPanel109.ResumeLayout(false);
            this.flowLayoutPanel111.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel65;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel66;
        private System.Windows.Forms.TextBox txt_Serch_sub_cust;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView Grd_Login_Cust_Online;
        private System.Windows.Forms.DataGridView Grd_Agent_Sub_Cust;
        private System.Windows.Forms.TextBox Txt_Sub_Cust_User;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel58;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel59;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel61;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel62;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel63;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel64;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel67;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel68;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel69;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel70;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel71;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel72;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel73;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel74;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel75;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel76;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel77;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel78;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel79;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel80;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel81;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel82;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel83;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel84;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel85;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel86;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel87;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel88;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel89;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel90;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel91;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel92;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel93;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel94;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel95;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel96;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel97;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel98;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel99;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel100;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel101;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel102;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel103;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel104;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel105;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel106;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel107;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel108;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel109;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel110;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel111;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel112;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.ComboBox cbo_term;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Cbo_CityUser_Id;
        private System.Windows.Forms.Label label5;
    }
}