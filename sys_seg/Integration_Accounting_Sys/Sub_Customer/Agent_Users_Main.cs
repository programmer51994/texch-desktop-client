﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Agent_Users_Main : Form
    {
        BindingSource Agent_login_Bs = new BindingSource();
        BindingSource login_Bs = new BindingSource();
        bool cbo_subcust_change = false;
        int sub_id = 0;
        int sub_cust_id = 0;
        bool chn_grd = false;
        public Agent_Users_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), txt_User, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Agent_Main.AutoGenerateColumns = false;
            Grd_Agent_Main.ReadOnly = true;
            Grd_User.AutoGenerateColumns = false;

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Agent_Main.Columns["Column5"].DataPropertyName = "ESub_CustNmae";     
            }
            #endregion

        }
        //-------------------------------------------------
        private void Agent_Users_Main_Load(object sender, EventArgs e)
        {

            cbo_subcust_change = false;
            connection.SqlExec("exec agnet_user_Main_search ", "Main_Tbl");
            Cbo_Term_Main_sub.DataSource = connection.SQLDS.Tables["Main_Tbl"];
            Cbo_Term_Main_sub.ValueMember = "T_id";
            Cbo_Term_Main_sub.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
            cbo_subcust_change = true;
            Cbo_Main_sub_cust_SelectedIndexChanged(null, null);
            txt_Agent_Login.Text = "";

           
        }
//-------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Agent_User_Add_Upd AddFrm = new Agent_User_Add_Upd(1,0,0);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Agent_Users_Main_Load(sender, e);
            this.Visible = true;
        }

        //-------------------------------------------------
        private void Grd_Agent_Main_Fill()
        {
             chn_grd = false;
             string condstr = "";
             try
             {
                 if (txt_Agent_Login.Text.ToString() != "")
                 {

                     condstr = " and (ASub_CustName like '%" + txt_Agent_Login.Text + "%' or ESub_CustNmae like '%" + txt_Agent_Login.Text + "%' ) ";
                 }


                 Agent_login_Bs.DataSource = connection.SQLDS.Tables["Main_Tbl1"].Select("T_id = " + Cbo_Term_Main_sub.SelectedValue + condstr).CopyToDataTable();
                 Grd_Agent_Main.DataSource = Agent_login_Bs;
                 chn_grd = true;
                 Grd_Agent_Main_SelectionChanged(null, null);
             }
             catch
             {

                 Grd_Agent_Main.DataSource = new BindingSource();
                 Grd_User.DataSource = new BindingSource();
                 MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط  " : "There is record for this condition", MyGeneral_Lib.LblCap);
                 return;
             }
        }
        //-------------------------------------------------
        private void txt_Agent_Login_TextChanged(object sender, EventArgs e)
        {
            Grd_Agent_Main_Fill();
         }
        //-------------------------------------------------
        private void Cbo_Main_sub_cust_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_subcust_change)
            {
                 Grd_Agent_Main_Fill();
            }

        }
        //-------------------------------------------------
        private void UpdBtn_Click_1(object sender, EventArgs e)
        {
            Int16 T_id = 0;
              T_id = Convert.ToInt16(Cbo_Term_Main_sub.SelectedValue);
              if (Grd_Agent_Main.Rows.Count  == 0)
              {
                  MessageBox.Show(connection.Lang_id == 2 ? " please select The agnet  " : "يجب تحدد اســـم الوكيل", MyGeneral_Lib.LblCap);
                  return;
              }

            Agent_User_Add_Upd AddFrm = new Agent_User_Add_Upd(2, sub_cust_id, T_id);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Agent_Users_Main_Load(sender, e);
            this.Visible = true;
        }
        //-------------------------------------------------
        private void Grd_Agent_Main_SelectionChanged(object sender, EventArgs e)
        {
            if (chn_grd)
            {
              
                sub_cust_id = Convert.ToInt32(((DataRowView)Agent_login_Bs.Current).Row["sub_cust_id"]);
                login_Bs.DataSource = connection.SQLDS.Tables["Main_Tbl2"].Select("T_id = " + Cbo_Term_Main_sub.SelectedValue + "and cust_id = " + sub_cust_id).CopyToDataTable();
                Grd_User.DataSource = login_Bs;
            
            
            }
        }

        private void Agent_Users_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            cbo_subcust_change = false;
            chn_grd = false;

            string[] Used_Tbl = { "Main_Tbl" ,"Main_Tbl1", "Main_Tbl2" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

        }
    }
}
