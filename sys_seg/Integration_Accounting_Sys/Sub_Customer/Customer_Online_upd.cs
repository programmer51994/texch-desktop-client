﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Online_upd : Form
    {
        public static int FORM_Id = 0;
        public static int Cust_Id = 0;
        public static Int16 T_ID =  0;
        public static string acust_name ="";
        public static string ecust_name = "";
        public static Int16 CUR_ID = 0;
        public static int Rec_Cust_Id = 0;
        public static decimal Uper_Limit;
        BindingSource Cust_Upd_Onilne_bs = new BindingSource();
        BindingSource fininal_Cust_Upd_Onilne_bs = new BindingSource(); 
        string sql_txt = "";
        string sql_txt1 = "";
        DataTable cur_oper_Tbl = new DataTable();

        public Customer_Online_upd(int Frm_Id, int cus_id, Int16 cur_id, decimal uperlimt, Int16 t_id, string ACUST_NAMe, string eCUST_NAMe) //,int Rec_cust_id
        {
            InitializeComponent();
             Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            FORM_Id = Frm_Id;
            Cust_Id = cus_id;
            CUR_ID = cur_id;
            T_ID = t_id;
            Uper_Limit = uperlimt;
            acust_name = ACUST_NAMe;
            ecust_name = eCUST_NAMe;
            // Rec_Cust_Id = Rec_cust_id;
            Grd_Cust_Main_Onilne.AutoGenerateColumns = false;
            grd_cur_oper.AutoGenerateColumns = false;
            Grd_Cust_Main_Onilne.ReadOnly = true;
            Txt_sell_rate.Enabled = true;
            Txt_buy_rate.Enabled = true;

            if (connection.Lang_id != 1)
            {
                Grd_Cust_Main_Onilne.Columns["Column21"].DataPropertyName = "Acc_EName";
            }

        }


        private void Customer_Online_upd_Load(object sender, EventArgs e)
        {
            Txt_TErminal.Text = connection.Lang_id == 1 ? acust_name : ecust_name ;
            string Sql_cust_typ_Text = "SELECT cust_typ_id,cust_typ_Aname,cust_typ_Ename FROM cust_type";
            cbo_deal_nature.DataSource = connection.SqlExec(Sql_cust_typ_Text, "Type_cust_typ");
            cbo_deal_nature.ValueMember = "cust_typ_id";
            cbo_deal_nature.DisplayMember = connection.Lang_id == 1 ? "cust_typ_Aname" : "cust_typ_Ename";


            sql_txt = " select * from Sub_CUSTOMERS_online  "
                    + " where sub_cust_id = " + Cust_Id
                    + " and cur_id = " + CUR_ID
                    + " and t_id = " + T_ID;

            Cust_Upd_Onilne_bs.DataSource = connection.SqlExec(sql_txt, "Sub_CUSTOMERS_online_Ubd_Tbl");
            Grd_Cust_Main_Onilne.DataSource = Cust_Upd_Onilne_bs;

        



            Txt_Cur_Aname.DataBindings.Clear();
            txt_daily_uper_limt_upd.DataBindings.Clear();
            txt_per_tran_rem_upd.DataBindings.Clear();
            //txt_open_uper_limt.DataBindings.Clear();
            Txt_sell_rate.DataBindings.Clear();
            Txt_buy_rate.DataBindings.Clear();

            Check.Checked = Convert.ToBoolean(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["chk_daily"]);
            //Chk_Cur.Checked = Convert.ToBoolean(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["chk_open"]);
            byte sub_cust_state = Convert.ToByte(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["Cust_state"]);
          //  Cust_Check.Checked = Convert.ToBoolean(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["ver_check"]);
            

            Txt_buy_rate.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Buy_rate");
            Txt_sell_rate.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Sell_rate");

       //     cbo_deal_nature.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "cust_typ_Aname");

            Int16 X = Convert.ToInt16(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["cust_typ_id"]);
            cbo_deal_nature.SelectedValue = X;
            // chk_daily_upd.Checked = DR.Field<bool>("chk_daily");
            //  chk_open_upd.Checked = DR.Field<bool>("chk_open");

            Txt_Cur_Aname.DataBindings.Add("Text", Cust_Upd_Onilne_bs, connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME");

            //txt_daily_uper_limt_upd.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Daily_Transction");
            //txt_per_tran_rem_upd.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Rem_Transction");
            //txt_open_uper_limt.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Up_Limt_Rem_Transction");

            CboType_sub_cust.SelectedIndex = sub_cust_state;
            CHK1.Checked = Convert.ToBoolean(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["Chk_Exch_Price"]);
            CHK2.Checked = Convert.ToBoolean(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["CHK_exch_agent"]); // ahmad
            if (connection.Lang_id == 2)
            {
                Column4.DataPropertyName = "eSub_CustNmae";
                Column6.DataPropertyName = "City_ename";
                Column8.DataPropertyName = "Con_eName";
                Column19.DataPropertyName = "Nat_eName";
                Column13.DataPropertyName = "Fmd_eName";

                CboType_sub_cust.Items[0] = "Inactive";
                CboType_sub_cust.Items[1] = "Active";
            }

            if (CHK1.Checked == true && CHK2.Checked == false)
            {
                CHK2.Checked = false;
                Txt_sell_rate.Enabled = true;
                Txt_buy_rate.Enabled = true;
                Txt_sell_rate.DataBindings.Clear();
                Txt_buy_rate.DataBindings.Clear();
                Txt_sell_rate.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Sell_rate");
                Txt_buy_rate.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Buy_rate");
            }

            if (CHK1.Checked == false && CHK2.Checked == true)
            {
                CHK2.Checked = true;
                Txt_sell_rate.Enabled = false;
                Txt_buy_rate.Enabled = false;
                Txt_sell_rate.Text = "0.0000000";
                Txt_buy_rate.Text = "0.0000000";
            }

            if (CHK1.Checked == false && CHK2.Checked == false)
            {
                CHK1.Checked = false;
                CHK2.Checked = false;
                Txt_sell_rate.Enabled = false;
                Txt_buy_rate.Enabled = false;
                Txt_sell_rate.Text = "0.0000000";
                Txt_buy_rate.Text = "0.0000000";
            }

            DataTable TimeZoneTBL = new DataTable();
            TimeZoneTBL.Columns.Add("TimeZoneID");
            TimeZoneTBL.Columns.Add("TimeZoneName");

            var infos = TimeZoneInfo.GetSystemTimeZones();
            foreach (var info in infos)
            {
                TimeZoneTBL.Rows.Add(info.Id, info.DisplayName);
            }

            cbo_time_zone.DataSource = TimeZoneTBL;
            cbo_time_zone.ValueMember = "TimeZoneID";
            cbo_time_zone.DisplayMember = "TimeZoneName";

            cbo_time_zone.SelectedValue = Convert.ToString(((DataRowView)Cust_Upd_Onilne_bs.Current).Row["SubCustTimeZone"]);


            //Chk_Exch_Price_CheckedChanged(null, null);
            Check_CheckedChanged(null, null);

            sql_txt1 = " select distinct 1 as cur_oper_chk, CUST_ID,	a.cur_oper_id,	a.CUR_ID, b.cur_oper_aname,b.cur_oper_ename, c.Cur_ANAME,c.Cur_ENAME   from Sub_CUST_Financial_cur_oper a, Financial_cur_oper b,Cur_Tbl c where  a.cur_oper_id = b.cur_oper_id and a.CUR_ID=c.Cur_ID   "
                    + " and CUST_ID = " + Cust_Id
                    + " and a.cur_id =" + CUR_ID
                    + " and b.cur_oper_id  in (select cur_oper_id  from Sub_CUST_Financial_cur_oper where CUST_ID = " + Cust_Id
                    + "and CUR_ID =" + CUR_ID
                    + ")" 
                    + " union " 
                    + " select  distinct 0 as cur_oper_chk,0 as CUST_ID,b.cur_oper_id,0 as CUR_ID, b.cur_oper_aname,b.cur_oper_ename,'' as Cur_ANAME,'' as Cur_ENAME "  
                    + " from  Financial_cur_oper b "  
                    + "where b.cur_oper_id not in (select cur_oper_id  from Sub_CUST_Financial_cur_oper where "  
                    + " CUST_ID = "  + Cust_Id
                     + " and  cur_id ="  + CUR_ID
                     + ")";

            fininal_Cust_Upd_Onilne_bs.DataSource = connection.SqlExec(sql_txt1, "Sub_CUSTOMERS_online_Ubd_Tbl3");
            grd_cur_oper.DataSource = fininal_Cust_Upd_Onilne_bs;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (Check.Checked == true)
            {

                if (txt_daily_uper_limt_upd.Text == "0.000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Please enter the upper limit for conversion daily" : "الرجاء ادخال الحد الاعلى للتحويل اليومي", MyGeneral_Lib.LblCap);
                    txt_daily_uper_limt_upd.Focus();
                    return;
                }

                if (txt_per_tran_rem_upd.Text == "0.000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Please enter your limit per transaction limit" : "الرجاء ادخال الحد حد المعاملة الواحدة", MyGeneral_Lib.LblCap);
                    txt_per_tran_rem_upd.Focus();
                    return;
                }

            }

            if (CHK1.Checked == true)
            {
                if (Convert.ToString(Txt_sell_rate.Text) == "0.0000000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Exch Price for Sell please" : " ادخل سعر التعادل البيع رجاءا", MyGeneral_Lib.LblCap);
                    Txt_sell_rate.Focus();
                    return;
                }

                if (Convert.ToString(Txt_buy_rate.Text) == "0.0000000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Exch Price for Buy please" : " ادخل سعر التعادل الشراء رجاءا", MyGeneral_Lib.LblCap);
                    Txt_buy_rate.Focus();
                    return;
                }

            }
            if (Check.Checked == true)
            {
                if (Convert.ToDecimal(txt_per_tran_rem_upd.Text) > Convert.ToDecimal(txt_daily_uper_limt_upd.Text))
                {
                    MessageBox.Show(connection.Lang_id == 2 ? " can not enter per transaction price greater than daily price " : " لا يمكن ادخال حد المعاملة للحوالة الواحدة اكبر من حد التحويل اليومي ", MyGeneral_Lib.LblCap);
                    txt_daily_uper_limt_upd.Focus();
                    return;

                }
            }


            DataRowView DRV = Cust_Upd_Onilne_bs.Current as DataRowView;
            DataRow DR = DRV.Row;

            int Sub_Cust_ID = DR.Field<int>("Sub_Cust_ID");
            string Cust_Name = DR.Field<string>("ASub_CustName");
            string Cust_EName = DR.Field<string>("ESub_CustNmae");
            string Cit_AName = DR.Field<string>("City_Aname");
            string Con_AName = DR.Field<string>("Con_AName");
            string A_Address = DR.Field<string>("A_ADDRESS");
            string Fmd_AName = DR.Field<string>("Fmd_AName");
            string Phone = DR.Field<string>("PHONE");
            string Email = DR.Field<string>("EMAIL");
            string Nat_Aname = DR.Field<string>("Nat_AName");
            string frm_doc_no = DR.Field<string>("frm_doc_no");
            string frm_doc_Da1 = ((DataRowView)Cust_Upd_Onilne_bs.Current).Row["FRM_DOC_DA"].ToString();
            string doc_eda1 = ((DataRowView)Cust_Upd_Onilne_bs.Current).Row["DOC_EDA"].ToString();
            string frm_doc_Is = DR.Field<string>("FRM_DOC_IS");
            Int16 FMD_ID = DR.Field<Int16>("FRM_DOC_ID");
            Int16 con_id = DR.Field<Int16>("con_id");
            Int16 Cit_Id = DR.Field<Int16>("City_Id");
            Int16 nat_id = DR.Field<Int16>("nat_id");
            string Fmd_EName = DR.Field<string>("Fmd_EName");
            string Cit_EName = DR.Field<string>("City_Ename");
            string Con_EName = DR.Field<string>("Con_EName");
            string Nat_Ename = DR.Field<string>("Nat_Ename");

            string cust_code = DR.Field<string>("Cust_Code");

          //  int T_id = connection.T_ID;
            int user_id = connection.user_id;

            //-------------------------------------------------------------------------------------------------
            Int16 Cur_ID = DR.Field<Int16>("Cur_ID");
            string Cur_ANAME = DR.Field<string>("Cur_ANAME");
            string Cur_ENAME = DR.Field<string>("Cur_ENAME");
            //---------------------------------------------------------------------------------------------------



            //decimal Daily_Transction = DR.Field<decimal>("Daily_Transction");
            //decimal Rem_Transction = DR.Field<decimal>("Rem_Transction");
            //decimal Up_Limt_Rem_Transction = DR.Field<decimal>("Up_Limt_Rem_Transction");

            int Acc_Id = DR.Field<int>("Acc_Id");
            string Acc_AName = DR.Field<string>("Acc_AName");
            string Acc_EName = DR.Field<string>("Acc_EName");

            byte sub_cust_State = Convert.ToByte(CboType_sub_cust.SelectedIndex);

            #region Validation

            if (Check.Checked == true)
            {

                if (txt_daily_uper_limt_upd.Text == "0.000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Please enter the upper limit for conversion daily" : "الرجاء ادخال الحد الاعلى للتحويل اليومي", MyGeneral_Lib.LblCap);
                    txt_daily_uper_limt_upd.Focus();
                    return;
                }

                if (txt_per_tran_rem_upd.Text == "0.000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Please enter your limit per transaction limit" : "الرجاء ادخال الحد حد المعاملة الواحدة", MyGeneral_Lib.LblCap);
                    txt_per_tran_rem_upd.Focus();
                    return;
                }


                //if (Convert.ToDecimal(txt_daily_uper_limt_upd.Text) > Uper_Limit)
                //{
                //    MessageBox.Show(connection.Lang_id == 2 ? "The upper limit of the Money transfer is greater than the value of a for highest limit" : "قيمــة الحد الاعلى للتحويــل اليومي اكبــر مـن قيمة الحــد الاعلى للتحويـــل ", MyGeneral_Lib.LblCap);
                //    txt_daily_uper_limt_upd.Focus();
                //    return;
                //}

                //if (Convert.ToDecimal(txt_per_tran_rem_upd.Text) > Uper_Limit)
                //{
                //    MessageBox.Show(connection.Lang_id == 2 ? "Value per transaction limit is greater than the value of the daily conversion limit" : "قيمــة حد تحويل المعاملة الواحدة اكبــر مـن قيمة الحد الاعلى التحويل اليومي ", MyGeneral_Lib.LblCap);
                //    txt_per_tran_rem_upd.Focus();
                //    return;
                //}



                //if (Convert.ToDecimal(txt_per_tran_rem_upd.Text) > Convert.ToDecimal(txt_daily_uper_limt_upd.Text))
                //{
                //    MessageBox.Show(connection.Lang_id == 2 ? "Value per transaction limit is greater than the value of the daily conversion limit" : "قيمــة حد تحويل المعاملة الواحدة اكبــر مـن قيمة الحد الاعلى التحويل اليومي ", MyGeneral_Lib.LblCap);
                //    txt_per_tran_rem_upd.Focus();
                //    return;
                //}




            }

            //if (Chk_Cur.Checked == true)
            //{

            //    if (txt_open_uper_limt.Text == "0.000")
            //    {
            //        MessageBox.Show(connection.Lang_id == 2 ? "Please enter your open borders highest limit value" : "الرجاء ادخال قيمة الحد الاعلى الخاص  بالحدود المفتوحة", MyGeneral_Lib.LblCap);
            //        txt_open_uper_limt.Focus();
            //        return;
            //    }


            //    //if (Convert.ToDecimal(txt_open_uper_limt.Text) > Uper_Limit)
            //    //{
            //    //    MessageBox.Show(connection.Lang_id == 2 ? "The open upper limit  is greater than the value of a for highest limit" : "قيمــة الحد المفتوح اكبــر مـن قيمة الحــد الاعلى للتحويـــل ", MyGeneral_Lib.LblCap);
            //    //    txt_open_uper_limt.Focus();
            //    //    return;
            //    //}


            //}

            //if (Check.Checked == false && Chk_Cur.Checked == false)
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Please check the boundaries" : "الرجاء قم بتأشير الحدود", MyGeneral_Lib.LblCap);
            //    return;

            //}
            #endregion


            Decimal buy_rate = Convert.ToDecimal(Txt_buy_rate.Text);
            Decimal sell_rate = Convert.ToDecimal(Txt_sell_rate.Text);
            int cbo_deal_nat = Convert.ToInt32(cbo_deal_nature.SelectedValue);

            cur_oper_Tbl = connection.SQLDS.Tables["Sub_CUSTOMERS_online_Ubd_Tbl3"].DefaultView.ToTable(false, "cur_oper_chk", "cur_oper_id").Select().CopyToDataTable();
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Customer_Online_Add_Upd";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                // connection.SQLCMD.Parameters.AddWithValue("@Rec_cust_id", Rec_Cust_Id);
                connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_ID", Sub_Cust_ID);
                connection.SQLCMD.Parameters.AddWithValue("@ASub_CustName", Cust_Name);
                connection.SQLCMD.Parameters.AddWithValue("@ESub_CustName", Cust_EName);
                connection.SQLCMD.Parameters.AddWithValue("@City_Id", Cit_Id);
                connection.SQLCMD.Parameters.AddWithValue("@City_Aname", Cit_AName);
                connection.SQLCMD.Parameters.AddWithValue("@City_Ename", Cit_EName);
                connection.SQLCMD.Parameters.AddWithValue("@Con_Id", con_id);
                connection.SQLCMD.Parameters.AddWithValue("@Con_AName", Con_AName);
                connection.SQLCMD.Parameters.AddWithValue("@Con_EName", Con_EName);
                connection.SQLCMD.Parameters.AddWithValue("@A_ADDRESS", A_Address);
                connection.SQLCMD.Parameters.AddWithValue("@PHONE", Phone);
                connection.SQLCMD.Parameters.AddWithValue("@EMAIL", Email);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_ID", FMD_ID);
                connection.SQLCMD.Parameters.AddWithValue("@Fmd_AName", Fmd_AName);
                connection.SQLCMD.Parameters.AddWithValue("@Fmd_EName", Fmd_EName);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_NO", frm_doc_no);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_DA", frm_doc_Da1);
                connection.SQLCMD.Parameters.AddWithValue("@DOC_EDA", doc_eda1);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_IS", frm_doc_Is);
                connection.SQLCMD.Parameters.AddWithValue("@NAT_ID", nat_id);
                connection.SQLCMD.Parameters.AddWithValue("@Nat_AName", Nat_Aname);
                connection.SQLCMD.Parameters.AddWithValue("@Nat_EName", Nat_Ename);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_Id", Cur_ID);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_ANAME", Cur_ANAME);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_ENAME", Cur_ENAME);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", user_id);
                connection.SQLCMD.Parameters.AddWithValue("@chk_daily", Check.Checked);
                connection.SQLCMD.Parameters.AddWithValue("@Daily_Transction", Convert.ToDecimal(txt_daily_uper_limt_upd.Text));
                connection.SQLCMD.Parameters.AddWithValue("@Rem_Transction", Convert.ToDecimal(txt_per_tran_rem_upd.Text));
                connection.SQLCMD.Parameters.AddWithValue("@chk_open", 0);
                connection.SQLCMD.Parameters.AddWithValue("@Up_Limt_Rem_Transction", 0);//ملغى
                connection.SQLCMD.Parameters.AddWithValue("@Acc_Id", Acc_Id);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_AName", Acc_AName);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_EName", Acc_EName);
                connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", FORM_Id);
                connection.SQLCMD.Parameters.AddWithValue("@T_Id", T_ID);
                connection.SQLCMD.Parameters.AddWithValue("@State_sub_cust", sub_cust_State);
                connection.SQLCMD.Parameters.AddWithValue("@Cust_Code", cust_code);
               // connection.SQLCMD.Parameters.AddWithValue("@ver_check", Cust_Check.Checked == true ? 1 : 0);
                connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.AddWithValue("@Buy_rate", buy_rate);
                connection.SQLCMD.Parameters.AddWithValue("@Sell_rate", sell_rate);
                connection.SQLCMD.Parameters.AddWithValue("@chk_exch_price", Convert.ToInt16(CHK1.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@cbo_deal_nat", cbo_deal_nat);
                connection.SQLCMD.Parameters.AddWithValue("@CHK_exch_agent", Convert.ToInt16(CHK2.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@SubCust_Financial_cur_oper_Tbl", cur_oper_Tbl);
                connection.SQLCMD.Parameters.AddWithValue("@TimeZone", cbo_time_zone.SelectedValue.ToString());

                MyGeneral_Lib.Copytocliptext("Customer_Online_Add_Upd", connection.SQLCMD);
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Customer_Online_Add_Upd");
                obj.Close();
                connection.SQLCS.Close();
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }

            this.Close();

        }

        //if (CHK1.Checked == true)
        //    {
        //        Txt_sell_rate.Enabled = true;
        //        Txt_buy_rate.Enabled = true;
        //        Txt_sell_rate.Text = "0.000";
        //        Txt_buy_rate.Text = "0.000";
        //        CHK2.Checked = false;
        //    }
        //    else
        //    {
        //        Txt_sell_rate.Enabled = false;
        //        Txt_buy_rate.Enabled = false;
        //        Txt_sell_rate.Text = "0.000";
        //        Txt_buy_rate.Text = "0.000";
        //        CHK2.Checked = true;

        //    }


        private void Chk_Exch_Price_CheckedChanged(object sender, EventArgs e)
        {
            if (CHK1.Checked == true) // && CHK2.Checked == false)
            {
                Txt_sell_rate.Enabled = true;
                Txt_buy_rate.Enabled = true;
                //Txt_sell_rate.Text = "0.000";
                //Txt_buy_rate.Text = "0.000";
                CHK2.Checked = false;
            }

            if (CHK1.Checked == false) // && CHK2.Checked == true)
            {
                Txt_sell_rate.Enabled = false;
                Txt_buy_rate.Enabled = false;
                Txt_sell_rate.Text = "0.000";
                Txt_buy_rate.Text = "0.000";
                CHK2.Checked = true;
            }

            //if (CHK1.Checked == false && CHK2.Checked == false)
            //{
            //    CHK1.Checked = false;
            //    CHK2.Checked = false;
            //    Txt_sell_rate.Enabled = false;
            //    Txt_buy_rate.Enabled = false;
            //    Txt_sell_rate.Text = "0.0000000";
            //    Txt_buy_rate.Text = "0.0000000";
            //}
        }

        private void Check_CheckedChanged(object sender, EventArgs e)
        {
            if (Check.Checked == true)
            {
                txt_daily_uper_limt_upd.Enabled = true;
                txt_per_tran_rem_upd.Enabled = true;

                txt_daily_uper_limt_upd.DataBindings.Clear();
                txt_per_tran_rem_upd.DataBindings.Clear();
                txt_daily_uper_limt_upd.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Daily_Transction");
                txt_per_tran_rem_upd.DataBindings.Add("Text", Cust_Upd_Onilne_bs, "Rem_Transction");

                //txt_open_uper_limt.Enabled = false;
                //Chk_Cur.Checked = false;
                //txt_open_uper_limt.Text = "0.000";
            }
            else
            {
                txt_daily_uper_limt_upd.Enabled = false;
                txt_per_tran_rem_upd.Enabled = false;

                txt_daily_uper_limt_upd.Text = "0.0000000";
                txt_per_tran_rem_upd.Text = "0.0000000";
                //  txt_open_uper_limt.Enabled = true;
            }
        }

        private void CHK2_CheckedChanged(object sender, EventArgs e)
        {
            if (CHK2.Checked == true)
            {
                CHK1.Checked = false;
                Txt_sell_rate.Text = "0.000";
                Txt_buy_rate.Text = "0.000";
            }
            else
            {
                CHK1.Checked = true;
            }
        }
    }
}
