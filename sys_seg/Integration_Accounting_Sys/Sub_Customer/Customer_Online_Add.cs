﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Customer_Online_Add : Form
    {
        bool chag_Grd_Cust_Online = false;
        bool chag_term = false;
        string SqlTxt = "";
        string SqlTxt3 = "";
        string Sql_cust_typ_Text = "";
        BindingSource Grd_Cust_Online_bs = new BindingSource();
        BindingSource Grd_Cust_Currency_bs = new BindingSource();
        BindingSource grd_cur_oper_bs = new BindingSource();
        public static int FORM_Id = 0;
        DataTable cur_oper_Tbl = new DataTable();

        public Customer_Online_Add(int Frm_Id)
        {

            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_Cust_Online.AutoGenerateColumns = false;
            Grd_Cust_Currency.AutoGenerateColumns = false;
            grd_cur_oper.AutoGenerateColumns = false;
            FORM_Id = Frm_Id;

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust_Online.Columns["Column2"].DataPropertyName = "ECust_Name";
                Grd_Cust_Online.Columns["Column4"].DataPropertyName = "Fmd_EName";
                Grd_Cust_Online.Columns["Column11"].DataPropertyName = "Cit_EName";
                Grd_Cust_Online.Columns["Column12"].DataPropertyName = "Con_EName";
                Grd_Cust_Online.Columns["Column13"].DataPropertyName = "E_Address";
                Grd_Cust_Online.Columns["Column16"].DataPropertyName = "Nat_Ename";
                Grd_Cust_Online.Columns["Column20"].DataPropertyName = "Cust_Type_EName";
                Grd_Cust_Online.Columns["Column21"].DataPropertyName = "Gender_Ename";

                Grd_Cust_Currency.Columns["Column24"].DataPropertyName = "Cur_ENAME";
                Grd_Cust_Currency.Columns["Column27"].DataPropertyName = "DC_Ename";
                Grd_Cust_Currency.Columns["Column28"].DataPropertyName = "Acc_EName";
                CboType_sub_cust.Items[0] = "Inactive";
                CboType_sub_cust.Items[1] = "Active";

            }
            #endregion

        }

        private void Customer_Online_Add_Load_1(object sender, EventArgs e)
        {
            //Txt_buy_rate.Enabled = true;
            //Txt_sell_rate.Enabled = true;

            chag_Grd_Cust_Online = false;
            Txt_Cust_Name.Text = "";

            SqlTxt = "select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  "
               + " From CUSTOMERS A, TERMINALS B   "
               + " Where A.CUST_ID = B.CUST_ID  "
               + " And A.Cust_Flag <> 0 ";


            cbo_term.DataSource = connection.SqlExec(SqlTxt, "Term_tbl");
            cbo_term.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "Ecust_name";
            cbo_term.ValueMember = "T_id";
            chag_term = false;

            Sql_cust_typ_Text = "SELECT cust_typ_id,cust_typ_Aname,cust_typ_Ename FROM cust_type";
            cbo_deal_nature.DataSource = connection.SqlExec(Sql_cust_typ_Text, "Type_cust_typ");
            cbo_deal_nature.ValueMember = "cust_typ_id";
            cbo_deal_nature.DisplayMember = connection.Lang_id == 1 ? "cust_typ_Aname" : "cust_typ_Ename";





            if (connection.SQLDS.Tables["Term_tbl"].Rows.Count > 0)
            {

                chag_term = true;
                cbo_term_SelectedIndexChanged(null, null);
            }

            CboType_sub_cust.SelectedIndex = 1;
            //Chk_Exch_Price_CheckedChanged(null, null);

            DataTable TimeZoneTBL = new DataTable();
            TimeZoneTBL.Columns.Add("TimeZoneID");
            TimeZoneTBL.Columns.Add("TimeZoneName");

            var infos = TimeZoneInfo.GetSystemTimeZones();
            foreach (var info in infos)
            {
                TimeZoneTBL.Rows.Add(info.Id, info.DisplayName);
            }

            cbo_time_zone.DataSource = TimeZoneTBL;
            cbo_time_zone.ValueMember = "TimeZoneID";
            cbo_time_zone.DisplayMember = "TimeZoneName";

            cbo_time_zone.SelectedIndex = 0;
        }

        private void Grd_Cust_Online_fill()
        {

            //int.TryParse(TxtCust_Name.Text, out Cust_id);

            SqlTxt = " exec Main_sub_Customer_Onlie '" + Txt_Cust_Name.Text + "'" + "," + Convert.ToInt16(cbo_term.SelectedValue);
            connection.SqlExec(SqlTxt, "Customer_Online_Tbl");


            if (connection.SQLDS.Tables["Customer_Online_Tbl"].Rows.Count > 0)
            {


                DataTable DT_Online_Cust = new DataTable();
                DT_Online_Cust = connection.SQLDS.Tables["Customer_Online_Tbl"].DefaultView.ToTable(true, "Cit_AName", "Cit_EName", "Con_AName", "Con_EName", "A_Address", "E_Address", "Fmd_AName", "Fmd_EName", "Phone", "Email", "ACust_Name", "ECust_Name", "birth_date", "Nat_Aname", "Nat_Ename", "Cust_Type_AName", "Cust_Type_EName", "Gender_aname", "Gender_Ename", "frm_doc_no", "Occupation", "frm_doc_Da", "doc_eda", "frm_doc_Is", "Cust_Id", "FMD_ID", "con_id", "Cit_Id", "nat_id", "Gender_id").Select().CopyToDataTable();
                Grd_Cust_Online_bs.DataSource = DT_Online_Cust;
                Grd_Cust_Online.DataSource = Grd_Cust_Online_bs;
                chag_Grd_Cust_Online = true;
                Grd_Cust_Online_SelectionChanged(null, null);

            }
            else
            {
                MessageBox.Show(connection.Lang_id == 2 ? "There is no data" : "لاتوجد بيانات تحقق الشروط", MyGeneral_Lib.LblCap);
                return;

            }

        }


        private void Txt_Cust_Name_TextChanged(object sender, EventArgs e)
        {
            Grd_Cust_Online.DataSource = new BindingSource();
            Grd_Cust_Currency.DataSource = new BindingSource();
            Grd_Cust_Online_fill();
        }

        private void Grd_Cust_Online_SelectionChanged(object sender, EventArgs e)
        {
            //Check.Checked = false;
            Chk_Cur.Checked = false;

            int cust_id = Convert.ToInt32(((DataRowView)Grd_Cust_Online_bs.Current).Row["cust_id"]);
            Int16 cbo_term_id  = Convert.ToInt16(cbo_term.SelectedValue);
            if (chag_Grd_Cust_Online)
            {
                try
                {

                    Grd_Cust_Currency_bs.DataSource = connection.SQLDS.Tables["Customer_Online_Tbl"].Select("cust_id = " + cust_id + " and T_id = " + cbo_term_id).CopyToDataTable();
                    Grd_Cust_Currency.DataSource = Grd_Cust_Currency_bs;
                }
                catch
                {
                    Grd_Cust_Currency_bs.DataSource = null;  //تفريغ الجدول
                }
            }

            //---------------------------------------------------------------

            txt_Cust_Code.Text = "";
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Get_Custcode_Subcustomer_Online";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Sub_cust_id", cust_id);
                connection.SQLCMD.Parameters.Add("@Cust_Code", SqlDbType.Int).Value = 0;
                connection.SQLCMD.Parameters["@Cust_Code"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Get_Custcode_Subcustomer_Online");


                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Dispose();

            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);

            }

            int cust_code = Convert.ToInt32(connection.SQLCMD.Parameters["@Cust_Code"].Value);
            txt_Cust_Code.Text = cust_code.ToString();

            connection.SQLCMD.Parameters.Clear();
            //--------------------------------------------------------------

        }



        private void chk_daily_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked == true)
            {
                txt_daily_uper_limt.Enabled = true;
                txt_per_tran_rem.Enabled = true;
                //txt_open_uper_limt.Enabled = false;
                //txt_open_uper_limt.Text = "0.000";
                //Check.Checked = false;

            }
            else
            {

                txt_daily_uper_limt.Text = "0.000";
                txt_per_tran_rem.Text = "0.000";
                //txt_open_uper_limt.Enabled = true;
                txt_daily_uper_limt.Enabled = false;
                txt_per_tran_rem.Enabled = false;

            }
        }

        private void chk_open_CheckedChanged(object sender, EventArgs e)
        {
            //if (Check.Checked == true)
            //{
              //  txt_open_uper_limt.Enabled = true;
                txt_daily_uper_limt.Enabled = false;
                txt_per_tran_rem.Enabled = false;
                txt_daily_uper_limt.Text = "0.000";
                txt_per_tran_rem.Text = "0.000";
                Chk_Cur.Checked = false;

          //  }
            //else
            //{
            //    txt_open_uper_limt.Text = "0.000";
            //    // txt_daily_uper_limt.Enabled = true;
            //    // txt_per_tran_rem.Enabled = true;
            //    txt_open_uper_limt.Enabled = false;
            //}

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked == true)
            {

                if (txt_daily_uper_limt.Text == "0.000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Please enter the upper limit for conversion daily" : "الرجاء ادخال الحد الاعلى للتحويل اليومي", MyGeneral_Lib.LblCap);
                    txt_daily_uper_limt.Focus();
                    return;
                }

                if (txt_per_tran_rem.Text == "0.000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Please enter your limit per transaction limit" : "الرجاء ادخال الحد حد المعاملة الواحدة", MyGeneral_Lib.LblCap);
                    txt_per_tran_rem.Focus();
                    return;
                }

            }

            if (CHK1.Checked == true )
            {
                if (Convert.ToString(Txt_sell_rate.Text )== "0.0000000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Exch Price for Sell please" : " ادخل سعر التعادل البيع رجاءا", MyGeneral_Lib.LblCap);
                    Txt_sell_rate.Focus();
                    return;
                }

                if (Convert.ToString(Txt_buy_rate.Text) == "0.0000000")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Exch Price for Buy please" : " ادخل سعر التعادل الشراء رجاءا", MyGeneral_Lib.LblCap);
                    Txt_buy_rate.Focus();
                    return;
                }

            }
            if (Chk_Cur.Checked == true)
            {
                if (Convert.ToDecimal(txt_per_tran_rem.Text) > Convert.ToDecimal(txt_daily_uper_limt.Text))
                {
                    MessageBox.Show(connection.Lang_id == 2 ? " can not enter per transaction price greater than daily price " : " لا يمكن ادخال حد المعاملة للحوالة الواحدة اكبر من حد التحويل اليومي ", MyGeneral_Lib.LblCap);
                    txt_daily_uper_limt.Focus();
                    return;

                }
            }

            //if (CHK2.Checked == true)
            //{
            //    if (Convert.ToDecimal(txt_per_tran_rem.Text) > Convert.ToDecimal(txt_daily_uper_limt.Text))
            //    {
            //        MessageBox.Show(connection.Lang_id == 2 ? " can not enter per transaction price greater than daily price " : " لا يمكن ادخال حد المعاملة للحوالة الواحدة اكبر من حد التحويل اليومي ", MyGeneral_Lib.LblCap);
            //        txt_daily_uper_limt.Focus();
            //        return;

            //    }
            //}

            //CHK2

            Int16 frm_id = 1;
            //if (frm_id == 1)
            //{
            //    txt_daily_uper_limt.Text = "";
            //    txt_per_tran_rem.Text = "";
            //    txt_open_uper_limt.Text = "";//--حدود مفتوحة
            //    Txt_buy_rate.Text = "";
            //    Txt_sell_rate.Text = "";

            //}
            //--------------------------------------------------------------------------------------------
            DataRowView DRV = Grd_Cust_Online_bs.Current as DataRowView;
            DataRow DR = DRV.Row;

            int Sub_Cust_ID = DR.Field<int>("Cust_ID");
            string Cust_Name = DR.Field<string>("ACust_Name");
            string Cust_EName = DR.Field<string>("ECust_Name");
            string Cit_AName = DR.Field<string>("Cit_AName");
            string Con_AName = DR.Field<string>("Con_AName");
            string A_Address = DR.Field<string>("A_Address");
            string Fmd_AName = DR.Field<string>("Fmd_AName");
            string Phone = DR.Field<string>("Phone");
            string Email = DR.Field<string>("Email");
            string Nat_Aname = DR.Field<string>("Nat_Aname");
            string Cust_Type_AName = DR.Field<string>("Cust_Type_AName");
            string Gender_aname = DR.Field<string>("Gender_aname");
            string Occupation = DR.Field<string>("Occupation");
            string frm_doc_no = DR.Field<string>("frm_doc_no");
            //string frm_doc_Da1 = DR.Field<string>("frm_doc_Da");
            string frm_doc_Da1 = ((DataRowView)Grd_Cust_Online_bs.Current).Row["frm_doc_Da"].ToString();
            string doc_eda1 = ((DataRowView)Grd_Cust_Online_bs.Current).Row["doc_eda"].ToString();
            // string doc_eda1 = DR.Field<string>("doc_eda"); 
            string frm_doc_Is = DR.Field<string>("frm_doc_Is");
            Int16 FMD_ID = DR.Field<Int16>("FMD_ID");
            Int16 con_id = DR.Field<Int16>("con_id");
            Int16 Cit_Id = DR.Field<Int16>("Cit_Id");
            Int16 nat_id = DR.Field<Int16>("nat_id");
            byte Gender_id = DR.Field<byte>("Gender_id");
            string Ecust_name = DR.Field<string>("Ecust_name");
            string Fmd_EName = DR.Field<string>("Fmd_EName");
            string Cit_EName = DR.Field<string>("Cit_EName");
            string Con_EName = DR.Field<string>("Con_EName");
            string E_Address = DR.Field<string>("E_Address");
            string Nat_Ename = DR.Field<string>("Nat_Ename");
            string Cust_Type_EName = DR.Field<string>("Cust_Type_EName");
            string Gender_Ename = DR.Field<string>("Gender_Ename");
            int T_id = Convert.ToInt32(cbo_term.SelectedValue );
            int user_id = connection.user_id;
            Int16 FORM_Id = frm_id;

            //-------------------------------------------------------------------------------------------------
            DataRowView DRV1 = Grd_Cust_Currency_bs.Current as DataRowView;
            DataRow DR1 = DRV1.Row;

            Int16 Cur_ID = DR1.Field<Int16>("Cur_ID");
            string Cur_ANAME = DR1.Field<string>("Cur_ANAME");
            decimal UPPER_LIMIT = DR1.Field<decimal>("UPPER_LIMIT");
            string DC_Aname = DR1.Field<string>("DC_Aname");
            string Cur_ENAME = DR1.Field<string>("Cur_ENAME");
            string DC_Ename = DR1.Field<string>("DC_Ename");
            int Acc_Id = DR1.Field<int>("Acc_Id");
            string Acc_AName = DR1.Field<string>("Acc_AName");
            string Acc_EName = DR1.Field<string>("Acc_EName");

            //---------------------------------------------------------------------------------------------------
            Decimal daily_uper_limt = Convert.ToDecimal(txt_daily_uper_limt.Text);
            Decimal per_tran_rem = Convert.ToDecimal(txt_per_tran_rem.Text);
            //Decimal open_uper_limt = Convert.ToDecimal(txt_open_uper_limt.Text);//--حدود مفتوحة
            Decimal buy_rate = Convert.ToDecimal(Txt_buy_rate.Text);
            Decimal sell_rate = Convert.ToDecimal(Txt_sell_rate.Text);
            int cbo_deal_nat = Convert.ToInt32(cbo_deal_nature.SelectedValue);
            //-------------------------------------------------------------------------------------------------

            byte sub_cust_State = Convert.ToByte(CboType_sub_cust.SelectedIndex);
       
            #region Validation

            //if (daily_uper_limt > UPPER_LIMIT)
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "The upper limit of the Money transfer is greater than the value of a for highest limit" : "قيمــة الحد الاعلى للتحويــل اليومي اكبــر مـن قيمة الحــد الاعلى للتحويـــل ", MyGeneral_Lib.LblCap);
            //    txt_daily_uper_limt.Focus();
            //    //return;
            //}

            //if (per_tran_rem > daily_uper_limt)
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Value per transaction limit is greater than the value of the daily conversion limit" : "قيمــة حد تحويل المعاملة الواحدة اكبــر مـن قيمة الحد الاعلى التحويل اليومي ", MyGeneral_Lib.LblCap);
            //    txt_per_tran_rem.Focus();
            //    return;
            //}


            //if (open_uper_limt > UPPER_LIMIT)
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "The open upper limit  is greater than the value of a for highest limit" : "قيمــة الحد المفتوح اكبــر مـن قيمة الحــد الاعلى للتحويـــل ", MyGeneral_Lib.LblCap);
            //    txt_open_uper_limt.Focus();
            //    return;
            //}



            //if (Chk_Cur.Checked == true && Check.Checked == true)
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Please select the type of boundaries and daily limits of open limits" : "الرجاء قم بأختيار نوع الحدود اما حدود يومية أو حدود مفتوحة", MyGeneral_Lib.LblCap);
            //    return;

            //}



            //if (Check.Checked == true)
            //{

            //    if (txt_open_uper_limt.Text == "0.000")
            //    {
            //        MessageBox.Show(connection.Lang_id == 2 ? "Please enter your open boundary of highest limit value" : "الرجاء ادخال قيمة الحد الاعلى الخاص  بالحدود المفتوحة", MyGeneral_Lib.LblCap);
            //        txt_open_uper_limt.Focus();
            //        return;
            //    }
            //}

            //if (Chk_Cur.Checked == false && Check.Checked == false)
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Please check the boundaries" : "الرجاء قم بتأشير الحدود", MyGeneral_Lib.LblCap);
            //    return;

            //}

            //if (Chk_Cur.Checked == true && Check.Checked == true)
            //{

            //    MessageBox.Show(connection.Lang_id == 2 ? "Please select the type of boundaries and daily limits of open limits" : "الرجاء قم بأختيار نوع الحدود اما حدود يومية أو حدود مفتوحة", MyGeneral_Lib.LblCap);
            //    return;

            //}
            #endregion



            cur_oper_Tbl = connection.SQLDS.Tables["Customer_oper_cur_Tbl"].DefaultView.ToTable(false, "cur_oper_chk", "cur_oper_id").Select("cur_oper_chk > 0").CopyToDataTable();





            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Customer_Online_Add_Upd";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                //   connection.SQLCMD.Parameters.AddWithValue("@Rec_cust_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_ID", Sub_Cust_ID);
                connection.SQLCMD.Parameters.AddWithValue("@ASub_CustName", Cust_Name);
                connection.SQLCMD.Parameters.AddWithValue("@ESub_CustName", Cust_EName);
                connection.SQLCMD.Parameters.AddWithValue("@City_Id", Cit_Id);
                connection.SQLCMD.Parameters.AddWithValue("@City_Aname", Cit_AName);
                connection.SQLCMD.Parameters.AddWithValue("@City_Ename", Cit_EName);
                connection.SQLCMD.Parameters.AddWithValue("@Con_Id", con_id);
                connection.SQLCMD.Parameters.AddWithValue("@Con_AName", Con_AName);
                connection.SQLCMD.Parameters.AddWithValue("@Con_EName", Con_EName);
                connection.SQLCMD.Parameters.AddWithValue("@A_ADDRESS", A_Address);
                connection.SQLCMD.Parameters.AddWithValue("@PHONE", Phone);
                connection.SQLCMD.Parameters.AddWithValue("@EMAIL", Email);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_ID", FMD_ID);
                connection.SQLCMD.Parameters.AddWithValue("@Fmd_AName", Fmd_AName);
                connection.SQLCMD.Parameters.AddWithValue("@Fmd_EName", Fmd_EName);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_NO", frm_doc_no);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_DA", frm_doc_Da1);
                connection.SQLCMD.Parameters.AddWithValue("@DOC_EDA", doc_eda1);
                connection.SQLCMD.Parameters.AddWithValue("@FRM_DOC_IS", frm_doc_Is);
                connection.SQLCMD.Parameters.AddWithValue("@NAT_ID", nat_id);
                connection.SQLCMD.Parameters.AddWithValue("@Nat_AName", Nat_Aname);
                connection.SQLCMD.Parameters.AddWithValue("@Nat_EName", Nat_Ename);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_Id", Cur_ID);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_ANAME", Cur_ANAME);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_ENAME", Cur_ENAME);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", user_id);
                connection.SQLCMD.Parameters.AddWithValue("@chk_daily", Chk_Cur.Checked);
                connection.SQLCMD.Parameters.AddWithValue("@Daily_Transction", daily_uper_limt);
                connection.SQLCMD.Parameters.AddWithValue("@Rem_Transction", per_tran_rem);
                connection.SQLCMD.Parameters.AddWithValue("@chk_open", 0);
                connection.SQLCMD.Parameters.AddWithValue("@Up_Limt_Rem_Transction", 0);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_Id", Acc_Id);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_AName", Acc_AName);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_EName", Acc_EName);
                connection.SQLCMD.Parameters.AddWithValue("@FORM_Id", FORM_Id);
                connection.SQLCMD.Parameters.AddWithValue("@T_Id", T_id);
                connection.SQLCMD.Parameters.AddWithValue("@State_sub_cust", sub_cust_State);
                connection.SQLCMD.Parameters.AddWithValue("@Cust_Code", txt_Cust_Code.Text);
              //  connection.SQLCMD.Parameters.AddWithValue("@ver_check", Chk_Cust.Checked == true ? 1 : 0);
                connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.AddWithValue("@Buy_rate",buy_rate);
                connection.SQLCMD.Parameters.AddWithValue("@Sell_rate",sell_rate);
                connection.SQLCMD.Parameters.AddWithValue("@chk_exch_price",Convert.ToInt16(CHK1.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@cbo_deal_nat", cbo_deal_nat);
                connection.SQLCMD.Parameters.AddWithValue("@CHK_exch_agent", Convert.ToInt16(CHK2.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@SubCust_Financial_cur_oper_Tbl", cur_oper_Tbl);
                connection.SQLCMD.Parameters.AddWithValue("@TimeZone", cbo_time_zone.SelectedValue.ToString());

               // MyGeneral_Lib.Copytocliptext("Customer_Online_Add_Upd", connection.SQLCMD);
                IDataReader obj = connection.SQLCMD.ExecuteReader();
              
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Customer_Online_Add_Upd");
              
                obj.Close();
                connection.SQLCS.Close();
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                MessageBox.Show(connection.Lang_id == 2 ? " Please choose operation for this currency" : " يرجى اختيار نوع العملية لهذه العملة ", MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }

            this.Close();

            //-----------------------------------------------------------------------------------------------
        }

        private void cbo_term_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chag_term)
            {
                Grd_Cust_Online_fill();

            }
        }
        //-----------------------------------------------------------------------------------------------
        private void Chk_Exch_Price_CheckedChanged(object sender, EventArgs e)
        {
            if (CHK1.Checked == true)
            {
                Txt_sell_rate.Enabled = true;
                Txt_buy_rate.Enabled = true;
                Txt_sell_rate.Text = "0.000";
                Txt_buy_rate.Text = "0.000";
                CHK2.Checked = false;
            }
            else
            {
                Txt_sell_rate.Enabled = false;
                Txt_buy_rate.Enabled = false;
                Txt_sell_rate.Text = "0.000";
                Txt_buy_rate.Text = "0.000";
                CHK2.Checked = true;

            }
        }

        private void CHK2_CheckedChanged(object sender, EventArgs e)
        {
            if (CHK2.Checked == true)
            {
                CHK1.Checked = false;
                Txt_sell_rate.Text = "0.000";
                Txt_buy_rate.Text = "0.000";
            }
            else
            {
                CHK1.Checked = true;
                Txt_sell_rate.Text = "0.000";
                Txt_buy_rate.Text = "0.000";
            }
        }

        private void Grd_Cust_Currency_SelectionChanged(object sender, EventArgs e)
        {

            int sub_cust_id = Convert.ToInt32(((DataRowView)Grd_Cust_Online_bs.Current).Row["Cust_ID"]);
            Int16 cur_id = Convert.ToInt16(((DataRowView)Grd_Cust_Currency_bs.Current).Row["CUR_ID"]);


            SqlTxt3 = "select distinct 0 as cur_oper_chk, cur_oper_id,cur_oper_aname,cur_oper_ename,c.CUR_ID ,d.CUST_ID ,ACC_Id, Cur_ANAME,Cur_ENAME,d.ACUST_NAME,d.ECUST_NAME "
               + " From Financial_cur_oper a,  CUSTOMERS_ACCOUNTS  b ,  Cur_Tbl c, CUSTOMERS d  "
               + " where  b.cur_id = c.cur_id  "
               + " and b.cust_id= d.cust_id "
               + " and  b.cust_id = " + sub_cust_id
               + " and b.cur_id = " + cur_id ;

            connection.SqlExec(SqlTxt3, "Customer_oper_cur_Tbl");


            if (connection.SQLDS.Tables["Customer_oper_cur_Tbl"].Rows.Count > 0)
            {


                DataTable DT_oper_cur = new DataTable();
                grd_cur_oper_bs.DataSource = connection.SQLDS.Tables["Customer_oper_cur_Tbl"];


                grd_cur_oper.DataSource = grd_cur_oper_bs;




                //chag_Grd_Cust_Online = true;
                //Grd_Cust_Online_SelectionChanged(null, null);

            }
            else
            {
                grd_cur_oper.DataSource = null;

            }



        }



    }
}