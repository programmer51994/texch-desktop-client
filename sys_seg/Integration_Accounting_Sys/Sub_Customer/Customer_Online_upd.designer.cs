﻿namespace Integration_Accounting_Sys
{
    partial class Customer_Online_upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel65 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel66 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Cust_Main_Onilne = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel58 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel59 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel60 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel61 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel62 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel63 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel64 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Cur_Aname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Check = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.CboType_sub_cust = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.flowLayoutPanel99 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel100 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel101 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel102 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel103 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel104 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel105 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel106 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel107 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel108 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel109 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel110 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel111 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel112 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel113 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel114 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel115 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel116 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel117 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel118 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel119 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel120 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel121 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel122 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel123 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel124 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel125 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel126 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel127 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel128 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel129 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel130 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel131 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel132 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel133 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel134 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel135 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel136 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel137 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel138 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel139 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel140 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel141 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel142 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel143 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel144 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel145 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel146 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel147 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel148 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel149 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel150 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel151 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel152 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel153 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel154 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel155 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel156 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel157 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel158 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel159 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel160 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel161 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel162 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.CHK1 = new System.Windows.Forms.CheckBox();
            this.Txt_sell_rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_buy_rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_per_tran_rem_upd = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_daily_uper_limt_upd = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_TErminal = new System.Windows.Forms.TextBox();
            this.cbo_deal_nature = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.CHK2 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.grd_cur_oper = new System.Windows.Forms.DataGridView();
            this.Column30 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cur_oper_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_time_zone = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel33.SuspendLayout();
            this.flowLayoutPanel35.SuspendLayout();
            this.flowLayoutPanel37.SuspendLayout();
            this.flowLayoutPanel39.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel43.SuspendLayout();
            this.flowLayoutPanel45.SuspendLayout();
            this.flowLayoutPanel19.SuspendLayout();
            this.flowLayoutPanel21.SuspendLayout();
            this.flowLayoutPanel23.SuspendLayout();
            this.flowLayoutPanel25.SuspendLayout();
            this.flowLayoutPanel27.SuspendLayout();
            this.flowLayoutPanel29.SuspendLayout();
            this.flowLayoutPanel47.SuspendLayout();
            this.flowLayoutPanel65.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Main_Onilne)).BeginInit();
            this.flowLayoutPanel49.SuspendLayout();
            this.flowLayoutPanel51.SuspendLayout();
            this.flowLayoutPanel53.SuspendLayout();
            this.flowLayoutPanel55.SuspendLayout();
            this.flowLayoutPanel57.SuspendLayout();
            this.flowLayoutPanel59.SuspendLayout();
            this.flowLayoutPanel61.SuspendLayout();
            this.flowLayoutPanel63.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel15.SuspendLayout();
            this.flowLayoutPanel99.SuspendLayout();
            this.flowLayoutPanel101.SuspendLayout();
            this.flowLayoutPanel103.SuspendLayout();
            this.flowLayoutPanel105.SuspendLayout();
            this.flowLayoutPanel107.SuspendLayout();
            this.flowLayoutPanel109.SuspendLayout();
            this.flowLayoutPanel111.SuspendLayout();
            this.flowLayoutPanel113.SuspendLayout();
            this.flowLayoutPanel115.SuspendLayout();
            this.flowLayoutPanel117.SuspendLayout();
            this.flowLayoutPanel119.SuspendLayout();
            this.flowLayoutPanel121.SuspendLayout();
            this.flowLayoutPanel123.SuspendLayout();
            this.flowLayoutPanel125.SuspendLayout();
            this.flowLayoutPanel127.SuspendLayout();
            this.flowLayoutPanel129.SuspendLayout();
            this.flowLayoutPanel131.SuspendLayout();
            this.flowLayoutPanel133.SuspendLayout();
            this.flowLayoutPanel135.SuspendLayout();
            this.flowLayoutPanel137.SuspendLayout();
            this.flowLayoutPanel139.SuspendLayout();
            this.flowLayoutPanel141.SuspendLayout();
            this.flowLayoutPanel143.SuspendLayout();
            this.flowLayoutPanel145.SuspendLayout();
            this.flowLayoutPanel147.SuspendLayout();
            this.flowLayoutPanel149.SuspendLayout();
            this.flowLayoutPanel151.SuspendLayout();
            this.flowLayoutPanel153.SuspendLayout();
            this.flowLayoutPanel155.SuspendLayout();
            this.flowLayoutPanel157.SuspendLayout();
            this.flowLayoutPanel159.SuspendLayout();
            this.flowLayoutPanel161.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cur_oper)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(368, 7);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(240, 23);
            this.TxtUser.TabIndex = 906;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(685, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(160, 23);
            this.TxtIn_Rec_Date.TabIndex = 908;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel31.Location = new System.Drawing.Point(-6, 33);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(1030, 2);
            this.flowLayoutPanel31.TabIndex = 932;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(8, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel32.TabIndex = 630;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(71, 10);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel33.TabIndex = 924;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel34.TabIndex = 630;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel35.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(71, 18);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel35.TabIndex = 925;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel36.TabIndex = 630;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel37.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel37.TabIndex = 924;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel38.TabIndex = 630;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel39.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(283, 26);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel39.TabIndex = 931;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel40.TabIndex = 630;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel41.TabIndex = 924;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel42.TabIndex = 630;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel43.TabIndex = 925;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel44.TabIndex = 630;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel45.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel45.TabIndex = 924;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel46.TabIndex = 630;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel20);
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel23);
            this.flowLayoutPanel19.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(1030, 2);
            this.flowLayoutPanel19.TabIndex = 933;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(8, 3);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel20.TabIndex = 630;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel22);
            this.flowLayoutPanel21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel21.Location = new System.Drawing.Point(71, 10);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel21.TabIndex = 924;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel22.TabIndex = 630;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel25);
            this.flowLayoutPanel23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel23.Location = new System.Drawing.Point(71, 18);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel23.TabIndex = 925;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel24.TabIndex = 630;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel25.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel25.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel25.TabIndex = 924;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel26.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel26.TabIndex = 630;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel28);
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel27.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(283, 26);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel27.TabIndex = 931;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel28.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel28.TabIndex = 630;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel29.Controls.Add(this.flowLayoutPanel30);
            this.flowLayoutPanel29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel29.TabIndex = 924;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel30.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel30.TabIndex = 630;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel47.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel47.Controls.Add(this.flowLayoutPanel65);
            this.flowLayoutPanel47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel47.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel47.TabIndex = 925;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel48.TabIndex = 630;
            // 
            // flowLayoutPanel65
            // 
            this.flowLayoutPanel65.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel65.Controls.Add(this.flowLayoutPanel66);
            this.flowLayoutPanel65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel65.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel65.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel65.Name = "flowLayoutPanel65";
            this.flowLayoutPanel65.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel65.TabIndex = 924;
            // 
            // flowLayoutPanel66
            // 
            this.flowLayoutPanel66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel66.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel66.Name = "flowLayoutPanel66";
            this.flowLayoutPanel66.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel66.TabIndex = 630;
            // 
            // Grd_Cust_Main_Onilne
            // 
            this.Grd_Cust_Main_Onilne.AllowUserToAddRows = false;
            this.Grd_Cust_Main_Onilne.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Main_Onilne.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Cust_Main_Onilne.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Main_Onilne.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust_Main_Onilne.ColumnHeadersHeight = 35;
            this.Grd_Cust_Main_Onilne.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column21,
            this.Column22,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column19,
            this.Column12,
            this.Column9,
            this.Column11,
            this.Column10,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column20});
            this.Grd_Cust_Main_Onilne.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Cust_Main_Onilne.Location = new System.Drawing.Point(7, 90);
            this.Grd_Cust_Main_Onilne.Name = "Grd_Cust_Main_Onilne";
            this.Grd_Cust_Main_Onilne.ReadOnly = true;
            this.Grd_Cust_Main_Onilne.RowHeadersWidth = 20;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Main_Onilne.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cust_Main_Onilne.Size = new System.Drawing.Size(835, 100);
            this.Grd_Cust_Main_Onilne.TabIndex = 941;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Rec_cust_id";
            this.Column1.HeaderText = "Rec_cust_id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Cust_Online_Main_Id";
            this.Column2.HeaderText = "cust_online_main_id";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Sub_Cust_ID";
            this.Column3.HeaderText = "sub_cust_id";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "ASub_CustName";
            this.Column4.HeaderText = "الاســــم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 210;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "City_Id";
            this.Column5.HeaderText = "city_id";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Acc_AName";
            this.Column21.HeaderText = "اسم الحساب";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 150;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "Cust_Code";
            this.Column22.HeaderText = "رمـــز الحســـاب";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "City_Aname";
            this.Column6.HeaderText = "المديـــنة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Con_Id";
            this.Column7.HeaderText = "con_id";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Con_AName";
            this.Column8.HeaderText = "البلد";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Nat_AName";
            this.Column19.HeaderText = "الجنســـية";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "FRM_DOC_ID";
            this.Column12.HeaderText = "frm_doc_id";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Visible = false;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "A_ADDRESS";
            this.Column9.HeaderText = "العنـــــوان";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 250;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "EMAIL";
            this.Column11.HeaderText = "البريد الالكتروني";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "PHONE";
            this.Column10.HeaderText = "الهاتـــف";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Fmd_AName";
            this.Column13.HeaderText = "نــوع الوثيقة";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "FRM_DOC_NO";
            this.Column14.HeaderText = "رقـــم الوثيقة";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "FRM_DOC_DA";
            this.Column15.HeaderText = "تاريـــخ الوثيقة";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "DOC_EDA";
            this.Column16.HeaderText = "تاريخ انتهائها";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "FRM_DOC_IS";
            this.Column17.HeaderText = "جهـــة الاصدار";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "NAT_ID";
            this.Column18.HeaderText = "nat_id";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Visible = false;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "Acc_Id";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Visible = false;
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel49.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(108, 329);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(775, 1);
            this.flowLayoutPanel49.TabIndex = 700;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(-247, 3);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel50.TabIndex = 630;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel51.Location = new System.Drawing.Point(-184, 10);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel51.TabIndex = 924;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel52.TabIndex = 630;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel53.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(-184, 18);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel53.TabIndex = 925;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel54.TabIndex = 630;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel55.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel55.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel55.TabIndex = 924;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel56.TabIndex = 630;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel58);
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel59);
            this.flowLayoutPanel57.Controls.Add(this.flowLayoutPanel61);
            this.flowLayoutPanel57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel57.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(28, 26);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel57.TabIndex = 931;
            // 
            // flowLayoutPanel58
            // 
            this.flowLayoutPanel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel58.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel58.Name = "flowLayoutPanel58";
            this.flowLayoutPanel58.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel58.TabIndex = 630;
            // 
            // flowLayoutPanel59
            // 
            this.flowLayoutPanel59.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel59.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel59.Controls.Add(this.flowLayoutPanel60);
            this.flowLayoutPanel59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel59.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel59.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel59.Name = "flowLayoutPanel59";
            this.flowLayoutPanel59.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel59.TabIndex = 924;
            // 
            // flowLayoutPanel60
            // 
            this.flowLayoutPanel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel60.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel60.Name = "flowLayoutPanel60";
            this.flowLayoutPanel60.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel60.TabIndex = 630;
            // 
            // flowLayoutPanel61
            // 
            this.flowLayoutPanel61.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel62);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel63);
            this.flowLayoutPanel61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel61.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel61.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel61.Name = "flowLayoutPanel61";
            this.flowLayoutPanel61.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel61.TabIndex = 925;
            // 
            // flowLayoutPanel62
            // 
            this.flowLayoutPanel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel62.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel62.Name = "flowLayoutPanel62";
            this.flowLayoutPanel62.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel62.TabIndex = 630;
            // 
            // flowLayoutPanel63
            // 
            this.flowLayoutPanel63.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel63.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel63.Controls.Add(this.flowLayoutPanel64);
            this.flowLayoutPanel63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel63.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel63.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel63.Name = "flowLayoutPanel63";
            this.flowLayoutPanel63.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel63.TabIndex = 924;
            // 
            // flowLayoutPanel64
            // 
            this.flowLayoutPanel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel64.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel64.Name = "flowLayoutPanel64";
            this.flowLayoutPanel64.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel64.TabIndex = 630;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-249, 34);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1021, 2);
            this.flowLayoutPanel1.TabIndex = 944;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-1, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel2.TabIndex = 630;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(62, 10);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel3.TabIndex = 924;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(62, 18);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel5.TabIndex = 925;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel6.TabIndex = 630;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel7.TabIndex = 924;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel8.TabIndex = 630;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(274, 26);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel9.TabIndex = 931;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel11.TabIndex = 924;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel12.TabIndex = 630;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel13.TabIndex = 925;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel14.TabIndex = 630;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel15.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel15.TabIndex = 924;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel16.TabIndex = 630;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(3, 319);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 14);
            this.label8.TabIndex = 944;
            this.label8.Text = "تفاصيــــل العملة..\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(10, 360);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(60, 14);
            this.label2.TabIndex = 945;
            this.label2.Text = "العملـــة :";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(425, 437);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 29);
            this.button2.TabIndex = 948;
            this.button2.Text = "انهـــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(344, 437);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(81, 29);
            this.Btn_Add.TabIndex = 947;
            this.Btn_Add.Text = "موافـــق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(262, 338);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(1, 89);
            this.flowLayoutPanel17.TabIndex = 949;
            // 
            // Txt_Cur_Aname
            // 
            this.Txt_Cur_Aname.BackColor = System.Drawing.Color.White;
            this.Txt_Cur_Aname.Enabled = false;
            this.Txt_Cur_Aname.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cur_Aname.Location = new System.Drawing.Point(74, 356);
            this.Txt_Cur_Aname.Name = "Txt_Cur_Aname";
            this.Txt_Cur_Aname.Size = new System.Drawing.Size(182, 23);
            this.Txt_Cur_Aname.TabIndex = 950;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(265, 401);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(176, 14);
            this.label3.TabIndex = 952;
            this.label3.Text = "حد المعاملــــــة الواحــــــــــدة :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(265, 372);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(174, 14);
            this.label4.TabIndex = 951;
            this.label4.Text = "الحد الاعلى للتحويل اليومي :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(631, 6);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(48, 16);
            this.label7.TabIndex = 961;
            this.label7.Text = "التاريـخ:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(5, 6);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(251, 23);
            this.TxtTerm_Name.TabIndex = 903;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(278, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 907;
            this.label1.Text = "المستخـــــدم:";
            // 
            // Check
            // 
            this.Check.AutoSize = true;
            this.Check.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check.ForeColor = System.Drawing.Color.Maroon;
            this.Check.Location = new System.Drawing.Point(271, 334);
            this.Check.Name = "Check";
            this.Check.Size = new System.Drawing.Size(150, 17);
            this.Check.TabIndex = 962;
            this.Check.Text = "الحـــــــــدود اليوميــــة....";
            this.Check.UseVisualStyleBackColor = true;
            this.Check.CheckedChanged += new System.EventHandler(this.Check_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(592, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 16);
            this.label10.TabIndex = 965;
            this.label10.Text = "حــالة الثانوي :";
            // 
            // CboType_sub_cust
            // 
            this.CboType_sub_cust.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboType_sub_cust.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType_sub_cust.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboType_sub_cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboType_sub_cust.FormattingEnabled = true;
            this.CboType_sub_cust.Items.AddRange(new object[] {
            "غير فعال",
            "فعال"});
            this.CboType_sub_cust.Location = new System.Drawing.Point(683, 37);
            this.CboType_sub_cust.Name = "CboType_sub_cust";
            this.CboType_sub_cust.Size = new System.Drawing.Size(157, 24);
            this.CboType_sub_cust.TabIndex = 964;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 471);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(851, 22);
            this.statusStrip1.TabIndex = 979;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // flowLayoutPanel99
            // 
            this.flowLayoutPanel99.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.flowLayoutPanel99.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel100);
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel101);
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel103);
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel107);
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel115);
            this.flowLayoutPanel99.Controls.Add(this.flowLayoutPanel131);
            this.flowLayoutPanel99.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel99.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel99.Location = new System.Drawing.Point(-5, 435);
            this.flowLayoutPanel99.Name = "flowLayoutPanel99";
            this.flowLayoutPanel99.Size = new System.Drawing.Size(861, 1);
            this.flowLayoutPanel99.TabIndex = 980;
            // 
            // flowLayoutPanel100
            // 
            this.flowLayoutPanel100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel100.Location = new System.Drawing.Point(-161, 3);
            this.flowLayoutPanel100.Name = "flowLayoutPanel100";
            this.flowLayoutPanel100.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel100.TabIndex = 630;
            // 
            // flowLayoutPanel101
            // 
            this.flowLayoutPanel101.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel101.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel102);
            this.flowLayoutPanel101.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel101.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel101.Location = new System.Drawing.Point(-98, 10);
            this.flowLayoutPanel101.Name = "flowLayoutPanel101";
            this.flowLayoutPanel101.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel101.TabIndex = 924;
            // 
            // flowLayoutPanel102
            // 
            this.flowLayoutPanel102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel102.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel102.Name = "flowLayoutPanel102";
            this.flowLayoutPanel102.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel102.TabIndex = 630;
            // 
            // flowLayoutPanel103
            // 
            this.flowLayoutPanel103.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel103.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel103.Controls.Add(this.flowLayoutPanel104);
            this.flowLayoutPanel103.Controls.Add(this.flowLayoutPanel105);
            this.flowLayoutPanel103.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel103.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel103.Location = new System.Drawing.Point(-98, 18);
            this.flowLayoutPanel103.Name = "flowLayoutPanel103";
            this.flowLayoutPanel103.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel103.TabIndex = 925;
            // 
            // flowLayoutPanel104
            // 
            this.flowLayoutPanel104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel104.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel104.Name = "flowLayoutPanel104";
            this.flowLayoutPanel104.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel104.TabIndex = 630;
            // 
            // flowLayoutPanel105
            // 
            this.flowLayoutPanel105.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel105.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel105.Controls.Add(this.flowLayoutPanel106);
            this.flowLayoutPanel105.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel105.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel105.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel105.Name = "flowLayoutPanel105";
            this.flowLayoutPanel105.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel105.TabIndex = 924;
            // 
            // flowLayoutPanel106
            // 
            this.flowLayoutPanel106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel106.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel106.Name = "flowLayoutPanel106";
            this.flowLayoutPanel106.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel106.TabIndex = 630;
            // 
            // flowLayoutPanel107
            // 
            this.flowLayoutPanel107.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel107.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel107.Controls.Add(this.flowLayoutPanel108);
            this.flowLayoutPanel107.Controls.Add(this.flowLayoutPanel109);
            this.flowLayoutPanel107.Controls.Add(this.flowLayoutPanel111);
            this.flowLayoutPanel107.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel107.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel107.Location = new System.Drawing.Point(114, 26);
            this.flowLayoutPanel107.Name = "flowLayoutPanel107";
            this.flowLayoutPanel107.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel107.TabIndex = 931;
            // 
            // flowLayoutPanel108
            // 
            this.flowLayoutPanel108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel108.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel108.Name = "flowLayoutPanel108";
            this.flowLayoutPanel108.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel108.TabIndex = 630;
            // 
            // flowLayoutPanel109
            // 
            this.flowLayoutPanel109.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel109.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel109.Controls.Add(this.flowLayoutPanel110);
            this.flowLayoutPanel109.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel109.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel109.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel109.Name = "flowLayoutPanel109";
            this.flowLayoutPanel109.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel109.TabIndex = 924;
            // 
            // flowLayoutPanel110
            // 
            this.flowLayoutPanel110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel110.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel110.Name = "flowLayoutPanel110";
            this.flowLayoutPanel110.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel110.TabIndex = 630;
            // 
            // flowLayoutPanel111
            // 
            this.flowLayoutPanel111.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel111.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel112);
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel113);
            this.flowLayoutPanel111.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel111.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel111.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel111.Name = "flowLayoutPanel111";
            this.flowLayoutPanel111.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel111.TabIndex = 925;
            // 
            // flowLayoutPanel112
            // 
            this.flowLayoutPanel112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel112.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel112.Name = "flowLayoutPanel112";
            this.flowLayoutPanel112.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel112.TabIndex = 630;
            // 
            // flowLayoutPanel113
            // 
            this.flowLayoutPanel113.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel113.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel113.Controls.Add(this.flowLayoutPanel114);
            this.flowLayoutPanel113.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel113.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel113.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel113.Name = "flowLayoutPanel113";
            this.flowLayoutPanel113.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel113.TabIndex = 924;
            // 
            // flowLayoutPanel114
            // 
            this.flowLayoutPanel114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel114.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel114.Name = "flowLayoutPanel114";
            this.flowLayoutPanel114.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel114.TabIndex = 630;
            // 
            // flowLayoutPanel115
            // 
            this.flowLayoutPanel115.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel115.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel115.Controls.Add(this.flowLayoutPanel116);
            this.flowLayoutPanel115.Controls.Add(this.flowLayoutPanel117);
            this.flowLayoutPanel115.Controls.Add(this.flowLayoutPanel119);
            this.flowLayoutPanel115.Controls.Add(this.flowLayoutPanel123);
            this.flowLayoutPanel115.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel115.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel115.Location = new System.Drawing.Point(60, 34);
            this.flowLayoutPanel115.Name = "flowLayoutPanel115";
            this.flowLayoutPanel115.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel115.TabIndex = 973;
            // 
            // flowLayoutPanel116
            // 
            this.flowLayoutPanel116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel116.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel116.Name = "flowLayoutPanel116";
            this.flowLayoutPanel116.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel116.TabIndex = 630;
            // 
            // flowLayoutPanel117
            // 
            this.flowLayoutPanel117.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel117.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel117.Controls.Add(this.flowLayoutPanel118);
            this.flowLayoutPanel117.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel117.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel117.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel117.Name = "flowLayoutPanel117";
            this.flowLayoutPanel117.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel117.TabIndex = 924;
            // 
            // flowLayoutPanel118
            // 
            this.flowLayoutPanel118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel118.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel118.Name = "flowLayoutPanel118";
            this.flowLayoutPanel118.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel118.TabIndex = 630;
            // 
            // flowLayoutPanel119
            // 
            this.flowLayoutPanel119.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel119.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel119.Controls.Add(this.flowLayoutPanel120);
            this.flowLayoutPanel119.Controls.Add(this.flowLayoutPanel121);
            this.flowLayoutPanel119.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel119.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel119.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel119.Name = "flowLayoutPanel119";
            this.flowLayoutPanel119.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel119.TabIndex = 925;
            // 
            // flowLayoutPanel120
            // 
            this.flowLayoutPanel120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel120.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel120.Name = "flowLayoutPanel120";
            this.flowLayoutPanel120.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel120.TabIndex = 630;
            // 
            // flowLayoutPanel121
            // 
            this.flowLayoutPanel121.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel121.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel121.Controls.Add(this.flowLayoutPanel122);
            this.flowLayoutPanel121.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel121.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel121.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel121.Name = "flowLayoutPanel121";
            this.flowLayoutPanel121.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel121.TabIndex = 924;
            // 
            // flowLayoutPanel122
            // 
            this.flowLayoutPanel122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel122.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel122.Name = "flowLayoutPanel122";
            this.flowLayoutPanel122.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel122.TabIndex = 630;
            // 
            // flowLayoutPanel123
            // 
            this.flowLayoutPanel123.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel123.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel123.Controls.Add(this.flowLayoutPanel124);
            this.flowLayoutPanel123.Controls.Add(this.flowLayoutPanel125);
            this.flowLayoutPanel123.Controls.Add(this.flowLayoutPanel127);
            this.flowLayoutPanel123.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel123.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel123.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel123.Name = "flowLayoutPanel123";
            this.flowLayoutPanel123.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel123.TabIndex = 931;
            // 
            // flowLayoutPanel124
            // 
            this.flowLayoutPanel124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel124.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel124.Name = "flowLayoutPanel124";
            this.flowLayoutPanel124.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel124.TabIndex = 630;
            // 
            // flowLayoutPanel125
            // 
            this.flowLayoutPanel125.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel125.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel126);
            this.flowLayoutPanel125.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel125.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel125.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel125.Name = "flowLayoutPanel125";
            this.flowLayoutPanel125.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel125.TabIndex = 924;
            // 
            // flowLayoutPanel126
            // 
            this.flowLayoutPanel126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel126.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel126.Name = "flowLayoutPanel126";
            this.flowLayoutPanel126.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel126.TabIndex = 630;
            // 
            // flowLayoutPanel127
            // 
            this.flowLayoutPanel127.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel127.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel128);
            this.flowLayoutPanel127.Controls.Add(this.flowLayoutPanel129);
            this.flowLayoutPanel127.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel127.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel127.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel127.Name = "flowLayoutPanel127";
            this.flowLayoutPanel127.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel127.TabIndex = 925;
            // 
            // flowLayoutPanel128
            // 
            this.flowLayoutPanel128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel128.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel128.Name = "flowLayoutPanel128";
            this.flowLayoutPanel128.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel128.TabIndex = 630;
            // 
            // flowLayoutPanel129
            // 
            this.flowLayoutPanel129.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel129.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel129.Controls.Add(this.flowLayoutPanel130);
            this.flowLayoutPanel129.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel129.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel129.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel129.Name = "flowLayoutPanel129";
            this.flowLayoutPanel129.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel129.TabIndex = 924;
            // 
            // flowLayoutPanel130
            // 
            this.flowLayoutPanel130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel130.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel130.Name = "flowLayoutPanel130";
            this.flowLayoutPanel130.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel130.TabIndex = 630;
            // 
            // flowLayoutPanel131
            // 
            this.flowLayoutPanel131.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel131.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel131.Controls.Add(this.flowLayoutPanel132);
            this.flowLayoutPanel131.Controls.Add(this.flowLayoutPanel133);
            this.flowLayoutPanel131.Controls.Add(this.flowLayoutPanel135);
            this.flowLayoutPanel131.Controls.Add(this.flowLayoutPanel139);
            this.flowLayoutPanel131.Controls.Add(this.flowLayoutPanel147);
            this.flowLayoutPanel131.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel131.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel131.Location = new System.Drawing.Point(60, 42);
            this.flowLayoutPanel131.Name = "flowLayoutPanel131";
            this.flowLayoutPanel131.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel131.TabIndex = 974;
            // 
            // flowLayoutPanel132
            // 
            this.flowLayoutPanel132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel132.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel132.Name = "flowLayoutPanel132";
            this.flowLayoutPanel132.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel132.TabIndex = 630;
            // 
            // flowLayoutPanel133
            // 
            this.flowLayoutPanel133.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel133.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel133.Controls.Add(this.flowLayoutPanel134);
            this.flowLayoutPanel133.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel133.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel133.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel133.Name = "flowLayoutPanel133";
            this.flowLayoutPanel133.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel133.TabIndex = 924;
            // 
            // flowLayoutPanel134
            // 
            this.flowLayoutPanel134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel134.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel134.Name = "flowLayoutPanel134";
            this.flowLayoutPanel134.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel134.TabIndex = 630;
            // 
            // flowLayoutPanel135
            // 
            this.flowLayoutPanel135.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel135.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel136);
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel137);
            this.flowLayoutPanel135.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel135.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel135.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel135.Name = "flowLayoutPanel135";
            this.flowLayoutPanel135.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel135.TabIndex = 925;
            // 
            // flowLayoutPanel136
            // 
            this.flowLayoutPanel136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel136.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel136.Name = "flowLayoutPanel136";
            this.flowLayoutPanel136.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel136.TabIndex = 630;
            // 
            // flowLayoutPanel137
            // 
            this.flowLayoutPanel137.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel137.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel137.Controls.Add(this.flowLayoutPanel138);
            this.flowLayoutPanel137.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel137.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel137.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel137.Name = "flowLayoutPanel137";
            this.flowLayoutPanel137.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel137.TabIndex = 924;
            // 
            // flowLayoutPanel138
            // 
            this.flowLayoutPanel138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel138.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel138.Name = "flowLayoutPanel138";
            this.flowLayoutPanel138.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel138.TabIndex = 630;
            // 
            // flowLayoutPanel139
            // 
            this.flowLayoutPanel139.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel139.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel139.Controls.Add(this.flowLayoutPanel140);
            this.flowLayoutPanel139.Controls.Add(this.flowLayoutPanel141);
            this.flowLayoutPanel139.Controls.Add(this.flowLayoutPanel143);
            this.flowLayoutPanel139.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel139.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel139.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel139.Name = "flowLayoutPanel139";
            this.flowLayoutPanel139.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel139.TabIndex = 931;
            // 
            // flowLayoutPanel140
            // 
            this.flowLayoutPanel140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel140.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel140.Name = "flowLayoutPanel140";
            this.flowLayoutPanel140.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel140.TabIndex = 630;
            // 
            // flowLayoutPanel141
            // 
            this.flowLayoutPanel141.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel141.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel141.Controls.Add(this.flowLayoutPanel142);
            this.flowLayoutPanel141.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel141.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel141.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel141.Name = "flowLayoutPanel141";
            this.flowLayoutPanel141.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel141.TabIndex = 924;
            // 
            // flowLayoutPanel142
            // 
            this.flowLayoutPanel142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel142.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel142.Name = "flowLayoutPanel142";
            this.flowLayoutPanel142.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel142.TabIndex = 630;
            // 
            // flowLayoutPanel143
            // 
            this.flowLayoutPanel143.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel143.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel143.Controls.Add(this.flowLayoutPanel144);
            this.flowLayoutPanel143.Controls.Add(this.flowLayoutPanel145);
            this.flowLayoutPanel143.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel143.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel143.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel143.Name = "flowLayoutPanel143";
            this.flowLayoutPanel143.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel143.TabIndex = 925;
            // 
            // flowLayoutPanel144
            // 
            this.flowLayoutPanel144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel144.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel144.Name = "flowLayoutPanel144";
            this.flowLayoutPanel144.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel144.TabIndex = 630;
            // 
            // flowLayoutPanel145
            // 
            this.flowLayoutPanel145.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel145.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel146);
            this.flowLayoutPanel145.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel145.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel145.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel145.Name = "flowLayoutPanel145";
            this.flowLayoutPanel145.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel145.TabIndex = 924;
            // 
            // flowLayoutPanel146
            // 
            this.flowLayoutPanel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel146.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel146.Name = "flowLayoutPanel146";
            this.flowLayoutPanel146.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel146.TabIndex = 630;
            // 
            // flowLayoutPanel147
            // 
            this.flowLayoutPanel147.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel147.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel148);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel149);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel151);
            this.flowLayoutPanel147.Controls.Add(this.flowLayoutPanel155);
            this.flowLayoutPanel147.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel147.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel147.Location = new System.Drawing.Point(-3, 34);
            this.flowLayoutPanel147.Name = "flowLayoutPanel147";
            this.flowLayoutPanel147.Size = new System.Drawing.Size(798, 2);
            this.flowLayoutPanel147.TabIndex = 973;
            // 
            // flowLayoutPanel148
            // 
            this.flowLayoutPanel148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel148.Location = new System.Drawing.Point(-224, 3);
            this.flowLayoutPanel148.Name = "flowLayoutPanel148";
            this.flowLayoutPanel148.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel148.TabIndex = 630;
            // 
            // flowLayoutPanel149
            // 
            this.flowLayoutPanel149.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel149.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel149.Controls.Add(this.flowLayoutPanel150);
            this.flowLayoutPanel149.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel149.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel149.Location = new System.Drawing.Point(-161, 10);
            this.flowLayoutPanel149.Name = "flowLayoutPanel149";
            this.flowLayoutPanel149.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel149.TabIndex = 924;
            // 
            // flowLayoutPanel150
            // 
            this.flowLayoutPanel150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel150.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel150.Name = "flowLayoutPanel150";
            this.flowLayoutPanel150.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel150.TabIndex = 630;
            // 
            // flowLayoutPanel151
            // 
            this.flowLayoutPanel151.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel151.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel151.Controls.Add(this.flowLayoutPanel152);
            this.flowLayoutPanel151.Controls.Add(this.flowLayoutPanel153);
            this.flowLayoutPanel151.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel151.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel151.Location = new System.Drawing.Point(-161, 18);
            this.flowLayoutPanel151.Name = "flowLayoutPanel151";
            this.flowLayoutPanel151.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel151.TabIndex = 925;
            // 
            // flowLayoutPanel152
            // 
            this.flowLayoutPanel152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel152.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel152.Name = "flowLayoutPanel152";
            this.flowLayoutPanel152.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel152.TabIndex = 630;
            // 
            // flowLayoutPanel153
            // 
            this.flowLayoutPanel153.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel153.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel153.Controls.Add(this.flowLayoutPanel154);
            this.flowLayoutPanel153.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel153.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel153.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel153.Name = "flowLayoutPanel153";
            this.flowLayoutPanel153.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel153.TabIndex = 924;
            // 
            // flowLayoutPanel154
            // 
            this.flowLayoutPanel154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel154.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel154.Name = "flowLayoutPanel154";
            this.flowLayoutPanel154.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel154.TabIndex = 630;
            // 
            // flowLayoutPanel155
            // 
            this.flowLayoutPanel155.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel155.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel156);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel157);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel159);
            this.flowLayoutPanel155.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel155.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel155.Location = new System.Drawing.Point(51, 26);
            this.flowLayoutPanel155.Name = "flowLayoutPanel155";
            this.flowLayoutPanel155.Size = new System.Drawing.Size(744, 2);
            this.flowLayoutPanel155.TabIndex = 931;
            // 
            // flowLayoutPanel156
            // 
            this.flowLayoutPanel156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel156.Location = new System.Drawing.Point(-278, 3);
            this.flowLayoutPanel156.Name = "flowLayoutPanel156";
            this.flowLayoutPanel156.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel156.TabIndex = 630;
            // 
            // flowLayoutPanel157
            // 
            this.flowLayoutPanel157.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel157.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel157.Controls.Add(this.flowLayoutPanel158);
            this.flowLayoutPanel157.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel157.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel157.Location = new System.Drawing.Point(-215, 10);
            this.flowLayoutPanel157.Name = "flowLayoutPanel157";
            this.flowLayoutPanel157.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel157.TabIndex = 924;
            // 
            // flowLayoutPanel158
            // 
            this.flowLayoutPanel158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel158.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel158.Name = "flowLayoutPanel158";
            this.flowLayoutPanel158.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel158.TabIndex = 630;
            // 
            // flowLayoutPanel159
            // 
            this.flowLayoutPanel159.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel159.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel159.Controls.Add(this.flowLayoutPanel160);
            this.flowLayoutPanel159.Controls.Add(this.flowLayoutPanel161);
            this.flowLayoutPanel159.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel159.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel159.Location = new System.Drawing.Point(-215, 18);
            this.flowLayoutPanel159.Name = "flowLayoutPanel159";
            this.flowLayoutPanel159.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel159.TabIndex = 925;
            // 
            // flowLayoutPanel160
            // 
            this.flowLayoutPanel160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel160.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel160.Name = "flowLayoutPanel160";
            this.flowLayoutPanel160.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel160.TabIndex = 630;
            // 
            // flowLayoutPanel161
            // 
            this.flowLayoutPanel161.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel161.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel161.Controls.Add(this.flowLayoutPanel162);
            this.flowLayoutPanel161.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel161.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel161.Location = new System.Drawing.Point(-3, 10);
            this.flowLayoutPanel161.Name = "flowLayoutPanel161";
            this.flowLayoutPanel161.Size = new System.Drawing.Size(956, 2);
            this.flowLayoutPanel161.TabIndex = 924;
            // 
            // flowLayoutPanel162
            // 
            this.flowLayoutPanel162.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel162.Location = new System.Drawing.Point(-66, 3);
            this.flowLayoutPanel162.Name = "flowLayoutPanel162";
            this.flowLayoutPanel162.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel162.TabIndex = 630;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(592, 359);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(0, 14);
            this.label5.TabIndex = 957;
            this.label5.Visible = false;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(568, 339);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(1, 89);
            this.flowLayoutPanel18.TabIndex = 986;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Crimson;
            this.label14.Location = new System.Drawing.Point(816, 401);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(21, 14);
            this.label14.TabIndex = 1001;
            this.label14.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Crimson;
            this.label13.Location = new System.Drawing.Point(818, 372);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(21, 14);
            this.label13.TabIndex = 1000;
            this.label13.Text = "%";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(570, 401);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(137, 14);
            this.label19.TabIndex = 998;
            this.label19.Text = "مقـدار العمولة للشراء :";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(570, 372);
            this.label41.Name = "label41";
            this.label41.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label41.Size = new System.Drawing.Size(138, 14);
            this.label41.TabIndex = 996;
            this.label41.Text = "مقـدار العمولة للبيــــع :";
            // 
            // CHK1
            // 
            this.CHK1.AutoSize = true;
            this.CHK1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK1.ForeColor = System.Drawing.Color.Maroon;
            this.CHK1.Location = new System.Drawing.Point(578, 334);
            this.CHK1.Name = "CHK1";
            this.CHK1.Size = new System.Drawing.Size(140, 17);
            this.CHK1.TabIndex = 1002;
            this.CHK1.Text = "اعتماد اسعار الصرف...";
            this.CHK1.UseVisualStyleBackColor = true;
            this.CHK1.CheckedChanged += new System.EventHandler(this.Chk_Exch_Price_CheckedChanged);
            // 
            // Txt_sell_rate
            // 
            this.Txt_sell_rate.BackColor = System.Drawing.Color.White;
            this.Txt_sell_rate.Enabled = false;
            this.Txt_sell_rate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sell_rate.Location = new System.Drawing.Point(709, 368);
            this.Txt_sell_rate.Name = "Txt_sell_rate";
            this.Txt_sell_rate.NumberDecimalDigits = 7;
            this.Txt_sell_rate.NumberDecimalSeparator = ".";
            this.Txt_sell_rate.NumberGroupSeparator = ",";
            this.Txt_sell_rate.Size = new System.Drawing.Size(105, 23);
            this.Txt_sell_rate.TabIndex = 999;
            this.Txt_sell_rate.Text = "0.0000000";
            // 
            // Txt_buy_rate
            // 
            this.Txt_buy_rate.BackColor = System.Drawing.Color.White;
            this.Txt_buy_rate.Enabled = false;
            this.Txt_buy_rate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_buy_rate.Location = new System.Drawing.Point(710, 397);
            this.Txt_buy_rate.Name = "Txt_buy_rate";
            this.Txt_buy_rate.NumberDecimalDigits = 7;
            this.Txt_buy_rate.NumberDecimalSeparator = ".";
            this.Txt_buy_rate.NumberGroupSeparator = ",";
            this.Txt_buy_rate.Size = new System.Drawing.Size(105, 23);
            this.Txt_buy_rate.TabIndex = 997;
            this.Txt_buy_rate.Text = "0.0000000";
            // 
            // txt_per_tran_rem_upd
            // 
            this.txt_per_tran_rem_upd.BackColor = System.Drawing.Color.White;
            this.txt_per_tran_rem_upd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_per_tran_rem_upd.Location = new System.Drawing.Point(446, 397);
            this.txt_per_tran_rem_upd.Name = "txt_per_tran_rem_upd";
            this.txt_per_tran_rem_upd.NumberDecimalDigits = 3;
            this.txt_per_tran_rem_upd.NumberDecimalSeparator = ".";
            this.txt_per_tran_rem_upd.NumberGroupSeparator = ",";
            this.txt_per_tran_rem_upd.Size = new System.Drawing.Size(116, 23);
            this.txt_per_tran_rem_upd.TabIndex = 954;
            this.txt_per_tran_rem_upd.Text = "0.000";
            // 
            // txt_daily_uper_limt_upd
            // 
            this.txt_daily_uper_limt_upd.BackColor = System.Drawing.Color.White;
            this.txt_daily_uper_limt_upd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_daily_uper_limt_upd.Location = new System.Drawing.Point(446, 368);
            this.txt_daily_uper_limt_upd.Name = "txt_daily_uper_limt_upd";
            this.txt_daily_uper_limt_upd.NumberDecimalDigits = 3;
            this.txt_daily_uper_limt_upd.NumberDecimalSeparator = ".";
            this.txt_daily_uper_limt_upd.NumberGroupSeparator = ",";
            this.txt_daily_uper_limt_upd.Size = new System.Drawing.Size(116, 23);
            this.txt_daily_uper_limt_upd.TabIndex = 953;
            this.txt_daily_uper_limt_upd.Text = "0.000";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(10, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 16);
            this.label12.TabIndex = 1003;
            this.label12.Text = "الفرع :";
            // 
            // Txt_TErminal
            // 
            this.Txt_TErminal.BackColor = System.Drawing.Color.White;
            this.Txt_TErminal.Enabled = false;
            this.Txt_TErminal.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_TErminal.Location = new System.Drawing.Point(59, 38);
            this.Txt_TErminal.Name = "Txt_TErminal";
            this.Txt_TErminal.Size = new System.Drawing.Size(214, 23);
            this.Txt_TErminal.TabIndex = 1004;
            // 
            // cbo_deal_nature
            // 
            this.cbo_deal_nature.Cursor = System.Windows.Forms.Cursors.No;
            this.cbo_deal_nature.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_deal_nature.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_deal_nature.FormattingEnabled = true;
            this.cbo_deal_nature.Items.AddRange(new object[] {
            "حالة الحســــاب",
            "حساب ثانوي خاص",
            "حساب ثانوي عام"});
            this.cbo_deal_nature.Location = new System.Drawing.Point(385, 37);
            this.cbo_deal_nature.Name = "cbo_deal_nature";
            this.cbo_deal_nature.Size = new System.Drawing.Size(201, 24);
            this.cbo_deal_nature.TabIndex = 1006;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(282, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 16);
            this.label28.TabIndex = 1005;
            this.label28.Text = "طبـيـعة التعامـــل:";
            // 
            // CHK2
            // 
            this.CHK2.AutoSize = true;
            this.CHK2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK2.ForeColor = System.Drawing.Color.Maroon;
            this.CHK2.Location = new System.Drawing.Point(578, 349);
            this.CHK2.Name = "CHK2";
            this.CHK2.Size = new System.Drawing.Size(177, 17);
            this.CHK2.TabIndex = 1007;
            this.CHK2.Text = "اعتماد اسعار الصرف العميل...";
            this.CHK2.UseVisualStyleBackColor = true;
            this.CHK2.CheckedChanged += new System.EventHandler(this.CHK2_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(10, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 14);
            this.label6.TabIndex = 1009;
            this.label6.Text = "عــــمــليات العــمـلات.....";
            // 
            // grd_cur_oper
            // 
            this.grd_cur_oper.AllowUserToAddRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grd_cur_oper.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_cur_oper.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.grd_cur_oper.ColumnHeadersHeight = 25;
            this.grd_cur_oper.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column30,
            this.dataGridViewTextBoxColumn3,
            this.cur_oper_id});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.NullValue = null;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_cur_oper.DefaultCellStyle = dataGridViewCellStyle6;
            this.grd_cur_oper.Location = new System.Drawing.Point(3, 211);
            this.grd_cur_oper.Name = "grd_cur_oper";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = null;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_cur_oper.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.grd_cur_oper.RowHeadersWidth = 15;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grd_cur_oper.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.grd_cur_oper.Size = new System.Drawing.Size(839, 106);
            this.grd_cur_oper.TabIndex = 1008;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "cur_oper_chk";
            this.Column30.FalseValue = "0";
            this.Column30.HeaderText = "التاشير";
            this.Column30.Name = "Column30";
            this.Column30.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column30.TrueValue = "1";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "cur_oper_aname";
            this.dataGridViewTextBoxColumn3.HeaderText = "نوع العملية";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 250;
            // 
            // cur_oper_id
            // 
            this.cur_oper_id.DataPropertyName = "cur_oper_id";
            this.cur_oper_id.HeaderText = "cur_oper_id";
            this.cur_oper_id.Name = "cur_oper_id";
            this.cur_oper_id.ReadOnly = true;
            this.cur_oper_id.Visible = false;
            // 
            // cbo_time_zone
            // 
            this.cbo_time_zone.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbo_time_zone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_time_zone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_time_zone.FormattingEnabled = true;
            this.cbo_time_zone.Location = new System.Drawing.Point(79, 63);
            this.cbo_time_zone.Name = "cbo_time_zone";
            this.cbo_time_zone.Size = new System.Drawing.Size(360, 24);
            this.cbo_time_zone.TabIndex = 1011;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(12, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 16);
            this.label9.TabIndex = 1010;
            this.label9.Text = "نظام الوقت:";
            // 
            // Customer_Online_upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 493);
            this.Controls.Add(this.cbo_time_zone);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.grd_cur_oper);
            this.Controls.Add(this.CHK2);
            this.Controls.Add(this.cbo_deal_nature);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Txt_TErminal);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CHK1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_sell_rate);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Txt_buy_rate);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.flowLayoutPanel18);
            this.Controls.Add(this.flowLayoutPanel99);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.CboType_sub_cust);
            this.Controls.Add(this.Check);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_per_tran_rem_upd);
            this.Controls.Add(this.txt_daily_uper_limt_upd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Txt_Cur_Aname);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel49);
            this.Controls.Add(this.Grd_Cust_Main_Onilne);
            this.Controls.Add(this.flowLayoutPanel31);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customer_Online_upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "515";
            this.Text = "Customer_Online_upd";
            this.Load += new System.EventHandler(this.Customer_Online_upd_Load);
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel33.ResumeLayout(false);
            this.flowLayoutPanel35.ResumeLayout(false);
            this.flowLayoutPanel37.ResumeLayout(false);
            this.flowLayoutPanel39.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel43.ResumeLayout(false);
            this.flowLayoutPanel45.ResumeLayout(false);
            this.flowLayoutPanel19.ResumeLayout(false);
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel23.ResumeLayout(false);
            this.flowLayoutPanel25.ResumeLayout(false);
            this.flowLayoutPanel27.ResumeLayout(false);
            this.flowLayoutPanel29.ResumeLayout(false);
            this.flowLayoutPanel47.ResumeLayout(false);
            this.flowLayoutPanel65.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Main_Onilne)).EndInit();
            this.flowLayoutPanel49.ResumeLayout(false);
            this.flowLayoutPanel51.ResumeLayout(false);
            this.flowLayoutPanel53.ResumeLayout(false);
            this.flowLayoutPanel55.ResumeLayout(false);
            this.flowLayoutPanel57.ResumeLayout(false);
            this.flowLayoutPanel59.ResumeLayout(false);
            this.flowLayoutPanel61.ResumeLayout(false);
            this.flowLayoutPanel63.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel15.ResumeLayout(false);
            this.flowLayoutPanel99.ResumeLayout(false);
            this.flowLayoutPanel101.ResumeLayout(false);
            this.flowLayoutPanel103.ResumeLayout(false);
            this.flowLayoutPanel105.ResumeLayout(false);
            this.flowLayoutPanel107.ResumeLayout(false);
            this.flowLayoutPanel109.ResumeLayout(false);
            this.flowLayoutPanel111.ResumeLayout(false);
            this.flowLayoutPanel113.ResumeLayout(false);
            this.flowLayoutPanel115.ResumeLayout(false);
            this.flowLayoutPanel117.ResumeLayout(false);
            this.flowLayoutPanel119.ResumeLayout(false);
            this.flowLayoutPanel121.ResumeLayout(false);
            this.flowLayoutPanel123.ResumeLayout(false);
            this.flowLayoutPanel125.ResumeLayout(false);
            this.flowLayoutPanel127.ResumeLayout(false);
            this.flowLayoutPanel129.ResumeLayout(false);
            this.flowLayoutPanel131.ResumeLayout(false);
            this.flowLayoutPanel133.ResumeLayout(false);
            this.flowLayoutPanel135.ResumeLayout(false);
            this.flowLayoutPanel137.ResumeLayout(false);
            this.flowLayoutPanel139.ResumeLayout(false);
            this.flowLayoutPanel141.ResumeLayout(false);
            this.flowLayoutPanel143.ResumeLayout(false);
            this.flowLayoutPanel145.ResumeLayout(false);
            this.flowLayoutPanel147.ResumeLayout(false);
            this.flowLayoutPanel149.ResumeLayout(false);
            this.flowLayoutPanel151.ResumeLayout(false);
            this.flowLayoutPanel153.ResumeLayout(false);
            this.flowLayoutPanel155.ResumeLayout(false);
            this.flowLayoutPanel157.ResumeLayout(false);
            this.flowLayoutPanel159.ResumeLayout(false);
            this.flowLayoutPanel161.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_cur_oper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.DataGridView Grd_Cust_Main_Onilne;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel58;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel59;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel61;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel62;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel63;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel64;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.TextBox Txt_Cur_Aname;
        private System.Windows.Forms.Sample.DecimalTextBox txt_per_tran_rem_upd;
        private System.Windows.Forms.Sample.DecimalTextBox txt_daily_uper_limt_upd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel65;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel66;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox Check;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CboType_sub_cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel99;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel100;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel101;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel102;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel103;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel104;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel105;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel106;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel107;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel108;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel109;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel110;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel111;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel112;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel113;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel114;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel115;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel116;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel117;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel118;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel119;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel120;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel121;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel122;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel123;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel124;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel125;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel126;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel127;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel128;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel129;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel130;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel131;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel132;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel133;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel134;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel135;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel136;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel137;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel138;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel139;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel140;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel141;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel142;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel143;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel144;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel145;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel146;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel147;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel148;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel149;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel150;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel151;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel152;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel153;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel154;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel155;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel156;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel157;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel158;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel159;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel160;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel161;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel162;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_sell_rate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_buy_rate;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.CheckBox CHK1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_TErminal;
        private System.Windows.Forms.ComboBox cbo_deal_nature;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox CHK2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView grd_cur_oper;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn cur_oper_id;
        private System.Windows.Forms.ComboBox cbo_time_zone;
        private System.Windows.Forms.Label label9;
    }
}