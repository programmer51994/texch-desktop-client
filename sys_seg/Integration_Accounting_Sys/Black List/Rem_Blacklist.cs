﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Rem_Blacklist : Form
    {
        BindingSource _Bs_Blacklist = new BindingSource();
      
        //-----------------------
        public Rem_Blacklist()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Grd_Rem_Blacklist.AutoGenerateColumns = false;
          
        }

        private void Rem_Blacklist_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "per_rem_blacklist_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Grd_Rem_Blacklist_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Blacklist);
        }

        private void Rem_Blacklist_Load(object sender, EventArgs e)
        {

            if (connection.Lang_id != 1)
            {

                Btn_Add.Text = "Export";
                Column1.HeaderText = "Name";
                Column2.HeaderText = "Title";
                Column3.HeaderText = "Date of Birth";
                Column5.HeaderText = "Address";
                Column8.HeaderText = "Nationality";
                Column9.HeaderText = "Alias Information";
                Column18.HeaderText = "ORIGINAL NAME";
                Column4.HeaderText = "List Name";

            }
 
            _Bs_Blacklist.DataSource = connection.SQLDS.Tables["per_rem_blacklist_tbl"];
            Grd_Rem_Blacklist.DataSource = _Bs_Blacklist;
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            DataTable ExpDt = connection.SQLDS.Tables["per_rem_blacklist_tbl"].DefaultView.ToTable(false,"List_Name", "Name", "NAME_ORIGINAL_SCRIPT", "Alias_Information", "Title",
                "Date_of_Birth", "Address", "Nationality" );
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_Rem_Blacklist };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }
    }
}