﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class black_list_search_frm : Form
    {
        public black_list_search_frm()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            black_list_dgv.AutoGenerateColumns = false;
        }

        private void search_type_cbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (search_type_cbo.SelectedIndex == 0)
            {
                black_list_dgv.Columns["Column46"].Visible = true;
                black_list_dgv.Columns["Column47"].Visible = true;
            }
            else
            {


                black_list_dgv.Columns["Column46"].Visible = false;
                black_list_dgv.Columns["Column47"].Visible = false;
            }

        }

        private void black_list_search_frm_Load(object sender, EventArgs e)
        {
            string sql_txt = "";
            if (connection.Lang_id == 1)
                sql_txt = " select 0 as number,'جميع القوائم' as list_name union select 1 as number , list_name as list_name from black_list_tbl group by list_name order by number ";
            else
                sql_txt = " select 0 as number,'ALL Lists' as list_name union select 1 as number , list_name as list_name from black_list_tbl group by list_name order by number ";
            list_name_cbo.DataSource = connection.SqlExec(sql_txt, "list_name");
            list_name_cbo.DisplayMember = "list_name";
            list_name_cbo.ValueMember = "list_name";

            LblRec.Text = "0";
            if (connection.Lang_id == 1)
                sql_txt = "select 'تشابه' as search_type union select 'تطابق' as search_type";
            else
                sql_txt = "select 'Similar' as search_type union select 'Exact' as search_type";
            search_type_cbo.DataSource = connection.SqlExec(sql_txt, "search_type_tbl");
            search_type_cbo.DisplayMember = "search_type";
            search_type_cbo.ValueMember = "search_type";

        }

        private void search_btn_Click(object sender, EventArgs e)
        {
            if (search_txt.Text.Trim() == "")
            {
                string sql_txt = "select * from black_list_tbl ";
                //if (search_txt.Text.Trim() != "")
                //    sql_txt = sql_txt + " and name like '%" + search_txt.Text.Trim() + "%' ";
                if (list_name_cbo.SelectedIndex > 0)
                    sql_txt = sql_txt + " where list_name like '" + list_name_cbo.Text + "' ";
                black_list_dgv.DataSource = connection.SqlExec(sql_txt, "black_list_tbl");

                LblRec.Text = black_list_dgv.Rows.Count.ToString();
            }
            else
            {

                string column_search_str = "";
                if (name_chk.Checked == true)
                    column_search_str = column_search_str + "name,";
                if (NAME_ORIGINAL_SCRIPT_chk.Checked == true)
                    column_search_str = column_search_str + "NAME_ORIGINAL_SCRIPT,";
                if (Alias_Information_chk.Checked == true)
                    column_search_str = column_search_str + "Alias_Information,";
                if (Title_chk.Checked == true)
                    column_search_str = column_search_str + "Title,";
                if (Nationality_chk.Checked == true)
                    column_search_str = column_search_str + "Nationality,";
                if (Date_of_Birth_chk.Checked == true)
                    column_search_str = column_search_str + "Date_of_Birth,";
                if (Country_of_Birth_chk.Checked == true)
                    column_search_str = column_search_str + "Country_of_Birth,";
                if (Address_chk.Checked == true)
                    column_search_str = column_search_str + "Address,";
                if (NI_Number_chk.Checked == true)
                    column_search_str = column_search_str + "NI_Number,";
                if (Position_chk.Checked == true)
                    column_search_str = column_search_str + "Position,";
                if (Post_Zip_Code_chk.Checked == true)
                    column_search_str = column_search_str + "Post_Zip_Code,";
                if (Group_Type_chk.Checked == true)
                    column_search_str = column_search_str + "Group_Type,";
                if (Group_ID.Checked == true)
                    column_search_str = column_search_str + "Group_ID,";
                if (GENDER.Checked == true)
                    column_search_str = column_search_str + "GENDER,";
                if (Listed_On_chk.Checked == true)
                    column_search_str = column_search_str + "Listed_On,";
                if (TYPE_OF_DOCUMENT_chk.Checked == true)
                    column_search_str = column_search_str + "TYPE_OF_DOCUMENT,";
                if (UN_LIST_TYPE_chk.Checked == true)
                    column_search_str = column_search_str + "UN_LIST_TYPE,";
                if (Regime_chk.Checked == true)
                    column_search_str = column_search_str + "Regime,";


                int similarity_per = 0;
                if (similarity_per_txt.Text.Trim().Length == 0)
                    similarity_per = 0;
                else
                    similarity_per = Convert.ToInt16(similarity_per_txt.Text.Trim());

                string list_name = "%%";
                if (list_name_cbo.SelectedIndex > 0)
                    list_name = list_name_cbo.Text.Trim();

                black_list_dgv.DataSource = connection.SqlExec("similar_search "
                                                                + "'" + search_txt.Text.Trim() + "',"
                                                                + "'" + "black_list_tbl" + "',"
                                                                + "'" + list_name + "',"
                                                                + "'" + column_search_str + "',"
                                                                + similarity_per, "black_list_tbl");

                LblRec.Text = black_list_dgv.Rows.Count.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {


            DataTable Dt = new DataTable();

            try
            {

                //header for the table to be exported

                DataTable DT_Blacklist_Header = new DataTable();
                DT_Blacklist_Header.Columns.Add("اسم القائمة");
                DT_Blacklist_Header.Columns.Add("الاسم");
                DT_Blacklist_Header.Columns.Add("اللقب");
                DT_Blacklist_Header.Columns.Add("تاريخ الولاده");
                DT_Blacklist_Header.Columns.Add("بلد الولاده");
                DT_Blacklist_Header.Columns.Add("مدينة الولاده");
                DT_Blacklist_Header.Columns.Add("الاقليم");
                DT_Blacklist_Header.Columns.Add("عنوان الولاده");
                DT_Blacklist_Header.Columns.Add("العنوان");
                DT_Blacklist_Header.Columns.Add("الجنسية");
                DT_Blacklist_Header.Columns.Add("NI رقم");
                DT_Blacklist_Header.Columns.Add("تفاصيل جواز السفر");
                DT_Blacklist_Header.Columns.Add("المنصب");
                DT_Blacklist_Header.Columns.Add("Post/ZIP رقم");
                DT_Blacklist_Header.Columns.Add("البلد");
                DT_Blacklist_Header.Columns.Add("معلومات اخرى");
                DT_Blacklist_Header.Columns.Add("نوع الشبكة");
                DT_Blacklist_Header.Columns.Add("نوع اسم الشهرة");
                DT_Blacklist_Header.Columns.Add("السلطة");
                DT_Blacklist_Header.Columns.Add("تاريخ التثبيت");
                DT_Blacklist_Header.Columns.Add("اخر تحديث");
                DT_Blacklist_Header.Columns.Add("رقم الشبكة");
                DT_Blacklist_Header.Columns.Add("معلومات القائمة");
                DT_Blacklist_Header.Columns.Add("قائمة الامم المتحدة");
                DT_Blacklist_Header.Columns.Add("رقم المرجع");
                DT_Blacklist_Header.Columns.Add("الجنس");
                DT_Blacklist_Header.Columns.Add("مسؤول التثبيت");
                DT_Blacklist_Header.Columns.Add("تاريخ_التثبيت");
                DT_Blacklist_Header.Columns.Add("الاسم الاصلي");
                DT_Blacklist_Header.Columns.Add("تعليق");
                DT_Blacklist_Header.Columns.Add("نوع الوثيقة");
                DT_Blacklist_Header.Columns.Add("تفاصيل الوثيقة");
                DT_Blacklist_Header.Columns.Add("اسم الشهرة");
                DT_Blacklist_Header.Columns.Add("البرنامج");
                DT_Blacklist_Header.Columns.Add("اسم الام");
                DT_Blacklist_Header.Columns.Add("المرجع");
                DT_Blacklist_Header.Columns.Add("تاريخ التحكم");
                DT_Blacklist_Header.Columns.Add("رقم القيد");
                DT_Blacklist_Header.Columns.Add("رقم البيانات");
                DT_Blacklist_Header.Columns.Add("رقم الاصدار");
                DT_Blacklist_Header.Columns.Add("الجنسية2");
                DT_Blacklist_Header.Columns.Add("تاريخ الازالة");
                DT_Blacklist_Header.Columns.Add("ID_#2");
                DT_Blacklist_Header.Columns.Add("Basis3");
                DT_Blacklist_Header.Columns.Add("ID_#3");
                DT_Blacklist_Header.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");


                //data table to be exported

                DataTable Exprt_DT = connection.SQLDS.Tables["black_list_tbl"].DefaultView.ToTable(false,
                "list_name", "Name", "Title", "Date_of_Birth", "Country_of_Birth", "City_of_Birth", "State_Province", "Place_of_Birth", "Address", "Nationality",
                "NI_Number", "Passport_Details", "Position", "Post_Zip_Code", "Country", "Other_Information", "Group_Type", "Alias_Type", "Regime", "Listed_On",
                "Last_Updated", "Group_ID", "Listing_Information", "UN_LIST_TYPE", "REFERENCE_NUMBER", "GENDER", "SUBMITTED_BY", "SUBMITTED_ON",
                "NAME_ORIGINAL_SCRIPT", "COMMENTS1", "TYPE_OF_DOCUMENT", "DOCUMENT_DETAILS", "Alias_Information", "Program", "MOTHER_NAME", "Reference",
                "Control_Date", "BLACK_LIST_ID", "DATAID", "VERSIONNUM", "NATIONALITY2", "DELISTED_ON", "ID_#2", "Basis3", "ID_#3"
                ).Select().CopyToDataTable();

                if (Exprt_DT.Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا يـــوجد قيـــود للتـــصدير" : "There is no data to be exported", MyGeneral_Lib.LblCap);
                    return;
                }


                // exporting to textfile
                MyExports.To_TxtFile(Exprt_DT, DT_Blacklist_Header);


            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدث خطأ ما" : "Something went wrong", MyGeneral_Lib.LblCap);
                return;
            }


        }
    }
}