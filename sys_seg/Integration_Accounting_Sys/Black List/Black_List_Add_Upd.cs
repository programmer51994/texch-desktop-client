﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Black_List_Add_Upd : Form
    {
        #region Defintion
        string Sql_Text = "";
        DataTable Family_Tbl;
        DataTable Pic_Tbl;
        byte[] img = new byte[0];
        int Frm_id = 0;
        int Bl_List_id = 0;
        #endregion
        public Black_List_Add_Upd(int Form_id)
        {
            InitializeComponent();
            //connection.SQLBSMainGrd.DataSource = new BindingSource();
            //connection.SQLBSSubGrd1.DataSource = new BindingSource();
            if (Form_id == 1)
            {
                Btn_Browser.Visible = false;
                pictureBox1.Visible = false;
                this.Size = new System.Drawing.Size(690, 505);
                label3.Location = new System.Drawing.Point(429, 10);
                TxtIn_Rec_Date.Location = new System.Drawing.Point(485, 6);
                AddBtn.Location = new System.Drawing.Point(254, 447);
                ExtBtn.Location = new System.Drawing.Point(342, 447);
            }
            else
            {
                Btn_Browser.Visible = true;
                pictureBox1.Visible = true;
                this.Size = new System.Drawing.Size(999, 505);
                AddBtn.Location = new System.Drawing.Point(408, 447);
                ExtBtn.Location = new System.Drawing.Point(496, 447);
                label3.Location = new System.Drawing.Point(678, 10);
                TxtIn_Rec_Date.Location = new System.Drawing.Point(734, 6);
            }
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Frm_id = Form_id;
            Grd_Family.AutoGenerateColumns = false;
            Net_Connect.Visible = false;
            Net_Connect1.Visible = false;
            Fml_Tbl();
            Create_Pic_Tbl();
        }
        //-----------------------------------       
        private void Black_List_Add_Load(object sender, EventArgs e)
        {

            Sql_Text = " Select  Nat_ANAME,NAT_ENAME,NAT_ID FROM NAt_Tbl "
                        //+ " union "
                        //+ " Select ' (جميع الجنسيات) ' as Nat_ANAME,' (All Nationality) ' as NAT_ENAME,-1 as NAT_ID FROM NAt_Tbl"
                        + " order by " + (connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_ENAME");

            CboNat_id.DataSource = connection.SqlExec(Sql_Text, "Nat_tbl");
            CboNat_id.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_ENAME";
            CboNat_id.ValueMember = "Nat_id";


            Sql_Text = "SELECT Rel_ID, Rel_AName, Rel_EName FROM Rel_Tbl order by Rel_AName ";
            Cbo_Rel_id.DataSource = connection.SqlExec(Sql_Text, "Rel_Tbl");
            Cbo_Rel_id.DisplayMember = connection.Lang_id == 1 ? "Rel_AName" : "Rel_EName";
            Cbo_Rel_id.ValueMember = "Rel_ID";

            if (Frm_id == 2)
            {

                EditRecord();
            }

        }
        //-----------------------------------
        private void Fml_Tbl()
        {
            string[] Column = { "Rel_id", "Black_Rel_Aname", "Black_Rel_Ename" };
            string[] DType = { "System.Int32", "System.String", "System.String" };
            Family_Tbl = CustomControls.Custom_DataTable("Family_Tbl", Column, DType);
            Grd_Family.DataSource = Family_Tbl;
        }
        //--------------------------------
        private void Create_Pic_Tbl()
        {
            string[] Column = { "pic_id", "Bl_Pic" };
            string[] DType = { "System.Int32", "System.Byte[]" };
            Pic_Tbl = CustomControls.Custom_DataTable("Pic_Tbl", Column, DType);
        }

        //-----------------------------------
        private void EditRecord()
        {
            TxtArb_Name.DataBindings.Clear();
            TxtEng_Name.DataBindings.Clear();
            TxtFrst_name.DataBindings.Clear();
            Txtlast_Name.DataBindings.Clear();
            TxtBrth_Date.DataBindings.Clear();
            TxtBrth_place.DataBindings.Clear();
            TxtMother_AName.DataBindings.Clear();
            TxtMother_EName.DataBindings.Clear();
            TxtOccupation.DataBindings.Clear();
            TxtAddress.DataBindings.Clear();
            Txt_Note.DataBindings.Clear();
            CboNat_id.DataBindings.Clear();
            TxtArb_Name.DataBindings.Add("Text", connection.SQLBS, "AB_name");
            TxtEng_Name.DataBindings.Add("Text", connection.SQLBS, "EB_name");
            TxtFrst_name.DataBindings.Add("Text", connection.SQLBS, "BLACK_N1");
            Txtlast_Name.DataBindings.Add("Text", connection.SQLBS, "BLACK_N2");
            TxtBrth_Date.DataBindings.Add("Text", connection.SQLBS, "BIRTH_DATE");
            TxtBrth_place.DataBindings.Add("Text", connection.SQLBS, "PLACE_BIRTH");
            TxtMother_AName.DataBindings.Add("Text", connection.SQLBS, "MOTHER_ANAME");
            TxtMother_EName.DataBindings.Add("Text", connection.SQLBS, "MOTHER_ENAME");
            TxtOccupation.DataBindings.Add("Text", connection.SQLBS, "Occupation");
            TxtAddress.DataBindings.Add("Text", connection.SQLBS, "ADDRESS");
            Txt_Note.DataBindings.Add("Text", connection.SQLBS, "Notes");
            CboNat_id.DataBindings.Add("Selectedvalue", connection.SQLBS, "Nat_id");
            if (connection.SQLBSMainGrd.List.Count > 0)
            {
                Family_Tbl.Clear();
                Family_Tbl = ((DataTable)connection.SQLBSMainGrd.DataSource).DefaultView.ToTable(false, "Rel_id", "Black_Rel_Aname", "Black_Rel_Ename");
                Cbo_Rel_id.DataPropertyName = "Rel_id";
                Grd_Family.DataSource = Family_Tbl;
            }

            Pic_Tbl = ((DataTable)connection.SQLBSSubGrd1.DataSource).DefaultView.ToTable(false, "pic_id", "BL_PIC");
            if (Pic_Tbl.Rows.Count > 0)
            {
                img = (Byte[])((DataRowView)connection.SQLBSSubGrd1.Current).Row["BL_PIC"];
                MemoryStream mem = new MemoryStream(img);
                pictureBox1.Image = Image.FromStream(mem);
            }
            else
            {
                pictureBox1.Image = null;
            }
            Bl_List_id = Convert.ToInt32(((DataRowView)connection.SQLBS.Current).Row["Bl_List_id"]);
        }
        //-----------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            DataRow DRow = Family_Tbl.NewRow();
            Family_Tbl.Rows.Add(DRow);
        }
        //-----------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Family_Tbl.Rows.Count > 0)
            {
                Family_Tbl.Rows[Grd_Family.CurrentRow.Index].Delete();
            }
        }
        //-----------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            
                for (int i = 0; i < Grd_Family.RowCount; i++)
                {

                    Family_Tbl.Rows[i].SetField("Rel_id", Grd_Family[0, i].Value);
                }
           

            #region Validation
            for (int i = 0; i < Family_Tbl.Rows.Count; i++)
            {
                if (Family_Tbl.Rows[i][0] == DBNull.Value || Family_Tbl.Rows[i][1] == DBNull.Value || Family_Tbl.Rows[i][1].ToString() == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تاكد من ادخال القيم بشكل صحيح" : "Check Records Please", MyGeneral_Lib.LblCap);
                    return;
                }
            }

            if (TxtArb_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please Insert Arabic Name", MyGeneral_Lib.LblCap);
                TxtArb_Name.Focus();
                return;
            }
            if (TxtEng_Name.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم الاجنبي" : "Please Insert English Name", MyGeneral_Lib.LblCap);
                TxtEng_Name.Focus();
                return;
            }
            if (Convert.ToInt16(CboNat_id.SelectedValue) < 0)
            {
                CboNat_id.SelectedValue = 0;
            }
            string Ord_Strt = MyGeneral_Lib.DateChecking(TxtBrth_Date.Text);
            if (Ord_Strt == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ التولد" : "Please make sure of birth date", MyGeneral_Lib.LblCap);
                TxtBrth_Date.Focus();
                Ord_Strt = "0";
                return;
            }
            #endregion

            #region Add Pic
            if (Frm_id == 1)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد اضافة صورة  ؟" :
                "Do you want to add  Photo", MyGeneral_Lib.LblCap,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Dr == DialogResult.Yes)
                {

                    OpenFileDialog Open_Pic = new OpenFileDialog();
                    Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|" + "All files (*.*)|*.*";
                    Open_Pic.Title = connection.Lang_id == 1 ? "اختر صورة " : "Select Photo";
                    Open_Pic.Multiselect = true;
                    if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
                    {
                        foreach (String file in Open_Pic.FileNames)
                        {

                            FileStream FS = new FileStream(file, FileMode.Open, FileAccess.Read);
                            img = new byte[FS.Length];
                            FS.Read(img, 0, Convert.ToInt32(FS.Length));
                            FS.Close();

                            DataRow DRow1 = Pic_Tbl.NewRow();
                            DRow1.SetField("Bl_Pic", img);
                            Pic_Tbl.Rows.Add(DRow1);
                        }

                    }

                }
            }
            #endregion
            connection.SQLCMD.Parameters.AddWithValue("@Bl_List_id", Bl_List_id);
            connection.SQLCMD.Parameters.AddWithValue("@AB_NAME", TxtArb_Name.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@EB_NAME", TxtEng_Name.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@BIRTH_DATE", Ord_Strt);
            connection.SQLCMD.Parameters.AddWithValue("@PLACE_BIRTH", TxtBrth_place.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@NAT_ID", CboNat_id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@BLACK_N1", TxtFrst_name.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@BLACK_N2", Txtlast_Name.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@MOTHER_ANAME", TxtMother_AName.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@MOTHER_ENAME", TxtMother_EName.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@ADDRESS", TxtAddress.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@Occupation", TxtOccupation.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@NOTES", Txt_Note.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Rel_Tbl", Family_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@Bl_Pic_Tbl", Pic_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@Frm_id", Frm_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_Black_list", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
        //-----------------------------------
        private void Grd_Family_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // ---------- required
        }
        //----------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {


            OpenFileDialog Open_Pic = new OpenFileDialog();
            Open_Pic.Filter = "Images (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|" + "All files (*.*)|*.*";
            Open_Pic.Title = connection.Lang_id == 1 ? "اختر صورة " : "Select  Photo";
            Open_Pic.Multiselect = true;
            if (Open_Pic.ShowDialog() == DialogResult.OK && Open_Pic.FileName != "")
            {
                foreach (String file in Open_Pic.FileNames)
                {

                    FileStream FS = new FileStream(file, FileMode.Open, FileAccess.Read);
                    img = new byte[FS.Length];
                    FS.Read(img, 0, Convert.ToInt32(FS.Length));
                    FS.Close();

                    DataRow DRow1 = Pic_Tbl.NewRow();
                    DRow1.SetField("Bl_Pic", img);
                    Pic_Tbl.Rows.Add(DRow1);
                }
                connection.SQLBSSubGrd1.DataSource = Pic_Tbl;
                Btn_Next_Click(sender, e);
            }
        }
        //-----------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        //-----------------------------------
        private void Black_List_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Used_Tbl = { "Nat_tbl", "Rel_Tbl"}; ;
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);

            }
        }
        private void Btn_Previous_Click(object sender, EventArgs e)
        {
            connection.SQLBSSubGrd1.MovePrevious();
            img = (Byte[])((DataRowView)connection.SQLBSSubGrd1.Current).Row["BL_PIC"];
            MemoryStream mem = new MemoryStream(img);
            pictureBox1.Image = Image.FromStream(mem);
        }
        //-------------------------------------------------
        private void Btn_Next_Click(object sender, EventArgs e)
        {
            connection.SQLBSSubGrd1.MoveNext();
            img = (Byte[])((DataRowView)connection.SQLBSSubGrd1.Current).Row["BL_PIC"];
            MemoryStream mem = new MemoryStream(img);
            pictureBox1.Image = Image.FromStream(mem);
        }
        //-------------------------------------------------
        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            if (connection.SQLBSSubGrd1.Position != -1)
            {
                Pic_Tbl.Rows[connection.SQLBSSubGrd1.Position].Delete();
                connection.SQLBSSubGrd1.DataSource = Pic_Tbl;
                try
                {
                    Btn_Next_Click(sender, e);
                }
                catch
                {
                    pictureBox1.Image = null;
                }
            }
        }
        //-------------------------------------------------
        private void TxtArb_Name_Leave(object sender, EventArgs e)
        {
            TxtEng_Name.Text = TxtArb_Name.Text != "" ? MyGeneral_Lib.TranslateText(TxtArb_Name.Text, "ar", "en") : "";
            if (TxtEng_Name.Text != "-1")
                Net_Connect1.Visible = false;
            else
            {
                Net_Connect1.Visible = true;
                TxtEng_Name.Text = "";
            }
        }
        //-------------------------------------------------
        private void TxtMother_AName_Leave(object sender, EventArgs e)
        {
            TxtMother_EName.Text = TxtMother_AName.Text != "" ? MyGeneral_Lib.TranslateText(TxtMother_AName.Text, "ar", "en") : "";
            if (TxtMother_EName.Text != "-1")
                Net_Connect1.Visible = false;
            else
            {
                Net_Connect1.Visible = true;
                TxtMother_EName.Text = "";
            }
        }

        private void CboNat_id_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}