﻿namespace Integration_Accounting_Sys
{
    partial class black_list_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.black_list_dgv = new System.Windows.Forms.DataGridView();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ok_btn = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.list_name_cbo = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.upload_btn = new System.Windows.Forms.Button();
            this.clear_btn = new System.Windows.Forms.Button();
            this.upload_type_cbo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.black_list_dgv)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // black_list_dgv
            // 
            this.black_list_dgv.AllowUserToAddRows = false;
            this.black_list_dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.black_list_dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.black_list_dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.black_list_dgv.ColumnHeadersHeight = 40;
            this.black_list_dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column45,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column35,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column40,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44});
            this.black_list_dgv.Location = new System.Drawing.Point(6, 58);
            this.black_list_dgv.MultiSelect = false;
            this.black_list_dgv.Name = "black_list_dgv";
            this.black_list_dgv.ReadOnly = true;
            this.black_list_dgv.RowHeadersWidth = 30;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.black_list_dgv.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.black_list_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.black_list_dgv.Size = new System.Drawing.Size(1006, 485);
            this.black_list_dgv.TabIndex = 4;
            // 
            // Column45
            // 
            this.Column45.DataPropertyName = "list_name";
            this.Column45.HeaderText = "اسم القائمة";
            this.Column45.Name = "Column45";
            this.Column45.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Name";
            this.Column1.HeaderText = "ألاسم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 300;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Title";
            this.Column2.HeaderText = "اللقــب";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Date_of_Birth";
            this.Column3.HeaderText = "تاريخ الولادة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 180;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Country_of_Birth";
            this.Column4.HeaderText = "بلد الولادة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "City_of_Birth";
            this.Column5.HeaderText = "مدينة الولادة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "State_Province";
            this.Column6.HeaderText = "الاقليم";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Place_of_Birth";
            this.Column7.HeaderText = "عنوان الولادة";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 180;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Address";
            this.Column8.HeaderText = "العنوان";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Nationality";
            this.Column9.HeaderText = "الجنسية";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "NI_Number";
            this.Column10.HeaderText = "رقم NI";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Passport_Details";
            this.Column11.HeaderText = "تفاصيل جواز السفر";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Position";
            this.Column12.HeaderText = "المنصب";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Post_Zip_Code";
            this.Column13.HeaderText = "رقم Post/Zip";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Country";
            this.Column14.HeaderText = "البلد";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Other_Information";
            this.Column15.HeaderText = "معلومات اخرى";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 250;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Group_Type";
            this.Column16.HeaderText = "نوع الشبكة";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Alias_Type";
            this.Column17.HeaderText = "نوع اسم الشهرة";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 150;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "Regime";
            this.Column18.HeaderText = "السلطة";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 150;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Listed_On";
            this.Column19.HeaderText = "تاريخ التثبيت";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "Last_Updated";
            this.Column20.HeaderText = "اخر تحديث";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Group_ID";
            this.Column21.HeaderText = "رقم الشبكة";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "Listing_Information";
            this.Column22.HeaderText = "معلومات القائمة";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Width = 200;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "UN_LIST_TYPE";
            this.Column23.HeaderText = "قائمة الامم المتحدة";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "REFERENCE_NUMBER";
            this.Column24.HeaderText = "رقم المرجع";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "GENDER";
            this.Column25.HeaderText = "الجنس";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "SUBMITTED_BY";
            this.Column26.HeaderText = "مسؤول التثبيت";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "SUBMITTED_ON";
            this.Column27.HeaderText = "تاريخ التثبيت";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "NAME_ORIGINAL_SCRIPT";
            this.Column28.HeaderText = "الاسم الاصلي";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.Width = 250;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "COMMENTS1";
            this.Column29.HeaderText = "تعليق";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.Width = 200;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "TYPE_OF_DOCUMENT";
            this.Column30.HeaderText = "نوع الوثيقة";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "DOCUMENT_DETAILS";
            this.Column31.HeaderText = "تفاصيل الوثيقة";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "Alias_Information";
            this.Column32.HeaderText = "اسم الشهرة";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.Width = 250;
            // 
            // Column33
            // 
            this.Column33.DataPropertyName = "Program";
            this.Column33.HeaderText = "البرنامج";
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            // 
            // Column34
            // 
            this.Column34.DataPropertyName = "MOTHER_NAME";
            this.Column34.HeaderText = "اسم الام";
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            // 
            // Column35
            // 
            this.Column35.DataPropertyName = "Reference";
            this.Column35.HeaderText = "المرجع";
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            // 
            // Column36
            // 
            this.Column36.DataPropertyName = "Control_Date";
            this.Column36.HeaderText = "تاريخ التحكم";
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "BLACK_LIST_ID";
            this.Column37.HeaderText = "رقم القيد";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "DATAID";
            this.Column38.HeaderText = "رقم البيانات";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            // 
            // Column39
            // 
            this.Column39.DataPropertyName = "VERSIONNUM";
            this.Column39.HeaderText = "رقم الاصدار";
            this.Column39.Name = "Column39";
            this.Column39.ReadOnly = true;
            // 
            // Column40
            // 
            this.Column40.DataPropertyName = "NATIONALITY2";
            this.Column40.HeaderText = "الجنسية2";
            this.Column40.Name = "Column40";
            this.Column40.ReadOnly = true;
            this.Column40.Width = 120;
            // 
            // Column41
            // 
            this.Column41.DataPropertyName = "DELISTED_ON";
            this.Column41.HeaderText = "تاريخ الازالة";
            this.Column41.Name = "Column41";
            this.Column41.ReadOnly = true;
            // 
            // Column42
            // 
            this.Column42.DataPropertyName = "ID_#2";
            this.Column42.HeaderText = "ID #2";
            this.Column42.Name = "Column42";
            this.Column42.ReadOnly = true;
            // 
            // Column43
            // 
            this.Column43.DataPropertyName = "Basis3";
            this.Column43.HeaderText = "Basis3";
            this.Column43.Name = "Column43";
            this.Column43.ReadOnly = true;
            // 
            // Column44
            // 
            this.Column44.DataPropertyName = "ID_#3";
            this.Column44.HeaderText = "ID #3";
            this.Column44.Name = "Column44";
            this.Column44.ReadOnly = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ok_btn
            // 
            this.ok_btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ok_btn.Location = new System.Drawing.Point(391, 554);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(118, 33);
            this.ok_btn.TabIndex = 7;
            this.ok_btn.Tag = "";
            this.ok_btn.Text = "حـفـــــــظ";
            this.ok_btn.UseVisualStyleBackColor = true;
            this.ok_btn.Click += new System.EventHandler(this.ok_btn_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 547);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1024, 1);
            this.flowLayoutPanel1.TabIndex = 695;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label8.Location = new System.Drawing.Point(11, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 18);
            this.label8.TabIndex = 711;
            this.label8.Text = "نـوع الاستيـــــــراد:";
            // 
            // list_name_cbo
            // 
            this.list_name_cbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.list_name_cbo.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.list_name_cbo.FormattingEnabled = true;
            this.list_name_cbo.Location = new System.Drawing.Point(118, 8);
            this.list_name_cbo.Name = "list_name_cbo";
            this.list_name_cbo.Size = new System.Drawing.Size(270, 24);
            this.list_name_cbo.TabIndex = 712;
            this.list_name_cbo.SelectedIndexChanged += new System.EventHandler(this.list_name_cbo_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 589);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1018, 22);
            this.statusStrip1.TabIndex = 714;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-5, 51);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1024, 1);
            this.flowLayoutPanel3.TabIndex = 695;
            // 
            // upload_btn
            // 
            this.upload_btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.upload_btn.Location = new System.Drawing.Point(805, 10);
            this.upload_btn.Name = "upload_btn";
            this.upload_btn.Size = new System.Drawing.Size(105, 33);
            this.upload_btn.TabIndex = 702;
            this.upload_btn.Text = "استيـــــراد";
            this.upload_btn.UseVisualStyleBackColor = true;
            this.upload_btn.Click += new System.EventHandler(this.upload_btn_Click);
            // 
            // clear_btn
            // 
            this.clear_btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.clear_btn.Location = new System.Drawing.Point(911, 10);
            this.clear_btn.Name = "clear_btn";
            this.clear_btn.Size = new System.Drawing.Size(101, 33);
            this.clear_btn.TabIndex = 703;
            this.clear_btn.Text = "افراغ البيانــات";
            this.clear_btn.UseVisualStyleBackColor = true;
            this.clear_btn.Click += new System.EventHandler(this.clear_btn_Click);
            // 
            // upload_type_cbo
            // 
            this.upload_type_cbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.upload_type_cbo.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.upload_type_cbo.FormattingEnabled = true;
            this.upload_type_cbo.Location = new System.Drawing.Point(392, 8);
            this.upload_type_cbo.Name = "upload_type_cbo";
            this.upload_type_cbo.Size = new System.Drawing.Size(156, 24);
            this.upload_type_cbo.TabIndex = 715;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.Location = new System.Drawing.Point(509, 554);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 33);
            this.button1.TabIndex = 716;
            this.button1.Tag = "";
            this.button1.Text = "انهـــــاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // black_list_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 611);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.upload_type_cbo);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.list_name_cbo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.clear_btn);
            this.Controls.Add(this.upload_btn);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ok_btn);
            this.Controls.Add(this.black_list_dgv);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "black_list_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "526";
            this.Text = "black_list_frm";
            this.Load += new System.EventHandler(this.black_list_frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.black_list_dgv)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView black_list_dgv;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox list_name_cbo;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button upload_btn;
        private System.Windows.Forms.Button clear_btn;
        private System.Windows.Forms.ComboBox upload_type_cbo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.Button button1;
    }
}