﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Rem_Blacklist_OK : Form
    {
        BindingSource _Bs_Blacklist = new BindingSource();
        public static Int16 Back_btn = 0;
        public static string  Notes_BL = "";
       
        //-----------------------
        public Rem_Blacklist_OK()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Grd_Rem_Blacklist.AutoGenerateColumns = false;
            
        }


        private void Grd_Rem_Blacklist_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Blacklist);
        }

        private void Rem_Blacklist_OK_Load(object sender, EventArgs e)
        {
            Back_btn = 0;

            if (connection.Lang_id != 1)
            {

                Btn_Add.Text = "Export";
                Btn_Ok.Text = "Continue";
                Btn_End.Text = "Refuse";
                label12.Text = "Reason to continue ........";
                Column1.HeaderText = "Name";
                Column2.HeaderText = "Title";
                Column3.HeaderText = "Date of Birth";
                Column5.HeaderText = "Address";
                Column8.HeaderText = "Nationality";
                Column9.HeaderText = "Alias Information";
                Column18.HeaderText = "ORIGINAL NAME";
                Column4.HeaderText = "List Name";

            }

            _Bs_Blacklist.DataSource = connection.SQLDS.Tables["per_rem_blacklist_tbl"];
            Grd_Rem_Blacklist.DataSource = _Bs_Blacklist;
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Back_btn = 0;

            DataTable ExpDt = connection.SQLDS.Tables["per_rem_blacklist_tbl"].DefaultView.ToTable(false,"List_Name", "Name", "NAME_ORIGINAL_SCRIPT", "Alias_Information", "Title",
                                                                                                         "Date_of_Birth", "Address", "Nationality" );
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_Rem_Blacklist };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Btn_End_Click(object sender, EventArgs e)
        {
           

            Back_btn = 1;
            Notes_BL = Txt_Notes_BL.Text.Trim();
            this.Close();
       
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {

            if (Txt_Notes_BL.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى ادخال الملاحظات" : "Please Enter The Notes", MyGeneral_Lib.LblCap);
                Txt_Notes_BL.Focus();
                return;
            }

            Notes_BL = Txt_Notes_BL.Text.Trim();
            this.Close();
        }

        private void Rem_Blacklist_OK_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "per_rem_blacklist_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }


    }
}