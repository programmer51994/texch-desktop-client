﻿namespace Integration_Accounting_Sys
{
    partial class Black_List_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Black_List_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnUpd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.StopBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.SearchBtn = new System.Windows.Forms.ToolStripButton();
            this.Txt_BlackSearch = new System.Windows.Forms.ToolStripTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Note = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Grd_Black_list = new System.Windows.Forms.DataGridView();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Relatives = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel189 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel190 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel191 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel192 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel193 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel194 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel195 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel196 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel197 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel198 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel199 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel200 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel201 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel202 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel203 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel204 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel205 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel206 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel207 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel208 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel209 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel210 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel211 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel212 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel213 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel214 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel215 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel216 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel217 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel218 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel219 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel220 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel221 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel222 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel223 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel224 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel225 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel226 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel227 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel228 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel229 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel230 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel231 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel232 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel233 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel234 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel235 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel236 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel237 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel238 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel239 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel240 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel241 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel242 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel243 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel244 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel245 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel246 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel247 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel248 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel249 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel250 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Previous = new System.Windows.Forms.Button();
            this.Btn_Next = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Black_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Relatives)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel189.SuspendLayout();
            this.flowLayoutPanel194.SuspendLayout();
            this.flowLayoutPanel199.SuspendLayout();
            this.flowLayoutPanel204.SuspendLayout();
            this.flowLayoutPanel209.SuspendLayout();
            this.flowLayoutPanel214.SuspendLayout();
            this.flowLayoutPanel219.SuspendLayout();
            this.flowLayoutPanel224.SuspendLayout();
            this.flowLayoutPanel229.SuspendLayout();
            this.flowLayoutPanel234.SuspendLayout();
            this.flowLayoutPanel239.SuspendLayout();
            this.flowLayoutPanel244.SuspendLayout();
            this.flowLayoutPanel249.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(124, 41);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(208, 22);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(770, 37);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(183, 22);
            this.TxtIn_Rec_Date.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(705, 41);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 687;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(106, 14);
            this.label2.TabIndex = 686;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.BtnUpd,
            this.toolStripSeparator2,
            this.StopBtn,
            this.toolStripSeparator1,
            this.AllBtn,
            this.toolStripSeparator4,
            this.printToolStripButton,
            this.toolStripSeparator7,
            this.SearchBtn,
            this.Txt_BlackSearch});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(958, 30);
            this.BND.TabIndex = 0;
            this.BND.Text = "BND";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(80, 27);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "اضافة جديد";
            this.BtnAdd.ToolTipText = "F9";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // BtnUpd
            // 
            this.BtnUpd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnUpd.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpd.Image")));
            this.BtnUpd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnUpd.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.BtnUpd.Name = "BtnUpd";
            this.BtnUpd.Size = new System.Drawing.Size(60, 27);
            this.BtnUpd.Tag = "3";
            this.BtnUpd.Text = "تعـديــل";
            this.BtnUpd.ToolTipText = "F7";
            this.BtnUpd.Click += new System.EventHandler(this.BtnUpd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // StopBtn
            // 
            this.StopBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.StopBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(75, 27);
            this.StopBtn.Text = "رفع من القائمة";
            this.StopBtn.Click += new System.EventHandler(this.Btn_State_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // AllBtn
            // 
            this.AllBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(84, 27);
            this.AllBtn.Tag = "4";
            this.AllBtn.Text = "عرض الكـل";
            this.AllBtn.ToolTipText = "F4";
            this.AllBtn.Click += new System.EventHandler(this.AllBtn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.printToolStripButton.Image = global::Integration_Accounting_Sys.Properties.Resources._00;
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(58, 27);
            this.printToolStripButton.Text = "تصدير";
            this.printToolStripButton.Click += new System.EventHandler(this.printToolStripButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(64, 27);
            this.SearchBtn.Text = "بحـــــث";
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // Txt_BlackSearch
            // 
            this.Txt_BlackSearch.Name = "Txt_BlackSearch";
            this.Txt_BlackSearch.Size = new System.Drawing.Size(200, 30);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-5, 33);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(963, 1);
            this.flowLayoutPanel1.TabIndex = 692;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-8, 69);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(973, 1);
            this.flowLayoutPanel2.TabIndex = 693;
            // 
            // Txt_Note
            // 
            this.Txt_Note.BackColor = System.Drawing.Color.White;
            this.Txt_Note.Enabled = false;
            this.Txt_Note.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Note.Location = new System.Drawing.Point(119, 471);
            this.Txt_Note.Multiline = true;
            this.Txt_Note.Name = "Txt_Note";
            this.Txt_Note.ReadOnly = true;
            this.Txt_Note.Size = new System.Drawing.Size(834, 62);
            this.Txt_Note.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(6, 468);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 14);
            this.label7.TabIndex = 694;
            this.label7.Text = "المــــــلاحـظــــات :";
            // 
            // TxtAddress
            // 
            this.TxtAddress.BackColor = System.Drawing.Color.White;
            this.TxtAddress.Enabled = false;
            this.TxtAddress.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtAddress.Location = new System.Drawing.Point(119, 442);
            this.TxtAddress.Multiline = true;
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.ReadOnly = true;
            this.TxtAddress.Size = new System.Drawing.Size(834, 23);
            this.TxtAddress.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(6, 442);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 14);
            this.label6.TabIndex = 698;
            this.label6.Text = "العــــــــــــــــــنوان : ";
            // 
            // Grd_Black_list
            // 
            this.Grd_Black_list.AllowUserToAddRows = false;
            this.Grd_Black_list.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Black_list.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Black_list.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Black_list.ColumnHeadersHeight = 40;
            this.Grd_Black_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column12,
            this.Column1,
            this.Column14,
            this.Column15,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column16,
            this.Column2,
            this.Column3,
            this.Column9});
            this.Grd_Black_list.Location = new System.Drawing.Point(9, 80);
            this.Grd_Black_list.Name = "Grd_Black_list";
            this.Grd_Black_list.ReadOnly = true;
            this.Grd_Black_list.RowHeadersWidth = 10;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Black_list.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Black_list.Size = new System.Drawing.Size(622, 235);
            this.Grd_Black_list.TabIndex = 3;
            this.Grd_Black_list.SelectionChanged += new System.EventHandler(this.Grd_Cust_SelectionChanged);
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Bl_List_id";
            this.Column12.Frozen = true;
            this.Column12.HeaderText = "الرقم التعريفي";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 50;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "AB_NAME";
            this.Column1.HeaderText = "ألاسم العــربي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "EB_NAME";
            this.Column14.HeaderText = "الاسم الاجــنبي";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 150;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "BL_FLAG_ASTATE";
            this.Column15.HeaderText = "حالتـه";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "BIRTH_DATE";
            this.Column4.HeaderText = "تاريخ الولادة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "PLACE_BIRTH";
            this.Column5.HeaderText = "مكان الولادة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 160;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Nat_AName";
            this.Column6.HeaderText = "الجنسية";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Occupation";
            this.Column7.HeaderText = "المهنة";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Mother_Aname";
            this.Column8.HeaderText = "اسم الام العربي";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 150;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Mother_Ename";
            this.Column16.HeaderText = "اسم الام الأجنبي";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "BLACK_N1";
            this.Column2.HeaderText = "اسم الشهرة الاول";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "BLACK_N2";
            this.Column3.HeaderText = "اسم الشهرة الثاني ";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "User_Name";
            this.Column9.HeaderText = "المـــنظم";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Grd_Relatives
            // 
            this.Grd_Relatives.AllowUserToAddRows = false;
            this.Grd_Relatives.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Relatives.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Relatives.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Relatives.ColumnHeadersHeight = 30;
            this.Grd_Relatives.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.Column11,
            this.Column13});
            this.Grd_Relatives.Location = new System.Drawing.Point(9, 342);
            this.Grd_Relatives.Name = "Grd_Relatives";
            this.Grd_Relatives.ReadOnly = true;
            this.Grd_Relatives.RowHeadersWidth = 10;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Relatives.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Relatives.Size = new System.Drawing.Size(622, 94);
            this.Grd_Relatives.TabIndex = 6;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Rel_aname";
            this.Column10.HeaderText = "الصــلة";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 110;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Black_rel_aname";
            this.Column11.HeaderText = "الاسم العربي";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 250;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Black_rel_ename";
            this.Column13.HeaderText = "الاسم الانكليزي";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 250;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(637, 70);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 368);
            this.flowLayoutPanel4.TabIndex = 705;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(958, 22);
            this.statusStrip1.TabIndex = 706;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.DarkRed;
            this.label5.Location = new System.Drawing.Point(12, 319);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 14);
            this.label5.TabIndex = 707;
            this.label5.Text = "الأقـــــــــارب.....";
            // 
            // flowLayoutPanel189
            // 
            this.flowLayoutPanel189.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel190);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel191);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel192);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel193);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel194);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel204);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel219);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel249);
            this.flowLayoutPanel189.Location = new System.Drawing.Point(105, 330);
            this.flowLayoutPanel189.Name = "flowLayoutPanel189";
            this.flowLayoutPanel189.Size = new System.Drawing.Size(533, 1);
            this.flowLayoutPanel189.TabIndex = 708;
            // 
            // flowLayoutPanel190
            // 
            this.flowLayoutPanel190.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel190.Location = new System.Drawing.Point(526, 3);
            this.flowLayoutPanel190.Name = "flowLayoutPanel190";
            this.flowLayoutPanel190.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel190.TabIndex = 634;
            // 
            // flowLayoutPanel191
            // 
            this.flowLayoutPanel191.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel191.Location = new System.Drawing.Point(518, 3);
            this.flowLayoutPanel191.Name = "flowLayoutPanel191";
            this.flowLayoutPanel191.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel191.TabIndex = 633;
            // 
            // flowLayoutPanel192
            // 
            this.flowLayoutPanel192.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel192.Location = new System.Drawing.Point(-235, 241);
            this.flowLayoutPanel192.Name = "flowLayoutPanel192";
            this.flowLayoutPanel192.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel192.TabIndex = 632;
            // 
            // flowLayoutPanel193
            // 
            this.flowLayoutPanel193.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel193.Location = new System.Drawing.Point(-235, 249);
            this.flowLayoutPanel193.Name = "flowLayoutPanel193";
            this.flowLayoutPanel193.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel193.TabIndex = 631;
            // 
            // flowLayoutPanel194
            // 
            this.flowLayoutPanel194.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel195);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel196);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel197);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel198);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel199);
            this.flowLayoutPanel194.Location = new System.Drawing.Point(-80, 257);
            this.flowLayoutPanel194.Name = "flowLayoutPanel194";
            this.flowLayoutPanel194.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel194.TabIndex = 676;
            // 
            // flowLayoutPanel195
            // 
            this.flowLayoutPanel195.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel195.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel195.Name = "flowLayoutPanel195";
            this.flowLayoutPanel195.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel195.TabIndex = 634;
            // 
            // flowLayoutPanel196
            // 
            this.flowLayoutPanel196.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel196.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel196.Name = "flowLayoutPanel196";
            this.flowLayoutPanel196.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel196.TabIndex = 633;
            // 
            // flowLayoutPanel197
            // 
            this.flowLayoutPanel197.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel197.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel197.Name = "flowLayoutPanel197";
            this.flowLayoutPanel197.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel197.TabIndex = 632;
            // 
            // flowLayoutPanel198
            // 
            this.flowLayoutPanel198.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel198.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel198.Name = "flowLayoutPanel198";
            this.flowLayoutPanel198.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel198.TabIndex = 631;
            // 
            // flowLayoutPanel199
            // 
            this.flowLayoutPanel199.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel200);
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel201);
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel202);
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel203);
            this.flowLayoutPanel199.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel199.Name = "flowLayoutPanel199";
            this.flowLayoutPanel199.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel199.TabIndex = 676;
            // 
            // flowLayoutPanel200
            // 
            this.flowLayoutPanel200.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel200.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel200.Name = "flowLayoutPanel200";
            this.flowLayoutPanel200.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel200.TabIndex = 634;
            // 
            // flowLayoutPanel201
            // 
            this.flowLayoutPanel201.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel201.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel201.Name = "flowLayoutPanel201";
            this.flowLayoutPanel201.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel201.TabIndex = 633;
            // 
            // flowLayoutPanel202
            // 
            this.flowLayoutPanel202.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel202.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel202.Name = "flowLayoutPanel202";
            this.flowLayoutPanel202.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel202.TabIndex = 632;
            // 
            // flowLayoutPanel203
            // 
            this.flowLayoutPanel203.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel203.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel203.Name = "flowLayoutPanel203";
            this.flowLayoutPanel203.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel203.TabIndex = 631;
            // 
            // flowLayoutPanel204
            // 
            this.flowLayoutPanel204.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel205);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel206);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel207);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel208);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel209);
            this.flowLayoutPanel204.Location = new System.Drawing.Point(-80, 264);
            this.flowLayoutPanel204.Name = "flowLayoutPanel204";
            this.flowLayoutPanel204.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel204.TabIndex = 677;
            // 
            // flowLayoutPanel205
            // 
            this.flowLayoutPanel205.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel205.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel205.Name = "flowLayoutPanel205";
            this.flowLayoutPanel205.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel205.TabIndex = 634;
            // 
            // flowLayoutPanel206
            // 
            this.flowLayoutPanel206.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel206.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel206.Name = "flowLayoutPanel206";
            this.flowLayoutPanel206.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel206.TabIndex = 633;
            // 
            // flowLayoutPanel207
            // 
            this.flowLayoutPanel207.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel207.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel207.Name = "flowLayoutPanel207";
            this.flowLayoutPanel207.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel207.TabIndex = 632;
            // 
            // flowLayoutPanel208
            // 
            this.flowLayoutPanel208.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel208.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel208.Name = "flowLayoutPanel208";
            this.flowLayoutPanel208.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel208.TabIndex = 631;
            // 
            // flowLayoutPanel209
            // 
            this.flowLayoutPanel209.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel210);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel211);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel212);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel213);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel214);
            this.flowLayoutPanel209.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel209.Name = "flowLayoutPanel209";
            this.flowLayoutPanel209.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel209.TabIndex = 676;
            // 
            // flowLayoutPanel210
            // 
            this.flowLayoutPanel210.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel210.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel210.Name = "flowLayoutPanel210";
            this.flowLayoutPanel210.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel210.TabIndex = 634;
            // 
            // flowLayoutPanel211
            // 
            this.flowLayoutPanel211.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel211.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel211.Name = "flowLayoutPanel211";
            this.flowLayoutPanel211.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel211.TabIndex = 633;
            // 
            // flowLayoutPanel212
            // 
            this.flowLayoutPanel212.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel212.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel212.Name = "flowLayoutPanel212";
            this.flowLayoutPanel212.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel212.TabIndex = 632;
            // 
            // flowLayoutPanel213
            // 
            this.flowLayoutPanel213.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel213.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel213.Name = "flowLayoutPanel213";
            this.flowLayoutPanel213.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel213.TabIndex = 631;
            // 
            // flowLayoutPanel214
            // 
            this.flowLayoutPanel214.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel215);
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel216);
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel217);
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel218);
            this.flowLayoutPanel214.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel214.Name = "flowLayoutPanel214";
            this.flowLayoutPanel214.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel214.TabIndex = 676;
            // 
            // flowLayoutPanel215
            // 
            this.flowLayoutPanel215.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel215.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel215.Name = "flowLayoutPanel215";
            this.flowLayoutPanel215.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel215.TabIndex = 634;
            // 
            // flowLayoutPanel216
            // 
            this.flowLayoutPanel216.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel216.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel216.Name = "flowLayoutPanel216";
            this.flowLayoutPanel216.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel216.TabIndex = 633;
            // 
            // flowLayoutPanel217
            // 
            this.flowLayoutPanel217.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel217.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel217.Name = "flowLayoutPanel217";
            this.flowLayoutPanel217.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel217.TabIndex = 632;
            // 
            // flowLayoutPanel218
            // 
            this.flowLayoutPanel218.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel218.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel218.Name = "flowLayoutPanel218";
            this.flowLayoutPanel218.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel218.TabIndex = 631;
            // 
            // flowLayoutPanel219
            // 
            this.flowLayoutPanel219.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel220);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel221);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel222);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel223);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel224);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel234);
            this.flowLayoutPanel219.Location = new System.Drawing.Point(-80, 271);
            this.flowLayoutPanel219.Name = "flowLayoutPanel219";
            this.flowLayoutPanel219.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel219.TabIndex = 678;
            // 
            // flowLayoutPanel220
            // 
            this.flowLayoutPanel220.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel220.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel220.Name = "flowLayoutPanel220";
            this.flowLayoutPanel220.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel220.TabIndex = 634;
            // 
            // flowLayoutPanel221
            // 
            this.flowLayoutPanel221.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel221.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel221.Name = "flowLayoutPanel221";
            this.flowLayoutPanel221.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel221.TabIndex = 633;
            // 
            // flowLayoutPanel222
            // 
            this.flowLayoutPanel222.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel222.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel222.Name = "flowLayoutPanel222";
            this.flowLayoutPanel222.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel222.TabIndex = 632;
            // 
            // flowLayoutPanel223
            // 
            this.flowLayoutPanel223.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel223.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel223.Name = "flowLayoutPanel223";
            this.flowLayoutPanel223.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel223.TabIndex = 631;
            // 
            // flowLayoutPanel224
            // 
            this.flowLayoutPanel224.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel225);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel226);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel227);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel228);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel229);
            this.flowLayoutPanel224.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel224.Name = "flowLayoutPanel224";
            this.flowLayoutPanel224.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel224.TabIndex = 676;
            // 
            // flowLayoutPanel225
            // 
            this.flowLayoutPanel225.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel225.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel225.Name = "flowLayoutPanel225";
            this.flowLayoutPanel225.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel225.TabIndex = 634;
            // 
            // flowLayoutPanel226
            // 
            this.flowLayoutPanel226.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel226.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel226.Name = "flowLayoutPanel226";
            this.flowLayoutPanel226.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel226.TabIndex = 633;
            // 
            // flowLayoutPanel227
            // 
            this.flowLayoutPanel227.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel227.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel227.Name = "flowLayoutPanel227";
            this.flowLayoutPanel227.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel227.TabIndex = 632;
            // 
            // flowLayoutPanel228
            // 
            this.flowLayoutPanel228.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel228.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel228.Name = "flowLayoutPanel228";
            this.flowLayoutPanel228.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel228.TabIndex = 631;
            // 
            // flowLayoutPanel229
            // 
            this.flowLayoutPanel229.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel230);
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel231);
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel232);
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel233);
            this.flowLayoutPanel229.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel229.Name = "flowLayoutPanel229";
            this.flowLayoutPanel229.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel229.TabIndex = 676;
            // 
            // flowLayoutPanel230
            // 
            this.flowLayoutPanel230.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel230.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel230.Name = "flowLayoutPanel230";
            this.flowLayoutPanel230.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel230.TabIndex = 634;
            // 
            // flowLayoutPanel231
            // 
            this.flowLayoutPanel231.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel231.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel231.Name = "flowLayoutPanel231";
            this.flowLayoutPanel231.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel231.TabIndex = 633;
            // 
            // flowLayoutPanel232
            // 
            this.flowLayoutPanel232.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel232.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel232.Name = "flowLayoutPanel232";
            this.flowLayoutPanel232.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel232.TabIndex = 632;
            // 
            // flowLayoutPanel233
            // 
            this.flowLayoutPanel233.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel233.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel233.Name = "flowLayoutPanel233";
            this.flowLayoutPanel233.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel233.TabIndex = 631;
            // 
            // flowLayoutPanel234
            // 
            this.flowLayoutPanel234.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel235);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel236);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel237);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel238);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel239);
            this.flowLayoutPanel234.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel234.Name = "flowLayoutPanel234";
            this.flowLayoutPanel234.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel234.TabIndex = 677;
            // 
            // flowLayoutPanel235
            // 
            this.flowLayoutPanel235.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel235.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel235.Name = "flowLayoutPanel235";
            this.flowLayoutPanel235.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel235.TabIndex = 634;
            // 
            // flowLayoutPanel236
            // 
            this.flowLayoutPanel236.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel236.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel236.Name = "flowLayoutPanel236";
            this.flowLayoutPanel236.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel236.TabIndex = 633;
            // 
            // flowLayoutPanel237
            // 
            this.flowLayoutPanel237.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel237.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel237.Name = "flowLayoutPanel237";
            this.flowLayoutPanel237.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel237.TabIndex = 632;
            // 
            // flowLayoutPanel238
            // 
            this.flowLayoutPanel238.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel238.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel238.Name = "flowLayoutPanel238";
            this.flowLayoutPanel238.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel238.TabIndex = 631;
            // 
            // flowLayoutPanel239
            // 
            this.flowLayoutPanel239.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel240);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel241);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel242);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel243);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel244);
            this.flowLayoutPanel239.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel239.Name = "flowLayoutPanel239";
            this.flowLayoutPanel239.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel239.TabIndex = 676;
            // 
            // flowLayoutPanel240
            // 
            this.flowLayoutPanel240.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel240.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel240.Name = "flowLayoutPanel240";
            this.flowLayoutPanel240.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel240.TabIndex = 634;
            // 
            // flowLayoutPanel241
            // 
            this.flowLayoutPanel241.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel241.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel241.Name = "flowLayoutPanel241";
            this.flowLayoutPanel241.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel241.TabIndex = 633;
            // 
            // flowLayoutPanel242
            // 
            this.flowLayoutPanel242.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel242.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel242.Name = "flowLayoutPanel242";
            this.flowLayoutPanel242.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel242.TabIndex = 632;
            // 
            // flowLayoutPanel243
            // 
            this.flowLayoutPanel243.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel243.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel243.Name = "flowLayoutPanel243";
            this.flowLayoutPanel243.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel243.TabIndex = 631;
            // 
            // flowLayoutPanel244
            // 
            this.flowLayoutPanel244.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel245);
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel246);
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel247);
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel248);
            this.flowLayoutPanel244.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel244.Name = "flowLayoutPanel244";
            this.flowLayoutPanel244.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel244.TabIndex = 676;
            // 
            // flowLayoutPanel245
            // 
            this.flowLayoutPanel245.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel245.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel245.Name = "flowLayoutPanel245";
            this.flowLayoutPanel245.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel245.TabIndex = 634;
            // 
            // flowLayoutPanel246
            // 
            this.flowLayoutPanel246.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel246.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel246.Name = "flowLayoutPanel246";
            this.flowLayoutPanel246.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel246.TabIndex = 633;
            // 
            // flowLayoutPanel247
            // 
            this.flowLayoutPanel247.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel247.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel247.Name = "flowLayoutPanel247";
            this.flowLayoutPanel247.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel247.TabIndex = 632;
            // 
            // flowLayoutPanel248
            // 
            this.flowLayoutPanel248.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel248.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel248.Name = "flowLayoutPanel248";
            this.flowLayoutPanel248.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel248.TabIndex = 631;
            // 
            // flowLayoutPanel249
            // 
            this.flowLayoutPanel249.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel249.Controls.Add(this.flowLayoutPanel250);
            this.flowLayoutPanel249.Location = new System.Drawing.Point(526, 278);
            this.flowLayoutPanel249.Name = "flowLayoutPanel249";
            this.flowLayoutPanel249.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel249.TabIndex = 679;
            // 
            // flowLayoutPanel250
            // 
            this.flowLayoutPanel250.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel250.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel250.Name = "flowLayoutPanel250";
            this.flowLayoutPanel250.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel250.TabIndex = 632;
            // 
            // Btn_Previous
            // 
            this.Btn_Previous.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Previous.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Previous.Location = new System.Drawing.Point(704, 413);
            this.Btn_Previous.Name = "Btn_Previous";
            this.Btn_Previous.Size = new System.Drawing.Size(107, 23);
            this.Btn_Previous.TabIndex = 4;
            this.Btn_Previous.Text = "<<";
            this.Btn_Previous.UseVisualStyleBackColor = true;
            this.Btn_Previous.Click += new System.EventHandler(this.Btn_Previous_Click);
            // 
            // Btn_Next
            // 
            this.Btn_Next.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Btn_Next.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Next.Location = new System.Drawing.Point(817, 414);
            this.Btn_Next.Name = "Btn_Next";
            this.Btn_Next.Size = new System.Drawing.Size(107, 22);
            this.Btn_Next.TabIndex = 5;
            this.Btn_Next.Text = ">>";
            this.Btn_Next.UseVisualStyleBackColor = true;
            this.Btn_Next.Click += new System.EventHandler(this.Btn_Next_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(644, 80);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(309, 331);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 701;
            this.pictureBox1.TabStop = false;
            // 
            // Black_List_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 562);
            this.Controls.Add(this.Btn_Previous);
            this.Controls.Add(this.Btn_Next);
            this.Controls.Add(this.flowLayoutPanel189);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Grd_Relatives);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Grd_Black_list);
            this.Controls.Add(this.TxtAddress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_Note);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Black_List_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "178";
            this.Text = "Black_List_Main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Black_List_Main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Black_List_Main_FormClosed);
            this.Load += new System.EventHandler(this.Black_List_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Black_List_Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Black_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Relatives)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel189.ResumeLayout(false);
            this.flowLayoutPanel194.ResumeLayout(false);
            this.flowLayoutPanel199.ResumeLayout(false);
            this.flowLayoutPanel204.ResumeLayout(false);
            this.flowLayoutPanel209.ResumeLayout(false);
            this.flowLayoutPanel214.ResumeLayout(false);
            this.flowLayoutPanel219.ResumeLayout(false);
            this.flowLayoutPanel224.ResumeLayout(false);
            this.flowLayoutPanel229.ResumeLayout(false);
            this.flowLayoutPanel234.ResumeLayout(false);
            this.flowLayoutPanel239.ResumeLayout(false);
            this.flowLayoutPanel244.ResumeLayout(false);
            this.flowLayoutPanel249.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BtnUpd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripTextBox Txt_BlackSearch;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox Txt_Note;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView Grd_Black_list;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView Grd_Relatives;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel189;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel190;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel191;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel192;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel193;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel194;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel195;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel196;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel197;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel198;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel199;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel200;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel201;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel202;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel203;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel204;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel205;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel206;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel207;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel208;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel209;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel210;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel211;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel212;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel213;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel214;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel215;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel216;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel217;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel218;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel219;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel220;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel221;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel222;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel223;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel224;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel225;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel226;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel227;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel228;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel229;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel230;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel231;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel232;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel233;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel234;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel235;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel236;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel237;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel238;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel239;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel240;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel241;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel242;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel243;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel244;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel245;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel246;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel247;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel248;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel249;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel250;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.ToolStripButton StopBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button Btn_Previous;
        private System.Windows.Forms.Button Btn_Next;
        private System.Windows.Forms.ToolStripButton SearchBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
    }
}