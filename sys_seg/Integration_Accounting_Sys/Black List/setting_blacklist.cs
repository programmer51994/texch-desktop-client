﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class setting_blacklist : Form
    {
        #region Defintion
        BindingSource binding_Black_List = new BindingSource();
        BindingSource binding_Condition_BL = new BindingSource();
        BindingSource binding_Condition_S_Rem = new BindingSource();
        BindingSource binding_Condition_R_Rem = new BindingSource();
        BindingSource BS = new BindingSource();
        string Sql_Text = "";
        string Sql_Text1 = "";
        DataTable Dt_blacklist;
        DataTable Dt_Condition_BL;
        DataTable Dt_Rem_S_R;
        Int16 Similar_BL = 0;
        Int16 Sen_rem_day = 0;
        Int16 Rec_rem_day = 0;
        Int16 Similar_Rem = 0;
        Int16 search_type = 0;

        #endregion

        public setting_blacklist()
        {
            InitializeComponent(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Black_List.AutoGenerateColumns = false;
            Grd_Condition_BL.AutoGenerateColumns = false;
            Grd_Condition_S_Rem.AutoGenerateColumns = false;
            Grd_Condition_R_Rem.AutoGenerateColumns = false;

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Condition_BL.Columns["dataGridViewTextBoxColumn1"].DataPropertyName = "En_Condition";
                Grd_Condition_S_Rem.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "En_Online_Condition";
                Grd_Condition_R_Rem.Columns["dataGridViewTextBoxColumn3"].DataPropertyName = "En_Online_Condition";

            }
            #endregion
            
        }

        private void setting_blacklist_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {
                Cbo_search_type.Items.Clear();
                Cbo_search_type.Items.Add("Conditions are combined");
                Cbo_search_type.Items.Add("Conditions are individual");

                Cbo_Search_BL_Rem.Items.Clear();
                Cbo_Search_BL_Rem.Items.Add("without");
                Cbo_Search_BL_Rem.Items.Add("Similarity and Matching");
                Cbo_Search_BL_Rem.Items.Add("match");

            }

            Sql_Text = " Search_Condition_Blist_Online ";
            connection.SqlExec(Sql_Text, "Search_Condition_Blist_Online_Tbl");

            if (connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl"].Rows.Count > 0)
            {

                binding_Black_List.DataSource = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl"];
                Grd_Black_List.DataSource = binding_Black_List;
                similarity_Txt.Text = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl"].Rows[0]["Similar"].ToString();
            }

            
             else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد لديك قوائم منع" : "You do not have blacklists", MyGeneral_Lib.LblCap);
                return;
            }
          


            if (connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl1"].Rows.Count > 0)
            {
                binding_Condition_BL.DataSource = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl1"];
                Grd_Condition_BL.DataSource = binding_Condition_BL;
            }


            if (connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl2"].Rows.Count > 0)
            {
                binding_Condition_S_Rem.DataSource = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl2"];
                Grd_Condition_S_Rem.DataSource = binding_Condition_S_Rem;
                Search_Day_S_Txt.Text = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl2"].Rows[0]["Count_Sen_Day"].ToString();
                similarity_Rem_S_R_Txt.Text = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl2"].Rows[0]["Similar_Online"].ToString();
            }

            if (connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl3"].Rows.Count > 0)
            {
                binding_Condition_R_Rem.DataSource = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl3"];
                Grd_Condition_R_Rem.DataSource = binding_Condition_R_Rem;
                Search_Day_R_Txt.Text = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl3"].Rows[0]["Count_Rec_Day"].ToString();
            }


            if (connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl4"].Rows.Count > 0)
            {
                Int16 search_type_rem = 0;
                search_type_rem = Convert.ToInt16(connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl4"].Rows[0]["cond_search_id"]);


                if (search_type_rem == 2)
                {
                    search_type_rem = 0;
                }

                Cbo_Search_BL_Rem.SelectedIndex = Convert.ToInt16(connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl4"].Rows[0]["Cond_BL_Rem"]);
                Cbo_search_type.SelectedIndex = search_type_rem;

            }

           

            //Cbo_search_type.SelectedIndex = 0;
            //Cbo_Search_BL_Rem.SelectedIndex = 0;
        }

        private void setting_blacklist_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Search_Condition_Blist_Online_Tbl", "Search_Condition_Blist_Online_Tbl1",
                                  "Search_Condition_Blist_Online_Tbl2" ,"Search_Condition_Blist_Online_Tbl3"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            DataTable Dt_blacklist1 = new DataTable();
            
            try
            {
               
                Dt_blacklist1 = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl"].DefaultView.ToTable(false, "Chk", "list_name").Select("chk > 0").CopyToDataTable();
                Dt_blacklist1 = Dt_blacklist1.DefaultView.ToTable(false, "list_name").Select().CopyToDataTable();
            }
             catch
             {
                 MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد قوائم المنع" : "Please select blacklists", MyGeneral_Lib.LblCap);
                 return;
             }

            //-------------------------------------------- condition blacklist
            DataTable Dt_blacklist2 = new DataTable();
            try
            {               
                Dt_blacklist2 = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl1"].DefaultView.ToTable(false, "Chk", "Col_Condition", "En_Condition", "AR_Condition").Select("Chk > 0").CopyToDataTable();
                Dt_blacklist2 = Dt_blacklist2.DefaultView.ToTable(false, "Col_Condition", "En_Condition", "AR_Condition").Select().CopyToDataTable();
            }
            catch
            {
               MessageBox.Show(connection.Lang_id == 1 ? "الرجاء اختيار  شروط البحث  لقوائم المنع" : "Please choose search terms for blacklists", MyGeneral_Lib.LblCap);
              return;           
            }
       

            //----------------------------------------------------Rem_Sender
            DataTable Dt_blacklist3 = new DataTable();
            try
            {               
                Dt_blacklist3 = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl2"].DefaultView.ToTable(false, "Chk", "Col_Online", "Col_r_type_id", "En_Online_Condition", "AR_Online_Condition").Select("Chk > 0").CopyToDataTable();
                Dt_blacklist3 = Dt_blacklist3.DefaultView.ToTable(false, "Col_Online", "Col_r_type_id", "En_Online_Condition", "AR_Online_Condition").Select().CopyToDataTable();
            }
            catch
            {
              MessageBox.Show(connection.Lang_id == 1 ? "الرجاء اختيار  شروط البحث  للحوالات الصادرة" : "Please choose search terms for outgoing Remittences", MyGeneral_Lib.LblCap);
              return; 
            }
          

            //---------------------------------------------Rem_Receiver
            DataTable Dt_blacklist4 = new DataTable();
            try
            {
               
                Dt_blacklist4 = connection.SQLDS.Tables["Search_Condition_Blist_Online_Tbl3"].DefaultView.ToTable(false, "Chk", "Col_Online", "Col_r_type_id", "En_Online_Condition", "AR_Online_Condition").Select("Chk > 0").CopyToDataTable();
                Dt_blacklist4 = Dt_blacklist4.DefaultView.ToTable(false, "Col_Online", "Col_r_type_id", "En_Online_Condition", "AR_Online_Condition").Select().CopyToDataTable();
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء اختيار  شروط البحث  للحوالات الواردة" : "Please choose search terms for incoming Remittences", MyGeneral_Lib.LblCap);
                return;
            }
          

            //----------------------------------------------------------

            if (similarity_Txt.Text != "" && similarity_Txt.Text != "0")
            {

                Similar_BL = Convert.ToInt16(similarity_Txt.Text);
            }
            else
            {
               MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال حد التشابه لقوائم المنع" : "Please enter the similar limit for black lists", MyGeneral_Lib.LblCap);
               return;            
            }
            //-----------------------------------------
            if (Search_Day_S_Txt.Text != "" && Search_Day_S_Txt.Text != "0")
            {
                Sen_rem_day = Convert.ToInt16(Search_Day_S_Txt.Text);
            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال عدد ايام البحث للحوالات الصادرة" : "Please enter the number of search days for outgoing remittences", MyGeneral_Lib.LblCap);
                return;
            }

            //----------------------------------
            if (Search_Day_R_Txt.Text != "" && Search_Day_R_Txt.Text != "0")
            {
                Rec_rem_day = Convert.ToInt16(Search_Day_R_Txt.Text);
            }

            else
            {
               MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال عدد ايام البحث للحوالات الواردة" : "Please enter the number of search days for incoming remittences", MyGeneral_Lib.LblCap);
               return;
            }
            //------------------------------------
            if (similarity_Rem_S_R_Txt.Text != "" && similarity_Rem_S_R_Txt.Text != "0")
            {

                Similar_Rem = Convert.ToInt16(similarity_Rem_S_R_Txt.Text);
            }

            else
            {

                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال حد التشابه  لبحث الحوالات" : "Please enter a similarity limit to search for remittances", MyGeneral_Lib.LblCap);
                return;
            }


            if (Cbo_search_type.SelectedIndex == 0)
            {
                search_type = 2;

            }
            if (Cbo_search_type.SelectedIndex == 1)
            
            {
                search_type = 1;
            }
         
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[Add_Condition_Blist_Online]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@Tbl_Blist1", Dt_blacklist1);
                        connection.SQLCMD.Parameters.AddWithValue("@Tbl_Blist2", Dt_blacklist2);
                        connection.SQLCMD.Parameters.AddWithValue("@Tbl_Blist3", Dt_blacklist3);
                        connection.SQLCMD.Parameters.AddWithValue("@Tbl_Blist4", Dt_blacklist4);
                        connection.SQLCMD.Parameters.AddWithValue("@similar_Blist", Similar_BL);
                        connection.SQLCMD.Parameters.AddWithValue("@Count_Sen_Day", Sen_rem_day);
                        connection.SQLCMD.Parameters.AddWithValue("@Count_Rec_Day", Rec_rem_day);
                        connection.SQLCMD.Parameters.AddWithValue("@Similar_Online", Similar_Rem);
                        connection.SQLCMD.Parameters.AddWithValue("@cond_id", search_type);
                        connection.SQLCMD.Parameters.AddWithValue("@Cond_id_BL_Rem", Cbo_Search_BL_Rem.SelectedIndex);
                        connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                        connection.SQLCMD.Parameters.AddWithValue("@Chk_rem_no", Chk_rem_no.Checked);
                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();

                        MessageBox.Show(connection.Lang_id == 1 ? "تم ضبط الاعدادات" : "Settings set", MyGeneral_Lib.LblCap);
                        this.Close();
                   
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }
    }
}
