﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Integration_Accounting_Sys
{
    public partial class Black_List_Main : Form
    {
        #region Definition
        bool Change = false;
        Byte[] img_data = new Byte[0];
        string Sql_text = "";
        int Black_id = 0;
        byte State = 0;
        
        #endregion
        public Black_List_Main()
        {
            InitializeComponent();
            //connection.SQLBS.DataSource = new BindingSource(); 
            //connection.SQLBSSubGrd1.DataSource = new BindingSource(); 
            //connection.SQLBSMainGrd.DataSource = new BindingSource(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

            Grd_Black_list.AutoGenerateColumns = false;
            Grd_Relatives.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Relatives .Columns["column10"].DataPropertyName = "Rel_ename";
                Grd_Black_list.Columns["column15"].DataPropertyName = "BL_FLAG_ESTATE";
                Grd_Black_list.Columns["column6"].DataPropertyName = "Nat_ename";
            }
        }
        //-----------------------------
        private void Black_List_Main_Load(object sender, EventArgs e)
        {
            Change = false;
            connection.SQLBS.DataSource = connection.SqlExec("Main_Black_list " + Txt_BlackSearch.Text, "Black_Lst_Tbl");
            if (connection.SQLDS.Tables["Black_Lst_Tbl"].Rows.Count > 0)
            {

                Grd_Black_list.DataSource = connection.SQLBS;
                Change = true;
                Grd_Cust_SelectionChanged(sender, e);
                BtnUpd.Enabled = true;
            }
            else
            {
                BtnUpd.Enabled = false;
                Grd_Relatives.DataSource = new BindingSource();
                pictureBox1.Image = null;
            } 

        }
        //-----------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Black_List_Add_Upd AddFrm = new Black_List_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Black_List_Main_Load(sender, e);
            this.Visible = true;
        }
        //-----------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
               
                Black_id = Convert.ToInt32(((DataRowView)connection.SQLBS.Current).Row["bl_list_id"]);
                State = Convert.ToByte(((DataRowView)connection.SQLBS.Current).Row["Bl_flag"]);
                Sql_text = "   select A.BL_LIST_ID ,B.Pic_Id,B.BL_PIC  from BLACK_LIST A,Black_list_pic B "
                + " WHERE A.BL_LIST_ID = B.BL_LIST_ID "
                + " and B.Bl_LIST_ID = " + Black_id ;
                connection.SQLBSSubGrd1.DataSource = connection.SqlExec(Sql_text, "BL_Pic_Tbl");
                if (connection.SQLDS.Tables["BL_Pic_Tbl"].Rows.Count > 0 )
                {
                    img_data = (Byte[])((DataRowView)connection.SQLBSSubGrd1.Current).Row["BL_PIC"];
                    MemoryStream mem = new MemoryStream(img_data);
                    pictureBox1.Image = Image.FromStream(mem);
                }
                else
                {
                    pictureBox1.Image = null;
                }
                if (connection.SQLDS.Tables["BL_Pic_Tbl"].Rows.Count > 1)
                {
                    Btn_Next.Enabled = true;
                    Btn_Previous.Enabled = true;
                }
                else
                {
                    Btn_Next.Enabled = false;
                    Btn_Previous.Enabled = false;
                }
                Txt_Note.DataBindings.Clear();
                TxtAddress.DataBindings.Clear();
                Txt_Note.DataBindings.Add("Text", connection.SQLBS, "NOTES");
                TxtAddress.DataBindings.Add("Text", connection.SQLBS, "Address");

                try
                {
                    connection.SQLBSMainGrd.DataSource = connection.SQLDS.Tables["Black_Lst_Tbl1"].Select("Bl_list_id = " + Black_id).CopyToDataTable();
                    Grd_Relatives.DataSource = connection.SQLBSMainGrd;
                }
                catch
                { 
                    connection.SQLBSMainGrd.DataSource = null;  //تفريغ الجدول
                }
                LblRec.Text = connection.Records(connection.SQLBS);
                if (State == 0)
                    StopBtn.Text = connection.Lang_id == 1 ? "رفع من القائمة" : "Deactivate";
                else
                    StopBtn.Text = connection.Lang_id == 1 ? "تفعيل" : "Activate";

            }

        }
        //-----------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            Black_List_Add_Upd AddFrm = new Black_List_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Black_List_Main_Load(sender, e);
            this.Visible = true;
        }
        //-----------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Txt_BlackSearch.Text = "";
            Black_List_Main_Load(sender, e);
        }
        //-----------------------------
        private void Btn_State_Click(object sender, EventArgs e)
        {
            State ^= 1;
            object [] Sparams = {Black_id,State,connection.user_id};
            connection.SqlExec("U_Black_List_State", "Blupdate", Sparams);
            Black_List_Main_Load(sender, e);
        }
        //-----------------------------
        private void Btn_Next_Click(object sender, EventArgs e)
        {
            connection.SQLBSSubGrd1.MoveNext();
            img_data = (Byte[])((DataRowView)connection.SQLBSSubGrd1.Current).Row["BL_PIC"];
            MemoryStream mem = new MemoryStream(img_data);
            pictureBox1.Image = Image.FromStream(mem);

        }
        //-----------------------------
        private void Btn_Previous_Click(object sender, EventArgs e)
        {
            connection.SQLBSSubGrd1.MovePrevious();
            img_data = (Byte[])((DataRowView)connection.SQLBSSubGrd1.Current).Row["BL_PIC"];
            MemoryStream mem = new MemoryStream(img_data);
            pictureBox1.Image = Image.FromStream(mem);

        }
        //-----------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Black_List_Main_Load(sender, e);
        }
        //-----------------------------
        private void Black_List_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            
            string[] Tbl = { "Black_Lst_Tbl", "BL_Pic_Tbl", "Black_Lst_Tbl1", "Blupdate" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }
        //-----------------------------
        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            DataTable Dt = connection.SQLDS.Tables["Black_Lst_Tbl"].DefaultView.ToTable(false, "BL_LIST_ID", "AB_NAME", "EB_NAME",
                                                                                            "BIRTH_DATE", "PLACE_BIRTH", connection.Lang_id == 1 ? "Nat_Aname" : "Nat_Ename", "Occupation", "MOTHER_ANAME", "MOTHER_ENAME", "BLACK_N1","BLACK_N2"
                                                                                            , connection.Lang_id == 1 ? "BL_FLAG_ASTATE" : "BL_FLAG_ESTATE", "User_Name").Select(connection.Lang_id == 1 ? "BL_FLAG_ASTATE = 'فعال'" : "BL_FLAG_ESTATE = 'Active'").CopyToDataTable();
            DataTable[] _EXP_DT = { Dt };
            DataGridView[] Export_GRD={Grd_Black_list};
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

      
        }

        private void Black_List_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    BtnUpd_Click(sender, e);
                    break;
                case Keys.F5:
                    Btn_State_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F3:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F2:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.F1:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Black_List_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
        //-----------------------------
    }
}