﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace Integration_Accounting_Sys
{
    public partial class black_list_frm : Form
    {
     
        DataTable sheet1_tbl = new DataTable();
      
        public black_list_frm()
        {
            InitializeComponent();
            black_list_dgv.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
          
        }


        private void black_list_frm_Load(object sender, EventArgs e)
        {
            string sql_txt = "";

            if (connection.Lang_id==1)
             sql_txt = "select distinct 1 as number,list_name as list_name from black_list_tbl union select  0 as number,'قائمة جديدة' from black_list_tbl order by number asc ";
            else
             sql_txt = "select distinct 1 as number,list_name as list_name from black_list_tbl union select  0 as number,'New List' from black_list_tbl order by number asc ";

            list_name_cbo.DataSource = connection.SqlExec(sql_txt, "list_name");
            list_name_cbo.DisplayMember = "list_name";
            list_name_cbo.ValueMember = "number";

            LblRec.Text = "0";

            if (connection.Lang_id==1)
                sql_txt = "select 'الحاق' as upload_type union select 'حذف والحاق' as upload_type";
            else
                sql_txt = "select 'Attach' as upload_type union select 'Delete and Attach' as upload_type";

            upload_type_cbo.DataSource = connection.SqlExec(sql_txt, "upload_type_tbl");
            upload_type_cbo.DisplayMember = "upload_type";
            upload_type_cbo.ValueMember = "upload_type";
            upload_type_cbo.SelectedIndex = 0;


            if (list_name_cbo.SelectedIndex == -1)
                upload_type_cbo.Visible = false;

           
        }


        private void list_name_cbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (list_name_cbo.SelectedIndex > 0)
            {
                upload_type_cbo.Visible = true;
            }
            else
            {
                upload_type_cbo.Visible = false;
            }
        }


        private void upload_btn_Click(object sender, EventArgs e)
        {
            sheet1_tbl = new DataTable();
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string excel_con_str = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openFileDialog1.FileName +
                        ";Extended Properties='Excel 12.0 xml;HDR=YES;'";
                OleDbConnection oledbcon = new OleDbConnection(excel_con_str);
                oledbcon.Open();
                OleDbCommand oledbcmd = new OleDbCommand("select * from [sheet1$] order by Name", oledbcon);
                OleDbDataAdapter oledbda = new OleDbDataAdapter(oledbcmd);

                oledbda.Fill(sheet1_tbl);
                oledbcon.Close();

                black_list_dgv.DataSource = sheet1_tbl;
                if (connection.Lang_id==1)
                MessageBox.Show("تمت عملية الاستيراد", "رسالة اعلام", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                MessageBox.Show("Export Complete", "Alarm Msg", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                LblRec.Text = black_list_dgv.Rows.Count.ToString();
            }
        }


        private void clear_btn_Click(object sender, EventArgs e)
        {
            DataTable sheet1_tbl = new DataTable();
            black_list_dgv.DataSource = sheet1_tbl;
            LblRec.Text = "0";
        }


        private void ok_btn_Click(object sender, EventArgs e)
        {

            if (black_list_dgv.RowCount > 0)
            {
                int Rec_No = (from DataRow row in sheet1_tbl.Rows
                              where
                                  row["List_Name"] == DBNull.Value || (string)row["List_Name"] == string.Empty ||
                                  row["Name"] == DBNull.Value || (string)row["Name"] == string.Empty
                              select row).Count();
                if (Rec_No > 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يجب ملئ اسم القائمة واسم الممنوع من التحويل" : "check the list name and the name of person", MyGeneral_Lib.LblCap);
                    return;
                }


                SqlConnection con = new SqlConnection(connection.GetConn());
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "add_black_list";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;

                int oper_id = 0;
                if (list_name_cbo.SelectedIndex == 0)
                    oper_id = 0;
                if (list_name_cbo.SelectedIndex > 0)
                {
                    if (upload_type_cbo.SelectedIndex == 0)
                        oper_id = 1;
                    else
                        oper_id = 2;
                }


                cmd.Parameters.AddWithValue("@oper_id", oper_id);
                cmd.Parameters.AddWithValue("@list_name", list_name_cbo.Text);
                cmd.Parameters.AddWithValue("@user_id", connection.user_id);
                cmd.Parameters.AddWithValue("@black_list_tbl", sheet1_tbl);


                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                if (connection.Lang_id == 1)
                    MessageBox.Show("تمت عملية الحفظ", "رسالة اعلام", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                    MessageBox.Show("Save Succeed", "Alarm Msg", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                sheet1_tbl = new DataTable();
                black_list_dgv.DataSource = sheet1_tbl;
                LblRec.Text = "0";


                black_list_frm_Load(sender, e);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب استيراد القائمة " : "you must import the list", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
    }

}
