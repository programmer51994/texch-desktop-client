﻿namespace Integration_Accounting_Sys
{
    partial class Black_List_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtArb_Name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEng_Name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txtlast_Name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtMother_AName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtBrth_place = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtOccupation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtAddress = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtFrst_name = new System.Windows.Forms.TextBox();
            this.CboNat_id = new System.Windows.Forms.ComboBox();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.BtnDel = new System.Windows.Forms.Button();
            this.Txt_Note = new System.Windows.Forms.TextBox();
            this.Grd_Family = new System.Windows.Forms.DataGridView();
            this.Cbo_Rel_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.Btn_Previous = new System.Windows.Forms.Button();
            this.Btn_Next = new System.Windows.Forms.Button();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtMother_EName = new System.Windows.Forms.TextBox();
            this.TxtBrth_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Net_Connect = new System.Windows.Forms.Label();
            this.Net_Connect1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Family)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1, 34);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(994, 1);
            this.flowLayoutPanel2.TabIndex = 535;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(121, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(208, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(734, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(191, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(678, 10);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 532;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 10);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(106, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "اسم المستخــدم:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(5, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "الإســــم العـــربي: ";
            // 
            // TxtArb_Name
            // 
            this.TxtArb_Name.BackColor = System.Drawing.Color.White;
            this.TxtArb_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtArb_Name.Location = new System.Drawing.Point(127, 40);
            this.TxtArb_Name.Multiline = true;
            this.TxtArb_Name.Name = "TxtArb_Name";
            this.TxtArb_Name.Size = new System.Drawing.Size(194, 26);
            this.TxtArb_Name.TabIndex = 2;
            this.TxtArb_Name.Leave += new System.EventHandler(this.TxtArb_Name_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(355, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 14);
            this.label5.TabIndex = 539;
            this.label5.Text = "الإســــم الأجـــنبي:";
            // 
            // TxtEng_Name
            // 
            this.TxtEng_Name.BackColor = System.Drawing.Color.White;
            this.TxtEng_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtEng_Name.Location = new System.Drawing.Point(482, 40);
            this.TxtEng_Name.Multiline = true;
            this.TxtEng_Name.Name = "TxtEng_Name";
            this.TxtEng_Name.Size = new System.Drawing.Size(194, 26);
            this.TxtEng_Name.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(351, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 14);
            this.label7.TabIndex = 543;
            this.label7.Text = "إسم الشهرة الثاني:";
            // 
            // Txtlast_Name
            // 
            this.Txtlast_Name.BackColor = System.Drawing.Color.White;
            this.Txtlast_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txtlast_Name.Location = new System.Drawing.Point(482, 70);
            this.Txtlast_Name.Multiline = true;
            this.Txtlast_Name.Name = "Txtlast_Name";
            this.Txtlast_Name.Size = new System.Drawing.Size(194, 26);
            this.Txtlast_Name.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(4, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 14);
            this.label6.TabIndex = 541;
            this.label6.Text = "إسم الشهرة الأول:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(4, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 14);
            this.label9.TabIndex = 547;
            this.label9.Text = "اسم الام العــربي :";
            // 
            // TxtMother_AName
            // 
            this.TxtMother_AName.BackColor = System.Drawing.Color.White;
            this.TxtMother_AName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtMother_AName.Location = new System.Drawing.Point(127, 100);
            this.TxtMother_AName.Multiline = true;
            this.TxtMother_AName.Name = "TxtMother_AName";
            this.TxtMother_AName.Size = new System.Drawing.Size(194, 26);
            this.TxtMother_AName.TabIndex = 6;
            this.TxtMother_AName.Leave += new System.EventHandler(this.TxtMother_AName_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(3, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 14);
            this.label8.TabIndex = 545;
            this.label8.Text = "تـــــــــأريخ الــولادة :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(353, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 14);
            this.label11.TabIndex = 551;
            this.label11.Text = "مكـــــــان الـــــولادة :";
            // 
            // TxtBrth_place
            // 
            this.TxtBrth_place.BackColor = System.Drawing.Color.White;
            this.TxtBrth_place.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtBrth_place.Location = new System.Drawing.Point(482, 129);
            this.TxtBrth_place.Multiline = true;
            this.TxtBrth_place.Name = "TxtBrth_place";
            this.TxtBrth_place.Size = new System.Drawing.Size(194, 26);
            this.TxtBrth_place.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(7, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 14);
            this.label10.TabIndex = 549;
            this.label10.Text = "الجنســــــــــــــــية:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(355, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 14);
            this.label12.TabIndex = 555;
            this.label12.Text = "المـــــــــــــــــــهــنة :";
            // 
            // TxtOccupation
            // 
            this.TxtOccupation.BackColor = System.Drawing.Color.White;
            this.TxtOccupation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtOccupation.Location = new System.Drawing.Point(482, 160);
            this.TxtOccupation.Multiline = true;
            this.TxtOccupation.Name = "TxtOccupation";
            this.TxtOccupation.Size = new System.Drawing.Size(194, 26);
            this.TxtOccupation.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(4, 201);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 14);
            this.label13.TabIndex = 553;
            this.label13.Text = "العــــــــــــــــــنــوان :";
            // 
            // TxtAddress
            // 
            this.TxtAddress.BackColor = System.Drawing.Color.White;
            this.TxtAddress.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtAddress.Location = new System.Drawing.Point(127, 195);
            this.TxtAddress.Multiline = true;
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.Size = new System.Drawing.Size(549, 26);
            this.TxtAddress.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(9, 313);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 16);
            this.label15.TabIndex = 557;
            this.label15.Text = "افـــــــراد العـــائلة";
            // 
            // TxtFrst_name
            // 
            this.TxtFrst_name.BackColor = System.Drawing.Color.White;
            this.TxtFrst_name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtFrst_name.Location = new System.Drawing.Point(127, 70);
            this.TxtFrst_name.Multiline = true;
            this.TxtFrst_name.Name = "TxtFrst_name";
            this.TxtFrst_name.Size = new System.Drawing.Size(194, 26);
            this.TxtFrst_name.TabIndex = 4;
            // 
            // CboNat_id
            // 
            this.CboNat_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboNat_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboNat_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboNat_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNat_id.FormattingEnabled = true;
            this.CboNat_id.Location = new System.Drawing.Point(127, 161);
            this.CboNat_id.Name = "CboNat_id";
            this.CboNat_id.Size = new System.Drawing.Size(194, 24);
            this.CboNat_id.TabIndex = 10;
            this.CboNat_id.SelectedIndexChanged += new System.EventHandler(this.CboNat_id_SelectedIndexChanged);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(408, 447);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 27);
            this.AddBtn.TabIndex = 21;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(496, 447);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 27);
            this.ExtBtn.TabIndex = 22;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(11, 345);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(107, 28);
            this.Btn_Add.TabIndex = 15;
            this.Btn_Add.Text = "إضـــافة";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(8, 225);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 14);
            this.label14.TabIndex = 567;
            this.label14.Text = "المــــــلاحـظــــات :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 442);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(996, 1);
            this.flowLayoutPanel1.TabIndex = 536;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(320, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(671, 1);
            this.flowLayoutPanel3.TabIndex = 537;
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(11, 375);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(107, 29);
            this.BtnDel.TabIndex = 16;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // Txt_Note
            // 
            this.Txt_Note.BackColor = System.Drawing.Color.White;
            this.Txt_Note.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Note.Location = new System.Drawing.Point(127, 228);
            this.Txt_Note.Multiline = true;
            this.Txt_Note.Name = "Txt_Note";
            this.Txt_Note.Size = new System.Drawing.Size(549, 62);
            this.Txt_Note.TabIndex = 13;
            // 
            // Grd_Family
            // 
            this.Grd_Family.AllowUserToAddRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Family.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Family.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Family.ColumnHeadersHeight = 40;
            this.Grd_Family.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cbo_Rel_id,
            this.Column1,
            this.Column2});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = null;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Family.DefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Family.Location = new System.Drawing.Point(127, 304);
            this.Grd_Family.Name = "Grd_Family";
            this.Grd_Family.RowHeadersWidth = 15;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Family.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Family.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Family.Size = new System.Drawing.Size(549, 111);
            this.Grd_Family.TabIndex = 14;
            this.Grd_Family.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Grd_Family_DataError);
            // 
            // Cbo_Rel_id
            // 
            this.Cbo_Rel_id.HeaderText = "صلــة الأقارب";
            this.Cbo_Rel_id.Name = "Cbo_Rel_id";
            this.Cbo_Rel_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Cbo_Rel_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Black_Rel_Aname";
            this.Column1.HeaderText = "الاسم العـربي";
            this.Column1.Name = "Column1";
            this.Column1.Width = 215;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Black_Rel_Ename";
            this.Column2.HeaderText = "الاسم الانكليزي";
            this.Column2.Name = "Column2";
            this.Column2.Width = 215;
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Browser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Browser.Location = new System.Drawing.Point(694, 418);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(75, 23);
            this.Btn_Browser.TabIndex = 17;
            this.Btn_Browser.Text = "اضافـة";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.Btn_Browser_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(695, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(292, 375);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 704;
            this.pictureBox1.TabStop = false;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(6, 304);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(116, 1);
            this.flowLayoutPanel4.TabIndex = 705;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(121, 304);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 111);
            this.flowLayoutPanel7.TabIndex = 708;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(6, 304);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 110);
            this.flowLayoutPanel5.TabIndex = 706;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(6, 414);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(116, 1);
            this.flowLayoutPanel6.TabIndex = 707;
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Cancel.Location = new System.Drawing.Point(770, 418);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(71, 23);
            this.Btn_Cancel.TabIndex = 18;
            this.Btn_Cancel.Text = "حــذف";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Btn_Previous
            // 
            this.Btn_Previous.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Previous.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Previous.Location = new System.Drawing.Point(910, 418);
            this.Btn_Previous.Name = "Btn_Previous";
            this.Btn_Previous.Size = new System.Drawing.Size(35, 23);
            this.Btn_Previous.TabIndex = 19;
            this.Btn_Previous.Text = "<<";
            this.Btn_Previous.UseVisualStyleBackColor = true;
            this.Btn_Previous.Click += new System.EventHandler(this.Btn_Previous_Click);
            // 
            // Btn_Next
            // 
            this.Btn_Next.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Next.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Next.Location = new System.Drawing.Point(951, 418);
            this.Btn_Next.Name = "Btn_Next";
            this.Btn_Next.Size = new System.Drawing.Size(36, 23);
            this.Btn_Next.TabIndex = 20;
            this.Btn_Next.Text = ">>";
            this.Btn_Next.UseVisualStyleBackColor = true;
            this.Btn_Next.Click += new System.EventHandler(this.Btn_Next_Click);
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(686, 34);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1, 409);
            this.flowLayoutPanel8.TabIndex = 706;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(355, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 14);
            this.label1.TabIndex = 713;
            this.label1.Text = "اسم الام الاجنبي :";
            // 
            // TxtMother_EName
            // 
            this.TxtMother_EName.BackColor = System.Drawing.Color.White;
            this.TxtMother_EName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtMother_EName.Location = new System.Drawing.Point(482, 100);
            this.TxtMother_EName.Multiline = true;
            this.TxtMother_EName.Name = "TxtMother_EName";
            this.TxtMother_EName.Size = new System.Drawing.Size(194, 26);
            this.TxtMother_EName.TabIndex = 7;
            // 
            // TxtBrth_Date
            // 
            this.TxtBrth_Date.DateSeperator = '/';
            this.TxtBrth_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtBrth_Date.Location = new System.Drawing.Point(127, 131);
            this.TxtBrth_Date.Mask = "0000/00/00";
            this.TxtBrth_Date.Name = "TxtBrth_Date";
            this.TxtBrth_Date.PromptChar = ' ';
            this.TxtBrth_Date.Size = new System.Drawing.Size(194, 22);
            this.TxtBrth_Date.TabIndex = 8;
            this.TxtBrth_Date.Text = "00000000";
            this.TxtBrth_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Net_Connect
            // 
            this.Net_Connect.AutoSize = true;
            this.Net_Connect.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Net_Connect.ForeColor = System.Drawing.Color.Red;
            this.Net_Connect.Location = new System.Drawing.Point(332, 104);
            this.Net_Connect.Name = "Net_Connect";
            this.Net_Connect.Size = new System.Drawing.Size(17, 22);
            this.Net_Connect.TabIndex = 714;
            this.Net_Connect.Text = "*";
            // 
            // Net_Connect1
            // 
            this.Net_Connect1.AutoSize = true;
            this.Net_Connect1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Net_Connect1.ForeColor = System.Drawing.Color.Red;
            this.Net_Connect1.Location = new System.Drawing.Point(332, 40);
            this.Net_Connect1.Name = "Net_Connect1";
            this.Net_Connect1.Size = new System.Drawing.Size(17, 22);
            this.Net_Connect1.TabIndex = 715;
            this.Net_Connect1.Text = "*";
            // 
            // Black_List_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 482);
            this.Controls.Add(this.Net_Connect1);
            this.Controls.Add(this.Net_Connect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtMother_EName);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Btn_Next);
            this.Controls.Add(this.Btn_Previous);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Btn_Browser);
            this.Controls.Add(this.Grd_Family);
            this.Controls.Add(this.Txt_Note);
            this.Controls.Add(this.TxtBrth_Date);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.CboNat_id);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtOccupation);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TxtAddress);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtBrth_place);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtMother_AName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txtlast_Name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtFrst_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtEng_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtArb_Name);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Black_List_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "177";
            this.Text = "Black_List_Add_Upd";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Black_List_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.Black_List_Add_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Family)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtArb_Name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtEng_Name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txtlast_Name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtMother_AName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtBrth_place;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtOccupation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TxtFrst_name;
        private System.Windows.Forms.ComboBox CboNat_id;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button BtnDel;
        private MyDateTextBox TxtBrth_Date;
        private System.Windows.Forms.TextBox Txt_Note;
        private System.Windows.Forms.DataGridView Grd_Family;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewComboBoxColumn Cbo_Rel_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.Button Btn_Previous;
        private System.Windows.Forms.Button Btn_Next;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtMother_EName;
        private System.Windows.Forms.Label Net_Connect;
        private System.Windows.Forms.Label Net_Connect1;
    }
}