﻿namespace Integration_Accounting_Sys
{
    partial class black_list_search_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.black_list_dgv = new System.Windows.Forms.DataGridView();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.search_btn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.search_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.list_name_cbo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.similarity_per_txt = new System.Windows.Forms.TextBox();
            this.name_chk = new System.Windows.Forms.CheckBox();
            this.NAME_ORIGINAL_SCRIPT_chk = new System.Windows.Forms.CheckBox();
            this.Alias_Information_chk = new System.Windows.Forms.CheckBox();
            this.Title_chk = new System.Windows.Forms.CheckBox();
            this.Nationality_chk = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Date_of_Birth_chk = new System.Windows.Forms.CheckBox();
            this.Country_of_Birth_chk = new System.Windows.Forms.CheckBox();
            this.Regime_chk = new System.Windows.Forms.CheckBox();
            this.UN_LIST_TYPE_chk = new System.Windows.Forms.CheckBox();
            this.TYPE_OF_DOCUMENT_chk = new System.Windows.Forms.CheckBox();
            this.Listed_On_chk = new System.Windows.Forms.CheckBox();
            this.GENDER = new System.Windows.Forms.CheckBox();
            this.Group_Type_chk = new System.Windows.Forms.CheckBox();
            this.Group_ID = new System.Windows.Forms.CheckBox();
            this.Post_Zip_Code_chk = new System.Windows.Forms.CheckBox();
            this.Position_chk = new System.Windows.Forms.CheckBox();
            this.NI_Number_chk = new System.Windows.Forms.CheckBox();
            this.Address_chk = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.search_type_cbo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.black_list_dgv)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // black_list_dgv
            // 
            this.black_list_dgv.AllowUserToAddRows = false;
            this.black_list_dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.black_list_dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.black_list_dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.black_list_dgv.ColumnHeadersHeight = 40;
            this.black_list_dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column47,
            this.Column46,
            this.Column45,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column35,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column40,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44});
            this.black_list_dgv.Location = new System.Drawing.Point(6, 171);
            this.black_list_dgv.MultiSelect = false;
            this.black_list_dgv.Name = "black_list_dgv";
            this.black_list_dgv.ReadOnly = true;
            this.black_list_dgv.RowHeadersWidth = 30;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.black_list_dgv.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.black_list_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.black_list_dgv.Size = new System.Drawing.Size(1006, 419);
            this.black_list_dgv.TabIndex = 5;
            // 
            // Column47
            // 
            this.Column47.DataPropertyName = "column_word";
            this.Column47.HeaderText = "حقل البحث";
            this.Column47.Name = "Column47";
            this.Column47.ReadOnly = true;
            this.Column47.Width = 130;
            // 
            // Column46
            // 
            this.Column46.DataPropertyName = "similar_degree";
            this.Column46.HeaderText = "نسبة التشابه";
            this.Column46.Name = "Column46";
            this.Column46.ReadOnly = true;
            this.Column46.Width = 80;
            // 
            // Column45
            // 
            this.Column45.DataPropertyName = "list_name";
            this.Column45.HeaderText = "اسم القائمة";
            this.Column45.Name = "Column45";
            this.Column45.ReadOnly = true;
            this.Column45.Width = 130;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Name";
            this.Column1.HeaderText = "الاسم";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 300;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Title";
            this.Column2.HeaderText = "اللقــب";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Date_of_Birth";
            this.Column3.HeaderText = "تاريخ الولادة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 180;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Country_of_Birth";
            this.Column4.HeaderText = "بلد الولادة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "City_of_Birth";
            this.Column5.HeaderText = "مدينة الولادة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "State_Province";
            this.Column6.HeaderText = "الاقليم";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Place_of_Birth";
            this.Column7.HeaderText = "عنوان الولادة";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 180;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Address";
            this.Column8.HeaderText = "العنوان";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Nationality";
            this.Column9.HeaderText = "الجنسية";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "NI_Number";
            this.Column10.HeaderText = "رقم NI";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Passport_Details";
            this.Column11.HeaderText = "تفاصيل جواز السفر";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Position";
            this.Column12.HeaderText = "المنصب";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Post_Zip_Code";
            this.Column13.HeaderText = "رقم Post/Zip";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Country";
            this.Column14.HeaderText = "البلد";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Other_Information";
            this.Column15.HeaderText = "معلومات اخرى";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 250;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Group_Type";
            this.Column16.HeaderText = "نوع الشبكة";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Alias_Type";
            this.Column17.HeaderText = "نوع اسم الشهرة";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 150;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "Regime";
            this.Column18.HeaderText = "السلطة";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 150;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Listed_On";
            this.Column19.HeaderText = "تاريخ التثبيت";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "Last_Updated";
            this.Column20.HeaderText = "اخر تحديث";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Group_ID";
            this.Column21.HeaderText = "رقم الشبكة";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "Listing_Information";
            this.Column22.HeaderText = "معلومات القائمة";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Width = 200;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "UN_LIST_TYPE";
            this.Column23.HeaderText = "قائمة الامم المتحدة";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "REFERENCE_NUMBER";
            this.Column24.HeaderText = "رقم المرجع";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "GENDER";
            this.Column25.HeaderText = "الجنس";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "SUBMITTED_BY";
            this.Column26.HeaderText = "مسؤول التثبيت";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "SUBMITTED_ON";
            this.Column27.HeaderText = "تاريخ التسجيل";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "NAME_ORIGINAL_SCRIPT";
            this.Column28.HeaderText = "الاسم الاصلي";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.Width = 250;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "COMMENTS1";
            this.Column29.HeaderText = "تعليق";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.Width = 200;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "TYPE_OF_DOCUMENT";
            this.Column30.HeaderText = "نوع الوثيقة";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "DOCUMENT_DETAILS";
            this.Column31.HeaderText = "تفاصيل الوثيقة";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "Alias_Information";
            this.Column32.HeaderText = "اسم الشهرة";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.Width = 250;
            // 
            // Column33
            // 
            this.Column33.DataPropertyName = "Program";
            this.Column33.HeaderText = "البرنامج";
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            // 
            // Column34
            // 
            this.Column34.DataPropertyName = "MOTHER_NAME";
            this.Column34.HeaderText = "اسم الام";
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            // 
            // Column35
            // 
            this.Column35.DataPropertyName = "Reference";
            this.Column35.HeaderText = "المرجع";
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            // 
            // Column36
            // 
            this.Column36.DataPropertyName = "Control_Date";
            this.Column36.HeaderText = "تاريخ التحكم";
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "BLACK_LIST_ID";
            this.Column37.HeaderText = "رقم القيد";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "DATAID";
            this.Column38.HeaderText = "رقم البيانات";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            // 
            // Column39
            // 
            this.Column39.DataPropertyName = "VERSIONNUM";
            this.Column39.HeaderText = "رقم الاصدار";
            this.Column39.Name = "Column39";
            this.Column39.ReadOnly = true;
            // 
            // Column40
            // 
            this.Column40.DataPropertyName = "NATIONALITY2";
            this.Column40.HeaderText = "الجنسية2";
            this.Column40.Name = "Column40";
            this.Column40.ReadOnly = true;
            // 
            // Column41
            // 
            this.Column41.DataPropertyName = "DELISTED_ON";
            this.Column41.HeaderText = "تاريخ الازالة";
            this.Column41.Name = "Column41";
            this.Column41.ReadOnly = true;
            // 
            // Column42
            // 
            this.Column42.DataPropertyName = "ID_#2";
            this.Column42.HeaderText = "ID #2";
            this.Column42.Name = "Column42";
            this.Column42.ReadOnly = true;
            // 
            // Column43
            // 
            this.Column43.DataPropertyName = "Basis3";
            this.Column43.HeaderText = "Basis3";
            this.Column43.Name = "Column43";
            this.Column43.ReadOnly = true;
            // 
            // Column44
            // 
            this.Column44.DataPropertyName = "ID_#3";
            this.Column44.HeaderText = "ID #3";
            this.Column44.Name = "Column44";
            this.Column44.ReadOnly = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 597);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1018, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-6, 164);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1024, 1);
            this.flowLayoutPanel3.TabIndex = 704;
            // 
            // search_btn
            // 
            this.search_btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.search_btn.Location = new System.Drawing.Point(778, 136);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(121, 28);
            this.search_btn.TabIndex = 705;
            this.search_btn.Text = "بـحـــــــــــث";
            this.search_btn.UseVisualStyleBackColor = true;
            this.search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label8.Location = new System.Drawing.Point(11, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 18);
            this.label8.TabIndex = 712;
            this.label8.Text = "بحـــث عن:";
            // 
            // search_txt
            // 
            this.search_txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.search_txt.Location = new System.Drawing.Point(76, 12);
            this.search_txt.Name = "search_txt";
            this.search_txt.Size = new System.Drawing.Size(229, 24);
            this.search_txt.TabIndex = 713;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(318, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 18);
            this.label1.TabIndex = 714;
            this.label1.Text = "القائمــة:";
            // 
            // list_name_cbo
            // 
            this.list_name_cbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.list_name_cbo.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.list_name_cbo.FormattingEnabled = true;
            this.list_name_cbo.Location = new System.Drawing.Point(366, 12);
            this.list_name_cbo.Name = "list_name_cbo";
            this.list_name_cbo.Size = new System.Drawing.Size(247, 24);
            this.list_name_cbo.TabIndex = 715;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(635, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 18);
            this.label3.TabIndex = 718;
            this.label3.Text = "حد العتبه للتشابه:";
            // 
            // similarity_per_txt
            // 
            this.similarity_per_txt.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.similarity_per_txt.Location = new System.Drawing.Point(722, 12);
            this.similarity_per_txt.Name = "similarity_per_txt";
            this.similarity_per_txt.Size = new System.Drawing.Size(70, 24);
            this.similarity_per_txt.TabIndex = 719;
            // 
            // name_chk
            // 
            this.name_chk.AutoSize = true;
            this.name_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.name_chk.Location = new System.Drawing.Point(14, 58);
            this.name_chk.Name = "name_chk";
            this.name_chk.Size = new System.Drawing.Size(51, 22);
            this.name_chk.TabIndex = 720;
            this.name_chk.Text = "الاسم";
            this.name_chk.UseVisualStyleBackColor = true;
            // 
            // NAME_ORIGINAL_SCRIPT_chk
            // 
            this.NAME_ORIGINAL_SCRIPT_chk.AutoSize = true;
            this.NAME_ORIGINAL_SCRIPT_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.NAME_ORIGINAL_SCRIPT_chk.Location = new System.Drawing.Point(93, 58);
            this.NAME_ORIGINAL_SCRIPT_chk.Name = "NAME_ORIGINAL_SCRIPT_chk";
            this.NAME_ORIGINAL_SCRIPT_chk.Size = new System.Drawing.Size(89, 22);
            this.NAME_ORIGINAL_SCRIPT_chk.TabIndex = 721;
            this.NAME_ORIGINAL_SCRIPT_chk.Text = "الاسم الاصلي";
            this.NAME_ORIGINAL_SCRIPT_chk.UseVisualStyleBackColor = true;
            // 
            // Alias_Information_chk
            // 
            this.Alias_Information_chk.AutoSize = true;
            this.Alias_Information_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Alias_Information_chk.Location = new System.Drawing.Point(239, 58);
            this.Alias_Information_chk.Name = "Alias_Information_chk";
            this.Alias_Information_chk.Size = new System.Drawing.Size(77, 22);
            this.Alias_Information_chk.TabIndex = 722;
            this.Alias_Information_chk.Text = "اسم الشهرة";
            this.Alias_Information_chk.UseVisualStyleBackColor = true;
            // 
            // Title_chk
            // 
            this.Title_chk.AutoSize = true;
            this.Title_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Title_chk.Location = new System.Drawing.Point(347, 58);
            this.Title_chk.Name = "Title_chk";
            this.Title_chk.Size = new System.Drawing.Size(50, 22);
            this.Title_chk.TabIndex = 723;
            this.Title_chk.Text = "اللقب";
            this.Title_chk.UseVisualStyleBackColor = true;
            // 
            // Nationality_chk
            // 
            this.Nationality_chk.AutoSize = true;
            this.Nationality_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Nationality_chk.Location = new System.Drawing.Point(442, 58);
            this.Nationality_chk.Name = "Nationality_chk";
            this.Nationality_chk.Size = new System.Drawing.Size(60, 22);
            this.Nationality_chk.TabIndex = 724;
            this.Nationality_chk.Text = "الجنسية";
            this.Nationality_chk.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(798, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 18);
            this.label4.TabIndex = 725;
            this.label4.Text = "%";
            // 
            // Date_of_Birth_chk
            // 
            this.Date_of_Birth_chk.AutoSize = true;
            this.Date_of_Birth_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Date_of_Birth_chk.Location = new System.Drawing.Point(539, 58);
            this.Date_of_Birth_chk.Name = "Date_of_Birth_chk";
            this.Date_of_Birth_chk.Size = new System.Drawing.Size(84, 22);
            this.Date_of_Birth_chk.TabIndex = 726;
            this.Date_of_Birth_chk.Text = "تاريخ الولادة";
            this.Date_of_Birth_chk.UseVisualStyleBackColor = true;
            // 
            // Country_of_Birth_chk
            // 
            this.Country_of_Birth_chk.AutoSize = true;
            this.Country_of_Birth_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Country_of_Birth_chk.Location = new System.Drawing.Point(655, 58);
            this.Country_of_Birth_chk.Name = "Country_of_Birth_chk";
            this.Country_of_Birth_chk.Size = new System.Drawing.Size(71, 22);
            this.Country_of_Birth_chk.TabIndex = 727;
            this.Country_of_Birth_chk.Text = "بلد الولادة";
            this.Country_of_Birth_chk.UseVisualStyleBackColor = true;
            // 
            // Regime_chk
            // 
            this.Regime_chk.AutoSize = true;
            this.Regime_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Regime_chk.Location = new System.Drawing.Point(14, 93);
            this.Regime_chk.Name = "Regime_chk";
            this.Regime_chk.Size = new System.Drawing.Size(57, 22);
            this.Regime_chk.TabIndex = 739;
            this.Regime_chk.Text = "السلطة";
            this.Regime_chk.UseVisualStyleBackColor = true;
            // 
            // UN_LIST_TYPE_chk
            // 
            this.UN_LIST_TYPE_chk.AutoSize = true;
            this.UN_LIST_TYPE_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.UN_LIST_TYPE_chk.Location = new System.Drawing.Point(93, 93);
            this.UN_LIST_TYPE_chk.Name = "UN_LIST_TYPE_chk";
            this.UN_LIST_TYPE_chk.Size = new System.Drawing.Size(71, 22);
            this.UN_LIST_TYPE_chk.TabIndex = 738;
            this.UN_LIST_TYPE_chk.Text = "قائمة UN";
            this.UN_LIST_TYPE_chk.UseVisualStyleBackColor = true;
            // 
            // TYPE_OF_DOCUMENT_chk
            // 
            this.TYPE_OF_DOCUMENT_chk.AutoSize = true;
            this.TYPE_OF_DOCUMENT_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.TYPE_OF_DOCUMENT_chk.Location = new System.Drawing.Point(239, 93);
            this.TYPE_OF_DOCUMENT_chk.Name = "TYPE_OF_DOCUMENT_chk";
            this.TYPE_OF_DOCUMENT_chk.Size = new System.Drawing.Size(75, 22);
            this.TYPE_OF_DOCUMENT_chk.TabIndex = 737;
            this.TYPE_OF_DOCUMENT_chk.Text = "نوع الوثيقة";
            this.TYPE_OF_DOCUMENT_chk.UseVisualStyleBackColor = true;
            // 
            // Listed_On_chk
            // 
            this.Listed_On_chk.AutoSize = true;
            this.Listed_On_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Listed_On_chk.Location = new System.Drawing.Point(347, 93);
            this.Listed_On_chk.Name = "Listed_On_chk";
            this.Listed_On_chk.Size = new System.Drawing.Size(83, 22);
            this.Listed_On_chk.TabIndex = 736;
            this.Listed_On_chk.Text = "تاريخ التثبيت";
            this.Listed_On_chk.UseVisualStyleBackColor = true;
            // 
            // GENDER
            // 
            this.GENDER.AutoSize = true;
            this.GENDER.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.GENDER.Location = new System.Drawing.Point(442, 93);
            this.GENDER.Name = "GENDER";
            this.GENDER.Size = new System.Drawing.Size(56, 22);
            this.GENDER.TabIndex = 735;
            this.GENDER.Text = "الجنس";
            this.GENDER.UseVisualStyleBackColor = true;
            // 
            // Group_Type_chk
            // 
            this.Group_Type_chk.AutoSize = true;
            this.Group_Type_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Group_Type_chk.Location = new System.Drawing.Point(655, 93);
            this.Group_Type_chk.Name = "Group_Type_chk";
            this.Group_Type_chk.Size = new System.Drawing.Size(76, 22);
            this.Group_Type_chk.TabIndex = 734;
            this.Group_Type_chk.Text = "نوع الشبكة";
            this.Group_Type_chk.UseVisualStyleBackColor = true;
            // 
            // Group_ID
            // 
            this.Group_ID.AutoSize = true;
            this.Group_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Group_ID.Location = new System.Drawing.Point(539, 93);
            this.Group_ID.Name = "Group_ID";
            this.Group_ID.Size = new System.Drawing.Size(75, 22);
            this.Group_ID.TabIndex = 732;
            this.Group_ID.Text = "رقم الشبكة";
            this.Group_ID.UseVisualStyleBackColor = true;
            // 
            // Post_Zip_Code_chk
            // 
            this.Post_Zip_Code_chk.AutoSize = true;
            this.Post_Zip_Code_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Post_Zip_Code_chk.Location = new System.Drawing.Point(778, 93);
            this.Post_Zip_Code_chk.Name = "Post_Zip_Code_chk";
            this.Post_Zip_Code_chk.Size = new System.Drawing.Size(106, 22);
            this.Post_Zip_Code_chk.TabIndex = 731;
            this.Post_Zip_Code_chk.Text = "رقم Post/Zip";
            this.Post_Zip_Code_chk.UseVisualStyleBackColor = true;
            // 
            // Position_chk
            // 
            this.Position_chk.AutoSize = true;
            this.Position_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Position_chk.Location = new System.Drawing.Point(909, 93);
            this.Position_chk.Name = "Position_chk";
            this.Position_chk.Size = new System.Drawing.Size(63, 22);
            this.Position_chk.TabIndex = 730;
            this.Position_chk.Text = "المنصب";
            this.Position_chk.UseVisualStyleBackColor = true;
            // 
            // NI_Number_chk
            // 
            this.NI_Number_chk.AutoSize = true;
            this.NI_Number_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.NI_Number_chk.Location = new System.Drawing.Point(909, 58);
            this.NI_Number_chk.Name = "NI_Number_chk";
            this.NI_Number_chk.Size = new System.Drawing.Size(62, 22);
            this.NI_Number_chk.TabIndex = 729;
            this.NI_Number_chk.Text = "رقم NI";
            this.NI_Number_chk.UseVisualStyleBackColor = true;
            // 
            // Address_chk
            // 
            this.Address_chk.AutoSize = true;
            this.Address_chk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Address_chk.Location = new System.Drawing.Point(778, 58);
            this.Address_chk.Name = "Address_chk";
            this.Address_chk.Size = new System.Drawing.Size(57, 22);
            this.Address_chk.TabIndex = 728;
            this.Address_chk.Text = "العنوان";
            this.Address_chk.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(836, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 18);
            this.label2.TabIndex = 716;
            this.label2.Text = "طبيعة البحث:";
            this.label2.Visible = false;
            // 
            // search_type_cbo
            // 
            this.search_type_cbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.search_type_cbo.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.search_type_cbo.FormattingEnabled = true;
            this.search_type_cbo.Location = new System.Drawing.Point(907, 12);
            this.search_type_cbo.Name = "search_type_cbo";
            this.search_type_cbo.Size = new System.Drawing.Size(99, 24);
            this.search_type_cbo.TabIndex = 717;
            this.search_type_cbo.Visible = false;
            this.search_type_cbo.SelectedIndexChanged += new System.EventHandler(this.search_type_cbo_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.Location = new System.Drawing.Point(899, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 28);
            this.button1.TabIndex = 740;
            this.button1.Text = "تصديــــر";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // black_list_search_frm
            // 
            this.AccessibleDescription = "";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 619);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Regime_chk);
            this.Controls.Add(this.UN_LIST_TYPE_chk);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TYPE_OF_DOCUMENT_chk);
            this.Controls.Add(this.similarity_per_txt);
            this.Controls.Add(this.Listed_On_chk);
            this.Controls.Add(this.GENDER);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Group_Type_chk);
            this.Controls.Add(this.search_type_cbo);
            this.Controls.Add(this.Group_ID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Post_Zip_Code_chk);
            this.Controls.Add(this.list_name_cbo);
            this.Controls.Add(this.Position_chk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NI_Number_chk);
            this.Controls.Add(this.search_txt);
            this.Controls.Add(this.Address_chk);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.name_chk);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.Country_of_Birth_chk);
            this.Controls.Add(this.search_btn);
            this.Controls.Add(this.NAME_ORIGINAL_SCRIPT_chk);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.Date_of_Birth_chk);
            this.Controls.Add(this.black_list_dgv);
            this.Controls.Add(this.Alias_Information_chk);
            this.Controls.Add(this.Nationality_chk);
            this.Controls.Add(this.Title_chk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "black_list_search_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "527";
            this.Text = "black_list_search_frm";
            this.Load += new System.EventHandler(this.black_list_search_frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.black_list_dgv)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView black_list_dgv;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button search_btn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox search_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox list_name_cbo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox similarity_per_txt;
        private System.Windows.Forms.CheckBox name_chk;
        private System.Windows.Forms.CheckBox NAME_ORIGINAL_SCRIPT_chk;
        private System.Windows.Forms.CheckBox Alias_Information_chk;
        private System.Windows.Forms.CheckBox Title_chk;
        private System.Windows.Forms.CheckBox Nationality_chk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox Date_of_Birth_chk;
        private System.Windows.Forms.CheckBox Country_of_Birth_chk;
        private System.Windows.Forms.CheckBox UN_LIST_TYPE_chk;
        private System.Windows.Forms.CheckBox TYPE_OF_DOCUMENT_chk;
        private System.Windows.Forms.CheckBox Listed_On_chk;
        private System.Windows.Forms.CheckBox GENDER;
        private System.Windows.Forms.CheckBox Group_Type_chk;
        private System.Windows.Forms.CheckBox Group_ID;
        private System.Windows.Forms.CheckBox Post_Zip_Code_chk;
        private System.Windows.Forms.CheckBox Position_chk;
        private System.Windows.Forms.CheckBox NI_Number_chk;
        private System.Windows.Forms.CheckBox Address_chk;
        private System.Windows.Forms.CheckBox Regime_chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox search_type_cbo;
        private System.Windows.Forms.Button button1;
    }
}