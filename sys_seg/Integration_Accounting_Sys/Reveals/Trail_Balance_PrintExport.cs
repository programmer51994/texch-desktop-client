﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class Trail_Balance_PrintExport : Form
    {
        DataTable Trail_Balance = new DataTable();
        DataTable Trail_Balance1 = new DataTable();
        DataTable Trail_Balance2 = new DataTable();
        public DataGridView Dgv1 { get; set; }
        public DataGridView Dgv2 { get; set; }
        public DataGridView Dgv3 { get; set; }
        public DataGridView Dgv4 { get; set; }
        DataGridView Grd_Loc_Sum = new DataGridView();
        DataTable Tbl_Loc_Sum = new DataTable();
        public Trail_Balance_PrintExport()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id == 2)
            {
                Cbo_Print.Items[0] = "Print total";
                Cbo_Print.Items[1] = "Detailed printing";
            }
            else
            {
                Cbo_Print.Items[0] = "طباعة اجمالـــي";
                Cbo_Print.Items[1] = "طباعة تفصيلــي";
               
            }
            if (connection.Lang_id == 2)
            {
                Cbo_Export.Items[0] = "Trial Balance";
                Cbo_Export.Items[1] = "The account specified analysis";
                Cbo_Export.Items[2] = "Selected account movement details";
            }
            else
            {
                Cbo_Export.Items[0] = "ميــــــزان المراجعـــة";
                Cbo_Export.Items[1] = "تحليل الحساب المحدد";
                Cbo_Export.Items[2] = "تفاصيل حركة الحساب المحدد";

            }
         

        }
        private void Trail_Balance_PrintExport_Load(object sender, EventArgs e)
        {

            Cbo_Print.SelectedIndex = 0;
            Cbo_Export.SelectedIndex = 0;
            Chk_Acc.Checked = true;
            Chk_Print_CheckedChanged(null, null);
        }
        private void Chk_Print_CheckedChanged(object sender, EventArgs e)
        {
            Cbo_Print.Enabled = true;
            Cbo_Export.Enabled = false;
        }

        private void Chk_Export_CheckedChanged(object sender, EventArgs e)
        {
            Cbo_Export.Enabled = true;
            Cbo_Print.Enabled = false;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            // ------------------طباعة
            if (Chk_Acc.Checked == true)
            {
                if (Cbo_Print.SelectedIndex == 0)
                {
                    Trail_Balance = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable().Select("T_id = " + Trial_Balance_Main_Details.T_Id).CopyToDataTable();
                    Trail_Balance1 = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable().Select("T_id = " + Trial_Balance_Main_Details.T_Id).CopyToDataTable();
                    DataTable Dt_Param = CustomControls.CustomParam_Dt();
                    Dt_Param.Rows.Add("User_rep", connection.User_Name);

                    connection.SQLDS.Tables.Add(Trail_Balance);
                    connection.SQLDS.Tables.Add(Trail_Balance1);
                    Trail_Balance.TableName = "Trail_Balance";
                    Trail_Balance1.TableName = "Trail_Balance1";
                    Trail_Balance_Details_Rep ObjRpt = new Trail_Balance_Details_Rep();
                    Trail_Balance_Details_Rep_Eng ObjRptEng = new Trail_Balance_Details_Rep_Eng();
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Trail_Balance");
                    connection.SQLDS.Tables.Remove("Trail_Balance1");
                }
                else
                {
                    Trail_Balance = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable().Select("T_id = " + Trial_Balance_Main_Details.T_Id).CopyToDataTable();
                    Trail_Balance1 = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable().Select("T_id = " + Trial_Balance_Main_Details.T_Id + "And Acc_id = " + Trial_Balance_Main_Details.Acc_Id  ).CopyToDataTable();

                    try
                    {
                        Trail_Balance2 = connection.SQLDS.Tables["Trail_Balance_Tbl2"].DefaultView.ToTable().Select("T_id = " + Trial_Balance_Main_Details.T_Id + "And Acc_id = " + Trial_Balance_Main_Details.Acc_Id ).CopyToDataTable();
                        connection.SQLDS.Tables.Add(Trail_Balance);
                        connection.SQLDS.Tables.Add(Trail_Balance1);
                        connection.SQLDS.Tables.Add(Trail_Balance2);
                        Trail_Balance.TableName = "Trail_Balance";
                        Trail_Balance1.TableName = "Trail_Balance1";
                        Trail_Balance2.TableName = "Trail_Balance2";
                        DataTable Dt_Param = CustomControls.CustomParam_Dt();
                        Dt_Param.Rows.Add("User_rep", connection.User_Name);

                        Trail_Balance_Details2_Rep ObjRpt = new Trail_Balance_Details2_Rep();
                        Trail_Balance_Details2_Rep ObjRptEng = new Trail_Balance_Details2_Rep();
                        Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);
                        this.Visible = false;
                        RptLangFrm.ShowDialog(this);
                        this.Visible = true;
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للرصيد " : " No records to this account ", MyGeneral_Lib.LblCap);

                        return;
                    }
                    connection.SQLDS.Tables.Remove("Trail_Balance");
                    connection.SQLDS.Tables.Remove("Trail_Balance1");
                    connection.SQLDS.Tables.Remove("Trail_Balance2");
                }
            }
            // ------------------تصدير
            if (CHK_FTB.Checked == true)
            {
                if (Cbo_Export.SelectedIndex == 0)//ميزان المراجعة
                {
                   // Getloc_AmountSum();
                    DataTable ExpDt_Term = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "T_ID", "ACust_Name", "ECust_Name"
                                                                                                     , "MinDrec_date", "MaxDrec_date").Select("T_Id=" + Trial_Balance_Main_Details.T_Id).CopyToDataTable();
                    DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                      , "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount", "T_Id").Select("T_Id =" + Trial_Balance_Main_Details.T_Id).CopyToDataTable();

                    ExpDt_Term_Acc = ExpDt_Term_Acc.DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                      , "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount").Select().CopyToDataTable(); 

                    ExpDt_Term_Acc.Rows.Add(new Object[] {  DBNull.Value,connection.Lang_id==1? "المجامــــيع":"Totals", Trial_Balance_Main_Details.OD_LocAmount, Trial_Balance_Main_Details.OC_LocAmount, Trial_Balance_Main_Details.PD_LocAmount,
                    Trial_Balance_Main_Details.PC_LocAmount ,Trial_Balance_Main_Details.ED_LocAmount ,Trial_Balance_Main_Details.EC_LocAmount});
                    DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc  };
                    DataGridView[] Export_GRD = { Dgv1, Dgv2 };
                    MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? "كشــــف ميـــزان مراجعـــة" : "Trial Balance Reveal");
                }
                if (Cbo_Export.SelectedIndex == 1)//تحليل الحساب المحدد
                {
                    if (Trial_Balance_Main_Details.Details_Acc_Bs2.Count > 0)
                    {
                        DataTable ExpDt_Term = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "T_ID", "ACust_Name", "ECust_Name"
                                                                                                            , "MinDrec_date", "MaxDrec_date").Select("T_Id=" + Trial_Balance_Main_Details.T_Id).CopyToDataTable();
                        DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable(false, "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                          , "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount", "T_Id").Select("T_Id=" + Trial_Balance_Main_Details.T_Id + " and Acc_no like'" + Trial_Balance_Main_Details.Acc_no +"'").CopyToDataTable();
                        ExpDt_Term_Acc = ExpDt_Term_Acc.DefaultView.ToTable(false, "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                          , "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount").Select().CopyToDataTable();
                        DataTable ExpDt_Sum_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl2"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name", "Dloc_Amount"
                                                                                                          , "Cloc_Amount", "DFor_Amount", "CFor_Amount", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "T_Id", "Acc_id").Select("T_Id=" + Trial_Balance_Main_Details.T_Id + " and Acc_id  =" + Trial_Balance_Main_Details.Acc_Id).CopyToDataTable();

                        ExpDt_Sum_Acc = ExpDt_Sum_Acc.DefaultView.ToTable(false, connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name", "Dloc_Amount"
                            , "Cloc_Amount", "DFor_Amount", "CFor_Amount", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename").Select().CopyToDataTable();

                        DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc, ExpDt_Sum_Acc };
                        DataGridView[] Export_GRD = { Dgv1, Dgv2, Dgv3 };
                        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? " كشــــف ميـــزان مراجعـــة -(تحليـــل الحســاب)-" : "Trial Balance Reveal (Account analysis)");
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للرصيد " : " No records to this account ", MyGeneral_Lib.LblCap);

                        return;
                    }

                }
                if (Cbo_Export.SelectedIndex == 2)//تفاصيل تحليل الحساب المحلل
                {
                    if (Trial_Balance_Main_Details.Details_Acc_Bs2.Count > 0)
                    {
                        DataTable ExpDt_Term = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "T_ID", "ACust_Name", "ECust_Name"
                                                                                                          , "MinDrec_date", "MaxDrec_date").Select("T_Id=" + Trial_Balance_Main_Details.T_Id).CopyToDataTable();
                        DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                          , "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount", "T_Id").Select("T_Id=" + Trial_Balance_Main_Details.T_Id + " and Acc_no like'" + Trial_Balance_Main_Details.Acc_no + "'").CopyToDataTable();

                        ExpDt_Term_Acc = ExpDt_Term_Acc.DefaultView.ToTable(false, "Acc_no", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                    , "COloc_Amount", "DPloc_Amount", "CPloc_Amount", "DEloc_Amount", "CEloc_Amount").Select().CopyToDataTable();

                        DataTable ExpDt_Sum_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl2"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name", "Dloc_Amount"
                                                                                                                    , "Cloc_Amount", "DFor_Amount", "CFor_Amount", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "T_Id", "Acc_id", "Cust_id", "For_Cur_id").
                                                                                                                    Select("T_Id=" + Trial_Balance_Main_Details.T_Id + " and  Acc_id =" + Trial_Balance_Main_Details.Acc_Id   + "And Cust_id = " + Trial_Balance_Main_Details.Cust_id + "and For_cur_id = " + Trial_Balance_Main_Details.For_Cur_Id).CopyToDataTable();


                        ExpDt_Sum_Acc = ExpDt_Sum_Acc.DefaultView.ToTable(false, connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name", "Dloc_Amount"
                            , "Cloc_Amount", "DFor_Amount", "CFor_Amount", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename").Select().CopyToDataTable();
                       
                            DataTable ExpDt_Term_voucher = connection.SQLDS.Tables["Trail_Balance_Tbl3"].DefaultView.ToTable(true, "DLoc_Amount", "CLoc_Amount", connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name"
                                , connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "VO_NO", "DFor_Amount", "CFor_Amount", "Real_Price", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "Notes", "T_Id", "Acc_id", "Cust_id", "Create_Nrec_date", "C_DATE").Select("T_Id=" + Trial_Balance_Main_Details.T_Id + " and Acc_id=" + Trial_Balance_Main_Details.Acc_Id + "And Cust_id= " + Trial_Balance_Main_Details.Cust_id).CopyToDataTable();
                      
                       

                        ExpDt_Term_voucher = ExpDt_Term_voucher.DefaultView.ToTable(true, "DLoc_Amount", "CLoc_Amount", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name"
                            , connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "VO_NO", "DFor_Amount", "CFor_Amount", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "Real_Price", "Create_Nrec_date", "C_DATE", "Notes").Select().CopyToDataTable();
                        DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc, ExpDt_Sum_Acc, ExpDt_Term_voucher };
                        DataGridView[] Export_GRD = { Dgv1, Dgv2, Dgv3, Dgv4 };
                        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id==1? " كشــــف ميـــزان مراجعـــة -(تحليـــل تفاصيـــل الحســاب)-": "Trial Balance Reveal (Account analysis)");
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للرصيد " : " No records to this account ", MyGeneral_Lib.LblCap);

                        return;
                    }
                }
            }
        }
        
        //------------------------------------------ 
        //private void Getloc_AmountSum()
        //{
        //    Grd_Loc_Sum.Columns.Add("Acc_NO", "");
        //    Grd_Loc_Sum.Columns.Add("Acc_Aname", "");
        //    Grd_Loc_Sum.Columns.Add("TxtOD_LocAmount", "ارصدة اول المدة _(ع .م)_");
        //    Grd_Loc_Sum.Columns.Add("TxtOC_LocAmount", "ارصدة اول المدة _(ع . م )_");
        //    Grd_Loc_Sum.Columns.Add("TxtPD_LocAmount", "ارصدة الفتره _(ع .م)_");
        //    Grd_Loc_Sum.Columns.Add("TxtPC_LocAmount", "ارصدة الفترة _(ع .م)_ ");
        //    Grd_Loc_Sum.Columns.Add("TxtED_LocAmount", " ارصدة نهاية الفترة _(ع. م )_");
        //    Grd_Loc_Sum.Columns.Add("TxtEC_LocAmount", "ارصدة نهاية المدة _( ع. م )_");

        //    string[] Column = { "Acc_NO", "Acc_Aname", "TxtOD_LocAmount", "TxtOC_LocAmount", "TxtPD_LocAmount", "TxtPC_LocAmount", "TxtED_LocAmount", "TxtEC_LocAmount" };
        //    string[] DType = { "System.String", "System.String", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal" };
        //    Tbl_Loc_Sum = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
        //    Tbl_Loc_Sum.Rows.Add(new object[] {"" ,"" , Trial_Balance_Main_Details.OD_LocAmount, Trial_Balance_Main_Details.OC_LocAmount, Trial_Balance_Main_Details.PD_LocAmount,
        //    Trial_Balance_Main_Details.PC_LocAmount ,Trial_Balance_Main_Details.ED_LocAmount ,Trial_Balance_Main_Details.EC_LocAmount});
        //}
        //-------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cbo_Print_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}