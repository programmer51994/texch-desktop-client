﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Trial_Balance : Form
    {
        #region Defintion
        DataTable DT_Cust = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        int T_Id = 0;
        string Filter = "";
        #endregion
      
        public Search_Trial_Balance()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            Create_Tbl();

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust_Id.Columns["Column8"].DataPropertyName = "Ecust_name";
                Grd_Cust.Columns["Column11"].DataPropertyName = "Ecust_name";
                CboDisplay.Items.Clear();
                CboDisplay.Items.Add("Detailed Account");
                CboDisplay.Items.Add("Accumulation Account");   
            }
            #endregion
        }

        private void Create_Tbl()
        {
            string[] Column1 = { "T_Id", "ACUST_NAME", "ECUST_NAME" };
            string[] DType1 = { "System.Int16", "System.String", "System.String" };
            DT_Cust = CustomControls.Custom_DataTable("DT_Cust", Column1, DType1);
            Grd_Cust.DataSource = DT_Cust;
           
           
        }

        private void Search_Trial_Balance_Load(object sender, EventArgs e)
        {
            CHK_FTB.Enabled = false;
            connection.SqlExec("Exec Search_Trial_Balance ","SearchTrial_Balance_Tbl");
            Cbo_Level.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl1"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "AAcc_Level" : "EAcc_Level";
            Cbo_Level.SelectedIndex = 0;
            CboDisplay.SelectedIndex = 0;
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                T_Id = 0;
                string Cond_Str_Cust_Id = "";
                string Con_Str = "";
                if (DT_Cust.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cust, "T_Id", out Cond_Str_Cust_Id);
                    Con_Str = " And T_Id not in(" + Cond_Str_Cust_Id + ")";
                }
                int.TryParse(TxtCust_Name.Text, out T_Id);


                Filter = " (Acust_name like '" + TxtCust_Name.Text + "%' or  Ecust_name like '" + TxtCust_Name.Text + "%'"
                            + " Or T_Id = " + T_Id + ")" + Con_Str;
                DT_Cust_TBL = connection.SQLDS.Tables["SearchTrial_Balance_Tbl"].DefaultView.ToTable(true, "T_Id", "ACust_name", "Ecust_name").Select(Filter).CopyToDataTable();
                Grd_Cust_Id.DataSource = DT_Cust_TBL;

            }
            catch
            {
                Grd_Cust_Id.DataSource = new DataTable();
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {

                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }

        private void Search_Trial_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "SearchTrial_Balance_Tbl", "SearchTrial_Balance_Tbl1", "Trail_Balance_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Chk_Cust_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cust.Checked)
            {
                TxtCust_Name.Enabled = true;
                TxtCust_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cust.Clear();
                DT_Cust_TBL.Clear();
                TxtCust_Name.ResetText();
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                TxtCust_Name.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cust.NewRow();

            row["T_id"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["T_Id"];
            row["ACUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ACust_name"];
            row["ECUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ECust_name"];
            DT_Cust.Rows.Add(row);
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);

            button3.Enabled = true;
            button4.Enabled = true;
            if (Grd_Cust_Id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cust_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cust.NewRow();
                row["T_id"] = DT_Cust_TBL.Rows[i]["T_Id"];
                row["ACUST_NAME"] = DT_Cust_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Cust_TBL.Rows[i]["ECUST_NAME"];
                DT_Cust.Rows.Add(row);
            }
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows[Grd_Cust.CurrentRow.Index].Delete();
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            if (Grd_Cust.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows.Clear();
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
            string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
            string InRecDate = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);
            if ((From_Date == "-1" || To_Date == "-1"))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تأكد من الفترة " : "Enter The Date Please ", MyGeneral_Lib.LblCap);

                return;
            }
            if (Convert.ToInt32(From_Date) > Convert.ToInt32(To_Date) && Convert.ToInt32(To_Date) != 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " تاريخ بداية الفترة اكبر من تاريخ النهاية " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            string T_IDStr = "";

            MyGeneral_Lib.ColumnToString(DT_Cust, "T_Id", out  T_IDStr);

            object[] Sparam = {T_IDStr,From_Date,
                                  To_Date,CHK_FTB.Checked,Cbo_Level.SelectedValue,
                                  Convert.ToByte(CHK_OP_ACC.Checked),CboDisplay.SelectedIndex,connection.user_id,connection.Lang_id,""};
            //this.Enabled = false;
            //MyGeneral_Lib.Copytocliptext("Trail_Balance_Main", Sparam);
            connection.SqlExec("Trail_Balance_Main_new", "Trail_Balance_Tbl", Sparam);

            if (connection.Col_Name != "")
            {
                MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            if (CboDisplay.SelectedIndex == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "قيد الانجاز" : "In progress", MyGeneral_Lib.LblCap);
                return;
                //Trial_Balance_Main Frm = new Trial_Balance_Main();
                //this.Visible = false;
                //Frm.ShowDialog(this);
                //this.Visible = true;
                //this.Enabled = true;
            }
            else
            {
               
                Trial_Balance_Main_Details Frm = new Trial_Balance_Main_Details(From_Date, To_Date);
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }
        }

        private void CboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboDisplay.SelectedIndex == 0)
            {
                Cbo_Level.Enabled = false;
            }
            else
            {
                Cbo_Level.Enabled = true;
            }
        }

        private void Search_Trial_Balance_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}
