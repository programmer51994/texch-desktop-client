﻿namespace Integration_Accounting_Sys
{
    partial class Report_Group_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Grp = new System.Windows.Forms.TextBox();
            this.Grd_Grp_Id = new System.Windows.Forms.DataGridView();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk_Acc = new System.Windows.Forms.CheckBox();
            this.Chk_Cust = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Oper_Check = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Oper = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.Chk_Acc_All = new System.Windows.Forms.CheckBox();
            this.Chk_Cust_All = new System.Windows.Forms.CheckBox();
            this.Chk_Oper_All = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Grp_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Oper)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(362, 13);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 713;
            this.label5.Text = "التاريــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(425, 9);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(89, 9);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(173, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(6, 10);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(82, 14);
            this.label4.TabIndex = 715;
            this.label4.Text = "المستخــــدم:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-2, 38);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(574, 1);
            this.flowLayoutPanel1.TabIndex = 717;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(8, 49);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(76, 14);
            this.label2.TabIndex = 718;
            this.label2.Text = "المجموعـــة:\r\n";
            // 
            // Txt_Grp
            // 
            this.Txt_Grp.BackColor = System.Drawing.Color.White;
            this.Txt_Grp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Grp.Location = new System.Drawing.Point(89, 45);
            this.Txt_Grp.Name = "Txt_Grp";
            this.Txt_Grp.Size = new System.Drawing.Size(173, 22);
            this.Txt_Grp.TabIndex = 2;
            this.Txt_Grp.TextChanged += new System.EventHandler(this.Txt_Grp_TextChanged);
            // 
            // Grd_Grp_Id
            // 
            this.Grd_Grp_Id.AllowUserToAddRows = false;
            this.Grd_Grp_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Grp_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Grp_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Grp_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Grp_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Grp_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Grp_Id.ColumnHeadersHeight = 45;
            this.Grd_Grp_Id.ColumnHeadersVisible = false;
            this.Grd_Grp_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column13,
            this.Column14});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Grp_Id.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Grp_Id.Location = new System.Drawing.Point(89, 67);
            this.Grd_Grp_Id.Name = "Grd_Grp_Id";
            this.Grd_Grp_Id.ReadOnly = true;
            this.Grd_Grp_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Grp_Id.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Grp_Id.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Grp_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Grp_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Grp_Id.Size = new System.Drawing.Size(431, 66);
            this.Grd_Grp_Id.TabIndex = 3;
            this.Grd_Grp_Id.SelectionChanged += new System.EventHandler(this.Grd_Grp_Id_SelectionChanged);
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Groups_id";
            this.Column13.Frozen = true;
            this.Column13.HeaderText = "رمز الثانوي";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 60;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Groups_AName";
            this.Column14.HeaderText = "اسم الثانوي";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 390;
            // 
            // Grd_Acc
            // 
            this.Grd_Acc.AllowUserToAddRows = false;
            this.Grd_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Acc.ColumnHeadersHeight = 24;
            this.Grd_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.Grd_Acc.Enabled = false;
            this.Grd_Acc.Location = new System.Drawing.Point(32, 153);
            this.Grd_Acc.Name = "Grd_Acc";
            this.Grd_Acc.RowHeadersWidth = 20;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc.Size = new System.Drawing.Size(516, 113);
            this.Grd_Acc.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Chk";
            this.Column1.HeaderText = "تاشير الكل";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.Width = 78;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "acc_id";
            this.Column2.HeaderText = "رقـم الحساب";
            this.Column2.Name = "Column2";
            this.Column2.Width = 65;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Acc_AName";
            this.Column3.HeaderText = "اسم الحساب";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "User_Name";
            this.Column4.HeaderText = "المنظم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Chk_Acc
            // 
            this.Chk_Acc.AutoSize = true;
            this.Chk_Acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Acc.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Acc.Location = new System.Drawing.Point(31, 133);
            this.Chk_Acc.Name = "Chk_Acc";
            this.Chk_Acc.Size = new System.Drawing.Size(171, 18);
            this.Chk_Acc.TabIndex = 4;
            this.Chk_Acc.Text = "على مستوى الحساب.....";
            this.Chk_Acc.UseVisualStyleBackColor = true;
            this.Chk_Acc.CheckedChanged += new System.EventHandler(this.Chk_Acc_CheckedChanged);
            // 
            // Chk_Cust
            // 
            this.Chk_Cust.AutoSize = true;
            this.Chk_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Cust.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Cust.Location = new System.Drawing.Point(38, 269);
            this.Chk_Cust.Name = "Chk_Cust";
            this.Chk_Cust.Size = new System.Drawing.Size(165, 18);
            this.Chk_Cust.TabIndex = 6;
            this.Chk_Cust.Text = "على مستوى الثانوي.....";
            this.Chk_Cust.UseVisualStyleBackColor = true;
            this.Chk_Cust.CheckedChanged += new System.EventHandler(this.Chk_Cust_CheckedChanged);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(26, 280);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(526, 1);
            this.flowLayoutPanel3.TabIndex = 725;
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            this.Grd_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust.ColumnHeadersHeight = 24;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.Grd_Cust.Enabled = false;
            this.Grd_Cust.Location = new System.Drawing.Point(32, 296);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust.Size = new System.Drawing.Size(516, 113);
            this.Grd_Cust.TabIndex = 7;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Chk";
            this.Column5.HeaderText = "تاشير الكل";
            this.Column5.Name = "Column5";
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column5.Width = 78;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Cust_id";
            this.Column6.HeaderText = "رقـم الثانوي";
            this.Column6.Name = "Column6";
            this.Column6.Width = 65;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Acust_Name";
            this.Column7.HeaderText = "اسم الثانوي";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 200;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "User_Name";
            this.Column8.HeaderText = "المنظم";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 150;
            // 
            // Oper_Check
            // 
            this.Oper_Check.AutoSize = true;
            this.Oper_Check.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Oper_Check.ForeColor = System.Drawing.Color.Maroon;
            this.Oper_Check.Location = new System.Drawing.Point(37, 412);
            this.Oper_Check.Name = "Oper_Check";
            this.Oper_Check.Size = new System.Drawing.Size(166, 18);
            this.Oper_Check.TabIndex = 8;
            this.Oper_Check.Text = "على مستوى العملية.....";
            this.Oper_Check.UseVisualStyleBackColor = true;
            this.Oper_Check.CheckedChanged += new System.EventHandler(this.Chk_Oper_CheckedChanged);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(26, 423);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(526, 1);
            this.flowLayoutPanel4.TabIndex = 728;
            // 
            // Grd_Oper
            // 
            this.Grd_Oper.AllowUserToAddRows = false;
            this.Grd_Oper.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Oper.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Oper.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Oper.ColumnHeadersHeight = 24;
            this.Grd_Oper.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.Grd_Oper.Enabled = false;
            this.Grd_Oper.Location = new System.Drawing.Point(33, 436);
            this.Grd_Oper.Name = "Grd_Oper";
            this.Grd_Oper.RowHeadersWidth = 20;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Oper.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_Oper.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Oper.Size = new System.Drawing.Size(515, 113);
            this.Grd_Oper.TabIndex = 9;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Chk";
            this.Column9.HeaderText = "تاشير الكل";
            this.Column9.Name = "Column9";
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column9.Width = 78;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Oper_Id";
            this.Column10.HeaderText = "رقـم العملية";
            this.Column10.Name = "Column10";
            this.Column10.Width = 65;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Aoper_Name";
            this.Column11.HeaderText = "اسم العملية";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 200;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "User_Name";
            this.Column12.HeaderText = "المنظم";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 150;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(26, 143);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 413);
            this.flowLayoutPanel5.TabIndex = 730;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(552, 143);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 413);
            this.flowLayoutPanel6.TabIndex = 731;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(27, 143);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(526, 1);
            this.flowLayoutPanel2.TabIndex = 722;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(26, 556);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(527, 1);
            this.flowLayoutPanel7.TabIndex = 732;
            // 
            // ExtBtn
            // 
            this.ExtBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(282, 560);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 11;
            this.ExtBtn.Text = "انهاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(193, 560);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(89, 26);
            this.Btn_Add.TabIndex = 10;
            this.Btn_Add.Text = "موافـق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 592);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(572, 22);
            this.statusStrip1.TabIndex = 736;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // Chk_Acc_All
            // 
            this.Chk_Acc_All.AutoSize = true;
            this.Chk_Acc_All.Enabled = false;
            this.Chk_Acc_All.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Acc_All.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Acc_All.Location = new System.Drawing.Point(36, 157);
            this.Chk_Acc_All.Name = "Chk_Acc_All";
            this.Chk_Acc_All.Size = new System.Drawing.Size(26, 18);
            this.Chk_Acc_All.TabIndex = 737;
            this.Chk_Acc_All.Text = "\r\n";
            this.Chk_Acc_All.UseVisualStyleBackColor = true;
            this.Chk_Acc_All.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chk_Acc_All_MouseClick);
            // 
            // Chk_Cust_All
            // 
            this.Chk_Cust_All.AutoSize = true;
            this.Chk_Cust_All.Enabled = false;
            this.Chk_Cust_All.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Cust_All.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Cust_All.Location = new System.Drawing.Point(36, 298);
            this.Chk_Cust_All.Name = "Chk_Cust_All";
            this.Chk_Cust_All.Size = new System.Drawing.Size(26, 18);
            this.Chk_Cust_All.TabIndex = 738;
            this.Chk_Cust_All.Text = "\r\n";
            this.Chk_Cust_All.UseVisualStyleBackColor = true;
            this.Chk_Cust_All.CheckedChanged += new System.EventHandler(this.Chk_Cust_All_CheckedChanged);
            // 
            // Chk_Oper_All
            // 
            this.Chk_Oper_All.AutoSize = true;
            this.Chk_Oper_All.Enabled = false;
            this.Chk_Oper_All.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Oper_All.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Oper_All.Location = new System.Drawing.Point(37, 439);
            this.Chk_Oper_All.Name = "Chk_Oper_All";
            this.Chk_Oper_All.Size = new System.Drawing.Size(26, 18);
            this.Chk_Oper_All.TabIndex = 739;
            this.Chk_Oper_All.Text = "\r\n";
            this.Chk_Oper_All.UseVisualStyleBackColor = true;
            this.Chk_Oper_All.CheckedChanged += new System.EventHandler(this.Chk_Oper_All_CheckedChanged);
            // 
            // Report_Group_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 614);
            this.Controls.Add(this.Chk_Oper_All);
            this.Controls.Add(this.Chk_Cust_All);
            this.Controls.Add(this.Chk_Acc_All);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Oper_Check);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Grd_Oper);
            this.Controls.Add(this.Chk_Cust);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.Chk_Acc);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Grd_Acc);
            this.Controls.Add(this.Grd_Grp_Id);
            this.Controls.Add(this.Txt_Grp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Report_Group_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "211";
            this.Text = "Report_Group_Add_Upd";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Report_Group_Add_Upd_FormClosed);
            this.Load += new System.EventHandler(this.Report_Group_Add_Upd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Grp_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Oper)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Grp;
        private System.Windows.Forms.DataGridView Grd_Grp_Id;
        private System.Windows.Forms.DataGridView Grd_Acc;
        private System.Windows.Forms.CheckBox Chk_Acc;
        private System.Windows.Forms.CheckBox Chk_Cust;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.CheckBox Oper_Check;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.DataGridView Grd_Oper;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.CheckBox Chk_Acc_All;
        private System.Windows.Forms.CheckBox Chk_Cust_All;
        private System.Windows.Forms.CheckBox Chk_Oper_All;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
    }
}