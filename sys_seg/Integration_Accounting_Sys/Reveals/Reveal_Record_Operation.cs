﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.remittences_online;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Record_Operation : Form
    {
        #region Definition
        DataTable Vo_Tbl;
        string Max_Nrec_Date;
        string Min_Nrec_Date;
        //int Max_Vo_no;
        //int Min_Vo_no;
        int Vo_No;
        //string Str_Oper_Tbl;
        //string Str_Term_Tbl;
        //string Str_User_Tbl;
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        bool Change = false;
        BindingSource _Bs_Grd_Vo = new BindingSource();
        BindingSource _Bs_GrdVo_Details = new BindingSource();
        string REF_ID = "";
        string Sqltext_rem = "";
        string sqltxt_vo = "";
        #endregion
        //---------------------------------------------------------------
        public Reveal_Record_Operation()
        {
            InitializeComponent();

            #region My Region
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            //Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, new TextBox());

            Grd_Vo.AutoGenerateColumns = false;
            GrdVo_Details.AutoGenerateColumns = false;
            // Min_Nrec_Date = Parameters[0];
            // Max_Nrec_Date  = Parameters[1];
            //Max_Vo_no = Convert.ToInt32(Parameters[2] == "" ? "0" : Parameters[2]);
            //Min_Vo_no = Convert.ToInt32(Parameters[3] == "" ? "0" : Parameters[3]);
            //Str_Oper_Tbl = Parameters[4];
            //Str_Term_Tbl = Parameters[5];
            //Str_User_Tbl = Parameters[6];

            if (connection.Lang_id != 1)
            {
                Column3.DataPropertyName = "Eoper_name";
                Column6.DataPropertyName = "ETerm_name";
                Column11.DataPropertyName = "Acc_Ename";
                Column13.DataPropertyName = "ECust_name";
                Column14.DataPropertyName = "Cur_Ename";
                Column19.DataPropertyName = "Eoper_name";
                Column23.DataPropertyName = "ETerm_Name";
            }
            #endregion

        }
        //---------------------------------------------------------------
        private void Reveal_Record_Operation_Load(object sender, EventArgs e)
        {
            //object[] Sparam = { Min_Vo_no, Max_Vo_no, Min_Nrec_Date, Max_Nrec_Date, Str_Oper_Tbl, Str_Term_Tbl, Str_User_Tbl };
            //MyGeneral_Lib.Copytocliptext("Reveal_Record_Operation", Sparam);





            //connection.SqlExec("Reveal_Record_Operation", "Vo_Record_Tbl", Sparam);
            Change = false;

            if (connection.SQLDS.Tables["Vo_Record_Tbl"].Rows.Count > 0)
            {
                var result = (from C in connection.SQLDS.Tables["Vo_Record_Tbl"].AsEnumerable()
                              select new
                              {
                                  VO_NO = C["Vo_NO"],
                                  oper_id = C["oper_id"],
                                  Aoper_name = C["Aoper_name"],
                                  Eoper_name = C["Eoper_name"],
                                  Create_Date = C["alter_date"],
                                  Nrec_date = C["Nrec_date"],
                                 // notes = C["notes"],
                                  T_id = C["T_id"],
                                  ATerm_name = C["ATerm_name"],
                                  ETerm_name = C["ETerm_name"],
                                  User_Name = C["User_Name"],
                                  nrec_date = C["nrec_date"],
                                  NREC_DATE1 = C["NREC_DATE1"],
                                  c_date=C["c_date"]
                              }).Distinct();
                Vo_Tbl = CustomControls.IEnumerableToDataTable(result);
                _Bs_Grd_Vo.DataSource = Vo_Tbl; ;
                Grd_Vo.DataSource = _Bs_Grd_Vo;
                Change = true;
                Grd_Vo_SelectionChanged(null, null);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتـوجد قيود تحقق الشروط" : "There Are No Records");
                this.Close();
                return;
            }

        }
        //---------------------------------------------------------------
        private void Grd_Vo_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                LblRec.Text = connection.Records(_Bs_Grd_Vo);

                _Bs_GrdVo_Details.DataSource =
                    connection.SQLDS.Tables["Vo_Record_Tbl"].Select("Vo_No = "
                    + ((DataRowView)_Bs_Grd_Vo.Current).Row["Vo_no"].ToString()
                    + " and oper_id =" + ((DataRowView)_Bs_Grd_Vo.Current).Row["Oper_id"].ToString()
                    + " and Nrec_date =" + ((DataRowView)_Bs_Grd_Vo.Current).Row["nrec_date"].ToString()
                     + " and T_id =" + ((DataRowView)_Bs_Grd_Vo.Current).Row["T_id"].ToString()
                      ).CopyToDataTable();
                GrdVo_Details.DataSource = _Bs_GrdVo_Details;
                Txt_Detl.DataBindings.Clear();
                Txt_Detl.DataBindings.Add("Text", _Bs_GrdVo_Details, "Notes");
                try
                {
                    REF_ID = ((DataRowView)_Bs_GrdVo_Details.Current).Row["REF_ID"].ToString();

                    if (REF_ID != "")
                    {
                        Sqltext_rem = " select rem_no,R_amount,R_ACUR_NAME,PR_ACUR_NAME,t_ACITY_NAME,S_ACITY_NAME,ACASE_NA,Case_Date,C_DATE,user_name,S_name,"
                                    + " S_address,r_name,r_address,Source_money,Relation_S_R from remittences_online where rem_id= " + REF_ID;
                        connection.SqlExec(Sqltext_rem, "Rem_Tbl");

                        sqltxt_vo = " select A.VO_NO,A.ACC_Id, Acc_Aname,Acc_EName, A.EXCH_PRICE, A.REAL_PRICE,A.OPER_ID,D.AOPER_NAME,D.EOPER_NAME , A.NREC_DATE, A.C_Date"
                            + " From  Voucher A "
                            + " join  Account_tree B on A.Acc_Id  = B.Acc_Id "
                            + " join OPERATIONS D   on A.Oper_id = D.Oper_id "
                            + " where ref_id= " + REF_ID;
                        connection.SqlExec(sqltxt_vo, "VO_Tbl");

                        button1.Enabled = true;
                    }

                    else
                    {
                        button1.Enabled = false;
                    }
                }
                catch { button1.Enabled = false; }


            }
        }
        //--------------------------------------------------------------
        private void Reveal_Record_Operation_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Str = { "Vo_Record_Tbl", "Rem_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
            //connection.SQLDS.Tables.Remove("Vo_Record_Tbl","Rem_Tbl");
        }

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {

            try
            {
                Int16 Vo_no = Convert.ToInt16(((DataRowView)_Bs_Grd_Vo.Current).Row["Vo_No"]);
                int T_id = Convert.ToInt32(((DataRowView)_Bs_Grd_Vo.Current).Row["T_id"]);
                string Nrec_date = ((DataRowView)_Bs_Grd_Vo.Current).Row["Nrec_date"].ToString();
                Int16 oper_id = Convert.ToInt16(((DataRowView)_Bs_Grd_Vo.Current).Row["oper_id"]); 
                Date();
                DataTable ExpDt_Term = Vo_Tbl.DefaultView.ToTable(false, "Vo_No", "oper_id",
                    connection.Lang_id == 1 ? "Aoper_name" : "Eoper_name",
                    "Nrec_date", "T_id", connection.Lang_id == 1 ? "Aterm_Name" : "Eterm_Name", "User_name")
                    .Select("oper_id =" + oper_id + "And Vo_no = " + Vo_no
                     + "And T_id = " + T_id + "And Nrec_date = " + Nrec_date).CopyToDataTable();
            
                DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Vo_Record_Tbl"].DefaultView.ToTable(false, "Vo_No", "debit_for_Amount",
                    "credit_for_amount", "Acc_id", connection.Lang_id == 1 ? "acc_aname" : "acc_ename",
                    connection.Lang_id == 1 ? "Acust_name" : "Ecust_name",
                    connection.Lang_id == 1 ? "cur_Aname" : "cur_Ename", "exch_price", "real_price",
                    "debit_loc_amount", "credit_loc_amount", "Oper_id", connection.Lang_id == 1 ? "Aoper_name" : "Eoper_name"
                    , "Nrec_date", "c_date", "notes" , "T_id", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", "User_Name").Select(" Vo_No = "
                        + Vo_no.ToString()
                        + " and oper_id =" + oper_id.ToString()
                        + " and Nrec_date =" + Nrec_date.ToString()
                         + " and T_id =" + T_id.ToString()
                          ).CopyToDataTable();

                ExpDt_Term_Acc = ExpDt_Term_Acc.DefaultView.ToTable(false, "Vo_No", "debit_for_Amount",
                    "credit_for_amount", "Acc_id", connection.Lang_id == 1 ? "acc_aname" : "acc_ename",
                    connection.Lang_id == 1 ? "Acust_name" : "Ecust_name",
                    connection.Lang_id == 1 ? "cur_Aname" : "cur_Ename", "exch_price", "real_price",
                    "debit_loc_amount", "credit_loc_amount", "Oper_id", connection.Lang_id == 1 ? "Aoper_name" : "Eoper_name"
                    , "Nrec_date", "c_date", "notes", "T_id", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", "User_Name").Select().CopyToDataTable();
                DataTable[] _EXP_DT = { Date_Tbl, ExpDt_Term, ExpDt_Term_Acc };
                DataGridView[] Export_GRD = { Date_Grd, Grd_Vo, GrdVo_Details };
                MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
            }
            catch { }

        }
        private void Date()
        {
            Date_Grd = new DataGridView();
            Date_Tbl = new DataTable();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            Date_Tbl.Rows.Add(new object[] { Min_Nrec_Date, Max_Nrec_Date });
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            Date();
            DataTable ExpDt_Term = Vo_Tbl.DefaultView.ToTable(false, "Vo_No", "Oper_ID",
                connection.Lang_id == 1 ? "Aoper_Name" : "Eoper_Name", "Create_Date", "T_id",
                connection.Lang_id == 1 ? "Aterm_Name" : "Eterm_Name", "User_name").Select().CopyToDataTable();
            DataTable[] _EXP_DT = { Date_Tbl, ExpDt_Term };
            DataGridView[] Export_GRD = { Date_Grd, Grd_Vo };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Box_ACust_Cur = "";
            string Box_ECust_Cur = "";
            int oper_id = 0;
            int Nrec_date = 0;
            string strNrec_date = "";
            oper_id = Convert.ToInt16(((DataRowView)_Bs_Grd_Vo.Current).Row["oper_id"]);
            Nrec_date = Convert.ToInt32(((DataRowView)_Bs_Grd_Vo.Current).Row["nrec_date"]);
            strNrec_date = Convert.ToString(((DataRowView)_Bs_Grd_Vo.Current).Row["nrec_date"]);
            Vo_No = Convert.ToInt16(((DataRowView)_Bs_Grd_Vo.Current).Row["Vo_No"]);
            //------------قيد تسويه

            if (oper_id == 75)
            {

                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",75," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
               // string SqlTxt = "Exec Main_Report " + Vo_No + "," + Convert.ToInt32(((DataRowView)_Bs_Rec_Daily.Current).Row["N_r_c"]) + ",75," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                connection.SqlExec(SqlTxt, "Rec_cur");
                DateTime C_Date;
                 C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date"]);
                 DateTime _Date1 = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date1"]);
                 var _Date = _Date1.ToShortDateString(); 
                //DateTime DD = Convert.ToDateTime( connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date1"]);
                //var x = DD.Date;
                //string _Date = x.ToString();
                // string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                //string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب الدائن" + ": " +
                //                 ((DataRowView)_Bs_Rec_Daily1.Current).Row["Acust_Name"].ToString();
                //string ECur_Cust = "Credit Account" + ": " + ((DataRowView)_Bs_Rec_Daily1.Current).Row["Ecust_Name"].ToString() + " / " +
                //                          connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

                Decimal neg_sum = Math.Abs(Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 < 0")));
                Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 75);
                Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("neg_sum", neg_sum);
                Dt_Param.Rows.Add("pos_sum", pos_sum);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());
                DataRow Dr;
                int y = connection.SQLDS.Tables["Rec_cur"].Rows.Count;
                if (y <= 10)
                {
                    for (int i = 0; i < (10 - y); i++)
                    {
                        Dr = connection.SQLDS.Tables["Rec_cur"].NewRow();
                        Dr["R_T_Y_Id"] = "100000000000000000"; // اضيف لترتيب القيود في الطباعة
                        connection.SQLDS.Tables["Rec_cur"].Rows.Add(Dr);
                    }


                    Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
                    Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();


                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 75);

                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Rec_cur");
                }
                else
                {
                    Rec_Daily_Rpt_Multirow ObjRpt1 = new Rec_Daily_Rpt_Multirow();
                    Rec_Daily_Rpt_Multirow ObjRptEng1 = new Rec_Daily_Rpt_Multirow();

                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 75);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Rec_cur");

                }

                //string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",75," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                //connection.SqlExec(SqlTxt, "Rec_cur");
                //DateTime C_Date;
                //C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date"]);
                //// string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                //string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب الدائن" + ": " +
                //  ((DataRowView)_Bs_GrdVo_Details.Current).Row["Acust_Name"].ToString();
                //string ECur_Cust = "Credit Account" + ": " + ((DataRowView)_Bs_GrdVo_Details.Current).Row["Ecust_Name"].ToString() + " / " +
                //                          connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString(); connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

                //Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
                //Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();
                //DataTable Dt_Param = CustomControls.CustomParam_Dt();
                //Dt_Param.Rows.Add("Vo_No", Vo_No);
                //Dt_Param.Rows.Add("Oper_Id", 75);
                //Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                //Dt_Param.Rows.Add("C_Date", C_Date);
                //Dt_Param.Rows.Add("_Date", _Date);
                //Dt_Param.Rows.Add("Frm_Id", "Main");
                //Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());
                //DataRow Dr;
                //int y = connection.SQLDS.Tables["Rec_cur"].Rows.Count;
                //for (int i = 0; i < (10 - y); i++)
                //{
                //    Dr = connection.SQLDS.Tables["Rec_cur"].NewRow();
                //    connection.SQLDS.Tables["Rec_cur"].Rows.Add(Dr);
                //}

                //Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 75);

                //this.Visible = false;
                //RptLangFrm.ShowDialog(this);
                //this.Visible = true;
                //connection.SQLDS.Tables.Remove("Rec_cur");

            }
            //--------------قبض نقدي----------
            if (oper_id == 2)
            {
            
                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",2," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                connection.SqlExec(SqlTxt, "main");
                DataTable Voucher_Cur = connection.SQLDS.Tables["main"].Select("For_Amount1 < 0").CopyToDataTable();
                Voucher_Cur.TableName = "Voucher_Cur";
                connection.SQLDS.Tables.Add(Voucher_Cur);
                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["C_Date"]);
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                string _Date = connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["alter_Date"].ToString();
                string Cur_ToWord = "";
                string Cur_ToEWord = "";
                ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Voucher_Cur"].Compute("sum(LOC_AMOUNT)", "")),
                    new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"].ToString() + " لاغير)- ";
                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
                connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToWord");
                connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToEWord");
                connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
                connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
                String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                //Box_ACust_Cur = Cur_Code + "/" + "الحساب المدين" + ":" + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"].ToString();
                //Box_ECust_Cur = "Debit Account : " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
                DataTable Voucher_Cur1 = connection.SQLDS.Tables["main"].Select("For_Amount1 > 0").CopyToDataTable();
                string ABox_name = Voucher_Cur1.Rows[0]["Acust_Name"].ToString();
                string EBox_name = Voucher_Cur1.Rows[0]["Ecust_Name"].ToString();

                Box_ACust_Cur =   "الحساب المدين" + ":" + ABox_name;
                Box_ECust_Cur = "Debit Account : " + EBox_name;// +"/" + Cur_Code;
                Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["main"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 2);
                Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
                Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("pos_sum", pos_sum);

                DataRow Dr;
                int y = connection.SQLDS.Tables["Voucher_Cur"].Rows.Count;
                if (y <= 10)
                {
                    for (int i = 0; i < (10 - y); i++)
                    {
                        Dr = connection.SQLDS.Tables["Voucher_Cur"].NewRow();
                        connection.SQLDS.Tables["Voucher_Cur"].Rows.Add(Dr);
                    }
                    Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
                    Rap_Spend_Cash_Rpt_Eng ObjRptEng = new Rap_Spend_Cash_Rpt_Eng();

                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 2);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Voucher_Cur");
                    connection.SQLDS.Tables.Remove("main");
                }
                else
                {


                    Sub_Bill_Rap_Spend_MultiRow ObjRpt1 = new Sub_Bill_Rap_Spend_MultiRow();
                    Sub_Bill_Rap_Spend_MultiRow ObjRptEng1 = new Sub_Bill_Rap_Spend_MultiRow();

                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 2);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Voucher_Cur");
                    connection.SQLDS.Tables.Remove("main");


                }

                //string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",2," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                //connection.SqlExec(SqlTxt, "main");
                //DataTable Voucher_Cur = connection.SQLDS.Tables["main"].Select("For_Amount1 < 0").CopyToDataTable();
                //Voucher_Cur.TableName = "Voucher_Cur";
                //connection.SQLDS.Tables.Add(Voucher_Cur);
                //DateTime C_Date;
                //C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["C_Date"]);
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                //string Cur_ToWord = "";
                //string Cur_ToEWord = "";
                //ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Voucher_Cur"].Compute("sum(for_amount)", "")),
                //    new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                //Cur_ToWord = "-(" + toWord.ConvertToArabic() + " لاغير)- ";
                //Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " not else.)-";
                //connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToWord");
                //connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToEWord");
                //connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
                //connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
                //String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                //Box_ACust_Cur = Cur_Code + "/" + "الحساب المدين" + ":" + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"].ToString();
                //Box_ECust_Cur = "Debit Account : " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
                //Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
                //Rap_Spend_Cash_Rpt_Eng ObjRptEng = new Rap_Spend_Cash_Rpt_Eng();
                //DataTable Dt_Param = CustomControls.CustomParam_Dt();
                //Dt_Param.Rows.Add("Vo_No", Vo_No);
                //Dt_Param.Rows.Add("Oper_Id", 2);
                //Dt_Param.Rows.Add("Year", strNrec_date.Substring(0, 4));
                //Dt_Param.Rows.Add("C_Date", C_Date);
                //Dt_Param.Rows.Add("_Date", _Date);
                //Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
                //Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
                //Dt_Param.Rows.Add("Frm_Id", "Main");
                //DataRow Dr;
                //int y = connection.SQLDS.Tables["Voucher_Cur"].Rows.Count;
                //for (int i = 0; i < (10 - y); i++)
                //{
                //    Dr = connection.SQLDS.Tables["Voucher_Cur"].NewRow();
                //    connection.SQLDS.Tables["Voucher_Cur"].Rows.Add(Dr);
                //}
                //Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 2);
                //this.Visible = false;
                //RptLangFrm.ShowDialog(this);
                //this.Visible = true;
                //connection.SQLDS.Tables.Remove("Voucher_Cur");
                //connection.SQLDS.Tables.Remove("main");
            }

            //--------------صرف نقدي----------
            if (oper_id == 7)
            {


                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",7," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                connection.SqlExec(SqlTxt, "main");
                DataTable Voucher_Cur = connection.SQLDS.Tables["main"].Select("For_Amount1 > 0").CopyToDataTable();
                Voucher_Cur.TableName = "Voucher_Cur";
                connection.SQLDS.Tables.Add(Voucher_Cur);
                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["C_Date"]);
               // string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                string _Date = connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["alter_Date"].ToString();
                string Cur_ToWord = "";
                string Cur_ToEWord = "";
                ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Voucher_Cur"].Compute("sum(LOC_AMOUNT)", "")),
                    new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"].ToString() + " لاغير)- ";
                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
                connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToWord");
                connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToEWord");
                connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
                connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
                String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                DataTable Voucher_Cur1 = connection.SQLDS.Tables["main"].Select("For_Amount1 < 0").CopyToDataTable();
                string ABox_name = Voucher_Cur1.Rows[0]["Acust_Name"].ToString();
                string EBox_name = Voucher_Cur1.Rows[0]["Ecust_Name"].ToString();

                Box_ACust_Cur =  "الحساب الدائن" + ":" + ABox_name;
                Box_ECust_Cur = "Credit Account : " + EBox_name;// +"/" + Cur_Code;
                Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["main"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 7);
                Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
                Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("pos_sum", pos_sum);

                DataRow Dr;
                int y = connection.SQLDS.Tables["Voucher_Cur"].Rows.Count;
                if (y <= 10)
                {
                    for (int i = 0; i < (10 - y); i++)
                    {
                        Dr = connection.SQLDS.Tables["Voucher_Cur"].NewRow();
                        connection.SQLDS.Tables["Voucher_Cur"].Rows.Add(Dr);
                    }
                    Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
                    Rap_Spend_Cash_Rpt_Eng ObjRptEng = new Rap_Spend_Cash_Rpt_Eng();
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 7);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Voucher_Cur");
                    connection.SQLDS.Tables.Remove("main");
                }
                else
                {
                    Sub_Bill_Rap_Spend_MultiRow ObjRpt1 = new Sub_Bill_Rap_Spend_MultiRow();
                    Sub_Bill_Rap_Spend_MultiRow ObjRptEng1 = new Sub_Bill_Rap_Spend_MultiRow();

                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 7);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Voucher_Cur");
                    connection.SQLDS.Tables.Remove("main");
                }

                //string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",7," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                //connection.SqlExec(SqlTxt, "main");
                //DataTable Voucher_Cur = connection.SQLDS.Tables["main"].Select("For_Amount1 > 0").CopyToDataTable();
                //Voucher_Cur.TableName = "Voucher_Cur";
                //connection.SQLDS.Tables.Add(Voucher_Cur);
                //DateTime C_Date;
                //C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Voucher_Cur"].Rows[0]["C_Date"]);
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                //string Cur_ToWord = "";
                //string Cur_ToEWord = "";
                //ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Voucher_Cur"].Compute("sum(for_amount)", "")),
                //    new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                //Cur_ToWord = "-(" + toWord.ConvertToArabic() + " لاغير)- ";
                //Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " not else.)-";
                //connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToWord");
                //connection.SQLDS.Tables["Voucher_Cur"].Columns.Add("Cur_ToEWord");
                //connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
                //connection.SQLDS.Tables["Voucher_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
                //String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                //Box_ACust_Cur = Cur_Code + "/" + "الحساب الدائن" + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
                //Box_ECust_Cur = "Credit Account : " + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
                //Rap_Spend_Cash_Rpt ObjRpt = new Rap_Spend_Cash_Rpt();
                //Rap_Spend_Cash_Rpt_Eng ObjRptEng = new Rap_Spend_Cash_Rpt_Eng();
                //DataTable Dt_Param = CustomControls.CustomParam_Dt();
                //Dt_Param.Rows.Add("Vo_No", Vo_No);
                //Dt_Param.Rows.Add("Oper_Id", 7);
                //Dt_Param.Rows.Add("Year", strNrec_date.Substring(0, 4));
                //Dt_Param.Rows.Add("C_Date", C_Date);
                //Dt_Param.Rows.Add("_Date", _Date);
                //Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
                //Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
                //Dt_Param.Rows.Add("Frm_Id", "Main");
                //DataRow Dr;
                //int y = connection.SQLDS.Tables["Voucher_Cur"].Rows.Count;
                //for (int i = 0; i < (10 - y); i++)
                //{
                //    Dr = connection.SQLDS.Tables["Voucher_Cur"].NewRow();
                //    connection.SQLDS.Tables["Voucher_Cur"].Rows.Add(Dr);
                //}
                //Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 7);
                //this.Visible = false;
                //RptLangFrm.ShowDialog(this);
                //this.Visible = true;
                //connection.SQLDS.Tables.Remove("Voucher_Cur");
                //connection.SQLDS.Tables.Remove("main");


            }
            //-------------- قيد يومية----------
            if (oper_id == 31)
            {


                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",31," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                connection.SqlExec(SqlTxt, "Rec_cur");
                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Rec_cur"].Rows[0]["C_Date"]);
                string _Date = connection.SQLDS.Tables["Rec_cur"].Rows[0]["alter_Date"].ToString();
               // string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                //string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب الدائن" + ": " +
                //                 ((DataRowView)_Bs_Rec_Daily1.Current).Row["Acust_Name"].ToString();
                //string ECur_Cust = "Credit Account" + ": " + ((DataRowView)_Bs_Rec_Daily1.Current).Row["Ecust_Name"].ToString() + " / " +
                //                          connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

                Decimal neg_sum = Math.Abs(Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 < 0")));
                Decimal pos_sum = Convert.ToDecimal(connection.SQLDS.Tables["Rec_cur"].Compute("Sum(LOC_AMOUNT1)", "LOC_AMOUNT1 > 0"));

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 31);
                Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("neg_sum", neg_sum);
                Dt_Param.Rows.Add("pos_sum", pos_sum);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());
                DataRow Dr;
                int y = connection.SQLDS.Tables["Rec_cur"].Rows.Count;
                if (y <= 10)
                {
                    for (int i = 0; i < (10 - y); i++)
                    {
                        Dr = connection.SQLDS.Tables["Rec_cur"].NewRow();
                        Dr["R_T_Y_Id"] = "10000000000"; // اضيف لترتيب القيود في الطباعة
                        connection.SQLDS.Tables["Rec_cur"].Rows.Add(Dr);
                    }


                    Rec_Daily_Rpt ObjRpt = new Rec_Daily_Rpt();
                    Rec_Daily_Rpt_Eng ObjRptEng = new Rec_Daily_Rpt_Eng();


                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 31);

                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Rec_cur");
                }
                else
                {
                    Rec_Daily_Rpt_Multirow ObjRpt1 = new Rec_Daily_Rpt_Multirow();
                    Rec_Daily_Rpt_Multirow ObjRptEng1 = new Rec_Daily_Rpt_Multirow();

                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt1, ObjRptEng1, true, true, Dt_Param, 31);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Rec_cur");

                }


            }


            //---------بيع موجود نقدي

            if (oper_id == 40)
            {
                // int Vo_No = Convert.ToInt16(((DataRowView)Bs_Sal_asstes_GRD.Current).Row["Vo_No"]);
                //string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",39," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;

                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",40," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                string SqlTxt1 = "Exec Main_Report_Assets_Details " + Vo_No + "," + Nrec_date + ",40," + connection.T_ID;

                connection.SqlExec(SqlTxt, "Asset_Voucher");
                connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
                string _Date = connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["alter_Date"].ToString();
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
                Main_Assets_Rpt ObjRptEng = new Main_Assets_Rpt();



                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 40);
                Dt_Param.Rows.Add("Year", strNrec_date.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

                DataRow Dr;
                int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                    connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
                }

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 40);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Asset_Voucher");
                connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
            }

            //-------------بيع موجودات نقدي


            if (oper_id == 39)
            {

                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",39," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                string SqlTxt1 = "Exec Main_Report_Assets_Details " + Vo_No + "," + Nrec_date + ",39," + connection.T_ID;

                connection.SqlExec(SqlTxt, "Asset_Voucher");
                connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                string _Date = connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["alter_Date"].ToString();
                connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
                Main_Assets_Rpt ObjRptEng = new Main_Assets_Rpt();



                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 39);
                Dt_Param.Rows.Add("Year", strNrec_date.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

                DataRow Dr;
                int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                    connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
                }

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 39);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Asset_Voucher");
                connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
            }


            //----------------------- شراء موجودات نقدي


            if (oper_id == 37)
            {
                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",37," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                string SqlTxt1 = "Exec Main_Report_Assets_Details " + Vo_No + "," + Nrec_date + ",37," + connection.T_ID;

                connection.SqlExec(SqlTxt, "Asset_Voucher");
                connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
               // string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                string _Date = connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["alter_Date"].ToString();
                connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
                Main_Assets_Rpt ObjRptEng = new Main_Assets_Rpt();



                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 37);
                Dt_Param.Rows.Add("Year", strNrec_date.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

                DataRow Dr;
                int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                    connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
                }

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 37);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Asset_Voucher");
                connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
            }


            //---------------------- شراء موجودات قيد يومية

            if (oper_id == 38)
            {
                string SqlTxt = "Exec Main_Report " + Vo_No + "," + Nrec_date + ",38," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
                string SqlTxt1 = "Exec Main_Report_Assets_Details " + Vo_No + "," + Nrec_date + ",38," + connection.T_ID;

                connection.SqlExec(SqlTxt, "Asset_Voucher");
                connection.SqlExec(SqlTxt1, "Asset_Details_Tbl");

                DateTime C_Date;
                C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["C_Date"]);
                //string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
                string _Date = connection.SQLDS.Tables["Asset_Voucher"].Rows[0]["alter_Date"].ToString();
                connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                Main_Assets_Rpt ObjRpt = new Main_Assets_Rpt();
                Main_Assets_Rpt ObjRptEng = new Main_Assets_Rpt();



                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", 38);
                Dt_Param.Rows.Add("Year", strNrec_date.Substring(0, 4));
                Dt_Param.Rows.Add("C_Date", C_Date);
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("Frm_Id", "Main");
                Dt_Param.Rows.Add("Cur_Code", connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString());

                DataRow Dr;
                int y = connection.SQLDS.Tables["Asset_Voucher"].Rows.Count;
                for (int i = 0; i < (10 - y); i++)
                {
                    Dr = connection.SQLDS.Tables["Asset_Voucher"].NewRow();
                    connection.SQLDS.Tables["Asset_Voucher"].Rows.Add(Dr);
                }

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 38);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Asset_Voucher");
                connection.SQLDS.Tables.Remove("Asset_Details_Tbl");
            }


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataTable Dt_Vo_Record_Tbl = connection.SQLDS.Tables["Vo_Record_Tbl"].DefaultView.ToTable(false, "Vo_No", "Debit_For_Amount",
                "Credit_For_Amount","Acc_id",connection.Lang_id == 1 ?"Acc_Aname":"Acc_Ename",
                connection.Lang_id == 1 ?"ACust_Name":"ECust_Name",connection.Lang_id == 1 ?"Cur_Aname":"Cur_Ename",
                "EXCH_PRICE","REAL_Price","Debit_loc_Amount","Credit_loc_Amount","Oper_id",connection.Lang_id == 1 ?"Aoper_name":"Eoper_name","NREC_DATE1",
                "c_date", "notes", "T_id", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", "User_Name").Select().CopyToDataTable();

            Dt_Vo_Record_Tbl.TableName = "Dt_Vo_Record_Tbl";
            //DataTable[] _EXP_DT = { Dt_Vo_Record_Tbl };
            //DataGridView[] Export_GRD = { GrdVo_Details };
            //MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

            NewExpoert.Export_ToExcel("استـــــــعلام عن قيود العمليات شامل", Dt_Vo_Record_Tbl, GrdVo_Details, true);


        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Remittances_Information AddFrm = new Remittances_Information();
            this.Visible = false;
            AddFrm.ShowDialog(this);
            this.Visible = true;
        }
    }
}