﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_CCP_List : Form
    {
        #region Defintion
        DataTable DT_Cur = new DataTable();
        DataTable DT_Term = new DataTable();
        int Cur_id = 0; 
        int T_id = 0;
        bool Change = false;
        BindingSource _Bs_grd1 = new BindingSource();
        BindingSource _Bs_grd2 = new BindingSource();
        #endregion
        public Reveal_CCP_List(DataTable DT_cur1 , DataTable DT_Term1)
        {
            InitializeComponent();

            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            DT_Cur = DT_cur1;
            DT_Term = DT_Term1 ;

            Grd_Terminals.AutoGenerateColumns = false;
            Grd_Cur.AutoGenerateColumns = false;
            Grd_CCP_List.AutoGenerateColumns = false;
            #endregion
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Column15.DataPropertyName = "ECust_Name";
                Column17.DataPropertyName = "Cur_Ename";
           
            }
            #endregion
        }
        //------------------------------------------------------------
        private void Reveal_CCP_List_Load(object sender, EventArgs e)
        {
            _Bs_grd1.DataSource = DT_Term;
            Grd_Terminals.DataSource = _Bs_grd1;
            _Bs_grd2.DataSource = DT_Cur;
            Grd_Cur.DataSource = _Bs_grd2;
            Change = false;
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Reveal_CCP_list]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Cur_TBL", DT_Cur);
                connection.SQLCMD.Parameters.AddWithValue("@Term_Tbl", DT_Term);               
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                 connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_CCP_list_TBL");
                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
            if (connection.SQLDS.Tables["Reveal_CCP_list_TBL"].Rows.Count > 0)
            {
                Change = true;
                Grd_Terminals_SelectionChanged(null, null);
            }
                      
        }
        //------------------------------------------------------------
        private void Grd_Cur_SelectionChanged(object sender, EventArgs e)
        {
            Grd_Terminals_SelectionChanged(null, null);
        }
        //------------------------------------------------------------
        private void Grd_Terminals_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                try
                {
                    Cur_id = Convert.ToInt32(((DataRowView)_Bs_grd2.Current).Row["Cur_id"]);
                    T_id = Convert.ToInt32(((DataRowView)_Bs_grd1.Current).Row["T_id"]);

                    Grd_CCP_List.DataSource = connection.SQLDS.Tables["Reveal_CCP_list_TBL"].DefaultView.ToTable(false).Select("Cur_id =" + Cur_id + " and T_id =" + T_id).CopyToDataTable();
                }
                catch
                {
                    Grd_CCP_List.DataSource = new BindingSource();
                }
            }
        }   
        //------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            DataTable ExpDt = connection.SQLDS.Tables["Reveal_CCP_list_TBL"].DefaultView.ToTable(false,
                "T_Id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", "Cur_Id", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "C_Date", "Exch_Rate",
               "Min_B_Price", "Max_B_Price", "Min_S_Price", "Max_S_price", "User_Name").Select("cur_id=" + ((DataRowView)_Bs_grd2.Current).Row["cur_id"] +
                                                                                               " And T_Id=" + ((DataRowView)_Bs_grd1.Current).Row["T_id"]).CopyToDataTable();
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_CCP_List };
            MyExports.To_ExcelByHtml(_EXP_DT,Export_GRD, true,true,this.Text);
        }
        //------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
           
            DataTable ExpDt = connection.SQLDS.Tables["Reveal_CCP_list_TBL"].DefaultView.ToTable(false,
                "T_Id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", "Cur_Id", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "C_Date", "Exch_Rate",
                "Min_B_Price", "Max_B_Price", "Min_S_Price", "Max_S_price", "User_Name");
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_CCP_List };

            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true,true,this.Text);
        }
        //------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
           
            this.Close();
        }
        //------------------------------------------------------------
        private void Reveal_CCP_List_FormClosed(object sender, FormClosedEventArgs e)
        {
            DT_Cur.Clear();
            DT_Term.Clear();
            Change = false;


            string[] Str = { "Reveal_CCP_list_TBL" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }
    }
}