﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;


namespace Integration_Accounting_Sys
{
    public partial class Reveal_Balance_Local_Amount : Form
    {
        #region Definshion
        bool TermChange = false;
        bool Assemble_AccChange = false;
        bool Details_AccChange = false;
        BindingSource Term_Bs = new BindingSource();
        BindingSource Assemble_Acc_Bs = new BindingSource();
        BindingSource Details_Acc_Bs = new BindingSource();
        DataTable Assembl_Acc_DT = new DataTable();
        DataTable Details_Acc_DT = new DataTable();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataTable Cust_DT = new DataTable();
        DataTable Dt = new DataTable();
        int T_Id = 0;
        int Acc_Id = 0;
        string Dot_Acc_No = "";
        DataTable Assembl_Acc_DT_Rep = new DataTable();
        DataTable Details_Acc_DT_Rep = new DataTable();
        DataTable Dt_Rep = new DataTable();
        #endregion
        //---------------------------
        public Reveal_Balance_Local_Amount(string FromDate, string ToDate)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Terminals.AutoGenerateColumns = false;
            Grd_Acc_Details.AutoGenerateColumns = false;
            Grd_Acc_Assembly.AutoGenerateColumns = false;
            Grd_Vo_Details.AutoGenerateColumns = false;
            DateTime Dfrom = new DateTime();
            DateTime DTo = new DateTime();
            Dfrom =Convert.ToDateTime( FromDate);
            DTo = Convert.ToDateTime(ToDate); 
            TxtFromDate.Text = Dfrom.ToString("yyyy/MM/dd");
            TxtToDate.Text = DTo.ToString("yyyy/MM/dd"); ;
            if (connection.Lang_id == 2)
            {
                Column7.DataPropertyName = "Acc_Ename";
                Column10.DataPropertyName = "Ecust_Name";
                Column11.DataPropertyName = "cur_EName";
                
            }
        }
        //---------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {
            TermChange = false;
            Term_Bs.DataSource = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl"];
            Grd_Terminals.DataSource = Term_Bs;
            if (connection.SQLDS.Tables["Reveal_Balance_Main_Tbl"].Rows.Count > 0)
            {
                TermChange = true;
                Grd_Terminals_SelectionChanged(sender, e);
            }
        }
        //---------------------------
        private void Grd_Terminals_SelectionChanged(object sender, EventArgs e)
        {
            if (TermChange)
            {
                try
                {
                    Assembl_Acc_DT = new DataTable();
                    Assemble_AccChange = false;
                    T_Id = Convert.ToInt32(((DataRowView)Term_Bs.Current).Row["T_Id"]);
                    Assembl_Acc_DT = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl1"].Select("T_Id = " + T_Id).CopyToDataTable();
                    Assemble_Acc_Bs.DataSource = Assembl_Acc_DT;
                    Grd_Acc_Assembly.DataSource = Assemble_Acc_Bs;
                    Assembl_Acc_DT.TableName = "Assembl_Acc_DT";
                   
                    TxtE_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(Eloc_Amount)", "").ToString();

                    //int linqSum = (from DataRow dr in Assembl_Acc_DT.AsEnumerable()
                    //               select  Convert.ToInt32(dr["Eloc_Amount"])).Sum();
                    //TxtE_LocAmount.Text = linqSum.ToString();

                    //object X = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl1"].Compute("Sum(Eloc_Amount)", "").ToString();
                    if (Assemble_Acc_Bs.List.Count > 0)
                    {
                        Assemble_AccChange = true;
                        Grd_Acc_Assembly_SelectionChanged(sender, e);
                    }
                }
                catch
                { Grd_Acc_Assembly.DataSource = new DataTable(); }
            }
        }
        //---------------------------
        private void Grd_Acc_Assembly_SelectionChanged(object sender, EventArgs e)
        {
            if (Assemble_AccChange)
            {
                try
                {
                    Details_Acc_DT = new DataTable();
                    Details_AccChange = false;
                    Dot_Acc_No = ((DataRowView)Assemble_Acc_Bs.Current).Row["Dot_Acc_No"].ToString();
                    Details_Acc_DT = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl2"].Select("T_Id = " + T_Id
                                    + " And Dot_Acc_No Like '" + Dot_Acc_No + "%'").CopyToDataTable();

                    var query = from rec in Details_Acc_DT.AsEnumerable()
                                select new
                                {
                                    Dot_Acc_No = rec["Dot_Acc_No"],
                                    Acc_Id = rec["Acc_Id"],
                                    Cust_Id = rec["Cust_Id"],
                                    Acust_Name = rec["Acust_Name"],
                                    Ecust_Name = rec["Ecust_Name"],
                                    Cur_Aname = rec["Cur_Aname"],
                                    Cur_Ename = rec["Cur_Ename"],
                                    Cur_Id = rec["For_Cur_Id"],
                                    DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? Convert.ToDecimal(rec["EFor_Amount"]) : 0,
                                    CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
                                    ELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"])
                                };
                    Details_Acc_DT = CustomControls.IEnumerableToDataTable(query);
                    Details_Acc_Bs.DataSource = Details_Acc_DT;
                    Grd_Acc_Details.DataSource = Details_Acc_Bs;


                    TxtED_ForAmount.Text = Details_Acc_DT.Compute("Sum(DEFor_Amount)", "").ToString();
                    TxtEC_ForAmount.Text = Details_Acc_DT.Compute("Sum(CEFor_Amount)", "").ToString();
                    Txt_LocAmount_Sum.Text = Details_Acc_DT.Compute("Sum(Eloc_Amount)", "").ToString();


                    if (Details_Acc_Bs.List.Count > 0)
                    {
                        Details_AccChange = true;
                        Grd_Acc_Details_SelectionChanged(sender, e);
                    }
                }
                catch
                { Grd_Acc_Details.DataSource = new DataTable();
                Grd_Vo_Details.DataSource = new DataTable();
                TxtED_ForAmount.ResetText();
                TxtEC_ForAmount.ResetText();
                Txt_LocAmount_Sum.ResetText();
                }
            }
        }
        //---------------------------
        private void Grd_Acc_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (Details_AccChange)
            {
                try
                {
                    Acc_Id = Convert.ToInt32(((DataRowView)Details_Acc_Bs.Current).Row["Acc_Id"]);
                    int Cust_id = Convert.ToInt32(((DataRowView)Details_Acc_Bs.Current).Row["Cust_id"]);
                    int Cur_Id = Convert.ToInt32(((DataRowView)Details_Acc_Bs.Current).Row["Cur_id"]);
                    Dot_Acc_No = ((DataRowView)Details_Acc_Bs.Current).Row["Dot_Acc_No"].ToString();
                    //---------------------------------------
                    decimal MBalance = 0;
                    decimal Mfor_Balane = 0;
                    try
                    {
                        Dt = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl3"].Select("T_id = " + T_Id
                                                                                    + " And Dot_Acc_No Like '" + Dot_Acc_No + "%'"
                                                                                    + " And Cust_Id = " + Cust_id
                                                                                    + " And For_Cur_id = " + Cur_Id).CopyToDataTable();
                    }
                    catch
                    {
                        Dt = new DataTable();
                    }
                    foreach (DataRow Drow in Dt.Rows)
                    {
                        if (Convert.ToInt32(Drow["RowNum"]) == 1)
                        {
                            MBalance = Convert.ToDecimal(Drow["DLoc_Amount"]) + Convert.ToDecimal(Drow["CLoc_Amount"]);
                            Mfor_Balane = Convert.ToDecimal(Drow["DEfor_Amount"]) + Convert.ToDecimal(Drow["CEfor_Amount"]);
                        }

                        if (Convert.ToInt32(Drow["RowNum"]) > 1)
                        {
                            MBalance += Convert.ToDecimal(Drow["DLoc_Amount"]) + Convert.ToDecimal(Drow["CLoc_Amount"]);
                            Mfor_Balane += Convert.ToDecimal(Drow["DEfor_Amount"]) + Convert.ToDecimal(Drow["CEfor_Amount"]);
                        }
                        Drow["Balance_Loc"] = MBalance;
                        Drow["Balance_For"] = Mfor_Balane;
                    }
                    //---------------
                    Grd_Vo_Details.DataSource = Dt;
                }

                catch
                { Grd_Vo_Details.DataSource = new DataTable(); }
            }
        }
        //---------------------------
        private void Btn_PrintAll_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Print_Details_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {
            if (Grd_Vo_Details.Rows.Count > 0)
            {
                Cust_DT = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl"].DefaultView.ToTable(false, "T_ID", "ACust_Name", "ECust_Name", "Min_DNrecDate", "Max_DNrecDate").Select("T_id = " + ((DataRowView)Term_Bs.Current).Row["T_id"]).CopyToDataTable();
                Assembl_Acc_DT_Rep = Assembl_Acc_DT.DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Eloc_Amount").Select("Acc_id = " + ((DataRowView)Assemble_Acc_Bs.Current).Row["Acc_id"]).CopyToDataTable();

                Details_Acc_DT_Rep = Details_Acc_DT.DefaultView.ToTable(false, "Cust_Id", connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name", connection.Lang_id == 1 ? "cur_AName" : "cur_EName", "DEFor_Amount", "CEFor_Amount", "Eloc_AMount").Select("Cust_id = " + ((DataRowView)Details_Acc_Bs.Current).Row["Cust_id"]).CopyToDataTable();

                Dt_Rep = Dt.DefaultView.ToTable(false, "DLoc_Amount", "CLoc_Amount", "Balance_loc", "Acc_Id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", connection.Lang_id == 1 ? "ACust_name" : "ECust_name",
                                            connection.Lang_id == 1 ? "AOper_name" : "EOper_name", "Real_Price", "DFor_Amount", "CFor_Amount",
                                           "Balance_For", "Vo_No", "Nrec_Date1", "C_Date", "Notes");
                DataGridView[] Export_GRD = { Grd_Terminals, Grd_Acc_Assembly, Grd_Acc_Details, Grd_Vo_Details };
                DataTable[] Export_DT = { Cust_DT, Assembl_Acc_DT_Rep, Details_Acc_DT_Rep, Dt_Rep };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
           
        }
        //---------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            
           // DataGridView GRD_Total = new DataGridView();
           // GRD_Total.Columns.Add("Column1", " ");
           // GRD_Total.Columns.Add("Column2", " ");
           // GRD_Total.Columns.Add("Column3", connection.Lang_id == 1 ? " المجمــوع:" : "Total:");
           // //GRD_Total.Columns.Add("Column21", connection.Lang_id == 1 ? " مديـــن" : "Debit");
           // //GRD_Total.Columns.Add("Column22", connection.Lang_id == 1 ? " دائـــن" : "Credit");
           // //GRD_Total.Columns.Add("Column23", connection.Lang_id == 1 ? " مديـــن" : "Debit");
           // //GRD_Total.Columns.Add("Column24", connection.Lang_id == 1 ? " دائـــن" : "Credit");
           // //GRD_Total.Columns.Add("Column25", connection.Lang_id == 1 ? " مديـــن" : "Debit");
           // //GRD_Total.Columns.Add("Column26", connection.Lang_id == 1 ? " دائـــن" : "Credit");

           // //DataTable Dt_Total = new DataTable();
           // //string[] Column = { "", "", "", "", "", "", "", "" };
           // //string[] DType = { "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String", "System.String" };
           // //Dt_Total = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
           // //Dt_Total.Rows.Add(new object[] { "", "",  TxtE_LocAmount.Text });
           //DataTable Dt_Total = new DataTable();
           // string[] Column = { "", "", "" };
           // string[] DType = { "System.String", "System.String", "System.String" };
           // Dt_Total = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
           // Dt_Total.Rows.Add(new object[] { "", "",  TxtE_LocAmount.Text });
            //DataGridView[] Export_GRD = { Date_Grd, Grd_Terminals, Grd_Acc_Assembly, GRD_Total };
            //DataTable[] Export_DT = { Date_Tbl, Cust_DT, Assembl_Acc_DT, Dt_Total };
           // Date();
            Cust_DT = connection.SQLDS.Tables["Reveal_Balance_Main_Tbl"].DefaultView.ToTable(false, "T_ID", "ACust_Name", "ECust_Name", "Min_DNrecDate", "Max_DNrecDate").Select("T_id = " + ((DataRowView)Term_Bs.Current).Row["T_id"]).CopyToDataTable();
            Assembl_Acc_DT_Rep = Assembl_Acc_DT.DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Eloc_Amount").Select().CopyToDataTable();
            DataGridView[] Export_GRD = {  Grd_Terminals, Grd_Acc_Assembly };
            DataTable[] Export_DT = { Cust_DT, Assembl_Acc_DT_Rep };
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            
        }
        //---------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Trial_Balance_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            TermChange = false;
            Assemble_AccChange = false;
            Details_AccChange = false;
            string[] Used_Tbl = { "Reveal_Balance_Main_Tbl", "Reveal_Balance_Main_Tbl1", 
                                     "Reveal_Balance_Main_Tbl2", "Reveal_Balance_Main_Tbl3","Trail_Balance_Tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //----------------------------
        //private void Date()
        //{

        //    Date_Grd = new DataGridView();
        //    Date_Tbl = new DataTable();
        //    Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
        //    Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

        //    string[] Column = { "From_Date", "To_Date" };
        //    string[] DType = { "System.String", "System.String" };
        //    Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
        //    Date_Tbl.Rows.Add(new object[] { TxtFromDate.Text, TxtToDate.Text });
        //}
    }
}