﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Report_Group_Add_Upd : Form
    {
        #region Defintion
        int Grp_Id = 0;
        bool change = false;
        bool GrdChange = false;
        DataTable Dt_Acc = new DataTable();
        DataTable Dt_Cust = new DataTable();
        DataTable Dt_Oper = new DataTable();
        string Filter = "";
        int Group_Id = 0;
        #endregion
        public Report_Group_Add_Upd()
        {
            InitializeComponent();
            connection.SQLBS.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Grp_Id.AutoGenerateColumns = false;
            Grd_Acc.AutoGenerateColumns = false;
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Oper.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["column7"].DataPropertyName = "Ecust_Name";
                Grd_Grp_Id.Columns["column14"].DataPropertyName = "Groups_EName";
                Grd_Acc.Columns["column3"].DataPropertyName = "Acc_Ename";
                Grd_Oper.Columns["column11"].DataPropertyName = "Eoper_Name";
            }
        }
        //----------------
        private void Report_Group_Add_Upd_Load(object sender, EventArgs e)
        {
            string SqlTxt = "SELECT Groups_id,Groups_AName,Groups_EName from Groups_Tbl";
            connection.SqlExec(SqlTxt, "Grp");


            if (connection.SQLDS.Tables["Grp"].Rows.Count > 0)
            {
                change = true;
                Txt_Grp_TextChanged(null, null);
            }

        }
        //-----------------------------------
        private void Txt_Grp_TextChanged(object sender, EventArgs e)
        {

            if (change)
            {
                try
                {
                    Grp_Id = 0;
                    if (int.TryParse(Txt_Grp.Text, out Grp_Id))
                        if (Txt_Grp.Text == "")
                            Grp_Id = 0;
                    Filter = " (Groups_AName like '" + Txt_Grp.Text + "%' or  Groups_EName like '" + Txt_Grp.Text + "%'"
                                 + " Or Groups_Id = " + Grp_Id + ")";
                    connection.SQLBS.DataSource = connection.SQLDS.Tables["Grp"].DefaultView.ToTable().Select(Filter).CopyToDataTable();
                    Grd_Grp_Id.DataSource = connection.SQLBS;
                    if (Grd_Grp_Id.Rows.Count > 0)
                    {
                        GrdChange = true;

                        Grd_Grp_Id_SelectionChanged(null, null);
                    }
                }
                catch
                {
                    Dt_Acc.Clear();
                    Dt_Cust.Clear();
                    Dt_Oper.Clear();
                    Grd_Grp_Id.DataSource = new DataTable();
                }
            }
        }

        //----------------------
        private void Grd_Grp_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                Group_Id = Convert.ToInt16(((DataRowView)connection.SQLBS.Current).Row["Groups_Id"]);
                connection.SqlExec("Exec Report_Group " + Group_Id, "Grp_Tbl");
                if (connection.SQLDS.Tables["Grp_Tbl"].Rows.Count > 0)
                {
                    Dt_Acc = connection.SQLDS.Tables["Grp_Tbl"];
                    Grd_Acc.DataSource = Dt_Acc;
                }
                if (connection.SQLDS.Tables["Grp_Tbl1"].Rows.Count > 0)
                {
                    Dt_Cust = connection.SQLDS.Tables["Grp_Tbl1"];
                    Grd_Cust.DataSource = Dt_Cust;
                }
                if (connection.SQLDS.Tables["Grp_Tbl2"].Rows.Count > 0)
                {
                    Dt_Oper = connection.SQLDS.Tables["Grp_Tbl2"];
                    Grd_Oper.DataSource = Dt_Oper;
                }

            }
            LblRec.Text = connection.Records(connection.SQLBS);
        }
        //--------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------------
        private void Report_Group_Add_Upd_FormClosed(object sender, FormClosedEventArgs e)
        {
            change = false;
            GrdChange = false;
            string[] Used_Tbl = { "Grp_Tbl", "Grp_Tbl1", "Grp_Tbl2", "Grp" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-----------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {

            if (Chk_Acc.Checked)
            {
                Dt_Acc = Dt_Acc.DefaultView.ToTable(false, "Acc_Id", "Chk").Select("Chk>0").CopyToDataTable();
            }

            else
            {
                Dt_Acc = new DataTable();
                Dt_Acc.Columns.Add("Acc_Id");
                Dt_Acc.Columns.Add("Chk");
            }
            if (Chk_Cust.Checked)
            {
                Dt_Cust = Dt_Cust.DefaultView.ToTable(false, "Cust_Id", "Chk").Select("Chk>0").CopyToDataTable();
            }
            else
            {
                Dt_Cust = new DataTable();
                Dt_Cust.Columns.Add("Cust_Id");
                Dt_Cust.Columns.Add("Chk");
            }
            if (Oper_Check.Checked)
            {
                Dt_Oper = Dt_Oper.DefaultView.ToTable(false, "oper_Id", "Chk").Select("Chk>0").CopyToDataTable();
            }
            else
            {
                Dt_Oper = new DataTable();
                Dt_Oper.Columns.Add("Oper_Id");
                Dt_Oper.Columns.Add("Chk");
            }
            connection.SQLCMD.Parameters.AddWithValue("@Dt_Acc", Dt_Acc);
            connection.SQLCMD.Parameters.AddWithValue("@Dt_Cust", Dt_Cust);
            connection.SQLCMD.Parameters.AddWithValue("@Dt_Oper", Dt_Oper);
            connection.SQLCMD.Parameters.AddWithValue("@Groups_Id", Group_Id);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Report_Group_Add_Upd", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Dt_Acc.Clear();
            Dt_Cust.Clear();
            Dt_Oper.Clear();
            Report_Group_Add_Upd_Load(null, null);
        }

        private void Chk_Acc_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Acc.Checked)
            {
                Grd_Acc.Enabled = true;
                Chk_Acc_All.Enabled = true;
            }
            else
            {
                Grd_Acc.Enabled = false;
                Chk_Acc_All.Enabled = false;
            }
        }
        //---------
        private void Chk_Cust_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cust.Checked)
            {
                Grd_Cust.Enabled = true;
                Chk_Cust_All.Enabled = true;
            }
            else
            {
                Grd_Cust.Enabled = false;
                Chk_Cust_All.Enabled = false;
            }
        }
        //---------
        private void Chk_Oper_CheckedChanged(object sender, EventArgs e)
        {
            if (Oper_Check.Checked)
            {
                Grd_Oper.Enabled = true;
                Chk_Oper_All.Enabled = true;
            }
            else
            {
                Grd_Oper.Enabled = false;
                Chk_Oper_All.Enabled = false;
            }
        }
        //------------
        private void Chk_Acc_All_MouseClick(object sender, MouseEventArgs e)
        {
            if (Chk_Acc_All.Checked)
                Column1.HeaderText = connection.Lang_id == 1 ? "الغاء التأشير" : "Remove Chk.";
            else
                Column1.HeaderText = connection.Lang_id == 1 ? "تاشير الكل" : "Check All";
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Acc.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

            Grd_Acc.RefreshEdit();
        }
        //----------
        private void Chk_Cust_All_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cust_All.Checked)
                Column1.HeaderText = connection.Lang_id == 1 ? "الغاء التأشير" : "Remove Chk.";
            else
                Column1.HeaderText = connection.Lang_id == 1 ? "تاشير الكل" : "Check All";
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Cust.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column5"]).Value = HCheckBox.Checked;

            Grd_Cust.RefreshEdit();
        }
        //---------
        private void Chk_Oper_All_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Oper_All.Checked)
                Column1.HeaderText = connection.Lang_id == 1 ? "الغاء التأشير" : "Remove Chk.";
            else
                Column1.HeaderText = connection.Lang_id == 1 ? "تاشير الكل" : "Check All";
          
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Oper.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column9"]).Value = HCheckBox.Checked;

            Grd_Oper.RefreshEdit();
        }

      

    }
}