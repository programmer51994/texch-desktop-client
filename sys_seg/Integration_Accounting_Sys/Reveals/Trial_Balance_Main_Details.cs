﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;
using System.Drawing;

namespace Integration_Accounting_Sys
{
    public partial class Trial_Balance_Main_Details : Form
    {
        #region Definshion
        bool TermChange = false;
        bool Details_AccChange = false;
        bool Details_AccChange2 = false;
        bool details_accchange3 = false;
        BindingSource Term_Bs = new BindingSource();
        BindingSource Assemble_Acc_Bs = new BindingSource();
        BindingSource Details_Acc_Bs = new BindingSource();
        public static BindingSource Details_Acc_Bs2 = new BindingSource();
        BindingSource details3 = new BindingSource();

        DataTable Assembl_Acc_DT = new DataTable();
        DataTable Details_Acc_DT = new DataTable();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataTable Trail_Balance = new DataTable();
        DataTable Trail_Balance1 = new DataTable();
        DataTable Trail_Balance2 = new DataTable();
        DataTable Dt = new DataTable();
        DataTable Dt2 = new DataTable();
        DataTable Dt3 = new DataTable();
        public static int T_Id = 0;
        public static string Acc_no = "" ;
        public static int Acc_Id = 0;
        public static int Cust_id = 0;
        public static int VO_NO = 0;
     
        public static string AOPER_NAME = "";
        public static string Create_Nrec_date = "";
        public static int For_Cur_Id = 0;
        public static string OD_LocAmount = "";
        public static string OC_LocAmount = "";
        public static string PD_LocAmount = "";
        public static string PC_LocAmount = "";
        public static string ED_LocAmount = "";
        public static string EC_LocAmount = "";
        #endregion
        //---------------------------
        public Trial_Balance_Main_Details(string FromDate, string ToDate)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
          connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Terminals.AutoGenerateColumns = false;
            Grd_Acc_Details.AutoGenerateColumns = false;
            Grd_Vo_Details.AutoGenerateColumns = false;
            Grd_SumAmount_Acc.AutoGenerateColumns = false;
            Grd_vo_details2.AutoGenerateColumns = false;
            if (Convert.ToInt32(FromDate) != 0)
            {
                TxtFromDate.Text = FromDate;
            }
            else
            {
                TxtFromDate.Text = "00/00/0000";
            }
            if (Convert.ToInt32(ToDate) != 0)
            {
                TxtToDate.Text = ToDate;
            }
            else
            {
                TxtToDate.Text = "00/00/0000";
            }
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Column7.DataPropertyName = "Acc_Ename";
                //Column26.DataPropertyName = "ACC_Ename";
                DataGridViewTextBoxColumn1.DataPropertyName = "Cur_Ename";
                DataGridViewTextBoxColumn2.DataPropertyName = "Ecust_Name";
                DataGridViewTextBoxColumn3.DataPropertyName = "Eoper_name";
                Column38.DataPropertyName = "Acc_Ename";
                
            }
            #endregion
        }
        //---------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {
            TermChange = false;
            Details_AccChange = false;
            Details_AccChange2 = false;
            Term_Bs.DataSource = connection.SQLDS.Tables["Trail_Balance_Tbl"];
            Grd_Terminals.DataSource = Term_Bs;
            if (connection.SQLDS.Tables["Trail_Balance_Tbl"].Rows.Count > 0)
            {
                TermChange = true;
                Grd_Terminals_SelectionChanged(sender, e);
            }

            if (connection.Lang_id != 1)
            {
                Column7.DataPropertyName = "Acc_Ename";
                Column32.DataPropertyName = "Ecust_Name";
                DataGridViewTextBoxColumn2.DataPropertyName = "Ecust_Name";
                DataGridViewTextBoxColumn3.DataPropertyName = "EOPER_NAME";
                DataGridViewTextBoxColumn1.DataPropertyName = "Cur_Ename";
                Column20.DataPropertyName = "Ecust_Name";              
                Column21.DataPropertyName = "EOPER_NAME";
                Column27.DataPropertyName = "Cur_Ename";
            }

                  
        }
        //---------------------------
        private void Grd_Terminals_SelectionChanged(object sender, EventArgs e)
        {
            if (TermChange)
            {

                Assembl_Acc_DT = new DataTable();
                T_Id = Convert.ToInt32(((DataRowView)Term_Bs.Current).Row["T_Id"]);
                Details_Acc_DT = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("T_Id = " + T_Id ).CopyToDataTable();
                DataView dv = Details_Acc_DT.DefaultView;
                dv.Sort = "Acc_no asc";
                Details_Acc_DT = dv.ToTable();


                //+ " And Dot_Acc_No Like '" + Dot_Acc_No + "%'").CopyToDataTable();

                //var query = from rec in Details_Acc_DT.AsEnumerable()
                //            select new
                //            {
                //                Acc_Aname = rec["Acc_Aname"],
                //                Acc_Ename = rec["Acc_Ename"],
                //                Acc_Id = rec["Acc_Id"],
                //                DOLoc_Amount = Convert.ToDecimal(rec["Oloc_Amount"]) > 0 ? Convert.ToDecimal(rec["Oloc_Amount"]) : 0,
                //                COLoc_Amount = Convert.ToDecimal(rec["Oloc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["Oloc_Amount"])),

                //                DPLoc_Amount = Convert.ToDecimal(rec["PLoc_Amount"]) > 0 ? Convert.ToDecimal(rec["PLoc_Amount"]) : 0,
                //                CPLoc_Amount = Convert.ToDecimal(rec["PLoc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["PLoc_Amount"])),

                //                DELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"]) > 0 ? Convert.ToDecimal(rec["ELoc_Amount"]) : 0,
                //                CELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["ELoc_Amount"]))
                //            };
                //Details_Acc_DT = CustomControls.IEnumerableToDataTable(query);
                Details_Acc_Bs.DataSource = Details_Acc_DT;
                Grd_Acc_Details.DataSource = Details_Acc_Bs;

                OD_LocAmount = Details_Acc_DT.Compute("Sum(DOloc_Amount)", "").ToString();
                TxtOD_LocAmount2.Text = OD_LocAmount;
                OC_LocAmount = Details_Acc_DT.Compute("Sum(COloc_Amount)", "").ToString();
                TxtOC_LocAmount2.Text = OC_LocAmount;
                PD_LocAmount = Details_Acc_DT.Compute("Sum(DPloc_Amount)", "").ToString();
                TxtPD_LocAmount2.Text = PD_LocAmount;
                PC_LocAmount = Details_Acc_DT.Compute("Sum(CPloc_Amount)", "").ToString();
                TxtPC_LocAmount2.Text = PC_LocAmount;
                ED_LocAmount = Details_Acc_DT.Compute("Sum(DEloc_Amount)", "").ToString();
                TxtED_LocAmount2.Text = ED_LocAmount;
                EC_LocAmount = Details_Acc_DT.Compute("Sum(CEloc_Amount)", "").ToString();
                TxtEC_LocAmount2.Text = EC_LocAmount;

                if (Details_Acc_Bs.List.Count > 0)
                {
                    Details_AccChange = true;
                    Grd_Acc_Details_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void Grd_Acc_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (Details_AccChange)
            {
                Acc_Id = Convert.ToInt32(((DataRowView)Details_Acc_Bs.Current).Row["Acc_Id"]);
                Acc_no = ((DataRowView)Details_Acc_Bs.Current).Row["Acc_no"].ToString();
                //---------------------------------------
                //decimal MBalance = 0;

                try
                {
                    Dt = connection.SQLDS.Tables["Trail_Balance_Tbl2"].Select("T_id = " + T_Id + " And Acc_Id = " + Acc_Id).CopyToDataTable();
                }
                catch
                {
                    Dt = new DataTable();

                    Grd_Vo_Details.DataSource = new DataTable();
                    Grd_vo_details2.DataSource = new DataTable();
                   
                   
                }
                //foreach (DataRow Drow in Dt.Rows)
                //{
                //    if (Convert.ToInt32(Drow["RowNum"]) == 1)
                //    {
                //        MBalance = Convert.ToDecimal(Drow["DLoc_Amount"]) + Convert.ToDecimal(Drow["CLoc_Amount"]);
                //    }

                //    if (Convert.ToInt32(Drow["RowNum"]) > 1)
                //    {
                //        MBalance += Convert.ToDecimal(Drow["DLoc_Amount"]) + Convert.ToDecimal(Drow["CLoc_Amount"]);
                //    }
                //    Drow["Balance_For"] = MBalance;
                //    //Drow["CLoc_Amount"] = Math.Abs(Convert.ToDecimal(Drow["CLoc_Amount"]));
                //}
                //---------------
                Details_Acc_Bs2.DataSource = Dt;
                Grd_SumAmount_Acc.DataSource = Details_Acc_Bs2;
               
                if (Dt.Rows.Count > 0)
                {
                    Details_AccChange2 = true;
                    Grd_SumAmount_Acc_SelectionChanged(sender, e);
                  //  Details_AccChange2 = false;
                }
            }
        }
        //---------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Trial_Balance_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            TermChange = false;
            Details_AccChange = false;
            Details_AccChange2 = false;
            details_accchange3 = false;

            string[] Used_Tbl = { "Trail_Balance_Tbl", "Trail_Balance_Tbl1", 
                                     "Trail_Balance_Tbl2", "Trail_Balance_Tbl3"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //--------------------------------------------------------
        private void Grd_SumAmount_Acc_SelectionChanged(object sender, EventArgs e)
        {
            if (Details_AccChange2)
            {
                

                try
                {
                    Cust_id = Convert.ToInt32(((DataRowView)Details_Acc_Bs2.Current).Row["Cust_id"]);
                    For_Cur_Id = Convert.ToInt32(((DataRowView)Details_Acc_Bs2.Current).Row["for_Cur_id"]);
                    Dt2 = connection.SQLDS.Tables["Trail_Balance_Tbl3"].Select("T_id = " + T_Id + " And Acc_Id = " + Acc_Id + " And Cust_Id = " + Cust_id + " And for_cur_id = " + For_Cur_Id).CopyToDataTable();

                    Txt_SumDloc.Text = connection.SQLDS.Tables["Trail_Balance_Tbl2"].Select("T_id = " + T_Id + " And Acc_Id = " + Acc_Id).CopyToDataTable().Compute("Sum(Dloc_Amount)", "").ToString();
                    Txt_SumCloc.Text = connection.SQLDS.Tables["Trail_Balance_Tbl2"].Select("T_id = " + T_Id + " And Acc_Id = " + Acc_Id).CopyToDataTable().Compute("Sum(Cloc_Amount)", "").ToString();
                    Txt_SumDFor.Text = connection.SQLDS.Tables["Trail_Balance_Tbl2"].Select("T_id = " + T_Id + " And Acc_Id = " + Acc_Id).CopyToDataTable().Compute("Sum(DFor_Amount)", "").ToString();
                    Txt_SumCfor.Text = connection.SQLDS.Tables["Trail_Balance_Tbl2"].Select("T_id = " + T_Id + " And Acc_Id = " + Acc_Id).CopyToDataTable().Compute("Sum(CFor_Amount)", "").ToString();
                }
                catch
                {
                    Dt2 = new DataTable();
                }

                details3.DataSource = Dt2;
                Grd_Vo_Details.DataSource = details3;

            }
            if (Dt2.Rows.Count > 0)
            {
                details_accchange3 = true;
                Grd_Vo_Details_SelectionChanged(sender, e);
            }
          
        }
        //--------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            Trail_Balance_PrintExport Frm = new Trail_Balance_PrintExport();
            Frm.Dgv1 = Grd_Terminals;
            Frm.Dgv2 = Grd_Acc_Details;
            Frm.Dgv3 = Grd_SumAmount_Acc;
            Frm.Dgv4 = Grd_Vo_Details;
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;
        }

        private void Grd_Vo_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (details_accchange3)
            {
              
                try
                {
                    VO_NO = Convert.ToInt32(((DataRowView)details3.Current).Row["VO_NO"]);
                    Create_Nrec_date = Convert.ToString(((DataRowView)details3.Current).Row["Create_Nrec_date"]);
                    AOPER_NAME = Convert.ToString(((DataRowView)details3.Current).Row["AOPER_NAME"]).ToString();
                   
                 

                    Dt3 = connection.SQLDS.Tables["Trail_Balance_Tbl3"].Select("T_id = " + T_Id + " And VO_NO = " + VO_NO + " And Create_Nrec_date like'" + Create_Nrec_date + "' And AOPER_NAME like'" + AOPER_NAME+"'").CopyToDataTable();

                   
                }
                catch
                {
                    Dt3 = new DataTable();
                }

                Grd_vo_details2.DataSource = Dt3;

            }

        }

        private void Grd_vo_details2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow row in Grd_vo_details2.Rows)
                if (Convert.ToInt32(row.Cells[13].Value) == Convert.ToInt32(Cust_id) && Convert.ToInt32(row.Cells[14].Value) == Convert.ToInt32(Acc_Id))
            {
               
                row.DefaultCellStyle.BackColor = Color.LightGreen;
            }
        }
    }
}