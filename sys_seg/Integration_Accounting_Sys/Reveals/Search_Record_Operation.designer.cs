﻿namespace Integration_Accounting_Sys
{
    partial class Search_Record_Operation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.Cust_Button4 = new System.Windows.Forms.Button();
            this.Cust_Button3 = new System.Windows.Forms.Button();
            this.Cust_Button2 = new System.Windows.Forms.Button();
            this.Cust_Button1 = new System.Windows.Forms.Button();
            this.Oper_Button4 = new System.Windows.Forms.Button();
            this.Oper_Button3 = new System.Windows.Forms.Button();
            this.Oper_Button2 = new System.Windows.Forms.Button();
            this.Oper_Button1 = new System.Windows.Forms.Button();
            this.TxtCust_Name = new System.Windows.Forms.TextBox();
            this.TxtOper_Name = new System.Windows.Forms.TextBox();
            this.Class_Button4 = new System.Windows.Forms.Button();
            this.Class_Button3 = new System.Windows.Forms.Button();
            this.Class_Button2 = new System.Windows.Forms.Button();
            this.Class_Button1 = new System.Windows.Forms.Button();
            this.TxtClass_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.Oper_Check = new System.Windows.Forms.CheckBox();
            this.Class_Check = new System.Windows.Forms.CheckBox();
            this.Cust_Check = new System.Windows.Forms.CheckBox();
            this.Grd_Class_1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Oper_1 = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_1 = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_2 = new System.Windows.Forms.DataGridView();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Oper_2 = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Class_2 = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrdUser_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrdUser_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User_Check = new System.Windows.Forms.CheckBox();
            this.User_Button4 = new System.Windows.Forms.Button();
            this.User_Button3 = new System.Windows.Forms.Button();
            this.User_Button2 = new System.Windows.Forms.Button();
            this.User_Button1 = new System.Windows.Forms.Button();
            this.Txt_User = new System.Windows.Forms.TextBox();
            this.Txt_Max_Vo_no = new Integration_Accounting_Sys.NumericTextBox();
            this.Txt_Min_Vo_no = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.Grd_years = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_year = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Class_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Oper_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Oper_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Class_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdUser_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdUser_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(125, 2);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(227, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(629, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(107, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(574, 6);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 526;
            this.label2.Text = "التاريـخ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(17, 6);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 14);
            this.label1.TabIndex = 525;
            this.label1.Text = "اسم المستخــدم:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(4, 153);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(732, 1);
            this.flowLayoutPanel2.TabIndex = 529;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 27);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 1);
            this.flowLayoutPanel1.TabIndex = 530;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(20, 111);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(89, 14);
            this.label3.TabIndex = 531;
            this.label3.Text = "التاريـــــــخ من:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(225, 111);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 533;
            this.label5.Text = "إلــــى :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(225, 134);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(48, 14);
            this.label8.TabIndex = 538;
            this.label8.Text = "إلــــى :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(20, 134);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(89, 14);
            this.label6.TabIndex = 536;
            this.label6.Text = "رقم القيد من :";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(4, 634);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(732, 1);
            this.flowLayoutPanel3.TabIndex = 530;
            // 
            // Cust_Button4
            // 
            this.Cust_Button4.Enabled = false;
            this.Cust_Button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Cust_Button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Cust_Button4.Location = new System.Drawing.Point(387, 487);
            this.Cust_Button4.Name = "Cust_Button4";
            this.Cust_Button4.Size = new System.Drawing.Size(68, 22);
            this.Cust_Button4.TabIndex = 28;
            this.Cust_Button4.Text = "<<";
            this.Cust_Button4.UseVisualStyleBackColor = true;
            this.Cust_Button4.Click += new System.EventHandler(this.Cust_Button4_Click);
            // 
            // Cust_Button3
            // 
            this.Cust_Button3.Enabled = false;
            this.Cust_Button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Cust_Button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Cust_Button3.Location = new System.Drawing.Point(387, 465);
            this.Cust_Button3.Name = "Cust_Button3";
            this.Cust_Button3.Size = new System.Drawing.Size(68, 22);
            this.Cust_Button3.TabIndex = 27;
            this.Cust_Button3.Text = "<";
            this.Cust_Button3.UseVisualStyleBackColor = true;
            this.Cust_Button3.Click += new System.EventHandler(this.Cust_Button3_Click);
            // 
            // Cust_Button2
            // 
            this.Cust_Button2.Enabled = false;
            this.Cust_Button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Cust_Button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Cust_Button2.Location = new System.Drawing.Point(387, 444);
            this.Cust_Button2.Name = "Cust_Button2";
            this.Cust_Button2.Size = new System.Drawing.Size(68, 22);
            this.Cust_Button2.TabIndex = 26;
            this.Cust_Button2.Text = ">>";
            this.Cust_Button2.UseVisualStyleBackColor = true;
            this.Cust_Button2.Click += new System.EventHandler(this.Cust_Button2_Click);
            // 
            // Cust_Button1
            // 
            this.Cust_Button1.Enabled = false;
            this.Cust_Button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Cust_Button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Cust_Button1.Location = new System.Drawing.Point(387, 422);
            this.Cust_Button1.Name = "Cust_Button1";
            this.Cust_Button1.Size = new System.Drawing.Size(68, 22);
            this.Cust_Button1.TabIndex = 25;
            this.Cust_Button1.Text = ">";
            this.Cust_Button1.UseVisualStyleBackColor = true;
            this.Cust_Button1.Click += new System.EventHandler(this.Cust_Button1_Click);
            // 
            // Oper_Button4
            // 
            this.Oper_Button4.Enabled = false;
            this.Oper_Button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Oper_Button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Oper_Button4.Location = new System.Drawing.Point(387, 366);
            this.Oper_Button4.Name = "Oper_Button4";
            this.Oper_Button4.Size = new System.Drawing.Size(68, 22);
            this.Oper_Button4.TabIndex = 20;
            this.Oper_Button4.Text = "<<";
            this.Oper_Button4.UseVisualStyleBackColor = true;
            this.Oper_Button4.Click += new System.EventHandler(this.Oper_Button4_Click);
            // 
            // Oper_Button3
            // 
            this.Oper_Button3.Enabled = false;
            this.Oper_Button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Oper_Button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Oper_Button3.Location = new System.Drawing.Point(387, 345);
            this.Oper_Button3.Name = "Oper_Button3";
            this.Oper_Button3.Size = new System.Drawing.Size(68, 22);
            this.Oper_Button3.TabIndex = 19;
            this.Oper_Button3.Text = "<";
            this.Oper_Button3.UseVisualStyleBackColor = true;
            this.Oper_Button3.Click += new System.EventHandler(this.Oper_Button3_Click);
            // 
            // Oper_Button2
            // 
            this.Oper_Button2.Enabled = false;
            this.Oper_Button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Oper_Button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Oper_Button2.Location = new System.Drawing.Point(387, 324);
            this.Oper_Button2.Name = "Oper_Button2";
            this.Oper_Button2.Size = new System.Drawing.Size(68, 22);
            this.Oper_Button2.TabIndex = 18;
            this.Oper_Button2.Text = ">>";
            this.Oper_Button2.UseVisualStyleBackColor = true;
            this.Oper_Button2.Click += new System.EventHandler(this.Oper_Button2_Click);
            // 
            // Oper_Button1
            // 
            this.Oper_Button1.Enabled = false;
            this.Oper_Button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Oper_Button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Oper_Button1.Location = new System.Drawing.Point(387, 303);
            this.Oper_Button1.Name = "Oper_Button1";
            this.Oper_Button1.Size = new System.Drawing.Size(68, 22);
            this.Oper_Button1.TabIndex = 17;
            this.Oper_Button1.Text = ">";
            this.Oper_Button1.UseVisualStyleBackColor = true;
            this.Oper_Button1.Click += new System.EventHandler(this.Oper_Button1_Click);
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.BackColor = System.Drawing.Color.White;
            this.TxtCust_Name.Enabled = false;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCust_Name.Location = new System.Drawing.Point(109, 397);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(261, 22);
            this.TxtCust_Name.TabIndex = 23;
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Name_TextChanged);
            // 
            // TxtOper_Name
            // 
            this.TxtOper_Name.BackColor = System.Drawing.Color.White;
            this.TxtOper_Name.Enabled = false;
            this.TxtOper_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtOper_Name.Location = new System.Drawing.Point(108, 276);
            this.TxtOper_Name.Name = "TxtOper_Name";
            this.TxtOper_Name.Size = new System.Drawing.Size(261, 22);
            this.TxtOper_Name.TabIndex = 15;
            this.TxtOper_Name.TextChanged += new System.EventHandler(this.TxtOper_Name_TextChanged);
            // 
            // Class_Button4
            // 
            this.Class_Button4.Enabled = false;
            this.Class_Button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Class_Button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Class_Button4.Location = new System.Drawing.Point(387, 252);
            this.Class_Button4.Name = "Class_Button4";
            this.Class_Button4.Size = new System.Drawing.Size(68, 22);
            this.Class_Button4.TabIndex = 12;
            this.Class_Button4.Text = "<<";
            this.Class_Button4.UseVisualStyleBackColor = true;
            this.Class_Button4.Click += new System.EventHandler(this.Class_Button4_Click);
            // 
            // Class_Button3
            // 
            this.Class_Button3.Enabled = false;
            this.Class_Button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Class_Button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Class_Button3.Location = new System.Drawing.Point(387, 230);
            this.Class_Button3.Name = "Class_Button3";
            this.Class_Button3.Size = new System.Drawing.Size(68, 22);
            this.Class_Button3.TabIndex = 11;
            this.Class_Button3.Text = "<";
            this.Class_Button3.UseVisualStyleBackColor = true;
            this.Class_Button3.Click += new System.EventHandler(this.Class_Button3_Click);
            // 
            // Class_Button2
            // 
            this.Class_Button2.Enabled = false;
            this.Class_Button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Class_Button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Class_Button2.Location = new System.Drawing.Point(387, 208);
            this.Class_Button2.Name = "Class_Button2";
            this.Class_Button2.Size = new System.Drawing.Size(68, 22);
            this.Class_Button2.TabIndex = 10;
            this.Class_Button2.Text = ">>";
            this.Class_Button2.UseVisualStyleBackColor = true;
            this.Class_Button2.Click += new System.EventHandler(this.Class_Button2_Click);
            // 
            // Class_Button1
            // 
            this.Class_Button1.Enabled = false;
            this.Class_Button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Class_Button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Class_Button1.Location = new System.Drawing.Point(387, 186);
            this.Class_Button1.Name = "Class_Button1";
            this.Class_Button1.Size = new System.Drawing.Size(68, 22);
            this.Class_Button1.TabIndex = 9;
            this.Class_Button1.Text = ">";
            this.Class_Button1.UseVisualStyleBackColor = true;
            this.Class_Button1.Click += new System.EventHandler(this.Class_Button1_Click);
            // 
            // TxtClass_Name
            // 
            this.TxtClass_Name.BackColor = System.Drawing.Color.White;
            this.TxtClass_Name.Enabled = false;
            this.TxtClass_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtClass_Name.Location = new System.Drawing.Point(108, 159);
            this.TxtClass_Name.Name = "TxtClass_Name";
            this.TxtClass_Name.Size = new System.Drawing.Size(261, 22);
            this.TxtClass_Name.TabIndex = 7;
            this.TxtClass_Name.TextChanged += new System.EventHandler(this.TxtClass_Name_TextChanged);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(4, 154);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 480);
            this.flowLayoutPanel4.TabIndex = 531;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(735, 154);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 481);
            this.flowLayoutPanel5.TabIndex = 532;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(282, 638);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 27);
            this.AddBtn.TabIndex = 30;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(370, 638);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 27);
            this.ExtBtn.TabIndex = 31;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // Oper_Check
            // 
            this.Oper_Check.AutoSize = true;
            this.Oper_Check.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Oper_Check.ForeColor = System.Drawing.Color.Navy;
            this.Oper_Check.Location = new System.Drawing.Point(11, 278);
            this.Oper_Check.Name = "Oper_Check";
            this.Oper_Check.Size = new System.Drawing.Size(83, 18);
            this.Oper_Check.TabIndex = 14;
            this.Oper_Check.Text = "الــعــملية:";
            this.Oper_Check.UseVisualStyleBackColor = true;
            this.Oper_Check.CheckedChanged += new System.EventHandler(this.Oper_Check_CheckedChanged);
            // 
            // Class_Check
            // 
            this.Class_Check.AutoSize = true;
            this.Class_Check.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Class_Check.ForeColor = System.Drawing.Color.Navy;
            this.Class_Check.Location = new System.Drawing.Point(10, 161);
            this.Class_Check.Name = "Class_Check";
            this.Class_Check.Size = new System.Drawing.Size(82, 18);
            this.Class_Check.TabIndex = 6;
            this.Class_Check.Text = "الصــنف  :";
            this.Class_Check.UseVisualStyleBackColor = true;
            this.Class_Check.CheckedChanged += new System.EventHandler(this.Class_Check_CheckedChanged);
            // 
            // Cust_Check
            // 
            this.Cust_Check.AutoSize = true;
            this.Cust_Check.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Cust_Check.ForeColor = System.Drawing.Color.Navy;
            this.Cust_Check.Location = new System.Drawing.Point(11, 399);
            this.Cust_Check.Name = "Cust_Check";
            this.Cust_Check.Size = new System.Drawing.Size(89, 18);
            this.Cust_Check.TabIndex = 22;
            this.Cust_Check.Text = "الثـــــانوي :";
            this.Cust_Check.UseVisualStyleBackColor = true;
            this.Cust_Check.CheckedChanged += new System.EventHandler(this.Cust_Check_CheckedChanged);
            // 
            // Grd_Class_1
            // 
            this.Grd_Class_1.AllowUserToAddRows = false;
            this.Grd_Class_1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Class_1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Class_1.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Class_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Class_1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Class_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Class_1.ColumnHeadersHeight = 45;
            this.Grd_Class_1.ColumnHeadersVisible = false;
            this.Grd_Class_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.Grd_Class_1.Location = new System.Drawing.Point(109, 181);
            this.Grd_Class_1.Name = "Grd_Class_1";
            this.Grd_Class_1.ReadOnly = true;
            this.Grd_Class_1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Class_1.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Class_1.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Class_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Class_1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Class_1.Size = new System.Drawing.Size(261, 91);
            this.Grd_Class_1.TabIndex = 8;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Class_Id";
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "AClass_name";
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 205;
            // 
            // Grd_Oper_1
            // 
            this.Grd_Oper_1.AllowUserToAddRows = false;
            this.Grd_Oper_1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Oper_1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Oper_1.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Oper_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Oper_1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Oper_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Oper_1.ColumnHeadersHeight = 45;
            this.Grd_Oper_1.ColumnHeadersVisible = false;
            this.Grd_Oper_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column6});
            this.Grd_Oper_1.Location = new System.Drawing.Point(109, 298);
            this.Grd_Oper_1.Name = "Grd_Oper_1";
            this.Grd_Oper_1.ReadOnly = true;
            this.Grd_Oper_1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Oper_1.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Oper_1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Oper_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Oper_1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Oper_1.Size = new System.Drawing.Size(261, 93);
            this.Grd_Oper_1.TabIndex = 16;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Oper_id";
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 50;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Aoper_name";
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 205;
            // 
            // Grd_Cust_1
            // 
            this.Grd_Cust_1.AllowUserToAddRows = false;
            this.Grd_Cust_1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Cust_1.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust_1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Cust_1.ColumnHeadersHeight = 45;
            this.Grd_Cust_1.ColumnHeadersVisible = false;
            this.Grd_Cust_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column10});
            this.Grd_Cust_1.Location = new System.Drawing.Point(109, 419);
            this.Grd_Cust_1.Name = "Grd_Cust_1";
            this.Grd_Cust_1.ReadOnly = true;
            this.Grd_Cust_1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust_1.RowHeadersVisible = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_1.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust_1.Size = new System.Drawing.Size(261, 92);
            this.Grd_Cust_1.TabIndex = 24;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "T_id";
            this.Column9.HeaderText = "Column9";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 50;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Acust_Name";
            this.Column10.HeaderText = "Column10";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 205;
            // 
            // Grd_Cust_2
            // 
            this.Grd_Cust_2.AllowUserToAddRows = false;
            this.Grd_Cust_2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Cust_2.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust_2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Cust_2.ColumnHeadersHeight = 45;
            this.Grd_Cust_2.ColumnHeadersVisible = false;
            this.Grd_Cust_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column11,
            this.Column12});
            this.Grd_Cust_2.Location = new System.Drawing.Point(468, 420);
            this.Grd_Cust_2.Name = "Grd_Cust_2";
            this.Grd_Cust_2.ReadOnly = true;
            this.Grd_Cust_2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust_2.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_2.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Cust_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust_2.Size = new System.Drawing.Size(261, 91);
            this.Grd_Cust_2.TabIndex = 29;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "T_id";
            this.Column11.HeaderText = "Column11";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 50;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Cust_name";
            this.Column12.HeaderText = "Column12";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 205;
            // 
            // Grd_Oper_2
            // 
            this.Grd_Oper_2.AllowUserToAddRows = false;
            this.Grd_Oper_2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Oper_2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_Oper_2.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Oper_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Oper_2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Oper_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Oper_2.ColumnHeadersHeight = 45;
            this.Grd_Oper_2.ColumnHeadersVisible = false;
            this.Grd_Oper_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8});
            this.Grd_Oper_2.Location = new System.Drawing.Point(468, 301);
            this.Grd_Oper_2.Name = "Grd_Oper_2";
            this.Grd_Oper_2.ReadOnly = true;
            this.Grd_Oper_2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Oper_2.RowHeadersVisible = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Oper_2.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_Oper_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Oper_2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Oper_2.Size = new System.Drawing.Size(261, 90);
            this.Grd_Oper_2.TabIndex = 21;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Oper_id";
            this.Column7.HeaderText = "Column7";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Oper_name";
            this.Column8.HeaderText = "Column8";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 205;
            // 
            // Grd_Class_2
            // 
            this.Grd_Class_2.AllowUserToAddRows = false;
            this.Grd_Class_2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Class_2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_Class_2.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Class_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Class_2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Class_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_Class_2.ColumnHeadersHeight = 45;
            this.Grd_Class_2.ColumnHeadersVisible = false;
            this.Grd_Class_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column4});
            this.Grd_Class_2.Location = new System.Drawing.Point(468, 181);
            this.Grd_Class_2.Name = "Grd_Class_2";
            this.Grd_Class_2.ReadOnly = true;
            this.Grd_Class_2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Class_2.RowHeadersVisible = false;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Class_2.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_Class_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Class_2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Class_2.Size = new System.Drawing.Size(261, 91);
            this.Grd_Class_2.TabIndex = 13;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Class_id";
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Class_Name";
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 205;
            // 
            // GrdUser_2
            // 
            this.GrdUser_2.AllowUserToAddRows = false;
            this.GrdUser_2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdUser_2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.GrdUser_2.BackgroundColor = System.Drawing.Color.White;
            this.GrdUser_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GrdUser_2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdUser_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.GrdUser_2.ColumnHeadersHeight = 45;
            this.GrdUser_2.ColumnHeadersVisible = false;
            this.GrdUser_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.GrdUser_2.Location = new System.Drawing.Point(467, 539);
            this.GrdUser_2.Name = "GrdUser_2";
            this.GrdUser_2.ReadOnly = true;
            this.GrdUser_2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.GrdUser_2.RowHeadersVisible = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdUser_2.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.GrdUser_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrdUser_2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdUser_2.Size = new System.Drawing.Size(261, 91);
            this.GrdUser_2.TabIndex = 548;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Buser_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "user_name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 205;
            // 
            // GrdUser_1
            // 
            this.GrdUser_1.AllowUserToAddRows = false;
            this.GrdUser_1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdUser_1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            this.GrdUser_1.BackgroundColor = System.Drawing.Color.White;
            this.GrdUser_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GrdUser_1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdUser_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.GrdUser_1.ColumnHeadersHeight = 45;
            this.GrdUser_1.ColumnHeadersVisible = false;
            this.GrdUser_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.GrdUser_1.Location = new System.Drawing.Point(108, 538);
            this.GrdUser_1.Name = "GrdUser_1";
            this.GrdUser_1.ReadOnly = true;
            this.GrdUser_1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.GrdUser_1.RowHeadersVisible = false;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdUser_1.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.GrdUser_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrdUser_1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdUser_1.Size = new System.Drawing.Size(261, 92);
            this.GrdUser_1.TabIndex = 543;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BUser_id";
            this.dataGridViewTextBoxColumn3.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 50;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "User_name";
            this.dataGridViewTextBoxColumn4.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 205;
            // 
            // User_Check
            // 
            this.User_Check.AutoSize = true;
            this.User_Check.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.User_Check.ForeColor = System.Drawing.Color.Navy;
            this.User_Check.Location = new System.Drawing.Point(10, 517);
            this.User_Check.Name = "User_Check";
            this.User_Check.Size = new System.Drawing.Size(93, 18);
            this.User_Check.TabIndex = 541;
            this.User_Check.Text = "المستخدم :";
            this.User_Check.UseVisualStyleBackColor = true;
            this.User_Check.CheckedChanged += new System.EventHandler(this.User_Check_CheckedChanged);
            // 
            // User_Button4
            // 
            this.User_Button4.Enabled = false;
            this.User_Button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.User_Button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.User_Button4.Location = new System.Drawing.Point(386, 607);
            this.User_Button4.Name = "User_Button4";
            this.User_Button4.Size = new System.Drawing.Size(68, 22);
            this.User_Button4.TabIndex = 547;
            this.User_Button4.Text = "<<";
            this.User_Button4.UseVisualStyleBackColor = true;
            this.User_Button4.Click += new System.EventHandler(this.User_Button4_Click);
            // 
            // User_Button3
            // 
            this.User_Button3.Enabled = false;
            this.User_Button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.User_Button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.User_Button3.Location = new System.Drawing.Point(386, 586);
            this.User_Button3.Name = "User_Button3";
            this.User_Button3.Size = new System.Drawing.Size(68, 22);
            this.User_Button3.TabIndex = 546;
            this.User_Button3.Text = "<";
            this.User_Button3.UseVisualStyleBackColor = true;
            this.User_Button3.Click += new System.EventHandler(this.User_Button3_Click);
            // 
            // User_Button2
            // 
            this.User_Button2.Enabled = false;
            this.User_Button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.User_Button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.User_Button2.Location = new System.Drawing.Point(386, 564);
            this.User_Button2.Name = "User_Button2";
            this.User_Button2.Size = new System.Drawing.Size(68, 22);
            this.User_Button2.TabIndex = 545;
            this.User_Button2.Text = ">>";
            this.User_Button2.UseVisualStyleBackColor = true;
            this.User_Button2.Click += new System.EventHandler(this.User_Button2_Click);
            // 
            // User_Button1
            // 
            this.User_Button1.Enabled = false;
            this.User_Button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.User_Button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.User_Button1.Location = new System.Drawing.Point(386, 543);
            this.User_Button1.Name = "User_Button1";
            this.User_Button1.Size = new System.Drawing.Size(68, 22);
            this.User_Button1.TabIndex = 544;
            this.User_Button1.Text = ">";
            this.User_Button1.UseVisualStyleBackColor = true;
            this.User_Button1.Click += new System.EventHandler(this.User_Button1_Click);
            // 
            // Txt_User
            // 
            this.Txt_User.BackColor = System.Drawing.Color.White;
            this.Txt_User.Enabled = false;
            this.Txt_User.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_User.Location = new System.Drawing.Point(108, 515);
            this.Txt_User.Name = "Txt_User";
            this.Txt_User.Size = new System.Drawing.Size(261, 22);
            this.Txt_User.TabIndex = 542;
            this.Txt_User.TextChanged += new System.EventHandler(this.Txt_User_TextChanged);
            // 
            // Txt_Max_Vo_no
            // 
            this.Txt_Max_Vo_no.AllowSpace = false;
            this.Txt_Max_Vo_no.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Max_Vo_no.Location = new System.Drawing.Point(274, 130);
            this.Txt_Max_Vo_no.Multiline = true;
            this.Txt_Max_Vo_no.Name = "Txt_Max_Vo_no";
            this.Txt_Max_Vo_no.Size = new System.Drawing.Size(111, 20);
            this.Txt_Max_Vo_no.TabIndex = 5;
            // 
            // Txt_Min_Vo_no
            // 
            this.Txt_Min_Vo_no.AllowSpace = false;
            this.Txt_Min_Vo_no.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Min_Vo_no.Location = new System.Drawing.Point(107, 130);
            this.Txt_Min_Vo_no.Multiline = true;
            this.Txt_Min_Vo_no.Name = "Txt_Min_Vo_no";
            this.Txt_Min_Vo_no.Size = new System.Drawing.Size(114, 22);
            this.Txt_Min_Vo_no.TabIndex = 4;
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "dd/mm/yyyy";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(274, 108);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(111, 20);
            this.TxtToDate.TabIndex = 1017;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "dd/mm/yyyy";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(107, 108);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(114, 20);
            this.TxtFromDate.TabIndex = 1016;
            // 
            // Grd_years
            // 
            this.Grd_years.AllowUserToAddRows = false;
            this.Grd_years.AllowUserToDeleteRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.Grd_years.BackgroundColor = System.Drawing.Color.White;
            this.Grd_years.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_years.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_years.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.Grd_years.ColumnHeadersHeight = 45;
            this.Grd_years.ColumnHeadersVisible = false;
            this.Grd_years.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_years.DefaultCellStyle = dataGridViewCellStyle27;
            this.Grd_years.Location = new System.Drawing.Point(107, 59);
            this.Grd_years.Name = "Grd_years";
            this.Grd_years.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_years.RowHeadersVisible = false;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.Grd_years.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_years.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_years.Size = new System.Drawing.Size(281, 48);
            this.Grd_years.TabIndex = 1025;
          //  this.Grd_years.SelectionChanged += new System.EventHandler(this.Grd_years_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "FromNrec_date";
            this.dataGridViewTextBoxColumn5.HeaderText = "التاريخ من";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 175;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TONrec_date";
            this.dataGridViewTextBoxColumn6.HeaderText = "التاريخ الى";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // cbo_year
            // 
            this.cbo_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_year.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_year.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_year.FormattingEnabled = true;
            this.cbo_year.Items.AddRange(new object[] {
            "البيانات الحالية",
            "البيانات المرحلة"});
            this.cbo_year.Location = new System.Drawing.Point(107, 34);
            this.cbo_year.Name = "cbo_year";
            this.cbo_year.Size = new System.Drawing.Size(281, 24);
            this.cbo_year.TabIndex = 1023;
            this.cbo_year.SelectedIndexChanged += new System.EventHandler(this.cbo_year_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(11, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 14);
            this.label9.TabIndex = 1024;
            this.label9.Text = "نـــــوع البحـــث:";
            // 
            // Search_Record_Operation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 668);
            this.Controls.Add(this.Grd_years);
            this.Controls.Add(this.cbo_year);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.GrdUser_2);
            this.Controls.Add(this.GrdUser_1);
            this.Controls.Add(this.User_Check);
            this.Controls.Add(this.User_Button4);
            this.Controls.Add(this.User_Button3);
            this.Controls.Add(this.User_Button2);
            this.Controls.Add(this.User_Button1);
            this.Controls.Add(this.Txt_User);
            this.Controls.Add(this.Grd_Class_2);
            this.Controls.Add(this.Grd_Oper_2);
            this.Controls.Add(this.Grd_Cust_2);
            this.Controls.Add(this.Grd_Cust_1);
            this.Controls.Add(this.Grd_Oper_1);
            this.Controls.Add(this.Grd_Class_1);
            this.Controls.Add(this.Cust_Check);
            this.Controls.Add(this.Class_Check);
            this.Controls.Add(this.Oper_Check);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Class_Button4);
            this.Controls.Add(this.Class_Button3);
            this.Controls.Add(this.Class_Button2);
            this.Controls.Add(this.Class_Button1);
            this.Controls.Add(this.TxtClass_Name);
            this.Controls.Add(this.Cust_Button4);
            this.Controls.Add(this.Cust_Button3);
            this.Controls.Add(this.Cust_Button2);
            this.Controls.Add(this.Cust_Button1);
            this.Controls.Add(this.Oper_Button4);
            this.Controls.Add(this.Oper_Button3);
            this.Controls.Add(this.Oper_Button2);
            this.Controls.Add(this.Oper_Button1);
            this.Controls.Add(this.TxtCust_Name);
            this.Controls.Add(this.TxtOper_Name);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.Txt_Max_Vo_no);
            this.Controls.Add(this.Txt_Min_Vo_no);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Search_Record_Operation";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "200";
            this.Text = "Search_Record_Operation";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Search_Record_Operation_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Search_Record_Operation_FormClosed);
            this.Load += new System.EventHandler(this.Search_Record_Operation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Class_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Oper_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Oper_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Class_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdUser_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdUser_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private NumericTextBox Txt_Min_Vo_no;
        private NumericTextBox Txt_Max_Vo_no;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button Cust_Button4;
        private System.Windows.Forms.Button Cust_Button3;
        private System.Windows.Forms.Button Cust_Button2;
        private System.Windows.Forms.Button Cust_Button1;
        private System.Windows.Forms.Button Oper_Button4;
        private System.Windows.Forms.Button Oper_Button3;
        private System.Windows.Forms.Button Oper_Button2;
        private System.Windows.Forms.Button Oper_Button1;
        private System.Windows.Forms.TextBox TxtCust_Name;
        private System.Windows.Forms.TextBox TxtOper_Name;
        private System.Windows.Forms.Button Class_Button4;
        private System.Windows.Forms.Button Class_Button3;
        private System.Windows.Forms.Button Class_Button2;
        private System.Windows.Forms.Button Class_Button1;
        private System.Windows.Forms.TextBox TxtClass_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.CheckBox Oper_Check;
        private System.Windows.Forms.CheckBox Class_Check;
        private System.Windows.Forms.CheckBox Cust_Check;
        private System.Windows.Forms.DataGridView Grd_Class_1;
        private System.Windows.Forms.DataGridView Grd_Oper_1;
        private System.Windows.Forms.DataGridView Grd_Cust_1;
        private System.Windows.Forms.DataGridView Grd_Cust_2;
        private System.Windows.Forms.DataGridView Grd_Oper_2;
        private System.Windows.Forms.DataGridView Grd_Class_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridView GrdUser_2;
        private System.Windows.Forms.DataGridView GrdUser_1;
        private System.Windows.Forms.CheckBox User_Check;
        private System.Windows.Forms.Button User_Button4;
        private System.Windows.Forms.Button User_Button3;
        private System.Windows.Forms.Button User_Button2;
        private System.Windows.Forms.Button User_Button1;
        private System.Windows.Forms.TextBox Txt_User;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.DataGridView Grd_years;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.ComboBox cbo_year;
        private System.Windows.Forms.Label label9;
    }
}