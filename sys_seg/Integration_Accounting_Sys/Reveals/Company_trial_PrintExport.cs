﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class Company_trial_PrintExport : Form
    {
        DataTable Compay_trail_Balanc_all = new DataTable();
        DataTable Comp_Trail_TBL2 = new DataTable();
        public DataGridView Dgv1 { get; set; }
        public DataGridView Dgv2 { get; set; }
        DataGridView Grd_Details_All = new DataGridView();

         String From_Date = "" ;
        String To_Date  = "" ; 
        String _Level  = "" ; 
       
        public Company_trial_PrintExport(String FromDate, String ToDate)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            From_Date = FromDate;
            To_Date = ToDate;
           

         

        }
        private void Trail_Balance_PrintExport_Load(object sender, EventArgs e)
        {

            Cbo_Print.SelectedIndex = 0;
            Cbo_Export.SelectedIndex = 0;
            if (connection.Lang_id == 2)
            {
                Cbo_Print.Items[0] = "Print All";
                Cbo_Print.Items[1] = "Detailed printing";
            }
            else
            {
                Cbo_Print.Items[0] = " طباعة اجمالـــي";
                Cbo_Print.Items[1] = "طباعة تفصيلــي";
            }
            if (connection.Lang_id == 2)
            {
                Cbo_Export.Items[0] = "All";
                Cbo_Export.Items[1] = "Detailed";
                Cbo_Export.Items[2] = "Comprehensive";
            }
            else
            {
                Cbo_Export.Items[0] = " اجمـــــالي ";
                Cbo_Export.Items[1] = "تفصيــــلي ";
                Cbo_Export.Items[2] = "شـامــــــل";
            }
            Chk_Print.Checked = true;
            Chk_Print_CheckedChanged(null, null);
        }
        private void Chk_Print_CheckedChanged(object sender, EventArgs e)
        {
            Cbo_Print.Enabled = true;
            Cbo_Export.Enabled = false;
        }

        private void Chk_Export_CheckedChanged(object sender, EventArgs e)
        {
            Cbo_Export.Enabled = true;
            Cbo_Print.Enabled = false;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            // ------------------طباعة
            if (Chk_Print.Checked == true)
            {
                //MessageBox.Show(connection.Lang_id == 1 ? "قيد الانجاز  " : " In Progress ", MyGeneral_Lib.LblCap);

                //      return;

                      if (Cbo_Print.SelectedIndex == 0)
                      {
                          Compay_trail_Balanc_all = connection.SQLDS.Tables["Trail_Balance_Tbl"].Select().CopyToDataTable();
                          DataTable Dt_Param = CustomControls.CustomParam_Dt();
                        //  Dt_Param.Rows.Add("User_rep", connection.User_Name);
                          Dt_Param.Rows.Add("From_Date", From_Date);
                          Dt_Param.Rows.Add("To_Date", To_Date);
                          Dt_Param.Rows.Add("_Level", _Level);
                          // Dt_Param.Rows.Add("Customer_Name", Customer_Name);

                          Compay_trail_Balanc_all.TableName = "Compay_trail_Balanc_all";

                          connection.SQLDS.Tables.Add(Compay_trail_Balanc_all);


                          Company_Trail_All_Rep ObjRpt = new Company_Trail_All_Rep();
                          Company_Trail_All_Rep_Eng ObjRptEng = new Company_Trail_All_Rep_Eng();
                          Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);
                          this.Visible = false;
                          RptLangFrm.ShowDialog(this);
                          this.Visible = true;
                          connection.SQLDS.Tables.Remove("Compay_trail_Balanc_all");

                      }
                      else
                      {

                          Company_Trial_Balance_Main_Details.Compay_trail_Balanc_Det.TableName = "Compay_trail_Balanc_all";
                          DataTable Dt_Param = CustomControls.CustomParam_Dt();

                          Dt_Param.Rows.Add("From_Date", From_Date);
                          Dt_Param.Rows.Add("To_Date", To_Date);

                          int Acc_id = Company_Trial_Balance_Main_Details.Acc_id;
                          DataTable Compay_trail_Balanc_Det = new DataTable();

                          Compay_trail_Balanc_Det = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("Acc_Id = " + Acc_id).CopyToDataTable();
                          Compay_trail_Balanc_Det.TableName = "Compay_trail_Balanc_Det";

                          connection.SQLDS.Tables.Add(Company_Trial_Balance_Main_Details.Compay_trail_Balanc_Det);

                          connection.SQLDS.Tables.Add(Compay_trail_Balanc_Det);
                          Company_Trial_Balance_Dails_Rep ObjRpt = new Company_Trial_Balance_Dails_Rep();
                          Company_Trial_Balance_Dails_Rep ObjRptEng = new Company_Trial_Balance_Dails_Rep();
                          Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);
                          this.Visible = false;
                          RptLangFrm.ShowDialog(this);
                          this.Visible = true;
                          connection.SQLDS.Tables.Remove("Compay_trail_Balanc_all");
                          connection.SQLDS.Tables.Remove("Compay_trail_Balanc_Det");
                      }
            }
            // ------------------تصدير
            if (Chk_Export.Checked == true)
            {
                if (Cbo_Export.SelectedIndex == 0)//تصدير اجمالي
                {

                    DataTable ExpDt_Term = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOloc_Amount"
                                                                                                     , "COloc_Amount", "Ploc_Amount", "pLoc_Amount1", "DEloc_Amount", "CEloc_Amount").Select().CopyToDataTable();

                    ExpDt_Term.Rows.Add(new Object[] {  DBNull.Value, "المجامــــيع_(ع.م)_" , Company_Trial_Balance_Main_Details.OD_LocAmount, Company_Trial_Balance_Main_Details.OC_LocAmount,
                        Company_Trial_Balance_Main_Details.PD_LocAmount,Company_Trial_Balance_Main_Details.PC_LocAmount ,Company_Trial_Balance_Main_Details.ED_LocAmount ,Company_Trial_Balance_Main_Details.EC_LocAmount});

                    DataTable[] _EXP_DT = { ExpDt_Term };
                    DataGridView[] Export_GRD = { Dgv1 };
                    MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? "ميزان مراجعة اجمالي على مستوى الشركة" : "All detailed review balance at the Company level");
                }
                if (Cbo_Export.SelectedIndex == 1)//تصدير تفصيلي
                {
                    if (Dgv2.Rows.Count > 0)
                    {
                        DataTable ExpDt_Term = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "Acc_No", "Acc_Aname", "DOloc_Amount"
                                                                                                     , "COloc_Amount", "pLoc_Amount", "pLoc_Amount1", "DEloc_Amount", "CEloc_Amount").Select("Acc_No like'" + Company_Trial_Balance_Main_Details.Acc_No + "'").CopyToDataTable();

                        DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", connection.Lang_id == 1 ? "cur_aname" : "cur_Ename"
                                                                                                          , "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "pFor_Amount", "EFor_Amount").Select(" Acc_No like '" + Company_Trial_Balance_Main_Details.Acc_No + "'").CopyToDataTable();

                        ExpDt_Term_Acc = ExpDt_Term_Acc.DefaultView.ToTable(false, connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", connection.Lang_id == 1 ? "cur_aname" : "cur_Ename"
                                                                                                          , "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "pFor_Amount", "EFor_Amount").Select().CopyToDataTable();


                        DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc };
                        DataGridView[] Export_GRD = { Dgv1, Dgv2 };
                        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? "ميزان مراجعة تفصيلي للشركة على مستوى الحساب" : " detailed review balance of the company at the account level");
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للحساب " : " No records to this account ", MyGeneral_Lib.LblCap);

                        return;
                    }
                }
                if (Cbo_Export.SelectedIndex == 2)//تصدير شامل
                {
                    if (Dgv1.Rows.Count > 0)
                    {
                        Get_Grd();

                        DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Trail_Balance_Tbl1"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name", connection.Lang_id == 1 ? "cur_aname" : "cur_Ename"
                                                                                                          , "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "pFor_Amount", "EFor_Amount").Select().CopyToDataTable();



                        DataTable[] _EXP_DT = { ExpDt_Term_Acc };
                        DataGridView[] Export_GRD = { Grd_Details_All };
                        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? "ميزان مراجعة شـــامل للشــركة" : "detailed review balance Comprehensive for company");
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للتصدير " : " No records to this account ", MyGeneral_Lib.LblCap);

                        return;
                    }
                }
            }
        }
        //----------------------------------------------------------
        private void Get_Grd()
        {
            Grd_Details_All.Columns.Add("Column1", connection.Lang_id == 1 ? "رمز الحساب" : " Account No.");
            Grd_Details_All.Columns.Add("Column2", connection.Lang_id == 1 ? "الحـــساب" : "Account Name");
            Grd_Details_All.Columns.Add("Column3", connection.Lang_id == 1 ? "الفـــرع" : "Terminals");
            Grd_Details_All.Columns.Add("Column4", connection.Lang_id == 1 ? "الثانوي" : "Customer Name ");
            Grd_Details_All.Columns.Add("Column5", connection.Lang_id == 1 ? "العملة الاصلية" : "Currency Name");
            Grd_Details_All.Columns.Add("Column6", connection.Lang_id == 1 ? "ارصدة اول المدة _(ع.م)_" : "First Balance (L.C)");
            Grd_Details_All.Columns.Add("Column7", connection.Lang_id == 1 ? "ارصدة الفتــرة _(ع.م)_" : "Peroid Balance (L.C)");
            Grd_Details_All.Columns.Add("Column8", connection.Lang_id == 1 ? "ارصدة نهاية المدة _(ع.م)_" : "Last Balance (L.C)");
            Grd_Details_All.Columns.Add("Column9", connection.Lang_id == 1 ? "ارصدة اول المدة _(ع.ص)_" : "First Balance (F.C)");
            Grd_Details_All.Columns.Add("Column10", connection.Lang_id == 1 ? "ارصدة الفتــرة _(ع.ص)_" : "Peroid Balance (F.C)");
            Grd_Details_All.Columns.Add("Column11", connection.Lang_id == 1 ? "ارصدة نهاية المدة _(ع.ص)_" : "Last Balance (F.C)");
        }
        //----------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}