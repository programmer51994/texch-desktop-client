﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class Search_Reveal_pl_Acc : Form
    {
        #region Defintion
        DataTable DT_Cust = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        int T_Id = 0;
        string Filter = "";
        string @format = "dd/MM/yyyy";
        BindingSource _bsyears = new BindingSource();
        bool Change_grd = false;
         int from_nrec_date =  0 ;
         int to_nrec_date = 0;
         int min_nrec_date_int = 0;
         int from_min_nrec_date_int = 0;
        
        #endregion

        public Search_Reveal_pl_Acc()
        {
            InitializeComponent(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
          //  Grd_years.AutoGenerateColumns = false;
            Create_Tbl();
            BindingSource _bsyears = new BindingSource();
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cust_Id.Columns["Column8"].DataPropertyName = "Ecust_name";
                Grd_Cust.Columns["Column11"].DataPropertyName = "Ecust_name";
                //cbo_year.Items.Clear();
                //cbo_year.Items.Add("current Data");
                //cbo_year.Items.Add("Old period data");
            }
            #endregion
        }

        private void Create_Tbl()
        {
            string[] Column1 = { "T_Id", "ACUST_NAME", "ECUST_NAME" };
            string[] DType1 = { "System.Int16", "System.String", "System.String" };
            DT_Cust = CustomControls.Custom_DataTable("DT_Cust", Column1, DType1);
            Grd_Cust.DataSource = DT_Cust;


        }

        private void Search_Trial_Balance_Load(object sender, EventArgs e)
        {
            //cbo_year.SelectedIndex = 0;
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;
            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl1"], "From_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            //TxtFromDate.Checked = true;
            //TxtToDate.Checked = true;

            connection.SqlExec("Exec Search_Reveal_pl_Acc ", "SearchTrial_Balance_Tbl");
        
            Cbo_Level.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl1"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "Acc_Level" : "Acc_Level";
            Cbo_Level.SelectedIndex = 0;

          
            //TxtFromDate.Text = connection.SQLDS.Tables["SearchTrial_Balance_Tbl2"].Rows[0]["Min"].ToString();
            //TxtToDate.Text = connection.SQLDS.Tables["SearchTrial_Balance_Tbl2"].Rows[0]["Max"].ToString();

        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                T_Id = 0;
                string Cond_Str_Cust_Id = "";
                string Con_Str = "";
                if (DT_Cust.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cust, "T_Id", out Cond_Str_Cust_Id);
                    Con_Str = " And T_Id not in(" + Cond_Str_Cust_Id + ")";
                }
                int.TryParse(TxtCust_Name.Text, out T_Id);


                Filter = " (Acust_name like '" + TxtCust_Name.Text + "%' or  Ecust_name like '" + TxtCust_Name.Text + "%'"
                            + " Or T_Id = " + T_Id + ")" + Con_Str;
                DT_Cust_TBL = connection.SQLDS.Tables["SearchTrial_Balance_Tbl"].DefaultView.ToTable(true, "T_Id", "ACust_name", "Ecust_name").Select(Filter).CopyToDataTable();
                Grd_Cust_Id.DataSource = DT_Cust_TBL;

            }
            catch
            {
                Grd_Cust_Id.DataSource = new DataTable();
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {

                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }

        private void Search_Trial_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "SearchTrial_Balance_Tbl", "SearchTrial_Balance_Tbl1", "Tbl_PL_Inquery" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Chk_Cust_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cust.Checked)
            {
                TxtCust_Name.Enabled = true;
                TxtCust_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cust.Clear();
                DT_Cust_TBL.Clear();
                TxtCust_Name.ResetText();
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                TxtCust_Name.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cust.NewRow();

            row["T_id"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["T_Id"];
            row["ACUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ACust_name"];
            row["ECUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ECust_name"];
            DT_Cust.Rows.Add(row);
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);

            button3.Enabled = true;
            button4.Enabled = true;
            if (Grd_Cust_Id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cust_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cust.NewRow();
                row["T_id"] = DT_Cust_TBL.Rows[i]["T_Id"];
                row["ACUST_NAME"] = DT_Cust_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Cust_TBL.Rows[i]["ECUST_NAME"];
                DT_Cust.Rows.Add(row);
            }
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows[Grd_Cust.CurrentRow.Index].Delete();
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            if (Grd_Cust.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows.Clear();
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            string From_Date = "";
            string To_Date = "";
            // string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
            //string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
            //if (cbo_year.SelectedIndex == 0)//current
            //{
                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }

                }
                if (TxtFromDate.Checked == true)
                {

                    DateTime date = TxtFromDate.Value.Date;
                    From_Date = (date.Day + date.Month * 100 + date.Year * 10000).ToString();
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ بداية الفترة" : "select the date of first period", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked == true)
                {

                    DateTime date = TxtToDate.Value.Date;
                    To_Date = (date.Day + date.Month * 100 + date.Year * 10000).ToString();
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ نهاية الفترة" : "select the date of end period", MyGeneral_Lib.LblCap);
                    return;
                }
                string InRecDate = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);

                if (Convert.ToInt32(From_Date) > Convert.ToInt32(InRecDate))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " تحقق من التأريخ " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                    return;
                }
                if (Convert.ToInt32(To_Date) != 0)
                {
                    if (Convert.ToInt32(From_Date) > Convert.ToInt32(To_Date) || Convert.ToInt32(From_Date) > Convert.ToInt32(InRecDate))
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " تاريخ بداية الفترة اكبر من تاريخ النهاية  " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                        return;
                    }
               }
          //  }
            //if (cbo_year.SelectedIndex == 1)//old
            //{
            //    if (TxtFromDate.Checked != true)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
            //        return;
            //    }

            //    if (TxtToDate.Checked != true)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
            //        return;
            //    }

            //    DateTime date_from = TxtFromDate.Value.Date;
            //     from_nrec_date = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
            //    DateTime date_to = TxtToDate.Value.Date;
            //     to_nrec_date = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

            //    int Real_from_date = Convert.ToInt32(((DataRowView)_bsyears.Current).Row["FromNrec_date_int"]);
            //    int Real_To_date = Convert.ToInt32(((DataRowView)_bsyears.Current).Row["TONrec_date_int"]);

            //    if ((from_nrec_date < Real_from_date) || (from_nrec_date > Real_To_date) || (to_nrec_date < Real_from_date) || (to_nrec_date > Real_To_date))
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
            //        return;
            //    }

            //    if (from_nrec_date > to_nrec_date)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
            //        return;
            //    }

            //}

            if (Grd_Cust.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " تأكد من اختيار الثانوي  " : "Enter the Customer please", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            string T_IDStr = "";

            MyGeneral_Lib.ColumnToString(DT_Cust, "T_Id", out  T_IDStr);

            //object[] Sparam = {T_IDStr,From_Date,
            //                    To_Date,Cbo_Level.SelectedValue,0};
            this.Enabled = false;
            //MyGeneral_Lib.Copytocliptext("PL_Inquery", Sparam);
            //connection.SqlExec("PL_Inquery", "Tbl_PL_Inquery", Sparam);
            connection.SQLCS.Open();
            connection.SQLCMD.CommandText =  "PL_Inquery"  ;
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.CommandTimeout = 0;


            connection.SQLCMD.Parameters.AddWithValue("@T_Columns", T_IDStr);
            connection.SQLCMD.Parameters.AddWithValue("@from_nrec_date", From_Date);
            connection.SQLCMD.Parameters.AddWithValue("@to_nrec_date", To_Date);
            connection.SQLCMD.Parameters.AddWithValue("@lev_acc", Cbo_Level.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@view_flag", 0);

            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Tbl_PL_Inquery");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Tbl_PL_Inquery1");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Tbl_PL_Inquery2");
            obj.Close();
            connection.SQLCS.Close();

            if (connection.Col_Name != "")
            {
                MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            connection.SQLCMD.Parameters.Clear();

            Reveal_pl_Acc_Main Frm = new Reveal_pl_Acc_Main(TxtFromDate.Text, TxtToDate.Text);
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;
            this.Enabled = true;
        }


        private void Search_Reveal_pl_Acc_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        //private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (cbo_year.SelectedIndex == 0)//البيانات الحالية
        //    {
        //       // Grd_years.Enabled = false;
        //      //  Grd_years.DataSource = new BindingSource();
        //        TxtFromDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl1"], "From_Date");
        //        TxtToDate.Checked = false;
        //        checkBox1.Visible = false;

        //    }
        //    if (cbo_year.SelectedIndex == 1)// old 
        //    {
        //       // Grd_years.Enabled = true;
        //      //  _bsyears.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl2"];
        //      //  Grd_years.DataSource = _bsyears;
        //        Change_grd = true;
        //     //   Grd_years_SelectionChanged(null, null);
        //        checkBox1.Visible = true;
        //    }
        //}

        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");

        //    }
        //}
    }
}
