﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Balance_Local_Amount : Form
    {
        #region Defintion
        string @format = "dd/MM/yyyy";
        DataTable DT_Cust = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        int T_Id = 0;
        string Filter = "";
        DataTable DT_Cur = new DataTable();
        DataTable DT_Acc = new DataTable();
        DataTable DT_Cur_TBL = new DataTable();
        DataTable DT_Acc_TBL = new DataTable();
        int Cur_id = 0;
        int Acc_Id = 0;
        string Str_Id = "";
        string Con_Str = "";
      //  bool Change_grd = false;
        BindingSource _bsyears = new BindingSource();
        int from_nrec_date = 0;
        int to_nrec_date = 0;
        int min_nrec_date_int = 0;
        int from_min_nrec_date_int = 0;
        #endregion

        public Search_Balance_Local_Amount()
        {
            InitializeComponent();
            Grd_years.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Cur.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Acc.AutoGenerateColumns = false;
            Create_Tbl();

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Column15.DataPropertyName = "Acc_Ename";
                Column16.DataPropertyName = "Acc_Ename";
                Column2.DataPropertyName = "Cur_Ename";
                Column5.DataPropertyName = "Cur_Ename";
                CboDisplay.Items.Clear();
                CboDisplay.Items.Add("Detailed Account");
                CboDisplay.Items.Add("Accumulation Account");
                cbo_year.Items.Clear();
                cbo_year.Items.Add("current Data");
                cbo_year.Items.Add("Old period data");
            }
            #endregion
        }
        //---------------------------------------
        private void Reveal_Balance_Local_Amount_Load(object sender, EventArgs e)
        {
            cbo_year.SelectedIndex = 0;
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;
            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            connection.SqlExec("Exec Search_Reveal_Balance_Local_Amount ", "Balance_Local_Amount_Tbl");
            Cbo_Level.DataSource = connection.SQLDS.Tables["Balance_Local_Amount_Tbl3"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "AAcc_Level" : "EAcc_Level";
            Cbo_Level.SelectedIndex = 0;
            CboDisplay.SelectedIndex = 0;
        }
        private void Create_Tbl()
        {
            //---
            string[] Column2 = { "Acc_id", "Acc_ANAME", "Acc_Ename", "Dot_Acc_No" };
            string[] DType2 = { "System.Int16", "System.String", "System.String", "System.String" };
            DT_Acc = CustomControls.Custom_DataTable("DT_Acc", Column2, DType2);
            Grd_Acc.DataSource = DT_Acc;
            //-----
            string[] Column = { "cur_id", "Cur_ANAME", "Cur_ENAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Cur = CustomControls.Custom_DataTable("DT_Cur", Column, DType);
            Grd_Cur.DataSource = DT_Cur;
        }
        //----------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------
        private void Search_Trial_Balance_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Balance_Local_Amount_Tbl", "Balance_Local_Amount_Tbl1", "Balance_Local_Amount_Tbl2", "Balance_Local_Amount_Tbl3", "Reveal_Balance_Main_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //----------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation

            if (TxtToDate.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                return;
            }
            
            if (cbo_year.SelectedIndex == 0)
            {
                if (TxtFromDate.Checked == true)
                {

                    DateTime date = TxtFromDate.Value.Date;
                    //int Date = date.Day;
                    from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;


                }
                else
                {
                    from_nrec_date = 0;
                }

                if (TxtToDate.Checked == true)
                {

                    DateTime date = TxtToDate.Value.Date;
                    to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    to_nrec_date = 0;
                }

                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }
                }


            if (Convert.ToInt32(from_nrec_date) > Convert.ToInt32(to_nrec_date) && Convert.ToInt32(to_nrec_date) != 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " تاريخ بداية الفترة اكبر من تاريخ النهاية " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                return;
            }

            }


        if (cbo_year.SelectedIndex == 1)//old
            {
                if (TxtFromDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                DateTime date_from = TxtFromDate.Value.Date;
                from_nrec_date = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
                DateTime date_to = TxtToDate.Value.Date;
                to_nrec_date = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

                int Real_from_date = Convert.ToInt32(connection.SQLDS.Tables["Balance_Local_Amount_Tbl5"].Rows[0]["min_nrec_date_int"]);
                int Real_To_date = Convert.ToInt32(connection.SQLDS.Tables["Balance_Local_Amount_Tbl5"].Rows[0]["max_nrec_date_int"]);

                if(( from_nrec_date < Real_from_date ) ||  (from_nrec_date > Real_To_date   )||(to_nrec_date < Real_from_date) || (to_nrec_date > Real_To_date))
                {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (from_nrec_date > to_nrec_date)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                } 

            }

           // xx string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
           //xx string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
            //string InRecDate = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);
            //if (from_nrec_date == "-1" || To_Date == "-1" || from_nrec_date == "0" || To_Date == "0")
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من الفترة " : "Enter The Date Please ", MyGeneral_Lib.LblCap);
            //    return;
            //}

            

            #endregion
            string Cur_IDStr = "";
            string Acc_IDStr = "";
            MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out  Cur_IDStr);
            MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out  Acc_IDStr);

            this.Enabled = false;
            //object[] Sparam = {connection.T_ID,Cur_IDStr,Acc_IDStr,from_nrec_date,to_nrec_date,
            //                      CHK_FTB_Flag.Checked,CboDisplay.SelectedIndex,Cbo_Level.SelectedValue,
            //                      CHK_Zero_Balancing.Checked,connection.user_id,""};

            //MyGeneral_Lib.Copytocliptext("Reveal_Balance_Main", Sparam);
            //connection.SqlExec("Reveal_Balance_Main", "Reveal_Balance_Main_Tbl", Sparam);

            try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "Reveal_Balance_Main" : "Reveal_Balance_Main_Phst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                if (cbo_year.SelectedIndex == 0)//cureent
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Tid_Str", connection.T_ID);
                    connection.SQLCMD.Parameters.AddWithValue("@CurId_Str", Cur_IDStr);
                    connection.SQLCMD.Parameters.AddWithValue("@AccId_Str", Acc_IDStr);
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@FTB_Flag", CHK_FTB_Flag.Checked);
                    connection.SQLCMD.Parameters.AddWithValue("@View_Style", CboDisplay.SelectedIndex);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Cbo_Level.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Zero_Balance", CHK_Zero_Balancing.Checked);
                    connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                    

                }
                else
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Tid_Str", connection.T_ID);
                    connection.SQLCMD.Parameters.AddWithValue("@CurId_Str", Cur_IDStr);
                    connection.SQLCMD.Parameters.AddWithValue("@AccId_Str", Acc_IDStr);
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@FTB_Flag", CHK_FTB_Flag.Checked);
                    connection.SQLCMD.Parameters.AddWithValue("@View_Style", CboDisplay.SelectedIndex);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Cbo_Level.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Zero_Balance", CHK_Zero_Balancing.Checked);
                    connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.AddWithValue("@p_id", Convert.ToByte(((DataRowView)_bsyears.Current).Row["P_ID"]));// period max
                   

                }
             
            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_Balance_Main_Tbl");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_Balance_Main_Tbl1");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_Balance_Main_Tbl2");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_Balance_Main_Tbl3");
            obj.Close();
            connection.SQLCS.Close();

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
           
            if (connection.Col_Name != "")
            {
                MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                this.Enabled = true;
                return;
            }

            Reveal_Balance_Local_Amount Frm = new Reveal_Balance_Local_Amount(TxtFromDate.Text, TxtToDate.Text);
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;
            this.Enabled = true;
        }
        //----------------------
        private void CboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboDisplay.SelectedIndex == 0)
            {
                Cbo_Level.Visible = false;
                label6.Visible = false;
                Chk_Acc_CheckedChanged(null, null);
                DT_Acc.Clear();
                DT_Cur.Clear();
                DT_Cust.Clear();
            }
            else
            {
                Cbo_Level.Visible = true;
                label6.Visible = true;
                Chk_Acc_CheckedChanged(null, null);
                DT_Acc.Clear();
                DT_Cur.Clear();
                DT_Cust.Clear();
            }

        }
        //----------------------
        private void Chk_Acc_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Acc.Checked)
            {
                TxtAcc_Name.Enabled = true;
                TxtAcc_Name_TextChanged(null, null);
            }
            else
            {
                DT_Acc.Clear();
                DT_Acc_TBL.Clear();
                TxtAcc_Name.ResetText();
                button8.Enabled = false;
                button7.Enabled = false;
                button6.Enabled = false;
                button5.Enabled = false;
                TxtAcc_Name.Enabled = false;
            }
        }
        //----------------------
        private void TxtAcc_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Acc_Id = 0;
                Con_Str = "";
                Str_Id = "";
                if (DT_Acc.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                    Con_Str = " And Acc_Id not in(" + Str_Id + ")";
                }
                int.TryParse(TxtAcc_Name.Text, out Acc_Id);

                if (Convert.ToInt16(Cbo_Level.SelectedIndex) != 0 && CboDisplay.SelectedIndex == 1)
                    Con_Str += " And Acc_Level = " + Cbo_Level.SelectedValue.ToString();

                Filter = " (Acc_Aname like '" + TxtAcc_Name.Text + "%' or  Acc_Ename like '" + TxtAcc_Name.Text + "%'"
                            + " Or Acc_id = " + Acc_Id + ")" + Con_Str;
                DT_Acc_TBL = connection.SQLDS.Tables["Balance_Local_Amount_Tbl"].DefaultView.ToTable(true, "Acc_Id", "Acc_Aname", "Acc_Ename", "Acc_Level", "Dot_Acc_No").Select(Filter).CopyToDataTable();
                Grd_Acc_Id.DataSource = DT_Acc_TBL;

            }
            catch
            {
                Grd_Acc_Id.DataSource = new DataTable();
            }
            if (Grd_Acc_Id.Rows.Count <= 0)
            {

                button8.Enabled = false;
                button7.Enabled = false;
            }
            else
            {
                button8.Enabled = true;
                button7.Enabled = true;
            }
        }

        private void Cbo_Level_SelectedIndexChanged(object sender, EventArgs e)
        {
            Chk_Acc_CheckedChanged(null, null);
            DT_Acc.Clear();
            DT_Cur.Clear();
            DT_Cust.Clear();
        }
        //---------------------------------------
        private void button8_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Acc.NewRow();

            row["Acc_id"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_Id"];
            row["Acc_aname"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_aname"];
            row["Acc_ename"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_ename"];
            row["Dot_Acc_No"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Dot_Acc_No"];
            DT_Acc.Rows.Add(row);
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);

            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Acc_Id.Rows.Count == 0)
            {
                button8.Enabled = false;
                button7.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
        }
        //---------------------------------------
        private void button7_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Acc_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Acc.NewRow();
                row["Acc_id"] = DT_Acc_TBL.Rows[i]["Acc_id"];
                row["Acc_aname"] = DT_Acc_TBL.Rows[i]["Acc_aname"];
                row["Acc_ename"] = DT_Acc_TBL.Rows[i]["Acc_ename"];
                row["Dot_Acc_No"] = DT_Acc_TBL.Rows[i]["Dot_Acc_No"];
                DT_Acc.Rows.Add(row);
            }
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button8.Enabled = false;
            button7.Enabled = false;
            button6.Enabled = true;
            button5.Enabled = true;
            Chk_Cur_CheckedChanged(null, null);
        }
        //---------------------------------------
        private void button6_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows[Grd_Acc.CurrentRow.Index].Delete();
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button7.Enabled = true;
            button8.Enabled = true;
            if (Grd_Acc.Rows.Count == 0)
            {
                button6.Enabled = false;
                button5.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
        }
        //---------------------------------------
        private void button5_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows.Clear();
            TxtAcc_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = false;
            button5.Enabled = false;
            Chk_Cur_CheckedChanged(null, null);
        }
        //---------------------------------------
        private void Chk_Cur_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked)
            {
                TxtCur_Name.Enabled = true;
                TxtCur_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cur.Clear();
                DT_Cur_TBL.Clear();
                TxtCur_Name.ResetText();
                button12.Enabled = false;
                button11.Enabled = false;
                button10.Enabled = false;
                button9.Enabled = false;
                TxtCur_Name.Enabled = false;
            }
        }
        //---------------------------------------
        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Acc_Id = 0;
                Cur_id = 0;
                Str_Id = "";
                Con_Str = "";
                if (DT_Acc.Rows.Count > 0 && Convert.ToByte(Cbo_Level.SelectedValue) <= 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                    Con_Str = " And  Acc_Id  in(" + Str_Id + ")";
                }
                else if (DT_Acc.Rows.Count > 0 && Convert.ToByte(Cbo_Level.SelectedValue) > 0)
                {
                    Str_Id = "";
                    string Acc_ID_Str = "";
                    foreach (DataRow Drow in DT_Acc.Rows)
                    {
                        DataTable Dt = connection.SQLDS.Tables["Balance_Local_Amount_Tbl"].Select("Dot_Acc_No like '" + Drow["Dot_Acc_No"] + "%'").CopyToDataTable();
                        MyGeneral_Lib.ColumnToString(Dt, "Acc_Id", out Str_Id);
                        Acc_ID_Str += Str_Id + ",";
                      
                    }
                    Con_Str = " And  Acc_Id  in(" + Acc_ID_Str + ")";
                }

                if (DT_Cur.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out Str_Id);
                    Con_Str += " And  Cur_ID not in(" + Str_Id + ")";
                }
                int.TryParse(TxtCur_Name.Text, out Cur_id);

                Filter = " (Cur_Aname like '" + TxtCur_Name.Text + "%' or  Cur_Ename like '" + TxtCur_Name.Text + "%'"
                            + " Or Cur_id = " + Cur_id + ")" + Con_Str;
                # endregion
                DT_Cur_TBL = connection.SQLDS.Tables["Balance_Local_Amount_Tbl1"].Select(Filter).CopyToDataTable();
                DT_Cur_TBL = DT_Cur_TBL.DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
                Grd_Cur_Id.DataSource = DT_Cur_TBL;
            }
            catch
            {
                Grd_Cur_Id.DataSource = new DataTable();
            }
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                button12.Enabled = false;
                button11.Enabled = false;
            }
            else
            {
                button12.Enabled = true;
                button11.Enabled = true;
            }
        }
        //---------------------------------------
        private void button12_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cur.NewRow();

            row["Cur_id"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_Id"];
            row["Cur_aname"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_aname"];
            row["Cur_ename"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_ename"];
            DT_Cur.Rows.Add(row);
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);

            button10.Enabled = true;
            button9.Enabled = true;
            if (Grd_Cur_Id.Rows.Count == 0)
            {
                button12.Enabled = false;
                button11.Enabled = false;
            }
        }
        //---------------------------------------
        private void button11_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cur_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cur.NewRow();
                row["Cur_id"] = DT_Cur_TBL.Rows[i]["Cur_id"];
                row["Cur_aname"] = DT_Cur_TBL.Rows[i]["Cur_aname"];
                row["Cur_ename"] = DT_Cur_TBL.Rows[i]["Cur_ename"];
                DT_Cur.Rows.Add(row);
            }
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button12.Enabled = false;
            button11.Enabled = false;
            button10.Enabled = true;
            button9.Enabled = true;
        }
        //---------------------------------------
        private void button10_Click(object sender, EventArgs e)
        {
            DT_Cur.Rows[Grd_Cur.CurrentRow.Index].Delete();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button12.Enabled = true;
            button11.Enabled = true;
            if (Grd_Cur.Rows.Count == 0)
            {
                button10.Enabled = false;
                button9.Enabled = false;
            }
        }
        //---------------------------------------
        private void button9_Click(object sender, EventArgs e)
        {
            DT_Cur.Rows.Clear();
            TxtCur_Name_TextChanged(null, null);
            button12.Enabled = true;
            button11.Enabled = true;
            button10.Enabled = false;
            button9.Enabled = false;
        }
        //---------------------------------------
        private void Search_Balance_Local_Amount_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
        //---------------------------------------
        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();
                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");
                TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();
                TxtToDate.Checked = false;
                 

            }
            if (cbo_year.SelectedIndex == 1)// old 
            {
                Grd_years.Enabled = true;
              //  _bsyears.DataSource = connection.SQLDS.Tables["Balance_Local_Amount_Tbl4"];
                //Grd_years.DataSource = _bsyears;

                if (connection.SQLDS.Tables["Balance_Local_Amount_Tbl4"].Rows.Count > 0)
                {

                    Grd_years.DataSource = connection.SQLDS.Tables["Balance_Local_Amount_Tbl4"].Select("t_id =" + connection.T_ID).CopyToDataTable();

                    TxtFromDate.Text = connection.SQLDS.Tables["Balance_Local_Amount_Tbl5"].Rows[0]["min_nrec_date"].ToString();
                    TxtToDate.Text = connection.SQLDS.Tables["Balance_Local_Amount_Tbl5"].Rows[0]["max_nrec_date"].ToString();
                }


                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }


                //Change_grd = true;
                //Grd_years_SelectionChanged(null, null);
                


            }
        }
        //---------------------------------------
        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");
        //    }
        //}
    }
}