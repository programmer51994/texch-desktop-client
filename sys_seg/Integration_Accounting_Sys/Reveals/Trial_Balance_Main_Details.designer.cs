﻿namespace Integration_Accounting_Sys
{
    partial class Trial_Balance_Main_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtToDate = new System.Windows.Forms.MaskedTextBox();
            this.TxtFromDate = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Terminals = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Grd_Acc_Details = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.Grd_Vo_Details = new System.Windows.Forms.DataGridView();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.Grd_SumAmount_Acc = new System.Windows.Forms.DataGridView();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.AddBtn = new System.Windows.Forms.Button();
            this.Grd_vo_details2 = new System.Windows.Forms.DataGridView();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_SumCfor = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_SumDFor = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_SumDloc = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_SumCloc = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtED_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtEC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPD_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOD_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Terminals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Vo_Details)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_SumAmount_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_vo_details2)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtToDate
            // 
            this.TxtToDate.BackColor = System.Drawing.Color.White;
            this.TxtToDate.Enabled = false;
            this.TxtToDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToDate.Location = new System.Drawing.Point(286, 5);
            this.TxtToDate.Mask = "0000/00/00";
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.PromptChar = ' ';
            this.TxtToDate.Size = new System.Drawing.Size(126, 23);
            this.TxtToDate.TabIndex = 3;
            this.TxtToDate.Text = "00000000";
            this.TxtToDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.BackColor = System.Drawing.Color.White;
            this.TxtFromDate.Enabled = false;
            this.TxtFromDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFromDate.Location = new System.Drawing.Point(99, 5);
            this.TxtFromDate.Mask = "0000/00/00";
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.PromptChar = ' ';
            this.TxtFromDate.Size = new System.Drawing.Size(126, 23);
            this.TxtFromDate.TabIndex = 2;
            this.TxtFromDate.Text = "00000000";
            this.TxtFromDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 718;
            this.label2.Text = "التــاريــخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(232, 9);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 717;
            this.label5.Text = "إلــــى :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1094, 1);
            this.flowLayoutPanel1.TabIndex = 716;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(632, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(242, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(880, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(59, 14);
            this.label4.TabIndex = 710;
            this.label4.Text = "التاريــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(943, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(545, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(82, 14);
            this.label1.TabIndex = 709;
            this.label1.Text = "المستخــــدم:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 33);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1087, 1);
            this.flowLayoutPanel4.TabIndex = 722;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(1091, 33);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 616);
            this.flowLayoutPanel7.TabIndex = 723;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 34);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 615);
            this.flowLayoutPanel6.TabIndex = 725;
            // 
            // Grd_Terminals
            // 
            this.Grd_Terminals.AllowUserToAddRows = false;
            this.Grd_Terminals.AllowUserToDeleteRows = false;
            this.Grd_Terminals.AllowUserToResizeColumns = false;
            this.Grd_Terminals.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Terminals.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Terminals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Terminals.ColumnHeadersHeight = 25;
            this.Grd_Terminals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.Grd_Terminals.Location = new System.Drawing.Point(9, 36);
            this.Grd_Terminals.Name = "Grd_Terminals";
            this.Grd_Terminals.ReadOnly = true;
            this.Grd_Terminals.RowHeadersWidth = 10;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Terminals.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Terminals.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Terminals.Size = new System.Drawing.Size(1076, 73);
            this.Grd_Terminals.TabIndex = 4;
            this.Grd_Terminals.SelectionChanged += new System.EventHandler(this.Grd_Terminals_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cust_Id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رمز الثانوي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Acust_Name";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "الإســم العــربي";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 270;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Ecust_Name";
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "الإســم الأجنبــي";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 268;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "MinDrec_date";
            this.Column4.HeaderText = "بداية تاريخ الاغلاق المحاسبي";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 175;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "MaxDrec_date";
            this.Column5.HeaderText = "اخر تاريخ اغلاق محاسبي";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 250;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 119);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel2.TabIndex = 737;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(159, 266);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(75, 14);
            this.label6.TabIndex = 748;
            this.label6.Text = "المجاميـــــع:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(818, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(265, 20);
            this.label7.TabIndex = 741;
            this.label7.Text = "                ارصـــدة نهايـــة المـــــدة (ع/م)  ";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(557, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(259, 20);
            this.label8.TabIndex = 740;
            this.label8.Text = "                   ارصــــدة الفتـــــرة (ع/م)  ";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(299, 131);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(256, 20);
            this.label10.TabIndex = 739;
            this.label10.Text = "                ارصــدة اول المـــدة(ع/م)  ";
            // 
            // Grd_Acc_Details
            // 
            this.Grd_Acc_Details.AllowUserToAddRows = false;
            this.Grd_Acc_Details.AllowUserToDeleteRows = false;
            this.Grd_Acc_Details.AllowUserToResizeColumns = false;
            this.Grd_Acc_Details.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Acc_Details.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Acc_Details.ColumnHeadersHeight = 41;
            this.Grd_Acc_Details.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.Grd_Acc_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13});
            this.Grd_Acc_Details.Location = new System.Drawing.Point(11, 128);
            this.Grd_Acc_Details.Name = "Grd_Acc_Details";
            this.Grd_Acc_Details.ReadOnly = true;
            this.Grd_Acc_Details.RowHeadersWidth = 10;
            this.Grd_Acc_Details.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Acc_Details.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Details.Size = new System.Drawing.Size(1076, 132);
            this.Grd_Acc_Details.TabIndex = 5;
            this.Grd_Acc_Details.SelectionChanged += new System.EventHandler(this.Grd_Acc_Details_SelectionChanged);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_no";
            this.Column6.HeaderText = "رقم الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Acc_Aname";
            this.Column7.HeaderText = "الحســـــــاب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 175;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "DOloc_Amount";
            dataGridViewCellStyle6.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column8.HeaderText = "مديــــن";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 131;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "COloc_Amount";
            dataGridViewCellStyle7.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column9.HeaderText = "دائــــن";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Width = 131;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "DPloc_Amount";
            dataGridViewCellStyle8.Format = "N3";
            this.Column10.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column10.HeaderText = "مديــــن";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column10.Width = 131;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "CPloc_Amount";
            dataGridViewCellStyle9.Format = "N3";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column11.HeaderText = "دائــــن";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column11.Width = 131;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "DEloc_Amount";
            dataGridViewCellStyle10.Format = "N3";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column12.HeaderText = "مديــــن";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column12.Width = 131;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "CEloc_Amount";
            dataGridViewCellStyle11.Format = "N3";
            this.Column13.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column13.HeaderText = "دائــــن";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column13.Width = 131;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 423);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel3.TabIndex = 749;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Maroon;
            this.label11.Location = new System.Drawing.Point(13, 413);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(284, 14);
            this.label11.TabIndex = 752;
            this.label11.Text = "تفاصيل الحراكات الماليه للحساب المحدد اعلاه....";
            // 
            // Grd_Vo_Details
            // 
            this.Grd_Vo_Details.AllowUserToAddRows = false;
            this.Grd_Vo_Details.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Vo_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Vo_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Vo_Details.ColumnHeadersHeight = 25;
            this.Grd_Vo_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column22,
            this.Column23,
            this.DataGridViewTextBoxColumn2,
            this.DataGridViewTextBoxColumn3,
            this.DataGridViewTextBoxColumn7,
            this.DataGridViewTextBoxColumn5,
            this.DataGridViewTextBoxColumn6,
            this.DataGridViewTextBoxColumn1,
            this.DataGridViewTextBoxColumn4,
            this.Column14,
            this.Column15,
            this.DataGridViewTextBoxColumn8});
            this.Grd_Vo_Details.Location = new System.Drawing.Point(11, 433);
            this.Grd_Vo_Details.Name = "Grd_Vo_Details";
            this.Grd_Vo_Details.ReadOnly = true;
            this.Grd_Vo_Details.RowHeadersWidth = 10;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Vo_Details.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_Vo_Details.Size = new System.Drawing.Size(1076, 90);
            this.Grd_Vo_Details.TabIndex = 12;
            this.Grd_Vo_Details.SelectionChanged += new System.EventHandler(this.Grd_Vo_Details_SelectionChanged);
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "DLoc_Amount";
            dataGridViewCellStyle15.Format = "N3";
            this.Column22.DefaultCellStyle = dataGridViewCellStyle15;
            this.Column22.HeaderText = "مديــن ـ(ع.م)ـ";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column22.Width = 150;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "CLoc_Amount";
            dataGridViewCellStyle16.Format = "N3";
            this.Column23.DefaultCellStyle = dataGridViewCellStyle16;
            this.Column23.HeaderText = "دائــن ـ(ع.م)ـ";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column23.Width = 150;
            // 
            // DataGridViewTextBoxColumn2
            // 
            this.DataGridViewTextBoxColumn2.DataPropertyName = "Acust_Name";
            this.DataGridViewTextBoxColumn2.HeaderText = "الثانـــوي";
            this.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2";
            this.DataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // DataGridViewTextBoxColumn3
            // 
            this.DataGridViewTextBoxColumn3.DataPropertyName = "AOPER_NAME";
            this.DataGridViewTextBoxColumn3.HeaderText = "العملية";
            this.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3";
            this.DataGridViewTextBoxColumn3.ReadOnly = true;
            this.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DataGridViewTextBoxColumn7
            // 
            this.DataGridViewTextBoxColumn7.DataPropertyName = "VO_NO";
            this.DataGridViewTextBoxColumn7.HeaderText = "رقم السند";
            this.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7";
            this.DataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // DataGridViewTextBoxColumn5
            // 
            this.DataGridViewTextBoxColumn5.DataPropertyName = "DFor_Amount";
            dataGridViewCellStyle17.Format = "N3";
            this.DataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle17;
            this.DataGridViewTextBoxColumn5.HeaderText = "مدين ـ(ع.ص)ـ";
            this.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5";
            this.DataGridViewTextBoxColumn5.ReadOnly = true;
            this.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DataGridViewTextBoxColumn5.Width = 150;
            // 
            // DataGridViewTextBoxColumn6
            // 
            this.DataGridViewTextBoxColumn6.DataPropertyName = "CFor_Amount";
            dataGridViewCellStyle18.Format = "N3";
            this.DataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle18;
            this.DataGridViewTextBoxColumn6.HeaderText = "دائن ـ(ع.ص)ـ";
            this.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6";
            this.DataGridViewTextBoxColumn6.ReadOnly = true;
            this.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DataGridViewTextBoxColumn6.Width = 150;
            // 
            // DataGridViewTextBoxColumn1
            // 
            this.DataGridViewTextBoxColumn1.DataPropertyName = "Cur_Aname";
            this.DataGridViewTextBoxColumn1.HeaderText = "الـعـمـلـة";
            this.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1";
            this.DataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // DataGridViewTextBoxColumn4
            // 
            this.DataGridViewTextBoxColumn4.DataPropertyName = "Real_Price";
            dataGridViewCellStyle19.Format = "N7";
            this.DataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle19;
            this.DataGridViewTextBoxColumn4.HeaderText = "السـعر الفعلـي";
            this.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4";
            this.DataGridViewTextBoxColumn4.ReadOnly = true;
            this.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Create_Nrec_date";
            this.Column14.HeaderText = "تاريخ تنظيم السجلات";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 120;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "C_DATE";
            this.Column15.HeaderText = "تاريخ تنظيم السند";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 150;
            // 
            // DataGridViewTextBoxColumn8
            // 
            this.DataGridViewTextBoxColumn8.DataPropertyName = "Notes";
            this.DataGridViewTextBoxColumn8.HeaderText = "التفاصـــيل";
            this.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8";
            this.DataGridViewTextBoxColumn8.ReadOnly = true;
            this.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DataGridViewTextBoxColumn8.Width = 250;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(5, 638);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel8.TabIndex = 754;
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(547, 642);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(121, 24);
            this.Btn_Ext.TabIndex = 17;
            this.Btn_Ext.Text = "انـهـاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Ext_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(12, 111);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(114, 14);
            this.label16.TabIndex = 761;
            this.label16.Text = "ارصدة تفصيليــة....";
            // 
            // Grd_SumAmount_Acc
            // 
            this.Grd_SumAmount_Acc.AllowUserToAddRows = false;
            this.Grd_SumAmount_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_SumAmount_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_SumAmount_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_SumAmount_Acc.ColumnHeadersHeight = 25;
            this.Grd_SumAmount_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column35,
            this.Column36,
            this.Column37});
            this.Grd_SumAmount_Acc.Location = new System.Drawing.Point(10, 299);
            this.Grd_SumAmount_Acc.Name = "Grd_SumAmount_Acc";
            this.Grd_SumAmount_Acc.ReadOnly = true;
            this.Grd_SumAmount_Acc.RowHeadersWidth = 10;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_SumAmount_Acc.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.Grd_SumAmount_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_SumAmount_Acc.Size = new System.Drawing.Size(1076, 91);
            this.Grd_SumAmount_Acc.TabIndex = 762;
            this.Grd_SumAmount_Acc.SelectionChanged += new System.EventHandler(this.Grd_SumAmount_Acc_SelectionChanged);
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "Acust_Name";
            this.Column32.HeaderText = "الثانـــوي";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.Width = 270;
            // 
            // Column33
            // 
            this.Column33.DataPropertyName = "DLoc_Amount";
            dataGridViewCellStyle23.Format = "N3";
            this.Column33.DefaultCellStyle = dataGridViewCellStyle23;
            this.Column33.HeaderText = "الرصيد مدين ـ(ع.م)ـ";
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            this.Column33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column33.Width = 160;
            // 
            // Column34
            // 
            this.Column34.DataPropertyName = "CLoc_Amount";
            dataGridViewCellStyle24.Format = "N3";
            this.Column34.DefaultCellStyle = dataGridViewCellStyle24;
            this.Column34.HeaderText = " الرصيد دائــن ـ(ع.م)ـ";
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            this.Column34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column34.Width = 160;
            // 
            // Column35
            // 
            this.Column35.DataPropertyName = "DFor_Amount";
            dataGridViewCellStyle25.Format = "N3";
            this.Column35.DefaultCellStyle = dataGridViewCellStyle25;
            this.Column35.HeaderText = " الرصيد مدين ـ(ع.ص)ـ";
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            this.Column35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column35.Width = 160;
            // 
            // Column36
            // 
            this.Column36.DataPropertyName = "CFor_Amount";
            dataGridViewCellStyle26.Format = "N3";
            this.Column36.DefaultCellStyle = dataGridViewCellStyle26;
            this.Column36.HeaderText = " الرصيد دائن ـ(ع.ص)ـ";
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            this.Column36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column36.Width = 160;
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "Cur_Aname";
            this.Column37.HeaderText = "الـعـمـلـة الاصلية";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            this.Column37.Width = 150;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(10, 281);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(149, 14);
            this.label3.TabIndex = 764;
            this.label3.Text = "تحليل الحساب المحدد....";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(4, 291);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel5.TabIndex = 763;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(427, 642);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(121, 24);
            this.AddBtn.TabIndex = 769;
            this.AddBtn.Text = "طباعة وتصدير";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // Grd_vo_details2
            // 
            this.Grd_vo_details2.AllowUserToAddRows = false;
            this.Grd_vo_details2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_vo_details2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle28;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_vo_details2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.Grd_vo_details2.ColumnHeadersHeight = 25;
            this.Grd_vo_details2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column19,
            this.Column18,
            this.Column38,
            this.Column20,
            this.Column21,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column16,
            this.Column17});
            this.Grd_vo_details2.Location = new System.Drawing.Point(12, 545);
            this.Grd_vo_details2.Name = "Grd_vo_details2";
            this.Grd_vo_details2.ReadOnly = true;
            this.Grd_vo_details2.RowHeadersWidth = 10;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_vo_details2.RowsDefaultCellStyle = dataGridViewCellStyle35;
            this.Grd_vo_details2.Size = new System.Drawing.Size(1076, 90);
            this.Grd_vo_details2.TabIndex = 770;
            this.Grd_vo_details2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Grd_vo_details2_CellFormatting);
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "DLoc_Amount";
            dataGridViewCellStyle30.Format = "N3";
            this.Column19.DefaultCellStyle = dataGridViewCellStyle30;
            this.Column19.HeaderText = "مديــن ـ(ع.م)ـ";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column19.Width = 150;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "CLoc_Amount";
            dataGridViewCellStyle31.Format = "N3";
            this.Column18.DefaultCellStyle = dataGridViewCellStyle31;
            this.Column18.HeaderText = "دائــن ـ(ع.م)ـ";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column18.Width = 150;
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "Acc_AName";
            this.Column38.HeaderText = "الحســــاب";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            this.Column38.Width = 150;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "Acust_Name";
            this.Column20.HeaderText = "الثانـــوي";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 150;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "AOPER_NAME";
            this.Column21.HeaderText = "العملية";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column21.Width = 120;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "VO_NO";
            this.Column24.HeaderText = "رقم السند";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "DFor_Amount";
            dataGridViewCellStyle32.Format = "N3";
            this.Column25.DefaultCellStyle = dataGridViewCellStyle32;
            this.Column25.HeaderText = "مدين ـ(ع.ص)ـ";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column25.Width = 150;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "CFor_Amount";
            dataGridViewCellStyle33.Format = "N3";
            this.Column26.DefaultCellStyle = dataGridViewCellStyle33;
            this.Column26.HeaderText = "دائن ـ(ع.ص)ـ";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column26.Width = 150;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "Cur_Aname";
            this.Column27.HeaderText = "الـعـمـلـة";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "Real_Price";
            dataGridViewCellStyle34.Format = "N7";
            this.Column28.DefaultCellStyle = dataGridViewCellStyle34;
            this.Column28.HeaderText = "السـعر الفعلـي";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "Create_Nrec_date";
            this.Column29.HeaderText = "تاريخ تنظيم السجلات";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.Width = 120;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "C_DATE";
            this.Column30.HeaderText = "تاريخ تنظيم السند";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Width = 150;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "Notes";
            this.Column31.HeaderText = "التفاصـــيل";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column31.Width = 250;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "cust_id";
            this.Column16.HeaderText = "Column16";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Visible = false;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Acc_Id";
            this.Column17.HeaderText = "Column17";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(13, 526);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(201, 14);
            this.label9.TabIndex = 772;
            this.label9.Text = "تفاصيل الحركات الماليه للقـيــــد....";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(5, 537);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel9.TabIndex = 771;
            // 
            // Txt_SumCfor
            // 
            this.Txt_SumCfor.BackColor = System.Drawing.Color.White;
            this.Txt_SumCfor.Enabled = false;
            this.Txt_SumCfor.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumCfor.Location = new System.Drawing.Point(769, 394);
            this.Txt_SumCfor.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumCfor.Name = "Txt_SumCfor";
            this.Txt_SumCfor.NumberDecimalDigits = 3;
            this.Txt_SumCfor.NumberDecimalSeparator = ".";
            this.Txt_SumCfor.NumberGroupSeparator = ",";
            this.Txt_SumCfor.Size = new System.Drawing.Size(159, 23);
            this.Txt_SumCfor.TabIndex = 768;
            this.Txt_SumCfor.Text = "0.000";
            // 
            // Txt_SumDFor
            // 
            this.Txt_SumDFor.BackColor = System.Drawing.Color.White;
            this.Txt_SumDFor.Enabled = false;
            this.Txt_SumDFor.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumDFor.Location = new System.Drawing.Point(615, 394);
            this.Txt_SumDFor.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumDFor.Name = "Txt_SumDFor";
            this.Txt_SumDFor.NumberDecimalDigits = 3;
            this.Txt_SumDFor.NumberDecimalSeparator = ".";
            this.Txt_SumDFor.NumberGroupSeparator = ",";
            this.Txt_SumDFor.Size = new System.Drawing.Size(151, 23);
            this.Txt_SumDFor.TabIndex = 767;
            this.Txt_SumDFor.Text = "0.000";
            // 
            // Txt_SumDloc
            // 
            this.Txt_SumDloc.BackColor = System.Drawing.Color.White;
            this.Txt_SumDloc.Enabled = false;
            this.Txt_SumDloc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumDloc.Location = new System.Drawing.Point(286, 394);
            this.Txt_SumDloc.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumDloc.Name = "Txt_SumDloc";
            this.Txt_SumDloc.NumberDecimalDigits = 3;
            this.Txt_SumDloc.NumberDecimalSeparator = ".";
            this.Txt_SumDloc.NumberGroupSeparator = ",";
            this.Txt_SumDloc.Size = new System.Drawing.Size(161, 23);
            this.Txt_SumDloc.TabIndex = 765;
            this.Txt_SumDloc.Text = "0.000";
            // 
            // Txt_SumCloc
            // 
            this.Txt_SumCloc.BackColor = System.Drawing.Color.White;
            this.Txt_SumCloc.Enabled = false;
            this.Txt_SumCloc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumCloc.Location = new System.Drawing.Point(450, 394);
            this.Txt_SumCloc.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumCloc.Name = "Txt_SumCloc";
            this.Txt_SumCloc.NumberDecimalDigits = 3;
            this.Txt_SumCloc.NumberDecimalSeparator = ".";
            this.Txt_SumCloc.NumberGroupSeparator = ",";
            this.Txt_SumCloc.Size = new System.Drawing.Size(162, 23);
            this.Txt_SumCloc.TabIndex = 766;
            this.Txt_SumCloc.Text = "0.000";
            // 
            // TxtPC_LocAmount2
            // 
            this.TxtPC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtPC_LocAmount2.Enabled = false;
            this.TxtPC_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPC_LocAmount2.Location = new System.Drawing.Point(670, 262);
            this.TxtPC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPC_LocAmount2.Name = "TxtPC_LocAmount2";
            this.TxtPC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtPC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtPC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtPC_LocAmount2.Size = new System.Drawing.Size(137, 23);
            this.TxtPC_LocAmount2.TabIndex = 9;
            this.TxtPC_LocAmount2.Text = "0.000";
            // 
            // TxtED_LocAmount2
            // 
            this.TxtED_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtED_LocAmount2.Enabled = false;
            this.TxtED_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtED_LocAmount2.Location = new System.Drawing.Point(811, 262);
            this.TxtED_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtED_LocAmount2.Name = "TxtED_LocAmount2";
            this.TxtED_LocAmount2.NumberDecimalDigits = 3;
            this.TxtED_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtED_LocAmount2.NumberGroupSeparator = ",";
            this.TxtED_LocAmount2.Size = new System.Drawing.Size(131, 23);
            this.TxtED_LocAmount2.TabIndex = 10;
            this.TxtED_LocAmount2.Text = "0.000";
            // 
            // TxtEC_LocAmount2
            // 
            this.TxtEC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtEC_LocAmount2.Enabled = false;
            this.TxtEC_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEC_LocAmount2.Location = new System.Drawing.Point(943, 262);
            this.TxtEC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtEC_LocAmount2.Name = "TxtEC_LocAmount2";
            this.TxtEC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtEC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtEC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtEC_LocAmount2.Size = new System.Drawing.Size(142, 23);
            this.TxtEC_LocAmount2.TabIndex = 11;
            this.TxtEC_LocAmount2.Text = "0.000";
            // 
            // TxtPD_LocAmount2
            // 
            this.TxtPD_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtPD_LocAmount2.Enabled = false;
            this.TxtPD_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPD_LocAmount2.Location = new System.Drawing.Point(530, 262);
            this.TxtPD_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPD_LocAmount2.Name = "TxtPD_LocAmount2";
            this.TxtPD_LocAmount2.NumberDecimalDigits = 3;
            this.TxtPD_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtPD_LocAmount2.NumberGroupSeparator = ",";
            this.TxtPD_LocAmount2.Size = new System.Drawing.Size(137, 23);
            this.TxtPD_LocAmount2.TabIndex = 8;
            this.TxtPD_LocAmount2.Text = "0.000";
            // 
            // TxtOD_LocAmount2
            // 
            this.TxtOD_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtOD_LocAmount2.Enabled = false;
            this.TxtOD_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOD_LocAmount2.Location = new System.Drawing.Point(240, 262);
            this.TxtOD_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOD_LocAmount2.Name = "TxtOD_LocAmount2";
            this.TxtOD_LocAmount2.NumberDecimalDigits = 3;
            this.TxtOD_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtOD_LocAmount2.NumberGroupSeparator = ",";
            this.TxtOD_LocAmount2.Size = new System.Drawing.Size(138, 23);
            this.TxtOD_LocAmount2.TabIndex = 6;
            this.TxtOD_LocAmount2.Text = "0.000";
            // 
            // TxtOC_LocAmount2
            // 
            this.TxtOC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtOC_LocAmount2.Enabled = false;
            this.TxtOC_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOC_LocAmount2.Location = new System.Drawing.Point(382, 262);
            this.TxtOC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOC_LocAmount2.Name = "TxtOC_LocAmount2";
            this.TxtOC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtOC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtOC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtOC_LocAmount2.Size = new System.Drawing.Size(143, 23);
            this.TxtOC_LocAmount2.TabIndex = 7;
            this.TxtOC_LocAmount2.Text = "0.000";
            // 
            // Trial_Balance_Main_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 669);
            this.Controls.Add(this.Grd_vo_details2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.Txt_SumCfor);
            this.Controls.Add(this.Txt_SumDFor);
            this.Controls.Add(this.Txt_SumDloc);
            this.Controls.Add(this.Txt_SumCloc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Grd_SumAmount_Acc);
            this.Controls.Add(this.TxtPC_LocAmount2);
            this.Controls.Add(this.TxtED_LocAmount2);
            this.Controls.Add(this.TxtEC_LocAmount2);
            this.Controls.Add(this.TxtPD_LocAmount2);
            this.Controls.Add(this.TxtOD_LocAmount2);
            this.Controls.Add(this.TxtOC_LocAmount2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Grd_Vo_Details);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Grd_Acc_Details);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Grd_Terminals);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Trial_Balance_Main_Details";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "213";
            this.Text = "ميزان مراجعة تفصيلي ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Trial_Balance_Main_FormClosed);
            this.Load += new System.EventHandler(this.Trial_Balance_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Terminals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Vo_Details)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_SumAmount_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_vo_details2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox TxtToDate;
        private System.Windows.Forms.MaskedTextBox TxtFromDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView Grd_Acc_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView Grd_Vo_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOC_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOD_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPD_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtEC_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtED_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPC_LocAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridView Grd_SumAmount_Acc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumCfor;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumDFor;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumDloc;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumCloc;
        private System.Windows.Forms.Button AddBtn;
        public System.Windows.Forms.DataGridView Grd_Terminals;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridView Grd_vo_details2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
    }
}