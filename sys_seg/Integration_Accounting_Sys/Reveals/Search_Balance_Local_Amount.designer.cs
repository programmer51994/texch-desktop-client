﻿namespace Integration_Accounting_Sys
{
    partial class Search_Balance_Local_Amount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CboDisplay = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Cbo_Level = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHK_Zero_Balancing = new System.Windows.Forms.CheckBox();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHK_FTB_Flag = new System.Windows.Forms.CheckBox();
            this.Grd_Acc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk_Acc = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.TxtAcc_Name = new System.Windows.Forms.TextBox();
            this.Grd_Cur = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cur_Id = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk_Cur = new System.Windows.Forms.CheckBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.TxtCur_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_years = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_year = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(10, 39);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel2.TabIndex = 530;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(124, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(247, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(486, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(132, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(430, 10);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 527;
            this.label2.Text = "التاريـخ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 14);
            this.label1.TabIndex = 526;
            this.label1.Text = "اسم المستخــدم:";
            // 
            // CboDisplay
            // 
            this.CboDisplay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDisplay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDisplay.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDisplay.FormattingEnabled = true;
            this.CboDisplay.Items.AddRange(new object[] {
            "حساب تفصيلي",
            "حساب تجميعي"});
            this.CboDisplay.Location = new System.Drawing.Point(101, 137);
            this.CboDisplay.Name = "CboDisplay";
            this.CboDisplay.Size = new System.Drawing.Size(171, 24);
            this.CboDisplay.TabIndex = 4;
            this.CboDisplay.SelectedIndexChanged += new System.EventHandler(this.CboDisplay_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(7, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 14);
            this.label4.TabIndex = 574;
            this.label4.Text = "اسلوب العرض:";
            // 
            // Cbo_Level
            // 
            this.Cbo_Level.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Level.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Level.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Level.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Level.FormattingEnabled = true;
            this.Cbo_Level.Location = new System.Drawing.Point(433, 137);
            this.Cbo_Level.Name = "Cbo_Level";
            this.Cbo_Level.Size = new System.Drawing.Size(172, 24);
            this.Cbo_Level.TabIndex = 5;
            this.Cbo_Level.SelectedIndexChanged += new System.EventHandler(this.Cbo_Level_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(364, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 576;
            this.label6.Text = "المستوى:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 39);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 438);
            this.flowLayoutPanel1.TabIndex = 585;
            // 
            // CHK_Zero_Balancing
            // 
            this.CHK_Zero_Balancing.AutoSize = true;
            this.CHK_Zero_Balancing.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_Zero_Balancing.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_Zero_Balancing.Location = new System.Drawing.Point(16, 437);
            this.CHK_Zero_Balancing.Name = "CHK_Zero_Balancing";
            this.CHK_Zero_Balancing.Size = new System.Drawing.Size(174, 18);
            this.CHK_Zero_Balancing.TabIndex = 22;
            this.CHK_Zero_Balancing.Text = "استبعاد الارصدة المصفرة.";
            this.CHK_Zero_Balancing.UseVisualStyleBackColor = true;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(222, 479);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 27);
            this.AddBtn.TabIndex = 24;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(310, 479);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 27);
            this.ExtBtn.TabIndex = 25;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-9, 31);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(642, 1);
            this.flowLayoutPanel3.TabIndex = 589;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(617, 40);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 437);
            this.flowLayoutPanel4.TabIndex = 590;
            // 
            // CHK_FTB_Flag
            // 
            this.CHK_FTB_Flag.AutoSize = true;
            this.CHK_FTB_Flag.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_FTB_Flag.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_FTB_Flag.Location = new System.Drawing.Point(16, 455);
            this.CHK_FTB_Flag.Name = "CHK_FTB_Flag";
            this.CHK_FTB_Flag.Size = new System.Drawing.Size(135, 18);
            this.CHK_FTB_Flag.TabIndex = 23;
            this.CHK_FTB_Flag.Text = "احتساب اول المدة.";
            this.CHK_FTB_Flag.UseVisualStyleBackColor = true;
            // 
            // Grd_Acc
            // 
            this.Grd_Acc.AllowUserToAddRows = false;
            this.Grd_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_Acc.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Acc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_Acc.ColumnHeadersHeight = 45;
            this.Grd_Acc.ColumnHeadersVisible = false;
            this.Grd_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column16});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Acc.DefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_Acc.Location = new System.Drawing.Point(384, 185);
            this.Grd_Acc.Name = "Grd_Acc";
            this.Grd_Acc.ReadOnly = true;
            this.Grd_Acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Acc.RowHeadersVisible = false;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.Grd_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Acc.Size = new System.Drawing.Size(222, 110);
            this.Grd_Acc.TabIndex = 13;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز العملة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Acc_Aname";
            this.Column16.HeaderText = "اسم العملة";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 160;
            // 
            // Grd_Acc_Id
            // 
            this.Grd_Acc_Id.AllowUserToAddRows = false;
            this.Grd_Acc_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.Grd_Acc_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Acc_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Acc_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.Grd_Acc_Id.ColumnHeadersHeight = 45;
            this.Grd_Acc_Id.ColumnHeadersVisible = false;
            this.Grd_Acc_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.Column15});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Acc_Id.DefaultCellStyle = dataGridViewCellStyle27;
            this.Grd_Acc_Id.Location = new System.Drawing.Point(48, 185);
            this.Grd_Acc_Id.Name = "Grd_Acc_Id";
            this.Grd_Acc_Id.ReadOnly = true;
            this.Grd_Acc_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Acc_Id.RowHeadersVisible = false;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.Grd_Acc_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Acc_Id.Size = new System.Drawing.Size(222, 110);
            this.Grd_Acc_Id.TabIndex = 8;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "رمز العملة";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 70;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Acc_Aname";
            this.Column15.HeaderText = "اسم العملة";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 150;
            // 
            // Chk_Acc
            // 
            this.Chk_Acc.AutoSize = true;
            this.Chk_Acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Acc.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Acc.Location = new System.Drawing.Point(13, 163);
            this.Chk_Acc.Name = "Chk_Acc";
            this.Chk_Acc.Size = new System.Drawing.Size(85, 18);
            this.Chk_Acc.TabIndex = 6;
            this.Chk_Acc.Text = "الحســـاب:";
            this.Chk_Acc.UseVisualStyleBackColor = true;
            this.Chk_Acc.CheckedChanged += new System.EventHandler(this.Chk_Acc_CheckedChanged);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button5.Location = new System.Drawing.Point(283, 262);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(81, 22);
            this.button5.TabIndex = 12;
            this.button5.Text = "<<";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button6.Location = new System.Drawing.Point(283, 240);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(81, 22);
            this.button6.TabIndex = 11;
            this.button6.Text = "<";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button7.Location = new System.Drawing.Point(283, 218);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(81, 22);
            this.button7.TabIndex = 10;
            this.button7.Text = ">>";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button8.Location = new System.Drawing.Point(283, 196);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(81, 22);
            this.button8.TabIndex = 9;
            this.button8.Text = ">";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // TxtAcc_Name
            // 
            this.TxtAcc_Name.BackColor = System.Drawing.Color.White;
            this.TxtAcc_Name.Enabled = false;
            this.TxtAcc_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtAcc_Name.Location = new System.Drawing.Point(100, 162);
            this.TxtAcc_Name.Name = "TxtAcc_Name";
            this.TxtAcc_Name.Size = new System.Drawing.Size(170, 22);
            this.TxtAcc_Name.TabIndex = 7;
            this.TxtAcc_Name.TextChanged += new System.EventHandler(this.TxtAcc_Name_TextChanged);
            // 
            // Grd_Cur
            // 
            this.Grd_Cur.AllowUserToAddRows = false;
            this.Grd_Cur.AllowUserToDeleteRows = false;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle29;
            this.Grd_Cur.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cur.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.Grd_Cur.ColumnHeadersHeight = 45;
            this.Grd_Cur.ColumnHeadersVisible = false;
            this.Grd_Cur.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5});
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cur.DefaultCellStyle = dataGridViewCellStyle31;
            this.Grd_Cur.Location = new System.Drawing.Point(384, 319);
            this.Grd_Cur.Name = "Grd_Cur";
            this.Grd_Cur.ReadOnly = true;
            this.Grd_Cur.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cur.RowHeadersVisible = false;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this.Grd_Cur.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cur.Size = new System.Drawing.Size(222, 112);
            this.Grd_Cur.TabIndex = 21;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Cur_id";
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "رمز العملة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 60;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Cur_Aname";
            this.Column5.HeaderText = "اسم العملة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 160;
            // 
            // Grd_Cur_Id
            // 
            this.Grd_Cur_Id.AllowUserToAddRows = false;
            this.Grd_Cur_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle33;
            this.Grd_Cur_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cur_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cur_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.Grd_Cur_Id.ColumnHeadersHeight = 45;
            this.Grd_Cur_Id.ColumnHeadersVisible = false;
            this.Grd_Cur_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cur_Id.DefaultCellStyle = dataGridViewCellStyle35;
            this.Grd_Cur_Id.Location = new System.Drawing.Point(49, 319);
            this.Grd_Cur_Id.Name = "Grd_Cur_Id";
            this.Grd_Cur_Id.ReadOnly = true;
            this.Grd_Cur_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cur_Id.RowHeadersVisible = false;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.Grd_Cur_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cur_Id.Size = new System.Drawing.Size(222, 112);
            this.Grd_Cur_Id.TabIndex = 16;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cur_id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رمز العملة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 70;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Cur_Aname";
            this.Column2.HeaderText = "اسم العملة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Chk_Cur
            // 
            this.Chk_Cur.AutoSize = true;
            this.Chk_Cur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Cur.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Cur.Location = new System.Drawing.Point(12, 297);
            this.Chk_Cur.Name = "Chk_Cur";
            this.Chk_Cur.Size = new System.Drawing.Size(84, 18);
            this.Chk_Cur.TabIndex = 14;
            this.Chk_Cur.Text = "العملــــــة:";
            this.Chk_Cur.UseVisualStyleBackColor = true;
            this.Chk_Cur.CheckedChanged += new System.EventHandler(this.Chk_Cur_CheckedChanged);
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button9.Location = new System.Drawing.Point(283, 395);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(81, 22);
            this.button9.TabIndex = 20;
            this.button9.Text = "<<";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Enabled = false;
            this.button10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button10.Location = new System.Drawing.Point(283, 373);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(81, 22);
            this.button10.TabIndex = 19;
            this.button10.Text = "<";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Enabled = false;
            this.button11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button11.Location = new System.Drawing.Point(283, 351);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(81, 22);
            this.button11.TabIndex = 18;
            this.button11.Text = ">>";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Enabled = false;
            this.button12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button12.Location = new System.Drawing.Point(283, 329);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(81, 22);
            this.button12.TabIndex = 17;
            this.button12.Text = ">";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // TxtCur_Name
            // 
            this.TxtCur_Name.BackColor = System.Drawing.Color.White;
            this.TxtCur_Name.Enabled = false;
            this.TxtCur_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCur_Name.Location = new System.Drawing.Point(100, 296);
            this.TxtCur_Name.Name = "TxtCur_Name";
            this.TxtCur_Name.Size = new System.Drawing.Size(171, 22);
            this.TxtCur_Name.TabIndex = 15;
            this.TxtCur_Name.TextChanged += new System.EventHandler(this.TxtCur_Name_TextChanged);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(10, 476);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel5.TabIndex = 609;
            // 
            // Grd_years
            // 
            this.Grd_years.AllowUserToAddRows = false;
            this.Grd_years.AllowUserToDeleteRows = false;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            this.Grd_years.BackgroundColor = System.Drawing.Color.White;
            this.Grd_years.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_years.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_years.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.Grd_years.ColumnHeadersHeight = 45;
            this.Grd_years.ColumnHeadersVisible = false;
            this.Grd_years.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.Column3});
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_years.DefaultCellStyle = dataGridViewCellStyle39;
            this.Grd_years.Location = new System.Drawing.Point(15, 66);
            this.Grd_years.Name = "Grd_years";
            this.Grd_years.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_years.RowHeadersVisible = false;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this.Grd_years.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_years.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_years.Size = new System.Drawing.Size(317, 48);
            this.Grd_years.TabIndex = 927;
           // this.Grd_years.SelectionChanged += new System.EventHandler(this.Grd_years_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FromNrec_date";
            this.dataGridViewTextBoxColumn2.HeaderText = "التاريخ من";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 175;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "TONrec_date";
            this.Column3.HeaderText = "التاريخ الى";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // cbo_year
            // 
            this.cbo_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_year.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_year.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_year.FormattingEnabled = true;
            this.cbo_year.Items.AddRange(new object[] {
            "البيانات الحالية",
            "البيانات المرحلة"});
            this.cbo_year.Location = new System.Drawing.Point(103, 41);
            this.cbo_year.Name = "cbo_year";
            this.cbo_year.Size = new System.Drawing.Size(230, 24);
            this.cbo_year.TabIndex = 925;
            this.cbo_year.SelectedIndexChanged += new System.EventHandler(this.cbo_year_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(10, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 14);
            this.label7.TabIndex = 926;
            this.label7.Text = "نـــــوع البحـــث:";
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "dd/mm/yyyy";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(236, 116);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(96, 20);
            this.TxtToDate.TabIndex = 924;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "dd/mm/yyyy";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(101, 116);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(96, 20);
            this.TxtFromDate.TabIndex = 923;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(12, 119);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(84, 14);
            this.label8.TabIndex = 922;
            this.label8.Text = "التــاريــخ من :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(197, 119);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(39, 14);
            this.label9.TabIndex = 921;
            this.label9.Text = "إلـى :";
            // 
            // Search_Balance_Local_Amount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 508);
            this.Controls.Add(this.Grd_years);
            this.Controls.Add(this.cbo_year);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Grd_Cur);
            this.Controls.Add(this.Grd_Cur_Id);
            this.Controls.Add(this.Chk_Cur);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.TxtCur_Name);
            this.Controls.Add(this.Grd_Acc);
            this.Controls.Add(this.Grd_Acc_Id);
            this.Controls.Add(this.Chk_Acc);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.TxtAcc_Name);
            this.Controls.Add(this.CHK_FTB_Flag);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.CHK_Zero_Balancing);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Cbo_Level);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CboDisplay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Search_Balance_Local_Amount";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "210";
            this.Text = "Search_Balance_Local_Amount";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Search_Balance_Local_Amount_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Search_Trial_Balance_FormClosed);
            this.Load += new System.EventHandler(this.Reveal_Balance_Local_Amount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboDisplay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Cbo_Level;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox CHK_Zero_Balancing;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.CheckBox CHK_FTB_Flag;
        private System.Windows.Forms.DataGridView Grd_Acc;
        private System.Windows.Forms.DataGridView Grd_Acc_Id;
        private System.Windows.Forms.CheckBox Chk_Acc;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox TxtAcc_Name;
        private System.Windows.Forms.DataGridView Grd_Cur;
        private System.Windows.Forms.DataGridView Grd_Cur_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.CheckBox Chk_Cur;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox TxtCur_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridView Grd_years;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.ComboBox cbo_year;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}