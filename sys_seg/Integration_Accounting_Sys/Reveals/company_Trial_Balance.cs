﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branchs_Forms.Branch_Report;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys.Reveals
{
    public partial class company_Trial_Balance : Form
    {
        public static string from_date = "";
        public static string to_date = "";

        public company_Trial_Balance()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            // connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }

        private void company_Trial_Balance_Load(object sender, EventArgs e)
        {

            connection.SqlExec("Exec Search_Trial_Balance ", "SearchTrial_Balance_Tbl");
            Cbo_Level.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl1"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "AAcc_Level" : "EAcc_Level";
        }

        private void CboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (CboDisplay.SelectedIndex == 0)
            { Cbo_Level.Enabled = true; }
            else
            { Cbo_Level.Enabled = false; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CboDisplay.SelectedIndex == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "قيد الانجاز" : "", MyGeneral_Lib.LblCap);
                return;

            }
            //AccChange = false;
            TxtFromDate.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            TxtToDate.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            int FromDate = 0;
            int ToDate = 0;
            FromDate = Convert.ToInt32(TxtFromDate.Text);
            ToDate = Convert.ToInt32(TxtToDate.Text);



            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Trail_Balance_Main_Comp]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", FromDate);
                connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", ToDate);
                connection.SQLCMD.Parameters.AddWithValue("@FTB_Flag", CHK_FTB.Checked);
                connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Cbo_Level.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@Op_Acc_Flag", Convert.ToByte(CHK_OP_ACC.Checked));
                connection.SQLCMD.Parameters.AddWithValue("@View_Style", CboDisplay.SelectedIndex);
                connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.Add("@RealFromDate", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@RealFromDate"].Direction = ParameterDirection.Output;
                connection.SQLCMD.Parameters.Add("@RealToDate", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@RealToDate"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Trail_Balance_Tbl");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Trail_Balance_Tbl1");
                obj.Close();





                connection.SQLCS.Close();
                from_date = connection.SQLCMD.Parameters["@RealFromDate"].Value.ToString();
                to_date = connection.SQLCMD.Parameters["@RealToDate"].Value.ToString();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();


                Company_Trial_Balance_Main_Details Frm = new Company_Trial_Balance_Main_Details();
                this.Visible = false;
                Frm.ShowDialog(this);
                this.Visible = true;
                this.Enabled = true;
            }



            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
        }
    }
}
                  
                
