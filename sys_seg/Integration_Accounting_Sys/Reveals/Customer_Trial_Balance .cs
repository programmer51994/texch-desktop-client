﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;
using Integration_Accounting_Sys.Reveals;
using System.ComponentModel;
using Integration_Accounting_Sys.Report;


namespace Integration_Accounting_Sys
{
    public partial class Customer_Trial_Balance : Form
    {
        #region Definshion
        bool AccChange = false;
        BindingSource _BS_GRD1 = new BindingSource();
        BindingSource _BS_GRD2 = new BindingSource();
        // public static BindingSource Details_Acc_Bs2 = new BindingSource();
        //public static int T_Id = 0;
        public static string Acc_No = "";

        Int16 CboIsTerminal = 0;
        DataTable DT = new DataTable();
        #endregion
        //---------------------------
        public Customer_Trial_Balance(Int16 IsTerminal)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Customer.AutoGenerateColumns = false;

            CboIsTerminal = IsTerminal;

            #region Enghlish
            if (connection.Lang_id == 1 && CboIsTerminal == 1)
            {
                this.Text = "ميزان مراجعة شامل لجميع الحسابات المحاسبية والثانوية لكل فرع";
            }
            else if (connection.Lang_id == 1 && CboIsTerminal == 2)
            {
                this.Text = "ميزان مراجعة شامل لجميع الحسابات المحاسبية والثانوية";
            }
            else if (connection.Lang_id == 2 && CboIsTerminal == 1)
            { this.Text = "Custermor Trial Balance per Branch"; }
            else if (connection.Lang_id == 2 && CboIsTerminal == 2)
            { this.Text = "Custermor Trial Balance"; }

            if (connection.Lang_id != 1)
            {
                // Column7.DataPropertyName = "Acc_Ename";
                //Column26.DataPropertyName = "ACC_Ename";
                Column2.DataPropertyName = "ETerm_Name";
                Column4.DataPropertyName = "Acc_EName";
                Column6.DataPropertyName = "ECUST_NAME";
                Column7.DataPropertyName = "Cur_Ename";

            }



            #endregion
        }
        //----------------------------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {

            if (CboIsTerminal == 2)
            {

                Grd_Customer.Columns.Remove("Column1");
                Grd_Customer.Columns.Remove("Column2");

            }
            Txt_Real_From.Text = Search_Comany_Trail_Balance.from_date;
            Txt_Real_To.Text = Search_Comany_Trail_Balance.to_date;

            Grd_Customer.DataSource = connection.SQLDS.Tables["Trail_Balance_Tbl"];

        }

        //----------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {

            if (Grd_Customer.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود للطباعة " : " No record to print ", MyGeneral_Lib.LblCap);
                return;
            }

            DataTable Details_Acc_DT = new DataTable();
            if (CboIsTerminal == 1) // لكل الفروع
            {

                Details_Acc_DT = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "T_id", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "CUST_ID", connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "PFor_Amount", "EFor_Amount").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { Grd_Customer };
                DataTable[] Export_DT = { Details_Acc_DT };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

            }
            else if (CboIsTerminal == 2)// شامل
            {
                DataTable acc_details = new DataTable();

                acc_details = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "acc_no", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", "CUST_ID", connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME", connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "PFor_Amount", "EFor_Amount").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { Grd_Customer };
                DataTable[] Export_DT = { acc_details };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }

        }
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Company_Trial_Balance_Main_Details_FormClosed(object sender, FormClosedEventArgs e)
        {
            AccChange = false;
            string[] Used_Tbl = { "Trail_Balance_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            DataTable Customer_Trial_Balance = new DataTable();
            if (CboIsTerminal == 1)
            {
                Customer_Trial_Balance = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "acc_no",
                        "Acc_AName", "Acc_EName", "CUST_ID", "ACUST_NAME", "ECUST_NAME", "Cur_Aname", "Cur_Ename", "OLoc_Amount",
                       "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "EFor_Amount", "PFor_Amount","Aterm_name","Eterm_name").Select().CopyToDataTable();
            }

            if (CboIsTerminal == 2)
            {
                

                Customer_Trial_Balance = connection.SQLDS.Tables["Trail_Balance_Tbl"].DefaultView.ToTable(false, "acc_no",
                     "Acc_AName", "Acc_EName", "CUST_ID", "ACUST_NAME", "ECUST_NAME", "Cur_Aname", "Cur_Ename", "OLoc_Amount",
                    "pLoc_Amount", "Eloc_Amount", "OFor_Amount", "EFor_Amount", "PFor_Amount").Select().CopyToDataTable();
            }
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("From_Date", Txt_Real_From.Text.Trim());
            Dt_Param.Rows.Add("To_Date", Txt_Real_To.Text.Trim());

            Customer_Trial_Balance.TableName = "Customer_Trial_Balance";
            connection.SQLDS.Tables.Add(Customer_Trial_Balance);
            if (CboIsTerminal == 1)
            {
                Customer_Trial_Balance_Comp_Rep ObjRpt = new Customer_Trial_Balance_Comp_Rep();
                Customer_Trial_Balance_Comp_Rep ObjRptEng = new Customer_Trial_Balance_Comp_Rep();
                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            if (CboIsTerminal == 2)
            {
                Customer_Trial_Balance_Rep ObjRpt = new Customer_Trial_Balance_Rep();
                Customer_Trial_Balance_Rep ObjRptEng = new Customer_Trial_Balance_Rep();

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
           
            connection.SQLDS.Tables.Remove("Customer_Trial_Balance");
        }
    }
}