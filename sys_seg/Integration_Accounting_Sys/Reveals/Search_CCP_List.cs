﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Search_CCP_List : Form
    {
        #region Defintion
        String SqlText = "";
        DataTable DT_Cur = new DataTable();
        DataTable DT_Term = new DataTable();
        BindingSource _Bs1 = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        BindingSource _Bs3 = new BindingSource();
        BindingSource _Bs4 = new BindingSource();
        int Cur_id = 0;
        int T_id = 0;
        String Filter = "";
        String Filter1 = "";

        #endregion
        public Search_CCP_List()
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Lst_cur.AutoGenerateColumns = false;
            Lst_Cur_id.AutoGenerateColumns = false;
            Lst_Term.AutoGenerateColumns = false;
            Lst_Term_id.AutoGenerateColumns = false;
            Create_Cur_Tbl();
            Create_Term_Tbl();
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Lst_Cur_id.Columns["Column2"].DataPropertyName = "Cur_Ename";
                Lst_cur.Columns["Column5"].DataPropertyName = "Cur_Ename";
                Lst_Term_id.Columns["Column8"].DataPropertyName = "Ecust_name";
                Lst_Term.Columns["Column11"].DataPropertyName = "Ecust_name";
            }
            #endregion
            #endregion
        }
        //-----------------------------------------------------
        private void Create_Cur_Tbl()
        {
            string[] Column = { "cur_id", "Cur_ANAME", "Cur_ENAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Cur = CustomControls.Custom_DataTable("DT_Cur", Column, DType);
        }
        //-----------------------------------------------------
        private void Create_Term_Tbl()
        {
            string[] Column = { "T_ID", "ACUST_NAME", "ECUST_NAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Term = CustomControls.Custom_DataTable("DT_Term", Column, DType);
        }
        //-----------------------------------------------------
        private void Search_CCP_List_New_Load(object sender, EventArgs e)
        {
            SqlText = "SELECT Cur_ID, Cur_ANAME, Cur_ENAME  FROM  Cur_Tbl where Cur_ID <> 0"
            + " And Cur_ID in ( Select Cur_ID from CCP_LIST union Select Cur_ID from HST_CCP_LIST) order by " + (connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME");
            _Bs1.DataSource = connection.SqlExec(SqlText, "cur_Tbl");

            SqlText = "select B.T_id,A.ACUST_NAME , A.ECUST_NAME "
                    + " From CUSTOMERS A, TERMINALS B "
                    + " Where A.CUST_ID = B.CUST_ID "
                    +" And A.Cust_Flag <>0 "
                    + " Order By " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");
            _Bs2.DataSource = connection.SqlExec(SqlText, "Term_Tbl");

            _Bs3.DataSource = DT_Cur;
            Lst_cur.DataSource = _Bs3;
            _Bs4.DataSource = DT_Term;
            Lst_Term.DataSource = _Bs4;
            Chk_Cur_CheckedChanged(null, null);
            Chk_Term_CheckedChanged(null, null);
        }
        //-----------------------------------------------------
        private void Chk_Cur_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked)
            {
                Lst_Cur_id.DataSource = _Bs1;
                button1.Enabled = true;
                button2.Enabled = true;
                TxtCur_Name.Enabled = true;
            }
            else
            {
                if (DT_Cur.Rows.Count > 0)
                {
                    for (int i = 0; i < DT_Cur.Rows.Count; i++)
                    {
                        DataRow Rows = connection.SQLDS.Tables["cur_Tbl"].NewRow();
                        Rows["cur_id"] = DT_Cur.Rows[i]["cur_id"];
                        Rows["Cur_ANAME"] = DT_Cur.Rows[i]["Cur_ANAME"];
                        Rows["Cur_ENAME"] = DT_Cur.Rows[i]["Cur_ENAME"];
                        connection.SQLDS.Tables["Cur_Tbl"].Rows.Add(Rows);
                        _Bs1.DataSource = connection.SQLDS.Tables["Cur_Tbl"];
                    }
                }

                Lst_Cur_id.DataSource = new DataTable();
                TxtCur_Name.ResetText();
                DT_Cur.Clear();
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                TxtCur_Name.Enabled = false;
            }
        }
        //-----------------------------------------------------
        private void Chk_Term_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Term.Checked)
            {
                Lst_Term_id.DataSource = _Bs2;
                button7.Enabled = true;
                button8.Enabled = true;
                TxtTem_Name.Enabled = true;
            }
            else
            {
                if (DT_Term.Rows.Count > 0)
                {
                    for (int i = 0; i < DT_Term.Rows.Count; i++)
                    {
                        DataRow Rows = connection.SQLDS.Tables["Term_Tbl"].NewRow();
                        Rows["T_ID"] = DT_Term.Rows[i]["T_ID"];
                        Rows["ACUST_NAME"] = DT_Term.Rows[i]["ACUST_NAME"];
                        Rows["ECUST_NAME"] = DT_Term.Rows[i]["ECUST_NAME"];
                        connection.SQLDS.Tables["Term_Tbl"].Rows.Add(Rows);
                        _Bs2.DataSource = connection.SQLDS.Tables["Term_Tbl"];
                    }
                }
                DT_Term.Clear();
                Lst_Term_id.DataSource = new DataTable();
                TxtTem_Name.ResetText();
                button5.Enabled = false;
                button6.Enabled = false;
                button7.Enabled = false;
                button8.Enabled = false;
                TxtTem_Name.Enabled = false;
            }
        }
        //-----------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------------------
        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (int.TryParse(TxtCur_Name.Text, out Cur_id))
                {

                    Filter = "(Cur_id =" + TxtCur_Name.Text + ")";
                }
                else
                {
                    Filter = "(Cur_ANAME like '%" + TxtCur_Name.Text + "%') or (Cur_ENAME like '%" + TxtCur_Name.Text + "%')";
                }

                _Bs1.DataSource = connection.SQLDS.Tables["Cur_tbl"].Select(Filter).CopyToDataTable();
                Lst_Cur_id.DataSource = _Bs1;
            }
            catch
            {
                Lst_Cur_id.DataSource = new BindingSource();
            }
            if (Lst_Cur_id.Rows.Count <= 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }
        //-----------------------------------------------------
        private void TxtTem_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (int.TryParse(TxtTem_Name.Text, out T_id))
                {

                    Filter1 = "(T_id =" + TxtTem_Name.Text + ")";
                }
                else
                {
                    Filter1 = "(Acust_name like '%" + TxtTem_Name.Text + "%') or (Ecust_name like '%" + TxtTem_Name.Text + "%')";
                }

                _Bs2.DataSource = connection.SQLDS.Tables["Term_tbl"].Select(Filter1).CopyToDataTable();
                Lst_Term_id.DataSource = _Bs2;
            }
            catch
            {
                Lst_Term_id.DataSource = new BindingSource();
            }
            if (Lst_Term_id.Rows.Count <= 0)
            {

                button7.Enabled = false;
                button8.Enabled = false;
            }
            else
            {
                button7.Enabled = true;
                button8.Enabled = true;
            }
        }
        //-----------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cur.NewRow();
            row["cur_id"] = ((DataRowView)_Bs1.Current).Row["cur_id"];
            row["Cur_ANAME"] = ((DataRowView)_Bs1.Current).Row["Cur_ANAME"];
            row["Cur_ENAME"] = ((DataRowView)_Bs1.Current).Row["Cur_ENAME"];
            DT_Cur.Rows.Add(row);
            int cur_id = Convert.ToInt16(((DataRowView)_Bs1.Current).Row["cur_id"]);
            DataRow[] DRow = connection.SQLDS.Tables["cur_Tbl"].Select("cur_id=" + cur_id);
            DRow[0].Delete();
            connection.SQLDS.Tables["cur_Tbl"].AcceptChanges();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button3.Enabled = true;
            button4.Enabled = true;
            if (Lst_Cur_id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }
        //-----------------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < connection.SQLDS.Tables["cur_Tbl"].Rows.Count; i++)
            {
                DataRow row = DT_Cur.NewRow();
                row["cur_id"] = connection.SQLDS.Tables["cur_Tbl"].Rows[i]["cur_id"];
                row["Cur_ANAME"] = connection.SQLDS.Tables["cur_Tbl"].Rows[i]["Cur_ANAME"];
                row["Cur_ENAME"] = connection.SQLDS.Tables["cur_Tbl"].Rows[i]["Cur_ENAME"];
                DT_Cur.Rows.Add(row);
            }
            connection.SQLDS.Tables["cur_Tbl"].Clear();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button3.Enabled = true;
            button4.Enabled = true;
            button1.Enabled = false;
            button2.Enabled = false;
        }
        //-----------------------------------------------------
        private void button3_Click(object sender, EventArgs e)
        {
            DataRow row = connection.SQLDS.Tables["cur_Tbl"].NewRow();
            row["cur_id"] = ((DataRowView)_Bs3.Current).Row["cur_id"];
            row["Cur_ANAME"] = ((DataRowView)_Bs3.Current).Row["Cur_ANAME"];
            row["Cur_ENAME"] = ((DataRowView)_Bs3.Current).Row["Cur_ENAME"]; 
            connection.SQLDS.Tables["cur_Tbl"].Rows.Add(row);
            connection.SQLDS.Tables["cur_Tbl"].AcceptChanges();

            DT_Cur.Rows[Convert.ToInt32(Lst_cur.CurrentRow.Index)].Delete();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            if (Lst_cur.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
        }
        //-----------------------------------------------------
        private void button4_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < DT_Cur.Rows.Count; i++)
            {
                DataRow Rows = connection.SQLDS.Tables["cur_Tbl"].NewRow();
                Rows["cur_id"] = DT_Cur.Rows[i]["cur_id"];
                Rows["Cur_ANAME"] = DT_Cur.Rows[i]["Cur_ANAME"];
                Rows["Cur_ENAME"] = DT_Cur.Rows[i]["Cur_ENAME"];
                connection.SQLDS.Tables["cur_Tbl"].Rows.Add(Rows);
            }
            DT_Cur.Rows.Clear();
            connection.SQLDS.Tables["cur_Tbl"].AcceptChanges();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
        }
        //-----------------------------------------------------
        private void button8_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Term.NewRow();

            row["T_ID"] = ((DataRowView)_Bs2.Current).Row["T_ID"];
            row["ACUST_NAME"] = ((DataRowView)_Bs2.Current).Row["ACUST_NAME"];
            row["ECUST_NAME"] = ((DataRowView)_Bs2.Current).Row["ECUST_NAME"];
            DT_Term.Rows.Add(row);
            int T_ID = Convert.ToInt16(((DataRowView)_Bs2.Current).Row["T_ID"]);
            DataRow[] DRow = connection.SQLDS.Tables["term_Tbl"].Select("T_id = " + T_ID);
            DRow[0].Delete();
            connection.SQLDS.Tables["Term_Tbl"].AcceptChanges();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button6.Enabled = true;
            button5.Enabled = true;
            if (Lst_Term_id.Rows.Count == 0)
            {
                button8.Enabled = false;
                button7.Enabled = false;
            }
        }
        //-----------------------------------------------------
        private void button7_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < connection.SQLDS.Tables["Term_Tbl"].Rows.Count; i++)
            {
                DataRow row = DT_Term.NewRow();
                row["T_ID"] = connection.SQLDS.Tables["Term_Tbl"].Rows[i]["T_ID"];
                row["ACUST_NAME"] = connection.SQLDS.Tables["Term_Tbl"].Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = connection.SQLDS.Tables["Term_Tbl"].Rows[i]["ECUST_NAME"];
                DT_Term.Rows.Add(row);
            }
            connection.SQLDS.Tables["Term_Tbl"].Clear();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button6.Enabled = true;
            button5.Enabled = true;
            button8.Enabled = false;
            button7.Enabled = false;
        }
        //-----------------------------------------------------
        private void button6_Click(object sender, EventArgs e)
        {
            DataRow row = connection.SQLDS.Tables["term_Tbl"].NewRow();
            row["T_id"] = ((DataRowView)_Bs4.Current).Row["T_ID"];
            row["Acust_name"] = ((DataRowView)_Bs4.Current).Row["Acust_name"];
            row["Ecust_name"] = ((DataRowView)_Bs4.Current).Row["Ecust_name"]; 
            connection.SQLDS.Tables["Term_Tbl"].Rows.Add(row);
            connection.SQLDS.Tables["Term_Tbl"].AcceptChanges();
            DT_Term.Rows[Lst_Term.CurrentRow.Index].Delete();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            if (Lst_Term.Rows.Count == 0)
            {
                button6.Enabled = false;
                button5.Enabled = false;
            }
        }
        //-----------------------------------------------------
        private void button5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Term.Rows.Count; i++)
            {
                DataRow Rows = connection.SQLDS.Tables["Term_Tbl"].NewRow();
                Rows["T_ID"] = DT_Term.Rows[i]["T_ID"];
                Rows["ACUST_NAME"] = DT_Term.Rows[i]["ACUST_NAME"];
                Rows["ECUST_NAME"] = DT_Term.Rows[i]["ECUST_NAME"];
                connection.SQLDS.Tables["Term_Tbl"].Rows.Add(Rows);
            }
            DT_Term.Rows.Clear();
            connection.SQLDS.Tables["Term_Tbl"].AcceptChanges();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = false;
            button5.Enabled = false;
        }
        //-----------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Chk_Cur.Checked)
            {
                if (Lst_cur.Rows.Count == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " تأكد من اختيار العملات " : "Enter the currency please", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            if (Chk_Term.Checked)
            {
                if (Lst_Term.Rows.Count == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " تأكد من اختيار الفروع  " : "Enter the Terminalse please", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            #endregion
            if (!Chk_Cur.Checked)
            {
                DT_Cur = connection.SQLDS.Tables["Cur_Tbl"];
            }
            if (!Chk_Term.Checked)
            {
                DT_Term = connection.SQLDS.Tables["Term_Tbl"];
            }
            this.Visible = false;
            Reveal_CCP_List AddFrm = new Reveal_CCP_List(DT_Cur, DT_Term);
            AddFrm.ShowDialog(this);
            Search_CCP_List_New_Load(null, null);
            Chk_Cur.Checked = true;
            Chk_Term.Checked = true;
            TxtCur_Name.Text = "";
            TxtTem_Name.Text = "";
            this.Visible = true;
        }

        private void Search_CCP_List_FormClosed(object sender, FormClosedEventArgs e)
        {
     

            string[] Str = { "Cur_Tbl","Term_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

    }
}