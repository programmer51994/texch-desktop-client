﻿namespace Integration_Accounting_Sys
{
    partial class Reveal_Balance_Local_Amount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtToDate = new System.Windows.Forms.MaskedTextBox();
            this.TxtFromDate = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Terminals = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc_Assembly = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.Grd_Acc_Details = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.Grd_Vo_Details = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.Btn_Pdf = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Btn_Rpt = new System.Windows.Forms.Button();
            this.Prt_Btn = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtE_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtED_ForAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtEC_ForAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_LocAmount_Sum = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Terminals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Assembly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Vo_Details)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtToDate
            // 
            this.TxtToDate.BackColor = System.Drawing.Color.White;
            this.TxtToDate.Enabled = false;
            this.TxtToDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToDate.Location = new System.Drawing.Point(600, 35);
            this.TxtToDate.Mask = "0000/00/00";
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.PromptChar = ' ';
            this.TxtToDate.Size = new System.Drawing.Size(126, 23);
            this.TxtToDate.TabIndex = 3;
            this.TxtToDate.Text = "00000000";
            this.TxtToDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.BackColor = System.Drawing.Color.White;
            this.TxtFromDate.Enabled = false;
            this.TxtFromDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFromDate.Location = new System.Drawing.Point(413, 35);
            this.TxtFromDate.Mask = "0000/00/00";
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.PromptChar = ' ';
            this.TxtFromDate.Size = new System.Drawing.Size(126, 23);
            this.TxtFromDate.TabIndex = 2;
            this.TxtFromDate.Text = "00000000";
            this.TxtFromDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(323, 39);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 718;
            this.label2.Text = "التــاريــخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(546, 39);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 717;
            this.label5.Text = "إلــــى :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 31);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1094, 1);
            this.flowLayoutPanel1.TabIndex = 716;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(90, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(242, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(880, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(59, 14);
            this.label4.TabIndex = 710;
            this.label4.Text = "التاريــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(943, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(82, 14);
            this.label1.TabIndex = 709;
            this.label1.Text = "المستخــــدم:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 60);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1087, 1);
            this.flowLayoutPanel4.TabIndex = 722;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(1091, 60);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 536);
            this.flowLayoutPanel7.TabIndex = 723;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 60);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 535);
            this.flowLayoutPanel6.TabIndex = 725;
            // 
            // Grd_Terminals
            // 
            this.Grd_Terminals.AllowUserToAddRows = false;
            this.Grd_Terminals.AllowUserToDeleteRows = false;
            this.Grd_Terminals.AllowUserToResizeColumns = false;
            this.Grd_Terminals.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Terminals.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Terminals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Terminals.ColumnHeadersHeight = 30;
            this.Grd_Terminals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.Grd_Terminals.Location = new System.Drawing.Point(11, 66);
            this.Grd_Terminals.Name = "Grd_Terminals";
            this.Grd_Terminals.ReadOnly = true;
            this.Grd_Terminals.RowHeadersWidth = 10;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Terminals.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Terminals.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Terminals.Size = new System.Drawing.Size(1076, 107);
            this.Grd_Terminals.TabIndex = 4;
            this.Grd_Terminals.SelectionChanged += new System.EventHandler(this.Grd_Terminals_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cust_Id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رمز الثانوي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Acust_Name";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "الإســم العــربي";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 270;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Ecust_Name";
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "الإســم الأجنبــي";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 268;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Min_DNrecDate";
            this.Column4.HeaderText = "التاريخ مــن";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Max_DNrecDate";
            this.Column5.HeaderText = "التاريخ الـــى";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 300;
            // 
            // Grd_Acc_Assembly
            // 
            this.Grd_Acc_Assembly.AllowUserToAddRows = false;
            this.Grd_Acc_Assembly.AllowUserToDeleteRows = false;
            this.Grd_Acc_Assembly.AllowUserToResizeColumns = false;
            this.Grd_Acc_Assembly.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Assembly.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Assembly.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Acc_Assembly.ColumnHeadersHeight = 30;
            this.Grd_Acc_Assembly.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8});
            this.Grd_Acc_Assembly.Location = new System.Drawing.Point(11, 180);
            this.Grd_Acc_Assembly.Name = "Grd_Acc_Assembly";
            this.Grd_Acc_Assembly.ReadOnly = true;
            this.Grd_Acc_Assembly.RowHeadersWidth = 10;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Assembly.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Acc_Assembly.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Assembly.Size = new System.Drawing.Size(1076, 98);
            this.Grd_Acc_Assembly.TabIndex = 5;
            this.Grd_Acc_Assembly.SelectionChanged += new System.EventHandler(this.Grd_Acc_Assembly_SelectionChanged);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_Id";
            this.Column6.HeaderText = "رمز الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Acc_Aname";
            this.Column7.HeaderText = "الحســـــــاب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 540;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Eloc_Amount";
            dataGridViewCellStyle6.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column8.HeaderText = "ارصدة نهاية المدة";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 450;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(577, 284);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(83, 14);
            this.label9.TabIndex = 736;
            this.label9.Text = "المـجمـــــــوع:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 319);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel2.TabIndex = 737;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(439, 432);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(75, 14);
            this.label6.TabIndex = 748;
            this.label6.Text = "المجاميـــــع:";
            // 
            // Grd_Acc_Details
            // 
            this.Grd_Acc_Details.AllowUserToAddRows = false;
            this.Grd_Acc_Details.AllowUserToDeleteRows = false;
            this.Grd_Acc_Details.AllowUserToResizeColumns = false;
            this.Grd_Acc_Details.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Acc_Details.ColumnHeadersHeight = 30;
            this.Grd_Acc_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14});
            this.Grd_Acc_Details.Location = new System.Drawing.Point(11, 328);
            this.Grd_Acc_Details.Name = "Grd_Acc_Details";
            this.Grd_Acc_Details.ReadOnly = true;
            this.Grd_Acc_Details.RowHeadersWidth = 10;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Acc_Details.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Details.Size = new System.Drawing.Size(1076, 98);
            this.Grd_Acc_Details.TabIndex = 6;
            this.Grd_Acc_Details.SelectionChanged += new System.EventHandler(this.Grd_Acc_Details_SelectionChanged);
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Cust_id";
            this.Column9.HeaderText = "رمز الثانوي";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Acust_Name";
            this.Column10.HeaderText = "أسم الثانوي";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 200;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "cur_AName";
            dataGridViewCellStyle10.Format = "N3";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column11.HeaderText = "اسم العملة";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 200;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "DEFor_Amount";
            dataGridViewCellStyle11.Format = "N3";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column12.HeaderText = "مديــــن-(ع.ص)";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 200;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "CEFor_Amount";
            dataGridViewCellStyle12.Format = "N3";
            this.Column13.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column13.HeaderText = "دائــــن-(ع.ص)";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 200;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Eloc_AMount";
            dataGridViewCellStyle13.Format = "N3";
            this.Column14.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column14.HeaderText = "عملة محلية";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 175;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 470);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel3.TabIndex = 749;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 627);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1094, 22);
            this.statusStrip1.TabIndex = 751;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(13, 459);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(241, 14);
            this.label7.TabIndex = 752;
            this.label7.Text = "تفاصيل الحراكات الماليه للحساب اعلاه....";
            // 
            // Grd_Vo_Details
            // 
            this.Grd_Vo_Details.AllowUserToAddRows = false;
            this.Grd_Vo_Details.AllowUserToDeleteRows = false;
            this.Grd_Vo_Details.AllowUserToOrderColumns = true;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Vo_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Vo_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_Vo_Details.ColumnHeadersHeight = 30;
            this.Grd_Vo_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column30,
            this.Column29,
            this.Column28});
            this.Grd_Vo_Details.Location = new System.Drawing.Point(11, 481);
            this.Grd_Vo_Details.Name = "Grd_Vo_Details";
            this.Grd_Vo_Details.ReadOnly = true;
            this.Grd_Vo_Details.RowHeadersWidth = 10;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Vo_Details.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_Vo_Details.Size = new System.Drawing.Size(1076, 109);
            this.Grd_Vo_Details.TabIndex = 6;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(5, 595);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel8.TabIndex = 754;
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Browser.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Browser.Location = new System.Drawing.Point(602, 599);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(111, 24);
            this.Btn_Browser.TabIndex = 14;
            this.Btn_Browser.Text = "تصـدير اجمالي";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.button1_Click);
            // 
            // Btn_Pdf
            // 
            this.Btn_Pdf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Pdf.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Pdf.Location = new System.Drawing.Point(492, 599);
            this.Btn_Pdf.Name = "Btn_Pdf";
            this.Btn_Pdf.Size = new System.Drawing.Size(111, 24);
            this.Btn_Pdf.TabIndex = 13;
            this.Btn_Pdf.Text = "تصـدير تفصيلي";
            this.Btn_Pdf.UseVisualStyleBackColor = true;
            this.Btn_Pdf.Click += new System.EventHandler(this.Btn_Export_Details_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(712, 599);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(111, 24);
            this.Btn_Ext.TabIndex = 15;
            this.Btn_Ext.Text = "انـهـاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Ext_Click);
            // 
            // Btn_Rpt
            // 
            this.Btn_Rpt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Rpt.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Rpt.Location = new System.Drawing.Point(382, 599);
            this.Btn_Rpt.Name = "Btn_Rpt";
            this.Btn_Rpt.Size = new System.Drawing.Size(111, 24);
            this.Btn_Rpt.TabIndex = 12;
            this.Btn_Rpt.Text = "طباعة تفصيلي";
            this.Btn_Rpt.UseVisualStyleBackColor = true;
            this.Btn_Rpt.Click += new System.EventHandler(this.Btn_Print_Details_Click);
            // 
            // Prt_Btn
            // 
            this.Prt_Btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prt_Btn.ForeColor = System.Drawing.Color.Navy;
            this.Prt_Btn.Location = new System.Drawing.Point(272, 599);
            this.Prt_Btn.Name = "Prt_Btn";
            this.Prt_Btn.Size = new System.Drawing.Size(111, 24);
            this.Prt_Btn.TabIndex = 11;
            this.Prt_Btn.Text = "طباعة اجمالـي";
            this.Prt_Btn.UseVisualStyleBackColor = true;
            this.Prt_Btn.Click += new System.EventHandler(this.Btn_PrintAll_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(13, 309);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(286, 14);
            this.label16.TabIndex = 761;
            this.label16.Text = "ارصدة الحساب على مستوى الثانوي والعملة .....";
            // 
            // TxtE_LocAmount
            // 
            this.TxtE_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtE_LocAmount.Enabled = false;
            this.TxtE_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtE_LocAmount.Location = new System.Drawing.Point(666, 280);
            this.TxtE_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtE_LocAmount.Name = "TxtE_LocAmount";
            this.TxtE_LocAmount.NumberDecimalDigits = 3;
            this.TxtE_LocAmount.NumberDecimalSeparator = ".";
            this.TxtE_LocAmount.NumberGroupSeparator = ",";
            this.TxtE_LocAmount.Size = new System.Drawing.Size(419, 23);
            this.TxtE_LocAmount.TabIndex = 769;
            this.TxtE_LocAmount.Text = "0.000";
            // 
            // TxtED_ForAmount
            // 
            this.TxtED_ForAmount.BackColor = System.Drawing.Color.White;
            this.TxtED_ForAmount.Enabled = false;
            this.TxtED_ForAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtED_ForAmount.Location = new System.Drawing.Point(520, 428);
            this.TxtED_ForAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtED_ForAmount.Name = "TxtED_ForAmount";
            this.TxtED_ForAmount.NumberDecimalDigits = 3;
            this.TxtED_ForAmount.NumberDecimalSeparator = ".";
            this.TxtED_ForAmount.NumberGroupSeparator = ",";
            this.TxtED_ForAmount.Size = new System.Drawing.Size(200, 23);
            this.TxtED_ForAmount.TabIndex = 7;
            this.TxtED_ForAmount.Text = "0.000";
            // 
            // TxtEC_ForAmount
            // 
            this.TxtEC_ForAmount.BackColor = System.Drawing.Color.White;
            this.TxtEC_ForAmount.Enabled = false;
            this.TxtEC_ForAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEC_ForAmount.Location = new System.Drawing.Point(723, 428);
            this.TxtEC_ForAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtEC_ForAmount.Name = "TxtEC_ForAmount";
            this.TxtEC_ForAmount.NumberDecimalDigits = 3;
            this.TxtEC_ForAmount.NumberDecimalSeparator = ".";
            this.TxtEC_ForAmount.NumberGroupSeparator = ",";
            this.TxtEC_ForAmount.Size = new System.Drawing.Size(195, 23);
            this.TxtEC_ForAmount.TabIndex = 8;
            this.TxtEC_ForAmount.Text = "0.000";
            // 
            // Txt_LocAmount_Sum
            // 
            this.Txt_LocAmount_Sum.BackColor = System.Drawing.Color.White;
            this.Txt_LocAmount_Sum.Enabled = false;
            this.Txt_LocAmount_Sum.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_LocAmount_Sum.Location = new System.Drawing.Point(921, 428);
            this.Txt_LocAmount_Sum.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_LocAmount_Sum.Name = "Txt_LocAmount_Sum";
            this.Txt_LocAmount_Sum.NumberDecimalDigits = 3;
            this.Txt_LocAmount_Sum.NumberDecimalSeparator = ".";
            this.Txt_LocAmount_Sum.NumberGroupSeparator = ",";
            this.Txt_LocAmount_Sum.Size = new System.Drawing.Size(166, 23);
            this.Txt_LocAmount_Sum.TabIndex = 9;
            this.Txt_LocAmount_Sum.Text = "0.000";
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column15.DataPropertyName = "DLoc_Amount";
            dataGridViewCellStyle17.Format = "N3";
            this.Column15.DefaultCellStyle = dataGridViewCellStyle17;
            this.Column15.HeaderText = "مديــن ـ(ع.م)ـ";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column15.Width = 75;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column16.DataPropertyName = "CLoc_Amount";
            dataGridViewCellStyle18.Format = "N3";
            this.Column16.DefaultCellStyle = dataGridViewCellStyle18;
            this.Column16.HeaderText = "دائــن ـ(ع.م)ـ";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column16.Width = 72;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column17.DataPropertyName = "Balance_Loc";
            dataGridViewCellStyle19.Format = "N3";
            this.Column17.DefaultCellStyle = dataGridViewCellStyle19;
            this.Column17.HeaderText = "الرصيد ـ(ع.م)ـ";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column17.Width = 78;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column18.DataPropertyName = "ACC_Id";
            this.Column18.HeaderText = "رقم الحســاب";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column18.Width = 77;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column19.DataPropertyName = "Acc_Aname";
            this.Column19.HeaderText = "الحساب";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column19.Width = 51;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column20.DataPropertyName = "Cur_Aname";
            this.Column20.HeaderText = "الـعـمـلـة";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 71;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column21.DataPropertyName = "Acust_Name";
            this.Column21.HeaderText = "الثانـــوي";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 77;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column22.DataPropertyName = "AOPER_NAME";
            this.Column22.HeaderText = "العملية";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column22.Width = 44;
            // 
            // Column23
            // 
            this.Column23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column23.DataPropertyName = "Real_Price";
            dataGridViewCellStyle20.Format = "N7";
            this.Column23.DefaultCellStyle = dataGridViewCellStyle20;
            this.Column23.HeaderText = "السـعر الفعلـي";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column23.Width = 80;
            // 
            // Column24
            // 
            this.Column24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column24.DataPropertyName = "DEFor_Amount";
            dataGridViewCellStyle21.Format = "N3";
            this.Column24.DefaultCellStyle = dataGridViewCellStyle21;
            this.Column24.HeaderText = "مدين ـ(ع.ص)ـ";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column24.Width = 79;
            // 
            // Column25
            // 
            this.Column25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column25.DataPropertyName = "CEFor_Amount";
            dataGridViewCellStyle22.Format = "N3";
            this.Column25.DefaultCellStyle = dataGridViewCellStyle22;
            this.Column25.HeaderText = "دائن ـ(ع.ص)ـ";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column25.Width = 76;
            // 
            // Column26
            // 
            this.Column26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column26.DataPropertyName = "Balance_For";
            this.Column26.HeaderText = "الرصيد ـ(ع.ص)ـ";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.Width = 107;
            // 
            // Column27
            // 
            this.Column27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column27.DataPropertyName = "VO_NO";
            this.Column27.HeaderText = "رقم القيد";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            this.Column27.Width = 73;
            // 
            // Column30
            // 
            this.Column30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column30.DataPropertyName = "Nrec_Date1";
            this.Column30.HeaderText = "تاريخ تنظيم السجلات";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Width = 131;
            // 
            // Column29
            // 
            this.Column29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column29.DataPropertyName = "C_date";
            this.Column29.HeaderText = "وقت وتاريخ التنظيم";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.Width = 124;
            // 
            // Column28
            // 
            this.Column28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column28.DataPropertyName = "Notes";
            this.Column28.HeaderText = "التفاصـــيل";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column28.Width = 64;
            // 
            // Reveal_Balance_Local_Amount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 649);
            this.Controls.Add(this.TxtE_LocAmount);
            this.Controls.Add(this.TxtED_ForAmount);
            this.Controls.Add(this.TxtEC_ForAmount);
            this.Controls.Add(this.Txt_LocAmount_Sum);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Btn_Browser);
            this.Controls.Add(this.Btn_Pdf);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.Btn_Rpt);
            this.Controls.Add(this.Prt_Btn);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Grd_Vo_Details);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Grd_Acc_Details);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Grd_Acc_Assembly);
            this.Controls.Add(this.Grd_Terminals);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reveal_Balance_Local_Amount";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "215";
            this.Text = "كــــــشف أرصدة";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Trial_Balance_Main_FormClosed);
            this.Load += new System.EventHandler(this.Trial_Balance_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Terminals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Assembly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Vo_Details)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox TxtToDate;
        private System.Windows.Forms.MaskedTextBox TxtFromDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.DataGridView Grd_Terminals;
        private System.Windows.Forms.DataGridView Grd_Acc_Assembly;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView Grd_Acc_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView Grd_Vo_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.Button Btn_Pdf;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.Button Btn_Rpt;
        private System.Windows.Forms.Button Prt_Btn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_LocAmount_Sum;
        private System.Windows.Forms.Sample.DecimalTextBox TxtEC_ForAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtED_ForAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.Sample.DecimalTextBox TxtE_LocAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
    }
}