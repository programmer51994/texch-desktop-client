﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class Profit_Loss_PrintExport : Form
    {
        DataTable Profit_loss_1 = new DataTable();
        DataTable Profit_loss_2 = new DataTable();
        DataTable Trail_Balance2 = new DataTable();
        public DataGridView Dgv1 { get; set; }
        public DataGridView Dgv2 { get; set; }
        public DataGridView Dgv3 { get; set; }
        public DataGridView Dgv4 { get; set; }
        DataGridView Grd_Loc_Sum = new DataGridView();
        DataTable Tbl_Loc_Sum = new DataTable();
         String From_Date = "" ;
        String To_Date  = "" ; 
        String _Level  = "" ; 
        String Customer_Name   = "" ;
        public Profit_Loss_PrintExport( String FromDate , String ToDate , String Level , String Cust_Name)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            From_Date = FromDate;
            To_Date = ToDate;
            _Level = Level;
            Customer_Name = Cust_Name;
            if (connection.Lang_id == 2)
            {
                Cbo_Print.Items[0] = "Print total";
                Cbo_Print.Items[1] = "Detailed printing";
            }
            else
            {
                Cbo_Print.Items[0] = "طباعة اجمالـــي";
                Cbo_Print.Items[1] = "طباعة تفصيلــي";
            }
            if (connection.Lang_id == 2)
            {
                Cbo_Export.Items[0] = "total";
                Cbo_Export.Items[1] = "Detailed";
            }
            else
            {
                Cbo_Export.Items[0] = " اجمالـــي";
                Cbo_Export.Items[1] = " تفصيلــي";
            }
         

        }
        private void Trail_Balance_PrintExport_Load(object sender, EventArgs e)
        {

            Cbo_Print.SelectedIndex = 0;
            Cbo_Export.SelectedIndex = 0;
            Chk_Print.Checked = true;
            Chk_Print_CheckedChanged(null, null);
        }
        private void Chk_Print_CheckedChanged(object sender, EventArgs e)
        {
            Cbo_Print.Enabled = true;
            Cbo_Export.Enabled = false;
        }

        private void Chk_Export_CheckedChanged(object sender, EventArgs e)
        {
            Cbo_Export.Enabled = true;
            Cbo_Print.Enabled = false;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            string Frm_name = "";
            string Frm_Ename = "";
            Frm_name = "قائمة الأرباح والخسائر";
            Frm_Ename = "Reveal Profit and Loss";
            // ------------------طباعة
            if (Chk_Print.Checked == true)
            {
                if (Cbo_Print.SelectedIndex == 0)
                {
                    Profit_loss_1 = connection.SQLDS.Tables["Profit_loss_TBL"].DefaultView.ToTable().Select().CopyToDataTable(); 
                    DataTable Dt_Param = CustomControls.CustomParam_Dt();
                    Dt_Param.Rows.Add("User_rep", connection.User_Name);
                    Dt_Param.Rows.Add("From_Date", From_Date);
                    Dt_Param.Rows.Add("To_Date", To_Date);
                    Dt_Param.Rows.Add("_Level",_Level );
                    Dt_Param.Rows.Add("Customer_Name", Customer_Name);
                    Dt_Param.Rows.Add("Frm_name", Frm_name);
                    Dt_Param.Rows.Add("Frm_Ename", Frm_Ename);

                    Profit_loss_1.TableName = "Profit_loss_1";

                    connection.SQLDS.Tables.Add(Profit_loss_1);
                    
                    
                    Proift_Loss_Rep ObjRpt = new Proift_Loss_Rep();
                    Proift_Loss_Rep_Eng ObjRptEng = new Proift_Loss_Rep_Eng();
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove("Profit_loss_1");
                  
                }
                else
                {
                   

                    try
                    {
                        Profit_loss_1 = connection.SQLDS.Tables["Profit_loss_TBL"].DefaultView.ToTable().Select("Acc_No like'" + Reveal_Profit_Loss.Acc_No + "'").CopyToDataTable();

                        Profit_loss_2 = connection.SQLDS.Tables["Profit_loss_TBL1"].DefaultView.ToTable().Select(" dot_acc_no like '" + Reveal_Profit_Loss.Dot_Acc_no + "%'").CopyToDataTable();

                        Profit_loss_1.TableName = "Profit_loss_1";
                        Profit_loss_2.TableName = "Profit_loss_2";
                       
                        connection.SQLDS.Tables.Add(Profit_loss_1);
                        connection.SQLDS.Tables.Add(Profit_loss_2);
                       
                       
                        DataTable Dt_Param = CustomControls.CustomParam_Dt();
                        Dt_Param.Rows.Add("User_rep", connection.User_Name);
                        Dt_Param.Rows.Add("From_Date", From_Date);
                        Dt_Param.Rows.Add("To_Date", To_Date);
                        Dt_Param.Rows.Add("_Level", _Level);
                        Dt_Param.Rows.Add("Customer_Name", Customer_Name);
                        Dt_Param.Rows.Add("Frm_name", Frm_name);
                        Dt_Param.Rows.Add("Frm_Ename", Frm_Ename);

                        Proift_Loss_Det_Rep ObjRpt = new Proift_Loss_Det_Rep();
                        Proift_Loss_Rep_Eng ObjRptEng = new Proift_Loss_Rep_Eng();
                        Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);
                        this.Visible = false;
                        RptLangFrm.ShowDialog(this);
                        this.Visible = true;
                        connection.SQLDS.Tables.Remove("Profit_loss_1");
                        connection.SQLDS.Tables.Remove("Profit_loss_2");
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للرصيد " : " No records to this account ", MyGeneral_Lib.LblCap);
                       
                        return;
                    }
                    //connection.SQLDS.Tables.Remove("Profit_loss_1");
                    //connection.SQLDS.Tables.Remove("Profit_loss_2");
                }
            }
            // ------------------تصدير
            if (Chk_Export.Checked == true)
            {
                if (Cbo_Export.SelectedIndex == 0)//تصدير اجمالي
                {

                    DataTable ExpDt_Term = connection.SQLDS.Tables["Profit_loss_TBL"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Sub_total"
                                                                                                     , "Totals").Select().CopyToDataTable();
              
                    DataTable[] _EXP_DT = { ExpDt_Term };
                    DataGridView[] Export_GRD = { Dgv1 };
                    MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? "كشـــف الارباح والخسائر" : "Profits and losses Reveal");
                }
                if (Cbo_Export.SelectedIndex == 1)//تصدير تفصيلي
                {
                    if (Dgv2.Rows.Count > 0)
                    {
                        DataTable ExpDt_Term = connection.SQLDS.Tables["Profit_loss_TBL"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Sub_total"
                                                                                                    , "Totals").Select("Acc_No like'" + Reveal_Profit_Loss.Acc_No + "'").CopyToDataTable();

                        DataTable ExpDt_Term_Acc = connection.SQLDS.Tables["Profit_loss_TBL1"].DefaultView.ToTable(false, "Acc_No", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "loc_Amount1"
                            , connection.Lang_id == 1 ? "Aoper_name" : "Aoper_name", "vo_no", "Acc_id", "Nrec_date1", "C_Date", "Notes", "Dot_Acc_no").Select(" dot_acc_no like '" + Reveal_Profit_Loss.Dot_Acc_no + "%'").CopyToDataTable();
                        ExpDt_Term_Acc = ExpDt_Term_Acc.DefaultView.ToTable(false, "Acc_No", "Acc_Aname", "loc_Amount1"
                                                                                                          , connection.Lang_id == 1 ? "Aoper_name" : "Aoper_name", "vo_no", "Acc_id", "Nrec_date1", "C_Date", "Notes").Select().CopyToDataTable();


                        DataTable[] _EXP_DT = { ExpDt_Term, ExpDt_Term_Acc };
                        DataGridView[] Export_GRD = { Dgv1, Dgv2 };
                        MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, connection.Lang_id == 1 ? " كشــــف الارباح والخســـائر -(تحليـــل الحســاب)-" : "Profits and losses Reveal (Account analysis)");
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للحساب " : " No records to this account ", MyGeneral_Lib.LblCap);

                        return;
                    }
                }
            }
        }
        
       
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}