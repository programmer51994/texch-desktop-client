﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Trial_Balance_Main : Form
    {
        #region Definshion
        bool TermChange = false;
        bool Assemble_AccChange = false;
        bool Details_AccChange = false;
        BindingSource Term_Bs = new BindingSource();
        BindingSource Assemble_Acc_Bs = new BindingSource();
        BindingSource Details_Acc_Bs = new BindingSource();
        DataTable Assembl_Acc_DT = new DataTable();
        DataTable Details_Acc_DT = new DataTable();
        int T_Id = 0;
        int Acc_Id = 0;
        string Dot_Acc_No = "";
        #endregion
        //---------------------------
        public Trial_Balance_Main(/*string FromDate, string ToDate*/)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Terminals.AutoGenerateColumns = false;
            Grd_Acc_Details.AutoGenerateColumns = false;
            Grd_Acc_Assembly.AutoGenerateColumns = false;
            Grd_Vo_Details.AutoGenerateColumns = false;
            //TxtFromDate.Text = FromDate;
            //TxtToDate.Text = ToDate;
              #region Enghlish
            if (connection.Lang_id != 1)
            {
                Column7.DataPropertyName = "Acc_Ename";
                Column15.DataPropertyName = "Acc_Ename";
                Column26.DataPropertyName = "ACC_Ename";
                dataGridViewTextBoxColumn1.DataPropertyName = "Cur_Ename";
                dataGridViewTextBoxColumn2.DataPropertyName = "Ecust_Name";
                dataGridViewTextBoxColumn3.DataPropertyName = "Eoper_name";
            }
              #endregion
        }
        //---------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {
            TermChange = false;
            Term_Bs.DataSource = connection.SQLDS.Tables["Trail_Balance_Tbl"];
            Grd_Terminals.DataSource = Term_Bs;
            if (connection.SQLDS.Tables["Trail_Balance_Tbl"].Rows.Count > 0)
            {
                TermChange = true;
                Grd_Terminals_SelectionChanged(sender, e);
            }
        }
        //---------------------------
        private void Grd_Terminals_SelectionChanged(object sender, EventArgs e)
        {
            if (TermChange)
            {
                Assembl_Acc_DT = new DataTable();
                Assemble_AccChange = false;
                T_Id = Convert.ToInt32(((DataRowView)Term_Bs.Current).Row["T_Id"]);
                Assembl_Acc_DT = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("T_Id = " + T_Id).CopyToDataTable();

                var query = from rec in Assembl_Acc_DT.AsEnumerable()
                            select new
                            {
                                Acc_Aname = rec["Acc_Aname"],
                                Acc_Ename = rec["Acc_Ename"],
                                Dot_Acc_No = rec["Dot_Acc_No"],
                                DOLoc_Amount = Convert.ToDecimal(rec["Oloc_Amount"]) > 0 ? Convert.ToDecimal(rec["Oloc_Amount"]) : 0,
                                COLoc_Amount = Convert.ToDecimal(rec["Oloc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["Oloc_Amount"])),

                                DPLoc_Amount = Convert.ToDecimal(rec["PLoc_Amount"]) > 0 ? Convert.ToDecimal(rec["PLoc_Amount"]) : 0,
                                CPLoc_Amount = Convert.ToDecimal(rec["PLoc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["PLoc_Amount"])),

                                DELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"]) > 0 ? Convert.ToDecimal(rec["ELoc_Amount"]) : 0,
                                CELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["ELoc_Amount"]))
                            };
                Assembl_Acc_DT = CustomControls.IEnumerableToDataTable(query);
                Assemble_Acc_Bs.DataSource = Assembl_Acc_DT;
                Grd_Acc_Assembly.DataSource = Assemble_Acc_Bs;

                TxtOD_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(DOloc_Amount)", "").ToString();
                TxtOC_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(COloc_Amount)", "").ToString();
                TxtPD_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(DPloc_Amount)", "").ToString();
                TxtPC_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(CPloc_Amount)", "").ToString();
                TxtED_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(DEloc_Amount)", "").ToString();
                TxtEC_LocAmount.Text = Assembl_Acc_DT.Compute("Sum(CEloc_Amount)", "").ToString();


                if (Assemble_Acc_Bs.List.Count > 0)
                {
                    Assemble_AccChange = true;
                    Grd_Acc_Assembly_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void Grd_Acc_Assembly_SelectionChanged(object sender, EventArgs e)
        {
            if (Assemble_AccChange)
            {
                Assembl_Acc_DT = new DataTable();
                Details_AccChange = false;
                Dot_Acc_No = ((DataRowView)Assemble_Acc_Bs.Current).Row["Dot_Acc_No"].ToString();
                Details_Acc_DT = connection.SQLDS.Tables["Trail_Balance_Tbl2"].Select("T_Id = " + T_Id
                                + " And Dot_Acc_No Like '" + Dot_Acc_No + "%'").CopyToDataTable();

                var query = from rec in Details_Acc_DT.AsEnumerable()
                            select new
                            {
                                Acc_Aname = rec["Acc_Aname"],
                                Acc_Ename = rec["Acc_Ename"],
                                Acc_Id = rec["Acc_Id"],
                                DOLoc_Amount = Convert.ToDecimal(rec["Oloc_Amount"]) > 0 ? Convert.ToDecimal(rec["Oloc_Amount"]) : 0,
                                COLoc_Amount = Convert.ToDecimal(rec["Oloc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["Oloc_Amount"])),

                                DPLoc_Amount = Convert.ToDecimal(rec["PLoc_Amount"]) > 0 ? Convert.ToDecimal(rec["PLoc_Amount"]) : 0,
                                CPLoc_Amount = Convert.ToDecimal(rec["PLoc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["PLoc_Amount"])),

                                DELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"]) > 0 ? Convert.ToDecimal(rec["ELoc_Amount"]) : 0,
                                CELoc_Amount = Convert.ToDecimal(rec["ELoc_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["ELoc_Amount"]))
                            };
                Details_Acc_DT = CustomControls.IEnumerableToDataTable(query);
                Details_Acc_Bs.DataSource = Details_Acc_DT;
                Grd_Acc_Details.DataSource = Details_Acc_Bs;

                TxtOD_LocAmount2.Text = Details_Acc_DT.Compute("Sum(DOloc_Amount)", "").ToString();
                TxtOC_LocAmount2.Text = Details_Acc_DT.Compute("Sum(COloc_Amount)", "").ToString();
                TxtPD_LocAmount2.Text = Details_Acc_DT.Compute("Sum(DPloc_Amount)", "").ToString();
                TxtPC_LocAmount2.Text = Details_Acc_DT.Compute("Sum(CPloc_Amount)", "").ToString();
                TxtED_LocAmount2.Text = Details_Acc_DT.Compute("Sum(DEloc_Amount)", "").ToString();
                TxtEC_LocAmount2.Text = Details_Acc_DT.Compute("Sum(CEloc_Amount)", "").ToString();

                if (Details_Acc_Bs.List.Count > 0)
                {
                    Details_AccChange = true;
                    Grd_Acc_Details_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void Grd_Acc_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (Details_AccChange)
            {
                Acc_Id = Convert.ToInt32(((DataRowView)Details_Acc_Bs.Current).Row["Acc_Id"]);
                //---------------------------------------
                decimal MBalance = 0;
                DataTable Dt;
                try
                {
                    Dt = connection.SQLDS.Tables["Trail_Balance_Tbl3"].Select("T_id = " + T_Id
                                                                                                   + " And Acc_Id = " + Acc_Id).CopyToDataTable();
                }
                catch
                {
                    Dt = new DataTable();
                }
                foreach (DataRow Drow in Dt.Rows)
                {
                    if (Convert.ToInt32(Drow["RowNum"]) == 1)
                    {
                        MBalance = Convert.ToDecimal(Drow["DLoc_Amount"]) + Convert.ToDecimal(Drow["CLoc_Amount"]);
                    }

                    if (Convert.ToInt32(Drow["RowNum"]) > 1)
                    {
                        MBalance += Convert.ToDecimal(Drow["DLoc_Amount"]) + Convert.ToDecimal(Drow["CLoc_Amount"]);
                    }
                    Drow["Balance_For"] = MBalance;
                    //Drow["CLoc_Amount"] = Math.Abs(Convert.ToDecimal(Drow["CLoc_Amount"]));
                }
                //---------------
                Grd_Vo_Details.DataSource = Dt;
            }
        }
        //---------------------------
        private void Btn_PrintAll_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Print_Details_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void button1_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Trial_Balance_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            TermChange = false;
            Assemble_AccChange = false;
            Details_AccChange = false;
            string[] Used_Tbl = { "Trail_Balance_Tbl", "Trail_Balance_Tbl1", 
                                     "Trail_Balance_Tbl2", "Trail_Balance_Tbl3"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void decimalTextBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}