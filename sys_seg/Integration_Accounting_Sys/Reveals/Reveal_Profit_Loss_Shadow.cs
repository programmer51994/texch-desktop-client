﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Profit_Loss_Shadow : Form
    {
        #region Definshion
        string Sql_text = "";
        bool Change_voucher = false;
        public static string Dot_Acc_no = "";
        BindingSource _Bs_pl = new BindingSource();
        public static string Acc_No = "" ;
        public static  Int16 Acc_level = 0;
        string @format = "dd/MM/yyyy";
        Int32 From_Date = 0;
        Int32 To_Date = 0;
        BindingSource _bsyears = new BindingSource();
        //bool Change_grd = false;
        int from_nrec_date = 0;
        int to_nrec_date = 0;
        int min_nrec_date_int = 0;
        int from_min_nrec_date_int = 0;
        //BindingSource _Bs_Level = new BindingSource();
        #endregion
        //---------------------------
        public Reveal_Profit_Loss_Shadow()
        {
            InitializeComponent();
            Grd_Voucher_Deatails.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            if (connection.Lang_id == 2)
            {
                Column7.DataPropertyName = "Acc_Ename";
                dataGridViewTextBoxColumn2.DataPropertyName = "Acc_Ename";
                dataGridViewTextBoxColumn4.DataPropertyName = "EOPER_NAME";
                cbo_year.Items.Clear();
                cbo_year.Items.Add("current Data");
                cbo_year.Items.Add("Old period data");
            }
            Grd_years.AutoGenerateColumns = false;
        }
        //---------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {
            cbo_year.SelectedIndex = 0;
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;
            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            connection.SqlExec("exec search_Reveal_Profit_Loss ", "Term_Tbl");

            CboTerm.DataSource = connection.SQLDS.Tables["Term_Tbl"];
            CboTerm.DisplayMember = connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name";
            CboTerm.ValueMember = "T_ID";

            Cbo_Level.DataSource = connection.SQLDS.Tables["Term_Tbl1"];
            Cbo_Level.DisplayMember ="Acc_Level";
            Cbo_Level.ValueMember = "Acc_Level";

            Cbo_style.DataSource = connection.SQLDS.Tables["Term_Tbl2"];
            Cbo_style.DisplayMember = "Temp_ID";
            Cbo_style.ValueMember = "Temp_ID";
           
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            Change_voucher = false;
            string[] Str = { "Profit_loss_TBL", "Profit_loss_TBL1"};
            foreach (string Tbl in Str)
            { 
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);      
            }
            _Bs_pl.DataSource = new BindingSource();

            if (cbo_year.SelectedIndex == 0)
            {

                if (TxtFromDate.Checked == true)
                {

                    DateTime date = TxtFromDate.Value.Date;
                    From_Date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    From_Date = 0;
                }

                if (TxtToDate.Checked == true)
                {

                    DateTime date = TxtToDate.Value.Date;
                    To_Date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    To_Date = 0;
                }

                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString(); 

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }

                }

                if (Convert.ToInt32(From_Date) > Convert.ToInt32(To_Date) && Convert.ToInt32(To_Date) != 0)
           
               {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
               }
                 
            }
            if (cbo_year.SelectedIndex == 1 && Grd_years.RowCount > 0)//old


            { 
                if (TxtFromDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                DateTime date_from = TxtFromDate.Value.Date;
                 from_nrec_date = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
                DateTime date_to = TxtToDate.Value.Date;
                 to_nrec_date = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

                 int Real_from_date = Convert.ToInt32(connection.SQLDS.Tables["Term_Tbl4"].Rows[0]["min_nrec_date_int"]);
                 int Real_To_date = Convert.ToInt32(connection.SQLDS.Tables["Term_Tbl4"].Rows[0]["max_nrec_date_int"]);

                 if ((from_nrec_date < Real_from_date) || (from_nrec_date > Real_To_date) || (to_nrec_date < Real_from_date) || (to_nrec_date > Real_To_date))
                 {
                     MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
                     return;
                 }

                if (from_nrec_date > to_nrec_date)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }

            }
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "Profit_loss_statment_shadow" : "Profit_loss_statment_Phst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                if (cbo_year.SelectedIndex == 0)//cureent
                {
                    connection.SQLCMD.Parameters.AddWithValue("@T_id", Convert.ToInt16(CboTerm.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", From_Date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", To_Date);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Convert.ToByte(Cbo_Level.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@Style_Id", Convert.ToByte(Cbo_style.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@Param_From_Date", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_From_Date"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@Param_to_Date", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_to_Date"].Direction = ParameterDirection.Output;
                }
                else
                {
                    connection.SQLCMD.Parameters.AddWithValue("@T_id", Convert.ToInt16(CboTerm.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date",  from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Convert.ToByte(Cbo_Level.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@Style_Id", Convert.ToByte(Cbo_style.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@Param_From_Date", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_From_Date"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.Add("@Param_to_Date", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_to_Date"].Direction = ParameterDirection.Output;
             //     connection.SQLCMD.Parameters.AddWithValue("@p_id", Convert.ToByte(((DataRowView)_bsyears.Current).Row["P_ID"]));// period max
                }
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Profit_loss_TBL");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Profit_loss_TBL1");
                obj.Close();
                connection.SQLCS.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                Txt_Real_From.Text = connection.SQLCMD.Parameters["@Param_From_Date"].Value.ToString();
                Txt_Real_To.Text = connection.SQLCMD.Parameters["@Param_to_Date"].Value.ToString();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();

               
                
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
            Grd_Acc_Details.AutoGenerateColumns = false;
            _Bs_pl.DataSource =connection.SQLDS.Tables["Profit_loss_TBL"];
            Grd_Acc_Details.DataSource = _Bs_pl;

            if (connection.SQLDS.Tables["Profit_loss_TBL"].Rows.Count > 0)
            {
                Change_voucher = true;
                Grd_Acc_Details_SelectionChanged(null, null);
            }
        }

        private void Grd_Acc_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_voucher)
            {
               
                try
                {
                     Acc_No = ((DataRowView)_Bs_pl.Current).Row["Acc_No"].ToString();
                     Acc_level =Convert.ToInt16(((DataRowView)_Bs_pl.Current).Row["Acc_level"]);
                     if (Acc_level > 2)
                    {
                        Dot_Acc_no = ((DataRowView)_Bs_pl.Current).Row["dot_acc_no"].ToString();
                        Grd_Voucher_Deatails.DataSource = connection.SQLDS.Tables["Profit_loss_TBL1"].DefaultView.ToTable().Select(" dot_acc_no like '" + Dot_Acc_no + "%'").CopyToDataTable();
                    }
                    else
                    {
                        Grd_Voucher_Deatails.DataSource = new BindingSource();
                    }
                }
                catch
                {
                    Grd_Voucher_Deatails.DataSource = new BindingSource();
                
                }
            
            }

        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //لتفريغ الكرد عند الكتابة
        private void TxtFromDate_TextChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Voucher_Deatails.DataSource = new BindingSource();
            _Bs_pl.DataSource = new BindingSource();
            Txt_Real_From.ResetText();
            Txt_Real_To.ResetText();

            string[] Str = { "Profit_loss_TBL", "Profit_loss_TBL1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
            
        }

        private void TxtToDate_TextChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Voucher_Deatails.DataSource = new BindingSource();
            _Bs_pl.DataSource = new BindingSource();
            Txt_Real_From.ResetText();
            Txt_Real_To.ResetText();
            string[] Str = { "Profit_loss_TBL", "Profit_loss_TBL1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
            
        }

        private void Cbo_Level_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Voucher_Deatails.DataSource = new BindingSource();  
            string[] Str = { "Profit_loss_TBL", "Profit_loss_TBL1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
            
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (Grd_Acc_Details.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود للطباعة " : " No record to print ", MyGeneral_Lib.LblCap);
                return;
            }
            Profit_Loss_PrintExport Frm = new Profit_Loss_PrintExport(Convert.ToString(Txt_Real_From.Text), Convert.ToString(Txt_Real_To.Text), Convert.ToString(Cbo_Level.SelectedValue), Convert.ToString(CboTerm.Text));
            Frm.Dgv1 = Grd_Acc_Details;
            Frm.Dgv2 = Grd_Voucher_Deatails;
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;

        }

        private void Reveal_Profit_Loss_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change_voucher = false; 
            string[] Used_Tbl = { "Profit_loss_TBL", "Profit_loss_TBL1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Reveal_Profit_Loss_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Voucher_Deatails.DataSource = new BindingSource();

            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();
                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");
                TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();
                TxtToDate.Checked = false;
                

            }
            if (cbo_year.SelectedIndex == 1)// old 
            {
                Grd_years.Enabled = true;
              //  _bsyears.DataSource = connection.SQLDS.Tables["Term_Tbl3"];
                if (connection.SQLDS.Tables["Term_Tbl3"].Rows.Count > 0)
                {
                    Grd_years.DataSource = connection.SQLDS.Tables["Term_Tbl3"].Select("t_id =" + connection.T_ID).CopyToDataTable();


                    //TxtFromDate.DataBindings.Clear();
                    //TxtToDate.DataBindings.Clear();

                    //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["Term_Tbl4"], "min_nrec_date");
                    //TxtToDate.DataBindings.Add("Text", connection.SQLDS.Tables["Term_Tbl4"], "max_nrec_date");


                    TxtFromDate.Text = connection.SQLDS.Tables["Term_Tbl4"].Rows[0]["min_nrec_date"].ToString();
                    TxtToDate.Text = connection.SQLDS.Tables["Term_Tbl4"].Rows[0]["max_nrec_date"].ToString();
                }

                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }



                //Change_grd = true;
                //Grd_years_SelectionChanged(null, null);
               


            }
        }

        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");




        //    }
      //  }

        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Voucher_Deatails.DataSource = new BindingSource();
        }

        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Voucher_Deatails.DataSource = new BindingSource();
        }

    }
}