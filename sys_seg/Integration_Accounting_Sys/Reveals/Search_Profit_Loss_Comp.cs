﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Profit_Loss_Comp : Form
    {
        #region Defintion
        string Sql_test = "";
        string Sql_test1 = "";
        string Sql_test2 = "";
        string SqlText = "";
        DataTable DT_Term = new DataTable();
        int T_id = 0;
        String Filter1 = "";
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs_profit_loss = new BindingSource();
        BindingSource _Bs_profit = new BindingSource();
        BindingSource _bsyears = new BindingSource();
        #endregion
        //---------------------------
        public Search_Profit_Loss_Comp()
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            if (connection.Lang_id == 2)
            {
                Column8.DataPropertyName = "Ecust_name";
                Column11.DataPropertyName = "Ecust_name";

                Cbo_year_all.Items.Clear();
                Cbo_year_all.Items.Add("current Data");
                Cbo_year_all.Items.Add("Old period data");

            }

            #endregion
        }
        private void Create_Term_Tbl()
        {
            string[] Column = { "T_ID", "ACUST_NAME", "ECUST_NAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Term = CustomControls.Custom_DataTable("DT_Term", Column, DType);
        }
     
        //-----------------------------------------------------
        private void Search_New_Load(object sender, EventArgs e)
        {
            Cbo_year_all.SelectedIndex = 0;
            Create_Term_Tbl();
            //Sql_test = "SELECT  Peroid_Assets_ID, Peroid_AName, Peroid_EName FROM Peroid_Assets_Tbl";
            //Cbo_Step.DataSource = connection.SqlExec(Sql_test, "Assets_period_tbl");
            //Cbo_Step.ValueMember = "Peroid_Assets_ID";
            //Cbo_Step.DisplayMember = connection.Lang_id == 1 ? "Peroid_AName" : "Peroid_EName";



            SqlText = "select B.T_id,A.ACUST_NAME , A.ECUST_NAME "
                  + " From CUSTOMERS A, TERMINALS B "
                  + " Where A.CUST_ID = B.CUST_ID "
                  + " And A.Cust_Flag <>0 "
                  + " Order By " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");
            _Bs_profit_loss.DataSource = connection.SqlExec(SqlText, "Term_Tbl");

            
            Cbo_Year.SelectedIndex = 0;
            Cbo_Month.SelectedIndex = 0;
            Cbo_Rpt.SelectedIndex = 0;
            if (connection.Lang_id == 2)
            {
                Cbo_Rpt.Items[0] = "Detailed Reveal of the specific period";
                Cbo_Rpt.Items[1] = "Profit and loss a comprehensive institution specified period";
                Cbo_Rpt.Items[2] = "Net profit and loss for the period at the branch level";
            }
            else
            {
                Cbo_Rpt.Items[0] = "كشف تفصيلي بالفترة المحددة";
                Cbo_Rpt.Items[1] = "أرباح وخسائر شامل للمؤسسة بالفترة المحددة";
                Cbo_Rpt.Items[2] = "صافي أرباح وخسائر للفترة على مستوى الفروع";
            }

            _Bs_profit.DataSource = DT_Term;
            Lst_Term.DataSource = _Bs_profit;
            Chk_Term_CheckedChanged(null, null);
        }
        //-------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtNO_Month.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب كتابة تاريخ ولمـــدة  " : "Check The Date TO Please ", MyGeneral_Lib.LblCap);
                 return;
            }

            if (DT_Term.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار الفرع   " : "Check The Terminals Please ", MyGeneral_Lib.LblCap);
                return;
            }

            if (DT_Term.Rows.Count > 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار فرع واحد فقط " : "Check one Terminal Please ", MyGeneral_Lib.LblCap);
                return;
            }

            if (Txt_Step.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب ادخال مدة التجميع كل  " : "You must enter the step ", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            String Str_T_id = "";
            MyGeneral_Lib.ColumnToString(DT_Term, "T_ID", out  Str_T_id);



            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = Cbo_year_all.SelectedIndex == 0 ?"Profit_loss_statment_Comp" : "Profit_loss_statment_Comp_Phst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                if (Cbo_year_all.SelectedIndex == 0)
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Term_ID", Str_T_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Term_TBl", DT_Term);
                    connection.SQLCMD.Parameters.AddWithValue("@Year", Cbo_Year.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Month", Cbo_Month.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@NO_Month", Convert.ToInt16(TxtNO_Month.Text));
                    connection.SQLCMD.Parameters.AddWithValue("@Step", Convert.ToInt16(Txt_Step.Text));
                    connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Rpt_Type", Cbo_Rpt.SelectedIndex);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200);
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                }
                else
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Term_ID", Str_T_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Term_TBl", DT_Term);
                    connection.SQLCMD.Parameters.AddWithValue("@Year", Cbo_Year.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@Month", Cbo_Month.SelectedValue);
                    connection.SQLCMD.Parameters.AddWithValue("@NO_Month", Convert.ToInt16(TxtNO_Month.Text));
                    connection.SQLCMD.Parameters.AddWithValue("@Step", Convert.ToInt16(Txt_Step.Text));
                    connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Rpt_Type", Cbo_Rpt.SelectedIndex);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200);
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SQLCMD.Parameters.AddWithValue("@p_id", Convert.ToByte(((DataRowView)_bsyears.Current).Row["P_ID"]));// period max
                }


                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Profit_Loss_Comp_TbL");
                obj.Close();


                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();

                }
                else
                {
                    //Report 
                    #region  Report

                    DataGridView GRD1 = new DataGridView();
                    int j = 1;
                    foreach (DataColumn column in connection.SQLDS.Tables["Profit_Loss_Comp_TbL"].Columns)
                    {
                        GRD1.Columns.Add("Column" + j.ToString(), column.ColumnName);
                        j = j + 1;

                    }


                    DataTable[] _EXP_DT = { connection.SQLDS.Tables["Profit_Loss_Comp_TbL"] };
                    DataGridView[] Export_GRD = { GRD1 };
                    MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

                    #endregion
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                   
                }
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            };
            string[] Used_Tbl = { "Profit_Loss_Comp_TbL" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }
            }
            
        }

        //-------------------------------------------------------------------
        private void Cbo_Dep_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Cbo_Dep.SelectedIndex == 0)
            //{
            //    TxtToDate.Enabled = false;
            //    Cbo_Peroid.Enabled = false; 
            //}
            //else
            //{
            //    TxtToDate.Enabled = true;
            //    Cbo_Peroid.Enabled = true; 
            //}
        }
        //-------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
        private void Chk_Term_CheckedChanged(object sender, EventArgs e)
        {

            if (Chk_Term.Checked)
            {
                Lst_Term_id.DataSource = _Bs_profit_loss;
                button7.Enabled = true;
                button8.Enabled = true;
                TxtTem_Name.Enabled = true;
            }
            else
            {
                if (DT_Term.Rows.Count > 0)
                {
                    for (int i = 0; i < DT_Term.Rows.Count; i++)
                    {
                        DataRow Rows = connection.SQLDS.Tables["Term_Tbl"].NewRow();
                        Rows["T_ID"] = DT_Term.Rows[i]["T_ID"];
                        Rows["ACUST_NAME"] = DT_Term.Rows[i]["ACUST_NAME"];
                        Rows["ECUST_NAME"] = DT_Term.Rows[i]["ECUST_NAME"];
                        connection.SQLDS.Tables["Term_Tbl"].Rows.Add(Rows);
                        _Bs_profit_loss.DataSource = connection.SQLDS.Tables["Term_Tbl"];
                    }
                }
                DT_Term.Clear();
                Lst_Term_id.DataSource = new DataTable();
                TxtTem_Name.ResetText();
                button5.Enabled = false;
                button6.Enabled = false;
                button7.Enabled = false;
                button8.Enabled = false;
                TxtTem_Name.Enabled = false;
            }
            //-------------------------------------------------------------------

        }

        private void TxtTem_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (int.TryParse(TxtTem_Name.Text, out T_id))
                {

                    Filter1 = "(T_id =" + TxtTem_Name.Text + ")";
                }
                else
                {
                    Filter1 = "(Acust_name like '%" + TxtTem_Name.Text + "%') or (Ecust_name like '%" + TxtTem_Name.Text + "%')";
                }

                _Bs_profit_loss.DataSource = connection.SQLDS.Tables["Term_tbl"].Select(Filter1).CopyToDataTable();
                Lst_Term_id.DataSource = _Bs_profit_loss;
            }
            catch
            {
                Lst_Term_id.DataSource = new BindingSource();
            }
            if (Lst_Term_id.Rows.Count <= 0)
            {

                button7.Enabled = false;
                button8.Enabled = false;
            }
            else
            {
                button7.Enabled = true;
                button8.Enabled = true;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Term.NewRow();

            row["T_ID"] = ((DataRowView)_Bs_profit_loss.Current).Row["T_ID"];
            row["ACUST_NAME"] = ((DataRowView)_Bs_profit_loss.Current).Row["ACUST_NAME"];
            row["ECUST_NAME"] = ((DataRowView)_Bs_profit_loss.Current).Row["ECUST_NAME"];
            DT_Term.Rows.Add(row);
            int T_ID = Convert.ToInt16(((DataRowView)_Bs_profit_loss.Current).Row["T_ID"]);
            DataRow[] DRow = connection.SQLDS.Tables["term_Tbl"].Select("T_id = " + T_ID);
            DRow[0].Delete();
            connection.SQLDS.Tables["Term_Tbl"].AcceptChanges();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button6.Enabled = true;
            button5.Enabled = true;
            if (Lst_Term_id.Rows.Count == 0)
            {
                button8.Enabled = false;
                button7.Enabled = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < connection.SQLDS.Tables["Term_Tbl"].Rows.Count; i++)
            {
                DataRow row = DT_Term.NewRow();
                row["T_ID"] = connection.SQLDS.Tables["Term_Tbl"].Rows[i]["T_ID"];
                row["ACUST_NAME"] = connection.SQLDS.Tables["Term_Tbl"].Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = connection.SQLDS.Tables["Term_Tbl"].Rows[i]["ECUST_NAME"];
                DT_Term.Rows.Add(row);
            }
            connection.SQLDS.Tables["Term_Tbl"].Clear();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button6.Enabled = true;
            button5.Enabled = true;
            button8.Enabled = false;
            button7.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataRow row = connection.SQLDS.Tables["term_Tbl"].NewRow();
            row["T_id"] = ((DataRowView)_Bs_profit.Current).Row["T_ID"];
            row["Acust_name"] = ((DataRowView)_Bs_profit.Current).Row["Acust_name"];
            row["Ecust_name"] = ((DataRowView)_Bs_profit.Current).Row["Ecust_name"];
            connection.SQLDS.Tables["Term_Tbl"].Rows.Add(row);
            connection.SQLDS.Tables["Term_Tbl"].AcceptChanges();
            DT_Term.Rows[Lst_Term.CurrentRow.Index].Delete();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            if (Lst_Term.Rows.Count == 0)
            {
                button6.Enabled = false;
                button5.Enabled = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Term.Rows.Count; i++)
            {
                DataRow Rows = connection.SQLDS.Tables["Term_Tbl"].NewRow();
                Rows["T_ID"] = DT_Term.Rows[i]["T_ID"];
                Rows["ACUST_NAME"] = DT_Term.Rows[i]["ACUST_NAME"];
                Rows["ECUST_NAME"] = DT_Term.Rows[i]["ECUST_NAME"];
                connection.SQLDS.Tables["Term_Tbl"].Rows.Add(Rows);
            }
            DT_Term.Rows.Clear();
            connection.SQLDS.Tables["Term_Tbl"].AcceptChanges();
            TxtTem_Name.Text = "";
            TxtTem_Name_TextChanged(null, null);
            button8.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = false;
            button5.Enabled = false;
        }

        private void Search_Profit_Loss_Comp_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "Profit_Loss_Comp_TbL" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }
            }
        }

        private void Search_Profit_Loss_Comp_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void Cbo_year_all_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_year_all.SelectedIndex == 0)
            {
                Sql_test1 = "select distinct Cast(Left(Nrec_Date,4)as int) as Year from Daily_Opening where Acc_Close_Flag = 1";
                connection.SqlExec(Sql_test1, "Year_Tbl");
                Cbo_Year.DataSource = connection.SqlExec(Sql_test1, "Year_Tbl");
                Cbo_Year.ValueMember = "Year";
                Cbo_Year.DisplayMember = "Year";

                Sql_test2 = "Select distinct  cast(Substring(Cast(Nrec_Date  as varchar(8)),5,2) as int) as Month  from Daily_Opening where Acc_Close_Flag = 1 ";
                connection.SqlExec(Sql_test1, "Month_Tbl");
                Cbo_Month.DataSource = connection.SqlExec(Sql_test2, "Month_Tbl");
                Cbo_Month.ValueMember = "Month";
                Cbo_Month.DisplayMember = "Month";
            }
            else
            {

                Sql_test1 = "select distinct Cast(Left(Nrec_Date,4)as int) as Year , p_id from PHst_Daily_Opening where Acc_Close_Flag = 1 ";
             _bsyears.DataSource =    connection.SqlExec(Sql_test1, "Year_Tbl");
             Cbo_Year.DataSource = _bsyears;
                Cbo_Year.ValueMember = "Year";
                Cbo_Year.DisplayMember = "Year";

                Sql_test2 = "Select distinct  cast(Substring(Cast(Nrec_Date  as varchar(8)),5,2) as int) as Month  from PHst_Daily_Opening where Acc_Close_Flag = 1 ";
                connection.SqlExec(Sql_test1, "Month_Tbl");
                Cbo_Month.DataSource = connection.SqlExec(Sql_test2, "Month_Tbl");
                Cbo_Month.ValueMember = "Month";
                Cbo_Month.DisplayMember = "Month";

            }
        }
    }
}