﻿namespace Integration_Accounting_Sys
{
    partial class Search_Trial_Balance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CboDisplay = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Cbo_Level = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_Id = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk_Cust = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.TxtCust_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHK_OP_ACC = new System.Windows.Forms.CheckBox();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHK_FTB = new System.Windows.Forms.CheckBox();
            this.TxtToDate = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtFromDate = new Integration_Accounting_Sys.MyDateTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(10, 44);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(598, 1);
            this.flowLayoutPanel2.TabIndex = 530;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(120, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(247, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(486, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(120, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(430, 10);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 527;
            this.label2.Text = "التاريـخ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 14);
            this.label1.TabIndex = 526;
            this.label1.Text = "اسم المستخــدم:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(144, 63);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 569;
            this.label3.Text = "التــاريــخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(323, 63);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 568;
            this.label5.Text = "إلــــى :";
            // 
            // CboDisplay
            // 
            this.CboDisplay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDisplay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDisplay.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDisplay.FormattingEnabled = true;
            this.CboDisplay.Items.AddRange(new object[] {
            "حساب تفصيلي",
            "حساب تجميعي"});
            this.CboDisplay.Location = new System.Drawing.Point(100, 89);
            this.CboDisplay.Name = "CboDisplay";
            this.CboDisplay.Size = new System.Drawing.Size(172, 24);
            this.CboDisplay.TabIndex = 4;
            this.CboDisplay.SelectedIndexChanged += new System.EventHandler(this.CboDisplay_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(7, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 14);
            this.label4.TabIndex = 574;
            this.label4.Text = "اسلوب العرض:";
            // 
            // Cbo_Level
            // 
            this.Cbo_Level.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Level.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Level.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Level.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Level.FormattingEnabled = true;
            this.Cbo_Level.Location = new System.Drawing.Point(99, 118);
            this.Cbo_Level.Name = "Cbo_Level";
            this.Cbo_Level.Size = new System.Drawing.Size(173, 24);
            this.Cbo_Level.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(31, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 576;
            this.label6.Text = "المستوى:";
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            this.Grd_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Cust.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust.ColumnHeadersHeight = 45;
            this.Grd_Cust.ColumnHeadersVisible = false;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.Column11});
            this.Grd_Cust.Location = new System.Drawing.Point(384, 179);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.ReadOnly = true;
            this.Grd_Cust.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust.Size = new System.Drawing.Size(222, 111);
            this.Grd_Cust.TabIndex = 13;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "T_id";
            this.Column10.Frozen = true;
            this.Column10.HeaderText = "رمز الثانوي";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 60;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Acust_name";
            this.Column11.HeaderText = "اسم الثانوي";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 160;
            // 
            // Grd_Cust_Id
            // 
            this.Grd_Cust_Id.AllowUserToAddRows = false;
            this.Grd_Cust_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cust_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cust_Id.ColumnHeadersHeight = 45;
            this.Grd_Cust_Id.ColumnHeadersVisible = false;
            this.Grd_Cust_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust_Id.DefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Cust_Id.Location = new System.Drawing.Point(50, 179);
            this.Grd_Cust_Id.Name = "Grd_Cust_Id";
            this.Grd_Cust_Id.ReadOnly = true;
            this.Grd_Cust_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust_Id.RowHeadersVisible = false;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Cust_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust_Id.Size = new System.Drawing.Size(222, 111);
            this.Grd_Cust_Id.TabIndex = 8;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "T_id";
            this.Column7.Frozen = true;
            this.Column7.HeaderText = "رمز الثانوي";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 60;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Acust_name";
            this.Column8.HeaderText = "اسم الثانوي";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 160;
            // 
            // Chk_Cust
            // 
            this.Chk_Cust.AutoSize = true;
            this.Chk_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Cust.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Cust.Location = new System.Drawing.Point(14, 157);
            this.Chk_Cust.Name = "Chk_Cust";
            this.Chk_Cust.Size = new System.Drawing.Size(85, 18);
            this.Chk_Cust.TabIndex = 6;
            this.Chk_Cust.Text = "الثـــانــوٍي:";
            this.Chk_Cust.UseVisualStyleBackColor = true;
            this.Chk_Cust.CheckedChanged += new System.EventHandler(this.Chk_Cust_CheckedChanged);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button4.Location = new System.Drawing.Point(286, 256);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 22);
            this.button4.TabIndex = 12;
            this.button4.Text = "<<";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button3.Location = new System.Drawing.Point(286, 235);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 22);
            this.button3.TabIndex = 11;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(286, 213);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 22);
            this.button2.TabIndex = 10;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(286, 191);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 22);
            this.button1.TabIndex = 9;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.BackColor = System.Drawing.Color.White;
            this.TxtCust_Name.Enabled = false;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCust_Name.Location = new System.Drawing.Point(99, 157);
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(173, 22);
            this.TxtCust_Name.TabIndex = 7;
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Name_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 44);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 303);
            this.flowLayoutPanel1.TabIndex = 585;
            // 
            // CHK_OP_ACC
            // 
            this.CHK_OP_ACC.AutoSize = true;
            this.CHK_OP_ACC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_OP_ACC.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_OP_ACC.Location = new System.Drawing.Point(15, 296);
            this.CHK_OP_ACC.Name = "CHK_OP_ACC";
            this.CHK_OP_ACC.Size = new System.Drawing.Size(278, 18);
            this.CHK_OP_ACC.TabIndex = 14;
            this.CHK_OP_ACC.Text = "عكس الحساب للأرصدة ذات الطبيعة الشاذة.";
            this.CHK_OP_ACC.UseVisualStyleBackColor = true;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(219, 356);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 27);
            this.AddBtn.TabIndex = 16;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(307, 356);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 27);
            this.ExtBtn.TabIndex = 17;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-9, 31);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(626, 1);
            this.flowLayoutPanel3.TabIndex = 589;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(607, 44);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 303);
            this.flowLayoutPanel4.TabIndex = 590;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(9, 346);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(598, 1);
            this.flowLayoutPanel5.TabIndex = 591;
            // 
            // CHK_FTB
            // 
            this.CHK_FTB.AutoSize = true;
            this.CHK_FTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_FTB.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_FTB.Location = new System.Drawing.Point(16, 320);
            this.CHK_FTB.Name = "CHK_FTB";
            this.CHK_FTB.Size = new System.Drawing.Size(165, 18);
            this.CHK_FTB.TabIndex = 15;
            this.CHK_FTB.Text = "حساب ارصدة اول المدة.";
            this.CHK_FTB.UseVisualStyleBackColor = true;
            // 
            // TxtToDate
            // 
            this.TxtToDate.DateSeperator = '/';
            this.TxtToDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TxtToDate.Location = new System.Drawing.Point(369, 59);
            this.TxtToDate.Mask = "0000/00/00";
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.PromptChar = ' ';
            this.TxtToDate.Size = new System.Drawing.Size(88, 23);
            this.TxtToDate.TabIndex = 3;
            this.TxtToDate.Text = "00000000";
            this.TxtToDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.DateSeperator = '/';
            this.TxtFromDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFromDate.Location = new System.Drawing.Point(234, 59);
            this.TxtFromDate.Mask = "0000/00/00";
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.PromptChar = ' ';
            this.TxtFromDate.Size = new System.Drawing.Size(88, 23);
            this.TxtFromDate.TabIndex = 2;
            this.TxtFromDate.Text = "00000000";
            this.TxtFromDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Search_Trial_Balance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 387);
            this.Controls.Add(this.CHK_FTB);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.CHK_OP_ACC);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.Grd_Cust_Id);
            this.Controls.Add(this.Chk_Cust);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TxtCust_Name);
            this.Controls.Add(this.Cbo_Level);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CboDisplay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Search_Trial_Balance";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "208";
            this.Text = "Search_Trial_Balance";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Search_Trial_Balance_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Search_Trial_Balance_FormClosed);
            this.Load += new System.EventHandler(this.Search_Trial_Balance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private MyDateTextBox TxtFromDate;
        private MyDateTextBox TxtToDate;
        private System.Windows.Forms.ComboBox CboDisplay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Cbo_Level;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.DataGridView Grd_Cust_Id;
        private System.Windows.Forms.CheckBox Chk_Cust;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TxtCust_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox CHK_OP_ACC;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.CheckBox CHK_FTB;
    }
}