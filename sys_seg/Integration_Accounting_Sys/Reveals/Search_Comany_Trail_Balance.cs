﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Comany_Trail_Balance : Form
    {
        #region Defintion
        public static string from_date = "";
        public static string to_date = "";
        //public static Int16 Chk_Remove_Zero = 0;
        decimal DOloc_Amount = 0;
        decimal COloc_Amount = 0;
        decimal DPloc_Amount = 0;
        decimal Cploc_Amount = 0;
        decimal DEloc_Amount = 0;
        decimal CEloc_Amount = 0;
        string @format = "dd/MM/yyyy";
        Int32 FromDate = 0;
        Int32 ToDate = 0;
     //   bool Change_grd = false;
        BindingSource _bsyears = new BindingSource();
        int min_nrec_date_int = 0;
        int from_min_nrec_date_int = 0;

        #endregion

        public Search_Comany_Trail_Balance()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_years.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                cbo_year.Items.Clear();
                cbo_year.Items.Add("current Data");
                cbo_year.Items.Add("Old period data");
            }
        }

        private void Search_Comany_Trail_Balance_Load(object sender, EventArgs e)
        {
            cbo_year.SelectedIndex = 0;
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;
            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            connection.SqlExec("Exec Search_Trial_Balance ", "SearchTrial_Balance_Tbl");
            Cbo_Level.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl1"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "AAcc_Level" : "EAcc_Level";
            CboDisplay.SelectedIndex = 0;
            Cbo_Style.SelectedIndex = 0;
            if (connection.Lang_id == 2)


            {
                Cbo_Style.Items[0] = "Only a comprehensive accounting of accounts";
                Cbo_Style.Items[1] = "A comprehensive accounting of all secondary accounts for each branch";
                Cbo_Style.Items[2] = "A comprehensive accounting of all the accounts and secondary";
            }
            else 
            { 
                  Cbo_Style.Items[0] = "شامل للحسابات المحاسبية فقط ";
                Cbo_Style.Items[1] = "شامل لجميع الحسابات المحاسبية والثانوية لكل فرع";
                Cbo_Style.Items[2] = "شامل لجميع الحسابات المحاسبية والثانوية";
            }
            if (connection.Lang_id == 2)
            {
                CboDisplay.Items[0] = "A detailed account";
                CboDisplay.Items[1] = "Aggregate account";

            }
            else
            {
                CboDisplay.Items[0] = "حساب تفصيلي";

                CboDisplay.Items[1] = "حساب تجميعي";

            }
        }

        private void CboDisplay_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (CboDisplay.SelectedIndex == 0)
            { Cbo_Level.Enabled = true; }
            else
            { Cbo_Level.Enabled = false; }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (CboDisplay.SelectedIndex == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "قيد الانجاز" : "In progress", MyGeneral_Lib.LblCap);
                return;

            }
            if (CboDisplay.SelectedIndex == 0 && Convert.ToInt16(Cbo_Level.SelectedValue)> 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "قيد الانجاز" : "In progress", MyGeneral_Lib.LblCap);
                return;

            }
            if (cbo_year.SelectedIndex == 0)
            {
                if (TxtFromDate.Checked == true)
                {

                    DateTime date = TxtFromDate.Value.Date;
                    FromDate = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    FromDate = 0;
                }

                if (TxtToDate.Checked == true)
                {

                    DateTime date = TxtToDate.Value.Date;
                    ToDate = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    ToDate = 0;
                }


                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }
                }

                if (Convert.ToInt32(FromDate) > Convert.ToInt32(ToDate) && Convert.ToInt32(ToDate) != 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            if (cbo_year.SelectedIndex == 1)//old
            {
                if (TxtFromDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                DateTime date_from = TxtFromDate.Value.Date;
                FromDate = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
                DateTime date_to = TxtToDate.Value.Date;
                ToDate = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

                int Real_from_date = Convert.ToInt32(connection.SQLDS.Tables["SearchTrial_Balance_Tbl3"].Rows[0]["min_nrec_date_int"]);
                int Real_To_date = Convert.ToInt32(connection.SQLDS.Tables["SearchTrial_Balance_Tbl3"].Rows[0]["max_nrec_date_int"]);

                if ((FromDate < Real_from_date) || (FromDate > Real_To_date) || (ToDate < Real_from_date) || (ToDate > Real_To_date))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (FromDate > ToDate)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }

            }
            //TxtFromDate.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            //TxtToDate.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
         
            //FromDate = Convert.ToInt32(TxtFromDate.Text);
            //ToDate = Convert.ToInt32(TxtToDate.Text);



            try
            {

        connection.SQLCS.Open();
        connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "Trail_Balance_Main_Comp_test" : "Trail_Balance_Main_Comp_test_phst";
        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
        connection.SQLCMD.Connection = connection.SQLCS;
        connection.SQLCMD.CommandTimeout = 0;
        if (cbo_year.SelectedIndex == 0)//cureent
        {
            connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", FromDate);
            connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", ToDate);
            connection.SQLCMD.Parameters.AddWithValue("@FTB_Flag", CHK_FTB.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Cbo_Level.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Op_Acc_Flag", Convert.ToByte(CHK_OP_ACC.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@View_Style", CboDisplay.SelectedIndex);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Style", Cbo_Style.SelectedIndex);
            connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_Remove_Zero", Chk_Remove_Zero.Checked);//استبعاد الارصدة المصفرة
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@RealFromDate", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@RealFromDate"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@RealToDate", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@RealToDate"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@DOloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@DOloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@COloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@COloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@DPloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@DPloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Cploc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@Cploc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@DEloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@DEloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@CEloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@CEloc_Amount"].Direction = ParameterDirection.Output;
        }

        else
        {
            connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", FromDate);
            connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", ToDate);
            connection.SQLCMD.Parameters.AddWithValue("@FTB_Flag", CHK_FTB.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_level", Cbo_Level.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Op_Acc_Flag", Convert.ToByte(CHK_OP_ACC.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@View_Style", CboDisplay.SelectedIndex);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Style", Cbo_Style.SelectedIndex);
            connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_Remove_Zero", Chk_Remove_Zero.Checked);//استبعاد الارصدة المصفرة
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@RealFromDate", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@RealFromDate"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@RealToDate", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@RealToDate"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@DOloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@DOloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@COloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@COloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@DPloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@DPloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Cploc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@Cploc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@DEloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@DEloc_Amount"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@CEloc_Amount", SqlDbType.Float).Value = 0;
            connection.SQLCMD.Parameters["@CEloc_Amount"].Direction = ParameterDirection.Output;
           // connection.SQLCMD.Parameters.AddWithValue("@p_id", Convert.ToByte(((DataRowView)_bsyears.Current).Row["P_ID"]));// period max
           // connection.SQLCMD.Parameters.AddWithValue("@chk_close_year", Convert.ToByte(checkBox1.Checked));// clsoe year 
        
        }
            IDataReader obj = connection.SQLCMD.ExecuteReader();

            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Trail_Balance_Tbl");
            if (Cbo_Style.SelectedIndex == 0)
            {
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Trail_Balance_Tbl1");
            }
            obj.Close();

            connection.SQLCS.Close();
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
            MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
            connection.SQLCMD.Parameters.Clear();
            return;
            }

                if (Chk_Remove_Zero.Checked == true)
                {
                    DOloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@DOloc_Amount"].Value);
                    COloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@COloc_Amount"].Value);
                    DPloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@DPloc_Amount"].Value);
                    Cploc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@Cploc_Amount"].Value);
                    DEloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@DEloc_Amount"].Value);
                    CEloc_Amount = Convert.ToDecimal(connection.SQLCMD.Parameters["@CEloc_Amount"].Value);
                }
                from_date = connection.SQLCMD.Parameters["@RealFromDate"].Value.ToString();
                to_date = connection.SQLCMD.Parameters["@RealToDate"].Value.ToString();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();


                Int16 chk_zero = 0;
                chk_zero = Convert.ToInt16(Chk_Remove_Zero.Checked);

                if (Cbo_Style.SelectedIndex == 0)
                {
                    Company_Trial_Balance_Main_Details Frm = new Company_Trial_Balance_Main_Details(chk_zero, DOloc_Amount, COloc_Amount, DPloc_Amount, Cploc_Amount, DEloc_Amount, CEloc_Amount);
                    this.Visible = false;
                    Frm.ShowDialog(this);
                    this.Visible = true;
                    this.Enabled = true;
                }

                if (Cbo_Style.SelectedIndex > 0)
                {
                    Customer_Trial_Balance Frm = new Customer_Trial_Balance(Convert.ToInt16(Cbo_Style.SelectedIndex));
                    this.Visible = false;
                    Frm.ShowDialog(this);
                    this.Visible = true;
                    this.Enabled = true;
                }
            }
            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Search_Comany_Trail_Balance_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();
                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

                TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();
                TxtToDate.Checked = false;
               // checkBox1.Visible = false;

            }
            if (cbo_year.SelectedIndex == 1)// old 
            {
                try
                {
                    Grd_years.Enabled = true;
                    //_bsyears.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl2"];
                    //Grd_years.DataSource = _bsyears;

                    if (connection.SQLDS.Tables["SearchTrial_Balance_Tbl2"].Rows.Count > 0)
                    {

                        Grd_years.DataSource = connection.SQLDS.Tables["SearchTrial_Balance_Tbl2"].Select("t_id =" + connection.T_ID).CopyToDataTable();


                        TxtFromDate.Text = connection.SQLDS.Tables["SearchTrial_Balance_Tbl3"].Rows[0]["min_nrec_date"].ToString();
                        TxtToDate.Text = connection.SQLDS.Tables["SearchTrial_Balance_Tbl3"].Rows[0]["max_nrec_date"].ToString();
                    }

                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                        cbo_year.SelectedIndex = 0;
                        return;
                    }
                }

                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }
                //Change_grd = true;
                //Grd_years_SelectionChanged(null, null);
               //checkBox1.Visible = true;


            }
        }

        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");
        //    }
        //}
    }
}