﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Record_Operation : Form
    {
        #region Definition
        DataTable DT_Class;
        DataTable DT_Oper;
        DataTable DT_Cust;
        DataTable DT_User;
        DataTable Class;
        DataTable Oper;
        DataTable Cust;
        DataTable User;
        string Result = "";
        string Cond_Str = "";
        string Cond_Str_cust = "";
        string Cond_Str_user = "";
        string Class_Result = "";
        string Class_Cond_Str = "";
        string SqlText = "";
        //string MinNrec_Date = "";
        //string MaxNrec_Date = "";
        Int32 from_nrec_date = 0;
        Int32 to_nrec_date = 0;
        string @format = "dd/MM/yyyy";
        BindingSource _bsyears = new BindingSource();
        bool Change_grd = false;
        int min_nrec_date_int = 0;
        int from_min_nrec_date_int = 0;
        #endregion
        public Search_Record_Operation()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Create_Class_Tbl();
            Create_Oper_Tbl();
            Create_Cust_Tbl();
            Create_user_Tbl();
            Grd_Class_1.AutoGenerateColumns = false;
            Grd_Class_2.AutoGenerateColumns = false;
            Grd_Cust_1.AutoGenerateColumns = false;
            Grd_Cust_2.AutoGenerateColumns = false;
            Grd_Oper_1.AutoGenerateColumns = false;
            Grd_Oper_2.AutoGenerateColumns = false;
            Grd_years.AutoGenerateColumns = false;
            
            
            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "EClass_name";
                Column6.DataPropertyName = "Eoper_name";
                Column10.DataPropertyName = "ECust_name";
            }
 
        }
        //-----------------------------------------------------
        private void Create_Class_Tbl()
        {
            string[] Column = { "Class_Id", "Class_name" };
            string[] DType = { "System.Int16", "System.String" };
            DT_Class = CustomControls.Custom_DataTable("DT_Class", Column, DType);
            Grd_Class_2.DataSource = DT_Class;
        }
        //-----------------------------------------------------
        private void Create_Oper_Tbl()
        {
            string[] Column = { "OPER_ID", "OPER_NAME" };
            string[] DType = { "System.Int16", "System.String" };
            DT_Oper = CustomControls.Custom_DataTable("DT_Oper", Column, DType);
            Grd_Oper_2.DataSource = DT_Oper;
        }
        //-----------------------------------------------------
        private void Create_Cust_Tbl()
        {
            string[] Column = { "T_ID", "CUST_NAME" };
            string[] DType = { "System.Int16", "System.String" };
            DT_Cust = CustomControls.Custom_DataTable("DT_Cust", Column, DType);
            Grd_Cust_2.DataSource = DT_Cust;
        }
        //-----------------------------------------------------
        private void Create_user_Tbl()
        {
            string[] Column = { "user_Id", "User_name" };
            string[] DType = { "System.Int16", "System.String" };
            DT_User = CustomControls.Custom_DataTable("DT_User", Column, DType);
            GrdUser_2.DataSource = DT_User;
        }
        //-----------------------------------------------------
        private void Search_Record_Operation_Load(object sender, EventArgs e)
        {

            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;
            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            //TxtFromDate.Checked = true;
            //TxtToDate.Checked = true;

            connection.SqlExec("Exec Search_Record_Operation", "Records_Tbl");

            cbo_year.SelectedIndex = 0;
            //if (connection.SQLDS.Tables["Records_Tbl"].Rows.Count > 0)
            //{
            //    TxtMaxNrec_Date.Text = connection.SQLDS.Tables["Records_Tbl"].Rows[0]["Max"].ToString();
            //    TxtMinNrec_Date.Text = connection.SQLDS.Tables["Records_Tbl"].Rows[0]["Min"].ToString();
            //}

        }
        //-----------------------------------------------------
        private void Class_Check_CheckedChanged(object sender, EventArgs e)
        {
            if (Class_Check.Checked)
            {
                TxtClass_Name.Enabled = true;
                Class_Button1.Enabled = true;
                Class_Button2.Enabled = true;
                Class_Button3.Enabled = true;
                Class_Button4.Enabled = true;
                TxtClass_Name_TextChanged(sender, e);
            }
            else
            {
                TxtClass_Name.ResetText();
                TxtClass_Name.Enabled = false;
                Class_Button1.Enabled = false;
                Class_Button2.Enabled = false;
                Class_Button3.Enabled = false;
                Class_Button4.Enabled = false;
               if( Class!=null ) Class.Clear();
                DT_Class.Clear();
                Oper_Check.Checked = false;

            }
        }
        //-----------------------------------------------------
        private void TxtClass_Name_TextChanged(object sender, EventArgs e)
        {
            Result = string.Empty;
            Cond_Str = string.Empty;

            if (DT_Class.Rows.Count > 0)
            { 
            MyGeneral_Lib.ColumnToString(DT_Class, "class_id",out Result);
            Cond_Str = " And class_id not in (" + Result + ")";
            }

            try
            {
                int Class_id = 0;
                int.TryParse(TxtClass_Name.Text, out Class_id);
                Class = connection.SQLDS.Tables["Records_Tbl"].Select("(AClass_name like '%"
                     + TxtClass_Name.Text + "%' or " + "EClass_name like '%" + TxtClass_Name.Text
                     + "%' or " + "Class_id = " + Class_id + ")" + Cond_Str).CopyToDataTable();
                 Grd_Class_1.DataSource = Class ;
            }
            catch 
            {
                Grd_Class_1.DataSource = new DataTable(); 
            }
            Class_button_Enable();
        }
        //-----------------------------------------------------
        private void Class_button_Enable()
        {
            if (Grd_Class_1.RowCount <= 0)
            {
                Class_Button1.Enabled = false;
                Class_Button2.Enabled = false;
            }
            else
            {
                Class_Button1.Enabled = true;
                Class_Button2.Enabled = true;
            }
            if (Grd_Class_2.RowCount <= 0)
            {

                Class_Button3.Enabled = false;
                Class_Button4.Enabled = false;
            }
            else
            {
                Class_Button3.Enabled = true;
                Class_Button4.Enabled = true;
            }
        }
        //-----------------------------------------------------
        private void Class_Button1_Click(object sender, EventArgs e)
        {
            DataRow Class_Row = DT_Class.NewRow();
            Class_Row["Class_id"] = Class.Rows[Grd_Class_1.CurrentRow.Index]["Class_id"];
            Class_Row["Class_name"] = Class.Rows[Grd_Class_1.CurrentRow.Index][connection.Lang_id == 1 ? "Aclass_name" : "Eclass_name"];
            DT_Class.Rows.Add(Class_Row);
            TxtClass_Name_TextChanged(sender, e);
            if (Oper_Check.Checked)
            {
                DT_Oper.Clear();
                TxtOper_Name_TextChanged(sender, e);
            }
        }
        //-----------------------------------------------------
        private void Class_Button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Class.Rows.Count; i++)
            {
                DataRow Class_Row = DT_Class.NewRow();
                Class_Row["Class_id"] = Class.Rows[i]["Class_id"];
                Class_Row["Class_name"] = Class.Rows[i][connection.Lang_id == 1 ? "Aclass_Name" : "Eclass_name"];
                DT_Class.Rows.Add(Class_Row);
            }
            Class.Clear();
            Class_button_Enable();
            if (Oper_Check.Checked )
            {
                DT_Oper.Clear();
                TxtOper_Name_TextChanged(sender, e);
            }
        }
        //-----------------------------------------------------
        private void Class_Button3_Click(object sender, EventArgs e)
        {
            DT_Class.Rows[Grd_Class_2.CurrentRow.Index].Delete();
            TxtClass_Name_TextChanged(sender, e);
            if (Oper_Check.Checked)
            {
                DT_Oper.Clear();
                TxtOper_Name_TextChanged(sender, e);
            }
        }
        //-----------------------------------------------------
        private void Class_Button4_Click(object sender, EventArgs e)
        {
            DT_Class.Rows.Clear();
            TxtClass_Name_TextChanged(sender, e);
            Class_button_Enable();
            if (Oper_Check.Checked)
            {
                DT_Oper.Clear();
                TxtOper_Name_TextChanged(sender, e);
            }
        }
        //-----------------------------------------------------
        private void Oper_Check_CheckedChanged(object sender, EventArgs e)
        {
            if (Oper_Check.Checked)
            {
                TxtOper_Name.Enabled = true;
                Oper_Button1.Enabled = true;
                Oper_Button2.Enabled = true;
                Oper_Button3.Enabled = true;
                Oper_Button4.Enabled = true;
                TxtOper_Name_TextChanged(sender, e);
            }
            else
            {
                TxtOper_Name.ResetText();
                TxtOper_Name.Enabled = false;
                Oper_Button1.Enabled = false;
                Oper_Button2.Enabled = false;
                Oper_Button3.Enabled = false;
                Oper_Button4.Enabled = false;
                if(Oper!=null) Oper.Clear();
                DT_Oper.Clear();
            }
        }
        //-----------------------------------------------------
        private void TxtOper_Name_TextChanged(object sender, EventArgs e)
        {
            Result = string.Empty;
            Cond_Str = string.Empty;
            Class_Result = string.Empty;
            Class_Cond_Str = string.Empty;
            if (DT_Oper.Rows.Count > 0)
            {
                MyGeneral_Lib.ColumnToString(DT_Oper, "Oper_Id", out Result);
                Cond_Str = " And Oper_id not in (" + Result + ")";
            }

            if (DT_Class.Rows.Count > 0)
            {
                MyGeneral_Lib.ColumnToString(DT_Class, "class_id", out Class_Result);
                Class_Cond_Str = " And class_id in (" + Class_Result + ")";
            }

             try
            {
                int Oper_id = 0;
                int.TryParse(TxtOper_Name.Text, out Oper_id);
                Oper = connection.SQLDS.Tables["Records_Tbl1"].Select("(AOper_name like '%"
                    + TxtOper_Name.Text + "%' or " + "EOper_name like '%" + TxtOper_Name.Text
                    + "%' or " + "Oper_id = " + Oper_id + ")" + Class_Cond_Str + Cond_Str).CopyToDataTable();
               Grd_Oper_1.DataSource = Oper;
            }
            catch 
            {
                Grd_Oper_1.DataSource = new DataTable(); 
            }

            Oper_button_Enable();
        }
        //----------------------------------------------------
        private void Oper_button_Enable()
        {
            if (Grd_Oper_1.RowCount <= 0)
            {
                Oper_Button1.Enabled = false;
                Oper_Button2.Enabled = false;
            }
            else
            {
                Oper_Button1.Enabled = true;
                Oper_Button2.Enabled = true;
            }
            if (Grd_Oper_2.RowCount <= 0)
            {
                Oper_Button3.Enabled = false;
                Oper_Button4.Enabled = false;
            }
            else
            {
                Oper_Button3.Enabled = true;
                Oper_Button4.Enabled = true;
            }
        }
        //----------------------------------------------------
        private void Oper_Button1_Click(object sender, EventArgs e)
        {
            DataRow Oper_Row = DT_Oper.NewRow();
            Oper_Row["Oper_id"] = Oper.Rows[Grd_Oper_1.CurrentRow.Index]["Oper_id"];
            Oper_Row["Oper_name"] = Oper.Rows[Grd_Oper_1.CurrentRow.Index][connection.Lang_id == 1 ? "Aoper_name" : "Eoper_name"];
            DT_Oper.Rows.Add(Oper_Row);
            TxtOper_Name_TextChanged(sender, e);
        }
        //----------------------------------------------------
        private void Oper_Button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Oper.Rows.Count; i++)
            {
                DataRow Oper_Row = DT_Oper.NewRow();
                Oper_Row["Oper_id"] = Oper.Rows[i]["Oper_id"];
                Oper_Row["Oper_name"] = Oper.Rows[i][connection.Lang_id == 1 ? "AOper_Name" : "EOper_name"];
                DT_Oper.Rows.Add(Oper_Row);
            }
            Oper.Clear();
            Oper_button_Enable();
        }
        //----------------------------------------------------
        private void Oper_Button3_Click(object sender, EventArgs e)
        {
            DT_Oper.Rows[Grd_Oper_2.CurrentRow.Index ].Delete();
            TxtOper_Name_TextChanged(sender, e);
        }
        //----------------------------------------------------
        private void Oper_Button4_Click(object sender, EventArgs e)
        {
            DT_Oper.Rows.Clear();
            TxtOper_Name_TextChanged(sender, e);
            Oper_button_Enable();
        }
        //-----------------------------------------------------
        private void Cust_Check_CheckedChanged(object sender, EventArgs e)
        {
            if (Cust_Check.Checked)
            {
                TxtCust_Name.Enabled = true;
                Cust_Button1.Enabled = true;
                Cust_Button2.Enabled = true;
                Cust_Button3.Enabled = true;
                Cust_Button4.Enabled = true;
                TxtCust_Name_TextChanged(sender, e);
            }
            else
            {
                TxtCust_Name.ResetText();
                TxtCust_Name.Enabled = false;
                Cust_Button1.Enabled = false;
                Cust_Button2.Enabled = false;
                Cust_Button3.Enabled = false;
                Cust_Button4.Enabled = false;
                if(Cust !=null) Cust.Clear();
                DT_Cust.Clear();
            }
        }
        //-----------------------------------------------------
        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            Result = string.Empty;
            Cond_Str = string.Empty;

            if (DT_Cust.Rows.Count > 0)
            {
                MyGeneral_Lib.ColumnToString(DT_Cust, "T_id", out Result);
                Cond_Str = " And T_id not in (" + Result + ")";
            }

            try
            {
                int T_id = 0;
                int.TryParse(TxtCust_Name.Text, out T_id);
                Cust = connection.SQLDS.Tables["Records_Tbl2"].Select("(ACust_name like '%"
                     + TxtCust_Name.Text + "%' or " + "ECust_name like '%" + TxtCust_Name.Text
                     + "%' or " + "T_id = " + T_id + ")" + Cond_Str).CopyToDataTable();
                Grd_Cust_1.DataSource = Cust;
            }
            catch
            {
                Grd_Cust_1.DataSource = new DataTable();
            }
            Cust_button_Enable();
        }
        //-----------------------------------------------------
        private void Cust_button_Enable()
        {
            if (Grd_Cust_1.RowCount <= 0)
            {
                Cust_Button1.Enabled = false;
                Cust_Button2.Enabled = false;
            }
            else
            {
                Cust_Button1.Enabled = true;
                Cust_Button2.Enabled = true;
            }
            if (Grd_Cust_2.RowCount <= 0)
            {
                Cust_Button3.Enabled = false;
                Cust_Button4.Enabled = false;
            }
            else
            {
                Cust_Button3.Enabled = true;
                Cust_Button4.Enabled = true;
            }
        }
        //-----------------------------------------------------
        private void Cust_Button1_Click(object sender, EventArgs e)
        {
            DataRow Cust_Row = DT_Cust.NewRow();
            Cust_Row["T_id"] = Cust.Rows[Grd_Cust_1.CurrentRow.Index]["T_id"];
            Cust_Row["Cust_name"] = Cust.Rows[Grd_Cust_1.CurrentRow.Index][connection.Lang_id == 1 ? "ACust_name" : "ECust_name"];
            DT_Cust.Rows.Add(Cust_Row);
            TxtCust_Name_TextChanged(sender, e);
        }
        //-----------------------------------------------------
        private void Cust_Button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Cust.Rows.Count; i++)
            {
                DataRow Cust_Row = DT_Cust.NewRow();
                Cust_Row["T_ID"] = Cust.Rows[i]["T_id"];
                Cust_Row["Cust_name"] = Cust.Rows[i][connection.Lang_id == 1 ? "ACust_Name" : "ECust_name"];
                DT_Cust.Rows.Add(Cust_Row);
            }
            Cust.Clear();
            Cust_button_Enable();
        }
        //-----------------------------------------------------
        private void Cust_Button3_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows[Grd_Cust_2.CurrentRow.Index].Delete();
            TxtCust_Name_TextChanged(sender, e);
        }
        //-----------------------------------------------------
        private void Cust_Button4_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows.Clear();
            TxtCust_Name_TextChanged(sender, e);
            Cust_button_Enable();
        }
        //-----------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region 


            if (Class_Check.Checked && !Oper_Check.Checked)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد العــملية " : "Select The operations Please ", MyGeneral_Lib.LblCap);
                return;
            }
            if (DT_Oper.Rows.Count == 0 && Oper_Check.Checked)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " حدد العــملية" : "Select The operations Please ", MyGeneral_Lib.LblCap);
                return;
            }

            if (DT_Cust.Rows.Count == 0 && Cust_Check.Checked)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الثـــانوي" : "Select The Customer Please ", MyGeneral_Lib.LblCap);
                return;
            }

         if (cbo_year.SelectedIndex == 0)

          {

            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ بداية الفترة" : "select the date of first period", MyGeneral_Lib.LblCap);
                return;
            }

            if (TxtToDate.Checked == true)
            {

                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                return;
            }

      
                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }

                }

                if (Convert.ToInt32(from_nrec_date) > Convert.ToInt32(from_nrec_date) && Convert.ToInt32(to_nrec_date) != 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }

            }
            if (cbo_year.SelectedIndex == 1)//old
            {
                if (TxtFromDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                DateTime date_from = TxtFromDate.Value.Date;
                from_nrec_date = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
                DateTime date_to = TxtToDate.Value.Date;
                to_nrec_date = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

                int Real_from_date = Convert.ToInt32(connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["min_nrec_date_int"]);
                int Real_To_date = Convert.ToInt32(connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["max_nrec_date_int"]);

                if ((from_nrec_date < Real_from_date) || (from_nrec_date > Real_To_date) || (to_nrec_date < Real_from_date) || (to_nrec_date > Real_To_date))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (from_nrec_date > to_nrec_date)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }

            }


            #endregion

            string Oper_ = "";
            string Cust_ = "";
            string User_ = "";

            if (DT_Oper.Rows.Count > 0)
            {
                MyGeneral_Lib.ColumnToString(DT_Oper, "Oper_id", out Oper_);
            }
            //-----------------------------------------------------
            if (DT_Cust.Rows.Count > 0)
            { 
                MyGeneral_Lib.ColumnToString(DT_Cust, "T_id", out Cust_); 
            }
            //-------------------------------------------------------------
            if (DT_User.Rows.Count > 0)
            {
                MyGeneral_Lib.ColumnToString(DT_User, "user_id", out User_);
            }
            //string[] Param = { , , , , , ,  };

            string[] Str = { "Vo_Record_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }

           try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "Reveal_Record_Operation" : "Reveal_Record_Operation_phst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                if (cbo_year.SelectedIndex == 0)//cureent
                {

                    connection.SQLCMD.Parameters.AddWithValue("@Min_Vo",  Txt_Min_Vo_no.Text);
                    connection.SQLCMD.Parameters.AddWithValue("@Max_Vo",  Txt_Max_Vo_no.Text);
                    connection.SQLCMD.Parameters.AddWithValue("@Min_N_Date", from_nrec_date.ToString()  );
                    connection.SQLCMD.Parameters.AddWithValue("@Max_N_Date", to_nrec_date.ToString() );
                    connection.SQLCMD.Parameters.AddWithValue("@Str_Oper_Tbl",  Oper_);
                    connection.SQLCMD.Parameters.AddWithValue("@Str_Term_Tbl", Cust_ );
                    connection.SQLCMD.Parameters.AddWithValue("@Str_User_Tbl", User_ );
                    
                }
                else
                {
                    connection.SQLCMD.Parameters.AddWithValue("@Min_Vo",  Txt_Min_Vo_no.Text);
                    connection.SQLCMD.Parameters.AddWithValue("@Max_Vo",  Txt_Max_Vo_no.Text);
                    connection.SQLCMD.Parameters.AddWithValue("@Min_N_Date", from_nrec_date.ToString()  );
                    connection.SQLCMD.Parameters.AddWithValue("@Max_N_Date", to_nrec_date.ToString() );
                    connection.SQLCMD.Parameters.AddWithValue("@Str_Oper_Tbl",  Oper_);
                    connection.SQLCMD.Parameters.AddWithValue("@Str_Term_Tbl", Cust_ );
                    connection.SQLCMD.Parameters.AddWithValue("@Str_User_Tbl", User_ );
                   // connection.SQLCMD.Parameters.AddWithValue("@p_id",  Convert.ToByte(((DataRowView)_bsyears.Current).Row["P_ID"]));
                }
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Vo_Record_Tbl");

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();

             Reveal_Record_Operation AddFrm = new Reveal_Record_Operation();
            this.Visible = false;
            AddFrm.ShowDialog(this);
            this.Visible = true;

           
           }
            catch
           {
               
               connection.SQLCS.Close();
               connection.SQLCMD.Parameters.Clear();
               connection.SQLCMD.Dispose();
           }

        }
        //-----------------------------------------------------
        private void Search_Record_Operation_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Records_Tbl", "Records_Tbl1", "Records_Tbl2", "Records_Tbl3", "Records_Tbl4" }; ;
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);

            }
        }
        //----------------------------------------------------------------------------
        private void User_Check_CheckedChanged(object sender, EventArgs e)
        {
            if (User_Check.Checked)
            {
                Txt_User.Enabled = true;
                User_Button1.Enabled = true;
                User_Button2.Enabled = true;
                User_Button3.Enabled = true;
                User_Button4.Enabled = true;
                Txt_User_TextChanged(sender, e);
            }
            else
            {
                Txt_User.ResetText();
                Txt_User.Enabled = false;
                User_Button1.Enabled = false;
                User_Button2.Enabled = false;
                User_Button3.Enabled = false;
                User_Button4.Enabled = false;
                if (User != null) User.Clear();
                DT_User.Clear();
            }
        }
        //----------------------------------------------------------------------------
        private void Txt_User_TextChanged(object sender, EventArgs e)
        {
            Result = string.Empty;
            Cond_Str_user = string.Empty;
            //if (DT_Cust.Rows.Count > 0)
            //{
            //    MyGeneral_Lib.ColumnToString(DT_Cust, "T_id", out Result);
            //    Cond_Str_cust = " And T_id  in (" + Result + ")";
            //}
            if (DT_User.Rows.Count > 0)
            {
                MyGeneral_Lib.ColumnToString(DT_User, "user_id", out Result);
                Cond_Str_user = " And user_id  not in  (" + Result + ")";
            }
            try
            {
                int User_ID = 0;
                int.TryParse(Txt_User.Text, out User_ID);
                User = connection.SQLDS.Tables["Records_Tbl3"].Select("(user_name like '%"
                     + Txt_User.Text + "%' "
                     + " or " + "User_ID = " + User_ID + ")"  + Cond_Str_user).CopyToDataTable();
                GrdUser_1.DataSource = User;
            }
            catch
            {
                GrdUser_1.DataSource = new DataTable();
            }
            user_button_Enable();
        }
        //----------------------------------------------------------------------------
        private void user_button_Enable()
        {
            if (GrdUser_1.RowCount <= 0)
            {
                User_Button1.Enabled = false;
                User_Button2.Enabled = false;
            }
            else
            {
                User_Button1.Enabled = true;
                User_Button2.Enabled = true;
            }
            if (GrdUser_2.RowCount <= 0)
            {
                User_Button3.Enabled = false;
                User_Button4.Enabled = false;
            }
            else
            {
                User_Button3.Enabled = true;
                User_Button4.Enabled = true;
            }
        }
        //----------------------------------------------------------------------------
        private void User_Button1_Click(object sender, EventArgs e)
        {

            DataRow user_Row = DT_User.NewRow();
            user_Row["user_id"] = User.Rows[GrdUser_1.CurrentRow.Index]["user_id"];
            user_Row["user_name"] = User.Rows[GrdUser_1.CurrentRow.Index]["user_name"];
            DT_User.Rows.Add(user_Row);
            Txt_User_TextChanged(sender, e);
        }
        //----------------------------------------------------------------------------
        private void User_Button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < User.Rows.Count; i++)
            {
                DataRow user_Row = DT_User.NewRow();
                user_Row["user_id"] = User.Rows[i]["user_id"];
                user_Row["user_name"] = User.Rows[i]["user_name"];
                DT_User.Rows.Add(user_Row);
            }
            User.Clear();
            user_button_Enable();
        }
        //----------------------------------------------------------------------------
        private void User_Button3_Click(object sender, EventArgs e)
        {
            DT_User.Rows[GrdUser_2.CurrentRow.Index].Delete();
            Txt_User_TextChanged(sender, e);
        }
        //----------------------------------------------------------------------------
        private void User_Button4_Click(object sender, EventArgs e)
        {

            DT_User.Rows.Clear();
            Txt_User_TextChanged(sender, e);
            user_button_Enable();
        }

        private void Search_Record_Operation_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();
                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

                TxtFromDate.Text = connection.SQLDS.Tables["HeaderPage_Tbl2"].Rows[0]["From_Nrec_Date"].ToString();

                TxtToDate.Checked = false;
              

            }
            if (cbo_year.SelectedIndex == 1)// old 
            {

                try
                {
                    Grd_years.Enabled = true;
                    //_bsyears.DataSource = connection.SQLDS.Tables["Records_Tbl4"];
                    //Grd_years.DataSource = _bsyears;
                    if (connection.SQLDS.Tables["Records_Tbl4"].Rows.Count > 0)
                    {
                        Grd_years.DataSource = connection.SQLDS.Tables["Records_Tbl4"].Select("t_id =" + connection.T_ID).CopyToDataTable();

                        TxtFromDate.Text = connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["min_nrec_date"].ToString();
                        TxtToDate.Text = connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["max_nrec_date"].ToString();
                    }


                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                        cbo_year.SelectedIndex = 0;
                        return;
                    }
                    //Change_grd = true;
                    //Grd_years_SelectionChanged(null, null);

                }

                catch

                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }
            }
        }

        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");

        //    }
        //}
        //----------------------------------------------------------------------------
    }
}