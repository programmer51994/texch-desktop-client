﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys.Reveals
{
    public partial class Reveal_pl_Acc_Main : Form
    {
        BindingSource Terminal_bs = new BindingSource();
        BindingSource Acc_Bs = new BindingSource();
        BindingSource Details_Acc_Bs = new BindingSource();
        bool acc_id = false;
        bool details_acc = false;
         DataGridView Date_Grd = new DataGridView();
         DataTable Date_Tbl = new DataTable();
         DataTable Cust_DT= new DataTable();
         DataTable Acc_DT = new DataTable();
         DataTable Details_Acc_DT = new DataTable();
         string FromDate_1 = "";
         string ToDate_1 = "";
         string @format = "dd/MM/yyyy";
        
        public Reveal_pl_Acc_Main(string FromDate, string ToDate)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            FromDate_1 = FromDate;
            ToDate_1  = ToDate;
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Acc_Details.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "ECust_name";
                Column12.DataPropertyName = "Acc_Ename";
                Column15.DataPropertyName = "Acc_Ename";
                Column3.DataPropertyName = "Eoper_Name";

            }
           
        }

        private void Reveal_pl_Acc_Main_Load(object sender, EventArgs e)
        {
            Txt_From.Format = DateTimePickerFormat.Custom;
            Txt_From.CustomFormat = @format;
            Txt_From.Text = FromDate_1;


            Txt_TO.Format = DateTimePickerFormat.Custom;
            Txt_TO.CustomFormat = @format;
            Txt_TO.Text = ToDate_1;

            Grd_Terminals.AutoResizeColumns();

            // Configure the details DataGridView so that its columns automatically 
            // adjust their widths when the data changes.
            Grd_Terminals.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            if (connection.SQLDS.Tables["Tbl_PL_Inquery"].Rows.Count > 0)
            {
                Terminal_bs.DataSource = connection.SQLDS.Tables["Tbl_PL_Inquery"];
                Grd_Terminals.DataSource = Terminal_bs;
                acc_id = true;
                Grd_Terminals_SelectionChanged(null, null);
            }

        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Grd_Terminals_SelectionChanged(object sender, EventArgs e)
        {

            if (acc_id && connection.SQLDS.Tables["Tbl_PL_Inquery1"].Rows.Count > 0)
            {
                try
                {
                    Acc_Bs.DataSource = connection.SQLDS.Tables["Tbl_PL_Inquery1"].Select("t_id = "
                       + ((DataRowView)Terminal_bs.Current).Row["t_id"].ToString()).CopyToDataTable();
                    if (Acc_Bs.List.Count > 0)
                    {
                        Grd_Acc_Id.DataSource = Acc_Bs;
                        details_acc = true;
                        Grd_Acc_Id_SelectionChanged(null, null);
                    }
                }
                catch
                {
                    details_acc = false;
                    Grd_Acc_Id.DataSource = null;
                }

            }


        }

        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (details_acc && connection.SQLDS.Tables["Tbl_PL_Inquery2"].Rows.Count > 0)
                {
                    Details_Acc_Bs.DataSource = connection.SQLDS.Tables["Tbl_PL_Inquery2"].Select("t_id = "
                    + ((DataRowView)Terminal_bs.Current).Row["t_id"].ToString()
                    + " and acc_id = " + ((DataRowView)Acc_Bs.Current).Row["acc_id"].ToString()).CopyToDataTable();
                    if (Details_Acc_Bs.List.Count > 0)
                    {
                        Grd_Acc_Details.DataSource = Details_Acc_Bs;
                    }
                }
                else { Grd_Acc_Details.DataSource = null; }

            }
            catch
            {
                Grd_Acc_Details.DataSource = null;
            }
        }

        private void Reveal_pl_Acc_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            acc_id = false;
            details_acc = false;
            string[] Used_Tbl = { "Tbl_PL_Inquery", "Tbl_PL_Inquery1", 
                                     "Tbl_PL_Inquery2"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }



        private void Date()
        {
            if (Date_Grd.ColumnCount > 0)
            { Date_Grd.Columns.Remove("Column1");
            Date_Grd.Columns.Remove("Column2");
            Date_Tbl.Columns.Remove("From_Date");
            Date_Tbl.Columns.Remove("To_Date");
            }
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");
            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            Date_Tbl.Rows.Add(new object[] { Txt_From.Text, Txt_TO.Text });
        }

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {
            Date();
            Cust_DT = connection.SQLDS.Tables["Tbl_PL_Inquery"].DefaultView.ToTable().Select().CopyToDataTable();
            Acc_DT = connection.SQLDS.Tables["Tbl_PL_Inquery1"].DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "loc_amount").Select("Acc_id = " + ((DataRowView)Acc_Bs.Current).Row["Acc_id"]).CopyToDataTable();
            Details_Acc_DT = connection.SQLDS.Tables["Tbl_PL_Inquery2"].DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "loc_amount", "vo_no" , "Aoper_Name", "CNrec_date" , "C_Date").Select("Acc_id = " + ((DataRowView)Acc_Bs.Current).Row["Acc_id"]).CopyToDataTable();
            DataGridView[] Export_GRD = { Date_Grd, Grd_Terminals, Grd_Acc_Id, Grd_Acc_Details };
            DataTable[] Export_DT = { Date_Tbl, Cust_DT, Acc_DT, Details_Acc_DT };
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {

            Date();
            Cust_DT = connection.SQLDS.Tables["Tbl_PL_Inquery"].DefaultView.ToTable().Select().CopyToDataTable();
            Acc_DT = connection.SQLDS.Tables["Tbl_PL_Inquery1"].DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "loc_amount").Select().CopyToDataTable();
            Details_Acc_DT = connection.SQLDS.Tables["Tbl_PL_Inquery2"].DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "loc_amount", "vo_no", "Aoper_Name", "CNrec_date", "C_Date").Select().CopyToDataTable();
            DataGridView[] Export_GRD = { Date_Grd, Grd_Terminals, Grd_Acc_Id, Grd_Acc_Details };
            DataTable[] Export_DT = { Date_Tbl, Cust_DT, Acc_DT, Details_Acc_DT };
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        
    }
}