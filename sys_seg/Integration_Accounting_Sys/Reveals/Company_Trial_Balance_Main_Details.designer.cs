﻿namespace Integration_Accounting_Sys
{
    partial class Company_Trial_Balance_Main_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Grd_Acc_Details = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Grd_Details = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.AddBtn = new System.Windows.Forms.Button();
            this.Txt_Real_To = new System.Windows.Forms.MaskedTextBox();
            this.Txt_Real_From = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_SumELoc = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_SumOloc = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_SumPloc = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtED_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtEC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPD_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOD_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Details)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(25, 44);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(0, 14);
            this.label2.TabIndex = 718;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1094, 1);
            this.flowLayoutPanel1.TabIndex = 716;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(99, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(242, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(880, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(59, 14);
            this.label4.TabIndex = 710;
            this.label4.Text = "التاريــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(943, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(82, 14);
            this.label1.TabIndex = 709;
            this.label1.Text = "المستخــــدم:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 33);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1087, 1);
            this.flowLayoutPanel4.TabIndex = 722;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(1091, 33);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 556);
            this.flowLayoutPanel7.TabIndex = 723;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 34);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 555);
            this.flowLayoutPanel6.TabIndex = 725;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(820, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(261, 20);
            this.label7.TabIndex = 741;
            this.label7.Text = "                ارصـــدة نهايـــة المـــــدة (ع/م)  ";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(556, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(264, 20);
            this.label8.TabIndex = 740;
            this.label8.Text = "                   ارصــــدة الفتـــــرة (ع/م)  ";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(296, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(258, 20);
            this.label10.TabIndex = 739;
            this.label10.Text = "                ارصــدة اول المـــدة(ع/م)  ";
            // 
            // Grd_Acc_Details
            // 
            this.Grd_Acc_Details.AllowUserToAddRows = false;
            this.Grd_Acc_Details.AllowUserToDeleteRows = false;
            this.Grd_Acc_Details.AllowUserToResizeColumns = false;
            this.Grd_Acc_Details.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Acc_Details.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Acc_Details.ColumnHeadersHeight = 41;
            this.Grd_Acc_Details.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.Grd_Acc_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13});
            this.Grd_Acc_Details.Location = new System.Drawing.Point(10, 40);
            this.Grd_Acc_Details.Name = "Grd_Acc_Details";
            this.Grd_Acc_Details.ReadOnly = true;
            this.Grd_Acc_Details.RowHeadersWidth = 10;
            this.Grd_Acc_Details.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Acc_Details.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Details.Size = new System.Drawing.Size(1076, 330);
            this.Grd_Acc_Details.TabIndex = 5;
            this.Grd_Acc_Details.SelectionChanged += new System.EventHandler(this.Grd_Acc_Details_SelectionChanged);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_no";
            this.Column6.HeaderText = "رقم الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column6.Width = 80;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Acc_Aname";
            this.Column7.HeaderText = "الحســـــــاب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 195;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "DOloc_Amount";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column8.HeaderText = "مديــــن";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 131;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "COloc_Amount";
            dataGridViewCellStyle4.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column9.HeaderText = "دائــــن";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Width = 131;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Ploc_Amount";
            dataGridViewCellStyle5.Format = "N3";
            this.Column10.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column10.HeaderText = "مديــــن";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column10.Width = 131;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Ploc_Amount1";
            dataGridViewCellStyle6.Format = "N3";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column11.HeaderText = "دائــــن";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column11.Width = 131;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "DEloc_Amount";
            dataGridViewCellStyle7.Format = "N3";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column12.HeaderText = "مديــــن";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column12.Width = 131;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "CEloc_Amount";
            dataGridViewCellStyle8.Format = "N3";
            this.Column13.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column13.HeaderText = "دائــــن";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column13.Width = 131;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(5, 588);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel8.TabIndex = 754;
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(547, 594);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(121, 24);
            this.Btn_Ext.TabIndex = 17;
            this.Btn_Ext.Text = "انـهـاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Ext_Click);
            // 
            // Grd_Details
            // 
            this.Grd_Details.AllowUserToAddRows = false;
            this.Grd_Details.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Details.ColumnHeadersHeight = 25;
            this.Grd_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.Column2,
            this.Column3});
            this.Grd_Details.Location = new System.Drawing.Point(9, 418);
            this.Grd_Details.Name = "Grd_Details";
            this.Grd_Details.ReadOnly = true;
            this.Grd_Details.RowHeadersWidth = 10;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Details.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_Details.Size = new System.Drawing.Size(1076, 133);
            this.Grd_Details.TabIndex = 762;
            this.Grd_Details.Tag = "213";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(10, 398);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(149, 14);
            this.label3.TabIndex = 764;
            this.label3.Text = "تحليل الحساب المحدد....";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(4, 407);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel5.TabIndex = 763;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(427, 594);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(121, 24);
            this.AddBtn.TabIndex = 769;
            this.AddBtn.Text = "طباعة وتصدير";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // Txt_Real_To
            // 
            this.Txt_Real_To.BackColor = System.Drawing.Color.White;
            this.Txt_Real_To.Enabled = false;
            this.Txt_Real_To.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Real_To.Location = new System.Drawing.Point(703, 4);
            this.Txt_Real_To.Mask = "0000/00/00";
            this.Txt_Real_To.Name = "Txt_Real_To";
            this.Txt_Real_To.PromptChar = ' ';
            this.Txt_Real_To.Size = new System.Drawing.Size(86, 23);
            this.Txt_Real_To.TabIndex = 787;
            this.Txt_Real_To.Text = "00000000";
            this.Txt_Real_To.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Real_From
            // 
            this.Txt_Real_From.BackColor = System.Drawing.Color.White;
            this.Txt_Real_From.Enabled = false;
            this.Txt_Real_From.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Real_From.Location = new System.Drawing.Point(563, 4);
            this.Txt_Real_From.Mask = "0000/00/00";
            this.Txt_Real_From.Name = "Txt_Real_From";
            this.Txt_Real_From.PromptChar = ' ';
            this.Txt_Real_From.Size = new System.Drawing.Size(89, 23);
            this.Txt_Real_From.TabIndex = 786;
            this.Txt_Real_From.Text = "00000000";
            this.Txt_Real_From.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.DarkRed;
            this.label11.Location = new System.Drawing.Point(658, 8);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(39, 14);
            this.label11.TabIndex = 785;
            this.label11.Text = "الى /";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DarkRed;
            this.label13.Location = new System.Drawing.Point(381, 8);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(176, 14);
            this.label13.TabIndex = 784;
            this.label13.Text = "الفترة الحقيقة للسجلات من /";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DarkRed;
            this.label14.Location = new System.Drawing.Point(420, 563);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(144, 14);
            this.label14.TabIndex = 788;
            this.label14.Text = "مجمـــوع الارصدة (ع. م):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkRed;
            this.label6.Location = new System.Drawing.Point(143, 379);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(144, 14);
            this.label6.TabIndex = 789;
            this.label6.Text = "مجمـــوع الارصدة (ع. م):";
            // 
            // Txt_SumELoc
            // 
            this.Txt_SumELoc.BackColor = System.Drawing.Color.White;
            this.Txt_SumELoc.Enabled = false;
            this.Txt_SumELoc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumELoc.Location = new System.Drawing.Point(916, 559);
            this.Txt_SumELoc.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumELoc.Name = "Txt_SumELoc";
            this.Txt_SumELoc.NumberDecimalDigits = 3;
            this.Txt_SumELoc.NumberDecimalSeparator = ".";
            this.Txt_SumELoc.NumberGroupSeparator = ",";
            this.Txt_SumELoc.Size = new System.Drawing.Size(167, 23);
            this.Txt_SumELoc.TabIndex = 767;
            this.Txt_SumELoc.Text = "0.000";
            // 
            // Txt_SumOloc
            // 
            this.Txt_SumOloc.BackColor = System.Drawing.Color.White;
            this.Txt_SumOloc.Enabled = false;
            this.Txt_SumOloc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumOloc.Location = new System.Drawing.Point(570, 559);
            this.Txt_SumOloc.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumOloc.Name = "Txt_SumOloc";
            this.Txt_SumOloc.NumberDecimalDigits = 3;
            this.Txt_SumOloc.NumberDecimalSeparator = ".";
            this.Txt_SumOloc.NumberGroupSeparator = ",";
            this.Txt_SumOloc.Size = new System.Drawing.Size(167, 23);
            this.Txt_SumOloc.TabIndex = 765;
            this.Txt_SumOloc.Text = "0.000";
            // 
            // Txt_SumPloc
            // 
            this.Txt_SumPloc.BackColor = System.Drawing.Color.White;
            this.Txt_SumPloc.Enabled = false;
            this.Txt_SumPloc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SumPloc.Location = new System.Drawing.Point(743, 559);
            this.Txt_SumPloc.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_SumPloc.Name = "Txt_SumPloc";
            this.Txt_SumPloc.NumberDecimalDigits = 3;
            this.Txt_SumPloc.NumberDecimalSeparator = ".";
            this.Txt_SumPloc.NumberGroupSeparator = ",";
            this.Txt_SumPloc.Size = new System.Drawing.Size(167, 23);
            this.Txt_SumPloc.TabIndex = 766;
            this.Txt_SumPloc.Text = "0.000";
            // 
            // TxtPC_LocAmount2
            // 
            this.TxtPC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtPC_LocAmount2.Enabled = false;
            this.TxtPC_LocAmount2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPC_LocAmount2.Location = new System.Drawing.Point(690, 375);
            this.TxtPC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPC_LocAmount2.Name = "TxtPC_LocAmount2";
            this.TxtPC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtPC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtPC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtPC_LocAmount2.Size = new System.Drawing.Size(127, 23);
            this.TxtPC_LocAmount2.TabIndex = 9;
            this.TxtPC_LocAmount2.Text = "0.000";
            // 
            // TxtED_LocAmount2
            // 
            this.TxtED_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtED_LocAmount2.Enabled = false;
            this.TxtED_LocAmount2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtED_LocAmount2.Location = new System.Drawing.Point(821, 375);
            this.TxtED_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtED_LocAmount2.Name = "TxtED_LocAmount2";
            this.TxtED_LocAmount2.NumberDecimalDigits = 3;
            this.TxtED_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtED_LocAmount2.NumberGroupSeparator = ",";
            this.TxtED_LocAmount2.Size = new System.Drawing.Size(127, 23);
            this.TxtED_LocAmount2.TabIndex = 10;
            this.TxtED_LocAmount2.Text = "0.000";
            // 
            // TxtEC_LocAmount2
            // 
            this.TxtEC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtEC_LocAmount2.Enabled = false;
            this.TxtEC_LocAmount2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEC_LocAmount2.Location = new System.Drawing.Point(951, 375);
            this.TxtEC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtEC_LocAmount2.Name = "TxtEC_LocAmount2";
            this.TxtEC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtEC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtEC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtEC_LocAmount2.Size = new System.Drawing.Size(133, 23);
            this.TxtEC_LocAmount2.TabIndex = 11;
            this.TxtEC_LocAmount2.Text = "0.000";
            // 
            // TxtPD_LocAmount2
            // 
            this.TxtPD_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtPD_LocAmount2.Enabled = false;
            this.TxtPD_LocAmount2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPD_LocAmount2.Location = new System.Drawing.Point(554, 375);
            this.TxtPD_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPD_LocAmount2.Name = "TxtPD_LocAmount2";
            this.TxtPD_LocAmount2.NumberDecimalDigits = 3;
            this.TxtPD_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtPD_LocAmount2.NumberGroupSeparator = ",";
            this.TxtPD_LocAmount2.Size = new System.Drawing.Size(133, 23);
            this.TxtPD_LocAmount2.TabIndex = 8;
            this.TxtPD_LocAmount2.Text = "0.000";
            // 
            // TxtOD_LocAmount2
            // 
            this.TxtOD_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtOD_LocAmount2.Enabled = false;
            this.TxtOD_LocAmount2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOD_LocAmount2.Location = new System.Drawing.Point(295, 375);
            this.TxtOD_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOD_LocAmount2.Name = "TxtOD_LocAmount2";
            this.TxtOD_LocAmount2.NumberDecimalDigits = 3;
            this.TxtOD_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtOD_LocAmount2.NumberGroupSeparator = ",";
            this.TxtOD_LocAmount2.Size = new System.Drawing.Size(124, 23);
            this.TxtOD_LocAmount2.TabIndex = 6;
            this.TxtOD_LocAmount2.Text = "0.000";
            // 
            // TxtOC_LocAmount2
            // 
            this.TxtOC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtOC_LocAmount2.Enabled = false;
            this.TxtOC_LocAmount2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOC_LocAmount2.Location = new System.Drawing.Point(420, 375);
            this.TxtOC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOC_LocAmount2.Name = "TxtOC_LocAmount2";
            this.TxtOC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtOC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtOC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtOC_LocAmount2.Size = new System.Drawing.Size(131, 23);
            this.TxtOC_LocAmount2.TabIndex = 7;
            this.TxtOC_LocAmount2.Text = "0.000";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ATerm_Name";
            this.Column1.HeaderText = "الفــرع";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 180;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Acust_Name";
            this.dataGridViewTextBoxColumn11.HeaderText = "الثانـــوي";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 270;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Cur_Aname";
            this.dataGridViewTextBoxColumn17.HeaderText = "الـعـمـلـة الاصلية";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "OLoc_Amount";
            dataGridViewCellStyle12.Format = "N3";
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn9.HeaderText = " رصيد اول المدة ـ(ع.م)ـ";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 170;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "pLoc_Amount";
            dataGridViewCellStyle13.Format = "N3";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn10.HeaderText = " رصيد الفترة ـ(ع.م)ـ";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 170;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Eloc_Amount";
            dataGridViewCellStyle14.Format = "N3";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn14.HeaderText = " رصيد نهاية المدة ـ(ع.م)ـ";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 170;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "OFor_Amount";
            dataGridViewCellStyle15.Format = "N3";
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn15.HeaderText = " رصيد اول المدة ـ(ع.ص)ـ";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 160;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "PFor_Amount";
            dataGridViewCellStyle16.Format = "N3";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle16;
            this.Column2.HeaderText = " رصيد الفترة ـ(ع.ص)ـ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 160;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "EFor_Amount";
            dataGridViewCellStyle17.Format = "N3";
            this.Column3.DefaultCellStyle = dataGridViewCellStyle17;
            this.Column3.HeaderText = " رصيد نهاية المدة ـ(ع.ص)ـ";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 160;
            // 
            // Company_Trial_Balance_Main_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 626);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_Real_To);
            this.Controls.Add(this.Txt_Real_From);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.Txt_SumELoc);
            this.Controls.Add(this.Txt_SumOloc);
            this.Controls.Add(this.Txt_SumPloc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Grd_Details);
            this.Controls.Add(this.TxtPC_LocAmount2);
            this.Controls.Add(this.TxtED_LocAmount2);
            this.Controls.Add(this.TxtEC_LocAmount2);
            this.Controls.Add(this.TxtPD_LocAmount2);
            this.Controls.Add(this.TxtOD_LocAmount2);
            this.Controls.Add(this.TxtOC_LocAmount2);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Grd_Acc_Details);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Company_Trial_Balance_Main_Details";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "266";
            this.Text = "ميزان مراجعة على مستوى الشركة";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Company_Trial_Balance_Main_Details_FormClosed);
            this.Load += new System.EventHandler(this.Trial_Balance_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Details)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView Grd_Acc_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOC_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOD_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPD_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtEC_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtED_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPC_LocAmount2;
        private System.Windows.Forms.DataGridView Grd_Details;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumELoc;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumPloc;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.MaskedTextBox Txt_Real_To;
        private System.Windows.Forms.MaskedTextBox Txt_Real_From;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_SumOloc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}