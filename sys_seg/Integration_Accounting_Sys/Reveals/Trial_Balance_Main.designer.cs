﻿namespace Integration_Accounting_Sys
{
    partial class Trial_Balance_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtToDate = new System.Windows.Forms.MaskedTextBox();
            this.TxtFromDate = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Terminals = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc_Assembly = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Grd_Acc_Details = new System.Windows.Forms.DataGridView();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.Grd_Vo_Details = new System.Windows.Forms.DataGridView();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.Btn_Pdf = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Btn_Rpt = new System.Windows.Forms.Button();
            this.Prt_Btn = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtPC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtED_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtEC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOC_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPD_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtEC_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtED_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPC_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOD_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOC_LocAmount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtPD_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtOD_LocAmount = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Terminals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Assembly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Vo_Details)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtToDate
            // 
            this.TxtToDate.BackColor = System.Drawing.Color.White;
            this.TxtToDate.Enabled = false;
            this.TxtToDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToDate.Location = new System.Drawing.Point(600, 35);
            this.TxtToDate.Mask = "0000/00/00";
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.PromptChar = ' ';
            this.TxtToDate.Size = new System.Drawing.Size(126, 23);
            this.TxtToDate.TabIndex = 3;
            this.TxtToDate.Text = "00000000";
            this.TxtToDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.BackColor = System.Drawing.Color.White;
            this.TxtFromDate.Enabled = false;
            this.TxtFromDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFromDate.Location = new System.Drawing.Point(413, 35);
            this.TxtFromDate.Mask = "0000/00/00";
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.PromptChar = ' ';
            this.TxtFromDate.Size = new System.Drawing.Size(126, 23);
            this.TxtFromDate.TabIndex = 2;
            this.TxtFromDate.Text = "00000000";
            this.TxtFromDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(323, 39);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 718;
            this.label2.Text = "التــاريــخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(546, 39);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 717;
            this.label5.Text = "إلــــى :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 31);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1094, 1);
            this.flowLayoutPanel1.TabIndex = 716;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(90, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(242, 23);
            this.TxtUser.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(880, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(59, 14);
            this.label4.TabIndex = 710;
            this.label4.Text = "التاريــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(943, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(82, 14);
            this.label1.TabIndex = 709;
            this.label1.Text = "المستخــــدم:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 60);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1087, 1);
            this.flowLayoutPanel4.TabIndex = 722;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(1091, 60);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 564);
            this.flowLayoutPanel7.TabIndex = 723;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(6, 188);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel5.TabIndex = 724;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(5, 60);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 564);
            this.flowLayoutPanel6.TabIndex = 725;
            // 
            // Grd_Terminals
            // 
            this.Grd_Terminals.AllowUserToAddRows = false;
            this.Grd_Terminals.AllowUserToDeleteRows = false;
            this.Grd_Terminals.AllowUserToResizeColumns = false;
            this.Grd_Terminals.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Terminals.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Terminals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Terminals.ColumnHeadersHeight = 30;
            this.Grd_Terminals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.Grd_Terminals.Location = new System.Drawing.Point(11, 66);
            this.Grd_Terminals.Name = "Grd_Terminals";
            this.Grd_Terminals.ReadOnly = true;
            this.Grd_Terminals.RowHeadersWidth = 10;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Terminals.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Terminals.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Terminals.Size = new System.Drawing.Size(1076, 107);
            this.Grd_Terminals.TabIndex = 4;
            this.Grd_Terminals.SelectionChanged += new System.EventHandler(this.Grd_Terminals_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cust_Id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رمز الثانوي";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Acust_Name";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "الإســم العــربي";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 270;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Ecust_Name";
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "الإســم الأجنبــي";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 268;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Min_NrecDate";
            this.Column4.HeaderText = "التاريخ مــن";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Max_NrecDate";
            this.Column5.HeaderText = "التاريخ الـــى";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 300;
            // 
            // Grd_Acc_Assembly
            // 
            this.Grd_Acc_Assembly.AllowUserToAddRows = false;
            this.Grd_Acc_Assembly.AllowUserToDeleteRows = false;
            this.Grd_Acc_Assembly.AllowUserToResizeColumns = false;
            this.Grd_Acc_Assembly.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Assembly.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Assembly.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Acc_Assembly.ColumnHeadersHeight = 40;
            this.Grd_Acc_Assembly.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13});
            this.Grd_Acc_Assembly.Location = new System.Drawing.Point(11, 198);
            this.Grd_Acc_Assembly.Name = "Grd_Acc_Assembly";
            this.Grd_Acc_Assembly.ReadOnly = true;
            this.Grd_Acc_Assembly.RowHeadersWidth = 10;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Assembly.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Acc_Assembly.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Assembly.Size = new System.Drawing.Size(1076, 107);
            this.Grd_Acc_Assembly.TabIndex = 5;
            this.Grd_Acc_Assembly.SelectionChanged += new System.EventHandler(this.Grd_Acc_Assembly_SelectionChanged);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_Id";
            this.Column6.HeaderText = "رمز الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 50;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Acc_Aname";
            this.Column7.HeaderText = "الحســـــــاب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 120;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "DOloc_Amount";
            dataGridViewCellStyle6.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column8.HeaderText = "مديــــن";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 150;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "COloc_Amount";
            dataGridViewCellStyle7.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column9.HeaderText = "دائــــن";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 150;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "DPloc_Amount";
            dataGridViewCellStyle8.Format = "N3";
            this.Column10.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column10.HeaderText = "مديــــن";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 150;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "CPloc_Amount";
            dataGridViewCellStyle9.Format = "N3";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column11.HeaderText = "دائــــن";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 150;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "DEloc_Amount";
            dataGridViewCellStyle10.Format = "N3";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column12.HeaderText = "مديــــن";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 150;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "CEloc_Amount";
            dataGridViewCellStyle11.Format = "N3";
            this.Column13.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column13.HeaderText = "دائــــن";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 150;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(191, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(300, 20);
            this.label15.TabIndex = 727;
            this.label15.Text = "                ارصــدة اول المـــدة(ع/م)  ";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(492, 201);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(298, 20);
            this.label14.TabIndex = 728;
            this.label14.Text = "                   ارصــــدة الفتـــــرة (ع/م)  ";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(791, 201);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(294, 20);
            this.label13.TabIndex = 729;
            this.label13.Text = "                ارصـــدة نهايـــة المـــــدة (ع/م)  ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(48, 311);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 736;
            this.label9.Text = "المجاميـــــع:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 344);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel2.TabIndex = 737;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(49, 466);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(75, 14);
            this.label6.TabIndex = 748;
            this.label6.Text = "المجاميـــــع:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(794, 356);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(288, 20);
            this.label7.TabIndex = 741;
            this.label7.Text = "                ارصـــدة نهايـــة المـــــدة (ع/م)  ";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(492, 356);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(301, 20);
            this.label8.TabIndex = 740;
            this.label8.Text = "                   ارصــــدة الفتـــــرة (ع/م)  ";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(194, 355);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(296, 20);
            this.label10.TabIndex = 739;
            this.label10.Text = "                ارصــدة اول المـــدة(ع/م)  ";
            // 
            // Grd_Acc_Details
            // 
            this.Grd_Acc_Details.AllowUserToAddRows = false;
            this.Grd_Acc_Details.AllowUserToDeleteRows = false;
            this.Grd_Acc_Details.AllowUserToResizeColumns = false;
            this.Grd_Acc_Details.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Acc_Details.ColumnHeadersHeight = 41;
            this.Grd_Acc_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21});
            this.Grd_Acc_Details.Location = new System.Drawing.Point(11, 353);
            this.Grd_Acc_Details.Name = "Grd_Acc_Details";
            this.Grd_Acc_Details.ReadOnly = true;
            this.Grd_Acc_Details.RowHeadersWidth = 10;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Details.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_Acc_Details.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Details.Size = new System.Drawing.Size(1076, 107);
            this.Grd_Acc_Details.TabIndex = 12;
            this.Grd_Acc_Details.SelectionChanged += new System.EventHandler(this.Grd_Acc_Details_SelectionChanged);
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "Acc_Id";
            this.Column14.HeaderText = "رمز الحساب";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 50;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Acc_Aname";
            this.Column15.HeaderText = "الحســـــــاب";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 120;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "DOloc_Amount";
            dataGridViewCellStyle15.Format = "N3";
            this.Column16.DefaultCellStyle = dataGridViewCellStyle15;
            this.Column16.HeaderText = "مديــــن";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 150;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "COloc_Amount";
            dataGridViewCellStyle16.Format = "N3";
            this.Column17.DefaultCellStyle = dataGridViewCellStyle16;
            this.Column17.HeaderText = "دائــــن";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 150;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "DPloc_Amount";
            dataGridViewCellStyle17.Format = "N3";
            this.Column18.DefaultCellStyle = dataGridViewCellStyle17;
            this.Column18.HeaderText = "مديــــن";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 150;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "CPloc_Amount";
            dataGridViewCellStyle18.Format = "N3";
            this.Column19.DefaultCellStyle = dataGridViewCellStyle18;
            this.Column19.HeaderText = "دائــــن";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 150;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "DEloc_Amount";
            dataGridViewCellStyle19.Format = "N3";
            this.Column20.DefaultCellStyle = dataGridViewCellStyle19;
            this.Column20.HeaderText = "مديــــن";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 150;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "CEloc_Amount";
            dataGridViewCellStyle20.Format = "N3";
            this.Column21.DefaultCellStyle = dataGridViewCellStyle20;
            this.Column21.HeaderText = "دائــــن";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 150;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 498);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel3.TabIndex = 749;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 653);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1094, 22);
            this.statusStrip1.TabIndex = 751;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Maroon;
            this.label11.Location = new System.Drawing.Point(13, 487);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(241, 14);
            this.label11.TabIndex = 752;
            this.label11.Text = "تفاصيل الحراكات الماليه للحساب اعلاه....";
            // 
            // Grd_Vo_Details
            // 
            this.Grd_Vo_Details.AllowUserToAddRows = false;
            this.Grd_Vo_Details.AllowUserToDeleteRows = false;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Vo_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Vo_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_Vo_Details.ColumnHeadersHeight = 30;
            this.Grd_Vo_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.Grd_Vo_Details.Location = new System.Drawing.Point(11, 505);
            this.Grd_Vo_Details.Name = "Grd_Vo_Details";
            this.Grd_Vo_Details.ReadOnly = true;
            this.Grd_Vo_Details.RowHeadersWidth = 10;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Vo_Details.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.Grd_Vo_Details.Size = new System.Drawing.Size(1076, 115);
            this.Grd_Vo_Details.TabIndex = 20;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "DLoc_Amount";
            dataGridViewCellStyle24.Format = "N3";
            this.Column22.DefaultCellStyle = dataGridViewCellStyle24;
            this.Column22.Frozen = true;
            this.Column22.HeaderText = "مديــن ـ(ع.م)ـ";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column22.Width = 150;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "CLoc_Amount";
            dataGridViewCellStyle25.Format = "N3";
            this.Column23.DefaultCellStyle = dataGridViewCellStyle25;
            this.Column23.Frozen = true;
            this.Column23.HeaderText = "دائــن ـ(ع.م)ـ";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column23.Width = 150;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "Balance_For";
            dataGridViewCellStyle26.Format = "N3";
            this.Column24.DefaultCellStyle = dataGridViewCellStyle26;
            this.Column24.HeaderText = "الرصيد ـ(ع.م)ـ";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column24.Width = 150;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "ACC_Id";
            this.Column25.HeaderText = "رقم الحســاب";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "Acc_Aname";
            this.Column26.HeaderText = "الحساب";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column26.Width = 150;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Cur_Aname";
            this.dataGridViewTextBoxColumn1.HeaderText = "الـعـمـلـة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Acust_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "الثانـــوي";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "AOPER_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "العملية";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Real_Price";
            dataGridViewCellStyle27.Format = "N7";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn4.HeaderText = "السـعر الفعلـي";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "DFor_Amount";
            dataGridViewCellStyle28.Format = "N3";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn5.HeaderText = "مدين ـ(ع.ص)ـ";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 150;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "CFor_Amount";
            dataGridViewCellStyle29.Format = "N3";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewTextBoxColumn6.HeaderText = "دائن ـ(ع.ص)ـ";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "VO_NO";
            this.dataGridViewTextBoxColumn7.HeaderText = "رقم القيد";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Notes";
            this.dataGridViewTextBoxColumn8.HeaderText = "التفاصـــيل";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 250;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(5, 623);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1086, 1);
            this.flowLayoutPanel8.TabIndex = 754;
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Browser.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Browser.Location = new System.Drawing.Point(602, 627);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(111, 24);
            this.Btn_Browser.TabIndex = 24;
            this.Btn_Browser.Text = "تصـدير اجمالي";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.button1_Click);
            // 
            // Btn_Pdf
            // 
            this.Btn_Pdf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Pdf.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Pdf.Location = new System.Drawing.Point(492, 627);
            this.Btn_Pdf.Name = "Btn_Pdf";
            this.Btn_Pdf.Size = new System.Drawing.Size(111, 24);
            this.Btn_Pdf.TabIndex = 23;
            this.Btn_Pdf.Text = "تصـدير تفصيلي";
            this.Btn_Pdf.UseVisualStyleBackColor = true;
            this.Btn_Pdf.Click += new System.EventHandler(this.Btn_Export_Details_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(712, 627);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(111, 24);
            this.Btn_Ext.TabIndex = 25;
            this.Btn_Ext.Text = "انـهـاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Ext_Click);
            // 
            // Btn_Rpt
            // 
            this.Btn_Rpt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Rpt.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Rpt.Location = new System.Drawing.Point(382, 627);
            this.Btn_Rpt.Name = "Btn_Rpt";
            this.Btn_Rpt.Size = new System.Drawing.Size(111, 24);
            this.Btn_Rpt.TabIndex = 22;
            this.Btn_Rpt.Text = "طباعة تفصيلي";
            this.Btn_Rpt.UseVisualStyleBackColor = true;
            this.Btn_Rpt.Click += new System.EventHandler(this.Btn_Print_Details_Click);
            // 
            // Prt_Btn
            // 
            this.Prt_Btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prt_Btn.ForeColor = System.Drawing.Color.Navy;
            this.Prt_Btn.Location = new System.Drawing.Point(272, 627);
            this.Prt_Btn.Name = "Prt_Btn";
            this.Prt_Btn.Size = new System.Drawing.Size(111, 24);
            this.Prt_Btn.TabIndex = 21;
            this.Prt_Btn.Text = "طباعة اجمالـي";
            this.Prt_Btn.UseVisualStyleBackColor = true;
            this.Prt_Btn.Click += new System.EventHandler(this.Btn_PrintAll_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(26, 177);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(109, 14);
            this.label12.TabIndex = 760;
            this.label12.Text = "ارصدة تجميعــة....";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(19, 334);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(114, 14);
            this.label16.TabIndex = 761;
            this.label16.Text = "ارصدة تفصيليــة....";
            // 
            // TxtPC_LocAmount2
            // 
            this.TxtPC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtPC_LocAmount2.Enabled = false;
            this.TxtPC_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPC_LocAmount2.Location = new System.Drawing.Point(592, 462);
            this.TxtPC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPC_LocAmount2.Name = "TxtPC_LocAmount2";
            this.TxtPC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtPC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtPC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtPC_LocAmount2.Size = new System.Drawing.Size(167, 23);
            this.TxtPC_LocAmount2.TabIndex = 16;
            this.TxtPC_LocAmount2.Text = "0.000";
            // 
            // TxtED_LocAmount2
            // 
            this.TxtED_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtED_LocAmount2.Enabled = false;
            this.TxtED_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtED_LocAmount2.Location = new System.Drawing.Point(762, 462);
            this.TxtED_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtED_LocAmount2.Name = "TxtED_LocAmount2";
            this.TxtED_LocAmount2.NumberDecimalDigits = 3;
            this.TxtED_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtED_LocAmount2.NumberGroupSeparator = ",";
            this.TxtED_LocAmount2.Size = new System.Drawing.Size(160, 23);
            this.TxtED_LocAmount2.TabIndex = 17;
            this.TxtED_LocAmount2.Text = "0.000";
            // 
            // TxtEC_LocAmount2
            // 
            this.TxtEC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtEC_LocAmount2.Enabled = false;
            this.TxtEC_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEC_LocAmount2.Location = new System.Drawing.Point(928, 462);
            this.TxtEC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtEC_LocAmount2.Name = "TxtEC_LocAmount2";
            this.TxtEC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtEC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtEC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtEC_LocAmount2.Size = new System.Drawing.Size(157, 23);
            this.TxtEC_LocAmount2.TabIndex = 18;
            this.TxtEC_LocAmount2.Text = "0.000";
            // 
            // TxtOC_LocAmount
            // 
            this.TxtOC_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtOC_LocAmount.Enabled = false;
            this.TxtOC_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOC_LocAmount.Location = new System.Drawing.Point(279, 307);
            this.TxtOC_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOC_LocAmount.Name = "TxtOC_LocAmount";
            this.TxtOC_LocAmount.NumberDecimalDigits = 3;
            this.TxtOC_LocAmount.NumberDecimalSeparator = ".";
            this.TxtOC_LocAmount.NumberGroupSeparator = ",";
            this.TxtOC_LocAmount.Size = new System.Drawing.Size(159, 23);
            this.TxtOC_LocAmount.TabIndex = 7;
            this.TxtOC_LocAmount.Text = "0.000";
            // 
            // TxtPD_LocAmount2
            // 
            this.TxtPD_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtPD_LocAmount2.Enabled = false;
            this.TxtPD_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPD_LocAmount2.Location = new System.Drawing.Point(437, 462);
            this.TxtPD_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPD_LocAmount2.Name = "TxtPD_LocAmount2";
            this.TxtPD_LocAmount2.NumberDecimalDigits = 3;
            this.TxtPD_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtPD_LocAmount2.NumberGroupSeparator = ",";
            this.TxtPD_LocAmount2.Size = new System.Drawing.Size(150, 23);
            this.TxtPD_LocAmount2.TabIndex = 15;
            this.TxtPD_LocAmount2.Text = "0.000";
            // 
            // TxtEC_LocAmount
            // 
            this.TxtEC_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtEC_LocAmount.Enabled = false;
            this.TxtEC_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEC_LocAmount.Location = new System.Drawing.Point(930, 307);
            this.TxtEC_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtEC_LocAmount.Name = "TxtEC_LocAmount";
            this.TxtEC_LocAmount.NumberDecimalDigits = 3;
            this.TxtEC_LocAmount.NumberDecimalSeparator = ".";
            this.TxtEC_LocAmount.NumberGroupSeparator = ",";
            this.TxtEC_LocAmount.Size = new System.Drawing.Size(155, 23);
            this.TxtEC_LocAmount.TabIndex = 11;
            this.TxtEC_LocAmount.Text = "0.000";
            // 
            // TxtED_LocAmount
            // 
            this.TxtED_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtED_LocAmount.Enabled = false;
            this.TxtED_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtED_LocAmount.Location = new System.Drawing.Point(774, 307);
            this.TxtED_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtED_LocAmount.Name = "TxtED_LocAmount";
            this.TxtED_LocAmount.NumberDecimalDigits = 3;
            this.TxtED_LocAmount.NumberDecimalSeparator = ".";
            this.TxtED_LocAmount.NumberGroupSeparator = ",";
            this.TxtED_LocAmount.Size = new System.Drawing.Size(152, 23);
            this.TxtED_LocAmount.TabIndex = 10;
            this.TxtED_LocAmount.Text = "0.000";
            // 
            // TxtPC_LocAmount
            // 
            this.TxtPC_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtPC_LocAmount.Enabled = false;
            this.TxtPC_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPC_LocAmount.Location = new System.Drawing.Point(600, 307);
            this.TxtPC_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPC_LocAmount.Name = "TxtPC_LocAmount";
            this.TxtPC_LocAmount.NumberDecimalDigits = 3;
            this.TxtPC_LocAmount.NumberDecimalSeparator = ".";
            this.TxtPC_LocAmount.NumberGroupSeparator = ",";
            this.TxtPC_LocAmount.Size = new System.Drawing.Size(168, 23);
            this.TxtPC_LocAmount.TabIndex = 9;
            this.TxtPC_LocAmount.Text = "0.000";
            this.TxtPC_LocAmount.TextChanged += new System.EventHandler(this.decimalTextBox4_TextChanged);
            // 
            // TxtOD_LocAmount2
            // 
            this.TxtOD_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtOD_LocAmount2.Enabled = false;
            this.TxtOD_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOD_LocAmount2.Location = new System.Drawing.Point(126, 462);
            this.TxtOD_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOD_LocAmount2.Name = "TxtOD_LocAmount2";
            this.TxtOD_LocAmount2.NumberDecimalDigits = 3;
            this.TxtOD_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtOD_LocAmount2.NumberGroupSeparator = ",";
            this.TxtOD_LocAmount2.Size = new System.Drawing.Size(148, 23);
            this.TxtOD_LocAmount2.TabIndex = 13;
            this.TxtOD_LocAmount2.Text = "0.000";
            // 
            // TxtOC_LocAmount2
            // 
            this.TxtOC_LocAmount2.BackColor = System.Drawing.Color.White;
            this.TxtOC_LocAmount2.Enabled = false;
            this.TxtOC_LocAmount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOC_LocAmount2.Location = new System.Drawing.Point(279, 462);
            this.TxtOC_LocAmount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOC_LocAmount2.Name = "TxtOC_LocAmount2";
            this.TxtOC_LocAmount2.NumberDecimalDigits = 3;
            this.TxtOC_LocAmount2.NumberDecimalSeparator = ".";
            this.TxtOC_LocAmount2.NumberGroupSeparator = ",";
            this.TxtOC_LocAmount2.Size = new System.Drawing.Size(151, 23);
            this.TxtOC_LocAmount2.TabIndex = 14;
            this.TxtOC_LocAmount2.Text = "0.000";
            // 
            // TxtPD_LocAmount
            // 
            this.TxtPD_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtPD_LocAmount.Enabled = false;
            this.TxtPD_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPD_LocAmount.Location = new System.Drawing.Point(442, 307);
            this.TxtPD_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtPD_LocAmount.Name = "TxtPD_LocAmount";
            this.TxtPD_LocAmount.NumberDecimalDigits = 3;
            this.TxtPD_LocAmount.NumberDecimalSeparator = ".";
            this.TxtPD_LocAmount.NumberGroupSeparator = ",";
            this.TxtPD_LocAmount.Size = new System.Drawing.Size(154, 23);
            this.TxtPD_LocAmount.TabIndex = 8;
            this.TxtPD_LocAmount.Text = "0.000";
            // 
            // TxtOD_LocAmount
            // 
            this.TxtOD_LocAmount.BackColor = System.Drawing.Color.White;
            this.TxtOD_LocAmount.Enabled = false;
            this.TxtOD_LocAmount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOD_LocAmount.Location = new System.Drawing.Point(126, 307);
            this.TxtOD_LocAmount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtOD_LocAmount.Name = "TxtOD_LocAmount";
            this.TxtOD_LocAmount.NumberDecimalDigits = 3;
            this.TxtOD_LocAmount.NumberDecimalSeparator = ".";
            this.TxtOD_LocAmount.NumberGroupSeparator = ",";
            this.TxtOD_LocAmount.Size = new System.Drawing.Size(150, 23);
            this.TxtOD_LocAmount.TabIndex = 6;
            this.TxtOD_LocAmount.Text = "0.000";
            // 
            // Trial_Balance_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 675);
            this.Controls.Add(this.TxtPC_LocAmount2);
            this.Controls.Add(this.TxtED_LocAmount2);
            this.Controls.Add(this.TxtEC_LocAmount2);
            this.Controls.Add(this.TxtOC_LocAmount);
            this.Controls.Add(this.TxtPD_LocAmount2);
            this.Controls.Add(this.TxtEC_LocAmount);
            this.Controls.Add(this.TxtED_LocAmount);
            this.Controls.Add(this.TxtPC_LocAmount);
            this.Controls.Add(this.TxtOD_LocAmount2);
            this.Controls.Add(this.TxtOC_LocAmount2);
            this.Controls.Add(this.TxtPD_LocAmount);
            this.Controls.Add(this.TxtOD_LocAmount);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Btn_Browser);
            this.Controls.Add(this.Btn_Pdf);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.Btn_Rpt);
            this.Controls.Add(this.Prt_Btn);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Grd_Vo_Details);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Grd_Acc_Details);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Grd_Acc_Assembly);
            this.Controls.Add(this.Grd_Terminals);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Trial_Balance_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "212";
            this.Text = "ميزان مراجعة تجميعي على مستوى الثانوي";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Trial_Balance_Main_FormClosed);
            this.Load += new System.EventHandler(this.Trial_Balance_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Terminals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Assembly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Details)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Vo_Details)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox TxtToDate;
        private System.Windows.Forms.MaskedTextBox TxtFromDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.DataGridView Grd_Terminals;
        private System.Windows.Forms.DataGridView Grd_Acc_Assembly;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView Grd_Acc_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView Grd_Vo_Details;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.Button Btn_Pdf;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.Button Btn_Rpt;
        private System.Windows.Forms.Button Prt_Btn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOD_LocAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPD_LocAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOC_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOD_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPC_LocAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtED_LocAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtEC_LocAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPD_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtOC_LocAmount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtEC_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtED_LocAmount2;
        private System.Windows.Forms.Sample.DecimalTextBox TxtPC_LocAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    }
}