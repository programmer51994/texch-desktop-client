﻿namespace Integration_Accounting_Sys.Reveals
{
    partial class company_Trial_Balance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtFromDate = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtToDate = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CboDisplay = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Cbo_Level = new System.Windows.Forms.ComboBox();
            this.CHK_OP_ACC = new System.Windows.Forms.CheckBox();
            this.CHK_FTB = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(387, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(82, 14);
            this.label1.TabIndex = 710;
            this.label1.Text = "المستخــــدم:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(162, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(59, 14);
            this.label4.TabIndex = 712;
            this.label4.Text = "التاريــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(12, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(144, 23);
            this.TxtIn_Rec_Date.TabIndex = 713;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-6, 34);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(626, 1);
            this.flowLayoutPanel3.TabIndex = 714;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(364, 54);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 719;
            this.label2.Text = "التــاريــخ من :";
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.BackColor = System.Drawing.Color.White;
            this.TxtFromDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFromDate.Location = new System.Drawing.Point(232, 50);
            this.TxtFromDate.Mask = "0000/00/00";
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.PromptChar = ' ';
            this.TxtFromDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtFromDate.Size = new System.Drawing.Size(126, 23);
            this.TxtFromDate.TabIndex = 720;
            this.TxtFromDate.Text = "00000000";
            this.TxtFromDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(178, 54);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 721;
            this.label5.Text = "إلــــى :";
            // 
            // TxtToDate
            // 
            this.TxtToDate.BackColor = System.Drawing.Color.White;
            this.TxtToDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToDate.Location = new System.Drawing.Point(46, 50);
            this.TxtToDate.Mask = "0000/00/00";
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.PromptChar = ' ';
            this.TxtToDate.Size = new System.Drawing.Size(126, 23);
            this.TxtToDate.TabIndex = 722;
            this.TxtToDate.Text = "00000000";
            this.TxtToDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(384, 92);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label12.Size = new System.Drawing.Size(93, 14);
            this.label12.TabIndex = 775;
            this.label12.Text = ":اسلوب العرض";
            // 
            // CboDisplay
            // 
            this.CboDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDisplay.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDisplay.FormattingEnabled = true;
            this.CboDisplay.Items.AddRange(new object[] {
            "حساب تفصيلي",
            "حساب تجميعي"});
            this.CboDisplay.Location = new System.Drawing.Point(209, 88);
            this.CboDisplay.Name = "CboDisplay";
            this.CboDisplay.Size = new System.Drawing.Size(172, 24);
            this.CboDisplay.TabIndex = 776;
            this.CboDisplay.SelectedIndexChanged += new System.EventHandler(this.CboDisplay_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(384, 126);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(71, 14);
            this.label9.TabIndex = 777;
            this.label9.Text = ":المستــوى";
            // 
            // Cbo_Level
            // 
            this.Cbo_Level.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Level.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Level.FormattingEnabled = true;
            this.Cbo_Level.Location = new System.Drawing.Point(209, 122);
            this.Cbo_Level.Name = "Cbo_Level";
            this.Cbo_Level.Size = new System.Drawing.Size(173, 24);
            this.Cbo_Level.TabIndex = 778;
            // 
            // CHK_OP_ACC
            // 
            this.CHK_OP_ACC.AutoSize = true;
            this.CHK_OP_ACC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_OP_ACC.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_OP_ACC.Location = new System.Drawing.Point(199, 172);
            this.CHK_OP_ACC.Name = "CHK_OP_ACC";
            this.CHK_OP_ACC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CHK_OP_ACC.Size = new System.Drawing.Size(278, 18);
            this.CHK_OP_ACC.TabIndex = 779;
            this.CHK_OP_ACC.Text = "عكس الحساب للأرصدة ذات الطبيعة الشاذة.";
            this.CHK_OP_ACC.UseVisualStyleBackColor = true;
            // 
            // CHK_FTB
            // 
            this.CHK_FTB.AutoSize = true;
            this.CHK_FTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_FTB.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_FTB.Location = new System.Drawing.Point(312, 196);
            this.CHK_FTB.Name = "CHK_FTB";
            this.CHK_FTB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CHK_FTB.Size = new System.Drawing.Size(165, 18);
            this.CHK_FTB.TabIndex = 780;
            this.CHK_FTB.Text = "حساب ارصدة اول المدة.";
            this.CHK_FTB.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(209, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 27);
            this.button1.TabIndex = 781;
            this.button1.Text = "عــــرض";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(247, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(134, 23);
            this.TxtUser.TabIndex = 782;
            // 
            // company_Trial_Balance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 247);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CHK_FTB);
            this.Controls.Add(this.CHK_OP_ACC);
            this.Controls.Add(this.Cbo_Level);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CboDisplay);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "company_Trial_Balance";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "company_Trial_Balance";
            this.Load += new System.EventHandler(this.company_Trial_Balance_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox TxtFromDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox TxtToDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CboDisplay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Cbo_Level;
        private System.Windows.Forms.CheckBox CHK_OP_ACC;
        private System.Windows.Forms.CheckBox CHK_FTB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TxtUser;
    }
}