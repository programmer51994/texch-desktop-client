﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys.remittences_online
{
    public partial class Remittances_Information : Form
    {
        BindingSource Bs_rem = new BindingSource();
        BindingSource Bs_Vo = new BindingSource();
        BindingSource Bs_Grdrem = new BindingSource();
        DataTable Dt_Rem = new DataTable();
        DataTable Dt_vo_inf = new DataTable();
    
        bool Change = false;
        bool Change_grd = false;
        int Mcase_id = 0;
        // bool chang_GRem = false;
        bool chang_Gcase = false;
        // bool Change = false;
        decimal param_Exch_rate_rem = 0;
        decimal param_Exch_rate_com = 0;
        string Gender = "";
        string _Date = "";
        string local_Cur = "";
        string forgin_Cur = "";
        string SqlTxt = "";
        int Oper_Id = 0;
        string Vo_No = "";
        string Rem_no = "";
        string User = "";
        string Exch_rate_com = "";
        double Total_Amount = 0;
        double Rem_Amount = 0;
        string term = "";
        string com_Cur = "";
        string local_ACur = "";
        string forgin_ACur = "";
        string comm_Acur = "";
        string Frm_id = "";


        public Remittances_Information()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grdrec_rem.AutoGenerateColumns = false;
            GrdVo_Details.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "R_ECUR_NAME";
                Column4.DataPropertyName = "PR_ECUR_NAME";
                Column8.DataPropertyName = "t_ECITY_NAME";
                Column9.DataPropertyName = "S_ECITY_NAME";
                Column5.DataPropertyName = "ECASE_NA";
                dataGridViewTextBoxColumn5.DataPropertyName = "Acc_Ename";
                dataGridViewTextBoxColumn11.DataPropertyName = "Eoper_name";
            }
        }

        private void Inquery_Remittances_Load(object sender, EventArgs e)
        {


            Bs_rem.DataSource = connection.SQLDS.Tables["Rem_Tbl"];


            if (connection.SQLDS.Tables["Rem_Tbl"].Rows.Count > 0)
            {
                //Change = true;
                //Grdrec_rem_SelectionChanged(null, null);
                Grdrec_rem.DataSource = Bs_rem;
            }

            Bs_Vo.DataSource = connection.SQLDS.Tables["VO_Tbl"];

            if (connection.SQLDS.Tables["VO_Tbl"].Rows.Count > 0)
            {
                //Change = true;
                //Grdrec_rem_SelectionChanged(null, null);
                GrdVo_Details.DataSource = Bs_Vo;
            }

          


        }

        private void Grdrec_rem_SelectionChanged(object sender, EventArgs e)
        {
          
             
        }

     

        private void Btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Print_All_Click(object sender, EventArgs e)
        {

            Rem_no = ((DataRowView)Bs_rem.Current).Row["Rem_no"].ToString();
            DataTable Dt = new DataTable();

            Dt = connection.SQLDS.Tables["Rem_Tbl"];

            Dt_Rem = connection.SQLDS.Tables["Rem_Tbl"];

           // DataGridView[] Export_GRD = { Grdrec_rem };
            DataTable Export_DT =  connection.SQLDS.Tables["Rem_Tbl"].DefaultView.ToTable(false ,"rem_no","R_amount","R_ACUR_NAME",
            "PR_ACUR_NAME","t_ACITY_NAME","S_ACITY_NAME","ACASE_NA","Case_Date","C_DATE","user_name","S_name",
            "S_address","r_name","r_address","Source_money","Relation_S_R").Select().CopyToDataTable();
            //.Select("cust_id = " + ((DataRowView)Bs_rem.Current).Row["login_NAME"]).CopyToDataTable()};

            


            DataTable Dt_vo = new DataTable();

            Dt_vo = connection.SQLDS.Tables["VO_Tbl"];

            Dt_vo_inf = connection.SQLDS.Tables["VO_Tbl"];

           // DataGridView[] Export_GRD1 = { GrdVo_Details };
            DataTable Export_DT1 =  connection.SQLDS.Tables["VO_Tbl"].DefaultView.ToTable(false ,"VO_NO","ACC_Id", connection.Lang_id==1?"Acc_Aname" : "Acc_EName","EXCH_PRICE","REAL_Price"
                                      ,"Oper_id" ,connection.Lang_id==1 ?"AOPER_NAME" : "EOPER_NAME", "NREC_DATE", "C_Date").Select().CopyToDataTable();

            //MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            //MyExports.To_ExcelByHtml(Export_DT1, Export_GRD1, true, true, this.Text);

            DataTable[] _EXP_DT = { Export_DT, Export_DT1 };
            DataGridView[] Export_GRD = { Grdrec_rem, GrdVo_Details };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

        }






        private void Inquery_Remittances_FormClosed(object sender, FormClosedEventArgs e)
        {
             Change = false;
             Change_grd = false;
             chang_Gcase = false;
            string[] Used_Tbl = { "Query_remittences_tbl", "Query_remittences_tbl1" }; 
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);

            }
        }
    }
}