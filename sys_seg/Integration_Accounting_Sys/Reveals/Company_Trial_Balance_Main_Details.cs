﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class Company_Trial_Balance_Main_Details : Form
    {
        #region Definshion
        bool AccChange = false;
        BindingSource _BS_GRD1 = new BindingSource();
        BindingSource _BS_GRD2 = new BindingSource();
        // public static BindingSource Details_Acc_Bs2 = new BindingSource();
        //public static int T_Id = 0;
        public static string Acc_No = "";
        public static string OD_LocAmount = "";
        public static string OC_LocAmount = "";
        public static string PD_LocAmount = "";
        public static string PC_LocAmount = "";
        public static string ED_LocAmount = "";
        public static string EC_LocAmount = "";
        public static DataTable Compay_trail_Balanc_Det = new DataTable();
        public static int Acc_id = 0;
        Int16 Remove_Zero = 0;
        decimal DOloc_Amount1 = 0;
        decimal COloc_Amount1 = 0;
        decimal DPloc_Amount1 = 0;
        decimal Cploc_Amount1 = 0;
        decimal DEloc_Amount1 = 0;
        decimal CEloc_Amount1 = 0;
        #endregion
        //---------------------------
        public Company_Trial_Balance_Main_Details(Int16 chk_zero, decimal DOloc_Amount = 0, decimal COloc_Amount = 0, decimal DPloc_Amount = 0, decimal Cploc_Amount = 0, decimal DEloc_Amount = 0, decimal CEloc_Amount = 0)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Acc_Details.AutoGenerateColumns = false;
            Remove_Zero = chk_zero;
            Grd_Details.AutoGenerateColumns = false;

            DOloc_Amount1 = DOloc_Amount;
            COloc_Amount1 = COloc_Amount;
            DPloc_Amount1 = DPloc_Amount;
            Cploc_Amount1 = Cploc_Amount;
            DEloc_Amount1 = DEloc_Amount;
            CEloc_Amount1 = CEloc_Amount;

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Column7.DataPropertyName = "Acc_Ename";
                //Column26.DataPropertyName = "ACC_Ename";
                Column1.DataPropertyName = "ETerm_Name";
                dataGridViewTextBoxColumn11.DataPropertyName = "Ecust_Name";
                dataGridViewTextBoxColumn17.DataPropertyName = "Cur_Ename";

            }
            #endregion
        }
        //----------------------------------------------
        private void Trial_Balance_Main_Load(object sender, EventArgs e)
        {


            Txt_Real_From.Text = Search_Comany_Trail_Balance.from_date;
            Txt_Real_To.Text = Search_Comany_Trail_Balance.to_date;


            if (connection.SQLDS.Tables["Trail_Balance_Tbl"].Rows.Count > 0)
            {
                _BS_GRD1.DataSource = connection.SQLDS.Tables["Trail_Balance_Tbl"];
                Grd_Acc_Details.DataSource = _BS_GRD1;

                if (Remove_Zero == 0)
                {
                    OD_LocAmount = connection.SQLDS.Tables["Trail_Balance_Tbl"].Compute("Sum(DOloc_Amount)", "").ToString();
                    TxtOD_LocAmount2.Text = OD_LocAmount;
                    OC_LocAmount = connection.SQLDS.Tables["Trail_Balance_Tbl"].Compute("Sum(COloc_Amount)", "").ToString();
                    TxtOC_LocAmount2.Text = OC_LocAmount;
                    PD_LocAmount = connection.SQLDS.Tables["Trail_Balance_Tbl"].Compute("Sum(pLoc_Amount)", "").ToString();
                    TxtPD_LocAmount2.Text = PD_LocAmount;
                    PC_LocAmount = connection.SQLDS.Tables["Trail_Balance_Tbl"].Compute("Sum(pLoc_Amount1)", "").ToString();
                    TxtPC_LocAmount2.Text = PC_LocAmount;
                    ED_LocAmount = connection.SQLDS.Tables["Trail_Balance_Tbl"].Compute("Sum(DEloc_Amount)", "").ToString();
                    TxtED_LocAmount2.Text = ED_LocAmount;
                    EC_LocAmount = connection.SQLDS.Tables["Trail_Balance_Tbl"].Compute("Sum(CEloc_Amount)", "").ToString();
                    TxtEC_LocAmount2.Text = EC_LocAmount;
                }
                else
                {

                    TxtOD_LocAmount2.Text = DOloc_Amount1.ToString();
                    TxtOC_LocAmount2.Text = COloc_Amount1.ToString();
                    TxtPD_LocAmount2.Text = DPloc_Amount1.ToString();
                    TxtPC_LocAmount2.Text = Cploc_Amount1.ToString();
                    TxtED_LocAmount2.Text = DEloc_Amount1.ToString();
                    TxtEC_LocAmount2.Text = CEloc_Amount1.ToString();
                }

                AccChange = true;
                Grd_Acc_Details_SelectionChanged(null, null);

            }

          
        }

        //----------------------------------------------
        private void Grd_Acc_Details_SelectionChanged(object sender, EventArgs e)
        {
            if (AccChange)
            {

                Acc_id = Convert.ToInt32(((DataRowView)_BS_GRD1.Current).Row["Acc_id"]);
                Acc_No = ((DataRowView)_BS_GRD1.Current).Row["Acc_No"].ToString();
                _BS_GRD2.DataSource = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("Acc_Id = " + Acc_id).CopyToDataTable();
                Grd_Details.DataSource = _BS_GRD2;

                Txt_SumOloc.Text = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("Acc_id =" + Acc_id).CopyToDataTable().Compute("Sum(OLoc_Amount)", "").ToString();
                Txt_SumPloc.Text = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("Acc_id =" + Acc_id).CopyToDataTable().Compute("Sum(pLoc_Amount)", "").ToString();
                Txt_SumELoc.Text = connection.SQLDS.Tables["Trail_Balance_Tbl1"].Select("Acc_id =" + Acc_id).CopyToDataTable().Compute("Sum(Eloc_Amount)", "").ToString();

            }
        }
        //----------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {

            if (Grd_Acc_Details.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود للطباعة " : " No record to print ", MyGeneral_Lib.LblCap);
                return;
            }
            string name = ((DataRowView)_BS_GRD1.Current).Row["Acc_No"].ToString();
            Compay_trail_Balanc_Det = connection.SQLDS.Tables["Trail_Balance_Tbl"].Select("Acc_no like '" + name + "'").CopyToDataTable();

            Company_trial_PrintExport Frm = new Company_trial_PrintExport(Convert.ToString(Txt_Real_From.Text), Convert.ToString(Txt_Real_To.Text));
            Frm.Dgv1 = Grd_Acc_Details;
            Frm.Dgv2 = Grd_Details;
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;
        }

        private void TxtFromDate_TextChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Details.DataSource = new BindingSource();
            TxtOD_LocAmount2.Text  = "0";         
            TxtOC_LocAmount2.Text  = "0";
            TxtPD_LocAmount2.Text  = "0";
            TxtPC_LocAmount2.Text = "0";
            TxtED_LocAmount2.Text = "0";
            TxtEC_LocAmount2.Text = "0";
            Txt_SumOloc.Text = "0";
            Txt_SumPloc.Text = "0";
            Txt_SumELoc.Text = "0";
            Txt_Real_From.Text = "0000/00/00";
            Txt_Real_To.Text = "0000/00/00";
            string[] Str = { "Trail_Balance_Tbl", "Trail_Balance_Tbl1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtToDate_TextChanged(object sender, EventArgs e)
        {
            Grd_Acc_Details.DataSource = new BindingSource();
            Grd_Details.DataSource = new BindingSource();
            TxtOD_LocAmount2.Text = "0";
            TxtOC_LocAmount2.Text = "0";
            TxtPD_LocAmount2.Text = "0";
            TxtPC_LocAmount2.Text = "0";
            TxtED_LocAmount2.Text = "0";
            TxtEC_LocAmount2.Text = "0";
            Txt_SumOloc.Text = "0";
            Txt_SumPloc.Text = "0";
            Txt_SumELoc.Text = "0";
            Txt_Real_From.Text = "0000/00/00";
            Txt_Real_To.Text = "0000/00/00";
            string[] Str = { "Trail_Balance_Tbl", "Trail_Balance_Tbl1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }

        }

        private void Company_Trial_Balance_Main_Details_FormClosed(object sender, FormClosedEventArgs e)
        {
            AccChange = false; 
            string[] Used_Tbl = { "Trail_Balance_Tbl", "Trail_Balance_Tbl1", "Cur_Buy_Sal_Tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
    }
}