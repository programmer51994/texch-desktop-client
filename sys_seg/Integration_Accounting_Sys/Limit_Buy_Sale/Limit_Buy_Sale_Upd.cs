﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{

    public partial class Limit_Buy_Sale_Upd : Form
    {
        #region Defintion
        int Cur_id = 0;
        DataTable lmt_Tbl;
        #endregion
        public Limit_Buy_Sale_Upd()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Create_lmt_Tbl();
        }
        //----------------------------------
        private void Create_lmt_Tbl()
        {
            string[] Column = { "Cur_id",  "Amount" };
            string[] DType = { "System.Int32", "System.Decimal" };
            lmt_Tbl = CustomControls.Custom_DataTable("lmt_Tbl", Column, DType);

        }
        //----------------------------------
        private void Limit_Buy_Sale_Upd_Load(object sender, EventArgs e)
        {
            Cur_id = Convert.ToInt32(((DataRowView)connection.SQLBS.Current).Row["cur_id"]);
            Txt_Amount.Text = ((DataRowView)connection.SQLBS.Current).Row["Amount"].ToString();
            TxtCurr_Name.Text = ((DataRowView)connection.SQLBS.Current).Row[connection.Lang_id ==1 ? "Cur_Aname":"Cur_Ename"].ToString();
        }
        //----------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(Txt_Amount.Text) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ رجاءاً" : "Please Check The Amount ", MyGeneral_Lib.LblCap);
                Txt_Amount.Focus();
                return;
            }

            DataRow LmtDrow = lmt_Tbl.NewRow();
            LmtDrow["Cur_id"] = Cur_id;
            LmtDrow["Amount"] = Convert.ToDecimal(Txt_Amount.Text);
            lmt_Tbl.Rows.Add(LmtDrow);
            connection.SQLCMD.Parameters.AddWithValue("@Cur_id", Cur_id);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@limit_Amount_Tbl", lmt_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@Notes", "");
            connection.SQLCMD.Parameters.AddWithValue("@Frm_id", 2);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_Del_Limit_Buy_Sale", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }

       
    }
}
