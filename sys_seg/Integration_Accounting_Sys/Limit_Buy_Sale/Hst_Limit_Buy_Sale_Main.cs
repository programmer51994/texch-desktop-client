﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Hst_Limit_Buy_Sale_Main : Form
    {
        string Sql_Text = "";
        int Cur_id = 0;
        public Hst_Limit_Buy_Sale_Main()
        {

            InitializeComponent();
            connection.SQLBSMainGrd.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Hst_Limit_Buy_Sale.AutoGenerateColumns = false;
            Column4.DataPropertyName = connection.Lang_id == 1 ? "Dl_Flag_ANote" : "Dl_Flag_ENote";
        }
        //----------------------------------
        private void Hst_Limit_Buy_Sale_Main_Load(object sender, EventArgs e)
        {
             Cur_id = Convert.ToInt32(((DataRowView)connection.SQLBS.Current).Row["cur_id"]);
             Sql_Text = " SELECT a.Cur_Id,Cur_aname,Cur_ename , Amount, User_Name, Isnull(a.U_Date,a.C_Date) as C_Date , "
                         + " case Dl_Flag  when 1 then' Deleted '  end  + NOTES  as Dl_Flag_ENote ,case Dl_Flag  when 1 then ' محــذوف '  end  + NOTES  as Dl_Flag_ANote "
                         + " FROM  Hst_Limit_Buy_Sale a,USERS b,Cur_Tbl c  "
                         + " where a.User_Id = b.USER_ID  "
                         + " and a.Cur_id  = c.Cur_ID  and a.Cur_id = " + Cur_id 
                         + " order by C_Date ";
            connection.SQLBSMainGrd.DataSource = connection.SqlExec(Sql_Text , "Hst_lmt_Tbl");

            if (connection.SQLDS.Tables["Hst_lmt_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد معلومات تاريخية لهذه العملة" : "No History Info. for this Currency");
                this.Close();
                return; 
            }

            TxtCur.Text = ((DataRowView)connection.SQLBSMainGrd.Current).Row["Cur_id"].ToString();
            TxtCur_name.Text = ((DataRowView)connection.SQLBSMainGrd.Current).Row[connection.Lang_id == 1?"Cur_Aname":"Cur_EName"].ToString();
            Grd_Hst_Limit_Buy_Sale.DataSource = connection.SQLBSMainGrd;
            Grd_Hst_Limit_Buy_Sale_SelectionChanged(null, null);
        }
        //----------------------------------
        private void Grd_Hst_Limit_Buy_Sale_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(connection.SQLBSMainGrd);
        }
        //----------------------------------
        private void Hst_Limit_Buy_Sale_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
           
            if (connection.SQLDS.Tables.Contains("Hst_lmt_Tbl"))
                connection.SQLDS.Tables.Remove("Hst_lmt_Tbl");
        }
        //----------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
