﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Limit_Buy_Sale_Add : Form
    {
        #region Defintion
        string Sql_Text = "";
        DataTable lmt_Tbl;
        string Str = "";
        string Result = "";
        private BindingSource _Bs = new BindingSource();
        
        #endregion
        public Limit_Buy_Sale_Add()
        {
            InitializeComponent();
            connection.SQLBSLst.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Create_lmt_Tbl();
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_LmtAmount.AutoGenerateColumns = false;
            Column4.DataPropertyName = connection.Lang_id == 1 ? "Cur_aname" : "Cur_ename";
        }
        //-----------------------------------       
        private void Create_lmt_Tbl()
        {
            string[] Column = { "Cur_id","Cur_name", "Amount" };
            string[] DType = { "System.Int32", "System.String", "System.Decimal" };
            lmt_Tbl = CustomControls.Custom_DataTable("lmt_Tbl", Column, DType);
            Grd_LmtAmount.DataSource = _Bs;
        }
        //-----------------------------------       
        private void TxtCurr_Name_TextChanged(object sender, EventArgs e)
       {
           Result = string.Empty;
           Str = string.Empty;
           if (lmt_Tbl.Rows.Count > 0)
           {
               for (int i = 0; i < lmt_Tbl.Rows.Count; i++)
               {
                   Result += lmt_Tbl.Rows[i]["Cur_id"].ToString() + ",";
               }
               Result = Result.Substring(0, Result.Length - 1);
               Str = " And A.Cur_id not in (" + Result + ")";
           }

           Sql_Text = " select b.Cur_Id ,Cur_aname,Cur_ename from Cur_Tbl A, Currencies_Rate B "
                      + " where A.Cur_ID = B.Cur_Id "
                      + " and(Cur_aname like '" + TxtCurr_Name.Text + "%'"
                      + " or Cur_Ename like '" + TxtCurr_Name.Text + "%' "
                      + " or A.Cur_id like '" + TxtCurr_Name.Text + "%') "
                      + " and A.Cur_Id not in (select Cur_Id from Limit_Buy_Sale)"
                      + Str
                      + " order by " + (connection.Lang_id == 1 ? "Cur_aname" : "Cur_ename");
                 connection.SQLBSLst.DataSource   = connection.SqlExec(Sql_Text, "Cur_Tbl");
                 Grd_Cur_Id.DataSource = connection.SQLBSLst;
        }
        //-----------------------------------       
        private void Limit_Buy_Sale_Add_Upd_Load(object sender, EventArgs e)
        {
                TxtCurr_Name_TextChanged(sender, e);
        }
        //-----------------------------------       
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Grd_Cur_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد عمــلة للاضافة" : "No Currenct To Add", MyGeneral_Lib.LblCap);
                return;
            }

            int Rec_No = (from DataRow row in lmt_Tbl.Rows
                          where
                              row["Amount"] == DBNull.Value || (decimal)row["Amount"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0 && lmt_Tbl.Rows.Count > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Enter  The Amount", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion 
            DataRow LmtDrow = lmt_Tbl.NewRow();
            LmtDrow["Cur_id"] = ((DataRowView)connection.SQLBSLst.Current).Row["cur_id"];
            LmtDrow["Cur_name"] = ((DataRowView)connection.SQLBSLst.Current).Row[connection.Lang_id ==1 ?"Cur_aname":"Cur_ename"];
            lmt_Tbl.Rows.Add(LmtDrow);
            _Bs.DataSource = lmt_Tbl;
            TxtCurr_Name_TextChanged(sender, e);
        }
        //-----------------------------------       
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Grd_LmtAmount.RowCount  <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات للاضافة" : "No Data To Add", MyGeneral_Lib.LblCap);
                return;
            }

            int Rec_No = (from DataRow row in lmt_Tbl.Rows
                          where
                              row["Amount"] == DBNull.Value || (decimal)row["Amount"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0 && lmt_Tbl.Rows.Count > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المبلغ" : "Enter  The Amount", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            lmt_Tbl = lmt_Tbl.DefaultView.ToTable(false, "Cur_id", "Amount");
            connection.SQLCMD.Parameters.AddWithValue("@Cur_id", 0);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@limit_Amount_Tbl", lmt_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@Notes", "");
            connection.SQLCMD.Parameters.AddWithValue("@Frm_id", 1);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_Del_Limit_Buy_Sale", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();

        }
        //-----------------------------------    
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (lmt_Tbl.Rows.Count > 0)
            {
                lmt_Tbl.Rows[Grd_LmtAmount.CurrentRow.Index].Delete();
                TxtCurr_Name_TextChanged(sender, e);
            }
        }
        //-----------------------------------    
        private void Grd_LmtAmount_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //______Required....
        }
        //-----------------------------------
        private void Limit_Buy_Sale_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
         
            if (connection.SQLDS.Tables.Contains("cur_Tbl"))
                connection.SQLDS.Tables.Remove("cur_Tbl");

        }
        //-----------------------------------
        private void Grd_LmtAmount_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= TextboxNumeric_KeyPress;
            if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column2.Index)
            {
                e.Control.KeyPress += TextboxNumeric_KeyPress;
            }
        }
        //-----------------------------------
        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == (char)46))
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
        }
    }
}
