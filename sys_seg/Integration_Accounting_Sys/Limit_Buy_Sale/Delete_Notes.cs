﻿using System;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Delete_Notes : Form
    {
        public static string Delete_Note = "";
        public static bool Delete_Flag = false;
        public Delete_Notes()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }
        //-----------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            Delete_Flag = false; 
            this.Close();
        }
        //-----------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            DialogResult Dr = MessageBox.Show(connection.Lang_id ==1 ?"هل متاكد من عملية الحذف ":"Are You Sure You Want To Delete" , MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            Delete_Flag = false; 
            if (Dr == DialogResult.Yes)
            {
                Delete_Note = TxtNotes.Text.Trim();
                Delete_Flag = true;
            }
                this.Close();
        }
        //-----------------------------
        private void Delete_Notes_Load(object sender, EventArgs e)
        {
            Delete_Flag = false; 
        }

       
    }
}
