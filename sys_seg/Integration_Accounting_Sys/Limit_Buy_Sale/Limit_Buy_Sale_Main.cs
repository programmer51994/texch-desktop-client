﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Limit_Buy_Sale_Main : Form
    {
        #region Defintion
        string Sql_Text = "";
        int Cur_id = 0;
        DataTable lmt_Tbl;
        #endregion
        public Limit_Buy_Sale_Main()
        {
            InitializeComponent();
            connection.SQLBS.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Lmt_buy_sale.AutoGenerateColumns = false;
            Column2.DataPropertyName = connection.Lang_id == 1 ? "Cur_Aname":"Cur_Ename";
        }
        //----------------------------------
        private void Create_lmt_Tbl()
        {
            string[] Column = { "Cur_id", "Amount" };
            string[] DType = { "System.Int32", "System.Decimal" };
            lmt_Tbl = CustomControls.Custom_DataTable("lmt_Tbl", Column, DType);
        }
        //-----------------------------
        private void Limit_Buy_Sale_Main_Load(object sender, EventArgs e)
        {
            Txt_CurSrch_TextChanged(sender, e);
        }
        //----------------------------------
        private void Txt_CurSrch_TextChanged(object sender, EventArgs e)
        {
            Sql_Text = " SELECT Id, a.Cur_id,Cur_aname,Cur_ename, Amount,User_name"
                        + "	FROM  Limit_Buy_Sale a ,USERS b ,Cur_Tbl c "
                        + "	Where a.Cur_id = C.Cur_ID "
                        + "	and a.User_Id = B.USER_ID "
                        + "	and (Cur_aname like '" + Txt_CurSrch.Text + "%'  "
                        + "	or Cur_Ename like '" + Txt_CurSrch.Text + "%'  "
                        + "	or a.Cur_id like '" + Txt_CurSrch.Text + "%') "
                        + "order by Cur_aname,Cur_ename";
            connection.SQLBS.DataSource = connection.SqlExec(Sql_Text, "Lmt_cur_tbl");
            Grd_Lmt_buy_sale.DataSource = connection.SQLBS;
            if (connection.SQLDS.Tables["lmt_cur_tbl"].Rows.Count > 0)
            {
                BtnUpd.Enabled = true;
                DelBtn.Enabled = true;
                Btn_Hst.Enabled = true;
                Grd_Lmt_buy_sale_SelectionChanged(sender, e);
            }
            else
            {
                BtnUpd.Enabled = false;
                DelBtn.Enabled = false;
                Btn_Hst.Enabled = false;
            }
        }
        //----------------------------------
        private void Grd_Lmt_buy_sale_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(connection.SQLBS);
        }
        //----------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Limit_Buy_Sale_Add AddFrm = new Limit_Buy_Sale_Add();
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Limit_Buy_Sale_Main_Load(sender, e);
            this.Visible = true;
        }
        //----------------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            Limit_Buy_Sale_Upd AddFrm = new Limit_Buy_Sale_Upd();
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Limit_Buy_Sale_Main_Load(sender, e);
            this.Visible = true;
        }
        //----------------------------------
        private void DelBtn_Click(object sender, EventArgs e)
        {
            Delete_Notes AddFrm = new Delete_Notes();
            AddFrm.ShowDialog(this);
            if (Delete_Notes.Delete_Flag)
            {
                Cur_id = Convert.ToInt32(((DataRowView)connection.SQLBS.Current).Row["cur_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@Cur_id", Cur_id);
                connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@limit_Amount_Tbl", lmt_Tbl);//ارسال جدول فارغ في حالة الحذف
                connection.SQLCMD.Parameters.AddWithValue("@Notes", Delete_Notes.Delete_Note);
                connection.SQLCMD.Parameters.AddWithValue("@Frm_id", 3);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SqlExec("Add_Upd_Del_Limit_Buy_Sale", connection.SQLCMD);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }
                connection.SQLCMD.Parameters.Clear();
                Limit_Buy_Sale_Main_Load(sender, e);
            }
        }
        //----------------------------------
        private void Limit_Buy_Sale_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
          
            if (connection.SQLDS.Tables.Contains("Lmt_cur_tbl"))
                connection.SQLDS.Tables.Remove("Lmt_cur_tbl");
        }
        //----------------------------------
        private void Hst_Btn_Click(object sender, EventArgs e)
        {
            Hst_Limit_Buy_Sale_Main HstFrm = new Hst_Limit_Buy_Sale_Main();
            this.Visible = false;
            HstFrm.ShowDialog(this);
            Limit_Buy_Sale_Main_Load(sender, e);
            this.Visible = true;
        }

        private void Limit_Buy_Sale_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    BtnUpd_Click(sender, e);
                    break;
                case Keys.F5:
                    Hst_Btn_Click(sender, e);
                    break;
            
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Limit_Buy_Sale_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

    
    }
}