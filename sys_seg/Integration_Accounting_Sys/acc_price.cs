﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys

{
    public partial class acc_price : Form
    {
        String Sql_Text = "";
        public acc_price()
        {
            InitializeComponent();
            GrdAcc_price.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
 
                    Sql_Text=" select * from acc_price"
 
                   +"Select Cur_Id,Cur_Aname,Cur_Ename from Cur_Tbl" 
                   +" where cur_id not in (select cur_id from acc_price Where c_date is not null)"
                   +"And cur_id <> 0"
                   +"Order By cur_Aname";

            Cbo_Cur_ID.DataSource = connection.SqlExec(Sql_Text, "acc_price");
            Cbo_Cur_ID.ValueMember = "cur_id";
            Cbo_Cur_ID.DisplayMember = (connection.Lang_id) == 1 ? "cur_Aname" : "cur_Ename";


           // Sql_Text = " SELECT A.Cur_id, Acc_close_Price, Str_Nrec_Date, End_Nrec_Date, "
            //+ " B.Cur_ANAME , B.Cur_ENAME , C.User_Name "
            //+ " FROM  Acc_Close_Price A , Cur_TBL B , Users C "
            //+ " where A.Cur_id = B.Cur_ID "
           // + " And A.user_id = C.user_id  "
          //  + " And End_Nrec_Date is null";
          //  connection.SQLBS.DataSource = connection.SqlExec(Sql_Text, "ACC_price_Tbl");
            GrdAcc_price.DataSource = connection.SQLBS;     

        }

                            
            private void btn_add(object sender, EventArgs e)
        {

           //string Nrec_Date = MyGeneral_Lib.DateChecking(TxtStart_Date.Text);
            //if (Nrec_Date == "0" || Nrec_Date == "-1")
            //{
              //  MessageBox.Show(connection.Lang_id == 1 ? "تأكد من اسم الزبون" : "Please make sure of the Name custm.", MyGeneral_Lib.LblCap);
            //    TxtStart_Date.Focus();
                //return;
            //}

            int Rec_No = (from DataRow row in connection.SQLDS.Tables["acc_price"].Rows
                          where (Int16)row["user_id"] == Convert.ToInt16(Cbo_Cur_ID.SelectedValue)
                          select row).Count();
            if (Rec_No > 0)
            {
                MessageBox.Show("لا يمكن اضافة عملة مكررة", MyGeneral_Lib.LblCap);
                return;
            }

          //  TxtStart_Date.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            //connection.SQLDS.Tables["ACC_price_Tbl"].Rows.Add(new object[] { Cbo_Cur_ID.SelectedValue, Txtacc_Price.Text, TxtStart_Date.Text, null, Cbo_Cur_ID.Text, "", Txt_UsrName.Text });

          //  TxtStart_Date.Enabled = false;

          //  TxtStart_Date.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
           // Txtacc_Price.ResetText();
        }

        private void btn_ok(object sender, EventArgs e)
        {
          // string Nrec_Date = MyGeneral_Lib.DateChecking(TxtStart_Date.Text);
            //if (Nrec_Date == "0" || Nrec_Date == "-1")
            //{
               //MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ بداية سعر الاغلاق" : "Please make sure of closing date.", MyGeneral_Lib.LblCap);
               //TxtStart_Date.Focus();
               //return;
           //}

            if (connection.SQLDS.Tables["ACC_Close_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show("لا توجد قيود للاضافة", MyGeneral_Lib.LblCap);
                return;
            }

            DataTable Dt = connection.SQLDS.Tables["acc_price"].DefaultView.ToTable(false, "user_id", "acc_Price").Select().CopyToDataTable();

            //TxtStart_Date.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            connection.SQLCMD.Parameters.AddWithValue("@x", Dt);
            connection.SQLCMD.Parameters["@y"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.AddWithValue("@z", connection.user_id);
            connection.SQLCMD.Parameters["@c"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@p", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@pr"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_acc_Price", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Pr"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Pr"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
           
        }

        private void extbtn(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void Cbo_Cur_ID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
