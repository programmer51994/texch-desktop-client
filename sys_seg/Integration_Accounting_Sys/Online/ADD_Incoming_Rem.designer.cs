﻿namespace Integration_Accounting_Sys
{
    partial class ADD_Incoming_Rem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Txt_Reciever = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_job = new System.Windows.Forms.TextBox();
            this.Txt_notes = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_S_doc_issue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtScity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_S_doc_type = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.Txt_s_job = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.txt_code_rem = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.Txt_Rem_No = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label59 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.cmb_cur = new System.Windows.Forms.ComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.Cbo_Oper = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.Txt_Accproc = new System.Windows.Forms.TextBox();
            this.Grd_procer = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_procer = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.Txtr_Suburb = new System.Windows.Forms.TextBox();
            this.Txtr_Street = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.Txtr_State = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.Txtr_Post_Code = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Txt_r_Doc_No = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Txt_Doc_r_Issue = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Txt_r_Doc_Type = new System.Windows.Forms.TextBox();
            this.txt_rbirth_place = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.Txt_mail = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txt_Mother_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txt_sbirth_place = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.Txts_State = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Txts_Street = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.Txts_Suburb = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.Txts_Post_Code = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.Txt_r_cur_name = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.Cbo_city = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Cbo_Rem_Pay_Type = new System.Windows.Forms.ComboBox();
            this.label63 = new System.Windows.Forms.Label();
            this.Txtr_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Doc_S_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Doc_S_Exp = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Doc_r_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Doc_r_Exp = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Tot_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtDiscount_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_locamount_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_minrate_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_maxrate_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_ExRate_Rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Rem_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_r_birthdate = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_Birth = new Integration_Accounting_Sys.MyDateTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_procer)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-8, 52);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(953, 1);
            this.flowLayoutPanel9.TabIndex = 1001;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-71, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(753, 6);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(71, 14);
            this.label4.TabIndex = 998;
            this.label4.Text = "التاريــــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(827, 2);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(112, 23);
            this.TxtIn_Rec_Date.TabIndex = 999;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(9, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(202, 23);
            this.TxtTerm_Name.TabIndex = 996;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(120, 233);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel3.TabIndex = 1071;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(1, 223);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(120, 14);
            this.label15.TabIndex = 1070;
            this.label15.Text = "معلومات المستلم...";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Maroon;
            this.label81.Location = new System.Drawing.Point(847, 263);
            this.label81.Name = "label81";
            this.label81.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label81.Size = new System.Drawing.Size(87, 14);
            this.label81.TabIndex = 1069;
            this.label81.Text = "yyyy/mm/dd";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Purpose.Location = new System.Drawing.Point(95, 515);
            this.Txt_Purpose.Multiline = true;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(541, 20);
            this.Txt_Purpose.TabIndex = 1066;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(-4, 517);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 1067;
            this.label17.Text = "غرض التحويل:";
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_City.Location = new System.Drawing.Point(88, 260);
            this.Txt_R_City.Multiline = true;
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(247, 20);
            this.Txt_R_City.TabIndex = 1058;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(2, 262);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(71, 16);
            this.label27.TabIndex = 1059;
            this.label27.Text = "مدينة المستلم:";
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Nat.Location = new System.Drawing.Point(727, 238);
            this.Txt_R_Nat.Multiline = true;
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(217, 20);
            this.Txt_R_Nat.TabIndex = 1056;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(644, 240);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(77, 16);
            this.label28.TabIndex = 1057;
            this.label28.Text = "جنسية المستلم:";
            // 
            // Txt_Reciever
            // 
            this.Txt_Reciever.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Reciever.Location = new System.Drawing.Point(88, 238);
            this.Txt_Reciever.Multiline = true;
            this.Txt_Reciever.Name = "Txt_Reciever";
            this.Txt_Reciever.ReadOnly = true;
            this.Txt_Reciever.Size = new System.Drawing.Size(552, 20);
            this.Txt_Reciever.TabIndex = 1052;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(3, 240);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(64, 16);
            this.label30.TabIndex = 1053;
            this.label30.Text = "اسم المستلم:";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Phone.Location = new System.Drawing.Point(414, 260);
            this.Txt_R_Phone.Multiline = true;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(226, 20);
            this.Txt_R_Phone.TabIndex = 1051;
            // 
            // Txt_R_job
            // 
            this.Txt_R_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_job.Location = new System.Drawing.Point(414, 306);
            this.Txt_R_job.Multiline = true;
            this.Txt_R_job.Name = "Txt_R_job";
            this.Txt_R_job.ReadOnly = true;
            this.Txt_R_job.Size = new System.Drawing.Size(228, 20);
            this.Txt_R_job.TabIndex = 1042;
            // 
            // Txt_notes
            // 
            this.Txt_notes.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_notes.Location = new System.Drawing.Point(92, 569);
            this.Txt_notes.Multiline = true;
            this.Txt_notes.Name = "Txt_notes";
            this.Txt_notes.ReadOnly = true;
            this.Txt_notes.Size = new System.Drawing.Size(541, 20);
            this.Txt_notes.TabIndex = 1041;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(337, 308);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(68, 16);
            this.label32.TabIndex = 1049;
            this.label32.Text = "مهنة المستلم:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(1, 571);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 1048;
            this.label33.Text = "التفاصيـــــــل:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(645, 262);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(71, 16);
            this.label35.TabIndex = 1047;
            this.label35.Text = "التولــــــــــــد:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(334, 262);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 16);
            this.label38.TabIndex = 1044;
            this.label38.Text = "هاتف المستلم:";
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(117, 86);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel17.TabIndex = 1039;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(1, 76);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(116, 14);
            this.label14.TabIndex = 1038;
            this.label14.Text = "معلومات المرسل...";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Maroon;
            this.label80.Location = new System.Drawing.Point(850, 118);
            this.label80.Name = "label80";
            this.label80.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label80.Size = new System.Drawing.Size(87, 14);
            this.label80.TabIndex = 1037;
            this.label80.Text = "yyyy/mm/dd";
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relionship.Location = new System.Drawing.Point(457, 204);
            this.Txt_Relionship.Multiline = true;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(185, 20);
            this.Txt_Relionship.TabIndex = 1034;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(332, 206);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(116, 16);
            this.label3.TabIndex = 1035;
            this.label3.Text = "علاقة المستلم بالمرسل:";
            // 
            // Txt_S_doc_issue
            // 
            this.Txt_S_doc_issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_issue.Location = new System.Drawing.Point(88, 204);
            this.Txt_S_doc_issue.Multiline = true;
            this.Txt_S_doc_issue.Name = "Txt_S_doc_issue";
            this.Txt_S_doc_issue.ReadOnly = true;
            this.Txt_S_doc_issue.Size = new System.Drawing.Size(247, 20);
            this.Txt_S_doc_issue.TabIndex = 1030;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(3, 206);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 1031;
            this.label10.Text = "محل اصدارها:";
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_no.Location = new System.Drawing.Point(414, 182);
            this.Txt_S_doc_no.Multiline = true;
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.ReadOnly = true;
            this.Txt_S_doc_no.Size = new System.Drawing.Size(226, 20);
            this.Txt_S_doc_no.TabIndex = 1028;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(336, 184);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 1029;
            this.label9.Text = "رقم الوثيقـــــة:";
            // 
            // TxtScity
            // 
            this.TxtScity.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtScity.Location = new System.Drawing.Point(88, 115);
            this.TxtScity.Multiline = true;
            this.TxtScity.Name = "TxtScity";
            this.TxtScity.ReadOnly = true;
            this.TxtScity.Size = new System.Drawing.Size(247, 20);
            this.TxtScity.TabIndex = 1026;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(3, 117);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 1027;
            this.label8.Text = "مدينة المرسل:";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txts_nat.Location = new System.Drawing.Point(727, 93);
            this.Txts_nat.Multiline = true;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(217, 20);
            this.Txts_nat.TabIndex = 1024;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(641, 95);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 1025;
            this.label7.Text = "جنسية المرسل:";
            // 
            // Txt_S_doc_type
            // 
            this.Txt_S_doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_type.Location = new System.Drawing.Point(88, 182);
            this.Txt_S_doc_type.Multiline = true;
            this.Txt_S_doc_type.Name = "Txt_S_doc_type";
            this.Txt_S_doc_type.ReadOnly = true;
            this.Txt_S_doc_type.Size = new System.Drawing.Size(247, 20);
            this.Txt_S_doc_type.TabIndex = 1023;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(5, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 1022;
            this.label6.Text = "نوع الوثيقة:";
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_name.Location = new System.Drawing.Point(88, 93);
            this.TxtS_name.Multiline = true;
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(553, 20);
            this.TxtS_name.TabIndex = 1020;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(-3, 95);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 1021;
            this.label5.Text = "اسم المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_phone.Location = new System.Drawing.Point(414, 115);
            this.TxtS_phone.Multiline = true;
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(226, 20);
            this.TxtS_phone.TabIndex = 1019;
            // 
            // Txt_s_job
            // 
            this.Txt_s_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_s_job.Location = new System.Drawing.Point(414, 159);
            this.Txt_s_job.Multiline = true;
            this.Txt_s_job.Name = "Txt_s_job";
            this.Txt_s_job.ReadOnly = true;
            this.Txt_s_job.Size = new System.Drawing.Size(225, 20);
            this.Txt_s_job.TabIndex = 1010;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(93, 541);
            this.Txt_Soruce_money.Multiline = true;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(541, 20);
            this.Txt_Soruce_money.TabIndex = 1009;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(335, 161);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(69, 16);
            this.label22.TabIndex = 1017;
            this.label22.Text = "مهنة المرسل:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(-5, 543);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(71, 16);
            this.label21.TabIndex = 1016;
            this.label21.Text = "مصدر المــال:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(644, 117);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(65, 16);
            this.label20.TabIndex = 1015;
            this.label20.Text = "التولــــــــــد:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(330, 117);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 16);
            this.label19.TabIndex = 1012;
            this.label19.Text = "هاتف المرسل:";
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.SearchBtn.ForeColor = System.Drawing.Color.Navy;
            this.SearchBtn.Location = new System.Drawing.Point(861, 55);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(77, 25);
            this.SearchBtn.TabIndex = 4;
            this.SearchBtn.Text = "بحـــــث";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(472, 597);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 25);
            this.button1.TabIndex = 8;
            this.button1.Text = "انهـــاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-4, 595);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(953, 1);
            this.flowLayoutPanel2.TabIndex = 1078;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-71, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(392, 597);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(81, 25);
            this.Btn_Add.TabIndex = 6;
            this.Btn_Add.Text = "مـــوافق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // txt_code_rem
            // 
            this.txt_code_rem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_code_rem.Location = new System.Drawing.Point(432, 56);
            this.txt_code_rem.MaxLength = 49;
            this.txt_code_rem.Name = "txt_code_rem";
            this.txt_code_rem.Size = new System.Drawing.Size(103, 22);
            this.txt_code_rem.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(343, 60);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(81, 14);
            this.label11.TabIndex = 1080;
            this.label11.Text = "رقم االسري:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(1, 60);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(80, 14);
            this.label39.TabIndex = 1072;
            this.label39.Text = "رقم الحوالـة:";
            // 
            // Txt_Rem_No
            // 
            this.Txt_Rem_No.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_No.Location = new System.Drawing.Point(80, 56);
            this.Txt_Rem_No.MaxLength = 14;
            this.Txt_Rem_No.Name = "Txt_Rem_No";
            this.Txt_Rem_No.Size = new System.Drawing.Size(257, 22);
            this.Txt_Rem_No.TabIndex = 3;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkBox1.ForeColor = System.Drawing.Color.Maroon;
            this.checkBox1.Location = new System.Drawing.Point(3, 416);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox1.Size = new System.Drawing.Size(208, 18);
            this.checkBox1.TabIndex = 1081;
            this.checkBox1.Text = "تحويل الى العملة المحلية.........";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(57, 428);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel6.Size = new System.Drawing.Size(893, 1);
            this.flowLayoutPanel6.TabIndex = 1098;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-131, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Navy;
            this.label59.Location = new System.Drawing.Point(734, 438);
            this.label59.Name = "label59";
            this.label59.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label59.Size = new System.Drawing.Size(78, 14);
            this.label59.TabIndex = 1097;
            this.label59.Text = "الخصـــــــــــم";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(583, 438);
            this.label50.Name = "label50";
            this.label50.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label50.Size = new System.Drawing.Size(133, 14);
            this.label50.TabIndex = 1096;
            this.label50.Text = "المبلغ  بلعملة المحلية";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(488, 438);
            this.label49.Name = "label49";
            this.label49.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label49.Size = new System.Drawing.Size(71, 14);
            this.label49.TabIndex = 1095;
            this.label49.Text = "الحد الادنى";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(385, 438);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 1094;
            this.label1.Text = "الحد الاعلى";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(273, 438);
            this.label57.Name = "label57";
            this.label57.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label57.Size = new System.Drawing.Size(80, 14);
            this.label57.TabIndex = 1093;
            this.label57.Text = "المعــــــــــادل";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Navy;
            this.label60.Location = new System.Drawing.Point(37, 438);
            this.label60.Name = "label60";
            this.label60.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label60.Size = new System.Drawing.Size(76, 14);
            this.label60.TabIndex = 1092;
            this.label60.Text = "مبلغ الحوالة";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Navy;
            this.label55.Location = new System.Drawing.Point(840, 438);
            this.label55.Name = "label55";
            this.label55.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label55.Size = new System.Drawing.Size(88, 14);
            this.label55.TabIndex = 1091;
            this.label55.Text = "المجمـــــــــــوع";
            // 
            // cmb_cur
            // 
            this.cmb_cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_cur.FormattingEnabled = true;
            this.cmb_cur.Location = new System.Drawing.Point(114, 460);
            this.cmb_cur.Name = "cmb_cur";
            this.cmb_cur.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmb_cur.Size = new System.Drawing.Size(151, 24);
            this.cmb_cur.TabIndex = 1083;
            this.cmb_cur.SelectedIndexChanged += new System.EventHandler(this.cmb_cur_SelectedIndexChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(184, 437);
            this.label58.Name = "label58";
            this.label58.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label58.Size = new System.Drawing.Size(40, 16);
            this.label58.TabIndex = 1090;
            this.label58.Text = "العملــة";
            // 
            // Cbo_Oper
            // 
            this.Cbo_Oper.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Oper.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Oper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Oper.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Oper.FormattingEnabled = true;
            this.Cbo_Oper.Items.AddRange(new object[] {
            "نقـــــــــــــدي",
            "على استحقاق(قيد يومية)",
            "على استحقاق (الوكيل)"});
            this.Cbo_Oper.Location = new System.Drawing.Point(114, 488);
            this.Cbo_Oper.Name = "Cbo_Oper";
            this.Cbo_Oper.Size = new System.Drawing.Size(151, 24);
            this.Cbo_Oper.TabIndex = 1099;
            this.Cbo_Oper.SelectedIndexChanged += new System.EventHandler(this.Cbo_Oper_SelectedIndexChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Navy;
            this.label61.Location = new System.Drawing.Point(1, 491);
            this.label61.Name = "label61";
            this.label61.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label61.Size = new System.Drawing.Size(90, 14);
            this.label61.TabIndex = 1100;
            this.label61.Text = "نــوع العمليـــة:";
            // 
            // Txt_Accproc
            // 
            this.Txt_Accproc.BackColor = System.Drawing.Color.White;
            this.Txt_Accproc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Accproc.Location = new System.Drawing.Point(702, 569);
            this.Txt_Accproc.Name = "Txt_Accproc";
            this.Txt_Accproc.ReadOnly = true;
            this.Txt_Accproc.Size = new System.Drawing.Size(239, 23);
            this.Txt_Accproc.TabIndex = 1103;
            this.Txt_Accproc.Visible = false;
            // 
            // Grd_procer
            // 
            this.Grd_procer.AllowUserToAddRows = false;
            this.Grd_procer.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_procer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_procer.BackgroundColor = System.Drawing.Color.White;
            this.Grd_procer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_procer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_procer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_procer.ColumnHeadersHeight = 45;
            this.Grd_procer.ColumnHeadersVisible = false;
            this.Grd_procer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_procer.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_procer.Location = new System.Drawing.Point(640, 513);
            this.Grd_procer.Name = "Grd_procer";
            this.Grd_procer.ReadOnly = true;
            this.Grd_procer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_procer.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_procer.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_procer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_procer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_procer.Size = new System.Drawing.Size(301, 55);
            this.Grd_procer.TabIndex = 1105;
            this.Grd_procer.Visible = false;
            this.Grd_procer.SelectionChanged += new System.EventHandler(this.Grd_procer_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "رمز الوسيط";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ASub_CustName";
            this.dataGridViewTextBoxColumn3.HeaderText = "اسم الوسيط";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // Txt_procer
            // 
            this.Txt_procer.BackColor = System.Drawing.Color.White;
            this.Txt_procer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_procer.Location = new System.Drawing.Point(702, 490);
            this.Txt_procer.Name = "Txt_procer";
            this.Txt_procer.Size = new System.Drawing.Size(239, 22);
            this.Txt_procer.TabIndex = 1104;
            this.Txt_procer.Visible = false;
            this.Txt_procer.TextChanged += new System.EventHandler(this.Txt_procer_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(637, 575);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(60, 14);
            this.label16.TabIndex = 1102;
            this.label16.Text = "الحسـاب:";
            this.label16.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(637, 494);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 14);
            this.label13.TabIndex = 1101;
            this.label13.Text = "الوسيـط  :";
            this.label13.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(1, 285);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(77, 16);
            this.label43.TabIndex = 1106;
            this.label43.Text = "الحــــــــــــــي:";
            // 
            // Txtr_Suburb
            // 
            this.Txtr_Suburb.BackColor = System.Drawing.Color.White;
            this.Txtr_Suburb.Location = new System.Drawing.Point(88, 283);
            this.Txtr_Suburb.MaxLength = 99;
            this.Txtr_Suburb.Name = "Txtr_Suburb";
            this.Txtr_Suburb.ReadOnly = true;
            this.Txtr_Suburb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txtr_Suburb.Size = new System.Drawing.Size(247, 20);
            this.Txtr_Suburb.TabIndex = 1107;
            // 
            // Txtr_Street
            // 
            this.Txtr_Street.BackColor = System.Drawing.Color.White;
            this.Txtr_Street.Location = new System.Drawing.Point(414, 283);
            this.Txtr_Street.MaxLength = 99;
            this.Txtr_Street.Name = "Txtr_Street";
            this.Txtr_Street.ReadOnly = true;
            this.Txtr_Street.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txtr_Street.Size = new System.Drawing.Size(227, 20);
            this.Txtr_Street.TabIndex = 1109;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(336, 285);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(78, 16);
            this.label42.TabIndex = 1108;
            this.label42.Text = "الزقــــــــــــاق:";
            // 
            // Txtr_State
            // 
            this.Txtr_State.BackColor = System.Drawing.Color.White;
            this.Txtr_State.Location = new System.Drawing.Point(727, 283);
            this.Txtr_State.MaxLength = 99;
            this.Txtr_State.Name = "Txtr_State";
            this.Txtr_State.ReadOnly = true;
            this.Txtr_State.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txtr_State.Size = new System.Drawing.Size(217, 20);
            this.Txtr_State.TabIndex = 1110;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(645, 285);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(72, 16);
            this.label45.TabIndex = 1113;
            this.label45.Text = "المحافـظـــــة:";
            // 
            // Txtr_Post_Code
            // 
            this.Txtr_Post_Code.BackColor = System.Drawing.Color.White;
            this.Txtr_Post_Code.Location = new System.Drawing.Point(88, 306);
            this.Txtr_Post_Code.MaxLength = 99;
            this.Txtr_Post_Code.Name = "Txtr_Post_Code";
            this.Txtr_Post_Code.ReadOnly = true;
            this.Txtr_Post_Code.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txtr_Post_Code.Size = new System.Drawing.Size(247, 20);
            this.Txtr_Post_Code.TabIndex = 1111;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(0, 308);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(79, 16);
            this.label44.TabIndex = 1112;
            this.label44.Text = "الرمز البريــدي:";
            // 
            // Txt_r_Doc_No
            // 
            this.Txt_r_Doc_No.BackColor = System.Drawing.Color.White;
            this.Txt_r_Doc_No.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_Doc_No.Location = new System.Drawing.Point(414, 330);
            this.Txt_r_Doc_No.MaxLength = 49;
            this.Txt_r_Doc_No.Name = "Txt_r_Doc_No";
            this.Txt_r_Doc_No.ReadOnly = true;
            this.Txt_r_Doc_No.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_Doc_No.Size = new System.Drawing.Size(226, 23);
            this.Txt_r_Doc_No.TabIndex = 1114;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(336, 333);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 16);
            this.label26.TabIndex = 1115;
            this.label26.Text = "رقم الوثيقــــة:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(795, 333);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(55, 16);
            this.label25.TabIndex = 1118;
            this.label25.Text = "إنتهائهـا :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(640, 333);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(58, 16);
            this.label23.TabIndex = 1122;
            this.label23.Text = "تاريخهـــا:";
            // 
            // Txt_Doc_r_Issue
            // 
            this.Txt_Doc_r_Issue.BackColor = System.Drawing.Color.White;
            this.Txt_Doc_r_Issue.Location = new System.Drawing.Point(90, 356);
            this.Txt_Doc_r_Issue.MaxLength = 49;
            this.Txt_Doc_r_Issue.Name = "Txt_Doc_r_Issue";
            this.Txt_Doc_r_Issue.ReadOnly = true;
            this.Txt_Doc_r_Issue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Doc_r_Issue.Size = new System.Drawing.Size(247, 20);
            this.Txt_Doc_r_Issue.TabIndex = 1124;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(-1, 358);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(80, 16);
            this.label29.TabIndex = 1125;
            this.label29.Text = "م:اصدارهــــــا :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(-2, 333);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 16);
            this.label31.TabIndex = 1126;
            this.label31.Text = "نوع الوثيقـــــــة:";
            // 
            // Txt_r_Doc_Type
            // 
            this.Txt_r_Doc_Type.BackColor = System.Drawing.Color.White;
            this.Txt_r_Doc_Type.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_Doc_Type.Location = new System.Drawing.Point(88, 330);
            this.Txt_r_Doc_Type.MaxLength = 49;
            this.Txt_r_Doc_Type.Name = "Txt_r_Doc_Type";
            this.Txt_r_Doc_Type.ReadOnly = true;
            this.Txt_r_Doc_Type.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_Doc_Type.Size = new System.Drawing.Size(247, 23);
            this.Txt_r_Doc_Type.TabIndex = 1127;
            // 
            // txt_rbirth_place
            // 
            this.txt_rbirth_place.BackColor = System.Drawing.Color.White;
            this.txt_rbirth_place.Location = new System.Drawing.Point(727, 306);
            this.txt_rbirth_place.MaxLength = 49;
            this.txt_rbirth_place.Name = "txt_rbirth_place";
            this.txt_rbirth_place.ReadOnly = true;
            this.txt_rbirth_place.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_rbirth_place.Size = new System.Drawing.Size(217, 20);
            this.txt_rbirth_place.TabIndex = 1128;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(645, 308);
            this.label48.Name = "label48";
            this.label48.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label48.Size = new System.Drawing.Size(79, 16);
            this.label48.TabIndex = 1129;
            this.label48.Text = "مكــان الولادة :";
            // 
            // Txt_mail
            // 
            this.Txt_mail.BackColor = System.Drawing.Color.White;
            this.Txt_mail.Location = new System.Drawing.Point(429, 356);
            this.Txt_mail.MaxLength = 99;
            this.Txt_mail.Name = "Txt_mail";
            this.Txt_mail.ReadOnly = true;
            this.Txt_mail.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_mail.Size = new System.Drawing.Size(249, 20);
            this.Txt_mail.TabIndex = 1130;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(336, 358);
            this.label41.Name = "label41";
            this.label41.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label41.Size = new System.Drawing.Size(90, 16);
            this.label41.TabIndex = 1131;
            this.label41.Text = "البريد الالكتروني:";
            // 
            // txt_Mother_name
            // 
            this.txt_Mother_name.BackColor = System.Drawing.Color.White;
            this.txt_Mother_name.Location = new System.Drawing.Point(757, 356);
            this.txt_Mother_name.MaxLength = 49;
            this.txt_Mother_name.Name = "txt_Mother_name";
            this.txt_Mother_name.ReadOnly = true;
            this.txt_Mother_name.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_Mother_name.Size = new System.Drawing.Size(185, 20);
            this.txt_Mother_name.TabIndex = 1132;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(678, 358);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 1133;
            this.label2.Text = "اســــــــــم الام:";
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(602, 2);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(152, 23);
            this.TxtBox_User.TabIndex = 1134;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Navy;
            this.label53.Location = new System.Drawing.Point(527, 6);
            this.label53.Name = "label53";
            this.label53.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label53.Size = new System.Drawing.Size(74, 14);
            this.label53.TabIndex = 1135;
            this.label53.Text = "الصــــندوق:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(638, 184);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(55, 16);
            this.label24.TabIndex = 1139;
            this.label24.Text = "تاريخهــا:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(793, 184);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(55, 16);
            this.label36.TabIndex = 1136;
            this.label36.Text = "إنتهائهـا :";
            // 
            // txt_sbirth_place
            // 
            this.txt_sbirth_place.BackColor = System.Drawing.Color.White;
            this.txt_sbirth_place.Location = new System.Drawing.Point(727, 159);
            this.txt_sbirth_place.MaxLength = 49;
            this.txt_sbirth_place.Name = "txt_sbirth_place";
            this.txt_sbirth_place.ReadOnly = true;
            this.txt_sbirth_place.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_sbirth_place.Size = new System.Drawing.Size(216, 20);
            this.txt_sbirth_place.TabIndex = 1140;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(639, 161);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(79, 16);
            this.label37.TabIndex = 1141;
            this.label37.Text = "مكــان الولادة :";
            // 
            // Txts_State
            // 
            this.Txts_State.BackColor = System.Drawing.Color.White;
            this.Txts_State.Location = new System.Drawing.Point(727, 137);
            this.Txts_State.MaxLength = 99;
            this.Txts_State.Name = "Txts_State";
            this.Txts_State.ReadOnly = true;
            this.Txts_State.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txts_State.Size = new System.Drawing.Size(216, 20);
            this.Txts_State.TabIndex = 1146;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(649, 139);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label40.Size = new System.Drawing.Size(72, 16);
            this.label40.TabIndex = 1147;
            this.label40.Text = "المحافـظـــــة:";
            // 
            // Txts_Street
            // 
            this.Txts_Street.BackColor = System.Drawing.Color.White;
            this.Txts_Street.Location = new System.Drawing.Point(414, 137);
            this.Txts_Street.MaxLength = 99;
            this.Txts_Street.Name = "Txts_Street";
            this.Txts_Street.ReadOnly = true;
            this.Txts_Street.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txts_Street.Size = new System.Drawing.Size(226, 20);
            this.Txts_Street.TabIndex = 1145;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(335, 139);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(78, 16);
            this.label47.TabIndex = 1144;
            this.label47.Text = "الزقــــــــــــاق:";
            // 
            // Txts_Suburb
            // 
            this.Txts_Suburb.BackColor = System.Drawing.Color.White;
            this.Txts_Suburb.Location = new System.Drawing.Point(88, 137);
            this.Txts_Suburb.MaxLength = 99;
            this.Txts_Suburb.Name = "Txts_Suburb";
            this.Txts_Suburb.ReadOnly = true;
            this.Txts_Suburb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txts_Suburb.Size = new System.Drawing.Size(247, 20);
            this.Txts_Suburb.TabIndex = 1143;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label51.ForeColor = System.Drawing.Color.Navy;
            this.label51.Location = new System.Drawing.Point(2, 139);
            this.label51.Name = "label51";
            this.label51.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label51.Size = new System.Drawing.Size(77, 16);
            this.label51.TabIndex = 1142;
            this.label51.Text = "الحــــــــــــــي:";
            // 
            // Txts_Post_Code
            // 
            this.Txts_Post_Code.BackColor = System.Drawing.Color.White;
            this.Txts_Post_Code.Location = new System.Drawing.Point(88, 159);
            this.Txts_Post_Code.MaxLength = 99;
            this.Txts_Post_Code.Name = "Txts_Post_Code";
            this.Txts_Post_Code.ReadOnly = true;
            this.Txts_Post_Code.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txts_Post_Code.Size = new System.Drawing.Size(247, 20);
            this.Txts_Post_Code.TabIndex = 1148;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(2, 161);
            this.label52.Name = "label52";
            this.label52.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label52.Size = new System.Drawing.Size(79, 16);
            this.label52.TabIndex = 1149;
            this.label52.Text = "الرمز البريــدي:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Navy;
            this.label56.Location = new System.Drawing.Point(3, 395);
            this.label56.Name = "label56";
            this.label56.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label56.Size = new System.Drawing.Size(80, 14);
            this.label56.TabIndex = 1151;
            this.label56.Text = "مبلغ الحوالة:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-1, 385);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel5.Size = new System.Drawing.Size(944, 1);
            this.flowLayoutPanel5.TabIndex = 1152;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-80, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel8.TabIndex = 630;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(339, 395);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(84, 14);
            this.label18.TabIndex = 1154;
            this.label18.Text = "عملة الحوالة:";
            // 
            // Txt_r_cur_name
            // 
            this.Txt_r_cur_name.BackColor = System.Drawing.Color.White;
            this.Txt_r_cur_name.Location = new System.Drawing.Point(429, 392);
            this.Txt_r_cur_name.MaxLength = 99;
            this.Txt_r_cur_name.Name = "Txt_r_cur_name";
            this.Txt_r_cur_name.ReadOnly = true;
            this.Txt_r_cur_name.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_cur_name.Size = new System.Drawing.Size(249, 20);
            this.Txt_r_cur_name.TabIndex = 1155;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(1, 375);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(162, 14);
            this.label34.TabIndex = 1156;
            this.label34.Text = "المعلومات المالية للحوالة...";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(3, 31);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(94, 14);
            this.label46.TabIndex = 1159;
            this.label46.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(99, 27);
            this.Txt_Loc_Cur.Multiline = true;
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(226, 22);
            this.Txt_Loc_Cur.TabIndex = 1158;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(329, 31);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(85, 14);
            this.label54.TabIndex = 1161;
            this.label54.Text = "المستخـــــدم:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(416, 27);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(226, 23);
            this.TxtUser.TabIndex = 1160;
            // 
            // Cbo_city
            // 
            this.Cbo_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_city.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_city.FormattingEnabled = true;
            this.Cbo_city.Location = new System.Drawing.Point(285, 1);
            this.Cbo_city.Name = "Cbo_city";
            this.Cbo_city.Size = new System.Drawing.Size(240, 24);
            this.Cbo_city.TabIndex = 1164;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(212, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 16);
            this.label12.TabIndex = 1165;
            this.label12.Text = "المدينــــــــــة:";
            // 
            // Cbo_Rem_Pay_Type
            // 
            this.Cbo_Rem_Pay_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Rem_Pay_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Rem_Pay_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rem_Pay_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rem_Pay_Type.FormattingEnabled = true;
            this.Cbo_Rem_Pay_Type.Items.AddRange(new object[] {
            "نقـــــــــــــدي",
            "على استحقاق"});
            this.Cbo_Rem_Pay_Type.Location = new System.Drawing.Point(366, 488);
            this.Cbo_Rem_Pay_Type.Name = "Cbo_Rem_Pay_Type";
            this.Cbo_Rem_Pay_Type.Size = new System.Drawing.Size(270, 24);
            this.Cbo_Rem_Pay_Type.TabIndex = 1166;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Navy;
            this.label63.Location = new System.Drawing.Point(273, 493);
            this.label63.Name = "label63";
            this.label63.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label63.Size = new System.Drawing.Size(84, 14);
            this.label63.TabIndex = 1167;
            this.label63.Text = "طريقة الدفع :";
            // 
            // Txtr_amount
            // 
            this.Txtr_amount.BackColor = System.Drawing.Color.White;
            this.Txtr_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_amount.Location = new System.Drawing.Point(90, 391);
            this.Txtr_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtr_amount.Name = "Txtr_amount";
            this.Txtr_amount.NumberDecimalDigits = 3;
            this.Txtr_amount.NumberDecimalSeparator = ".";
            this.Txtr_amount.NumberGroupSeparator = ",";
            this.Txtr_amount.ReadOnly = true;
            this.Txtr_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_amount.Size = new System.Drawing.Size(247, 23);
            this.Txtr_amount.TabIndex = 1150;
            this.Txtr_amount.Text = "0.000";
            // 
            // Txt_Doc_S_Date
            // 
            this.Txt_Doc_S_Date.BackColor = System.Drawing.Color.White;
            this.Txt_Doc_S_Date.DateSeperator = '/';
            this.Txt_Doc_S_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_S_Date.Location = new System.Drawing.Point(698, 181);
            this.Txt_Doc_S_Date.Mask = "0000/00/00";
            this.Txt_Doc_S_Date.Name = "Txt_Doc_S_Date";
            this.Txt_Doc_S_Date.PromptChar = ' ';
            this.Txt_Doc_S_Date.ReadOnly = true;
            this.Txt_Doc_S_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Doc_S_Date.Size = new System.Drawing.Size(93, 22);
            this.Txt_Doc_S_Date.TabIndex = 1138;
            this.Txt_Doc_S_Date.Text = "00000000";
            this.Txt_Doc_S_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Doc_S_Exp
            // 
            this.Txt_Doc_S_Exp.BackColor = System.Drawing.Color.White;
            this.Txt_Doc_S_Exp.DateSeperator = '/';
            this.Txt_Doc_S_Exp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_S_Exp.Location = new System.Drawing.Point(858, 181);
            this.Txt_Doc_S_Exp.Mask = "0000/00/00";
            this.Txt_Doc_S_Exp.Name = "Txt_Doc_S_Exp";
            this.Txt_Doc_S_Exp.PromptChar = ' ';
            this.Txt_Doc_S_Exp.ReadOnly = true;
            this.Txt_Doc_S_Exp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Doc_S_Exp.Size = new System.Drawing.Size(85, 22);
            this.Txt_Doc_S_Exp.TabIndex = 1137;
            this.Txt_Doc_S_Exp.Text = "00000000";
            this.Txt_Doc_S_Exp.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Doc_r_Date
            // 
            this.Txt_Doc_r_Date.BackColor = System.Drawing.Color.White;
            this.Txt_Doc_r_Date.DateSeperator = '/';
            this.Txt_Doc_r_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_r_Date.Location = new System.Drawing.Point(703, 330);
            this.Txt_Doc_r_Date.Mask = "0000/00/00";
            this.Txt_Doc_r_Date.Name = "Txt_Doc_r_Date";
            this.Txt_Doc_r_Date.PromptChar = ' ';
            this.Txt_Doc_r_Date.ReadOnly = true;
            this.Txt_Doc_r_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Doc_r_Date.Size = new System.Drawing.Size(93, 22);
            this.Txt_Doc_r_Date.TabIndex = 1121;
            this.Txt_Doc_r_Date.Text = "00000000";
            this.Txt_Doc_r_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Doc_r_Exp
            // 
            this.Txt_Doc_r_Exp.BackColor = System.Drawing.Color.White;
            this.Txt_Doc_r_Exp.DateSeperator = '/';
            this.Txt_Doc_r_Exp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_r_Exp.Location = new System.Drawing.Point(852, 330);
            this.Txt_Doc_r_Exp.Mask = "0000/00/00";
            this.Txt_Doc_r_Exp.Name = "Txt_Doc_r_Exp";
            this.Txt_Doc_r_Exp.PromptChar = ' ';
            this.Txt_Doc_r_Exp.ReadOnly = true;
            this.Txt_Doc_r_Exp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Doc_r_Exp.Size = new System.Drawing.Size(85, 22);
            this.Txt_Doc_r_Exp.TabIndex = 1119;
            this.Txt_Doc_r_Exp.Text = "00000000";
            this.Txt_Doc_r_Exp.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Tot_amount
            // 
            this.Txt_Tot_amount.BackColor = System.Drawing.Color.White;
            this.Txt_Tot_amount.Enabled = false;
            this.Txt_Tot_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Tot_amount.Location = new System.Drawing.Point(827, 460);
            this.Txt_Tot_amount.Name = "Txt_Tot_amount";
            this.Txt_Tot_amount.NumberDecimalDigits = 3;
            this.Txt_Tot_amount.NumberDecimalSeparator = ".";
            this.Txt_Tot_amount.NumberGroupSeparator = ",";
            this.Txt_Tot_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Tot_amount.Size = new System.Drawing.Size(114, 23);
            this.Txt_Tot_amount.TabIndex = 1089;
            this.Txt_Tot_amount.Text = "0.000";
            // 
            // TxtDiscount_Amount
            // 
            this.TxtDiscount_Amount.BackColor = System.Drawing.Color.White;
            this.TxtDiscount_Amount.Enabled = false;
            this.TxtDiscount_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscount_Amount.Location = new System.Drawing.Point(712, 461);
            this.TxtDiscount_Amount.Name = "TxtDiscount_Amount";
            this.TxtDiscount_Amount.NumberDecimalDigits = 3;
            this.TxtDiscount_Amount.NumberDecimalSeparator = ".";
            this.TxtDiscount_Amount.NumberGroupSeparator = ",";
            this.TxtDiscount_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDiscount_Amount.Size = new System.Drawing.Size(114, 23);
            this.TxtDiscount_Amount.TabIndex = 1088;
            this.TxtDiscount_Amount.Text = "0.000";
            // 
            // txt_locamount_rem
            // 
            this.txt_locamount_rem.BackColor = System.Drawing.Color.White;
            this.txt_locamount_rem.Enabled = false;
            this.txt_locamount_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_locamount_rem.Location = new System.Drawing.Point(590, 461);
            this.txt_locamount_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_locamount_rem.Name = "txt_locamount_rem";
            this.txt_locamount_rem.NumberDecimalDigits = 3;
            this.txt_locamount_rem.NumberDecimalSeparator = ".";
            this.txt_locamount_rem.NumberGroupSeparator = ",";
            this.txt_locamount_rem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_locamount_rem.Size = new System.Drawing.Size(122, 23);
            this.txt_locamount_rem.TabIndex = 1087;
            this.txt_locamount_rem.Text = "0.000";
            // 
            // txt_minrate_rem
            // 
            this.txt_minrate_rem.BackColor = System.Drawing.Color.White;
            this.txt_minrate_rem.Enabled = false;
            this.txt_minrate_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_minrate_rem.Location = new System.Drawing.Point(483, 461);
            this.txt_minrate_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_minrate_rem.Name = "txt_minrate_rem";
            this.txt_minrate_rem.NumberDecimalDigits = 7;
            this.txt_minrate_rem.NumberDecimalSeparator = ".";
            this.txt_minrate_rem.NumberGroupSeparator = ",";
            this.txt_minrate_rem.ReadOnly = true;
            this.txt_minrate_rem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_minrate_rem.Size = new System.Drawing.Size(106, 23);
            this.txt_minrate_rem.TabIndex = 1086;
            this.txt_minrate_rem.Text = "0.0000000";
            // 
            // txt_maxrate_rem
            // 
            this.txt_maxrate_rem.BackColor = System.Drawing.Color.White;
            this.txt_maxrate_rem.Enabled = false;
            this.txt_maxrate_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maxrate_rem.Location = new System.Drawing.Point(376, 461);
            this.txt_maxrate_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_maxrate_rem.Name = "txt_maxrate_rem";
            this.txt_maxrate_rem.NumberDecimalDigits = 7;
            this.txt_maxrate_rem.NumberDecimalSeparator = ".";
            this.txt_maxrate_rem.NumberGroupSeparator = ",";
            this.txt_maxrate_rem.ReadOnly = true;
            this.txt_maxrate_rem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_maxrate_rem.Size = new System.Drawing.Size(106, 23);
            this.txt_maxrate_rem.TabIndex = 1085;
            this.txt_maxrate_rem.Text = "0.0000000";
            // 
            // Txt_ExRate_Rem
            // 
            this.Txt_ExRate_Rem.BackColor = System.Drawing.Color.White;
            this.Txt_ExRate_Rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ExRate_Rem.Location = new System.Drawing.Point(267, 461);
            this.Txt_ExRate_Rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_ExRate_Rem.Name = "Txt_ExRate_Rem";
            this.Txt_ExRate_Rem.NumberDecimalDigits = 7;
            this.Txt_ExRate_Rem.NumberDecimalSeparator = ".";
            this.Txt_ExRate_Rem.NumberGroupSeparator = ",";
            this.Txt_ExRate_Rem.ReadOnly = true;
            this.Txt_ExRate_Rem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_ExRate_Rem.Size = new System.Drawing.Size(106, 23);
            this.Txt_ExRate_Rem.TabIndex = 1084;
            this.Txt_ExRate_Rem.Text = "0.0000000";
            this.Txt_ExRate_Rem.TextChanged += new System.EventHandler(this.Txt_ExRate_Rem_TextChanged);
            // 
            // Txt_Rem_Amount
            // 
            this.Txt_Rem_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Rem_Amount.Enabled = false;
            this.Txt_Rem_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_Amount.Location = new System.Drawing.Point(5, 461);
            this.Txt_Rem_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Rem_Amount.Name = "Txt_Rem_Amount";
            this.Txt_Rem_Amount.NumberDecimalDigits = 3;
            this.Txt_Rem_Amount.NumberDecimalSeparator = ".";
            this.Txt_Rem_Amount.NumberGroupSeparator = ",";
            this.Txt_Rem_Amount.ReadOnly = true;
            this.Txt_Rem_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Rem_Amount.Size = new System.Drawing.Size(106, 23);
            this.Txt_Rem_Amount.TabIndex = 1082;
            this.Txt_Rem_Amount.Text = "0.000";
            // 
            // Txt_r_birthdate
            // 
            this.Txt_r_birthdate.BackColor = System.Drawing.Color.White;
            this.Txt_r_birthdate.DateSeperator = '/';
            this.Txt_r_birthdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_birthdate.Location = new System.Drawing.Point(727, 259);
            this.Txt_r_birthdate.Mask = "0000/00/00";
            this.Txt_r_birthdate.Name = "Txt_r_birthdate";
            this.Txt_r_birthdate.PromptChar = ' ';
            this.Txt_r_birthdate.ReadOnly = true;
            this.Txt_r_birthdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_birthdate.Size = new System.Drawing.Size(122, 22);
            this.Txt_r_birthdate.TabIndex = 1068;
            this.Txt_r_birthdate.Text = "00000000";
            this.Txt_r_birthdate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_S_Birth
            // 
            this.Txt_S_Birth.BackColor = System.Drawing.Color.White;
            this.Txt_S_Birth.DateSeperator = '/';
            this.Txt_S_Birth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Birth.Location = new System.Drawing.Point(727, 114);
            this.Txt_S_Birth.Mask = "0000/00/00";
            this.Txt_S_Birth.Name = "Txt_S_Birth";
            this.Txt_S_Birth.PromptChar = ' ';
            this.Txt_S_Birth.ReadOnly = true;
            this.Txt_S_Birth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_Birth.Size = new System.Drawing.Size(123, 22);
            this.Txt_S_Birth.TabIndex = 1036;
            this.Txt_S_Birth.Text = "00000000";
            this.Txt_S_Birth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // ADD_Incoming_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 627);
            this.Controls.Add(this.Cbo_Rem_Pay_Type);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.Cbo_city);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.Txt_r_cur_name);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.Txtr_amount);
            this.Controls.Add(this.Txts_Post_Code);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.Txts_State);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.Txts_Street);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.Txts_Suburb);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.txt_sbirth_place);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Txt_Doc_S_Date);
            this.Controls.Add(this.Txt_Doc_S_Exp);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.txt_Mother_name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_mail);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.txt_rbirth_place);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.Txt_r_Doc_Type);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.Txt_Doc_r_Issue);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.Txt_Doc_r_Date);
            this.Controls.Add(this.Txt_Doc_r_Exp);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.Txt_r_Doc_No);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.Txtr_State);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.Txtr_Post_Code);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.Txtr_Street);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.Txtr_Suburb);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.Txt_Accproc);
            this.Controls.Add(this.Grd_procer);
            this.Controls.Add(this.Txt_procer);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Cbo_Oper);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.cmb_cur);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.Txt_Tot_amount);
            this.Controls.Add(this.TxtDiscount_Amount);
            this.Controls.Add(this.txt_locamount_rem);
            this.Controls.Add(this.txt_minrate_rem);
            this.Controls.Add(this.txt_maxrate_rem);
            this.Controls.Add(this.Txt_ExRate_Rem);
            this.Controls.Add(this.Txt_Rem_Amount);
            this.Controls.Add(this.txt_code_rem);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.Txt_Rem_No);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.Txt_r_birthdate);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Txt_Reciever);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_job);
            this.Controls.Add(this.Txt_notes);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.Txt_S_Birth);
            this.Controls.Add(this.Txt_Relionship);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_S_doc_issue);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_S_doc_no);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtScity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_S_doc_type);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.Txt_s_job);
            this.Controls.Add(this.Txt_Soruce_money);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtTerm_Name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ADD_Incoming_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "525";
            this.Text = "ADD_Incoming_Rem";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ADD_Incoming_Rem_FormClosed);
            this.Load += new System.EventHandler(this.ADD_Incoming_Rem_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_procer)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label81;
        private MyDateTextBox Txt_r_birthdate;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Txt_Reciever;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_R_job;
        private System.Windows.Forms.TextBox Txt_notes;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label80;
        private MyDateTextBox Txt_S_Birth;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_S_doc_issue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtScity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_S_doc_type;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.TextBox Txt_s_job;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.TextBox txt_code_rem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox Txt_Rem_No;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.ComboBox cmb_cur;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Tot_amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDiscount_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox txt_locamount_rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_minrate_rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_maxrate_rem;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_ExRate_Rem;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Rem_Amount;
        private System.Windows.Forms.ComboBox Cbo_Oper;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox Txt_Accproc;
        private System.Windows.Forms.DataGridView Grd_procer;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox Txt_procer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox Txtr_Suburb;
        private System.Windows.Forms.TextBox Txtr_Street;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox Txtr_State;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox Txtr_Post_Code;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox Txt_r_Doc_No;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private MyDateTextBox Txt_Doc_r_Exp;
        private MyDateTextBox Txt_Doc_r_Date;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Txt_Doc_r_Issue;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txt_r_Doc_Type;
        private System.Windows.Forms.TextBox txt_rbirth_place;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox Txt_mail;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txt_Mother_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label24;
        private MyDateTextBox Txt_Doc_S_Date;
        private MyDateTextBox Txt_Doc_S_Exp;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txt_sbirth_place;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox Txts_State;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox Txts_Street;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox Txts_Suburb;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox Txts_Post_Code;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Sample.DecimalTextBox Txtr_amount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Txt_r_cur_name;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.ComboBox Cbo_city;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Cbo_Rem_Pay_Type;
        private System.Windows.Forms.Label label63;
    }
}