﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Query_Code_Rem : Form
    {
        public Query_Code_Rem()
        {
            InitializeComponent();
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }

        private void Query_Code_Rem_Load(object sender, EventArgs e)
        {

        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            txt_code_rem.Text = "";
            BindingSource Bs_code_Rem = new BindingSource();
            string SqlTxt = "";
            SqlTxt = " exec Get_Code_Rem  '" + Txt_Rem_No.Text.Trim() + "'";
            connection.SqlExec(SqlTxt, "Code_Rem_tbl");
            Bs_code_Rem.DataSource = connection.SQLDS.Tables["Code_Rem_tbl"];

            txt_code_rem.DataBindings.Clear();
            txt_code_rem.DataBindings.Add("Text", Bs_code_Rem, "Code_Rem");
        }

        private void Txt_Rem_No_TextChanged(object sender, EventArgs e)
        {
            txt_code_rem.Text = "";
        }
    }
}
