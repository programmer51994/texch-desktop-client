﻿namespace Integration_Accounting_Sys 
{
    partial class oto_Sys_Refues_Reveal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSacity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_PrintAll = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Grd_rem_view = new System.Windows.Forms.DataGridView();
            this.Txtracity = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txtr_nat = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtR_name = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Txtr_phone = new System.Windows.Forms.TextBox();
            this.Txtr_address = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Grd_User_Deatils = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Txtrem_no = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_rem_view)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_User_Deatils)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(0, 70);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(179, 14);
            this.label2.TabIndex = 909;
            this.label2.Text = "معلومــــات الحوالـــة الانـــي....";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(7, 423);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(147, 14);
            this.label3.TabIndex = 910;
            this.label3.Text = "معلومات الحوالــــــــــــة...";
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.Color.White;
            this.TxtS_address.Location = new System.Drawing.Point(578, 467);
            this.TxtS_address.Multiline = true;
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(371, 25);
            this.TxtS_address.TabIndex = 917;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(491, 471);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(75, 16);
            this.label23.TabIndex = 924;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(5, 471);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 16);
            this.label19.TabIndex = 918;
            this.label19.Text = "هاتف المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.Color.White;
            this.TxtS_phone.Location = new System.Drawing.Point(82, 467);
            this.TxtS_phone.Multiline = true;
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(166, 25);
            this.TxtS_phone.TabIndex = 925;
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.Color.White;
            this.TxtS_name.Location = new System.Drawing.Point(82, 440);
            this.TxtS_name.Multiline = true;
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(554, 25);
            this.TxtS_name.TabIndex = 926;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(5, 444);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 927;
            this.label5.Text = "اسم المرسل:";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.Color.White;
            this.Txts_nat.Location = new System.Drawing.Point(724, 440);
            this.Txts_nat.Multiline = true;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(226, 25);
            this.Txts_nat.TabIndex = 930;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(640, 444);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 931;
            this.label7.Text = "جنسية المرسل:";
            // 
            // TxtSacity
            // 
            this.TxtSacity.BackColor = System.Drawing.Color.White;
            this.TxtSacity.Location = new System.Drawing.Point(329, 467);
            this.TxtSacity.Multiline = true;
            this.TxtSacity.Name = "TxtSacity";
            this.TxtSacity.ReadOnly = true;
            this.TxtSacity.Size = new System.Drawing.Size(164, 25);
            this.TxtSacity.TabIndex = 932;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(250, 471);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 933;
            this.label8.Text = "مدينة المرسل:";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.Color.White;
            this.Txt_Purpose.Location = new System.Drawing.Point(83, 546);
            this.Txt_Purpose.Multiline = true;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(867, 25);
            this.Txt_Purpose.TabIndex = 972;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(5, 550);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 973;
            this.label17.Text = "غرض التحويل:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(159, 433);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(818, 1);
            this.flowLayoutPanel5.TabIndex = 993;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(179, 81);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(798, 1);
            this.flowLayoutPanel6.TabIndex = 994;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 581);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(978, 1);
            this.flowLayoutPanel4.TabIndex = 986;
            // 
            // Btn_PrintAll
            // 
            this.Btn_PrintAll.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_PrintAll.ForeColor = System.Drawing.Color.Navy;
            this.Btn_PrintAll.Location = new System.Drawing.Point(382, 585);
            this.Btn_PrintAll.Name = "Btn_PrintAll";
            this.Btn_PrintAll.Size = new System.Drawing.Size(105, 29);
            this.Btn_PrintAll.TabIndex = 987;
            this.Btn_PrintAll.Text = "تصدير اجمالي";
            this.Btn_PrintAll.UseVisualStyleBackColor = true;
            this.Btn_PrintAll.Click += new System.EventHandler(this.Print_All_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(487, 585);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(105, 29);
            this.Btn_Ext.TabIndex = 989;
            this.Btn_Ext.Text = "انهـــــاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Exist_Click);
            // 
            // Grd_rem_view
            // 
            this.Grd_rem_view.AllowUserToAddRows = false;
            this.Grd_rem_view.AllowUserToDeleteRows = false;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_rem_view.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle31;
            this.Grd_rem_view.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_rem_view.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.Grd_rem_view.ColumnHeadersHeight = 24;
            this.Grd_rem_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column10,
            this.Column14,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column8,
            this.Column9,
            this.Column5,
            this.Column6,
            this.Column11,
            this.Column19,
            this.Column7,
            this.Column48,
            this.Column12,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column40,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44,
            this.Column45,
            this.Column46,
            this.Column47,
            this.Column49,
            this.Column50,
            this.Column51,
            this.Column52,
            this.Column53,
            this.Column54,
            this.Column55,
            this.Column56,
            this.Column35,
            this.Column28,
            this.Column27});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_rem_view.DefaultCellStyle = dataGridViewCellStyle37;
            this.Grd_rem_view.Location = new System.Drawing.Point(7, 84);
            this.Grd_rem_view.Name = "Grd_rem_view";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_rem_view.RowHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.Grd_rem_view.RowHeadersVisible = false;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_rem_view.RowsDefaultCellStyle = dataGridViewCellStyle39;
            this.Grd_rem_view.Size = new System.Drawing.Size(964, 259);
            this.Grd_rem_view.TabIndex = 712;
            this.Grd_rem_view.VirtualMode = true;
            this.Grd_rem_view.SelectionChanged += new System.EventHandler(this.Grd_rem_view_SelectionChanged);
            // 
            // Txtracity
            // 
            this.Txtracity.BackColor = System.Drawing.Color.White;
            this.Txtracity.Location = new System.Drawing.Point(329, 520);
            this.Txtracity.Multiline = true;
            this.Txtracity.Name = "Txtracity";
            this.Txtracity.ReadOnly = true;
            this.Txtracity.Size = new System.Drawing.Size(164, 25);
            this.Txtracity.TabIndex = 1137;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(252, 524);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(71, 16);
            this.label13.TabIndex = 1138;
            this.label13.Text = "مدينة المستلم:";
            // 
            // Txtr_nat
            // 
            this.Txtr_nat.BackColor = System.Drawing.Color.White;
            this.Txtr_nat.Location = new System.Drawing.Point(726, 494);
            this.Txtr_nat.Multiline = true;
            this.Txtr_nat.Name = "Txtr_nat";
            this.Txtr_nat.ReadOnly = true;
            this.Txtr_nat.Size = new System.Drawing.Size(226, 25);
            this.Txtr_nat.TabIndex = 1135;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(642, 498);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(77, 16);
            this.label15.TabIndex = 1136;
            this.label15.Text = "جنسية المستلم:";
            // 
            // TxtR_name
            // 
            this.TxtR_name.BackColor = System.Drawing.Color.White;
            this.TxtR_name.Location = new System.Drawing.Point(82, 494);
            this.TxtR_name.Multiline = true;
            this.TxtR_name.Name = "TxtR_name";
            this.TxtR_name.ReadOnly = true;
            this.TxtR_name.Size = new System.Drawing.Size(554, 25);
            this.TxtR_name.TabIndex = 1133;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(5, 498);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(64, 16);
            this.label16.TabIndex = 1134;
            this.label16.Text = "اسم المستلم:";
            // 
            // Txtr_phone
            // 
            this.Txtr_phone.BackColor = System.Drawing.Color.White;
            this.Txtr_phone.Location = new System.Drawing.Point(82, 520);
            this.Txtr_phone.Multiline = true;
            this.Txtr_phone.Name = "Txtr_phone";
            this.Txtr_phone.ReadOnly = true;
            this.Txtr_phone.Size = new System.Drawing.Size(166, 25);
            this.Txtr_phone.TabIndex = 1132;
            // 
            // Txtr_address
            // 
            this.Txtr_address.BackColor = System.Drawing.Color.White;
            this.Txtr_address.Location = new System.Drawing.Point(580, 520);
            this.Txtr_address.Multiline = true;
            this.Txtr_address.Name = "Txtr_address";
            this.Txtr_address.ReadOnly = true;
            this.Txtr_address.Size = new System.Drawing.Size(371, 25);
            this.Txtr_address.TabIndex = 1129;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(493, 524);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(74, 16);
            this.label20.TabIndex = 1131;
            this.label20.Text = "عنوان المستلم:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(5, 524);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 16);
            this.label21.TabIndex = 1130;
            this.label21.Text = "هاتف المستلم:";
            // 
            // Grd_User_Deatils
            // 
            this.Grd_User_Deatils.AllowUserToAddRows = false;
            this.Grd_User_Deatils.AllowUserToDeleteRows = false;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle40.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_User_Deatils.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle40;
            this.Grd_User_Deatils.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_User_Deatils.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this.Grd_User_Deatils.ColumnHeadersHeight = 24;
            this.Grd_User_Deatils.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn15});
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle43.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_User_Deatils.DefaultCellStyle = dataGridViewCellStyle43;
            this.Grd_User_Deatils.Location = new System.Drawing.Point(7, 346);
            this.Grd_User_Deatils.Name = "Grd_User_Deatils";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_User_Deatils.RowHeadersDefaultCellStyle = dataGridViewCellStyle44;
            this.Grd_User_Deatils.RowHeadersVisible = false;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_User_Deatils.RowsDefaultCellStyle = dataGridViewCellStyle45;
            this.Grd_User_Deatils.Size = new System.Drawing.Size(964, 74);
            this.Grd_User_Deatils.TabIndex = 1143;
            this.Grd_User_Deatils.VirtualMode = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "note_user";
            dataGridViewCellStyle42.Format = "N3";
            dataGridViewCellStyle42.NullValue = null;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridViewTextBoxColumn14.HeaderText = "ملاحظات المستخدم";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 450;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "threshold";
            this.dataGridViewTextBoxColumn4.HeaderText = "نسبة التشابه";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "c_date";
            this.dataGridViewTextBoxColumn15.HeaderText = "تاريخ الحوالة";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 165;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(768, 7);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 1146;
            this.label4.Text = "التاريــــــخ:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(415, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(303, 23);
            this.TxtUser.TabIndex = 1145;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(333, 7);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 1144;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 3);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(320, 23);
            this.TxtTerm_Name.TabIndex = 1148;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(608, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(180, 16);
            this.label6.TabIndex = 1004;
            this.label6.Text = "*يجب تهيئة الحولات المرفوضة وردها";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(977, 1);
            this.flowLayoutPanel1.TabIndex = 1149;
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "yyyy/mm/dd";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(237, 35);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(102, 20);
            this.TxtToDate.TabIndex = 975;
            this.TxtToDate.ValueChanged += new System.EventHandler(this.TxtToDate_ValueChanged);
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "yyyy/mm/dd";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(89, 35);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(106, 20);
            this.TxtFromDate.TabIndex = 974;
            this.TxtFromDate.ValueChanged += new System.EventHandler(this.TxtFromDate_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(201, 38);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(36, 14);
            this.label9.TabIndex = 973;
            this.label9.Text = "الى :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(11, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 16);
            this.label11.TabIndex = 972;
            this.label11.Text = "تاريــخ الحالـة:";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(888, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 24);
            this.button2.TabIndex = 1150;
            this.button2.Text = "بحــــــث";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Txtrem_no
            // 
            this.Txtrem_no.Location = new System.Drawing.Point(438, 35);
            this.Txtrem_no.MaxLength = 14;
            this.Txtrem_no.Name = "Txtrem_no";
            this.Txtrem_no.Size = new System.Drawing.Size(148, 20);
            this.Txtrem_no.TabIndex = 1152;
            this.Txtrem_no.TextChanged += new System.EventHandler(this.Txtrem_no_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(348, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 16);
            this.label10.TabIndex = 1153;
            this.label10.Text = "رقـم الحوالــــــة:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(839, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(132, 22);
            this.TxtIn_Rec_Date.TabIndex = 1147;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحوالة";
            this.Column3.Name = "Column3";
            this.Column3.Width = 84;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "S_Acust_name";
            this.Column10.HeaderText = "اسم الفرع";
            this.Column10.Name = "Column10";
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "customer_Aname";
            this.Column14.HeaderText = "اسم الوكيل";
            this.Column14.Name = "Column14";
            this.Column14.Width = 84;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "R_amount";
            dataGridViewCellStyle33.Format = "N3";
            dataGridViewCellStyle33.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle33;
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.Width = 88;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.Width = 88;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PR_ACUR_NAME";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.Width = 88;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "t_ACITY_NAME";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.Width = 96;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "S_ACITY_NAME";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.Width = 97;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.Width = 88;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Case_Date";
            dataGridViewCellStyle34.Format = "G";
            dataGridViewCellStyle34.NullValue = "dd/MM/yyyy";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle34;
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.Width = 91;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "C_DATE";
            dataGridViewCellStyle35.NullValue = "dd\'/\'MM\'/\'yyyy";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle35;
            this.Column11.HeaderText = "تاريخ ووقت الانشاء";
            this.Column11.Name = "Column11";
            this.Column11.Width = 127;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "user_NAME";
            this.Column19.HeaderText = "المستخدم";
            this.Column19.Name = "Column19";
            this.Column19.Width = 76;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "اسم المرسل";
            this.Column7.Name = "Column7";
            this.Column7.Width = 87;
            // 
            // Column48
            // 
            this.Column48.DataPropertyName = "S_Ename";
            this.Column48.HeaderText = "الاسم الاخر";
            this.Column48.Name = "Column48";
            this.Column48.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "S_phone";
            this.Column12.HeaderText = "هاتف المرسل";
            this.Column12.Name = "Column12";
            this.Column12.Width = 97;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "S_address";
            this.Column16.HeaderText = "عنوان المرسل";
            this.Column16.Name = "Column16";
            this.Column16.Width = 97;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "S_Street";
            this.Column17.HeaderText = "الزقاق";
            this.Column17.Name = "Column17";
            this.Column17.Width = 64;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "S_Suburb";
            this.Column18.HeaderText = "الحي";
            this.Column18.Name = "Column18";
            this.Column18.Width = 57;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column20.DataPropertyName = "S_Post_Code";
            this.Column20.HeaderText = "الرقم البريدي";
            this.Column20.Name = "Column20";
            this.Column20.Width = 94;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column21.DataPropertyName = "S_State";
            this.Column21.HeaderText = "المحافظة";
            this.Column21.Name = "Column21";
            this.Column21.Width = 76;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column22.DataPropertyName = "S_doc_no";
            this.Column22.HeaderText = "رقم هوية المرسل";
            this.Column22.Name = "Column22";
            this.Column22.Width = 113;
            // 
            // Column23
            // 
            this.Column23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column23.DataPropertyName = "S_doc_ida";
            this.Column23.HeaderText = "تاريخ اصدار هوية المرسل";
            this.Column23.Name = "Column23";
            this.Column23.Width = 154;
            // 
            // Column24
            // 
            this.Column24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column24.DataPropertyName = "S_doc_eda";
            this.Column24.HeaderText = "تاريخ انتهاء هوية المرسل";
            this.Column24.Name = "Column24";
            this.Column24.Width = 154;
            // 
            // Column25
            // 
            this.Column25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column25.DataPropertyName = "S_doc_issue";
            this.Column25.HeaderText = "نوع الوثيقة للمرسل";
            this.Column25.Name = "Column25";
            this.Column25.Width = 121;
            // 
            // Column26
            // 
            this.Column26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column26.DataPropertyName = "s_Email";
            this.Column26.HeaderText = "ايميل المرسل";
            this.Column26.Name = "Column26";
            this.Column26.Width = 94;
            // 
            // Column29
            // 
            this.Column29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column29.DataPropertyName = "s_A_NAT_NAME";
            this.Column29.HeaderText = "جنسية المرسل";
            this.Column29.Name = "Column29";
            // 
            // Column30
            // 
            this.Column30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column30.DataPropertyName = "s_notes";
            this.Column30.HeaderText = "ملاحظات المرسل";
            this.Column30.Name = "Column30";
            this.Column30.Width = 113;
            // 
            // Column31
            // 
            this.Column31.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column31.DataPropertyName = "r_name";
            this.Column31.HeaderText = "اسم المستلم";
            this.Column31.Name = "Column31";
            this.Column31.Width = 86;
            // 
            // Column32
            // 
            this.Column32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column32.DataPropertyName = "R_phone";
            this.Column32.HeaderText = "هاتف المستلم";
            this.Column32.Name = "Column32";
            this.Column32.Width = 96;
            // 
            // Column33
            // 
            this.Column33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column33.DataPropertyName = "r_ACITY_NAME";
            this.Column33.HeaderText = "مدينة المستلم";
            this.Column33.Name = "Column33";
            this.Column33.Width = 93;
            // 
            // Column34
            // 
            this.Column34.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column34.DataPropertyName = "r_ACOUN_NAME";
            this.Column34.HeaderText = "بلد المستلم";
            this.Column34.Name = "Column34";
            this.Column34.Width = 81;
            // 
            // Column36
            // 
            this.Column36.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column36.DataPropertyName = "r_address";
            this.Column36.HeaderText = "عنوان المستلم";
            this.Column36.Name = "Column36";
            this.Column36.Width = 96;
            // 
            // Column37
            // 
            this.Column37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column37.DataPropertyName = "r_Street";
            this.Column37.HeaderText = "الزقاق";
            this.Column37.Name = "Column37";
            this.Column37.Width = 64;
            // 
            // Column38
            // 
            this.Column38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column38.DataPropertyName = "r_Suburb";
            this.Column38.HeaderText = "الحي للمستلم";
            this.Column38.Name = "Column38";
            this.Column38.Width = 93;
            // 
            // Column39
            // 
            this.Column39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column39.DataPropertyName = "r_Post_Code";
            this.Column39.HeaderText = "الرمز البريدي للمستلم";
            this.Column39.Name = "Column39";
            this.Column39.Width = 131;
            // 
            // Column40
            // 
            this.Column40.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column40.DataPropertyName = "r_State";
            this.Column40.HeaderText = "محافظة المستلم";
            this.Column40.Name = "Column40";
            this.Column40.Width = 106;
            // 
            // Column41
            // 
            this.Column41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column41.DataPropertyName = "r_doc_no";
            this.Column41.HeaderText = "رقم وثيقة المستلم";
            this.Column41.Name = "Column41";
            this.Column41.Width = 113;
            // 
            // Column42
            // 
            this.Column42.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column42.DataPropertyName = "r_doc_ida";
            this.Column42.HeaderText = "تاريخ اصدار المستلم";
            this.Column42.Name = "Column42";
            this.Column42.Width = 127;
            // 
            // Column43
            // 
            this.Column43.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column43.DataPropertyName = "r_doc_eda";
            this.Column43.HeaderText = "تاريخ انتهاء الوثيقة";
            this.Column43.Name = "Column43";
            this.Column43.Width = 124;
            // 
            // Column44
            // 
            this.Column44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column44.DataPropertyName = "r_doc_issue";
            this.Column44.HeaderText = "نوع الوثيقة";
            this.Column44.Name = "Column44";
            this.Column44.Width = 84;
            // 
            // Column45
            // 
            this.Column45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column45.DataPropertyName = "r_Email";
            this.Column45.HeaderText = "ايميل المستلم";
            this.Column45.Name = "Column45";
            this.Column45.Width = 93;
            // 
            // Column46
            // 
            this.Column46.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column46.DataPropertyName = "T_purpose";
            this.Column46.HeaderText = "غرض الارسال";
            this.Column46.Name = "Column46";
            this.Column46.Width = 101;
            // 
            // Column47
            // 
            this.Column47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column47.DataPropertyName = "r_notes";
            this.Column47.HeaderText = "ملاحظات المستلم";
            this.Column47.Name = "Column47";
            this.Column47.Width = 112;
            // 
            // Column49
            // 
            this.Column49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column49.DataPropertyName = "In_rec_date";
            this.Column49.HeaderText = "تاريخ السجلات";
            this.Column49.Name = "Column49";
            this.Column49.Width = 104;
            // 
            // Column50
            // 
            this.Column50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column50.DataPropertyName = "sbirth_date";
            this.Column50.HeaderText = "تولد المرسل";
            this.Column50.Name = "Column50";
            this.Column50.Width = 88;
            // 
            // Column51
            // 
            this.Column51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column51.DataPropertyName = "rbirth_date";
            this.Column51.HeaderText = "تولد المستلم";
            this.Column51.Name = "Column51";
            this.Column51.Width = 87;
            // 
            // Column52
            // 
            this.Column52.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column52.DataPropertyName = "SBirth_Place";
            this.Column52.HeaderText = "مكان تولد المرسل";
            this.Column52.Name = "Column52";
            this.Column52.Width = 117;
            // 
            // Column53
            // 
            this.Column53.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column53.DataPropertyName = "s_ocomm";
            this.Column53.HeaderText = "العمولة المقبوضة من الزبون";
            this.Column53.Name = "Column53";
            this.Column53.Width = 158;
            // 
            // Column54
            // 
            this.Column54.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column54.DataPropertyName = "s_ACUR_NAME";
            this.Column54.HeaderText = "عملة العمولة المقبوضة من الزبون";
            this.Column54.Name = "Column54";
            this.Column54.Width = 182;
            // 
            // Column55
            // 
            this.Column55.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column55.DataPropertyName = "c_ocomm";
            this.Column55.HeaderText = "عمولة مركز الاونلاين";
            this.Column55.Name = "Column55";
            this.Column55.Width = 131;
            // 
            // Column56
            // 
            this.Column56.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column56.DataPropertyName = "c_ACUR_NAME";
            this.Column56.HeaderText = "عملة عمولة مركز الاونلاين";
            this.Column56.Name = "Column56";
            this.Column56.Width = 155;
            // 
            // Column35
            // 
            this.Column35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column35.DataPropertyName = "rate_online";
            dataGridViewCellStyle36.Format = "N3";
            dataGridViewCellStyle36.NullValue = null;
            this.Column35.DefaultCellStyle = dataGridViewCellStyle36;
            this.Column35.HeaderText = "سعر التعادل";
            this.Column35.Name = "Column35";
            this.Column35.Width = 90;
            // 
            // Column28
            // 
            this.Column28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column28.DataPropertyName = "Source_money";
            this.Column28.HeaderText = "مصدر المال";
            this.Column28.Name = "Column28";
            this.Column28.Width = 87;
            // 
            // Column27
            // 
            this.Column27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column27.DataPropertyName = "Relation_S_R";
            this.Column27.HeaderText = "علاقة المرسل بالمستلم";
            this.Column27.Name = "Column27";
            this.Column27.Width = 138;
            // 
            // oto_Sys_Refues_Reveal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 616);
            this.Controls.Add(this.Txtrem_no);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Grd_User_Deatils);
            this.Controls.Add(this.Txtracity);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txtr_nat);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtR_name);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txtr_phone);
            this.Controls.Add(this.Txtr_address);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.Btn_PrintAll);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.TxtSacity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.TxtS_address);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Grd_rem_view);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "oto_Sys_Refues_Reveal";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "576";
            this.Text = "كشف حوالات نظام المراسلات";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.oto_Sys_Refues_Reveal_FormClosed);
            this.Load += new System.EventHandler(this.oto_Sys_Refues_Reveal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_rem_view)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_User_Deatils)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtSacity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button Btn_PrintAll;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.DataGridView Grd_rem_view;
        private System.Windows.Forms.TextBox Txtracity;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txtr_nat;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TxtR_name;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txtr_phone;
        private System.Windows.Forms.TextBox Txtr_address;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView Grd_User_Deatils;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox Txtrem_no;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column50;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column51;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column52;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column53;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column54;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column55;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column56;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
    }
}