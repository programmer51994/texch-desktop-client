﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class reveal_rem_Case_purpose : Form
    {

        BindingSource Bs_Quey = new BindingSource();
        BindingSource binding_cbo_city = new BindingSource();
        BindingSource binding_tcity_id = new BindingSource();
        BindingSource binding_rcur = new BindingSource();
        BindingSource binding_pcur = new BindingSource();
        BindingSource binding_Query = new BindingSource();
        BindingSource binding_Query2 = new BindingSource();
        string @format = "dd/MM/yyyy";
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();

        DataGridView Date_Grd_1 = new DataGridView();
        DataTable Date_Tbl_1 = new DataTable();

        Int32 from_nrec_date = 0;
        Int32 to_nrec_date = 0;


        public reveal_rem_Case_purpose()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

        }


        private void Query_Rem_Load(object sender, EventArgs e)
        {

            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            if (connection.Lang_id == 2)
            {
                Cbo_Rem_type.Items.Add("out");
                Cbo_Rem_type.Items.Add("in");

                cbo_Rem_cur.Items.Add(" in real currency");
                cbo_Rem_cur.Items.Add(" in Local currency");


            }
            else
            {

                Cbo_Rem_type.Items.Add("صادر");
                Cbo_Rem_type.Items.Add("وارد");

                cbo_Rem_cur.Items.Add("بالعملة الاصلية");
                cbo_Rem_cur.Items.Add("بالعملة المحلية");
            }

            Cbo_Rem_type.SelectedIndex = 0;
            cbo_Rem_cur.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {
                if (Grd_comm_info.Rows.Count > 0)
                {
                    Date();


                    DataTable Dt = new DataTable();
                    Dt = connection.SQLDS.Tables["Query_comm_tbl"].DefaultView.ToTable(false, "ACOUN_NAME", "resd_flag", "cur_name", "Personal Rem", "Trade",
                        "Study", "Treatment", "Retirement", "Others", "Workers  Rem", "Investment Rem", "trans").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Date_Grd_1, Grd_comm_info };
                    DataTable[] Export_DT = { Date_Tbl, Date_Tbl_1, Dt };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }

                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            else
            {

                if (Grd_comm_info.Rows.Count > 0)
                {
                    Date();


                    DataTable Dt = new DataTable();
                    Dt = connection.SQLDS.Tables["Query_comm_tbl"].DefaultView.ToTable(false, "ACOUN_NAME", "resd_flag", "cur_name", "تحويلات شخصية", "تجارة",
                        "دراسة", "علاج", "تقاعد", "اخرى", "حوالات عاملين", "حوالات استثمارية", "ترانزيت").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Date_Grd, Date_Grd_1, Grd_comm_info };
                    DataTable[] Export_DT = { Date_Tbl, Date_Tbl_1, Dt };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                    return;
                }
            }


        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            Int16 cbo_r_type = 0;
            Int16 cbo_r_cur = 0;
            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ بداية الفترة" : "select the date of first period", MyGeneral_Lib.LblCap);
                return;
            }

            if (TxtToDate.Checked == true)
            {

                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ نهاية الفترة" : "select the date of end period", MyGeneral_Lib.LblCap);
                return;
            }



            if (Cbo_Rem_type.SelectedIndex == 0)
            {
                cbo_r_type = 2;

            }

            else
            {
                cbo_r_type = 1;
            }


            if (cbo_Rem_cur.SelectedIndex == 0)
            {
                cbo_r_cur = 2;

            }

            else
            {
                cbo_r_cur = 1;
            }



            string Sqltexe = "EXec query_rem_Case_purpose  " + from_nrec_date + "," + to_nrec_date + ","
                + cbo_r_type + ","
                + cbo_r_cur + "," + connection.Lang_id;

            connection.SqlExec(Sqltexe, "Query_comm_tbl");
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();

            if (connection.SQLDS.Tables["Query_comm_tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات للعرض" : "No data exist to show", MyGeneral_Lib.LblCap);
                Grd_comm_info.DataSource = new BindingSource();
                return;
            }

            if (connection.SQLDS.Tables["Query_comm_tbl"].Rows.Count > 0)
            {

                binding_Query.DataSource = connection.SQLDS.Tables["Query_comm_tbl"];
                Grd_comm_info.DataSource = binding_Query;
            }
        }



        private void Query_Commission_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "Get_info_Tbl", "Full_information_tab", "Full_information_tab4", "Query_comm_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

        }



        private void Date()
        {

            Date_Grd = new DataGridView();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "Date From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "Date To:");
            Date_Grd_1 = new DataGridView();
            Date_Grd_1.Columns.Add("Column1", connection.Lang_id == 1 ? "نوع الحوالة" : "Remittence Type");
            Date_Grd_1.Columns.Add("Column2", connection.Lang_id == 1 ? "مبلغ الحوالة:" : "Remittence amount");

            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);

            string[] Column1 = { "Remittence_Type", "Remittence_currency" };
            string[] DType1 = { "System.String", "System.String" };
            Date_Tbl_1 = CustomControls.Custom_DataTable("Date_Tbl_1", Column1, DType1);

            string date_from = "";
            date_from = TxtFromDate.Text;
            string date_To = "";
            date_To = TxtToDate.Text;
            string rem_ytpe = "";
            rem_ytpe = Cbo_Rem_type.SelectedItem.ToString();
            string rem_cur = "";
            rem_cur = cbo_Rem_cur.SelectedItem.ToString();
            Date_Tbl.Rows.Add(new object[] { date_from, date_To });
            Date_Tbl_1.Rows.Add(new object[] { rem_ytpe, rem_cur });


        }

        private void Cbo_Rem_type_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void cbo_Rem_cur_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (Grd_comm_info.Rows.Count > 0)
            {

                Int32 Frm_id = 0;
                string frm_name = "إستعلام عن مجاميع الحوالات لأغراض التحويل";
                string frm_Ename = "Inquire about transfer funds for transfer purposes";
                string user_name = connection.User_Name;
                reveal_rem_Case_purpose_Rpt ObjRpt = new reveal_rem_Case_purpose_Rpt();
                reveal_rem_Case_purpose_Rpt_eng ObjRptEng = new reveal_rem_Case_purpose_Rpt_eng();
                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("Frm_id", Frm_id);
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("frm_Ename", frm_Ename);
                Dt_Param.Rows.Add("user_name", user_name);
                Dt_Param.Rows.Add("date_from", TxtFromDate.Text);
                Dt_Param.Rows.Add("to_date", TxtToDate.Text);
                Dt_Param.Rows.Add("Rem_type", Cbo_Rem_type.SelectedIndex == 0 ? "صادر" : "وارد");//Cbo_Rem_type.SelectedItem.ToString());
                Dt_Param.Rows.Add("Rem_Etype", Cbo_Rem_type.SelectedIndex == 0 ? "out" : "in");
                Dt_Param.Rows.Add("Rem_cur", cbo_Rem_cur.SelectedIndex == 0 ? "بالعملة الاصلية" : "بالعملة المحلية");
                Dt_Param.Rows.Add("Rem_Ecur", cbo_Rem_cur.SelectedIndex == 0 ? "in real currency" : "in Local currency");



                DataTable Exp_Dt = new DataTable();

                Exp_Dt = connection.SQLDS.Tables["Query_comm_tbl1"].DefaultView.ToTable(false, "ACOUN_NAME", "ECOUN_NAME", "resd_flag", "Eresd_flag", "cur_name", "Ecur_name", "Personal_Rem", "Trade",
                      "Study", "Treatment", "Retirement", "Others", "Workers_Rem", "Investment_Rem", "trans").Select().CopyToDataTable();

                DataTable[] _Exp_Dt = { Exp_Dt };
                DataGridView[] GRd = { Grd_comm_info };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, true, Dt_Param, GRd, _Exp_Dt, true, false, this.Text);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
    }
}