﻿namespace Integration_Accounting_Sys
{
    partial class View_Remittence_Rem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.Grdrec_rem = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_address = new System.Windows.Forms.TextBox();
            this.Txt_r_job = new System.Windows.Forms.TextBox();
            this.Txt_Discreption = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_S_doc_issue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtScity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_S_doc_type = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.Txt_s_job = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Txt_Rem_No = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.Cbo_city = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Rpt = new System.Windows.Forms.Button();
            this.txt_rname = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_sname = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.Txt_Nrec_date = new System.Windows.Forms.DateTimePicker();
            this.Cmb_oper_type = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txt_S_details_job = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.Txt_R_details_job = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txt_purpose_rec = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Txt_R_Relation = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.Txt_R_notes = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_ToNrec_date = new System.Windows.Forms.DateTimePicker();
            this.label36 = new System.Windows.Forms.Label();
            this.Chk_Hst_Info = new System.Windows.Forms.CheckBox();
            this.txt_ramont = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_r_birthdate = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_Birth = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_doc_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(166, 14);
            this.label2.TabIndex = 1002;
            this.label2.Text = "استعلام عن حالة الحوالــــــة";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-8, 31);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(953, 1);
            this.flowLayoutPanel9.TabIndex = 1001;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-71, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(676, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(71, 14);
            this.label4.TabIndex = 998;
            this.label4.Text = "التاريــــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(755, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(184, 23);
            this.TxtIn_Rec_Date.TabIndex = 999;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(9, 5);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(265, 23);
            this.TxtTerm_Name.TabIndex = 996;
            // 
            // Grdrec_rem
            // 
            this.Grdrec_rem.AllowUserToAddRows = false;
            this.Grdrec_rem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grdrec_rem.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grdrec_rem.ColumnHeadersHeight = 24;
            this.Grdrec_rem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column7,
            this.Column12,
            this.Column13,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column5,
            this.Column6,
            this.Column14,
            this.Column11});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdrec_rem.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grdrec_rem.Location = new System.Drawing.Point(8, 136);
            this.Grdrec_rem.Name = "Grdrec_rem";
            this.Grdrec_rem.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Grdrec_rem.RowHeadersVisible = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grdrec_rem.Size = new System.Drawing.Size(932, 137);
            this.Grdrec_rem.TabIndex = 5;
            this.Grdrec_rem.VirtualMode = true;
            this.Grdrec_rem.SelectionChanged += new System.EventHandler(this.Grdrec_rem_SelectionChanged);
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحولة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 81;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "اسم المرسـل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 90;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "rate_online";
            this.Column12.HeaderText = "المعادل";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 68;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "rate_online_amount";
            this.Column13.HeaderText = "المبلغ المدفوع";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "R_amount";
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 88;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_Cur_name";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 88;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PR_CUR_name";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 88;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "t_city_aname";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 96;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "scity_name";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 97;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "r_name";
            this.Column10.HeaderText = "اسم المستلم";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 86;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column15.DataPropertyName = "r_address";
            this.Column15.HeaderText = "عنوان المستلم";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 96;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "r_job";
            this.Column16.HeaderText = "مهنة المستلم";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 90;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "rfrm_adoc_na";
            this.Column17.HeaderText = "نوع وثيقة المستلم";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 114;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "r_doc_no";
            this.Column18.HeaderText = "رقم وثيقة المستلم";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 113;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "r_doc_ida";
            this.Column19.HeaderText = "تاريخ اصدار وثيقة المستلم";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 154;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column20.DataPropertyName = "r_doc_eda";
            this.Column20.HeaderText = "تاريخ انتهاء وثيقة المستلم";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 154;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column21.DataPropertyName = "r_doc_issue";
            this.Column21.HeaderText = "جهة اصدار وثيقة المستلم";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 145;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column22.DataPropertyName = "D_Acust_name";
            this.Column22.HeaderText = "الوكيل الدافع";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Width = 95;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 88;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Case_Date";
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 91;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "ver_flag_name";
            this.Column14.HeaderText = "حالة التحقق";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 88;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "C_DATE";
            this.Column11.HeaderText = "تاريخ ووقت الانشاء";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 127;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(323, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 14);
            this.label12.TabIndex = 1005;
            this.label12.Text = "المدينــــــــة:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(8, 117);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(165, 14);
            this.label1.TabIndex = 1006;
            this.label1.Text = "معلومــــات الحوالــــــــــــــة...";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(120, 462);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel3.TabIndex = 1071;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(2, 452);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(120, 14);
            this.label15.TabIndex = 1070;
            this.label15.Text = "معلومات المستلم...";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(234, 528);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(87, 14);
            this.label16.TabIndex = 1069;
            this.label16.Text = "yyyy/mm/dd";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Purpose.Location = new System.Drawing.Point(720, 404);
            this.Txt_Purpose.Multiline = true;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(217, 25);
            this.Txt_Purpose.TabIndex = 1066;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(635, 408);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 1067;
            this.label17.Text = "غرض التحويل:";
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_City.Location = new System.Drawing.Point(80, 496);
            this.Txt_R_City.Multiline = true;
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_City.TabIndex = 1058;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(1, 500);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(71, 16);
            this.label27.TabIndex = 1059;
            this.label27.Text = "مدينة المستلم:";
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Nat.Location = new System.Drawing.Point(717, 470);
            this.Txt_R_Nat.Multiline = true;
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(217, 25);
            this.Txt_R_Nat.TabIndex = 1056;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(638, 474);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(77, 16);
            this.label28.TabIndex = 1057;
            this.label28.Text = "جنسية المستلم:";
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Name.Location = new System.Drawing.Point(80, 470);
            this.Txt_R_Name.Multiline = true;
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(247, 25);
            this.Txt_R_Name.TabIndex = 1052;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(1, 474);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(64, 16);
            this.label30.TabIndex = 1053;
            this.label30.Text = "اسم المستلم:";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Phone.Location = new System.Drawing.Point(416, 470);
            this.Txt_R_Phone.Multiline = true;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(218, 25);
            this.Txt_R_Phone.TabIndex = 1051;
            // 
            // Txt_R_address
            // 
            this.Txt_R_address.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_address.Location = new System.Drawing.Point(417, 496);
            this.Txt_R_address.Multiline = true;
            this.Txt_R_address.Name = "Txt_R_address";
            this.Txt_R_address.ReadOnly = true;
            this.Txt_R_address.Size = new System.Drawing.Size(516, 25);
            this.Txt_R_address.TabIndex = 1043;
            // 
            // Txt_r_job
            // 
            this.Txt_r_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_r_job.Location = new System.Drawing.Point(80, 551);
            this.Txt_r_job.Multiline = true;
            this.Txt_r_job.Name = "Txt_r_job";
            this.Txt_r_job.ReadOnly = true;
            this.Txt_r_job.Size = new System.Drawing.Size(247, 25);
            this.Txt_r_job.TabIndex = 1042;
            // 
            // Txt_Discreption
            // 
            this.Txt_Discreption.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Discreption.Location = new System.Drawing.Point(720, 432);
            this.Txt_Discreption.Multiline = true;
            this.Txt_Discreption.Name = "Txt_Discreption";
            this.Txt_Discreption.ReadOnly = true;
            this.Txt_Discreption.Size = new System.Drawing.Size(219, 25);
            this.Txt_Discreption.TabIndex = 1041;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(332, 500);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(74, 16);
            this.label31.TabIndex = 1050;
            this.label31.Text = "عنوان المستلم:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(1, 555);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(68, 16);
            this.label32.TabIndex = 1049;
            this.label32.Text = "مهنة المستلم:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(636, 436);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 1048;
            this.label33.Text = "التفاصيـــــــل:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(6, 527);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(71, 16);
            this.label35.TabIndex = 1047;
            this.label35.Text = "التولــــــــــــد:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(332, 474);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 16);
            this.label38.TabIndex = 1044;
            this.label38.Text = "هاتف المستلم:";
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(116, 283);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel17.TabIndex = 1039;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(2, 272);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(116, 14);
            this.label14.TabIndex = 1038;
            this.label14.Text = "معلومات المرسل...";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(849, 355);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(87, 14);
            this.label13.TabIndex = 1037;
            this.label13.Text = "yyyy/mm/dd";
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relionship.Location = new System.Drawing.Point(417, 350);
            this.Txt_Relionship.Multiline = true;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(218, 25);
            this.Txt_Relionship.TabIndex = 1034;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(326, 354);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 1035;
            this.label3.Text = "علاقة مر /  مس:";
            // 
            // Txt_S_doc_issue
            // 
            this.Txt_S_doc_issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_issue.Location = new System.Drawing.Point(79, 350);
            this.Txt_S_doc_issue.Multiline = true;
            this.Txt_S_doc_issue.Name = "Txt_S_doc_issue";
            this.Txt_S_doc_issue.ReadOnly = true;
            this.Txt_S_doc_issue.Size = new System.Drawing.Size(247, 25);
            this.Txt_S_doc_issue.TabIndex = 1030;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(0, 354);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 1031;
            this.label10.Text = "محل اصدارها:";
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_no.Location = new System.Drawing.Point(414, 377);
            this.Txt_S_doc_no.Multiline = true;
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.ReadOnly = true;
            this.Txt_S_doc_no.Size = new System.Drawing.Size(218, 25);
            this.Txt_S_doc_no.TabIndex = 1028;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(327, 381);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 1029;
            this.label9.Text = "رقم الوثيقـــــة:";
            // 
            // TxtScity
            // 
            this.TxtScity.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtScity.Location = new System.Drawing.Point(79, 322);
            this.TxtScity.Multiline = true;
            this.TxtScity.Name = "TxtScity";
            this.TxtScity.ReadOnly = true;
            this.TxtScity.Size = new System.Drawing.Size(247, 25);
            this.TxtScity.TabIndex = 1026;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(0, 326);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 1027;
            this.label8.Text = "مدينة المرسل:";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txts_nat.Location = new System.Drawing.Point(717, 296);
            this.Txts_nat.Multiline = true;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(217, 25);
            this.Txts_nat.TabIndex = 1024;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(631, 300);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 1025;
            this.label7.Text = "جنسية المرسل:";
            // 
            // Txt_S_doc_type
            // 
            this.Txt_S_doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_doc_type.Location = new System.Drawing.Point(79, 377);
            this.Txt_S_doc_type.Multiline = true;
            this.Txt_S_doc_type.Name = "Txt_S_doc_type";
            this.Txt_S_doc_type.ReadOnly = true;
            this.Txt_S_doc_type.Size = new System.Drawing.Size(247, 25);
            this.Txt_S_doc_type.TabIndex = 1023;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(0, 380);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 1022;
            this.label6.Text = "نوع الوثيقة:";
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_name.Location = new System.Drawing.Point(79, 296);
            this.TxtS_name.Multiline = true;
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(247, 25);
            this.TxtS_name.TabIndex = 1020;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(0, 300);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 1021;
            this.label5.Text = "اسم المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_phone.Location = new System.Drawing.Point(414, 296);
            this.TxtS_phone.Multiline = true;
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(218, 25);
            this.TxtS_phone.TabIndex = 1019;
            // 
            // Txt_s_job
            // 
            this.Txt_s_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_s_job.Location = new System.Drawing.Point(79, 404);
            this.Txt_s_job.Multiline = true;
            this.Txt_s_job.Name = "Txt_s_job";
            this.Txt_s_job.ReadOnly = true;
            this.Txt_s_job.Size = new System.Drawing.Size(247, 25);
            this.Txt_s_job.TabIndex = 1010;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(80, 432);
            this.Txt_Soruce_money.Multiline = true;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(553, 25);
            this.Txt_Soruce_money.TabIndex = 1009;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(327, 326);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(75, 16);
            this.label23.TabIndex = 1018;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(0, 407);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(69, 16);
            this.label22.TabIndex = 1017;
            this.label22.Text = "مهنة المرسل:";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(1, 436);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(103, 16);
            this.label21.TabIndex = 1016;
            this.label21.Text = "مصـــدر المــال:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(644, 354);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(65, 16);
            this.label20.TabIndex = 1015;
            this.label20.Text = "التولــــــــــد:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(846, 383);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 1014;
            this.label34.Text = "yyyy/mm/dd";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(636, 381);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(73, 16);
            this.label18.TabIndex = 1013;
            this.label18.Text = "تاريخهــــــــا:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(327, 300);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 16);
            this.label19.TabIndex = 1012;
            this.label19.Text = "هاتف المرسل:";
            // 
            // Txt_Rem_No
            // 
            this.Txt_Rem_No.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_No.Location = new System.Drawing.Point(92, 91);
            this.Txt_Rem_No.MaxLength = 14;
            this.Txt_Rem_No.Name = "Txt_Rem_No";
            this.Txt_Rem_No.Size = new System.Drawing.Size(222, 22);
            this.Txt_Rem_No.TabIndex = 3;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(3, 95);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(89, 14);
            this.label39.TabIndex = 1072;
            this.label39.Text = "رقم الحوالــــة:";
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.SearchBtn.ForeColor = System.Drawing.Color.Navy;
            this.SearchBtn.Location = new System.Drawing.Point(848, 88);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(93, 26);
            this.SearchBtn.TabIndex = 4;
            this.SearchBtn.Text = "بحـــــث";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.Btn_Search_Click);
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_address.Location = new System.Drawing.Point(414, 322);
            this.TxtS_address.Multiline = true;
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(519, 25);
            this.TxtS_address.TabIndex = 1011;
            // 
            // Cbo_city
            // 
            this.Cbo_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_city.Enabled = false;
            this.Cbo_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_city.FormattingEnabled = true;
            this.Cbo_city.Location = new System.Drawing.Point(402, 4);
            this.Cbo_city.Name = "Cbo_city";
            this.Cbo_city.Size = new System.Drawing.Size(256, 24);
            this.Cbo_city.TabIndex = 1004;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(472, 590);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 25);
            this.button1.TabIndex = 8;
            this.button1.Text = "انهـــاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(391, 590);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 25);
            this.button2.TabIndex = 7;
            this.button2.Text = "تصديـــر";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-4, 585);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(953, 1);
            this.flowLayoutPanel2.TabIndex = 1078;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-71, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // Btn_Rpt
            // 
            this.Btn_Rpt.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Rpt.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Rpt.Location = new System.Drawing.Point(6, 592);
            this.Btn_Rpt.Name = "Btn_Rpt";
            this.Btn_Rpt.Size = new System.Drawing.Size(81, 25);
            this.Btn_Rpt.TabIndex = 6;
            this.Btn_Rpt.Text = "طباعــــة";
            this.Btn_Rpt.UseVisualStyleBackColor = true;
            this.Btn_Rpt.Visible = false;
            this.Btn_Rpt.Click += new System.EventHandler(this.button3_Click);
            // 
            // txt_rname
            // 
            this.txt_rname.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_rname.Location = new System.Drawing.Point(90, 63);
            this.txt_rname.MaxLength = 49;
            this.txt_rname.Name = "txt_rname";
            this.txt_rname.Size = new System.Drawing.Size(224, 22);
            this.txt_rname.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(1, 67);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(90, 14);
            this.label11.TabIndex = 1080;
            this.label11.Text = "اسم المستلم:";
            // 
            // txt_sname
            // 
            this.txt_sname.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_sname.Location = new System.Drawing.Point(401, 63);
            this.txt_sname.MaxLength = 49;
            this.txt_sname.Name = "txt_sname";
            this.txt_sname.Size = new System.Drawing.Size(202, 22);
            this.txt_sname.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(315, 67);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(86, 14);
            this.label24.TabIndex = 1082;
            this.label24.Text = "اسم المرسل:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(602, 94);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(89, 14);
            this.label25.TabIndex = 1084;
            this.label25.Text = "مبلغ الحوالـــة:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(604, 67);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(87, 14);
            this.label26.TabIndex = 1086;
            this.label26.Text = "تاريخ الانشــاء:";
            // 
            // Txt_Nrec_date
            // 
            this.Txt_Nrec_date.Checked = false;
            this.Txt_Nrec_date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Nrec_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Nrec_date.Location = new System.Drawing.Point(690, 64);
            this.Txt_Nrec_date.Name = "Txt_Nrec_date";
            this.Txt_Nrec_date.ShowCheckBox = true;
            this.Txt_Nrec_date.Size = new System.Drawing.Size(110, 20);
            this.Txt_Nrec_date.TabIndex = 1089;
            this.Txt_Nrec_date.ValueChanged += new System.EventHandler(this.Txt_Nrec_date_ValueChanged);
            // 
            // Cmb_oper_type
            // 
            this.Cmb_oper_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_oper_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_oper_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_oper_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_oper_type.FormattingEnabled = true;
            this.Cmb_oper_type.Items.AddRange(new object[] {
            "حوالـة صادرة",
            "حوالــة واردة"});
            this.Cmb_oper_type.Location = new System.Drawing.Point(401, 91);
            this.Cmb_oper_type.Name = "Cmb_oper_type";
            this.Cmb_oper_type.Size = new System.Drawing.Size(202, 24);
            this.Cmb_oper_type.TabIndex = 1090;
            this.Cmb_oper_type.SelectedIndexChanged += new System.EventHandler(this.Cmb_oper_type_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(314, 95);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(87, 14);
            this.label29.TabIndex = 1091;
            this.label29.Text = "نوع العمليــــة:";
            // 
            // Txt_S_details_job
            // 
            this.Txt_S_details_job.BackColor = System.Drawing.Color.White;
            this.Txt_S_details_job.Location = new System.Drawing.Point(348, 404);
            this.Txt_S_details_job.MaxLength = 99;
            this.Txt_S_details_job.Multiline = true;
            this.Txt_S_details_job.Name = "Txt_S_details_job";
            this.Txt_S_details_job.ReadOnly = true;
            this.Txt_S_details_job.Size = new System.Drawing.Size(283, 25);
            this.Txt_S_details_job.TabIndex = 1149;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(326, 410);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(16, 20);
            this.label45.TabIndex = 1148;
            this.label45.Text = "/";
            // 
            // Txt_R_details_job
            // 
            this.Txt_R_details_job.BackColor = System.Drawing.Color.White;
            this.Txt_R_details_job.Location = new System.Drawing.Point(348, 551);
            this.Txt_R_details_job.MaxLength = 99;
            this.Txt_R_details_job.Multiline = true;
            this.Txt_R_details_job.Name = "Txt_R_details_job";
            this.Txt_R_details_job.ReadOnly = true;
            this.Txt_R_details_job.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_details_job.Size = new System.Drawing.Size(283, 25);
            this.Txt_R_details_job.TabIndex = 1153;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(328, 553);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(16, 20);
            this.label46.TabIndex = 1152;
            this.label46.Text = "/";
            // 
            // txt_purpose_rec
            // 
            this.txt_purpose_rec.BackColor = System.Drawing.Color.White;
            this.txt_purpose_rec.Location = new System.Drawing.Point(710, 523);
            this.txt_purpose_rec.Multiline = true;
            this.txt_purpose_rec.Name = "txt_purpose_rec";
            this.txt_purpose_rec.ReadOnly = true;
            this.txt_purpose_rec.Size = new System.Drawing.Size(223, 25);
            this.txt_purpose_rec.TabIndex = 1154;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(633, 527);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(77, 16);
            this.label44.TabIndex = 1155;
            this.label44.Text = "غرض التحويل:";
            // 
            // Txt_R_Relation
            // 
            this.Txt_R_Relation.BackColor = System.Drawing.Color.White;
            this.Txt_R_Relation.Location = new System.Drawing.Point(416, 523);
            this.Txt_R_Relation.MaxLength = 200;
            this.Txt_R_Relation.Multiline = true;
            this.Txt_R_Relation.Name = "Txt_R_Relation";
            this.Txt_R_Relation.ReadOnly = true;
            this.Txt_R_Relation.Size = new System.Drawing.Size(218, 25);
            this.Txt_R_Relation.TabIndex = 1156;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(327, 528);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(96, 25);
            this.label58.TabIndex = 1157;
            this.label58.Text = "علاقة مس /  مر:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(637, 555);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(73, 16);
            this.label47.TabIndex = 1159;
            this.label47.Text = "التفاصيـــــــل:";
            // 
            // Txt_R_notes
            // 
            this.Txt_R_notes.BackColor = System.Drawing.Color.White;
            this.Txt_R_notes.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_notes.Location = new System.Drawing.Point(710, 552);
            this.Txt_R_notes.MaxLength = 199;
            this.Txt_R_notes.Name = "Txt_R_notes";
            this.Txt_R_notes.ReadOnly = true;
            this.Txt_R_notes.Size = new System.Drawing.Size(219, 23);
            this.Txt_R_notes.TabIndex = 1158;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(114, 128);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel5.TabIndex = 1160;
            // 
            // Txt_ToNrec_date
            // 
            this.Txt_ToNrec_date.Checked = false;
            this.Txt_ToNrec_date.CustomFormat = "dd/mm/yyyy";
            this.Txt_ToNrec_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_ToNrec_date.Location = new System.Drawing.Point(833, 64);
            this.Txt_ToNrec_date.Name = "Txt_ToNrec_date";
            this.Txt_ToNrec_date.ShowCheckBox = true;
            this.Txt_ToNrec_date.Size = new System.Drawing.Size(110, 20);
            this.Txt_ToNrec_date.TabIndex = 1162;
            this.Txt_ToNrec_date.ValueChanged += new System.EventHandler(this.Txt_ToNrec_date_ValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(801, 67);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(32, 14);
            this.label36.TabIndex = 1161;
            this.label36.Text = "الى:";
            // 
            // Chk_Hst_Info
            // 
            this.Chk_Hst_Info.AutoSize = true;
            this.Chk_Hst_Info.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Hst_Info.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Hst_Info.Location = new System.Drawing.Point(659, 37);
            this.Chk_Hst_Info.Name = "Chk_Hst_Info";
            this.Chk_Hst_Info.Size = new System.Drawing.Size(284, 18);
            this.Chk_Hst_Info.TabIndex = 1163;
            this.Chk_Hst_Info.Text = "أظهـــــار مـــعـــلـــومات الــــحـــوالة التاريخيـــــة";
            this.Chk_Hst_Info.UseVisualStyleBackColor = true;
            // 
            // txt_ramont
            // 
            this.txt_ramont.BackColor = System.Drawing.Color.White;
            this.txt_ramont.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ramont.Location = new System.Drawing.Point(690, 90);
            this.txt_ramont.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_ramont.Name = "txt_ramont";
            this.txt_ramont.NumberDecimalDigits = 3;
            this.txt_ramont.NumberDecimalSeparator = ".";
            this.txt_ramont.NumberGroupSeparator = ",";
            this.txt_ramont.Size = new System.Drawing.Size(110, 23);
            this.txt_ramont.TabIndex = 2;
            this.txt_ramont.Text = "0.000";
            // 
            // Txt_r_birthdate
            // 
            this.Txt_r_birthdate.BackColor = System.Drawing.Color.White;
            this.Txt_r_birthdate.DateSeperator = '/';
            this.Txt_r_birthdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_birthdate.Location = new System.Drawing.Point(84, 524);
            this.Txt_r_birthdate.Mask = "0000/00/00";
            this.Txt_r_birthdate.Name = "Txt_r_birthdate";
            this.Txt_r_birthdate.PromptChar = ' ';
            this.Txt_r_birthdate.ReadOnly = true;
            this.Txt_r_birthdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_birthdate.Size = new System.Drawing.Size(145, 22);
            this.Txt_r_birthdate.TabIndex = 1068;
            this.Txt_r_birthdate.Text = "00000000";
            this.Txt_r_birthdate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_S_Birth
            // 
            this.Txt_S_Birth.BackColor = System.Drawing.Color.White;
            this.Txt_S_Birth.DateSeperator = '/';
            this.Txt_S_Birth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Birth.Location = new System.Drawing.Point(717, 351);
            this.Txt_S_Birth.Mask = "0000/00/00";
            this.Txt_S_Birth.Name = "Txt_S_Birth";
            this.Txt_S_Birth.PromptChar = ' ';
            this.Txt_S_Birth.ReadOnly = true;
            this.Txt_S_Birth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_Birth.Size = new System.Drawing.Size(128, 22);
            this.Txt_S_Birth.TabIndex = 1036;
            this.Txt_S_Birth.Text = "00000000";
            this.Txt_S_Birth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_S_doc_Date
            // 
            this.Txt_S_doc_Date.BackColor = System.Drawing.Color.White;
            this.Txt_S_doc_Date.DateSeperator = '/';
            this.Txt_S_doc_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_doc_Date.Location = new System.Drawing.Point(717, 378);
            this.Txt_S_doc_Date.Mask = "0000/00/00";
            this.Txt_S_doc_Date.Name = "Txt_S_doc_Date";
            this.Txt_S_doc_Date.PromptChar = ' ';
            this.Txt_S_doc_Date.ReadOnly = true;
            this.Txt_S_doc_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_doc_Date.Size = new System.Drawing.Size(128, 22);
            this.Txt_S_doc_Date.TabIndex = 1008;
            this.Txt_S_doc_Date.Text = "00000000";
            this.Txt_S_doc_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // View_Remittence_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 620);
            this.Controls.Add(this.Chk_Hst_Info);
            this.Controls.Add(this.Txt_ToNrec_date);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.Txt_R_notes);
            this.Controls.Add(this.Txt_R_Relation);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.txt_purpose_rec);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.Txt_R_details_job);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.Txt_S_details_job);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Cmb_oper_type);
            this.Controls.Add(this.Txt_Nrec_date);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.txt_ramont);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txt_sname);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txt_rname);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Btn_Rpt);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.Txt_Rem_No);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_r_birthdate);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_address);
            this.Controls.Add(this.Txt_r_job);
            this.Controls.Add(this.Txt_Discreption);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_S_Birth);
            this.Controls.Add(this.Txt_Relionship);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_S_doc_issue);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_S_doc_no);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtScity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_S_doc_type);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.Txt_S_doc_Date);
            this.Controls.Add(this.TxtS_address);
            this.Controls.Add(this.Txt_s_job);
            this.Controls.Add(this.Txt_Soruce_money);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Cbo_city);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Grdrec_rem);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtTerm_Name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "View_Remittence_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "510";
            this.Text = "View_Remittence_Rem";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.View_Remittence_Rem_FormClosed);
            this.Load += new System.EventHandler(this.View_Remittence_Rem_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.DataGridView Grdrec_rem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private MyDateTextBox Txt_r_birthdate;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_R_address;
        private System.Windows.Forms.TextBox Txt_r_job;
        private System.Windows.Forms.TextBox Txt_Discreption;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private MyDateTextBox Txt_S_Birth;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_S_doc_issue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtScity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_S_doc_type;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtS_phone;
        private MyDateTextBox Txt_S_doc_Date;
        private System.Windows.Forms.TextBox Txt_s_job;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Txt_Rem_No;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.ComboBox Cbo_city;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button Btn_Rpt;
        private System.Windows.Forms.TextBox txt_rname;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_sname;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Sample.DecimalTextBox txt_ramont;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker Txt_Nrec_date;
        private System.Windows.Forms.ComboBox Cmb_oper_type;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txt_S_details_job;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox Txt_R_details_job;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txt_purpose_rec;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox Txt_R_Relation;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox Txt_R_notes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DateTimePicker Txt_ToNrec_date;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox Chk_Hst_Info;
    }
}