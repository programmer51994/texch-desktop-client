﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Confirm_Remittences_atx : Form
    {
        #region Defintion
        Boolean change = false;
        BindingSource BS_Rem_atx = new BindingSource();
        BindingSource BS_Rem_Confirm = new BindingSource();
        DataTable Dt_grd = new DataTable();
        DataTable Per_blacklist_tbl = new DataTable();
        DataTable Refuse_rem_confirm = new DataTable();
        DataTable DT_final = new DataTable();

       // -------------
        string s_Ejob = "";
        string r_Ejob = "";
        string S_Social_No = "";
        string R_Social_No = "";
        string forgin_Cur="";
        string local_Cur ="";
        string _Date = "";
        string rem_no="";
        string Vo_No = "";
        double Rem_Amount = 0;
        string term = "";
        int Oper_Id = 0;

        public static int Del_Btn = 0;
        #endregion
        public Confirm_Remittences_atx()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Rem_atx.AutoGenerateColumns = false;
            Create_Table();
        }

        private void Create_Table()
        {
            string[] Column4 = { "Per_AName", "Per_EName" };
            string[] DType4 = { "System.String", "System.String" };

            Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);
        }

 
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
           
            if (change)
            {

                BS_Rem_atx = new BindingSource();
                string[] Str = { "Search_Remittences_atx_tbl" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }
            }

            Int64 code_rem = Txt_Rem_Code.Text.Trim() == "" ? 0 : Convert.ToInt64(Txt_Rem_Code.Text.Trim().ToString());
            connection.SqlExec("exec Search_Remittences_atx "  +  code_rem , "Search_Remittences_atx_tbl");

           
            try
            {
                
                    if (connection.SQLDS.Tables["Search_Remittences_atx_tbl"].Rows.Count > 0)
                    {  
                       
                       
                        BS_Rem_atx.DataSource = connection.SQLDS.Tables["Search_Remittences_atx_tbl"];
                        Grd_Rem_atx.DataSource = BS_Rem_atx;

                        Txt_purpose.DataBindings.Clear();
                        Txt_details.DataBindings.Clear();
                        Txt_sender.DataBindings.Clear();
                        Txt_S_nat.DataBindings.Clear();
                        Txt_s_phone.DataBindings.Clear();
                        Txt_S_cit.DataBindings.Clear();
                        Txt_doc.DataBindings.Clear();
                        Txt_doc_no.DataBindings.Clear();
                        Txt_date.DataBindings.Clear();
                        Txt_sdate_exp.DataBindings.Clear();
                        Txt_oc_issue.DataBindings.Clear();
                        Txt_birth_date.DataBindings.Clear();
                        Txt_s_address.DataBindings.Clear();
                        Txt_r_name.DataBindings.Clear();
                        Txt_R_nat.DataBindings.Clear();
                        Txt_r_phone.DataBindings.Clear();
                        Txt_r_cit.DataBindings.Clear();
                        txt_r_doc.DataBindings.Clear();
                        txt_r_doc_no.DataBindings.Clear();
                        txt_r_doc_date.DataBindings.Clear();
                        Txt_rdate_exp.DataBindings.Clear();
                        txt_r_doc_issue.DataBindings.Clear();
                        txt_r_birth_date.DataBindings.Clear();
                        txt_r_address.DataBindings.Clear();
                        Txt_purpose.DataBindings.Add("Text", BS_Rem_atx, "T_purpose");
                        Txt_details.DataBindings.Add("Text", BS_Rem_atx, "s_notes");
                        Txt_sender.DataBindings.Add("Text", BS_Rem_atx, "S_name");
                        Txt_S_nat.DataBindings.Add("Text", BS_Rem_atx, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                        Txt_s_phone.DataBindings.Add("Text", BS_Rem_atx, "S_phone");
                        Txt_S_cit.DataBindings.Add("Text", BS_Rem_atx, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                        Txt_doc.DataBindings.Add("Text", BS_Rem_atx, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                        Txt_doc_no.DataBindings.Add("Text", BS_Rem_atx, "S_doc_no");
                        Txt_date.DataBindings.Add("Text", BS_Rem_atx, "S_doc_ida");
                        Txt_sdate_exp.DataBindings.Add("Text", BS_Rem_atx, "S_doc_eda");
                        Txt_oc_issue.DataBindings.Add("Text", BS_Rem_atx, "S_doc_issue");
                        Txt_birth_date.DataBindings.Add("Text", BS_Rem_atx, "sbirth_date");
                        Txt_s_address.DataBindings.Add("Text", BS_Rem_atx, "S_address");
                        Txt_r_name.DataBindings.Add("Text", BS_Rem_atx, "r_name");
                        Txt_R_nat.DataBindings.Add("Text", BS_Rem_atx, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                        Txt_r_phone.DataBindings.Add("Text", BS_Rem_atx, "R_phone");
                        Txt_r_cit.DataBindings.Add("Text", BS_Rem_atx, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                        txt_r_doc.DataBindings.Add("Text", BS_Rem_atx, connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA");
                        txt_r_doc_no.DataBindings.Add("Text", BS_Rem_atx, "r_doc_no");
                        txt_r_doc_date.DataBindings.Add("Text", BS_Rem_atx, "r_doc_ida");
                        Txt_rdate_exp.DataBindings.Add("Text", BS_Rem_atx, "r_doc_eda");
                        txt_r_doc_issue.DataBindings.Add("Text", BS_Rem_atx, "r_doc_issue");
                        txt_r_birth_date.DataBindings.Add("Text", BS_Rem_atx, "rbirth_date");
                        txt_r_address.DataBindings.Add("Text", BS_Rem_atx, "r_address");
                        change = true;

                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات تحقق الشروط" : "no remmitance confirm this conditions", MyGeneral_Lib.LblCap);
                        clear_all();
                        change = true;
                        return;
                    }

                }

            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "catch" : "no remmitance confirm this conditions", MyGeneral_Lib.LblCap);
                clear_all();
                change = true;
                return;
            }
           


        }


        //----------------------------------------------
        private void clear_all()
        {
            BS_Rem_atx = new BindingSource();
            Grd_Rem_atx.DataSource = BS_Rem_atx;
            Txt_purpose.Text = "";
            Txt_details.Text = "";
            Txt_sender.Text = "";
            Txt_S_nat.Text = "";
            Txt_s_phone.Text = "";
            Txt_S_cit.Text = "";
            Txt_doc.Text = "";
            Txt_doc_no.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_date.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_oc_issue.Text = "";
            Txt_birth_date.Text = "";
            Txt_s_address.Text = "";
            Txt_r_name.Text = "";
            Txt_R_nat.Text = "";
            Txt_r_phone.Text = "";
            Txt_r_cit.Text = "";
            txt_r_doc.Text = "";
            txt_r_doc_no.Text = "";
            txt_r_doc_date.Text = "";
            Txt_rdate_exp.Text = "";
            txt_r_doc_issue.Text = "";
            txt_r_birth_date.Text = "";
            txt_r_address.Text = "";
         
        }


        //---------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            if (Grd_Rem_atx.RowCount > 0)
            {

                try
                {

                    DT_final = connection.SQLDS.Tables["Search_Remittences_atx_tbl"].DefaultView.ToTable().Select("Check_id > 0").CopyToDataTable();
                    BS_Rem_Confirm.DataSource = DT_final;
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الحوالة  " : "Choose the Rimmetance Please", MyGeneral_Lib.LblCap);
                    return;
                }

                if (DT_final.Rows.Count > 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار حوالة واحدة فقط" : "Choose five Rimmetance only Please", MyGeneral_Lib.LblCap);
                    return;
                }


                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? " هل تريد التأكيد فعلا" : "Do you want to confirm", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {


                    DateTime S_doc_ida_datatime = Convert.ToDateTime(((DataRowView)BS_Rem_Confirm.Current).Row["S_doc_ida"]);
                    string S_doc_ida = S_doc_ida_datatime.ToString("yyyy/MM/dd");

                    DateTime S_doc_eda_datatime = Convert.ToDateTime(((DataRowView)BS_Rem_Confirm.Current).Row["S_doc_eda"]);
                    string S_doc_eda = S_doc_eda_datatime.ToString("yyyy/MM/dd");

                    DateTime sbirth_date_datatime = Convert.ToDateTime(((DataRowView)BS_Rem_Confirm.Current).Row["sbirth_date"]);
                    string sbirth_date = sbirth_date_datatime.ToString("yyyy/MM/dd");

                    ArrayList ItemList1 = new ArrayList();
                    ItemList1.Insert(0, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["R_Cur_Id"]));
                    ItemList1.Insert(1, Convert.ToDecimal(((DataRowView)BS_Rem_Confirm.Current).Row["R_amount"]));
                    ItemList1.Insert(2, ((DataRowView)BS_Rem_Confirm.Current).Row["S_name"]);
                    ItemList1.Insert(3, ((DataRowView)BS_Rem_Confirm.Current).Row["S_phone"]);
                    ItemList1.Insert(4, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["S_city_id"]));
                    ItemList1.Insert(5, ((DataRowView)BS_Rem_Confirm.Current).Row["S_address"]);
                    ItemList1.Insert(6, ((DataRowView)BS_Rem_Confirm.Current).Row["S_doc_no"]);
                    ItemList1.Insert(7, S_doc_ida);
                    ItemList1.Insert(8, S_doc_eda);
                    ItemList1.Insert(9, ((DataRowView)BS_Rem_Confirm.Current).Row["S_doc_issue"]);
                    ItemList1.Insert(10, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["sFrm_doc_id"]));//----@sfrm_doc_id
                    ItemList1.Insert(11, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["snat_Id"]));//-----@snat_id
                    ItemList1.Insert(12, ((DataRowView)BS_Rem_Confirm.Current).Row["r_name"]);
                    ItemList1.Insert(13, ((DataRowView)BS_Rem_Confirm.Current).Row["R_phone"]);
                    ItemList1.Insert(14, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["r_city_id"]));//----@r_city_id;   binding_cbo_rcity_con 
                    ItemList1.Insert(15, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["t_city_id"]));//-----@T_city_id
                    ItemList1.Insert(16, ((DataRowView)BS_Rem_Confirm.Current).Row["r_address"]);
                    ItemList1.Insert(17, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["rnat_Id"]));//----@rnat_id  binding_cb_rnat
                    ItemList1.Insert(18, ((DataRowView)BS_Rem_Confirm.Current).Row["T_purpose"]);
                    ItemList1.Insert(19, ((DataRowView)BS_Rem_Confirm.Current).Row["s_notes"]);
                    ItemList1.Insert(20, Convert.ToInt32(((DataRowView)BS_Rem_Confirm.Current).Row["nrec_date"]));//txt_Date.Tag                 
                    ItemList1.Insert(21, 0);//@r_ocomm
                    ItemList1.Insert(22, 0);//@r_ccur_id
                    ItemList1.Insert(23, ""); // mv_notes.Trim()
                    ItemList1.Insert(24, sbirth_date);// Sbirth_Date);
                    ItemList1.Insert(25, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["PR_cur_id"]));//----@Pr_cur_id-----//////////!!!
                    ItemList1.Insert(26, connection.user_id);//login_id
                    ItemList1.Insert(27, connection.Cust_online_Id);//cust_id_online     
                    ItemList1.Insert(28, 0);// @rem_amount_exch----cur_comm
                    ItemList1.Insert(29, 0);// @com_amount_exch----cur_comm
                    ItemList1.Insert(30, 0);//Param_cur_Rem);//-----@cur_amount_exch  binding_cur_bsrem
                    ItemList1.Insert(31, 0);//Param_cur_Com);
                    ItemList1.Insert(32, 0);//Param_ExchageLoc_rem);
                    ItemList1.Insert(33, ((DataRowView)BS_Rem_Confirm.Current).Row["SBirth_Place"]);
                    ItemList1.Insert(34, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["sa_city_id"]));//------@sa_city_id  binding_cbo_scity_con
                    ItemList1.Insert(35, ((DataRowView)BS_Rem_Confirm.Current).Row["s_job_ID"]);
                    ItemList1.Insert(36, connection.Lang_id);
                    ItemList1.Insert(37, ((DataRowView)BS_Rem_Confirm.Current).Row["Source_money"]);
                    ItemList1.Insert(38, ((DataRowView)BS_Rem_Confirm.Current).Row["Relation_S_R"]);
                    ItemList1.Insert(39, "");//@r_job
                    ItemList1.Insert(40, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["Case_purpose_id"]));
                    ItemList1.Insert(41, ((DataRowView)BS_Rem_Confirm.Current).Row["R_ACUR_NAME"]);//----@R_ACUR_NAME              
                    ItemList1.Insert(42, ((DataRowView)BS_Rem_Confirm.Current).Row["R_ECUR_NAME"]);//----@R_ECUR_NAME              
                    ItemList1.Insert(43, ((DataRowView)BS_Rem_Confirm.Current).Row["PR_ACUR_NAME"]);//---@PR_ACUR_NAME  "ACur_Name_pay" : "eCur_Name_pay";             
                    ItemList1.Insert(44, ((DataRowView)BS_Rem_Confirm.Current).Row["PR_ECUR_NAME"]);//----PR_eCUR_NAME               
                    ItemList1.Insert(45, ((DataRowView)BS_Rem_Confirm.Current).Row["S_ACITY_NAME"]);//----@S_ACITY_NAME   مدينة الاصدار  "acity_name" : "ecity_name";               
                    ItemList1.Insert(46, ((DataRowView)BS_Rem_Confirm.Current).Row["S_ECITY_NAME"]);//----@S_eCITY_NAME               
                    ItemList1.Insert(47, ((DataRowView)BS_Rem_Confirm.Current).Row["S_ACOUN_NAME"]);//----@@S_ACOUN_NAME               
                    ItemList1.Insert(48, ((DataRowView)BS_Rem_Confirm.Current).Row["S_ECOUN_NAME"]);//----@@S_ECOUN_NAME
                    ItemList1.Insert(49, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Street"]);//---@S_Street
                    ItemList1.Insert(50, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Suburb"]);//--@S_Suburb
                    ItemList1.Insert(51, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Post_Code"]);//----@S_Post_Code
                    ItemList1.Insert(52, ((DataRowView)BS_Rem_Confirm.Current).Row["S_State"]);//----@S_State
                    ItemList1.Insert(53, ((DataRowView)BS_Rem_Confirm.Current).Row["sFRM_ADOC_NA"]);//---@sFRM_ADOC_NA  binding_sfmd
                    ItemList1.Insert(54, ((DataRowView)BS_Rem_Confirm.Current).Row["sFRM_eDOC_NA"]);//---@sFRM_eDOC_NA  binding_sfmd
                    ItemList1.Insert(55, ((DataRowView)BS_Rem_Confirm.Current).Row["s_A_NAT_NAME"]);//---@@s_A_NAT_NAME  binding_sfmd
                    ItemList1.Insert(56, ((DataRowView)BS_Rem_Confirm.Current).Row["s_e_NAT_NAME"]);//---@@s_e_NAT_NAME  binding_sfmd
                    ItemList1.Insert(57, ((DataRowView)BS_Rem_Confirm.Current).Row["r_ACITY_NAME"]);//---@r_ACITY_NAME 
                    ItemList1.Insert(58, ((DataRowView)BS_Rem_Confirm.Current).Row["r_ECITY_NAME"]);//---@r_ECITY_NAME
                    ItemList1.Insert(59, ((DataRowView)BS_Rem_Confirm.Current).Row["r_ACOUN_NAME"]);//---@r_ACOUN_NAME
                    ItemList1.Insert(60, ((DataRowView)BS_Rem_Confirm.Current).Row["r_eCOUN_NAME"]);//---@r_eCOUN_NAME
                    ItemList1.Insert(61, ((DataRowView)BS_Rem_Confirm.Current).Row["r_Street"]);//---@r_Street
                    ItemList1.Insert(62, ((DataRowView)BS_Rem_Confirm.Current).Row["r_Suburb"]);//---@r_Suburb
                    ItemList1.Insert(63, ((DataRowView)BS_Rem_Confirm.Current).Row["r_Post_Code"]);//---@@r_Post_Code
                    ItemList1.Insert(64, ((DataRowView)BS_Rem_Confirm.Current).Row["r_State"]);//---@r_State
                    ItemList1.Insert(65, "");//---@rFRM_ADOC_NA
                    ItemList1.Insert(66, "");//---@rFRM_EDOC_NA
                    ItemList1.Insert(67, ((DataRowView)BS_Rem_Confirm.Current).Row["r_A_NAT_NAME"]);//---@@r_A_NAT_NAME
                    ItemList1.Insert(68, ((DataRowView)BS_Rem_Confirm.Current).Row["r_E_NAT_NAME"]);//---@@r_E_NAT_NAME
                    ItemList1.Insert(69, "101");//----@ACASE_NA  binding_Cmb_Case_Purpose
                    ItemList1.Insert(70, "101");//----@eCASE_NA  binding_Cmb_Case_Purpose
                    ItemList1.Insert(71, "");//---@@r_CCur_ACUR_NAME
                    ItemList1.Insert(72, "");//---@@r_CCur_ECUR_NAME
                    ItemList1.Insert(73, ((DataRowView)BS_Rem_Confirm.Current).Row["t_ACITY_NAME"]);//-----@@t_ACITY_NAME   binding_tcity_id
                    ItemList1.Insert(74, ((DataRowView)BS_Rem_Confirm.Current).Row["t_eCITY_NAME"]);//-----@@t_eCITY_NAME   binding_tcity_id
                    ItemList1.Insert(75, ((DataRowView)BS_Rem_Confirm.Current).Row["Sa_ACITY_NAME"]);//-----@@@Sa_ACITY_NAME   binding_cbo_scity_con
                    ItemList1.Insert(76, ((DataRowView)BS_Rem_Confirm.Current).Row["Sa_ECITY_NAME"]);//-----@@@Sa_ECITY_NAME   binding_cbo_scity_con
                    ItemList1.Insert(77, ((DataRowView)BS_Rem_Confirm.Current).Row["Sa_ACOUN_NAME"]);//-----@Sa_ACOUN_NAME   binding_cbo_scity_con
                    ItemList1.Insert(78, ((DataRowView)BS_Rem_Confirm.Current).Row["Sa_ECOUN_NAME"]);//-----@Sa_ECOUN_NAME   binding_cbo_scity_con
                    ItemList1.Insert(79, ((DataRowView)BS_Rem_Confirm.Current).Row["t_ACOUN_NAME"]);//-----@t_ACOUN_NAME   binding_tcity_id
                    ItemList1.Insert(80, ((DataRowView)BS_Rem_Confirm.Current).Row["t_ECOUN_NAME"]);//-----@t_ECOUN_NAME   binding_tcity_id
                    ItemList1.Insert(81, ((DataRowView)BS_Rem_Confirm.Current).Row["Case_purpose_Aname"]);//----@@Case_purpose_Aname  binding_Cmb_Case_Purpose
                    ItemList1.Insert(82, ((DataRowView)BS_Rem_Confirm.Current).Row["Case_purpose_ename"]);//----@@Case_purpose_ename  binding_Cmb_Case_Purpose
                    ItemList1.Insert(83, Convert.ToString(((DataRowView)BS_Rem_Confirm.Current).Row["Sender_Code_phone"]));//----@Sender_Code_phone
                    ItemList1.Insert(84, Convert.ToString(((DataRowView)BS_Rem_Confirm.Current).Row["Receiver_Code_phone"]));//----@@Receiver_Code_phone
                    ItemList1.Insert(85, connection.User_Name);
                    ItemList1.Insert(86, connection.T_ID);
                    ItemList1.Insert(87, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["sub_cust_id"]));//@sub_cust_id
                    ItemList1.Insert(88, 1);//  @Catg_Id      
                    ItemList1.Insert(89, 0);//@comm_type
                    ItemList1.Insert(90, 0);//((DataRowView)BS_Rem_Confirm.Current).Row["S_per_id"]);//----@S_per_id
                    ItemList1.Insert(91, 0);//((DataRowView)BS_Rem_Confirm.Current).Row["R_per_id"]);//----@R_per_id
                    ItemList1.Insert(92, "");//Txt_another_Sen.Text.Trim());
                    ItemList1.Insert(93, "");//Txt_another_Rec.Text.Trim());
                    ItemList1.Insert(94, ((DataRowView)BS_Rem_Confirm.Current).Row["s_Email"]);// Txt_mail.Text.Trim());
                    ItemList1.Insert(95, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Mother_name"]);//txt_Mother_name.Text.Trim());
                    ItemList1.Insert(96, 0);// cmb_Gender_id.SelectedValue);
                    ItemList1.Insert(97, connection.user_id);
                    ItemList1.Insert(98, Convert.ToInt32(((DataRowView)BS_Rem_Confirm.Current).Row["local_international"])); //Cbo_rem_path.SelectedIndex);
                    ItemList1.Insert(99, "");//@Param_Result
                    ItemList1.Insert(100, "");//@Param_Result_rem_no
                    ItemList1.Insert(101, 0); //---VO_No
                    ItemList1.Insert(102, 0); //---Code_rem
                    ItemList1.Insert(103, ""); //---Case_Date  
                    ItemList1.Insert(104, "");//@Param_Result_err
                    ItemList1.Insert(105, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Gender_Aname"]);
                    ItemList1.Insert(106, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Gender_Ename"]);
                    ItemList1.Insert(107, 11);//@rem_flag
                    ItemList1.Insert(108, 0);// agent_id 
                    ItemList1.Insert(109, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["s_Cur_Id"]));
                    ItemList1.Insert(110, Convert.ToDecimal(((DataRowView)BS_Rem_Confirm.Current).Row["s_ocomm"]));
                    ItemList1.Insert(111, ((DataRowView)BS_Rem_Confirm.Current).Row["s_ACUR_NAME"]);
                    ItemList1.Insert(112, ((DataRowView)BS_Rem_Confirm.Current).Row["s_ECUR_NAME"]);
                    ItemList1.Insert(113, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["cust_broker_id"]));
                    ItemList1.Insert(114, ((DataRowView)BS_Rem_Confirm.Current).Row["cust_broker_aname"]);
                    ItemList1.Insert(115, ((DataRowView)BS_Rem_Confirm.Current).Row["cust_broker_ename"]);
                    ItemList1.Insert(116, 0);////@comm_cur_paid
                    ItemList1.Insert(117, 0);////@comm_amount_paid
                    ItemList1.Insert(118, "");////@i_ACUR_NAME
                    ItemList1.Insert(119, "");////@i_ECUR_NAME
                    ItemList1.Insert(120, 0);////@cust_paid_id
                    ItemList1.Insert(121, "");////@cust_paid_aname
                    ItemList1.Insert(122, "");////@cust_paid_ename
                    ItemList1.Insert(123, 0);////@paid_comm_type
                    ItemList1.Insert(124, 0);////@buy_sal_flag
                    ItemList1.Insert(125, 0);//Param_ExchageLoc_rem);
                    ItemList1.Insert(126, 0);//Param_ExchageLoc_Com);
                    ItemList1.Insert(127, Convert.ToDecimal(((DataRowView)BS_Rem_Confirm.Current).Row["tot_comm"]));
                    ItemList1.Insert(128, 0);//checkBox1.Checked == true || checkBox2.Checked == true ? param_Exch_rate_rem : 0);
                    ItemList1.Insert(129, 0);//checkBox1.Checked == true || checkBox2.Checked == true ? param_Exch_rate_com : 0);
                    ItemList1.Insert(130, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["resd_flag"]));
                    ItemList1.Insert(131, connection.Lang_id);// btnlang);
                    ItemList1.Insert(132, connection.Lang_id);// btnlang_rec);
                    ItemList1.Insert(133, ((DataRowView)BS_Rem_Confirm.Current).Row["S_Social_No"]);
                    ItemList1.Insert(134, 0);////@chk_tran_flag
                    ItemList1.Insert(135, ((DataRowView)BS_Rem_Confirm.Current).Row["s_details_job"]);
                    ItemList1.Insert(136, "");//Convert.ToInt16(cmb_comm_type.SelectedValue));
                    ItemList1.Insert(137, Convert.ToDecimal(((DataRowView)BS_Rem_Confirm.Current).Row["s_Acomm"]));
                    ItemList1.Insert(138, Convert.ToInt16(((DataRowView)BS_Rem_Confirm.Current).Row["Comm_Rem_ID"]));
                    ItemList1.Insert(139, Rem_Blacklist_confirm_ok.Notes_BL);// 
                    ItemList1.Insert(140, 0);// نظام ويندوز
                    ItemList1.Insert(141, Convert.ToInt64(((DataRowView)BS_Rem_Confirm.Current).Row["Code_Rem"]));
                    ItemList1.Insert(142, 0);
                    ItemList1.Insert(143, 0);
                    ItemList1.Insert(144, 0);
                    ItemList1.Insert(145, "");
                    ItemList1.Insert(146, "");
                    ItemList1.Insert(147, 0);

                    MyGeneral_Lib.Copytocliptext("Add_rem_out_Voucher_daily", ItemList1);
                    connection.scalar("Add_rem_out_Voucher_daily", ItemList1);

                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);//
                        connection.SQLCMD.Parameters.Clear();
                        return;

                    }
                    if (connection.SQLCMD.Parameters["@Param_Result_err"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result_err"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        return;
                    }

                    if (connection.SQLCMD.Parameters["@Param_Result_rem_no"].Value.ToString() != "")
                    {
                        rem_no = connection.SQLCMD.Parameters["@Param_Result_rem_no"].Value.ToString();
                        MessageBox.Show((connection.Lang_id == 1 ? "تم تأكيد الحوالة بنجاح " : "Choose the Rimmetance Please") + connection.SQLCMD.Parameters["@Param_Result_rem_no"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        print_Rpt_Pdf_NEW();
                        Btn_Browser_Click(null, null);
                        return;
                    }

                    connection.SQLCMD.Parameters.Clear();

                }
            }

            else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }
        }
        //----------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //----------------------------------------------
        private void clear_Tables()
        {
            BS_Rem_atx = new BindingSource();
            string[] Str = { "Search_Remittences_atx_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
        }

        private void Rem_Confirmation_FormClosed(object sender, FormClosedEventArgs e)
        {
            Del_Btn = 0;
            string[] comm_Tbl = { "Oper_TBL", "Search_Remittences_atx_tbl", "Rem_Confirmation_tbl", "Rem_Confirmation_tbl1", "Rem_Confirmation_tbl2" };

            foreach (string Tbl in comm_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }


 

        private void label77_Click(object sender, EventArgs e)
        {
            if (Grd_Rem_atx.RowCount > 0)
            {
                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.CommandTimeout = 0;
                    connection.SQLCMD.Parameters.AddWithValue("@name", ((DataRowView)BS_Rem_atx.Current).Row["s_name"].ToString() );
                    connection.SQLCMD.Parameters.AddWithValue("@S_doc_no",  ((DataRowView)BS_Rem_atx.Current).Row["S_doc_no"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@S_Social_No",   ((DataRowView)BS_Rem_atx.Current).Row["S_Social_No"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", Convert.ToInt32(((DataRowView)BS_Rem_atx.Current).Row["sFrm_doc_id"]));
                    connection.SQLCMD.Parameters.AddWithValue("@r_doc_no",  "");//((DataRowView)BS_Rem_atx.Current).Row["r_doc_no"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", "");// ((DataRowView)BS_Rem_atx.Current).Row["r_Social_No"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", 0);// Convert.ToInt32(((DataRowView)BS_Rem_atx.Current).Row["rFrm_doc_id"]));
                    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 2 );
                    connection.SQLCMD.Parameters.AddWithValue("@sbirth_date",   ((DataRowView)BS_Rem_atx.Current).Row["sbirth_date"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@rbirth_date",  "");//((DataRowView)BS_Rem_atx.Current).Row["rbirth_date"].ToString());


                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch
                {

                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }


                if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
                {

                    Rem_Count AddFrm = new Rem_Count(1);
                    AddFrm.ShowDialog(this);
                }
                else
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                    return;
                }
            }

            else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }
        }

        private void label80_Click(object sender, EventArgs e)
        {
            try
            {

                if (Grd_Rem_atx.RowCount > 0)
                {

                    label80.Enabled = false;
                    Per_blacklist_tbl.Rows.Clear();
                    Per_blacklist_tbl.Rows.Add(((DataRowView)BS_Rem_atx.Current).Row["S_Name"].ToString(), ((DataRowView)BS_Rem_atx.Current).Row["S_Ename"].ToString()); 


                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        label80.Enabled = true;
                    }
                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        label80.Enabled = true;
                    }

                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
                    {
                        Rem_Blacklist AddFrm = new Rem_Blacklist();
                        AddFrm.ShowDialog(this);
                    }
                    else
                    {
                        label80.Enabled = true;
                        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                        return;

                    }

                }
            }
            catch
            {
                label80.Enabled = true;
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
        }

        private void GrdRem_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(BS_Rem_atx);
        }
        private void print_Rpt_Pdf_NEW()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int int_nrecdate = 0;
            string Frm_id = "";
            string User = TxtUser.Text;
            string phone = "";
            int procker = 0;
            string prockr_name = "";
            string procker_ename = "";
            int Paid = 0;
            string Paid_name = "";
            string Paid_ename = "";
            string local_Cur1 = "";
            string local_ECur1 = "";
            string forgin_Cur1 = "";
            string forgin_ECur1 = "";
            string Com_ToWord = "";
            string Com_ToEWord = "";
            double com_amount = 0;
            string com_Cur_code = "";
            byte chk_s_ocomm = 0;
            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();

            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();

            //if (Convert.ToInt16(checkBox1.Checked) == 1) { Exch_rate_com = Txt_ExRate_Rem.Text; }
            //if (Convert.ToInt16(checkBox2.Checked) == 1) { Exch_rate_com = Txt_ExRate_Rem2.Text; }
              _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);


            string SqlTxt = "Exec Report_Rem_Information_new" + "'" + rem_no + "'," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Rem_Info");



            if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0)
            {
                procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
                prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
                procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
            }
            if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]) != 0)
            {
                Paid = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]);
                Paid_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_aname"].ToString();
                Paid_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_ename"].ToString();
            }

            if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)
            {
                procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]);
                prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_Aname"].ToString();
                procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_ename"].ToString();
            }


            local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
            local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

            forgin_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Cur_Code"].ToString();
            forgin_Cur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ACUR_NAME"].ToString();
            forgin_ECur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ECUR_NAME"].ToString();

             s_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_Ejob"].ToString();
             r_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_Ejob"].ToString();
             S_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["S_Social_No"].ToString();
             R_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Social_No"].ToString();


            Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();

            if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
            {
                Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                {
                    com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]);
                    com_Cur_code = connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_code"].ToString();
                    ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                    Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                }
            }
            else// عملة محلية
            {
                Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                {
                    com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]);
                    com_Cur_code = local_Cur;
                    ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                }
            }

            Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();


            term = TxtTerm_Name.Text;

            Oper_Id = Convert.ToInt16(connection.SQLDS.Tables["Rem_Info"].Rows[0]["oper_id"]);
            decimal txt_comm_amount = 0;

          
             chk_s_ocomm = 0;
             txt_comm_amount = 0;
            
            try
            {

                Add_Rem_cash_daily_rpt_1 ObjRpt = new Add_Rem_cash_daily_rpt_1();
                Add_Rem_cash_daily_rpt_eng_1 ObjRptEng = new Add_Rem_cash_daily_rpt_eng_1();


                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("local_Cur", local_Cur);
                Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                Dt_Param.Rows.Add("User", User);
                Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                Dt_Param.Rows.Add("term", term);
                Dt_Param.Rows.Add("Frm_id", Frm_id);
                Dt_Param.Rows.Add("phone", phone);
                Dt_Param.Rows.Add("com_cur", com_Cur_code);
                Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                Dt_Param.Rows.Add("procker", procker);
                Dt_Param.Rows.Add("procker_ename", procker_ename);
                Dt_Param.Rows.Add("prockr_name", prockr_name);
                Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                Dt_Param.Rows.Add("Anote_report", Anote_report);
                Dt_Param.Rows.Add("Enote_report", Enote_report);
                Dt_Param.Rows.Add("chk_s_ocomm", chk_s_ocomm);
                Dt_Param.Rows.Add("txt_comm_amount", txt_comm_amount);
                Dt_Param.Rows.Add("Paid", Paid);
                Dt_Param.Rows.Add("Paid_ename", Paid_ename);
                Dt_Param.Rows.Add("Paid_name", Paid_name);


                connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
                connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);

                this.Visible = true;
                connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);

            }
            catch
            { }

        }

        private void Confirm_Remittences_atx_Load(object sender, EventArgs e)
        {
            string sql_report_txt = "select * from Reports_setting_Tbl";
            connection.SqlExec(sql_report_txt, "Reports_setting_Tbl");
            if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار اعدادات الطباعة" : "please add reporting setting", MyGeneral_Lib.LblCap);
                this.Close();
                return;
            }
        }
    }
}