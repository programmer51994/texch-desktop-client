﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Document_person_view : Form
    {
        BindingSource BS_Document_person_view = new BindingSource();
        public static Int16 Del_Btn = 0;

        //-----------------------
        public Document_person_view()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Grd_Document_person_view.AutoGenerateColumns = false;
            btn_cust_browser.Text = "Browse";

        }
        private void Document_person_view_Load(object sender, EventArgs e)
        {
            btn_cust_browser.UseColumnTextForButtonValue = true;
            BS_Document_person_view.DataSource = connection.SQLDS.Tables["Tbl_Person"];
            Grd_Document_person_view.DataSource = BS_Document_person_view;
        }

        private void Grd_Document_person_view_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(BS_Document_person_view);
        }


        private void Document_person_view_FormClosed(object sender, FormClosedEventArgs e)
        {
            Del_Btn = 0;

            //string[] Used_Tbl = { "Tbl_Person" };
            //foreach (string Tbl in Used_Tbl)
            //{
            //    if (connection.SQLDS.Tables.Contains(Tbl))
            //    {
            //        connection.SQLDS.Tables.Remove(Tbl);
            //    }
            //}
        }

        private void Grd_Document_person_view_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)//  الوثائق
            {
                btn_cust_browser_Click(sender, e);
            }
        }

        private void btn_cust_browser_Click(object sender, EventArgs e)
        {
            int Per_id = 0;
            string Per_AName = "";
            string Per_EName = "";

            Per_id = Convert.ToInt32(((DataRowView)BS_Document_person_view.Current).Row["per_id"]);
            Per_AName = ((DataRowView)BS_Document_person_view.Current).Row["per_aname"].ToString();
            Per_EName = ((DataRowView)BS_Document_person_view.Current).Row["per_ename"].ToString();

            Del_Btn = 1;
            Person_Document_main UpdFrm = new Person_Document_main(Per_AName, Per_id, Per_EName);
           // this.Visible = false;
            UpdFrm.ShowDialog(this);
          //  this.Visible = true;

        }

     
    }
}