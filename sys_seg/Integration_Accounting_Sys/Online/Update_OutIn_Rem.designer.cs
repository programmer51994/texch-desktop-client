﻿namespace Integration_Accounting_Sys
{
    partial class Update_OutIn_Rem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Rem_no = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.TxT_Agent_Real_Cur = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.TxT_Agent_Real_Comm_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.TxT_Agent_Real_Rem_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.TxT_Agent_Rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.cmb_job_receiver = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.cmb_comm_type = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.Cmb_Comm_Cur = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.Cmb_PR_Cur_Id = new System.Windows.Forms.ComboBox();
            this.Cmb_R_CUR_ID = new System.Windows.Forms.ComboBox();
            this.Cbo_rem_path = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Cmb_T_City = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.Txtr_State = new System.Windows.Forms.TextBox();
            this.Txtr_Post_Code = new System.Windows.Forms.TextBox();
            this.Txtr_Street = new System.Windows.Forms.TextBox();
            this.Txtr_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_Reciever = new System.Windows.Forms.TextBox();
            this.Cmb_phone_Code_R = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.Cmb_R_Nat = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Cmb_R_City = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.Txt_Com_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txtr_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tex_reason = new System.Windows.Forms.TextBox();
            this.lab_reason = new System.Windows.Forms.Label();
            this.txt_notes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_S_details_job = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.cmb_job_sender = new System.Windows.Forms.ComboBox();
            this.Tex_Social_ID = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.resd_cmb = new System.Windows.Forms.ComboBox();
            this.Txt_Sbirth_Date = new System.Windows.Forms.DateTimePicker();
            this.Txt_Doc_S_Exp = new System.Windows.Forms.DateTimePicker();
            this.Txt_Doc_S_Date = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmb_Gender_id = new System.Windows.Forms.ComboBox();
            this.cmb_s_nat = new System.Windows.Forms.ComboBox();
            this.Txt_T_Purpose = new System.Windows.Forms.TextBox();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.txt_Mother_name = new System.Windows.Forms.TextBox();
            this.Txt_mail = new System.Windows.Forms.TextBox();
            this.Txt_S_Birth_Place = new System.Windows.Forms.TextBox();
            this.Txt_Doc_S_Issue = new System.Windows.Forms.TextBox();
            this.Txt_S_Phone = new System.Windows.Forms.TextBox();
            this.TxtS_State = new System.Windows.Forms.TextBox();
            this.TxtS_Post_Code = new System.Windows.Forms.TextBox();
            this.Txts_street = new System.Windows.Forms.TextBox();
            this.txtS_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Sender = new System.Windows.Forms.TextBox();
            this.Cmb_Case_Purpose = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_S_Doc_No = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Cmb_S_Doc_Type = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.Cmb_Code_phone_S = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Cmb_S_City = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.lbl_branch = new System.Windows.Forms.Label();
            this.Cbo_Cust_Id = new System.Windows.Forms.ComboBox();
            this.cbo_order_type = new System.Windows.Forms.ComboBox();
            this.txt_srch = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(676, 6);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 613;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(268, 2);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(179, 23);
            this.TxtUser.TabIndex = 612;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(199, 6);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 611;
            this.label1.Text = "المستخدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(4, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(195, 23);
            this.TxtTerm_Name.TabIndex = 610;
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(539, 2);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.ReadOnly = true;
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(137, 23);
            this.Txt_Loc_Cur.TabIndex = 866;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(446, 6);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 865;
            this.label2.Text = "العملة المحلية:";
            // 
            // Txt_Rem_no
            // 
            this.Txt_Rem_no.Location = new System.Drawing.Point(626, 37);
            this.Txt_Rem_no.Name = "Txt_Rem_no";
            this.Txt_Rem_no.Size = new System.Drawing.Size(207, 20);
            this.Txt_Rem_no.TabIndex = 0;
            this.Txt_Rem_no.TextChanged += new System.EventHandler(this.Txt_Rem_no_TextChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(481, 40);
            this.label65.Name = "label65";
            this.label65.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label65.Size = new System.Drawing.Size(131, 14);
            this.label65.TabIndex = 875;
            this.label65.Text = "رقــــــم الحوالــــــــــــة:";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(724, 102);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(111, 25);
            this.BtnAdd.TabIndex = 1;
            this.BtnAdd.Text = "عرض";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 27);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(842, 1);
            this.flowLayoutPanel1.TabIndex = 921;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.TxT_Agent_Real_Cur);
            this.tabPage2.Controls.Add(this.label50);
            this.tabPage2.Controls.Add(this.TxT_Agent_Real_Comm_Amount);
            this.tabPage2.Controls.Add(this.label49);
            this.tabPage2.Controls.Add(this.TxT_Agent_Real_Rem_Amount);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.TxT_Agent_Rate);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.flowLayoutPanel15);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.cmb_job_receiver);
            this.tabPage2.Controls.Add(this.flowLayoutPanel14);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label47);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.flowLayoutPanel9);
            this.tabPage2.Controls.Add(this.flowLayoutPanel2);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.cmb_comm_type);
            this.tabPage2.Controls.Add(this.label60);
            this.tabPage2.Controls.Add(this.Cmb_Comm_Cur);
            this.tabPage2.Controls.Add(this.label62);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.label57);
            this.tabPage2.Controls.Add(this.label56);
            this.tabPage2.Controls.Add(this.label55);
            this.tabPage2.Controls.Add(this.flowLayoutPanel19);
            this.tabPage2.Controls.Add(this.Cmb_PR_Cur_Id);
            this.tabPage2.Controls.Add(this.Cmb_R_CUR_ID);
            this.tabPage2.Controls.Add(this.Cbo_rem_path);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.Cmb_T_City);
            this.tabPage2.Controls.Add(this.label54);
            this.tabPage2.Controls.Add(this.Txtr_State);
            this.tabPage2.Controls.Add(this.Txtr_Post_Code);
            this.tabPage2.Controls.Add(this.Txtr_Street);
            this.tabPage2.Controls.Add(this.Txtr_Suburb);
            this.tabPage2.Controls.Add(this.Txt_R_Phone);
            this.tabPage2.Controls.Add(this.Txt_Reciever);
            this.tabPage2.Controls.Add(this.Cmb_phone_Code_R);
            this.tabPage2.Controls.Add(this.label48);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.Cmb_R_Nat);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.Cmb_R_City);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.label38);
            this.tabPage2.Controls.Add(this.Txt_Com_Amount);
            this.tabPage2.Controls.Add(this.Txtr_amount);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(833, 491);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "معلومات المستلم والمعلومات المالية";
            // 
            // TxT_Agent_Real_Cur
            // 
            this.TxT_Agent_Real_Cur.Enabled = false;
            this.TxT_Agent_Real_Cur.Location = new System.Drawing.Point(628, 391);
            this.TxT_Agent_Real_Cur.MaxLength = 99;
            this.TxT_Agent_Real_Cur.Name = "TxT_Agent_Real_Cur";
            this.TxT_Agent_Real_Cur.Size = new System.Drawing.Size(202, 20);
            this.TxT_Agent_Real_Cur.TabIndex = 973;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(44, 369);
            this.label50.Name = "label50";
            this.label50.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label50.Size = new System.Drawing.Size(123, 14);
            this.label50.TabIndex = 972;
            this.label50.Text = "مبلغ العمولة الفعلي";
            // 
            // TxT_Agent_Real_Comm_Amount
            // 
            this.TxT_Agent_Real_Comm_Amount.BackColor = System.Drawing.Color.White;
            this.TxT_Agent_Real_Comm_Amount.Enabled = false;
            this.TxT_Agent_Real_Comm_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxT_Agent_Real_Comm_Amount.Location = new System.Drawing.Point(3, 390);
            this.TxT_Agent_Real_Comm_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxT_Agent_Real_Comm_Amount.Name = "TxT_Agent_Real_Comm_Amount";
            this.TxT_Agent_Real_Comm_Amount.NumberDecimalDigits = 3;
            this.TxT_Agent_Real_Comm_Amount.NumberDecimalSeparator = ".";
            this.TxT_Agent_Real_Comm_Amount.NumberGroupSeparator = ",";
            this.TxT_Agent_Real_Comm_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxT_Agent_Real_Comm_Amount.Size = new System.Drawing.Size(202, 23);
            this.TxT_Agent_Real_Comm_Amount.TabIndex = 971;
            this.TxT_Agent_Real_Comm_Amount.Text = "0.000";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(254, 369);
            this.label49.Name = "label49";
            this.label49.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label49.Size = new System.Drawing.Size(121, 14);
            this.label49.TabIndex = 970;
            this.label49.Text = "مبلغ الحوالة الفعلي";
            // 
            // TxT_Agent_Real_Rem_Amount
            // 
            this.TxT_Agent_Real_Rem_Amount.BackColor = System.Drawing.Color.White;
            this.TxT_Agent_Real_Rem_Amount.Enabled = false;
            this.TxT_Agent_Real_Rem_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxT_Agent_Real_Rem_Amount.Location = new System.Drawing.Point(212, 390);
            this.TxT_Agent_Real_Rem_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxT_Agent_Real_Rem_Amount.Name = "TxT_Agent_Real_Rem_Amount";
            this.TxT_Agent_Real_Rem_Amount.NumberDecimalDigits = 3;
            this.TxT_Agent_Real_Rem_Amount.NumberDecimalSeparator = ".";
            this.TxT_Agent_Real_Rem_Amount.NumberGroupSeparator = ",";
            this.TxT_Agent_Real_Rem_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxT_Agent_Real_Rem_Amount.Size = new System.Drawing.Size(202, 23);
            this.TxT_Agent_Real_Rem_Amount.TabIndex = 969;
            this.TxT_Agent_Real_Rem_Amount.Text = "0.000";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(478, 369);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(95, 14);
            this.label46.TabIndex = 968;
            this.label46.Text = "المعادل الفعلي";
            // 
            // TxT_Agent_Rate
            // 
            this.TxT_Agent_Rate.BackColor = System.Drawing.Color.White;
            this.TxT_Agent_Rate.Enabled = false;
            this.TxT_Agent_Rate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxT_Agent_Rate.Location = new System.Drawing.Point(420, 390);
            this.TxT_Agent_Rate.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxT_Agent_Rate.Name = "TxT_Agent_Rate";
            this.TxT_Agent_Rate.NumberDecimalDigits = 3;
            this.TxT_Agent_Rate.NumberDecimalSeparator = ".";
            this.TxT_Agent_Rate.NumberGroupSeparator = ",";
            this.TxT_Agent_Rate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxT_Agent_Rate.Size = new System.Drawing.Size(202, 23);
            this.TxT_Agent_Rate.TabIndex = 967;
            this.TxT_Agent_Rate.Text = "0.000";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Maroon;
            this.label37.Location = new System.Drawing.Point(579, 344);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(250, 14);
            this.label37.TabIndex = 966;
            this.label37.Text = "المعلومات المالية للحوالة بالعملة الفعلية...";
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(3, 354);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(829, 1);
            this.flowLayoutPanel15.TabIndex = 965;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(680, 369);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 14);
            this.label8.TabIndex = 964;
            this.label8.Text = "العملة الفعلية";
            // 
            // cmb_job_receiver
            // 
            this.cmb_job_receiver.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_job_receiver.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_job_receiver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_job_receiver.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_job_receiver.FormattingEnabled = true;
            this.cmb_job_receiver.Location = new System.Drawing.Point(16, 142);
            this.cmb_job_receiver.Name = "cmb_job_receiver";
            this.cmb_job_receiver.Size = new System.Drawing.Size(292, 24);
            this.cmb_job_receiver.TabIndex = 8;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-1, 429);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(833, 1);
            this.flowLayoutPanel14.TabIndex = 919;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(305, 447);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 26);
            this.button2.TabIndex = 18;
            this.button2.Text = "انهــــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(416, 447);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 26);
            this.button1.TabIndex = 17;
            this.button1.Text = "موافــــق";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Maroon;
            this.label47.Location = new System.Drawing.Point(279, 226);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(121, 14);
            this.label47.TabIndex = 933;
            this.label47.Text = "مبلــــغ العمولـــــة....";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(713, 226);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(119, 14);
            this.label12.TabIndex = 932;
            this.label12.Text = "مبلــــغ الحوالـــــة....";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1, 236);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(733, 1);
            this.flowLayoutPanel9.TabIndex = 931;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(5, 46);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(704, 1);
            this.flowLayoutPanel2.TabIndex = 930;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(704, 1);
            this.flowLayoutPanel12.TabIndex = 931;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(712, 36);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(113, 14);
            this.label6.TabIndex = 929;
            this.label6.Text = "الـعنـــــــــــــوان......";
            // 
            // cmb_comm_type
            // 
            this.cmb_comm_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_comm_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_comm_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_comm_type.Enabled = false;
            this.cmb_comm_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_comm_type.FormattingEnabled = true;
            this.cmb_comm_type.Items.AddRange(new object[] {
            "مقبوضة"});
            this.cmb_comm_type.Location = new System.Drawing.Point(16, 309);
            this.cmb_comm_type.Name = "cmb_comm_type";
            this.cmb_comm_type.Size = new System.Drawing.Size(259, 24);
            this.cmb_comm_type.TabIndex = 16;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label60.ForeColor = System.Drawing.Color.Navy;
            this.label60.Location = new System.Drawing.Point(279, 313);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(54, 16);
            this.label60.TabIndex = 928;
            this.label60.Text = "نوعهـــــا:";
            // 
            // Cmb_Comm_Cur
            // 
            this.Cmb_Comm_Cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Comm_Cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Comm_Cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Comm_Cur.Enabled = false;
            this.Cmb_Comm_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Comm_Cur.FormattingEnabled = true;
            this.Cmb_Comm_Cur.Location = new System.Drawing.Point(16, 280);
            this.Cmb_Comm_Cur.Name = "Cmb_Comm_Cur";
            this.Cmb_Comm_Cur.Size = new System.Drawing.Size(259, 24);
            this.Cmb_Comm_Cur.TabIndex = 15;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(278, 284);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(55, 16);
            this.label62.TabIndex = 927;
            this.label62.Text = "العملــــــة:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Navy;
            this.label63.Location = new System.Drawing.Point(278, 257);
            this.label63.Name = "label63";
            this.label63.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label63.Size = new System.Drawing.Size(54, 14);
            this.label63.TabIndex = 926;
            this.label63.Text = "المبلـــغ:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(731, 314);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 14);
            this.label58.TabIndex = 922;
            this.label58.Text = "عملة الدفــع:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(727, 285);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(84, 14);
            this.label57.TabIndex = 921;
            this.label57.Text = "عملة الحوالة:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Navy;
            this.label56.Location = new System.Drawing.Point(729, 257);
            this.label56.Name = "label56";
            this.label56.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label56.Size = new System.Drawing.Size(80, 14);
            this.label56.TabIndex = 920;
            this.label56.Text = "مبلغ الحوالة:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Maroon;
            this.label55.Location = new System.Drawing.Point(699, 163);
            this.label55.Name = "label55";
            this.label55.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label55.Size = new System.Drawing.Size(130, 14);
            this.label55.TabIndex = 919;
            this.label55.Text = "معلومات الحوالـــــة....";
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(1, 174);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(708, 1);
            this.flowLayoutPanel19.TabIndex = 918;
            // 
            // Cmb_PR_Cur_Id
            // 
            this.Cmb_PR_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_PR_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_PR_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_PR_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_PR_Cur_Id.FormattingEnabled = true;
            this.Cmb_PR_Cur_Id.Location = new System.Drawing.Point(430, 309);
            this.Cmb_PR_Cur_Id.Name = "Cmb_PR_Cur_Id";
            this.Cmb_PR_Cur_Id.Size = new System.Drawing.Size(292, 24);
            this.Cmb_PR_Cur_Id.TabIndex = 13;
            // 
            // Cmb_R_CUR_ID
            // 
            this.Cmb_R_CUR_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_CUR_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_CUR_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_CUR_ID.Enabled = false;
            this.Cmb_R_CUR_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_CUR_ID.FormattingEnabled = true;
            this.Cmb_R_CUR_ID.Location = new System.Drawing.Point(430, 280);
            this.Cmb_R_CUR_ID.Name = "Cmb_R_CUR_ID";
            this.Cmb_R_CUR_ID.Size = new System.Drawing.Size(292, 24);
            this.Cmb_R_CUR_ID.TabIndex = 12;
            // 
            // Cbo_rem_path
            // 
            this.Cbo_rem_path.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_rem_path.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_rem_path.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_rem_path.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_rem_path.FormattingEnabled = true;
            this.Cbo_rem_path.Items.AddRange(new object[] {
            "شبكة محلية/اوفلاين",
            "شبكة Online "});
            this.Cbo_rem_path.Location = new System.Drawing.Point(64, 193);
            this.Cbo_rem_path.Name = "Cbo_rem_path";
            this.Cbo_rem_path.Size = new System.Drawing.Size(237, 24);
            this.Cbo_rem_path.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(299, 198);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(101, 14);
            this.label3.TabIndex = 867;
            this.label3.Text = "مسار الحوالـــــة:";
            // 
            // Cmb_T_City
            // 
            this.Cmb_T_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_T_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_T_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_T_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_T_City.FormattingEnabled = true;
            this.Cmb_T_City.Location = new System.Drawing.Point(435, 193);
            this.Cmb_T_City.Name = "Cmb_T_City";
            this.Cmb_T_City.Size = new System.Drawing.Size(299, 24);
            this.Cmb_T_City.TabIndex = 9;
            //this.Cmb_T_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_T_City_SelectedIndexChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(733, 197);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(87, 16);
            this.label54.TabIndex = 868;
            this.label54.Text = "مدينة التسليــــم  :";
            // 
            // Txtr_State
            // 
            this.Txtr_State.Location = new System.Drawing.Point(444, 60);
            this.Txtr_State.MaxLength = 99;
            this.Txtr_State.Name = "Txtr_State";
            this.Txtr_State.Size = new System.Drawing.Size(291, 20);
            this.Txtr_State.TabIndex = 1;
            // 
            // Txtr_Post_Code
            // 
            this.Txtr_Post_Code.Location = new System.Drawing.Point(443, 141);
            this.Txtr_Post_Code.MaxLength = 99;
            this.Txtr_Post_Code.Name = "Txtr_Post_Code";
            this.Txtr_Post_Code.Size = new System.Drawing.Size(291, 20);
            this.Txtr_Post_Code.TabIndex = 7;
            // 
            // Txtr_Street
            // 
            this.Txtr_Street.Location = new System.Drawing.Point(443, 114);
            this.Txtr_Street.MaxLength = 99;
            this.Txtr_Street.Name = "Txtr_Street";
            this.Txtr_Street.Size = new System.Drawing.Size(291, 20);
            this.Txtr_Street.TabIndex = 5;
            // 
            // Txtr_Suburb
            // 
            this.Txtr_Suburb.Location = new System.Drawing.Point(443, 86);
            this.Txtr_Suburb.MaxLength = 99;
            this.Txtr_Suburb.Name = "Txtr_Suburb";
            this.Txtr_Suburb.Size = new System.Drawing.Size(291, 20);
            this.Txtr_Suburb.TabIndex = 3;
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.Location = new System.Drawing.Point(16, 114);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.Size = new System.Drawing.Size(200, 20);
            this.Txt_R_Phone.TabIndex = 6;
            // 
            // Txt_Reciever
            // 
            this.Txt_Reciever.Location = new System.Drawing.Point(444, 12);
            this.Txt_Reciever.MaxLength = 49;
            this.Txt_Reciever.Name = "Txt_Reciever";
            this.Txt_Reciever.Size = new System.Drawing.Size(291, 20);
            this.Txt_Reciever.TabIndex = 0;
            // 
            // Cmb_phone_Code_R
            // 
            this.Cmb_phone_Code_R.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_phone_Code_R.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_phone_Code_R.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_phone_Code_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_phone_Code_R.FormattingEnabled = true;
            this.Cmb_phone_Code_R.Location = new System.Drawing.Point(216, 112);
            this.Cmb_phone_Code_R.Name = "Cmb_phone_Code_R";
            this.Cmb_phone_Code_R.Size = new System.Drawing.Size(92, 24);
            this.Cmb_phone_Code_R.TabIndex = 7;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(317, 143);
            this.label48.Name = "label48";
            this.label48.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label48.Size = new System.Drawing.Size(83, 16);
            this.label48.TabIndex = 824;
            this.label48.Text = "المهنـــــــــــــــة:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(739, 62);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(84, 16);
            this.label45.TabIndex = 819;
            this.label45.Text = "المحافظـــــــــة :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(744, 143);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(79, 16);
            this.label44.TabIndex = 817;
            this.label44.Text = "الرمز البريــدي:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(743, 88);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(80, 16);
            this.label43.TabIndex = 815;
            this.label43.Text = "الحـــــــــــــــي:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(739, 116);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(84, 16);
            this.label42.TabIndex = 813;
            this.label42.Text = "الزقــــــــــــــاق:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(319, 116);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(81, 16);
            this.label41.TabIndex = 810;
            this.label41.Text = "الهاتـــــــــــــف:";
            // 
            // Cmb_R_Nat
            // 
            this.Cmb_R_Nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_Nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_Nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_Nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_Nat.FormattingEnabled = true;
            this.Cmb_R_Nat.Location = new System.Drawing.Point(16, 84);
            this.Cmb_R_Nat.Name = "Cmb_R_Nat";
            this.Cmb_R_Nat.Size = new System.Drawing.Size(292, 24);
            this.Cmb_R_Nat.TabIndex = 4;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(320, 88);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label40.Size = new System.Drawing.Size(80, 16);
            this.label40.TabIndex = 808;
            this.label40.Text = "الجنسيـــــــــــة:";
            // 
            // Cmb_R_City
            // 
            this.Cmb_R_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_City.FormattingEnabled = true;
            this.Cmb_R_City.Location = new System.Drawing.Point(16, 58);
            this.Cmb_R_City.Name = "Cmb_R_City";
            this.Cmb_R_City.Size = new System.Drawing.Size(292, 24);
            this.Cmb_R_City.TabIndex = 2;
            this.Cmb_R_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_R_City_SelectedIndexChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(320, 62);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 16);
            this.label39.TabIndex = 807;
            this.label39.Text = "المدينـــــــــــــة:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(740, 15);
            this.label38.Name = "label38";
            this.label38.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label38.Size = new System.Drawing.Size(75, 14);
            this.label38.TabIndex = 804;
            this.label38.Text = "الاســــــــــم:";
            // 
            // Txt_Com_Amount
            // 
            this.Txt_Com_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Com_Amount.Enabled = false;
            this.Txt_Com_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Com_Amount.Location = new System.Drawing.Point(64, 253);
            this.Txt_Com_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Com_Amount.Name = "Txt_Com_Amount";
            this.Txt_Com_Amount.NumberDecimalDigits = 3;
            this.Txt_Com_Amount.NumberDecimalSeparator = ".";
            this.Txt_Com_Amount.NumberGroupSeparator = ",";
            this.Txt_Com_Amount.ReadOnly = true;
            this.Txt_Com_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Com_Amount.Size = new System.Drawing.Size(211, 23);
            this.Txt_Com_Amount.TabIndex = 14;
            this.Txt_Com_Amount.Text = "0.000";
            // 
            // Txtr_amount
            // 
            this.Txtr_amount.BackColor = System.Drawing.Color.White;
            this.Txtr_amount.Enabled = false;
            this.Txtr_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_amount.Location = new System.Drawing.Point(520, 253);
            this.Txtr_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtr_amount.Name = "Txtr_amount";
            this.Txtr_amount.NumberDecimalDigits = 3;
            this.Txtr_amount.NumberDecimalSeparator = ".";
            this.Txtr_amount.NumberGroupSeparator = ",";
            this.Txtr_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_amount.Size = new System.Drawing.Size(202, 23);
            this.Txtr_amount.TabIndex = 11;
            this.Txtr_amount.Text = "0.000";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(833, 491);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "معلومات المرسل";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tex_reason);
            this.panel1.Controls.Add(this.lab_reason);
            this.panel1.Controls.Add(this.txt_notes);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.Txt_S_details_job);
            this.panel1.Controls.Add(this.label89);
            this.panel1.Controls.Add(this.cmb_job_sender);
            this.panel1.Controls.Add(this.Tex_Social_ID);
            this.panel1.Controls.Add(this.label79);
            this.panel1.Controls.Add(this.label78);
            this.panel1.Controls.Add(this.resd_cmb);
            this.panel1.Controls.Add(this.Txt_Sbirth_Date);
            this.panel1.Controls.Add(this.Txt_Doc_S_Exp);
            this.panel1.Controls.Add(this.Txt_Doc_S_Date);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.flowLayoutPanel17);
            this.panel1.Controls.Add(this.cmb_Gender_id);
            this.panel1.Controls.Add(this.cmb_s_nat);
            this.panel1.Controls.Add(this.Txt_T_Purpose);
            this.panel1.Controls.Add(this.Txt_Relionship);
            this.panel1.Controls.Add(this.Txt_Soruce_money);
            this.panel1.Controls.Add(this.txt_Mother_name);
            this.panel1.Controls.Add(this.Txt_mail);
            this.panel1.Controls.Add(this.Txt_S_Birth_Place);
            this.panel1.Controls.Add(this.Txt_Doc_S_Issue);
            this.panel1.Controls.Add(this.Txt_S_Phone);
            this.panel1.Controls.Add(this.TxtS_State);
            this.panel1.Controls.Add(this.TxtS_Post_Code);
            this.panel1.Controls.Add(this.Txts_street);
            this.panel1.Controls.Add(this.txtS_Suburb);
            this.panel1.Controls.Add(this.Txt_Sender);
            this.panel1.Controls.Add(this.Cmb_Case_Purpose);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.flowLayoutPanel7);
            this.panel1.Controls.Add(this.flowLayoutPanel8);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.Txt_S_Doc_No);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.Cmb_S_Doc_Type);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.flowLayoutPanel5);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.Cmb_Code_phone_S);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.Cmb_S_City);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(827, 485);
            this.panel1.TabIndex = 1;
            // 
            // tex_reason
            // 
            this.tex_reason.Location = new System.Drawing.Point(15, 359);
            this.tex_reason.MaxLength = 199;
            this.tex_reason.Name = "tex_reason";
            this.tex_reason.Size = new System.Drawing.Size(709, 20);
            this.tex_reason.TabIndex = 884;
            // 
            // lab_reason
            // 
            this.lab_reason.AutoSize = true;
            this.lab_reason.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.lab_reason.ForeColor = System.Drawing.Color.Navy;
            this.lab_reason.Location = new System.Drawing.Point(732, 360);
            this.lab_reason.Name = "lab_reason";
            this.lab_reason.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lab_reason.Size = new System.Drawing.Size(68, 16);
            this.lab_reason.TabIndex = 885;
            this.lab_reason.Text = "سبب التعديل:";
            // 
            // txt_notes
            // 
            this.txt_notes.Location = new System.Drawing.Point(14, 333);
            this.txt_notes.MaxLength = 199;
            this.txt_notes.Name = "txt_notes";
            this.txt_notes.ReadOnly = true;
            this.txt_notes.Size = new System.Drawing.Size(709, 20);
            this.txt_notes.TabIndex = 882;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(731, 334);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(78, 16);
            this.label5.TabIndex = 883;
            this.label5.Text = "مـلاحظـــــــات:";
            // 
            // Txt_S_details_job
            // 
            this.Txt_S_details_job.Location = new System.Drawing.Point(15, 259);
            this.Txt_S_details_job.MaxLength = 99;
            this.Txt_S_details_job.Name = "Txt_S_details_job";
            this.Txt_S_details_job.Size = new System.Drawing.Size(292, 20);
            this.Txt_S_details_job.TabIndex = 880;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label89.ForeColor = System.Drawing.Color.Navy;
            this.label89.Location = new System.Drawing.Point(313, 261);
            this.label89.Name = "label89";
            this.label89.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label89.Size = new System.Drawing.Size(77, 16);
            this.label89.TabIndex = 881;
            this.label89.Text = "تفاصيل العمل :";
            // 
            // cmb_job_sender
            // 
            this.cmb_job_sender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_job_sender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_job_sender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_job_sender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_job_sender.FormattingEnabled = true;
            this.cmb_job_sender.Location = new System.Drawing.Point(477, 257);
            this.cmb_job_sender.Name = "cmb_job_sender";
            this.cmb_job_sender.Size = new System.Drawing.Size(245, 24);
            this.cmb_job_sender.TabIndex = 15;
            // 
            // Tex_Social_ID
            // 
            this.Tex_Social_ID.Location = new System.Drawing.Point(475, 309);
            this.Tex_Social_ID.MaxLength = 29;
            this.Tex_Social_ID.Name = "Tex_Social_ID";
            this.Tex_Social_ID.Size = new System.Drawing.Size(247, 20);
            this.Tex_Social_ID.TabIndex = 19;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label79.ForeColor = System.Drawing.Color.Navy;
            this.label79.Location = new System.Drawing.Point(735, 311);
            this.label79.Name = "label79";
            this.label79.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label79.Size = new System.Drawing.Size(73, 16);
            this.label79.TabIndex = 879;
            this.label79.Text = "الرقم الوطني :";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label78.ForeColor = System.Drawing.Color.Navy;
            this.label78.Location = new System.Drawing.Point(311, 402);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(73, 16);
            this.label78.TabIndex = 877;
            this.label78.Text = "نوع الاقامـــة:";
            // 
            // resd_cmb
            // 
            this.resd_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_cmb.FormattingEnabled = true;
            this.resd_cmb.Location = new System.Drawing.Point(14, 398);
            this.resd_cmb.Name = "resd_cmb";
            this.resd_cmb.Size = new System.Drawing.Size(292, 24);
            this.resd_cmb.TabIndex = 20;
            // 
            // Txt_Sbirth_Date
            // 
            this.Txt_Sbirth_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Sbirth_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Sbirth_Date.Location = new System.Drawing.Point(125, 209);
            this.Txt_Sbirth_Date.Name = "Txt_Sbirth_Date";
            this.Txt_Sbirth_Date.Size = new System.Drawing.Size(181, 20);
            this.Txt_Sbirth_Date.TabIndex = 12;
            this.Txt_Sbirth_Date.Enter += new System.EventHandler(this.Txt_Sbirth_Date_Enter);
            // 
            // Txt_Doc_S_Exp
            // 
            this.Txt_Doc_S_Exp.Checked = false;
            this.Txt_Doc_S_Exp.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_S_Exp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_S_Exp.Location = new System.Drawing.Point(125, 186);
            this.Txt_Doc_S_Exp.Name = "Txt_Doc_S_Exp";
            this.Txt_Doc_S_Exp.ShowCheckBox = true;
            this.Txt_Doc_S_Exp.Size = new System.Drawing.Size(181, 20);
            this.Txt_Doc_S_Exp.TabIndex = 10;
            this.Txt_Doc_S_Exp.ValueChanged += new System.EventHandler(this.Txt_Doc_S_Exp_ValueChanged);
            // 
            // Txt_Doc_S_Date
            // 
            this.Txt_Doc_S_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_S_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_S_Date.Location = new System.Drawing.Point(568, 186);
            this.Txt_Doc_S_Date.Name = "Txt_Doc_S_Date";
            this.Txt_Doc_S_Date.Size = new System.Drawing.Size(151, 20);
            this.Txt_Doc_S_Date.TabIndex = 9;
            this.Txt_Doc_S_Date.Enter += new System.EventHandler(this.Txt_Doc_S_Date_Enter);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(475, 188);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 867;
            this.label34.Text = "dd/mm/yyyy";
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(0, 47);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(704, 1);
            this.flowLayoutPanel17.TabIndex = 863;
            // 
            // cmb_Gender_id
            // 
            this.cmb_Gender_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Gender_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Gender_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Gender_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Gender_id.FormattingEnabled = true;
            this.cmb_Gender_id.Location = new System.Drawing.Point(15, 284);
            this.cmb_Gender_id.Name = "cmb_Gender_id";
            this.cmb_Gender_id.Size = new System.Drawing.Size(292, 24);
            this.cmb_Gender_id.TabIndex = 18;
            // 
            // cmb_s_nat
            // 
            this.cmb_s_nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_s_nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_s_nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_s_nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_s_nat.FormattingEnabled = true;
            this.cmb_s_nat.Location = new System.Drawing.Point(15, 232);
            this.cmb_s_nat.Name = "cmb_s_nat";
            this.cmb_s_nat.Size = new System.Drawing.Size(292, 24);
            this.cmb_s_nat.TabIndex = 14;
            // 
            // Txt_T_Purpose
            // 
            this.Txt_T_Purpose.Location = new System.Drawing.Point(256, 456);
            this.Txt_T_Purpose.MaxLength = 199;
            this.Txt_T_Purpose.Name = "Txt_T_Purpose";
            this.Txt_T_Purpose.Size = new System.Drawing.Size(474, 20);
            this.Txt_T_Purpose.TabIndex = 23;
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.Location = new System.Drawing.Point(18, 427);
            this.Txt_Relionship.MaxLength = 99;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.Size = new System.Drawing.Size(712, 20);
            this.Txt_Relionship.TabIndex = 22;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.Location = new System.Drawing.Point(390, 400);
            this.Txt_Soruce_money.MaxLength = 99;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.Size = new System.Drawing.Size(340, 20);
            this.Txt_Soruce_money.TabIndex = 21;
            // 
            // txt_Mother_name
            // 
            this.txt_Mother_name.Location = new System.Drawing.Point(15, 309);
            this.txt_Mother_name.MaxLength = 49;
            this.txt_Mother_name.Name = "txt_Mother_name";
            this.txt_Mother_name.Size = new System.Drawing.Size(293, 20);
            this.txt_Mother_name.TabIndex = 16;
            // 
            // Txt_mail
            // 
            this.Txt_mail.Location = new System.Drawing.Point(475, 284);
            this.Txt_mail.MaxLength = 29;
            this.Txt_mail.Name = "Txt_mail";
            this.Txt_mail.Size = new System.Drawing.Size(247, 20);
            this.Txt_mail.TabIndex = 17;
            // 
            // Txt_S_Birth_Place
            // 
            this.Txt_S_Birth_Place.Location = new System.Drawing.Point(475, 232);
            this.Txt_S_Birth_Place.MaxLength = 49;
            this.Txt_S_Birth_Place.Name = "Txt_S_Birth_Place";
            this.Txt_S_Birth_Place.Size = new System.Drawing.Size(247, 20);
            this.Txt_S_Birth_Place.TabIndex = 13;
            // 
            // Txt_Doc_S_Issue
            // 
            this.Txt_Doc_S_Issue.Location = new System.Drawing.Point(475, 208);
            this.Txt_Doc_S_Issue.MaxLength = 49;
            this.Txt_Doc_S_Issue.Name = "Txt_Doc_S_Issue";
            this.Txt_Doc_S_Issue.Size = new System.Drawing.Size(247, 20);
            this.Txt_Doc_S_Issue.TabIndex = 11;
            // 
            // Txt_S_Phone
            // 
            this.Txt_S_Phone.Location = new System.Drawing.Point(21, 113);
            this.Txt_S_Phone.MaxLength = 19;
            this.Txt_S_Phone.Name = "Txt_S_Phone";
            this.Txt_S_Phone.Size = new System.Drawing.Size(210, 20);
            this.Txt_S_Phone.TabIndex = 6;
            // 
            // TxtS_State
            // 
            this.TxtS_State.Location = new System.Drawing.Point(423, 86);
            this.TxtS_State.MaxLength = 99;
            this.TxtS_State.Name = "TxtS_State";
            this.TxtS_State.Size = new System.Drawing.Size(302, 20);
            this.TxtS_State.TabIndex = 3;
            // 
            // TxtS_Post_Code
            // 
            this.TxtS_Post_Code.Location = new System.Drawing.Point(21, 86);
            this.TxtS_Post_Code.MaxLength = 99;
            this.TxtS_Post_Code.Name = "TxtS_Post_Code";
            this.TxtS_Post_Code.Size = new System.Drawing.Size(301, 20);
            this.TxtS_Post_Code.TabIndex = 4;
            // 
            // Txts_street
            // 
            this.Txts_street.Location = new System.Drawing.Point(21, 58);
            this.Txts_street.MaxLength = 99;
            this.Txts_street.Name = "Txts_street";
            this.Txts_street.Size = new System.Drawing.Size(301, 20);
            this.Txts_street.TabIndex = 2;
            // 
            // txtS_Suburb
            // 
            this.txtS_Suburb.Location = new System.Drawing.Point(424, 113);
            this.txtS_Suburb.MaxLength = 99;
            this.txtS_Suburb.Name = "txtS_Suburb";
            this.txtS_Suburb.Size = new System.Drawing.Size(301, 20);
            this.txtS_Suburb.TabIndex = 5;
            // 
            // Txt_Sender
            // 
            this.Txt_Sender.Location = new System.Drawing.Point(419, 11);
            this.Txt_Sender.MaxLength = 49;
            this.Txt_Sender.Name = "Txt_Sender";
            this.Txt_Sender.Size = new System.Drawing.Size(347, 20);
            this.Txt_Sender.TabIndex = 0;
            // 
            // Cmb_Case_Purpose
            // 
            this.Cmb_Case_Purpose.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Case_Purpose.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Case_Purpose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Case_Purpose.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Case_Purpose.FormattingEnabled = true;
            this.Cmb_Case_Purpose.Location = new System.Drawing.Point(18, 454);
            this.Cmb_Case_Purpose.Name = "Cmb_Case_Purpose";
            this.Cmb_Case_Purpose.Size = new System.Drawing.Size(232, 24);
            this.Cmb_Case_Purpose.TabIndex = 24;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(736, 460);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(86, 16);
            this.label36.TabIndex = 831;
            this.label36.Text = "غرض التحويـــل:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(736, 431);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(80, 16);
            this.label35.TabIndex = 829;
            this.label35.Text = "علاقـــة م . س :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(736, 404);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(86, 16);
            this.label31.TabIndex = 827;
            this.label31.Text = "مصدر المـــــــال:";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(0, 391);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(685, 1);
            this.flowLayoutPanel7.TabIndex = 826;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-80, 3);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel11.TabIndex = 758;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(959, 555);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(21, 1);
            this.flowLayoutPanel8.TabIndex = 825;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-744, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel10.TabIndex = 758;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Maroon;
            this.label30.Location = new System.Drawing.Point(685, 381);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(135, 14);
            this.label30.TabIndex = 824;
            this.label30.Text = "معلومــــات اخـــرى......";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(316, 288);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(82, 16);
            this.label29.TabIndex = 822;
            this.label29.Text = "الجنـــــــــــــس:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(316, 311);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(82, 16);
            this.label28.TabIndex = 820;
            this.label28.Text = "اســــــــــــم الام:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(316, 236);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(83, 16);
            this.label27.TabIndex = 818;
            this.label27.Text = "الجنسيــــــــــــة:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Maroon;
            this.label33.Location = new System.Drawing.Point(21, 215);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(87, 14);
            this.label33.TabIndex = 817;
            this.label33.Text = "dd/mm/yyyy";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(316, 212);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(83, 16);
            this.label24.TabIndex = 815;
            this.label24.Text = "التولــــــــــــــــد:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Maroon;
            this.label32.Location = new System.Drawing.Point(23, 190);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(87, 14);
            this.label32.TabIndex = 814;
            this.label32.Text = "dd/mm/yyyy";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(312, 190);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(87, 16);
            this.label25.TabIndex = 812;
            this.label25.Text = "انتهائهـــــــــــــا:";
            // 
            // Txt_S_Doc_No
            // 
            this.Txt_S_Doc_No.BackColor = System.Drawing.Color.White;
            this.Txt_S_Doc_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Doc_No.Location = new System.Drawing.Point(15, 157);
            this.Txt_S_Doc_No.MaxLength = 49;
            this.Txt_S_Doc_No.Name = "Txt_S_Doc_No";
            this.Txt_S_Doc_No.Size = new System.Drawing.Size(292, 23);
            this.Txt_S_Doc_No.TabIndex = 8;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(312, 161);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 16);
            this.label26.TabIndex = 810;
            this.label26.Text = "رقم الوثيقــــــــــة:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(731, 288);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(90, 16);
            this.label23.TabIndex = 808;
            this.label23.Text = "البريد الالكتروني:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(731, 262);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(77, 16);
            this.label22.TabIndex = 806;
            this.label22.Text = "المهنـــــــــــــة:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(731, 236);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(77, 16);
            this.label21.TabIndex = 804;
            this.label21.Text = "محــــل الولادة:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(731, 212);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(77, 16);
            this.label20.TabIndex = 802;
            this.label20.Text = "م:اصدارهـــــا :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(731, 190);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(76, 16);
            this.label18.TabIndex = 799;
            this.label18.Text = "تاريخهـــــــــا:";
            // 
            // Cmb_S_Doc_Type
            // 
            this.Cmb_S_Doc_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_S_Doc_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_S_Doc_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_S_Doc_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_S_Doc_Type.FormattingEnabled = true;
            this.Cmb_S_Doc_Type.Location = new System.Drawing.Point(474, 157);
            this.Cmb_S_Doc_Type.Name = "Cmb_S_Doc_Type";
            this.Cmb_S_Doc_Type.Size = new System.Drawing.Size(247, 24);
            this.Cmb_S_Doc_Type.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(731, 161);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 16);
            this.label19.TabIndex = 769;
            this.label19.Text = "نوع الوثيقـــــــة:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(968, 249);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(21, 1);
            this.flowLayoutPanel5.TabIndex = 767;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-744, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-2, 150);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(712, 1);
            this.flowLayoutPanel3.TabIndex = 766;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-53, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel4.TabIndex = 758;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Maroon;
            this.label17.Location = new System.Drawing.Point(712, 139);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(107, 14);
            this.label17.TabIndex = 765;
            this.label17.Text = "الهويــــــــــــــة......";
            // 
            // Cmb_Code_phone_S
            // 
            this.Cmb_Code_phone_S.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Code_phone_S.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Code_phone_S.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Code_phone_S.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Code_phone_S.FormattingEnabled = true;
            this.Cmb_Code_phone_S.Location = new System.Drawing.Point(232, 111);
            this.Cmb_Code_phone_S.Name = "Cmb_Code_phone_S";
            this.Cmb_Code_phone_S.Size = new System.Drawing.Size(90, 24);
            this.Cmb_Code_phone_S.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(331, 115);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 16);
            this.label16.TabIndex = 763;
            this.label16.Text = "الهاتـــــــــــــف:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(730, 88);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(81, 16);
            this.label15.TabIndex = 760;
            this.label15.Text = "المحافظـــــــــة:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(331, 88);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(79, 16);
            this.label14.TabIndex = 758;
            this.label14.Text = "الرمز البريــدي:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(732, 115);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(77, 16);
            this.label13.TabIndex = 756;
            this.label13.Text = "الحــــــــــــــي:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(328, 60);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(78, 16);
            this.label11.TabIndex = 754;
            this.label11.Text = "الزقــــــــــــاق:";
            // 
            // Cmb_S_City
            // 
            this.Cmb_S_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_S_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_S_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_S_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_S_City.FormattingEnabled = true;
            this.Cmb_S_City.Location = new System.Drawing.Point(424, 56);
            this.Cmb_S_City.Name = "Cmb_S_City";
            this.Cmb_S_City.Size = new System.Drawing.Size(301, 24);
            this.Cmb_S_City.TabIndex = 1;
            this.Cmb_S_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_S_City_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(731, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 16);
            this.label10.TabIndex = 753;
            this.label10.Text = "المدينـــــــــــــة:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(707, 37);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(113, 14);
            this.label9.TabIndex = 750;
            this.label9.Text = "الـعنـــــــــــــوان......";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(768, 14);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(54, 14);
            this.label7.TabIndex = 746;
            this.label7.Text = "الاســـم:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 136);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(841, 517);
            this.tabControl1.TabIndex = 1;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 1;
            this.lineShape3.X2 = 1;
            this.lineShape3.Y1 = 50;
            this.lineShape3.Y2 = 188;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 969;
            this.lineShape4.X2 = 969;
            this.lineShape4.Y1 = 20;
            this.lineShape4.Y2 = 259;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 411;
            this.lineShape1.X2 = 411;
            this.lineShape1.Y1 = 49;
            this.lineShape1.Y2 = 188;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(-1, 133);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(845, 1);
            this.flowLayoutPanel13.TabIndex = 922;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(728, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(111, 22);
            this.TxtIn_Rec_Date.TabIndex = 864;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // lbl_branch
            // 
            this.lbl_branch.AutoSize = true;
            this.lbl_branch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_branch.ForeColor = System.Drawing.Color.Navy;
            this.lbl_branch.Location = new System.Drawing.Point(488, 69);
            this.lbl_branch.Name = "lbl_branch";
            this.lbl_branch.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_branch.Size = new System.Drawing.Size(128, 14);
            this.lbl_branch.TabIndex = 924;
            this.lbl_branch.Text = " الفــــــرع / الوكـــــلاء:";
            // 
            // Cbo_Cust_Id
            // 
            this.Cbo_Cust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Cust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Cust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Cust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Cust_Id.FormattingEnabled = true;
            this.Cbo_Cust_Id.Location = new System.Drawing.Point(623, 64);
            this.Cbo_Cust_Id.Name = "Cbo_Cust_Id";
            this.Cbo_Cust_Id.Size = new System.Drawing.Size(212, 24);
            this.Cbo_Cust_Id.TabIndex = 923;
            // 
            // cbo_order_type
            // 
            this.cbo_order_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_order_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_order_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_order_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_order_type.FormattingEnabled = true;
            this.cbo_order_type.Items.AddRange(new object[] {
            "فروع",
            "وكالات"});
            this.cbo_order_type.Location = new System.Drawing.Point(304, 32);
            this.cbo_order_type.Name = "cbo_order_type";
            this.cbo_order_type.Size = new System.Drawing.Size(142, 24);
            this.cbo_order_type.TabIndex = 1262;
            this.cbo_order_type.SelectedIndexChanged += new System.EventHandler(this.cbo_order_type_SelectedIndexChanged);
            // 
            // txt_srch
            // 
            this.txt_srch.AutoSize = true;
            this.txt_srch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.txt_srch.ForeColor = System.Drawing.Color.Navy;
            this.txt_srch.Location = new System.Drawing.Point(8, 36);
            this.txt_srch.Name = "txt_srch";
            this.txt_srch.Size = new System.Drawing.Size(62, 16);
            this.txt_srch.TabIndex = 1261;
            this.txt_srch.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(71, 34);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(230, 22);
            this.Txt_Sub_Cust.TabIndex = 1260;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(8, 58);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(439, 72);
            this.Grd_Sub_Cust.TabIndex = 1259;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "cust_id";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 91;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 225;
            // 
            // Update_OutIn_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 654);
            this.Controls.Add(this.cbo_order_type);
            this.Controls.Add(this.txt_srch);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.Controls.Add(this.lbl_branch);
            this.Controls.Add(this.Cbo_Cust_Id);
            this.Controls.Add(this.flowLayoutPanel13);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Txt_Rem_no);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Update_OutIn_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update_OutIn_Rem";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Add_rem_FormClosed);
            this.Load += new System.EventHandler(this.Add_rem_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Rem_no;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox Txtr_State;
        private System.Windows.Forms.TextBox Txtr_Post_Code;
        private System.Windows.Forms.TextBox Txtr_Street;
        private System.Windows.Forms.TextBox Txtr_Suburb;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_Reciever;
        private System.Windows.Forms.ComboBox Cmb_phone_Code_R;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox Cmb_R_Nat;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox Cmb_R_City;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker Txt_Sbirth_Date;
        private System.Windows.Forms.DateTimePicker Txt_Doc_S_Exp;
        private System.Windows.Forms.DateTimePicker Txt_Doc_S_Date;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.ComboBox cmb_Gender_id;
        private System.Windows.Forms.ComboBox cmb_s_nat;
        private System.Windows.Forms.TextBox Txt_T_Purpose;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.TextBox txt_Mother_name;
        private System.Windows.Forms.TextBox Txt_mail;
        private System.Windows.Forms.TextBox Txt_S_Birth_Place;
        private System.Windows.Forms.TextBox Txt_Doc_S_Issue;
        private System.Windows.Forms.TextBox Txt_S_Phone;
        private System.Windows.Forms.TextBox TxtS_State;
        private System.Windows.Forms.TextBox TxtS_Post_Code;
        private System.Windows.Forms.TextBox Txts_street;
        private System.Windows.Forms.TextBox txtS_Suburb;
        private System.Windows.Forms.TextBox Txt_Sender;
        private System.Windows.Forms.ComboBox Cmb_Case_Purpose;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_S_Doc_No;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Cmb_S_Doc_Type;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox Cmb_Code_phone_S;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Cmb_S_City;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tabControl1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.ComboBox Cbo_rem_path;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Cmb_T_City;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.ComboBox Cmb_PR_Cur_Id;
        private System.Windows.Forms.ComboBox Cmb_R_CUR_ID;
        private System.Windows.Forms.Sample.DecimalTextBox Txtr_amount;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox cmb_comm_type;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox Cmb_Comm_Cur;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Com_Amount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.TextBox Tex_Social_ID;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox resd_cmb;
        private System.Windows.Forms.ComboBox cmb_job_sender;
        private System.Windows.Forms.ComboBox cmb_job_receiver;
        private System.Windows.Forms.TextBox Txt_S_details_job;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox txt_notes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxT_Agent_Real_Cur;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Sample.DecimalTextBox TxT_Agent_Real_Comm_Amount;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Sample.DecimalTextBox TxT_Agent_Real_Rem_Amount;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Sample.DecimalTextBox TxT_Agent_Rate;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_branch;
        private System.Windows.Forms.ComboBox Cbo_Cust_Id;
        private System.Windows.Forms.TextBox tex_reason;
        private System.Windows.Forms.Label lab_reason;
        private System.Windows.Forms.ComboBox cbo_order_type;
        private System.Windows.Forms.Label txt_srch;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

    }
}