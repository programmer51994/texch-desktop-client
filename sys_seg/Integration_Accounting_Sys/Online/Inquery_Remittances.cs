﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys.remittences_online
{
    public partial class Inquery_Remittances : Form
    {
        BindingSource Bs_rem = new BindingSource();
        BindingSource binding_Cbo = new BindingSource();
        BindingSource Bs_Grdrem = new BindingSource();
        DataTable Dt_Rem = new DataTable();
        DataTable Rpt_dt = new DataTable();
        bool Change = false;
        bool Change_grd = false;
        // int Mcase_id = 0;
        // bool chang_GRem = false;
        bool chang_Gcase = false;
        Int16 cmb_op = 0;
        // bool Change = false;
        //  decimal param_Exch_rate_rem = 0;
        //   decimal param_Exch_rate_com = 0;
        string Gender = "";
        string _Date = "";
        string local_Cur = "";
        string forgin_Cur = "";
        // string SqlTxt = "";
        int Oper_Id = 0;
        string Vo_No = "";
        //string Rem_no = "";
        //Int32 case_id_current = 0;
        Int32 Row_id_current = 0;

        string User = "";
        // string Exch_rate_com = "";
        // double Total_Amount = 0;
        double Rem_Amount = 0;
        string term = "";
        string com_Cur = "";
        //string local_ACur = "";
        //string forgin_ACur = "";
        //string comm_Acur = "";
        //string Frm_id = "";
        int int_nrecdate = 0;
        // int code_rem = 0;
        bool CHN = false;
        //int procker=0;
        //string prockr_name="";
        //string procker_ename="";
        string s_Ejob = "";
        string r_Ejob = "";
        string S_Social_No = "";
        string R_Social_No = "";
        public static int R_TypeId = 0;
        public static Int16 remit_flag = 0;
       
        public Inquery_Remittances(int R_Type_Id, Int16 rem_flag)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grdrec_rem.AutoGenerateColumns = false;
            Grd_Rem_Cases.AutoGenerateColumns = false;
            R_TypeId = R_Type_Id;
            remit_flag = rem_flag;
        }

        private void Inquery_Remittances_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
            if (R_TypeId == 1)
            {
                Column83.Visible = true;
                Column84.Visible = true;
                Column85.Visible = true;
                Column86.Visible = true;
                Column87.Visible = true;
                Column88.Visible = true;
                Column90.Visible = true; 
                Column91.Visible = true;
                Column92.Visible = true;
                Column93.Visible = true;
                Column94.Visible = true;
                Column89.Visible = true;
                Column74.Visible = true;
                Column73.Visible = false; //الجهة المصدرة
                Column27.HeaderText = "اسم الوكيل الدافع"; // الوكيل المصدر
            }
            else
            {
                Column83.Visible = false;//عمولة وارد
                Column84.Visible = false;//عملة عمولة 
                Column85.Visible = false;//نوع العمولة
                Column86.Visible = false;//عمولة وارد2
                Column87.Visible = false;//عملة عمولة2 
                Column88.Visible = false;//نوع العمولة2
                Column90.Visible = false;//مستخدم تاكيد دفع2
                Column91.Visible = false;//طريق دفع
                Column92.Visible = false;//طريق دفع2
                Column93.Visible = false;//معلومات دفع
                Column94.Visible = false;//معلومات دفع2
                Column89.Visible = false;//مستخدم تاكيد دفع  
                Column74.Visible = false;//الجهة الدافعة
                Column73.Visible = true; //الجهة المصدرة
            }

            if (remit_flag == 1 && R_TypeId == 0)
            {
                Column19.Visible = true;//  الجهة المسددة
            }
            if (remit_flag == 11)
            {
                Column19.Visible = false;//  الجهة المسددة
            }
            if (remit_flag == 0 && R_TypeId == 0)
            {
                Column19.Visible = true;//  الجهة المسددة
            }

            Bs_rem.DataSource = connection.SQLDS.Tables["Query_remittences_tbl"];
         
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);
            if (connection.SQLDS.Tables["Query_remittences_tbl"].Rows.Count > 0)
            {


                Change = true;
                Grdrec_rem_SelectionChanged(null, null);
                CHN = false;
                binding_Cbo.DataSource = connection.SQLDS.Tables["Query_remittences_tbl"].DefaultView.ToTable(true, "R_cur_id", "R_ACUR_NAME", "R_ECUR_NAME").Select().CopyToDataTable();
                Cbo_Rcur.DataSource = binding_Cbo;
                Cbo_Rcur.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
                Cbo_Rcur.ValueMember = "R_cur_id";
                CHN = true;
                Cbo_Rcur_SelectedIndexChanged(null, null);

            }

      

            if (connection.Lang_id == 2)
            {
                Column10.DataPropertyName = "e_local_international";
                Column13.DataPropertyName = "e_Send_rem_flag";
                Column2.DataPropertyName = "R_ECUR_NAME";
                Column8.DataPropertyName = "t_ECITY_NAME";
                Column9.DataPropertyName = "S_ECITY_NAME";
                Column33.DataPropertyName = "r_ECITY_NAME";
                Column5.DataPropertyName = "ECASE_NA";
                Column29.DataPropertyName = "s_E_NAT_NAME";
                Column34.DataPropertyName = "r_ECOUN_NAME";
                Column60.DataPropertyName = "s_ECUR_NAME";
                Column62.DataPropertyName = "c_ECUR_NAME";
                Column14.DataPropertyName = "Ename_rem_state";
                Column4.DataPropertyName = "PR_ECUR_NAME";
                dataGridViewTextBoxColumn3.DataPropertyName = "R_ECUR_NAME";
                dataGridViewTextBoxColumn4.DataPropertyName = "PR_ECUR_NAME";
                dataGridViewTextBoxColumn5.DataPropertyName = "t_ECITY_NAME";
                dataGridViewTextBoxColumn6.DataPropertyName = "S_ECITY_NAME";
                dataGridViewTextBoxColumn7.DataPropertyName = "ECASE_NA";
                dataGridViewTextBoxColumn14.DataPropertyName = "S_ECOUN_NAME";
                dataGridViewTextBoxColumn25.DataPropertyName = "s_E_NAT_NAME";
                dataGridViewTextBoxColumn30.DataPropertyName = "r_ECOUN_NAME";
                 Column73.DataPropertyName = "S_Ecust_name";
                 Column74.DataPropertyName = "D_Ecust_name";
                 Column27.DataPropertyName = "ECUST_NAME";
                Column78.DataPropertyName = "S_Ecust_name";
                Column79.DataPropertyName = "D_Ecust_name";
                Column84.DataPropertyName = "i_SECUR_NAME";
                Column85.DataPropertyName = "i_SEType_name";
                Column87.DataPropertyName = "i_SECUR_NAME";
                Column88.DataPropertyName = "i_SEType_name";
                Column91.DataPropertyName = "rem_pay_ename";
                Column92.DataPropertyName = "rem_pay_ename";
                Column96.DataPropertyName = "cur_Ename_exch_agent";
                if (R_TypeId == 1)
                {
                    Column27.HeaderText = "Paid Agent Name"; // الوكيل المصدر

                }
            }

        }

        private void Grdrec_rem_SelectionChanged(object sender, EventArgs e)
        {
            Change_grd = false;

            Change_grd = false;

            if (Change)
            {

              

                Grdrec_rem.DataSource = Bs_rem;
                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtScity.DataBindings.Clear();
                txt_resd_sen.DataBindings.Clear();
                Txt_S_Birth.DataBindings.Clear();
                Txt_S_Birth_Place.DataBindings.Clear();
                Txt_s_job.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                txt_purpose_sen.DataBindings.Clear();
                Txt_S_doc_type.DataBindings.Clear();
                txt_social_sen.DataBindings.Clear();
                Txt_S_doc_no.DataBindings.Clear();
                Txt_S_doc_Date.DataBindings.Clear();
                Txt_S_doc_issue.DataBindings.Clear();
                Txt_Relionship.DataBindings.Clear();
                Txt_Soruce_money.DataBindings.Clear();
                Txt_R_Name.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                Txt_R_Nat.DataBindings.Clear();
                Txt_R_City.DataBindings.Clear();
                Txt_r_birthdate.DataBindings.Clear();
                Txt_R_Birth_Place.DataBindings.Clear();
                Txt_r_job.DataBindings.Clear();
                Txt_R_address.DataBindings.Clear();
                txt_purpose_rec.DataBindings.Clear();
                Txt_R_doc_type.DataBindings.Clear();
                Txt_R_doc_no.DataBindings.Clear();
                Txt_R_doc_date.DataBindings.Clear();
                txt_social_rec.DataBindings.Clear();
                Txt_r_doc_issue.DataBindings.Clear();
                txt_resd_rec.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                Txt_Discreption.DataBindings.Clear();
                Txt_R_Relation.DataBindings.Clear();
                Txt_S_details_job.DataBindings.Clear();
                Txt_R_details_job.DataBindings.Clear();
                Txt_R_notes.DataBindings.Clear();


                string s_doc_date = Convert.ToString(((DataRowView)Bs_rem.Current).Row["S_doc_ida"].ToString());
                Txt_S_doc_Date.Text = s_doc_date;

                string S_Birth = Convert.ToString(((DataRowView)Bs_rem.Current).Row["sbirth_date"].ToString());
                Txt_S_Birth.Text = S_Birth;

                string r_Birth = Convert.ToString(((DataRowView)Bs_rem.Current).Row["rbirth_date"].ToString());
                Txt_r_birthdate.Text = r_Birth;

                string R_doc_date = Convert.ToString(((DataRowView)Bs_rem.Current).Row["r_doc_ida"].ToString());
                Txt_R_doc_date.Text = R_doc_date;

                TxtS_name.DataBindings.Add("Text", Bs_rem, "S_name");
                TxtS_phone.DataBindings.Add("Text", Bs_rem, "S_phone");
                Txts_nat.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                TxtScity.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                txt_resd_sen.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "resd_flag_Aname" : "resd_flag_Ename");
                Txt_S_Birth.DataBindings.Add("Text", Bs_rem, "sbirth_date");
                Txt_S_Birth_Place.DataBindings.Add("Text", Bs_rem, "SBirth_Place");
                Txt_s_job.DataBindings.Add("Text", Bs_rem, "s_job");
                TxtS_address.DataBindings.Add("Text", Bs_rem, "S_address");
                txt_purpose_sen.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename");
                Txt_S_doc_type.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                txt_social_sen.DataBindings.Add("Text", Bs_rem, "S_Social_No");
                Txt_S_doc_no.DataBindings.Add("Text", Bs_rem, "S_doc_no");
                Txt_S_doc_Date.DataBindings.Add("Text", Bs_rem, "S_doc_ida");
                Txt_S_doc_issue.DataBindings.Add("Text", Bs_rem, "S_doc_issue");
                Txt_Relionship.DataBindings.Add("Text", Bs_rem, "Relation_S_R");
                Txt_Soruce_money.DataBindings.Add("Text", Bs_rem, "Source_money");
                Txt_R_Name.DataBindings.Add("Text", Bs_rem, "r_name");
                Txt_R_Phone.DataBindings.Add("Text", Bs_rem, "R_phone");
                Txt_R_Nat.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                Txt_R_City.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                Txt_r_birthdate.DataBindings.Add("Text", Bs_rem, "rbirth_date");
                Txt_R_Birth_Place.DataBindings.Add("Text", Bs_rem, "TBirth_Place");
                Txt_r_job.DataBindings.Add("Text", Bs_rem, "r_job");
                Txt_R_address.DataBindings.Add("Text", Bs_rem, "r_address");
                txt_purpose_rec.DataBindings.Add("Text", Bs_rem, "R_T_purpose");
                Txt_R_doc_type.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA");
                txt_social_rec.DataBindings.Add("Text", Bs_rem, "R_Social_No");
                Txt_R_doc_no.DataBindings.Add("Text", Bs_rem, "r_doc_no");
                Txt_R_doc_date.DataBindings.Add("Text", Bs_rem, "r_doc_ida");
                Txt_r_doc_issue.DataBindings.Add("Text", Bs_rem, "r_doc_issue");
                txt_resd_rec.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename");
                Txt_Purpose.DataBindings.Add("Text", Bs_rem, "T_purpose");
                Txt_Discreption.DataBindings.Add("Text", Bs_rem, "s_notes");
                Txt_R_Relation.DataBindings.Add("Text", Bs_rem, "r_Relation");
                Txt_S_details_job.DataBindings.Add("Text", Bs_rem, "s_details_job");
                Txt_R_details_job.DataBindings.Add("Text", Bs_rem, "r_details_job");
                Txt_R_notes.DataBindings.Add("Text", Bs_rem, "r_notes");

                string Rem_no = ((DataRowView)Bs_rem.Current).Row["Rem_no"].ToString();
               

                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Query_remittences_tbl1"];
                try
                {
                    Change_grd = false;

                    DataTable Dt1 = Dt.Select("Rem_no = '" + Rem_no + "'").CopyToDataTable();
                    if (Dt1.Rows.Count > 0)
                    {
                        Bs_Grdrem.DataSource = Dt1;
                        Grd_Rem_Cases.DataSource = Bs_Grdrem;
                        Change_grd = true;
                    }
                    else
                    { Grd_Rem_Cases.DataSource = new DataTable(); }
                }
                catch { Grd_Rem_Cases.DataSource = new DataTable(); }

                if (Grd_Rem_Cases.RowCount > 0)
                { button1.Visible = true; }
                else
                { button1.Visible = false; }
            }
        }

        private void Btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Print_All_Click(object sender, EventArgs e)
        {
            DataTable Dt = new DataTable();
            
            DataTable DT_Header = new DataTable();
            DataTable DT_Header_eng = new DataTable();


            Dt = connection.SQLDS.Tables["Query_remittences_tbl"];
            try
            {
              
                    //header for the table to be exported
                if (connection.Lang_id == 1)
                {
                   

                    DT_Header.Columns.Add("رقم الحوالة");
                    DT_Header.Columns.Add("نوع الارسال");
                    DT_Header.Columns.Add("حالة الارسال");
                    DT_Header.Columns.Add("حالة الحوالة");
                    DT_Header.Columns.Add("مبلغ الحوالة");
                    DT_Header.Columns.Add("عملة الحوالة");
                    DT_Header.Columns.Add("عملة التسليم");
                    DT_Header.Columns.Add("الجهة المصدرة");
                    DT_Header.Columns.Add("الجهة الدافعة");
                    DT_Header.Columns.Add("اسم الوكيل المصدر");
                    DT_Header.Columns.Add("الجهة المسددة للحوالة");
                    DT_Header.Columns.Add("مدينة الارسال");
                    DT_Header.Columns.Add("مدينة الاستلام");
                    DT_Header.Columns.Add("تاريخ الحالة");
                    DT_Header.Columns.Add("تاريخ تنظيم السجلات");
                    DT_Header.Columns.Add("اسم المرسل");
                    DT_Header.Columns.Add("هاتف المرسل");
                    DT_Header.Columns.Add("عنوان المرسل");
                    DT_Header.Columns.Add("زقاق المرسل");
                    DT_Header.Columns.Add("الحي");
                    DT_Header.Columns.Add("الرقم البريدي");
                    DT_Header.Columns.Add("المحافظة");
                    DT_Header.Columns.Add("الرقم الوطني للمرسل");
                    DT_Header.Columns.Add("نوع وثيقة المرسل");
                    DT_Header.Columns.Add("رقم وثيقة المرسل");
                    DT_Header.Columns.Add("تاريخ اصدار وثيقة المرسل");
                    DT_Header.Columns.Add("تاريخ انتهاء هوية المرسل");
                    DT_Header.Columns.Add("محل اصدار الوثيقة للمرسل");
                    DT_Header.Columns.Add("ايميل المرسل");
                    DT_Header.Columns.Add("جنسية المرسل");
                    DT_Header.Columns.Add("مكان تولد المرسل");
                    DT_Header.Columns.Add("تولد المرسل");
                    DT_Header.Columns.Add("مهنة المرسل");
                    DT_Header.Columns.Add("تفاصيل مهنة المرسل");
                    DT_Header.Columns.Add("ملاحظات المرسل");
                    DT_Header.Columns.Add("اسم المستلم");
                    DT_Header.Columns.Add("الاسم كما في الهوية");
                    DT_Header.Columns.Add("هاتف المستلم");
                    DT_Header.Columns.Add("مدينة المستلم");
                    DT_Header.Columns.Add("بلد المستلم");
                    DT_Header.Columns.Add("عنوان المستلم");
                    DT_Header.Columns.Add("زقاق المستلم");
                    DT_Header.Columns.Add("الحي للمستلم");
                    DT_Header.Columns.Add("الرمز البريدي للمستلم");
                    DT_Header.Columns.Add("محافظة المستلم");
                    DT_Header.Columns.Add("الرقم الوطني للمستلم");
                    DT_Header.Columns.Add("نوع وثيقة المستلم");
                    DT_Header.Columns.Add("رقم وثيقة المستلم");
                    DT_Header.Columns.Add("تاريخ إصدار الوثيقة");
                    DT_Header.Columns.Add("تاريخ إنتهاء الوثيقة");
                    DT_Header.Columns.Add("نوع الوثيقة");
                    DT_Header.Columns.Add("ايميل المستلم");
                    DT_Header.Columns.Add("تولد المستلم");
                    DT_Header.Columns.Add("مهنة المستلم");
                    DT_Header.Columns.Add("تفاصيل مهنة المستلم");
                    DT_Header.Columns.Add("ملاحظات المستلم");
                    DT_Header.Columns.Add("رقم السند");
                    DT_Header.Columns.Add("تاريخ السجلات");
                    DT_Header.Columns.Add("تاريخ انشاء الحوالة في مركز الاونلاين");
                    DT_Header.Columns.Add("عمولة المرسل");
                    DT_Header.Columns.Add("عملة الارسال");
                    DT_Header.Columns.Add("مبلغ العمولة");
                    DT_Header.Columns.Add("عملة العمولة");
                    DT_Header.Columns.Add("سعر التعادل");
                    DT_Header.Columns.Add("مصدر المال");
                    DT_Header.Columns.Add("علاقة المرسل بالمستلم");
                    DT_Header.Columns.Add("علاقة المستلم بالمرسل");
                    DT_Header.Columns.Add("غرض الارسال");
                    DT_Header.Columns.Add("غرض الاستلام");
                    DT_Header.Columns.Add("العمولة الواردة");
                    DT_Header.Columns.Add("عملة العمولة الواردة");
                    DT_Header.Columns.Add("نوع العمولة الواردة");
                    DT_Header.Columns.Add("المستخدم");
                    DT_Header.Columns.Add("مستخدم تأكيد الحوالة");
                    DT_Header.Columns.Add("مستخدم تأكيد الحوالة الواردة");
                    DT_Header.Columns.Add("طريقة الدفع");
                    DT_Header.Columns.Add("معلومات عن طريق الدفع");
                    DT_Header.Columns.Add("المبلغ الفعلي لأستلامه");
                    DT_Header.Columns.Add("عملة المبلغ الفعلي");
                    DT_Header.Columns.Add("العمولة الفعلية");
                    DT_Header.Columns.Add("سعر تعادل الوكيل");

                    DT_Header.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }

                if (connection.Lang_id == 2)
                {

                    
                    DT_Header_eng.Columns.Add("Rem. No.");
                    DT_Header_eng.Columns.Add("Send Rem. Type");
                    DT_Header_eng.Columns.Add("Send Status");
                    DT_Header_eng.Columns.Add("rem state");
                    DT_Header_eng.Columns.Add("Rem Amount");
                    DT_Header_eng.Columns.Add("Rem Curreny");
                    DT_Header_eng.Columns.Add("Deliver Currency");
                    DT_Header_eng.Columns.Add("Sender Source");
                    DT_Header_eng.Columns.Add("Paid Source");
                    DT_Header_eng.Columns.Add("Sender Agent Name");
                    DT_Header_eng.Columns.Add("Customer Paid Rem.");
                    DT_Header_eng.Columns.Add("Sender City");
                    DT_Header_eng.Columns.Add("Deliver City");
                    DT_Header_eng.Columns.Add("Case Date");
                    DT_Header_eng.Columns.Add("Organize Date records");
                    DT_Header_eng.Columns.Add("Sender Name");
                    DT_Header_eng.Columns.Add("Sender Phone");
                    DT_Header_eng.Columns.Add("Address");
                    DT_Header_eng.Columns.Add("Street");
                    DT_Header_eng.Columns.Add("Subrub");
                    DT_Header_eng.Columns.Add("Sender Post Code");
                    DT_Header_eng.Columns.Add("City");
                    DT_Header_eng.Columns.Add("Sender Social No");
                    DT_Header_eng.Columns.Add("Sender Doc Type");
                    DT_Header_eng.Columns.Add("Sender document no.");
                    DT_Header_eng.Columns.Add("Sender Doc  Date");
                    DT_Header_eng.Columns.Add("Sender End Doc. Date");
                    DT_Header_eng.Columns.Add("Sender Doc Issue");
                    DT_Header_eng.Columns.Add("Sender Email");
                    DT_Header_eng.Columns.Add("Sender Nationalty");
                    DT_Header_eng.Columns.Add("Sender birth place");
                    DT_Header_eng.Columns.Add("Sender birth date");
                    DT_Header_eng.Columns.Add("Sender  job");
                    DT_Header_eng.Columns.Add("details sender job");
                    DT_Header_eng.Columns.Add("Sender notes");
                    DT_Header_eng.Columns.Add("Receiver name");
                    DT_Header_eng.Columns.Add("Real Paid Name");
                    DT_Header_eng.Columns.Add("Receiver  Phone");
                    DT_Header_eng.Columns.Add("Receiver city");
                    DT_Header_eng.Columns.Add("Receiver country");
                    DT_Header_eng.Columns.Add("Receiver address");
                    DT_Header_eng.Columns.Add("Receiver Street");
                    DT_Header_eng.Columns.Add("Receiver  Suburb");
                    DT_Header_eng.Columns.Add("Receiver Post Code");
                    DT_Header_eng.Columns.Add("Reciver's city");
                    DT_Header_eng.Columns.Add("Receiver Social No");
                    DT_Header_eng.Columns.Add("reciever Doc Type");
                    DT_Header_eng.Columns.Add("Receiver Doc. no.");
                    DT_Header_eng.Columns.Add("Doc. issue date");
                    DT_Header_eng.Columns.Add("Doc. issue ex. date");
                    DT_Header_eng.Columns.Add("Doc. type");
                    DT_Header_eng.Columns.Add("Receiver E_Mail");
                    DT_Header_eng.Columns.Add("Receiver birthdate");
                    DT_Header_eng.Columns.Add("Receiver  job");
                    DT_Header_eng.Columns.Add("details receiver  job");
                    DT_Header_eng.Columns.Add("Receiver notes");
                    DT_Header_eng.Columns.Add("Voucher number");
                    DT_Header_eng.Columns.Add("In records date");
                    DT_Header_eng.Columns.Add("The date of creation of rem in online");
                    DT_Header_eng.Columns.Add("sender commission");
                    DT_Header_eng.Columns.Add("Sending currency");
                    DT_Header_eng.Columns.Add("Commission amount");
                    DT_Header_eng.Columns.Add("Commission currency");
                    DT_Header_eng.Columns.Add("Exchange rate");
                    DT_Header_eng.Columns.Add("Money source");
                    DT_Header_eng.Columns.Add("Sender / Receiver relationship");
                    DT_Header_eng.Columns.Add("Sen./Rec. Relation");
                    DT_Header_eng.Columns.Add("Sending purpose");
                    DT_Header_eng.Columns.Add("Purpose");
                    DT_Header_eng.Columns.Add("Commission In");
                    DT_Header_eng.Columns.Add("Currency Commission In");
                    DT_Header_eng.Columns.Add("Type Commission In");
                    DT_Header_eng.Columns.Add("User");
                    DT_Header_eng.Columns.Add("Confirm user Rem");
                    DT_Header_eng.Columns.Add("User Confirm In Rem");
                    DT_Header_eng.Columns.Add("Payment Type");
                    DT_Header_eng.Columns.Add("Payment Information");
                    DT_Header_eng.Columns.Add("Real Agent Amount");
                    DT_Header_eng.Columns.Add("Real Agent Currency");
                    DT_Header_eng.Columns.Add("Real Agent Commition");
                    DT_Header_eng.Columns.Add("Agent Exchange Rate");


                    DT_Header_eng.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }



                //data table to be exported

                DataTable Exprt_DT = connection.SQLDS.Tables["Query_remittences_tbl"].DefaultView.ToTable(false, "rem_no", connection.Lang_id == 1 ? "a_local_international" : "e_local_international",
                connection.Lang_id == 1 ? "a_Send_rem_flag" : "e_Send_rem_flag", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME",
                connection.Lang_id == 1 ? "S_Acust_name" : "S_Ecust_name", connection.Lang_id == 1 ? "D_Acust_name" : "D_Ecust_name", connection.Lang_id == 1 ? "ACUST_NAME" : "ACUST_NAME", connection.Lang_id == 1 ? "cust_paid_aname" : "cust_paid_aname",
                connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME", "Case_Date", "NDate1",
                    "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue",
                    "s_Email", connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME", "SBirth_Place", "sbirth_date", connection.Lang_id == 1 ? "s_job" : "s_Ejob", "s_details_job", "s_notes",
                    "r_name", "Real_Paid_Name", "R_phone", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb",
                    "r_Post_Code", "r_State", "R_Social_No", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email",
                    "rbirth_date", connection.Lang_id == 1 ? "r_job" : "r_Ejob", "r_details_job", "r_notes", "vo_no", "In_rec_date", "create_date",
                    "s_ocomm", connection.Lang_id == 1 ? "s_ACUR_NAME" : "s_ECUR_NAME", "c_ocomm", connection.Lang_id == 1 ? "c_ACUR_NAME" : "c_ECUR_NAME", "rate_online",
                    "Source_money", "Relation_S_R", "r_Relation", "T_purpose", "R_T_purpose",
                    "i_SOComm", connection.Lang_id == 1 ? "i_SACUR_NAME" : "i_SECUR_NAME", connection.Lang_id == 1 ? "i_SAType_name" : "i_SEType_name",
                    "user_name", "User_Confirm_Name", "User_INConfirm_Name", connection.Lang_id == 1 ? "rem_pay_aname" : "rem_pay_ename", "payment_info",
                    "real_agent_amount", connection.Lang_id == 1 ? "cur_Aname_exch_agent" : "cur_Ename_exch_agent","agent_comm_amount","exchange_agent_amount").Select().CopyToDataTable();

                if (Exprt_DT.Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا يـــوجد قيـــود للتـــصدير" : "There is no data to be exported", MyGeneral_Lib.LblCap);
                    return;
                }

                if (connection.Lang_id == 1)
                // exporting to textfile
                {
                    MyExports.To_TxtFile(Exprt_DT, DT_Header);
                }

                else
                {
                    MyExports.To_TxtFile(Exprt_DT, DT_Header_eng);
                }

            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدث خطأ ما" : "Something went wrong", MyGeneral_Lib.LblCap);
                return;

            }
        }     
        private void Print_Rem_Cases_Click(object sender, EventArgs e)
        {
            //Rem_no = ((DataRowView)Bs_rem.Current).Row["Rem_no"].ToString(); 
            //case_id_current = Convert.ToInt32(((DataRowView)Bs_rem.Current).Row["case_id"].ToString());

            Row_id_current = Convert.ToInt32(((DataRowView)Bs_rem.Current).Row["Row_id"].ToString());

            print_Rpt_Pdf_NEw(1);
           
        }

        private void print_Rpt_Pdf_NEw(Int16 Print_flag = 0)
        {
            //-------------------------report

        if (connection.SQLDS.Tables.Contains("Rem_Info"))
            connection.SQLDS.Tables.Remove("Rem_Info");

            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int int_nrecdate = 0;
            string phone = "";
            int procker = 0;
            string prockr_name = "";
            string procker_ename = "";
            string local_Cur1 = "";
            string local_ECur1 = "";
            string forgin_Cur1 = "";
            string forgin_ECur1 = "";
            string Com_ToWord = "";
            string Com_ToEWord = "";
            double com_amount = 0;
            string com_Cur_code = "";
            byte chk_s_ocomm = 0;
            string Rem_Pay_Type_Aname = "";     
            string Rem_Pay_Type_Ename = "";
            int Rem_Pay_Type_id = 0;
            int Paid = 0;
            string Paid_name = "";
            string Paid_ename = "";


            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);

            if (Print_flag == 1) //-طباعة حالة الحوالة
            {
                
                DataTable Rem_Info = new DataTable();
                //  Rem_Info = connection.SQLDS.Tables["Query_remittences_tbl"].Select("Rem_no = '" + Rem_no + "' and  case_id = " + case_id_current).CopyToDataTable();
                Rem_Info = connection.SQLDS.Tables["Query_remittences_tbl"].Select("Row_id = " + Row_id_current ).CopyToDataTable();

                string sql_report_txt = "select * from Reports_setting_Tbl";
                connection.SqlExec(sql_report_txt, "Reports_setting_Tbl");
                if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار اعدادات الطباعة" : "please add reporting setting", MyGeneral_Lib.LblCap);
                    return;
                }

                else
                {


                    User = Rem_Info.Rows[0]["user_name"].ToString();

                 if (connection.SQLDS.Tables.Contains("cust_id_Tbl"))
                     connection.SQLDS.Tables.Remove("cust_id_Tbl");

                    Int32 cust_id_current = 0;

                    string sql_cust_id = " Select  isnull(CUST_ID,0) as CUST_ID From CUSTOMERS Where Cust_Type_ID not in(6,7,50) and cust_id = " + Rem_Info.Rows[0]["CUST_ID"];
                    connection.SqlExec(sql_cust_id, "cust_id_Tbl");


                     if (connection.SQLDS.Tables["cust_id_Tbl"].Rows.Count > 0)
                     {
                       cust_id_current = Convert.ToInt32(connection.SQLDS.Tables["cust_id_Tbl"].Rows[0]["CUST_ID"]);

                     }

                     else
                     { cust_id_current = 0; }

                     if (cust_id_current != 0 )
                    {
                        procker = cust_id_current;
                        prockr_name = Rem_Info.Rows[0]["ACUST_NAME"].ToString();
                        procker_ename = Rem_Info.Rows[0]["eCUST_NAME"].ToString();
                    }
                    else
                    {
                        procker = 0;
                        prockr_name = "";
                        procker_ename = "";
                    }

                    if (Convert.ToInt32(Rem_Info.Rows[0]["cust_paid_id"]) != 0)
                    {
                        Paid = Convert.ToInt32(Rem_Info.Rows[0]["cust_paid_id"]);
                        Paid_name = Rem_Info.Rows[0]["cust_paid_aname"].ToString();
                        Paid_ename = Rem_Info.Rows[0]["cust_paid_ename"].ToString();
                    }
                    //if (Convert.ToInt32(Rem_Info.Rows[0]["cust_paid_id"]) != 0)
                    //{
                    //    procker = Convert.ToInt32(Rem_Info.Rows[0]["cust_paid_id"]);
                    //    prockr_name = Rem_Info.Rows[0]["cust_paid_aname"].ToString();
                    //    procker_ename = Rem_Info.Rows[0]["cust_paid_ename"].ToString();
                    //}

                    //if (Convert.ToInt32(Rem_Info.Rows[0]["sub_cust_id"]) != 0)
                    //{
                    //    procker = Convert.ToInt32(Rem_Info.Rows[0]["sub_cust_id"]);
                    //    prockr_name = Rem_Info.Rows[0]["sub_cust_Aname"].ToString();
                    //    procker_ename = Rem_Info.Rows[0]["sub_cust_ename"].ToString();
                    //}


                    local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                    local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
                    local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

                    forgin_Cur = Rem_Info.Rows[0]["Cur_Code"].ToString();  
                    forgin_Cur1 = Rem_Info.Rows[0]["R_ACUR_NAME"].ToString();
                    forgin_ECur1 = Rem_Info.Rows[0]["R_ECUR_NAME"].ToString();

                    s_Ejob = Rem_Info.Rows[0]["s_Ejob"].ToString();
                    r_Ejob = Rem_Info.Rows[0]["r_Ejob"].ToString();
                    S_Social_No = Rem_Info.Rows[0]["S_Social_No"].ToString();
                    R_Social_No = Rem_Info.Rows[0]["R_Social_No"].ToString();




                    Vo_No = Rem_Info.Rows[0]["vo_no"].ToString();

                    if (Convert.ToDouble(Rem_Info.Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                    {
                        Rem_Amount = Convert.ToDouble(Rem_Info.Rows[0]["R_amount"]);
                        ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                        if (Convert.ToDouble(Rem_Info.Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                        {
                            com_amount = Convert.ToDouble(Rem_Info.Rows[0]["s_ocomm"]);
                            com_Cur_code = Rem_Info.Rows[0]["com_code"].ToString();
                            ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                            Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + Rem_Info.Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                            Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + Rem_Info.Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                        }
                    }
                    else// عملة محلية
                    {
                        Rem_Amount = Convert.ToDouble(Rem_Info.Rows[0]["rem_amount_cust"]);
                        ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                        if (Convert.ToDouble(Rem_Info.Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                        {
                            com_amount = Convert.ToDouble(Rem_Info.Rows[0]["com_amount_cust"]);
                            com_Cur_code = local_Cur;
                            ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                            Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                            Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                        }
                    }

                    Vo_No = Rem_Info.Rows[0]["vo_no"].ToString();


                    term = TxtTerm_Name.Text;
                    #region Add_rem
                    //---------------f
                    if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 101 || Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 103 || Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 1)
                    {
                        Oper_Id = Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]);
                        decimal txt_comm_amount = 0;

                        chk_s_ocomm = 0;
                        txt_comm_amount = 0;
                        try
                        {

                            Add_Rem_cash_daily_rpt_1 ObjRpt = new Add_Rem_cash_daily_rpt_1();
                            Add_Rem_cash_daily_rpt_eng_1 ObjRptEng = new Add_Rem_cash_daily_rpt_eng_1();


                            DataTable Dt_Param = CustomControls.CustomParam_Dt();
                            Dt_Param.Rows.Add("_Date", _Date);
                            Dt_Param.Rows.Add("local_Cur", local_Cur);
                            Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                            Dt_Param.Rows.Add("Vo_No", Vo_No);
                            Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                            Dt_Param.Rows.Add("User", User);
                            Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                            Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                            Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                            Dt_Param.Rows.Add("term", term);
                            Dt_Param.Rows.Add("Frm_id", "Main");
                            Dt_Param.Rows.Add("phone", phone);
                            Dt_Param.Rows.Add("com_cur", com_Cur_code);
                            Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                            Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                            Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                            Dt_Param.Rows.Add("procker", procker);
                            Dt_Param.Rows.Add("procker_ename", procker_ename);
                            Dt_Param.Rows.Add("prockr_name", prockr_name);
                            Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                            Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                            Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                            Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                            Dt_Param.Rows.Add("Anote_report", Anote_report);
                            Dt_Param.Rows.Add("Enote_report", Enote_report);
                            Dt_Param.Rows.Add("chk_s_ocomm", chk_s_ocomm);
                            Dt_Param.Rows.Add("txt_comm_amount", txt_comm_amount);
                            Dt_Param.Rows.Add("Paid", Paid);
                            Dt_Param.Rows.Add("Paid_ename", Paid_ename);
                            Dt_Param.Rows.Add("Paid_name", Paid_name);

                            Rem_Info.TableName = "Rem_Info";
                            connection.SQLDS.Tables.Add(Rem_Info);
                            connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);


                            RptLangFrm.ShowDialog(this);

                        }
                        catch
                        { }
                    }


                    #endregion

                    #region IN_rem
                    if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 2)
                    {

                        Oper_Id = Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]);
                        try
                        {

                            Delivery_Rem_rpt_1 ObjRpt = new Delivery_Rem_rpt_1();
                            Delivery_Rem_rpt_eng_1 ObjRptEng = new Delivery_Rem_rpt_eng_1();
                            string Real_Paid_Name = "";
                            Real_Paid_Name = Rem_Info.Rows[0]["Real_Paid_Name"].ToString();

                            DataTable Dt_Param = CustomControls.CustomParam_Dt();
                            Dt_Param.Rows.Add("_Date", _Date);
                            Dt_Param.Rows.Add("local_Cur", local_Cur);
                            Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                            Dt_Param.Rows.Add("Vo_No", Vo_No);
                            Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                            Dt_Param.Rows.Add("User", User);
                            Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                            Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                            Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                            Dt_Param.Rows.Add("term", term);
                            Dt_Param.Rows.Add("Frm_id", "Main");
                            Dt_Param.Rows.Add("phone", phone);
                            Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                            Dt_Param.Rows.Add("procker", procker);
                            Dt_Param.Rows.Add("procker_ename", procker_ename);
                            Dt_Param.Rows.Add("prockr_name", prockr_name);
                            Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                            Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                            Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                            Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                            Dt_Param.Rows.Add("Real_Paid_name", Real_Paid_Name);
                            Dt_Param.Rows.Add("Anote_report", Anote_report);
                            Dt_Param.Rows.Add("Enote_report", Enote_report);
                            Dt_Param.Rows.Add("Rem_Pay_Type_Aname", ((DataRowView)Bs_rem.Current).Row["rem_pay_aname"]);
                            Dt_Param.Rows.Add("Rem_Pay_Type_Ename", ((DataRowView)Bs_rem.Current).Row["rem_pay_ename"]);
                            Dt_Param.Rows.Add("Rem_Pay_Type_id", ((DataRowView)Bs_rem.Current).Row["rem_pay_id"]);


                            Rem_Info.TableName = "Rem_Info";
                            connection.SQLDS.Tables.Add(Rem_Info);
                            connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);


                            RptLangFrm.ShowDialog(this);

                        }
                        catch
                        { }



                    }
                    #endregion

                    #region return_rem
                    if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 151 || Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 52 || Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 51)
                    {


                        cmb_op = 1;

                        if (Convert.ToDouble(Rem_Info.Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                        {
                            Rem_Amount = Convert.ToDouble(Rem_Info.Rows[0]["R_amount"]);
                            ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                            if (Convert.ToDouble(Rem_Info.Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                            {
                                com_amount = Convert.ToDouble(Rem_Info.Rows[0]["s_ocomm"]);
                                com_Cur = Rem_Info.Rows[0]["com_code"].ToString();
                                ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                                Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + Rem_Info.Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                                Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + Rem_Info.Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                            }
                        }
                        else// عملة محلية
                        {
                            Rem_Amount = Convert.ToDouble(Rem_Info.Rows[0]["rem_amount_cust"]);
                            ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                            if (Convert.ToDouble(Rem_Info.Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                            {
                                com_amount = Convert.ToDouble(Rem_Info.Rows[0]["com_amount_cust"]);
                                com_Cur = local_Cur;
                                ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                                Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                                Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                            }
                        }


 

                        if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 52)
                        {
                            if (Convert.ToDouble(Rem_Info.Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                            {
                                Rem_Amount = Convert.ToDouble(Rem_Info.Rows[0]["R_amount"]);
                                ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";
                                com_amount = 0;

                            }
                            else// عملة محلية
                            {
                                Rem_Amount = Convert.ToDouble(Rem_Info.Rows[0]["rem_amount_cust"]);
                                ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                                Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                                Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                                com_amount = 0;

                            }

                        }
 



                        if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 151)
                        {

                            //--صادر اصلي
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 3 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 9)
                            { Oper_Id = 9; }
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 6 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 11)
                            { Oper_Id = 11; }
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 27 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 32)
                            { Oper_Id = 32; }
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 33 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 34)
                            { Oper_Id = 34; }

                        }
                        if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 52)
                        {
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 8 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 4)
                            { Oper_Id = 4; }
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 10 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 5)
                            { Oper_Id = 5; }
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 22 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 29)
                            { Oper_Id = 29; }
                            if (Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 35 || Convert.ToInt16(Rem_Info.Rows[0]["oper_id"]) == 36)
                            { Oper_Id = 36; }

                        }

                        if (Convert.ToInt16(Rem_Info.Rows[0]["case_id"]) == 51)
                        {
                            Oper_Id = 23;
                        }

                        try
                        {
                            string U_Date = "";
                            U_Date = Rem_Info.Rows[0]["U_DATE"].ToString();

                            return_delivery_rem_1 ObjRpt = new return_delivery_rem_1();
                            return_delivery_rem_eng_1 ObjRptEng = new return_delivery_rem_eng_1();
                            DataTable Dt_Param = CustomControls.CustomParam_Dt();
                            Dt_Param.Rows.Add("_Date", _Date);
                            Dt_Param.Rows.Add("local_Cur", local_Cur);
                            Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                            Dt_Param.Rows.Add("Vo_No", Vo_No);
                            Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                            Dt_Param.Rows.Add("User", User);
                            Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                            Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                            Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                            Dt_Param.Rows.Add("term", term);
                            Dt_Param.Rows.Add("Frm_id", "Main");
                            Dt_Param.Rows.Add("cmb_op", cmb_op);
                            Dt_Param.Rows.Add("phone", phone);
                            Dt_Param.Rows.Add("com_Cur", com_Cur);
                            Dt_Param.Rows.Add("U_Date", U_Date);
                            Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                            Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                            Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                            Dt_Param.Rows.Add("procker", procker);
                            Dt_Param.Rows.Add("procker_ename", procker_ename);
                            Dt_Param.Rows.Add("prockr_name", prockr_name);
                            Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                            Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                            Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                            Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                            Dt_Param.Rows.Add("Anote_report", Anote_report);
                            Dt_Param.Rows.Add("Enote_report", Enote_report);

                            Rem_Info.TableName = "Rem_Info";
                            connection.SQLDS.Tables.Add(Rem_Info);
                            connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);


                            RptLangFrm.ShowDialog(this);

                        }
                        catch
                        { }

                    #endregion
                    }
                }
            }
            else
            {
                 
            }
        }


        //--------------------------------------------------------
        private void Print_Details_Click(object sender, EventArgs e)
        {
          string Rem_no = ((DataRowView)Bs_rem.Current).Row["Rem_no"].ToString();
          //Row_id_current = 0;
          //Row_id_current = Convert.ToInt32(((DataRowView)Bs_rem.Current).Row["Row_id"].ToString());

            DataTable Dt = new DataTable();
            DataTable Dt_details = new DataTable();
            DataTable Rpt_dt_details = new DataTable();

            DataTable Export_DT_main = new DataTable();
            DataTable Export_DT_details = new DataTable();
            if (Grd_Rem_Cases.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
            else
            {

                Dt = connection.SQLDS.Tables["Query_remittences_tbl1"];
                Dt_details = connection.SQLDS.Tables["Query_remittences_tbl1"];


                Rpt_dt_details = Dt_details.DefaultView.ToTable().Select("Rem_no = '" + Rem_no + "'").CopyToDataTable();

                    Export_DT_main = connection.SQLDS.Tables["Query_remittences_tbl"].DefaultView.ToTable(false,"rem_no", connection.Lang_id == 1 ? "a_local_international" : "e_local_international",
     connection.Lang_id == 1 ? "a_Send_rem_flag" : "e_Send_rem_flag",connection.Lang_id == 1 ?"ACASE_NA" : "ECASE_NA","R_amount",connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME",connection.Lang_id == 1 ?  "PR_ACUR_NAME" : "PR_ECUR_NAME" ,    
     connection.Lang_id == 1 ? "S_Acust_name" : "S_Ecust_name" , connection.Lang_id==1 ? "D_Acust_name" : "D_Ecust_name",connection.Lang_id==1? "ACUST_NAME" : "ACUST_NAME" ,connection.Lang_id==1? "cust_paid_aname" : "cust_paid_aname" ,  
     connection.Lang_id == 1 ?"S_ACITY_NAME" : "S_ECITY_NAME" ,connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME" ,"Case_Date","NDate1",
        "S_name","S_phone","S_address","S_Street","S_Suburb","S_Post_Code","S_State", "S_Social_No", connection.Lang_id == 1 ?"sFRM_ADOC_NA": "sFRM_EDOC_NA","S_doc_no","S_doc_ida","S_doc_eda" ,"S_doc_issue",
        "s_Email",connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME" , "SBirth_Place","sbirth_date",connection.Lang_id==1 ?"s_job" :"s_Ejob","s_details_job","s_notes",  
        "r_name","Real_Paid_Name" ,"R_phone", connection.Lang_id == 1 ? "r_ACITY_NAME" :"r_ECITY_NAME" ,connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME","r_address","r_Street","r_Suburb",    
        "r_Post_Code","r_State","R_Social_No",connection.Lang_id == 1 ?"rFRM_ADOC_NA": "rFRM_EDOC_NA" ,"r_doc_no","r_doc_ida","r_doc_eda","r_doc_issue","r_Email",
        "rbirth_date",connection.Lang_id==1 ?"r_job" :"r_Ejob","r_details_job" , "r_notes", "vo_no","In_rec_date","create_date", 
        "s_ocomm",connection.Lang_id == 1 ? "s_ACUR_NAME" : "s_ECUR_NAME","c_ocomm",connection.Lang_id == 1 ? "c_ACUR_NAME" :  "c_ECUR_NAME","rate_online",
        "Source_money", "Relation_S_R" ,"r_Relation", "T_purpose","R_T_purpose" ,
        "i_SOComm",  connection.Lang_id == 1 ? "i_SACUR_NAME" : "i_SECUR_NAME", connection.Lang_id == 1 ? "i_SAType_name" : "i_SEType_name",
        "user_name", "User_Confirm_Name", "User_INConfirm_Name", connection.Lang_id == 1 ? "rem_pay_aname" : "rem_pay_ename", "payment_info" ,"code_rem",
        "real_agent_amount", connection.Lang_id == 1 ? "cur_Aname_exch_agent" : "cur_Ename_exch_agent", "agent_comm_amount", "exchange_agent_amount").Select("Rem_no = '" + Rem_no + "'").CopyToDataTable();
 
                    //--------------------------------------------------------------------------------------------

        Export_DT_details = Rpt_dt_details.DefaultView.ToTable(false, "rem_no", connection.Lang_id == 1 ? "Aname_rem_state" : "Ename_rem_state", "R_amount", "rem_amount_cust", "com_amount_cust", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME",
       connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME",
       connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "Case_Date", "C_DATE", connection.Lang_id == 1 ? "S_Acust_name" : "S_Ecust_name", connection.Lang_id == 1 ? "D_Acust_name" : "D_Ecust_name",
      "S_name", "S_phone", connection.Lang_id == 1 ? "S_ACOUN_NAME" : "S_ECOUN_NAME","S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue",
     "s_Email", connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME", "SBirth_Place", "sbirth_date", connection.Lang_id == 1 ? "s_job" : "s_Ejob", "s_details_job", "s_notes",
      "r_name", "Real_Paid_Name", "R_phone", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb",
      "r_Post_Code", "r_State", "R_Social_No", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email",
       "rbirth_date", connection.Lang_id == 1 ? "r_job" : "r_Ejob", "r_details_job", "r_notes", "vo_no", "In_rec_date",  
       "s_ocomm", connection.Lang_id == 1 ? "s_ACUR_NAME" : "s_ECUR_NAME", "c_ocomm", connection.Lang_id == 1 ? "c_ACUR_NAME" : "c_ECUR_NAME", "rate_online",
       "Source_money", "Relation_S_R", "r_Relation", "T_purpose", "R_T_purpose"
       , "i_SOComm", connection.Lang_id == 1 ? "i_SACUR_NAME" : "i_SECUR_NAME", connection.Lang_id == 1 ? "i_SAType_name" : "i_SEType_name",
       "user_name", "User_Confirm_Name", "User_INConfirm_Name", connection.Lang_id == 1 ? "rem_pay_aname" : "rem_pay_ename", "payment_info").Select().CopyToDataTable();
                    DataGridView[] Export_GRD = { Grdrec_rem, Grd_Rem_Cases };
                    DataTable[] Export_DT = { Export_DT_main, Export_DT_details };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);


               
                }
            }
        
        //--------------------------------------------------------
        private void Grd_Rem_Cases_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_grd)
            {
                try
                {

                    TxtS_name.DataBindings.Clear();
                    TxtS_phone.DataBindings.Clear();
                    Txts_nat.DataBindings.Clear();
                    TxtScity.DataBindings.Clear();
                    txt_resd_sen.DataBindings.Clear();
                    Txt_S_Birth.DataBindings.Clear();
                    Txt_S_Birth_Place.DataBindings.Clear();
                    Txt_s_job.DataBindings.Clear();
                    TxtS_address.DataBindings.Clear();
                    txt_purpose_sen.DataBindings.Clear();
                    Txt_S_doc_type.DataBindings.Clear();
                    txt_social_sen.DataBindings.Clear();
                    Txt_S_doc_no.DataBindings.Clear();
                    Txt_S_doc_Date.DataBindings.Clear();
                    Txt_S_doc_issue.DataBindings.Clear();
                    Txt_Relionship.DataBindings.Clear();
                    Txt_Soruce_money.DataBindings.Clear();
                    Txt_R_Name.DataBindings.Clear();
                    Txt_R_Phone.DataBindings.Clear();
                    Txt_R_Nat.DataBindings.Clear();
                    Txt_R_City.DataBindings.Clear();
                    Txt_r_birthdate.DataBindings.Clear();
                    Txt_R_Birth_Place.DataBindings.Clear();
                    Txt_r_job.DataBindings.Clear();
                    Txt_R_address.DataBindings.Clear();
                    txt_purpose_rec.DataBindings.Clear();
                    Txt_R_doc_type.DataBindings.Clear();
                    Txt_R_doc_no.DataBindings.Clear();
                    txt_social_rec.DataBindings.Clear();
                    Txt_R_doc_date.DataBindings.Clear();
                    Txt_r_doc_issue.DataBindings.Clear();
                    txt_resd_rec.DataBindings.Clear();
                    Txt_Purpose.DataBindings.Clear();
                    Txt_Discreption.DataBindings.Clear();
                    Txt_R_Relation.DataBindings.Clear();
                    Txt_S_details_job.DataBindings.Clear();
                    Txt_R_details_job.DataBindings.Clear();
                    Txt_R_notes.DataBindings.Clear();

                    //CboR_cur_name.DataBindings.Clear();
                    //Txtreccount.DataBindings.Clear();
                    //Txttotal_amuont.DataBindings.Clear();
                    TxtS_name.DataBindings.Add("Text", Bs_Grdrem, "S_name");
                    TxtS_phone.DataBindings.Add("Text", Bs_Grdrem, "S_phone");
                    Txts_nat.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                    TxtScity.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME");
                    txt_resd_sen.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "resd_flag_Aname" : "resd_flag_Ename");
                    Txt_S_Birth.DataBindings.Add("Text", Bs_Grdrem, "sbirth_date");
                    Txt_S_Birth_Place.DataBindings.Add("Text", Bs_Grdrem, "SBirth_Place");
                    Txt_s_job.DataBindings.Add("Text", Bs_Grdrem, "s_job");
                    TxtS_address.DataBindings.Add("Text", Bs_Grdrem, "S_address");
                    txt_purpose_sen.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename");
                    Txt_S_doc_type.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                    txt_social_sen.DataBindings.Add("Text", Bs_rem, "S_Social_No");
                    Txt_S_doc_no.DataBindings.Add("Text", Bs_Grdrem, "S_doc_no");
                    Txt_S_doc_Date.DataBindings.Add("Text", Bs_Grdrem, "S_doc_ida");
                    Txt_S_doc_issue.DataBindings.Add("Text", Bs_Grdrem, "S_doc_issue");
                    Txt_Relionship.DataBindings.Add("Text", Bs_Grdrem, "Relation_S_R");
                    Txt_Soruce_money.DataBindings.Add("Text", Bs_Grdrem, "Source_money");
                    Txt_R_Name.DataBindings.Add("Text", Bs_Grdrem, "r_name");
                    Txt_R_Phone.DataBindings.Add("Text", Bs_Grdrem, "R_phone");
                    Txt_R_Nat.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                    Txt_R_City.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                    Txt_r_birthdate.DataBindings.Add("Text", Bs_Grdrem, "rbirth_date");
                    Txt_R_Birth_Place.DataBindings.Add("Text", Bs_Grdrem, "TBirth_Place");
                    Txt_r_job.DataBindings.Add("Text", Bs_Grdrem, "r_job");
                    Txt_R_address.DataBindings.Add("Text", Bs_Grdrem, "r_address");
                    txt_purpose_rec.DataBindings.Add("Text", Bs_Grdrem, "R_T_purpose");
                    Txt_R_doc_type.DataBindings.Add("Text", Bs_Grdrem, connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA");
                    Txt_R_doc_no.DataBindings.Add("Text", Bs_Grdrem, "r_doc_no");
                    txt_social_rec.DataBindings.Add("Text", Bs_Grdrem, "R_Social_No");
                    Txt_R_doc_date.DataBindings.Add("Text", Bs_Grdrem, "r_doc_ida");
                    Txt_r_doc_issue.DataBindings.Add("Text", Bs_Grdrem, "r_doc_issue");
                    txt_resd_rec.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename");
                    Txt_Purpose.DataBindings.Add("Text", Bs_Grdrem, "T_purpose");
                    Txt_Discreption.DataBindings.Add("Text", Bs_Grdrem, "s_notes");
                    Txt_R_Relation.DataBindings.Add("Text", Bs_Grdrem, "r_Relation");
                    Txt_S_details_job.DataBindings.Add("Text", Bs_Grdrem, "s_details_job");
                    Txt_R_details_job.DataBindings.Add("Text", Bs_Grdrem, "r_details_job");
                    Txt_R_notes.DataBindings.Add("Text", Bs_Grdrem, "r_notes");
                    ////CboR_cur_name.DataBindings.Add("SelectedValue", Bs_rem, "R_Cur_Id");
                    ////Txtreccount.DataBindings.Add("Text", Bs_rem, "Phone");
                    ////Txttotal_amuont.DataBindings.Add("Text", Bs_rem, "Phone");

                }
                catch { }

            }
        }
        //--------------------------------------------------------
        private void Inquery_Remittances_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            Change_grd = false;
            chang_Gcase = false;
            string[] Used_Tbl = { "Query_remittences_tbl", "Query_remittences_tbl1", "Rem_Info" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);

            }
        }
        //--------------------------------------------------------
        private void Cbo_Rcur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CHN)
            {
                Decimal Amount = 0;
                DataTable DT = new DataTable();
                DT = connection.SQLDS.Tables["Query_remittences_tbl"].DefaultView.ToTable().Select("r_Cur_id = " + Cbo_Rcur.SelectedValue).CopyToDataTable();
                Amount = Convert.ToDecimal(DT.Compute("Sum(R_Amount)", ""));
                Tot_Amount.Text = Amount.ToString();
                Txt_RCount.Text = DT.Rows.Count.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
         //// Rem_no = ((DataRowView)Bs_Grdrem.Current).Row["Rem_no"].ToString();
         //   Row_id_current = 0;
         //   Row_id_current = Convert.ToInt32(((DataRowView)Bs_rem.Current).Row["Row_id"].ToString());

         //   print_Rpt_Pdf_NEw(2);
        }

        private void Btn_Rpt_Click(object sender, EventArgs e)
        {
            DataTable Inquery_Rem_TBl = new DataTable();
            


            Inquery_Rem_TBl = connection.SQLDS.Tables["Query_remittences_tbl"].DefaultView.ToTable(false, "rem_no",
                connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME",
                connection.Lang_id == 1 ?"customer_Aname": "customer_Ename",
                connection.Lang_id == 1 ? "S_ACITY_NAME": "S_ECITY_NAME",
                connection.Lang_id == 1 ? "t_ACITY_NAME": "t_ECITY_NAME"
                , "Case_Date", "NDate1", "S_name", "r_name", "a_local_international", "e_local_international", "rem_flag" ,"r_cur_id").Select().CopyToDataTable();

            DataView dv = Inquery_Rem_TBl.DefaultView;
            dv.Sort = "rem_flag ASC ,r_cur_id";
            Inquery_Rem_TBl = dv.ToTable();
            
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
           

            Inquery_Rem_TBl.TableName = "Inquery_Rem_TBl";
            connection.SQLDS.Tables.Add(Inquery_Rem_TBl);
 
             Inquery_rem_rpt_all ObjRpt = new Inquery_rem_rpt_all();
             Inquery_rem_rpt_all_eng ObjRptEng = new Inquery_rem_rpt_all_eng();

             Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

             this.Visible = false;
             RptLangFrm.ShowDialog(this);
             this.Visible = true;
             

             connection.SQLDS.Tables.Remove("Inquery_Rem_TBl");
        }

        private void Btn_OK_Click(object sender, EventArgs e)
       {
        //    DataTable Dt_Record_Tbl = new DataTable();

           
              
        //        Dt_Record_Tbl = connection.SQLDS.Tables["Query_remittences_tbl"].DefaultView.ToTable(false, "rem_no", connection.Lang_id == 1 ? "a_local_international" : "e_local_international",
        //      connection.Lang_id == 1 ? "a_Send_rem_flag" : "e_Send_rem_flag", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME",
        //     connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME", connection.Lang_id == 1 ? "S_Acust_name" : "S_Ecust_name", connection.Lang_id == 1 ? "D_Acust_name" : "D_Ecust_name", connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME",
        //     connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME", "user_name",
        //     "Case_Date", "NDate1", "S_name", "S_phone",
        //       "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda"
        //       , "S_doc_issue",
        //       "s_Email",
        //      connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME", "s_notes", "r_name", "R_phone",
        //     connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb",
        //       "r_Post_Code", "r_State", "R_Social_No", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm"
        //       , connection.Lang_id == 1 ? "s_ACUR_NAME" : "s_ECUR_NAME", "c_ocomm", connection.Lang_id == 1 ? "c_ACUR_NAME" : "c_ECUR_NAME", "rate_online",
        //       "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", connection.Lang_id == 1 ? "s_job" : "s_Ejob", "s_details_job", connection.Lang_id == 1 ? "r_job" : "r_Ejob", "r_details_job", "Real_Paid_Name", 
        //       "i_SOComm", 
        //       connection.Lang_id == 1 ? "i_SACUR_NAME" : "i_SECUR_NAME", connection.Lang_id == 1 ? "i_SAType_name" : "i_SEType_name","create_date",
        //       "User_Confirm_Name", "User_INConfirm_Name", connection.Lang_id == 1 ? "rem_pay_aname" : "rem_pay_ename", "check_no").Select().CopyToDataTable();


        //        Dt_Record_Tbl.TableName = "Dt_Record_Tbl";

        //        NewExpoert.Export_ToExcel("استـــــــعلام عن  الحوالات شامل", Dt_Record_Tbl, Grdrec_rem, true);
            }
    }
}