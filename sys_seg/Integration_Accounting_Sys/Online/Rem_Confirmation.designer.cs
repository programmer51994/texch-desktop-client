﻿namespace Integration_Accounting_Sys
{
    partial class Rem_Confirmation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label12 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Cbo_city = new System.Windows.Forms.ComboBox();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbo_rem_type = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rem_no = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.R_name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.S_name = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Check = new System.Windows.Forms.CheckBox();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_details = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_purpose = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_sender = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_S_nat = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_S_cit = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_s_phone = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_oc_issue = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_date = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Txt_doc_no = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Txt_doc = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Txt_birth_date = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Txt_s_address = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt_r_address = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txt_r_birth_date = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txt_r_doc_issue = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txt_r_doc_date = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txt_r_doc_no = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txt_r_doc = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txt_r_cit = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_r_phone = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.Txt_R_nat = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.Txt_r_name = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CboCust_Id = new System.Windows.Forms.ComboBox();
            this.GrdRem_Details = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label80 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.Txt_sdate_exp = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Txt_rdate_exp = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbo_order_type = new System.Windows.Forms.ComboBox();
            this.txt_srch = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_reject = new System.Windows.Forms.Button();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdRem_Details)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(758, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 14);
            this.label12.TabIndex = 960;
            this.label12.Text = "البلد والمدينـة :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(401, 8);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 14);
            this.label36.TabIndex = 955;
            this.label36.Text = "المستخدم:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(476, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(231, 23);
            this.TxtUser.TabIndex = 954;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(2, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 953;
            this.label2.Text = "الوكيـــــل:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(74, 4);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(269, 23);
            this.TxtTerm_Name.TabIndex = 952;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-22, 28);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1099, 1);
            this.flowLayoutPanel9.TabIndex = 951;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(75, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(75, 10);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel3.TabIndex = 952;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // Cbo_city
            // 
            this.Cbo_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_city.FormattingEnabled = true;
            this.Cbo_city.Location = new System.Drawing.Point(854, 95);
            this.Cbo_city.Name = "Cbo_city";
            this.Cbo_city.Size = new System.Drawing.Size(216, 24);
            this.Cbo_city.TabIndex = 950;
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "dd/mm/yyyy";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(857, 28);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(96, 20);
            this.TxtToDate.TabIndex = 964;
            this.TxtToDate.ValueChanged += new System.EventHandler(this.TxtToDate_ValueChanged);
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "dd/mm/yyyy";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(470, 31);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(96, 20);
            this.TxtFromDate.TabIndex = 963;
            this.TxtFromDate.ValueChanged += new System.EventHandler(this.TxtFromDate_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(384, 34);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(81, 14);
            this.label3.TabIndex = 962;
            this.label3.Text = "التــاريـخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(802, 31);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 961;
            this.label5.Text = "إلــــى :";
            // 
            // cbo_rem_type
            // 
            this.cbo_rem_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_rem_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_rem_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_rem_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_rem_type.FormattingEnabled = true;
            this.cbo_rem_type.Items.AddRange(new object[] {
            "صادر",
            "وارد"});
            this.cbo_rem_type.Location = new System.Drawing.Point(855, 48);
            this.cbo_rem_type.Name = "cbo_rem_type";
            this.cbo_rem_type.Size = new System.Drawing.Size(214, 24);
            this.cbo_rem_type.TabIndex = 965;
            this.cbo_rem_type.SelectedIndexChanged += new System.EventHandler(this.cbo_rem_type_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(760, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 14);
            this.label6.TabIndex = 966;
            this.label6.Text = "نوع الحوالـــــة :";
            // 
            // rem_no
            // 
            this.rem_no.BackColor = System.Drawing.Color.White;
            this.rem_no.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rem_no.Location = new System.Drawing.Point(468, 51);
            this.rem_no.Name = "rem_no";
            this.rem_no.Size = new System.Drawing.Size(217, 23);
            this.rem_no.TabIndex = 967;
            this.rem_no.Text = " ";
            this.rem_no.TextChanged += new System.EventHandler(this.rem_no_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(380, 57);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(83, 14);
            this.label1.TabIndex = 968;
            this.label1.Text = "رقم الحوالــة:";
            // 
            // R_name
            // 
            this.R_name.BackColor = System.Drawing.Color.White;
            this.R_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R_name.Location = new System.Drawing.Point(854, 72);
            this.R_name.Name = "R_name";
            this.R_name.Size = new System.Drawing.Size(216, 23);
            this.R_name.TabIndex = 969;
            this.R_name.TextChanged += new System.EventHandler(this.R_name_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(760, 77);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(94, 14);
            this.label7.TabIndex = 970;
            this.label7.Text = "إسم المستلم :";
            // 
            // S_name
            // 
            this.S_name.BackColor = System.Drawing.Color.White;
            this.S_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S_name.Location = new System.Drawing.Point(469, 74);
            this.S_name.Name = "S_name";
            this.S_name.Size = new System.Drawing.Size(216, 23);
            this.S_name.TabIndex = 971;
            this.S_name.TextChanged += new System.EventHandler(this.S_name_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(376, 80);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(90, 14);
            this.label8.TabIndex = 972;
            this.label8.Text = "إسم المرسل :";
            // 
            // Check
            // 
            this.Check.AutoSize = true;
            this.Check.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check.ForeColor = System.Drawing.Color.Navy;
            this.Check.Location = new System.Drawing.Point(689, 78);
            this.Check.Name = "Check";
            this.Check.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Check.Size = new System.Drawing.Size(72, 17);
            this.Check.TabIndex = 973;
            this.Check.Text = "أينما وجد";
            this.Check.UseVisualStyleBackColor = true;
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Browser.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Browser.Location = new System.Drawing.Point(997, 120);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(76, 25);
            this.Btn_Browser.TabIndex = 974;
            this.Btn_Browser.Text = "عـــــرض";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.Btn_Browser_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(4, 132);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(154, 14);
            this.label9.TabIndex = 976;
            this.label9.Text = "معلومــــات الحوالـــــــــة ...";
            // 
            // Txt_details
            // 
            this.Txt_details.BackColor = System.Drawing.Color.White;
            this.Txt_details.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_details.Location = new System.Drawing.Point(111, 356);
            this.Txt_details.Name = "Txt_details";
            this.Txt_details.ReadOnly = true;
            this.Txt_details.Size = new System.Drawing.Size(738, 23);
            this.Txt_details.TabIndex = 980;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(13, 360);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(95, 14);
            this.label10.TabIndex = 981;
            this.label10.Text = "التفاصيــــــــــل :";
            // 
            // Txt_purpose
            // 
            this.Txt_purpose.BackColor = System.Drawing.Color.White;
            this.Txt_purpose.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_purpose.Location = new System.Drawing.Point(111, 332);
            this.Txt_purpose.Name = "Txt_purpose";
            this.Txt_purpose.ReadOnly = true;
            this.Txt_purpose.Size = new System.Drawing.Size(738, 23);
            this.Txt_purpose.TabIndex = 978;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(13, 336);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(96, 14);
            this.label11.TabIndex = 979;
            this.label11.Text = "غرض التحويـل :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Maroon;
            this.label37.Location = new System.Drawing.Point(1, 379);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(120, 14);
            this.label37.TabIndex = 982;
            this.label37.Text = "معلومات المرسل ...";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(122, 388);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(954, 1);
            this.flowLayoutPanel8.TabIndex = 983;
            // 
            // Txt_sender
            // 
            this.Txt_sender.BackColor = System.Drawing.Color.White;
            this.Txt_sender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sender.Location = new System.Drawing.Point(111, 395);
            this.Txt_sender.Name = "Txt_sender";
            this.Txt_sender.ReadOnly = true;
            this.Txt_sender.Size = new System.Drawing.Size(318, 23);
            this.Txt_sender.TabIndex = 984;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(12, 398);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(96, 14);
            this.label13.TabIndex = 985;
            this.label13.Text = "إسم المرســل :";
            // 
            // Txt_S_nat
            // 
            this.Txt_S_nat.BackColor = System.Drawing.Color.White;
            this.Txt_S_nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_nat.Location = new System.Drawing.Point(553, 395);
            this.Txt_S_nat.Name = "Txt_S_nat";
            this.Txt_S_nat.ReadOnly = true;
            this.Txt_S_nat.Size = new System.Drawing.Size(296, 23);
            this.Txt_S_nat.TabIndex = 986;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(431, 400);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(117, 14);
            this.label14.TabIndex = 987;
            this.label14.Text = "جنسية المرســــل :";
            // 
            // Txt_S_cit
            // 
            this.Txt_S_cit.BackColor = System.Drawing.Color.White;
            this.Txt_S_cit.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_cit.Location = new System.Drawing.Point(553, 419);
            this.Txt_S_cit.Name = "Txt_S_cit";
            this.Txt_S_cit.ReadOnly = true;
            this.Txt_S_cit.Size = new System.Drawing.Size(296, 23);
            this.Txt_S_cit.TabIndex = 990;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(431, 423);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(116, 14);
            this.label15.TabIndex = 991;
            this.label15.Text = "مدينة المرســـــــل :";
            // 
            // Txt_s_phone
            // 
            this.Txt_s_phone.BackColor = System.Drawing.Color.White;
            this.Txt_s_phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_s_phone.Location = new System.Drawing.Point(111, 419);
            this.Txt_s_phone.Name = "Txt_s_phone";
            this.Txt_s_phone.ReadOnly = true;
            this.Txt_s_phone.Size = new System.Drawing.Size(318, 23);
            this.Txt_s_phone.TabIndex = 988;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(12, 423);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(97, 14);
            this.label16.TabIndex = 989;
            this.label16.Text = "هاتف المرسـل :";
            // 
            // Txt_oc_issue
            // 
            this.Txt_oc_issue.BackColor = System.Drawing.Color.White;
            this.Txt_oc_issue.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_oc_issue.Location = new System.Drawing.Point(396, 468);
            this.Txt_oc_issue.Name = "Txt_oc_issue";
            this.Txt_oc_issue.ReadOnly = true;
            this.Txt_oc_issue.Size = new System.Drawing.Size(153, 23);
            this.Txt_oc_issue.TabIndex = 998;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(275, 472);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(117, 14);
            this.label17.TabIndex = 999;
            this.label17.Text = "محل إصدارهـــــــــا :";
            // 
            // Txt_date
            // 
            this.Txt_date.BackColor = System.Drawing.Color.White;
            this.Txt_date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_date.Location = new System.Drawing.Point(111, 470);
            this.Txt_date.Name = "Txt_date";
            this.Txt_date.ReadOnly = true;
            this.Txt_date.Size = new System.Drawing.Size(153, 23);
            this.Txt_date.TabIndex = 996;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(12, 472);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(98, 14);
            this.label18.TabIndex = 997;
            this.label18.Text = "تأريخهـــــــــــــــا :";
            // 
            // Txt_doc_no
            // 
            this.Txt_doc_no.BackColor = System.Drawing.Color.White;
            this.Txt_doc_no.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_doc_no.Location = new System.Drawing.Point(553, 443);
            this.Txt_doc_no.Name = "Txt_doc_no";
            this.Txt_doc_no.ReadOnly = true;
            this.Txt_doc_no.Size = new System.Drawing.Size(296, 23);
            this.Txt_doc_no.TabIndex = 994;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(431, 448);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(116, 14);
            this.label19.TabIndex = 995;
            this.label19.Text = "رقم الوثيقــــــــــــة :";
            // 
            // Txt_doc
            // 
            this.Txt_doc.BackColor = System.Drawing.Color.White;
            this.Txt_doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_doc.Location = new System.Drawing.Point(111, 443);
            this.Txt_doc.Name = "Txt_doc";
            this.Txt_doc.ReadOnly = true;
            this.Txt_doc.Size = new System.Drawing.Size(318, 23);
            this.Txt_doc.TabIndex = 992;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(12, 446);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(98, 14);
            this.label20.TabIndex = 993;
            this.label20.Text = "نوع الوثيقـــــــة :";
            // 
            // Txt_birth_date
            // 
            this.Txt_birth_date.BackColor = System.Drawing.Color.White;
            this.Txt_birth_date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_birth_date.Location = new System.Drawing.Point(654, 468);
            this.Txt_birth_date.Name = "Txt_birth_date";
            this.Txt_birth_date.ReadOnly = true;
            this.Txt_birth_date.Size = new System.Drawing.Size(195, 23);
            this.Txt_birth_date.TabIndex = 1000;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(553, 472);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(97, 14);
            this.label21.TabIndex = 1001;
            this.label21.Text = "التولـــــــــــــــــد :";
            // 
            // Txt_s_address
            // 
            this.Txt_s_address.BackColor = System.Drawing.Color.White;
            this.Txt_s_address.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_s_address.Location = new System.Drawing.Point(396, 492);
            this.Txt_s_address.Name = "Txt_s_address";
            this.Txt_s_address.ReadOnly = true;
            this.Txt_s_address.Size = new System.Drawing.Size(453, 23);
            this.Txt_s_address.TabIndex = 1002;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(291, 496);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(97, 14);
            this.label22.TabIndex = 1003;
            this.label22.Text = "عنوان المرسل :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Maroon;
            this.label23.Location = new System.Drawing.Point(6, 516);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(124, 14);
            this.label23.TabIndex = 1004;
            this.label23.Text = "معلومات المستلم ...";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(142, 526);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(936, 1);
            this.flowLayoutPanel2.TabIndex = 1005;
            // 
            // txt_r_address
            // 
            this.txt_r_address.BackColor = System.Drawing.Color.White;
            this.txt_r_address.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_address.Location = new System.Drawing.Point(396, 632);
            this.txt_r_address.Name = "txt_r_address";
            this.txt_r_address.ReadOnly = true;
            this.txt_r_address.Size = new System.Drawing.Size(455, 23);
            this.txt_r_address.TabIndex = 1024;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(285, 636);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(101, 14);
            this.label24.TabIndex = 1025;
            this.label24.Text = "عنوان المستلم :";
            // 
            // txt_r_birth_date
            // 
            this.txt_r_birth_date.BackColor = System.Drawing.Color.White;
            this.txt_r_birth_date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_birth_date.Location = new System.Drawing.Point(654, 609);
            this.txt_r_birth_date.Name = "txt_r_birth_date";
            this.txt_r_birth_date.ReadOnly = true;
            this.txt_r_birth_date.Size = new System.Drawing.Size(195, 23);
            this.txt_r_birth_date.TabIndex = 1022;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(553, 611);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(97, 14);
            this.label25.TabIndex = 1023;
            this.label25.Text = "التولـــــــــــــــــد :";
            // 
            // txt_r_doc_issue
            // 
            this.txt_r_doc_issue.BackColor = System.Drawing.Color.White;
            this.txt_r_doc_issue.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_doc_issue.Location = new System.Drawing.Point(396, 607);
            this.txt_r_doc_issue.Name = "txt_r_doc_issue";
            this.txt_r_doc_issue.ReadOnly = true;
            this.txt_r_doc_issue.Size = new System.Drawing.Size(153, 23);
            this.txt_r_doc_issue.TabIndex = 1020;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(275, 611);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(117, 14);
            this.label26.TabIndex = 1021;
            this.label26.Text = "محل إصدارهـــــــــا :";
            // 
            // txt_r_doc_date
            // 
            this.txt_r_doc_date.BackColor = System.Drawing.Color.White;
            this.txt_r_doc_date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_doc_date.Location = new System.Drawing.Point(111, 607);
            this.txt_r_doc_date.Name = "txt_r_doc_date";
            this.txt_r_doc_date.ReadOnly = true;
            this.txt_r_doc_date.Size = new System.Drawing.Size(153, 23);
            this.txt_r_doc_date.TabIndex = 1018;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(11, 611);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(98, 14);
            this.label27.TabIndex = 1019;
            this.label27.Text = "تأريخهـــــــــــــــا :";
            // 
            // txt_r_doc_no
            // 
            this.txt_r_doc_no.BackColor = System.Drawing.Color.White;
            this.txt_r_doc_no.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_doc_no.Location = new System.Drawing.Point(553, 582);
            this.txt_r_doc_no.Name = "txt_r_doc_no";
            this.txt_r_doc_no.ReadOnly = true;
            this.txt_r_doc_no.Size = new System.Drawing.Size(296, 23);
            this.txt_r_doc_no.TabIndex = 1016;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(431, 587);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(116, 14);
            this.label28.TabIndex = 1017;
            this.label28.Text = "رقم الوثيقــــــــــــة :";
            // 
            // txt_r_doc
            // 
            this.txt_r_doc.BackColor = System.Drawing.Color.White;
            this.txt_r_doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_doc.Location = new System.Drawing.Point(111, 582);
            this.txt_r_doc.Name = "txt_r_doc";
            this.txt_r_doc.ReadOnly = true;
            this.txt_r_doc.Size = new System.Drawing.Size(318, 23);
            this.txt_r_doc.TabIndex = 1014;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(11, 585);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(98, 14);
            this.label29.TabIndex = 1015;
            this.label29.Text = "نوع الوثيقـــــــة :";
            // 
            // Txt_r_cit
            // 
            this.Txt_r_cit.BackColor = System.Drawing.Color.White;
            this.Txt_r_cit.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_cit.Location = new System.Drawing.Point(553, 558);
            this.Txt_r_cit.Name = "Txt_r_cit";
            this.Txt_r_cit.ReadOnly = true;
            this.Txt_r_cit.Size = new System.Drawing.Size(296, 23);
            this.Txt_r_cit.TabIndex = 1012;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(431, 563);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(117, 14);
            this.label30.TabIndex = 1013;
            this.label30.Text = "مدينة المستلــــــم :";
            // 
            // Txt_r_phone
            // 
            this.Txt_r_phone.BackColor = System.Drawing.Color.White;
            this.Txt_r_phone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_phone.Location = new System.Drawing.Point(111, 557);
            this.Txt_r_phone.Name = "Txt_r_phone";
            this.Txt_r_phone.ReadOnly = true;
            this.Txt_r_phone.Size = new System.Drawing.Size(318, 23);
            this.Txt_r_phone.TabIndex = 1010;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(11, 560);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(98, 14);
            this.label31.TabIndex = 1011;
            this.label31.Text = "هاتف المستلم :";
            // 
            // Txt_R_nat
            // 
            this.Txt_R_nat.BackColor = System.Drawing.Color.White;
            this.Txt_R_nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_nat.Location = new System.Drawing.Point(553, 533);
            this.Txt_R_nat.Name = "Txt_R_nat";
            this.Txt_R_nat.ReadOnly = true;
            this.Txt_R_nat.Size = new System.Drawing.Size(296, 23);
            this.Txt_R_nat.TabIndex = 1008;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(431, 538);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(118, 14);
            this.label32.TabIndex = 1009;
            this.label32.Text = "جنسية المستلـــم :";
            // 
            // Txt_r_name
            // 
            this.Txt_r_name.BackColor = System.Drawing.Color.White;
            this.Txt_r_name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_name.Location = new System.Drawing.Point(111, 533);
            this.Txt_r_name.Name = "Txt_r_name";
            this.Txt_r_name.ReadOnly = true;
            this.Txt_r_name.Size = new System.Drawing.Size(318, 23);
            this.Txt_r_name.TabIndex = 1006;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(11, 536);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(97, 14);
            this.label33.TabIndex = 1007;
            this.label33.Text = "إسم المستلـم :";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(311, 660);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 25);
            this.button1.TabIndex = 1026;
            this.button1.Text = "تأكـــــيد";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(672, 660);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 25);
            this.button2.TabIndex = 1027;
            this.button2.Text = "إنهـــــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-85, 659);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1161, 1);
            this.flowLayoutPanel5.TabIndex = 1028;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(137, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel6.TabIndex = 630;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(161, 146);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(915, 1);
            this.flowLayoutPanel7.TabIndex = 1029;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(768, 1);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(187, 23);
            this.TxtIn_Rec_Date.TabIndex = 1030;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TxtIn_Rec_Date.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(371, 103);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(95, 14);
            this.label4.TabIndex = 1032;
            this.label4.Text = " الفرع / الوكلاء:";
            // 
            // CboCust_Id
            // 
            this.CboCust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCust_Id.FormattingEnabled = true;
            this.CboCust_Id.Location = new System.Drawing.Point(468, 97);
            this.CboCust_Id.Name = "CboCust_Id";
            this.CboCust_Id.Size = new System.Drawing.Size(220, 24);
            this.CboCust_Id.TabIndex = 1031;
            this.CboCust_Id.SelectedValueChanged += new System.EventHandler(this.CboCust_Id_SelectedValueChanged);
            // 
            // GrdRem_Details
            // 
            this.GrdRem_Details.AllowUserToAddRows = false;
            this.GrdRem_Details.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdRem_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdRem_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.GrdRem_Details.ColumnHeadersHeight = 37;
            this.GrdRem_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column10,
            this.Column2,
            this.Column5,
            this.Column9,
            this.Column3,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column4,
            this.Column11});
            this.GrdRem_Details.Location = new System.Drawing.Point(12, 149);
            this.GrdRem_Details.Name = "GrdRem_Details";
            this.GrdRem_Details.RowHeadersWidth = 25;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdRem_Details.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.GrdRem_Details.Size = new System.Drawing.Size(1053, 159);
            this.GrdRem_Details.TabIndex = 1035;
            this.GrdRem_Details.SelectionChanged += new System.EventHandler(this.GrdRem_Details_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "check_id";
            this.Column1.FalseValue = "0";
            this.Column1.HeaderText = "تأشير ";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.TrueValue = "1";
            this.Column1.Width = 50;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "ver_flag_name";
            this.Column10.HeaderText = "حالة التحقق";
            this.Column10.Name = "Column10";
            this.Column10.Width = 81;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "rem_no";
            this.Column2.HeaderText = "رقم الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 78;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "R_Amount";
            this.Column5.HeaderText = "مبلغ الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 81;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "R_CUR_NAME";
            this.Column9.HeaderText = "عملة الحوالة";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 81;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "PR_CUR_NAME";
            this.Column3.HeaderText = "عملة التسليم";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 81;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "sub_cust_aname";
            this.Column6.HeaderText = "الجهة المصدرة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 92;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "ACASE_NA";
            this.Column7.HeaderText = "حالة الحوالة";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 81;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "case_Date";
            this.Column8.HeaderText = "تاريخ الحالة";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 61;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "SCITY_NAME";
            this.Column4.HeaderText = "مدينة الارسال";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 89;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "login_name";
            this.Column11.HeaderText = "المستخدم المصدر للحوالة";
            this.Column11.Name = "Column11";
            this.Column11.Width = 107;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Maroon;
            this.label80.Location = new System.Drawing.Point(9, 315);
            this.label80.Name = "label80";
            this.label80.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label80.Size = new System.Drawing.Size(146, 14);
            this.label80.TabIndex = 1037;
            this.label80.Text = "قـــــوائـــــــــم الـــمــــنــــع";
            this.label80.Click += new System.EventHandler(this.label80_Click);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Maroon;
            this.label77.Location = new System.Drawing.Point(236, 315);
            this.label77.Name = "label77";
            this.label77.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label77.Size = new System.Drawing.Size(168, 14);
            this.label77.TabIndex = 1038;
            this.label77.Text = "عـــدد الحــوالات الــمــرســلـة";
            this.label77.Click += new System.EventHandler(this.label77_Click);
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-5, 311);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1081, 1);
            this.flowLayoutPanel10.TabIndex = 1039;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button3.ForeColor = System.Drawing.Color.Navy;
            this.button3.Location = new System.Drawing.Point(405, 660);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(173, 25);
            this.button3.TabIndex = 1040;
            this.button3.Text = "تأكيد مع التحقق من قوائم المنع ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Maroon;
            this.label35.Location = new System.Drawing.Point(874, 315);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(115, 14);
            this.label35.TabIndex = 1041;
            this.label35.Text = "عـــــرض الوثــــــائق";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-83, 685);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(1161, 1);
            this.flowLayoutPanel11.TabIndex = 1042;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(137, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel12.TabIndex = 630;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 685);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.ShowItemToolTips = true;
            this.statusStrip1.Size = new System.Drawing.Size(1075, 22);
            this.statusStrip1.TabIndex = 1043;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // Txt_sdate_exp
            // 
            this.Txt_sdate_exp.BackColor = System.Drawing.Color.White;
            this.Txt_sdate_exp.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sdate_exp.Location = new System.Drawing.Point(111, 496);
            this.Txt_sdate_exp.Name = "Txt_sdate_exp";
            this.Txt_sdate_exp.ReadOnly = true;
            this.Txt_sdate_exp.Size = new System.Drawing.Size(153, 23);
            this.Txt_sdate_exp.TabIndex = 1044;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(12, 500);
            this.label38.Name = "label38";
            this.label38.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 1045;
            this.label38.Text = "أنتهائهـــــــــــــــا :";
            // 
            // Txt_rdate_exp
            // 
            this.Txt_rdate_exp.BackColor = System.Drawing.Color.White;
            this.Txt_rdate_exp.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_rdate_exp.Location = new System.Drawing.Point(111, 632);
            this.Txt_rdate_exp.Name = "Txt_rdate_exp";
            this.Txt_rdate_exp.ReadOnly = true;
            this.Txt_rdate_exp.Size = new System.Drawing.Size(153, 23);
            this.Txt_rdate_exp.TabIndex = 1046;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(11, 636);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(99, 14);
            this.label39.TabIndex = 1047;
            this.label39.Text = "أنتهائهـــــــــــــــا :";
            // 
            // cbo_order_type
            // 
            this.cbo_order_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_order_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_order_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_order_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_order_type.FormattingEnabled = true;
            this.cbo_order_type.Items.AddRange(new object[] {
            "فروع",
            "وكالات"});
            this.cbo_order_type.Location = new System.Drawing.Point(256, 30);
            this.cbo_order_type.Name = "cbo_order_type";
            this.cbo_order_type.Size = new System.Drawing.Size(111, 24);
            this.cbo_order_type.TabIndex = 1270;
            this.cbo_order_type.SelectedIndexChanged += new System.EventHandler(this.cbo_order_type_SelectedIndexChanged);
            // 
            // txt_srch
            // 
            this.txt_srch.AutoSize = true;
            this.txt_srch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.txt_srch.ForeColor = System.Drawing.Color.Navy;
            this.txt_srch.Location = new System.Drawing.Point(7, 33);
            this.txt_srch.Name = "txt_srch";
            this.txt_srch.Size = new System.Drawing.Size(62, 16);
            this.txt_srch.TabIndex = 1269;
            this.txt_srch.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(70, 31);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(184, 22);
            this.Txt_Sub_Cust.TabIndex = 1268;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(4, 55);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(362, 76);
            this.Grd_Sub_Cust.TabIndex = 1267;
            this.Grd_Sub_Cust.SelectionChanged += new System.EventHandler(this.Grd_Sub_Cust_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "cust_id";
            dataGridViewCellStyle13.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الفرع او الوكيل";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 225;
            // 
            // btn_reject
            // 
            this.btn_reject.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.btn_reject.ForeColor = System.Drawing.Color.Navy;
            this.btn_reject.Location = new System.Drawing.Point(578, 660);
            this.btn_reject.Name = "btn_reject";
            this.btn_reject.Size = new System.Drawing.Size(94, 25);
            this.btn_reject.TabIndex = 1271;
            this.btn_reject.Text = "رفــــــض";
            this.btn_reject.UseVisualStyleBackColor = true;
            this.btn_reject.Click += new System.EventHandler(this.btn_reject_Click);
            // 
            // Rem_Confirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 707);
            this.Controls.Add(this.btn_reject);
            this.Controls.Add(this.cbo_order_type);
            this.Controls.Add(this.txt_srch);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.Controls.Add(this.Txt_rdate_exp);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.Txt_sdate_exp);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel11);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.GrdRem_Details);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CboCust_Id);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txt_r_address);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txt_r_birth_date);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txt_r_doc_issue);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.txt_r_doc_date);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.txt_r_doc_no);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txt_r_doc);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Txt_r_cit);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.Txt_r_phone);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.Txt_R_nat);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.Txt_r_name);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Txt_s_address);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.Txt_birth_date);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Txt_oc_issue);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_date);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Txt_doc_no);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Txt_doc);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Txt_S_cit);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_s_phone);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_S_nat);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_sender);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.Txt_details);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_purpose);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Btn_Browser);
            this.Controls.Add(this.Check);
            this.Controls.Add(this.S_name);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.R_name);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rem_no);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbo_rem_type);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Cbo_city);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Rem_Confirmation";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "504";
            this.Text = "Rem_Confirmation";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Rem_Confirmation_FormClosed);
            this.Load += new System.EventHandler(this.Rem_Confirmation_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrdRem_Details)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ComboBox Cbo_city;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbo_rem_type;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox rem_no;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox R_name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox S_name;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox Check;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txt_details;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_purpose;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.TextBox Txt_sender;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_S_nat;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_S_cit;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txt_s_phone;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txt_oc_issue;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_date;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Txt_doc_no;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Txt_doc;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Txt_birth_date;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Txt_s_address;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox txt_r_address;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txt_r_birth_date;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txt_r_doc_issue;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txt_r_doc_date;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txt_r_doc_no;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txt_r_doc;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txt_r_cit;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_r_phone;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txt_R_nat;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox Txt_r_name;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CboCust_Id;
        private System.Windows.Forms.DataGridView GrdRem_Details;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.TextBox Txt_sdate_exp;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox Txt_rdate_exp;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.ComboBox cbo_order_type;
        private System.Windows.Forms.Label txt_srch;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button btn_reject;
    }
}