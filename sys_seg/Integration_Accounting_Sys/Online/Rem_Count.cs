﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Rem_Count : Form
    {
        BindingSource _Bs_count = new BindingSource();
        int form_id =  0 ;
        public static Int16 Rem_Count_Back_btn = 0;
        //-----------------------
        public Rem_Count( int Frm_id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Grd_Rem_count.AutoGenerateColumns = false;
            form_id = Frm_id;
        }

        private void Rem_Count_Load(object sender, EventArgs e)
        {
            if (form_id == 1) // قبض جوالة
            {
                if (connection.Lang_id == 1)
                {
                    Column1.HeaderText = "أسم المرســل";
                    Column2.HeaderText = "أسم المستلـم";

                    Column8.HeaderText = "وثيقة المرســل";
                    Column9.HeaderText = "رقم وثيقة المرســل";
                    Column10.HeaderText = "رقم الوطني للمرســل";
                    Column12.HeaderText = "تاريخ التولد للمرسل";


                    Column8.DataPropertyName = "SFRM_ADOC_NA";
                }

                if (connection.Lang_id != 1)
                {
                    Btn_Add.Text = "Export";
                    Column1.HeaderText = "Sender Name";
                    Column2.HeaderText = "Reciever Name";
                    Column3.HeaderText = "Rem. Amount";
                    Column4.HeaderText = "Rem. Currency";
                    Column5.HeaderText = "Create Date";
                    Column6.HeaderText = "Sender City";
                    Column7.HeaderText = "Reciever City";
                    Column8.HeaderText = "Sender Doc.";
                    Column9.HeaderText = "Sender DOC. No";
                    Column10.HeaderText = "Sender Social No";
                    Column11.HeaderText = "Rem. Number";
                    Column12.HeaderText = "Date birth of sender";

                    Column4.DataPropertyName = "R_ECUR_NAME";
                    Column6.DataPropertyName = "S_ECITY_NAME";
                    Column7.DataPropertyName = "t_ECITY_NAME";
                    Column8.DataPropertyName = "SFRM_EDOC_NA";
                  
                                 
 
                }
                     Column1.DataPropertyName = "s_name";
                     Column2.DataPropertyName = "r_name";
                     Column9.DataPropertyName = "S_doc_no";
                     Column10.DataPropertyName = "S_social_no";
                     Column12.DataPropertyName = "sbirth_date";
                   

            } 
            else
            {
                if (connection.Lang_id == 1)
                {
                    Column1.HeaderText = "أسم المستـلم";
                    Column2.HeaderText = "أسم المرســل";
                    //-----------------------------------------
                    Column8.HeaderText = "وثيقةالمستلم";
                    Column9.HeaderText = "رقم وثيقة المستلم";
                    Column10.HeaderText = "رقم الوطني للمستلم";

                    Column12.HeaderText = "تاريخ التولد للمستلم";

                    Column8.DataPropertyName = "RFRM_ADOC_NA";

                }

                if (connection.Lang_id != 1)
                {
                    Btn_Add.Text = "Export";
                    Column1.HeaderText = "Reciever Name";
                    Column2.HeaderText = "Sender Name";
                    Column3.HeaderText = "Rem. Amount";
                    Column4.HeaderText = "Rem. Currency";
                    Column5.HeaderText = "Create Date";
                    Column6.HeaderText = "Sender City";
                    Column7.HeaderText = "Reciever City";
                    Column8.HeaderText = "Reciever Doc.";
                    Column9.HeaderText = "Reciever DOC. No.";
                    Column10.HeaderText = " Reciever Social No";
                    Column11.HeaderText = "Rem. Number";


                    Column4.DataPropertyName = "R_ECUR_NAME";
                    Column6.DataPropertyName = "S_ECITY_NAME";
                    Column7.DataPropertyName = "t_ECITY_NAME";
                    Column8.DataPropertyName = "RFRM_EDOC_NA";
                    Column12.HeaderText = "Date birth of Receiver";
                   

                }
                    Column1.DataPropertyName = "r_name";
                    Column2.DataPropertyName = "s_name";
                    Column9.DataPropertyName = "r_doc_no";
                    Column10.DataPropertyName = "r_social_no";
                    Column12.DataPropertyName = "rbirth_date";
               
            }
                    _Bs_count.DataSource = connection.SQLDS.Tables["similar_per_rem_tbl"];
                    Grd_Rem_count.DataSource = _Bs_count;
        }

        private void Grd_Rem_count_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_count);
        }

        private void Rem_Count_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = {"similar_per_rem_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

            Rem_Count_Back_btn = 1;
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (form_id == 1)
            {
                DataTable ExpDt = connection.SQLDS.Tables["similar_per_rem_tbl"].DefaultView.ToTable(false,
                    "s_name", "r_name", "rem_no", "r_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "c_date", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME",
                    connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME", connection.Lang_id == 1 ? "SFRM_ADOC_NA" : "SFRM_EDOC_NA", "S_doc_no", "S_social_no"
                    );
                DataTable[] _EXP_DT = { ExpDt };
                DataGridView[] Export_GRD = { Grd_Rem_count };
                MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
            }

            if (form_id == 2)
            {

                DataTable ExpDt = connection.SQLDS.Tables["similar_per_rem_tbl"].DefaultView.ToTable(false,
                    "s_name", "r_name", "rem_no", "r_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "c_date", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME",
                    connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME", connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA", "r_doc_no", "r_social_no"
                    );
                DataTable[] _EXP_DT = { ExpDt };
                DataGridView[] Export_GRD = { Grd_Rem_count };
                MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
            }

        }

        //-----------------------------------------------------
    }
}