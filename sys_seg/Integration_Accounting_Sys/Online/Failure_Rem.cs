﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Failure_Rem : Form
    {
        BindingSource _Bs_count_rem = new BindingSource();
        //-----------------------
        public Failure_Rem()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Grd_Rem_count.AutoGenerateColumns = false;
       
        }

        private void Rem_Count_Load(object sender, EventArgs e)
        {
       
               this.Text = "كشف الحوالات غير المرسلة" ;
                if (connection.Lang_id != 1)
                {
                    this.Text = "Reveal Failure Rem.";
                    Column1.HeaderText = "Remittances NO.";
                    Column2.HeaderText = "Create Date";
                    Column3.HeaderText = "Error Msg.";
                    Column4.HeaderText = "Operation Name";
                    Column5.HeaderText = "Terminal Name";

                    Column4.DataPropertyName = "EOPER_NAME";
                    Column5.DataPropertyName = "Ecust_name";

                }
               
   
            connection.SqlExec("exec get_Failure_Rem ", "Failure_Rem_Tbl");
            if (connection.SQLDS.Tables["Failure_Rem_Tbl"].Rows.Count > 0)
            {
                _Bs_count_rem.DataSource = connection.SQLDS.Tables["Failure_Rem_Tbl"];
                Grd_Rem_count.DataSource = _Bs_count_rem;
            }
            else
            { Grd_Rem_count.DataSource = new BindingSource();

            MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات غير مرسلة" : "No Failure Rem.", MyGeneral_Lib.LblCap);
            return;
            }
        }

        private void Grd_Rem_count_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_count_rem);
        }
        //-----------------------------------------------------
    }
}