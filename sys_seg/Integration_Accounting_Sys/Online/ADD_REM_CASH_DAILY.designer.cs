﻿namespace Integration_Accounting_Sys
{
    partial class ADD_REM_CASH_DAILY
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Chk_Term = new System.Windows.Forms.CheckBox();
            this.Txt_another_Sen = new System.Windows.Forms.TextBox();
            this.Txt_S_details_job = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label80 = new System.Windows.Forms.Label();
            this.cmb_job_sender = new System.Windows.Forms.ComboBox();
            this.resd_cmb = new System.Windows.Forms.ComboBox();
            this.Tex_Social_ID = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ar_sen = new System.Windows.Forms.Button();
            this.ENAR_BTN = new System.Windows.Forms.Button();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.Txt_Sbirth_Date = new System.Windows.Forms.DateTimePicker();
            this.Txt_Doc_S_Exp = new System.Windows.Forms.DateTimePicker();
            this.Txt_Doc_S_Date = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmb_Gender_id = new System.Windows.Forms.ComboBox();
            this.cmb_s_nat = new System.Windows.Forms.ComboBox();
            this.Grd_CustSen_Name = new System.Windows.Forms.DataGridView();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Notes = new System.Windows.Forms.TextBox();
            this.Txt_T_Purpose = new System.Windows.Forms.TextBox();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.txt_Mother_name = new System.Windows.Forms.TextBox();
            this.Txt_mail = new System.Windows.Forms.TextBox();
            this.Txt_S_Birth_Place = new System.Windows.Forms.TextBox();
            this.Txt_Doc_S_Issue = new System.Windows.Forms.TextBox();
            this.Txt_S_Phone = new System.Windows.Forms.TextBox();
            this.TxtS_State = new System.Windows.Forms.TextBox();
            this.TxtS_Post_Code = new System.Windows.Forms.TextBox();
            this.Txts_street = new System.Windows.Forms.TextBox();
            this.txtS_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Sender = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.Cmb_Case_Purpose = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_S_Doc_No = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Cmb_S_Doc_Type = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.Cmb_Code_phone_S = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Cmb_S_City = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Chk_Term1 = new System.Windows.Forms.CheckBox();
            this.Txt_another_Rec = new System.Windows.Forms.TextBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.en_rec_btn = new System.Windows.Forms.Button();
            this.ar_rec_btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Grd_CustRec_Name = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cmb_phone_Code_R = new System.Windows.Forms.ComboBox();
            this.Txtr_State = new System.Windows.Forms.TextBox();
            this.Txtr_Post_Code = new System.Windows.Forms.TextBox();
            this.Txtr_Street = new System.Windows.Forms.TextBox();
            this.Txtr_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_Reciever = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.Cmb_R_Nat = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Cmb_R_City = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label91 = new System.Windows.Forms.Label();
            this.lbl_comm = new System.Windows.Forms.Label();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.label48 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label88 = new System.Windows.Forms.Label();
            this.Lab_for_Com = new System.Windows.Forms.Label();
            this.Lab_for_Rem = new System.Windows.Forms.Label();
            this.lab_Loc_comm = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.Lab_Loc_Rem = new System.Windows.Forms.Label();
            this.Cbo_Oper = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.Txt_Accproc = new System.Windows.Forms.TextBox();
            this.Grd_procer = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_procer = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.chk_comm_info = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_com_type = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txt_com_curr = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.txt_commamount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.Cbo_rem_path = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.Cmb_PR_Cur_Id = new System.Windows.Forms.ComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.Cmb_R_CUR_ID = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmb_comm_type = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.Cmb_Comm_Cur = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cmb_cur_comm = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.label73 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.cmb_cur = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.Cmb_T_City = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TXt_Total_Comm = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Add_Com_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Tot_amount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_minrate_comm2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_maxrate_comm2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_ExRate_comm2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Com_Amnt2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_locamount_comm2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_minrate_rem2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_maxrate_rem2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_ExRate_Rem2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Rem_Amount2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_locamount_rem2 = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txtr_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Tot_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_locamount_comm = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_locamount_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_minrate_comm = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_maxrate_comm = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_minrate_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_maxrate_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_ExRate_comm = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Com_Amnt = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_ExRate_Rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Rem_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Com_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.Cbo_city = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.Txt_MobilePhone = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.Btn_Phone = new System.Windows.Forms.Button();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.CHK_CITY = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_CustSen_Name)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_CustRec_Name)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_procer)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(2, 60);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(841, 573);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(833, 547);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "معلومات المرسل";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Chk_Term);
            this.panel1.Controls.Add(this.Txt_another_Sen);
            this.panel1.Controls.Add(this.Txt_S_details_job);
            this.panel1.Controls.Add(this.label89);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.label80);
            this.panel1.Controls.Add(this.cmb_job_sender);
            this.panel1.Controls.Add(this.resd_cmb);
            this.panel1.Controls.Add(this.Tex_Social_ID);
            this.panel1.Controls.Add(this.label79);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.ar_sen);
            this.panel1.Controls.Add(this.ENAR_BTN);
            this.panel1.Controls.Add(this.label78);
            this.panel1.Controls.Add(this.label77);
            this.panel1.Controls.Add(this.Btn_Browser);
            this.panel1.Controls.Add(this.Txt_Sbirth_Date);
            this.panel1.Controls.Add(this.Txt_Doc_S_Exp);
            this.panel1.Controls.Add(this.Txt_Doc_S_Date);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.flowLayoutPanel17);
            this.panel1.Controls.Add(this.cmb_Gender_id);
            this.panel1.Controls.Add(this.cmb_s_nat);
            this.panel1.Controls.Add(this.Grd_CustSen_Name);
            this.panel1.Controls.Add(this.Txt_Notes);
            this.panel1.Controls.Add(this.Txt_T_Purpose);
            this.panel1.Controls.Add(this.Txt_Relionship);
            this.panel1.Controls.Add(this.Txt_Soruce_money);
            this.panel1.Controls.Add(this.txt_Mother_name);
            this.panel1.Controls.Add(this.Txt_mail);
            this.panel1.Controls.Add(this.Txt_S_Birth_Place);
            this.panel1.Controls.Add(this.Txt_Doc_S_Issue);
            this.panel1.Controls.Add(this.Txt_S_Phone);
            this.panel1.Controls.Add(this.TxtS_State);
            this.panel1.Controls.Add(this.TxtS_Post_Code);
            this.panel1.Controls.Add(this.Txts_street);
            this.panel1.Controls.Add(this.txtS_Suburb);
            this.panel1.Controls.Add(this.Txt_Sender);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.Cmb_Case_Purpose);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.flowLayoutPanel7);
            this.panel1.Controls.Add(this.flowLayoutPanel8);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.Txt_S_Doc_No);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.Cmb_S_Doc_Type);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.flowLayoutPanel5);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.Cmb_Code_phone_S);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.Cmb_S_City);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(827, 541);
            this.panel1.TabIndex = 1;
            // 
            // Chk_Term
            // 
            this.Chk_Term.AutoSize = true;
            this.Chk_Term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_Term.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Term.Location = new System.Drawing.Point(322, 27);
            this.Chk_Term.Name = "Chk_Term";
            this.Chk_Term.Size = new System.Drawing.Size(84, 20);
            this.Chk_Term.TabIndex = 883;
            this.Chk_Term.Text = "الاسـم الاخر:";
            this.Chk_Term.UseVisualStyleBackColor = true;
            this.Chk_Term.CheckedChanged += new System.EventHandler(this.Chk_Term_CheckedChanged);
            // 
            // Txt_another_Sen
            // 
            this.Txt_another_Sen.Location = new System.Drawing.Point(17, 28);
            this.Txt_another_Sen.MaxLength = 150;
            this.Txt_another_Sen.Name = "Txt_another_Sen";
            this.Txt_another_Sen.Size = new System.Drawing.Size(301, 20);
            this.Txt_another_Sen.TabIndex = 881;
            // 
            // Txt_S_details_job
            // 
            this.Txt_S_details_job.Location = new System.Drawing.Point(15, 412);
            this.Txt_S_details_job.MaxLength = 99;
            this.Txt_S_details_job.Name = "Txt_S_details_job";
            this.Txt_S_details_job.Size = new System.Drawing.Size(367, 20);
            this.Txt_S_details_job.TabIndex = 21;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label89.ForeColor = System.Drawing.Color.Navy;
            this.label89.Location = new System.Drawing.Point(387, 414);
            this.label89.Name = "label89";
            this.label89.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label89.Size = new System.Drawing.Size(77, 16);
            this.label89.TabIndex = 879;
            this.label89.Text = "تفاصيل العمل :";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(808, 14);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 877;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Maroon;
            this.label80.Location = new System.Drawing.Point(653, 193);
            this.label80.Name = "label80";
            this.label80.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label80.Size = new System.Drawing.Size(146, 14);
            this.label80.TabIndex = 876;
            this.label80.Text = "قـــــوائـــــــــم الـــمــــنــــع";
            this.label80.Click += new System.EventHandler(this.label80_Click);
            // 
            // cmb_job_sender
            // 
            this.cmb_job_sender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_job_sender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_job_sender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_job_sender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_job_sender.FormattingEnabled = true;
            this.cmb_job_sender.Location = new System.Drawing.Point(474, 410);
            this.cmb_job_sender.Name = "cmb_job_sender";
            this.cmb_job_sender.Size = new System.Drawing.Size(258, 24);
            this.cmb_job_sender.TabIndex = 20;
            // 
            // resd_cmb
            // 
            this.resd_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_cmb.FormattingEnabled = true;
            this.resd_cmb.Location = new System.Drawing.Point(21, 437);
            this.resd_cmb.Name = "resd_cmb";
            this.resd_cmb.Size = new System.Drawing.Size(232, 24);
            this.resd_cmb.TabIndex = 23;
            // 
            // Tex_Social_ID
            // 
            this.Tex_Social_ID.Location = new System.Drawing.Point(16, 365);
            this.Tex_Social_ID.MaxLength = 29;
            this.Tex_Social_ID.Name = "Tex_Social_ID";
            this.Tex_Social_ID.Size = new System.Drawing.Size(293, 20);
            this.Tex_Social_ID.TabIndex = 19;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label79.ForeColor = System.Drawing.Color.Navy;
            this.label79.Location = new System.Drawing.Point(318, 367);
            this.label79.Name = "label79";
            this.label79.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label79.Size = new System.Drawing.Size(73, 16);
            this.label79.TabIndex = 875;
            this.label79.Text = "الرقم الوطني :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(118, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 873;
            this.label5.Text = "Msg_label";
            // 
            // ar_sen
            // 
            this.ar_sen.Location = new System.Drawing.Point(444, 9);
            this.ar_sen.Name = "ar_sen";
            this.ar_sen.Size = new System.Drawing.Size(29, 23);
            this.ar_sen.TabIndex = 0;
            this.ar_sen.Text = "AR";
            this.ar_sen.UseVisualStyleBackColor = false;
            this.ar_sen.Click += new System.EventHandler(this.ar_sen_Click);
            // 
            // ENAR_BTN
            // 
            this.ENAR_BTN.Location = new System.Drawing.Point(417, 9);
            this.ENAR_BTN.Name = "ENAR_BTN";
            this.ENAR_BTN.Size = new System.Drawing.Size(28, 23);
            this.ENAR_BTN.TabIndex = 1;
            this.ENAR_BTN.Text = "EN";
            this.ENAR_BTN.UseVisualStyleBackColor = true;
            this.ENAR_BTN.Click += new System.EventHandler(this.ENAR_BTN_Click);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label78.ForeColor = System.Drawing.Color.Navy;
            this.label78.Location = new System.Drawing.Point(256, 441);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(64, 16);
            this.label78.TabIndex = 870;
            this.label78.Text = "نوع الاقامة:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Maroon;
            this.label77.Location = new System.Drawing.Point(419, 193);
            this.label77.Name = "label77";
            this.label77.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label77.Size = new System.Drawing.Size(168, 14);
            this.label77.TabIndex = 868;
            this.label77.Text = "عـــدد الحــوالات الــمــرســلـة";
            this.label77.Click += new System.EventHandler(this.label77_Click);
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Enabled = false;
            this.Btn_Browser.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Browser.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Browser.Location = new System.Drawing.Point(21, 514);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(111, 25);
            this.Btn_Browser.TabIndex = 28;
            this.Btn_Browser.Text = "عرض الوثائق";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.Btn_Browser_Click);
            // 
            // Txt_Sbirth_Date
            // 
            this.Txt_Sbirth_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Sbirth_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Sbirth_Date.Location = new System.Drawing.Point(128, 283);
            this.Txt_Sbirth_Date.Name = "Txt_Sbirth_Date";
            this.Txt_Sbirth_Date.Size = new System.Drawing.Size(181, 20);
            this.Txt_Sbirth_Date.TabIndex = 13;
            this.Txt_Sbirth_Date.Enter += new System.EventHandler(this.Txt_Sbirth_Date_Enter);
            // 
            // Txt_Doc_S_Exp
            // 
            this.Txt_Doc_S_Exp.Checked = false;
            this.Txt_Doc_S_Exp.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_S_Exp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_S_Exp.Location = new System.Drawing.Point(128, 256);
            this.Txt_Doc_S_Exp.Name = "Txt_Doc_S_Exp";
            this.Txt_Doc_S_Exp.ShowCheckBox = true;
            this.Txt_Doc_S_Exp.Size = new System.Drawing.Size(181, 20);
            this.Txt_Doc_S_Exp.TabIndex = 11;
            // 
            // Txt_Doc_S_Date
            // 
            this.Txt_Doc_S_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_S_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_S_Date.Location = new System.Drawing.Point(571, 256);
            this.Txt_Doc_S_Date.Name = "Txt_Doc_S_Date";
            this.Txt_Doc_S_Date.Size = new System.Drawing.Size(151, 20);
            this.Txt_Doc_S_Date.TabIndex = 10;
            this.Txt_Doc_S_Date.Enter += new System.EventHandler(this.Txt_Doc_S_Date_Enter);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(478, 258);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 867;
            this.label34.Text = "dd/mm/yyyy";
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(2, 55);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(297, 1);
            this.flowLayoutPanel17.TabIndex = 863;
            // 
            // cmb_Gender_id
            // 
            this.cmb_Gender_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Gender_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Gender_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Gender_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Gender_id.FormattingEnabled = true;
            this.cmb_Gender_id.Location = new System.Drawing.Point(18, 337);
            this.cmb_Gender_id.Name = "cmb_Gender_id";
            this.cmb_Gender_id.Size = new System.Drawing.Size(292, 24);
            this.cmb_Gender_id.TabIndex = 17;
            // 
            // cmb_s_nat
            // 
            this.cmb_s_nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_s_nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_s_nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_s_nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_s_nat.FormattingEnabled = true;
            this.cmb_s_nat.Location = new System.Drawing.Point(18, 309);
            this.cmb_s_nat.Name = "cmb_s_nat";
            this.cmb_s_nat.Size = new System.Drawing.Size(292, 24);
            this.cmb_s_nat.TabIndex = 15;
            // 
            // Grd_CustSen_Name
            // 
            this.Grd_CustSen_Name.AllowUserToAddRows = false;
            this.Grd_CustSen_Name.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_CustSen_Name.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_CustSen_Name.BackgroundColor = System.Drawing.Color.White;
            this.Grd_CustSen_Name.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_CustSen_Name.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_CustSen_Name.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_CustSen_Name.ColumnHeadersHeight = 45;
            this.Grd_CustSen_Name.ColumnHeadersVisible = false;
            this.Grd_CustSen_Name.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column17,
            this.Column1});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_CustSen_Name.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_CustSen_Name.Location = new System.Drawing.Point(419, 32);
            this.Grd_CustSen_Name.Name = "Grd_CustSen_Name";
            this.Grd_CustSen_Name.ReadOnly = true;
            this.Grd_CustSen_Name.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_CustSen_Name.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_CustSen_Name.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_CustSen_Name.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_CustSen_Name.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_CustSen_Name.Size = new System.Drawing.Size(403, 157);
            this.Grd_CustSen_Name.TabIndex = 0;
            this.Grd_CustSen_Name.SelectionChanged += new System.EventHandler(this.Grd_CustSen_Name_SelectionChanged);
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "per_id";
            this.Column15.Frozen = true;
            this.Column15.HeaderText = "رمز الزبون";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 70;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Per_AName";
            this.Column17.HeaderText = "اسم الزبون";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 250;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Aouthorized_name";
            this.Column1.HeaderText = "مخول";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Txt_Notes
            // 
            this.Txt_Notes.Location = new System.Drawing.Point(138, 517);
            this.Txt_Notes.MaxLength = 199;
            this.Txt_Notes.Name = "Txt_Notes";
            this.Txt_Notes.Size = new System.Drawing.Size(595, 20);
            this.Txt_Notes.TabIndex = 27;
            // 
            // Txt_T_Purpose
            // 
            this.Txt_T_Purpose.Location = new System.Drawing.Point(259, 467);
            this.Txt_T_Purpose.MaxLength = 199;
            this.Txt_T_Purpose.Name = "Txt_T_Purpose";
            this.Txt_T_Purpose.Size = new System.Drawing.Size(474, 20);
            this.Txt_T_Purpose.TabIndex = 24;
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.Location = new System.Drawing.Point(262, 491);
            this.Txt_Relionship.MaxLength = 99;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.Size = new System.Drawing.Size(472, 20);
            this.Txt_Relionship.TabIndex = 26;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.Location = new System.Drawing.Point(324, 439);
            this.Txt_Soruce_money.MaxLength = 99;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.Size = new System.Drawing.Size(410, 20);
            this.Txt_Soruce_money.TabIndex = 22;
            // 
            // txt_Mother_name
            // 
            this.txt_Mother_name.Location = new System.Drawing.Point(478, 339);
            this.txt_Mother_name.MaxLength = 49;
            this.txt_Mother_name.Name = "txt_Mother_name";
            this.txt_Mother_name.Size = new System.Drawing.Size(244, 20);
            this.txt_Mother_name.TabIndex = 16;
            // 
            // Txt_mail
            // 
            this.Txt_mail.Location = new System.Drawing.Point(478, 365);
            this.Txt_mail.MaxLength = 29;
            this.Txt_mail.Name = "Txt_mail";
            this.Txt_mail.Size = new System.Drawing.Size(247, 20);
            this.Txt_mail.TabIndex = 18;
            // 
            // Txt_S_Birth_Place
            // 
            this.Txt_S_Birth_Place.Location = new System.Drawing.Point(478, 309);
            this.Txt_S_Birth_Place.MaxLength = 49;
            this.Txt_S_Birth_Place.Name = "Txt_S_Birth_Place";
            this.Txt_S_Birth_Place.Size = new System.Drawing.Size(247, 20);
            this.Txt_S_Birth_Place.TabIndex = 14;
            // 
            // Txt_Doc_S_Issue
            // 
            this.Txt_Doc_S_Issue.Location = new System.Drawing.Point(477, 282);
            this.Txt_Doc_S_Issue.MaxLength = 49;
            this.Txt_Doc_S_Issue.Name = "Txt_Doc_S_Issue";
            this.Txt_Doc_S_Issue.Size = new System.Drawing.Size(247, 20);
            this.Txt_Doc_S_Issue.TabIndex = 12;
            // 
            // Txt_S_Phone
            // 
            this.Txt_S_Phone.Location = new System.Drawing.Point(17, 186);
            this.Txt_S_Phone.MaxLength = 19;
            this.Txt_S_Phone.Name = "Txt_S_Phone";
            this.Txt_S_Phone.Size = new System.Drawing.Size(210, 20);
            this.Txt_S_Phone.TabIndex = 7;
            // 
            // TxtS_State
            // 
            this.TxtS_State.Location = new System.Drawing.Point(16, 91);
            this.TxtS_State.MaxLength = 99;
            this.TxtS_State.Name = "TxtS_State";
            this.TxtS_State.Size = new System.Drawing.Size(302, 20);
            this.TxtS_State.TabIndex = 2;
            // 
            // TxtS_Post_Code
            // 
            this.TxtS_Post_Code.Location = new System.Drawing.Point(17, 161);
            this.TxtS_Post_Code.MaxLength = 99;
            this.TxtS_Post_Code.Name = "TxtS_Post_Code";
            this.TxtS_Post_Code.Size = new System.Drawing.Size(301, 20);
            this.TxtS_Post_Code.TabIndex = 5;
            // 
            // Txts_street
            // 
            this.Txts_street.Location = new System.Drawing.Point(17, 138);
            this.Txts_street.MaxLength = 99;
            this.Txts_street.Name = "Txts_street";
            this.Txts_street.Size = new System.Drawing.Size(301, 20);
            this.Txts_street.TabIndex = 4;
            // 
            // txtS_Suburb
            // 
            this.txtS_Suburb.Location = new System.Drawing.Point(17, 114);
            this.txtS_Suburb.MaxLength = 99;
            this.txtS_Suburb.Name = "txtS_Suburb";
            this.txtS_Suburb.Size = new System.Drawing.Size(301, 20);
            this.txtS_Suburb.TabIndex = 3;
            // 
            // Txt_Sender
            // 
            this.Txt_Sender.Location = new System.Drawing.Point(472, 11);
            this.Txt_Sender.MaxLength = 49;
            this.Txt_Sender.Name = "Txt_Sender";
            this.Txt_Sender.Size = new System.Drawing.Size(285, 20);
            this.Txt_Sender.TabIndex = 0;
            this.Txt_Sender.TextChanged += new System.EventHandler(this.Txt_Sender_TextChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(739, 519);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(75, 16);
            this.label37.TabIndex = 834;
            this.label37.Text = "الملاحظـــــات:";
            // 
            // Cmb_Case_Purpose
            // 
            this.Cmb_Case_Purpose.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Case_Purpose.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Case_Purpose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Case_Purpose.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Case_Purpose.FormattingEnabled = true;
            this.Cmb_Case_Purpose.Location = new System.Drawing.Point(21, 465);
            this.Cmb_Case_Purpose.Name = "Cmb_Case_Purpose";
            this.Cmb_Case_Purpose.Size = new System.Drawing.Size(232, 24);
            this.Cmb_Case_Purpose.TabIndex = 25;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(739, 469);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(86, 16);
            this.label36.TabIndex = 831;
            this.label36.Text = "غرض التحويـــل:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(740, 493);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(80, 16);
            this.label35.TabIndex = 829;
            this.label35.Text = "علاقـــة م . س :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(739, 441);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(86, 16);
            this.label31.TabIndex = 827;
            this.label31.Text = "مصدر المـــــــال:";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(2, 402);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(685, 1);
            this.flowLayoutPanel7.TabIndex = 826;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-80, 3);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel11.TabIndex = 758;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(959, 555);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(21, 1);
            this.flowLayoutPanel8.TabIndex = 825;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-744, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel10.TabIndex = 758;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Maroon;
            this.label30.Location = new System.Drawing.Point(688, 391);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(135, 14);
            this.label30.TabIndex = 824;
            this.label30.Text = "معلومــــات اخـــرى......";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(319, 341);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(82, 16);
            this.label29.TabIndex = 822;
            this.label29.Text = "الجنـــــــــــــس:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(730, 341);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(82, 16);
            this.label28.TabIndex = 820;
            this.label28.Text = "اســــــــــــم الام:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(319, 313);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(83, 16);
            this.label27.TabIndex = 818;
            this.label27.Text = "الجنسيــــــــــــة:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Maroon;
            this.label33.Location = new System.Drawing.Point(24, 285);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(87, 14);
            this.label33.TabIndex = 817;
            this.label33.Text = "dd/mm/yyyy";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(319, 286);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(83, 16);
            this.label24.TabIndex = 815;
            this.label24.Text = "التولــــــــــــــــد:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Maroon;
            this.label32.Location = new System.Drawing.Point(26, 260);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(87, 14);
            this.label32.TabIndex = 814;
            this.label32.Text = "dd/mm/yyyy";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(315, 260);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(87, 16);
            this.label25.TabIndex = 812;
            this.label25.Text = "انتهائهـــــــــــــا:";
            // 
            // Txt_S_Doc_No
            // 
            this.Txt_S_Doc_No.BackColor = System.Drawing.Color.White;
            this.Txt_S_Doc_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Doc_No.Location = new System.Drawing.Point(18, 227);
            this.Txt_S_Doc_No.MaxLength = 49;
            this.Txt_S_Doc_No.Name = "Txt_S_Doc_No";
            this.Txt_S_Doc_No.Size = new System.Drawing.Size(292, 23);
            this.Txt_S_Doc_No.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(315, 231);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 16);
            this.label26.TabIndex = 810;
            this.label26.Text = "رقم الوثيقــــــــــة:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(734, 367);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(90, 16);
            this.label23.TabIndex = 808;
            this.label23.Text = "البريد الالكتروني:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(737, 414);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(68, 16);
            this.label22.TabIndex = 806;
            this.label22.Text = "المهنــــــــــة:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(734, 313);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(77, 16);
            this.label21.TabIndex = 804;
            this.label21.Text = "محــــل الولادة:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(734, 286);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(77, 16);
            this.label20.TabIndex = 802;
            this.label20.Text = "م:اصدارهـــــا :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(734, 260);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(76, 16);
            this.label18.TabIndex = 799;
            this.label18.Text = "تاريخهـــــــــا:";
            // 
            // Cmb_S_Doc_Type
            // 
            this.Cmb_S_Doc_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_S_Doc_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_S_Doc_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_S_Doc_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_S_Doc_Type.FormattingEnabled = true;
            this.Cmb_S_Doc_Type.Location = new System.Drawing.Point(477, 227);
            this.Cmb_S_Doc_Type.Name = "Cmb_S_Doc_Type";
            this.Cmb_S_Doc_Type.Size = new System.Drawing.Size(247, 24);
            this.Cmb_S_Doc_Type.TabIndex = 8;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(734, 231);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 16);
            this.label19.TabIndex = 769;
            this.label19.Text = "نوع الوثيقـــــــة:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(968, 249);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(21, 1);
            this.flowLayoutPanel5.TabIndex = 767;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-744, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(1, 220);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(712, 1);
            this.flowLayoutPanel3.TabIndex = 766;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-53, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel4.TabIndex = 758;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Maroon;
            this.label17.Location = new System.Drawing.Point(715, 209);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(107, 14);
            this.label17.TabIndex = 765;
            this.label17.Text = "الهويــــــــــــــة......";
            // 
            // Cmb_Code_phone_S
            // 
            this.Cmb_Code_phone_S.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Code_phone_S.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Code_phone_S.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Code_phone_S.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Code_phone_S.FormattingEnabled = true;
            this.Cmb_Code_phone_S.Location = new System.Drawing.Point(228, 184);
            this.Cmb_Code_phone_S.Name = "Cmb_Code_phone_S";
            this.Cmb_Code_phone_S.Size = new System.Drawing.Size(90, 24);
            this.Cmb_Code_phone_S.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(327, 188);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 16);
            this.label16.TabIndex = 763;
            this.label16.Text = "الهاتـــــــــــــف:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(323, 93);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(81, 16);
            this.label15.TabIndex = 760;
            this.label15.Text = "المحافظـــــــــة:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(327, 163);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(79, 16);
            this.label14.TabIndex = 758;
            this.label14.Text = "الرمز البريــدي:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(325, 116);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(77, 16);
            this.label13.TabIndex = 756;
            this.label13.Text = "الحــــــــــــــي:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(324, 140);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(78, 16);
            this.label11.TabIndex = 754;
            this.label11.Text = "الزقــــــــــــاق:";
            // 
            // Cmb_S_City
            // 
            this.Cmb_S_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_S_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_S_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_S_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_S_City.FormattingEnabled = true;
            this.Cmb_S_City.Location = new System.Drawing.Point(17, 64);
            this.Cmb_S_City.Name = "Cmb_S_City";
            this.Cmb_S_City.Size = new System.Drawing.Size(301, 24);
            this.Cmb_S_City.TabIndex = 1;
            this.Cmb_S_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_S_City_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(324, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 16);
            this.label10.TabIndex = 753;
            this.label10.Text = "المدينـــــــــــــة:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(300, 46);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(113, 14);
            this.label9.TabIndex = 750;
            this.label9.Text = "الـعنـــــــــــــوان......";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(754, 13);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(54, 14);
            this.label7.TabIndex = 746;
            this.label7.Text = "الاســـم:";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.Chk_Term1);
            this.tabPage2.Controls.Add(this.Txt_another_Rec);
            this.tabPage2.Controls.Add(this.checkBox4);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.en_rec_btn);
            this.tabPage2.Controls.Add(this.ar_rec_btn);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.Grd_CustRec_Name);
            this.tabPage2.Controls.Add(this.Cmb_phone_Code_R);
            this.tabPage2.Controls.Add(this.Txtr_State);
            this.tabPage2.Controls.Add(this.Txtr_Post_Code);
            this.tabPage2.Controls.Add(this.Txtr_Street);
            this.tabPage2.Controls.Add(this.Txtr_Suburb);
            this.tabPage2.Controls.Add(this.Txt_R_Phone);
            this.tabPage2.Controls.Add(this.Txt_Reciever);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.Cmb_R_Nat);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.Cmb_R_City);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.label38);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(833, 547);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "معلومات المستلم";
            // 
            // Chk_Term1
            // 
            this.Chk_Term1.AutoSize = true;
            this.Chk_Term1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Chk_Term1.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Term1.Location = new System.Drawing.Point(314, 25);
            this.Chk_Term1.Name = "Chk_Term1";
            this.Chk_Term1.Size = new System.Drawing.Size(84, 20);
            this.Chk_Term1.TabIndex = 884;
            this.Chk_Term1.Text = "الاسـم الاخر:";
            this.Chk_Term1.UseVisualStyleBackColor = true;
            this.Chk_Term1.CheckedChanged += new System.EventHandler(this.Chk_Term1_CheckedChanged);
            // 
            // Txt_another_Rec
            // 
            this.Txt_another_Rec.Location = new System.Drawing.Point(10, 25);
            this.Txt_another_Rec.MaxLength = 150;
            this.Txt_another_Rec.Name = "Txt_another_Rec";
            this.Txt_another_Rec.Size = new System.Drawing.Size(297, 20);
            this.Txt_another_Rec.TabIndex = 879;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(811, 29);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 878;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(127, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 875;
            this.label8.Text = "Msg_label";
            // 
            // en_rec_btn
            // 
            this.en_rec_btn.Location = new System.Drawing.Point(416, 23);
            this.en_rec_btn.Name = "en_rec_btn";
            this.en_rec_btn.Size = new System.Drawing.Size(28, 23);
            this.en_rec_btn.TabIndex = 874;
            this.en_rec_btn.Text = "EN";
            this.en_rec_btn.UseVisualStyleBackColor = true;
            this.en_rec_btn.Click += new System.EventHandler(this.en_rec_btn_Click);
            // 
            // ar_rec_btn
            // 
            this.ar_rec_btn.Location = new System.Drawing.Point(443, 23);
            this.ar_rec_btn.Name = "ar_rec_btn";
            this.ar_rec_btn.Size = new System.Drawing.Size(29, 23);
            this.ar_rec_btn.TabIndex = 873;
            this.ar_rec_btn.Text = "AR";
            this.ar_rec_btn.UseVisualStyleBackColor = false;
            this.ar_rec_btn.Click += new System.EventHandler(this.ar_rec_btn_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(16, 269);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 25);
            this.button1.TabIndex = 9;
            this.button1.Text = "عرض الوثائق";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Grd_CustRec_Name
            // 
            this.Grd_CustRec_Name.AllowUserToAddRows = false;
            this.Grd_CustRec_Name.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_CustRec_Name.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_CustRec_Name.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_CustRec_Name.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_CustRec_Name.ColumnHeadersHeight = 40;
            this.Grd_CustRec_Name.ColumnHeadersVisible = false;
            this.Grd_CustRec_Name.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.Column2});
            this.Grd_CustRec_Name.Location = new System.Drawing.Point(418, 46);
            this.Grd_CustRec_Name.Name = "Grd_CustRec_Name";
            this.Grd_CustRec_Name.ReadOnly = true;
            this.Grd_CustRec_Name.RowHeadersVisible = false;
            this.Grd_CustRec_Name.RowHeadersWidth = 15;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_CustRec_Name.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_CustRec_Name.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_CustRec_Name.Size = new System.Drawing.Size(408, 200);
            this.Grd_CustRec_Name.TabIndex = 856;
            this.Grd_CustRec_Name.SelectionChanged += new System.EventHandler(this.Grd_CustRec_Name_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "per_id";
            this.dataGridViewTextBoxColumn3.HeaderText = "رمز الزبون";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 128;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Per_AName";
            this.dataGridViewTextBoxColumn4.HeaderText = "اسم الزبون";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 250;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = " Aouthorized_name";
            this.Column2.HeaderText = "اسم المخول";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Cmb_phone_Code_R
            // 
            this.Cmb_phone_Code_R.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_phone_Code_R.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_phone_Code_R.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_phone_Code_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_phone_Code_R.FormattingEnabled = true;
            this.Cmb_phone_Code_R.Location = new System.Drawing.Point(214, 113);
            this.Cmb_phone_Code_R.Name = "Cmb_phone_Code_R";
            this.Cmb_phone_Code_R.Size = new System.Drawing.Size(92, 24);
            this.Cmb_phone_Code_R.TabIndex = 3;
            // 
            // Txtr_State
            // 
            this.Txtr_State.Location = new System.Drawing.Point(15, 144);
            this.Txtr_State.MaxLength = 99;
            this.Txtr_State.Name = "Txtr_State";
            this.Txtr_State.Size = new System.Drawing.Size(291, 20);
            this.Txtr_State.TabIndex = 5;
            // 
            // Txtr_Post_Code
            // 
            this.Txtr_Post_Code.Location = new System.Drawing.Point(14, 223);
            this.Txtr_Post_Code.MaxLength = 99;
            this.Txtr_Post_Code.Name = "Txtr_Post_Code";
            this.Txtr_Post_Code.Size = new System.Drawing.Size(291, 20);
            this.Txtr_Post_Code.TabIndex = 8;
            // 
            // Txtr_Street
            // 
            this.Txtr_Street.Location = new System.Drawing.Point(14, 197);
            this.Txtr_Street.MaxLength = 99;
            this.Txtr_Street.Name = "Txtr_Street";
            this.Txtr_Street.Size = new System.Drawing.Size(291, 20);
            this.Txtr_Street.TabIndex = 7;
            // 
            // Txtr_Suburb
            // 
            this.Txtr_Suburb.Location = new System.Drawing.Point(14, 170);
            this.Txtr_Suburb.MaxLength = 99;
            this.Txtr_Suburb.Name = "Txtr_Suburb";
            this.Txtr_Suburb.Size = new System.Drawing.Size(291, 20);
            this.Txtr_Suburb.TabIndex = 6;
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.Location = new System.Drawing.Point(14, 114);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.Size = new System.Drawing.Size(200, 20);
            this.Txt_R_Phone.TabIndex = 4;
            // 
            // Txt_Reciever
            // 
            this.Txt_Reciever.Location = new System.Drawing.Point(473, 25);
            this.Txt_Reciever.MaxLength = 49;
            this.Txt_Reciever.Name = "Txt_Reciever";
            this.Txt_Reciever.Size = new System.Drawing.Size(283, 20);
            this.Txt_Reciever.TabIndex = 0;
            this.Txt_Reciever.TextChanged += new System.EventHandler(this.Txt_Reciever_TextChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(311, 146);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(84, 16);
            this.label45.TabIndex = 819;
            this.label45.Text = "المحافظـــــــــة :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(311, 225);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(79, 16);
            this.label44.TabIndex = 817;
            this.label44.Text = "الرمز البريــدي:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(311, 171);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(77, 16);
            this.label43.TabIndex = 815;
            this.label43.Text = "الحــــــــــــــي:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(312, 199);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(78, 16);
            this.label42.TabIndex = 813;
            this.label42.Text = "الزقــــــــــــاق:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(311, 117);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(81, 16);
            this.label41.TabIndex = 810;
            this.label41.Text = "الهاتـــــــــــــف:";
            // 
            // Cmb_R_Nat
            // 
            this.Cmb_R_Nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_Nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_Nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_Nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_Nat.FormattingEnabled = true;
            this.Cmb_R_Nat.Location = new System.Drawing.Point(14, 83);
            this.Cmb_R_Nat.Name = "Cmb_R_Nat";
            this.Cmb_R_Nat.Size = new System.Drawing.Size(292, 24);
            this.Cmb_R_Nat.TabIndex = 2;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(311, 87);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label40.Size = new System.Drawing.Size(80, 16);
            this.label40.TabIndex = 808;
            this.label40.Text = "الجنسيـــــــــــة:";
            // 
            // Cmb_R_City
            // 
            this.Cmb_R_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_City.FormattingEnabled = true;
            this.Cmb_R_City.Location = new System.Drawing.Point(14, 53);
            this.Cmb_R_City.Name = "Cmb_R_City";
            this.Cmb_R_City.Size = new System.Drawing.Size(292, 24);
            this.Cmb_R_City.TabIndex = 1;
            this.Cmb_R_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_R_City_SelectedIndexChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(311, 57);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 16);
            this.label39.TabIndex = 807;
            this.label39.Text = "المدينـــــــــــــة:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(756, 27);
            this.label38.Name = "label38";
            this.label38.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label38.Size = new System.Drawing.Size(54, 14);
            this.label38.TabIndex = 804;
            this.label38.Text = "الاســـم:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.Controls.Add(this.label91);
            this.tabPage3.Controls.Add(this.lbl_comm);
            this.tabPage3.Controls.Add(this.checkBox5);
            this.tabPage3.Controls.Add(this.label48);
            this.tabPage3.Controls.Add(this.checkBox2);
            this.tabPage3.Controls.Add(this.flowLayoutPanel1);
            this.tabPage3.Controls.Add(this.label88);
            this.tabPage3.Controls.Add(this.Lab_for_Com);
            this.tabPage3.Controls.Add(this.Lab_for_Rem);
            this.tabPage3.Controls.Add(this.lab_Loc_comm);
            this.tabPage3.Controls.Add(this.label86);
            this.tabPage3.Controls.Add(this.label87);
            this.tabPage3.Controls.Add(this.label83);
            this.tabPage3.Controls.Add(this.label84);
            this.tabPage3.Controls.Add(this.label85);
            this.tabPage3.Controls.Add(this.label82);
            this.tabPage3.Controls.Add(this.label81);
            this.tabPage3.Controls.Add(this.Lab_Loc_Rem);
            this.tabPage3.Controls.Add(this.Cbo_Oper);
            this.tabPage3.Controls.Add(this.label76);
            this.tabPage3.Controls.Add(this.Txt_Accproc);
            this.tabPage3.Controls.Add(this.Grd_procer);
            this.tabPage3.Controls.Add(this.Txt_procer);
            this.tabPage3.Controls.Add(this.label68);
            this.tabPage3.Controls.Add(this.label75);
            this.tabPage3.Controls.Add(this.label67);
            this.tabPage3.Controls.Add(this.chk_comm_info);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.label47);
            this.tabPage3.Controls.Add(this.Cbo_rem_path);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label61);
            this.tabPage3.Controls.Add(this.flowLayoutPanel19);
            this.tabPage3.Controls.Add(this.Cmb_PR_Cur_Id);
            this.tabPage3.Controls.Add(this.label58);
            this.tabPage3.Controls.Add(this.Cmb_R_CUR_ID);
            this.tabPage3.Controls.Add(this.label57);
            this.tabPage3.Controls.Add(this.label56);
            this.tabPage3.Controls.Add(this.label55);
            this.tabPage3.Controls.Add(this.flowLayoutPanel13);
            this.tabPage3.Controls.Add(this.cmb_comm_type);
            this.tabPage3.Controls.Add(this.label60);
            this.tabPage3.Controls.Add(this.Cmb_Comm_Cur);
            this.tabPage3.Controls.Add(this.checkBox1);
            this.tabPage3.Controls.Add(this.cmb_cur_comm);
            this.tabPage3.Controls.Add(this.label51);
            this.tabPage3.Controls.Add(this.label52);
            this.tabPage3.Controls.Add(this.label53);
            this.tabPage3.Controls.Add(this.label74);
            this.tabPage3.Controls.Add(this.label50);
            this.tabPage3.Controls.Add(this.label49);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.flowLayoutPanel2);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.flowLayoutPanel18);
            this.tabPage3.Controls.Add(this.label73);
            this.tabPage3.Controls.Add(this.label70);
            this.tabPage3.Controls.Add(this.label71);
            this.tabPage3.Controls.Add(this.label72);
            this.tabPage3.Controls.Add(this.cmb_cur);
            this.tabPage3.Controls.Add(this.label69);
            this.tabPage3.Controls.Add(this.flowLayoutPanel16);
            this.tabPage3.Controls.Add(this.flowLayoutPanel15);
            this.tabPage3.Controls.Add(this.flowLayoutPanel14);
            this.tabPage3.Controls.Add(this.label62);
            this.tabPage3.Controls.Add(this.label63);
            this.tabPage3.Controls.Add(this.Cmb_T_City);
            this.tabPage3.Controls.Add(this.label54);
            this.tabPage3.Controls.Add(this.shapeContainer1);
            this.tabPage3.Controls.Add(this.TXt_Total_Comm);
            this.tabPage3.Controls.Add(this.Txt_Add_Com_Amount);
            this.tabPage3.Controls.Add(this.Txt_Tot_amount2);
            this.tabPage3.Controls.Add(this.txt_minrate_comm2);
            this.tabPage3.Controls.Add(this.txt_maxrate_comm2);
            this.tabPage3.Controls.Add(this.Txt_ExRate_comm2);
            this.tabPage3.Controls.Add(this.Txt_Com_Amnt2);
            this.tabPage3.Controls.Add(this.txt_locamount_comm2);
            this.tabPage3.Controls.Add(this.txt_minrate_rem2);
            this.tabPage3.Controls.Add(this.txt_maxrate_rem2);
            this.tabPage3.Controls.Add(this.Txt_ExRate_Rem2);
            this.tabPage3.Controls.Add(this.Txt_Rem_Amount2);
            this.tabPage3.Controls.Add(this.txt_locamount_rem2);
            this.tabPage3.Controls.Add(this.Txtr_amount);
            this.tabPage3.Controls.Add(this.Txt_Tot_amount);
            this.tabPage3.Controls.Add(this.txt_locamount_comm);
            this.tabPage3.Controls.Add(this.txt_locamount_rem);
            this.tabPage3.Controls.Add(this.txt_minrate_comm);
            this.tabPage3.Controls.Add(this.txt_maxrate_comm);
            this.tabPage3.Controls.Add(this.txt_minrate_rem);
            this.tabPage3.Controls.Add(this.txt_maxrate_rem);
            this.tabPage3.Controls.Add(this.Txt_ExRate_comm);
            this.tabPage3.Controls.Add(this.Txt_Com_Amnt);
            this.tabPage3.Controls.Add(this.Txt_ExRate_Rem);
            this.tabPage3.Controls.Add(this.Txt_Rem_Amount);
            this.tabPage3.Controls.Add(this.Txt_Com_Amount);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabPage3.Size = new System.Drawing.Size(833, 547);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "المعلومات المالية";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label91.ForeColor = System.Drawing.Color.Navy;
            this.label91.Location = new System.Drawing.Point(523, 168);
            this.label91.Name = "label91";
            this.label91.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label91.Size = new System.Drawing.Size(52, 16);
            this.label91.TabIndex = 1015;
            this.label91.Text = "اجمالــي:";
            // 
            // lbl_comm
            // 
            this.lbl_comm.AutoSize = true;
            this.lbl_comm.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_comm.ForeColor = System.Drawing.Color.Firebrick;
            this.lbl_comm.Location = new System.Drawing.Point(325, 189);
            this.lbl_comm.Name = "lbl_comm";
            this.lbl_comm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_comm.Size = new System.Drawing.Size(209, 14);
            this.lbl_comm.TabIndex = 1013;
            this.lbl_comm.Text = "(لاتــوجـــــــــــــد عـمـــــــــــــــــــــولات)";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkBox5.ForeColor = System.Drawing.Color.Maroon;
            this.checkBox5.Location = new System.Drawing.Point(556, 44);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 6;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(521, 93);
            this.label48.Name = "label48";
            this.label48.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label48.Size = new System.Drawing.Size(54, 16);
            this.label48.TabIndex = 1011;
            this.label48.Text = "اضافــي :";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkBox2.ForeColor = System.Drawing.Color.Maroon;
            this.checkBox2.Location = new System.Drawing.Point(652, 263);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(176, 18);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "تحويل من العملة المحلية1";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 274);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(658, 1);
            this.flowLayoutPanel1.TabIndex = 1007;
            this.flowLayoutPanel1.Visible = false;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Navy;
            this.label88.Location = new System.Drawing.Point(153, 372);
            this.label88.Name = "label88";
            this.label88.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label88.Size = new System.Drawing.Size(74, 14);
            this.label88.TabIndex = 1004;
            this.label88.Text = "المجمـــــوع:";
            this.label88.Visible = false;
            // 
            // Lab_for_Com
            // 
            this.Lab_for_Com.AutoSize = true;
            this.Lab_for_Com.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lab_for_Com.ForeColor = System.Drawing.Color.Navy;
            this.Lab_for_Com.Location = new System.Drawing.Point(414, 324);
            this.Lab_for_Com.Name = "Lab_for_Com";
            this.Lab_for_Com.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lab_for_Com.Size = new System.Drawing.Size(78, 14);
            this.Lab_for_Com.TabIndex = 1003;
            this.Lab_for_Com.Text = "مبلغ العمولة";
            this.Lab_for_Com.Visible = false;
            // 
            // Lab_for_Rem
            // 
            this.Lab_for_Rem.AutoSize = true;
            this.Lab_for_Rem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lab_for_Rem.ForeColor = System.Drawing.Color.Navy;
            this.Lab_for_Rem.Location = new System.Drawing.Point(410, 280);
            this.Lab_for_Rem.Name = "Lab_for_Rem";
            this.Lab_for_Rem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lab_for_Rem.Size = new System.Drawing.Size(76, 14);
            this.Lab_for_Rem.TabIndex = 1002;
            this.Lab_for_Rem.Text = "مبلغ الحوالة";
            this.Lab_for_Rem.Visible = false;
            // 
            // lab_Loc_comm
            // 
            this.lab_Loc_comm.AutoSize = true;
            this.lab_Loc_comm.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_Loc_comm.ForeColor = System.Drawing.Color.Navy;
            this.lab_Loc_comm.Location = new System.Drawing.Point(712, 347);
            this.lab_Loc_comm.Name = "lab_Loc_comm";
            this.lab_Loc_comm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lab_Loc_comm.Size = new System.Drawing.Size(108, 14);
            this.lab_Loc_comm.TabIndex = 1001;
            this.lab_Loc_comm.Text = "مبلغ العمولة(ع.م)\r\n";
            this.lab_Loc_comm.Visible = false;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Navy;
            this.label86.Location = new System.Drawing.Point(393, 347);
            this.label86.Name = "label86";
            this.label86.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label86.Size = new System.Drawing.Size(17, 14);
            this.label86.TabIndex = 997;
            this.label86.Text = "=";
            this.label86.Visible = false;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Navy;
            this.label87.Location = new System.Drawing.Point(558, 347);
            this.label87.Name = "label87";
            this.label87.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label87.Size = new System.Drawing.Size(14, 14);
            this.label87.TabIndex = 995;
            this.label87.Text = "/";
            this.label87.Visible = false;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Navy;
            this.label83.Location = new System.Drawing.Point(56, 280);
            this.label83.Name = "label83";
            this.label83.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label83.Size = new System.Drawing.Size(71, 14);
            this.label83.TabIndex = 993;
            this.label83.Text = "الحد الادنى";
            this.label83.Visible = false;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Navy;
            this.label84.Location = new System.Drawing.Point(174, 280);
            this.label84.Name = "label84";
            this.label84.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label84.Size = new System.Drawing.Size(73, 14);
            this.label84.TabIndex = 992;
            this.label84.Text = "الحد الاعلى";
            this.label84.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Navy;
            this.label85.Location = new System.Drawing.Point(285, 280);
            this.label85.Name = "label85";
            this.label85.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label85.Size = new System.Drawing.Size(80, 14);
            this.label85.TabIndex = 991;
            this.label85.Text = "المعــــــــــادل";
            this.label85.Visible = false;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Navy;
            this.label82.Location = new System.Drawing.Point(392, 303);
            this.label82.Name = "label82";
            this.label82.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label82.Size = new System.Drawing.Size(17, 14);
            this.label82.TabIndex = 987;
            this.label82.Text = "=";
            this.label82.Visible = false;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Navy;
            this.label81.Location = new System.Drawing.Point(557, 303);
            this.label81.Name = "label81";
            this.label81.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label81.Size = new System.Drawing.Size(14, 14);
            this.label81.TabIndex = 985;
            this.label81.Text = "/";
            this.label81.Visible = false;
            // 
            // Lab_Loc_Rem
            // 
            this.Lab_Loc_Rem.AutoSize = true;
            this.Lab_Loc_Rem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lab_Loc_Rem.ForeColor = System.Drawing.Color.Navy;
            this.Lab_Loc_Rem.Location = new System.Drawing.Point(709, 303);
            this.Lab_Loc_Rem.Name = "Lab_Loc_Rem";
            this.Lab_Loc_Rem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lab_Loc_Rem.Size = new System.Drawing.Size(110, 14);
            this.Lab_Loc_Rem.TabIndex = 984;
            this.Lab_Loc_Rem.Text = "مبلغ الحوالة (ع.م)";
            this.Lab_Loc_Rem.Visible = false;
            // 
            // Cbo_Oper
            // 
            this.Cbo_Oper.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Oper.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Oper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Oper.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Oper.FormattingEnabled = true;
            this.Cbo_Oper.Items.AddRange(new object[] {
            "نقـــــــــــــدي",
            "على استحقاق"});
            this.Cbo_Oper.Location = new System.Drawing.Point(9, 11);
            this.Cbo_Oper.Name = "Cbo_Oper";
            this.Cbo_Oper.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cbo_Oper.Size = new System.Drawing.Size(109, 24);
            this.Cbo_Oper.TabIndex = 2;
            this.Cbo_Oper.SelectedIndexChanged += new System.EventHandler(this.Cbo_Oper_SelectedIndexChanged);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Navy;
            this.label76.Location = new System.Drawing.Point(122, 16);
            this.label76.Name = "label76";
            this.label76.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label76.Size = new System.Drawing.Size(84, 14);
            this.label76.TabIndex = 957;
            this.label76.Text = "نوع العمليـــة:";
            // 
            // Txt_Accproc
            // 
            this.Txt_Accproc.BackColor = System.Drawing.Color.White;
            this.Txt_Accproc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Accproc.Location = new System.Drawing.Point(9, 191);
            this.Txt_Accproc.Name = "Txt_Accproc";
            this.Txt_Accproc.ReadOnly = true;
            this.Txt_Accproc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Accproc.Size = new System.Drawing.Size(239, 23);
            this.Txt_Accproc.TabIndex = 953;
            this.Txt_Accproc.Visible = false;
            // 
            // Grd_procer
            // 
            this.Grd_procer.AllowUserToAddRows = false;
            this.Grd_procer.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_procer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_procer.BackgroundColor = System.Drawing.Color.White;
            this.Grd_procer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_procer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_procer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_procer.ColumnHeadersHeight = 45;
            this.Grd_procer.ColumnHeadersVisible = false;
            this.Grd_procer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_procer.DefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_procer.Location = new System.Drawing.Point(9, 82);
            this.Grd_procer.Name = "Grd_procer";
            this.Grd_procer.ReadOnly = true;
            this.Grd_procer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Grd_procer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_procer.RowHeadersVisible = false;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_procer.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_procer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_procer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_procer.Size = new System.Drawing.Size(301, 107);
            this.Grd_procer.TabIndex = 955;
            this.Grd_procer.Visible = false;
            this.Grd_procer.SelectionChanged += new System.EventHandler(this.Grd_procer_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الوسيط";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ASub_CustName";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم الوسيط";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // Txt_procer
            // 
            this.Txt_procer.BackColor = System.Drawing.Color.White;
            this.Txt_procer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_procer.Location = new System.Drawing.Point(9, 59);
            this.Txt_procer.Name = "Txt_procer";
            this.Txt_procer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_procer.Size = new System.Drawing.Size(239, 22);
            this.Txt_procer.TabIndex = 954;
            this.Txt_procer.Visible = false;
            this.Txt_procer.TextChanged += new System.EventHandler(this.Txt_procer_TextChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Navy;
            this.label68.Location = new System.Drawing.Point(253, 196);
            this.label68.Name = "label68";
            this.label68.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label68.Size = new System.Drawing.Size(60, 14);
            this.label68.TabIndex = 952;
            this.label68.Text = "الحسـاب:";
            this.label68.Visible = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Navy;
            this.label75.Location = new System.Drawing.Point(248, 65);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(58, 14);
            this.label75.TabIndex = 951;
            this.label75.Text = ": الوسيط";
            this.label75.Visible = false;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Firebrick;
            this.label67.Location = new System.Drawing.Point(313, 255);
            this.label67.Name = "label67";
            this.label67.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label67.Size = new System.Drawing.Size(209, 14);
            this.label67.TabIndex = 922;
            this.label67.Text = "(لاتــوجـــــــــــــد عـمـــــــــــــــــــــولات)";
            // 
            // chk_comm_info
            // 
            this.chk_comm_info.AutoSize = true;
            this.chk_comm_info.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk_comm_info.ForeColor = System.Drawing.Color.Maroon;
            this.chk_comm_info.Location = new System.Drawing.Point(661, 232);
            this.chk_comm_info.Name = "chk_comm_info";
            this.chk_comm_info.Size = new System.Drawing.Size(167, 18);
            this.chk_comm_info.TabIndex = 9;
            this.chk_comm_info.Text = "عرض معلومـات العمولـــة";
            this.chk_comm_info.UseVisualStyleBackColor = true;
            this.chk_comm_info.CheckedChanged += new System.EventHandler(this.chk_comm_info_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_com_type);
            this.groupBox1.Controls.Add(this.label66);
            this.groupBox1.Controls.Add(this.txt_com_curr);
            this.groupBox1.Controls.Add(this.label65);
            this.groupBox1.Controls.Add(this.label64);
            this.groupBox1.Controls.Add(this.txt_commamount);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.groupBox1.Location = new System.Drawing.Point(5, 221);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(656, 34);
            this.groupBox1.TabIndex = 917;
            this.groupBox1.TabStop = false;
            // 
            // txt_com_type
            // 
            this.txt_com_type.BackColor = System.Drawing.Color.White;
            this.txt_com_type.Enabled = false;
            this.txt_com_type.Location = new System.Drawing.Point(9, 10);
            this.txt_com_type.MaxLength = 49;
            this.txt_com_type.Name = "txt_com_type";
            this.txt_com_type.Size = new System.Drawing.Size(135, 20);
            this.txt_com_type.TabIndex = 920;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label66.ForeColor = System.Drawing.Color.Navy;
            this.label66.Location = new System.Drawing.Point(150, 12);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(48, 16);
            this.label66.TabIndex = 919;
            this.label66.Text = "نوعــها:";
            // 
            // txt_com_curr
            // 
            this.txt_com_curr.BackColor = System.Drawing.Color.White;
            this.txt_com_curr.Enabled = false;
            this.txt_com_curr.Location = new System.Drawing.Point(202, 10);
            this.txt_com_curr.MaxLength = 49;
            this.txt_com_curr.Name = "txt_com_curr";
            this.txt_com_curr.Size = new System.Drawing.Size(135, 20);
            this.txt_com_curr.TabIndex = 917;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(340, 13);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(86, 14);
            this.label65.TabIndex = 916;
            this.label65.Text = "عملة العمولة:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label64.ForeColor = System.Drawing.Color.Navy;
            this.label64.Location = new System.Drawing.Point(569, 13);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(82, 14);
            this.label64.TabIndex = 914;
            this.label64.Text = "مبلغ العمولة:";
            // 
            // txt_commamount
            // 
            this.txt_commamount.BackColor = System.Drawing.Color.White;
            this.txt_commamount.Enabled = false;
            this.txt_commamount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_commamount.Location = new System.Drawing.Point(429, 9);
            this.txt_commamount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_commamount.Name = "txt_commamount";
            this.txt_commamount.NumberDecimalDigits = 3;
            this.txt_commamount.NumberDecimalSeparator = ".";
            this.txt_commamount.NumberGroupSeparator = ",";
            this.txt_commamount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_commamount.Size = new System.Drawing.Size(135, 23);
            this.txt_commamount.TabIndex = 915;
            this.txt_commamount.Text = "0.000";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(579, 449);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(49, 16);
            this.label47.TabIndex = 915;
            this.label47.Text = "العمـلــــة";
            this.label47.Visible = false;
            // 
            // Cbo_rem_path
            // 
            this.Cbo_rem_path.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_rem_path.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_rem_path.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_rem_path.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_rem_path.FormattingEnabled = true;
            this.Cbo_rem_path.Items.AddRange(new object[] {
            "شبكة محلية",
            "شبكة Online "});
            this.Cbo_rem_path.Location = new System.Drawing.Point(207, 11);
            this.Cbo_rem_path.Name = "Cbo_rem_path";
            this.Cbo_rem_path.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cbo_rem_path.Size = new System.Drawing.Size(113, 24);
            this.Cbo_rem_path.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(326, 16);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(101, 14);
            this.label3.TabIndex = 618;
            this.label3.Text = "مسار الحوالـــــة:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Maroon;
            this.label61.Location = new System.Drawing.Point(467, 42);
            this.label61.Name = "label61";
            this.label61.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label61.Size = new System.Drawing.Size(89, 14);
            this.label61.TabIndex = 780;
            this.label61.Text = "العمــــــــولة....";
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(414, 52);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(279, 1);
            this.flowLayoutPanel19.TabIndex = 914;
            // 
            // Cmb_PR_Cur_Id
            // 
            this.Cmb_PR_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_PR_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_PR_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_PR_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_PR_Cur_Id.FormattingEnabled = true;
            this.Cmb_PR_Cur_Id.Location = new System.Drawing.Point(582, 118);
            this.Cmb_PR_Cur_Id.Name = "Cmb_PR_Cur_Id";
            this.Cmb_PR_Cur_Id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cmb_PR_Cur_Id.Size = new System.Drawing.Size(163, 24);
            this.Cmb_PR_Cur_Id.TabIndex = 5;
            this.Cmb_PR_Cur_Id.SelectedIndexChanged += new System.EventHandler(this.Cmb_PR_Cur_Id_SelectedIndexChanged_1);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(750, 123);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 14);
            this.label58.TabIndex = 913;
            this.label58.Text = ":عملة الدفــع";
            // 
            // Cmb_R_CUR_ID
            // 
            this.Cmb_R_CUR_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_CUR_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_CUR_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_CUR_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_CUR_ID.FormattingEnabled = true;
            this.Cmb_R_CUR_ID.Location = new System.Drawing.Point(582, 90);
            this.Cmb_R_CUR_ID.Name = "Cmb_R_CUR_ID";
            this.Cmb_R_CUR_ID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cmb_R_CUR_ID.Size = new System.Drawing.Size(163, 24);
            this.Cmb_R_CUR_ID.TabIndex = 4;
            this.Cmb_R_CUR_ID.SelectedIndexChanged += new System.EventHandler(this.Cmb_R_CUR_ID_SelectedIndexChanged_1);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(746, 95);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(84, 14);
            this.label57.TabIndex = 912;
            this.label57.Text = ":عملة الحوالة";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Navy;
            this.label56.Location = new System.Drawing.Point(748, 68);
            this.label56.Name = "label56";
            this.label56.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label56.Size = new System.Drawing.Size(80, 14);
            this.label56.TabIndex = 911;
            this.label56.Text = "مبلغ الحوالة:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Maroon;
            this.label55.Location = new System.Drawing.Point(697, 41);
            this.label55.Name = "label55";
            this.label55.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label55.Size = new System.Drawing.Size(130, 14);
            this.label55.TabIndex = 910;
            this.label55.Text = "معلومات الحوالـــــة....";
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(6, 52);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(408, 1);
            this.flowLayoutPanel13.TabIndex = 906;
            // 
            // cmb_comm_type
            // 
            this.cmb_comm_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_comm_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_comm_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_comm_type.Enabled = false;
            this.cmb_comm_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_comm_type.FormattingEnabled = true;
            this.cmb_comm_type.Items.AddRange(new object[] {
            "مقبوضة",
            "مدفوعة"});
            this.cmb_comm_type.Location = new System.Drawing.Point(320, 139);
            this.cmb_comm_type.Name = "cmb_comm_type";
            this.cmb_comm_type.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmb_comm_type.Size = new System.Drawing.Size(197, 24);
            this.cmb_comm_type.TabIndex = 10;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label60.ForeColor = System.Drawing.Color.Navy;
            this.label60.Location = new System.Drawing.Point(518, 145);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(57, 16);
            this.label60.TabIndex = 900;
            this.label60.Text = ":نوعهـــــا";
            // 
            // Cmb_Comm_Cur
            // 
            this.Cmb_Comm_Cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Comm_Cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Comm_Cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Comm_Cur.Enabled = false;
            this.Cmb_Comm_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Comm_Cur.FormattingEnabled = true;
            this.Cmb_Comm_Cur.Location = new System.Drawing.Point(320, 113);
            this.Cmb_Comm_Cur.Name = "Cmb_Comm_Cur";
            this.Cmb_Comm_Cur.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cmb_Comm_Cur.Size = new System.Drawing.Size(197, 24);
            this.Cmb_Comm_Cur.TabIndex = 9;
            this.Cmb_Comm_Cur.SelectedIndexChanged += new System.EventHandler(this.Cmb_Comm_Cur_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkBox1.ForeColor = System.Drawing.Color.Maroon;
            this.checkBox1.Location = new System.Drawing.Point(660, 386);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(168, 18);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "تحويل من العملة المحلية";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cmb_cur_comm
            // 
            this.cmb_cur_comm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_cur_comm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_cur_comm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_cur_comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_cur_comm.FormattingEnabled = true;
            this.cmb_cur_comm.Location = new System.Drawing.Point(534, 466);
            this.cmb_cur_comm.Name = "cmb_cur_comm";
            this.cmb_cur_comm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmb_cur_comm.Size = new System.Drawing.Size(136, 24);
            this.cmb_cur_comm.TabIndex = 17;
            this.cmb_cur_comm.Visible = false;
            this.cmb_cur_comm.SelectedIndexChanged += new System.EventHandler(this.cmb_cur_comm_SelectedIndexChanged_1);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Navy;
            this.label51.Location = new System.Drawing.Point(36, 450);
            this.label51.Name = "label51";
            this.label51.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label51.Size = new System.Drawing.Size(136, 14);
            this.label51.TabIndex = 875;
            this.label51.Text = "المبلغ  بالعملة المحلية";
            this.label51.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(193, 450);
            this.label52.Name = "label52";
            this.label52.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label52.Size = new System.Drawing.Size(71, 14);
            this.label52.TabIndex = 874;
            this.label52.Text = "الحد الادنى";
            this.label52.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Navy;
            this.label53.Location = new System.Drawing.Point(320, 450);
            this.label53.Name = "label53";
            this.label53.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label53.Size = new System.Drawing.Size(73, 14);
            this.label53.TabIndex = 873;
            this.label53.Text = "الحد الاعلى";
            this.label53.Visible = false;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Navy;
            this.label74.Location = new System.Drawing.Point(426, 450);
            this.label74.Name = "label74";
            this.label74.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label74.Size = new System.Drawing.Size(80, 14);
            this.label74.TabIndex = 869;
            this.label74.Text = "المعــــــــــادل";
            this.label74.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(36, 406);
            this.label50.Name = "label50";
            this.label50.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label50.Size = new System.Drawing.Size(136, 14);
            this.label50.TabIndex = 865;
            this.label50.Text = "المبلغ  بالعملة المحلية";
            this.label50.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(194, 406);
            this.label49.Name = "label49";
            this.label49.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label49.Size = new System.Drawing.Size(71, 14);
            this.label49.TabIndex = 864;
            this.label49.Text = "الحد الادنى";
            this.label49.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(320, 406);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 863;
            this.label6.Text = "الحد الاعلى";
            this.label6.Visible = false;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-1, 516);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(832, 1);
            this.flowLayoutPanel2.TabIndex = 812;
            this.flowLayoutPanel2.Visible = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(321, 518);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 26);
            this.button2.TabIndex = 12;
            this.button2.Text = "انهـــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button3.ForeColor = System.Drawing.Color.Navy;
            this.button3.Location = new System.Drawing.Point(416, 518);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(95, 26);
            this.button3.TabIndex = 11;
            this.button3.Text = "موافـــق";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(128, 616);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(865, 1);
            this.flowLayoutPanel18.TabIndex = 809;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Navy;
            this.label73.Location = new System.Drawing.Point(426, 406);
            this.label73.Name = "label73";
            this.label73.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label73.Size = new System.Drawing.Size(80, 14);
            this.label73.TabIndex = 804;
            this.label73.Text = "المعــــــــــادل";
            this.label73.Visible = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Navy;
            this.label70.Location = new System.Drawing.Point(715, 403);
            this.label70.Name = "label70";
            this.label70.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label70.Size = new System.Drawing.Size(76, 14);
            this.label70.TabIndex = 802;
            this.label70.Text = "مبلغ الحوالة";
            this.label70.Visible = false;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Navy;
            this.label71.Location = new System.Drawing.Point(181, 496);
            this.label71.Name = "label71";
            this.label71.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label71.Size = new System.Drawing.Size(74, 14);
            this.label71.TabIndex = 22;
            this.label71.Text = "المجمـــــوع:";
            this.label71.Visible = false;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Navy;
            this.label72.Location = new System.Drawing.Point(713, 450);
            this.label72.Name = "label72";
            this.label72.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label72.Size = new System.Drawing.Size(78, 14);
            this.label72.TabIndex = 798;
            this.label72.Text = "مبلغ العمولة";
            this.label72.Visible = false;
            // 
            // cmb_cur
            // 
            this.cmb_cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_cur.FormattingEnabled = true;
            this.cmb_cur.Location = new System.Drawing.Point(535, 422);
            this.cmb_cur.Name = "cmb_cur";
            this.cmb_cur.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmb_cur.Size = new System.Drawing.Size(136, 24);
            this.cmb_cur.TabIndex = 11;
            this.cmb_cur.Visible = false;
            this.cmb_cur.SelectedIndexChanged += new System.EventHandler(this.cmb_cur_SelectedIndexChanged);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label69.ForeColor = System.Drawing.Color.Navy;
            this.label69.Location = new System.Drawing.Point(579, 405);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(49, 16);
            this.label69.TabIndex = 797;
            this.label69.Text = "العمـلــــة";
            this.label69.Visible = false;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(974, 295);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(19, 2);
            this.flowLayoutPanel16.TabIndex = 795;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-1, 396);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(658, 1);
            this.flowLayoutPanel15.TabIndex = 794;
            this.flowLayoutPanel15.Visible = false;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-3, 219);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(844, 1);
            this.flowLayoutPanel14.TabIndex = 786;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(521, 119);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(54, 16);
            this.label62.TabIndex = 784;
            this.label62.Text = ": العملـــة";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label63.ForeColor = System.Drawing.Color.Navy;
            this.label63.Location = new System.Drawing.Point(525, 69);
            this.label63.Name = "label63";
            this.label63.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label63.Size = new System.Drawing.Size(50, 16);
            this.label63.TabIndex = 781;
            this.label63.Text = "المبلــــغ:";
            // 
            // Cmb_T_City
            // 
            this.Cmb_T_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_T_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_T_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_T_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_T_City.FormattingEnabled = true;
            this.Cmb_T_City.Location = new System.Drawing.Point(434, 11);
            this.Cmb_T_City.Name = "Cmb_T_City";
            this.Cmb_T_City.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cmb_T_City.Size = new System.Drawing.Size(305, 24);
            this.Cmb_T_City.TabIndex = 0;
            this.Cmb_T_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_T_City_SelectedIndexChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(738, 15);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(84, 16);
            this.label54.TabIndex = 621;
            this.label54.Text = ":مدينة التسليــــم";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape3,
            this.lineShape4,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(827, 541);
            this.shapeContainer1.TabIndex = 769;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.Visible = false;
            this.lineShape2.X1 = 313;
            this.lineShape2.X2 = 313;
            this.lineShape2.Y1 = 49;
            this.lineShape2.Y2 = 216;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 1;
            this.lineShape3.X2 = 1;
            this.lineShape3.Y1 = 49;
            this.lineShape3.Y2 = 215;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 969;
            this.lineShape4.X2 = 969;
            this.lineShape4.Y1 = 20;
            this.lineShape4.Y2 = 259;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 573;
            this.lineShape1.X2 = 573;
            this.lineShape1.Y1 = 49;
            this.lineShape1.Y2 = 216;
            // 
            // TXt_Total_Comm
            // 
            this.TXt_Total_Comm.BackColor = System.Drawing.Color.White;
            this.TXt_Total_Comm.Enabled = false;
            this.TXt_Total_Comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXt_Total_Comm.Location = new System.Drawing.Point(320, 164);
            this.TXt_Total_Comm.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TXt_Total_Comm.Name = "TXt_Total_Comm";
            this.TXt_Total_Comm.NumberDecimalDigits = 3;
            this.TXt_Total_Comm.NumberDecimalSeparator = ".";
            this.TXt_Total_Comm.NumberGroupSeparator = ",";
            this.TXt_Total_Comm.ReadOnly = true;
            this.TXt_Total_Comm.Size = new System.Drawing.Size(197, 23);
            this.TXt_Total_Comm.TabIndex = 1014;
            this.TXt_Total_Comm.Text = "0.000";
            // 
            // Txt_Add_Com_Amount
            // 
            this.Txt_Add_Com_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Add_Com_Amount.Enabled = false;
            this.Txt_Add_Com_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Add_Com_Amount.Location = new System.Drawing.Point(320, 89);
            this.Txt_Add_Com_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Add_Com_Amount.Name = "Txt_Add_Com_Amount";
            this.Txt_Add_Com_Amount.NumberDecimalDigits = 3;
            this.Txt_Add_Com_Amount.NumberDecimalSeparator = ".";
            this.Txt_Add_Com_Amount.NumberGroupSeparator = ",";
            this.Txt_Add_Com_Amount.ReadOnly = true;
            this.Txt_Add_Com_Amount.Size = new System.Drawing.Size(197, 23);
            this.Txt_Add_Com_Amount.TabIndex = 8;
            this.Txt_Add_Com_Amount.Text = "0.000";
            this.Txt_Add_Com_Amount.TextChanged += new System.EventHandler(this.Txt_Add_Com_Amount_TextChanged);
            // 
            // Txt_Tot_amount2
            // 
            this.Txt_Tot_amount2.BackColor = System.Drawing.Color.White;
            this.Txt_Tot_amount2.Enabled = false;
            this.Txt_Tot_amount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Tot_amount2.Location = new System.Drawing.Point(13, 368);
            this.Txt_Tot_amount2.Name = "Txt_Tot_amount2";
            this.Txt_Tot_amount2.NumberDecimalDigits = 3;
            this.Txt_Tot_amount2.NumberDecimalSeparator = ".";
            this.Txt_Tot_amount2.NumberGroupSeparator = ",";
            this.Txt_Tot_amount2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Tot_amount2.Size = new System.Drawing.Size(134, 23);
            this.Txt_Tot_amount2.TabIndex = 1005;
            this.Txt_Tot_amount2.Text = "0.000";
            // 
            // txt_minrate_comm2
            // 
            this.txt_minrate_comm2.BackColor = System.Drawing.Color.White;
            this.txt_minrate_comm2.Enabled = false;
            this.txt_minrate_comm2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_minrate_comm2.Location = new System.Drawing.Point(13, 343);
            this.txt_minrate_comm2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_minrate_comm2.Name = "txt_minrate_comm2";
            this.txt_minrate_comm2.NumberDecimalDigits = 7;
            this.txt_minrate_comm2.NumberDecimalSeparator = ".";
            this.txt_minrate_comm2.NumberGroupSeparator = ",";
            this.txt_minrate_comm2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_minrate_comm2.Size = new System.Drawing.Size(134, 23);
            this.txt_minrate_comm2.TabIndex = 1000;
            this.txt_minrate_comm2.Text = "0.0000000";
            // 
            // txt_maxrate_comm2
            // 
            this.txt_maxrate_comm2.BackColor = System.Drawing.Color.White;
            this.txt_maxrate_comm2.Enabled = false;
            this.txt_maxrate_comm2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maxrate_comm2.Location = new System.Drawing.Point(148, 343);
            this.txt_maxrate_comm2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_maxrate_comm2.Name = "txt_maxrate_comm2";
            this.txt_maxrate_comm2.NumberDecimalDigits = 7;
            this.txt_maxrate_comm2.NumberDecimalSeparator = ".";
            this.txt_maxrate_comm2.NumberGroupSeparator = ",";
            this.txt_maxrate_comm2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_maxrate_comm2.Size = new System.Drawing.Size(117, 23);
            this.txt_maxrate_comm2.TabIndex = 999;
            this.txt_maxrate_comm2.Text = "0.0000000";
            // 
            // Txt_ExRate_comm2
            // 
            this.Txt_ExRate_comm2.BackColor = System.Drawing.Color.White;
            this.Txt_ExRate_comm2.Enabled = false;
            this.Txt_ExRate_comm2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ExRate_comm2.Location = new System.Drawing.Point(267, 343);
            this.Txt_ExRate_comm2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_ExRate_comm2.Name = "Txt_ExRate_comm2";
            this.Txt_ExRate_comm2.NumberDecimalDigits = 7;
            this.Txt_ExRate_comm2.NumberDecimalSeparator = ".";
            this.Txt_ExRate_comm2.NumberGroupSeparator = ",";
            this.Txt_ExRate_comm2.ReadOnly = true;
            this.Txt_ExRate_comm2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_ExRate_comm2.Size = new System.Drawing.Size(117, 23);
            this.Txt_ExRate_comm2.TabIndex = 998;
            this.Txt_ExRate_comm2.Text = "0.0000000";
            // 
            // Txt_Com_Amnt2
            // 
            this.Txt_Com_Amnt2.BackColor = System.Drawing.Color.White;
            this.Txt_Com_Amnt2.Enabled = false;
            this.Txt_Com_Amnt2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Com_Amnt2.Location = new System.Drawing.Point(413, 343);
            this.Txt_Com_Amnt2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Com_Amnt2.Name = "Txt_Com_Amnt2";
            this.Txt_Com_Amnt2.NumberDecimalDigits = 3;
            this.Txt_Com_Amnt2.NumberDecimalSeparator = ".";
            this.Txt_Com_Amnt2.NumberGroupSeparator = ",";
            this.Txt_Com_Amnt2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Com_Amnt2.Size = new System.Drawing.Size(139, 23);
            this.Txt_Com_Amnt2.TabIndex = 996;
            this.Txt_Com_Amnt2.Text = "0.000";
            // 
            // txt_locamount_comm2
            // 
            this.txt_locamount_comm2.BackColor = System.Drawing.Color.White;
            this.txt_locamount_comm2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_locamount_comm2.Location = new System.Drawing.Point(576, 343);
            this.txt_locamount_comm2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_locamount_comm2.Name = "txt_locamount_comm2";
            this.txt_locamount_comm2.NumberDecimalDigits = 3;
            this.txt_locamount_comm2.NumberDecimalSeparator = ".";
            this.txt_locamount_comm2.NumberGroupSeparator = ",";
            this.txt_locamount_comm2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_locamount_comm2.Size = new System.Drawing.Size(132, 23);
            this.txt_locamount_comm2.TabIndex = 994;
            this.txt_locamount_comm2.Text = "0.000";
            this.txt_locamount_comm2.TextChanged += new System.EventHandler(this.txt_locamount_comm2_TextChanged);
            // 
            // txt_minrate_rem2
            // 
            this.txt_minrate_rem2.BackColor = System.Drawing.Color.White;
            this.txt_minrate_rem2.Enabled = false;
            this.txt_minrate_rem2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_minrate_rem2.Location = new System.Drawing.Point(13, 299);
            this.txt_minrate_rem2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_minrate_rem2.Name = "txt_minrate_rem2";
            this.txt_minrate_rem2.NumberDecimalDigits = 7;
            this.txt_minrate_rem2.NumberDecimalSeparator = ".";
            this.txt_minrate_rem2.NumberGroupSeparator = ",";
            this.txt_minrate_rem2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_minrate_rem2.Size = new System.Drawing.Size(134, 23);
            this.txt_minrate_rem2.TabIndex = 990;
            this.txt_minrate_rem2.Text = "0.0000000";
            // 
            // txt_maxrate_rem2
            // 
            this.txt_maxrate_rem2.BackColor = System.Drawing.Color.White;
            this.txt_maxrate_rem2.Enabled = false;
            this.txt_maxrate_rem2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maxrate_rem2.Location = new System.Drawing.Point(148, 299);
            this.txt_maxrate_rem2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_maxrate_rem2.Name = "txt_maxrate_rem2";
            this.txt_maxrate_rem2.NumberDecimalDigits = 7;
            this.txt_maxrate_rem2.NumberDecimalSeparator = ".";
            this.txt_maxrate_rem2.NumberGroupSeparator = ",";
            this.txt_maxrate_rem2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_maxrate_rem2.Size = new System.Drawing.Size(117, 23);
            this.txt_maxrate_rem2.TabIndex = 989;
            this.txt_maxrate_rem2.Text = "0.0000000";
            // 
            // Txt_ExRate_Rem2
            // 
            this.Txt_ExRate_Rem2.BackColor = System.Drawing.Color.White;
            this.Txt_ExRate_Rem2.Enabled = false;
            this.Txt_ExRate_Rem2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ExRate_Rem2.Location = new System.Drawing.Point(267, 299);
            this.Txt_ExRate_Rem2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_ExRate_Rem2.Name = "Txt_ExRate_Rem2";
            this.Txt_ExRate_Rem2.NumberDecimalDigits = 7;
            this.Txt_ExRate_Rem2.NumberDecimalSeparator = ".";
            this.Txt_ExRate_Rem2.NumberGroupSeparator = ",";
            this.Txt_ExRate_Rem2.ReadOnly = true;
            this.Txt_ExRate_Rem2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_ExRate_Rem2.Size = new System.Drawing.Size(117, 23);
            this.Txt_ExRate_Rem2.TabIndex = 988;
            this.Txt_ExRate_Rem2.Text = "0.0000000";
            // 
            // Txt_Rem_Amount2
            // 
            this.Txt_Rem_Amount2.BackColor = System.Drawing.Color.White;
            this.Txt_Rem_Amount2.Enabled = false;
            this.Txt_Rem_Amount2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_Amount2.Location = new System.Drawing.Point(413, 299);
            this.Txt_Rem_Amount2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Rem_Amount2.Name = "Txt_Rem_Amount2";
            this.Txt_Rem_Amount2.NumberDecimalDigits = 3;
            this.Txt_Rem_Amount2.NumberDecimalSeparator = ".";
            this.Txt_Rem_Amount2.NumberGroupSeparator = ",";
            this.Txt_Rem_Amount2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Rem_Amount2.Size = new System.Drawing.Size(139, 23);
            this.Txt_Rem_Amount2.TabIndex = 986;
            this.Txt_Rem_Amount2.Text = "0.000";
            // 
            // txt_locamount_rem2
            // 
            this.txt_locamount_rem2.BackColor = System.Drawing.Color.White;
            this.txt_locamount_rem2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_locamount_rem2.Location = new System.Drawing.Point(576, 299);
            this.txt_locamount_rem2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_locamount_rem2.Name = "txt_locamount_rem2";
            this.txt_locamount_rem2.NumberDecimalDigits = 3;
            this.txt_locamount_rem2.NumberDecimalSeparator = ".";
            this.txt_locamount_rem2.NumberGroupSeparator = ",";
            this.txt_locamount_rem2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_locamount_rem2.Size = new System.Drawing.Size(132, 23);
            this.txt_locamount_rem2.TabIndex = 983;
            this.txt_locamount_rem2.Text = "0.000";
            this.txt_locamount_rem2.TextChanged += new System.EventHandler(this.txt_locamount_rem2_TextChanged);
            // 
            // Txtr_amount
            // 
            this.Txtr_amount.BackColor = System.Drawing.Color.White;
            this.Txtr_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_amount.Location = new System.Drawing.Point(582, 64);
            this.Txtr_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtr_amount.Name = "Txtr_amount";
            this.Txtr_amount.NumberDecimalDigits = 3;
            this.Txtr_amount.NumberDecimalSeparator = ".";
            this.Txtr_amount.NumberGroupSeparator = ",";
            this.Txtr_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_amount.Size = new System.Drawing.Size(163, 23);
            this.Txtr_amount.TabIndex = 3;
            this.Txtr_amount.Text = "0.000";
            this.Txtr_amount.TextChanged += new System.EventHandler(this.Txtr_amount_TextChanged);
            // 
            // Txt_Tot_amount
            // 
            this.Txt_Tot_amount.BackColor = System.Drawing.Color.White;
            this.Txt_Tot_amount.Enabled = false;
            this.Txt_Tot_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Tot_amount.ForeColor = System.Drawing.Color.Maroon;
            this.Txt_Tot_amount.Location = new System.Drawing.Point(42, 492);
            this.Txt_Tot_amount.Name = "Txt_Tot_amount";
            this.Txt_Tot_amount.NumberDecimalDigits = 3;
            this.Txt_Tot_amount.NumberDecimalSeparator = ".";
            this.Txt_Tot_amount.NumberGroupSeparator = ",";
            this.Txt_Tot_amount.Size = new System.Drawing.Size(134, 23);
            this.Txt_Tot_amount.TabIndex = 23;
            this.Txt_Tot_amount.Text = "0.000";
            // 
            // txt_locamount_comm
            // 
            this.txt_locamount_comm.BackColor = System.Drawing.Color.White;
            this.txt_locamount_comm.Enabled = false;
            this.txt_locamount_comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_locamount_comm.Location = new System.Drawing.Point(44, 467);
            this.txt_locamount_comm.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_locamount_comm.Name = "txt_locamount_comm";
            this.txt_locamount_comm.NumberDecimalDigits = 3;
            this.txt_locamount_comm.NumberDecimalSeparator = ".";
            this.txt_locamount_comm.NumberGroupSeparator = ",";
            this.txt_locamount_comm.Size = new System.Drawing.Size(132, 23);
            this.txt_locamount_comm.TabIndex = 21;
            this.txt_locamount_comm.Text = "0.000";
            // 
            // txt_locamount_rem
            // 
            this.txt_locamount_rem.BackColor = System.Drawing.Color.White;
            this.txt_locamount_rem.Enabled = false;
            this.txt_locamount_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_locamount_rem.Location = new System.Drawing.Point(43, 423);
            this.txt_locamount_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_locamount_rem.Name = "txt_locamount_rem";
            this.txt_locamount_rem.NumberDecimalDigits = 3;
            this.txt_locamount_rem.NumberDecimalSeparator = ".";
            this.txt_locamount_rem.NumberGroupSeparator = ",";
            this.txt_locamount_rem.Size = new System.Drawing.Size(132, 23);
            this.txt_locamount_rem.TabIndex = 15;
            this.txt_locamount_rem.Text = "0.000";
            // 
            // txt_minrate_comm
            // 
            this.txt_minrate_comm.BackColor = System.Drawing.Color.White;
            this.txt_minrate_comm.Enabled = false;
            this.txt_minrate_comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_minrate_comm.Location = new System.Drawing.Point(176, 467);
            this.txt_minrate_comm.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_minrate_comm.Name = "txt_minrate_comm";
            this.txt_minrate_comm.NumberDecimalDigits = 7;
            this.txt_minrate_comm.NumberDecimalSeparator = ".";
            this.txt_minrate_comm.NumberGroupSeparator = ",";
            this.txt_minrate_comm.Size = new System.Drawing.Size(117, 23);
            this.txt_minrate_comm.TabIndex = 20;
            this.txt_minrate_comm.Text = "0.0000000";
            // 
            // txt_maxrate_comm
            // 
            this.txt_maxrate_comm.BackColor = System.Drawing.Color.White;
            this.txt_maxrate_comm.Enabled = false;
            this.txt_maxrate_comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maxrate_comm.Location = new System.Drawing.Point(296, 467);
            this.txt_maxrate_comm.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_maxrate_comm.Name = "txt_maxrate_comm";
            this.txt_maxrate_comm.NumberDecimalDigits = 7;
            this.txt_maxrate_comm.NumberDecimalSeparator = ".";
            this.txt_maxrate_comm.NumberGroupSeparator = ",";
            this.txt_maxrate_comm.Size = new System.Drawing.Size(117, 23);
            this.txt_maxrate_comm.TabIndex = 19;
            this.txt_maxrate_comm.Text = "0.0000000";
            // 
            // txt_minrate_rem
            // 
            this.txt_minrate_rem.BackColor = System.Drawing.Color.White;
            this.txt_minrate_rem.Enabled = false;
            this.txt_minrate_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_minrate_rem.Location = new System.Drawing.Point(176, 423);
            this.txt_minrate_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_minrate_rem.Name = "txt_minrate_rem";
            this.txt_minrate_rem.NumberDecimalDigits = 7;
            this.txt_minrate_rem.NumberDecimalSeparator = ".";
            this.txt_minrate_rem.NumberGroupSeparator = ",";
            this.txt_minrate_rem.Size = new System.Drawing.Size(117, 23);
            this.txt_minrate_rem.TabIndex = 14;
            this.txt_minrate_rem.Text = "0.0000000";
            // 
            // txt_maxrate_rem
            // 
            this.txt_maxrate_rem.BackColor = System.Drawing.Color.White;
            this.txt_maxrate_rem.Enabled = false;
            this.txt_maxrate_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maxrate_rem.Location = new System.Drawing.Point(295, 423);
            this.txt_maxrate_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_maxrate_rem.Name = "txt_maxrate_rem";
            this.txt_maxrate_rem.NumberDecimalDigits = 7;
            this.txt_maxrate_rem.NumberDecimalSeparator = ".";
            this.txt_maxrate_rem.NumberGroupSeparator = ",";
            this.txt_maxrate_rem.Size = new System.Drawing.Size(117, 23);
            this.txt_maxrate_rem.TabIndex = 13;
            this.txt_maxrate_rem.Text = "0.0000000";
            // 
            // Txt_ExRate_comm
            // 
            this.Txt_ExRate_comm.BackColor = System.Drawing.Color.White;
            this.Txt_ExRate_comm.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ExRate_comm.Location = new System.Drawing.Point(416, 467);
            this.Txt_ExRate_comm.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_ExRate_comm.Name = "Txt_ExRate_comm";
            this.Txt_ExRate_comm.NumberDecimalDigits = 7;
            this.Txt_ExRate_comm.NumberDecimalSeparator = ".";
            this.Txt_ExRate_comm.NumberGroupSeparator = ",";
            this.Txt_ExRate_comm.ReadOnly = true;
            this.Txt_ExRate_comm.Size = new System.Drawing.Size(117, 23);
            this.Txt_ExRate_comm.TabIndex = 18;
            this.Txt_ExRate_comm.Text = "0.0000000";
            this.Txt_ExRate_comm.TextChanged += new System.EventHandler(this.Txt_ExRate_comm_TextChanged);
            // 
            // Txt_Com_Amnt
            // 
            this.Txt_Com_Amnt.BackColor = System.Drawing.Color.White;
            this.Txt_Com_Amnt.Enabled = false;
            this.Txt_Com_Amnt.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Com_Amnt.Location = new System.Drawing.Point(673, 467);
            this.Txt_Com_Amnt.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Com_Amnt.Name = "Txt_Com_Amnt";
            this.Txt_Com_Amnt.NumberDecimalDigits = 3;
            this.Txt_Com_Amnt.NumberDecimalSeparator = ".";
            this.Txt_Com_Amnt.NumberGroupSeparator = ",";
            this.Txt_Com_Amnt.Size = new System.Drawing.Size(134, 23);
            this.Txt_Com_Amnt.TabIndex = 16;
            this.Txt_Com_Amnt.Text = "0.000";
            this.Txt_Com_Amnt.TextChanged += new System.EventHandler(this.Txt_Com_Amnt_TextChanged);
            // 
            // Txt_ExRate_Rem
            // 
            this.Txt_ExRate_Rem.BackColor = System.Drawing.Color.White;
            this.Txt_ExRate_Rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ExRate_Rem.Location = new System.Drawing.Point(414, 423);
            this.Txt_ExRate_Rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_ExRate_Rem.Name = "Txt_ExRate_Rem";
            this.Txt_ExRate_Rem.NumberDecimalDigits = 7;
            this.Txt_ExRate_Rem.NumberDecimalSeparator = ".";
            this.Txt_ExRate_Rem.NumberGroupSeparator = ",";
            this.Txt_ExRate_Rem.ReadOnly = true;
            this.Txt_ExRate_Rem.Size = new System.Drawing.Size(117, 23);
            this.Txt_ExRate_Rem.TabIndex = 12;
            this.Txt_ExRate_Rem.Text = "0.0000000";
            this.Txt_ExRate_Rem.TextChanged += new System.EventHandler(this.Txt_ExRate_Rem_TextChanged);
            // 
            // Txt_Rem_Amount
            // 
            this.Txt_Rem_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Rem_Amount.Enabled = false;
            this.Txt_Rem_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_Amount.Location = new System.Drawing.Point(673, 423);
            this.Txt_Rem_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Rem_Amount.Name = "Txt_Rem_Amount";
            this.Txt_Rem_Amount.NumberDecimalDigits = 3;
            this.Txt_Rem_Amount.NumberDecimalSeparator = ".";
            this.Txt_Rem_Amount.NumberGroupSeparator = ",";
            this.Txt_Rem_Amount.Size = new System.Drawing.Size(134, 23);
            this.Txt_Rem_Amount.TabIndex = 10;
            this.Txt_Rem_Amount.Text = "0.000";
            this.Txt_Rem_Amount.TextChanged += new System.EventHandler(this.Txt_Rem_Amount_TextChanged);
            // 
            // Txt_Com_Amount
            // 
            this.Txt_Com_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Com_Amount.Enabled = false;
            this.Txt_Com_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Com_Amount.Location = new System.Drawing.Point(320, 65);
            this.Txt_Com_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Com_Amount.Name = "Txt_Com_Amount";
            this.Txt_Com_Amount.NumberDecimalDigits = 3;
            this.Txt_Com_Amount.NumberDecimalSeparator = ".";
            this.Txt_Com_Amount.NumberGroupSeparator = ",";
            this.Txt_Com_Amount.ReadOnly = true;
            this.Txt_Com_Amount.Size = new System.Drawing.Size(197, 23);
            this.Txt_Com_Amount.TabIndex = 7;
            this.Txt_Com_Amount.Text = "0.000";
            this.Txt_Com_Amount.TextChanged += new System.EventHandler(this.Txt_Com_Amount_TextChanged);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(0, 56);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(842, 1);
            this.flowLayoutPanel9.TabIndex = 619;
            // 
            // Cbo_city
            // 
            this.Cbo_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_city.FormattingEnabled = true;
            this.Cbo_city.Location = new System.Drawing.Point(91, 29);
            this.Cbo_city.Name = "Cbo_city";
            this.Cbo_city.Size = new System.Drawing.Size(179, 24);
            this.Cbo_city.TabIndex = 0;
            this.Cbo_city.SelectedIndexChanged += new System.EventHandler(this.Cbo_city_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(20, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 16);
            this.label12.TabIndex = 616;
            this.label12.Text = "البلد والمدينـة:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(607, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 613;
            this.label4.Text = "التاريــــــخ:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(368, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(224, 23);
            this.TxtUser.TabIndex = 612;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(288, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 611;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(6, 5);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(264, 23);
            this.TxtTerm_Name.TabIndex = 610;
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(672, 30);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(163, 23);
            this.TxtBox_User.TabIndex = 620;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(602, 34);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(71, 14);
            this.label2.TabIndex = 621;
            this.label2.Text = "الصـــندوق:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(368, 30);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.ReadOnly = true;
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(224, 23);
            this.Txt_Loc_Cur.TabIndex = 866;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(278, 35);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(94, 14);
            this.label46.TabIndex = 865;
            this.label46.Text = "العملة المحلية:";
            // 
            // Txt_MobilePhone
            // 
            this.Txt_MobilePhone.BackColor = System.Drawing.Color.White;
            this.Txt_MobilePhone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_MobilePhone.Location = new System.Drawing.Point(539, 57);
            this.Txt_MobilePhone.Name = "Txt_MobilePhone";
            this.Txt_MobilePhone.Size = new System.Drawing.Size(225, 23);
            this.Txt_MobilePhone.TabIndex = 883;
            this.Txt_MobilePhone.TextChanged += new System.EventHandler(this.Txt_MobilePhone_TextChanged);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Navy;
            this.label90.Location = new System.Drawing.Point(387, 61);
            this.label90.Name = "label90";
            this.label90.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label90.Size = new System.Drawing.Size(150, 14);
            this.label90.TabIndex = 882;
            this.label90.Text = "رقم حوالة تطبيق الهاتف:";
            // 
            // Btn_Phone
            // 
            this.Btn_Phone.Location = new System.Drawing.Point(764, 57);
            this.Btn_Phone.Name = "Btn_Phone";
            this.Btn_Phone.Size = new System.Drawing.Size(75, 25);
            this.Btn_Phone.TabIndex = 881;
            this.Btn_Phone.Text = "بحــــث";
            this.Btn_Phone.UseVisualStyleBackColor = true;
            this.Btn_Phone.Click += new System.EventHandler(this.Btn_Phone_Click);
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(673, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(162, 22);
            this.TxtIn_Rec_Date.TabIndex = 864;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // CHK_CITY
            // 
            this.CHK_CITY.AutoSize = true;
            this.CHK_CITY.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.CHK_CITY.ForeColor = System.Drawing.Color.Maroon;
            this.CHK_CITY.Location = new System.Drawing.Point(3, 34);
            this.CHK_CITY.Name = "CHK_CITY";
            this.CHK_CITY.Size = new System.Drawing.Size(15, 14);
            this.CHK_CITY.TabIndex = 885;
            this.CHK_CITY.UseVisualStyleBackColor = true;
            this.CHK_CITY.CheckedChanged += new System.EventHandler(this.CHK_CITY_CheckedChanged);
            // 
            // ADD_REM_CASH_DAILY
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 642);
            this.ControlBox = false;
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CHK_CITY);
            this.Controls.Add(this.Txt_MobilePhone);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.Btn_Phone);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Cbo_city);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ADD_REM_CASH_DAILY";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADD_REM_CASH_DAILY";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Add_rem_FormClosed);
            this.Load += new System.EventHandler(this.Add_rem_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_CustSen_Name)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_CustRec_Name)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_procer)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox Cmb_Case_Purpose;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_S_Doc_No;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Cmb_S_Doc_Type;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox Cmb_Code_phone_S;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Cmb_S_City;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox Cmb_R_Nat;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox Cmb_R_City;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox Cmb_T_City;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ComboBox cmb_cur;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Cbo_city;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Notes;
        private System.Windows.Forms.TextBox Txt_T_Purpose;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.TextBox txt_Mother_name;
        private System.Windows.Forms.TextBox Txt_mail;
        private System.Windows.Forms.TextBox Txt_S_Birth_Place;
        private System.Windows.Forms.TextBox Txt_Doc_S_Issue;
        private System.Windows.Forms.TextBox Txt_S_Phone;
        private System.Windows.Forms.TextBox TxtS_State;
        private System.Windows.Forms.TextBox TxtS_Post_Code;
        private System.Windows.Forms.TextBox Txts_street;
        private System.Windows.Forms.TextBox txtS_Suburb;
        private System.Windows.Forms.TextBox Txt_Sender;
        private System.Windows.Forms.TextBox Txtr_State;
        private System.Windows.Forms.TextBox Txtr_Post_Code;
        private System.Windows.Forms.TextBox Txtr_Street;
        private System.Windows.Forms.TextBox Txtr_Suburb;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_Reciever;
        private System.Windows.Forms.ComboBox Cmb_phone_Code_R;
        private System.Windows.Forms.DataGridView Grd_CustSen_Name;
        private System.Windows.Forms.DataGridView Grd_CustRec_Name;
        private System.Windows.Forms.ComboBox cmb_s_nat;
        private System.Windows.Forms.ComboBox cmb_Gender_id;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Sample.DecimalTextBox txt_locamount_comm;
        private System.Windows.Forms.Sample.DecimalTextBox txt_locamount_rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_minrate_comm;
        private System.Windows.Forms.Sample.DecimalTextBox txt_maxrate_comm;
        private System.Windows.Forms.Sample.DecimalTextBox txt_minrate_rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_maxrate_rem;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_ExRate_comm;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Com_Amnt;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_ExRate_Rem;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Rem_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Com_Amount;
        private System.Windows.Forms.ComboBox cmb_cur_comm;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox Cmb_Comm_Cur;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.ComboBox cmb_comm_type;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Tot_amount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox Cmb_R_CUR_ID;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Sample.DecimalTextBox Txtr_amount;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DateTimePicker Txt_Doc_S_Date;
        private System.Windows.Forms.DateTimePicker Txt_Sbirth_Date;
        private System.Windows.Forms.DateTimePicker Txt_Doc_S_Exp;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox Cbo_rem_path;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox chk_comm_info;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txt_com_curr;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Sample.DecimalTextBox txt_commamount;
        private System.Windows.Forms.TextBox txt_com_type;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox Cmb_PR_Cur_Id;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.DataGridView Grd_procer;
        private System.Windows.Forms.TextBox Txt_procer;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox Txt_Accproc;
        private System.Windows.Forms.ComboBox Cbo_Oper;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Button ENAR_BTN;
        private System.Windows.Forms.Button ar_sen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button en_rec_btn;
        private System.Windows.Forms.Button ar_rec_btn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.TextBox Tex_Social_ID;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox resd_cmb;
        private System.Windows.Forms.ComboBox cmb_job_sender;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Tot_amount2;
        private System.Windows.Forms.Label Lab_for_Com;
        private System.Windows.Forms.Label Lab_for_Rem;
        private System.Windows.Forms.Label lab_Loc_comm;
        private System.Windows.Forms.Sample.DecimalTextBox txt_minrate_comm2;
        private System.Windows.Forms.Sample.DecimalTextBox txt_maxrate_comm2;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_ExRate_comm2;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Com_Amnt2;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Sample.DecimalTextBox txt_locamount_comm2;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Sample.DecimalTextBox txt_minrate_rem2;
        private System.Windows.Forms.Sample.DecimalTextBox txt_maxrate_rem2;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_ExRate_Rem2;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Rem_Amount2;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label Lab_Loc_Rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_locamount_rem2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.TextBox Txt_S_details_job;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox Txt_MobilePhone;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Button Btn_Phone;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Add_Com_Amount;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Label lbl_comm;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Sample.DecimalTextBox TXt_Total_Comm;
        private System.Windows.Forms.TextBox Txt_another_Rec;
        private System.Windows.Forms.TextBox Txt_another_Sen;
        private System.Windows.Forms.CheckBox Chk_Term;
        private System.Windows.Forms.CheckBox Chk_Term1;
        private System.Windows.Forms.CheckBox CHK_CITY;

    }
}