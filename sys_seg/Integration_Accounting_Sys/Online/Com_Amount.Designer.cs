﻿namespace Integration_Accounting_Sys
{
    partial class Com_Amount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.Txt_com_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txtr_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(223, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 14);
            this.label6.TabIndex = 577;
            this.label6.Text = "العملـــــــة ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 14);
            this.label1.TabIndex = 578;
            this.label1.Text = "مبلــغ الحوالة :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 14);
            this.label2.TabIndex = 580;
            this.label2.Text = "مبلــغ العمولـة:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(223, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 14);
            this.label3.TabIndex = 579;
            this.label3.Text = "العملـــــــة ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(13, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 14);
            this.label4.TabIndex = 582;
            this.label4.Text = "عملـــــة الدفـع:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(111, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 14);
            this.label5.TabIndex = 581;
            this.label5.Text = "العملـــــــة ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(13, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 14);
            this.label7.TabIndex = 583;
            this.label7.Text = "نوعهــــــــــــــــا :";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(105, 112);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(181, 23);
            this.TxtTerm_Name.TabIndex = 611;
            // 
            // Txt_com_amount
            // 
            this.Txt_com_amount.BackColor = System.Drawing.Color.White;
            this.Txt_com_amount.Enabled = false;
            this.Txt_com_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_com_amount.Location = new System.Drawing.Point(105, 77);
            this.Txt_com_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_com_amount.Name = "Txt_com_amount";
            this.Txt_com_amount.NumberDecimalDigits = 3;
            this.Txt_com_amount.NumberDecimalSeparator = ".";
            this.Txt_com_amount.NumberGroupSeparator = ",";
            this.Txt_com_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_com_amount.Size = new System.Drawing.Size(114, 23);
            this.Txt_com_amount.TabIndex = 586;
            this.Txt_com_amount.Text = "0.000";
            // 
            // Txtr_amount
            // 
            this.Txtr_amount.BackColor = System.Drawing.Color.White;
            this.Txtr_amount.Enabled = false;
            this.Txtr_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_amount.Location = new System.Drawing.Point(105, 17);
            this.Txtr_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtr_amount.Name = "Txtr_amount";
            this.Txtr_amount.NumberDecimalDigits = 3;
            this.Txtr_amount.NumberDecimalSeparator = ".";
            this.Txtr_amount.NumberGroupSeparator = ",";
            this.Txtr_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_amount.Size = new System.Drawing.Size(114, 23);
            this.Txtr_amount.TabIndex = 584;
            this.Txtr_amount.Text = "0.000";
            // 
            // Com_Amount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 164);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.Txt_com_amount);
            this.Controls.Add(this.Txtr_amount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Com_Amount";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "العمـــــولات";
            this.Load += new System.EventHandler(this.Com_Amount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Sample.DecimalTextBox Txtr_amount;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_com_amount;
        private System.Windows.Forms.TextBox TxtTerm_Name;
    }
}