﻿namespace Integration_Accounting_Sys.remittences_online
{
    partial class Inquery_Remittances
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.Txt_s_job = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_S_doc_type = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtScity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_S_doc_issue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_S_Birth_Place = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_R_Birth_Place = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Txt_r_doc_issue = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_R_doc_no = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Txt_R_doc_type = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_address = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.Txt_r_job = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Print_All = new System.Windows.Forms.Button();
            this.Btn_Exist = new System.Windows.Forms.Button();
            this.Print_Details = new System.Windows.Forms.Button();
            this.Print_Rem_Cases = new System.Windows.Forms.Button();
            this.Grdrec_rem = new System.Windows.Forms.DataGridView();
            this.Grd_Rem_Cases = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column79 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column86 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column87 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column99 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column82 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column90 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column92 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column94 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Discreption = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.Cbo_Rcur = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.Txt_RCount = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_resd_sen = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_resd_rec = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txt_social_sen = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txt_social_rec = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txt_purpose_sen = new System.Windows.Forms.TextBox();
            this.txt_purpose_rec = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Btn_Rpt = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.Txt_S_details_job = new System.Windows.Forms.TextBox();
            this.Txt_R_Relation = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.Txt_R_details_job = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.Txt_R_notes = new System.Windows.Forms.TextBox();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Tot_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_r_birthdate = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_R_doc_date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_Birth = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_doc_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column80 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column83 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column84 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column85 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column98 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column81 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column89 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column91 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column93 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column95 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column96 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column97 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column100 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rem_Cases)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(307, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 906;
            this.label1.Text = "المستخـــــدم:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(682, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(71, 14);
            this.label4.TabIndex = 904;
            this.label4.Text = "التاريــــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(761, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(184, 23);
            this.TxtIn_Rec_Date.TabIndex = 905;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(394, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(250, 23);
            this.TxtUser.TabIndex = 903;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(15, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(265, 23);
            this.TxtTerm_Name.TabIndex = 902;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1, 29);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(969, 1);
            this.flowLayoutPanel9.TabIndex = 907;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-55, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(13, 32);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(179, 14);
            this.label2.TabIndex = 909;
            this.label2.Text = "معلومــــات الحوالـــة الانـــي....";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(9, 212);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(165, 14);
            this.label3.TabIndex = 910;
            this.label3.Text = "حالات الحوالــة الارشيفية ...";
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.Color.White;
            this.TxtS_address.Location = new System.Drawing.Point(75, 406);
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(560, 20);
            this.TxtS_address.TabIndex = 917;
            // 
            // Txt_s_job
            // 
            this.Txt_s_job.BackColor = System.Drawing.Color.White;
            this.Txt_s_job.Location = new System.Drawing.Point(455, 458);
            this.Txt_s_job.Name = "Txt_s_job";
            this.Txt_s_job.ReadOnly = true;
            this.Txt_s_job.Size = new System.Drawing.Size(190, 20);
            this.Txt_s_job.TabIndex = 916;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.Color.White;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(75, 458);
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(301, 20);
            this.Txt_Soruce_money.TabIndex = 915;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(2, 408);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(72, 15);
            this.label23.TabIndex = 924;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(379, 460);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(68, 15);
            this.label22.TabIndex = 923;
            this.label22.Text = "مهنة المرسـل:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(-3, 460);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(77, 15);
            this.label21.TabIndex = 922;
            this.label21.Text = "مصــدر المــــال:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(491, 355);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(73, 15);
            this.label20.TabIndex = 921;
            this.label20.Text = "التولـــــــــــــد:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(445, 434);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(44, 15);
            this.label18.TabIndex = 919;
            this.label18.Text = "تاريخها:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(714, 355);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 15);
            this.label19.TabIndex = 918;
            this.label19.Text = "هاتف المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.Color.White;
            this.TxtS_phone.Location = new System.Drawing.Point(795, 353);
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(168, 20);
            this.TxtS_phone.TabIndex = 925;
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.Color.White;
            this.TxtS_name.Location = new System.Drawing.Point(75, 353);
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(407, 20);
            this.TxtS_name.TabIndex = 926;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(2, 355);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(68, 15);
            this.label5.TabIndex = 927;
            this.label5.Text = "اسم المرســل:";
            // 
            // Txt_S_doc_type
            // 
            this.Txt_S_doc_type.BackColor = System.Drawing.Color.White;
            this.Txt_S_doc_type.Location = new System.Drawing.Point(75, 432);
            this.Txt_S_doc_type.Name = "Txt_S_doc_type";
            this.Txt_S_doc_type.ReadOnly = true;
            this.Txt_S_doc_type.Size = new System.Drawing.Size(208, 20);
            this.Txt_S_doc_type.TabIndex = 929;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(2, 434);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 15);
            this.label6.TabIndex = 928;
            this.label6.Text = "نوع الوثيقـــة:";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.Color.White;
            this.Txts_nat.Location = new System.Drawing.Point(795, 380);
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(168, 20);
            this.Txts_nat.TabIndex = 930;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(712, 382);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(74, 15);
            this.label7.TabIndex = 931;
            this.label7.Text = "جنسية المرسل:";
            // 
            // TxtScity
            // 
            this.TxtScity.BackColor = System.Drawing.Color.White;
            this.TxtScity.Location = new System.Drawing.Point(75, 380);
            this.TxtScity.Name = "TxtScity";
            this.TxtScity.ReadOnly = true;
            this.TxtScity.Size = new System.Drawing.Size(204, 20);
            this.TxtScity.TabIndex = 932;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(2, 382);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 933;
            this.label8.Text = "مدينة المرسـل:";
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.BackColor = System.Drawing.Color.White;
            this.Txt_S_doc_no.Location = new System.Drawing.Point(328, 432);
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.ReadOnly = true;
            this.Txt_S_doc_no.Size = new System.Drawing.Size(117, 20);
            this.Txt_S_doc_no.TabIndex = 934;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(283, 434);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(36, 15);
            this.label9.TabIndex = 935;
            this.label9.Text = "رقمها:";
            // 
            // Txt_S_doc_issue
            // 
            this.Txt_S_doc_issue.BackColor = System.Drawing.Color.White;
            this.Txt_S_doc_issue.Location = new System.Drawing.Point(633, 432);
            this.Txt_S_doc_issue.Name = "Txt_S_doc_issue";
            this.Txt_S_doc_issue.ReadOnly = true;
            this.Txt_S_doc_issue.Size = new System.Drawing.Size(168, 20);
            this.Txt_S_doc_issue.TabIndex = 936;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(583, 434);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(46, 15);
            this.label10.TabIndex = 937;
            this.label10.Text = "اصدارها:";
            // 
            // Txt_S_Birth_Place
            // 
            this.Txt_S_Birth_Place.BackColor = System.Drawing.Color.White;
            this.Txt_S_Birth_Place.Location = new System.Drawing.Point(569, 380);
            this.Txt_S_Birth_Place.Name = "Txt_S_Birth_Place";
            this.Txt_S_Birth_Place.ReadOnly = true;
            this.Txt_S_Birth_Place.Size = new System.Drawing.Size(143, 20);
            this.Txt_S_Birth_Place.TabIndex = 938;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(483, 382);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 939;
            this.label11.Text = "بلد ومحل الولادة:";
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.Color.White;
            this.Txt_Relionship.Location = new System.Drawing.Point(768, 484);
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(197, 20);
            this.Txt_Relionship.TabIndex = 940;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(646, 486);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(110, 15);
            this.label12.TabIndex = 941;
            this.label12.Text = "علاقة المرسل بالمستلم:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(635, 374);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(0, 14);
            this.label13.TabIndex = 943;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(119, 341);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(852, 1);
            this.flowLayoutPanel17.TabIndex = 945;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(8, 330);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(116, 14);
            this.label14.TabIndex = 944;
            this.label14.Text = "معلومات المرسل...";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(4, 504);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(120, 14);
            this.label15.TabIndex = 976;
            this.label15.Text = "معلومات المستلم...";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.Color.White;
            this.Txt_Purpose.Location = new System.Drawing.Point(75, 484);
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(297, 20);
            this.Txt_Purpose.TabIndex = 972;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(-3, 486);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(76, 15);
            this.label17.TabIndex = 973;
            this.label17.Text = "غرض التحويــل:";
            // 
            // Txt_R_Birth_Place
            // 
            this.Txt_R_Birth_Place.BackColor = System.Drawing.Color.White;
            this.Txt_R_Birth_Place.Location = new System.Drawing.Point(569, 549);
            this.Txt_R_Birth_Place.Name = "Txt_R_Birth_Place";
            this.Txt_R_Birth_Place.ReadOnly = true;
            this.Txt_R_Birth_Place.Size = new System.Drawing.Size(143, 20);
            this.Txt_R_Birth_Place.TabIndex = 970;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(482, 551);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(82, 15);
            this.label24.TabIndex = 971;
            this.label24.Text = "بلد ومحل الولادة:";
            // 
            // Txt_r_doc_issue
            // 
            this.Txt_r_doc_issue.BackColor = System.Drawing.Color.White;
            this.Txt_r_doc_issue.Location = new System.Drawing.Point(633, 604);
            this.Txt_r_doc_issue.Name = "Txt_r_doc_issue";
            this.Txt_r_doc_issue.ReadOnly = true;
            this.Txt_r_doc_issue.Size = new System.Drawing.Size(168, 20);
            this.Txt_r_doc_issue.TabIndex = 968;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(581, 607);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(46, 15);
            this.label25.TabIndex = 969;
            this.label25.Text = "اصدارها:";
            // 
            // Txt_R_doc_no
            // 
            this.Txt_R_doc_no.BackColor = System.Drawing.Color.White;
            this.Txt_R_doc_no.Location = new System.Drawing.Point(323, 604);
            this.Txt_R_doc_no.Name = "Txt_R_doc_no";
            this.Txt_R_doc_no.ReadOnly = true;
            this.Txt_R_doc_no.Size = new System.Drawing.Size(117, 20);
            this.Txt_R_doc_no.TabIndex = 966;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(283, 606);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(36, 15);
            this.label26.TabIndex = 967;
            this.label26.Text = "رقمها:";
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.Color.White;
            this.Txt_R_City.Location = new System.Drawing.Point(79, 549);
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(204, 20);
            this.Txt_R_City.TabIndex = 964;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(2, 551);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(77, 15);
            this.label27.TabIndex = 965;
            this.label27.Text = "مدينة المستلـــم:";
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.Color.White;
            this.Txt_R_Nat.Location = new System.Drawing.Point(798, 549);
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(168, 20);
            this.Txt_R_Nat.TabIndex = 962;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(723, 551);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(74, 15);
            this.label28.TabIndex = 963;
            this.label28.Text = "جنسية المستلم:";
            // 
            // Txt_R_doc_type
            // 
            this.Txt_R_doc_type.BackColor = System.Drawing.Color.White;
            this.Txt_R_doc_type.Location = new System.Drawing.Point(78, 604);
            this.Txt_R_doc_type.Name = "Txt_R_doc_type";
            this.Txt_R_doc_type.ReadOnly = true;
            this.Txt_R_doc_type.Size = new System.Drawing.Size(208, 20);
            this.Txt_R_doc_type.TabIndex = 961;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(2, 606);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 15);
            this.label29.TabIndex = 960;
            this.label29.Text = "نوع الوثيقـــة:";
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.Color.White;
            this.Txt_R_Name.Location = new System.Drawing.Point(79, 522);
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(407, 20);
            this.Txt_R_Name.TabIndex = 958;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(2, 524);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(77, 15);
            this.label30.TabIndex = 959;
            this.label30.Text = "اسم المستـــلــم:";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.Color.White;
            this.Txt_R_Phone.Location = new System.Drawing.Point(798, 522);
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(168, 20);
            this.Txt_R_Phone.TabIndex = 957;
            // 
            // Txt_R_address
            // 
            this.Txt_R_address.BackColor = System.Drawing.Color.White;
            this.Txt_R_address.Location = new System.Drawing.Point(79, 577);
            this.Txt_R_address.Name = "Txt_R_address";
            this.Txt_R_address.ReadOnly = true;
            this.Txt_R_address.Size = new System.Drawing.Size(401, 20);
            this.Txt_R_address.TabIndex = 949;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(2, 579);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(72, 15);
            this.label31.TabIndex = 956;
            this.label31.Text = "عنوان المستلم:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(517, 524);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(52, 15);
            this.label35.TabIndex = 953;
            this.label35.Text = "التولــــــد:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(442, 607);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(44, 15);
            this.label37.TabIndex = 951;
            this.label37.Text = "تاريخها:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(726, 524);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(69, 15);
            this.label38.TabIndex = 950;
            this.label38.Text = "هاتف المستلم:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(480, 579);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(80, 15);
            this.label32.TabIndex = 955;
            this.label32.Text = "مهنة المستلـــــم:";
            // 
            // Txt_r_job
            // 
            this.Txt_r_job.BackColor = System.Drawing.Color.White;
            this.Txt_r_job.Location = new System.Drawing.Point(564, 577);
            this.Txt_r_job.Name = "Txt_r_job";
            this.Txt_r_job.ReadOnly = true;
            this.Txt_r_job.Size = new System.Drawing.Size(190, 20);
            this.Txt_r_job.TabIndex = 948;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(121, 515);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(854, 1);
            this.flowLayoutPanel2.TabIndex = 977;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(176, 223);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(788, 1);
            this.flowLayoutPanel5.TabIndex = 993;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(193, 40);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(777, 1);
            this.flowLayoutPanel6.TabIndex = 994;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 655);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(980, 1);
            this.flowLayoutPanel4.TabIndex = 986;
            // 
            // Print_All
            // 
            this.Print_All.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Print_All.ForeColor = System.Drawing.Color.Navy;
            this.Print_All.Location = new System.Drawing.Point(174, 658);
            this.Print_All.Name = "Print_All";
            this.Print_All.Size = new System.Drawing.Size(125, 29);
            this.Print_All.TabIndex = 987;
            this.Print_All.Text = "تصدير اجمالي";
            this.Print_All.UseVisualStyleBackColor = true;
            this.Print_All.Click += new System.EventHandler(this.Print_All_Click);
            // 
            // Btn_Exist
            // 
            this.Btn_Exist.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Exist.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Exist.Location = new System.Drawing.Point(670, 658);
            this.Btn_Exist.Name = "Btn_Exist";
            this.Btn_Exist.Size = new System.Drawing.Size(125, 29);
            this.Btn_Exist.TabIndex = 989;
            this.Btn_Exist.Text = "انهـــــاء";
            this.Btn_Exist.UseVisualStyleBackColor = true;
            this.Btn_Exist.Click += new System.EventHandler(this.Btn_Exist_Click);
            // 
            // Print_Details
            // 
            this.Print_Details.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Print_Details.ForeColor = System.Drawing.Color.Navy;
            this.Print_Details.Location = new System.Drawing.Point(298, 658);
            this.Print_Details.Name = "Print_Details";
            this.Print_Details.Size = new System.Drawing.Size(125, 29);
            this.Print_Details.TabIndex = 990;
            this.Print_Details.Text = "تصدير تفصيلي";
            this.Print_Details.UseVisualStyleBackColor = true;
            this.Print_Details.Click += new System.EventHandler(this.Print_Details_Click);
            // 
            // Print_Rem_Cases
            // 
            this.Print_Rem_Cases.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Print_Rem_Cases.ForeColor = System.Drawing.Color.Navy;
            this.Print_Rem_Cases.Location = new System.Drawing.Point(422, 658);
            this.Print_Rem_Cases.Name = "Print_Rem_Cases";
            this.Print_Rem_Cases.Size = new System.Drawing.Size(125, 29);
            this.Print_Rem_Cases.TabIndex = 991;
            this.Print_Rem_Cases.Text = "طباعة حالة حوالة";
            this.Print_Rem_Cases.UseVisualStyleBackColor = true;
            this.Print_Rem_Cases.Click += new System.EventHandler(this.Print_Rem_Cases_Click);
            // 
            // Grdrec_rem
            // 
            this.Grdrec_rem.AllowUserToAddRows = false;
            this.Grdrec_rem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grdrec_rem.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grdrec_rem.ColumnHeadersHeight = 24;
            this.Grdrec_rem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column10,
            this.Column13,
            this.Column5,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column73,
            this.Column74,
            this.Column27,
            this.Column19,
            this.Column9,
            this.Column8,
            this.Column6,
            this.Column11,
            this.Column7,
            this.Column12,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column20,
            this.Column21,
            this.Column28,
            this.dataGridViewCheckBoxColumn4,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column29,
            this.Column72,
            this.Column57,
            this.Column54,
            this.Column55,
            this.Column30,
            this.Column31,
            this.Column70,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column35,
            this.Column40,
            this.dataGridViewTextBoxColumn13,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44,
            this.Column45,
            this.Column58,
            this.Column56,
            this.Column63,
            this.Column49,
            this.Column52,
            this.Column53,
            this.Column80,
            this.Column59,
            this.Column60,
            this.Column61,
            this.Column62,
            this.Column71,
            this.Column75,
            this.Column76,
            this.Column51,
            this.Column48,
            this.Column50,
            this.Column83,
            this.Column84,
            this.Column85,
            this.Column98,
            this.Column81,
            this.Column89,
            this.Column91,
            this.Column93,
            this.Column15,
            this.Column95,
            this.Column96,
            this.Column97,
            this.Column100});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdrec_rem.DefaultCellStyle = dataGridViewCellStyle8;
            this.Grdrec_rem.Location = new System.Drawing.Point(13, 50);
            this.Grdrec_rem.MultiSelect = false;
            this.Grdrec_rem.Name = "Grdrec_rem";
            this.Grdrec_rem.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.Grdrec_rem.RowHeadersVisible = false;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grdrec_rem.Size = new System.Drawing.Size(951, 138);
            this.Grdrec_rem.TabIndex = 712;
            this.Grdrec_rem.VirtualMode = true;
            this.Grdrec_rem.SelectionChanged += new System.EventHandler(this.Grdrec_rem_SelectionChanged);
            // 
            // Grd_Rem_Cases
            // 
            this.Grd_Rem_Cases.AllowUserToAddRows = false;
            this.Grd_Rem_Cases.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rem_Cases.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Rem_Cases.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rem_Cases.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Rem_Cases.ColumnHeadersHeight = 24;
            this.Grd_Rem_Cases.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column14,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.Column78,
            this.Column79,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.Column46,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn48,
            this.Column66,
            this.Column67,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.Column77,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.Column47,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn49,
            this.Column68,
            this.Column69,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.Column65,
            this.dataGridViewTextBoxColumn42,
            this.Column64,
            this.Column86,
            this.Column87,
            this.Column88,
            this.Column99,
            this.Column82,
            this.Column90,
            this.Column92,
            this.Column94});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Rem_Cases.DefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_Rem_Cases.Location = new System.Drawing.Point(12, 230);
            this.Grd_Rem_Cases.MultiSelect = false;
            this.Grd_Rem_Cases.Name = "Grd_Rem_Cases";
            this.Grd_Rem_Cases.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rem_Cases.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_Rem_Cases.RowHeadersVisible = false;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rem_Cases.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_Rem_Cases.Size = new System.Drawing.Size(952, 99);
            this.Grd_Rem_Cases.TabIndex = 713;
            this.Grd_Rem_Cases.VirtualMode = true;
            this.Grd_Rem_Cases.SelectionChanged += new System.EventHandler(this.Grd_Rem_Cases_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "rem_no";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحوالة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 84;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "Aname_rem_state";
            this.Column14.HeaderText = "حالة الحوالة الارشيفيـــة";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 143;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "R_amount";
            this.dataGridViewTextBoxColumn2.HeaderText = "مبلغ الحوالة";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 88;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn54.DataPropertyName = "rem_amount_cust";
            this.dataGridViewTextBoxColumn54.HeaderText = "المبلغ بعد التحويل للعملة المحلية";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Visible = false;
            this.dataGridViewTextBoxColumn54.Width = 180;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn55.DataPropertyName = "com_amount_cust";
            this.dataGridViewTextBoxColumn55.HeaderText = "العمولة بعد التحويل للعملة المحلية";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            this.dataGridViewTextBoxColumn55.Visible = false;
            this.dataGridViewTextBoxColumn55.Width = 185;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "R_ACUR_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "عملة الحوالة";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 88;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PR_ACUR_NAME";
            this.dataGridViewTextBoxColumn4.HeaderText = "عملة التسليم";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 88;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "S_ACITY_NAME";
            this.dataGridViewTextBoxColumn6.HeaderText = "مدينة الارسال";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 97;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "t_ACITY_NAME";
            this.dataGridViewTextBoxColumn5.HeaderText = "مدينة الاستلام";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 96;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ACASE_NA";
            this.dataGridViewTextBoxColumn7.HeaderText = "حالة الحوالة";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 88;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Case_Date";
            dataGridViewCellStyle13.Format = "G";
            dataGridViewCellStyle13.NullValue = "DD/MM/YYYY";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn8.HeaderText = "تاريخ الحالة";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 91;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "C_DATE";
            dataGridViewCellStyle14.Format = "G";
            dataGridViewCellStyle14.NullValue = "hh:mm:ssAM/PMDD/MM/YYYY";
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn9.HeaderText = "تاريخ ووقت الانشاء";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 127;
            // 
            // Column78
            // 
            this.Column78.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column78.DataPropertyName = "S_Acust_name";
            this.Column78.HeaderText = "الجهة المصدرة";
            this.Column78.Name = "Column78";
            this.Column78.ReadOnly = true;
            // 
            // Column79
            // 
            this.Column79.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column79.DataPropertyName = "D_Acust_name";
            this.Column79.HeaderText = "الجهة الدافعة";
            this.Column79.Name = "Column79";
            this.Column79.ReadOnly = true;
            this.Column79.Width = 92;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "S_name";
            this.dataGridViewTextBoxColumn11.HeaderText = "اسم المرسل";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 87;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "S_phone";
            this.dataGridViewTextBoxColumn12.HeaderText = "هاتف المرسل";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 97;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn14.DataPropertyName = "S_ACOUN_NAME";
            this.dataGridViewTextBoxColumn14.HeaderText = "بلد المرسل";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            this.dataGridViewTextBoxColumn14.Width = 82;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "S_address";
            this.dataGridViewTextBoxColumn15.HeaderText = "عنوان المرسل";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 97;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "S_Street";
            this.dataGridViewTextBoxColumn16.HeaderText = "الزقاق";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 64;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn17.DataPropertyName = "S_Suburb";
            this.dataGridViewTextBoxColumn17.HeaderText = "الحي";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Visible = false;
            this.dataGridViewTextBoxColumn17.Width = 57;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn18.DataPropertyName = "S_Post_Code";
            this.dataGridViewTextBoxColumn18.HeaderText = "الرقم البريدي";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            this.dataGridViewTextBoxColumn18.Width = 94;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn19.DataPropertyName = "S_State";
            this.dataGridViewTextBoxColumn19.HeaderText = "المحافظة";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Visible = false;
            this.dataGridViewTextBoxColumn19.Width = 76;
            // 
            // Column46
            // 
            this.Column46.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column46.DataPropertyName = "S_Social_No";
            this.Column46.HeaderText = "الرقم الوطني للمرسل";
            this.Column46.Name = "Column46";
            this.Column46.ReadOnly = true;
            this.Column46.Visible = false;
            this.Column46.Width = 129;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "sFRM_ADOC_NA";
            this.dataGridViewCheckBoxColumn2.HeaderText = "نوع وثيقة المرسل";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.ReadOnly = true;
            this.dataGridViewCheckBoxColumn2.Visible = false;
            this.dataGridViewCheckBoxColumn2.Width = 115;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn20.DataPropertyName = "S_doc_no";
            this.dataGridViewTextBoxColumn20.HeaderText = "رقم وثيقة المرسل";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Visible = false;
            this.dataGridViewTextBoxColumn20.Width = 114;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn21.DataPropertyName = "S_doc_ida";
            this.dataGridViewTextBoxColumn21.HeaderText = "تاريخ اصدار وثيقة المرسل";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Visible = false;
            this.dataGridViewTextBoxColumn21.Width = 155;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn22.DataPropertyName = "S_doc_eda";
            this.dataGridViewTextBoxColumn22.HeaderText = "تاريخ انتهاء وثيقة المرسل";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Visible = false;
            this.dataGridViewTextBoxColumn22.Width = 155;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn23.DataPropertyName = "S_doc_issue";
            this.dataGridViewTextBoxColumn23.HeaderText = "اصدار وثيقة للمرسل";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Visible = false;
            this.dataGridViewTextBoxColumn23.Width = 124;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn24.DataPropertyName = "s_Email";
            this.dataGridViewTextBoxColumn24.HeaderText = "ايميل المرسل";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Visible = false;
            this.dataGridViewTextBoxColumn24.Width = 94;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn25.DataPropertyName = "s_A_NAT_NAME";
            this.dataGridViewTextBoxColumn25.HeaderText = "جنسية المرسل";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Visible = false;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn57.DataPropertyName = "SBirth_Place";
            this.dataGridViewTextBoxColumn57.HeaderText = "مكان تولد المرسل";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            this.dataGridViewTextBoxColumn57.Visible = false;
            this.dataGridViewTextBoxColumn57.Width = 117;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn48.DataPropertyName = "sbirth_date";
            this.dataGridViewTextBoxColumn48.HeaderText = "تولد المرسل";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Visible = false;
            this.dataGridViewTextBoxColumn48.Width = 88;
            // 
            // Column66
            // 
            this.Column66.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column66.DataPropertyName = "s_job";
            this.Column66.HeaderText = "مهنة المرسل";
            this.Column66.Name = "Column66";
            this.Column66.ReadOnly = true;
            this.Column66.Visible = false;
            this.Column66.Width = 91;
            // 
            // Column67
            // 
            this.Column67.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column67.DataPropertyName = "s_details_job";
            this.Column67.HeaderText = "تفاصيل مهنة المرسل";
            this.Column67.Name = "Column67";
            this.Column67.ReadOnly = true;
            this.Column67.Visible = false;
            this.Column67.Width = 129;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn26.DataPropertyName = "s_notes";
            this.dataGridViewTextBoxColumn26.HeaderText = "ملاحظات المرسل";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            this.dataGridViewTextBoxColumn26.Width = 113;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn27.DataPropertyName = "r_name";
            this.dataGridViewTextBoxColumn27.HeaderText = "اسم المستلم";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 86;
            // 
            // Column77
            // 
            this.Column77.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column77.DataPropertyName = "Real_Paid_Name";
            this.Column77.HeaderText = "الاسم كما في الهوية";
            this.Column77.Name = "Column77";
            this.Column77.ReadOnly = true;
            this.Column77.Width = 128;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn28.DataPropertyName = "R_phone";
            this.dataGridViewTextBoxColumn28.HeaderText = "هاتف المستلم";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Visible = false;
            this.dataGridViewTextBoxColumn28.Width = 96;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn29.DataPropertyName = "r_ACITY_NAME";
            this.dataGridViewTextBoxColumn29.HeaderText = "مدينة المستلم";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Visible = false;
            this.dataGridViewTextBoxColumn29.Width = 93;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn30.DataPropertyName = "r_ACOUN_NAME";
            this.dataGridViewTextBoxColumn30.HeaderText = "بلد المستلم";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Visible = false;
            this.dataGridViewTextBoxColumn30.Width = 81;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn32.DataPropertyName = "r_address";
            this.dataGridViewTextBoxColumn32.HeaderText = "عنوان المستلم";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Visible = false;
            this.dataGridViewTextBoxColumn32.Width = 96;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn33.DataPropertyName = "r_Street";
            this.dataGridViewTextBoxColumn33.HeaderText = "الزقاق";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Visible = false;
            this.dataGridViewTextBoxColumn33.Width = 64;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn34.DataPropertyName = "r_Suburb";
            this.dataGridViewTextBoxColumn34.HeaderText = "الحي للمستلم";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Visible = false;
            this.dataGridViewTextBoxColumn34.Width = 93;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn35.DataPropertyName = "r_Post_Code";
            this.dataGridViewTextBoxColumn35.HeaderText = "الرمز البريدي للمستلم";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Visible = false;
            this.dataGridViewTextBoxColumn35.Width = 131;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn36.DataPropertyName = "r_State";
            this.dataGridViewTextBoxColumn36.HeaderText = "محافظة المستلم";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Visible = false;
            this.dataGridViewTextBoxColumn36.Width = 106;
            // 
            // Column47
            // 
            this.Column47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column47.DataPropertyName = "R_Social_No";
            this.Column47.HeaderText = "الرقم الوطني للمستلم";
            this.Column47.Name = "Column47";
            this.Column47.ReadOnly = true;
            this.Column47.Visible = false;
            this.Column47.Width = 128;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "rFRM_ADOC_NA";
            this.dataGridViewCheckBoxColumn1.HeaderText = "نوع وثيقة المستلم";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Visible = false;
            this.dataGridViewCheckBoxColumn1.Width = 114;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn37.DataPropertyName = "r_doc_no";
            this.dataGridViewTextBoxColumn37.HeaderText = "رقم وثيقة المستلم";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Visible = false;
            this.dataGridViewTextBoxColumn37.Width = 113;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn38.DataPropertyName = "r_doc_ida";
            this.dataGridViewTextBoxColumn38.HeaderText = "تاريخ اصدار المستلم";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Visible = false;
            this.dataGridViewTextBoxColumn38.Width = 127;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn39.DataPropertyName = "r_doc_eda";
            this.dataGridViewTextBoxColumn39.HeaderText = "تاريخ انتهاء الوثيقة";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Visible = false;
            this.dataGridViewTextBoxColumn39.Width = 124;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn40.DataPropertyName = "r_doc_issue";
            this.dataGridViewTextBoxColumn40.HeaderText = "اصدار وثيقة المستلم";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Visible = false;
            this.dataGridViewTextBoxColumn40.Width = 123;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn41.DataPropertyName = "r_Email";
            this.dataGridViewTextBoxColumn41.HeaderText = "ايميل المستلم";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Visible = false;
            this.dataGridViewTextBoxColumn41.Width = 93;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn49.DataPropertyName = "rbirth_date";
            this.dataGridViewTextBoxColumn49.HeaderText = "تولد المستلم";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Visible = false;
            this.dataGridViewTextBoxColumn49.Width = 87;
            // 
            // Column68
            // 
            this.Column68.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column68.DataPropertyName = "r_job";
            this.Column68.HeaderText = "مهنة المستلم";
            this.Column68.Name = "Column68";
            this.Column68.ReadOnly = true;
            this.Column68.Visible = false;
            this.Column68.Width = 90;
            // 
            // Column69
            // 
            this.Column69.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column69.DataPropertyName = "r_details_job";
            this.Column69.HeaderText = "تفاصيل مهنة المستلم";
            this.Column69.Name = "Column69";
            this.Column69.ReadOnly = true;
            this.Column69.Visible = false;
            this.Column69.Width = 128;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn43.DataPropertyName = "r_notes";
            this.dataGridViewTextBoxColumn43.HeaderText = "ملاحظات المستلم";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Visible = false;
            this.dataGridViewTextBoxColumn43.Width = 112;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn46.DataPropertyName = "vo_no";
            this.dataGridViewTextBoxColumn46.HeaderText = "رقم السند";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 77;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn47.DataPropertyName = "In_rec_date";
            this.dataGridViewTextBoxColumn47.HeaderText = "تاريخ السجلات";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            this.dataGridViewTextBoxColumn47.Visible = false;
            this.dataGridViewTextBoxColumn47.Width = 104;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn50.DataPropertyName = "s_ocomm";
            this.dataGridViewTextBoxColumn50.HeaderText = "عمولة المرسل";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            this.dataGridViewTextBoxColumn50.Visible = false;
            this.dataGridViewTextBoxColumn50.Width = 97;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn51.DataPropertyName = "s_ACUR_NAME";
            this.dataGridViewTextBoxColumn51.HeaderText = "عملة المرسل";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Visible = false;
            this.dataGridViewTextBoxColumn51.Width = 91;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn52.DataPropertyName = "c_ocomm";
            this.dataGridViewTextBoxColumn52.HeaderText = "مبلغ العمولة";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Visible = false;
            this.dataGridViewTextBoxColumn52.Width = 89;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn53.DataPropertyName = "c_ACUR_NAME";
            this.dataGridViewTextBoxColumn53.HeaderText = "عملة العمولة";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            this.dataGridViewTextBoxColumn53.Visible = false;
            this.dataGridViewTextBoxColumn53.Width = 89;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn56.DataPropertyName = "rate_online";
            this.dataGridViewTextBoxColumn56.HeaderText = "سعر التعادل";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            this.dataGridViewTextBoxColumn56.Visible = false;
            this.dataGridViewTextBoxColumn56.Width = 90;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn60.DataPropertyName = "Source_money";
            this.dataGridViewTextBoxColumn60.HeaderText = "مصدر المال";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Visible = false;
            this.dataGridViewTextBoxColumn60.Width = 87;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn61.DataPropertyName = "Relation_S_R";
            this.dataGridViewTextBoxColumn61.HeaderText = "علاقة المرسل بالمستلم";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.ReadOnly = true;
            this.dataGridViewTextBoxColumn61.Visible = false;
            this.dataGridViewTextBoxColumn61.Width = 138;
            // 
            // Column65
            // 
            this.Column65.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column65.DataPropertyName = "r_Relation";
            this.Column65.HeaderText = "علاقة المستلم بالمرسل";
            this.Column65.Name = "Column65";
            this.Column65.ReadOnly = true;
            this.Column65.Visible = false;
            this.Column65.Width = 138;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn42.DataPropertyName = "T_purpose";
            this.dataGridViewTextBoxColumn42.HeaderText = "غرض الارسال";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Visible = false;
            this.dataGridViewTextBoxColumn42.Width = 101;
            // 
            // Column64
            // 
            this.Column64.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column64.DataPropertyName = "R_T_purpose";
            this.Column64.HeaderText = "غرض المستلم";
            this.Column64.Name = "Column64";
            this.Column64.ReadOnly = true;
            this.Column64.Visible = false;
            this.Column64.Width = 97;
            // 
            // Column86
            // 
            this.Column86.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column86.DataPropertyName = "i_SOComm";
            this.Column86.HeaderText = "العمولة الواردة";
            this.Column86.Name = "Column86";
            this.Column86.ReadOnly = true;
            this.Column86.Visible = false;
            // 
            // Column87
            // 
            this.Column87.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column87.DataPropertyName = "i_SACUR_NAME";
            this.Column87.HeaderText = "عملة العمولة الواردة";
            this.Column87.Name = "Column87";
            this.Column87.ReadOnly = true;
            this.Column87.Visible = false;
            this.Column87.Width = 124;
            // 
            // Column88
            // 
            this.Column88.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column88.DataPropertyName = "i_SAType_name";
            this.Column88.HeaderText = "نوع العمولة الواردة";
            this.Column88.Name = "Column88";
            this.Column88.ReadOnly = true;
            this.Column88.Visible = false;
            this.Column88.Width = 121;
            // 
            // Column99
            // 
            this.Column99.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column99.DataPropertyName = "user_name";
            this.Column99.HeaderText = "المستخدم ";
            this.Column99.Name = "Column99";
            this.Column99.ReadOnly = true;
            this.Column99.Width = 81;
            // 
            // Column82
            // 
            this.Column82.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column82.DataPropertyName = "User_Confirm_Name";
            this.Column82.HeaderText = "مستخدم تأكيد الحوالة";
            this.Column82.Name = "Column82";
            this.Column82.ReadOnly = true;
            this.Column82.Width = 134;
            // 
            // Column90
            // 
            this.Column90.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column90.DataPropertyName = "User_INConfirm_Name";
            this.Column90.HeaderText = "مستخدم تأكيد الحوالة الواردة";
            this.Column90.Name = "Column90";
            this.Column90.ReadOnly = true;
            this.Column90.Width = 169;
            // 
            // Column92
            // 
            this.Column92.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column92.DataPropertyName = "rem_pay_aname";
            this.Column92.HeaderText = "طريقة الدفع";
            this.Column92.Name = "Column92";
            this.Column92.ReadOnly = true;
            this.Column92.Width = 88;
            // 
            // Column94
            // 
            this.Column94.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column94.DataPropertyName = "payment_info";
            this.Column94.HeaderText = "معلومات عن طريق الدفع";
            this.Column94.Name = "Column94";
            this.Column94.ReadOnly = true;
            this.Column94.Width = 147;
            // 
            // Txt_Discreption
            // 
            this.Txt_Discreption.Location = new System.Drawing.Point(732, 406);
            this.Txt_Discreption.MaxLength = 199;
            this.Txt_Discreption.Name = "Txt_Discreption";
            this.Txt_Discreption.ReadOnly = true;
            this.Txt_Discreption.Size = new System.Drawing.Size(234, 20);
            this.Txt_Discreption.TabIndex = 995;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(641, 408);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(82, 15);
            this.label33.TabIndex = 996;
            this.label33.Text = "التفاصيـــــــــــل:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(374, 195);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(90, 14);
            this.label39.TabIndex = 1124;
            this.label39.Text = "مبالغ الحوالات:";
            // 
            // Cbo_Rcur
            // 
            this.Cbo_Rcur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rcur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rcur.FormattingEnabled = true;
            this.Cbo_Rcur.Location = new System.Drawing.Point(108, 191);
            this.Cbo_Rcur.Name = "Cbo_Rcur";
            this.Cbo_Rcur.Size = new System.Drawing.Size(184, 22);
            this.Cbo_Rcur.TabIndex = 1123;
            this.Cbo_Rcur.SelectedIndexChanged += new System.EventHandler(this.Cbo_Rcur_SelectedIndexChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(18, 195);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(84, 14);
            this.label40.TabIndex = 1122;
            this.label40.Text = "عملة الحوالة:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(721, 195);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(84, 14);
            this.label41.TabIndex = 1121;
            this.label41.Text = "عدد الحوالات:";
            // 
            // Txt_RCount
            // 
            this.Txt_RCount.BackColor = System.Drawing.Color.White;
            this.Txt_RCount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_RCount.Location = new System.Drawing.Point(806, 191);
            this.Txt_RCount.Name = "Txt_RCount";
            this.Txt_RCount.ReadOnly = true;
            this.Txt_RCount.Size = new System.Drawing.Size(151, 22);
            this.Txt_RCount.TabIndex = 1120;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(876, 658);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 29);
            this.button1.TabIndex = 1126;
            this.button1.Text = "طباعة الارشيف";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_resd_sen
            // 
            this.txt_resd_sen.BackColor = System.Drawing.Color.White;
            this.txt_resd_sen.Location = new System.Drawing.Point(369, 380);
            this.txt_resd_sen.Name = "txt_resd_sen";
            this.txt_resd_sen.ReadOnly = true;
            this.txt_resd_sen.Size = new System.Drawing.Size(113, 20);
            this.txt_resd_sen.TabIndex = 1127;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(283, 382);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(74, 15);
            this.label16.TabIndex = 1128;
            this.label16.Text = "نوع الاقامـــــة:";
            // 
            // txt_resd_rec
            // 
            this.txt_resd_rec.BackColor = System.Drawing.Color.White;
            this.txt_resd_rec.Location = new System.Drawing.Point(367, 549);
            this.txt_resd_rec.Name = "txt_resd_rec";
            this.txt_resd_rec.ReadOnly = true;
            this.txt_resd_rec.Size = new System.Drawing.Size(113, 20);
            this.txt_resd_rec.TabIndex = 1129;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(283, 551);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(74, 15);
            this.label34.TabIndex = 1130;
            this.label34.Text = "نوع الاقامـــــة:";
            // 
            // txt_social_sen
            // 
            this.txt_social_sen.BackColor = System.Drawing.Color.White;
            this.txt_social_sen.Location = new System.Drawing.Point(861, 432);
            this.txt_social_sen.Name = "txt_social_sen";
            this.txt_social_sen.ReadOnly = true;
            this.txt_social_sen.Size = new System.Drawing.Size(103, 20);
            this.txt_social_sen.TabIndex = 1131;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(801, 434);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(59, 15);
            this.label36.TabIndex = 1132;
            this.label36.Text = "الرقم.وطني";
            // 
            // txt_social_rec
            // 
            this.txt_social_rec.BackColor = System.Drawing.Color.White;
            this.txt_social_rec.Location = new System.Drawing.Point(860, 604);
            this.txt_social_rec.Name = "txt_social_rec";
            this.txt_social_rec.ReadOnly = true;
            this.txt_social_rec.Size = new System.Drawing.Size(103, 20);
            this.txt_social_rec.TabIndex = 1133;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(800, 607);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(59, 15);
            this.label42.TabIndex = 1134;
            this.label42.Text = "الرقم.وطني";
            // 
            // txt_purpose_sen
            // 
            this.txt_purpose_sen.BackColor = System.Drawing.Color.White;
            this.txt_purpose_sen.Location = new System.Drawing.Point(401, 484);
            this.txt_purpose_sen.Name = "txt_purpose_sen";
            this.txt_purpose_sen.ReadOnly = true;
            this.txt_purpose_sen.Size = new System.Drawing.Size(243, 20);
            this.txt_purpose_sen.TabIndex = 1135;
            // 
            // txt_purpose_rec
            // 
            this.txt_purpose_rec.BackColor = System.Drawing.Color.White;
            this.txt_purpose_rec.Location = new System.Drawing.Point(74, 627);
            this.txt_purpose_rec.Name = "txt_purpose_rec";
            this.txt_purpose_rec.ReadOnly = true;
            this.txt_purpose_rec.Size = new System.Drawing.Size(297, 20);
            this.txt_purpose_rec.TabIndex = 1137;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(-1, 629);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(70, 15);
            this.label44.TabIndex = 1138;
            this.label44.Text = "غرض التحويل:";
            // 
            // Btn_Rpt
            // 
            this.Btn_Rpt.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Rpt.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Rpt.Location = new System.Drawing.Point(546, 658);
            this.Btn_Rpt.Name = "Btn_Rpt";
            this.Btn_Rpt.Size = new System.Drawing.Size(125, 29);
            this.Btn_Rpt.TabIndex = 1139;
            this.Btn_Rpt.Text = "طباعة اجمالي";
            this.Btn_Rpt.UseVisualStyleBackColor = true;
            this.Btn_Rpt.Click += new System.EventHandler(this.Btn_Rpt_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(379, 484);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(16, 20);
            this.label43.TabIndex = 1145;
            this.label43.Text = "/";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(648, 458);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(16, 20);
            this.label45.TabIndex = 1146;
            this.label45.Text = "/";
            // 
            // Txt_S_details_job
            // 
            this.Txt_S_details_job.BackColor = System.Drawing.Color.White;
            this.Txt_S_details_job.Location = new System.Drawing.Point(670, 458);
            this.Txt_S_details_job.MaxLength = 99;
            this.Txt_S_details_job.Name = "Txt_S_details_job";
            this.Txt_S_details_job.ReadOnly = true;
            this.Txt_S_details_job.Size = new System.Drawing.Size(296, 20);
            this.Txt_S_details_job.TabIndex = 1147;
            // 
            // Txt_R_Relation
            // 
            this.Txt_R_Relation.BackColor = System.Drawing.Color.White;
            this.Txt_R_Relation.Location = new System.Drawing.Point(758, 627);
            this.Txt_R_Relation.MaxLength = 200;
            this.Txt_R_Relation.Name = "Txt_R_Relation";
            this.Txt_R_Relation.ReadOnly = true;
            this.Txt_R_Relation.Size = new System.Drawing.Size(204, 20);
            this.Txt_R_Relation.TabIndex = 1148;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(637, 630);
            this.label58.Name = "label58";
            this.label58.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label58.Size = new System.Drawing.Size(120, 15);
            this.label58.TabIndex = 1149;
            this.label58.Text = "علاقة المستلم بالمرسل:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(755, 577);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(16, 20);
            this.label46.TabIndex = 1150;
            this.label46.Text = "/";
            // 
            // Txt_R_details_job
            // 
            this.Txt_R_details_job.BackColor = System.Drawing.Color.White;
            this.Txt_R_details_job.Location = new System.Drawing.Point(775, 577);
            this.Txt_R_details_job.MaxLength = 99;
            this.Txt_R_details_job.Name = "Txt_R_details_job";
            this.Txt_R_details_job.ReadOnly = true;
            this.Txt_R_details_job.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_details_job.Size = new System.Drawing.Size(190, 20);
            this.Txt_R_details_job.TabIndex = 1151;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(372, 630);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(64, 15);
            this.label47.TabIndex = 1153;
            this.label47.Text = "التفاصيـــــل:";
            // 
            // Txt_R_notes
            // 
            this.Txt_R_notes.BackColor = System.Drawing.Color.White;
            this.Txt_R_notes.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_notes.Location = new System.Drawing.Point(440, 626);
            this.Txt_R_notes.MaxLength = 199;
            this.Txt_R_notes.Name = "Txt_R_notes";
            this.Txt_R_notes.ReadOnly = true;
            this.Txt_R_notes.Size = new System.Drawing.Size(193, 23);
            this.Txt_R_notes.TabIndex = 1152;
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_OK.ForeColor = System.Drawing.Color.Navy;
            this.Btn_OK.Location = new System.Drawing.Point(64, 658);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(111, 28);
            this.Btn_OK.TabIndex = 1155;
            this.Btn_OK.Text = "تصدير شامل";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Visible = false;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Tot_Amount
            // 
            this.Tot_Amount.BackColor = System.Drawing.Color.White;
            this.Tot_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount.Location = new System.Drawing.Point(466, 191);
            this.Tot_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount.Name = "Tot_Amount";
            this.Tot_Amount.NumberDecimalDigits = 3;
            this.Tot_Amount.NumberDecimalSeparator = ".";
            this.Tot_Amount.NumberGroupSeparator = ",";
            this.Tot_Amount.ReadOnly = true;
            this.Tot_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Tot_Amount.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount.TabIndex = 1125;
            this.Tot_Amount.Text = "0.000";
            // 
            // Txt_r_birthdate
            // 
            this.Txt_r_birthdate.BackColor = System.Drawing.Color.White;
            this.Txt_r_birthdate.DateSeperator = '/';
            this.Txt_r_birthdate.Enabled = false;
            this.Txt_r_birthdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_birthdate.Location = new System.Drawing.Point(568, 521);
            this.Txt_r_birthdate.Mask = "0000/00/00";
            this.Txt_r_birthdate.Name = "Txt_r_birthdate";
            this.Txt_r_birthdate.PromptChar = ' ';
            this.Txt_r_birthdate.ReadOnly = true;
            this.Txt_r_birthdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_birthdate.Size = new System.Drawing.Size(143, 22);
            this.Txt_r_birthdate.TabIndex = 974;
            this.Txt_r_birthdate.Text = "00000000";
            this.Txt_r_birthdate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.Txt_r_birthdate.ValidatingType = typeof(System.DateTime);
            // 
            // Txt_R_doc_date
            // 
            this.Txt_R_doc_date.BackColor = System.Drawing.Color.White;
            this.Txt_R_doc_date.DateSeperator = '/';
            this.Txt_R_doc_date.Enabled = false;
            this.Txt_R_doc_date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_doc_date.Location = new System.Drawing.Point(487, 603);
            this.Txt_R_doc_date.Mask = "0000/00/00";
            this.Txt_R_doc_date.Name = "Txt_R_doc_date";
            this.Txt_R_doc_date.PromptChar = ' ';
            this.Txt_R_doc_date.ReadOnly = true;
            this.Txt_R_doc_date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_doc_date.Size = new System.Drawing.Size(94, 22);
            this.Txt_R_doc_date.TabIndex = 946;
            this.Txt_R_doc_date.Text = "00000000";
            this.Txt_R_doc_date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.Txt_R_doc_date.ValidatingType = typeof(System.DateTime);
            // 
            // Txt_S_Birth
            // 
            this.Txt_S_Birth.BackColor = System.Drawing.Color.White;
            this.Txt_S_Birth.DateSeperator = '/';
            this.Txt_S_Birth.Enabled = false;
            this.Txt_S_Birth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Birth.Location = new System.Drawing.Point(568, 352);
            this.Txt_S_Birth.Mask = "0000/00/00";
            this.Txt_S_Birth.Name = "Txt_S_Birth";
            this.Txt_S_Birth.PromptChar = ' ';
            this.Txt_S_Birth.ReadOnly = true;
            this.Txt_S_Birth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_Birth.Size = new System.Drawing.Size(143, 22);
            this.Txt_S_Birth.TabIndex = 942;
            this.Txt_S_Birth.Text = "00000000";
            this.Txt_S_Birth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.Txt_S_Birth.ValidatingType = typeof(System.DateTime);
            // 
            // Txt_S_doc_Date
            // 
            this.Txt_S_doc_Date.BackColor = System.Drawing.Color.White;
            this.Txt_S_doc_Date.DateSeperator = '/';
            this.Txt_S_doc_Date.Enabled = false;
            this.Txt_S_doc_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_doc_Date.Location = new System.Drawing.Point(490, 431);
            this.Txt_S_doc_Date.Mask = "0000/00/00";
            this.Txt_S_doc_Date.Name = "Txt_S_doc_Date";
            this.Txt_S_doc_Date.PromptChar = ' ';
            this.Txt_S_doc_Date.ReadOnly = true;
            this.Txt_S_doc_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_doc_Date.Size = new System.Drawing.Size(90, 22);
            this.Txt_S_doc_Date.TabIndex = 913;
            this.Txt_S_doc_Date.Text = "00000000";
            this.Txt_S_doc_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحوالة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 84;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "a_local_international";
            this.Column10.HeaderText = "نوع الارسال";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 91;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "a_Send_rem_flag";
            this.Column13.HeaderText = "حالة الارسال";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 94;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 88;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "R_amount";
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 88;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 88;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "PR_ACUR_NAME";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 88;
            // 
            // Column73
            // 
            this.Column73.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column73.DataPropertyName = "S_Acust_name";
            this.Column73.HeaderText = "الجهة المصدرة";
            this.Column73.Name = "Column73";
            this.Column73.ReadOnly = true;
            // 
            // Column74
            // 
            this.Column74.DataPropertyName = "D_Acust_name";
            this.Column74.HeaderText = "الجهة الدافعة";
            this.Column74.Name = "Column74";
            this.Column74.ReadOnly = true;
            // 
            // Column27
            // 
            this.Column27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column27.DataPropertyName = "ACUST_NAME";
            this.Column27.HeaderText = "اسم الوكيل المصدر";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            this.Column27.Width = 120;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "cust_paid_aname";
            this.Column19.HeaderText = "الجهة المسددة للحوالة";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "S_ACITY_NAME";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 97;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "t_ACITY_NAME";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 96;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Case_Date";
            dataGridViewCellStyle3.Format = "G";
            dataGridViewCellStyle3.NullValue = "dd/MM/yyyy";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 91;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "NDate1";
            dataGridViewCellStyle4.NullValue = "dd\'/\'MM\'/\'yyyy";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column11.HeaderText = "تاريخ تنظيم السجلات";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 131;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "اسم المرسل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 87;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "S_phone";
            this.Column12.HeaderText = "هاتف المرسل";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Visible = false;
            this.Column12.Width = 97;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "S_address";
            this.Column16.HeaderText = "عنوان المرسل";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Visible = false;
            this.Column16.Width = 97;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "S_Street";
            this.Column17.HeaderText = "الزقاق";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Visible = false;
            this.Column17.Width = 64;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "S_Suburb";
            this.Column18.HeaderText = "الحي";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Visible = false;
            this.Column18.Width = 57;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column20.DataPropertyName = "S_Post_Code";
            this.Column20.HeaderText = "الرقم البريدي";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Visible = false;
            this.Column20.Width = 94;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column21.DataPropertyName = "S_State";
            this.Column21.HeaderText = "المحافظة";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Visible = false;
            this.Column21.Width = 76;
            // 
            // Column28
            // 
            this.Column28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column28.DataPropertyName = "S_Social_No";
            this.Column28.HeaderText = "الرقم الوطني للمرسل";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.Visible = false;
            this.Column28.Width = 129;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn4.DataPropertyName = "sFRM_ADOC_NA";
            this.dataGridViewCheckBoxColumn4.HeaderText = "نوع وثيقة المرسل";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.ReadOnly = true;
            this.dataGridViewCheckBoxColumn4.Visible = false;
            this.dataGridViewCheckBoxColumn4.Width = 115;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column22.DataPropertyName = "S_doc_no";
            this.Column22.HeaderText = "رقم وثيقة المرسل";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Visible = false;
            this.Column22.Width = 114;
            // 
            // Column23
            // 
            this.Column23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column23.DataPropertyName = "S_doc_ida";
            this.Column23.HeaderText = "تاريخ اصدار وثيقة المرسل";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.Visible = false;
            this.Column23.Width = 155;
            // 
            // Column24
            // 
            this.Column24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column24.DataPropertyName = "S_doc_eda";
            this.Column24.HeaderText = "تاريخ انتهاء وثيقة المرسل";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.Visible = false;
            this.Column24.Width = 155;
            // 
            // Column25
            // 
            this.Column25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column25.DataPropertyName = "S_doc_issue";
            this.Column25.HeaderText = "اصدار وثيقة المرسل";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.Visible = false;
            this.Column25.Width = 124;
            // 
            // Column26
            // 
            this.Column26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column26.DataPropertyName = "s_Email";
            this.Column26.HeaderText = "ايميل المرسل";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.Visible = false;
            this.Column26.Width = 94;
            // 
            // Column29
            // 
            this.Column29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column29.DataPropertyName = "s_A_NAT_NAME";
            this.Column29.HeaderText = "جنسية المرسل";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.Visible = false;
            // 
            // Column72
            // 
            this.Column72.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column72.DataPropertyName = "SBirth_Place";
            this.Column72.HeaderText = "مكان تولد المرسل";
            this.Column72.Name = "Column72";
            this.Column72.ReadOnly = true;
            this.Column72.Visible = false;
            this.Column72.Width = 117;
            // 
            // Column57
            // 
            this.Column57.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column57.DataPropertyName = "sbirth_date";
            this.Column57.HeaderText = "تولد المرسل";
            this.Column57.Name = "Column57";
            this.Column57.ReadOnly = true;
            this.Column57.Visible = false;
            this.Column57.Width = 88;
            // 
            // Column54
            // 
            this.Column54.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column54.DataPropertyName = "s_job";
            this.Column54.HeaderText = "مهنة المرسل";
            this.Column54.Name = "Column54";
            this.Column54.ReadOnly = true;
            this.Column54.Visible = false;
            this.Column54.Width = 91;
            // 
            // Column55
            // 
            this.Column55.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column55.DataPropertyName = "s_details_job";
            this.Column55.HeaderText = "تفاصيل مهنة المرسل";
            this.Column55.Name = "Column55";
            this.Column55.ReadOnly = true;
            this.Column55.Visible = false;
            this.Column55.Width = 129;
            // 
            // Column30
            // 
            this.Column30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column30.DataPropertyName = "s_notes";
            this.Column30.HeaderText = "ملاحظات المرسل";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Visible = false;
            this.Column30.Width = 113;
            // 
            // Column31
            // 
            this.Column31.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column31.DataPropertyName = "r_name";
            this.Column31.HeaderText = "اسم المستلم";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.Width = 86;
            // 
            // Column70
            // 
            this.Column70.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column70.DataPropertyName = "Real_Paid_Name";
            this.Column70.HeaderText = "الاسم كما في الهوية";
            this.Column70.Name = "Column70";
            this.Column70.ReadOnly = true;
            this.Column70.Width = 128;
            // 
            // Column32
            // 
            this.Column32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column32.DataPropertyName = "R_phone";
            this.Column32.HeaderText = "هاتف المستلم";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.Visible = false;
            this.Column32.Width = 96;
            // 
            // Column33
            // 
            this.Column33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column33.DataPropertyName = "r_ACITY_NAME";
            this.Column33.HeaderText = "مدينة المستلم";
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            this.Column33.Visible = false;
            this.Column33.Width = 93;
            // 
            // Column34
            // 
            this.Column34.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column34.DataPropertyName = "r_ACOUN_NAME";
            this.Column34.HeaderText = "بلد المستلم";
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            this.Column34.Visible = false;
            this.Column34.Width = 81;
            // 
            // Column36
            // 
            this.Column36.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column36.DataPropertyName = "r_address";
            this.Column36.HeaderText = "عنوان المستلم";
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            this.Column36.Visible = false;
            this.Column36.Width = 96;
            // 
            // Column37
            // 
            this.Column37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column37.DataPropertyName = "r_Street";
            this.Column37.HeaderText = "الزقاق";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            this.Column37.Visible = false;
            this.Column37.Width = 64;
            // 
            // Column38
            // 
            this.Column38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column38.DataPropertyName = "r_Suburb";
            this.Column38.HeaderText = "الحي للمستلم";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            this.Column38.Visible = false;
            this.Column38.Width = 93;
            // 
            // Column39
            // 
            this.Column39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column39.DataPropertyName = "r_Post_Code";
            this.Column39.HeaderText = "الرمز البريدي للمستلم";
            this.Column39.Name = "Column39";
            this.Column39.ReadOnly = true;
            this.Column39.Visible = false;
            this.Column39.Width = 131;
            // 
            // Column35
            // 
            this.Column35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column35.DataPropertyName = "r_State";
            this.Column35.HeaderText = "محافظة المستلم";
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            this.Column35.Visible = false;
            this.Column35.Width = 106;
            // 
            // Column40
            // 
            this.Column40.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column40.DataPropertyName = "R_Social_No";
            this.Column40.HeaderText = "الرقم الوطني للمستلم";
            this.Column40.Name = "Column40";
            this.Column40.ReadOnly = true;
            this.Column40.Visible = false;
            this.Column40.Width = 128;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn13.DataPropertyName = "rFRM_ADOC_NA";
            this.dataGridViewTextBoxColumn13.HeaderText = "نوع وثيقة المستلم";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 114;
            // 
            // Column41
            // 
            this.Column41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column41.DataPropertyName = "r_doc_no";
            this.Column41.HeaderText = "رقم وثيقة المستلم";
            this.Column41.Name = "Column41";
            this.Column41.ReadOnly = true;
            this.Column41.Visible = false;
            this.Column41.Width = 113;
            // 
            // Column42
            // 
            this.Column42.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column42.DataPropertyName = "r_doc_ida";
            this.Column42.HeaderText = "تاريخ اصدار الوثيقة المستلم";
            this.Column42.Name = "Column42";
            this.Column42.ReadOnly = true;
            this.Column42.Visible = false;
            this.Column42.Width = 160;
            // 
            // Column43
            // 
            this.Column43.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column43.DataPropertyName = "r_doc_eda";
            this.Column43.HeaderText = "تاريخ انتهاء الوثيقة المستلم";
            this.Column43.Name = "Column43";
            this.Column43.ReadOnly = true;
            this.Column43.Visible = false;
            this.Column43.Width = 160;
            // 
            // Column44
            // 
            this.Column44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column44.DataPropertyName = "r_doc_issue";
            this.Column44.HeaderText = "اصدار وثيقة المستلم";
            this.Column44.Name = "Column44";
            this.Column44.ReadOnly = true;
            this.Column44.Visible = false;
            this.Column44.Width = 123;
            // 
            // Column45
            // 
            this.Column45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column45.DataPropertyName = "r_Email";
            this.Column45.HeaderText = "ايميل المستلم";
            this.Column45.Name = "Column45";
            this.Column45.ReadOnly = true;
            this.Column45.Visible = false;
            this.Column45.Width = 93;
            // 
            // Column58
            // 
            this.Column58.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column58.DataPropertyName = "rbirth_date";
            this.Column58.HeaderText = "تولد المستلم";
            this.Column58.Name = "Column58";
            this.Column58.ReadOnly = true;
            this.Column58.Visible = false;
            this.Column58.Width = 87;
            // 
            // Column56
            // 
            this.Column56.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column56.DataPropertyName = "r_job";
            this.Column56.HeaderText = "مهنة المستلم";
            this.Column56.Name = "Column56";
            this.Column56.ReadOnly = true;
            this.Column56.Visible = false;
            this.Column56.Width = 90;
            // 
            // Column63
            // 
            this.Column63.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column63.DataPropertyName = "r_details_job";
            this.Column63.HeaderText = "تفاصيل مهنة المستلم";
            this.Column63.Name = "Column63";
            this.Column63.ReadOnly = true;
            this.Column63.Visible = false;
            this.Column63.Width = 128;
            // 
            // Column49
            // 
            this.Column49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column49.DataPropertyName = "r_notes";
            this.Column49.HeaderText = "ملاحظات المستلم";
            this.Column49.Name = "Column49";
            this.Column49.ReadOnly = true;
            this.Column49.Visible = false;
            this.Column49.Width = 112;
            // 
            // Column52
            // 
            this.Column52.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column52.DataPropertyName = "vo_no";
            this.Column52.HeaderText = "رقم السند";
            this.Column52.Name = "Column52";
            this.Column52.ReadOnly = true;
            this.Column52.Width = 77;
            // 
            // Column53
            // 
            this.Column53.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column53.DataPropertyName = "In_rec_date";
            this.Column53.HeaderText = "تاريخ السجلات";
            this.Column53.Name = "Column53";
            this.Column53.ReadOnly = true;
            this.Column53.Visible = false;
            this.Column53.Width = 104;
            // 
            // Column80
            // 
            this.Column80.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column80.DataPropertyName = "create_date";
            this.Column80.HeaderText = "تاريخ انشاء الحوالة في مركز الاونلاين";
            this.Column80.Name = "Column80";
            this.Column80.ReadOnly = true;
            this.Column80.Width = 214;
            // 
            // Column59
            // 
            this.Column59.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column59.DataPropertyName = "s_ocomm";
            this.Column59.HeaderText = "عمولة المرسل";
            this.Column59.Name = "Column59";
            this.Column59.ReadOnly = true;
            this.Column59.Visible = false;
            this.Column59.Width = 97;
            // 
            // Column60
            // 
            this.Column60.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column60.DataPropertyName = "s_ACUR_NAME";
            this.Column60.HeaderText = "عملة المرسل";
            this.Column60.Name = "Column60";
            this.Column60.ReadOnly = true;
            this.Column60.Visible = false;
            this.Column60.Width = 91;
            // 
            // Column61
            // 
            this.Column61.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column61.DataPropertyName = "c_ocomm";
            this.Column61.HeaderText = "مبلغ العمولة";
            this.Column61.Name = "Column61";
            this.Column61.ReadOnly = true;
            this.Column61.Visible = false;
            this.Column61.Width = 89;
            // 
            // Column62
            // 
            this.Column62.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column62.DataPropertyName = "c_ACUR_NAME";
            this.Column62.HeaderText = "عملة العمولة";
            this.Column62.Name = "Column62";
            this.Column62.ReadOnly = true;
            this.Column62.Visible = false;
            this.Column62.Width = 89;
            // 
            // Column71
            // 
            this.Column71.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column71.DataPropertyName = "rate_online";
            this.Column71.HeaderText = "سعر التعادل";
            this.Column71.Name = "Column71";
            this.Column71.ReadOnly = true;
            this.Column71.Visible = false;
            this.Column71.Width = 90;
            // 
            // Column75
            // 
            this.Column75.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column75.DataPropertyName = "Source_money";
            this.Column75.HeaderText = "مصدر المال";
            this.Column75.Name = "Column75";
            this.Column75.ReadOnly = true;
            this.Column75.Visible = false;
            this.Column75.Width = 87;
            // 
            // Column76
            // 
            this.Column76.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column76.DataPropertyName = "Relation_S_R";
            this.Column76.HeaderText = "علاقة المرسل بالمستلم";
            this.Column76.Name = "Column76";
            this.Column76.ReadOnly = true;
            this.Column76.Visible = false;
            this.Column76.Width = 138;
            // 
            // Column51
            // 
            this.Column51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column51.DataPropertyName = "r_Relation";
            this.Column51.HeaderText = "علاقة المستلم بالمرسل";
            this.Column51.Name = "Column51";
            this.Column51.ReadOnly = true;
            this.Column51.Visible = false;
            this.Column51.Width = 138;
            // 
            // Column48
            // 
            this.Column48.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column48.DataPropertyName = "T_purpose";
            this.Column48.HeaderText = "غرض الارسال";
            this.Column48.Name = "Column48";
            this.Column48.ReadOnly = true;
            this.Column48.Visible = false;
            this.Column48.Width = 101;
            // 
            // Column50
            // 
            this.Column50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column50.DataPropertyName = "R_T_purpose";
            this.Column50.HeaderText = "غرض المستلم ";
            this.Column50.Name = "Column50";
            this.Column50.ReadOnly = true;
            this.Column50.Visible = false;
            this.Column50.Width = 102;
            // 
            // Column83
            // 
            this.Column83.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column83.DataPropertyName = "i_SOComm";
            this.Column83.HeaderText = "العمولة الواردة";
            this.Column83.Name = "Column83";
            this.Column83.ReadOnly = true;
            this.Column83.Visible = false;
            // 
            // Column84
            // 
            this.Column84.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column84.DataPropertyName = "i_SACUR_NAME";
            this.Column84.HeaderText = "عملة العمولة الواردة";
            this.Column84.Name = "Column84";
            this.Column84.ReadOnly = true;
            this.Column84.Visible = false;
            this.Column84.Width = 124;
            // 
            // Column85
            // 
            this.Column85.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column85.DataPropertyName = "i_SAType_name";
            this.Column85.HeaderText = "نوع العمولة الواردة";
            this.Column85.Name = "Column85";
            this.Column85.ReadOnly = true;
            this.Column85.Visible = false;
            this.Column85.Width = 121;
            // 
            // Column98
            // 
            this.Column98.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column98.DataPropertyName = "user_name";
            this.Column98.HeaderText = "المستخدم";
            this.Column98.Name = "Column98";
            this.Column98.ReadOnly = true;
            this.Column98.Width = 76;
            // 
            // Column81
            // 
            this.Column81.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column81.DataPropertyName = "User_Confirm_Name";
            this.Column81.HeaderText = "مستخدم تأكيد الحوالة";
            this.Column81.Name = "Column81";
            this.Column81.ReadOnly = true;
            this.Column81.Width = 134;
            // 
            // Column89
            // 
            this.Column89.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column89.DataPropertyName = "User_INConfirm_Name";
            this.Column89.HeaderText = "مستخدم تأكيد الحوالة الواردة";
            this.Column89.Name = "Column89";
            this.Column89.ReadOnly = true;
            this.Column89.Width = 169;
            // 
            // Column91
            // 
            this.Column91.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column91.DataPropertyName = "rem_pay_aname";
            this.Column91.HeaderText = "طريقة الدفع";
            this.Column91.Name = "Column91";
            this.Column91.ReadOnly = true;
            this.Column91.Width = 88;
            // 
            // Column93
            // 
            this.Column93.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column93.DataPropertyName = "payment_info";
            this.Column93.HeaderText = "معلومات عن طريق الدفع";
            this.Column93.Name = "Column93";
            this.Column93.ReadOnly = true;
            this.Column93.Width = 147;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column15.DataPropertyName = "Code_Rem";
            this.Column15.HeaderText = "الرقم السري";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 90;
            // 
            // Column95
            // 
            this.Column95.DataPropertyName = "real_agent_amount";
            dataGridViewCellStyle5.Format = "N3";
            dataGridViewCellStyle5.NullValue = null;
            this.Column95.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column95.HeaderText = "المبلغ الفعلي لأستلامه";
            this.Column95.Name = "Column95";
            this.Column95.ReadOnly = true;
            // 
            // Column96
            // 
            this.Column96.DataPropertyName = "cur_Aname_exch_agent";
            this.Column96.HeaderText = "عملة المبلغ الفعلي";
            this.Column96.Name = "Column96";
            this.Column96.ReadOnly = true;
            // 
            // Column97
            // 
            this.Column97.DataPropertyName = "agent_comm_amount";
            dataGridViewCellStyle6.Format = "N3";
            dataGridViewCellStyle6.NullValue = null;
            this.Column97.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column97.HeaderText = "العمولة الفعلية";
            this.Column97.Name = "Column97";
            this.Column97.ReadOnly = true;
            // 
            // Column100
            // 
            this.Column100.DataPropertyName = "exchange_agent_amount";
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = null;
            this.Column100.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column100.HeaderText = "سعر تعادل الوكيل";
            this.Column100.Name = "Column100";
            this.Column100.ReadOnly = true;
            // 
            // Inquery_Remittances
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 689);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.Txt_R_notes);
            this.Controls.Add(this.Txt_R_details_job);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.Txt_R_Relation);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.Txt_S_details_job);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.Btn_Rpt);
            this.Controls.Add(this.txt_purpose_rec);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.txt_purpose_sen);
            this.Controls.Add(this.txt_social_rec);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.txt_social_sen);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.txt_resd_rec);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.txt_resd_sen);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Tot_Amount);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.Cbo_Rcur);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.Txt_RCount);
            this.Controls.Add(this.Txt_Discreption);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.Grd_Rem_Cases);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Print_Rem_Cases);
            this.Controls.Add(this.Print_Details);
            this.Controls.Add(this.Btn_Exist);
            this.Controls.Add(this.Print_All);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_r_birthdate);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_R_Birth_Place);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Txt_r_doc_issue);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.Txt_R_doc_no);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Txt_R_doc_type);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_doc_date);
            this.Controls.Add(this.Txt_R_address);
            this.Controls.Add(this.Txt_r_job);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_S_Birth);
            this.Controls.Add(this.Txt_Relionship);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_S_Birth_Place);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_S_doc_issue);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_S_doc_no);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtScity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_S_doc_type);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.Txt_S_doc_Date);
            this.Controls.Add(this.TxtS_address);
            this.Controls.Add(this.Txt_s_job);
            this.Controls.Add(this.Txt_Soruce_money);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.Grdrec_rem);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Inquery_Remittances";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "506";
            this.Text = "Inquery_Remittances";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Inquery_Remittances_FormClosed);
            this.Load += new System.EventHandler(this.Inquery_Remittances_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rem_Cases)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private MyDateTextBox Txt_S_doc_Date;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.TextBox Txt_s_job;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txt_S_doc_type;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtScity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txt_S_doc_issue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_S_Birth_Place;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label12;
        private MyDateTextBox Txt_S_Birth;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private MyDateTextBox Txt_r_birthdate;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_R_Birth_Place;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Txt_r_doc_issue;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_R_doc_no;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Txt_R_doc_type;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private MyDateTextBox Txt_R_doc_date;
        private System.Windows.Forms.TextBox Txt_R_address;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox Txt_r_job;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button Print_All;
        private System.Windows.Forms.Button Btn_Exist;
        private System.Windows.Forms.Button Print_Details;
        private System.Windows.Forms.Button Print_Rem_Cases;
        private System.Windows.Forms.DataGridView Grdrec_rem;
        private System.Windows.Forms.DataGridView Grd_Rem_Cases;
        private System.Windows.Forms.TextBox Txt_Discreption;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox Cbo_Rcur;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox Txt_RCount;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_resd_sen;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_resd_rec;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txt_social_sen;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txt_social_rec;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txt_purpose_sen;
        private System.Windows.Forms.TextBox txt_purpose_rec;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button Btn_Rpt;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox Txt_S_details_job;
        private System.Windows.Forms.TextBox Txt_R_Relation;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox Txt_R_details_job;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox Txt_R_notes;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column78;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column79;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column66;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column77;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column68;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column64;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column86;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column87;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column88;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column99;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column82;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column90;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column92;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column94;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column73;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column74;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column72;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column57;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column54;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column55;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column70;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column58;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column56;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column63;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column52;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column53;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column80;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column59;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column60;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column61;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column62;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column71;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column75;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column76;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column51;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column50;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column83;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column84;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column85;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column98;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column81;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column89;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column91;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column93;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column95;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column96;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column97;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column100;
    }
}