﻿namespace Integration_Accounting_Sys
{
    partial class Prepare_Return_Rem_online
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CboCust_Id = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.LblDiscount = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GrdRem_Details = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_S_name = new System.Windows.Forms.TextBox();
            this.Txt_R_name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Cbo_Record = new System.Windows.Forms.ComboBox();
            this.Txt_Rem_no = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbo_local_intr = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Cbo_Off_On = new System.Windows.Forms.ComboBox();
            this.TxtNrec_Date = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.Cmbo_Oper_type = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbo_order_type = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TXT_Reason = new System.Windows.Forms.TextBox();
            this.LBL_Reason = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdRem_Details)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            this.SuspendLayout();
            // 
            // CboCust_Id
            // 
            this.CboCust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCust_Id.FormattingEnabled = true;
            this.CboCust_Id.Location = new System.Drawing.Point(147, 215);
            this.CboCust_Id.Name = "CboCust_Id";
            this.CboCust_Id.Size = new System.Drawing.Size(311, 24);
            this.CboCust_Id.TabIndex = 2;
            this.CboCust_Id.SelectedIndexChanged += new System.EventHandler(this.CboCust_Id_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(482, 95);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(128, 14);
            this.label6.TabIndex = 194;
            this.label6.Text = "اسم مرسـل الحوالة :";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(444, 496);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(99, 28);
            this.ExtBtn.TabIndex = 11;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(289, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(156, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(774, 218);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(103, 25);
            this.BtnAdd.TabIndex = 8;
            this.BtnAdd.Text = "عرض";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(346, 496);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(99, 28);
            this.BtnOk.TabIndex = 10;
            this.BtnOk.Text = "موافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 6);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(201, 23);
            this.TxtTerm_Name.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(696, 10);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 180;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(750, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(127, 23);
            this.TxtIn_Rec_Date.TabIndex = 3;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(3, 35);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(881, 3);
            this.flowLayoutPanel9.TabIndex = 182;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(210, 10);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(80, 14);
            this.label1.TabIndex = 179;
            this.label1.Text = " المستخــدم:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 442);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(881, 1);
            this.flowLayoutPanel1.TabIndex = 203;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 527);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(889, 22);
            this.statusStrip1.TabIndex = 544;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(3, 35);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 407);
            this.flowLayoutPanel7.TabIndex = 555;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(883, 35);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1, 408);
            this.flowLayoutPanel3.TabIndex = 556;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(15, 220);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(128, 14);
            this.label8.TabIndex = 561;
            this.label8.Text = " الفــــــرع / الوكـــــلاء:";
            // 
            // LblDiscount
            // 
            this.LblDiscount.AutoSize = true;
            this.LblDiscount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDiscount.ForeColor = System.Drawing.Color.Maroon;
            this.LblDiscount.Location = new System.Drawing.Point(260, 345);
            this.LblDiscount.Name = "LblDiscount";
            this.LblDiscount.Size = new System.Drawing.Size(0, 16);
            this.LblDiscount.TabIndex = 545;
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(554, 6);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(139, 23);
            this.Txt_Loc_Cur.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(454, 10);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(94, 14);
            this.label3.TabIndex = 562;
            this.label3.Text = "العملة المحلية:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(481, 120);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(129, 14);
            this.label2.TabIndex = 564;
            this.label2.Text = "اسم مستلم الحوالة :";
            // 
            // GrdRem_Details
            // 
            this.GrdRem_Details.AllowUserToAddRows = false;
            this.GrdRem_Details.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdRem_Details.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdRem_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.GrdRem_Details.ColumnHeadersHeight = 30;
            this.GrdRem_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column9,
            this.Column3,
            this.Column4,
            this.Column7,
            this.Column6,
            this.Column8});
            this.GrdRem_Details.Location = new System.Drawing.Point(10, 248);
            this.GrdRem_Details.Name = "GrdRem_Details";
            this.GrdRem_Details.RowHeadersWidth = 25;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdRem_Details.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.GrdRem_Details.Size = new System.Drawing.Size(867, 192);
            this.GrdRem_Details.TabIndex = 9;
            this.GrdRem_Details.SelectionChanged += new System.EventHandler(this.GrdRem_Details_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Chk";
            this.Column1.HeaderText = "تأشير ";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "Rem_no";
            this.Column2.HeaderText = "رقم الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 84;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "R_Amount";
            this.Column5.HeaderText = "مبلغ الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 88;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "R_acur_name";
            this.Column9.HeaderText = "عملة الحوالة";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 88;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "S_name";
            this.Column3.HeaderText = "اسم المرسل";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 87;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "R_name";
            this.Column4.HeaderText = "اسم المستلم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 86;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "t_acoun_name";
            this.Column7.HeaderText = "بلد الاستلام";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 84;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "t_acity_name";
            this.Column6.HeaderText = "مدينة الاستلام";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 96;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "Case_date";
            this.Column8.HeaderText = "تاريخ انشاء الحوالة";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 124;
            // 
            // Txt_S_name
            // 
            this.Txt_S_name.Location = new System.Drawing.Point(612, 92);
            this.Txt_S_name.Name = "Txt_S_name";
            this.Txt_S_name.Size = new System.Drawing.Size(260, 20);
            this.Txt_S_name.TabIndex = 4;
            this.Txt_S_name.TextChanged += new System.EventHandler(this.Txt_S_name_TextChanged);
            // 
            // Txt_R_name
            // 
            this.Txt_R_name.Location = new System.Drawing.Point(612, 117);
            this.Txt_R_name.Name = "Txt_R_name";
            this.Txt_R_name.Size = new System.Drawing.Size(260, 20);
            this.Txt_R_name.TabIndex = 6;
            this.Txt_R_name.TextChanged += new System.EventHandler(this.Txt_R_name_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(6, 44);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(127, 14);
            this.label9.TabIndex = 572;
            this.label9.Text = "نـــــوع العمليـــــــــــة :";
            // 
            // Cbo_Record
            // 
            this.Cbo_Record.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Record.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Record.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Record.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Record.FormattingEnabled = true;
            this.Cbo_Record.Items.AddRange(new object[] {
            "تهيئـــــــــــــــــة",
            "الغاء التهيـــــــــة",
            "الغاء التوقيف مع تهيئـــــــــة"});
            this.Cbo_Record.Location = new System.Drawing.Point(139, 39);
            this.Cbo_Record.Name = "Cbo_Record";
            this.Cbo_Record.Size = new System.Drawing.Size(311, 24);
            this.Cbo_Record.TabIndex = 0;
            this.Cbo_Record.SelectedIndexChanged += new System.EventHandler(this.Cbo_Record_SelectedIndexChanged);
            // 
            // Txt_Rem_no
            // 
            this.Txt_Rem_no.Location = new System.Drawing.Point(612, 143);
            this.Txt_Rem_no.Name = "Txt_Rem_no";
            this.Txt_Rem_no.Size = new System.Drawing.Size(260, 20);
            this.Txt_Rem_no.TabIndex = 5;
            this.Txt_Rem_no.TextChanged += new System.EventHandler(this.Txt_Rem_no_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(491, 146);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(119, 14);
            this.label10.TabIndex = 573;
            this.label10.Text = "رقــــــم الحوالــــــــة:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(491, 198);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(120, 14);
            this.label11.TabIndex = 917;
            this.label11.Text = "مسار الحوالــــــــــة :";
            // 
            // cbo_local_intr
            // 
            this.cbo_local_intr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_local_intr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_local_intr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_local_intr.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_local_intr.FormattingEnabled = true;
            this.cbo_local_intr.Items.AddRange(new object[] {
            "شبكــــــة محلية",
            "شبكــــــة اونلاين"});
            this.cbo_local_intr.Location = new System.Drawing.Point(613, 193);
            this.cbo_local_intr.Name = "cbo_local_intr";
            this.cbo_local_intr.Size = new System.Drawing.Size(260, 24);
            this.cbo_local_intr.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(494, 171);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(117, 14);
            this.label12.TabIndex = 919;
            this.label12.Text = "نـــوع الحوالــــــــــة :";
            // 
            // Cbo_Off_On
            // 
            this.Cbo_Off_On.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Off_On.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Off_On.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Off_On.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Off_On.FormattingEnabled = true;
            this.Cbo_Off_On.Items.AddRange(new object[] {
            "حــــــوالات اونـــلايـن",
            "حـــــــولات اوفـــلايـن"});
            this.Cbo_Off_On.Location = new System.Drawing.Point(613, 166);
            this.Cbo_Off_On.Name = "Cbo_Off_On";
            this.Cbo_Off_On.Size = new System.Drawing.Size(260, 24);
            this.Cbo_Off_On.TabIndex = 918;
            this.Cbo_Off_On.SelectedIndexChanged += new System.EventHandler(this.Cbo_Off_On_SelectedIndexChanged);
            // 
            // TxtNrec_Date
            // 
            this.TxtNrec_Date.Checked = false;
            this.TxtNrec_Date.CustomFormat = "dd/mm/yyyy";
            this.TxtNrec_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtNrec_Date.Location = new System.Drawing.Point(611, 69);
            this.TxtNrec_Date.Name = "TxtNrec_Date";
            this.TxtNrec_Date.ShowCheckBox = true;
            this.TxtNrec_Date.Size = new System.Drawing.Size(138, 20);
            this.TxtNrec_Date.TabIndex = 921;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(489, 47);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(120, 14);
            this.label5.TabIndex = 923;
            this.label5.Text = "حالـــة الحوالـــــــــة :";
            // 
            // Cmbo_Oper_type
            // 
            this.Cmbo_Oper_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmbo_Oper_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmbo_Oper_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmbo_Oper_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmbo_Oper_type.FormattingEnabled = true;
            this.Cmbo_Oper_type.Items.AddRange(new object[] {
            "حوالـــــــة صـــادرة",
            "حــوالـــــــــة واردة"});
            this.Cmbo_Oper_type.Location = new System.Drawing.Point(611, 42);
            this.Cmbo_Oper_type.Name = "Cmbo_Oper_type";
            this.Cmbo_Oper_type.Size = new System.Drawing.Size(260, 24);
            this.Cmbo_Oper_type.TabIndex = 920;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(488, 72);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(121, 14);
            this.label7.TabIndex = 922;
            this.label7.Text = "تاريخ حالة الحوالــة :";
            // 
            // cbo_order_type
            // 
            this.cbo_order_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_order_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_order_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_order_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_order_type.FormattingEnabled = true;
            this.cbo_order_type.Items.AddRange(new object[] {
            "فروع",
            "وكالات"});
            this.cbo_order_type.Location = new System.Drawing.Point(308, 66);
            this.cbo_order_type.Name = "cbo_order_type";
            this.cbo_order_type.Size = new System.Drawing.Size(142, 24);
            this.cbo_order_type.TabIndex = 1254;
            this.cbo_order_type.SelectedIndexChanged += new System.EventHandler(this.cbo_order_type_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(17, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 16);
            this.label13.TabIndex = 1253;
            this.label13.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(87, 68);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(224, 22);
            this.Txt_Sub_Cust.TabIndex = 1252;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column10,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(12, 91);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(439, 120);
            this.Grd_Sub_Cust.TabIndex = 1251;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "cust_id";
            dataGridViewCellStyle13.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 91;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Sub_Cust_ID";
            this.Column10.HeaderText = "رمز الثانوي";
            this.Column10.Name = "Column10";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 225;
            // 
            // TXT_Reason
            // 
            this.TXT_Reason.Location = new System.Drawing.Point(96, 449);
            this.TXT_Reason.Multiline = true;
            this.TXT_Reason.Name = "TXT_Reason";
            this.TXT_Reason.Size = new System.Drawing.Size(781, 43);
            this.TXT_Reason.TabIndex = 1255;
            // 
            // LBL_Reason
            // 
            this.LBL_Reason.AutoSize = true;
            this.LBL_Reason.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Reason.ForeColor = System.Drawing.Color.Navy;
            this.LBL_Reason.Location = new System.Drawing.Point(10, 452);
            this.LBL_Reason.Name = "LBL_Reason";
            this.LBL_Reason.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LBL_Reason.Size = new System.Drawing.Size(80, 14);
            this.LBL_Reason.TabIndex = 1256;
            this.LBL_Reason.Text = "السبــــــــــب:";
            // 
            // Prepare_Return_Rem_online
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 549);
            this.Controls.Add(this.TXT_Reason);
            this.Controls.Add(this.LBL_Reason);
            this.Controls.Add(this.cbo_order_type);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.Controls.Add(this.TxtNrec_Date);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cmbo_Oper_type);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Cbo_Off_On);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbo_local_intr);
            this.Controls.Add(this.Txt_Rem_no);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Cbo_Record);
            this.Controls.Add(this.Txt_R_name);
            this.Controls.Add(this.Txt_S_name);
            this.Controls.Add(this.GrdRem_Details);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.LblDiscount);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.CboCust_Id);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Prepare_Return_Rem_online";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "502";
            this.Text = "تهيئة حوالات";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Prepare_Return_Buy_sale_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Prepare_Return_Buy_sale_FormClosed);
            this.Load += new System.EventHandler(this.Prepare_Return_Buy_sale_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdRem_Details)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CboCust_Id;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LblDiscount;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView GrdRem_Details;
        private System.Windows.Forms.TextBox Txt_S_name;
        private System.Windows.Forms.TextBox Txt_R_name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Cbo_Record;
        private System.Windows.Forms.TextBox Txt_Rem_no;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbo_local_intr;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Cbo_Off_On;
        private System.Windows.Forms.DateTimePicker TxtNrec_Date;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox Cmbo_Oper_type;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbo_order_type;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox TXT_Reason;
        private System.Windows.Forms.Label LBL_Reason;
    }
}