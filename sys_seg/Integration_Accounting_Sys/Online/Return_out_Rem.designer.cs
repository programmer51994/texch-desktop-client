﻿namespace Integration_Accounting_Sys
{
    partial class Return_out_Rem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.Cmb_oper_type = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Grd_Return = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lbl_r_note = new System.Windows.Forms.Label();
            this.BtnOk = new System.Windows.Forms.Button();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Txt_Rem_No = new System.Windows.Forms.TextBox();
            this.Txt_S_Name = new System.Windows.Forms.TextBox();
            this.Txt_S_Phone = new System.Windows.Forms.TextBox();
            this.Txt_S_Doc_No = new System.Windows.Forms.TextBox();
            this.Txt_Doc_S_Issue = new System.Windows.Forms.TextBox();
            this.Txt_S_job = new System.Windows.Forms.TextBox();
            this.Txt_source_money = new System.Windows.Forms.TextBox();
            this.Txt_S_Addres = new System.Windows.Forms.TextBox();
            this.Txt_Relship = new System.Windows.Forms.TextBox();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_job = new System.Windows.Forms.TextBox();
            this.Txt_R_Addres = new System.Windows.Forms.TextBox();
            this.Txt_T_Purpose = new System.Windows.Forms.TextBox();
            this.Txt_Note = new System.Windows.Forms.TextBox();
            this.Txt_S_Nat_name = new System.Windows.Forms.TextBox();
            this.Txt_S_City = new System.Windows.Forms.TextBox();
            this.Txt_S_Doc_type = new System.Windows.Forms.TextBox();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CboCust_Id = new System.Windows.Forms.ComboBox();
            this.cbo_order_type = new System.Windows.Forms.ComboBox();
            this.txt_srch = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_r_reson = new System.Windows.Forms.TextBox();
            this.lbl_r_reson = new System.Windows.Forms.Label();
            this.txt_s_note = new System.Windows.Forms.TextBox();
            this.txt_s_reson = new System.Windows.Forms.TextBox();
            this.lbl_s_note = new System.Windows.Forms.Label();
            this.lbl_s_reson = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Rbirth_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Doc_S_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_Sbirth_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Return)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-78, 30);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1059, 1);
            this.flowLayoutPanel9.TabIndex = 639;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(35, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 10);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(688, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 633;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(741, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(129, 23);
            this.TxtIn_Rec_Date.TabIndex = 9;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // Cmb_oper_type
            // 
            this.Cmb_oper_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_oper_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_oper_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_oper_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_oper_type.FormattingEnabled = true;
            this.Cmb_oper_type.Items.AddRange(new object[] {
            "اختر نوع العملية",
            "رد حوالـة صادرة",
            "رد حوالة واردة"});
            this.Cmb_oper_type.Location = new System.Drawing.Point(536, 33);
            this.Cmb_oper_type.Name = "Cmb_oper_type";
            this.Cmb_oper_type.Size = new System.Drawing.Size(199, 24);
            this.Cmb_oper_type.TabIndex = 1;
            this.Cmb_oper_type.SelectedIndexChanged += new System.EventHandler(this.Cmb_oper_type_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(467, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 645;
            this.label3.Text = "نوع العملية :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(469, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 647;
            this.label5.Text = "رقم الحوالة:";
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_OK.ForeColor = System.Drawing.Color.Navy;
            this.Btn_OK.Location = new System.Drawing.Point(806, 113);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(71, 24);
            this.Btn_OK.TabIndex = 4;
            this.Btn_OK.Text = "بحـــث";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_Search_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(7, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 16);
            this.label6.TabIndex = 789;
            this.label6.Text = "معلومات الحوالــــة........";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(602, 233);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(93, 14);
            this.label27.TabIndex = 855;
            this.label27.Text = "الجنسيـــــــــــة:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(125, 305);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(94, 14);
            this.label26.TabIndex = 848;
            this.label26.Text = "رقم الوثيقــــــة:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(602, 333);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(96, 14);
            this.label25.TabIndex = 868;
            this.label25.Text = "مهنــة المرسـل:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(570, 599);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(138, 14);
            this.label8.TabIndex = 866;
            this.label8.Text = "علاقة مرسل/ مستلم :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(120, 595);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(98, 14);
            this.label13.TabIndex = 870;
            this.label13.Text = "مصـــدر المـــال :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(150, 231);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(69, 14);
            this.label11.TabIndex = 842;
            this.label11.Text = "الاســــــــم:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(602, 257);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(95, 14);
            this.label10.TabIndex = 858;
            this.label10.Text = "مدينة المرسل :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(127, 354);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(97, 14);
            this.label22.TabIndex = 864;
            this.label22.Text = "عنوان المرسل :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(125, 331);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(94, 14);
            this.label20.TabIndex = 853;
            this.label20.Text = "جهــة الاصـــدار:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(878, 305);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(87, 14);
            this.label34.TabIndex = 863;
            this.label34.Text = "yyyy/mm/dd";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(602, 307);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(97, 14);
            this.label18.TabIndex = 861;
            this.label18.Text = "تاريخ الوثيقــــة :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(602, 281);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 860;
            this.label9.Text = "نوع الوثيقـــــــة:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Maroon;
            this.label33.Location = new System.Drawing.Point(400, 281);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(87, 14);
            this.label33.TabIndex = 17;
            this.label33.Text = "yyyy/mm/dd";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(147, 279);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(72, 14);
            this.label24.TabIndex = 845;
            this.label24.Text = "التولــــــــــد:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(0, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 14);
            this.label7.TabIndex = 791;
            this.label7.Text = "معلومـــات المرســــل ....";
            // 
            // Grd_Return
            // 
            this.Grd_Return.AllowUserToAddRows = false;
            this.Grd_Return.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Return.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Return.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_Return.ColumnHeadersHeight = 24;
            this.Grd_Return.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2,
            this.Column7,
            this.Column8,
            this.Column5,
            this.Column6,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Return.DefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_Return.Location = new System.Drawing.Point(4, 149);
            this.Grd_Return.Name = "Grd_Return";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Return.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_Return.RowHeadersVisible = false;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Return.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_Return.Size = new System.Drawing.Size(966, 75);
            this.Grd_Return.TabIndex = 5;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحولة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "R_amount";
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "S_ACITY_NAME";
            this.Column7.HeaderText = "مدينة الارسال";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 120;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "t_ACITY_NAME";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 120;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "حالة الحوالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 130;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Case_Date";
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 110;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "r_ocomm";
            this.Column9.HeaderText = "العمولة";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "r_CCur_ACUR_NAME";
            this.Column10.HeaderText = "عملة العمولة";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(145, 255);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(74, 14);
            this.label16.TabIndex = 843;
            this.label16.Text = "الهاتـــــــــف:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(604, 464);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(99, 14);
            this.label14.TabIndex = 881;
            this.label14.Text = "مدينة المستلم :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(604, 439);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(90, 14);
            this.label15.TabIndex = 878;
            this.label15.Text = "الجنسيــــــــــة:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Maroon;
            this.label21.Location = new System.Drawing.Point(0, 435);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(149, 14);
            this.label21.TabIndex = 872;
            this.label21.Text = "معلومـــات المستلــــم ....";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Maroon;
            this.label23.Location = new System.Drawing.Point(390, 489);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(87, 14);
            this.label23.TabIndex = 884;
            this.label23.Text = "yyyy/mm/dd";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(142, 488);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(72, 14);
            this.label28.TabIndex = 882;
            this.label28.Text = "التولــــــــــد:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(604, 488);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(98, 14);
            this.label29.TabIndex = 885;
            this.label29.Text = "مهنـة المستلم :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(119, 513);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(97, 14);
            this.label30.TabIndex = 887;
            this.label30.Text = "عنوان المستلم:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(125, 622);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(93, 14);
            this.label32.TabIndex = 892;
            this.label32.Text = "غرض التحويل :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Maroon;
            this.label35.Location = new System.Drawing.Point(-3, 592);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(115, 14);
            this.label35.TabIndex = 889;
            this.label35.Text = "معلومات الحوالة ...";
            // 
            // lbl_r_note
            // 
            this.lbl_r_note.AutoSize = true;
            this.lbl_r_note.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_r_note.ForeColor = System.Drawing.Color.Navy;
            this.lbl_r_note.Location = new System.Drawing.Point(101, 541);
            this.lbl_r_note.Name = "lbl_r_note";
            this.lbl_r_note.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_r_note.Size = new System.Drawing.Size(118, 14);
            this.lbl_r_note.TabIndex = 894;
            this.lbl_r_note.Text = "ملاحظات المستلم :";
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(360, 653);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(81, 25);
            this.BtnOk.TabIndex = 6;
            this.BtnOk.Text = "موافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.Btn_Ok1_Click);
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(442, 653);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(81, 25);
            this.Btn_Ext.TabIndex = 7;
            this.Btn_Ext.Text = "انهـــاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // Txt_Rem_No
            // 
            this.Txt_Rem_No.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_No.Location = new System.Drawing.Point(536, 60);
            this.Txt_Rem_No.MaxLength = 14;
            this.Txt_Rem_No.Name = "Txt_Rem_No";
            this.Txt_Rem_No.Size = new System.Drawing.Size(200, 22);
            this.Txt_Rem_No.TabIndex = 2;
            this.Txt_Rem_No.TextChanged += new System.EventHandler(this.Txt_Rem_No_TextChanged);
            // 
            // Txt_S_Name
            // 
            this.Txt_S_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Name.Location = new System.Drawing.Point(225, 228);
            this.Txt_S_Name.MaxLength = 49;
            this.Txt_S_Name.Name = "Txt_S_Name";
            this.Txt_S_Name.ReadOnly = true;
            this.Txt_S_Name.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Name.TabIndex = 10;
            // 
            // Txt_S_Phone
            // 
            this.Txt_S_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Phone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Phone.Location = new System.Drawing.Point(225, 252);
            this.Txt_S_Phone.MaxLength = 19;
            this.Txt_S_Phone.Name = "Txt_S_Phone";
            this.Txt_S_Phone.ReadOnly = true;
            this.Txt_S_Phone.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Phone.TabIndex = 16;
            // 
            // Txt_S_Doc_No
            // 
            this.Txt_S_Doc_No.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Doc_No.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Doc_No.Location = new System.Drawing.Point(225, 301);
            this.Txt_S_Doc_No.MaxLength = 49;
            this.Txt_S_Doc_No.Name = "Txt_S_Doc_No";
            this.Txt_S_Doc_No.ReadOnly = true;
            this.Txt_S_Doc_No.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Doc_No.TabIndex = 14;
            // 
            // Txt_Doc_S_Issue
            // 
            this.Txt_Doc_S_Issue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Doc_S_Issue.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_S_Issue.Location = new System.Drawing.Point(225, 327);
            this.Txt_Doc_S_Issue.MaxLength = 49;
            this.Txt_Doc_S_Issue.Name = "Txt_Doc_S_Issue";
            this.Txt_Doc_S_Issue.ReadOnly = true;
            this.Txt_Doc_S_Issue.Size = new System.Drawing.Size(259, 23);
            this.Txt_Doc_S_Issue.TabIndex = 13;
            // 
            // Txt_S_job
            // 
            this.Txt_S_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_job.Location = new System.Drawing.Point(710, 326);
            this.Txt_S_job.MaxLength = 49;
            this.Txt_S_job.Name = "Txt_S_job";
            this.Txt_S_job.ReadOnly = true;
            this.Txt_S_job.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_job.TabIndex = 23;
            // 
            // Txt_source_money
            // 
            this.Txt_source_money.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_source_money.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_source_money.Location = new System.Drawing.Point(224, 593);
            this.Txt_source_money.MaxLength = 99;
            this.Txt_source_money.Name = "Txt_source_money";
            this.Txt_source_money.ReadOnly = true;
            this.Txt_source_money.Size = new System.Drawing.Size(259, 23);
            this.Txt_source_money.TabIndex = 922;
            // 
            // Txt_S_Addres
            // 
            this.Txt_S_Addres.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Addres.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Addres.Location = new System.Drawing.Point(224, 352);
            this.Txt_S_Addres.MaxLength = 99;
            this.Txt_S_Addres.Name = "Txt_S_Addres";
            this.Txt_S_Addres.ReadOnly = true;
            this.Txt_S_Addres.Size = new System.Drawing.Size(745, 23);
            this.Txt_S_Addres.TabIndex = 12;
            // 
            // Txt_Relship
            // 
            this.Txt_Relship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relship.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Relship.Location = new System.Drawing.Point(719, 593);
            this.Txt_Relship.MaxLength = 99;
            this.Txt_Relship.Name = "Txt_Relship";
            this.Txt_Relship.ReadOnly = true;
            this.Txt_Relship.Size = new System.Drawing.Size(255, 23);
            this.Txt_Relship.TabIndex = 924;
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Name.Location = new System.Drawing.Point(226, 435);
            this.Txt_R_Name.MaxLength = 49;
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_Name.TabIndex = 925;
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Phone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Phone.Location = new System.Drawing.Point(225, 460);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_Phone.TabIndex = 926;
            // 
            // Txt_R_job
            // 
            this.Txt_R_job.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_job.Location = new System.Drawing.Point(712, 484);
            this.Txt_R_job.MaxLength = 49;
            this.Txt_R_job.Name = "Txt_R_job";
            this.Txt_R_job.ReadOnly = true;
            this.Txt_R_job.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_job.TabIndex = 927;
            // 
            // Txt_R_Addres
            // 
            this.Txt_R_Addres.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Addres.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Addres.Location = new System.Drawing.Point(223, 511);
            this.Txt_R_Addres.MaxLength = 99;
            this.Txt_R_Addres.Name = "Txt_R_Addres";
            this.Txt_R_Addres.ReadOnly = true;
            this.Txt_R_Addres.Size = new System.Drawing.Size(748, 23);
            this.Txt_R_Addres.TabIndex = 928;
            // 
            // Txt_T_Purpose
            // 
            this.Txt_T_Purpose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_T_Purpose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_T_Purpose.Location = new System.Drawing.Point(224, 619);
            this.Txt_T_Purpose.MaxLength = 199;
            this.Txt_T_Purpose.Name = "Txt_T_Purpose";
            this.Txt_T_Purpose.ReadOnly = true;
            this.Txt_T_Purpose.Size = new System.Drawing.Size(751, 23);
            this.Txt_T_Purpose.TabIndex = 929;
            // 
            // Txt_Note
            // 
            this.Txt_Note.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Note.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Note.Location = new System.Drawing.Point(223, 537);
            this.Txt_Note.MaxLength = 199;
            this.Txt_Note.Name = "Txt_Note";
            this.Txt_Note.ReadOnly = true;
            this.Txt_Note.Size = new System.Drawing.Size(749, 23);
            this.Txt_Note.TabIndex = 930;
            // 
            // Txt_S_Nat_name
            // 
            this.Txt_S_Nat_name.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Nat_name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Nat_name.Location = new System.Drawing.Point(710, 226);
            this.Txt_S_Nat_name.MaxLength = 49;
            this.Txt_S_Nat_name.Name = "Txt_S_Nat_name";
            this.Txt_S_Nat_name.ReadOnly = true;
            this.Txt_S_Nat_name.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Nat_name.TabIndex = 22;
            // 
            // Txt_S_City
            // 
            this.Txt_S_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_City.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_City.Location = new System.Drawing.Point(710, 250);
            this.Txt_S_City.MaxLength = 49;
            this.Txt_S_City.Name = "Txt_S_City";
            this.Txt_S_City.ReadOnly = true;
            this.Txt_S_City.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_City.TabIndex = 20;
            // 
            // Txt_S_Doc_type
            // 
            this.Txt_S_Doc_type.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Doc_type.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Doc_type.Location = new System.Drawing.Point(710, 274);
            this.Txt_S_Doc_type.MaxLength = 49;
            this.Txt_S_Doc_type.Name = "Txt_S_Doc_type";
            this.Txt_S_Doc_type.ReadOnly = true;
            this.Txt_S_Doc_type.Size = new System.Drawing.Size(259, 23);
            this.Txt_S_Doc_type.TabIndex = 19;
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_Nat.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Nat.Location = new System.Drawing.Point(712, 432);
            this.Txt_R_Nat.MaxLength = 49;
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_Nat.TabIndex = 937;
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_R_City.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_City.Location = new System.Drawing.Point(712, 458);
            this.Txt_R_City.MaxLength = 49;
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(259, 23);
            this.Txt_R_City.TabIndex = 938;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(145, 437);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(69, 14);
            this.label19.TabIndex = 939;
            this.label19.Text = "الاســــــــم:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(140, 462);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(74, 14);
            this.label17.TabIndex = 940;
            this.label17.Text = "الهاتـــــــــف:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(1, 8);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 14);
            this.label36.TabIndex = 942;
            this.label36.Text = "المستخدم:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(71, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(168, 23);
            this.TxtUser.TabIndex = 6;
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(556, 4);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(134, 23);
            this.TxtBox_User.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(496, 8);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(62, 14);
            this.label12.TabIndex = 946;
            this.label12.Text = "الصندوق:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(241, 8);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(94, 14);
            this.label37.TabIndex = 944;
            this.label37.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(337, 3);
            this.Txt_Loc_Cur.Multiline = true;
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(159, 25);
            this.Txt_Loc_Cur.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(457, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 16);
            this.label2.TabIndex = 951;
            this.label2.Text = "الوكيل/الفــرع:";
            // 
            // CboCust_Id
            // 
            this.CboCust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCust_Id.FormattingEnabled = true;
            this.CboCust_Id.Location = new System.Drawing.Point(536, 93);
            this.CboCust_Id.Name = "CboCust_Id";
            this.CboCust_Id.Size = new System.Drawing.Size(236, 24);
            this.CboCust_Id.TabIndex = 3;
            // 
            // cbo_order_type
            // 
            this.cbo_order_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_order_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_order_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_order_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_order_type.FormattingEnabled = true;
            this.cbo_order_type.Items.AddRange(new object[] {
            "فروع",
            "وكالات"});
            this.cbo_order_type.Location = new System.Drawing.Point(308, 32);
            this.cbo_order_type.Name = "cbo_order_type";
            this.cbo_order_type.Size = new System.Drawing.Size(142, 24);
            this.cbo_order_type.TabIndex = 1258;
            this.cbo_order_type.SelectedIndexChanged += new System.EventHandler(this.cbo_order_type_SelectedIndexChanged);
            // 
            // txt_srch
            // 
            this.txt_srch.AutoSize = true;
            this.txt_srch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.txt_srch.ForeColor = System.Drawing.Color.Navy;
            this.txt_srch.Location = new System.Drawing.Point(12, 32);
            this.txt_srch.Name = "txt_srch";
            this.txt_srch.Size = new System.Drawing.Size(62, 16);
            this.txt_srch.TabIndex = 1257;
            this.txt_srch.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(75, 34);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(230, 22);
            this.Txt_Sub_Cust.TabIndex = 1256;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(12, 58);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(439, 72);
            this.Grd_Sub_Cust.TabIndex = 1255;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "cust_id";
            dataGridViewCellStyle26.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 91;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 225;
            // 
            // txt_r_reson
            // 
            this.txt_r_reson.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txt_r_reson.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_r_reson.Location = new System.Drawing.Point(222, 563);
            this.txt_r_reson.MaxLength = 199;
            this.txt_r_reson.Name = "txt_r_reson";
            this.txt_r_reson.Size = new System.Drawing.Size(748, 23);
            this.txt_r_reson.TabIndex = 1260;
            // 
            // lbl_r_reson
            // 
            this.lbl_r_reson.AutoSize = true;
            this.lbl_r_reson.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_r_reson.ForeColor = System.Drawing.Color.Navy;
            this.lbl_r_reson.Location = new System.Drawing.Point(147, 568);
            this.lbl_r_reson.Name = "lbl_r_reson";
            this.lbl_r_reson.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_r_reson.Size = new System.Drawing.Size(70, 14);
            this.lbl_r_reson.TabIndex = 1259;
            this.lbl_r_reson.Text = "سبب الرد :";
            // 
            // txt_s_note
            // 
            this.txt_s_note.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txt_s_note.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_s_note.Location = new System.Drawing.Point(224, 378);
            this.txt_s_note.MaxLength = 99;
            this.txt_s_note.Name = "txt_s_note";
            this.txt_s_note.ReadOnly = true;
            this.txt_s_note.Size = new System.Drawing.Size(746, 23);
            this.txt_s_note.TabIndex = 1261;
            // 
            // txt_s_reson
            // 
            this.txt_s_reson.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txt_s_reson.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_s_reson.Location = new System.Drawing.Point(225, 404);
            this.txt_s_reson.MaxLength = 99;
            this.txt_s_reson.Name = "txt_s_reson";
            this.txt_s_reson.Size = new System.Drawing.Size(745, 23);
            this.txt_s_reson.TabIndex = 1262;
            // 
            // lbl_s_note
            // 
            this.lbl_s_note.AutoSize = true;
            this.lbl_s_note.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_s_note.ForeColor = System.Drawing.Color.Navy;
            this.lbl_s_note.Location = new System.Drawing.Point(109, 381);
            this.lbl_s_note.Name = "lbl_s_note";
            this.lbl_s_note.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_s_note.Size = new System.Drawing.Size(114, 14);
            this.lbl_s_note.TabIndex = 1263;
            this.lbl_s_note.Text = "ملاحظات المرسل :";
            // 
            // lbl_s_reson
            // 
            this.lbl_s_reson.AutoSize = true;
            this.lbl_s_reson.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_s_reson.ForeColor = System.Drawing.Color.Navy;
            this.lbl_s_reson.Location = new System.Drawing.Point(152, 408);
            this.lbl_s_reson.Name = "lbl_s_reson";
            this.lbl_s_reson.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_s_reson.Size = new System.Drawing.Size(70, 14);
            this.lbl_s_reson.TabIndex = 1264;
            this.lbl_s_reson.Text = "سبب الرد :";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-63, 430);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1042, 1);
            this.flowLayoutPanel2.TabIndex = 1265;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-36, 591);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel3.TabIndex = 1266;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-38, 649);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 1267;
            // 
            // Txt_Rbirth_Date
            // 
            this.Txt_Rbirth_Date.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Rbirth_Date.DateSeperator = '/';
            this.Txt_Rbirth_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rbirth_Date.Location = new System.Drawing.Point(225, 485);
            this.Txt_Rbirth_Date.Mask = "0000/00/00";
            this.Txt_Rbirth_Date.Name = "Txt_Rbirth_Date";
            this.Txt_Rbirth_Date.PromptChar = ' ';
            this.Txt_Rbirth_Date.ReadOnly = true;
            this.Txt_Rbirth_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Rbirth_Date.Size = new System.Drawing.Size(165, 23);
            this.Txt_Rbirth_Date.TabIndex = 933;
            this.Txt_Rbirth_Date.Text = "00000000";
            this.Txt_Rbirth_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txt_Rbirth_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Doc_S_Date
            // 
            this.Txt_Doc_S_Date.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Doc_S_Date.DateSeperator = '/';
            this.Txt_Doc_S_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_S_Date.Location = new System.Drawing.Point(711, 301);
            this.Txt_Doc_S_Date.Mask = "0000/00/00";
            this.Txt_Doc_S_Date.Name = "Txt_Doc_S_Date";
            this.Txt_Doc_S_Date.PromptChar = ' ';
            this.Txt_Doc_S_Date.ReadOnly = true;
            this.Txt_Doc_S_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Doc_S_Date.Size = new System.Drawing.Size(164, 23);
            this.Txt_Doc_S_Date.TabIndex = 18;
            this.Txt_Doc_S_Date.Text = "00000000";
            this.Txt_Doc_S_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txt_Doc_S_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Txt_Sbirth_Date
            // 
            this.Txt_Sbirth_Date.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Sbirth_Date.DateSeperator = '/';
            this.Txt_Sbirth_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sbirth_Date.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Txt_Sbirth_Date.Location = new System.Drawing.Point(226, 276);
            this.Txt_Sbirth_Date.Mask = "0000/00/00";
            this.Txt_Sbirth_Date.Name = "Txt_Sbirth_Date";
            this.Txt_Sbirth_Date.PromptChar = ' ';
            this.Txt_Sbirth_Date.ReadOnly = true;
            this.Txt_Sbirth_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Sbirth_Date.Size = new System.Drawing.Size(167, 23);
            this.Txt_Sbirth_Date.TabIndex = 15;
            this.Txt_Sbirth_Date.Text = "00000000";
            this.Txt_Sbirth_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txt_Sbirth_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Return_out_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 679);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.lbl_s_reson);
            this.Controls.Add(this.lbl_s_note);
            this.Controls.Add(this.txt_s_reson);
            this.Controls.Add(this.txt_s_note);
            this.Controls.Add(this.txt_r_reson);
            this.Controls.Add(this.lbl_r_reson);
            this.Controls.Add(this.cbo_order_type);
            this.Controls.Add(this.txt_srch);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.Controls.Add(this.CboCust_Id);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.Txt_S_Doc_type);
            this.Controls.Add(this.Txt_S_City);
            this.Controls.Add(this.Txt_S_Nat_name);
            this.Controls.Add(this.Txt_Rbirth_Date);
            this.Controls.Add(this.Txt_Doc_S_Date);
            this.Controls.Add(this.Txt_Sbirth_Date);
            this.Controls.Add(this.Txt_Note);
            this.Controls.Add(this.Txt_T_Purpose);
            this.Controls.Add(this.Txt_R_Addres);
            this.Controls.Add(this.Txt_R_job);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.Txt_Relship);
            this.Controls.Add(this.Txt_S_Addres);
            this.Controls.Add(this.Txt_source_money);
            this.Controls.Add(this.Txt_S_job);
            this.Controls.Add(this.Txt_Doc_S_Issue);
            this.Controls.Add(this.Txt_S_Doc_No);
            this.Controls.Add(this.Txt_S_Phone);
            this.Controls.Add(this.Txt_S_Name);
            this.Controls.Add(this.Txt_Rem_No);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.lbl_r_note);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Grd_Return);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cmb_oper_type);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Return_out_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "503";
            this.Text = "رد الحوالات";
            this.Load += new System.EventHandler(this.Returen_Delivery_Rem_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Return)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.ComboBox Cmb_oper_type;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView Grd_Return;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lbl_r_note;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.TextBox Txt_Rem_No;
        private System.Windows.Forms.TextBox Txt_S_Name;
        private System.Windows.Forms.TextBox Txt_S_Phone;
        private System.Windows.Forms.TextBox Txt_S_Doc_No;
        private System.Windows.Forms.TextBox Txt_Doc_S_Issue;
        private System.Windows.Forms.TextBox Txt_S_job;
        private System.Windows.Forms.TextBox Txt_source_money;
        private System.Windows.Forms.TextBox Txt_S_Addres;
        private System.Windows.Forms.TextBox Txt_Relship;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_R_job;
        private System.Windows.Forms.TextBox Txt_R_Addres;
        private System.Windows.Forms.TextBox Txt_T_Purpose;
        private System.Windows.Forms.TextBox Txt_Note;
        private MyDateTextBox Txt_Sbirth_Date;
        private MyDateTextBox Txt_Doc_S_Date;
        private MyDateTextBox Txt_Rbirth_Date;
        private System.Windows.Forms.TextBox Txt_S_Nat_name;
        private System.Windows.Forms.TextBox Txt_S_City;
        private System.Windows.Forms.TextBox Txt_S_Doc_type;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CboCust_Id;
        private System.Windows.Forms.ComboBox cbo_order_type;
        private System.Windows.Forms.Label txt_srch;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox txt_r_reson;
        private System.Windows.Forms.Label lbl_r_reson;
        private System.Windows.Forms.TextBox txt_s_note;
        private System.Windows.Forms.TextBox txt_s_reson;
        private System.Windows.Forms.Label lbl_s_note;
        private System.Windows.Forms.Label lbl_s_reson;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
    }
}