﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class View_Remittence_Rem : Form
    {


        string @format = "dd/MM/yyyy";
        BindingSource Bs_Viewrem = new BindingSource();
        BindingSource binding_log = new BindingSource();
        BindingSource binding = new BindingSource();
        DataTable DtView_Rpt = new DataTable();
        DataTable Rpt_dt = new DataTable();
        DataTable DtView_dt = new DataTable();
        DataTable Dt = new DataTable();
        string User = "";
        string _Date = "";
        string local_Cur = "";
        int Oper_Id = 0;
        string forgin_Cur = "";
        string Vo_No = "";
        string case_id = "";
        DataTable ddddd = new DataTable();
        bool Change = false;
        double Total_Amount = 0;
        string term = "";
        string Cur_ToWord = "";
        string local_ACur = "";
        string forgin_ACur = "";
        string frm_id = "main";
        string inrec_Date = "";
        string toinrec_Date = "";
        int R_type_id=0;
        string Cur_ToEWord = "";
        int int_nrecdate = 0;
        int cust_id_reveal = 0;
        int cust_id_online = 0; 

        public View_Remittence_Rem()
        {
            InitializeComponent();
            Grdrec_rem.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), new TextBox(), new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
        }

        private void Grdrec_rem_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {

                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtScity.DataBindings.Clear();
                Txt_S_Birth.DataBindings.Clear();
                //Txt_S_Birth_Place.DataBindings.Clear();
                Txt_s_job.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                Txt_S_doc_type.DataBindings.Clear();
                Txt_S_doc_no.DataBindings.Clear();
                Txt_S_doc_Date.DataBindings.Clear();
                Txt_S_doc_issue.DataBindings.Clear();
                Txt_Relionship.DataBindings.Clear();
                Txt_Soruce_money.DataBindings.Clear();
                Txt_R_Name.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                Txt_R_Nat.DataBindings.Clear();
                Txt_R_City.DataBindings.Clear();
                Txt_r_birthdate.DataBindings.Clear();
                // Txt_R_Birth_Place.DataBindings.Clear();
                Txt_r_job.DataBindings.Clear();
                Txt_R_address.DataBindings.Clear();
                //Txt_R_doc_type.DataBindings.Clear();
                //Txt_R_doc_no.DataBindings.Clear();
                //Txt_R_doc_date.DataBindings.Clear();
                // Txt_r_doc_issue.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                Txt_Discreption.DataBindings.Clear();

                Txt_S_details_job.DataBindings.Clear();
                Txt_R_details_job.DataBindings.Clear();
                Txt_R_notes.DataBindings.Clear();
                txt_purpose_rec.DataBindings.Clear();
                Txt_R_Relation.DataBindings.Clear();




                //CboR_cur_name.DataBindings.Clear();
                //Txtreccount.DataBindings.Clear();
                //Txttotal_amuont.DataBindings.Clear();
                TxtS_name.DataBindings.Add("Text", Bs_Viewrem, "S_name");
                TxtS_phone.DataBindings.Add("Text", Bs_Viewrem, "S_phone");
                Txts_nat.DataBindings.Add("Text", Bs_Viewrem, "snat_name");
                TxtScity.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "sa_city_aname" : "sa_city_ename");
                Txt_S_Birth.DataBindings.Add("Text", Bs_Viewrem, "sbirth_date");
                // Txt_S_Birth_Place.DataBindings.Add("Text", Bs_rem, "SBirth_Place");
                Txt_s_job.DataBindings.Add("Text", Bs_Viewrem, "s_job");
                TxtS_address.DataBindings.Add("Text", Bs_Viewrem, "S_address");
                Txt_S_doc_type.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "FRM_ADOC_NA" : "frm_Edoc_na");
                Txt_S_doc_no.DataBindings.Add("Text", Bs_Viewrem, "S_doc_no");
                Txt_S_doc_Date.DataBindings.Add("Text", Bs_Viewrem, "S_doc_ida");
                Txt_S_doc_issue.DataBindings.Add("Text", Bs_Viewrem, "S_doc_issue");
                Txt_Relionship.DataBindings.Add("Text", Bs_Viewrem, "Relation_S_R");
                Txt_Soruce_money.DataBindings.Add("Text", Bs_Viewrem, "Source_money");
                Txt_R_Name.DataBindings.Add("Text", Bs_Viewrem, "r_name");
                Txt_R_Phone.DataBindings.Add("Text", Bs_Viewrem, "R_phone");
                Txt_R_Nat.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "Rnat_name" : "renat_name");
                Txt_R_City.DataBindings.Add("Text", Bs_Viewrem, connection.Lang_id == 1 ? "rcity_name" : "recity_name");
                Txt_r_birthdate.DataBindings.Add("Text", Bs_Viewrem, "rbirth_date");
                // Txt_R_Birth_Place.DataBindings.Add("Text", Bs_rem, "TBirth_Place");
                Txt_r_job.DataBindings.Add("Text", Bs_Viewrem, "r_job");
                Txt_R_address.DataBindings.Add("Text", Bs_Viewrem, "r_address");
                //Txt_R_doc_type.DataBindings.Add("Text", Bs_rem, connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA");
                //Txt_R_doc_no.DataBindings.Add("Text", Bs_rem, "r_doc_no");
                //Txt_R_doc_date.DataBindings.Add("Text", Bs_rem, "r_doc_ida");
                //Txt_r_doc_issue.DataBindings.Add("Text", Bs_rem, "r_doc_issue");
                Txt_Purpose.DataBindings.Add("Text", Bs_Viewrem, "T_purpose");
                Txt_Discreption.DataBindings.Add("Text", Bs_Viewrem, "s_notes");

                Txt_S_details_job.DataBindings.Add("Text", Bs_Viewrem, "s_details_job");
                Txt_R_details_job.DataBindings.Add("Text", Bs_Viewrem, "r_details_job");
                Txt_R_notes.DataBindings.Add("Text", Bs_Viewrem, "r_notes");
                txt_purpose_rec.DataBindings.Add("Text", Bs_Viewrem, "R_T_purpose");
                Txt_R_Relation.DataBindings.Add("Text", Bs_Viewrem, "r_Relation");



                string rate_online = ((DataRowView)Bs_Viewrem.Current).Row["rate_online"].ToString();
                string rate_online_amount = ((DataRowView)Bs_Viewrem.Current).Row["rate_online_amount"].ToString();
                double R_amount = Convert.ToDouble(((DataRowView)Bs_Viewrem.Current).Row["R_amount"]);
                string R_Cur_name = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "R_Cur_name" : "Re_Cur_name"].ToString();
                string PR_CUR_name = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "PR_CUR_name" : "PRe_CUR_name"].ToString();
                string t_city_aname = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "t_city_aname" : "t_city_ename"].ToString();
                string scity_name = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "scity_name" : "secity_name"].ToString();
                string r_name = ((DataRowView)Bs_Viewrem.Current).Row["r_name"].ToString();
                string ACASE_NA = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "ACASE_NA" : "eCASE_NA"].ToString();
                DateTime Case_Date = Convert.ToDateTime(((DataRowView)Bs_Viewrem.Current).Row["Case_Date"]);
                string ver_flag_name = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "ver_flag_name" : "ver_flag_ename"].ToString();
                DateTime C_DATE = Convert.ToDateTime(((DataRowView)Bs_Viewrem.Current).Row["C_DATE"]);
                string user_update = ((DataRowView)Bs_Viewrem.Current).Row["user_update"].ToString();
                string S_phone = ((DataRowView)Bs_Viewrem.Current).Row["S_phone"].ToString();
                string snat_name = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "snat_name" : "senat_name"].ToString();
                string sa_city_aname = ((DataRowView)Bs_Viewrem.Current).Row["sa_city_aname"].ToString();
                string sa_city_ename = ((DataRowView)Bs_Viewrem.Current).Row["sa_city_ename"].ToString();
                DateTime sbirth_date = Convert.ToDateTime(((DataRowView)Bs_Viewrem.Current).Row["sbirth_date"]);
                string s_job = ((DataRowView)Bs_Viewrem.Current).Row["s_job"].ToString();
                string S_address = ((DataRowView)Bs_Viewrem.Current).Row["S_address"].ToString();
                string FRM_ADOC_NA = ((DataRowView)Bs_Viewrem.Current).Row["FRM_ADOC_NA"].ToString();
                string frm_Edoc_na = ((DataRowView)Bs_Viewrem.Current).Row["frm_Edoc_na"].ToString();
                string S_doc_no = ((DataRowView)Bs_Viewrem.Current).Row["S_doc_no"].ToString();
                DateTime S_doc_ida = Convert.ToDateTime(((DataRowView)Bs_Viewrem.Current).Row["S_doc_ida"]);
                string S_doc_issue = ((DataRowView)Bs_Viewrem.Current).Row["S_doc_issue"].ToString();
                string Relation_S_R = ((DataRowView)Bs_Viewrem.Current).Row["Relation_S_R"].ToString();
                string Source_money = ((DataRowView)Bs_Viewrem.Current).Row["Source_money"].ToString();
                string R_phone = ((DataRowView)Bs_Viewrem.Current).Row["R_phone"].ToString();
                string Rnat_name = ((DataRowView)Bs_Viewrem.Current).Row["Rnat_name"].ToString();
                string renat_name = ((DataRowView)Bs_Viewrem.Current).Row["renat_name"].ToString();
                string rcity_name = ((DataRowView)Bs_Viewrem.Current).Row["rcity_name"].ToString();
                string recity_name = ((DataRowView)Bs_Viewrem.Current).Row["recity_name"].ToString();
                string rbirth_date = ((DataRowView)Bs_Viewrem.Current).Row["rbirth_date"].ToString();
                string r_job = ((DataRowView)Bs_Viewrem.Current).Row["r_job"].ToString();
                string r_address = ((DataRowView)Bs_Viewrem.Current).Row["r_address"].ToString();
                string T_purpose = ((DataRowView)Bs_Viewrem.Current).Row["T_purpose"].ToString();
                string sfcoun_name = ((DataRowView)Bs_Viewrem.Current).Row["sfcoun_name"].ToString();
                DateTime S_doc_eda = Convert.ToDateTime(((DataRowView)Bs_Viewrem.Current).Row["S_doc_eda"]);
                string rcoun_name = ((DataRowView)Bs_Viewrem.Current).Row[connection.Lang_id == 1 ? "rcoun_name" : "recoun_name"].ToString();
              
                Int16 R_cur_id = Convert.ToInt16(((DataRowView)Bs_Viewrem.Current).Row["R_cur_id"]);
                Int32 Code_Rem = Convert.ToInt32(((DataRowView)Bs_Viewrem.Current).Row["Code_Rem"]);
                Int32 vo_no = Convert.ToInt32(((DataRowView)Bs_Viewrem.Current).Row["vo_no"]);
                string Rem_No = ((DataRowView)Bs_Viewrem.Current).Row["Rem_No"].ToString();
                string t_coun_aname = ((DataRowView)Bs_Viewrem.Current).Row["t_coun_aname"].ToString();
                int s_cust_id = Convert.ToInt16(((DataRowView)Bs_Viewrem.Current).Row["s_cust_id"]);
                int d_cust_id = Convert.ToInt16(((DataRowView)Bs_Viewrem.Current).Row["d_cust_id"]);
                int nrec_date1 = Convert.ToInt32(((DataRowView)Bs_Viewrem.Current).Row["nrec_date"]);
                


                DtView_dt.Clear();
                DtView_dt.Rows.Add(TxtS_name.Text, Rem_No, rate_online, rate_online_amount, R_amount,
                                    R_Cur_name, PR_CUR_name, t_city_aname, scity_name, r_name, ACASE_NA, Case_Date,
                                    ver_flag_name, C_DATE, user_update, S_phone, snat_name, sa_city_aname, sa_city_ename,
                                    sbirth_date, s_job, S_address, FRM_ADOC_NA, frm_Edoc_na, S_doc_no, S_doc_ida, S_doc_issue,
                                    Relation_S_R, Source_money, R_phone, Rnat_name, renat_name, rcity_name, recity_name, rbirth_date
                                    , r_job, r_address, T_purpose, sfcoun_name, S_doc_eda, rcoun_name, R_cur_id, Code_Rem, vo_no, s_cust_id, d_cust_id, nrec_date1
                                );

                case_id = ((DataRowView)Bs_Viewrem.Current).Row["case_id"].ToString();

                if (d_cust_id != connection.Cust_online_Id && s_cust_id != connection.Cust_online_Id)
                {
                    Btn_Rpt.Enabled = false;
                }
                else
                {

                    if (d_cust_id == connection.Cust_online_Id && s_cust_id != connection.Cust_online_Id)
                    {
                        if (case_id == "2" || case_id == "52")
                        {
                            Btn_Rpt.Enabled = true;
                        }
                        else
                        {
                            Btn_Rpt.Enabled = false;
                        }
                    }

                    if (d_cust_id != connection.Cust_online_Id && s_cust_id == connection.Cust_online_Id)
                    {
                        if (case_id == "101" || case_id == "151")
                        {
                            Btn_Rpt.Enabled = true;
                        }
                        else
                        {
                            Btn_Rpt.Enabled = false;
                        }
                    }


                    if (d_cust_id == connection.Cust_online_Id && s_cust_id == connection.Cust_online_Id)
                    {
                        Btn_Rpt.Enabled = true;

                    }
                }
            }
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
           
            string[] Tbl = { "DtView_Rpt1", "DtView_dt" };
            foreach (string str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(str))
                {
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[str]);
                }
            }

                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[view_rem_web_1]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@rem_no", Txt_Rem_No.Text.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@R_name", txt_rname.Text.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@S_name", txt_sname.Text.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@R_amount",Convert.ToDecimal(txt_ramont.Text));
                    connection.SQLCMD.Parameters.AddWithValue("@Nrec_Dare", inrec_Date =="" ? 0 :Convert.ToInt32(inrec_Date));
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Dare", toinrec_Date == "" ? 0 : Convert.ToInt32(toinrec_Date));
                    connection.SQLCMD.Parameters.AddWithValue("@cust_online_id", cust_id_online);
                    connection.SQLCMD.Parameters.AddWithValue("@R_type_id", R_type_id);
                    connection.SQLCMD.Parameters.AddWithValue("@Chk_Hst_flag", Chk_Hst_Info.Checked);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 50).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "DtView_Rpt1");
                    obj.Close();
                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Dispose();
                        return;
                    }

                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    inrec_Date = "";
                    toinrec_Date = "";
                }

                catch (Exception _Err)
                {
                    connection.SQLCMD.Parameters.Clear();
                    MyErrorHandler.ExceptionHandler(_Err);
                }
            
               
                if (connection.SQLDS.Tables["DtView_Rpt1"].Rows.Count > 0)
                {
                    Bs_Viewrem.DataSource = connection.SQLDS.Tables["DtView_Rpt1"];
                    Change = false;
                    Grdrec_rem.DataSource = Bs_Viewrem;
                    Change = true;
                    Grdrec_rem_SelectionChanged(null, null);
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "no Rem. for this Condition" : "لا توجد حوالة تحقق الشروط", MyGeneral_Lib.LblCap);
                    Grdrec_rem.DataSource = new BindingSource();
                    TxtS_name.Text = "";
                    TxtS_phone.Text="";
                    Txts_nat.Text="";
                    TxtScity.Text="";
                    Txt_S_Birth.Text = "0000/00/00";
                    Txt_s_job.Text = "";
                    TxtS_address.Text = "";
                    Txt_S_doc_type.Text = "";
                    Txt_S_doc_no.Text = "";
                    Txt_S_doc_Date.Text = "0000/00/00";
                    Txt_S_doc_issue.Text = "";
                    Txt_Relionship.Text = "";
                    Txt_Soruce_money.Text = "";
                    Txt_R_Name.Text = "";
                    Txt_R_Phone.Text = "";
                    Txt_R_Nat.Text = "";
                    Txt_R_City.Text = "";
                    Txt_r_birthdate.Text = "0000/00/00";
                    Txt_r_job.Text = "";
                    Txt_R_address.Text = "";
                    Txt_Purpose.Text = "";
                    Txt_Discreption.Text = "";
                    Txt_S_details_job.Text = "";
                    Txt_R_details_job.Text = "";
                    Txt_R_notes.Text = "";
                    txt_purpose_rec.Text = "";
                    Txt_R_Relation.Text = "";


                  
                    return;
                }
        }
        private void View_Remittence_Rem_Load(object sender, EventArgs e)
        {
           
            //Users_Login AddFrm = new Users_Login();
            //this.Visible = false;
            //AddFrm.ShowDialog(this);
            //this.Visible = true;
            //if (Users_Login.ISCancleflag != 0)
            //{ this.Close(); }

           
            //binding_log.DataSource = connection.SQLDS.Tables["Sub_cust_tbl"];
            //Cbo_city.DataSource = binding_log.DataSource;
            //Cbo_city.ValueMember = "city_id";
            //Cbo_city.DisplayMember = connection.Lang_id == 1 ? "acity_name" : "ecity_name";
            string SqlTxt = " Select  CUST_ID ,cust_online_id  From  TERMINALS where t_id = " + connection.T_ID;
            connection.SqlExec(SqlTxt, "Cust_tbl");


            cust_id_reveal = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);
            cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["cust_online_id"]);
            //--------------------------------------- لتعبئة الكومبو المدينة
            connection.SqlExec("exec Get_information_inrem " + cust_id_reveal, "Get_info_Tbl");

             binding.DataSource = connection.SQLDS.Tables["Get_info_Tbl"].Select("CITY_ID <>" + 0).CopyToDataTable();
             Cbo_city.DataSource = binding;
             Cbo_city.ValueMember = "CITY_ID";
             Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";
            //--------------------------------------
             string SqlTxt1 = " exec Full_information_Web " + connection.Lang_id + "," + 0 + "," + cust_id_reveal;
            connection.SqlExec(SqlTxt1, "Full_information_tab");

            //binding.DataSource = connection.SQLDS.Tables["Full_information_tab"].Select("cit_id = " + connection.city_id_online).CopyToDataTable();
            //Cbo_city.DataSource = binding;
            //Cbo_city.ValueMember = "Cit_ID";
            //Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "eCity_Name";

            Create_Table();
            Txt_Nrec_date_ValueChanged(null, null);
            Txt_ToNrec_date_ValueChanged(null, null);
            Cmb_oper_type.SelectedIndex = 0;
            if (connection.Lang_id == 2)
            {
                Cmb_oper_type.Items[0] = "out remittance";
                Cmb_oper_type.Items[1] = "in remittance";

                Column8.DataPropertyName = "t_city_ename";
                Column5.DataPropertyName = "eCASE_NA";
                Column2.DataPropertyName = "Re_CUR_name";
                Column4.DataPropertyName = "PRe_CUR_name";
                Column9.DataPropertyName = "secity_name";

                Column17.DataPropertyName = "rfrm_Edoc_na";
                Column22.DataPropertyName = "D_Ecust_name";

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["DtView_Rpt1"].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DataTable Exp_Dt = connection.SQLDS.Tables["DtView_Rpt1"].DefaultView.ToTable(false, "rem_no", "S_name", "rate_online", "rate_online_amount", "R_amount",
                    connection.Lang_id == 1 ? "R_Cur_name" : "Re_Cur_name", connection.Lang_id == 1 ? "PR_CUR_name" : "PRe_CUR_name", connection.Lang_id == 1 ? "t_city_aname" : "t_city_ename", connection.Lang_id == 1 ? "scity_name" : "secity_name",
                    "r_name", "r_address" ,"r_job" ,connection.Lang_id == 1 ? "rfrm_adoc_na" : "rfrm_Edoc_na", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue",connection.Lang_id == 1 ?  "D_Acust_name" : "D_Ecust_name", connection.Lang_id == 1 ? "ACASE_NA" : "eCASE_NA", "Case_Date", connection.Lang_id == 1 ? "ver_flag_name" : "ver_flag_ename", "C_DATE").Select().CopyToDataTable();

                DataTable[] _EXP_DT = { Exp_Dt };
                DataGridView[] Export_GRD = { Grdrec_rem };
                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, DT, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف حالة الحوالة" : "State Remittences Reveal ");
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
            }
            else
            {

                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات" : " No remittences", MyGeneral_Lib.LblCap);
                return;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Create_Table()
        {
            string[] Column =
            {
             
             "s_name" ,"rem_no","rate_online","rate_online_amount","R_amount" ,"R_Cur_name","PR_CUR_name","t_city_aname",
              "scity_name","r_name","ACASE_NA" ,"Case_Date" ,"ver_flag_name" ,"C_DATE","user_update" ,"S_phone" ,"snat_name",
             "sa_city_aname","sa_city_ename","sbirth_date","s_job" ,"S_address" , "FRM_ADOC_NA","frm_Edoc_na" ,"S_doc_no",
              "S_doc_ida","S_doc_issue" ,"Relation_S_R","Source_money", "R_phone" ,"Rnat_name", "renat_name" ,"rcity_name",
              "recity_name","rbirth_date","r_job" ,"r_address" ,"T_purpose" ,"sfcoun_name","S_doc_eda","rcoun_name","R_cur_id " ,"Code_Rem","vo_no",
              "s_cust_id","d_cust_id","nrec_date"
            };
            string[] DType =
            {
                "System.String","System.String","System.Decimal","System.Decimal", "System.Double", "System.String", "System.String","System.String",
                "System.String","System.String", "System.String", "System.DateTime","System.String","System.DateTime","System.String","System.String", "System.String", 
                "System.String", "System.String","System.DateTime","System.String","System.String", "System.String","System.String","System.String",
                "System.DateTime","System.String","System.String", "System.String","System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.DateTime","System.String","System.Int16","System.Int32","System.Int32"
                ,"System.Int32","System.Int32","System.Int32"       
            };
            
            DtView_dt = CustomControls.Custom_DataTable("DtView_dt", Column, DType);
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            string Rem_no = ((DataRowView)Bs_Viewrem.Current).Row["Rem_no"].ToString();
            string SqlTxt = "Exec Report_Rem_Information " + "'" + Rem_no + "'";
            connection.SqlExec(SqlTxt, "Rem_Info");
           
            User = connection.User_Name;
            Int32 forgin = Convert.ToInt32(((DataRowView)Bs_Viewrem.Current).Row["R_Cur_Id"]);

            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            local_Cur = connection.SqlExec("select Cur_Code from cur_tbl "
               + "where Cur_id = " + connection.Loc_Cur_Id, "Cur_loc_tbl").Rows[0]["Cur_Code"].ToString();

            forgin_Cur = connection.SqlExec("select Cur_Code from cur_tbl "
           + "where Cur_id = " + forgin, "Cur_loc_tbl").Rows[0]["Cur_Code"].ToString();

            local_ACur = connection.SqlExec("select Cur_ANAME from cur_tbl  "
                + "where Cur_id = " + (connection.Loc_Cur_Id), "Cur_loc_tbl").Rows[0]["Cur_ANAME"].ToString();
            string local_ECur = connection.SqlExec("select Cur_ENAME from cur_tbl  "
                  + "where Cur_id = " + (connection.Loc_Cur_Id), "Cur_loc_tbl").Rows[0]["Cur_ENAME"].ToString();
            forgin_ACur = connection.SqlExec("select Cur_ANAME from cur_tbl "
          + "where Cur_id = " + forgin, "Cur_loc_tbl").Rows[0]["Cur_ANAME"].ToString();
            string forgin_ECur = connection.SqlExec("select Cur_ENAME from cur_tbl "
             + "where Cur_id = " + forgin, "Cur_loc_tbl").Rows[0]["Cur_ENAME"].ToString();
            Vo_No = ((DataRowView)Bs_Viewrem.Current).Row["Vo_No"].ToString();
            case_id = ((DataRowView)Bs_Viewrem.Current).Row["case_id"].ToString();


            Total_Amount = Convert.ToDouble(((DataRowView)Bs_Viewrem.Current).Row["R_amount"]);
            int R_Cur_Id = Convert.ToInt16(((DataRowView)Bs_Viewrem.Current).Row["R_Cur_Id"]);
            int s_cust_id = Convert.ToInt16(((DataRowView)Bs_Viewrem.Current).Row["s_cust_id"]);
            int d_cust_id = Convert.ToInt16(((DataRowView)Bs_Viewrem.Current).Row["d_cust_id"]);

            ToWord toWord = new ToWord(Total_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            term = TxtTerm_Name.Text;

            if (case_id == "2" && d_cust_id == connection.Cust_online_Id && s_cust_id != connection.Cust_online_Id)
            {
                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                {
                    
                    Oper_Id = 10;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                    Print_Rpt(null, null);
                }
                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                {
                    Oper_Id = 8;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                    Print_Rpt(null, null);
                }
            }

            if (case_id == "52" && d_cust_id == connection.Cust_online_Id && s_cust_id != connection.Cust_online_Id)
            {

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                {
                    
                    Oper_Id = 5;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                    Print_Rpt(null, null);
                }

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                {
                    Oper_Id = 4;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                    Print_Rpt(null, null);
                }
            }


            if (case_id == "101" && s_cust_id == connection.Cust_online_Id && d_cust_id != connection.Cust_online_Id)
            {

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)// (R_Cur_Id != connection.Loc_Cur_Id)
                {
                    
                    Oper_Id = 6;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                    Print_Rpt(null, null);
                }

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                {

                    Oper_Id = 3;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                    Print_Rpt(null, null);

                    
                }
            }

            if (case_id == "151" && s_cust_id == connection.Cust_online_Id && d_cust_id != connection.Cust_online_Id)
            {

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                {
              
                    Oper_Id = 11;

                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_Cur + " not else.)- ";
                    Print_Rpt(null, null);
                }

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                {
                    Oper_Id = 9;
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                    Print_Rpt(null, null);
                   
                }
            }
            // Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";

            if (d_cust_id == connection.Cust_online_Id && s_cust_id == connection.Cust_online_Id)
            {
                if (case_id == "2")
                {

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                    {
                        
                        Oper_Id = 10;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                    {
                        Oper_Id = 8;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }
                }

                if (case_id == "52")
                {

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                    {
                     
                        Oper_Id = 5;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                    {
                        Oper_Id = 4;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }
                }


                if (case_id == "101")
                {
                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                    {
                 
                        Oper_Id = 6;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                        Print_Rpt(null, null);

                    }

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)// (R_Cur_Id == connection.Loc_Cur_Id)
                    {
                        Oper_Id = 3;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }
                }

                if (case_id == "151")
                {
                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) != 0)//(R_Cur_Id != connection.Loc_Cur_Id)
                    {
                     
                        Oper_Id = 11;

                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"].ToString()) == 0)//(R_Cur_Id == connection.Loc_Cur_Id)
                    {
                        Oper_Id = 9;
                        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_ACur + " لاغير)- ";
                        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur + " not else.)- ";
                        Print_Rpt(null, null);
                    }
                }
            }
        }

        private void Print_Rpt(object sender, EventArgs e)
        {

            View_Rem_State ObjRpt = new View_Rem_State();
            View_Rem_State_eng ObjRptEng = new View_Rem_State_eng();
            try
            {
                string phone = "";
                phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
                case_id = ((DataRowView)Bs_Viewrem.Current).Row["case_id"].ToString();
                string t_coun_aname = ((DataRowView)Bs_Viewrem.Current).Row["t_coun_aname"].ToString();
                int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("_Date", _Date);
                Dt_Param.Rows.Add("local_Cur", local_Cur);
                Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                Dt_Param.Rows.Add("User", User);
                Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                Dt_Param.Rows.Add("Total_Amount", Total_Amount);
                Dt_Param.Rows.Add("term", term);
                Dt_Param.Rows.Add("frm_id", frm_id);
                Dt_Param.Rows.Add("Vo_No", Vo_No);
                Dt_Param.Rows.Add("phone", phone);
                Dt_Param.Rows.Add("t_coun_aname", t_coun_aname);
                Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                DtView_dt.TableName = "DtView_dt";
                connection.SQLDS.Tables.Add(DtView_dt);
                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["DtView_dt"]);
            }
            catch { }
           
        }

        private void View_Remittence_Rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Str = { "DtView_dt", "DtView_Rpt1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void Txt_Nrec_date_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_Nrec_date.Checked == true)
            {
                Txt_Nrec_date.Format = DateTimePickerFormat.Custom;
                Txt_Nrec_date.CustomFormat = @format;
                inrec_Date = Txt_Nrec_date.Value.ToString("yyyyMMdd");
            }
            else
            {
                Txt_Nrec_date.Format = DateTimePickerFormat.Custom;
                Txt_Nrec_date.CustomFormat = " ";
                inrec_Date = "";
            }

           
        }

        private void Cmb_oper_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_oper_type.SelectedIndex==1)
            {
                R_type_id=1;
            }
            if (Cmb_oper_type.SelectedIndex==0)
            {
                R_type_id=2;
            }

        }

        private void Txt_ToNrec_date_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_ToNrec_date.Checked == true)
            {
                Txt_ToNrec_date.Format = DateTimePickerFormat.Custom;
                Txt_ToNrec_date.CustomFormat = @format;
                toinrec_Date = Txt_ToNrec_date.Value.ToString("yyyyMMdd");
            }
            else
            {
                Txt_ToNrec_date.Format = DateTimePickerFormat.Custom;
                Txt_ToNrec_date.CustomFormat = " ";
                toinrec_Date = "";
            }
        }
 
    }
}