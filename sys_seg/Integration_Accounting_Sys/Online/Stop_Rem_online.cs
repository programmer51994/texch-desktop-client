﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys 
{
    public partial class Stop_Rem_online : Form
    {
        BindingSource BS_Rem_Stop = new BindingSource();
        Int16 Flag_stop = 0;
        string Sql_Text = "";
        BindingSource  _Bs_cust1  =  new BindingSource();
        BindingSource Bs_Sub_Cust = new BindingSource();
        DataTable Dt_Cust_TBl = new DataTable();
        DataTable DT1 = new DataTable();


        public Stop_Rem_online()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            if (connection.Lang_id == 2)
            {
                cmb_Flag_stop.Items[0] = "Stop Rem.";
                cmb_Flag_stop.Items[1] = "Cancel Stop Rem.";
            
            }
        }

        private void Stop_Rem_online_Load(object sender, EventArgs e)
        {
            if (connection.T_ID != 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ايقاف الحوالة فقط من الادارة العامة" : "Only Head Office can stop the remittance", MyGeneral_Lib.LblCap);
                return;
            }

            cmb_Flag_stop.SelectedIndex = 0;
            if (connection.T_ID == 1)  // الادارة
            {

                Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as  Sub_Cust_ID , 0 as Order_type"
                    + "  From CUSTOMERS A, TERMINALS B   "
                    + "  Where A.CUST_ID = B.CUST_ID "
                    + "  And A.Cust_Flag <> 0  "

                    + " union "
                    + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID , 1 as Order_type"
                    + " From  Sub_CUSTOMERS_online A  "
                    //, Login_Cust_Online B "
                    + " Where A.Cust_state  =  1 "//فعال
                    //+ " and A.Sub_Cust_ID =  B.Cust_id" 
                    + " and A.t_id =  " + connection.T_ID
                    + " union "
                     + "select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID , 1 as Order_type"
                     + " From  Sub_CUSTOMERS_online A "
                     + " Where A.Cust_state  =  1"
                     + "  and A.t_id <>  1"
                     + " order by cust_id,Order_type desc";
            }

            _Bs_cust1.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
            cbo_order_type.SelectedIndex = 0;

            
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {


            if (Txt_Rem_no.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال رقم الحوالة" : "Please enter the Rem. number", MyGeneral_Lib.LblCap);
                this.Close();

            }

            Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);
            Int64 sub_cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["Sub_Cust_ID"]);


            if (connection.SQLDS.Tables.Contains("Stop_Rem_Online_Tbl"))
            {
                connection.SQLDS.Tables.Remove("Stop_Rem_Online_Tbl");
            }

            connection.SQLCS.Open();   
            connection.SQLCMD.CommandText = "[serch_update_stop_rem]";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_no.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@cust_id", cust_id);
            connection.SQLCMD.Parameters.AddWithValue("@Flag_stop", Flag_stop);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 50).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.AddWithValue("@sub_cust_id", sub_cust_id);
            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Stop_Rem_Online_Tbl");
            obj.Close();
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCS.Close();
                connection.SQLCMD.Dispose();
                return;
            }
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();


            if (connection.SQLDS.Tables["Stop_Rem_Online_Tbl"].Rows.Count > 0)
            {

                BS_Rem_Stop.DataSource = connection.SQLDS.Tables["Stop_Rem_Online_Tbl"];


                Txt_Rcur.DataBindings.Clear();
                Txt_PRCur.DataBindings.Clear();
                Txt_Scity.DataBindings.Clear();
                Txt_Tcity.DataBindings.Clear();
                Tot_Amount.DataBindings.Clear();
                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtScity.DataBindings.Clear();
                txt_resd_sen.DataBindings.Clear();
                Txt_S_Birth.DataBindings.Clear();
                Txt_S_Birth_Place.DataBindings.Clear();
                Txt_s_job.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                txt_purpose_sen.DataBindings.Clear();
                Txt_S_doc_type.DataBindings.Clear();
                txt_social_sen.DataBindings.Clear();
                Txt_S_doc_no.DataBindings.Clear();
                Txt_S_doc_Date.DataBindings.Clear();
                Txt_S_doc_issue.DataBindings.Clear();
                Txt_Relionship.DataBindings.Clear();
                Txt_Soruce_money.DataBindings.Clear();
                Txt_R_Name.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                Txt_R_Nat.DataBindings.Clear();
                Txt_R_City.DataBindings.Clear();
                Txt_r_birthdate.DataBindings.Clear();
                Txt_R_Birth_Place.DataBindings.Clear();
                Txt_r_job.DataBindings.Clear();
                Txt_R_address.DataBindings.Clear();
                txt_purpose_rec.DataBindings.Clear();
                Txt_R_doc_type.DataBindings.Clear();
                Txt_R_doc_no.DataBindings.Clear();
                Txt_R_doc_date.DataBindings.Clear();
                txt_social_rec.DataBindings.Clear();
                Txt_r_doc_issue.DataBindings.Clear();
                txt_resd_rec.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                Txt_Discreption.DataBindings.Clear();
                Txt_R_Relation.DataBindings.Clear();
                Txt_S_details_job.DataBindings.Clear();
                Txt_R_details_job.DataBindings.Clear();
                Txt_R_notes.DataBindings.Clear();
                 

                string s_doc_date = Convert.ToString(((DataRowView)BS_Rem_Stop.Current).Row["S_doc_ida"].ToString());
                Txt_S_doc_Date.Text = s_doc_date;

                string S_Birth = Convert.ToString(((DataRowView)BS_Rem_Stop.Current).Row["sbirth_date"].ToString());
                Txt_S_Birth.Text = S_Birth;

                string r_Birth = Convert.ToString(((DataRowView)BS_Rem_Stop.Current).Row["rbirth_date"].ToString());
                Txt_r_birthdate.Text = r_Birth;

                string R_doc_date = Convert.ToString(((DataRowView)BS_Rem_Stop.Current).Row["r_doc_ida"].ToString());
                Txt_R_doc_date.Text = R_doc_date;


                Txt_Rcur.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME");
                Txt_PRCur.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME");
                Txt_Scity.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME");
                Txt_Tcity.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME");
                Tot_Amount.DataBindings.Add("Text", BS_Rem_Stop, "R_amount");
                TxtS_name.DataBindings.Add("Text", BS_Rem_Stop, "S_name");
                TxtS_phone.DataBindings.Add("Text", BS_Rem_Stop, "S_phone");
                Txts_nat.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                TxtScity.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                txt_resd_sen.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "resd_flag_Aname" : "resd_flag_Ename");
                Txt_S_Birth.DataBindings.Add("Text", BS_Rem_Stop, "sbirth_date");
                Txt_S_Birth_Place.DataBindings.Add("Text", BS_Rem_Stop, "SBirth_Place");
                Txt_s_job.DataBindings.Add("Text", BS_Rem_Stop, "s_job");
                TxtS_address.DataBindings.Add("Text", BS_Rem_Stop, "S_address");
                txt_purpose_sen.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename");
                Txt_S_doc_type.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                txt_social_sen.DataBindings.Add("Text", BS_Rem_Stop, "S_Social_No");
                Txt_S_doc_no.DataBindings.Add("Text", BS_Rem_Stop, "S_doc_no");
                Txt_S_doc_Date.DataBindings.Add("Text", BS_Rem_Stop, "S_doc_ida");
                Txt_S_doc_issue.DataBindings.Add("Text", BS_Rem_Stop, "S_doc_issue");
                Txt_Relionship.DataBindings.Add("Text", BS_Rem_Stop, "Relation_S_R");
                Txt_Soruce_money.DataBindings.Add("Text", BS_Rem_Stop, "Source_money");
                Txt_R_Name.DataBindings.Add("Text", BS_Rem_Stop, "r_name");
                Txt_R_Phone.DataBindings.Add("Text", BS_Rem_Stop, "R_phone");
                Txt_R_Nat.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "r_A_NAT_NAME" : "r_E_NAT_NAME");
                Txt_R_City.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                Txt_r_birthdate.DataBindings.Add("Text", BS_Rem_Stop, "rbirth_date");
                Txt_R_Birth_Place.DataBindings.Add("Text", BS_Rem_Stop, "TBirth_Place");
                Txt_r_job.DataBindings.Add("Text", BS_Rem_Stop, "r_job");
                Txt_R_address.DataBindings.Add("Text", BS_Rem_Stop, "r_address");
                txt_purpose_rec.DataBindings.Add("Text", BS_Rem_Stop, "R_T_purpose");
                Txt_R_doc_type.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_EDOC_NA");
                txt_social_rec.DataBindings.Add("Text", BS_Rem_Stop, "R_Social_No");
                Txt_R_doc_no.DataBindings.Add("Text", BS_Rem_Stop, "r_doc_no");
                Txt_R_doc_date.DataBindings.Add("Text", BS_Rem_Stop, "r_doc_ida");
                Txt_r_doc_issue.DataBindings.Add("Text", BS_Rem_Stop, "r_doc_issue");
                txt_resd_rec.DataBindings.Add("Text", BS_Rem_Stop, connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename");
                Txt_Purpose.DataBindings.Add("Text", BS_Rem_Stop, "T_purpose");
                Txt_Discreption.DataBindings.Add("Text", BS_Rem_Stop, "s_notes");
                Txt_R_Relation.DataBindings.Add("Text", BS_Rem_Stop, "r_Relation");
                Txt_S_details_job.DataBindings.Add("Text", BS_Rem_Stop, "s_details_job");
                Txt_R_details_job.DataBindings.Add("Text", BS_Rem_Stop, "r_details_job");
                Txt_R_notes.DataBindings.Add("Text", BS_Rem_Stop, "r_notes");
                txt_Pres.DataBindings.Add("Text", BS_Rem_Stop, "notes_stop"); 

            }


            //else
            //{

            //    if( cmb_Flag_stop.SelectedIndex ==0)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "الحوالة موقوفة فعلا" : "Rem. is already suspended", MyGeneral_Lib.LblCap);
            //    return;
            //    }

            //  if (cmb_Flag_stop.SelectedIndex  == 1)
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "الحوالة ملغاة الايقاف فعلا" : "The Rem. actually cancels the suspension", MyGeneral_Lib.LblCap);
            //        return;
            //    }
            //}
        }

        private void Btn_Stop_rem_Click(object sender, EventArgs e)
        {
            if (Txt_Rcur.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap);
                return;
            }
            
            if (Txt_Note_Stop.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى ادخال ملاحظات ايقاف/الغاء ايقاف الحوالة" : "Please enter the stop / cancel stop notes", MyGeneral_Lib.LblCap);
                Txt_Note_Stop.Focus();
                return;
            }


            try
            {
                string Rem_no = ((DataRowView)BS_Rem_Stop.Current).Row["Rem_no"].ToString();
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[update_Stop_Rem]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Rem_no);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@user_name", connection.User_Name);
                connection.SQLCMD.Parameters.AddWithValue("@Flag_stop", Flag_stop );
                connection.SQLCMD.Parameters.AddWithValue("@Note_stop", Txt_Note_Stop.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@T_id", connection.T_ID);                                       
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Update_Stop_Rem_tbl");
                obj.Close();

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    this.Close();
                }

                MessageBox.Show(connection.Lang_id == 1 ? "تم التوقيف او الغاء التوقيف" + DateTime.Now : " Stoping or Cancle Stoping Done " + DateTime.Now, MyGeneral_Lib.LblCap);
                this.Close();
            }

            catch
            {
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();


            }
        }
   
        private void cmb_Flag_stop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_Flag_stop.SelectedIndex == 0)
            {  Flag_stop = 1 ;}
            if (cmb_Flag_stop.SelectedIndex == 1)
            { Flag_stop = 2; }
            if (cmb_Flag_stop.SelectedIndex == 2)
            { Flag_stop = 3; }


            Txt_Rcur.Text = "";
            Txt_PRCur.Text = "";
            Txt_Scity.Text = "";
            Txt_Tcity.Text = "";
            Tot_Amount.Text = "";
            TxtS_name.Text = "";
            TxtS_phone.Text = "";
            Txts_nat.Text = "";
            TxtScity.Text = "";
            txt_resd_sen.Text = "";
            Txt_S_Birth.Text = "";
            Txt_S_Birth_Place.Text = "";
            Txt_s_job.Text = "";
            TxtS_address.Text = "";
            txt_purpose_sen.Text = "";
            Txt_S_doc_type.Text = "";
            txt_social_sen.Text = "";
            Txt_S_doc_no.Text = "";
            Txt_S_doc_Date.Text = "";
            Txt_S_doc_issue.Text = "";
            Txt_Relionship.Text = "";
            Txt_Soruce_money.Text = "";
            Txt_R_Name.Text = "";
            Txt_R_Phone.Text = "";
            Txt_R_Nat.Text = "";
            Txt_R_City.Text = "";
            Txt_r_birthdate.Text = "";
            Txt_R_Birth_Place.Text = "";
            Txt_r_job.Text = "";
            Txt_R_address.Text = "";
            txt_purpose_rec.Text = "";
            Txt_R_doc_type.Text = "";
            Txt_R_doc_no.Text = "";
            Txt_R_doc_date.Text = "";
            txt_social_rec.Text = "";
            Txt_r_doc_issue.Text = "";
            txt_resd_rec.Text = "";
            Txt_Purpose.Text = "";
            Txt_Discreption.Text = "";
            Txt_R_Relation.Text = "";
            Txt_S_details_job.Text = "";
            Txt_R_details_job.Text = "";
            Txt_R_notes.Text = "";
            Txt_Note_Stop.Text = "";
        }

        private void Stop_Rem_online_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection.SQLDS.Tables.Contains("Stop_Rem_Online_Tbl"))
            {
                connection.SQLDS.Tables.Remove("Stop_Rem_Online_Tbl");
            }
               Bs_Sub_Cust = new BindingSource() ;
        }

        private void cbo_order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
         int Order_type = Convert.ToInt32(cbo_order_type.SelectedIndex);
            try
            {
                if (cbo_order_type.SelectedIndex == 0) //فروع
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select("Order_type = " + Order_type).CopyToDataTable();


                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                    }
                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }

                }

                else
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select("Order_type = " + Order_type).CopyToDataTable();

                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                    }

                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }
                }
            }
            catch { Grd_Sub_Cust.DataSource = new BindingSource(); }

            Txt_Sub_Cust.Text = "";
        }

        private void Txt_Sub_Cust_TextChanged(object sender, EventArgs e)
        {
            Get_details_Info();
        }

        private void Get_details_Info()
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);

                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "(Acust_name like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " Sub_Cust_ID > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }

        private void bon_end_Click(object sender, EventArgs e)
        {
            this.Close();
            Grd_Sub_Cust.DataSource = new BindingSource();
          
        }
       
    }
}