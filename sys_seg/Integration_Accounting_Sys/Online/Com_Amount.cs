﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Com_Amount : Form
    {

        public static string Remmitance_Amount1 = "";
        public static string Remmitance_Com1 = "";
        public static int Remmitance_cur1 = 0;
        public static int out_rem_cur1 = 0;
        public static int com_cur1 = 0;
        //string Remmitance_cur

        public Com_Amount(string Remmitance_Amount, string Remmitance_Com, int Remmitance_cur, int out_rem_cur, int com_cur)
        {
            InitializeComponent();
            Remmitance_Amount1 = Remmitance_Amount;
            //Remmitance_Com1 = Remmitance_Com;
            Remmitance_cur1 = Remmitance_cur;
            out_rem_cur1 = out_rem_cur;
            com_cur1 = com_cur;
        }

        private void Com_Amount_Load(object sender, EventArgs e)
        {
            string sql_txt = connection.SqlExec("select Cur_ANAME from Cur_Tbl "
                   + "where Cur_id = " + Remmitance_cur1, "Cur_loc_tbl").Rows[0]["Cur_ANAME"].ToString();

            string sql_txt1 = connection.SqlExec("select Cur_ANAME from Cur_Tbl "
                   + "where Cur_id = " + out_rem_cur1, "Cur_loc_tbl").Rows[0]["Cur_ANAME"].ToString();


           BindingSource BS_1 = new BindingSource();
           BindingSource BS_2 = new BindingSource();
           BS_1.DataSource = connection.SQLDS.Tables["Get_comm_online_tbl"];
            BS_2.DataSource = connection.SQLDS.Tables["Get_comm_online_tbl1"];



            Txtr_amount.Text = Remmitance_Amount1;
           Txt_com_amount.Text = connection.SQLDS.Tables["Get_comm_online_tbl"].Rows[0]["comm_amount"].ToString();
            label6.Text = sql_txt;
            label5.Text = sql_txt1;
            label3.Text = connection.SQLDS.Tables["Get_comm_online_tbl1"].Rows[0]["Cur_ANAME"].ToString();
            TxtTerm_Name.Text = connection.SQLDS.Tables["Get_comm_online_tbl"].Rows[0]["name_comm_Out"].ToString();

        }
    }
}
