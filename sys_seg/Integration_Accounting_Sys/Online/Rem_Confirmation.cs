﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Online;

namespace Integration_Accounting_Sys
{
    public partial class Rem_Confirmation : Form
    {
        #region Defintion
        Boolean change = false;
        string @format = "dd/MM/yyyy";
        Int32 from_nrec_date = 0;
        Int32 to_nrec_date = 0;
        Int32 City = 0;
        Int16 rem_type = 0;
        DataTable Rem_Tbl = new DataTable();
        BindingSource Bs_rem_details = new BindingSource();
        BindingSource binding_cbo_city = new BindingSource();
        BindingSource BS_CboCust_Id = new BindingSource();
        DataTable dt_cust_tbl = new DataTable();
        string Sql_Text = "";

        Int16 search_rem = 0;
        DataTable Dt_grd = new DataTable();
        Boolean change2 = false;
        Boolean change1 = false;
        Boolean ch = false;
        Boolean ch1 = false;
        DataTable Per_blacklist_tbl = new DataTable();
        DataTable Refuse_rem_confirm = new DataTable();
        DataTable DT_final = new DataTable();
        public static int Del_Btn = 0;
        BindingSource Bs_Sub_Cust = new BindingSource();
        DataTable Dt_Cust_TBl = new DataTable();
        DataTable DT1 = new DataTable();


        #endregion
        public Rem_Confirmation()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            GrdRem_Details.AutoGenerateColumns = false;
            Create_Table();
        }

        private void Create_Table()
        {
            string[] Column4 = { "Per_AName", "Per_EName" };
            string[] DType4 = { "System.String", "System.String" };

            Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);
        }

        private void Rem_Confirmation_Load(object sender, EventArgs e)
        {
            CboCust_Id.Visible = false;
            label4.Visible = false;
            CboCust_Id.SelectedValue = 0;
            Cbo_city.SelectedValue = 0;

            CboCust_Id.Enabled = true;

            connection.SqlExec("Rem_Confirmation " + connection.T_ID, "Rem_Confirmation_tbl");
            //Int16 Chk_Correspondence_Sys = Convert.ToInt16(connection.SQLDS.Tables["Rem_Confirmation_tbl"].Rows[0]["Chk_Correspondence_Sys"]);


            //if (Chk_Correspondence_Sys == 1)
            //{
            //    cbo_rem_type.SelectedIndex = 1;
            //    cbo_rem_type.Enabled = false;
            //}

            //else { cbo_rem_type.SelectedIndex = 0; }

            //string Sql_Text = "select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id ,1 as rem_search   "
            //                + " From CUSTOMERS A, TERMINALS B "
            //                + " Where A.CUST_ID = B.CUST_ID  "
            //                + " And A.Cust_Flag <> 0  "
            //                + " and b.Ver_Check = 1  "
            //                + " union "
            //                + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name , Sub_Cust_ID as cust_id ,2 as rem_search  "
            //                + " From  Sub_CUSTOMERS_online A , TERMINALS B "
            //                + " Where A.Cust_state  =  1  "
            //                + " and A.ver_check = 1 "
            //                + " and B.ver_check = 1 "
            //                + " union "
            //                + " select 0 as T_id,'جميع الوكلاء'  as Acust_name , 'All Agents' as Ecust_name , 0 as cust_id ,0 as rem_search "
            //                + " order by rem_search ";

            // BS_CboCust_Id.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");

            if (connection.T_ID == 1)  // الادارة
            {
                Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , 0 as Cust_Online_Main_Id, A.cust_id as cust_id  ,0 as rem_search "
                                 + "  From CUSTOMERS A, TERMINALS B   "
                                 + "  Where A.CUST_ID = B.CUST_ID "
                                 + "  And A.Cust_Flag <> 0  "
                                 + " union "
                                 + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id ,Sub_Cust_ID as cust_id ,1 as rem_search"
                                 + " From  Sub_CUSTOMERS_online A  "
                                 + " Where A.Cust_state  =  1 "//فعال
                                 + " and A.t_id =  " + connection.T_ID
                                 + " union "
                                 + "select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id   ,Sub_Cust_ID as cust_id ,1 as rem_search    "
                                 + " From  Sub_CUSTOMERS_online A "
                                 + " Where A.Cust_state  =  1"
                                 + "  and A.t_id <>  1"
                                 + " order by cust_id";


            }

            if (connection.T_ID != 1)
            {
                Sql_Text = "  select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , 0 as Cust_Online_Main_Id, A.cust_id as cust_id  ,0 as rem_search "
                      + "  From CUSTOMERS A, TERMINALS B   "
                      + "  Where A.CUST_ID = B.CUST_ID "
                      + "  And A.Cust_Flag <> 0  "
                      + " and B.t_id =  " + connection.T_ID
                      + " union "
                      + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id ,Sub_Cust_ID as cust_id ,1 as rem_search "
                      + " From  Sub_CUSTOMERS_online A  "
                      + " Where A.Cust_state  =  1 "//فعال
                      + " and A.t_id =  " + connection.T_ID
                      + " order by cust_id ";
            }


            connection.SqlExec(Sql_Text, "Confirmation11");


            dt_cust_tbl = connection.SQLDS.Tables["Confirmation11"];

            BS_CboCust_Id.DataSource = dt_cust_tbl;
            CboCust_Id.DataSource = BS_CboCust_Id;
            CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            CboCust_Id.ValueMember = "cust_id";
            ch = true;

            //string Sql_Text1 = " select CUST_online_ID, CUST_ID, CITY_ID, COUN_ID, Cit_ID, Con_ID, Cit_AName, Cit_EName, Cit_Code, user_id "
            //   + "  From CUST_CITY_ONLINE A , Cit_Tbl B "
            //   + "  Where A.CITY_ID = B.Cit_ID ";

            //Cbo_city.DataSource = connection.SqlExec(Sql_Text1, "cit_tbl");

            Cbo_city.DataSource = connection.SQLDS.Tables["Rem_Confirmation_tbl2"];
            Cbo_city.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
            Cbo_city.ValueMember = "CITY_ID";


           

            if (connection.Lang_id == 2)
            {
                cbo_rem_type.Items[0] = "out remittance";
                cbo_rem_type.Items[1] = "In remittance";

                //comboBox1.Items[0] = "Local network";
                //comboBox1.Items[1] = "Online Network";

                //if (comboBox1.SelectedIndex == 0)
                //{
                    Column10.DataPropertyName = "ver_flag_ename";
                    Column9.DataPropertyName = "RE_CUR_name";
                    Column3.DataPropertyName = "pre_cur_name";
                    Column4.DataPropertyName = "S_ECITY_NAME";
                    Column7.DataPropertyName = "eCASE_NA";
                //}
                //else
                //{
                    Column10.DataPropertyName = "ver_flag_ename";
                    Column9.DataPropertyName = "RE_CUR_name";
                    Column3.DataPropertyName = "pre_cur_name";
                    Column4.DataPropertyName = "sacity_ename";
                    Column7.DataPropertyName = "eCASE_NA";
                //}
            }
            cbo_order_type.SelectedIndex = 0;
            cbo_rem_type.SelectedIndex = 0;
        
            ////cbo_rem_type_SelectionChangeCommitted(null, null);

            cbo_rem_type_SelectedIndexChanged(null, null);
            //CboCust_Id_SelectionChangeCommitted(null, null);
            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                from_nrec_date = 0;
            }
            if (TxtToDate.Checked == true)
            {

                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                to_nrec_date = 0;
            }
            //Cbo_city.SelectedIndex = 1;
            cbo_order_type.SelectedIndex = 0;
        }
        //----------------------------------------------
        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtFromDate.Checked == true)
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = @format;
            }
            else
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = " ";
            }
        }
        //----------------------------------------------
        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtToDate.Checked == true)
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = @format;
            }
            else
            {
                TxtToDate.Format = DateTimePickerFormat.Custom;
                TxtToDate.CustomFormat = " ";
            }
        }
        //----------------------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            //checkBox1.Checked = false;

            if (change)
            {


                Bs_rem_details = new BindingSource();
                string[] Str = { "Main_Verrifications_search_Web_tbl" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }
            }
            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                from_nrec_date = 0;
            }
            if (TxtToDate.Checked == true)
            {

                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                to_nrec_date = 0;
            }


            //if (rem_type == 2)
            //{
            //    if (Convert.ToInt16(CboCust_Id.SelectedValue) == 0)
            //    {
            //        MessageBox.Show(connection.Lang_id == 2 ? "please select the branch or agent" : "يرجى اختيار الفرع او الوكيل", MyGeneral_Lib.LblCap);

            //        return;
            //    }
            //}


            if (Convert.ToInt16(Cbo_city.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "please select city" : "يرجى اختيار المدينة", MyGeneral_Lib.LblCap);
            
                return;
            }
 

          //  Int16 search_rem = Convert.ToInt16(((DataRowView)BS_CboCust_Id.Current).Row["sub_cust_id"]);

            Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);  //new
            Int64 search_rem = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["rem_search"]); //new
            Int64 t_id_rem = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["rem_search"]);



            MyGeneral_Lib.Copytocliptext("Main_Verrifications_search_Web", new object[] { from_nrec_date, to_nrec_date, cust_id
                , R_name.Text, S_name.Text, rem_type, rem_no.Text.Trim() ,Convert.ToInt16(Cbo_city.SelectedValue) , "", connection.Lang_id ,search_rem,t_id_rem});


            connection.SqlExec("Main_Verrifications_search_Web", "Main_Verrifications_search_Web_tbl", new object[] { from_nrec_date, to_nrec_date, cust_id
                , R_name.Text, S_name.Text, rem_type, rem_no.Text.Trim() ,Convert.ToInt16(Cbo_city.SelectedValue) , "", connection.Lang_id ,search_rem,t_id_rem});
            string Param_Result = connection.Col_Name;

            Bs_rem_details.DataSource = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"];

            string param_result = connection.Col_Name;
            if (connection.Col_Name != "")
            {
                string strMsg = param_result;
                MessageBox.Show(strMsg, MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }



            Bs_rem_details.DataSource = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"]; ;
            try
            {
                //if (comboBox1.SelectedIndex == 0)
                //{
                    if (connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].Rows.Count > 0)
                    {
                        Dt_grd = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"];
                        GrdRem_Details.DataSource = Bs_rem_details;
                    //    BS_CboCust_Id.DataSource = new BindingSource();

                        Txt_purpose.DataBindings.Clear();
                        Txt_details.DataBindings.Clear();
                        Txt_sender.DataBindings.Clear();
                        Txt_S_nat.DataBindings.Clear();
                        Txt_s_phone.DataBindings.Clear();
                        Txt_S_cit.DataBindings.Clear();
                        Txt_doc.DataBindings.Clear();
                        Txt_doc_no.DataBindings.Clear();
                        Txt_date.DataBindings.Clear();
                        Txt_sdate_exp.DataBindings.Clear();
                        Txt_oc_issue.DataBindings.Clear();
                        Txt_birth_date.DataBindings.Clear();
                        Txt_s_address.DataBindings.Clear();
                        Txt_r_name.DataBindings.Clear();
                        Txt_R_nat.DataBindings.Clear();
                        Txt_r_phone.DataBindings.Clear();
                        Txt_r_cit.DataBindings.Clear();
                        txt_r_doc.DataBindings.Clear();
                        txt_r_doc_no.DataBindings.Clear();
                        txt_r_doc_date.DataBindings.Clear();
                        Txt_rdate_exp.DataBindings.Clear();
                        txt_r_doc_issue.DataBindings.Clear();
                        txt_r_birth_date.DataBindings.Clear();
                        txt_r_address.DataBindings.Clear();
                        Txt_purpose.DataBindings.Add("Text", Bs_rem_details, "T_purpose");
                        Txt_details.DataBindings.Add("Text", Bs_rem_details, "s_notes");
                        Txt_sender.DataBindings.Add("Text", Bs_rem_details, "S_name");
                        Txt_S_nat.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "snat_name" : "senat_name");
                        Txt_s_phone.DataBindings.Add("Text", Bs_rem_details, "S_phone");
                        Txt_S_cit.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "scity_name" : "secity_name");
                        Txt_doc.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                        Txt_doc_no.DataBindings.Add("Text", Bs_rem_details, "S_doc_no");
                        Txt_date.DataBindings.Add("Text", Bs_rem_details, "S_doc_ida");
                        Txt_sdate_exp.DataBindings.Add("Text", Bs_rem_details, "S_doc_eda");
                        Txt_oc_issue.DataBindings.Add("Text", Bs_rem_details, "S_doc_issue");
                        Txt_birth_date.DataBindings.Add("Text", Bs_rem_details, "sbirth_date");
                        Txt_s_address.DataBindings.Add("Text", Bs_rem_details, "S_address");
                        Txt_r_name.DataBindings.Add("Text", Bs_rem_details, "r_name");
                        Txt_R_nat.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "rnat_name" : "renat_name");
                        Txt_r_phone.DataBindings.Add("Text", Bs_rem_details, "R_phone");
                        Txt_r_cit.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                        txt_r_doc.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_eDOC_NA");
                        txt_r_doc_no.DataBindings.Add("Text", Bs_rem_details, "r_doc_no");
                        txt_r_doc_date.DataBindings.Add("Text", Bs_rem_details, "r_doc_ida");
                        Txt_rdate_exp.DataBindings.Add("Text", Bs_rem_details, "r_doc_eda");
                        txt_r_doc_issue.DataBindings.Add("Text", Bs_rem_details, "r_doc_issue");
                        txt_r_birth_date.DataBindings.Add("Text", Bs_rem_details, "rbirth_date");
                        txt_r_address.DataBindings.Add("Text", Bs_rem_details, "r_address");
                        change = true;

                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات تحقق الشروط" : "no remmitance confirm this conditions", MyGeneral_Lib.LblCap);
                        clear_all();
                        change = true;
                        return;
                    }

                //}

                //---------------------------------------------------
                if (cbo_rem_type.SelectedIndex == 0)
                {
                    if (connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].Rows.Count > 0)
                    {
                        Dt_grd = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"];
                        GrdRem_Details.DataSource = Bs_rem_details;

                        Txt_purpose.DataBindings.Clear();
                        Txt_details.DataBindings.Clear();
                        Txt_sender.DataBindings.Clear();
                        Txt_S_nat.DataBindings.Clear();
                        Txt_s_phone.DataBindings.Clear();
                        Txt_S_cit.DataBindings.Clear();
                        Txt_doc.DataBindings.Clear();
                        Txt_doc_no.DataBindings.Clear();
                        Txt_date.DataBindings.Clear();
                        Txt_sdate_exp.DataBindings.Clear();
                        Txt_oc_issue.DataBindings.Clear();
                        Txt_birth_date.DataBindings.Clear();
                        Txt_s_address.DataBindings.Clear();
                        Txt_r_name.DataBindings.Clear();
                        Txt_R_nat.DataBindings.Clear();
                        Txt_r_phone.DataBindings.Clear();
                        Txt_r_cit.DataBindings.Clear();
                        txt_r_doc.DataBindings.Clear();
                        txt_r_doc_no.DataBindings.Clear();
                        txt_r_doc_date.DataBindings.Clear();
                        Txt_rdate_exp.DataBindings.Clear();
                        txt_r_doc_issue.DataBindings.Clear();
                        txt_r_birth_date.DataBindings.Clear();
                        txt_r_address.DataBindings.Clear();
                        Txt_purpose.DataBindings.Add("Text", Bs_rem_details, "T_purpose");
                        Txt_details.DataBindings.Add("Text", Bs_rem_details, "s_notes");
                        Txt_sender.DataBindings.Add("Text", Bs_rem_details, "S_name");
                        Txt_S_nat.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "snat_name" : "senat_name");
                        Txt_s_phone.DataBindings.Add("Text", Bs_rem_details, "S_phone");
                        Txt_S_cit.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "scity_name" : "secity_name");
                        Txt_doc.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "sFRM_EDOC_NA");
                        Txt_doc_no.DataBindings.Add("Text", Bs_rem_details, "S_doc_no");
                        Txt_date.DataBindings.Add("Text", Bs_rem_details, "S_doc_ida");
                        Txt_sdate_exp.DataBindings.Add("Text", Bs_rem_details, "S_doc_eda");
                        Txt_oc_issue.DataBindings.Add("Text", Bs_rem_details, "S_doc_issue");
                        Txt_birth_date.DataBindings.Add("Text", Bs_rem_details, "sbirth_date");
                        Txt_s_address.DataBindings.Add("Text", Bs_rem_details, "S_address");
                        Txt_r_name.DataBindings.Add("Text", Bs_rem_details, "r_name");
                        Txt_R_nat.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "rnat_name" : "renat_name");
                        Txt_r_phone.DataBindings.Add("Text", Bs_rem_details, "R_phone");
                        Txt_r_cit.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME");
                        txt_r_doc.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_eDOC_NA");
                        txt_r_doc_no.DataBindings.Add("Text", Bs_rem_details, "r_doc_no");
                        txt_r_doc_date.DataBindings.Add("Text", Bs_rem_details, "r_doc_ida");
                        Txt_rdate_exp.DataBindings.Add("Text", Bs_rem_details, "r_doc_eda");
                        txt_r_doc_issue.DataBindings.Add("Text", Bs_rem_details, "r_doc_issue");
                        txt_r_birth_date.DataBindings.Add("Text", Bs_rem_details, "rbirth_date");
                        txt_r_address.DataBindings.Add("Text", Bs_rem_details, "r_address");
                        change = true;

                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات تحقق الشروط" : "no remmitance confirm this conditions", MyGeneral_Lib.LblCap);
                        clear_all();
                        change = true;
                        return;
                    }

                }
                //---------------------------------------------------------------------------------
                if (cbo_rem_type.SelectedIndex == 1)
                {
                    if (connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].Rows.Count > 0)
                    {
                        Dt_grd = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"];
                        GrdRem_Details.DataSource = Bs_rem_details;

                        Txt_purpose.DataBindings.Clear();
                        Txt_details.DataBindings.Clear();
                        Txt_sender.DataBindings.Clear();
                        Txt_S_nat.DataBindings.Clear();
                        Txt_s_phone.DataBindings.Clear();
                        Txt_S_cit.DataBindings.Clear();
                        Txt_doc.DataBindings.Clear();
                        Txt_doc_no.DataBindings.Clear();
                        Txt_date.DataBindings.Clear();
                        Txt_sdate_exp.DataBindings.Clear();
                        Txt_oc_issue.DataBindings.Clear();
                        Txt_birth_date.DataBindings.Clear();
                        Txt_s_address.DataBindings.Clear();
                        Txt_r_name.DataBindings.Clear();
                        Txt_R_nat.DataBindings.Clear();
                        Txt_r_phone.DataBindings.Clear();
                        Txt_r_cit.DataBindings.Clear();
                        txt_r_doc.DataBindings.Clear();
                        txt_r_doc_no.DataBindings.Clear();
                        txt_r_doc_date.DataBindings.Clear();
                        Txt_rdate_exp.DataBindings.Clear();
                        txt_r_doc_issue.DataBindings.Clear();
                        txt_r_birth_date.DataBindings.Clear();
                        txt_r_address.DataBindings.Clear();
                        Txt_purpose.DataBindings.Add("Text", Bs_rem_details, "T_purpose");
                        Txt_details.DataBindings.Add("Text", Bs_rem_details, "s_notes");
                        Txt_sender.DataBindings.Add("Text", Bs_rem_details, "S_name");
                        Txt_S_nat.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "snat_name" : "senat_name");
                        Txt_s_phone.DataBindings.Add("Text", Bs_rem_details, "S_phone");
                        Txt_S_cit.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "sacity_aname" : "sacity_ename");
                        Txt_doc.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "sFRM_ADOC_NA" : "SFRM_EDOC_NA");
                        Txt_doc_no.DataBindings.Add("Text", Bs_rem_details, "S_doc_no");
                        Txt_date.DataBindings.Add("Text", Bs_rem_details, "S_doc_ida");
                        Txt_sdate_exp.DataBindings.Add("Text", Bs_rem_details, "S_doc_eda");
                        Txt_oc_issue.DataBindings.Add("Text", Bs_rem_details, "S_doc_issue");
                        Txt_birth_date.DataBindings.Add("Text", Bs_rem_details, "sbirth_date");
                        Txt_s_address.DataBindings.Add("Text", Bs_rem_details, "S_address");
                        Txt_r_name.DataBindings.Add("Text", Bs_rem_details, "r_name");
                        Txt_R_nat.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "rnat_name" : "renat_name");
                        Txt_r_phone.DataBindings.Add("Text", Bs_rem_details, "R_phone");
                        Txt_r_cit.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "rcity_name" : "recity_name");
                        txt_r_doc.DataBindings.Add("Text", Bs_rem_details, connection.Lang_id == 1 ? "FRM_ADOC_NA" : "FRM_eDOC_NA");
                        txt_r_doc_no.DataBindings.Add("Text", Bs_rem_details, "r_doc_no");
                        txt_r_doc_date.DataBindings.Add("Text", Bs_rem_details, "r_doc_ida");
                        Txt_rdate_exp.DataBindings.Add("Text", Bs_rem_details, "r_doc_eda");
                        txt_r_doc_issue.DataBindings.Add("Text", Bs_rem_details, "r_doc_issue");
                        txt_r_birth_date.DataBindings.Add("Text", Bs_rem_details, "rbirth_date");
                        txt_r_address.DataBindings.Add("Text", Bs_rem_details, "r_address");
                        change = true;

                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات تحقق الشروط" : "no remmitance confirm this conditions", MyGeneral_Lib.LblCap);
                        clear_all();
                        change = true;
                        return;
                    }
                }
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات تحقق الشروط" : "no remmitance confirm this conditions", MyGeneral_Lib.LblCap);
                clear_all();
                change = true;
                return;
            }
            change2 = true;
            change1 = true;


        }


        //----------------------------------------------
        private void clear_all()
        {
            Bs_rem_details = new BindingSource();
            GrdRem_Details.DataSource = Bs_rem_details;
            Txt_purpose.Text = "";
            Txt_details.Text = "";
            Txt_sender.Text = "";
            Txt_S_nat.Text = "";
            Txt_s_phone.Text = "";
            Txt_S_cit.Text = "";
            Txt_doc.Text = "";
            Txt_doc_no.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_date.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_oc_issue.Text = "";
            Txt_birth_date.Text = "";
            Txt_s_address.Text = "";
            Txt_r_name.Text = "";
            Txt_R_nat.Text = "";
            Txt_r_phone.Text = "";
            Txt_r_cit.Text = "";
            txt_r_doc.Text = "";
            txt_r_doc_no.Text = "";
            txt_r_doc_date.Text = "";
            Txt_rdate_exp.Text = "";
            txt_r_doc_issue.Text = "";
            txt_r_birth_date.Text = "";
            txt_r_address.Text = "";
            R_name.Text = "";
            S_name.Text = "";
            rem_no.Text = "";
        }


        //---------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
         //   Int16 t_id1 = Convert.ToInt16(((DataRowView)BS_CboCust_Id.Current).Row["T_id"]);

            if (GrdRem_Details.RowCount > 0)
            {
                Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);  //new
                Int64 t_id1 = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["T_id"]); //new
                
                
                try
                {

                    DT_final = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "rem_no", "transfer_flag", "cust_online_id", "Check_id").Select("Check_id > 0").CopyToDataTable();
                    DT_final = DT_final.DefaultView.ToTable(false, "rem_no", "transfer_flag", "cust_online_id").Select().CopyToDataTable();
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الحوالة  " : "Choose the Rimmetance Please", MyGeneral_Lib.LblCap);
                    return;
                }

                if (DT_final.Rows.Count > 5)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار  خمس حوالات  فقط" : "Choose five Rimmetance only Please", MyGeneral_Lib.LblCap);
                    return;
                }

                DataTable Dt_new = new DataTable();
                try
                {

                    Dt_new = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "Check_id", "vo_no", "S_per_id", "r_per_id", "r_cur_id", "R_amount", "S_name", "R_name", "Oper_id").Select("Check_id > 0").CopyToDataTable();
                }
                catch
                { }
                if (Dt_new.Rows.Count > 0)
                {

                    DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? " هل تريد التأكيد فعلا" : "Do you want to confirm", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (Dr == DialogResult.Yes)
                    {


                        Rem_Blacklist_confirm_ok.Notes_BL = "";

                        connection.SQLCMD.Parameters.AddWithValue("@Rem_Tbl", DT_final);
                        connection.SQLCMD.Parameters.AddWithValue("@cust_id", cust_id);
                        connection.SQLCMD.Parameters.AddWithValue("@r_type_id", rem_type);
                        //connection.SQLCMD.Parameters.AddWithValue("@cust_id_online", connection.Cust_online_Id);
                        connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                        //connection.SQLCMD.Parameters.AddWithValue("@loc_Inter_flag", comboBox1.SelectedIndex);
                        connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
                        connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                        connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                        connection.SQLCMD.Parameters.AddWithValue("@Oper_id_rem", Dt_new.Rows[0]["Oper_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@vo_no_rem", Dt_new.Rows[0]["vo_no"]);
                        connection.SQLCMD.Parameters.AddWithValue("@S_per_id_Rem", Dt_new.Rows[0]["S_per_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_per_id_Rem", Dt_new.Rows[0]["r_per_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_cur_id_Rem", Dt_new.Rows[0]["r_cur_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@R_amount_Rem", Dt_new.Rows[0]["R_amount"]);
                        connection.SQLCMD.Parameters.AddWithValue("@S_name_Rem", Dt_new.Rows[0]["S_name"]);
                        connection.SQLCMD.Parameters.AddWithValue("@R_name_Rem", Dt_new.Rows[0]["R_name"]);
                        connection.SQLCMD.Parameters.AddWithValue("@t_id", t_id1);
                        connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                        MyGeneral_Lib.Copytocliptext("Update_Ver_Remmittince_Web", connection.SQLCMD);
                        connection.SqlExec("Update_Ver_Remmittince_Web", connection.SQLCMD);

                        if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                        {
                            MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                            connection.SQLCMD.Parameters.Clear();
                            clear_all();
                            clear_Tables();
                            return;
                        }
                        MessageBox.Show(connection.Lang_id == 1 ? "تم تأكيد الحوالة" : "Rimmetance has been confirmed", MyGeneral_Lib.LblCap);
                        clear_all();
                        clear_Tables();
                        //   checkBox1.Checked = false;
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        Rem_Confirmation_Load(null, null);
                    }
                }

                else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }
            }
        }
        //----------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Grd_Sub_Cust.DataSource = new BindingSource();
        }
        //----------------------------------------------
        private void clear_Tables()
        {
            Bs_rem_details = new BindingSource();
            string[] Str = { "Main_Verrifications_search_Web_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
        }

        private void rem_no_TextChanged(object sender, EventArgs e)
        {
            //clear_all();
            clear_Tables();

            Bs_rem_details = new BindingSource();
            GrdRem_Details.DataSource = Bs_rem_details;
            Txt_purpose.Text = "";
            Txt_details.Text = "";
            Txt_sender.Text = "";
            Txt_S_nat.Text = "";
            Txt_s_phone.Text = "";
            Txt_S_cit.Text = "";
            Txt_doc.Text = "";
            Txt_doc_no.Text = "";
            Txt_date.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_oc_issue.Text = "";
            Txt_birth_date.Text = "";
            Txt_s_address.Text = "";
            Txt_r_name.Text = "";
            Txt_R_nat.Text = "";
            Txt_r_phone.Text = "";
            Txt_r_cit.Text = "";
            txt_r_doc.Text = "";
            txt_r_doc_no.Text = "";
            txt_r_doc_date.Text = "";
            Txt_rdate_exp.Text = "";
            txt_r_doc_issue.Text = "";
            txt_r_birth_date.Text = "";
            txt_r_address.Text = "";
            R_name.Text = "";
            S_name.Text = "";
            //rem_no.Text = "";
        }

        private void S_name_TextChanged(object sender, EventArgs e)
        {
            //clear_all();
            clear_Tables();

            Bs_rem_details = new BindingSource();
            GrdRem_Details.DataSource = Bs_rem_details;
            Txt_purpose.Text = "";
            Txt_details.Text = "";
            Txt_sender.Text = "";
            Txt_S_nat.Text = "";
            Txt_s_phone.Text = "";
            Txt_S_cit.Text = "";
            Txt_doc.Text = "";
            Txt_doc_no.Text = "";
            Txt_date.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_oc_issue.Text = "";
            Txt_birth_date.Text = "";
            Txt_s_address.Text = "";
            Txt_r_name.Text = "";
            Txt_R_nat.Text = "";
            Txt_r_phone.Text = "";
            Txt_r_cit.Text = "";
            txt_r_doc.Text = "";
            txt_r_doc_no.Text = "";
            txt_r_doc_date.Text = "";
            Txt_rdate_exp.Text = "";
            txt_r_doc_issue.Text = "";
            txt_r_birth_date.Text = "";
            txt_r_address.Text = "";
            R_name.Text = "";
            //S_name.Text = "";
            rem_no.Text = "";
        }

        private void R_name_TextChanged(object sender, EventArgs e)
        {
            //clear_all();
            clear_Tables();

            Bs_rem_details = new BindingSource();
            GrdRem_Details.DataSource = Bs_rem_details;
            Txt_purpose.Text = "";
            Txt_details.Text = "";
            Txt_sender.Text = "";
            Txt_S_nat.Text = "";
            Txt_s_phone.Text = "";
            Txt_S_cit.Text = "";
            Txt_doc.Text = "";
            Txt_doc_no.Text = "";
            Txt_date.Text = "";
            Txt_sdate_exp.Text = "";
            Txt_oc_issue.Text = "";
            Txt_birth_date.Text = "";
            Txt_s_address.Text = "";
            Txt_r_name.Text = "";
            Txt_R_nat.Text = "";
            Txt_r_phone.Text = "";
            Txt_r_cit.Text = "";
            txt_r_doc.Text = "";
            txt_r_doc_no.Text = "";
            txt_r_doc_date.Text = "";
            Txt_rdate_exp.Text = "";
            txt_r_doc_issue.Text = "";
            txt_r_birth_date.Text = "";
            txt_r_address.Text = "";
            //R_name.Text = "";
            S_name.Text = "";
            rem_no.Text = "";
        }

        private void cbo_rem_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (cbo_rem_type.SelectedIndex == 0)
            {
                rem_type = 2;
                CboCust_Id.SelectedValue = 0;
                CboCust_Id.Enabled = true;


            }
            else
            {
                rem_type = 1;
                CboCust_Id.SelectedValue = 0;
                CboCust_Id.Enabled = false;


                Cbo_city.DataSource = connection.SQLDS.Tables["Rem_Confirmation_tbl2"];
                Cbo_city.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                Cbo_city.ValueMember = "CITY_ID";

            }

            GrdRem_Details.DataSource = new BindingSource();


            if (change1)

            { clear_Tables(); }

            change1 = false;
        }

        //private void CboCust_Id_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (change2)
        //    {
        //        clear_Tables();
        //    }
        //    change2 = false;
 
        //}

        private void Rem_Confirmation_FormClosed(object sender, FormClosedEventArgs e)
        {
            Del_Btn = 0;
            string[] comm_Tbl = { "Oper_TBL", "Main_Verrifications_search_Web_tbl", "Rem_Confirmation_tbl", "Rem_Confirmation_tbl1", "Rem_Confirmation_tbl2" };

            foreach (string Tbl in comm_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

            Grd_Sub_Cust.DataSource = new BindingSource();
        }


        //private void checkBox1_MouseClick(object sender, MouseEventArgs e)
        //{
        //    if (GrdRem_Details.Rows.Count > 0)
        //    {
        //        CheckBox HCheckBox = ((CheckBox)sender);
        //        foreach (DataGridViewRow Row in GrdRem_Details.Rows)
        //            ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

        //        GrdRem_Details.RefreshEdit();
        //        connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].AcceptChanges();
        //    }

        //    else {
        //        MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap);
        //     //   checkBox1.Checked = false;

        //        }
        //}

        private void label77_Click(object sender, EventArgs e)
        {
            if (GrdRem_Details.RowCount > 0)
            {
                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.CommandTimeout = 0;
                    connection.SQLCMD.Parameters.AddWithValue("@name", cbo_rem_type.SelectedIndex == 0 ? ((DataRowView)Bs_rem_details.Current).Row["s_name"].ToString() : ((DataRowView)Bs_rem_details.Current).Row["r_name"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@S_doc_no", cbo_rem_type.SelectedIndex == 0 ? ((DataRowView)Bs_rem_details.Current).Row["S_doc_no"].ToString() : "");
                    connection.SQLCMD.Parameters.AddWithValue("@S_Social_No", cbo_rem_type.SelectedIndex == 0 ? ((DataRowView)Bs_rem_details.Current).Row["S_Social_No"].ToString() : "");
                    connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", cbo_rem_type.SelectedIndex == 0 ? Convert.ToInt32(((DataRowView)Bs_rem_details.Current).Row["sFrm_doc_id"]) : 0);
                    connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", cbo_rem_type.SelectedIndex == 0 ? "" : ((DataRowView)Bs_rem_details.Current).Row["r_doc_no"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", cbo_rem_type.SelectedIndex == 0 ? "" : ((DataRowView)Bs_rem_details.Current).Row["r_Social_No"].ToString());
                    connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", cbo_rem_type.SelectedIndex == 0 ? 0 : Convert.ToInt32(((DataRowView)Bs_rem_details.Current).Row["rFrm_doc_id"]));
                    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", cbo_rem_type.SelectedIndex == 0 ? 2 : 1);
                    connection.SQLCMD.Parameters.AddWithValue("@sbirth_date", cbo_rem_type.SelectedIndex == 0 ? ((DataRowView)Bs_rem_details.Current).Row["sbirth_date"].ToString() : "");
                    connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", cbo_rem_type.SelectedIndex == 0 ? "" : ((DataRowView)Bs_rem_details.Current).Row["rbirth_date"].ToString());


                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch
                {

                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }


                if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
                {

                    Rem_Count AddFrm = new Rem_Count(1);
                    AddFrm.ShowDialog(this);
                }
                else
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                    return;
                }
            }

            else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }
        }

        private void label80_Click(object sender, EventArgs e)
        {
            //if (GrdRem_Details.RowCount > 0)
            //{


            try
            {

                if (GrdRem_Details.RowCount > 0)
                {

                    label80.Enabled = false;
                    DataTable Per_blacklist_tbl = new DataTable();
                    string[] Column4 = { "Per_AName", "Per_EName" };
                    string[] DType4 = { "System.String", "System.String" };

                    Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                    Per_blacklist_tbl.Rows.Clear();

                    if (cbo_rem_type.SelectedIndex == 0)//صادر
                    { Per_blacklist_tbl.Rows.Add(((DataRowView)Bs_rem_details.Current).Row["S_Name"].ToString(), ((DataRowView)Bs_rem_details.Current).Row["S_Ename"].ToString()); }

                    if (cbo_rem_type.SelectedIndex == 1)//وارد
                    { Per_blacklist_tbl.Rows.Add(((DataRowView)Bs_rem_details.Current).Row["r_Name"].ToString(), ""); }


                    //    Per_blacklist_tbl.Rows.Add(((DataRowView)Bs_rem_details.Current).Row["S_name"].ToString(), cbo_rem_type.SelectedIndex == 1 ?((DataRowView)Bs_rem_details.Current).Row["S_Ename"].ToString() : "");

                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                        // connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)binding_Grd1.Current).Row["rem_no"].ToString());
                        connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)Bs_rem_details.Current).Row["snat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", ((DataRowView)Bs_rem_details.Current).Row["rnat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)Bs_rem_details.Current).Row["sa_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", ((DataRowView)Bs_rem_details.Current).Row["r_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@s_job", ((DataRowView)Bs_rem_details.Current).Row["s_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_job", ((DataRowView)Bs_rem_details.Current).Row["r_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", ((DataRowView)Bs_rem_details.Current).Row["rFrm_doc_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)Bs_rem_details.Current).Row["sFrm_doc_id"]);


                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        label80.Enabled = true;
                    }
                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                        label80.Enabled = true;
                    }

                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                    {
                        Rem_Blacklist_confirm AddFrm = new Rem_Blacklist_confirm();
                        AddFrm.ShowDialog(this);
                        string[] Str = { "per_rem_blacklist_tbl", "per_rem_blacklist_tbl1" };
                        foreach (string Tbl in Str)
                        {
                            if (connection.SQLDS.Tables.Contains(Tbl))
                                connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                        }

                    }
                    else
                    {
                        label80.Enabled = true;
                        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                        return;
                    }

                }
            }
            catch
            {
                label80.Enabled = true;
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            //try
            //{

            //    if (connection.SQLDS.Tables.Contains("per_rem_blacklist_tbl"))
            //    {
            //        connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
            //    }


            //    Per_blacklist_tbl.Rows.Clear();

            //    if (cbo_rem_type.SelectedIndex == 0)//صادر
            //    { Per_blacklist_tbl.Rows.Add(((DataRowView)Bs_rem_details.Current).Row["S_Name"].ToString(), ""); }

            //    if (cbo_rem_type.SelectedIndex == 1)//وارد
            //    { Per_blacklist_tbl.Rows.Add(((DataRowView)Bs_rem_details.Current).Row["r_Name"].ToString(), ""); }



            //    try
            //    {
            //        connection.SQLCS.Open();
            //        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
            //        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            //        connection.SQLCMD.Connection = connection.SQLCS;
            //        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
            //        IDataReader obj = connection.SQLCMD.ExecuteReader();
            //        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
            //        obj.Close();
            //        connection.SQLCS.Close();
            //        connection.SQLCMD.Parameters.Clear();
            //        connection.SQLCMD.Dispose();
            //    }

            //    catch
            //    {
            //        connection.SQLCS.Close();
            //        connection.SQLCMD.Parameters.Clear();
            //        connection.SQLCMD.Dispose();
            //    }

            //    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
            //    {
            //        Rem_Blacklist AddFrm = new Rem_Blacklist();
            //        AddFrm.ShowDialog(this);
            //    }

            //    else
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
            //        return;
            //    }
            //}

            //catch { }
            //}

            //else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (GrdRem_Details.RowCount > 0)
            {

                if (cbo_rem_type.SelectedIndex == 0)
                {

                    try
                    {
                        DT_final = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "Check_id", "S_name", "r_name", "S_EName", "rem_no", "oper_id", "vo_no", "S_per_id", "r_per_id" , "r_cur_id" ,"r_amount" ).Select("Check_id > 0").CopyToDataTable();

                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الحوالة  " : "Choose the Rimmetance Please", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                else
                {
                    try
                    {
                        DT_final = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "Check_id", "r_name", "S_Name",  "rem_no", "oper_id", "vo_no", "S_per_id", "r_per_id", "r_cur_id", "r_amount" ).Select("Check_id > 0").CopyToDataTable();

                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الحوالة  " : "Choose the Rimmetance Please", MyGeneral_Lib.LblCap);
                        return;
                    }


                }

                if (DT_final.Rows.Count > 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار حوالة واحدة فقط" : "Choose one Rimmetance only Please", MyGeneral_Lib.LblCap);
                    return;
                }


                try
                {



                    connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
                    Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

                    if (cond_bl_rem != 0)//في حال اختيار نوع البحث بلا من واجهة السيتنك فلا يبحث في قوائم البحث 
                    {

                        DataTable Per_blacklist_tbl = new DataTable();
                        string[] Column4 = { "Per_AName", "Per_EName" };
                        string[] DType4 = { "System.String", "System.String" };

                        Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                        Per_blacklist_tbl.Rows.Clear();

                        if (cbo_rem_type.SelectedIndex == 0)//صادر
                        { Per_blacklist_tbl.Rows.Add(DT_final.Rows[0]["S_Name"], Dt_grd.Rows[0]["S_EName"]); }

                        if (cbo_rem_type.SelectedIndex == 1)//وارد
                        { Per_blacklist_tbl.Rows.Add(DT_final.Rows[0]["r_Name"], ""); }



                        try
                        {
                            connection.SQLCS.Open();
                            connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                            connection.SQLCMD.Connection = connection.SQLCS;
                            connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                            connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)Bs_rem_details.Current).Row["snat_Id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", ((DataRowView)Bs_rem_details.Current).Row["rnat_Id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)Bs_rem_details.Current).Row["sa_coun_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", ((DataRowView)Bs_rem_details.Current).Row["r_coun_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@s_job", ((DataRowView)Bs_rem_details.Current).Row["s_job_ID"]);
                            connection.SQLCMD.Parameters.AddWithValue("@r_job", ((DataRowView)Bs_rem_details.Current).Row["r_job_ID"]);
                            connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", ((DataRowView)Bs_rem_details.Current).Row["rFrm_doc_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)Bs_rem_details.Current).Row["sFrm_doc_id"]);


                            IDataReader obj = connection.SQLCMD.ExecuteReader();
                            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                            obj.Close();
                            connection.SQLCS.Close();
                            connection.SQLCMD.Parameters.Clear();
                            connection.SQLCMD.Dispose();

                        }
                        catch
                        {
                            connection.SQLCS.Close();
                            connection.SQLCMD.Parameters.Clear();
                            connection.SQLCMD.Dispose();

                        }
                        if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                        {
                            Rem_Blacklist_confirm_ok AddFrm = new Rem_Blacklist_confirm_ok();
                            AddFrm.ShowDialog(this);
                            if (Rem_Blacklist_confirm_ok.Back_btn == 1)
                            {
                                Get_Black_list();
                                MessageBox.Show(connection.Lang_id == 1 ? "تم رفض الحوالة" : "Rimmetance has been refused", MyGeneral_Lib.LblCap);
                                button3.Enabled = true;
                                this.Close();
                                return;
                            }
                        }
                        else
                        {
                            Rem_Blacklist_confirm_ok.Back_btn = 0;
                            Rem_Blacklist_confirm_ok.Notes_BL = "";
                        }

                    }
                }

                catch { }


                Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);  //new
                Int64 t_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["T_id"]); //new T_id



                DT_final = DT_final.DefaultView.ToTable(false, "rem_no", "transfer_flag", "cust_online_id").Select().CopyToDataTable();
                DataTable Dt_new = new DataTable();
                try
                {

                    Dt_new = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "Check_id", "vo_no", "S_per_id", "r_per_id", "r_cur_id", "R_amount", "S_name", "R_name", "Oper_id").Select("Check_id > 0").CopyToDataTable();
                }
                catch
                { }
                //-------------------------------------------------
                connection.SQLCMD.Parameters.AddWithValue("@Rem_Tbl", DT_final);
                connection.SQLCMD.Parameters.AddWithValue("@cust_id", cust_id);
                connection.SQLCMD.Parameters.AddWithValue("@r_type_id", rem_type);
                //connection.SQLCMD.Parameters.AddWithValue("@cust_id_online", connection.Cust_online_Id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                //connection.SQLCMD.Parameters.AddWithValue("@loc_Inter_flag", comboBox1.SelectedIndex);
                connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
                connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                 connection.SQLCMD.Parameters.AddWithValue("@Oper_id_rem",Dt_new.Rows[0]["Oper_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@vo_no_rem", Dt_new.Rows[0]["vo_no"] );
                connection.SQLCMD.Parameters.AddWithValue("@S_per_id_Rem", Dt_new.Rows[0]["S_per_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@r_per_id_Rem", Dt_new.Rows[0]["r_per_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@r_cur_id_Rem", Dt_new.Rows[0]["r_cur_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@R_amount_Rem",  Dt_new.Rows[0]["R_amount"]);
                connection.SQLCMD.Parameters.AddWithValue("@S_name_Rem", Dt_new.Rows[0]["S_name"]);
                connection.SQLCMD.Parameters.AddWithValue("@R_name_Rem", Dt_new.Rows[0]["R_name"]);
                connection.SQLCMD.Parameters.AddWithValue("@t_id", t_id);
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                MyGeneral_Lib.Copytocliptext("Update_Ver_Remmittince_Web", connection.SQLCMD);
                connection.SqlExec("Update_Ver_Remmittince_Web", connection.SQLCMD);

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    clear_all();
                    clear_Tables();
                    return;
                }
                MessageBox.Show(connection.Lang_id == 1 ? "تم تأكيد الحوالة" : "Rimmetance has been confirmed", MyGeneral_Lib.LblCap);
                clear_all();
                clear_Tables();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                Rem_Confirmation_Load(null, null);

                //-------------------------
            }

            else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }

        }
        //---------------------------
        private void Get_Black_list()
        {


      
                try
                {

                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[Refuse_confirm_All]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@rem_no", DT_final.Rows[0]["rem_no"]);
                    connection.SQLCMD.Parameters.AddWithValue("@case_id", 101);
                    connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@note_prog", "Confirm Rem.");
                    connection.SQLCMD.Parameters.AddWithValue("@oper_id", DT_final.Rows[0]["oper_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@vo_no", DT_final.Rows[0]["vo_no"]);
                    connection.SQLCMD.Parameters.AddWithValue("@per_id", cbo_rem_type.SelectedIndex == 0 ? DT_final.Rows[0]["S_per_id"] : DT_final.Rows[0]["r_per_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", DT_final.Rows[0]["r_cur_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount", DT_final.Rows[0]["r_amount"]);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Name", cbo_rem_type.SelectedIndex == 0 ? DT_final.Rows[0]["s_name"] : DT_final.Rows[0]["r_name"]);
                    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", rem_type);
                    connection.SQLCMD.Parameters.AddWithValue("@transfer_flag ", DT_final.Rows[0]["transfer_flag"]);
                    connection.SQLCMD.Parameters.AddWithValue("@s_city_id", Cbo_city.SelectedValue);


                    IDataReader obj = connection.SQLCMD.ExecuteReader();

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }
           
        }

        private void Document_person()
        {
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1)// يملك خاصية رفع الوثائق
            {
                int Per_id = 0;
                string Per_AName = "";
                string Per_EName = "";

                if (cbo_rem_type.SelectedIndex == 0)
                {
                    Per_id = Convert.ToInt32(((DataRowView)Bs_rem_details.Current).Row["S_per_id"]);
                    Per_AName = ((DataRowView)Bs_rem_details.Current).Row["S_Name"].ToString();
                    Per_EName = ((DataRowView)Bs_rem_details.Current).Row["S_Ename"].ToString();
                }

                if (cbo_rem_type.SelectedIndex == 1)
                {
                    if (connection.SQLDS.Tables.Contains("Tbl_Person"))
                    { connection.SQLDS.Tables.Remove("Tbl_Person"); }

                    Per_AName = ((DataRowView)Bs_rem_details.Current).Row["r_Name"].ToString();
                    Per_EName = "";
                    connection.SqlExec("Select per_id , per_aname , per_ename from person_info where per_aname like '%" + Per_AName + "%' or per_ename like '%" + Per_AName + "%' ", "Tbl_Person");
                    if (connection.SQLDS.Tables["Tbl_Person"].Rows.Count == 1)
                    { Per_id = Convert.ToInt32(connection.SQLDS.Tables["Tbl_Person"].Rows[0]["per_id"]); }
                    else if (connection.SQLDS.Tables["Tbl_Person"].Rows.Count > 1)
                    {
                        Document_person_view UpdFrm = new Document_person_view();
                        //  this.Visible = false;
                        UpdFrm.ShowDialog(this);
                        //this.Visible = true;
                    }
                    else { Per_id = 0; }
                }
                if (Per_id != 0)
                {
                    Del_Btn = 1;
                    Person_Document_main UpdFrm = new Person_Document_main(Per_AName, Per_id, Per_EName);
                    //  this.Visible = false;
                    UpdFrm.ShowDialog(this);
                    //  this.Visible = true;
                }
                if (cbo_rem_type.SelectedIndex == 1 && Per_id == 0 && connection.SQLDS.Tables["Tbl_Person"].Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الثانوي غير معرف في الفرع" : "Customer is not defined in branch", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : "The company does not have the property to add documents", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void label35_Click(object sender, EventArgs e)
        {
            if (GrdRem_Details.RowCount > 0)
            {
                Document_person();
            }
        }

        private void GrdRem_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(Bs_rem_details);
        }

        private void fill()
        {
            string sxl_text = "";

            //// Int16 search_rem = Convert.ToInt16(((DataRowView)BS_CboCust_Id.Current).Row["rem_search"]);
            //Int16 cust_id = Convert.ToInt16(CboCust_Id.SelectedValue);

            //search_rem = Convert.ToInt16(dt_cust_tbl.Select("cust_id = " + cust_id).CopyToDataTable().Rows[0]["rem_search"]);

            ////DataRowView DRV = BS_CboCust_Id.Current as DataRowView;
            ////DataRow DR = DRV.Row;


            ////Int16 search_rem = DR.Field<Int16>("rem_search");

            ////int16 cust_id = Coun_City_Tbl.Select("Cit_ID = " + Cmb_T_City.SelectedValue).CopyToDataTable().Rows[0]["Con_AName"].ToString();

            Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);  //new
            Int64 search_rem = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["rem_search"]);


            if (cust_id != 0)
            {
                if (search_rem == 0)
                {
                    sxl_text = " select  Country_System_ID , Country_ID, System_ID,City_ID,Cit_AName,Cit_EName from Country_System_Tbl a,Cit_Tbl b "
                                   + " where a.City_ID =b.Cit_ID "
                                   + "    and city_id in (select distinct city_id from cust_city_online where cust_id = " + cust_id
                                   + " ) ";
                }
                else
                {
                    sxl_text = " select Country_System_ID , Country_ID, System_ID,City_ID,Cit_AName,Cit_EName from Country_System_Tbl a,Cit_Tbl b "
                                    + " where a.City_ID =b.Cit_ID "
                                    + "    and city_id in (select distinct city_id from Sub_CUST_CITY_ONLINE  where cust_id = " + cust_id
                                    + " ) ";
                                   
                }

                connection.SqlExec(sxl_text, "cust_cit_tbl");
                Cbo_city.DataSource = connection.SQLDS.Tables["cust_cit_tbl"];
                Cbo_city.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                Cbo_city.ValueMember = "CITY_ID";
            }

            else
            {
                Cbo_city.DataSource = connection.SQLDS.Tables["Rem_Confirmation_tbl2"];
                Cbo_city.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";
                Cbo_city.ValueMember = "CITY_ID";
            }

        }



        private void CboCust_Id_SelectedValueChanged(object sender, EventArgs e)
         {

            if (ch)
            {
                fill();
            }

        }

        private void cbo_order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Order_type = Convert.ToInt32(cbo_order_type.SelectedIndex);
            try
            {
                if (cbo_order_type.SelectedIndex == 0) //فروع 
 
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Confirmation11"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "Cust_Online_Main_Id", "cust_id", "rem_search").Select("rem_search = " + Order_type).CopyToDataTable();


                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                        ch1 = true;
                    }
                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }

                }

                else
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Confirmation11"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "Cust_Online_Main_Id", "cust_id", "rem_search").Select("rem_search = " + Order_type).CopyToDataTable();

                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;

                    }

                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }
                }
            }
            catch { Grd_Sub_Cust.DataSource = new BindingSource(); }

            Txt_Sub_Cust.Text = "";
        }

        private void Txt_Sub_Cust_TextChanged(object sender, EventArgs e)
        {
            Get_details_Info();
        }

        private void Get_details_Info()
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);

                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "(Acust_name like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " cust_id > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }

        private void Grd_Sub_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (ch)
            {
                fill();
            }
                GrdRem_Details.DataSource = new BindingSource();

        }

        private void btn_reject_Click(object sender, EventArgs e)
        {
            if (GrdRem_Details.RowCount > 0)
            {
                Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);  //new
                Int64 t_id1 = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["T_id"]); //new


                try
                {

                    DT_final = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "rem_no", "transfer_flag", "cust_online_id", "Check_id","case_id").Select("Check_id > 0").CopyToDataTable();
                    DT_final = DT_final.DefaultView.ToTable(false, "rem_no", "transfer_flag", "cust_online_id", "case_id").Select().CopyToDataTable();
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار الحوالة  " : "Choose the Rimmetance Please", MyGeneral_Lib.LblCap);
                    return;
                }

                if (DT_final.Rows.Count > 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار حوالة واحدة  فقط" : "Choose five Rimmetance only Please", MyGeneral_Lib.LblCap);
                    return;
                }

                DataTable Dt_new = new DataTable();
                try
                {

                    Dt_new = connection.SQLDS.Tables["Main_Verrifications_search_Web_tbl"].DefaultView.ToTable(false, "Check_id", "vo_no", "S_per_id", "r_per_id", "r_cur_id", "R_amount", "S_name", "R_name", "Oper_id").Select("Check_id > 0").CopyToDataTable();
                }
                catch
                { }
                if (Dt_new.Rows.Count > 0)
                {

                    DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? " هل تريد الرفض فعلا" : "Do you want to confirm", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (Dr == DialogResult.Yes)
                    {
                        Refuse_Reson AddFrm = new Refuse_Reson();
                        AddFrm.ShowDialog(this);


                        try
                        {

                            connection.SQLCS.Open();
                            connection.SQLCMD.CommandText = "[dbo].[Ubnormal_Refuse_confirm]";
                            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                            connection.SQLCMD.Connection = connection.SQLCS;
                            connection.SQLCMD.Parameters.AddWithValue("@rem_no", DT_final.Rows[0]["rem_no"]);
                            connection.SQLCMD.Parameters.AddWithValue("@case_id", DT_final.Rows[0]["case_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                            connection.SQLCMD.Parameters.AddWithValue("@note_user", Refuse_Reson.ab_Notes_BL.Trim());
                            connection.SQLCMD.Parameters.AddWithValue("@note_prog", "Ubnormal Confirm Rem.");
                            connection.SQLCMD.Parameters.AddWithValue("@oper_id", Dt_new.Rows[0]["oper_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@vo_no", Dt_new.Rows[0]["vo_no"]);
                            connection.SQLCMD.Parameters.AddWithValue("@per_id", cbo_rem_type.SelectedIndex == 0 ? Dt_new.Rows[0]["S_per_id"] : Dt_new.Rows[0]["r_per_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", Dt_new.Rows[0]["r_cur_id"]);
                            connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount", Dt_new.Rows[0]["r_amount"]);
                            connection.SQLCMD.Parameters.AddWithValue("@Per_Name", cbo_rem_type.SelectedIndex == 0 ? Dt_new.Rows[0]["S_name"] : Dt_new.Rows[0]["R_name"]);
                            connection.SQLCMD.Parameters.AddWithValue("@r_type_id", rem_type);
                            connection.SQLCMD.Parameters.AddWithValue("@transfer_flag ", DT_final.Rows[0]["transfer_flag"]);
                            connection.SQLCMD.Parameters.AddWithValue("@s_city_id", Cbo_city.SelectedValue);
                            MyGeneral_Lib.Copytocliptext("Ubnormal_Refuse_confirm", connection.SQLCMD);


                            IDataReader obj = connection.SQLCMD.ExecuteReader();

                            obj.Close();
                            connection.SQLCS.Close();
                            connection.SQLCMD.Parameters.Clear();
                            connection.SQLCMD.Dispose();


                            MessageBox.Show(connection.Lang_id == 1 ? " تم رفض الحوالة" : "Remittance Rejected", MyGeneral_Lib.LblCap);
                        }

                        catch
                        {
                            connection.SQLCS.Close();
                            connection.SQLCMD.Parameters.Clear();
                            connection.SQLCMD.Dispose();
                        }
                        Rem_Confirmation_Load(null, null);
                    }
                }

                else { MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap); }
            }
        }

    }
}