﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class ADD_Incoming_Rem : Form
    {
        BindingSource _Bs_Incom_Rem = new BindingSource();
        BindingSource binding_cur_bsrem = new BindingSource();
        BindingSource _BS_sub_cust = new BindingSource();
        BindingSource _BS_R_cur_id = new BindingSource();
        BindingSource binding_cbo_tcity = new BindingSource();
        BindingSource binding_Rem_Pay_Type = new BindingSource();
        decimal Mdiff_Amoun = 0;
        bool chang_cur_rrate = false;
        bool chang_procer = false;
        string Exch_rate_com = "";
        decimal param_Exch_rate_rem = 0;
        int Cust_id_online = 0;
        string SqlTxt = "";
        int r_per_id = 0;
        Int16 r_Gender_id =1;
        int Oper_Id = 0;
        string _Date = "";
        double Rem_Amount = 0;
        Int16 Rem_Flag = 0;
        string rem_no = "";
        string local_Cur = "";
        string term = "";
        string forgin_Cur = "";
        string User = "";
        int Vo_No = 0;
        public ADD_Incoming_Rem()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_procer.AutoGenerateColumns = false;

            if (connection.Lang_id != 1)
            {
                Cbo_Oper.Items[0] = "Cash";
                Cbo_Oper.Items[1] = "Daily";
                Cbo_Oper.Items[2] = "Daily(Agent)";
            }
        }

        private void ADD_Incoming_Rem_Load(object sender, EventArgs e)
        {
            Btn_Add.Enabled = false;
            string sql_report_txt = "select * from Reports_setting_Tbl";
            connection.SqlExec(sql_report_txt, "Reports_setting_Tbl");
            if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار اعدادات الطباعة" : "please add reporting setting", MyGeneral_Lib.LblCap);
                this.Close();
            }
            else
            {
                SqlTxt = " Select  cust_online_id ,cust_id From  TERMINALS where t_id = " + connection.T_ID;
                connection.SqlExec(SqlTxt, "Cust_tbl");

                connection.SqlExec("exec Get_information_inrem " + Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["cust_id"]), "Get_info_Tbl");

                binding_cbo_tcity.DataSource = connection.SQLDS.Tables["Get_info_Tbl"].Select("City_id  > 0 ").CopyToDataTable();
                Cbo_city.DataSource = binding_cbo_tcity.DataSource;
                Cbo_city.ValueMember = "CITY_ID";
                Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";

                Cbo_Oper.SelectedIndex = 0;

                binding_Rem_Pay_Type.DataSource = connection.SQLDS.Tables["Get_info_Tbl4"];
                Cbo_Rem_Pay_Type.DataSource = binding_Rem_Pay_Type;
                Cbo_Rem_Pay_Type.ValueMember = "rem_pay_id";
                Cbo_Rem_Pay_Type.DisplayMember = connection.Lang_id == 1 ? "rem_pay_aname" : "rem_pay_ename";

            }
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {

            string[] Str = { "Get_Incom_Rem_TBl"};
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
            Btn_Add.Enabled = false;
            Cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["cust_online_id"]);
            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "[Serach_InComing_Rem_Web]";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.CommandTimeout = 0;
            connection.SQLCMD.Parameters.AddWithValue("@rem_no",  Txt_Rem_No.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@code_rem", txt_code_rem.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@cust_online_id", Cust_id_online);
            connection.SQLCMD.Parameters.AddWithValue("@T_city_Id",Cbo_city.SelectedValue );
            connection.SQLCMD.Parameters.AddWithValue("@Param_Result", "");
            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Get_Incom_Rem_TBl");
            obj.Close();
            connection.SQLCS.Close();
           

               if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Btn_Add.Enabled = false;
                    return;
                }

               connection.SQLCMD.Parameters.Clear();
               connection.SQLCMD.Dispose();

           



            if (connection.SQLDS.Tables["Get_Incom_Rem_TBl"].Rows.Count > 0)
            {

                _Bs_Incom_Rem.DataSource = connection.SQLDS.Tables["Get_Incom_Rem_TBl"];
                Btn_Add.Enabled = true;
                

                TxtS_name.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                TxtScity.DataBindings.Clear();
               
                Txt_S_Birth.DataBindings.Clear();
                Txts_Suburb.DataBindings.Clear();
                Txts_Street.DataBindings.Clear();
                Txts_State.DataBindings.Clear();
                Txts_Post_Code.DataBindings.Clear();
                Txt_s_job.DataBindings.Clear();
                txt_sbirth_place.DataBindings.Clear();
                Txt_S_doc_type.DataBindings.Clear();
                Txt_S_doc_no.DataBindings.Clear();
                Txt_Doc_S_Date.DataBindings.Clear();
                Txt_Doc_S_Exp.DataBindings.Clear();
                Txt_S_doc_issue.DataBindings.Clear();
                Txt_Relionship.DataBindings.Clear();
                Txt_Reciever.DataBindings.Clear();
                Txt_R_Nat.DataBindings.Clear();
                Txt_R_City.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                Txt_r_birthdate.DataBindings.Clear();
                Txtr_Suburb.DataBindings.Clear();
                Txtr_Street.DataBindings.Clear();
                Txtr_State.DataBindings.Clear();
                Txtr_Post_Code.DataBindings.Clear();
                Txt_R_job.DataBindings.Clear();
                txt_rbirth_place.DataBindings.Clear();
                Txt_r_Doc_Type.DataBindings.Clear();
                Txt_r_Doc_No.DataBindings.Clear();
                Txt_Doc_r_Date.DataBindings.Clear();
                Txt_Doc_r_Exp.DataBindings.Clear();
                Txt_Doc_r_Issue.DataBindings.Clear();
                Txt_mail.DataBindings.Clear();
                txt_Mother_name.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();
                 Txt_Soruce_money.DataBindings.Clear();
                Txt_notes.DataBindings.Clear();
                Txtr_amount.DataBindings.Clear();
                Txt_r_cur_name.DataBindings.Clear();

                TxtS_name.DataBindings.Add("Text", _Bs_Incom_Rem, "S_name");
                Txts_nat.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                TxtS_phone.DataBindings.Add("Text", _Bs_Incom_Rem, "S_phone");
                TxtScity.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "SA_city_aname" : "SA_city_ename");
                Txt_S_Birth.DataBindings.Add("Text", _Bs_Incom_Rem, "sbirth_date");
                Txts_Suburb.DataBindings.Add("Text", _Bs_Incom_Rem, "S_Suburb");
                Txts_Street.DataBindings.Add("Text", _Bs_Incom_Rem, "S_Street");
                Txts_State.DataBindings.Add("Text", _Bs_Incom_Rem, "S_State");
                Txts_Post_Code.DataBindings.Add("Text", _Bs_Incom_Rem, "S_Post_Code");

                Txt_s_job.DataBindings.Add("Text", _Bs_Incom_Rem, "s_job");
                txt_sbirth_place.DataBindings.Add("Text", _Bs_Incom_Rem, "sbirth_place");
                Txt_S_doc_type.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "SFRM_ADOC_NA" : "SFRM_ADOC_NE");  
                Txt_S_doc_no.DataBindings.Add("Text", _Bs_Incom_Rem, "S_doc_no"); 
                Txt_Doc_S_Date.DataBindings.Add("Text", _Bs_Incom_Rem, "S_doc_ida"); 
                Txt_Doc_S_Exp.DataBindings.Add("Text", _Bs_Incom_Rem, "S_doc_eda"); 
                Txt_S_doc_issue.DataBindings.Add("Text", _Bs_Incom_Rem, "s_doc_issue") ;
                Txt_Relionship.DataBindings.Add("Text", _Bs_Incom_Rem, "Relation_S_R");
                Txt_Reciever.DataBindings.Add("Text", _Bs_Incom_Rem, "r_name");
                Txt_R_Nat.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "rnat_name" : "renat_name");
                Txt_R_City.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "rcity_name" : "recity_name");
                Txt_R_Phone.DataBindings.Add("Text", _Bs_Incom_Rem, "r_phone");
                Txt_r_birthdate.DataBindings.Add("Text", _Bs_Incom_Rem, "rec_birth_date");
                Txtr_Suburb.DataBindings.Add("Text", _Bs_Incom_Rem, "r_Suburb");
                Txtr_Street.DataBindings.Add("Text", _Bs_Incom_Rem, "r_Street");
                Txtr_State.DataBindings.Add("Text", _Bs_Incom_Rem, "r_State");
                Txtr_Post_Code.DataBindings.Add("Text", _Bs_Incom_Rem, "r_Post_Code");
                Txt_R_job.DataBindings.Add("Text", _Bs_Incom_Rem, "r_job");
                txt_rbirth_place.DataBindings.Add("Text", _Bs_Incom_Rem, "TBirth_Place");
                Txt_r_Doc_Type.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "rFRM_ADOC_NA" : "rFRM_ADOC_NE");  
                Txt_r_Doc_No.DataBindings.Add("Text", _Bs_Incom_Rem, "r_doc_no");
                Txt_Doc_r_Date.DataBindings.Add("Text", _Bs_Incom_Rem, "r_doc_ida");
                Txt_Doc_r_Exp.DataBindings.Add("Text", _Bs_Incom_Rem, "r_doc_eda");
                Txt_Doc_r_Issue.DataBindings.Add("Text", _Bs_Incom_Rem, "r_doc_issue");
                Txt_mail.DataBindings.Add("Text", _Bs_Incom_Rem, "R_email");
                txt_Mother_name.DataBindings.Add("Text", _Bs_Incom_Rem, "r_Mother_name");
                Txt_Purpose.DataBindings.Add("Text", _Bs_Incom_Rem, "T_purpose");
                Txt_Soruce_money.DataBindings.Add("Text", _Bs_Incom_Rem, "Source_money");
                Txt_notes.DataBindings.Add("Text", _Bs_Incom_Rem, "R_notes"); 
                //------------------------
                Txtr_amount.DataBindings.Add("Text", _Bs_Incom_Rem, "r_Amount");
                Txt_r_cur_name.DataBindings.Add("Text", _Bs_Incom_Rem, connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME");


                try
                {
                    SqlTxt = "  SELECT  top 1   isnull(Per_ID,0) as R_per_id ,isnull( per_Gender_ID  ,1) as per_Gender_ID , Per_AName, Per_EName FROM  Person_Info "
                    + " where Per_AName like '%" + ((DataRowView)_Bs_Incom_Rem.Current).Row["R_name"].ToString() + "%' order by C_date desc";
                    connection.SqlExec(SqlTxt, "R_per_id_tbl");
                    r_per_id = Convert.ToInt32(connection.SQLDS.Tables["R_per_id_tbl"].Rows[0]["R_per_id"]);
                    r_Gender_id = Convert.ToInt16(connection.SQLDS.Tables["R_per_id_tbl"].Rows[0]["per_Gender_ID"]);
                }
                catch
                {
                    r_per_id = 0;
                    r_Gender_id = 1;
                }
                
            }
            else
            {
                Btn_Add.Enabled = false;
                MessageBox.Show(connection.Lang_id == 2 ? "no Rem. for this Condition" : "لا توجد حوالة تحقق الشروط", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            #region valdiation

            if (TxtBox_User.Text == "" && Cbo_Oper.SelectedIndex == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if ((Cbo_Oper.SelectedIndex == 1 || Cbo_Oper.SelectedIndex == 2 ) && Convert.ToInt16(Grd_procer.RowCount) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "you must choose the broker Customer " : "يجب اختيار الوسيط ", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (checkBox1.Checked == false && (Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["r_CUR_ID"]) != Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["pr_CUR_ID"])))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Delivery of the remmittance in local currency" : "تسليم الحوالة بالعملة المحلية", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) > Convert.ToDecimal(txt_maxrate_rem.Text))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Can not be a process because the Buy Price above the upper limit" : "لايمكن اجراء العملية لان سعر الشراء اعلى من الحد الاعلى", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) < Convert.ToDecimal(txt_minrate_rem.Text))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Please pay attention to Buy Price less than the minimum" : "يرجى الانتباة الى سعر الشراء اقل من الحد الادنى", MyGeneral_Lib.LblCap);
            }
            #endregion
            string sen = ((DataRowView)_Bs_Incom_Rem.Current).Row["S_name"].ToString();
            string rec = ((DataRowView)_Bs_Incom_Rem.Current).Row["r_name"].ToString();
            string mv_notes = " :ح-مر" + sen + "," + " :مس " + rec;

            if (Cbo_Oper.SelectedIndex == 0)
            {

               

                ArrayList ItemList1 = new ArrayList();

                ItemList1.Insert(0, ((DataRowView)_Bs_Incom_Rem.Current).Row["r_CUR_ID"]);// @for_cur_id
                ItemList1.Insert(1, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_amount"]);//@for_amount
                ItemList1.Insert(2, Txtr_State.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());//@r_address
                ItemList1.Insert(3, Txt_r_Doc_No.Text);//@r_doc_no	
                ItemList1.Insert(4, Txt_Doc_r_Date.Text);//Txt_Doc_r_Date.Value.Date.ToString(@format_invers1)//@r_doc_ida
                ItemList1.Insert(5, Txt_Doc_r_Exp.Text);//Txt_Doc_r_Exp.Value.Date.ToString(@format_invers1)//@r_doc_eda
                ItemList1.Insert(6, Txt_Doc_r_Issue.Text);//@r_doc_issue
                ItemList1.Insert(7, ((DataRowView)_Bs_Incom_Rem.Current).Row["rFrm_doc_id"]);//@rfrm_doc_id
                ItemList1.Insert(8, ((DataRowView)_Bs_Incom_Rem.Current).Row["rnat_Id"]); //@rnat_id	
                ItemList1.Insert(9, Txt_Reciever.Text.Trim());//@r_name	
                ItemList1.Insert(10, Txt_R_Phone.Text);   //@r_phone	
                ItemList1.Insert(11, 11); //@rem_flag	
                ItemList1.Insert(12, connection.T_ID);//@T_Id		
                ItemList1.Insert(13, connection.user_id); //@User_Id
                ItemList1.Insert(14, TxtIn_Rec_Date.Text); //@nrec_date			int 
                ItemList1.Insert(15, ((DataRowView)_Bs_Incom_Rem.Current).Row["rem_no"]); //@rem_no    
                ItemList1.Insert(16, mv_notes); //@v_notes	
                ItemList1.Insert(17, Txt_r_birthdate.Text);////Txt_rbirth_Date.Value.Date.ToString(@format_invers1) //@rbirth_date
                ItemList1.Insert(18, ((DataRowView)_Bs_Incom_Rem.Current).Row["rem_id"]);  //@rem_id
                ItemList1.Insert(19, Txt_notes.Text.Trim()); //@notes
                ItemList1.Insert(20, "2");  //@case_id		
                ItemList1.Insert(21, connection.login_Id);//@login_ID
                ItemList1.Insert(22, connection.Cust_online_Id);//@cust_ID_online
                ItemList1.Insert(23, ((DataRowView)_Bs_Incom_Rem.Current).Row["Code_rem"]);//@code_rem
                ItemList1.Insert(24, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["t_city_ID"]));////@MT_city_id------------مهم t_city_id
                ItemList1.Insert(25, Txt_R_job.Text.Trim()); //@r_job
                ItemList1.Insert(26, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["r_city_ID"]));//@r_city_id 
                ItemList1.Insert(27, "");   // Param_Result rem_n
                ItemList1.Insert(28, "");   // Param_Result1 vo_no
                ItemList1.Insert(29, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//@Paid_Rate -----------------------------?
                ItemList1.Insert(30, Txtr_Street.Text.Trim());//@r_Street	
                ItemList1.Insert(31, Txtr_Suburb.Text.Trim()); //@r_Suburb	
                ItemList1.Insert(32, Txtr_Post_Code.Text.Trim()); //@r_Post_Code
                ItemList1.Insert(33, Txtr_State.Text.Trim());  //@r_State	
                ItemList1.Insert(34, r_per_id); //@R_per_id	**??
                ItemList1.Insert(35, "");//---@Er_name -- يجب رفعة
                ItemList1.Insert(36, Txt_mail.Text.Trim()); //@per_Email
                ItemList1.Insert(37, txt_Mother_name.Text.Trim());//---@per_Mother_name
                ItemList1.Insert(38, r_Gender_id);//---@per_Gender_ID  
                ItemList1.Insert(39, ((DataRowView)_Bs_Incom_Rem.Current).Row["rcity_name"]);//---@r_ACITY_NAME
                ItemList1.Insert(40, ((DataRowView)_Bs_Incom_Rem.Current).Row["recity_name"]);//---@r_ECITY_NAME
                ItemList1.Insert(41, ((DataRowView)_Bs_Incom_Rem.Current).Row["rcoun_name"]);//---@r_ACOUN_NAME
                ItemList1.Insert(42, ((DataRowView)_Bs_Incom_Rem.Current).Row["recoun_name"]);//---@r_ECOUN_NAME
                ItemList1.Insert(43, ((DataRowView)_Bs_Incom_Rem.Current).Row["rFRM_ADOC_NA"]);//---@rFRM_ADOC_NA
                ItemList1.Insert(44, ((DataRowView)_Bs_Incom_Rem.Current).Row["rFRM_ADOC_NE"]);//---@rFRM_EDOC_NA
                ItemList1.Insert(45, ((DataRowView)_Bs_Incom_Rem.Current).Row["rnat_name"]);//---@r_A_NAT_NAME
                ItemList1.Insert(46, ((DataRowView)_Bs_Incom_Rem.Current).Row["renat_name"]);//---@r_E_NAT_NAME
                ItemList1.Insert(47, checkBox1.Checked == true ? 1 : 0);//---@@buy_sal_flag
                ItemList1.Insert(48, checkBox1.Checked == true ? param_Exch_rate_rem : 0);//---@rem_amount_exch
                ItemList1.Insert(49, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//-----@@rem_amount_real
                ItemList1.Insert(50, 0);//---@rem_amount_exch
                ItemList1.Insert(51, 0);//---@rem_amount_real
                ItemList1.Insert(52, connection.Loc_Cur_Id);//---@LOC_CUR_ID
                ItemList1.Insert(53, 1);//---@Catg_Id
                ItemList1.Insert(54, connection.Box_Cust_Id);//---@@cust_box_id
                ItemList1.Insert(55, Convert.ToDecimal(Txt_Tot_amount.Text.Trim()));//---@tot_loc_amount
                ItemList1.Insert(56, TxtS_name.Text.Trim());//---@s_name
                ItemList1.Insert(57, TxtS_phone.Text.Trim());//---@s_phone
                ItemList1.Insert(58, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["s_city_id"]));//---@s_city_id
                ItemList1.Insert(59, Txt_S_doc_no.Text.Trim());//---@s_doc_no
                ItemList1.Insert(60, Txt_Doc_S_Date.Text);//---@@s_doc_ida
                ItemList1.Insert(61, Txt_Doc_S_Exp.Text);//---@s_doc_eda
                ItemList1.Insert(62, Txt_S_doc_issue.Text.Trim());//---@s_doc_issue
                ItemList1.Insert(63, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["sfrm_doc_id"]));//---@sfrm_doc_id
                ItemList1.Insert(64, ((DataRowView)_Bs_Incom_Rem.Current).Row["snat_id"]);//---@snat_id
                ItemList1.Insert(65, Txt_Purpose.Text.Trim());//---@t_purpose
                ItemList1.Insert(66, Txt_S_Birth.Text);//---@sbirth_date
                ItemList1.Insert(67, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["Pr_cur_id"]));//---@Pr_cur_id
                ItemList1.Insert(68, txt_sbirth_place.Text.Trim());//---@sbirth_place
                ItemList1.Insert(69, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["sa_city_id"]));//---@sa_city_id
                ItemList1.Insert(70, Txt_s_job.Text.Trim());//---@job_sen
                ItemList1.Insert(71, Txt_Soruce_money.Text);//---@Soruce_money
                ItemList1.Insert(72, Txt_Relionship.Text);//---@Relionship
                ItemList1.Insert(73, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["Case_purpose_id"]));//---@Case_purpose_id
                ItemList1.Insert(74, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_ACUR_NAME"]);//---@R_ACUR_NAME
                ItemList1.Insert(75, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_ECUR_NAME"]);//---@R_ECUR_NAME
                ItemList1.Insert(76, ((DataRowView)_Bs_Incom_Rem.Current).Row["PR_ACUR_NAME"]);//---@PR_ACUR_NAME
                ItemList1.Insert(77, ((DataRowView)_Bs_Incom_Rem.Current).Row["PR_ECUR_NAME"]);//---@PR_ECUR_NAME
                ItemList1.Insert(78, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ACITY_NAME"]);//---@S_ACITY_NAME
                ItemList1.Insert(79, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ECITY_NAME"]);//---@S_ECITY_NAME
                ItemList1.Insert(80, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ACOUN_NAME"]);//---@S_ACOUN_NAME
                ItemList1.Insert(81, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ECOUN_NAME"]);//---@S_ECOUN_NAME
                ItemList1.Insert(82, Txts_Street.Text.Trim());//@r_Street	
                ItemList1.Insert(83, Txts_Suburb.Text.Trim()); //@r_Suburb	
                ItemList1.Insert(84, Txts_Post_Code.Text.Trim()); //@r_Post_Code
                ItemList1.Insert(85, Txts_State.Text.Trim());  //@r_State	
                ItemList1.Insert(86, ((DataRowView)_Bs_Incom_Rem.Current).Row["SFRM_ADOC_NA"]);//---@sFRM_ADOC_NA
                ItemList1.Insert(87, ((DataRowView)_Bs_Incom_Rem.Current).Row["sFRM_EDOC_NA"]);//---@sFRM_EDOC_NA
                ItemList1.Insert(88, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_A_NAT_NAME"]);//---@s_A_NAT_NAME
                ItemList1.Insert(89, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_E_NAT_NAME"]);//---@s_E_NAT_NAME
                ItemList1.Insert(90, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ACITY_NAME"]);//---@t_ACITY_NAME  
                ItemList1.Insert(91, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ECITY_NAME"]);//---@t_ECITY_NAME
                ItemList1.Insert(92, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ACITY_NAME"]);//---@Sa_ACITY_NAME
                ItemList1.Insert(93, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ECITY_NAME"]);//---@Sa_ECITY_NAME
                ItemList1.Insert(94, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ACOUN_NAME"]);//---@Sa_ACOUN_NAME
                ItemList1.Insert(95, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ECOUN_NAME"]);//---@Sa_ECOUN_NAME
                ItemList1.Insert(96, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ACOUN_NAME"]);//---@t_ACOUN_NAME
                ItemList1.Insert(97, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ECOUN_NAME"]);//---@t_ECOUN_NAME
                ItemList1.Insert(98, ((DataRowView)_Bs_Incom_Rem.Current).Row["Case_purpose_Aname"]);//---@Case_purpose_Aname
                ItemList1.Insert(99, ((DataRowView)_Bs_Incom_Rem.Current).Row["Case_purpose_Ename"]);//---@Case_purpose_Ename
                ItemList1.Insert(100, Convert.ToDecimal(TxtDiscount_Amount.Text));//---@TxtDiscount_Amount
                ItemList1.Insert(101, txt_rbirth_place.Text); //@rbirth_place
                ItemList1.Insert(102, connection.User_Name);//@User_Name
                ItemList1.Insert(103, 1);//@loc_Inter_flag
                ItemList1.Insert(104, connection.Lang_id); //@lang_id
                ItemList1.Insert(105, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Mother_name"]); //@S_Mother_name
                ItemList1.Insert(106, "");//@S_EName ** تم رفعة	
                ItemList1.Insert(107, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Gender_Aname"]);
                ItemList1.Insert(108, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Gender_Ename"]);
                ItemList1.Insert(109, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_Gender_Aname"]);
                ItemList1.Insert(110, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_Gender_Ename"]);
                ItemList1.Insert(111, ((DataRowView)_Bs_Incom_Rem.Current).Row["Receiver_Code_phone"]); //@Receiver_Code_phone
                ItemList1.Insert(112, ((DataRowView)_Bs_Incom_Rem.Current).Row["sender_Code_phone"]); //@sender_Code_phone 
                ItemList1.Insert(113, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Acust_name"]);//@S_Acust_name
                ItemList1.Insert(114, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ecust_name"]); //@S_ecust_name 
                ItemList1.Insert(115, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_Email"]);//@s_Email	
                ItemList1.Insert(116, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_notes"]); //@s_notes
                ItemList1.Insert(117, connection.Loc_Cur_Id); //@cur_rem_amount_cust
                ItemList1.Insert(118, ((DataRowView)_Bs_Incom_Rem.Current).Row["i_ocomm"]); //@MIN_COMM_Cs_in
                ItemList1.Insert(119, ((DataRowView)_Bs_Incom_Rem.Current).Row["i_Cur_Id"]); //@mIN_COMM_CUR
                ItemList1.Insert(120, Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue));//rem_pay_id 
                ItemList1.Insert(121, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_aname"]);//---@rem_pay_aname
                ItemList1.Insert(122, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_ename"]);//---@rem_pay_ename


                MyGeneral_Lib.Copytocliptext("Add_incoming_Voucher", ItemList1);
                connection.scalar("Add_incoming_Voucher", ItemList1);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Btn_Add.Enabled = true;
                    return;

                }

                connection.SQLCMD.Parameters.Clear();
                Btn_Add.Enabled = true;
                print_Rpt_Pdf_NEW();
                this.Close();


            }
            else
            {

                    string Agent_user_Name = "";
                    Agent_user_Name = ((DataRowView)_Bs_Incom_Rem.Current).Row["login_name"].ToString();

                    ArrayList ItemList1 = new ArrayList();
                    ItemList1.Insert(0, ((DataRowView)_Bs_Incom_Rem.Current).Row["r_CUR_ID"]);//            @for_cur_id	
                    ItemList1.Insert(1, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_amount"]);//@for_amount
                    ItemList1.Insert(2, Txtr_State.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());//@r_address
                    ItemList1.Insert(3, Txt_r_Doc_No.Text);//@r_doc_no	
                    ItemList1.Insert(4, Txt_Doc_r_Date.Text);////@r_doc_ida	
                    ItemList1.Insert(5, Txt_Doc_r_Exp.Text);//@r_doc_eda
                    ItemList1.Insert(6, Txt_Doc_r_Issue.Text);//@r_doc_issue
                    ItemList1.Insert(7, ((DataRowView)_Bs_Incom_Rem.Current).Row["rFrm_doc_id"]);//@rfrm_doc_id
                    ItemList1.Insert(8, ((DataRowView)_Bs_Incom_Rem.Current).Row["rnat_Id"]);//@rnat_id
                    ItemList1.Insert(9, Txt_Reciever.Text.Trim());//@r_name
                    ItemList1.Insert(10, Txt_R_Phone.Text);//@r_phone	
                    ItemList1.Insert(11, 11);//@rem_flag	
                    ItemList1.Insert(12, connection.T_ID);//@T_Id		
                    ItemList1.Insert(13,connection.user_id );//user_id
                    ItemList1.Insert(14, TxtIn_Rec_Date.Text);//nrec_date
                    ItemList1.Insert(15, ((DataRowView)_Bs_Incom_Rem.Current).Row["rem_no"]);//@rem_no 
                    ItemList1.Insert(16, mv_notes);//@v_notes
                    ItemList1.Insert(17, Txt_r_birthdate.Text);//@rbirth_date		
                    ItemList1.Insert(18, ((DataRowView)_Bs_Incom_Rem.Current).Row["rem_id"]);//@rem_id
                    ItemList1.Insert(19, Txt_notes.Text.Trim());//@notes	
                    ItemList1.Insert(20, "2");//@case_id
                    ItemList1.Insert(21,  connection.login_Id );//@login_ID	
                    ItemList1.Insert(22, connection.Cust_online_Id);//@cust_ID_online
                    ItemList1.Insert(23, ((DataRowView)_Bs_Incom_Rem.Current).Row["Code_rem"]);//@code_rem  
                    ItemList1.Insert(24, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["t_city_ID"]));//@MT_city_id//--------------------------------------------------------مهم t_city_id
                    ItemList1.Insert(25, Txt_R_job.Text.Trim());//@r_job
                    ItemList1.Insert(26, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["r_city_ID"]));//@r_city_id  
                    ItemList1.Insert(27, "");   // Param_Result rem_n
                    ItemList1.Insert(28, "");   // Param_Result2 vo_no
                    ItemList1.Insert(29, 0);//@Paid_Rate  -----------------------------?
                    ItemList1.Insert(30, Txtr_Street.Text.Trim());//@r_Street		
                    ItemList1.Insert(31, Txtr_Suburb.Text.Trim());//@r_Suburb	
                    ItemList1.Insert(32, Txtr_Post_Code.Text.Trim());//@r_Post_Code
                    ItemList1.Insert(33, Txtr_State.Text.Trim());//@r_State	
                    ItemList1.Insert(34, r_per_id);//---@R_per_id
                    ItemList1.Insert(35, "");//---@Er_name تم رفعة
                    ItemList1.Insert(36, Txt_mail.Text.Trim());//@per_Email    
                    ItemList1.Insert(37, txt_Mother_name.Text.Trim());//---@per_Mother_name
                    ItemList1.Insert(38, r_Gender_id);//@per_Gender_ID 
                    ItemList1.Insert(39, ((DataRowView)_Bs_Incom_Rem.Current).Row["rcity_name"]);//---@r_ACITY_NAME
                    ItemList1.Insert(40, ((DataRowView)_Bs_Incom_Rem.Current).Row["recity_name"]);//---@r_ECITY_NAME
                    ItemList1.Insert(41, ((DataRowView)_Bs_Incom_Rem.Current).Row["rcoun_name"]);//---@r_ACOUN_NAME
                    ItemList1.Insert(42, ((DataRowView)_Bs_Incom_Rem.Current).Row["recoun_name"]);//---@r_ECOUN_NAME
                    ItemList1.Insert(43, ((DataRowView)_Bs_Incom_Rem.Current).Row["rFRM_ADOC_NA"]);//---@rFRM_ADOC_NA
                    ItemList1.Insert(44, ((DataRowView)_Bs_Incom_Rem.Current).Row["rFRM_ADOC_NE"]);//---@rFRM_EDOC_NA
                    ItemList1.Insert(45, ((DataRowView)_Bs_Incom_Rem.Current).Row["rnat_name"]);//---@r_A_NAT_NAME
                    ItemList1.Insert(46, ((DataRowView)_Bs_Incom_Rem.Current).Row["renat_name"]);//---@r_E_NAT_NAME
                    ItemList1.Insert(47, checkBox1.Checked == true ? 1 : 0);//@buy_sal_flag	
                    ItemList1.Insert(48, 1);//---@Catg_Id
                    ItemList1.Insert(49, 0);//---@@sub_cust_id    
                    ItemList1.Insert(50, TxtS_name.Text.Trim());//@s_name
                    ItemList1.Insert(51, TxtS_phone.Text.Trim());//@s_phone
                    ItemList1.Insert(52, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["s_city_id"]));//@s_city_id
                    ItemList1.Insert(53, Txt_S_doc_no.Text.Trim());//@s_doc_no	
                    ItemList1.Insert(54, Txt_Doc_S_Date.Text);//@s_doc_ida
                    ItemList1.Insert(55, Txt_Doc_S_Exp.Text);//@s_doc_eda
                    ItemList1.Insert(56, Txt_S_doc_issue.Text.Trim());//---@s_doc_issue
                    ItemList1.Insert(57, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["sfrm_doc_id"]));//@sfrm_doc_id
                    ItemList1.Insert(58, ((DataRowView)_Bs_Incom_Rem.Current).Row["snat_id"]);//---@snat_id
                    ItemList1.Insert(59, Txt_Purpose.Text.Trim());//@t_purpose
                    ItemList1.Insert(60, Txt_S_Birth.Text);//@sbirth_date
                    ItemList1.Insert(61, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["Pr_cur_id"]));//---@Pr_cur_id
                    ItemList1.Insert(62, txt_sbirth_place.Text.Trim());//---@sbirth_place
                    ItemList1.Insert(63, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["sa_city_id"]));//@sa_city_id
                    ItemList1.Insert(64, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_job"]);//---@job_sen
                    ItemList1.Insert(65, ((DataRowView)_Bs_Incom_Rem.Current).Row["Source_money"]);//---@Soruce_money
                    ItemList1.Insert(66, ((DataRowView)_Bs_Incom_Rem.Current).Row["Relation_S_R"]);//---@Relionship
                    ItemList1.Insert(67, Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["Case_purpose_id"]));//---@Case_purpose_id
                    ItemList1.Insert(68, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_ACUR_NAME"]);//---@R_ACUR_NAME
                    ItemList1.Insert(69, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_ECUR_NAME"]);//---@R_ECUR_NAME
                    ItemList1.Insert(70, ((DataRowView)_Bs_Incom_Rem.Current).Row["PR_ACUR_NAME"]);//---@PR_ACUR_NAME
                    ItemList1.Insert(71, ((DataRowView)_Bs_Incom_Rem.Current).Row["PR_ECUR_NAME"]);//---@PR_ECUR_NAME
                    ItemList1.Insert(72, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ACITY_NAME"]);//---@S_ACITY_NAME
                    ItemList1.Insert(73, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ECITY_NAME"]);//---@S_ECITY_NAME
                    ItemList1.Insert(74, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ACOUN_NAME"]);//---@S_ACOUN_NAME
                    ItemList1.Insert(75, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ECOUN_NAME"]);//---@S_ECOUN_NAME
                    ItemList1.Insert(76, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Street"]);//---@S_Street
                    ItemList1.Insert(77, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Suburb"]);//---@S_Suburb
                    ItemList1.Insert(78, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Post_Code"]);//---@S_Post_Code
                    ItemList1.Insert(79, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_State"]);//---@S_State
                    ItemList1.Insert(80, ((DataRowView)_Bs_Incom_Rem.Current).Row["SFRM_ADOC_NA"]);//---@sFRM_ADOC_NA
                    ItemList1.Insert(81, ((DataRowView)_Bs_Incom_Rem.Current).Row["sFRM_EDOC_NA"]);//---@sFRM_EDOC_NA
                    ItemList1.Insert(82, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_A_NAT_NAME"]);//---@s_A_NAT_NAME
                    ItemList1.Insert(83, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_E_NAT_NAME"]);//---@s_E_NAT_NAME
                    ItemList1.Insert(84, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ACITY_NAME"]);//---@t_ACITY_NAME  
                    ItemList1.Insert(85, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ECITY_NAME"]);//---@t_ECITY_NAME
                    ItemList1.Insert(86, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ACITY_NAME"]);//---@Sa_ACITY_NAME
                    ItemList1.Insert(87, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ECITY_NAME"]);//---@Sa_ECITY_NAME
                    ItemList1.Insert(88, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ACOUN_NAME"]);//---@Sa_ACOUN_NAME
                    ItemList1.Insert(89, ((DataRowView)_Bs_Incom_Rem.Current).Row["Sa_ECOUN_NAME"]);//---@Sa_ECOUN_NAME
                    ItemList1.Insert(90, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ACOUN_NAME"]);//---@t_ACOUN_NAME
                    ItemList1.Insert(91, ((DataRowView)_Bs_Incom_Rem.Current).Row["t_ECOUN_NAME"]);//---@t_ECOUN_NAME
                    ItemList1.Insert(92, ((DataRowView)_Bs_Incom_Rem.Current).Row["Case_purpose_Aname"]);//---@Case_purpose_Aname
                    ItemList1.Insert(93, ((DataRowView)_Bs_Incom_Rem.Current).Row["Case_purpose_Ename"]);//---@Case_purpose_Ename
                    //ItemList1.Insert(100, Convert.ToDecimal(TxtDiscount_Amount.Text));//---@TxtDiscount_Amount
                    ItemList1.Insert(94, txt_rbirth_place.Text);//@rbirth_place
                    ItemList1.Insert(95,  connection.User_Name  );//@User_Name
                    ItemList1.Insert(96, 1);//@loc_Inter_flag
                    ItemList1.Insert(97, connection.Lang_id);//@lang_id
                    ItemList1.Insert(98, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//@rem_amount_exch_cust	 سعر الحقيقي
                    ItemList1.Insert(99, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Mother_name"]);//@S_Mother_name
                    ItemList1.Insert(100, "");//@S_EName تم رفعة
                    ItemList1.Insert(101, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Gender_Aname"]);//@S_Gender_Aname
                    ItemList1.Insert(102, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Gender_Ename"]);//@S_Gender_Ename
                    ItemList1.Insert(103, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_Gender_Aname"]);//@R_Gender_Aname
                    ItemList1.Insert(104, ((DataRowView)_Bs_Incom_Rem.Current).Row["R_Gender_Ename"]);//@R_Gender_Ename
                    ItemList1.Insert(105, ((DataRowView)_Bs_Incom_Rem.Current).Row["Receiver_Code_phone"]);////@Receiver_Code_phone
                    ItemList1.Insert(106, ((DataRowView)_Bs_Incom_Rem.Current).Row["sender_Code_phone"]);//@sender_Code_phone
                    ItemList1.Insert(107, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_Email"]);//@s_Email
                    ItemList1.Insert(108, ((DataRowView)_Bs_Incom_Rem.Current).Row["s_notes"]);//@s_notes
                    ItemList1.Insert(109, 0);   //@Vo_No1
                    ItemList1.Insert(110, "");   //@Case_Date	
                    ItemList1.Insert(111, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_Acust_name"]);//@S_Acust_name
                    ItemList1.Insert(112, ((DataRowView)_Bs_Incom_Rem.Current).Row["S_ecust_name"]);//@S_Ecust_name
                    ItemList1.Insert(113, Grd_procer.RowCount > 0 ? ((DataRowView)_BS_sub_cust.Current).Row["ASub_CustName"] : "");
                    ItemList1.Insert(114, Grd_procer.RowCount > 0 ? ((DataRowView)_BS_sub_cust.Current).Row["ESub_CustNmae"] : "");
                    ItemList1.Insert(115, connection.Loc_Cur_Id);//---@LOC_CUR_ID
                    ItemList1.Insert(116, Grd_procer.RowCount > 0 ? Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["ACC_id"]) : 0);//??
                    ItemList1.Insert(117, Convert.ToDecimal(Txt_Tot_amount.Text.Trim()));//---@tot_loc_amount
                    ItemList1.Insert(118, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//-----@@rem_amount_real
                    ItemList1.Insert(119, 0);//-----@@com_amount_real
                    ItemList1.Insert(120, checkBox1.Checked == true ? param_Exch_rate_rem : 0);//@rem_amount_exch
                    ItemList1.Insert(121, 0);//-----@@com_amount_exch
                    ItemList1.Insert(122, Grd_procer.RowCount > 0 ? Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]) : 0);
                    ItemList1.Insert(123, "");
                    ItemList1.Insert(124, "");
                    ItemList1.Insert(125, connection.Loc_Cur_Id);
                    ItemList1.Insert(126, ((DataRowView)_Bs_Incom_Rem.Current).Row["i_ocomm"]);
                    ItemList1.Insert(127, ((DataRowView)_Bs_Incom_Rem.Current).Row["i_Cur_Id"]);
                    ItemList1.Insert(128, Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue));//rem_pay_id 
                    ItemList1.Insert(129, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_aname"]);//---@rem_pay_aname
                    ItemList1.Insert(130, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_ename"]);//---@rem_pay_ename
                    ItemList1.Insert(131, Agent_user_Name);//---@Agent_user_Name
                    ItemList1.Insert(132, Cbo_Oper.SelectedIndex);//---@flag_broker_agent
                MyGeneral_Lib.Copytocliptext("Add_incoming_Voucher_daily", ItemList1);
                connection.scalar("Add_incoming_Voucher_daily", ItemList1);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Btn_Add.Enabled = true;
                    return;

                }
                connection.SQLCMD.Parameters.Clear();
                Btn_Add.Enabled = true;
                print_Rpt_Pdf_NEW();
                this.Close();
              

            }
        }

        private void cmb_cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_cur_rrate)
            {
                Txt_ExRate_Rem.DataBindings.Clear();
                txt_maxrate_rem.DataBindings.Clear();
                txt_minrate_rem.DataBindings.Clear();
                txt_locamount_rem.ResetText();

                param_Exch_rate_rem = Convert.ToDecimal(((DataRowView)binding_cur_bsrem.Current).Row["Exch_rate"]);
                Txt_ExRate_Rem.DataBindings.Add("Text", binding_cur_bsrem, "Exch_rate");
                txt_maxrate_rem.DataBindings.Add("Text", binding_cur_bsrem, "Max_B_price");
                txt_minrate_rem.DataBindings.Add("Text", binding_cur_bsrem, "Min_B_price");
                txt_locamount_rem.Text = (Convert.ToDecimal(Txt_ExRate_Rem.Text) * Convert.ToDecimal(Txt_Rem_Amount.Text)).ToString();
                // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                Discount_Calculator();
                Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            }
        }
        //------------------------------------------------------------------------
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            chang_cur_rrate = false;
            if (Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["r_CUR_ID"]) != connection.Loc_Cur_Id)
            {

                //-----احضار اسعار الصرف
                if (checkBox1.Checked)
                {
                    Txt_Rem_Amount.Text = ((DataRowView)_Bs_Incom_Rem.Current).Row["R_amount"].ToString();
                    Int16 Get_cust_id = 0;
                    Int16 Get_ACC_id = 0;
                    if (Cbo_Oper.SelectedIndex == 0)
                    {

                        Get_cust_id = Convert.ToInt16(TxtBox_User.Tag);
                        Get_ACC_id = 0;
                    }
                    else
                    {
                        if (Grd_procer.RowCount > 0)
                        {
                            Get_cust_id = Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["sub_cust_ID"]);
                            Get_ACC_id = Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Acc_ID"]);
                        }
                        else
                        {
                            MessageBox.Show(connection.Lang_id == 2 ? "you must choose the broker Customer " : "يجب اختيار الوسيط ", MyGeneral_Lib.LblCap);
                            return;
                        }
                    }

                    connection.SqlExec("Exec Get_Cur_Buy_Sal " + connection.T_ID + " , " + Get_cust_id + " , "
                                                               + Get_ACC_id, "Cur_Buy_Sal_Tbl");
                    if (connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"].Rows.Count > 0)
                    {

                        binding_cur_bsrem.DataSource = connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"]
                                          .DefaultView.ToTable(true, "for_cur_id", "Cur_AName", "Cur_EName", "Exch_rate", "Max_B_price",
                                          "Min_B_price", "cur_online_id", "Cur_Code")
                                          .Select(" for_cur_id = " + Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["r_CUR_ID"])).CopyToDataTable();
                        cmb_cur.DataSource = binding_cur_bsrem;
                        cmb_cur.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
                        cmb_cur.ValueMember = "for_cur_id";
                        chang_cur_rrate = true;
                        cmb_cur_SelectedIndexChanged(null, null);




                    }
                }
                else
                {
                    txt_locamount_rem.ResetText();
                    txt_maxrate_rem.ResetText();
                    txt_minrate_rem.ResetText();
                    Txt_Rem_Amount.ResetText();
                    TxtDiscount_Amount.ResetText();
                    Txt_ExRate_Rem.ResetText();
                    cmb_cur.DataSource = new DataTable();

                }
            }

        }
        //----------------------------------------------
        private void Discount_Calculator()//------الخصم
        {
            //Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            decimal Loc_Amount = Convert.ToDecimal(Convert.ToDecimal(txt_locamount_rem.Text));
            Mdiff_Amoun = 0;
            decimal x = 0;
            Mdiff_Amoun = Convert.ToDecimal(Loc_Amount % connection.Discount_Amount);

            x = (connection.Discount_Amount / 2);
            if (Mdiff_Amoun > x)
            {

                Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
            }
            else
            {
                Mdiff_Amoun = Mdiff_Amoun * -1; //----مكتسب
            }

            TxtDiscount_Amount.Text = Mdiff_Amoun.ToString();
        }
        //----------------------------------------------
        private void Txt_procer_TextChanged(object sender, EventArgs e)
        {
            if (Txt_procer.Text != "")
            {
                string SqlTxt_sub = "";
                chang_procer = false;
                SqlTxt_sub = " Select  * From  Sub_CUSTOMERS_online where Cur_Id = " + Convert.ToInt32(((DataRowView)_Bs_Incom_Rem.Current).Row["pr_CUR_ID"])
                    + "And ASub_CustName like '" + Txt_procer.Text + "%' and t_id =" + connection.T_ID;
                _BS_sub_cust.DataSource = connection.SqlExec(SqlTxt_sub, "Sub_Cust_tbl");
                if (connection.SQLDS.Tables["Sub_Cust_tbl"].Rows.Count > 0)
                {
                    chang_procer = true;
                    Grd_procer.DataSource = _BS_sub_cust;
                }
            }

            else
            {
                Txt_Accproc.Text = "";
                chang_procer = false;
                Grd_procer.DataSource = new BindingSource();
            }
        }
        //--------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------------------------------------
        private void Cbo_Oper_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_Oper.SelectedIndex == 0)
            {

                Txt_procer.Visible = false;
                label13.Visible = false;
                Grd_procer.Visible = false;
                label16.Visible = false;
                Txt_Accproc.Visible = false;
                TxtDiscount_Amount.Visible = true;
                label59.Visible = true;
                if (Txt_procer.Text != "")
                {
                    Txt_procer.Text = "";

                }
            }
            if (Cbo_Oper.SelectedIndex == 1 || Cbo_Oper.SelectedIndex == 2)
            {

                Txt_procer.Visible = true;
                label13.Visible = true;
                Grd_procer.Visible = true;
                label16.Visible = true;
                Txt_Accproc.Visible = true;
                TxtDiscount_Amount.Visible = false;
                label59.Visible = false;
            }
            checkBox1.Checked = false;
        }
        //------------------------------------------------------------------
        private void ADD_Incoming_Rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Str = { "Get_Incom_Rem_TBl", "Get_info_Tbl4", "Get_info_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
        }
        //---------------------------------------------------------------------------
        private void print_Rpt_Pdf_NEW()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int int_nrecdate = 0;
            string Frm_id = "";
            User = TxtUser.Text;
            string phone = "";
            int procker = 0;
            string prockr_name = "";
            string procker_ename = "";
            string local_Cur1 = "";
            string local_ECur1 = "";
            string forgin_Cur1 = "";
            string forgin_ECur1 = "";
            string s_Ejob = "";
            string r_Ejob = "";
            string S_Social_No = "";
            string R_Social_No = "";
            string Rem_Pay_Type_Aname = "";
            string Rem_Pay_Type_Ename = "";
            int Rem_Pay_Type_id = 0;

            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();
            Exch_rate_com = Txt_ExRate_Rem.Text;
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);

            rem_no = ((DataRowView)_Bs_Incom_Rem.Current).Row["rem_no"].ToString();
            string SqlTxt = "Exec Report_Rem_Information_new" + "'" + rem_no + "'," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Rem_Info");

           
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
                }
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_ename"].ToString();
                }

                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_Aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_ename"].ToString();
                }


                local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
                local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

                forgin_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Cur_Code"].ToString();
                forgin_Cur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ACUR_NAME"].ToString();
                forgin_ECur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ECUR_NAME"].ToString();

                s_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_Ejob"].ToString();
                r_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_Ejob"].ToString();
                S_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["S_Social_No"].ToString();
                R_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Social_No"].ToString();

                Vo_No = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"]);

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                    //if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                    //{
                    //    com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]);
                    //    com_Cur_code = connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_code"].ToString();
                    //    ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    //    Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                    //    Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                    //}
                }
                else// عملة محلية
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                    //if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                    //{
                    //    com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]);
                    //    com_Cur_code = local_Cur;
                    //    ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    //    Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    //    Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                    //}
                }

                Rem_Flag = Convert.ToInt16(connection.SQLDS.Tables["Rem_Info"].Rows[0]["Rem_Flag"]);


                term = TxtTerm_Name.Text;

                //if (Rem_Flag == 11)
                //{
                //    if
                //      ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) == 0) ||
                //       (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) == 0))
                //    {
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //        {
                //            Oper_Id = 8;

                //        }
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //        {
                //            Oper_Id = 10;
                //        }
                //    }

                //    if
                //       ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0) ||
                //        (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0))
                //    {
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //        {
                //            Oper_Id = 22;

                //        }
                //        if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //        {
                //            Oper_Id = 35;
                //        }
                //    }
                //}

                //if (Rem_Flag == 1)
                //{
                    Oper_Id = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["oper_id"]);
                //}
                try
                {

                    Delivery_Rem_rpt_1 ObjRpt = new Delivery_Rem_rpt_1();
                    Delivery_Rem_rpt_eng_1 ObjRptEng = new Delivery_Rem_rpt_eng_1();

                    string Real_Paid_Name = "";
                    Real_Paid_Name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Real_Paid_Name"].ToString();

                    DataTable Dt_Param = CustomControls.CustomParam_Dt();
                    Dt_Param.Rows.Add("_Date", _Date);
                    Dt_Param.Rows.Add("local_Cur", local_Cur);
                    Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                    Dt_Param.Rows.Add("Vo_No", Vo_No);
                    Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                    Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                    Dt_Param.Rows.Add("User", User);
                    Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                    Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                    Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                    Dt_Param.Rows.Add("term", term);
                    Dt_Param.Rows.Add("Frm_id", Frm_id);
                    Dt_Param.Rows.Add("phone", phone);
                    // Dt_Param.Rows.Add("com_cur", com_Cur_code);
                    //Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                    //Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                    Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                    Dt_Param.Rows.Add("procker", procker);
                    Dt_Param.Rows.Add("procker_ename", procker_ename);
                    Dt_Param.Rows.Add("prockr_name", prockr_name);
                    Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                    Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                    Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                    Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                    Dt_Param.Rows.Add("Real_Paid_name", Real_Paid_Name);
                    Dt_Param.Rows.Add("Anote_report", Anote_report);
                    Dt_Param.Rows.Add("Enote_report", Enote_report);
                    Dt_Param.Rows.Add("Rem_Pay_Type_Aname", ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_aname"]);
                    Dt_Param.Rows.Add("Rem_Pay_Type_Ename", ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_ename"]);
                    Dt_Param.Rows.Add("Rem_Pay_Type_id", Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue));


                    connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
                    connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);
                }
                catch
                { }
            
        }
        //---------------------------------------------------------------------------
        private void Txt_ExRate_Rem_TextChanged(object sender, EventArgs e)
        {
            txt_locamount_rem.Text = (Convert.ToDecimal(Txt_ExRate_Rem.Text) * Convert.ToDecimal(Txt_Rem_Amount.Text)).ToString();
            // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            Discount_Calculator();
            Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
        }

        private void Grd_procer_SelectionChanged(object sender, EventArgs e)
        {
            if (chang_procer)
            {
                Txt_Accproc.Text = ((DataRowView)_BS_sub_cust.Current).Row["Acc_AName"].ToString();
            }
        }
    }
}