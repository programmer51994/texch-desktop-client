﻿namespace Integration_Accounting_Sys
{
    partial class Delivery_Add_eng
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Grdrec_rem = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label57 = new System.Windows.Forms.Label();
            this.Txt_R_details_job = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.Txt_Real_Paid_Name = new System.Windows.Forms.TextBox();
            this.cmb_job_receiver = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.ar_sen = new System.Windows.Forms.Button();
            this.ENAR_BTN = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.Txt_Social_No = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.resd_cmb = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.txt_rbirth_place = new System.Windows.Forms.TextBox();
            this.Txt_Doc_r_Date = new System.Windows.Forms.DateTimePicker();
            this.Txt_Doc_r_Exp = new System.Windows.Forms.DateTimePicker();
            this.Txt_rbirth_Date = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_sec_no = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.Grd_CustSen_Name = new System.Windows.Forms.DataGridView();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_mail = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_Mother_name = new System.Windows.Forms.TextBox();
            this.cmb_Gender_id = new System.Windows.Forms.ComboBox();
            this.Txtr_State = new System.Windows.Forms.TextBox();
            this.Txtr_Post_Code = new System.Windows.Forms.TextBox();
            this.Txtr_Street = new System.Windows.Forms.TextBox();
            this.Txtr_Suburb = new System.Windows.Forms.TextBox();
            this.Cmb_phone_Code_R = new System.Windows.Forms.ComboBox();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Cmb_R_Nat = new System.Windows.Forms.ComboBox();
            this.Cmb_R_City = new System.Windows.Forms.ComboBox();
            this.Txt_Reciever = new System.Windows.Forms.TextBox();
            this.Cmb_r_Doc_Type = new System.Windows.Forms.ComboBox();
            this.Txt_Doc_r_Issue = new System.Windows.Forms.TextBox();
            this.Txt_r_Doc_No = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_S_Birth = new System.Windows.Forms.DateTimePicker();
            this.txts_city = new System.Windows.Forms.TextBox();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.TxtS_State = new System.Windows.Forms.TextBox();
            this.TxtS_Post_Code = new System.Windows.Forms.TextBox();
            this.Txts_street = new System.Windows.Forms.TextBox();
            this.txtS_Suburb = new System.Windows.Forms.TextBox();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.Txt_job_Sender = new System.Windows.Forms.TextBox();
            this.Txt_S_Phone = new System.Windows.Forms.TextBox();
            this.Txt_Sender = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label62 = new System.Windows.Forms.Label();
            this.Txt_Pay_Info = new System.Windows.Forms.TextBox();
            this.Cbo_Rem_Pay_Type = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.Txt_R_Relation = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.Txt_R_T_Purpose = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Txt_Accproc = new System.Windows.Forms.TextBox();
            this.Grd_procer = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_procer = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.Cbo_Oper = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label59 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.cmb_cur = new System.Windows.Forms.ComboBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.Txt_T_Purpose = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_notes = new System.Windows.Forms.TextBox();
            this.Txt_Tot_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtDiscount_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_locamount_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_minrate_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.txt_maxrate_rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_ExRate_Rem = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Rem_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.Txtcity_con_online = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.Cbo_Rcur = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.Tot_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.Txt_RCount = new System.Windows.Forms.TextBox();
            this.Print_All = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_CustSen_Name)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_procer)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-4, 60);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(925, 1);
            this.flowLayoutPanel9.TabIndex = 629;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-99, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(264, 32);
            this.Txt_Loc_Cur.Multiline = true;
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(226, 25);
            this.Txt_Loc_Cur.TabIndex = 627;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(14, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(160, 23);
            this.TxtIn_Rec_Date.TabIndex = 624;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(262, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtUser.Size = new System.Drawing.Size(226, 23);
            this.TxtUser.TabIndex = 622;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(601, 5);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtTerm_Name.Size = new System.Drawing.Size(277, 23);
            this.TxtTerm_Name.TabIndex = 620;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(888, 679);
            this.shapeContainer1.TabIndex = 630;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(13, 66);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(855, 22);
            // 
            // Grdrec_rem
            // 
            this.Grdrec_rem.AllowUserToAddRows = false;
            this.Grdrec_rem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grdrec_rem.ColumnHeadersHeight = 24;
            this.Grdrec_rem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdrec_rem.DefaultCellStyle = dataGridViewCellStyle4;
            this.Grdrec_rem.Location = new System.Drawing.Point(16, 94);
            this.Grdrec_rem.Name = "Grdrec_rem";
            this.Grdrec_rem.ReadOnly = true;
            this.Grdrec_rem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grdrec_rem.Size = new System.Drawing.Size(859, 101);
            this.Grdrec_rem.TabIndex = 711;
            this.Grdrec_rem.SelectionChanged += new System.EventHandler(this.Grdrec_rem_SelectionChanged);
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "Rem. NO";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "R_amount";
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "Rem. amount";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "R_ECUR_NAME";
            this.Column2.HeaderText = "Rem. currency";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "PR_ECUR_NAME";
            this.Column4.HeaderText = "Delivery currency";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 130;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ECASE_NA";
            this.Column5.HeaderText = "Rem. case";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 130;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Case_Date";
            this.Column6.HeaderText = "Case date";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 110;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "Sender name";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 119;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "S_phone";
            this.Column8.HeaderText = "Sender Phone";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 124;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "r_name";
            this.Column9.HeaderText = "Receiver name";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 129;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "R_phone";
            this.Column10.HeaderText = "Receiver phone";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 134;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(15, 258);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(861, 421);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.label57);
            this.tabPage1.Controls.Add(this.Txt_R_details_job);
            this.tabPage1.Controls.Add(this.label56);
            this.tabPage1.Controls.Add(this.Txt_Real_Paid_Name);
            this.tabPage1.Controls.Add(this.cmb_job_receiver);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.ar_sen);
            this.tabPage1.Controls.Add(this.ENAR_BTN);
            this.tabPage1.Controls.Add(this.label54);
            this.tabPage1.Controls.Add(this.Txt_Social_No);
            this.tabPage1.Controls.Add(this.label78);
            this.tabPage1.Controls.Add(this.resd_cmb);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.label47);
            this.tabPage1.Controls.Add(this.label48);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.Btn_Browser);
            this.tabPage1.Controls.Add(this.txt_rbirth_place);
            this.tabPage1.Controls.Add(this.Txt_Doc_r_Date);
            this.tabPage1.Controls.Add(this.Txt_Doc_r_Exp);
            this.tabPage1.Controls.Add(this.Txt_rbirth_Date);
            this.tabPage1.Controls.Add(this.flowLayoutPanel8);
            this.tabPage1.Controls.Add(this.flowLayoutPanel17);
            this.tabPage1.Controls.Add(this.Txt_sec_no);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.Grd_CustSen_Name);
            this.tabPage1.Controls.Add(this.Txt_mail);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.txt_Mother_name);
            this.tabPage1.Controls.Add(this.cmb_Gender_id);
            this.tabPage1.Controls.Add(this.Txtr_State);
            this.tabPage1.Controls.Add(this.Txtr_Post_Code);
            this.tabPage1.Controls.Add(this.Txtr_Street);
            this.tabPage1.Controls.Add(this.Txtr_Suburb);
            this.tabPage1.Controls.Add(this.Cmb_phone_Code_R);
            this.tabPage1.Controls.Add(this.Txt_R_Phone);
            this.tabPage1.Controls.Add(this.Cmb_R_Nat);
            this.tabPage1.Controls.Add(this.Cmb_R_City);
            this.tabPage1.Controls.Add(this.Txt_Reciever);
            this.tabPage1.Controls.Add(this.Cmb_r_Doc_Type);
            this.tabPage1.Controls.Add(this.Txt_Doc_r_Issue);
            this.tabPage1.Controls.Add(this.Txt_r_Doc_No);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabPage1.Size = new System.Drawing.Size(853, 395);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Receiver Information ";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(502, 342);
            this.label57.Name = "label57";
            this.label57.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label57.Size = new System.Drawing.Size(79, 14);
            this.label57.TabIndex = 993;
            this.label57.Text = ": details job";
            // 
            // Txt_R_details_job
            // 
            this.Txt_R_details_job.Location = new System.Drawing.Point(588, 342);
            this.Txt_R_details_job.MaxLength = 99;
            this.Txt_R_details_job.Name = "Txt_R_details_job";
            this.Txt_R_details_job.Size = new System.Drawing.Size(245, 20);
            this.Txt_R_details_job.TabIndex = 24;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Navy;
            this.label56.Location = new System.Drawing.Point(522, 57);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(62, 14);
            this.label56.TabIndex = 991;
            this.label56.Text = "ID Name:";
            // 
            // Txt_Real_Paid_Name
            // 
            this.Txt_Real_Paid_Name.BackColor = System.Drawing.Color.White;
            this.Txt_Real_Paid_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Real_Paid_Name.Location = new System.Drawing.Point(592, 53);
            this.Txt_Real_Paid_Name.Name = "Txt_Real_Paid_Name";
            this.Txt_Real_Paid_Name.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Real_Paid_Name.Size = new System.Drawing.Size(242, 23);
            this.Txt_Real_Paid_Name.TabIndex = 2;
            // 
            // cmb_job_receiver
            // 
            this.cmb_job_receiver.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_job_receiver.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_job_receiver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_job_receiver.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_job_receiver.FormattingEnabled = true;
            this.cmb_job_receiver.Location = new System.Drawing.Point(111, 340);
            this.cmb_job_receiver.Name = "cmb_job_receiver";
            this.cmb_job_receiver.Size = new System.Drawing.Size(253, 24);
            this.cmb_job_receiver.TabIndex = 23;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(651, 6);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(72, 16);
            this.label38.TabIndex = 989;
            this.label38.Text = "Msg_label";
            // 
            // ar_sen
            // 
            this.ar_sen.Location = new System.Drawing.Point(367, 7);
            this.ar_sen.Name = "ar_sen";
            this.ar_sen.Size = new System.Drawing.Size(29, 23);
            this.ar_sen.TabIndex = 1;
            this.ar_sen.Text = "AR";
            this.ar_sen.UseVisualStyleBackColor = false;
            this.ar_sen.Click += new System.EventHandler(this.ar_sen_Click);
            // 
            // ENAR_BTN
            // 
            this.ENAR_BTN.Location = new System.Drawing.Point(340, 7);
            this.ENAR_BTN.Name = "ENAR_BTN";
            this.ENAR_BTN.Size = new System.Drawing.Size(28, 23);
            this.ENAR_BTN.TabIndex = 0;
            this.ENAR_BTN.Text = "EN";
            this.ENAR_BTN.UseVisualStyleBackColor = true;
            this.ENAR_BTN.Click += new System.EventHandler(this.ENAR_BTN_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(528, 236);
            this.label54.Name = "label54";
            this.label54.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label54.Size = new System.Drawing.Size(54, 14);
            this.label54.TabIndex = 986;
            this.label54.Text = ": SIN ID";
            // 
            // Txt_Social_No
            // 
            this.Txt_Social_No.BackColor = System.Drawing.Color.White;
            this.Txt_Social_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Social_No.Location = new System.Drawing.Point(592, 232);
            this.Txt_Social_No.MaxLength = 15;
            this.Txt_Social_No.Name = "Txt_Social_No";
            this.Txt_Social_No.Size = new System.Drawing.Size(155, 23);
            this.Txt_Social_No.TabIndex = 16;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Navy;
            this.label78.Location = new System.Drawing.Point(27, 319);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(77, 14);
            this.label78.TabIndex = 983;
            this.label78.Text = "Resd. type:";
            // 
            // resd_cmb
            // 
            this.resd_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_cmb.FormattingEnabled = true;
            this.resd_cmb.Items.AddRange(new object[] {
            "Resident",
            "Non-Resident"});
            this.resd_cmb.Location = new System.Drawing.Point(110, 314);
            this.resd_cmb.Name = "resd_cmb";
            this.resd_cmb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.resd_cmb.Size = new System.Drawing.Size(255, 24);
            this.resd_cmb.TabIndex = 21;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(536, 319);
            this.label41.Name = "label41";
            this.label41.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label41.Size = new System.Drawing.Size(46, 14);
            this.label41.TabIndex = 981;
            this.label41.Text = ": Email";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(510, 292);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(72, 14);
            this.label44.TabIndex = 980;
            this.label44.Text = ":Post code";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(528, 270);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(54, 14);
            this.label42.TabIndex = 979;
            this.label42.Text = ": Street";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(53, 294);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(49, 14);
            this.label45.TabIndex = 978;
            this.label45.Text = ": State";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(42, 273);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(60, 14);
            this.label43.TabIndex = 977;
            this.label43.Text = ": Suburb";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(40, 345);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(63, 14);
            this.label22.TabIndex = 976;
            this.label22.Text = ": The Job";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(35, 254);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(76, 14);
            this.label34.TabIndex = 975;
            this.label34.Text = ".... Address";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(508, 212);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(74, 14);
            this.label18.TabIndex = 974;
            this.label18.Text = ": Doc. date";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(520, 185);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(62, 14);
            this.label26.TabIndex = 973;
            this.label26.Text = ": Doc. No";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(30, 187);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(74, 14);
            this.label6.TabIndex = 972;
            this.label6.Text = ": Doc. type";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(30, 212);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(72, 14);
            this.label20.TabIndex = 971;
            this.label20.Text = ":Doc. issue";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(4, 236);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(100, 14);
            this.label25.TabIndex = 970;
            this.label25.Text = ":Doc. expirdate";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Maroon;
            this.label47.Location = new System.Drawing.Point(29, 168);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(101, 14);
            this.label47.TabIndex = 969;
            this.label47.Text = ".... Documment";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(506, 155);
            this.label48.Name = "label48";
            this.label48.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label48.Size = new System.Drawing.Size(76, 14);
            this.label48.TabIndex = 968;
            this.label48.Text = ":Birth place";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(507, 34);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(77, 14);
            this.label36.TabIndex = 1;
            this.label36.Text = ": Code Rem";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(489, 132);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(93, 14);
            this.label35.TabIndex = 965;
            this.label35.Text = ":Mother name";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(528, 83);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(54, 14);
            this.label28.TabIndex = 964;
            this.label28.Text = ":Gender";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(508, 109);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label40.Size = new System.Drawing.Size(74, 14);
            this.label40.TabIndex = 963;
            this.label40.Text = ":Nationalty";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(20, 102);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 954;
            this.label9.Text = ": Phone";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(35, 148);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(39, 14);
            this.label5.TabIndex = 953;
            this.label5.Text = ": City";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(5, 125);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(69, 14);
            this.label24.TabIndex = 952;
            this.label24.Text = ":Birthdate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(24, 12);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(44, 14);
            this.label7.TabIndex = 951;
            this.label7.Text = ":Name";
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Enabled = false;
            this.Btn_Browser.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Browser.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Browser.Location = new System.Drawing.Point(729, 366);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(111, 25);
            this.Btn_Browser.TabIndex = 25;
            this.Btn_Browser.Text = "Display .Doc";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.Btn_Browser_Click);
            // 
            // txt_rbirth_place
            // 
            this.txt_rbirth_place.Location = new System.Drawing.Point(591, 152);
            this.txt_rbirth_place.MaxLength = 49;
            this.txt_rbirth_place.Name = "txt_rbirth_place";
            this.txt_rbirth_place.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_rbirth_place.Size = new System.Drawing.Size(242, 20);
            this.txt_rbirth_place.TabIndex = 10;
            // 
            // Txt_Doc_r_Date
            // 
            this.Txt_Doc_r_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_r_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_r_Date.Location = new System.Drawing.Point(592, 209);
            this.Txt_Doc_r_Date.Name = "Txt_Doc_r_Date";
            this.Txt_Doc_r_Date.Size = new System.Drawing.Size(155, 20);
            this.Txt_Doc_r_Date.TabIndex = 14;
            // 
            // Txt_Doc_r_Exp
            // 
            this.Txt_Doc_r_Exp.Checked = false;
            this.Txt_Doc_r_Exp.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_r_Exp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_r_Exp.Location = new System.Drawing.Point(110, 233);
            this.Txt_Doc_r_Exp.Name = "Txt_Doc_r_Exp";
            this.Txt_Doc_r_Exp.ShowCheckBox = true;
            this.Txt_Doc_r_Exp.Size = new System.Drawing.Size(156, 20);
            this.Txt_Doc_r_Exp.TabIndex = 15;
            // 
            // Txt_rbirth_Date
            // 
            this.Txt_rbirth_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_rbirth_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_rbirth_Date.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Txt_rbirth_Date.Location = new System.Drawing.Point(80, 122);
            this.Txt_rbirth_Date.Name = "Txt_rbirth_Date";
            this.Txt_rbirth_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_rbirth_Date.Size = new System.Drawing.Size(154, 20);
            this.Txt_rbirth_Date.TabIndex = 7;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(3, 175);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(847, 1);
            this.flowLayoutPanel8.TabIndex = 949;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(2, 264);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(847, 1);
            this.flowLayoutPanel17.TabIndex = 947;
            // 
            // Txt_sec_no
            // 
            this.Txt_sec_no.BackColor = System.Drawing.Color.White;
            this.Txt_sec_no.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_sec_no.Location = new System.Drawing.Point(592, 30);
            this.Txt_sec_no.Name = "Txt_sec_no";
            this.Txt_sec_no.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_sec_no.Size = new System.Drawing.Size(242, 23);
            this.Txt_sec_no.TabIndex = 1;
            this.Txt_sec_no.Leave += new System.EventHandler(this.Txt_sec_no_Leave);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkBox2.ForeColor = System.Drawing.Color.Maroon;
            this.checkBox2.Location = new System.Drawing.Point(9, 13);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // Grd_CustSen_Name
            // 
            this.Grd_CustSen_Name.AllowUserToAddRows = false;
            this.Grd_CustSen_Name.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_CustSen_Name.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_CustSen_Name.BackgroundColor = System.Drawing.Color.White;
            this.Grd_CustSen_Name.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_CustSen_Name.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_CustSen_Name.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_CustSen_Name.ColumnHeadersHeight = 45;
            this.Grd_CustSen_Name.ColumnHeadersVisible = false;
            this.Grd_CustSen_Name.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column17,
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_CustSen_Name.DefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_CustSen_Name.Location = new System.Drawing.Point(6, 30);
            this.Grd_CustSen_Name.Name = "Grd_CustSen_Name";
            this.Grd_CustSen_Name.ReadOnly = true;
            this.Grd_CustSen_Name.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_CustSen_Name.RowHeadersVisible = false;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_CustSen_Name.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_CustSen_Name.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_CustSen_Name.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_CustSen_Name.Size = new System.Drawing.Size(390, 66);
            this.Grd_CustSen_Name.TabIndex = 900;
            this.Grd_CustSen_Name.SelectionChanged += new System.EventHandler(this.Grd_CustSen_Name_SelectionChanged);
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "per_id";
            this.Column15.Frozen = true;
            this.Column15.HeaderText = "رمز الزبون";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 70;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Per_AName";
            this.Column17.HeaderText = "اسم الزبون";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 200;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Aouthorized_name";
            this.dataGridViewTextBoxColumn1.HeaderText = "مخول";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // Txt_mail
            // 
            this.Txt_mail.Location = new System.Drawing.Point(588, 316);
            this.Txt_mail.MaxLength = 99;
            this.Txt_mail.Name = "Txt_mail";
            this.Txt_mail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_mail.Size = new System.Drawing.Size(245, 20);
            this.Txt_mail.TabIndex = 22;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Maroon;
            this.label37.Location = new System.Drawing.Point(749, 212);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(87, 14);
            this.label37.TabIndex = 902;
            this.label37.Text = "dd/mm/yyyy";
            // 
            // txt_Mother_name
            // 
            this.txt_Mother_name.Location = new System.Drawing.Point(591, 129);
            this.txt_Mother_name.MaxLength = 49;
            this.txt_Mother_name.Name = "txt_Mother_name";
            this.txt_Mother_name.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_Mother_name.Size = new System.Drawing.Size(242, 20);
            this.txt_Mother_name.TabIndex = 8;
            // 
            // cmb_Gender_id
            // 
            this.cmb_Gender_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Gender_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Gender_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Gender_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Gender_id.FormattingEnabled = true;
            this.cmb_Gender_id.Location = new System.Drawing.Point(592, 78);
            this.cmb_Gender_id.Name = "cmb_Gender_id";
            this.cmb_Gender_id.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmb_Gender_id.Size = new System.Drawing.Size(241, 24);
            this.cmb_Gender_id.TabIndex = 3;
            // 
            // Txtr_State
            // 
            this.Txtr_State.Location = new System.Drawing.Point(110, 292);
            this.Txtr_State.MaxLength = 99;
            this.Txtr_State.Name = "Txtr_State";
            this.Txtr_State.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_State.Size = new System.Drawing.Size(255, 20);
            this.Txtr_State.TabIndex = 19;
            // 
            // Txtr_Post_Code
            // 
            this.Txtr_Post_Code.Location = new System.Drawing.Point(588, 292);
            this.Txtr_Post_Code.MaxLength = 99;
            this.Txtr_Post_Code.Name = "Txtr_Post_Code";
            this.Txtr_Post_Code.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_Post_Code.Size = new System.Drawing.Size(245, 20);
            this.Txtr_Post_Code.TabIndex = 20;
            // 
            // Txtr_Street
            // 
            this.Txtr_Street.Location = new System.Drawing.Point(588, 268);
            this.Txtr_Street.MaxLength = 99;
            this.Txtr_Street.Name = "Txtr_Street";
            this.Txtr_Street.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_Street.Size = new System.Drawing.Size(245, 20);
            this.Txtr_Street.TabIndex = 18;
            // 
            // Txtr_Suburb
            // 
            this.Txtr_Suburb.Location = new System.Drawing.Point(110, 271);
            this.Txtr_Suburb.MaxLength = 99;
            this.Txtr_Suburb.Name = "Txtr_Suburb";
            this.Txtr_Suburb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_Suburb.Size = new System.Drawing.Size(255, 20);
            this.Txtr_Suburb.TabIndex = 17;
            // 
            // Cmb_phone_Code_R
            // 
            this.Cmb_phone_Code_R.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_phone_Code_R.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_phone_Code_R.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_phone_Code_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_phone_Code_R.FormattingEnabled = true;
            this.Cmb_phone_Code_R.Location = new System.Drawing.Point(80, 97);
            this.Cmb_phone_Code_R.Name = "Cmb_phone_Code_R";
            this.Cmb_phone_Code_R.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cmb_phone_Code_R.Size = new System.Drawing.Size(85, 24);
            this.Cmb_phone_Code_R.TabIndex = 4;
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.Location = new System.Drawing.Point(166, 98);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_R_Phone.Size = new System.Drawing.Size(197, 20);
            this.Txt_R_Phone.TabIndex = 5;
            // 
            // Cmb_R_Nat
            // 
            this.Cmb_R_Nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_Nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_Nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_Nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_Nat.FormattingEnabled = true;
            this.Cmb_R_Nat.Location = new System.Drawing.Point(592, 104);
            this.Cmb_R_Nat.Name = "Cmb_R_Nat";
            this.Cmb_R_Nat.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cmb_R_Nat.Size = new System.Drawing.Size(241, 24);
            this.Cmb_R_Nat.TabIndex = 6;
            // 
            // Cmb_R_City
            // 
            this.Cmb_R_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_City.FormattingEnabled = true;
            this.Cmb_R_City.Location = new System.Drawing.Point(78, 143);
            this.Cmb_R_City.Name = "Cmb_R_City";
            this.Cmb_R_City.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cmb_R_City.Size = new System.Drawing.Size(285, 24);
            this.Cmb_R_City.TabIndex = 9;
            this.Cmb_R_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_R_City_SelectedIndexChanged);
            // 
            // Txt_Reciever
            // 
            this.Txt_Reciever.BackColor = System.Drawing.Color.White;
            this.Txt_Reciever.Location = new System.Drawing.Point(67, 9);
            this.Txt_Reciever.MaxLength = 49;
            this.Txt_Reciever.Name = "Txt_Reciever";
            this.Txt_Reciever.ReadOnly = true;
            this.Txt_Reciever.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Reciever.Size = new System.Drawing.Size(273, 20);
            this.Txt_Reciever.TabIndex = 0;
            // 
            // Cmb_r_Doc_Type
            // 
            this.Cmb_r_Doc_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_r_Doc_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_r_Doc_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_r_Doc_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_r_Doc_Type.FormattingEnabled = true;
            this.Cmb_r_Doc_Type.Location = new System.Drawing.Point(110, 181);
            this.Cmb_r_Doc_Type.Name = "Cmb_r_Doc_Type";
            this.Cmb_r_Doc_Type.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cmb_r_Doc_Type.Size = new System.Drawing.Size(253, 24);
            this.Cmb_r_Doc_Type.TabIndex = 11;
            // 
            // Txt_Doc_r_Issue
            // 
            this.Txt_Doc_r_Issue.Location = new System.Drawing.Point(110, 209);
            this.Txt_Doc_r_Issue.MaxLength = 49;
            this.Txt_Doc_r_Issue.Name = "Txt_Doc_r_Issue";
            this.Txt_Doc_r_Issue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Doc_r_Issue.Size = new System.Drawing.Size(255, 20);
            this.Txt_Doc_r_Issue.TabIndex = 13;
            // 
            // Txt_r_Doc_No
            // 
            this.Txt_r_Doc_No.BackColor = System.Drawing.Color.White;
            this.Txt_r_Doc_No.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_Doc_No.Location = new System.Drawing.Point(591, 182);
            this.Txt_r_Doc_No.MaxLength = 49;
            this.Txt_r_Doc_No.Name = "Txt_r_Doc_No";
            this.Txt_r_Doc_No.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_r_Doc_No.Size = new System.Drawing.Size(242, 23);
            this.Txt_r_Doc_No.TabIndex = 12;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Maroon;
            this.label32.Location = new System.Drawing.Point(272, 236);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(87, 14);
            this.label32.TabIndex = 825;
            this.label32.Text = "dd/mm/yyyy";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Maroon;
            this.label33.Location = new System.Drawing.Point(256, 125);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(87, 14);
            this.label33.TabIndex = 820;
            this.label33.Text = "dd/mm/yyyy";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.Txt_S_Birth);
            this.tabPage2.Controls.Add(this.txts_city);
            this.tabPage2.Controls.Add(this.Txts_nat);
            this.tabPage2.Controls.Add(this.TxtS_State);
            this.tabPage2.Controls.Add(this.TxtS_Post_Code);
            this.tabPage2.Controls.Add(this.Txts_street);
            this.tabPage2.Controls.Add(this.txtS_Suburb);
            this.tabPage2.Controls.Add(this.Txt_Relionship);
            this.tabPage2.Controls.Add(this.Txt_job_Sender);
            this.tabPage2.Controls.Add(this.Txt_S_Phone);
            this.tabPage2.Controls.Add(this.Txt_Sender);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(853, 395);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sender Information";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(491, 143);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(46, 14);
            this.label11.TabIndex = 893;
            this.label11.Text = ": Email";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(421, 114);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(116, 14);
            this.label21.TabIndex = 892;
            this.label21.Text = ":Relationship R & S";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(474, 85);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(63, 14);
            this.label17.TabIndex = 891;
            this.label17.Text = ": The Job";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(464, 56);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(73, 14);
            this.label15.TabIndex = 890;
            this.label15.Text = ": Birthdate";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(463, 29);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(74, 14);
            this.label27.TabIndex = 889;
            this.label27.Text = ":Nationalty";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(19, 167);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 888;
            this.label8.Text = ": State";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(8, 143);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(60, 14);
            this.label23.TabIndex = 887;
            this.label23.Text = ": Suburb";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(14, 114);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(54, 14);
            this.label29.TabIndex = 886;
            this.label29.Text = ": Street";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(29, 85);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(39, 14);
            this.label13.TabIndex = 885;
            this.label13.Text = ": City";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(14, 56);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(54, 14);
            this.label16.TabIndex = 884;
            this.label16.Text = ": Phone";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(20, 29);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(48, 14);
            this.label10.TabIndex = 883;
            this.label10.Text = ": Name";
            // 
            // Txt_S_Birth
            // 
            this.Txt_S_Birth.CustomFormat = "dd/mm/yyyy";
            this.Txt_S_Birth.Enabled = false;
            this.Txt_S_Birth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_S_Birth.Location = new System.Drawing.Point(663, 53);
            this.Txt_S_Birth.Name = "Txt_S_Birth";
            this.Txt_S_Birth.Size = new System.Drawing.Size(174, 20);
            this.Txt_S_Birth.TabIndex = 882;
            // 
            // txts_city
            // 
            this.txts_city.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txts_city.Location = new System.Drawing.Point(87, 82);
            this.txts_city.MaxLength = 49;
            this.txts_city.Name = "txts_city";
            this.txts_city.ReadOnly = true;
            this.txts_city.Size = new System.Drawing.Size(271, 20);
            this.txts_city.TabIndex = 4;
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txts_nat.Location = new System.Drawing.Point(553, 26);
            this.Txts_nat.MaxLength = 49;
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(285, 20);
            this.Txts_nat.TabIndex = 1;
            // 
            // TxtS_State
            // 
            this.TxtS_State.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_State.Location = new System.Drawing.Point(87, 166);
            this.TxtS_State.MaxLength = 99;
            this.TxtS_State.Name = "TxtS_State";
            this.TxtS_State.ReadOnly = true;
            this.TxtS_State.Size = new System.Drawing.Size(271, 20);
            this.TxtS_State.TabIndex = 10;
            // 
            // TxtS_Post_Code
            // 
            this.TxtS_Post_Code.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtS_Post_Code.Location = new System.Drawing.Point(553, 140);
            this.TxtS_Post_Code.MaxLength = 29;
            this.TxtS_Post_Code.Name = "TxtS_Post_Code";
            this.TxtS_Post_Code.ReadOnly = true;
            this.TxtS_Post_Code.Size = new System.Drawing.Size(285, 20);
            this.TxtS_Post_Code.TabIndex = 9;
            // 
            // Txts_street
            // 
            this.Txts_street.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txts_street.Location = new System.Drawing.Point(87, 111);
            this.Txts_street.MaxLength = 99;
            this.Txts_street.Name = "Txts_street";
            this.Txts_street.ReadOnly = true;
            this.Txts_street.Size = new System.Drawing.Size(271, 20);
            this.Txts_street.TabIndex = 8;
            // 
            // txtS_Suburb
            // 
            this.txtS_Suburb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtS_Suburb.Location = new System.Drawing.Point(87, 140);
            this.txtS_Suburb.MaxLength = 99;
            this.txtS_Suburb.Name = "txtS_Suburb";
            this.txtS_Suburb.ReadOnly = true;
            this.txtS_Suburb.Size = new System.Drawing.Size(271, 20);
            this.txtS_Suburb.TabIndex = 6;
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Relionship.Location = new System.Drawing.Point(553, 111);
            this.Txt_Relionship.MaxLength = 99;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(285, 20);
            this.Txt_Relionship.TabIndex = 7;
            // 
            // Txt_job_Sender
            // 
            this.Txt_job_Sender.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_job_Sender.Location = new System.Drawing.Point(553, 82);
            this.Txt_job_Sender.MaxLength = 49;
            this.Txt_job_Sender.Name = "Txt_job_Sender";
            this.Txt_job_Sender.ReadOnly = true;
            this.Txt_job_Sender.Size = new System.Drawing.Size(285, 20);
            this.Txt_job_Sender.TabIndex = 5;
            // 
            // Txt_S_Phone
            // 
            this.Txt_S_Phone.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_S_Phone.Location = new System.Drawing.Point(87, 53);
            this.Txt_S_Phone.MaxLength = 19;
            this.Txt_S_Phone.Name = "Txt_S_Phone";
            this.Txt_S_Phone.ReadOnly = true;
            this.Txt_S_Phone.Size = new System.Drawing.Size(271, 20);
            this.Txt_S_Phone.TabIndex = 2;
            // 
            // Txt_Sender
            // 
            this.Txt_Sender.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Sender.Location = new System.Drawing.Point(87, 26);
            this.Txt_Sender.MaxLength = 49;
            this.Txt_Sender.Name = "Txt_Sender";
            this.Txt_Sender.ReadOnly = true;
            this.Txt_Sender.Size = new System.Drawing.Size(271, 20);
            this.Txt_Sender.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Maroon;
            this.label19.Location = new System.Drawing.Point(570, 56);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(87, 14);
            this.label19.TabIndex = 837;
            this.label19.Text = "dd/mm/yyyy";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.label62);
            this.tabPage3.Controls.Add(this.Txt_Pay_Info);
            this.tabPage3.Controls.Add(this.Cbo_Rem_Pay_Type);
            this.tabPage3.Controls.Add(this.label61);
            this.tabPage3.Controls.Add(this.Txt_R_Relation);
            this.tabPage3.Controls.Add(this.label58);
            this.tabPage3.Controls.Add(this.label55);
            this.tabPage3.Controls.Add(this.Txt_R_T_Purpose);
            this.tabPage3.Controls.Add(this.label50);
            this.tabPage3.Controls.Add(this.label49);
            this.tabPage3.Controls.Add(this.label30);
            this.tabPage3.Controls.Add(this.label73);
            this.tabPage3.Controls.Add(this.label70);
            this.tabPage3.Controls.Add(this.label69);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label39);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.Txt_Accproc);
            this.tabPage3.Controls.Add(this.Grd_procer);
            this.tabPage3.Controls.Add(this.Txt_procer);
            this.tabPage3.Controls.Add(this.label68);
            this.tabPage3.Controls.Add(this.label75);
            this.tabPage3.Controls.Add(this.Cbo_Oper);
            this.tabPage3.Controls.Add(this.label76);
            this.tabPage3.Controls.Add(this.checkBox1);
            this.tabPage3.Controls.Add(this.flowLayoutPanel6);
            this.tabPage3.Controls.Add(this.label59);
            this.tabPage3.Controls.Add(this.label71);
            this.tabPage3.Controls.Add(this.cmb_cur);
            this.tabPage3.Controls.Add(this.Txt_Soruce_money);
            this.tabPage3.Controls.Add(this.Txt_T_Purpose);
            this.tabPage3.Controls.Add(this.flowLayoutPanel2);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.Btn_Add);
            this.tabPage3.Controls.Add(this.flowLayoutPanel4);
            this.tabPage3.Controls.Add(this.Txt_notes);
            this.tabPage3.Controls.Add(this.Txt_Tot_amount);
            this.tabPage3.Controls.Add(this.TxtDiscount_Amount);
            this.tabPage3.Controls.Add(this.txt_locamount_rem);
            this.tabPage3.Controls.Add(this.txt_minrate_rem);
            this.tabPage3.Controls.Add(this.txt_maxrate_rem);
            this.tabPage3.Controls.Add(this.Txt_ExRate_Rem);
            this.tabPage3.Controls.Add(this.Txt_Rem_Amount);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(853, 395);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Remmittance information";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(461, 13);
            this.label62.Name = "label62";
            this.label62.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label62.Size = new System.Drawing.Size(143, 14);
            this.label62.TabIndex = 981;
            this.label62.Text = ":Payment Information";
            // 
            // Txt_Pay_Info
            // 
            this.Txt_Pay_Info.BackColor = System.Drawing.Color.White;
            this.Txt_Pay_Info.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Pay_Info.Location = new System.Drawing.Point(608, 9);
            this.Txt_Pay_Info.MaxLength = 199;
            this.Txt_Pay_Info.Name = "Txt_Pay_Info";
            this.Txt_Pay_Info.Size = new System.Drawing.Size(234, 23);
            this.Txt_Pay_Info.TabIndex = 980;
            // 
            // Cbo_Rem_Pay_Type
            // 
            this.Cbo_Rem_Pay_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Rem_Pay_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Rem_Pay_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rem_Pay_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rem_Pay_Type.FormattingEnabled = true;
            this.Cbo_Rem_Pay_Type.Items.AddRange(new object[] {
            "نقـــــــــــــدي",
            "على استحقاق"});
            this.Cbo_Rem_Pay_Type.Location = new System.Drawing.Point(333, 8);
            this.Cbo_Rem_Pay_Type.Name = "Cbo_Rem_Pay_Type";
            this.Cbo_Rem_Pay_Type.Size = new System.Drawing.Size(122, 24);
            this.Cbo_Rem_Pay_Type.TabIndex = 978;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Navy;
            this.label61.Location = new System.Drawing.Point(262, 13);
            this.label61.Name = "label61";
            this.label61.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label61.Size = new System.Drawing.Size(70, 14);
            this.label61.TabIndex = 979;
            this.label61.Text = ": Pay Type";
            // 
            // Txt_R_Relation
            // 
            this.Txt_R_Relation.BackColor = System.Drawing.Color.White;
            this.Txt_R_Relation.Location = new System.Drawing.Point(124, 139);
            this.Txt_R_Relation.MaxLength = 200;
            this.Txt_R_Relation.Name = "Txt_R_Relation";
            this.Txt_R_Relation.Size = new System.Drawing.Size(414, 20);
            this.Txt_R_Relation.TabIndex = 5;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(7, 142);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(114, 15);
            this.label58.TabIndex = 977;
            this.label58.Text = "relationship R/S:";
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label55.ForeColor = System.Drawing.Color.Navy;
            this.label55.Location = new System.Drawing.Point(6, 112);
            this.label55.Name = "label55";
            this.label55.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label55.Size = new System.Drawing.Size(113, 29);
            this.label55.TabIndex = 975;
            this.label55.Text = ": Rec. transfer purpose";
            // 
            // Txt_R_T_Purpose
            // 
            this.Txt_R_T_Purpose.BackColor = System.Drawing.Color.White;
            this.Txt_R_T_Purpose.Location = new System.Drawing.Point(124, 111);
            this.Txt_R_T_Purpose.MaxLength = 199;
            this.Txt_R_T_Purpose.Name = "Txt_R_T_Purpose";
            this.Txt_R_T_Purpose.Size = new System.Drawing.Size(414, 20);
            this.Txt_R_T_Purpose.TabIndex = 4;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(614, 204);
            this.label50.Name = "label50";
            this.label50.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label50.Size = new System.Drawing.Size(159, 14);
            this.label50.TabIndex = 973;
            this.label50.Text = "Amount in local currency";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(530, 204);
            this.label49.Name = "label49";
            this.label49.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label49.Size = new System.Drawing.Size(75, 14);
            this.label49.TabIndex = 972;
            this.label49.Text = "Lower limit";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(431, 204);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(73, 14);
            this.label30.TabIndex = 971;
            this.label30.Text = "Upper limit";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Navy;
            this.label73.Location = new System.Drawing.Point(317, 204);
            this.label73.Name = "label73";
            this.label73.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label73.Size = new System.Drawing.Size(94, 14);
            this.label73.TabIndex = 970;
            this.label73.Text = "Exchange rate";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Navy;
            this.label70.Location = new System.Drawing.Point(54, 204);
            this.label70.Name = "label70";
            this.label70.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label70.Size = new System.Drawing.Size(140, 14);
            this.label70.TabIndex = 969;
            this.label70.Text = "Remmittance amount";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label69.ForeColor = System.Drawing.Color.Navy;
            this.label69.Location = new System.Drawing.Point(216, 204);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(59, 14);
            this.label69.TabIndex = 968;
            this.label69.Text = "currency";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(22, 63);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(98, 14);
            this.label14.TabIndex = 967;
            this.label14.Text = ":Source Money";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(6, 41);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(114, 14);
            this.label39.TabIndex = 966;
            this.label39.Text = ":transfer purpose";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(70, 88);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(50, 14);
            this.label31.TabIndex = 965;
            this.label31.Text = ": Notes";
            // 
            // Txt_Accproc
            // 
            this.Txt_Accproc.BackColor = System.Drawing.Color.White;
            this.Txt_Accproc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Accproc.Location = new System.Drawing.Point(603, 137);
            this.Txt_Accproc.Name = "Txt_Accproc";
            this.Txt_Accproc.ReadOnly = true;
            this.Txt_Accproc.Size = new System.Drawing.Size(239, 23);
            this.Txt_Accproc.TabIndex = 962;
            this.Txt_Accproc.Visible = false;
            // 
            // Grd_procer
            // 
            this.Grd_procer.AllowUserToAddRows = false;
            this.Grd_procer.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_procer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_procer.BackgroundColor = System.Drawing.Color.White;
            this.Grd_procer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_procer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_procer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_procer.ColumnHeadersHeight = 45;
            this.Grd_procer.ColumnHeadersVisible = false;
            this.Grd_procer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_procer.DefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_procer.Location = new System.Drawing.Point(541, 61);
            this.Grd_procer.Name = "Grd_procer";
            this.Grd_procer.ReadOnly = true;
            this.Grd_procer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_procer.RowHeadersVisible = false;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_procer.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_procer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_procer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_procer.Size = new System.Drawing.Size(301, 75);
            this.Grd_procer.TabIndex = 964;
            this.Grd_procer.Visible = false;
            this.Grd_procer.SelectionChanged += new System.EventHandler(this.Grd_procer_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "رمز الوسيط";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ASub_CustName";
            this.dataGridViewTextBoxColumn3.HeaderText = "اسم الوسيط";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // Txt_procer
            // 
            this.Txt_procer.BackColor = System.Drawing.Color.White;
            this.Txt_procer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_procer.Location = new System.Drawing.Point(608, 38);
            this.Txt_procer.Name = "Txt_procer";
            this.Txt_procer.Size = new System.Drawing.Size(234, 22);
            this.Txt_procer.TabIndex = 963;
            this.Txt_procer.Visible = false;
            this.Txt_procer.TextChanged += new System.EventHandler(this.Txt_procer_TextChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Navy;
            this.label68.Location = new System.Drawing.Point(540, 141);
            this.label68.Name = "label68";
            this.label68.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label68.Size = new System.Drawing.Size(62, 14);
            this.label68.TabIndex = 961;
            this.label68.Text = "Account:";
            this.label68.Visible = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Navy;
            this.label75.Location = new System.Drawing.Point(548, 41);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(51, 14);
            this.label75.TabIndex = 960;
            this.label75.Text = "Broker:";
            this.label75.Visible = false;
            // 
            // Cbo_Oper
            // 
            this.Cbo_Oper.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Oper.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Oper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Oper.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Oper.FormattingEnabled = true;
            this.Cbo_Oper.Items.AddRange(new object[] {
            "Cash",
            "Record daily"});
            this.Cbo_Oper.Location = new System.Drawing.Point(124, 8);
            this.Cbo_Oper.Name = "Cbo_Oper";
            this.Cbo_Oper.Size = new System.Drawing.Size(134, 24);
            this.Cbo_Oper.TabIndex = 0;
            this.Cbo_Oper.SelectedIndexChanged += new System.EventHandler(this.Cbo_Oper_SelectedIndexChanged);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Navy;
            this.label76.Location = new System.Drawing.Point(11, 13);
            this.label76.Name = "label76";
            this.label76.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label76.Size = new System.Drawing.Size(104, 14);
            this.label76.TabIndex = 959;
            this.label76.Text = "Operation type:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkBox1.ForeColor = System.Drawing.Color.Maroon;
            this.checkBox1.Location = new System.Drawing.Point(5, 177);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(179, 18);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "Convert of local currency";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(153, 187);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(701, 1);
            this.flowLayoutPanel6.TabIndex = 921;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Navy;
            this.label59.Location = new System.Drawing.Point(549, 257);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(61, 14);
            this.label59.TabIndex = 919;
            this.label59.Text = "Discount";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Navy;
            this.label71.Location = new System.Drawing.Point(572, 283);
            this.label71.Name = "label71";
            this.label71.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label71.Size = new System.Drawing.Size(38, 14);
            this.label71.TabIndex = 906;
            this.label71.Text = "Total";
            // 
            // cmb_cur
            // 
            this.cmb_cur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_cur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_cur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_cur.FormattingEnabled = true;
            this.cmb_cur.Location = new System.Drawing.Point(188, 223);
            this.cmb_cur.Name = "cmb_cur";
            this.cmb_cur.Size = new System.Drawing.Size(118, 24);
            this.cmb_cur.TabIndex = 4;
            this.cmb_cur.SelectedIndexChanged += new System.EventHandler(this.cmb_cur_SelectedIndexChanged);
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.Color.White;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(124, 60);
            this.Txt_Soruce_money.MaxLength = 99;
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(414, 20);
            this.Txt_Soruce_money.TabIndex = 2;
            // 
            // Txt_T_Purpose
            // 
            this.Txt_T_Purpose.BackColor = System.Drawing.Color.White;
            this.Txt_T_Purpose.Location = new System.Drawing.Point(124, 38);
            this.Txt_T_Purpose.MaxLength = 199;
            this.Txt_T_Purpose.Name = "Txt_T_Purpose";
            this.Txt_T_Purpose.ReadOnly = true;
            this.Txt_T_Purpose.Size = new System.Drawing.Size(414, 20);
            this.Txt_T_Purpose.TabIndex = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-1, 307);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(854, 1);
            this.flowLayoutPanel2.TabIndex = 790;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel3.TabIndex = 630;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(466, 311);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 29);
            this.button2.TabIndex = 9;
            this.button2.Text = "End";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(383, 311);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 29);
            this.button1.TabIndex = 8;
            this.button1.Text = "Exoprt";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(301, 311);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(81, 29);
            this.Btn_Add.TabIndex = 7;
            this.Btn_Add.Text = "OK";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(1, 342);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(851, 1);
            this.flowLayoutPanel4.TabIndex = 786;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel5.TabIndex = 630;
            // 
            // Txt_notes
            // 
            this.Txt_notes.BackColor = System.Drawing.Color.White;
            this.Txt_notes.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_notes.Location = new System.Drawing.Point(124, 84);
            this.Txt_notes.MaxLength = 199;
            this.Txt_notes.Name = "Txt_notes";
            this.Txt_notes.Size = new System.Drawing.Size(414, 23);
            this.Txt_notes.TabIndex = 3;
            // 
            // Txt_Tot_amount
            // 
            this.Txt_Tot_amount.BackColor = System.Drawing.Color.White;
            this.Txt_Tot_amount.Enabled = false;
            this.Txt_Tot_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Tot_amount.Location = new System.Drawing.Point(617, 279);
            this.Txt_Tot_amount.Name = "Txt_Tot_amount";
            this.Txt_Tot_amount.NumberDecimalDigits = 3;
            this.Txt_Tot_amount.NumberDecimalSeparator = ".";
            this.Txt_Tot_amount.NumberGroupSeparator = ",";
            this.Txt_Tot_amount.Size = new System.Drawing.Size(139, 23);
            this.Txt_Tot_amount.TabIndex = 10;
            this.Txt_Tot_amount.Text = "0.000";
            // 
            // TxtDiscount_Amount
            // 
            this.TxtDiscount_Amount.BackColor = System.Drawing.Color.White;
            this.TxtDiscount_Amount.Enabled = false;
            this.TxtDiscount_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscount_Amount.Location = new System.Drawing.Point(617, 253);
            this.TxtDiscount_Amount.Name = "TxtDiscount_Amount";
            this.TxtDiscount_Amount.NumberDecimalDigits = 3;
            this.TxtDiscount_Amount.NumberDecimalSeparator = ".";
            this.TxtDiscount_Amount.NumberGroupSeparator = ",";
            this.TxtDiscount_Amount.Size = new System.Drawing.Size(139, 23);
            this.TxtDiscount_Amount.TabIndex = 9;
            this.TxtDiscount_Amount.Text = "0.000";
            // 
            // txt_locamount_rem
            // 
            this.txt_locamount_rem.BackColor = System.Drawing.Color.White;
            this.txt_locamount_rem.Enabled = false;
            this.txt_locamount_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_locamount_rem.Location = new System.Drawing.Point(617, 224);
            this.txt_locamount_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_locamount_rem.Name = "txt_locamount_rem";
            this.txt_locamount_rem.NumberDecimalDigits = 3;
            this.txt_locamount_rem.NumberDecimalSeparator = ".";
            this.txt_locamount_rem.NumberGroupSeparator = ",";
            this.txt_locamount_rem.Size = new System.Drawing.Size(139, 23);
            this.txt_locamount_rem.TabIndex = 8;
            this.txt_locamount_rem.Text = "0.000";
            // 
            // txt_minrate_rem
            // 
            this.txt_minrate_rem.BackColor = System.Drawing.Color.White;
            this.txt_minrate_rem.Enabled = false;
            this.txt_minrate_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_minrate_rem.Location = new System.Drawing.Point(527, 224);
            this.txt_minrate_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_minrate_rem.Name = "txt_minrate_rem";
            this.txt_minrate_rem.NumberDecimalDigits = 7;
            this.txt_minrate_rem.NumberDecimalSeparator = ".";
            this.txt_minrate_rem.NumberGroupSeparator = ",";
            this.txt_minrate_rem.ReadOnly = true;
            this.txt_minrate_rem.Size = new System.Drawing.Size(89, 23);
            this.txt_minrate_rem.TabIndex = 7;
            this.txt_minrate_rem.Text = "0.0000000";
            // 
            // txt_maxrate_rem
            // 
            this.txt_maxrate_rem.BackColor = System.Drawing.Color.White;
            this.txt_maxrate_rem.Enabled = false;
            this.txt_maxrate_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maxrate_rem.Location = new System.Drawing.Point(418, 224);
            this.txt_maxrate_rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txt_maxrate_rem.Name = "txt_maxrate_rem";
            this.txt_maxrate_rem.NumberDecimalDigits = 7;
            this.txt_maxrate_rem.NumberDecimalSeparator = ".";
            this.txt_maxrate_rem.NumberGroupSeparator = ",";
            this.txt_maxrate_rem.ReadOnly = true;
            this.txt_maxrate_rem.Size = new System.Drawing.Size(106, 23);
            this.txt_maxrate_rem.TabIndex = 6;
            this.txt_maxrate_rem.Text = "0.0000000";
            // 
            // Txt_ExRate_Rem
            // 
            this.Txt_ExRate_Rem.BackColor = System.Drawing.Color.White;
            this.Txt_ExRate_Rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ExRate_Rem.Location = new System.Drawing.Point(309, 224);
            this.Txt_ExRate_Rem.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_ExRate_Rem.Name = "Txt_ExRate_Rem";
            this.Txt_ExRate_Rem.NumberDecimalDigits = 7;
            this.Txt_ExRate_Rem.NumberDecimalSeparator = ".";
            this.Txt_ExRate_Rem.NumberGroupSeparator = ",";
            this.Txt_ExRate_Rem.ReadOnly = true;
            this.Txt_ExRate_Rem.Size = new System.Drawing.Size(106, 23);
            this.Txt_ExRate_Rem.TabIndex = 5;
            this.Txt_ExRate_Rem.Text = "0.0000000";
            this.Txt_ExRate_Rem.TextChanged += new System.EventHandler(this.Txt_ExRate_Rem_TextChanged);
            // 
            // Txt_Rem_Amount
            // 
            this.Txt_Rem_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_Rem_Amount.Enabled = false;
            this.Txt_Rem_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Rem_Amount.Location = new System.Drawing.Point(47, 224);
            this.Txt_Rem_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Rem_Amount.Name = "Txt_Rem_Amount";
            this.Txt_Rem_Amount.NumberDecimalDigits = 3;
            this.Txt_Rem_Amount.NumberDecimalSeparator = ".";
            this.Txt_Rem_Amount.NumberGroupSeparator = ",";
            this.Txt_Rem_Amount.ReadOnly = true;
            this.Txt_Rem_Amount.Size = new System.Drawing.Size(139, 23);
            this.Txt_Rem_Amount.TabIndex = 3;
            this.Txt_Rem_Amount.Text = "0.000";
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(11, 33);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtBox_User.Size = new System.Drawing.Size(161, 23);
            this.TxtBox_User.TabIndex = 713;
            // 
            // Txtcity_con_online
            // 
            this.Txtcity_con_online.Location = new System.Drawing.Point(601, 32);
            this.Txtcity_con_online.Multiline = true;
            this.Txtcity_con_online.Name = "Txtcity_con_online";
            this.Txtcity_con_online.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtcity_con_online.Size = new System.Drawing.Size(182, 25);
            this.Txtcity_con_online.TabIndex = 900;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(786, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 14);
            this.label12.TabIndex = 902;
            this.label12.Text = ":Country & City";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(492, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 14);
            this.label1.TabIndex = 903;
            this.label1.Text = ": User";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(179, 9);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(44, 14);
            this.label4.TabIndex = 904;
            this.label4.Text = ": Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(494, 37);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(98, 14);
            this.label3.TabIndex = 905;
            this.label3.Text = ":Local currency";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(180, 37);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(38, 14);
            this.label46.TabIndex = 906;
            this.label46.Text = ": Box";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(392, 71);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(127, 14);
            this.label2.TabIndex = 907;
            this.label2.Text = "Remmitance Details";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Maroon;
            this.label77.Location = new System.Drawing.Point(673, 237);
            this.label77.Name = "label77";
            this.label77.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label77.Size = new System.Drawing.Size(201, 14);
            this.label77.TabIndex = 947;
            this.label77.Text = "Number of remittances received";
            this.label77.Click += new System.EventHandler(this.label77_Click);
            // 
            // Cbo_Rcur
            // 
            this.Cbo_Rcur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rcur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rcur.FormattingEnabled = true;
            this.Cbo_Rcur.Location = new System.Drawing.Point(605, 198);
            this.Cbo_Rcur.Name = "Cbo_Rcur";
            this.Cbo_Rcur.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cbo_Rcur.Size = new System.Drawing.Size(184, 22);
            this.Cbo_Rcur.TabIndex = 1127;
            this.Cbo_Rcur.SelectedIndexChanged += new System.EventHandler(this.Cbo_Rcur_SelectedIndexChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Navy;
            this.label51.Location = new System.Drawing.Point(790, 202);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(98, 14);
            this.label51.TabIndex = 1126;
            this.label51.Text = ":Rem. currency";
            // 
            // Tot_Amount
            // 
            this.Tot_Amount.BackColor = System.Drawing.Color.White;
            this.Tot_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount.Location = new System.Drawing.Point(319, 198);
            this.Tot_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount.Name = "Tot_Amount";
            this.Tot_Amount.NumberDecimalDigits = 3;
            this.Tot_Amount.NumberDecimalSeparator = ".";
            this.Tot_Amount.NumberGroupSeparator = ",";
            this.Tot_Amount.ReadOnly = true;
            this.Tot_Amount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Tot_Amount.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount.TabIndex = 1129;
            this.Tot_Amount.Text = "0.000";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(480, 202);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(94, 14);
            this.label52.TabIndex = 1128;
            this.label52.Text = ":Rem. amount";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Navy;
            this.label53.Location = new System.Drawing.Point(171, 201);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(82, 14);
            this.label53.TabIndex = 1131;
            this.label53.Text = ":Rem. count";
            // 
            // Txt_RCount
            // 
            this.Txt_RCount.BackColor = System.Drawing.Color.White;
            this.Txt_RCount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_RCount.Location = new System.Drawing.Point(16, 197);
            this.Txt_RCount.Name = "Txt_RCount";
            this.Txt_RCount.ReadOnly = true;
            this.Txt_RCount.Size = new System.Drawing.Size(151, 22);
            this.Txt_RCount.TabIndex = 1130;
            // 
            // Print_All
            // 
            this.Print_All.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Print_All.ForeColor = System.Drawing.Color.Navy;
            this.Print_All.Location = new System.Drawing.Point(16, 241);
            this.Print_All.Name = "Print_All";
            this.Print_All.Size = new System.Drawing.Size(130, 29);
            this.Print_All.TabIndex = 1132;
            this.Print_All.Text = "export";
            this.Print_All.UseVisualStyleBackColor = true;
            this.Print_All.Click += new System.EventHandler(this.Print_All_Click);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Maroon;
            this.label80.Location = new System.Drawing.Point(534, 237);
            this.label80.Name = "label80";
            this.label80.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label80.Size = new System.Drawing.Size(66, 14);
            this.label80.TabIndex = 1133;
            this.label80.Text = "BlackLists";
            this.label80.Click += new System.EventHandler(this.label80_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label60.ForeColor = System.Drawing.Color.Red;
            this.label60.Location = new System.Drawing.Point(326, 259);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(17, 16);
            this.label60.TabIndex = 1134;
            this.label60.Text = "*";
            // 
            // Delivery_Add_eng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 679);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.Print_All);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.Txt_RCount);
            this.Controls.Add(this.Tot_Amount);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.Cbo_Rcur);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txtcity_con_online);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Grdrec_rem);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Delivery_Add_eng";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pay Out Remittance";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Delivery_Add_FormClosed);
            this.Load += new System.EventHandler(this.Delivery_Add_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_CustSen_Name)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_procer)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.DataGridView Grdrec_rem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Txt_notes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.TextBox Txt_r_Doc_No;
        private System.Windows.Forms.TextBox Txt_Doc_r_Issue;
        private System.Windows.Forms.ComboBox Cmb_r_Doc_Type;
        private System.Windows.Forms.TextBox Txt_Reciever;
        private System.Windows.Forms.ComboBox Cmb_R_City;
        private System.Windows.Forms.ComboBox Cmb_R_Nat;
        private System.Windows.Forms.ComboBox Cmb_phone_Code_R;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.TextBox Txt_Sender;
        private System.Windows.Forms.TextBox Txt_S_Phone;
        private System.Windows.Forms.TextBox Txt_job_Sender;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.TextBox Txtr_Suburb;
        private System.Windows.Forms.TextBox Txtr_Street;
        private System.Windows.Forms.TextBox Txtr_Post_Code;
        private System.Windows.Forms.TextBox Txtr_State;
        private System.Windows.Forms.TextBox TxtS_State;
        private System.Windows.Forms.TextBox TxtS_Post_Code;
        private System.Windows.Forms.TextBox Txts_street;
        private System.Windows.Forms.TextBox txtS_Suburb;
        private System.Windows.Forms.TextBox Txt_T_Purpose;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.ComboBox cmb_Gender_id;
        private System.Windows.Forms.TextBox Txtcity_con_online;
        private System.Windows.Forms.TextBox txts_city;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.ComboBox cmb_cur;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Tot_amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDiscount_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox txt_locamount_rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_minrate_rem;
        private System.Windows.Forms.Sample.DecimalTextBox txt_maxrate_rem;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_ExRate_Rem;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Rem_Amount;
        private System.Windows.Forms.TextBox txt_Mother_name;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox Txt_mail;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.DataGridView Grd_CustSen_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox Txt_sec_no;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.DateTimePicker Txt_Doc_r_Date;
        private System.Windows.Forms.DateTimePicker Txt_Doc_r_Exp;
        private System.Windows.Forms.DateTimePicker Txt_rbirth_Date;
        private System.Windows.Forms.DateTimePicker Txt_S_Birth;
        private System.Windows.Forms.TextBox txt_rbirth_place;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.ComboBox Cbo_Oper;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox Txt_Accproc;
        private System.Windows.Forms.DataGridView Grd_procer;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox Txt_procer;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox Cbo_Rcur;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox Txt_RCount;
        private System.Windows.Forms.Button Print_All;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox resd_cmb;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox Txt_Social_No;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox Txt_R_T_Purpose;
        private System.Windows.Forms.Button ar_sen;
        private System.Windows.Forms.Button ENAR_BTN;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmb_job_receiver;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox Txt_Real_Paid_Name;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox Txt_R_details_job;
        private System.Windows.Forms.TextBox Txt_R_Relation;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox Txt_Pay_Info;
        private System.Windows.Forms.ComboBox Cbo_Rem_Pay_Type;
        private System.Windows.Forms.Label label61;
    }
}