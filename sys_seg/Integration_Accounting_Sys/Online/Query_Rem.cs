﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

namespace Integration_Accounting_Sys
{
    public partial class Query_Rem : Form
    {
     
        BindingSource Bs_Quey = new BindingSource();
        BindingSource Bs_Qcus = new BindingSource();
        string @format = "yyyy/MM/dd";
        string @null = "0000/00/00";
        int resd_ID_sen = 0;
        int resd_ID_rec = 0;
        string Sql_Text = "";
        BindingSource Bs_job_sen = new BindingSource();
        BindingSource Bs_job_rec = new BindingSource();
        BindingSource Bs_full = new BindingSource();
        BindingSource Bs_doc_sender = new BindingSource();
        BindingSource Bs_doc_rec = new BindingSource();
        BindingSource binding_cb_snat = new BindingSource();
        BindingSource binding_cb_rnat = new BindingSource();
        int cust_id_online = 0;
        string SqlTxt_full = "";
 
        bool Xchangeus = false;
        bool cbo_change = false;
        bool change_R_Type_Id = false;

        public Query_Rem()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            if (connection.Lang_id != 1)
            { 
                Cbo_R_Type_Id.Items[0] = "Rem. Out ";
                Cbo_R_Type_Id.Items[1] = "Rem. In ";

             
                Cbo_Rem_OFF_ON.Items[0] = "OFFline Rem.";
                Cbo_Rem_OFF_ON.Items[1] = "Online  Rem.";

                Cbo_Data_Type.Items[0] = "Current data";
                Cbo_Data_Type.Items[1] = "Migrated data";


            }
        }


        private void Query_Rem_Load(object sender, EventArgs e)
        {
            cbo_change = false;
            change_R_Type_Id = false;
            Cbo_R_Type_Id.SelectedIndex = 0;
            Cbo_Rem_OFF_ON.SelectedIndex = 1;
            connection.SqlExec("exec Search_Qurey_Rem " + Cbo_R_Type_Id.SelectedIndex + "," + connection.T_ID, "Qurey_Rem_tbl");
            TxtFrom_Da_ValueChanged(null, null);
            TxtTo_Da_ValueChanged(null, null);
           // txt_case_from_ValueChanged(null, null);
           // txt_case_to_ValueChanged(null, null);

            cbo_change = true;
            Cbo_Rem_OFF_ON_SelectedIndexChanged(null, null);
            
            cmb_range.SelectedIndex = 0;
            Cbo_Data_Type.SelectedIndex = 0;

            Cmb_R_CUR.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl2"];
            Cmb_R_CUR.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
            Cmb_R_CUR.ValueMember = "R_Cur_Id";

            Cmb_Pr_Cur_id.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl3"];
            Cmb_Pr_Cur_id.DisplayMember = connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME";
            Cmb_Pr_Cur_id.ValueMember = "PR_cur_id";


            resd_rec_cmb.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl7"];
            resd_rec_cmb.DisplayMember = connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename";
            resd_rec_cmb.ValueMember = "resd_flag_rec";

            resd_sen_cmb.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl6"];
            resd_sen_cmb.DisplayMember = connection.Lang_id == 1 ? "resd_flag_Aname" : "resd_flag_Ename";
            resd_sen_cmb.ValueMember = "resd_flag";

            change_R_Type_Id = true;
            Cbo_R_Type_Id_SelectedIndexChanged(null, null);

            cmb_range.Items[0] = "=";
            cmb_range.Items[1] = "<=";
            cmb_range.Items[2] = ">=";


            Sql_Text = " Exec all_master ";
            connection.SqlExec(Sql_Text, "all_master");
            Bs_job_sen.DataSource = connection.SQLDS.Tables["all_master8"];
            Cbo_Occupation_sen.DataSource = Bs_job_sen;
            Cbo_Occupation_sen.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";
            Cbo_Occupation_sen.ValueMember = "Job_ID";

            Bs_job_rec.DataSource = connection.SQLDS.Tables["all_master8"];
            Cbo_Occupation_rec.DataSource = Bs_job_rec;
            Cbo_Occupation_rec.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";
            Cbo_Occupation_rec.ValueMember = "Job_ID";

            string SqlTxt_cust = " Select  CUST_ID From  TERMINALS where t_id = " + connection.T_ID;
            connection.SqlExec(SqlTxt_cust, "Cust_tbl");

            cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);
            

            Bs_full.DataSource = connection.SQLDS.Tables["all_master9"];
            Cbo_purpos.DataSource = Bs_full;
            Cbo_purpos.DisplayMember = connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename";
            Cbo_purpos.ValueMember = "Case_purpose_id";

            Bs_doc_sender.DataSource = connection.SQLDS.Tables["all_master12"];
            Cbo_doc_Sen.DataSource = Bs_doc_sender;
            Cbo_doc_Sen.DisplayMember = connection.Lang_id == 1 ? "Fmd_AName" : "Fmd_EName";
            Cbo_doc_Sen.ValueMember = "Fmd_ID";

            Bs_doc_rec.DataSource = connection.SQLDS.Tables["all_master12"];
            Cbo_doc_Rec.DataSource = Bs_doc_rec;
            Cbo_doc_Rec.DisplayMember = connection.Lang_id == 1 ? "Fmd_AName" : "Fmd_EName";
            Cbo_doc_Rec.ValueMember = "Fmd_ID";

            binding_cb_snat.DataSource = connection.SQLDS.Tables["all_master1"];
            cmb_s_nat.DataSource = binding_cb_snat;
            cmb_s_nat.ValueMember = "Nat_ID";
            cmb_s_nat.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "NAT_ENAME";
            ///------------------
            binding_cb_rnat.DataSource = connection.SQLDS.Tables["all_master1"];
            Cmb_R_Nat.DataSource = binding_cb_rnat;
            Cmb_R_Nat.ValueMember = "Nat_ID";
            Cmb_R_Nat.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "NAT_ENAME";
                      
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int from_date = 0;
            int to_date = 0;
            string X = TxtFrom_Da.Text;

            //=================

            if (TxtFrom_Da.Checked == true)
            {
                DateTime picker = TxtFrom_Da.Value;
                from_date = picker.Date.Day + picker.Date.Month * 100 + picker.Date.Year * 10000;
            }
            else
            {
             if (Txt_S_Name.Text.Trim() != "" || Txt_R_Name.Text.Trim() != "" || Txt_Rem_No.Text.Trim() != "")
                { from_date = 0; }

                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ بداية الفترة" : "select the date of first period", MyGeneral_Lib.LblCap);
                    return;
                }
            }

            if (TxtTo_Da.Checked == true)
            {

                DateTime picker_to = TxtTo_Da.Value;
                to_date = picker_to.Date.Day + picker_to.Date.Month * 100 + picker_to.Date.Year * 10000;
            }
            else
            {
                if (Txt_S_Name.Text.Trim() != "" || Txt_R_Name.Text.Trim() != "" || Txt_Rem_No.Text.Trim() != "")
                { to_date = 0; }

                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }
            }

            if (TxtFrom_Da.Checked == true && TxtTo_Da.Checked == true && (Convert.ToInt32(from_date) > Convert.ToInt32(to_date)))
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                return;
            }

            //if (TxtFrom_Da.Checked == true && TxtTo_Da.Checked == true &&  Txt_S_Name.Text.Trim() == "" && Txt_R_Name.Text.Trim() == "" && Txt_Rem_No.Text.Trim() == "")
            //{

            //    DateTime datataime_from = Convert.ToDateTime(TxtFrom_Da.Value.Date);
            //    DateTime datatimeto = Convert.ToDateTime(TxtTo_Da.Value.Date);
            //    try
            //    {
            //        TimeSpan difference = datatimeto - datataime_from;

            //        if (Convert.ToDecimal(difference.TotalDays) >= 90)
            //        {
            //            MessageBox.Show(connection.Lang_id == 2 ? "You must choose a period not exceeding three months" : "يجب اختيار فترة لاتتجاوز ثلاثة اشهر", MyGeneral_Lib.LblCap);
            //            return;
            //        }
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}




            //===================


            //if (TxtFrom_Da.Text == "0000/00/00" || TxtFrom_Da.Text == "" || TxtFrom_Da.Text == "00/00/0000")
            //{ from_date = 0; }
            //else
            //{
            //    DateTime picker = TxtFrom_Da.Value;
            //    from_date = picker.Date.Day + picker.Date.Month * 100 + picker.Date.Year * 10000;
            //}

            //if (TxtTo_Da.Text == "0000/00/00" || TxtTo_Da.Text == "" || TxtTo_Da.Text == "00/00/0000")
            //{ to_date = 0; }
            //else
            //{
            //    DateTime picker_to = TxtTo_Da.Value;
            //    to_date = picker_to.Date.Day + picker_to.Date.Month * 100 + picker_to.Date.Year * 10000;
            //}

           
            Int16 Cust_ID = 0;
            Int16 sub_Cust_ID = 0;
            Int16 T_id_current = 0;

            sub_Cust_ID = Convert.ToInt16(CboCust_Id.SelectedValue);
            Cust_ID = Convert.ToInt16(((DataRowView)Bs_Qcus.Current).Row["cust_id"]);
            T_id_current = Convert.ToInt16(((DataRowView)Bs_Qcus.Current).Row["T_id"]);

            Int16 rem_flag = 0;
            
            if (Cbo_Rem_OFF_ON.SelectedIndex == 0)
            {
                rem_flag = 1;
            }
            else 
            {
                rem_flag = 11;
            }
            resd_ID_sen = Convert.ToInt16(resd_sen_cmb.SelectedValue);
            resd_ID_rec = Convert.ToInt16(resd_rec_cmb.SelectedValue);

            string proc = Cbo_Data_Type.SelectedIndex == 0 ? "EXec main_Query_remittences_Windows " : "EXec main_Query_remittences_Windows_phst ";

            string Sqltexe = proc + from_date + "," + to_date + "," + CHK1.Checked + ","
                + "'" + Txt_R_Name.Text.Trim() + "'" + "," + CHK2.Checked + "," +
                                    "'" + Txt_S_Name.Text.Trim() + "'" + "," + CmbRcity.SelectedValue + "," + Cmb_R_CUR.SelectedValue + "," + "'" + cmb_range.Text.Trim() + "'" + "," +
            Convert.ToDecimal(Txt_Amonut.Text.Trim()) + "," + CmbScity.SelectedValue + "," + "'" + Txt_Rem_No.Text.Trim() + "'" + "," + "'" + Txt_Purpose.Text.Trim() + "'" + "," + "'" + Txt_s_phone.Text.Trim() + "'" + "," + "'" + Txt_R_Phone.Text.Trim() + "'" + "," +
                                  "'" + Txt_Note.Text.Trim() + "'" + "," + Cbo_R_Type_Id.SelectedIndex + "," + Cmb_Case_id.SelectedValue + "," + Cmb_Pr_Cur_id.SelectedValue + "," + Cust_ID + "," + sub_Cust_ID + "," + Cmb_sub_User_id.SelectedValue + "," + rem_flag + "," + resd_ID_sen + "," + resd_ID_rec
                                 + "," + "'" + txt_social_no_rec.Text + "'" + "," + "'" + txt_social_no_sen.Text + "'" + "," + Cbo_Occupation_sen.SelectedValue + "," + Cbo_Occupation_rec.SelectedValue + "," + Cbo_purpos.SelectedValue
                                 + "," + Cbo_doc_Sen.SelectedValue + "," + Cbo_doc_Rec.SelectedValue + "," + "'" + Txt_S_doc_no.Text + "'" + "," + "'" + Txt_R_doc_no.Text + "'" + "," + cmb_s_nat.SelectedValue + "," + Cmb_R_Nat.SelectedValue
                                 + "," + Convert.ToInt16(Chk_Last_Case.Checked) + "," + Convert.ToInt16(Chk_Hst_Info.Checked) + "," + T_id_current;


            connection.SqlExec(Sqltexe, "Query_remittences_tbl");
            if (connection.SQLDS.Tables["Query_remittences_tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حوالات تحقق الشروط " : "No Rem. For this cases", MyGeneral_Lib.LblCap);
                return;
            }

            int R_Type_Id=0;
            R_Type_Id = Cbo_R_Type_Id.SelectedIndex;
            remittences_online.Inquery_Remittances Frm = new remittences_online.Inquery_Remittances(R_Type_Id, rem_flag);
            this.Visible = false;
            Frm.ShowDialog();
            this.Visible = true;
            

        }

        private void Cbo_R_Type_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_R_Type_Id)
            {
                try
                {
                    if (Cbo_R_Type_Id.SelectedIndex == 0 && connection.SQLDS.Tables["Qurey_Rem_tbl"].Rows.Count > 0) //صادر
                    {
                        Cmb_Case_id.DataSource = new DataTable();
                        Cmb_Case_id.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl4"];
                        Cmb_Case_id.DisplayMember = connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA";
                        Cmb_Case_id.ValueMember = "CASE_ID";

                        // Cbo_purpos.Enabled = true;

                    }
                    else
                        if (Cbo_R_Type_Id.SelectedIndex == 1 && connection.SQLDS.Tables["Qurey_Rem_tbl"].Rows.Count > 0) //وارد
                        {
                            Cmb_Case_id.DataSource = new DataTable();
                            Cmb_Case_id.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl5"];
                            Cmb_Case_id.DisplayMember = connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA";
                            Cmb_Case_id.ValueMember = "CASE_ID";

                            //  Cbo_purpos.Enabled = false;

                        }
                }
                catch { }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtFrom_Da_ValueChanged(object sender, EventArgs e)
        {
            if (TxtFrom_Da.Checked == true)
            {
                TxtFrom_Da.Format = DateTimePickerFormat.Custom;
                TxtFrom_Da.CustomFormat = @format;
            }
            else
            {
                TxtFrom_Da.Format = DateTimePickerFormat.Custom;
                TxtFrom_Da.CustomFormat = @null;
            }
        }

        private void TxtTo_Da_ValueChanged(object sender, EventArgs e)
        {
            if (TxtTo_Da.Checked == true)
            {
                TxtTo_Da.Format = DateTimePickerFormat.Custom;
                TxtTo_Da.CustomFormat = @format;
            }
            else
            {
                TxtTo_Da.Format = DateTimePickerFormat.Custom;
                TxtTo_Da.CustomFormat = @null;
            }
        }

        //private void txt_case_from_ValueChanged(object sender, EventArgs e)
        //{
        //    if (txt_case_from.Checked == true)
        //    {
        //        txt_case_from.Format = DateTimePickerFormat.Custom;
        //        txt_case_from.CustomFormat = @format;
        //    }
        //    else
        //    {
        //        txt_case_from.Format = DateTimePickerFormat.Custom;
        //        txt_case_from.CustomFormat = @null;
        //    }
        //}

        //private void txt_case_to_ValueChanged(object sender, EventArgs e)
        //{
        //    if (txt_case_to.Checked == true)
        //    {
        //        txt_case_to.Format = DateTimePickerFormat.Custom;
        //        txt_case_to.CustomFormat = @format;
        //    }
        //    else
        //    {
        //        txt_case_to.Format = DateTimePickerFormat.Custom;
        //        txt_case_to.CustomFormat = @null;
        //    }

        //}

        private void CboCust_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Xchangeus)
            {
                  string sql = "";
                   
                  Int16 sub_cust_id1 =  Convert.ToInt16(CboCust_Id.SelectedValue);
                    sql = "select distinct user_name , user_name as user_name1, C.user_id   , sub_cust_id "
                        + " from    remittences_online B ,  Login_Cust_Online C "
                        + " where B.user_id is not null "
                        + " and C.user_id = B.user_id "
                        + " and sub_cust_id = " + sub_cust_id1
                        + " union "
                        + " select Distinct  'جميع المستخدمين' As user_name ,  'all users' as  user_ename, 0 as user_id  , 0 as sub_cust_id "
                        + " order by user_id ";
                     connection.SqlExec(sql, "Get_Sub_users");
                if (connection.SQLDS.Tables["Get_Sub_users"].Rows.Count > 1)
                {
                    Cmb_sub_User_id.DataSource = connection.SQLDS.Tables["Get_Sub_users"];
                    Cmb_sub_User_id.DisplayMember = connection.Lang_id == 1 ? "user_name" : "user_name1";
                    Cmb_sub_User_id.ValueMember = "user_id";
                }
                else

                {

                sql = "   select distinct user_name , user_name as user_name1, A.t_user_id as  user_id "
                + " from  User_Terminals A , Users b "
                + " where  A.T_User_Id = B.user_id  "
                + " and A.T_Id = " + connection.T_ID  
                + " and T_User_Id in(select User_ID from remittences_online ) "
                + " union " 
                + " select Distinct  'جميع المستخدمين' As user_name ,  'all users' as  user_ename, 0 as user_id    " 
                + " order by user_id   " ;


                Cmb_sub_User_id.DataSource = connection.SqlExec(sql, "Get_users_TBL");
                Cmb_sub_User_id.DisplayMember = connection.Lang_id == 1 ? "user_name" : "user_name1";
                Cmb_sub_User_id.ValueMember = "user_id";
                
                }
            }
        }

        private void Cbo_Rem_OFF_ON_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_change)
            {
                //if (Convert.ToInt16(Cbo_Rem_OFF_ON.SelectedIndex) == 0)//offline
                //{
                //    Xchangeus = false;
                //    string Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name  , A.cust_id as cust_id  , 0 as  Sub_Cust_ID  "
                //           + "  From CUSTOMERS A, TERMINALS B   "
                //           + "  Where A.CUST_ID = B.CUST_ID "
                //           + "  And A.Cust_Flag <>  0 "
                //           + "  and B.t_id =  " + connection.T_ID
                //           + " union "
                //           + " Select Distinct C.T_id , cust_broker_aname as Acust_name , cust_broker_ename as Ecust_name  , 0 as cust_id , cust_broker_id as  Sub_Cust_ID "
                //           + " from [dbo].[remittences_online] A , Customers B , Terminals C  "
                //           + " where cust_broker_id is not null "
                //           + " and B.CUST_ID = C.CUST_ID  "
                //           + "   "
                //           + "  union "
                //           + "  select " +connection.T_ID + " as T_id,'إختر الفرع اوالوكيل'  as Acust_name , "
                //           + "  'All Branches and sub branches' as     Ecust_name ,0 as    cust_id  , 0 as    Sub_Cust_ID    "
                //           + " order by cust_id ";
                //    Bs_Qcus.DataSource = connection.SqlExec(Sql_Text, "cust_TBL");
                //    CboCust_Id.DataSource = Bs_Qcus;
                //    CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                //    CboCust_Id.ValueMember = "Sub_Cust_ID";
                //    Xchangeus = true;
                //    CboCust_Id_SelectedIndexChanged(null, null);

                //    CmbScity.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl"].Select("rem_flag in(1,0)").CopyToDataTable();
                //    CmbScity.DisplayMember = connection.Lang_id == 1 ? "S_acity_name" : "S_ECITY_NAME";
                //    CmbScity.ValueMember = "S_City_id";

                //    CmbRcity.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl1"].Select("rem_flag in(1,0)").CopyToDataTable(); ;
                //    CmbRcity.DisplayMember = connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME";
                //    CmbRcity.ValueMember = "t_city_id";
                //}
                //else//online
                //{

                    Xchangeus = false;
                    string Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name  , A.cust_id as cust_id  , 0 as  Sub_Cust_ID  "
                           + "  From CUSTOMERS A, TERMINALS B   "
                           + "  Where A.CUST_ID = B.CUST_ID "
                           + "  And A.Cust_Flag <>0  "
                           + "  and B.t_id =  " + connection.T_ID
                           + " union "
                           + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  A.Cust_Online_Main_Id  as cust_id ,   Sub_Cust_ID "
                           + " From  Sub_CUSTOMERS_online A  , Login_Cust_Online B  "
                           + " Where A.Cust_state  =  1 "
                           + " and A.Sub_Cust_ID =  B.Cust_id"
                           + " and A.t_id =  " + connection.T_ID
                           + "  union "
                           + "  select " + connection.T_ID + " as T_id,'إختر الفرع اوالوكيل'  as Acust_name , "
                           + "  'All Branches and sub branches' as     Ecust_name ,0 as    cust_id  , 0 as    Sub_Cust_ID    "
                           + " order by cust_id ";
                    Bs_Qcus.DataSource = connection.SqlExec(Sql_Text, "cust_TBL");
                    CboCust_Id.DataSource = Bs_Qcus;
                    CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                    CboCust_Id.ValueMember = "Sub_Cust_ID";
                    Xchangeus = true;
                    CboCust_Id_SelectedIndexChanged(null, null);

                    CmbScity.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl"].Select("rem_flag in(11,0)").CopyToDataTable();
                    CmbScity.DisplayMember = connection.Lang_id == 1 ? "S_acity_name" : "S_ECITY_NAME";
                    CmbScity.ValueMember = "S_City_id";

                    CmbRcity.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl1"].Select("rem_flag in(11,0)").CopyToDataTable(); ;
                    CmbRcity.DisplayMember = connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME";
                    CmbRcity.ValueMember = "t_city_id";

                //}
            }
        }
    }
}