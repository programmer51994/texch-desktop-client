﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Delivery_Rem_Search : Form
    {
        BindingSource binding_cbo_tcity = new BindingSource();
        BindingSource binding_pay_rcur = new BindingSource();
        BindingSource binding_s_city = new BindingSource();
        string city_con_online = "";
        int city_con_online_id = 0;
        int Cust_id = 0;
        public static int local_inter_flag = 0;
        //-------------------

        public Delivery_Rem_Search()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
        }

        private void Delivery_Rem_Search_Load(object sender, EventArgs e)
        {
           
            if (connection.Lang_id == 2)
            {
                Cbo_Loc_Inter.Items[0] = "Local network-Offline";
                Cbo_Loc_Inter.Items[1] = "Online Network";
            }
            string SqlTxt = " Select  CUST_ID From  TERMINALS where t_id = " + connection.T_ID;
            connection.SqlExec(SqlTxt, "Cust_tbl");


            Cust_id = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);

            Cbo_Loc_Inter.SelectedIndex = 1;

               connection.SqlExec("exec Get_information_inrem " + Cust_id, "Get_info_Tbl");
                binding_cbo_tcity.DataSource = connection.SQLDS.Tables["Get_info_Tbl"];
                Cbo_city.DataSource = binding_cbo_tcity.DataSource;
                Cbo_city.ValueMember = "CITY_ID";
                Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";


                city_con_online = Cbo_city.Text;
                city_con_online_id = Convert.ToInt32(Cbo_city.SelectedValue);

                binding_pay_rcur.DataSource = connection.SQLDS.Tables["Get_info_Tbl1"];
                Cmb_R_CUR_ID.DataSource = binding_pay_rcur;
                Cmb_R_CUR_ID.ValueMember = "cur_id";
                Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "ACur_Name" : "ECur_Name";

                binding_s_city.DataSource = connection.SQLDS.Tables["Get_info_Tbl2"];
                Cmb_Scity_ID.DataSource = binding_s_city;
                Cmb_Scity_ID.ValueMember = "Cit_ID";
                Cmb_Scity_ID.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";
      

        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            local_inter_flag = Cbo_Loc_Inter.SelectedIndex  ; //محلي 
          
            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "[Main_delivery_Rem_Web]";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.CommandTimeout = 0;
            connection.SQLCMD.Parameters.AddWithValue("@R_name",  Txt_Reciever.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@S_name", Txt_Sender.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@T_city_id",  Cbo_city.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@R_cur_id",Cmb_R_CUR_ID.SelectedValue );
            connection.SQLCMD.Parameters.AddWithValue("@Amount", Convert.ToDecimal(Txtr_amount.Text));
            connection.SQLCMD.Parameters.AddWithValue("@S_city_id",Cmb_Scity_ID.SelectedValue );
            connection.SQLCMD.Parameters.AddWithValue("@rem_no", Txtrem_no.Text.Trim() );
            connection.SQLCMD.Parameters.AddWithValue("@cust_id",Cust_id );
            connection.SQLCMD.Parameters.AddWithValue("@Sub_Cust_ID", 0 );
            connection.SQLCMD.Parameters.AddWithValue("@loc_Inter_flag",local_inter_flag );
            connection.SQLCMD.Parameters.AddWithValue("@Param_Result", "");

            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Delivery_Tbl");
            obj.Close();
            connection.SQLCS.Close();


               if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    return;
                }

            connection.SQLCMD.Parameters.Clear();

            if (connection.SQLDS.Tables["Delivery_Tbl"].Rows.Count > 0)
            {
                if (connection.Lang_id == 1)
                {
                    Delivery_Add AddFrm = new Delivery_Add(city_con_online, city_con_online_id);
                    this.Visible = false;
                    AddFrm.ShowDialog(this);
                    this.Visible = true;
                }
                else
                {
                    Delivery_Add_eng AddFrm = new Delivery_Add_eng(city_con_online, city_con_online_id);
                    this.Visible = false;
                    AddFrm.ShowDialog(this);
                    this.Visible = true;
                }
            }
            else
            {
                string[] Str = { "Delivery_Tbl" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                MessageBox.Show("لا توجد حوالة تحقق الشروط" + (Char)Keys.Enter + " No Rem. For this Condition");
                return;
            }
        }


        private void Btn_Add_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Delivery_Rem_Search_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Str = { "Delivery_Tbl", "Get_info_Tbl", "Get_info_Tbl1", "Get_info_Tbl2", "Get_info_Tbl3", "Cust_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }

             
        }

        private void Delivery_Rem_Search_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

       
    }
}
