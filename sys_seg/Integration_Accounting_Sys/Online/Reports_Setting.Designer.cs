﻿namespace Integration_Accounting_Sys{
    partial class Reports_Setting
    {
        /// <summary>

        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Chk_S_Address = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Chk_R_Address = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Chk_S_Nat = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Chk_R_Nat = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Chk_S_Job = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Chk_R_Job = new System.Windows.Forms.CheckBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.Chk_T_city = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Chk_Commission = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Chk_Real_Paid_Name = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Chk_R_social = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Chk_S_social = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.AddBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.Chk_R_City = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Chk_S_City = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Chk_R_notes = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Chk_s_notes = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.chk_company_title = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(0, 40);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(119, 14);
            this.label9.TabIndex = 751;
            this.label9.Text = "أعدادات الطباعة.....";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(51, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 16);
            this.label12.TabIndex = 886;
            this.label12.Text = "عنـــوان المرســــــل";
            // 
            // Chk_S_Address
            // 
            this.Chk_S_Address.AutoSize = true;
            this.Chk_S_Address.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_S_Address.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_Address.Location = new System.Drawing.Point(34, 111);
            this.Chk_S_Address.Name = "Chk_S_Address";
            this.Chk_S_Address.Size = new System.Drawing.Size(15, 14);
            this.Chk_S_Address.TabIndex = 887;
            this.Chk_S_Address.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(51, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 888;
            this.label1.Text = "عنـــوان المستلـــــــم";
            // 
            // Chk_R_Address
            // 
            this.Chk_R_Address.AutoSize = true;
            this.Chk_R_Address.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_R_Address.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_Address.Location = new System.Drawing.Point(34, 133);
            this.Chk_R_Address.Name = "Chk_R_Address";
            this.Chk_R_Address.Size = new System.Drawing.Size(15, 14);
            this.Chk_R_Address.TabIndex = 889;
            this.Chk_R_Address.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(51, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 16);
            this.label2.TabIndex = 890;
            this.label2.Text = "جنسيـة المرســـــــل";
            // 
            // Chk_S_Nat
            // 
            this.Chk_S_Nat.AutoSize = true;
            this.Chk_S_Nat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_S_Nat.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_Nat.Location = new System.Drawing.Point(34, 155);
            this.Chk_S_Nat.Name = "Chk_S_Nat";
            this.Chk_S_Nat.Size = new System.Drawing.Size(15, 14);
            this.Chk_S_Nat.TabIndex = 891;
            this.Chk_S_Nat.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(51, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 16);
            this.label3.TabIndex = 892;
            this.label3.Text = "جنسيـة المستلــــــــم";
            // 
            // Chk_R_Nat
            // 
            this.Chk_R_Nat.AutoSize = true;
            this.Chk_R_Nat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_R_Nat.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_Nat.Location = new System.Drawing.Point(34, 176);
            this.Chk_R_Nat.Name = "Chk_R_Nat";
            this.Chk_R_Nat.Size = new System.Drawing.Size(15, 14);
            this.Chk_R_Nat.TabIndex = 893;
            this.Chk_R_Nat.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(277, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 16);
            this.label4.TabIndex = 894;
            this.label4.Text = "مهنـــــة المرســــــــــــل";
            // 
            // Chk_S_Job
            // 
            this.Chk_S_Job.AutoSize = true;
            this.Chk_S_Job.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_S_Job.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_Job.Location = new System.Drawing.Point(260, 66);
            this.Chk_S_Job.Name = "Chk_S_Job";
            this.Chk_S_Job.Size = new System.Drawing.Size(15, 14);
            this.Chk_S_Job.TabIndex = 895;
            this.Chk_S_Job.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(277, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 16);
            this.label5.TabIndex = 896;
            this.label5.Text = "مهنـــــة المستلـــــــــــــم";
            // 
            // Chk_R_Job
            // 
            this.Chk_R_Job.AutoSize = true;
            this.Chk_R_Job.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_R_Job.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_Job.Location = new System.Drawing.Point(260, 89);
            this.Chk_R_Job.Name = "Chk_R_Job";
            this.Chk_R_Job.Size = new System.Drawing.Size(15, 14);
            this.Chk_R_Job.TabIndex = 897;
            this.Chk_R_Job.UseVisualStyleBackColor = true;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(466, 284);
            this.shapeContainer1.TabIndex = 898;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 231;
            this.lineShape1.X2 = 231;
            this.lineShape1.Y1 = 49;
            this.lineShape1.Y2 = 240;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(123, 49);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(412, 1);
            this.flowLayoutPanel3.TabIndex = 899;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-353, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel4.TabIndex = 758;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(51, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 16);
            this.label6.TabIndex = 910;
            this.label6.Text = "بلد ومدينة الاستــلام";
            // 
            // Chk_T_city
            // 
            this.Chk_T_city.AutoSize = true;
            this.Chk_T_city.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_T_city.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_T_city.Location = new System.Drawing.Point(34, 197);
            this.Chk_T_city.Name = "Chk_T_city";
            this.Chk_T_city.Size = new System.Drawing.Size(15, 14);
            this.Chk_T_city.TabIndex = 911;
            this.Chk_T_city.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(277, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 16);
            this.label7.TabIndex = 908;
            this.label7.Text = "العمولــــــــــــــــــــة";
            // 
            // Chk_Commission
            // 
            this.Chk_Commission.AutoSize = true;
            this.Chk_Commission.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Commission.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Commission.Location = new System.Drawing.Point(260, 155);
            this.Chk_Commission.Name = "Chk_Commission";
            this.Chk_Commission.Size = new System.Drawing.Size(15, 14);
            this.Chk_Commission.TabIndex = 909;
            this.Chk_Commission.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(51, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 16);
            this.label8.TabIndex = 906;
            this.label8.Text = "الاسم كما في الهويـــــة";
            // 
            // Chk_Real_Paid_Name
            // 
            this.Chk_Real_Paid_Name.AutoSize = true;
            this.Chk_Real_Paid_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Real_Paid_Name.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Real_Paid_Name.Location = new System.Drawing.Point(34, 219);
            this.Chk_Real_Paid_Name.Name = "Chk_Real_Paid_Name";
            this.Chk_Real_Paid_Name.Size = new System.Drawing.Size(15, 14);
            this.Chk_Real_Paid_Name.TabIndex = 907;
            this.Chk_Real_Paid_Name.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(277, 132);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 16);
            this.label11.TabIndex = 902;
            this.label11.Text = "الرقم الوطني للمستلـــــم";
            // 
            // Chk_R_social
            // 
            this.Chk_R_social.AutoSize = true;
            this.Chk_R_social.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_R_social.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_social.Location = new System.Drawing.Point(260, 133);
            this.Chk_R_social.Name = "Chk_R_social";
            this.Chk_R_social.Size = new System.Drawing.Size(15, 14);
            this.Chk_R_social.TabIndex = 903;
            this.Chk_R_social.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(277, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(122, 16);
            this.label13.TabIndex = 900;
            this.label13.Text = "الرقم الوطني للمرســــــل";
            // 
            // Chk_S_social
            // 
            this.Chk_S_social.AutoSize = true;
            this.Chk_S_social.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_S_social.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_social.Location = new System.Drawing.Point(260, 111);
            this.Chk_S_social.Name = "Chk_S_social";
            this.Chk_S_social.Size = new System.Drawing.Size(15, 14);
            this.Chk_S_social.TabIndex = 901;
            this.Chk_S_social.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 240);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(472, 1);
            this.flowLayoutPanel1.TabIndex = 912;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-293, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(760, 1);
            this.flowLayoutPanel2.TabIndex = 758;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(79, 9);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(162, 23);
            this.TxtUser.TabIndex = 913;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(-1, 12);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(80, 16);
            this.label10.TabIndex = 914;
            this.label10.Text = "اسم المستخــدم:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(332, 9);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(126, 23);
            this.TxtIn_Rec_Date.TabIndex = 915;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(277, 12);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(54, 16);
            this.label14.TabIndex = 916;
            this.label14.Text = "التــاريـخ:";
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(146, 248);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 28);
            this.AddBtn.TabIndex = 917;
            this.AddBtn.Text = "حفظ";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(234, 248);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 28);
            this.button1.TabIndex = 918;
            this.button1.Text = "الغاء";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(51, 88);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 16);
            this.label15.TabIndex = 921;
            this.label15.Text = "مدينــة المستلـــــــــم";
            // 
            // Chk_R_City
            // 
            this.Chk_R_City.AutoSize = true;
            this.Chk_R_City.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_R_City.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_City.Location = new System.Drawing.Point(34, 89);
            this.Chk_R_City.Name = "Chk_R_City";
            this.Chk_R_City.Size = new System.Drawing.Size(15, 14);
            this.Chk_R_City.TabIndex = 922;
            this.Chk_R_City.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(51, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 16);
            this.label16.TabIndex = 919;
            this.label16.Text = "مدينــة المرســـــــــل";
            // 
            // Chk_S_City
            // 
            this.Chk_S_City.AutoSize = true;
            this.Chk_S_City.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_S_City.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_S_City.Location = new System.Drawing.Point(34, 66);
            this.Chk_S_City.Name = "Chk_S_City";
            this.Chk_S_City.Size = new System.Drawing.Size(15, 14);
            this.Chk_S_City.TabIndex = 920;
            this.Chk_S_City.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(277, 196);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 16);
            this.label17.TabIndex = 925;
            this.label17.Text = "ملاحظــات المستلــــم";
            // 
            // Chk_R_notes
            // 
            this.Chk_R_notes.AutoSize = true;
            this.Chk_R_notes.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_R_notes.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_R_notes.Location = new System.Drawing.Point(260, 198);
            this.Chk_R_notes.Name = "Chk_R_notes";
            this.Chk_R_notes.Size = new System.Drawing.Size(15, 14);
            this.Chk_R_notes.TabIndex = 926;
            this.Chk_R_notes.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(277, 175);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 16);
            this.label18.TabIndex = 923;
            this.label18.Text = "ملاحظــات المرســـل";
            // 
            // Chk_s_notes
            // 
            this.Chk_s_notes.AutoSize = true;
            this.Chk_s_notes.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_s_notes.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_s_notes.Location = new System.Drawing.Point(260, 176);
            this.Chk_s_notes.Name = "Chk_s_notes";
            this.Chk_s_notes.Size = new System.Drawing.Size(15, 14);
            this.Chk_s_notes.TabIndex = 924;
            this.Chk_s_notes.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(277, 217);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 16);
            this.label19.TabIndex = 927;
            this.label19.Text = "أسم الشركة في الوكالات";
            // 
            // chk_company_title
            // 
            this.chk_company_title.AutoSize = true;
            this.chk_company_title.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk_company_title.ForeColor = System.Drawing.Color.Maroon;
            this.chk_company_title.Location = new System.Drawing.Point(260, 219);
            this.chk_company_title.Name = "chk_company_title";
            this.chk_company_title.Size = new System.Drawing.Size(15, 14);
            this.chk_company_title.TabIndex = 928;
            this.chk_company_title.UseVisualStyleBackColor = true;
            // 
            // Reports_Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 284);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.chk_company_title);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Chk_R_notes);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Chk_s_notes);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Chk_R_City);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Chk_S_City);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Chk_T_city);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Chk_Commission);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Chk_Real_Paid_Name);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Chk_R_social);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Chk_S_social);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Chk_R_Job);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Chk_S_Job);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Chk_R_Nat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Chk_S_Nat);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Chk_R_Address);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Chk_S_Address);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.shapeContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reports_Setting";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "565";
            this.Text = "Reports_Setting";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Reports_Setting_FormClosed);
            this.Load += new System.EventHandler(this.Reports_Setting_Load);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox Chk_S_Address;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox Chk_R_Address;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox Chk_S_Nat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox Chk_R_Nat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox Chk_S_Job;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox Chk_R_Job;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox Chk_T_city;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox Chk_Commission;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox Chk_Real_Paid_Name;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox Chk_R_social;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox Chk_S_social;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox Chk_R_City;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox Chk_S_City;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox Chk_R_notes;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox Chk_s_notes;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox chk_company_title;
    }
}