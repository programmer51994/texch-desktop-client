﻿namespace Integration_Accounting_Sys
{
    partial class Update_Incoming_Rem 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Grdrec_rem = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_Reciever = new System.Windows.Forms.TextBox();
            this.Txt_Sender = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.Txt_mail = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.Txtr_State = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.Txtr_Post_Code = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Txtr_Street = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.Txtr_Suburb = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Cbo_city = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmb_job_receiver = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_rbirth_place = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.Txt_Doc_r_Date = new System.Windows.Forms.DateTimePicker();
            this.Txt_Doc_r_Exp = new System.Windows.Forms.DateTimePicker();
            this.Txt_rbirth_Date = new System.Windows.Forms.DateTimePicker();
            this.label47 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_Mother_name = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmb_Gender_id = new System.Windows.Forms.ComboBox();
            this.Cmb_phone_Code_R = new System.Windows.Forms.ComboBox();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Cmb_R_Nat = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Cmb_R_City = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Cmb_r_Doc_Type = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_Doc_r_Issue = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_r_Doc_No = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_S_D_Ver_Flag = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_R_T_Purpose = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Txt_notes = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.Txt_Real_Paid_Name = new System.Windows.Forms.TextBox();
            this.Txt_R_details_job = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).BeginInit();
            this.SuspendLayout();
            // 
            // Grdrec_rem
            // 
            this.Grdrec_rem.AllowUserToAddRows = false;
            this.Grdrec_rem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grdrec_rem.ColumnHeadersHeight = 24;
            this.Grdrec_rem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grdrec_rem.DefaultCellStyle = dataGridViewCellStyle4;
            this.Grdrec_rem.Location = new System.Drawing.Point(6, 34);
            this.Grdrec_rem.Name = "Grdrec_rem";
            this.Grdrec_rem.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdrec_rem.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdrec_rem.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grdrec_rem.Size = new System.Drawing.Size(874, 103);
            this.Grdrec_rem.TabIndex = 711;
            this.Grdrec_rem.SelectionChanged += new System.EventHandler(this.Grdrec_rem_SelectionChanged);
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "rem_no";
            this.Column3.HeaderText = "رقم الحولة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "R_amount";
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "مبلغ الحوالة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "R_ACUR_NAME";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "PR_ACUR_NAME";
            this.Column4.HeaderText = "عملة التسليم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 130;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ACASE_NA";
            this.Column5.HeaderText = "الحالة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 130;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Case_Date";
            this.Column6.HeaderText = "تاريخ الحالة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 110;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "S_name";
            this.Column7.HeaderText = "اسم المرسل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 87;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "S_phone";
            this.Column8.HeaderText = "هاتف المرسل";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 97;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "r_name";
            this.Column9.HeaderText = "اسم المستلم";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 86;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "R_phone";
            this.Column10.HeaderText = "هاتف المستلم";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 96;
            // 
            // Txt_Reciever
            // 
            this.Txt_Reciever.BackColor = System.Drawing.Color.White;
            this.Txt_Reciever.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Reciever.Location = new System.Drawing.Point(124, 145);
            this.Txt_Reciever.MaxLength = 49;
            this.Txt_Reciever.Name = "Txt_Reciever";
            this.Txt_Reciever.ReadOnly = true;
            this.Txt_Reciever.Size = new System.Drawing.Size(273, 21);
            this.Txt_Reciever.TabIndex = 0;
            // 
            // Txt_Sender
            // 
            this.Txt_Sender.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Txt_Sender.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sender.Location = new System.Drawing.Point(586, 145);
            this.Txt_Sender.MaxLength = 49;
            this.Txt_Sender.Name = "Txt_Sender";
            this.Txt_Sender.ReadOnly = true;
            this.Txt_Sender.Size = new System.Drawing.Size(273, 21);
            this.Txt_Sender.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(468, 148);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 14);
            this.label10.TabIndex = 861;
            this.label10.Text = "اســـــــــم المرسل:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(1, 372);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(117, 14);
            this.label34.TabIndex = 1143;
            this.label34.Text = "عنوان المستلم .....";
            // 
            // Txt_mail
            // 
            this.Txt_mail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_mail.Location = new System.Drawing.Point(122, 466);
            this.Txt_mail.MaxLength = 99;
            this.Txt_mail.Name = "Txt_mail";
            this.Txt_mail.Size = new System.Drawing.Size(285, 21);
            this.Txt_mail.TabIndex = 1138;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(12, 468);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(90, 16);
            this.label41.TabIndex = 1142;
            this.label41.Text = "البريد الالكتروني:";
            // 
            // Txtr_State
            // 
            this.Txtr_State.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_State.Location = new System.Drawing.Point(576, 418);
            this.Txtr_State.MaxLength = 99;
            this.Txtr_State.Name = "Txtr_State";
            this.Txtr_State.Size = new System.Drawing.Size(285, 21);
            this.Txtr_State.TabIndex = 1135;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(471, 420);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(72, 16);
            this.label45.TabIndex = 1141;
            this.label45.Text = "المحافـظـــــة:";
            // 
            // Txtr_Post_Code
            // 
            this.Txtr_Post_Code.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_Post_Code.Location = new System.Drawing.Point(122, 418);
            this.Txtr_Post_Code.MaxLength = 99;
            this.Txtr_Post_Code.Name = "Txtr_Post_Code";
            this.Txtr_Post_Code.Size = new System.Drawing.Size(285, 21);
            this.Txtr_Post_Code.TabIndex = 1136;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(12, 420);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 16);
            this.label44.TabIndex = 1140;
            this.label44.Text = "الرمز البريــدي:";
            // 
            // Txtr_Street
            // 
            this.Txtr_Street.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_Street.Location = new System.Drawing.Point(122, 394);
            this.Txtr_Street.MaxLength = 99;
            this.Txtr_Street.Name = "Txtr_Street";
            this.Txtr_Street.Size = new System.Drawing.Size(285, 21);
            this.Txtr_Street.TabIndex = 1133;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(466, 396);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(77, 16);
            this.label43.TabIndex = 1139;
            this.label43.Text = "الحــــــــــــــي:";
            // 
            // Txtr_Suburb
            // 
            this.Txtr_Suburb.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_Suburb.Location = new System.Drawing.Point(576, 394);
            this.Txtr_Suburb.MaxLength = 99;
            this.Txtr_Suburb.Name = "Txtr_Suburb";
            this.Txtr_Suburb.Size = new System.Drawing.Size(285, 21);
            this.Txtr_Suburb.TabIndex = 1132;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(12, 396);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(78, 16);
            this.label42.TabIndex = 1134;
            this.label42.Text = "الزقــــــــــــاق:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(3, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 14);
            this.label5.TabIndex = 1144;
            this.label5.Text = "اســـــــــم المستلم:";
            // 
            // Cbo_city
            // 
            this.Cbo_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_city.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_city.FormattingEnabled = true;
            this.Cbo_city.Location = new System.Drawing.Point(156, 529);
            this.Cbo_city.Name = "Cbo_city";
            this.Cbo_city.Size = new System.Drawing.Size(253, 24);
            this.Cbo_city.TabIndex = 1145;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(3, 533);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 16);
            this.label2.TabIndex = 1146;
            this.label2.Text = "مدينــــــة التسليـــــم الماليــــــة:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(3, 489);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(215, 14);
            this.label6.TabIndex = 1147;
            this.label6.Text = "المعلومات المالية لمدينة التسليم ....\r\n";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(443, 562);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 26);
            this.button2.TabIndex = 1149;
            this.button2.Text = "انهـــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button3.ForeColor = System.Drawing.Color.Navy;
            this.button3.Location = new System.Drawing.Point(349, 562);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(95, 26);
            this.button3.TabIndex = 1148;
            this.button3.Text = "موافـــق";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(209, 499);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(686, 1);
            this.flowLayoutPanel15.TabIndex = 1153;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(356, 2);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(245, 23);
            this.TxtUser.TabIndex = 1158;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(279, 6);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 1157;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 1);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(272, 25);
            this.TxtTerm_Name.TabIndex = 1156;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(671, 6);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 1154;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(725, 2);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(155, 23);
            this.TxtIn_Rec_Date.TabIndex = 1155;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-4, 557);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(900, 1);
            this.flowLayoutPanel2.TabIndex = 1150;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-4, 30);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(925, 1);
            this.flowLayoutPanel9.TabIndex = 629;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(110, 383);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(783, 1);
            this.flowLayoutPanel1.TabIndex = 1159;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-4, 142);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(901, 1);
            this.flowLayoutPanel3.TabIndex = 1160;
            // 
            // cmb_job_receiver
            // 
            this.cmb_job_receiver.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_job_receiver.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_job_receiver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_job_receiver.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_job_receiver.FormattingEnabled = true;
            this.cmb_job_receiver.Location = new System.Drawing.Point(123, 440);
            this.cmb_job_receiver.Name = "cmb_job_receiver";
            this.cmb_job_receiver.Size = new System.Drawing.Size(285, 24);
            this.cmb_job_receiver.TabIndex = 1161;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(13, 443);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 16);
            this.label22.TabIndex = 1162;
            this.label22.Text = "المهنـــــــــــــة:";
            // 
            // txt_rbirth_place
            // 
            this.txt_rbirth_place.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_rbirth_place.Location = new System.Drawing.Point(123, 259);
            this.txt_rbirth_place.MaxLength = 49;
            this.txt_rbirth_place.Name = "txt_rbirth_place";
            this.txt_rbirth_place.Size = new System.Drawing.Size(285, 21);
            this.txt_rbirth_place.TabIndex = 1170;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(12, 261);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(79, 16);
            this.label48.TabIndex = 1191;
            this.label48.Text = "مكــان الولادة :";
            // 
            // Txt_Doc_r_Date
            // 
            this.Txt_Doc_r_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_r_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_r_Date.Location = new System.Drawing.Point(123, 326);
            this.Txt_Doc_r_Date.Name = "Txt_Doc_r_Date";
            this.Txt_Doc_r_Date.Size = new System.Drawing.Size(173, 20);
            this.Txt_Doc_r_Date.TabIndex = 1174;
            // 
            // Txt_Doc_r_Exp
            // 
            this.Txt_Doc_r_Exp.CustomFormat = "dd/mm/yyyy";
            this.Txt_Doc_r_Exp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Doc_r_Exp.Location = new System.Drawing.Point(124, 349);
            this.Txt_Doc_r_Exp.Name = "Txt_Doc_r_Exp";
            this.Txt_Doc_r_Exp.Size = new System.Drawing.Size(173, 20);
            this.Txt_Doc_r_Exp.TabIndex = 1175;
            // 
            // Txt_rbirth_Date
            // 
            this.Txt_rbirth_Date.CustomFormat = "dd/mm/yyyy";
            this.Txt_rbirth_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_rbirth_Date.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Txt_rbirth_Date.Location = new System.Drawing.Point(579, 211);
            this.Txt_rbirth_Date.Name = "Txt_rbirth_Date";
            this.Txt_rbirth_Date.Size = new System.Drawing.Size(187, 20);
            this.Txt_rbirth_Date.TabIndex = 1166;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Maroon;
            this.label47.Location = new System.Drawing.Point(0, 281);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(113, 14);
            this.label47.TabIndex = 1189;
            this.label47.Text = "الــوثــــــــيـقــة.......";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(21, 291);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(876, 1);
            this.flowLayoutPanel8.TabIndex = 1190;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Maroon;
            this.label37.Location = new System.Drawing.Point(308, 329);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 14);
            this.label37.TabIndex = 1188;
            this.label37.Text = "dd/mm/yyyy";
            // 
            // txt_Mother_name
            // 
            this.txt_Mother_name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Mother_name.Location = new System.Drawing.Point(123, 236);
            this.txt_Mother_name.MaxLength = 49;
            this.txt_Mother_name.Name = "txt_Mother_name";
            this.txt_Mother_name.Size = new System.Drawing.Size(285, 21);
            this.txt_Mother_name.TabIndex = 1169;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(12, 238);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(76, 16);
            this.label35.TabIndex = 1187;
            this.label35.Text = "اســــــــــم الام:";
            // 
            // cmb_Gender_id
            // 
            this.cmb_Gender_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Gender_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Gender_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Gender_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Gender_id.FormattingEnabled = true;
            this.cmb_Gender_id.Location = new System.Drawing.Point(124, 183);
            this.cmb_Gender_id.Name = "cmb_Gender_id";
            this.cmb_Gender_id.Size = new System.Drawing.Size(285, 24);
            this.cmb_Gender_id.TabIndex = 1165;
            // 
            // Cmb_phone_Code_R
            // 
            this.Cmb_phone_Code_R.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_phone_Code_R.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_phone_Code_R.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_phone_Code_R.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_phone_Code_R.FormattingEnabled = true;
            this.Cmb_phone_Code_R.Location = new System.Drawing.Point(775, 183);
            this.Cmb_phone_Code_R.Name = "Cmb_phone_Code_R";
            this.Cmb_phone_Code_R.Size = new System.Drawing.Size(85, 24);
            this.Cmb_phone_Code_R.TabIndex = 1164;
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_Phone.Location = new System.Drawing.Point(580, 185);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.Size = new System.Drawing.Size(194, 21);
            this.Txt_R_Phone.TabIndex = 1163;
            // 
            // Cmb_R_Nat
            // 
            this.Cmb_R_Nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_Nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_Nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_Nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_Nat.FormattingEnabled = true;
            this.Cmb_R_Nat.Location = new System.Drawing.Point(124, 209);
            this.Cmb_R_Nat.Name = "Cmb_R_Nat";
            this.Cmb_R_Nat.Size = new System.Drawing.Size(285, 24);
            this.Cmb_R_Nat.TabIndex = 1167;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(12, 213);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(80, 16);
            this.label40.TabIndex = 1186;
            this.label40.Text = "الجنسيـــــــــــة:";
            // 
            // Cmb_R_City
            // 
            this.Cmb_R_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_City.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_City.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_City.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_City.FormattingEnabled = true;
            this.Cmb_R_City.Location = new System.Drawing.Point(576, 234);
            this.Cmb_R_City.Name = "Cmb_R_City";
            this.Cmb_R_City.Size = new System.Drawing.Size(285, 24);
            this.Cmb_R_City.TabIndex = 1168;
            this.Cmb_R_City.SelectedIndexChanged += new System.EventHandler(this.Cmb_R_City_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(466, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 1185;
            this.label3.Text = "المدينــــــــــــة:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(12, 328);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 16);
            this.label18.TabIndex = 1184;
            this.label18.Text = "تاريخهـــــــــا:";
            // 
            // Cmb_r_Doc_Type
            // 
            this.Cmb_r_Doc_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_r_Doc_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_r_Doc_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_r_Doc_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_r_Doc_Type.FormattingEnabled = true;
            this.Cmb_r_Doc_Type.Location = new System.Drawing.Point(576, 299);
            this.Cmb_r_Doc_Type.Name = "Cmb_r_Doc_Type";
            this.Cmb_r_Doc_Type.Size = new System.Drawing.Size(285, 24);
            this.Cmb_r_Doc_Type.TabIndex = 1171;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(460, 303);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 16);
            this.label7.TabIndex = 1183;
            this.label7.Text = "نوع الوثيقـــــــة:";
            // 
            // Txt_Doc_r_Issue
            // 
            this.Txt_Doc_r_Issue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doc_r_Issue.Location = new System.Drawing.Point(576, 326);
            this.Txt_Doc_r_Issue.MaxLength = 49;
            this.Txt_Doc_r_Issue.Name = "Txt_Doc_r_Issue";
            this.Txt_Doc_r_Issue.Size = new System.Drawing.Size(285, 21);
            this.Txt_Doc_r_Issue.TabIndex = 1173;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(463, 328);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 16);
            this.label20.TabIndex = 1182;
            this.label20.Text = "م.اصدارهــــــا :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(11, 351);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 16);
            this.label25.TabIndex = 1181;
            this.label25.Text = "إنتهائهــــــــا :";
            // 
            // Txt_r_Doc_No
            // 
            this.Txt_r_Doc_No.BackColor = System.Drawing.Color.White;
            this.Txt_r_Doc_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_Doc_No.Location = new System.Drawing.Point(121, 300);
            this.Txt_r_Doc_No.MaxLength = 49;
            this.Txt_r_Doc_No.Name = "Txt_r_Doc_No";
            this.Txt_r_Doc_No.Size = new System.Drawing.Size(285, 21);
            this.Txt_r_Doc_No.TabIndex = 1172;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(12, 303);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 16);
            this.label26.TabIndex = 1180;
            this.label26.Text = "رقم الوثيقــــة:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(472, 213);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 16);
            this.label24.TabIndex = 1179;
            this.label24.Text = "التولــــــــــــد:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Maroon;
            this.label32.Location = new System.Drawing.Point(309, 352);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(87, 14);
            this.label32.TabIndex = 1178;
            this.label32.Text = "dd/mm/yyyy";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Maroon;
            this.label33.Location = new System.Drawing.Point(770, 214);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(87, 14);
            this.label33.TabIndex = 1177;
            this.label33.Text = "dd/mm/yyyy";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(12, 187);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 16);
            this.label28.TabIndex = 1193;
            this.label28.Text = "الجنـــــــــــس:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(471, 187);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 16);
            this.label9.TabIndex = 1194;
            this.label9.Text = "الهاتــــــــــف:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-7, 175);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(904, 1);
            this.flowLayoutPanel4.TabIndex = 1195;
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Relionship.Location = new System.Drawing.Point(576, 259);
            this.Txt_Relionship.MaxLength = 99;
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.Size = new System.Drawing.Size(285, 21);
            this.Txt_Relionship.TabIndex = 1196;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(463, 261);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 1197;
            this.label8.Text = "علاقـــة م . س :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(439, 533);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 16);
            this.label11.TabIndex = 1199;
            this.label11.Text = "نوع تأكيد الحـــوالة:";
            // 
            // Txt_S_D_Ver_Flag
            // 
            this.Txt_S_D_Ver_Flag.BackColor = System.Drawing.Color.White;
            this.Txt_S_D_Ver_Flag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_D_Ver_Flag.Location = new System.Drawing.Point(574, 527);
            this.Txt_S_D_Ver_Flag.MaxLength = 49;
            this.Txt_S_D_Ver_Flag.Name = "Txt_S_D_Ver_Flag";
            this.Txt_S_D_Ver_Flag.ReadOnly = true;
            this.Txt_S_D_Ver_Flag.Size = new System.Drawing.Size(285, 21);
            this.Txt_S_D_Ver_Flag.TabIndex = 1198;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(0, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(136, 14);
            this.label12.TabIndex = 1200;
            this.label12.Text = "معلومات المستلم.......";
            // 
            // Txt_R_T_Purpose
            // 
            this.Txt_R_T_Purpose.BackColor = System.Drawing.Color.White;
            this.Txt_R_T_Purpose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_T_Purpose.Location = new System.Drawing.Point(156, 505);
            this.Txt_R_T_Purpose.MaxLength = 200;
            this.Txt_R_T_Purpose.Name = "Txt_R_T_Purpose";
            this.Txt_R_T_Purpose.Size = new System.Drawing.Size(253, 21);
            this.Txt_R_T_Purpose.TabIndex = 1203;
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Navy;
            this.label55.Location = new System.Drawing.Point(5, 507);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(150, 16);
            this.label55.TabIndex = 1204;
            this.label55.Text = "غرض التحويـــــل للمستلــم:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(452, 508);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(78, 16);
            this.label31.TabIndex = 1202;
            this.label31.Text = "الملاحظــــــات:";
            // 
            // Txt_notes
            // 
            this.Txt_notes.BackColor = System.Drawing.Color.White;
            this.Txt_notes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_notes.Location = new System.Drawing.Point(574, 500);
            this.Txt_notes.MaxLength = 199;
            this.Txt_notes.Name = "Txt_notes";
            this.Txt_notes.Size = new System.Drawing.Size(285, 21);
            this.Txt_notes.TabIndex = 1201;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Navy;
            this.label56.Location = new System.Drawing.Point(437, 352);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(106, 16);
            this.label56.TabIndex = 1206;
            this.label56.Text = "الاسم كما في الهوية:";
            // 
            // Txt_Real_Paid_Name
            // 
            this.Txt_Real_Paid_Name.BackColor = System.Drawing.Color.White;
            this.Txt_Real_Paid_Name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Real_Paid_Name.Location = new System.Drawing.Point(576, 348);
            this.Txt_Real_Paid_Name.Name = "Txt_Real_Paid_Name";
            this.Txt_Real_Paid_Name.Size = new System.Drawing.Size(285, 21);
            this.Txt_Real_Paid_Name.TabIndex = 1205;
            // 
            // Txt_R_details_job
            // 
            this.Txt_R_details_job.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_details_job.Location = new System.Drawing.Point(576, 442);
            this.Txt_R_details_job.MaxLength = 99;
            this.Txt_R_details_job.Name = "Txt_R_details_job";
            this.Txt_R_details_job.Size = new System.Drawing.Size(285, 21);
            this.Txt_R_details_job.TabIndex = 1207;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(466, 444);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(77, 16);
            this.label57.TabIndex = 1208;
            this.label57.Text = "تفاصيل العمل :";
            // 
            // Update_Incoming_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 589);
            this.Controls.Add(this.Txt_R_details_job);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.Txt_Real_Paid_Name);
            this.Controls.Add(this.Txt_R_T_Purpose);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.Txt_notes);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_S_D_Ver_Flag);
            this.Controls.Add(this.Txt_Relionship);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txt_rbirth_place);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.Txt_Doc_r_Date);
            this.Controls.Add(this.Txt_Doc_r_Exp);
            this.Controls.Add(this.Txt_rbirth_Date);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.txt_Mother_name);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.cmb_Gender_id);
            this.Controls.Add(this.Cmb_phone_Code_R);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Cmb_R_Nat);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.Cmb_R_City);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Cmb_r_Doc_Type);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_Doc_r_Issue);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.Txt_r_Doc_No);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.cmb_job_receiver);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.flowLayoutPanel15);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Cbo_city);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.Txt_mail);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.Txtr_State);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.Txtr_Post_Code);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.Txtr_Street);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.Txtr_Suburb);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.Txt_Sender);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_Reciever);
            this.Controls.Add(this.Grdrec_rem);
            this.Controls.Add(this.flowLayoutPanel9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Update_Incoming_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "541";
            this.Text = "Update_Incoming_Rem";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Update_Incoming_Rem_FormClosed);
            this.Load += new System.EventHandler(this.Update_IN_Rem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grdrec_rem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Grdrec_rem;
        private System.Windows.Forms.TextBox Txt_Reciever;
        private System.Windows.Forms.TextBox Txt_Sender;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox Txt_mail;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox Txtr_State;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox Txtr_Post_Code;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox Txtr_Street;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox Txtr_Suburb;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox Cbo_city;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.ComboBox cmb_job_receiver;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txt_rbirth_place;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.DateTimePicker Txt_Doc_r_Date;
        private System.Windows.Forms.DateTimePicker Txt_Doc_r_Exp;
        private System.Windows.Forms.DateTimePicker Txt_rbirth_Date;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txt_Mother_name;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmb_Gender_id;
        private System.Windows.Forms.ComboBox Cmb_phone_Code_R;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.ComboBox Cmb_R_Nat;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox Cmb_R_City;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Cmb_r_Doc_Type;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_Doc_r_Issue;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_r_Doc_No;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_S_D_Ver_Flag;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_R_T_Purpose;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txt_notes;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox Txt_Real_Paid_Name;
        private System.Windows.Forms.TextBox Txt_R_details_job;
        private System.Windows.Forms.Label label57;
    }
}