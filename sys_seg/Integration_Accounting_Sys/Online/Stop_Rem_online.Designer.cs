﻿namespace Integration_Accounting_Sys
{
    partial class Stop_Rem_online
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtS_address = new System.Windows.Forms.TextBox();
            this.Txt_s_job = new System.Windows.Forms.TextBox();
            this.Txt_Soruce_money = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtS_phone = new System.Windows.Forms.TextBox();
            this.TxtS_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_S_doc_type = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txts_nat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtScity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_S_doc_issue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_S_Birth_Place = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_Relionship = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Txt_R_Birth_Place = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Txt_r_doc_issue = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_R_doc_no = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.Txt_R_City = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Txt_R_Nat = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Txt_R_doc_type = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.Txt_R_address = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.Txt_r_job = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Stop_rem = new System.Windows.Forms.Button();
            this.Txt_Discreption = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txt_resd_sen = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_resd_rec = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txt_social_sen = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txt_social_rec = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txt_purpose_sen = new System.Windows.Forms.TextBox();
            this.txt_purpose_rec = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.Txt_S_details_job = new System.Windows.Forms.TextBox();
            this.Txt_R_Relation = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.Txt_R_details_job = new System.Windows.Forms.TextBox();
            this.Txt_R_notes = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.Txt_PRCur = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.Txt_Rem_no = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.Txt_Scity = new System.Windows.Forms.TextBox();
            this.Txt_Tcity = new System.Windows.Forms.TextBox();
            this.Txt_Rcur = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.cmb_Flag_stop = new System.Windows.Forms.ComboBox();
            this.Txt_Note_Stop = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.Tot_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_r_birthdate = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_R_doc_date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_Birth = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txt_S_doc_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.lal_stop = new System.Windows.Forms.Label();
            this.cbo_order_type = new System.Windows.Forms.ComboBox();
            this.txt_srch = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bon_end = new System.Windows.Forms.Button();
            this.txt_Pres = new System.Windows.Forms.TextBox();
            this.lab_old_reson = new System.Windows.Forms.Label();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(307, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 906;
            this.label1.Text = "المستخـــــدم:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(682, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(71, 14);
            this.label4.TabIndex = 904;
            this.label4.Text = "التاريــــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(761, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(184, 23);
            this.TxtIn_Rec_Date.TabIndex = 905;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(394, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(250, 23);
            this.TxtUser.TabIndex = 903;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(15, 2);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(265, 23);
            this.TxtTerm_Name.TabIndex = 902;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1, 29);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(969, 1);
            this.flowLayoutPanel9.TabIndex = 907;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-55, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(13, 131);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(179, 14);
            this.label2.TabIndex = 909;
            this.label2.Text = "معلومــــات الحوالـــة الانـــي....";
            // 
            // TxtS_address
            // 
            this.TxtS_address.BackColor = System.Drawing.SystemColors.Control;
            this.TxtS_address.Enabled = false;
            this.TxtS_address.Location = new System.Drawing.Point(75, 263);
            this.TxtS_address.Name = "TxtS_address";
            this.TxtS_address.ReadOnly = true;
            this.TxtS_address.Size = new System.Drawing.Size(560, 20);
            this.TxtS_address.TabIndex = 917;
            // 
            // Txt_s_job
            // 
            this.Txt_s_job.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_s_job.Enabled = false;
            this.Txt_s_job.Location = new System.Drawing.Point(455, 315);
            this.Txt_s_job.Name = "Txt_s_job";
            this.Txt_s_job.ReadOnly = true;
            this.Txt_s_job.Size = new System.Drawing.Size(190, 20);
            this.Txt_s_job.TabIndex = 916;
            // 
            // Txt_Soruce_money
            // 
            this.Txt_Soruce_money.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_Soruce_money.Enabled = false;
            this.Txt_Soruce_money.Location = new System.Drawing.Point(75, 315);
            this.Txt_Soruce_money.Name = "Txt_Soruce_money";
            this.Txt_Soruce_money.ReadOnly = true;
            this.Txt_Soruce_money.Size = new System.Drawing.Size(301, 20);
            this.Txt_Soruce_money.TabIndex = 915;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(2, 265);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(72, 15);
            this.label23.TabIndex = 924;
            this.label23.Text = "عنوان المرسل:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(379, 317);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(68, 15);
            this.label22.TabIndex = 923;
            this.label22.Text = "مهنة المرسـل:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(-3, 317);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(77, 15);
            this.label21.TabIndex = 922;
            this.label21.Text = "مصــدر المــــال:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(491, 212);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(73, 15);
            this.label20.TabIndex = 921;
            this.label20.Text = "التولـــــــــــــد:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(445, 291);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(44, 15);
            this.label18.TabIndex = 919;
            this.label18.Text = "تاريخها:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(714, 212);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 15);
            this.label19.TabIndex = 918;
            this.label19.Text = "هاتف المرسل:";
            // 
            // TxtS_phone
            // 
            this.TxtS_phone.BackColor = System.Drawing.SystemColors.Control;
            this.TxtS_phone.Enabled = false;
            this.TxtS_phone.Location = new System.Drawing.Point(795, 210);
            this.TxtS_phone.Name = "TxtS_phone";
            this.TxtS_phone.ReadOnly = true;
            this.TxtS_phone.Size = new System.Drawing.Size(168, 20);
            this.TxtS_phone.TabIndex = 925;
            // 
            // TxtS_name
            // 
            this.TxtS_name.BackColor = System.Drawing.SystemColors.Control;
            this.TxtS_name.Enabled = false;
            this.TxtS_name.Location = new System.Drawing.Point(75, 210);
            this.TxtS_name.Name = "TxtS_name";
            this.TxtS_name.ReadOnly = true;
            this.TxtS_name.Size = new System.Drawing.Size(407, 20);
            this.TxtS_name.TabIndex = 926;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(2, 212);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(68, 15);
            this.label5.TabIndex = 927;
            this.label5.Text = "اسم المرســل:";
            // 
            // Txt_S_doc_type
            // 
            this.Txt_S_doc_type.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_doc_type.Enabled = false;
            this.Txt_S_doc_type.Location = new System.Drawing.Point(75, 289);
            this.Txt_S_doc_type.Name = "Txt_S_doc_type";
            this.Txt_S_doc_type.ReadOnly = true;
            this.Txt_S_doc_type.Size = new System.Drawing.Size(208, 20);
            this.Txt_S_doc_type.TabIndex = 929;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(2, 291);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 15);
            this.label6.TabIndex = 928;
            this.label6.Text = "نوع الوثيقـــة:";
            // 
            // Txts_nat
            // 
            this.Txts_nat.BackColor = System.Drawing.SystemColors.Control;
            this.Txts_nat.Enabled = false;
            this.Txts_nat.Location = new System.Drawing.Point(795, 237);
            this.Txts_nat.Name = "Txts_nat";
            this.Txts_nat.ReadOnly = true;
            this.Txts_nat.Size = new System.Drawing.Size(168, 20);
            this.Txts_nat.TabIndex = 930;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(712, 239);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(74, 15);
            this.label7.TabIndex = 931;
            this.label7.Text = "جنسية المرسل:";
            // 
            // TxtScity
            // 
            this.TxtScity.BackColor = System.Drawing.SystemColors.Control;
            this.TxtScity.Enabled = false;
            this.TxtScity.Location = new System.Drawing.Point(75, 237);
            this.TxtScity.Name = "TxtScity";
            this.TxtScity.ReadOnly = true;
            this.TxtScity.Size = new System.Drawing.Size(204, 20);
            this.TxtScity.TabIndex = 932;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(2, 239);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 933;
            this.label8.Text = "مدينة المرسـل:";
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_doc_no.Enabled = false;
            this.Txt_S_doc_no.Location = new System.Drawing.Point(328, 289);
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.ReadOnly = true;
            this.Txt_S_doc_no.Size = new System.Drawing.Size(117, 20);
            this.Txt_S_doc_no.TabIndex = 934;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(283, 291);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(36, 15);
            this.label9.TabIndex = 935;
            this.label9.Text = "رقمها:";
            // 
            // Txt_S_doc_issue
            // 
            this.Txt_S_doc_issue.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_doc_issue.Enabled = false;
            this.Txt_S_doc_issue.Location = new System.Drawing.Point(633, 289);
            this.Txt_S_doc_issue.Name = "Txt_S_doc_issue";
            this.Txt_S_doc_issue.ReadOnly = true;
            this.Txt_S_doc_issue.Size = new System.Drawing.Size(168, 20);
            this.Txt_S_doc_issue.TabIndex = 936;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(583, 291);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(46, 15);
            this.label10.TabIndex = 937;
            this.label10.Text = "اصدارها:";
            // 
            // Txt_S_Birth_Place
            // 
            this.Txt_S_Birth_Place.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_Birth_Place.Enabled = false;
            this.Txt_S_Birth_Place.Location = new System.Drawing.Point(569, 237);
            this.Txt_S_Birth_Place.Name = "Txt_S_Birth_Place";
            this.Txt_S_Birth_Place.ReadOnly = true;
            this.Txt_S_Birth_Place.Size = new System.Drawing.Size(143, 20);
            this.Txt_S_Birth_Place.TabIndex = 938;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(483, 239);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 939;
            this.label11.Text = "بلد ومحل الولادة:";
            // 
            // Txt_Relionship
            // 
            this.Txt_Relionship.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_Relionship.Enabled = false;
            this.Txt_Relionship.Location = new System.Drawing.Point(768, 341);
            this.Txt_Relionship.Name = "Txt_Relionship";
            this.Txt_Relionship.ReadOnly = true;
            this.Txt_Relionship.Size = new System.Drawing.Size(197, 20);
            this.Txt_Relionship.TabIndex = 940;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(646, 343);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(110, 15);
            this.label12.TabIndex = 941;
            this.label12.Text = "علاقة المرسل بالمستلم:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(635, 231);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(0, 14);
            this.label13.TabIndex = 943;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(119, 200);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(852, 1);
            this.flowLayoutPanel17.TabIndex = 945;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(6, 189);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(116, 14);
            this.label14.TabIndex = 944;
            this.label14.Text = "معلومات المرسل...";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(4, 362);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(120, 14);
            this.label15.TabIndex = 976;
            this.label15.Text = "معلومات المستلم...";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_Purpose.Enabled = false;
            this.Txt_Purpose.Location = new System.Drawing.Point(75, 341);
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.ReadOnly = true;
            this.Txt_Purpose.Size = new System.Drawing.Size(297, 20);
            this.Txt_Purpose.TabIndex = 972;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(-3, 343);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(76, 15);
            this.label17.TabIndex = 973;
            this.label17.Text = "غرض التحويــل:";
            // 
            // Txt_R_Birth_Place
            // 
            this.Txt_R_Birth_Place.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_Birth_Place.Enabled = false;
            this.Txt_R_Birth_Place.Location = new System.Drawing.Point(569, 410);
            this.Txt_R_Birth_Place.Name = "Txt_R_Birth_Place";
            this.Txt_R_Birth_Place.ReadOnly = true;
            this.Txt_R_Birth_Place.Size = new System.Drawing.Size(143, 20);
            this.Txt_R_Birth_Place.TabIndex = 970;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(482, 412);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(82, 15);
            this.label24.TabIndex = 971;
            this.label24.Text = "بلد ومحل الولادة:";
            // 
            // Txt_r_doc_issue
            // 
            this.Txt_r_doc_issue.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_r_doc_issue.Enabled = false;
            this.Txt_r_doc_issue.Location = new System.Drawing.Point(632, 465);
            this.Txt_r_doc_issue.Name = "Txt_r_doc_issue";
            this.Txt_r_doc_issue.ReadOnly = true;
            this.Txt_r_doc_issue.Size = new System.Drawing.Size(168, 20);
            this.Txt_r_doc_issue.TabIndex = 968;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(581, 468);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(46, 15);
            this.label25.TabIndex = 969;
            this.label25.Text = "اصدارها:";
            // 
            // Txt_R_doc_no
            // 
            this.Txt_R_doc_no.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_doc_no.Enabled = false;
            this.Txt_R_doc_no.Location = new System.Drawing.Point(323, 465);
            this.Txt_R_doc_no.Name = "Txt_R_doc_no";
            this.Txt_R_doc_no.ReadOnly = true;
            this.Txt_R_doc_no.Size = new System.Drawing.Size(117, 20);
            this.Txt_R_doc_no.TabIndex = 966;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(283, 467);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(36, 15);
            this.label26.TabIndex = 967;
            this.label26.Text = "رقمها:";
            // 
            // Txt_R_City
            // 
            this.Txt_R_City.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_City.Enabled = false;
            this.Txt_R_City.Location = new System.Drawing.Point(79, 410);
            this.Txt_R_City.Name = "Txt_R_City";
            this.Txt_R_City.ReadOnly = true;
            this.Txt_R_City.Size = new System.Drawing.Size(204, 20);
            this.Txt_R_City.TabIndex = 964;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(2, 412);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label27.Size = new System.Drawing.Size(77, 15);
            this.label27.TabIndex = 965;
            this.label27.Text = "مدينة المستلـــم:";
            // 
            // Txt_R_Nat
            // 
            this.Txt_R_Nat.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_Nat.Enabled = false;
            this.Txt_R_Nat.Location = new System.Drawing.Point(798, 410);
            this.Txt_R_Nat.Name = "Txt_R_Nat";
            this.Txt_R_Nat.ReadOnly = true;
            this.Txt_R_Nat.Size = new System.Drawing.Size(168, 20);
            this.Txt_R_Nat.TabIndex = 962;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(723, 412);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(74, 15);
            this.label28.TabIndex = 963;
            this.label28.Text = "جنسية المستلم:";
            // 
            // Txt_R_doc_type
            // 
            this.Txt_R_doc_type.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_doc_type.Enabled = false;
            this.Txt_R_doc_type.Location = new System.Drawing.Point(78, 465);
            this.Txt_R_doc_type.Name = "Txt_R_doc_type";
            this.Txt_R_doc_type.ReadOnly = true;
            this.Txt_R_doc_type.Size = new System.Drawing.Size(208, 20);
            this.Txt_R_doc_type.TabIndex = 961;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(2, 467);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 15);
            this.label29.TabIndex = 960;
            this.label29.Text = "نوع الوثيقـــة:";
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_Name.Enabled = false;
            this.Txt_R_Name.Location = new System.Drawing.Point(79, 383);
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.ReadOnly = true;
            this.Txt_R_Name.Size = new System.Drawing.Size(407, 20);
            this.Txt_R_Name.TabIndex = 958;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(2, 385);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(77, 15);
            this.label30.TabIndex = 959;
            this.label30.Text = "اسم المستـــلــم:";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_Phone.Enabled = false;
            this.Txt_R_Phone.Location = new System.Drawing.Point(798, 383);
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.ReadOnly = true;
            this.Txt_R_Phone.Size = new System.Drawing.Size(168, 20);
            this.Txt_R_Phone.TabIndex = 957;
            // 
            // Txt_R_address
            // 
            this.Txt_R_address.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_address.Enabled = false;
            this.Txt_R_address.Location = new System.Drawing.Point(79, 438);
            this.Txt_R_address.Name = "Txt_R_address";
            this.Txt_R_address.ReadOnly = true;
            this.Txt_R_address.Size = new System.Drawing.Size(401, 20);
            this.Txt_R_address.TabIndex = 949;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(2, 440);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(72, 15);
            this.label31.TabIndex = 956;
            this.label31.Text = "عنوان المستلم:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(517, 385);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(52, 15);
            this.label35.TabIndex = 953;
            this.label35.Text = "التولــــــد:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(442, 468);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label37.Size = new System.Drawing.Size(44, 15);
            this.label37.TabIndex = 951;
            this.label37.Text = "تاريخها:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(726, 385);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(69, 15);
            this.label38.TabIndex = 950;
            this.label38.Text = "هاتف المستلم:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(480, 440);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(80, 15);
            this.label32.TabIndex = 955;
            this.label32.Text = "مهنة المستلـــــم:";
            // 
            // Txt_r_job
            // 
            this.Txt_r_job.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_r_job.Enabled = false;
            this.Txt_r_job.Location = new System.Drawing.Point(564, 438);
            this.Txt_r_job.Name = "Txt_r_job";
            this.Txt_r_job.ReadOnly = true;
            this.Txt_r_job.Size = new System.Drawing.Size(190, 20);
            this.Txt_r_job.TabIndex = 948;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(121, 374);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(854, 1);
            this.flowLayoutPanel2.TabIndex = 977;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(193, 142);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(777, 1);
            this.flowLayoutPanel6.TabIndex = 994;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-1, 558);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(980, 1);
            this.flowLayoutPanel4.TabIndex = 986;
            // 
            // Btn_Stop_rem
            // 
            this.Btn_Stop_rem.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Stop_rem.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Stop_rem.Location = new System.Drawing.Point(299, 561);
            this.Btn_Stop_rem.Name = "Btn_Stop_rem";
            this.Btn_Stop_rem.Size = new System.Drawing.Size(184, 25);
            this.Btn_Stop_rem.TabIndex = 991;
            this.Btn_Stop_rem.Text = "مــــــــــــوافــــــــــــــــــــــــق";
            this.Btn_Stop_rem.UseVisualStyleBackColor = true;
            this.Btn_Stop_rem.Click += new System.EventHandler(this.Btn_Stop_rem_Click);
            // 
            // Txt_Discreption
            // 
            this.Txt_Discreption.Enabled = false;
            this.Txt_Discreption.Location = new System.Drawing.Point(732, 263);
            this.Txt_Discreption.MaxLength = 199;
            this.Txt_Discreption.Name = "Txt_Discreption";
            this.Txt_Discreption.ReadOnly = true;
            this.Txt_Discreption.Size = new System.Drawing.Size(234, 20);
            this.Txt_Discreption.TabIndex = 995;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(641, 265);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(82, 15);
            this.label33.TabIndex = 996;
            this.label33.Text = "التفاصيـــــــــــل:";
            // 
            // txt_resd_sen
            // 
            this.txt_resd_sen.BackColor = System.Drawing.SystemColors.Control;
            this.txt_resd_sen.Enabled = false;
            this.txt_resd_sen.Location = new System.Drawing.Point(369, 237);
            this.txt_resd_sen.Name = "txt_resd_sen";
            this.txt_resd_sen.ReadOnly = true;
            this.txt_resd_sen.Size = new System.Drawing.Size(113, 20);
            this.txt_resd_sen.TabIndex = 1127;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(283, 239);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(74, 15);
            this.label16.TabIndex = 1128;
            this.label16.Text = "نوع الاقامـــــة:";
            // 
            // txt_resd_rec
            // 
            this.txt_resd_rec.BackColor = System.Drawing.SystemColors.Control;
            this.txt_resd_rec.Enabled = false;
            this.txt_resd_rec.Location = new System.Drawing.Point(367, 410);
            this.txt_resd_rec.Name = "txt_resd_rec";
            this.txt_resd_rec.ReadOnly = true;
            this.txt_resd_rec.Size = new System.Drawing.Size(113, 20);
            this.txt_resd_rec.TabIndex = 1129;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(283, 412);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(74, 15);
            this.label34.TabIndex = 1130;
            this.label34.Text = "نوع الاقامـــــة:";
            // 
            // txt_social_sen
            // 
            this.txt_social_sen.BackColor = System.Drawing.SystemColors.Control;
            this.txt_social_sen.Enabled = false;
            this.txt_social_sen.Location = new System.Drawing.Point(861, 289);
            this.txt_social_sen.Name = "txt_social_sen";
            this.txt_social_sen.ReadOnly = true;
            this.txt_social_sen.Size = new System.Drawing.Size(103, 20);
            this.txt_social_sen.TabIndex = 1131;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(801, 291);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(59, 15);
            this.label36.TabIndex = 1132;
            this.label36.Text = "الرقم.وطني";
            // 
            // txt_social_rec
            // 
            this.txt_social_rec.BackColor = System.Drawing.SystemColors.Control;
            this.txt_social_rec.Enabled = false;
            this.txt_social_rec.Location = new System.Drawing.Point(860, 465);
            this.txt_social_rec.Name = "txt_social_rec";
            this.txt_social_rec.ReadOnly = true;
            this.txt_social_rec.Size = new System.Drawing.Size(103, 20);
            this.txt_social_rec.TabIndex = 1133;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(800, 468);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(59, 15);
            this.label42.TabIndex = 1134;
            this.label42.Text = "الرقم.وطني";
            // 
            // txt_purpose_sen
            // 
            this.txt_purpose_sen.BackColor = System.Drawing.SystemColors.Control;
            this.txt_purpose_sen.Enabled = false;
            this.txt_purpose_sen.Location = new System.Drawing.Point(401, 341);
            this.txt_purpose_sen.Name = "txt_purpose_sen";
            this.txt_purpose_sen.ReadOnly = true;
            this.txt_purpose_sen.Size = new System.Drawing.Size(243, 20);
            this.txt_purpose_sen.TabIndex = 1135;
            // 
            // txt_purpose_rec
            // 
            this.txt_purpose_rec.BackColor = System.Drawing.SystemColors.Control;
            this.txt_purpose_rec.Enabled = false;
            this.txt_purpose_rec.Location = new System.Drawing.Point(74, 488);
            this.txt_purpose_rec.Name = "txt_purpose_rec";
            this.txt_purpose_rec.ReadOnly = true;
            this.txt_purpose_rec.Size = new System.Drawing.Size(297, 20);
            this.txt_purpose_rec.TabIndex = 1137;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(-1, 490);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label44.Size = new System.Drawing.Size(70, 15);
            this.label44.TabIndex = 1138;
            this.label44.Text = "غرض التحويل:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(379, 341);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(16, 20);
            this.label43.TabIndex = 1145;
            this.label43.Text = "/";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(648, 315);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label45.Size = new System.Drawing.Size(16, 20);
            this.label45.TabIndex = 1146;
            this.label45.Text = "/";
            // 
            // Txt_S_details_job
            // 
            this.Txt_S_details_job.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_details_job.Enabled = false;
            this.Txt_S_details_job.Location = new System.Drawing.Point(670, 315);
            this.Txt_S_details_job.MaxLength = 99;
            this.Txt_S_details_job.Name = "Txt_S_details_job";
            this.Txt_S_details_job.ReadOnly = true;
            this.Txt_S_details_job.Size = new System.Drawing.Size(296, 20);
            this.Txt_S_details_job.TabIndex = 1147;
            // 
            // Txt_R_Relation
            // 
            this.Txt_R_Relation.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_Relation.Enabled = false;
            this.Txt_R_Relation.Location = new System.Drawing.Point(758, 488);
            this.Txt_R_Relation.MaxLength = 200;
            this.Txt_R_Relation.Name = "Txt_R_Relation";
            this.Txt_R_Relation.ReadOnly = true;
            this.Txt_R_Relation.Size = new System.Drawing.Size(204, 20);
            this.Txt_R_Relation.TabIndex = 1148;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(637, 491);
            this.label58.Name = "label58";
            this.label58.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label58.Size = new System.Drawing.Size(120, 15);
            this.label58.TabIndex = 1149;
            this.label58.Text = "علاقة المستلم بالمرسل:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(755, 438);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label46.Size = new System.Drawing.Size(16, 20);
            this.label46.TabIndex = 1150;
            this.label46.Text = "/";
            // 
            // Txt_R_details_job
            // 
            this.Txt_R_details_job.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_details_job.Enabled = false;
            this.Txt_R_details_job.Location = new System.Drawing.Point(775, 438);
            this.Txt_R_details_job.MaxLength = 99;
            this.Txt_R_details_job.Name = "Txt_R_details_job";
            this.Txt_R_details_job.ReadOnly = true;
            this.Txt_R_details_job.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_details_job.Size = new System.Drawing.Size(190, 20);
            this.Txt_R_details_job.TabIndex = 1151;
            // 
            // Txt_R_notes
            // 
            this.Txt_R_notes.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_notes.Enabled = false;
            this.Txt_R_notes.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_notes.Location = new System.Drawing.Point(440, 487);
            this.Txt_R_notes.MaxLength = 199;
            this.Txt_R_notes.Name = "Txt_R_notes";
            this.Txt_R_notes.ReadOnly = true;
            this.Txt_R_notes.Size = new System.Drawing.Size(193, 23);
            this.Txt_R_notes.TabIndex = 1152;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(372, 491);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label47.Size = new System.Drawing.Size(64, 15);
            this.label47.TabIndex = 1153;
            this.label47.Text = "التفاصيـــــل:";
            // 
            // Txt_PRCur
            // 
            this.Txt_PRCur.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_PRCur.Enabled = false;
            this.Txt_PRCur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_PRCur.Location = new System.Drawing.Point(758, 147);
            this.Txt_PRCur.Name = "Txt_PRCur";
            this.Txt_PRCur.ReadOnly = true;
            this.Txt_PRCur.Size = new System.Drawing.Size(204, 22);
            this.Txt_PRCur.TabIndex = 1120;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(665, 151);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(90, 14);
            this.label41.TabIndex = 1121;
            this.label41.Text = "عملة التسليم:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(6, 151);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(84, 14);
            this.label40.TabIndex = 1122;
            this.label40.Text = "عملة الحوالة:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(362, 151);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 14);
            this.label39.TabIndex = 1124;
            this.label39.Text = "مبالغ الحوالة:";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(851, 111);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(111, 25);
            this.BtnAdd.TabIndex = 1155;
            this.BtnAdd.Text = "عرض";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // Txt_Rem_no
            // 
            this.Txt_Rem_no.Location = new System.Drawing.Point(718, 63);
            this.Txt_Rem_no.Name = "Txt_Rem_no";
            this.Txt_Rem_no.Size = new System.Drawing.Size(236, 20);
            this.Txt_Rem_no.TabIndex = 1154;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(587, 65);
            this.label65.Name = "label65";
            this.label65.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label65.Size = new System.Drawing.Size(104, 14);
            this.label65.TabIndex = 1156;
            this.label65.Text = "رقــــــم الحوالـــة:";
            // 
            // Txt_Scity
            // 
            this.Txt_Scity.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_Scity.Enabled = false;
            this.Txt_Scity.Location = new System.Drawing.Point(142, 174);
            this.Txt_Scity.Name = "Txt_Scity";
            this.Txt_Scity.ReadOnly = true;
            this.Txt_Scity.Size = new System.Drawing.Size(204, 20);
            this.Txt_Scity.TabIndex = 1157;
            // 
            // Txt_Tcity
            // 
            this.Txt_Tcity.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_Tcity.Enabled = false;
            this.Txt_Tcity.Location = new System.Drawing.Point(490, 174);
            this.Txt_Tcity.Name = "Txt_Tcity";
            this.Txt_Tcity.ReadOnly = true;
            this.Txt_Tcity.Size = new System.Drawing.Size(204, 20);
            this.Txt_Tcity.TabIndex = 1159;
            // 
            // Txt_Rcur
            // 
            this.Txt_Rcur.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_Rcur.Enabled = false;
            this.Txt_Rcur.Location = new System.Drawing.Point(94, 148);
            this.Txt_Rcur.Name = "Txt_Rcur";
            this.Txt_Rcur.ReadOnly = true;
            this.Txt_Rcur.Size = new System.Drawing.Size(204, 20);
            this.Txt_Rcur.TabIndex = 1161;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(352, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 14);
            this.label3.TabIndex = 1162;
            this.label3.Text = "مدينة الاستلام المالية:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(6, 174);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(131, 14);
            this.label48.TabIndex = 1163;
            this.label48.Text = "مدينة الارسال المالية:";
            // 
            // cmb_Flag_stop
            // 
            this.cmb_Flag_stop.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Flag_stop.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Flag_stop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Flag_stop.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Flag_stop.FormattingEnabled = true;
            this.cmb_Flag_stop.Items.AddRange(new object[] {
            "أيقاف الحوالة",
            "ألغاء الأيقاف"});
            this.cmb_Flag_stop.Location = new System.Drawing.Point(719, 34);
            this.cmb_Flag_stop.Name = "cmb_Flag_stop";
            this.cmb_Flag_stop.Size = new System.Drawing.Size(236, 24);
            this.cmb_Flag_stop.TabIndex = 1164;
            this.cmb_Flag_stop.SelectedIndexChanged += new System.EventHandler(this.cmb_Flag_stop_SelectedIndexChanged);
            // 
            // Txt_Note_Stop
            // 
            this.Txt_Note_Stop.BackColor = System.Drawing.SystemColors.Window;
            this.Txt_Note_Stop.Location = new System.Drawing.Point(190, 534);
            this.Txt_Note_Stop.Name = "Txt_Note_Stop";
            this.Txt_Note_Stop.Size = new System.Drawing.Size(774, 20);
            this.Txt_Note_Stop.TabIndex = 1165;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(0, 537);
            this.label49.Name = "label49";
            this.label49.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label49.Size = new System.Drawing.Size(191, 15);
            this.label49.TabIndex = 1166;
            this.label49.Text = "ملاحظات سبب ايقاف/الغاء ايقاف الحوالة :";
            // 
            // Tot_Amount
            // 
            this.Tot_Amount.BackColor = System.Drawing.SystemColors.Control;
            this.Tot_Amount.Enabled = false;
            this.Tot_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount.Location = new System.Drawing.Point(449, 147);
            this.Tot_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount.Name = "Tot_Amount";
            this.Tot_Amount.NumberDecimalDigits = 3;
            this.Tot_Amount.NumberDecimalSeparator = ".";
            this.Tot_Amount.NumberGroupSeparator = ",";
            this.Tot_Amount.ReadOnly = true;
            this.Tot_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Tot_Amount.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount.TabIndex = 1125;
            this.Tot_Amount.Text = "0.000";
            // 
            // Txt_r_birthdate
            // 
            this.Txt_r_birthdate.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_r_birthdate.DateSeperator = '/';
            this.Txt_r_birthdate.Enabled = false;
            this.Txt_r_birthdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_r_birthdate.Location = new System.Drawing.Point(568, 382);
            this.Txt_r_birthdate.Mask = "0000/00/00";
            this.Txt_r_birthdate.Name = "Txt_r_birthdate";
            this.Txt_r_birthdate.PromptChar = ' ';
            this.Txt_r_birthdate.ReadOnly = true;
            this.Txt_r_birthdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_r_birthdate.Size = new System.Drawing.Size(143, 22);
            this.Txt_r_birthdate.TabIndex = 974;
            this.Txt_r_birthdate.Text = "00000000";
            this.Txt_r_birthdate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.Txt_r_birthdate.ValidatingType = typeof(System.DateTime);
            // 
            // Txt_R_doc_date
            // 
            this.Txt_R_doc_date.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_R_doc_date.DateSeperator = '/';
            this.Txt_R_doc_date.Enabled = false;
            this.Txt_R_doc_date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_doc_date.Location = new System.Drawing.Point(487, 464);
            this.Txt_R_doc_date.Mask = "0000/00/00";
            this.Txt_R_doc_date.Name = "Txt_R_doc_date";
            this.Txt_R_doc_date.PromptChar = ' ';
            this.Txt_R_doc_date.ReadOnly = true;
            this.Txt_R_doc_date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_R_doc_date.Size = new System.Drawing.Size(94, 22);
            this.Txt_R_doc_date.TabIndex = 946;
            this.Txt_R_doc_date.Text = "00000000";
            this.Txt_R_doc_date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.Txt_R_doc_date.ValidatingType = typeof(System.DateTime);
            // 
            // Txt_S_Birth
            // 
            this.Txt_S_Birth.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_Birth.DateSeperator = '/';
            this.Txt_S_Birth.Enabled = false;
            this.Txt_S_Birth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_Birth.Location = new System.Drawing.Point(568, 209);
            this.Txt_S_Birth.Mask = "0000/00/00";
            this.Txt_S_Birth.Name = "Txt_S_Birth";
            this.Txt_S_Birth.PromptChar = ' ';
            this.Txt_S_Birth.ReadOnly = true;
            this.Txt_S_Birth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_Birth.Size = new System.Drawing.Size(143, 22);
            this.Txt_S_Birth.TabIndex = 942;
            this.Txt_S_Birth.Text = "00000000";
            this.Txt_S_Birth.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.Txt_S_Birth.ValidatingType = typeof(System.DateTime);
            // 
            // Txt_S_doc_Date
            // 
            this.Txt_S_doc_Date.BackColor = System.Drawing.SystemColors.Control;
            this.Txt_S_doc_Date.DateSeperator = '/';
            this.Txt_S_doc_Date.Enabled = false;
            this.Txt_S_doc_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_S_doc_Date.Location = new System.Drawing.Point(490, 288);
            this.Txt_S_doc_Date.Mask = "0000/00/00";
            this.Txt_S_doc_Date.Name = "Txt_S_doc_Date";
            this.Txt_S_doc_Date.PromptChar = ' ';
            this.Txt_S_doc_Date.ReadOnly = true;
            this.Txt_S_doc_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_S_doc_Date.Size = new System.Drawing.Size(90, 22);
            this.Txt_S_doc_Date.TabIndex = 913;
            this.Txt_S_doc_Date.Text = "00000000";
            this.Txt_S_doc_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // lal_stop
            // 
            this.lal_stop.AutoSize = true;
            this.lal_stop.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lal_stop.ForeColor = System.Drawing.Color.Navy;
            this.lal_stop.Location = new System.Drawing.Point(600, 39);
            this.lal_stop.Name = "lal_stop";
            this.lal_stop.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lal_stop.Size = new System.Drawing.Size(91, 14);
            this.lal_stop.TabIndex = 1167;
            this.lal_stop.Text = " نوع العــمـلـية:";
            // 
            // cbo_order_type
            // 
            this.cbo_order_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_order_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_order_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_order_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_order_type.FormattingEnabled = true;
            this.cbo_order_type.Items.AddRange(new object[] {
            "فروع",
            "وكالات"});
            this.cbo_order_type.Location = new System.Drawing.Point(305, 33);
            this.cbo_order_type.Name = "cbo_order_type";
            this.cbo_order_type.Size = new System.Drawing.Size(142, 24);
            this.cbo_order_type.TabIndex = 1266;
            this.cbo_order_type.SelectedIndexChanged += new System.EventHandler(this.cbo_order_type_SelectedIndexChanged);
            // 
            // txt_srch
            // 
            this.txt_srch.AutoSize = true;
            this.txt_srch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.txt_srch.ForeColor = System.Drawing.Color.Navy;
            this.txt_srch.Location = new System.Drawing.Point(11, 36);
            this.txt_srch.Name = "txt_srch";
            this.txt_srch.Size = new System.Drawing.Size(62, 16);
            this.txt_srch.TabIndex = 1265;
            this.txt_srch.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(74, 33);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(230, 22);
            this.Txt_Sub_Cust.TabIndex = 1264;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(8, 58);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(439, 72);
            this.Grd_Sub_Cust.TabIndex = 1263;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "cust_id";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 91;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Sub_Cust_ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Acust_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 225;
            // 
            // bon_end
            // 
            this.bon_end.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.bon_end.ForeColor = System.Drawing.Color.Navy;
            this.bon_end.Location = new System.Drawing.Point(489, 561);
            this.bon_end.Name = "bon_end";
            this.bon_end.Size = new System.Drawing.Size(184, 25);
            this.bon_end.TabIndex = 1267;
            this.bon_end.Text = "انـــــــــــهــــــاء";
            this.bon_end.UseVisualStyleBackColor = true;
            this.bon_end.Click += new System.EventHandler(this.bon_end_Click);
            // 
            // txt_Pres
            // 
            this.txt_Pres.BackColor = System.Drawing.SystemColors.Control;
            this.txt_Pres.Location = new System.Drawing.Point(191, 509);
            this.txt_Pres.Name = "txt_Pres";
            this.txt_Pres.ReadOnly = true;
            this.txt_Pres.Size = new System.Drawing.Size(772, 20);
            this.txt_Pres.TabIndex = 1268;
            // 
            // lab_old_reson
            // 
            this.lab_old_reson.AutoSize = true;
            this.lab_old_reson.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_old_reson.ForeColor = System.Drawing.Color.Navy;
            this.lab_old_reson.Location = new System.Drawing.Point(19, 512);
            this.lab_old_reson.Name = "lab_old_reson";
            this.lab_old_reson.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lab_old_reson.Size = new System.Drawing.Size(170, 15);
            this.lab_old_reson.TabIndex = 1269;
            this.lab_old_reson.Text = "ملاحظات ايقاف/الغاء ايقاف السابقة :";
            // 
            // Stop_Rem_online
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 589);
            this.Controls.Add(this.txt_Pres);
            this.Controls.Add(this.lab_old_reson);
            this.Controls.Add(this.bon_end);
            this.Controls.Add(this.cbo_order_type);
            this.Controls.Add(this.txt_srch);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.Controls.Add(this.lal_stop);
            this.Controls.Add(this.Txt_Note_Stop);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.cmb_Flag_stop);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Rcur);
            this.Controls.Add(this.Txt_Tcity);
            this.Controls.Add(this.Txt_Scity);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Txt_Rem_no);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.Txt_R_notes);
            this.Controls.Add(this.Txt_R_details_job);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.Txt_R_Relation);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.Txt_S_details_job);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.txt_purpose_rec);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.txt_purpose_sen);
            this.Controls.Add(this.txt_social_rec);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.txt_social_sen);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.txt_resd_rec);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.txt_resd_sen);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Tot_Amount);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.Txt_PRCur);
            this.Controls.Add(this.Txt_Discreption);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.Btn_Stop_rem);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_r_birthdate);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Txt_R_Birth_Place);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Txt_r_doc_issue);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.Txt_R_doc_no);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.Txt_R_City);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.Txt_R_Nat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Txt_R_doc_type);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.Txt_R_doc_date);
            this.Controls.Add(this.Txt_R_address);
            this.Controls.Add(this.Txt_r_job);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.flowLayoutPanel17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_S_Birth);
            this.Controls.Add(this.Txt_Relionship);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_S_Birth_Place);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_S_doc_issue);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_S_doc_no);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtScity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txts_nat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_S_doc_type);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtS_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtS_phone);
            this.Controls.Add(this.Txt_S_doc_Date);
            this.Controls.Add(this.TxtS_address);
            this.Controls.Add(this.Txt_s_job);
            this.Controls.Add(this.Txt_Soruce_money);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Stop_Rem_online";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "569";
            this.Text = "Stop_Rem_online";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Stop_Rem_online_FormClosed);
            this.Load += new System.EventHandler(this.Stop_Rem_online_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private MyDateTextBox Txt_S_doc_Date;
        private System.Windows.Forms.TextBox TxtS_address;
        private System.Windows.Forms.TextBox Txt_s_job;
        private System.Windows.Forms.TextBox Txt_Soruce_money;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TxtS_phone;
        private System.Windows.Forms.TextBox TxtS_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txt_S_doc_type;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txts_nat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtScity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txt_S_doc_issue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_S_Birth_Place;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_Relionship;
        private System.Windows.Forms.Label label12;
        private MyDateTextBox Txt_S_Birth;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private MyDateTextBox Txt_r_birthdate;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Txt_R_Birth_Place;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Txt_r_doc_issue;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_R_doc_no;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox Txt_R_City;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Txt_R_Nat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Txt_R_doc_type;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private MyDateTextBox Txt_R_doc_date;
        private System.Windows.Forms.TextBox Txt_R_address;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox Txt_r_job;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button Btn_Stop_rem;
        private System.Windows.Forms.TextBox Txt_Discreption;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txt_resd_sen;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_resd_rec;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txt_social_sen;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txt_social_rec;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txt_purpose_sen;
        private System.Windows.Forms.TextBox txt_purpose_rec;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox Txt_S_details_job;
        private System.Windows.Forms.TextBox Txt_R_Relation;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox Txt_R_details_job;
        private System.Windows.Forms.TextBox Txt_R_notes;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox Txt_PRCur;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.TextBox Txt_Rem_no;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox Txt_Scity;
        private System.Windows.Forms.TextBox Txt_Tcity;
        private System.Windows.Forms.TextBox Txt_Rcur;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox cmb_Flag_stop;
        private System.Windows.Forms.TextBox Txt_Note_Stop;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label lal_stop;
        private System.Windows.Forms.ComboBox cbo_order_type;
        private System.Windows.Forms.Label txt_srch;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button bon_end;
        private System.Windows.Forms.TextBox txt_Pres;
        private System.Windows.Forms.Label lab_old_reson;
    }
}