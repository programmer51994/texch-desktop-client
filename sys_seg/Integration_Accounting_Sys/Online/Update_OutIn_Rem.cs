﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Update_OutIn_Rem : Form
    {
        string Doc_S_Exp = "";
        string @format = "dd/MM/yyyy";
        string @format_null = "";
        string S_doc_exp_null = "0000/00/00";
        string _Date = "";
        string rem_no = "";
        string local_Cur = "";
        string forgin_Cur = "";
        int Oper_Id = 0;
       string Vo_No = "";
        string User = "";
        double Rem_Amount = 0;
        string SqlTxt1 = "";
        int cust_id_online = 0;
        bool change = false;
        bool change1 = false;
        bool chang_scity = false;
        bool chang_rcity = false;
        Int16 send_flag = 0;
        string s_notes = "";
        string Sql_Text = "";
        Int16 bs_t_city;
        //----

        BindingSource binding_Cmb_Case_Purpose = new BindingSource();
        BindingSource binding_tcity_id = new BindingSource();
        BindingSource binding_rcur = new BindingSource();
        BindingSource binding_pcur = new BindingSource();
        BindingSource binding_cbo_scity_con = new BindingSource();
        BindingSource binding_Code_phone_S = new BindingSource();
        BindingSource binding_Code_phone_R = new BindingSource();
        BindingSource binding_cbo_rcity_con = new BindingSource();
        BindingSource binding_sfmd = new BindingSource();
        BindingSource binding_cb_snat = new BindingSource();
        BindingSource binding_cb_rnat = new BindingSource();
        BindingSource _Bs_Rem = new BindingSource();
        BindingSource binding_Gender_id = new BindingSource();
        BindingSource cur_comm = new BindingSource();
        BindingSource binding_cmb_job_sender = new BindingSource();
        BindingSource binding_cmb_job_receiver = new BindingSource();
        BindingSource binding_resd = new BindingSource();
        BindingSource _Bs_cust1 = new BindingSource();
        string s_Ejob = "";
        string r_Ejob = "";
        string S_Social_No = "";
        string R_Social_No = "";
        string sender_name_BL = "";
        DataTable Dt_Cust_TBl = new DataTable();
        BindingSource Bs_Sub_Cust = new BindingSource();
        DataTable DT1 = new DataTable();


        //---
        public Update_OutIn_Rem()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);

        }

        private void Add_rem_Load(object sender, EventArgs e)
        {
            Cbo_rem_path.SelectedIndex = 1;
            resd_cmb.SelectedValue = 0;

            Cbo_Cust_Id.Visible = false;
            lbl_branch.Visible = false;


            Txt_Doc_S_Exp.Format = DateTimePickerFormat.Custom;
            Txt_Doc_S_Exp.CustomFormat = @format;


            Txt_Sbirth_Date.Format = DateTimePickerFormat.Custom;
            Txt_Sbirth_Date.CustomFormat = @format;


            Txt_Doc_S_Date.Format = DateTimePickerFormat.Custom;
            Txt_Doc_S_Date.CustomFormat = @format;

            //-------------------------------
            //String Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  "
            //    + "  From CUSTOMERS A, TERMINALS B   "
            //    + "  Where A.CUST_ID = B.CUST_ID "
            //    + "  And A.Cust_Flag <>0  "
            //    + "  and B.t_id = " +connection.T_ID
            //    + " union "
            //    + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name , Sub_Cust_ID as cust_id "
            //    + " From  Sub_CUSTOMERS_online A  "
            //    + " Where A.Cust_state  =  0 "
            //    + " and A.t_id = " + connection.T_ID
            //    + "  union "
            //    + "  select 0 as T_id,'جميع الفروع والوكلاء'  as Acust_name , "
            //    + "  'All Branches and sub branches' as     Ecust_name ,0 as    cust_id    "
            //    + " order by cust_id ";

            //CboCust_Id.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
            //CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            //CboCust_Id.ValueMember = "cust_id";
            //-----------------------------------------------------------------------------
            if (connection.T_ID == 1)  // الادارة
            {

                Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as  Sub_Cust_ID , 0 as Order_type"
                    + "  From CUSTOMERS A, TERMINALS B   "
                    + "  Where A.CUST_ID = B.CUST_ID "
                    + "  And A.Cust_Flag <> 0  "

                    + " union "
                    + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID , 1 as Order_type"
                    + " From  Sub_CUSTOMERS_online A  "
                    //, Login_Cust_Online B "
                    + " Where A.Cust_state  =  1 "//فعال
                    //+ " and A.Sub_Cust_ID =  B.Cust_id" 
                    + " and A.t_id =  " + connection.T_ID
                    + " union "
                     + "select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID , 1 as Order_type"
                     + " From  Sub_CUSTOMERS_online A "
                     + " Where A.Cust_state  =  1"
                     + "  and A.t_id <>  1"
                     + " order by cust_id,Order_type desc";
            }

            if (connection.T_ID != 1)
            {
                Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as  Sub_Cust_ID ,0 as Order_type"
                       + "  From CUSTOMERS A, TERMINALS B   "
                       + "  Where A.CUST_ID = B.CUST_ID "
                       + "  And A.Cust_Flag <> 0  "
                        + " and B.t_id =  " + connection.T_ID
                       + " union "
                       + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID  , 1 as Order_type "
                       + " From  Sub_CUSTOMERS_online A  "
                    //, Login_Cust_Online B "
                       + " Where A.Cust_state  =  1 "//فعال
                    //+ " and A.Sub_Cust_ID =  B.Cust_id" 
                       + " and A.t_id =  " + connection.T_ID
                       + " order by cust_id ,Order_type desc";
            }


            _Bs_cust1.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
            Cbo_Cust_Id.DataSource = _Bs_cust1;
            Cbo_Cust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            Cbo_Cust_Id.ValueMember = "cust_id";


           //------------------------------------------------------------------
            string sql_report_txt = "select * from Reports_setting_Tbl";
            connection.SqlExec(sql_report_txt, "Reports_setting_Tbl");

            if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار اعدادات الطباعة" : "please add reporting setting", MyGeneral_Lib.LblCap);
                this.Close();
            }
            else
            {
                string SqlTxt = " Select  CUST_ID From  TERMINALS where t_id = " + connection.T_ID;
                connection.SqlExec(SqlTxt, "Cust_tbl");


                cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);



                SqlTxt1 = " exec Full_information_Web_offline " + connection.Lang_id + "," + 0 + "," + cust_id_online;
                connection.SqlExec(SqlTxt1, "Full_information_tab");

                //-----
                cmb_comm_type.SelectedIndex = 0;
                //-----مدينة الاصدار
                //binding_cbo_city.DataSource = connection.SQLDS.Tables["Full_information_tab"].Select("cit_id=" + connection.city_id_online).CopyToDataTable();
                ////Cbo_city.DataSource = binding_cbo_city;
                ////Cbo_city.ValueMember = "Cit_ID";
                ////Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "eCity_Name";
                //----اسباب التحويل
                binding_Cmb_Case_Purpose.DataSource = connection.SQLDS.Tables["Full_information_tab5"];
                Cmb_Case_Purpose.DataSource = binding_Cmb_Case_Purpose;
                Cmb_Case_Purpose.ValueMember = "Case_purpose_id";
                Cmb_Case_Purpose.DisplayMember = connection.Lang_id == 1 ? "Case_purpose_Aname" : "Case_purpose_Ename";
                //----مدينة الاستلام
                binding_tcity_id.DataSource = connection.SQLDS.Tables["Full_information_tab"];

                Cmb_T_City.DataSource = binding_tcity_id;
                Cmb_T_City.ValueMember = "Cit_ID";
                Cmb_T_City.DisplayMember = connection.Lang_id == 1 ? "acity_name" : "ecity_name";
                Cmb_T_City.SelectedIndex = 0;
                 
                //----عملة الاصدار
                //Dt_rcur_rem = MicrosoftOfficeInterop.OneNote.SQLDS.Tables["Sub_cust_tbl2"].DefaultView.ToTable(true, "ACur_Name_rem", "eCur_Name_rem", "R_CUR_ID").Select("").CopyToDataTable();
                binding_rcur.DataSource = connection.SQLDS.Tables["Full_information_tab4"];

                Cmb_R_CUR_ID.DataSource = binding_rcur;
                Cmb_R_CUR_ID.ValueMember = "cur_id";
                Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
                //--------عملة الدفع
                // Dt_pcur_rem = MicrosoftOfficeInterop.OneNote.SQLDS.Tables["Sub_cust_tbl2"].DefaultView.ToTable(true, "ACur_Name_pay", "eCur_Name_pay", "pR_CUR_ID").Select("").CopyToDataTable();
                binding_pcur.DataSource = connection.SQLDS.Tables["Full_information_tab4"];

                Cmb_PR_Cur_Id.DataSource = binding_pcur;
                Cmb_PR_Cur_Id.ValueMember = "cur_id";
                Cmb_PR_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";


                if (connection.SQLDS.Tables["Full_information_tab"].Rows.Count > 0)
                {



                    binding_cbo_scity_con.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                    Cmb_S_City.DataSource = binding_cbo_scity_con;
                    Cmb_S_City.ValueMember = "Cit_ID";
                    Cmb_S_City.DisplayMember = "ACity_Name";
                    binding_Code_phone_S.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                    Cmb_Code_phone_S.DataSource = binding_Code_phone_S;
                    Cmb_Code_phone_S.ValueMember = "COUN_K_PH";
                    Cmb_Code_phone_S.DisplayMember = "COUN_K_PH";



                    //-----------------------
                    binding_cbo_rcity_con.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                    Cmb_R_City.DataSource = binding_cbo_rcity_con;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = "ACity_Name";

                    binding_Code_phone_R.DataSource = connection.SQLDS.Tables["Full_information_tab"];
                    Cmb_phone_Code_R.DataSource = binding_Code_phone_R;
                    Cmb_phone_Code_R.ValueMember = "COUN_K_PH";
                    Cmb_phone_Code_R.DisplayMember = "COUN_K_PH";


                }
                if (connection.SQLDS.Tables["Full_information_tab1"].Rows.Count > 0)
                {
                    binding_sfmd.DataSource = connection.SQLDS.Tables["Full_information_tab1"];
                    Cmb_S_Doc_Type.DataSource = binding_sfmd;
                    Cmb_S_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_S_Doc_Type.DisplayMember = "Fmd_AName";
                }
                if (connection.SQLDS.Tables["Full_information_tab2"].Rows.Count > 0)
                {
                    binding_cb_snat.DataSource = connection.SQLDS.Tables["Full_information_tab2"];
                    cmb_s_nat.DataSource = binding_cb_snat;
                    cmb_s_nat.ValueMember = "Nat_ID";
                    cmb_s_nat.DisplayMember = "Nat_AName";
                    ///------------------
                    binding_cb_rnat.DataSource = connection.SQLDS.Tables["Full_information_tab2"];
                    Cmb_R_Nat.DataSource = binding_cb_rnat;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = "Nat_AName";

                }

                if (connection.SQLDS.Tables["Full_information_tab3"].Rows.Count > 0)
                {
                    binding_Gender_id.DataSource = connection.SQLDS.Tables["Full_information_tab3"];
                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";
                        //"Gender_Aname";
                    //cmb_Gender_id.DisplayMember = "Gender_Aname";
                }


                cur_comm.DataSource = connection.SQLDS.Tables["Full_information_tab4"];
                Cmb_Comm_Cur.DataSource = cur_comm;
                Cmb_Comm_Cur.DisplayMember = connection.Lang_id == 1 ? "ACur_Name" : "ECur_Name";
                Cmb_Comm_Cur.ValueMember = "cur_id";

                //-------------------------------------------مقيم و غير مقيم 
                binding_resd.DataSource = connection.SQLDS.Tables["Full_information_tab8"];

                resd_cmb.DataSource = binding_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename";

                //-------------------------------------------------------
                binding_cmb_job_receiver.DataSource = connection.SQLDS.Tables["Full_information_tab9"];

                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";

                //-------------------------------------------------------
                binding_cmb_job_sender.DataSource = connection.SQLDS.Tables["Full_information_tab9"];

                cmb_job_sender.DataSource = binding_cmb_job_sender;
                cmb_job_sender.ValueMember = "Job_ID";
                cmb_job_sender.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";

                change1 = true;
                chang_scity = true;
                chang_rcity = true;
            }

            Cbo_rem_path.Visible = false;
            label3.Visible = false;

            if (connection.T_ID == 1)
            {
                Cmb_T_City.Enabled = true;
            }
            else
            { Cmb_T_City.Enabled = false; }

            cbo_order_type.SelectedIndex = 0;

         }


        //-------------------------
        private void Add_rem_FormClosed(object sender, FormClosedEventArgs e)
        {


              change = false;
              change1 = false;
              chang_scity = false;
              chang_rcity = false;
             
           
            string[] Used_Tbl = { "Sub_cust_tbl", "Sub_cust_tbl1", "Sub_cust_tbl2", "Cur_Buy_Sal_Tbl", 
                                     "per_info_tbl", "per_info_tbl1", "Full_information_tab","per_info_tbl_rec" ,"Sub_cust_tbl", 
                                     "Full_information_tab1", "Full_information_tab2" , "Full_information_tab3" ,  "Full_information_tab4", 
                                     "Full_information_tab5", "Full_information_tab6", "Full_information_tab7","Full_information_tab9","Cust_tbl" ,"Cur_loc_tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
            Bs_Sub_Cust.DataSource = new BindingSource();
            
        }

        //-------------------------
        private void Txt_Sbirth_Date_Enter(object sender, EventArgs e)
        {
            this.Focus();
        }
        //-------------------------
        private void Txt_Doc_S_Date_Enter(object sender, EventArgs e)
        {
            this.Focus();
        }
        //-------------------------
        private void TxtNrec_Date_ValueChanged(object sender, EventArgs e)
        {
           
            clear_all_txt();
        }
        //-------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
          

            chang_scity = false;
            
            string[] Str1 = { "Perpare_Rem_Flag_tbl" };
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
          

           
            if (Txt_Rem_no.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل المعلومات الكافية للبحث عن الحوالة" : "Enter Informations Please", MyGeneral_Lib.LblCap);
                clear_all_txt();
                return;

            }
            //Int64 x = Convert.ToInt64(((DataRowView)_Bs_cust1.Current).Row["sub_cust_id"]);
            Int64 cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]);
            Int64 sub_cust_id = Convert.ToInt64(((DataRowView)Bs_Sub_Cust.Current).Row["Sub_Cust_ID"]);


            Cbo_rem_path.Enabled = true;
            Cbo_rem_path.Enabled = true;
            Cmb_T_City.Enabled = true;
            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "[serch_update_rem_out]";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_no.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@cust_id", cust_id);
            connection.SQLCMD.Parameters.AddWithValue("@sub_cust_id", sub_cust_id);
            MyGeneral_Lib.Copytocliptext("serch_update_rem_out", connection.SQLCMD);
            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Perpare_Rem_Flag_tbl");
            obj.Close();
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            _Bs_Rem.DataSource = connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"];
            if (connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط" : "pleace check your remittance", MyGeneral_Lib.LblCap);
                return;
            }


            if (Convert.ToInt16(connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows[0]["case_id"]) == 103 && Convert.ToInt16(connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows[0]["rem_flag"]) == 1)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن تعديل الحوالة لان الحوالة اوفلاين مرسلة" : "Can not Send The rem. Because the rem is send", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows[0]["case_id"]) == 1 && Convert.ToInt16(connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows[0]["rem_flag"]) == 1)
            {
                Cbo_rem_path.Enabled = false;
                Cmb_T_City.Enabled = false;
            }  

            //if (connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows.Count <= 0)
            //{

            //    MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن تعديل الحوالة يرجى الاستعلام عنها" : " remittence could not be updated pleace check your remittance", MyGeneral_Lib.LblCap);
            //    return;
            //}     
            //else
            //{
                try
                {
                    Txt_S_Phone.DataBindings.Clear();
                    cmb_job_sender.DataBindings.Clear();
                    txtS_Suburb.DataBindings.Clear();
                    Txts_street.DataBindings.Clear();
                    TxtS_State.DataBindings.Clear();

                    TxtS_Post_Code.DataBindings.Clear();
                    Txt_Doc_S_Date.DataBindings.Clear();
                    Txt_Doc_S_Exp.DataBindings.Clear();
                    Txt_Doc_S_Issue.DataBindings.Clear();
                    Txt_S_Birth_Place.DataBindings.Clear();

                    Txt_Sbirth_Date.DataBindings.Clear();
                    Txt_S_Doc_No.DataBindings.Clear();
                  //  Txt_another.DataBindings.Clear();
                    txt_Mother_name.DataBindings.Clear();
                    Txt_mail.DataBindings.Clear();


                    Cmb_S_Doc_Type.DataBindings.Clear();
                    Cmb_Code_phone_S.DataBindings.Clear();
                    Cmb_S_City.DataBindings.Clear();
                    cmb_s_nat.DataBindings.Clear();
                    cmb_Gender_id.DataBindings.Clear();

                    Txt_Sender.DataBindings.Clear();
                    Txt_Soruce_money.DataBindings.Clear();
                    Txt_Relionship.DataBindings.Clear();
                    Txt_T_Purpose.DataBindings.Clear();

                    Cmb_Case_Purpose.DataBindings.Clear();

                    Txt_Reciever.DataBindings.Clear();
                    Cmb_R_City.DataBindings.Clear();
                    Cmb_R_Nat.DataBindings.Clear();
                    Cmb_phone_Code_R.DataBindings.Clear();
                    Txt_R_Phone.DataBindings.Clear();

                    Txtr_State.DataBindings.Clear();
                    Txtr_Suburb.DataBindings.Clear();
                    Txtr_Street.DataBindings.Clear();
                    Txtr_Post_Code.DataBindings.Clear();
                    Cmb_T_City.DataBindings.Clear();

                    Txtr_amount.DataBindings.Clear();
                    Cmb_R_CUR_ID.DataBindings.Clear();
                    Cmb_PR_Cur_Id.DataBindings.Clear();
                    Txt_Com_Amount.DataBindings.Clear();
                    Cmb_Comm_Cur.DataBindings.Clear();

                   // Txt_another_rec.DataBindings.Clear();
                    Cbo_rem_path.DataBindings.Clear();
                    cmb_job_receiver.DataBindings.Clear();

                    Tex_Social_ID.DataBindings.Clear();
                    resd_cmb.DataBindings.Clear();
                    Txt_S_details_job.DataBindings.Clear();
                    txt_notes.DataBindings.Clear();

                    TxT_Agent_Real_Cur.DataBindings.Clear();
                    TxT_Agent_Rate.DataBindings.Clear();
                    TxT_Agent_Real_Rem_Amount.DataBindings.Clear();
                    TxT_Agent_Real_Comm_Amount.DataBindings.Clear();




                    Txt_S_Phone.DataBindings.Add("Text", _Bs_Rem, "S_phone");
                    cmb_job_sender.DataBindings.Add("SelectedValue", _Bs_Rem, "s_job_ID");
                    txtS_Suburb.DataBindings.Add("Text", _Bs_Rem, "S_Suburb");
                    Txts_street.DataBindings.Add("Text", _Bs_Rem, "S_Street");
                    TxtS_State.DataBindings.Add("Text", _Bs_Rem, "S_State");

                    TxtS_Post_Code.DataBindings.Add("Text", _Bs_Rem, "s_Post_Code");
                    Txt_Doc_S_Date.DataBindings.Add("Text", _Bs_Rem, "S_doc_ida");
                    Txt_Doc_S_Exp.DataBindings.Add("Text", _Bs_Rem, "S_doc_eda");
                    Txt_Doc_S_Issue.DataBindings.Add("Text", _Bs_Rem, "S_doc_issue");
                    Txt_S_Birth_Place.DataBindings.Add("Text", _Bs_Rem, "SBirth_Place");

                    Txt_Sbirth_Date.DataBindings.Add("Text", _Bs_Rem, "sbirth_date");
                    Txt_S_Doc_No.DataBindings.Add("Text", _Bs_Rem, "S_doc_no");
                    Txt_mail.DataBindings.Add("Text", _Bs_Rem, "s_Email");
                    Cmb_S_City.DataBindings.Add("SelectedValue", _Bs_Rem, "Sa_city_id");
                    Cmb_S_Doc_Type.DataBindings.Add("SelectedValue", _Bs_Rem, "sFrm_doc_id");

                    cmb_s_nat.DataBindings.Add("SelectedValue", _Bs_Rem, "snat_Id");
                    Cmb_Code_phone_S.DataBindings.Add("SelectedValue", _Bs_Rem, "Sender_Code_phone");
                    cmb_Gender_id.DataBindings.Add("text", _Bs_Rem, "S_Gender_Aname");
                    Txt_Sender.DataBindings.Add("Text", _Bs_Rem, "s_Name");
                  //  Txt_another.DataBindings.Add("Text", _Bs_Rem, "S_EName");

                    txt_Mother_name.DataBindings.Add("Text", _Bs_Rem, "S_Mother_name");
                    Txt_Soruce_money.DataBindings.Add("Text", _Bs_Rem, "Source_money");
                    Txt_Relionship.DataBindings.Add("Text", _Bs_Rem, "Relation_S_R");
                    Txt_T_Purpose.DataBindings.Add("Text", _Bs_Rem, "T_purpose");
                    Cmb_Case_Purpose.DataBindings.Add("SelectedValue", _Bs_Rem, "Case_purpose_id");

                    Txt_Reciever.DataBindings.Add("Text", _Bs_Rem, "r_name");
                    Cmb_R_City.DataBindings.Add("SelectedValue", _Bs_Rem, "r_city_id");
                    Cmb_R_Nat.DataBindings.Add("SelectedValue", _Bs_Rem, "rnat_Id");
                    Cmb_phone_Code_R.DataBindings.Add("SelectedValue", _Bs_Rem, "Receiver_Code_phone");
                    Txt_R_Phone.DataBindings.Add("Text", _Bs_Rem, "R_phone");

                    Txtr_State.DataBindings.Add("Text", _Bs_Rem, "r_State");
                    Txtr_Suburb.DataBindings.Add("Text", _Bs_Rem, "r_Suburb");
                    Txtr_Street.DataBindings.Add("Text", _Bs_Rem, "r_Street");
                    Txtr_Post_Code.DataBindings.Add("Text", _Bs_Rem, "r_Post_Code");
                    Cmb_T_City.DataBindings.Add("SelectedValue", _Bs_Rem, "t_city_id");
                     bs_t_city = Convert.ToInt16(((DataRowView)_Bs_Rem.Current).Row["t_city_id"]);


                    Txtr_amount.DataBindings.Add("Text", _Bs_Rem, "r_amount");
                    Cmb_R_CUR_ID.DataBindings.Add("SelectedValue", _Bs_Rem, "R_Cur_Id");
                    Cmb_PR_Cur_Id.DataBindings.Add("SelectedValue", _Bs_Rem, "PR_cur_id");
                    Txt_Com_Amount.DataBindings.Add("Text", _Bs_Rem, "r_ocomm");
                    Cmb_Comm_Cur.DataBindings.Add("SelectedValue", _Bs_Rem, "r_CCur_Id");
                    //Txt_another_rec.DataBindings.Add("Text", _Bs_Rem, "R_EName");

                    cmb_job_receiver.DataBindings.Add("SelectedValue", _Bs_Rem, "r_job_ID");
                    Cbo_rem_path.DataBindings.Add("SelectedIndex", _Bs_Rem, "local_international");

                    Tex_Social_ID.DataBindings.Add("Text", _Bs_Rem, "S_Social_No");
                    resd_cmb.DataBindings.Add("SelectedValue", _Bs_Rem, "resd_flag");

                    Txt_S_details_job.DataBindings.Add("Text", _Bs_Rem, "S_details_job");
                    txt_notes.DataBindings.Add("Text", _Bs_Rem, "S_notes");

                    TxT_Agent_Real_Cur.DataBindings.Add("Text", _Bs_Rem, "cur_Aname_exch_agent");
                    TxT_Agent_Rate.DataBindings.Add("Text", _Bs_Rem, "exchange_agent_amount");
                    TxT_Agent_Real_Rem_Amount.DataBindings.Add("Text", _Bs_Rem, "real_agent_amount");
                    TxT_Agent_Real_Comm_Amount.DataBindings.Add("Text", _Bs_Rem, "agent_comm_amount");
                }
                catch
                { }
            //}
              //  s_notes = connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows[0]["s_notes"].ToString(); //((DataRowView)_Bs_Rem.Current).Row["s_notes"].ToString();
             send_flag = Convert.ToInt16(connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows[0]["send_rem_flag"].ToString());
            string[] Str = { "Perpare_Rem_Flag_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
            chang_scity = true;

            if (send_flag == 0  && Convert.ToInt16(((DataRowView)_Bs_Rem.Current).Row["Case_id"]) == 101)//في حال غير مرسلة فقط يتم تغيير عملة الدفع
            {
                Cmb_PR_Cur_Id.Enabled = true;

            }
            else
            { Cmb_PR_Cur_Id.Enabled = false; }

            if (connection.T_ID == 1)
            {
                Cmb_T_City.Enabled = true;
            }
            else
            { Cmb_T_City.Enabled = false; }
       
        }
        //-------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            #region Validation


            Int16 current_city =Convert.ToInt16(Cmb_T_City.SelectedValue);

            if (bs_t_city != current_city)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "عند تغير بلد او مدينة التسليم سيتم اعتماد عمولات المدينة السابقة هل انت موافق؟" : "When the delivery country or city changes, the previous commissions will be approved. Do you agree?", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (Dr == DialogResult.No)
                   {
                    return;  
                   }
              
            }



            if (Txt_Sender.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Sender Name" : "ادخل اسم المرسل", MyGeneral_Lib.LblCap);
                    return;
                }
            //if (Txt_another.Text == "")
            //    {
            //        MessageBox.Show(connection.Lang_id == 2 ? "Enter Another Sender Name" : "ادخل اسم المرسل الاخر", MyGeneral_Lib.LblCap);
            //        return;
            //    }
            if (Convert.ToInt16(Cmb_S_City.SelectedValue) == 0 || Cmb_S_City.SelectedValue == null)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the sender city" : " ادخل مدينة المرسل", MyGeneral_Lib.LblCap);
                    return;
                }
            if (Txts_street.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the street" : "ادخل زقاق المرسل", MyGeneral_Lib.LblCap);
                    return;
                }
            if (TxtS_State.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the State" : "ادخل محافظة المرسل", MyGeneral_Lib.LblCap);
                    return;
                }

            if (txtS_Suburb.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Sender Suburb" : "ادخل حـي المرسل", MyGeneral_Lib.LblCap);
                    return;
                }
            if (Txt_S_Phone.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter the phone No." : "ادخل هاتـف المرسل", MyGeneral_Lib.LblCap);
                    return;
                }

            if (Convert.ToInt16(Cmb_S_Doc_Type.SelectedValue) == 0 || Cmb_S_Doc_Type.SelectedValue == null)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter type of the Document" : "اختر نوع الوثيقـة المرسل", MyGeneral_Lib.LblCap);
                    return;
                }
            if (Txt_Doc_S_Issue.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the issue" : "ادخل محل اصدار الوثيقة المرسل", MyGeneral_Lib.LblCap);
                return;
            }
            if (Txt_S_Doc_No.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter Sender Name" : "ادخل رقم وثيقة المرسل", MyGeneral_Lib.LblCap);
                    return;
                }
            if (Cbo_rem_path.SelectedIndex == 0)
            {
                if (send_flag == 1)
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن تعديل الحوالة الاون لاين الى حوالة محلية" : " remittence could not be updated pleace check your remittance", MyGeneral_Lib.LblCap);
                    return;
                }
            }

            if (tex_reason.Text.Trim() == "") 
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Enter updating reason" : "ادخل سبب التعديل", MyGeneral_Lib.LblCap);
                    return;
                } 


                DateTime date_Now = Convert.ToDateTime(DateTime.Now.ToString("d"));
                DateTime Doc_S_date = Convert.ToDateTime(Txt_Doc_S_Date.Value.Date);

                if (date_Now == Doc_S_date)
                {
                    DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "تاريخ اصدار وثيقة المرسل مساوي الى التاريخ الحالي هل انت متأكد من تاريخ الاصدار" :
                    "The date of issuance of the document sender is equal to the current date. Are you sure of the release date ", MyGeneral_Lib.LblCap,
                     MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (Dr == DialogResult.No)
                    {
                        Txt_Doc_S_Date.Focus();
                        return;
                    }
                }
                DateTime date = Convert.ToDateTime(DateTime.Now.ToString("d"));
                DateTime text = Convert.ToDateTime(Txt_Doc_S_Exp.Value.Date.ToString("d"));
                if (text < date)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Document expired" : "وثيقةالمرسل منتهيةالصلاحية ", MyGeneral_Lib.LblCap);
                    return;
                }
                DateTime date1 = Convert.ToDateTime(DateTime.Now.ToString("d"));
                DateTime text1 = Convert.ToDateTime(Txt_Doc_S_Date.Value.Date.ToString("d"));
                if (text1 > date1)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Document Issuing date is higher than current date" : "تاريخ إصدار الوثيقة أعلى من التاريخ الحالي ", MyGeneral_Lib.LblCap);
                    return;
                }


                if (Txt_Doc_S_Exp.Checked == false)
                {
                    Txt_Doc_S_Exp.Format = DateTimePickerFormat.Custom;
                    S_doc_exp_null = Txt_Doc_S_Exp.CustomFormat = @format_null;
                }
            

                    DateTime from_date = Convert.ToDateTime(DateTime.Now.ToString("d"));
                    DateTime to = Convert.ToDateTime(Txt_Sbirth_Date.Value.Date);
                    try
                    {


                        TimeSpan difference = from_date - to;

                        if (Convert.ToDecimal(difference.TotalDays / 365.25) < 18)
                        {
                            MessageBox.Show(connection.Lang_id == 2 ? "Enter Sender Birth Date" : "عمرالمرسل اصغر من السن القانوني", MyGeneral_Lib.LblCap);
                            return;
                        }
                    }
                    catch 
                    {

                    }

                    if (Txt_S_Birth_Place.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the birth place" : "ادخل محل الولادة", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToInt16(cmb_s_nat.SelectedValue) == 0 || cmb_s_nat.SelectedValue == null)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the Nationalty" : "ادخل جنسيــة المرسل", MyGeneral_Lib.LblCap);
                        return;
                    }


                    if (Convert.ToInt16(cmb_job_sender.SelectedValue) == 0 || cmb_job_sender.SelectedValue == null)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the job" : "ادخل المهنة المرسل", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToInt16(cmb_Gender_id.SelectedValue) == 0 || cmb_Gender_id.SelectedValue == null)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the Gender" : " ادخل الجنـــس", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Txt_Soruce_money.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the money source" : "ادخل مصدر المال", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Txt_Relionship.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the Relationship of sender and receiver" : "ادخل علاقة المرسل بالمستلم", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Txt_T_Purpose.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the Purpose of remittences" : "ادخل الغرض من التحويل", MyGeneral_Lib.LblCap);
                        return;
                    }

                    //-----مستلم
                    if (Txt_Reciever.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter Reciever Name" : "ادخل اسم المستلم", MyGeneral_Lib.LblCap);
                        return;
                    }
                    //if (Txt_another_rec.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 2 ? "Enter Another Reciever Name" : "ادخل اسم المستلم الاخر", MyGeneral_Lib.LblCap);
                    //    return;
                    //}
                    //if (Txtr_State.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 2 ? "Enter the State" : "ادخل محافظة المستلم", MyGeneral_Lib.LblCap);
                    //    return;
                    //}
                    if (Convert.ToInt16(Cmb_R_City.SelectedValue) == 0 || Cmb_R_City.SelectedValue == null)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "Enter the Reciever city" : " ادخل مدينة المستلم", MyGeneral_Lib.LblCap);
                        return;
                    }
                    //if (Txtr_Suburb.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 2 ? "Enter Reciever Suburb" : "ادخل حـي المستلم", MyGeneral_Lib.LblCap);
                    //    return;
                    //}
                    //if (Convert.ToInt16(Cmb_R_Nat.SelectedValue) == 0 || Cmb_R_Nat.SelectedValue == null)
                    //{
                    //    MessageBox.Show(connection.Lang_id == 2 ? "Enter the Nationalty" : "ادخل جنسيــة المستلم", MyGeneral_Lib.LblCap);
                    //    return;
                    //}
                    //if (Txtr_Street.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 2 ? "Enter the street" : "ادخل زقاق المستلم", MyGeneral_Lib.LblCap);
                    //    return;
                    //}
                    //if (Txt_R_Phone.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 2 ? "Enter the phone No." : "ادخل رقم هاتف المستلم", MyGeneral_Lib.LblCap);
                    //    return;
                    //}

            #endregion
           string Sbirth_Date = Txt_Sbirth_Date.Value.ToString("yyyy/MM/dd");
            string doc_S_date_ida = Txt_Doc_S_Date.Value.ToString("yyyy/MM/dd");
            Txt_Doc_S_Exp_ValueChanged(null, null);
            
            string sen = Txt_Sender.Text.Trim();
            string rec = Txt_Reciever.Text.Trim();
            string mv_notes = " :ح-مر" + sen + "," + " :مس " + rec;


            //--------------------
            try
            {


                connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
                Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

                if (cond_bl_rem != 0)//في حال اختيار نوع البحث بلا من واجهة السيتنك فلا يبحث في قوائم البحث 
                {

                    DataTable Per_blacklist_tbl = new DataTable();
                    string[] Column4 = { "Per_AName", "Per_EName" };
                    string[] DType4 = { "System.String", "System.String" };

                    Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                    Per_blacklist_tbl.Rows.Clear();

                    Per_blacklist_tbl.Rows.Add(Txt_Sender.Text.Trim(), Txt_Sender.Text.Trim());
                    Per_blacklist_tbl.Rows.Add(Txt_Reciever.Text.Trim(), Txt_Reciever.Text.Trim());
                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                        connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)_Bs_Rem.Current).Row["snat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", 0);
                        connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)_Bs_Rem.Current).Row["s_coun_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", 0);
                        connection.SQLCMD.Parameters.AddWithValue("@s_job", ((DataRowView)_Bs_Rem.Current).Row["s_job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@r_job", 0);
                        connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", 0);
                        connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)_Bs_Rem.Current).Row["sFrm_doc_id"]);
                        

                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();

                    }
                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();

                    }
                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                    {
                        Rem_Blacklist_confirm_ok AddFrm = new Rem_Blacklist_confirm_ok();
                        AddFrm.ShowDialog(this);
                        if (Rem_Blacklist_confirm_ok.Back_btn == 1)
                        {

                             Get_Black_list();
                            MessageBox.Show(connection.Lang_id == 1 ? "تم رفض الحوالة" : "Rimmetance has been refused", MyGeneral_Lib.LblCap);
                            button1.Enabled = true;
                            this.Close();
                            return;

                            
                        }
                    }
                    else
                    {
                        Rem_Blacklist_confirm_ok.Back_btn = 0;
                        Rem_Blacklist_confirm_ok.Notes_BL = "";
                    }

                }
            }

            catch { }

            //try
            //{

            //    connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
            //    Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

            //    if (connection.SQLDS.Tables.Contains("per_rem_blacklist_tbl"))
            //    {
            //        connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
            //    }

            //    if (cond_bl_rem != 0)
            //    {

            //        if (Txt_Sender.Text.Trim() != "")
            //        {
            //            sender_name_BL = Txt_Sender.Text.Trim();
            //        }
            //        else
            //        { sender_name_BL = "" ; }
                   

            //        connection.SQLCS.Open();
            //        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
            //        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            //        connection.SQLCMD.Connection = connection.SQLCS;
            //        connection.SQLCMD.Parameters.AddWithValue("@search_name", sender_name_BL);
            //        IDataReader obj = connection.SQLCMD.ExecuteReader();
            //        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
            //        obj.Close();
            //        connection.SQLCS.Close();
            //        connection.SQLCMD.Parameters.Clear();
            //        connection.SQLCMD.Dispose();

            //        if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
            //        {

            //            DialogResult Dr = MessageBox.Show("الاسم موجود في قوائم المنع هل تريد الاستمرار ", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //            if (Dr == DialogResult.Yes)
            //            {
            //                Rem_Blacklist AddFrm = new Rem_Blacklist();
            //                AddFrm.ShowDialog(this);
            //            }


            //            if (Dr == DialogResult.No)
            //            {
            //              //  button1.Enabled = true;
            //                return;
            //            }
            //        }
            //    }
            //}

            //catch { }
        
            //if (s_notes == "")
            //{
            //    s_notes=txt_notes.Text.Trim();
            //}

            Int16 Agent_Real_Cur_id = Convert.ToInt16(((DataRowView)_Bs_Rem.Current).Row["cur_id_exch_agent"].ToString());
            string Agent_Real_Cur_Aname = Convert.ToString(((DataRowView)_Bs_Rem.Current).Row["cur_Aname_exch_agent"]);
            string Agent_Real_Cur_Ename = Convert.ToString(((DataRowView)_Bs_Rem.Current).Row["cur_Ename_exch_agent"]);
            string Gender_Ename = Convert.ToString(((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]);
            Int16 ger = Convert.ToInt16(cmb_Gender_id.SelectedIndex);
            ArrayList ItemList1 = new ArrayList();
            ItemList1.Insert(0, Txt_Sender.Text.Trim()); //@As_name    0
            ItemList1.Insert(1, Txt_S_Phone.Text.Trim());//@s_phone		1
            ItemList1.Insert(2, TxtS_State.Text.Trim() + '/' + txtS_Suburb.Text.Trim() + '/' + Txts_street.Text.Trim() + '/' + TxtS_Post_Code.Text.Trim());  //@s_address  3
            ItemList1.Insert(3, Txt_S_Doc_No.Text.Trim()); //@s_doc_no 4
            ItemList1.Insert(4, doc_S_date_ida); //@s_doc_ida  5
            ItemList1.Insert(5, Doc_S_Exp); //@s_doc_ida  6
            ItemList1.Insert(6, Txt_Doc_S_Issue.Text.Trim()); //@s_doc_issue   7
            ItemList1.Insert(7, ((DataRowView)binding_sfmd.Current).Row["fmd_id"]);//----@sfrm_doc_id  8
            ItemList1.Insert(8, ((DataRowView)binding_cb_snat.Current).Row["Nat_id"]);//-----@snat_id 9	
            ItemList1.Insert(9, Txt_Reciever.Text.Trim()); //@Ar_name  10
            ItemList1.Insert(10, Txt_R_Phone.Text.Trim()); //@r_phone   11
            ItemList1.Insert(11, ((DataRowView)binding_cbo_rcity_con.Current).Row["cit_id"]);//----@r_city_id   12   
            ItemList1.Insert(12, ((DataRowView)binding_tcity_id.Current).Row["Cit_ID"]);//-----@T_city_id     13
            ItemList1.Insert(13, Txtr_State.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());//@r_address  14
            ItemList1.Insert(14, ((DataRowView)binding_cb_rnat.Current).Row["nat_id"]);//----@rnat_id   15
            ItemList1.Insert(15, Txt_T_Purpose.Text.Trim()); //@t_purpose   16
            ItemList1.Insert(16, mv_notes); //@v_notes	 17 // Txt_Notes.Text.Trim()
            ItemList1.Insert(17, Sbirth_Date); // @sbirth_date  18
            ItemList1.Insert(18, connection.user_id); //@user_id   19
            //ItemList1.Insert(19, connection.Cust_online_Id);//cust_id_online   	 20
            ItemList1.Insert(19, Txt_S_Birth_Place.Text.Trim()); //@sbirth_place		 21
            ItemList1.Insert(20, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_ID"]); //@sa_city_id	 22		 
            ItemList1.Insert(21, ""); //@Param_Result	  23		 
            ItemList1.Insert(22, Convert.ToInt32(((DataRowView)binding_cmb_job_sender.Current).Row["Job_ID"])); 
            ItemList1.Insert(23, connection.Lang_id); //@lang_id	 25
            ItemList1.Insert(24, Txt_Soruce_money.Text.Trim());  //@Soruce_money 26
            ItemList1.Insert(25, Txt_Relionship.Text.Trim()); //@Relionship	27			 
            ItemList1.Insert(26, Convert.ToInt32(((DataRowView)binding_cmb_job_receiver.Current).Row["Job_ID"]));
            ItemList1.Insert(27, Cmb_Case_Purpose.SelectedValue);  //@Case_purpose_id	29	
            ItemList1.Insert(28, Txts_street.Text.Trim());//---@S_Street
            ItemList1.Insert(29, txtS_Suburb.Text.Trim()); //--@S_Suburb
            ItemList1.Insert(30, TxtS_Post_Code.Text.Trim());//----@S_Post_Code
            ItemList1.Insert(31, TxtS_State.Text.Trim());//----@S_State
            ItemList1.Insert(32, ((DataRowView)binding_sfmd.Current).Row["Fmd_AName"]);//---@sFRM_ADOC_NA   
            ItemList1.Insert(33, ((DataRowView)binding_sfmd.Current).Row["Fmd_eName"]);//---@sFRM_eDOC_NA   
            ItemList1.Insert(34, ((DataRowView)binding_cb_snat.Current).Row["Nat_AName"]);//---@@s_A_NAT_NAME   
            ItemList1.Insert(35, ((DataRowView)binding_cb_snat.Current).Row["Nat_eName"]);//---@@s_e_NAT_NAME   
            ItemList1.Insert(36, ((DataRowView)binding_cbo_rcity_con.Current).Row["Cit_AName"]);//---@r_ACITY_NAME 
            ItemList1.Insert(37, ((DataRowView)binding_cbo_rcity_con.Current).Row["Cit_eName"]);//---@r_ECITY_NAME
            ItemList1.Insert(38, ((DataRowView)binding_cbo_rcity_con.Current).Row["Con_AName"]);//---@r_ACOUN_NAME
            ItemList1.Insert(39, ((DataRowView)binding_cbo_rcity_con.Current).Row["Con_eName"]);//---@r_eCOUN_NAME
            ItemList1.Insert(40, Txtr_Street.Text.Trim());//---@r_Street
            ItemList1.Insert(41, Txtr_Suburb.Text.Trim());//---@r_Suburb
            ItemList1.Insert(42, Txtr_Post_Code.Text.Trim());//---@@r_Post_Code
            ItemList1.Insert(43, Txtr_State.Text.Trim());//---@r_State
            ItemList1.Insert(44, ((DataRowView)binding_cb_rnat.Current).Row["Nat_AName"]);//---@@r_A_NAT_NAME
            ItemList1.Insert(45, ((DataRowView)binding_cb_rnat.Current).Row["Nat_EName"]);//---@@r_E_NAT_NAME
            ItemList1.Insert(46, ((DataRowView)binding_tcity_id.Current).Row["acity_name"]);//-----@@t_ACITY_NAME    
            ItemList1.Insert(47, ((DataRowView)binding_tcity_id.Current).Row["ecity_name"]);//-----@@t_eCITY_NAME    
            ItemList1.Insert(48, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_AName"]);//-----@@@Sa_ACITY_NAME    
            ItemList1.Insert(49, ((DataRowView)binding_cbo_scity_con.Current).Row["Cit_eName"]);//-----@@@Sa_ECITY_NAME    
            ItemList1.Insert(50, ((DataRowView)binding_cbo_scity_con.Current).Row["Con_AName"]);//-----@Sa_ACOUN_NAME    
            ItemList1.Insert(51, ((DataRowView)binding_cbo_scity_con.Current).Row["Con_eName"]);//-----@Sa_ECOUN_NAME    
            ItemList1.Insert(52, ((DataRowView)binding_tcity_id.Current).Row["Con_AName"]);//-----@t_ACOUN_NAME   binding_tcity_id
            ItemList1.Insert(53, ((DataRowView)binding_tcity_id.Current).Row["Con_EName"]);//-----@t_ECOUN_NAME   binding_tcity_id
            ItemList1.Insert(54, ((DataRowView)binding_Cmb_Case_Purpose.Current).Row["Case_purpose_Aname"]);//----@@Case_purpose_Aname  binding_Cmb_Case_Purpose
            ItemList1.Insert(55, ((DataRowView)binding_Cmb_Case_Purpose.Current).Row["Case_purpose_ename"]);//----@@Case_purpose_ename  binding_Cmb_Case_Purpose
            ItemList1.Insert(56, connection.T_ID); //@T_Id	
            ItemList1.Insert(57, "");//@Es_name
            ItemList1.Insert(58,"");//@Er_name
            ItemList1.Insert(59, Txt_mail.Text.Trim());// //@per_Email
            ItemList1.Insert(60, txt_Mother_name.Text.Trim());  //@per_Mother_name
            ItemList1.Insert(61, Convert.ToString(Cmb_Code_phone_S.SelectedValue));//----@Sender_Code_phone
            ItemList1.Insert(62, Convert.ToString(Cmb_phone_Code_R.SelectedValue));//----@@Receiver_Code_phone
            //ItemList1.Insert(64, Convert.ToInt16(Cbo_rem_path.SelectedIndex));//----@@//@loc_Inter_flag
            ItemList1.Insert(63, ((DataRowView)_Bs_Rem.Current).Row["rem_no"]);//----@@//@rem_no
            ItemList1.Insert(64, tex_reason.Text.Trim()); //@note   
            ItemList1.Insert(65, ((DataRowView)binding_Gender_id.Current).Row["Gender_Aname"]); //@@S_Gender_Aname
            ItemList1.Insert(66, ((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]); //@@S_Gender_Ename
            ItemList1.Insert(67, ""); //@Param_Rem_no
            ItemList1.Insert(68, Cmb_PR_Cur_Id.SelectedValue); //Pr_cur_id
            ItemList1.Insert(69, Convert.ToInt16(resd_cmb.SelectedValue));
            ItemList1.Insert(70, Tex_Social_ID.Text.Trim());
            ItemList1.Insert(71, Txt_S_details_job.Text.Trim() != "" ? Txt_S_details_job.Text : "");
            ItemList1.Insert(72, Rem_Blacklist_confirm_ok.Notes_BL);
            ItemList1.Insert(73, Convert.ToDecimal(TxT_Agent_Real_Rem_Amount.Text));
            ItemList1.Insert(74, Convert.ToDecimal(TxT_Agent_Rate.Text));
            ItemList1.Insert(75, Agent_Real_Cur_id);
            ItemList1.Insert(76, Agent_Real_Cur_Aname);
            ItemList1.Insert(77, Agent_Real_Cur_Ename);
            ItemList1.Insert(78, Convert.ToDecimal(TxT_Agent_Real_Comm_Amount.Text));
       
            MyGeneral_Lib.Copytocliptext("update_rem_out", ItemList1);
            connection.scalar("update_rem_out", ItemList1);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
            }
      
           
            if (connection.SQLCMD.Parameters["@Param_Rem_no"].Value.ToString() != "")
            {
                rem_no = connection.SQLCMD.Parameters["@Param_Rem_no"].Value.ToString();
                print_Rpt_Pdf_NEW();
              
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
        //-------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Bs_Sub_Cust.DataSource = new BindingSource();
        }
        //-------------------------
        private void clear_all_txt()
        {
          

            Txt_Sender.Text = "";
         //   Txt_another.Text = "";
            Txtr_Suburb.Text = ""; Txt_R_Phone.Text = ""; cmb_job_receiver.SelectedIndex = 0;
            Txtr_Street.Text = ""; Txtr_State.Text = ""; Txtr_Post_Code.Text = "";
           // Txt_another_rec.Text = string.Empty;
            Cmb_R_Nat.SelectedIndex = 0;
            Cmb_phone_Code_R.SelectedIndex = 0;
            Cmb_R_City.SelectedIndex = 0;
            Txt_Reciever.Text = "";
           // Txt_another_rec.Text = "";
            Txt_S_Phone.Text = "";
            cmb_job_sender.SelectedIndex = 0;
            txtS_Suburb.Text = ""; TxtS_State.Text = ""; Txts_street.Text = ""; TxtS_Post_Code.Text = "";
            Txt_Doc_S_Date.Text = "";
            Txt_Doc_S_Exp.Text = "";
            Txt_Doc_S_Issue.Text = "";
            Txt_S_Birth_Place.Text = ""; Txt_Sbirth_Date.Text = ""; Txt_T_Purpose.Text = "";
            Txt_S_Doc_No.Text = ""; Txt_Soruce_money.Text = "";
            Txt_Relionship.Text = ""; Txt_mail.Text = ""; txt_Mother_name.Text = "";
            Txt_T_Purpose.Text = "";
            cmb_Gender_id.SelectedIndex = 0;
            Cmb_phone_Code_R.SelectedIndex = 0;
            Cmb_Case_Purpose.SelectedIndex = 0;
            Cmb_S_City.SelectedIndex = 0;
            Cmb_Code_phone_S.SelectedIndex = 0;
            Cmb_S_Doc_Type.SelectedIndex = 0;
            cmb_s_nat.SelectedIndex = 0;
            Txtr_amount.ResetText();
            Txt_Com_Amount.ResetText();
            Tex_Social_ID.Text = "";
            resd_cmb.SelectedValue = 0;
            Txt_S_details_job.Text = "";
            txt_notes.Text = "";

            TxT_Agent_Real_Cur.Text = "";
            TxT_Agent_Rate.Text = "";
            TxT_Agent_Real_Rem_Amount.Text = "";
            TxT_Agent_Real_Comm_Amount.Text = "";

                 
       
        }

        private void Cmbo_Oper_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change)
            {
                clear_all_txt();
            }
            change = true;
        }

        private void Txt_S_name_TextChanged(object sender, EventArgs e)
        {
            clear_all_txt();
        }

        private void CboCust_Id_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Txt_Rem_no_TextChanged(object sender, EventArgs e)
        {
            clear_all_txt();
        }

        private void Txt_R_name_TextChanged(object sender, EventArgs e)
        {
            clear_all_txt();
        }

        private void Cmb_R_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_rcity)
            {
                Cmb_phone_Code_R.DataBindings.Clear();
                Cmb_phone_Code_R.DataBindings.Add("SelectedValue", binding_cbo_rcity_con, "COUN_K_PH");

            }

        }

        private void Cmb_S_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_scity)
            {
                Cmb_Code_phone_S.DataBindings.Clear();
                Cmb_Code_phone_S.DataBindings.Add("SelectedValue", binding_cbo_scity_con, "COUN_K_PH");
            }
            
        }

        private void Txt_Doc_S_Exp_ValueChanged(object sender, EventArgs e)
        {
            if (Txt_Doc_S_Exp.Checked == false)
            {
                Doc_S_Exp = "";
            }
            else
            {
                Doc_S_Exp = Txt_Doc_S_Exp.Value.ToString("yyyy/MM/dd");
            }
        }

        private void CboCust_Id_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            _Bs_Rem.DataSource = new BindingSource();
        }
        //--------------------------------------------------------------
        private void print_Rpt_Pdf_NEW()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int int_nrecdate = 0;
            string Frm_id = "";
            User = TxtUser.Text;
            string phone = "";
            int procker = 0;
            string prockr_name = "";
            string procker_ename = "";
            int Paid = 0;
            string Paid_name = "";
            string Paid_ename = "";
            string local_Cur1 = "";
            string local_ECur1 = "";
            string forgin_Cur1 = "";
            string forgin_ECur1 = "";
            string Com_ToWord = "";
            string Com_ToEWord = "";
            double com_amount = 0;
            string com_Cur_code = "";
            string term = "";
            byte chk_s_ocomm = 0;
            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();

            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);


            string SqlTxt = "Exec Report_Rem_Information_new" + "'" + rem_no + "'," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Rem_Info");

           
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
                }
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]) != 0)
                {
                    Paid = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]);
                    Paid_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_aname"].ToString();
                    Paid_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_ename"].ToString();
                }

                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_Aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_ename"].ToString();
                }


                local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
                local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

                forgin_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Cur_Code"].ToString();
                forgin_Cur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ACUR_NAME"].ToString();
                forgin_ECur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ECUR_NAME"].ToString();

                s_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_Ejob"].ToString();
                r_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_Ejob"].ToString();
                S_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["S_Social_No"].ToString();
                R_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Social_No"].ToString();




                Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                    {
                        com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]);
                        com_Cur_code = connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_code"].ToString();
                        ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                        Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                    }
                }
                else// عملة محلية
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                    if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                    {
                        com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]);
                        com_Cur_code = local_Cur;
                        ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                        Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                        Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                    }
                }

                Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();


                term = TxtTerm_Name.Text;


                //if
                //  ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) == 0) ||
                //   (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) == 0))
                //{
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //    {
                //        Oper_Id = 3;

                //    }
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //    {
                //        Oper_Id = 6;
                //    }
                //}

                //if
                //   ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0) ||
                //    (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0))
                //{
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //    {
                //        Oper_Id = 27;

                //    }
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //    {
                //        Oper_Id = 33;
                //    }
                //}
                decimal txt_comm_amount = 0;

                chk_s_ocomm = 0;
                txt_comm_amount = 0;
                Oper_Id = Convert.ToInt16(connection.SQLDS.Tables["Rem_Info"].Rows[0]["oper_id"]);

                try
                {

                    Add_Rem_cash_daily_rpt_1 ObjRpt = new Add_Rem_cash_daily_rpt_1();
                    Add_Rem_cash_daily_rpt_eng_1 ObjRptEng = new Add_Rem_cash_daily_rpt_eng_1();


                    DataTable Dt_Param = CustomControls.CustomParam_Dt();
                    Dt_Param.Rows.Add("_Date", _Date);
                    Dt_Param.Rows.Add("local_Cur", local_Cur);
                    Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                    Dt_Param.Rows.Add("Vo_No", Vo_No);
                    Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                    Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                    Dt_Param.Rows.Add("User", User);
                    Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                    Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                    Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                    Dt_Param.Rows.Add("term", term);
                    Dt_Param.Rows.Add("Frm_id", Frm_id);
                    Dt_Param.Rows.Add("phone", phone);
                    Dt_Param.Rows.Add("com_cur", com_Cur_code);
                    Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                    Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                    Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                    Dt_Param.Rows.Add("procker", procker);
                    Dt_Param.Rows.Add("procker_ename", procker_ename);
                    Dt_Param.Rows.Add("prockr_name", prockr_name);
                    Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                    Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                    Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                    Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                    Dt_Param.Rows.Add("Anote_report", Anote_report);
                    Dt_Param.Rows.Add("Enote_report", Enote_report);
                    Dt_Param.Rows.Add("chk_s_ocomm", chk_s_ocomm);
                    Dt_Param.Rows.Add("txt_comm_amount", txt_comm_amount);
                    Dt_Param.Rows.Add("Paid", Paid);
                    Dt_Param.Rows.Add("Paid_ename", Paid_ename);
                    Dt_Param.Rows.Add("Paid_name", Paid_name);


                    connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
                    connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);
                }
                catch
                { }
            
        }
        //---------------------------------------
        private void Get_Black_list()
        {

            Int64 refuse_trans_flag = Convert.ToInt64(((DataRowView)_Bs_Rem.Current).Row["transfer_flag"]);  //new
            if (refuse_trans_flag == 0)
            {
                try
                {

                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[Refuse_confirm_All]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)_Bs_Rem.Current).Row["rem_no"]);
                    connection.SQLCMD.Parameters.AddWithValue("@case_id", ((DataRowView)_Bs_Rem.Current).Row["case_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@note_prog", "Update Rem.");
                    connection.SQLCMD.Parameters.AddWithValue("@oper_id", ((DataRowView)_Bs_Rem.Current).Row["oper_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@vo_no", ((DataRowView)_Bs_Rem.Current).Row["vo_no"]);
                    connection.SQLCMD.Parameters.AddWithValue("@per_id", ((DataRowView)_Bs_Rem.Current).Row["S_per_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", ((DataRowView)_Bs_Rem.Current).Row["r_cur_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount", ((DataRowView)_Bs_Rem.Current).Row["r_amount"]);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Name", ((DataRowView)_Bs_Rem.Current).Row["s_name"]);
                    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 2);
                    connection.SQLCMD.Parameters.AddWithValue("@transfer_flag", ((DataRowView)_Bs_Rem.Current).Row["transfer_flag"]);
                    connection.SQLCMD.Parameters.AddWithValue("@s_city_id", ((DataRowView)_Bs_Rem.Current).Row["s_city_id"]);
                    MyGeneral_Lib.Copytocliptext("Refuse_confirm_All", connection.SQLCMD);
                    IDataReader obj = connection.SQLCMD.ExecuteReader();

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }
            }

            if (refuse_trans_flag == 1)
            {
                try
                {

                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[oto_Refuse_confirm_All]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)_Bs_Rem.Current).Row["rem_no"]);
                    connection.SQLCMD.Parameters.AddWithValue("@case_id", ((DataRowView)_Bs_Rem.Current).Row["case_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                    connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                    connection.SQLCMD.Parameters.AddWithValue("@note_prog", "Update Rem.");
                    connection.SQLCMD.Parameters.AddWithValue("@oper_id", ((DataRowView)_Bs_Rem.Current).Row["oper_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@vo_no", ((DataRowView)_Bs_Rem.Current).Row["vo_no"]);
                    connection.SQLCMD.Parameters.AddWithValue("@per_id", ((DataRowView)_Bs_Rem.Current).Row["S_per_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", ((DataRowView)_Bs_Rem.Current).Row["r_cur_id"]);
                    connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount", ((DataRowView)_Bs_Rem.Current).Row["r_amount"]);
                    connection.SQLCMD.Parameters.AddWithValue("@Per_Name", ((DataRowView)_Bs_Rem.Current).Row["s_name"]);
                    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 2);
                    connection.SQLCMD.Parameters.AddWithValue("@transfer_flag", ((DataRowView)_Bs_Rem.Current).Row["transfer_flag"]);
                    connection.SQLCMD.Parameters.AddWithValue("@s_city_id", ((DataRowView)_Bs_Rem.Current).Row["s_city_id"]);
                    MyGeneral_Lib.Copytocliptext("Refuse_confirm_All", connection.SQLCMD);
                    IDataReader obj = connection.SQLCMD.ExecuteReader();

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }

                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                }
            }

        }

        private void cbo_order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Order_type = Convert.ToInt32(cbo_order_type.SelectedIndex);
            try
            {
                if (cbo_order_type.SelectedIndex == 0) //فروع
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select("Order_type = " + Order_type).CopyToDataTable();


                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                    }
                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }

                }

                else
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select("Order_type = " + Order_type).CopyToDataTable();

                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                    }

                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }
                }
            }
            catch { Grd_Sub_Cust.DataSource = new BindingSource(); }

            Txt_Sub_Cust.Text = "";
        }

        private void Txt_Sub_Cust_TextChanged(object sender, EventArgs e)
        {
            Get_details_Info();
        }

        private void Get_details_Info()
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);

                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "(Acust_name like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " Sub_Cust_ID > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }

        //private void Cmb_T_City_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //     DialogResult Dr = MessageBox.Show(connection.Lang_id==1?"عند تغير بلد او مدينة التسليم سيتم اعتماد عمولات المدينة السابقة هل انت موافق؟":"When the delivery country or city changes, the previous commissions will be approved. Do you agree?", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
        //        if (Dr == DialogResult.No)
        //        {
        //           return;  
        //        }
        //}

    }
}