﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Integration_Accounting_Sys.Branch_Report;
using System.Globalization;

namespace Integration_Accounting_Sys
{
    public partial class Update_Incoming_Rem : Form
    {

        string name_city_con = "";
        int Mcity_con_online_id = 0;
        BindingSource rec_rem = new BindingSource();
        BindingSource binding_tcity = new BindingSource();
        BindingSource binding_cbo_rcity = new BindingSource();
        BindingSource binding_Rfmd_id = new BindingSource();
        BindingSource binding_cb_Rnat_id = new BindingSource();
        BindingSource binding_Gender_id = new BindingSource();
        BindingSource binding_cmb_job_receiver = new BindingSource();
        bool Chk_change = false;
        bool chang_rcity = false;
        string @format = "dd/MM/yyyy";
        Int32 D_Ver_flag = 0;
        Int32 S_Ver_flag = 0;

        public Update_Incoming_Rem(string city_con_online, int city_con_online_id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            name_city_con = city_con_online;
            Mcity_con_online_id = city_con_online_id;
            Grdrec_rem.AutoGenerateColumns = false;

        }

        private void Update_IN_Rem_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {

                Column2.DataPropertyName = "Re_CUR_NAME";
                Column4.DataPropertyName = "PR_ECUR_NAME";
                Column5.DataPropertyName = "ECASE_NA";
            }
            //------------

            Txt_Doc_r_Exp.Format = DateTimePickerFormat.Custom;
            Txt_Doc_r_Exp.CustomFormat = @format;


            Txt_rbirth_Date.Format = DateTimePickerFormat.Custom;
            Txt_rbirth_Date.CustomFormat = @format;

            Txt_Doc_r_Date.Format = DateTimePickerFormat.Custom;
            Txt_Doc_r_Date.CustomFormat = @format;
            //------------

            binding_tcity.DataSource = connection.SQLDS.Tables["Get_info_Tbl"].Select("CITY_ID <> 0").CopyToDataTable();
            Cbo_city.DataSource = binding_tcity;
            Cbo_city.ValueMember = "CITY_ID";
            Cbo_city.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";

            //------------------------------------------------
            connection.SqlExec("Full_information_Web_inRem " + connection.Lang_id + "," + 30, "Full_information_tab_inRem");

            if (connection.SQLDS.Tables["Full_information_tab_inRem"].Rows.Count > 0)
            {
                chang_rcity = false;

                binding_cbo_rcity.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem"].Select(" Cit_ID <> 0 ").CopyToDataTable(); ;
                Cmb_R_City.DataSource = binding_cbo_rcity;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";

                Cmb_phone_Code_R.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem"];
                Cmb_phone_Code_R.ValueMember = "COUN_K_PH";
                Cmb_phone_Code_R.DisplayMember = "COUN_K_PH";
                chang_rcity = true;
                Cmb_R_City_SelectedIndexChanged(null, null);
            }
            if (connection.SQLDS.Tables["Full_information_tab_inRem1"].Rows.Count > 0)
            {
                binding_Rfmd_id.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem1"];
                Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_r_Doc_Type.DisplayMember = connection.Lang_id == 1 ? "Fmd_AName" : "Fmd_EName";
            }
            if (connection.SQLDS.Tables["Full_information_tab_inRem2"].Rows.Count > 0)
            {

                binding_cb_Rnat_id.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem2"].Select(" Nat_ID <> 0 ").CopyToDataTable();
                Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = connection.Lang_id == 1 ? "Nat_AName" : "Nat_EName";
            }

            if (connection.SQLDS.Tables["Full_information_tab_inRem3"].Rows.Count > 0)
            {
                binding_Gender_id.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem3"].Select(" Gender_id <> 0 ").CopyToDataTable();
                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";

            }

            if (connection.SQLDS.Tables["Full_information_tab_inRem6"].Rows.Count > 0)
            {
                binding_cmb_job_receiver.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem6"];
                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";
            }


            Chk_change = false;
            rec_rem.DataSource = connection.SQLDS.Tables["Delivery_Tbl"];
            Grdrec_rem.DataSource = rec_rem;
            Chk_change = true;
            Grdrec_rem_SelectionChanged(null, null);
        }

        //------------------------------
        private void Grdrec_rem_SelectionChanged(object sender, EventArgs e)
        {
            if (Chk_change)
            {
                //cmb_Gender_id.DataBindings.Clear();
                Txtr_Suburb.DataBindings.Clear();
                Txt_R_Phone.DataBindings.Clear();
                cmb_job_receiver.DataBindings.Clear();
                Txtr_Street.DataBindings.Clear();
                Txtr_State.DataBindings.Clear();
                Txtr_Post_Code.DataBindings.Clear();
                Txt_rbirth_Date.DataBindings.Clear();
                Txt_r_Doc_No.DataBindings.Clear();
                Txt_Doc_r_Issue.DataBindings.Clear();
                Txt_Doc_r_Date.DataBindings.Clear();
                Txt_mail.DataBindings.Clear();
                txt_Mother_name.DataBindings.Clear();
                txt_rbirth_place.DataBindings.Clear();
                Cmb_R_City.DataBindings.Clear();
                Cmb_R_Nat.DataBindings.Clear();
                Cmb_r_Doc_Type.DataBindings.Clear();
                cmb_Gender_id.DataBindings.Clear();
                Txt_R_T_Purpose.DataBindings.Clear();
                Txt_notes.DataBindings.Clear();
                Txt_Real_Paid_Name.DataBindings.Clear();
                Txt_R_details_job.DataBindings.Clear();

                cmb_Gender_id.DataBindings.Clear();
                cmb_Gender_id.SelectedValue = 0;

                cmb_Gender_id.Text = (Convert.ToString(connection.Lang_id == 1 ? (((DataRowView)rec_rem.Current).Row["R_Gender_Aname"]) : (((DataRowView)rec_rem.Current).Row["R_Gender_Ename"])));

                if (cmb_Gender_id.Text == "" || cmb_Gender_id.Text =="بلا")
                {
                    cmb_Gender_id.SelectedValue = 1;
                }

               // cmb_Gender_id.DataBindings.Add("Text", rec_rem,connection.Lang_id == 1 ? "R_Gender_Aname" : "R_Gender_Ename");
                Txt_Reciever.Text = ((DataRowView)rec_rem.Current).Row["r_name"].ToString();
                Txtr_Post_Code.Text = ((DataRowView)rec_rem.Current).Row["r_Post_Code"].ToString();
                Txtr_State.Text = ((DataRowView)rec_rem.Current).Row["r_State"].ToString();
                Txtr_Street.Text = ((DataRowView)rec_rem.Current).Row["r_Street"].ToString();
                Txtr_Suburb.Text = ((DataRowView)rec_rem.Current).Row["r_Suburb"].ToString();
                Txt_rbirth_Date.Text = ((DataRowView)rec_rem.Current).Row["rbirth_date"].ToString();
                Txt_R_Phone.Text = ((DataRowView)rec_rem.Current).Row["r_phone"].ToString();
                Txt_r_Doc_No.Text = ((DataRowView)rec_rem.Current).Row["r_doc_no"].ToString();
                Txt_Doc_r_Issue.Text = ((DataRowView)rec_rem.Current).Row["r_doc_issue"].ToString();
                Txt_Doc_r_Date.Text = ((DataRowView)rec_rem.Current).Row["r_doc_ida"].ToString();
                Txt_Doc_r_Exp.Text = ((DataRowView)rec_rem.Current).Row["r_doc_eda"].ToString();
                Txt_mail.Text = ((DataRowView)rec_rem.Current).Row["r_Email"].ToString();
                txt_Mother_name.Text = ((DataRowView)rec_rem.Current).Row["R_Mother_name"].ToString();
                txt_rbirth_place.Text = ((DataRowView)rec_rem.Current).Row["TBirth_Place"].ToString();
                Cmb_r_Doc_Type.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["rFrm_doc_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["rFrm_doc_id"]);
                Cmb_R_City.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["r_city_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["r_city_id"]);
                Cmb_R_Nat.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["rnat_Id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["rnat_Id"]);
                Txt_Sender.Text = ((DataRowView)rec_rem.Current).Row["s_name"].ToString();

                Cbo_city.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["t_city_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["t_city_id"]);

                Txt_Relionship.Text = ((DataRowView)rec_rem.Current).Row["r_Relation"].ToString();
                Txt_R_T_Purpose.Text = ((DataRowView)rec_rem.Current).Row["R_T_purpose"].ToString();
                Txt_notes.Text = ((DataRowView)rec_rem.Current).Row["r_notes"].ToString();
                Txt_Real_Paid_Name.Text = ((DataRowView)rec_rem.Current).Row["Real_Paid_Name"].ToString();
                Txt_R_details_job.Text = ((DataRowView)rec_rem.Current).Row["r_details_job"].ToString();


                D_Ver_flag = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["D_Ver_flag"]);
                S_Ver_flag = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["S_Ver_flag"]);

                if (D_Ver_flag == 0 && S_Ver_flag == 0 )
                {

                    Txt_S_D_Ver_Flag.Text = connection.Lang_id == 2 ? "The remittance certain from both sides" : "الحوالة مؤكدة من الطرفين";

                }
                  if (D_Ver_flag == 0 && S_Ver_flag == -1 )
                {

                    Txt_S_D_Ver_Flag.Text = connection.Lang_id == 2 ? "The remittance  uncertain from the source of the sender" : "الحوالة غير مؤكدة من مصدر المرسل";
                }
                  if (D_Ver_flag == -1 && S_Ver_flag == 0)
                  {

                      Txt_S_D_Ver_Flag.Text = connection.Lang_id == 2 ? "The remittance uncertain from the source of receipt" : "الحوالة غير مؤكدة من مصدر الاستلام";
                  }

                  if (D_Ver_flag == -1 && S_Ver_flag == -1)
                  {

                      Txt_S_D_Ver_Flag.Text = connection.Lang_id == 2 ? "The remittance uncertain from both sides" : "الحوالة غير مؤكدة من الطرفين";
                  }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string rbirth_Date = Txt_rbirth_Date.Value.ToString("yyyy/MM/dd");
            string doc_r_date_ida = Txt_Doc_r_Date.Value.ToString("yyyy/MM/dd");
            string Doc_r_Exp = Txt_Doc_r_Exp.Value.ToString("yyyy/MM/dd");


            string current_date = DateTime.Now.ToString("yyyy/MM/dd");

            if (rbirth_Date == current_date)
            {

                rbirth_Date = "";
            }

            if (doc_r_date_ida == current_date)
            {

                doc_r_date_ida = "";
            }

            if (Doc_r_Exp == current_date)
            {

                Doc_r_Exp = "";
            }

            if (Cbo_city.SelectedIndex < 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Select Financial Delivery City" : " اختر مدينة التسليم المالية", MyGeneral_Lib.LblCap);
                return;
            
            }


            connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)rec_rem.Current).Row["rem_no"]);
            connection.SQLCMD.Parameters.AddWithValue("@T_city_id", Cbo_city.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Cust_online_Id", connection.Cust_online_Id);
            connection.SQLCMD.Parameters.AddWithValue("@r_gender_id", cmb_Gender_id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", Cmb_R_Nat.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@R_Mother_name", txt_Mother_name.Text.Trim() != "" ? txt_Mother_name.Text :"");
            connection.SQLCMD.Parameters.AddWithValue("@TBirth_Place", txt_rbirth_place.Text.Trim() != "" ? txt_rbirth_place.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_phone", Txt_R_Phone.Text.Trim() != "" ? Txt_R_Phone.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@COUN_K_PH", Convert.ToString(Cmb_phone_Code_R.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@r_city_id", Cmb_R_City.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", Txt_r_Doc_No.Text.Trim() != "" ? Txt_r_Doc_No.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", Cmb_r_Doc_Type.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@r_doc_issue", Txt_Doc_r_Issue.Text.Trim() != "" ? Txt_Doc_r_Issue.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_Street", Txtr_Street.Text.Trim() != "" ? Txtr_Street.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_Post_Code", Txtr_Post_Code.Text.Trim() != "" ? Txtr_Post_Code.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_Email", Txt_mail.Text.Trim() != "" ? Txt_mail.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_Suburb", Txtr_Suburb.Text.Trim() != "" ? Txtr_Suburb.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_State", Txtr_State.Text.Trim() != "" ? Txtr_State.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_job", cmb_job_receiver.Text);
            connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", rbirth_Date);
            connection.SQLCMD.Parameters.AddWithValue("@r_doc_ida", doc_r_date_ida);
            connection.SQLCMD.Parameters.AddWithValue("@r_doc_eda", Doc_r_Exp);
            connection.SQLCMD.Parameters.AddWithValue("@r_Relation", Txt_Relionship.Text.Trim() != "" ? Txt_Relionship.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@R_details_job", Txt_R_details_job.Text.Trim() != "" ? Txt_R_details_job.Text : "");//
            connection.SQLCMD.Parameters.AddWithValue("@R_T_purpose", Txt_R_T_Purpose.Text.Trim() != "" ? Txt_R_T_Purpose.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@r_notes", Txt_notes.Text.Trim() != "" ? Txt_notes.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@Real_Paid_Name", Txt_Real_Paid_Name.Text.Trim() != "" ? Txt_Real_Paid_Name.Text : "");
            connection.SQLCMD.Parameters.AddWithValue("@user_name", connection.User_Name);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("update_Incoming_Rem", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
            connection.SQLCMD.Parameters.Clear();
            this.Close();

        }

        private void Cmb_R_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_rcity)
            {
                Cmb_phone_Code_R.DataBindings.Clear();
                Cmb_phone_Code_R.DataBindings.Add("SelectedValue", binding_cbo_rcity, "COUN_K_PH");
            }
        }

        private void Update_Incoming_Rem_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_rcity = false;
           
            string[] Used_Tbl = { "Full_information_tab_inRem", "Full_information_tab_inRem1", "Full_information_tab_inRem2",
                                    "Full_information_tab_inRem3","Full_information_tab_inRem4" ,"Full_information_tab_inRem6", "Delivery_Tbl"};

            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
    }
}