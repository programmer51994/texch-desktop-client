﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Prepare_Return_Rem_online : Form
    {
        #region MyRegion
        string Sql_Text = "";
        DataTable Dt_Cust_TBl = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable Dt = new DataTable();
        DataTable Dt_prepare_TBL = new DataTable();
        BindingSource _Bs_Rem = new BindingSource();
        BindingSource _Bs_cust = new BindingSource();
        BindingSource Bs_Sub_Cust = new BindingSource();
        DataTable Comm_TBl = new DataTable();
        int r_type_id = 0;
        string @format = "dd/MM/yyyy";
        Int32 from_nrec_date = 0;
        Int16 Rem_flag = 0;
        #endregion
        public Prepare_Return_Rem_online()
        {
            InitializeComponent();

            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            GrdRem_Details.AutoGenerateColumns = false;
            Grd_Sub_Cust.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
                cbo_order_type.Items[0] = "companies";
                cbo_order_type.Items[1] = "Agencies";
            }

        }
        //---------------------------------------------------
        private void Prepare_Return_Buy_sale_Load(object sender, EventArgs e)
        {
            CboCust_Id.Visible = false;
            label8.Visible = false;

            cbo_local_intr.SelectedIndex = 1;
            if (connection.Lang_id == 2)
            {
                Cbo_Record.Items[0]="prepare";
                Cbo_Record.Items[1] = "cancel prepare";
                Cbo_Record.Items[2] = "cancel stop & prepare";


                Cbo_Off_On.Items[0] = "Online remittance";
                Cbo_Off_On.Items[1] = "Offline remittance";

                cbo_local_intr.Items[0] = "Local network";
                cbo_local_intr.Items[1] = "Online Network";

                Cmbo_Oper_type.Items[0] = "Out remittance";
                Cmbo_Oper_type.Items[1] = "In remittance";

                Column9.DataPropertyName = "R_ecur_name";
                Column9.DataPropertyName = "t_ecoun_name";
                Column9.DataPropertyName = "t_ecity_name";
            }

            if (connection.T_ID == 1)  // الادارة
            {

                Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as  Sub_Cust_ID,0 as  Order_type"
                    + "  From CUSTOMERS A, TERMINALS B   "
                    + "  Where A.CUST_ID = B.CUST_ID "
                    + "  And A.Cust_Flag <> 0  "
                    + " union "
                    + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID,1 as  Order_type "
                    + " From  Sub_CUSTOMERS_online A  "
                    + " Where A.Cust_state  =  1 "
                    + " and A.t_id =  " + connection.T_ID
                    + " union "
                    + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID,1 as  Order_type "
                    + " From  Sub_CUSTOMERS_online A "
                    + " Where A.Cust_state  =  1"
                    + "  and A.t_id <>  1"
                    + " order by cust_id,Order_type desc";
                }

            if (connection.T_ID != 1)
            {
                Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name , A.cust_id as cust_id  , 0 as  Sub_Cust_ID ,0 as  Order_type"
                       + "  From CUSTOMERS A, TERMINALS B   "
                       + "  Where A.CUST_ID = B.CUST_ID "
                       + "  And A.Cust_Flag <> 0  "
                       + " and B.t_id =  " + connection.T_ID
                       + " union "
                       + " select distinct A.T_id,A.ASub_CustName as Acust_name , A.ESub_CustNmae as Ecust_name ,  Cust_Online_Main_Id as  cust_id  ,Sub_Cust_ID ,1 as  Order_type "
                       + " From  Sub_CUSTOMERS_online A  "
                       + " Where A.Cust_state  =  1 "//فعال
                       + " and A.t_id =  " + connection.T_ID
                       + " order by Order_type";
            }


                _Bs_cust.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
                //Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select(" Order_type = 0 ").CopyToDataTable();

                CboCust_Id.DataSource = _Bs_cust;
                CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
                CboCust_Id.ValueMember = "cust_id";
                Cmbo_Oper_type.SelectedIndex = 0;
                Cbo_Record.SelectedIndex = 0;
                TxtNrec_Date_ValueChanged(null, null);
                Cbo_Off_On.SelectedIndex = 0;
                cbo_local_intr.Visible = false;
                Cbo_Off_On.Visible = false;
                label12.Visible = false;
                label11.Visible = false;
                cbo_order_type.SelectedIndex = 0;    
        }
        //---------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {

            //    Convert.ToInt16(((DataRowView)BS_CboCust_Id.Current).Row["rem_search"]);  "cust_id", "Sub_Cust_ID"
            Int16 Sub_Cust_ID1 = Convert.ToInt16(((DataRowView)Bs_Sub_Cust.Current).Row["Sub_Cust_ID"]);
            Int16 cust_id1 = Convert.ToInt16(((DataRowView)Bs_Sub_Cust.Current).Row["cust_id"]); 


            if (TxtNrec_Date.Checked == true)
            {
                DateTime date = TxtNrec_Date.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                from_nrec_date = 0;
            }

            if (Txt_Rem_no.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ادخال رقم الحوالة" : "You must enter the Rem. number", MyGeneral_Lib.LblCap);
                return;
            }


            GrdRem_Details.DataSource = new BindingSource();

            

            if (Cmbo_Oper_type.SelectedIndex == 0)
            { r_type_id = 2; }// حوالات صادرة 
            else
            { r_type_id = 1; }

            if (connection.SQLDS.Tables.Contains("Perpare_Rem_Flag_tbl"))
            {
                connection.SQLDS.Tables.Remove("Perpare_Rem_Flag_tbl");
            }

           

            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "[dbo].[Search_Perpare_Rem_Flag]";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.Parameters.AddWithValue("@cust_id",cust_id1);
            connection.SQLCMD.Parameters.AddWithValue("@Rem_no", Txt_Rem_no.Text);
            connection.SQLCMD.Parameters.AddWithValue("@S_name", Txt_S_name.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@R_name", Txt_R_name.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@Case_date", from_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@r_type_id", r_type_id);
            connection.SQLCMD.Parameters.AddWithValue("@Record_Flag", Convert.ToInt16(Cbo_Record.SelectedIndex));
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.AddWithValue("@sub_cust_id", Sub_Cust_ID1);
            MyGeneral_Lib.Copytocliptext("Search_Perpare_Rem_Flag", connection.SQLCMD);
            //connection.SQLCMD.Parameters.AddWithValue("@Rem_flag", Rem_flag);

            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Perpare_Rem_Flag_tbl");
            obj.Close();
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCS.Close();
                connection.SQLCMD.Dispose();
                return;
            }
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();

          

             if (connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Rows.Count > 0)
             {
                 try
                 {
                     if (Cbo_Off_On.SelectedIndex == 0)// اونلاين 
                     {
                         Dt_prepare_TBL = connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Select(" local_international = " + cbo_local_intr.SelectedIndex).CopyToDataTable();
                     }
                     else
                     { Dt_prepare_TBL = connection.SQLDS.Tables["Perpare_Rem_Flag_tbl"].Select().CopyToDataTable(); }

                    _Bs_Rem.DataSource = Dt_prepare_TBL;
                    GrdRem_Details.DataSource = _Bs_Rem;
                    //TXT_Reason.DataBindings.Add("Text", _Bs_Rem, "notes_stop");
                     
                 }
                 catch
                 {
                     GrdRem_Details.DataSource = new BindingSource();
                     MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط" : "No Record for this Conditions", MyGeneral_Lib.LblCap);
                     return;
                 }
             }
             else
             {
                 GrdRem_Details.DataSource = new BindingSource();
                 MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط" : "No Record for this Conditions", MyGeneral_Lib.LblCap);
                 return;
             }
            


        }
        //---------------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (GrdRem_Details.RowCount < 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود " : "No Record ", MyGeneral_Lib.LblCap);
                return;
            }

            if (TXT_Reason.Text.Trim() == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى ادخال السبب" : "Please Enter The Reason", MyGeneral_Lib.LblCap);
                return;
            }


            DataTable Dt = new DataTable();
            string Rem_no = "";
            byte recourd_flag = 0;
            try
            {
                Dt = Dt_prepare_TBL.DefaultView.ToTable(false, "Chk", "Rem_no").Select("Chk > 0 ").CopyToDataTable();
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحوالات اولا " : "Check Rem. First  ", MyGeneral_Lib.LblCap);
                return;
            }

         




            MyGeneral_Lib.ColumnToString(Dt, "Rem_no", out Rem_no);
            Rem_no =  Rem_no + ",";
            if (Cbo_Record.SelectedIndex == 0 || Cbo_Record.SelectedIndex ==  2)
            { recourd_flag = 1; }
            else
            { recourd_flag = 0; }

            connection.SQLCMD.Parameters.AddWithValue("@Rem_no",  Rem_no  );
            connection.SQLCMD.Parameters.AddWithValue("@recourd_flag", recourd_flag);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            //connection.SQLCMD.Parameters.AddWithValue("@local_international",Rem_flag == 11 ? cbo_local_intr.SelectedIndex : 0);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.AddWithValue("@cust_online", connection.Cust_online_Id );
            connection.SQLCMD.Parameters.AddWithValue("@Lang_Id", connection.Lang_id);
            connection.SQLCMD.Parameters.AddWithValue("@notes_stop", TXT_Reason.Text.Trim()); 

            //connection.SQLCMD.Parameters.AddWithValue("@Rem_flag", Rem_flag);
           // String SS = "exec perpare_Rem_Flag " + Rem_no + "," + recourd_flag + "," + connection.user_id + "," + cbo_local_intr.SelectedIndex + "," + "'' " + "," + connection.Cust_online_Id + "," + connection.Lang_id;
            connection.SqlExec("perpare_Rem_Flag", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            GrdRem_Details.DataSource = new BindingSource();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
            //Txt_S_name.ResetText();
            // TxtNrec_Date.Text = "00000000";
            //GrdRem_Details.DataSource = new BindingSource();
            //CboCust_Id.SelectedIndex = 0;
            //BtnOk.Text = "تهيئة";
            //LblRec.Text = connection.Records(new BindingSource());
            TXT_Reason.Text = "";
            Txt_Rem_no.Text = "";
            Txt_Sub_Cust.Text = "";
            //_Bs_Rem.DataSource = new BindingSource();

        }
        //---------------------------------------------------
        private void GrdVou_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Rem);

        }
        //---------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------
        private void Prepare_Return_Buy_sale_FormClosed(object sender, FormClosedEventArgs e)
        {


            string[] Used_Tbl = { "Main_Voucher_Tbl", "Oper_TBL", "Nrec_Date_Tbl", "Perpare_Rem_Flag_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }


        private void Prepare_Return_Buy_sale_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void GrdRem_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Rem);
            TXT_Reason.DataBindings.Clear();
            TXT_Reason.DataBindings.Add("Text", _Bs_Rem, "notes_stop");
        }

        private void Cbo_Record_SelectedIndexChanged(object sender, EventArgs e)
        {
            GrdRem_Details.DataSource = new BindingSource();
        }

        private void CboCust_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            GrdRem_Details.DataSource = new BindingSource();
        }

        private void Cmbo_Oper_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmbo_Oper_type.SelectedIndex == 0)
            { r_type_id = 2; }
            else
            { r_type_id = 1; }


            GrdRem_Details.DataSource = new BindingSource();
        }

        private void Txt_S_name_TextChanged(object sender, EventArgs e)
        {
            GrdRem_Details.DataSource = new BindingSource();
        }

        private void Txt_Rem_no_TextChanged(object sender, EventArgs e)
        {
            GrdRem_Details.DataSource = new BindingSource();
        }

        private void Txt_R_name_TextChanged(object sender, EventArgs e)
        {
            GrdRem_Details.DataSource = new BindingSource();
        }

        private void TxtNrec_Date_ValueChanged(object sender, EventArgs e)
        {
            if (TxtNrec_Date.Checked == true)
            {
                TxtNrec_Date.Format = DateTimePickerFormat.Custom;
                TxtNrec_Date.CustomFormat = @format;

            }
            else
            {
                TxtNrec_Date.Format = DateTimePickerFormat.Custom;
                TxtNrec_Date.CustomFormat = " ";
            }
        }

        private void Cbo_Off_On_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_Off_On.SelectedIndex == 0)
            {
                cbo_local_intr.Visible = true;
                label11.Visible = true;  
            }
            else
            { cbo_local_intr.Visible = false;
            label11.Visible = false;
            }
         

            if (Cbo_Off_On.SelectedIndex == 0)//online
            { Rem_flag = 11; }
            else
            { Rem_flag = 1; }
            GrdRem_Details.DataSource = new BindingSource();
        }

        private void cbo_order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Order_type = Convert.ToInt32(cbo_order_type.SelectedIndex);
            try
            {
                if (cbo_order_type.SelectedIndex == 0)
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select("Order_type = " + Order_type).CopyToDataTable();
                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                    }
                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }

                }
                else
                {
                    Dt_Cust_TBl = connection.SQLDS.Tables["Oper_TBL"].DefaultView.ToTable(true, "T_id", "Acust_name", "Ecust_name", "cust_id", "Sub_Cust_ID", "Order_type").Select("Order_type = " + Order_type).CopyToDataTable();
                    if (Dt_Cust_TBl.Rows.Count > 0)
                    {
                        Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                        Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
                    }

                    else
                    {
                        Grd_Sub_Cust.DataSource = new BindingSource();
                    }

                }
            }
            catch
            { Grd_Sub_Cust.DataSource = new BindingSource(); }

            Txt_Sub_Cust.Text = "";
        }

        private void Txt_Sub_Cust_TextChanged(object sender, EventArgs e)
        {
            Get_details_Info();
        }


        private void Get_details_Info()
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);

                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "(Acust_name like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " Sub_Cust_ID > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }
         
    }
}