﻿namespace Integration_Accounting_Sys
{
    partial class Query_Rem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_R_Name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Txt_R_Phone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_s_phone = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_S_Name = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_Rem_No = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Cmb_R_CUR = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Cmb_Pr_Cur_id = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmb_range = new System.Windows.Forms.ComboBox();
            this.Cmb_Case_id = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Txt_Note = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.CHK4 = new System.Windows.Forms.CheckBox();
            this.CHK1 = new System.Windows.Forms.CheckBox();
            this.CHK2 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Cmb_sub_User_id = new System.Windows.Forms.ComboBox();
            this.CmbRcity = new System.Windows.Forms.ComboBox();
            this.CmbScity = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Cbo_R_Type_Id = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtFrom_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtTo_Da = new System.Windows.Forms.DateTimePicker();
            this.CboCust_Id = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.Cbo_Rem_OFF_ON = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.resd_sen_cmb = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.resd_rec_cmb = new System.Windows.Forms.ComboBox();
            this.txt_social_no_rec = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txt_social_no_sen = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.Cbo_Occupation_rec = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.Cbo_Occupation_sen = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.Txt_Purpose = new System.Windows.Forms.TextBox();
            this.CHK3 = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.Cbo_purpos = new System.Windows.Forms.ComboBox();
            this.Txt_R_doc_no = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.Cbo_doc_Rec = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.Cbo_doc_Sen = new System.Windows.Forms.ComboBox();
            this.Txt_S_doc_no = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cmb_s_nat = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.Cmb_R_Nat = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Chk_Hst_Info = new System.Windows.Forms.CheckBox();
            this.Chk_Last_Case = new System.Windows.Forms.CheckBox();
            this.Cbo_Data_Type = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Amonut = new System.Windows.Forms.Sample.DecimalTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(10, 1);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(284, 25);
            this.TxtTerm_Name.TabIndex = 646;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(631, 6);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 642;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(685, 2);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(137, 23);
            this.TxtIn_Rec_Date.TabIndex = 643;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(377, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(247, 23);
            this.TxtUser.TabIndex = 649;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(300, 6);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 648;
            this.label1.Text = "المستخــدم:";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-80, 29);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel9.TabIndex = 650;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(6, 31);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(847, 27);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape5,
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(846, 622);
            this.shapeContainer1.TabIndex = 651;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.Location = new System.Drawing.Point(3, 520);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(839, 18);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(353, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 19);
            this.label3.TabIndex = 652;
            this.label3.Text = "استعلام عن الحوالات";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(26, 64);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(85, 14);
            this.label11.TabIndex = 900;
            this.label11.Text = "تاريخ الانشاء :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(217, 64);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(36, 14);
            this.label5.TabIndex = 902;
            this.label5.Text = "الى :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(428, 143);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(110, 14);
            this.label6.TabIndex = 906;
            this.label6.Text = "المستخــــــــــــدم :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(6, 263);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(142, 16);
            this.label7.TabIndex = 909;
            this.label7.Text = "معلومـــات المستلـــــــــــم ....";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(435, 263);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(131, 16);
            this.label8.TabIndex = 910;
            this.label8.Text = "معلومـــــات المرســـــل.....";
            // 
            // Txt_R_Name
            // 
            this.Txt_R_Name.Location = new System.Drawing.Point(125, 286);
            this.Txt_R_Name.MaxLength = 49;
            this.Txt_R_Name.Name = "Txt_R_Name";
            this.Txt_R_Name.Size = new System.Drawing.Size(219, 20);
            this.Txt_R_Name.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(13, 289);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(109, 14);
            this.label9.TabIndex = 911;
            this.label9.Text = "اسم المستلـــــم :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(429, 116);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(109, 14);
            this.label10.TabIndex = 913;
            this.label10.Text = "مدينة الاستــــلام :";
            // 
            // Txt_R_Phone
            // 
            this.Txt_R_Phone.Location = new System.Drawing.Point(125, 309);
            this.Txt_R_Phone.MaxLength = 19;
            this.Txt_R_Phone.Name = "Txt_R_Phone";
            this.Txt_R_Phone.Size = new System.Drawing.Size(219, 20);
            this.Txt_R_Phone.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(12, 312);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(110, 14);
            this.label12.TabIndex = 915;
            this.label12.Text = "هاتف المستلــــم :";
            // 
            // Txt_s_phone
            // 
            this.Txt_s_phone.Location = new System.Drawing.Point(539, 309);
            this.Txt_s_phone.MaxLength = 19;
            this.Txt_s_phone.Name = "Txt_s_phone";
            this.Txt_s_phone.Size = new System.Drawing.Size(219, 20);
            this.Txt_s_phone.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(441, 312);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(94, 14);
            this.label13.TabIndex = 921;
            this.label13.Text = "هاتف المرسل :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(6, 170);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(109, 14);
            this.label14.TabIndex = 919;
            this.label14.Text = "مدينة الارســـــال :";
            // 
            // Txt_S_Name
            // 
            this.Txt_S_Name.Location = new System.Drawing.Point(539, 286);
            this.Txt_S_Name.MaxLength = 49;
            this.Txt_S_Name.Name = "Txt_S_Name";
            this.Txt_S_Name.Size = new System.Drawing.Size(219, 20);
            this.Txt_S_Name.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(445, 289);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(90, 14);
            this.label15.TabIndex = 917;
            this.label15.Text = "اسم المرسل :";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-4, 513);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(854, 1);
            this.flowLayoutPanel4.TabIndex = 923;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-170, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel5.TabIndex = 630;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(5, 191);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(187, 14);
            this.label16.TabIndex = 925;
            this.label16.Text = "المعلومات المالية للحوالـــــــة ...";
            // 
            // Txt_Rem_No
            // 
            this.Txt_Rem_No.Location = new System.Drawing.Point(121, 215);
            this.Txt_Rem_No.MaxLength = 14;
            this.Txt_Rem_No.Name = "Txt_Rem_No";
            this.Txt_Rem_No.Size = new System.Drawing.Size(225, 20);
            this.Txt_Rem_No.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(10, 218);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(105, 14);
            this.label17.TabIndex = 926;
            this.label17.Text = "رقم الحوالــــــــة :";
            // 
            // Cmb_R_CUR
            // 
            this.Cmb_R_CUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_CUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_CUR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_CUR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_CUR.FormattingEnabled = true;
            this.Cmb_R_CUR.Location = new System.Drawing.Point(121, 238);
            this.Cmb_R_CUR.Name = "Cmb_R_CUR";
            this.Cmb_R_CUR.Size = new System.Drawing.Size(225, 24);
            this.Cmb_R_CUR.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(9, 242);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(106, 14);
            this.label18.TabIndex = 928;
            this.label18.Text = "عملـة الحوالـــــة :";
            // 
            // Cmb_Pr_Cur_id
            // 
            this.Cmb_Pr_Cur_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Pr_Cur_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Pr_Cur_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Pr_Cur_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Pr_Cur_id.FormattingEnabled = true;
            this.Cmb_Pr_Cur_id.Location = new System.Drawing.Point(547, 238);
            this.Cmb_Pr_Cur_id.Name = "Cmb_Pr_Cur_id";
            this.Cmb_Pr_Cur_id.Size = new System.Drawing.Size(240, 24);
            this.Cmb_Pr_Cur_id.TabIndex = 15;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(440, 242);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(103, 14);
            this.label19.TabIndex = 930;
            this.label19.Text = "عملـة التسليــم :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(440, 218);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(96, 14);
            this.label21.TabIndex = 934;
            this.label21.Text = "مبلغ الحوالــــة :";
            // 
            // cmb_range
            // 
            this.cmb_range.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_range.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_range.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_range.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_range.FormattingEnabled = true;
            this.cmb_range.Items.AddRange(new object[] {
            "يساوي",
            "فمادون",
            "فمافوق"});
            this.cmb_range.Location = new System.Drawing.Point(684, 213);
            this.cmb_range.Name = "cmb_range";
            this.cmb_range.Size = new System.Drawing.Size(103, 24);
            this.cmb_range.TabIndex = 13;
            // 
            // Cmb_Case_id
            // 
            this.Cmb_Case_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Case_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Case_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Case_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Case_id.FormattingEnabled = true;
            this.Cmb_Case_id.Location = new System.Drawing.Point(547, 85);
            this.Cmb_Case_id.Name = "Cmb_Case_id";
            this.Cmb_Case_id.Size = new System.Drawing.Size(237, 24);
            this.Cmb_Case_id.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(421, 90);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(117, 14);
            this.label22.TabIndex = 937;
            this.label22.Text = "حالــة الحوالـــــــــة :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.Maroon;
            this.label24.Location = new System.Drawing.Point(7, 521);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(153, 14);
            this.label24.TabIndex = 941;
            this.label24.Text = "التفاصيــــــــــــــــــــــــل .....";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(10, 545);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(101, 14);
            this.label25.TabIndex = 942;
            this.label25.Text = "الغــــــــــــــــرض :";
            // 
            // Txt_Note
            // 
            this.Txt_Note.Location = new System.Drawing.Point(128, 563);
            this.Txt_Note.MaxLength = 199;
            this.Txt_Note.Name = "Txt_Note";
            this.Txt_Note.Size = new System.Drawing.Size(420, 20);
            this.Txt_Note.TabIndex = 28;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(10, 566);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(107, 14);
            this.label26.TabIndex = 944;
            this.label26.Text = "التفاصيــــــــــــــل :";
            // 
            // CHK4
            // 
            this.CHK4.AutoSize = true;
            this.CHK4.BackColor = System.Drawing.SystemColors.Control;
            this.CHK4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.CHK4.ForeColor = System.Drawing.Color.Navy;
            this.CHK4.Location = new System.Drawing.Point(553, 565);
            this.CHK4.Name = "CHK4";
            this.CHK4.Size = new System.Drawing.Size(78, 18);
            this.CHK4.TabIndex = 30;
            this.CHK4.Text = "اينما وجد";
            this.CHK4.UseVisualStyleBackColor = false;
            // 
            // CHK1
            // 
            this.CHK1.AutoSize = true;
            this.CHK1.BackColor = System.Drawing.SystemColors.Control;
            this.CHK1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK1.ForeColor = System.Drawing.Color.Navy;
            this.CHK1.Location = new System.Drawing.Point(346, 288);
            this.CHK1.Name = "CHK1";
            this.CHK1.Size = new System.Drawing.Size(72, 17);
            this.CHK1.TabIndex = 15;
            this.CHK1.Text = "اينما وجد";
            this.CHK1.UseVisualStyleBackColor = false;
            // 
            // CHK2
            // 
            this.CHK2.AutoSize = true;
            this.CHK2.BackColor = System.Drawing.SystemColors.Control;
            this.CHK2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK2.ForeColor = System.Drawing.Color.Navy;
            this.CHK2.Location = new System.Drawing.Point(761, 288);
            this.CHK2.Name = "CHK2";
            this.CHK2.Size = new System.Drawing.Size(72, 17);
            this.CHK2.TabIndex = 17;
            this.CHK2.Text = "اينما وجد";
            this.CHK2.UseVisualStyleBackColor = false;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-4, 586);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(855, 1);
            this.flowLayoutPanel6.TabIndex = 950;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-169, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(1, 616);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(849, 1);
            this.flowLayoutPanel8.TabIndex = 951;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-175, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(421, 590);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 25);
            this.button2.TabIndex = 32;
            this.button2.Text = "انهـــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.Location = new System.Drawing.Point(340, 590);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 25);
            this.button1.TabIndex = 31;
            this.button1.Text = "موافــق";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Cmb_sub_User_id
            // 
            this.Cmb_sub_User_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_sub_User_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_sub_User_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_sub_User_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_sub_User_id.FormattingEnabled = true;
            this.Cmb_sub_User_id.Location = new System.Drawing.Point(547, 138);
            this.Cmb_sub_User_id.Name = "Cmb_sub_User_id";
            this.Cmb_sub_User_id.Size = new System.Drawing.Size(236, 24);
            this.Cmb_sub_User_id.TabIndex = 9;
            // 
            // CmbRcity
            // 
            this.CmbRcity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CmbRcity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbRcity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbRcity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbRcity.FormattingEnabled = true;
            this.CmbRcity.Location = new System.Drawing.Point(547, 111);
            this.CmbRcity.Name = "CmbRcity";
            this.CmbRcity.Size = new System.Drawing.Size(237, 24);
            this.CmbRcity.TabIndex = 7;
            // 
            // CmbScity
            // 
            this.CmbScity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CmbScity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbScity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbScity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbScity.FormattingEnabled = true;
            this.CmbScity.Location = new System.Drawing.Point(117, 165);
            this.CmbScity.Name = "CmbScity";
            this.CmbScity.Size = new System.Drawing.Size(236, 24);
            this.CmbScity.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(6, 143);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(111, 14);
            this.label20.TabIndex = 932;
            this.label20.Text = "نــوع الحوالـــــــــة :";
            // 
            // Cbo_R_Type_Id
            // 
            this.Cbo_R_Type_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_R_Type_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_R_Type_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_R_Type_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_R_Type_Id.FormattingEnabled = true;
            this.Cbo_R_Type_Id.Items.AddRange(new object[] {
            "صادر",
            "وارد"});
            this.Cbo_R_Type_Id.Location = new System.Drawing.Point(117, 138);
            this.Cbo_R_Type_Id.Name = "Cbo_R_Type_Id";
            this.Cbo_R_Type_Id.Size = new System.Drawing.Size(236, 24);
            this.Cbo_R_Type_Id.TabIndex = 8;
            this.Cbo_R_Type_Id.SelectedIndexChanged += new System.EventHandler(this.Cbo_R_Type_Id_SelectedIndexChanged);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-2, 275);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(862, 1);
            this.flowLayoutPanel2.TabIndex = 964;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-162, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel3.TabIndex = 630;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-4, 202);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(852, 1);
            this.flowLayoutPanel11.TabIndex = 965;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-172, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel12.TabIndex = 630;
            // 
            // TxtFrom_Da
            // 
            this.TxtFrom_Da.Checked = false;
            this.TxtFrom_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtFrom_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFrom_Da.Location = new System.Drawing.Point(117, 61);
            this.TxtFrom_Da.Name = "TxtFrom_Da";
            this.TxtFrom_Da.ShowCheckBox = true;
            this.TxtFrom_Da.Size = new System.Drawing.Size(94, 20);
            this.TxtFrom_Da.TabIndex = 0;
            this.TxtFrom_Da.ValueChanged += new System.EventHandler(this.TxtFrom_Da_ValueChanged);
            // 
            // TxtTo_Da
            // 
            this.TxtTo_Da.Checked = false;
            this.TxtTo_Da.CustomFormat = "yyyy/mm/dd";
            this.TxtTo_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtTo_Da.Location = new System.Drawing.Point(258, 61);
            this.TxtTo_Da.Name = "TxtTo_Da";
            this.TxtTo_Da.ShowCheckBox = true;
            this.TxtTo_Da.Size = new System.Drawing.Size(94, 20);
            this.TxtTo_Da.TabIndex = 1;
            this.TxtTo_Da.ValueChanged += new System.EventHandler(this.TxtTo_Da_ValueChanged);
            // 
            // CboCust_Id
            // 
            this.CboCust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCust_Id.FormattingEnabled = true;
            this.CboCust_Id.Location = new System.Drawing.Point(117, 112);
            this.CboCust_Id.Name = "CboCust_Id";
            this.CboCust_Id.Size = new System.Drawing.Size(236, 24);
            this.CboCust_Id.TabIndex = 6;
            this.CboCust_Id.SelectedIndexChanged += new System.EventHandler(this.CboCust_Id_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(6, 117);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(111, 14);
            this.label23.TabIndex = 971;
            this.label23.Text = "الوكيـــــل/الفــــرع:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(15, 90);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(99, 14);
            this.label28.TabIndex = 972;
            this.label28.Text = "نـــــوع الحوالــة :";
            // 
            // Cbo_Rem_OFF_ON
            // 
            this.Cbo_Rem_OFF_ON.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Rem_OFF_ON.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Rem_OFF_ON.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Rem_OFF_ON.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Rem_OFF_ON.FormattingEnabled = true;
            this.Cbo_Rem_OFF_ON.Items.AddRange(new object[] {
            "حـــوالات اوفلايـــن",
            "حـــوالات اونلايـــن"});
            this.Cbo_Rem_OFF_ON.Location = new System.Drawing.Point(117, 85);
            this.Cbo_Rem_OFF_ON.Name = "Cbo_Rem_OFF_ON";
            this.Cbo_Rem_OFF_ON.Size = new System.Drawing.Size(236, 24);
            this.Cbo_Rem_OFF_ON.TabIndex = 4;
            this.Cbo_Rem_OFF_ON.SelectedIndexChanged += new System.EventHandler(this.Cbo_Rem_OFF_ON_SelectedIndexChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(430, 364);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(105, 14);
            this.label50.TabIndex = 975;
            this.label50.Text = "نوع الاقامـــــــــــة:";
            // 
            // resd_sen_cmb
            // 
            this.resd_sen_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_sen_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_sen_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_sen_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_sen_cmb.FormattingEnabled = true;
            this.resd_sen_cmb.Location = new System.Drawing.Point(539, 359);
            this.resd_sen_cmb.Name = "resd_sen_cmb";
            this.resd_sen_cmb.Size = new System.Drawing.Size(219, 24);
            this.resd_sen_cmb.TabIndex = 23;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(17, 364);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(105, 14);
            this.label29.TabIndex = 977;
            this.label29.Text = "نوع الاقامـــــــــــة:";
            // 
            // resd_rec_cmb
            // 
            this.resd_rec_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_rec_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_rec_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_rec_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_rec_cmb.FormattingEnabled = true;
            this.resd_rec_cmb.Location = new System.Drawing.Point(125, 359);
            this.resd_rec_cmb.Name = "resd_rec_cmb";
            this.resd_rec_cmb.Size = new System.Drawing.Size(219, 24);
            this.resd_rec_cmb.TabIndex = 22;
            // 
            // txt_social_no_rec
            // 
            this.txt_social_no_rec.Location = new System.Drawing.Point(125, 385);
            this.txt_social_no_rec.MaxLength = 49;
            this.txt_social_no_rec.Name = "txt_social_no_rec";
            this.txt_social_no_rec.Size = new System.Drawing.Size(219, 20);
            this.txt_social_no_rec.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(9, 388);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(113, 14);
            this.label30.TabIndex = 979;
            this.label30.Text = "الرقـــم الوطنـــــي:";
            // 
            // txt_social_no_sen
            // 
            this.txt_social_no_sen.Location = new System.Drawing.Point(538, 385);
            this.txt_social_no_sen.MaxLength = 49;
            this.txt_social_no_sen.Name = "txt_social_no_sen";
            this.txt_social_no_sen.Size = new System.Drawing.Size(219, 20);
            this.txt_social_no_sen.TabIndex = 25;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(431, 388);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(104, 14);
            this.label31.TabIndex = 981;
            this.label31.Text = "الرقـم الوطنــــي:";
            // 
            // Cbo_Occupation_rec
            // 
            this.Cbo_Occupation_rec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Occupation_rec.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Occupation_rec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Occupation_rec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Occupation_rec.FormattingEnabled = true;
            this.Cbo_Occupation_rec.Location = new System.Drawing.Point(125, 332);
            this.Cbo_Occupation_rec.Name = "Cbo_Occupation_rec";
            this.Cbo_Occupation_rec.Size = new System.Drawing.Size(219, 24);
            this.Cbo_Occupation_rec.TabIndex = 20;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(13, 337);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(109, 14);
            this.label32.TabIndex = 982;
            this.label32.Text = "مهنــة المستلــــم:";
            // 
            // Cbo_Occupation_sen
            // 
            this.Cbo_Occupation_sen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Occupation_sen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Occupation_sen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Occupation_sen.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Occupation_sen.FormattingEnabled = true;
            this.Cbo_Occupation_sen.Location = new System.Drawing.Point(538, 332);
            this.Cbo_Occupation_sen.Name = "Cbo_Occupation_sen";
            this.Cbo_Occupation_sen.Size = new System.Drawing.Size(219, 24);
            this.Cbo_Occupation_sen.TabIndex = 21;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(433, 337);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 14);
            this.label33.TabIndex = 984;
            this.label33.Text = "مهنـــــة المرسل:";
            // 
            // Txt_Purpose
            // 
            this.Txt_Purpose.Location = new System.Drawing.Point(128, 542);
            this.Txt_Purpose.MaxLength = 199;
            this.Txt_Purpose.Name = "Txt_Purpose";
            this.Txt_Purpose.Size = new System.Drawing.Size(420, 20);
            this.Txt_Purpose.TabIndex = 27;
            // 
            // CHK3
            // 
            this.CHK3.AutoSize = true;
            this.CHK3.BackColor = System.Drawing.SystemColors.Control;
            this.CHK3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.CHK3.ForeColor = System.Drawing.Color.Navy;
            this.CHK3.Location = new System.Drawing.Point(553, 543);
            this.CHK3.Name = "CHK3";
            this.CHK3.Size = new System.Drawing.Size(78, 18);
            this.CHK3.TabIndex = 29;
            this.CHK3.Text = "اينما وجد";
            this.CHK3.UseVisualStyleBackColor = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(2, 465);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(120, 14);
            this.label34.TabIndex = 987;
            this.label34.Text = "الغرض من الحوالــة:";
            // 
            // Cbo_purpos
            // 
            this.Cbo_purpos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_purpos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_purpos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_purpos.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_purpos.FormattingEnabled = true;
            this.Cbo_purpos.Location = new System.Drawing.Point(125, 460);
            this.Cbo_purpos.Name = "Cbo_purpos";
            this.Cbo_purpos.Size = new System.Drawing.Size(219, 24);
            this.Cbo_purpos.TabIndex = 26;
            // 
            // Txt_R_doc_no
            // 
            this.Txt_R_doc_no.Location = new System.Drawing.Point(125, 409);
            this.Txt_R_doc_no.MaxLength = 49;
            this.Txt_R_doc_no.Name = "Txt_R_doc_no";
            this.Txt_R_doc_no.Size = new System.Drawing.Size(219, 20);
            this.Txt_R_doc_no.TabIndex = 988;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(9, 412);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 989;
            this.label35.Text = "رقــــم الوثيقــــــــة:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(17, 437);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(103, 14);
            this.label36.TabIndex = 991;
            this.label36.Text = "نوع الوثـيقـــــــــة:";
            // 
            // Cbo_doc_Rec
            // 
            this.Cbo_doc_Rec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_doc_Rec.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_doc_Rec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_doc_Rec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_doc_Rec.FormattingEnabled = true;
            this.Cbo_doc_Rec.Location = new System.Drawing.Point(125, 432);
            this.Cbo_doc_Rec.Name = "Cbo_doc_Rec";
            this.Cbo_doc_Rec.Size = new System.Drawing.Size(219, 24);
            this.Cbo_doc_Rec.TabIndex = 990;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(432, 437);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(103, 14);
            this.label37.TabIndex = 995;
            this.label37.Text = "نوع الوثـيقـــــــــة:";
            // 
            // Cbo_doc_Sen
            // 
            this.Cbo_doc_Sen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_doc_Sen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_doc_Sen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_doc_Sen.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_doc_Sen.FormattingEnabled = true;
            this.Cbo_doc_Sen.Location = new System.Drawing.Point(539, 432);
            this.Cbo_doc_Sen.Name = "Cbo_doc_Sen";
            this.Cbo_doc_Sen.Size = new System.Drawing.Size(219, 24);
            this.Cbo_doc_Sen.TabIndex = 994;
            // 
            // Txt_S_doc_no
            // 
            this.Txt_S_doc_no.Location = new System.Drawing.Point(539, 409);
            this.Txt_S_doc_no.MaxLength = 49;
            this.Txt_S_doc_no.Name = "Txt_S_doc_no";
            this.Txt_S_doc_no.Size = new System.Drawing.Size(219, 20);
            this.Txt_S_doc_no.TabIndex = 992;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(423, 412);
            this.label38.Name = "label38";
            this.label38.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label38.Size = new System.Drawing.Size(112, 14);
            this.label38.TabIndex = 993;
            this.label38.Text = "رقــــم الوثيقــــــــة:";
            // 
            // cmb_s_nat
            // 
            this.cmb_s_nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_s_nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_s_nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_s_nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_s_nat.FormattingEnabled = true;
            this.cmb_s_nat.Location = new System.Drawing.Point(539, 460);
            this.cmb_s_nat.Name = "cmb_s_nat";
            this.cmb_s_nat.Size = new System.Drawing.Size(219, 24);
            this.cmb_s_nat.TabIndex = 996;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(439, 464);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(96, 14);
            this.label39.TabIndex = 997;
            this.label39.Text = "الجنسيــــــــــــة:";
            // 
            // Cmb_R_Nat
            // 
            this.Cmb_R_Nat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_Nat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_Nat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_Nat.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_Nat.FormattingEnabled = true;
            this.Cmb_R_Nat.Location = new System.Drawing.Point(125, 486);
            this.Cmb_R_Nat.Name = "Cmb_R_Nat";
            this.Cmb_R_Nat.Size = new System.Drawing.Size(219, 24);
            this.Cmb_R_Nat.TabIndex = 998;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(26, 490);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label40.Size = new System.Drawing.Size(93, 14);
            this.label40.TabIndex = 999;
            this.label40.Text = "الجنسيـــــــــــة:";
            // 
            // Chk_Hst_Info
            // 
            this.Chk_Hst_Info.AutoSize = true;
            this.Chk_Hst_Info.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Hst_Info.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Hst_Info.Location = new System.Drawing.Point(579, 62);
            this.Chk_Hst_Info.Name = "Chk_Hst_Info";
            this.Chk_Hst_Info.Size = new System.Drawing.Size(206, 18);
            this.Chk_Hst_Info.TabIndex = 1164;
            this.Chk_Hst_Info.Text = "أظهار معلومات الحوالة التاريخية";
            this.Chk_Hst_Info.UseVisualStyleBackColor = true;
            // 
            // Chk_Last_Case
            // 
            this.Chk_Last_Case.AutoSize = true;
            this.Chk_Last_Case.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Last_Case.ForeColor = System.Drawing.Color.Maroon;
            this.Chk_Last_Case.Location = new System.Drawing.Point(397, 62);
            this.Chk_Last_Case.Name = "Chk_Last_Case";
            this.Chk_Last_Case.Size = new System.Drawing.Size(147, 18);
            this.Chk_Last_Case.TabIndex = 1165;
            this.Chk_Last_Case.Text = "الحالة الاخيرة للحوالة";
            this.Chk_Last_Case.UseVisualStyleBackColor = true;
            // 
            // Cbo_Data_Type
            // 
            this.Cbo_Data_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Data_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Data_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Data_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Data_Type.FormattingEnabled = true;
            this.Cbo_Data_Type.Items.AddRange(new object[] {
            "بـــيــــانـــات حالية",
            "بـــيــــانـــات مرحلة"});
            this.Cbo_Data_Type.Location = new System.Drawing.Point(548, 165);
            this.Cbo_Data_Type.Name = "Cbo_Data_Type";
            this.Cbo_Data_Type.Size = new System.Drawing.Size(236, 24);
            this.Cbo_Data_Type.TabIndex = 1166;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(419, 170);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(121, 14);
            this.label2.TabIndex = 1167;
            this.label2.Text = "نـــــوع الــبــيـــانــات :";
            // 
            // Txt_Amonut
            // 
            this.Txt_Amonut.BackColor = System.Drawing.Color.White;
            this.Txt_Amonut.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Amonut.Location = new System.Drawing.Point(547, 214);
            this.Txt_Amonut.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Amonut.Name = "Txt_Amonut";
            this.Txt_Amonut.NumberDecimalDigits = 3;
            this.Txt_Amonut.NumberDecimalSeparator = ".";
            this.Txt_Amonut.NumberGroupSeparator = ",";
            this.Txt_Amonut.ReadOnly = true;
            this.Txt_Amonut.Size = new System.Drawing.Size(136, 23);
            this.Txt_Amonut.TabIndex = 12;
            this.Txt_Amonut.Text = "0.000";
            // 
            // Query_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 622);
            this.Controls.Add(this.Cbo_Data_Type);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Chk_Last_Case);
            this.Controls.Add(this.Chk_Hst_Info);
            this.Controls.Add(this.Cmb_R_Nat);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.cmb_s_nat);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.Cbo_doc_Sen);
            this.Controls.Add(this.Txt_S_doc_no);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.Cbo_doc_Rec);
            this.Controls.Add(this.Txt_R_doc_no);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.Cbo_purpos);
            this.Controls.Add(this.Cbo_Occupation_sen);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.Cbo_Occupation_rec);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.txt_social_no_sen);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.txt_social_no_rec);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.resd_rec_cmb);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.resd_sen_cmb);
            this.Controls.Add(this.Cbo_Rem_OFF_ON);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.CboCust_Id);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.TxtTo_Da);
            this.Controls.Add(this.TxtFrom_Da);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.flowLayoutPanel11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Txt_Amonut);
            this.Controls.Add(this.CmbScity);
            this.Controls.Add(this.CmbRcity);
            this.Controls.Add(this.Cmb_sub_User_id);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.CHK2);
            this.Controls.Add(this.CHK1);
            this.Controls.Add(this.CHK4);
            this.Controls.Add(this.CHK3);
            this.Controls.Add(this.Txt_Note);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.Txt_Purpose);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Cmb_Case_id);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.cmb_range);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Cbo_R_Type_Id);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Cmb_Pr_Cur_id);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Cmb_R_CUR);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Txt_Rem_No);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Txt_s_phone);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_S_Name);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_R_Phone);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_R_Name);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Query_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "505";
            this.Text = "Query_Rem";
            this.Load += new System.EventHandler(this.Query_Rem_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_R_Name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Txt_R_Phone;
        private System.Windows.Forms.Label label12;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private System.Windows.Forms.TextBox Txt_s_phone;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_S_Name;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Txt_Rem_No;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox Cmb_R_CUR;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Cmb_Pr_Cur_id;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmb_range;
        private System.Windows.Forms.ComboBox Cmb_Case_id;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txt_Note;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox CHK4;
        private System.Windows.Forms.CheckBox CHK1;
        private System.Windows.Forms.CheckBox CHK2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox Cmb_sub_User_id;
        private System.Windows.Forms.ComboBox CmbRcity;
        private System.Windows.Forms.ComboBox CmbScity;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Amonut;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox Cbo_R_Type_Id;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.DateTimePicker TxtFrom_Da;
        private System.Windows.Forms.DateTimePicker TxtTo_Da;
        private System.Windows.Forms.ComboBox CboCust_Id;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox Cbo_Rem_OFF_ON;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox resd_sen_cmb;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox resd_rec_cmb;
        private System.Windows.Forms.TextBox txt_social_no_rec;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txt_social_no_sen;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox Cbo_Occupation_rec;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox Cbo_Occupation_sen;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox Txt_Purpose;
        private System.Windows.Forms.CheckBox CHK3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox Cbo_purpos;
        private System.Windows.Forms.TextBox Txt_R_doc_no;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox Cbo_doc_Rec;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox Cbo_doc_Sen;
        private System.Windows.Forms.TextBox Txt_S_doc_no;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmb_s_nat;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox Cmb_R_Nat;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox Chk_Hst_Info;
        private System.Windows.Forms.CheckBox Chk_Last_Case;
        private System.Windows.Forms.ComboBox Cbo_Data_Type;
        private System.Windows.Forms.Label label2;
    }
}