﻿namespace Integration_Accounting_Sys
{
    partial class Query_Commission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Cmb_R_CUR = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Cmb_Pr_Cur_id = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.Btn_Excel = new System.Windows.Forms.Button();
            this.CmbRcity = new System.Windows.Forms.ComboBox();
            this.CmbScity = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Cbo_R_Type_Id = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.Grd_comm_info = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.Txt_R_MAX_AMNT = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_R_MIN_AMNT = new System.Windows.Forms.Sample.DecimalTextBox();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(3, 3);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(272, 25);
            this.TxtTerm_Name.TabIndex = 646;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(700, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 642;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(754, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(155, 23);
            this.TxtIn_Rec_Date.TabIndex = 643;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(390, 5);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(245, 23);
            this.TxtUser.TabIndex = 649;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(313, 8);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 648;
            this.label1.Text = "المستخــدم:";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-80, 31);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel9.TabIndex = 650;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel1.TabIndex = 630;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(475, 90);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(103, 14);
            this.label10.TabIndex = 913;
            this.label10.Text = "مدينة الاستــلام :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(27, 90);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(106, 14);
            this.label14.TabIndex = 919;
            this.label14.Text = "مدينة الارســــال :";
            // 
            // Cmb_R_CUR
            // 
            this.Cmb_R_CUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_CUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_CUR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_CUR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_CUR.FormattingEnabled = true;
            this.Cmb_R_CUR.Location = new System.Drawing.Point(140, 113);
            this.Cmb_R_CUR.Name = "Cmb_R_CUR";
            this.Cmb_R_CUR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cmb_R_CUR.Size = new System.Drawing.Size(249, 24);
            this.Cmb_R_CUR.TabIndex = 12;
            this.Cmb_R_CUR.SelectedValueChanged += new System.EventHandler(this.Cmb_R_CUR_SelectedValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(24, 117);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(109, 14);
            this.label18.TabIndex = 928;
            this.label18.Text = "عملـــــة الحوالــة :";
            // 
            // Cmb_Pr_Cur_id
            // 
            this.Cmb_Pr_Cur_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Pr_Cur_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Pr_Cur_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Pr_Cur_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Pr_Cur_id.FormattingEnabled = true;
            this.Cmb_Pr_Cur_id.Location = new System.Drawing.Point(590, 113);
            this.Cmb_Pr_Cur_id.Name = "Cmb_Pr_Cur_id";
            this.Cmb_Pr_Cur_id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cmb_Pr_Cur_id.Size = new System.Drawing.Size(250, 24);
            this.Cmb_Pr_Cur_id.TabIndex = 13;
            this.Cmb_Pr_Cur_id.SelectedValueChanged += new System.EventHandler(this.Cmb_Pr_Cur_id_SelectedValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(467, 117);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(112, 14);
            this.label19.TabIndex = 930;
            this.label19.Text = "عملـة التسليـــــم :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(472, 146);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(106, 14);
            this.label21.TabIndex = 934;
            this.label21.Text = "مدى المبالغ مـن :";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-4, 461);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(919, 1);
            this.flowLayoutPanel6.TabIndex = 950;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(-105, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(1, 497);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(914, 1);
            this.flowLayoutPanel8.TabIndex = 951;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-110, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel10.TabIndex = 630;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(449, 464);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 31);
            this.button2.TabIndex = 25;
            this.button2.Text = "انهـــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Btn_Excel
            // 
            this.Btn_Excel.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Excel.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Excel.Location = new System.Drawing.Point(368, 464);
            this.Btn_Excel.Name = "Btn_Excel";
            this.Btn_Excel.Size = new System.Drawing.Size(81, 31);
            this.Btn_Excel.TabIndex = 24;
            this.Btn_Excel.Text = "تصديــــر";
            this.Btn_Excel.UseVisualStyleBackColor = true;
            this.Btn_Excel.Click += new System.EventHandler(this.button1_Click);
            // 
            // CmbRcity
            // 
            this.CmbRcity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CmbRcity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbRcity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbRcity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbRcity.FormattingEnabled = true;
            this.CmbRcity.Location = new System.Drawing.Point(590, 85);
            this.CmbRcity.Name = "CmbRcity";
            this.CmbRcity.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CmbRcity.Size = new System.Drawing.Size(250, 24);
            this.CmbRcity.TabIndex = 7;
            this.CmbRcity.SelectedValueChanged += new System.EventHandler(this.CmbRcity_SelectedValueChanged);
            // 
            // CmbScity
            // 
            this.CmbScity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CmbScity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbScity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbScity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbScity.FormattingEnabled = true;
            this.CmbScity.Location = new System.Drawing.Point(140, 85);
            this.CmbScity.Name = "CmbScity";
            this.CmbScity.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CmbScity.Size = new System.Drawing.Size(249, 24);
            this.CmbScity.TabIndex = 6;
            this.CmbScity.SelectedValueChanged += new System.EventHandler(this.CmbScity_SelectedValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(21, 146);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(111, 14);
            this.label20.TabIndex = 932;
            this.label20.Text = "نــوع الحوالـــــــــة :";
            // 
            // Cbo_R_Type_Id
            // 
            this.Cbo_R_Type_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_R_Type_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_R_Type_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_R_Type_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_R_Type_Id.FormattingEnabled = true;
            this.Cbo_R_Type_Id.Items.AddRange(new object[] {
            "صادر",
            "وارد"});
            this.Cbo_R_Type_Id.Location = new System.Drawing.Point(140, 142);
            this.Cbo_R_Type_Id.Name = "Cbo_R_Type_Id";
            this.Cbo_R_Type_Id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cbo_R_Type_Id.Size = new System.Drawing.Size(249, 24);
            this.Cbo_R_Type_Id.TabIndex = 4;
            this.Cbo_R_Type_Id.SelectedValueChanged += new System.EventHandler(this.Cbo_R_Type_Id_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(701, 146);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(30, 16);
            this.label2.TabIndex = 953;
            this.label2.Text = "الـى:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(376, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 19);
            this.label3.TabIndex = 652;
            this.label3.Text = "استعلام عن العمولات";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(915, 508);
            this.shapeContainer1.TabIndex = 651;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(5, 34);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(906, 27);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(158, 202);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(757, 1);
            this.flowLayoutPanel2.TabIndex = 997;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(3, 193);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(150, 14);
            this.label5.TabIndex = 996;
            this.label5.Text = "معلومــــات العمولـــــــة....";
            // 
            // Grd_comm_info
            // 
            this.Grd_comm_info.AllowUserToAddRows = false;
            this.Grd_comm_info.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_comm_info.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_comm_info.ColumnHeadersHeight = 24;
            this.Grd_comm_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.Column13,
            this.Column2,
            this.Column4,
            this.Column1,
            this.Column8,
            this.Column9,
            this.Column3,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_comm_info.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_comm_info.Location = new System.Drawing.Point(4, 216);
            this.Grd_comm_info.Name = "Grd_comm_info";
            this.Grd_comm_info.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_comm_info.RowHeadersVisible = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_comm_info.Size = new System.Drawing.Size(909, 227);
            this.Grd_comm_info.TabIndex = 995;
            this.Grd_comm_info.VirtualMode = true;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "R_MIN_AMNT";
            this.Column10.HeaderText = "الحد الادنى ";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 89;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "R_MAX_AMNT";
            this.Column13.HeaderText = "الحد الاعلى";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 85;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "ACur_Name_rem";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 88;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "ACur_Name_pay";
            this.Column4.HeaderText = "عملة الدفع";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 82;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "ACur_Name_comm";
            this.Column1.HeaderText = "عملة العمولة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 89;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "Tacity_name";
            this.Column8.HeaderText = "مدينة الاستلام";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 96;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "acity_name";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 97;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "comm_type";
            this.Column3.HeaderText = "نوع العمولة";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 86;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "OUT_COMM_CUT_CS";
            this.Column5.HeaderText = "عمولة مقطوعة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 99;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "OUT_COMM_PER_CS";
            this.Column6.HeaderText = "عمولة نسبية";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 89;
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.SearchBtn.ForeColor = System.Drawing.Color.Navy;
            this.SearchBtn.Location = new System.Drawing.Point(847, 142);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(66, 26);
            this.SearchBtn.TabIndex = 998;
            this.SearchBtn.Text = "بحـــــث";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // Txt_R_MAX_AMNT
            // 
            this.Txt_R_MAX_AMNT.BackColor = System.Drawing.Color.White;
            this.Txt_R_MAX_AMNT.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_MAX_AMNT.Location = new System.Drawing.Point(732, 143);
            this.Txt_R_MAX_AMNT.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_R_MAX_AMNT.Name = "Txt_R_MAX_AMNT";
            this.Txt_R_MAX_AMNT.NumberDecimalDigits = 3;
            this.Txt_R_MAX_AMNT.NumberDecimalSeparator = ".";
            this.Txt_R_MAX_AMNT.NumberGroupSeparator = ",";
            this.Txt_R_MAX_AMNT.ReadOnly = true;
            this.Txt_R_MAX_AMNT.Size = new System.Drawing.Size(109, 23);
            this.Txt_R_MAX_AMNT.TabIndex = 952;
            this.Txt_R_MAX_AMNT.Text = "0.000";
            this.Txt_R_MAX_AMNT.TextChanged += new System.EventHandler(this.Txt_R_MAX_AMNT_TextChanged);
            // 
            // Txt_R_MIN_AMNT
            // 
            this.Txt_R_MIN_AMNT.BackColor = System.Drawing.Color.White;
            this.Txt_R_MIN_AMNT.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_R_MIN_AMNT.Location = new System.Drawing.Point(590, 143);
            this.Txt_R_MIN_AMNT.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_R_MIN_AMNT.Name = "Txt_R_MIN_AMNT";
            this.Txt_R_MIN_AMNT.NumberDecimalDigits = 3;
            this.Txt_R_MIN_AMNT.NumberDecimalSeparator = ".";
            this.Txt_R_MIN_AMNT.NumberGroupSeparator = ",";
            this.Txt_R_MIN_AMNT.ReadOnly = true;
            this.Txt_R_MIN_AMNT.Size = new System.Drawing.Size(108, 23);
            this.Txt_R_MIN_AMNT.TabIndex = 10;
            this.Txt_R_MIN_AMNT.Text = "0.000";
            this.Txt_R_MIN_AMNT.TextChanged += new System.EventHandler(this.Txt_R_MIN_AMNT_TextChanged);
            // 
            // Query_Commission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 508);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Grd_comm_info);
            this.Controls.Add(this.Txt_R_MAX_AMNT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_R_MIN_AMNT);
            this.Controls.Add(this.CmbScity);
            this.Controls.Add(this.CmbRcity);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Btn_Excel);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Cbo_R_Type_Id);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Cmb_Pr_Cur_id);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Cmb_R_CUR);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.shapeContainer1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Query_Commission";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "507";
            this.Text = "استعلام عن العمولات";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Query_Commission_FormClosed);
            this.Load += new System.EventHandler(this.Query_Rem_Load);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox Cmb_R_CUR;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Cmb_Pr_Cur_id;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Btn_Excel;
        private System.Windows.Forms.ComboBox CmbRcity;
        private System.Windows.Forms.ComboBox CmbScity;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_R_MIN_AMNT;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox Cbo_R_Type_Id;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_R_MAX_AMNT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView Grd_comm_info;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}