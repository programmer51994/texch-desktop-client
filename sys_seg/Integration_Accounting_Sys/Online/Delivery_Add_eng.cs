﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Integration_Accounting_Sys.Branch_Report;
using System.Globalization;

namespace Integration_Accounting_Sys
{
    public partial class Delivery_Add_eng : Form
    {
        string @format = "dd/MM/yyyy";
        string @format_null = " ";
        bool chag_grd_rem = false;
        bool chang_rcity = false;
        bool chang_cur_rrate = false;
        string name_city_con = "";
        int Mcity_con_online_id = 0;
        int Mr_cur_id = 0;
        int Mp_cur_id = 0;
        decimal Mdiff_Amoun = 0;
        decimal param_Exch_rate_rem = 0;
        int R_per_id = 0;
        string Sql_Txt = "";
        int Vo_No = 0;
        string _Date = "";
        string rem_no = "";
        string local_Cur = "";
        string forgin_Cur = "";
        string forgin_Cur1 = "";
        int Oper_Id = 0;
        string User = "";
        string Exch_rate_com = "";
        decimal param_Exch_rate_com = 0;
        double Total_Amount = 0;
        double Rem_Amount = 0;
        string term = "";
        public static Int16 Del_Btn = 0;
        // int int_nrecdate = 0;
        Int16 Rem_Flag = 0;
        BindingSource binding_cbo_rcity = new BindingSource();
        BindingSource binding_Rfmd_id = new BindingSource();
        BindingSource binding_cb_Rnat_id = new BindingSource();
        BindingSource binding_Gender_id = new BindingSource();
        BindingSource binding_cur_bsrem = new BindingSource();
        BindingSource binding_Grd_cust_Reciever = new BindingSource();
        BindingSource rec_rem = new BindingSource();
        BindingSource _BS_sub_cust = new BindingSource();
        BindingSource binding_Cbo = new BindingSource();
        BindingSource binding_cmb_job_receiver = new BindingSource();
        BindingSource binding_cmb_resd = new BindingSource();
        BindingSource binding_Rem_Pay_Type = new BindingSource();
        DataTable Rpt_dt = new DataTable();
        string r_doc_exp_null = "";
        bool Chanage_Grd_det = false;
        bool Chanage_Grd_per_det = false;
        bool CHN = false;
        Int16 Md_ver_check = 0;
        Int16 MS_ver_check = 0;
        Int16 Md_ver_flag = 0;
        Int16 MS_ver_flag = 0;
        Decimal Cur_real_Amount = 0;
        int Cur_real_Id = 0;
        bool chang_procer = false;
        int procker = 0;
        string prockr_name = "";
        string procker_ename = "";
        Int16 btnlang = 2;
        string s_Ejob = "";
        string r_Ejob = "";
        string S_Social_No = "";
        string R_Social_No = "";
        string receiver_name_rem = "";
        string receiver_name_BL = "";
       // bool change_Rem_Pay_Type = false;
        public Delivery_Add_eng(string city_con_online, int city_con_online_id)
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grdrec_rem.AutoGenerateColumns = false;
            Grd_CustSen_Name.AutoGenerateColumns = false;
            name_city_con = city_con_online;
            Mcity_con_online_id = city_con_online_id;
            Grd_procer.AutoGenerateColumns = false;
        }

        //-----------------------------------------------
        private void Delivery_Add_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()
            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            #endregion
            string sql_report_txt = "select * from Reports_setting_Tbl";
            connection.SqlExec(sql_report_txt, "Reports_setting_Tbl");
            if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار اعدادات الطباعة" : "please add reporting setting", MyGeneral_Lib.LblCap);
                this.Close();
            }
            else
            {
                //  label77.Visible = false;
                Chanage_Grd_det = false;
                Cbo_Oper.SelectedIndex = 0;
                //if (TxtBox_User.Text == "")
                //{
                //    MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
                //    this.Dispose();
                //    this.Close();
                //    return;
                //}
                Txt_Doc_r_Exp.Format = DateTimePickerFormat.Custom;
                Txt_Doc_r_Exp.CustomFormat = @format;


                Txt_rbirth_Date.Format = DateTimePickerFormat.Custom;
                Txt_rbirth_Date.CustomFormat = @format;

                Txt_Doc_r_Date.Format = DateTimePickerFormat.Custom;
                Txt_Doc_r_Date.CustomFormat = @format;

                Txt_S_Birth.Format = DateTimePickerFormat.Custom;
                Txt_S_Birth.CustomFormat = @format;

                connection.SqlExec("Full_information_Web_inRem " + connection.Lang_id + "," + 30, "Full_information_tab_inRem");

                if (connection.SQLDS.Tables["Full_information_tab_inRem"].Rows.Count > 0)
                {

                    chang_rcity = false;
                    Txtcity_con_online.Text = name_city_con;
                    //-----------------------احضار الثواب
                    binding_cbo_rcity.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem"];
                    Cmb_R_City.DataSource = binding_cbo_rcity;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "ECity_Name";

                    Cmb_phone_Code_R.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem"];
                    Cmb_phone_Code_R.ValueMember = "COUN_K_PH";
                    Cmb_phone_Code_R.DisplayMember = "COUN_K_PH";
                    chang_rcity = true;
                    Cmb_R_City_SelectedIndexChanged(null, null);
                }
                if (connection.SQLDS.Tables["Full_information_tab_inRem1"].Rows.Count > 0)
                {
                    binding_Rfmd_id.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem1"];
                    Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                    Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_r_Doc_Type.DisplayMember = connection.Lang_id == 1 ? "Fmd_AName" : "Fmd_EName";
                }
                if (connection.SQLDS.Tables["Full_information_tab_inRem2"].Rows.Count > 0)
                {
                    //------------------
                    binding_cb_Rnat_id.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem2"];
                    Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = connection.Lang_id == 1 ? "Nat_AName" : "Nat_EName";
                }

                if (connection.SQLDS.Tables["Full_information_tab_inRem3"].Rows.Count > 0)
                {
                    binding_Gender_id.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem3"];
                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";

                }

                binding_cmb_resd.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem5"];

                resd_cmb.DataSource = binding_cmb_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = connection.Lang_id == 1 ? "resd_flag_rec_Aname" : "resd_flag_rec_Ename";



                binding_cmb_job_receiver.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem6"];

                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";


              //  change_Rem_Pay_Type = false;
                binding_Rem_Pay_Type.DataSource = connection.SQLDS.Tables["Full_information_tab_inRem7"];
                Cbo_Rem_Pay_Type.DataSource = binding_Rem_Pay_Type;
                Cbo_Rem_Pay_Type.ValueMember = "rem_pay_id";
                Cbo_Rem_Pay_Type.DisplayMember = connection.Lang_id == 1 ? "rem_pay_aname" : "rem_pay_ename";
               // change_Rem_Pay_Type = true;
               // Cbo_Rem_Pay_Type_SelectedIndexChanged(null, null);


                rec_rem.DataSource = connection.SQLDS.Tables["Delivery_Tbl"];
                Grdrec_rem.DataSource = rec_rem;



                Chanage_Grd_det = true;
                Grdrec_rem_SelectionChanged(null, null);
                //get_information();
                CHN = false;
                binding_Cbo.DataSource = connection.SQLDS.Tables["Delivery_Tbl"].DefaultView.ToTable(true, "R_cur_id", "R_ACUR_NAME", "R_ECUR_NAME").Select().CopyToDataTable();
                Cbo_Rcur.DataSource = binding_Cbo;
                Cbo_Rcur.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
                Cbo_Rcur.ValueMember = "R_cur_id";
                CHN = true;
                Cbo_Rcur_SelectedIndexChanged(null, null);
            }

        }
        //----------------------------------------------
        private void Cmb_R_City_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_rcity)
            {
                Cmb_phone_Code_R.DataBindings.Clear();
                Cmb_phone_Code_R.DataBindings.Add("SelectedValue", binding_cbo_rcity, "COUN_K_PH");
            }
        }
        //----------------------------------------------
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            chang_cur_rrate = false;
            if (Mr_cur_id != connection.Loc_Cur_Id)
            {

                //-----احضار اسعار الصرف
                if (checkBox1.Checked)
                {
                    Txt_Rem_Amount.Text = ((DataRowView)rec_rem.Current).Row["R_amount"].ToString();
                    Int16 Get_cust_id = 0;
                    Int16 Get_ACC_id = 0;
                    if (Cbo_Oper.SelectedIndex == 0)
                    {

                        Get_cust_id = Convert.ToInt16(TxtBox_User.Tag);
                        Get_ACC_id = 0;
                    }
                    else
                    {
                        if (Grd_procer.RowCount > 0)
                        {
                            Get_cust_id = Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["sub_cust_ID"]);
                            Get_ACC_id = Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Acc_ID"]);
                        }
                        else
                        {
                            MessageBox.Show(connection.Lang_id == 2 ? "you must choose the broker Customer " : "يجب اختيار الوسيط ", MyGeneral_Lib.LblCap);
                            return;
                        }
                    }

                    connection.SqlExec("Exec Get_Cur_Buy_Sal " + connection.T_ID + " , " + Get_cust_id + " , "
                                                               + Get_ACC_id, "Cur_Buy_Sal_Tbl");
                    if (connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"].Rows.Count > 0)
                    {


                        binding_cur_bsrem.DataSource = connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"]
                                          .DefaultView.ToTable(true, "for_cur_id", "Cur_AName", "Cur_EName", "Exch_rate", "Max_B_price",
                                          "Min_B_price", "cur_online_id", "Cur_Code")
                                          .Select(" for_cur_id = " + Mr_cur_id).CopyToDataTable();
                        cmb_cur.DataSource = binding_cur_bsrem;
                        cmb_cur.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
                        cmb_cur.ValueMember = "for_cur_id";

                        chang_cur_rrate = true;
                        cmb_cur_SelectedIndexChanged(null, null);




                    }
                }
                else
                {
                    txt_locamount_rem.ResetText();
                    txt_maxrate_rem.ResetText();
                    txt_minrate_rem.ResetText();
                    Txt_Rem_Amount.ResetText();
                    TxtDiscount_Amount.ResetText();
                    Txt_ExRate_Rem.ResetText();
                    cmb_cur.DataSource = new DataTable();

                }
            }

        }
        //----------------------------------------------
        private void cmb_cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_cur_rrate)
            {
                Txt_ExRate_Rem.DataBindings.Clear();
                txt_maxrate_rem.DataBindings.Clear();
                txt_minrate_rem.DataBindings.Clear();
                txt_locamount_rem.ResetText();

                param_Exch_rate_rem = Convert.ToDecimal(((DataRowView)binding_cur_bsrem.Current).Row["Exch_rate"]);
                Txt_ExRate_Rem.DataBindings.Add("Text", binding_cur_bsrem, "Exch_rate");
                txt_maxrate_rem.DataBindings.Add("Text", binding_cur_bsrem, "Max_B_price");
                txt_minrate_rem.DataBindings.Add("Text", binding_cur_bsrem, "Min_B_price");
                txt_locamount_rem.Text = (Convert.ToDecimal(Txt_ExRate_Rem.Text) * Convert.ToDecimal(Txt_Rem_Amount.Text)).ToString();
                // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
                Discount_Calculator();
                Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            }
        }
        //----------------------------------------------
        private void Discount_Calculator()//------الخصم
        {
            //Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            decimal Loc_Amount = Convert.ToDecimal(Convert.ToDecimal(txt_locamount_rem.Text));
            Mdiff_Amoun = 0;
            decimal x = 0;
            Mdiff_Amoun = Convert.ToDecimal(Loc_Amount % connection.Discount_Amount);

            x = (connection.Discount_Amount / 2);
            if (Mdiff_Amoun > x)
            {

                Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
            }
            else
            {
                Mdiff_Amoun = Mdiff_Amoun * -1; //----مكتسب
            }

            TxtDiscount_Amount.Text = Mdiff_Amoun.ToString();
        }
        //----------------------------------------------
        private void get_information()
        {

            cmb_Gender_id.DataBindings.Clear();
            Txtr_Suburb.DataBindings.Clear();
            Txt_R_Phone.DataBindings.Clear();
            cmb_job_receiver.DataBindings.Clear();
            Txtr_Street.DataBindings.Clear();
            Txtr_State.DataBindings.Clear();
            Txtr_Post_Code.DataBindings.Clear();
            //Txt_another_rec.DataBindings.Clear();
            Txt_rbirth_Date.DataBindings.Clear();
            Txt_r_Doc_No.DataBindings.Clear();
            Txt_Doc_r_Issue.DataBindings.Clear();
            Txt_Doc_r_Date.DataBindings.Clear();
            Txt_mail.DataBindings.Clear();
            txt_Mother_name.DataBindings.Clear();
            txt_rbirth_place.DataBindings.Clear();
            Cmb_R_City.DataBindings.Clear();
            Cmb_R_Nat.DataBindings.Clear();
            Cmb_r_Doc_Type.DataBindings.Clear();
            cmb_Gender_id.DataBindings.Clear();
            resd_cmb.DataBindings.Clear();
            Txt_R_Relation.DataBindings.Clear();
            Txt_notes.DataBindings.Clear();
            Txt_R_T_Purpose.DataBindings.Clear();
            Txt_Real_Paid_Name.DataBindings.Clear();
            Txt_R_details_job.DataBindings.Clear();



            R_per_id = 0;
            Txt_Social_No.Text = "";




            //Txt_another_rec.Text = ((DataRowView)rec_rem.Current).Row["R_EName"].ToString();
            cmb_Gender_id.DataBindings.Add("Text", rec_rem, "R_Gender_Aname");
            Txt_Reciever.Text = ((DataRowView)rec_rem.Current).Row["r_name"].ToString();
            Txtr_Post_Code.Text = ((DataRowView)rec_rem.Current).Row["r_Post_Code"].ToString();
            Txtr_State.Text = ((DataRowView)rec_rem.Current).Row["r_State"].ToString();
            Txtr_Street.Text = ((DataRowView)rec_rem.Current).Row["r_Street"].ToString();
            Txtr_Suburb.Text = ((DataRowView)rec_rem.Current).Row["r_Suburb"].ToString();
            Txt_rbirth_Date.Text = ((DataRowView)rec_rem.Current).Row["rbirth_date"].ToString();
            Txt_R_Phone.Text = ((DataRowView)rec_rem.Current).Row["r_phone"].ToString();
            //  Txt_R_job.Text = ((DataRowView)rec_rem.Current).Row["r_job"].ToString();
            Txt_r_Doc_No.Text = ((DataRowView)rec_rem.Current).Row["r_doc_no"].ToString();
            Txt_Doc_r_Issue.Text = ((DataRowView)rec_rem.Current).Row["r_doc_issue"].ToString();
            Txt_Doc_r_Date.Text = ((DataRowView)rec_rem.Current).Row["r_doc_ida"].ToString();
            Txt_Doc_r_Exp.Text = ((DataRowView)rec_rem.Current).Row["r_doc_eda"].ToString();
            Txt_mail.Text = ((DataRowView)rec_rem.Current).Row["r_Email"].ToString();
            txt_Mother_name.Text = ((DataRowView)rec_rem.Current).Row["R_Mother_name"].ToString();
            txt_rbirth_place.Text = ((DataRowView)rec_rem.Current).Row["TBirth_Place"].ToString();
            Cmb_r_Doc_Type.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["rFrm_doc_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["rFrm_doc_id"]);
            Cmb_R_City.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["r_city_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["r_city_id"]);
            Cmb_R_Nat.SelectedValue = Convert.ToString(((DataRowView)rec_rem.Current).Row["rnat_Id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)rec_rem.Current).Row["rnat_Id"]);
            Txt_Sender.Text = ((DataRowView)rec_rem.Current).Row["s_name"].ToString();
            txtS_Suburb.Text = ((DataRowView)rec_rem.Current).Row["S_Suburb"].ToString();
            Txts_street.Text = ((DataRowView)rec_rem.Current).Row["S_Street"].ToString();
            TxtS_State.Text = ((DataRowView)rec_rem.Current).Row["S_State"].ToString();
            TxtS_Post_Code.Text = ((DataRowView)rec_rem.Current).Row["S_Post_Code"].ToString();
            Txt_S_Phone.Text = ((DataRowView)rec_rem.Current).Row["S_phone"].ToString();
            Txt_job_Sender.Text = ((DataRowView)rec_rem.Current).Row["s_job"].ToString();
            Txts_nat.Text = ((DataRowView)rec_rem.Current).Row["s_A_NAT_NAME"].ToString();
            txts_city.Text = ((DataRowView)rec_rem.Current).Row["Sa_ACITY_NAME"].ToString();
            Txt_S_Birth.Text = ((DataRowView)rec_rem.Current).Row["sbirth_date"].ToString();
            Txt_Relionship.Text = ((DataRowView)rec_rem.Current).Row["Relation_S_R"].ToString();
            Txt_T_Purpose.Text = ((DataRowView)rec_rem.Current).Row["T_purpose"].ToString();
            Txt_Soruce_money.Text = ((DataRowView)rec_rem.Current).Row["Source_money"].ToString();

            Txt_R_Relation.Text = ((DataRowView)rec_rem.Current).Row["r_Relation"].ToString();
            Txt_notes.Text = ((DataRowView)rec_rem.Current).Row["r_notes"].ToString();
            Txt_R_T_Purpose.Text = ((DataRowView)rec_rem.Current).Row["R_T_purpose"].ToString();
            Txt_Real_Paid_Name.Text = ((DataRowView)rec_rem.Current).Row["Real_Paid_Name"].ToString();
            Txt_R_details_job.Text = ((DataRowView)rec_rem.Current).Row["r_details_job"].ToString();



            cmb_job_receiver.SelectedValue = 0;
            resd_cmb.SelectedValue = 0;
            check_Browser_Btn();
        }
        //----------------------------------------------
        private void get_information_per()
        {
            Txtr_Suburb.DataBindings.Clear();
            Txtr_Suburb.DataBindings.Clear();
            Txt_R_Phone.DataBindings.Clear();
            cmb_job_receiver.DataBindings.Clear();
            Txtr_Street.DataBindings.Clear();
            Txtr_State.DataBindings.Clear();
            Txtr_Post_Code.DataBindings.Clear();
            //Txt_another_rec.DataBindings.Clear();
            Txt_rbirth_Date.DataBindings.Clear();
            Txt_r_Doc_No.DataBindings.Clear();
            Txt_Doc_r_Issue.DataBindings.Clear();
            Txt_Doc_r_Date.DataBindings.Clear();
            Txt_mail.DataBindings.Clear();
            txt_Mother_name.DataBindings.Clear();
            txt_rbirth_place.DataBindings.Clear();
            Cmb_R_City.DataBindings.Clear();
            Cmb_R_Nat.DataBindings.Clear();
            Cmb_r_Doc_Type.DataBindings.Clear();
            cmb_Gender_id.DataBindings.Clear();
            Txt_Social_No.DataBindings.Clear();
            resd_cmb.DataBindings.Clear();

            Txt_R_details_job.DataBindings.Clear();


            Txt_R_Relation.Text = "";
            Txt_notes.Text = "";
            Txt_R_T_Purpose.Text = "";
            Txt_Real_Paid_Name.Text = "";

            R_per_id = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_id"]);
            Txt_R_Phone.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Phone"].ToString();
            Txtr_Post_Code.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Post_Code"].ToString();
            Txt_rbirth_Date.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Birth_day"].ToString();
            Txt_r_Doc_No.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_NO"].ToString();
            Txt_Doc_r_Exp.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_EDate"].ToString();
            Txt_Doc_r_Date.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_Date"].ToString();
            Txt_mail.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Email"].ToString();
            Cmb_r_Doc_Type.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_ID"]);
            Cmb_R_City.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_City_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_City_ID"]);
            Cmb_R_Nat.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Nationalty_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Nationalty_ID"]);
            cmb_job_receiver.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Occupation_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Occupation_id"]);
            Txt_Social_No.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Social_No"].ToString();
            Txt_R_details_job.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["details_job"].ToString();
            resd_cmb.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["resd_flag"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["resd_flag"]);
            cmb_Gender_id.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Gender_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Gender_ID"]);

            if (btnlang == 1)
            {
                //ar_sen.BackColor = System.Drawing.Color.LightGreen;
                //ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;
                //label54.Text = "";

                //if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString() != "")
                //{
                txt_Mother_name.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Mother_name"].ToString();
                txt_rbirth_place.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Birth_place"].ToString();
                cmb_Gender_id.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Gender_Aname");
                Txtr_Suburb.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Suburb"].ToString();
                cmb_job_receiver.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Occupation"].ToString();
                Txtr_Street.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Street"].ToString();
                Txtr_State.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_State"].ToString();
                //Txt_another_rec.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_eName"].ToString();
                Txt_Doc_r_Issue.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_IS"].ToString();
                //}
                //else
                //{
                //    btnlang = 2;
                //    ENAR_BTN_Click(null, null);


                //}
            }
            else
            {
                ar_sen.BackColor = System.Drawing.Color.WhiteSmoke;
                ENAR_BTN.BackColor = System.Drawing.Color.LightGreen;
                label38.Text = "";
                if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString() != "")
                {
                    txt_Mother_name.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["mother_Ename"].ToString();
                    txt_rbirth_place.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EBirth_place"].ToString();
                    cmb_Gender_id.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Gender_Ename");
                    Txtr_Suburb.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_ESuburb"].ToString();
                    cmb_job_receiver.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["EOccupation"].ToString();
                    Txtr_Street.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EStreet"].ToString();
                    Txtr_State.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EState"].ToString();
                    //Txt_another_rec.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_ename"].ToString();
                    Txt_Doc_r_Issue.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_EFrm_Doc_IS"].ToString();
                }
                else
                {
                    btnlang = 1;
                    ar_sen_Click(null, null);


                }
            }
            //Txtr_Suburb.DataBindings.Clear();
            //Txt_R_Phone.DataBindings.Clear();
            //Txt_R_job.DataBindings.Clear();
            //Txtr_Street.DataBindings.Clear();
            //Txtr_State.DataBindings.Clear();
            //Txtr_Post_Code.DataBindings.Clear();
            ////Txt_another_rec.DataBindings.Clear();
            //Txt_rbirth_Date.DataBindings.Clear();
            //Txt_r_Doc_No.DataBindings.Clear();
            //Txt_Doc_r_Issue.DataBindings.Clear();
            //Txt_Doc_r_Date.DataBindings.Clear();
            //Txt_mail.DataBindings.Clear();
            //txt_Mother_name.DataBindings.Clear();
            //txt_rbirth_place.DataBindings.Clear();
            //Cmb_R_City.DataBindings.Clear();
            //Cmb_R_Nat.DataBindings.Clear();
            //Cmb_r_Doc_Type.DataBindings.Clear();
            //cmb_Gender_id.DataBindings.Clear();

            //R_per_id = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_id"]);
            //Txtr_Suburb.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Suburb"].ToString();
            //Txt_R_Phone.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Phone"].ToString();
            //Txt_R_job.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Occupation"].ToString();
            //Txtr_Street.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Street"].ToString();
            //Txtr_State.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_State"].ToString();
            //Txtr_Post_Code.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Post_Code"].ToString();
            ////Txt_another_rec.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_eName"].ToString();
            //Txt_rbirth_Date.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Birth_day"].ToString();
            //Txt_r_Doc_No.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_NO"].ToString();
            //Txt_Doc_r_Issue.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_IS"].ToString();
            //Txt_Doc_r_Exp.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_EDate"].ToString();
            //Txt_Doc_r_Date.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_Date"].ToString();
            //Txt_mail.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Email"].ToString();
            //txt_Mother_name.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Mother_name"].ToString();
            //txt_rbirth_place.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Birth_place"].ToString();
            //Cmb_r_Doc_Type.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Frm_Doc_ID"]);
            //cmb_Gender_id.DataBindings.Add("Text", binding_Grd_cust_Reciever, "Per_Gender_Aname");
            //// cmb_Gender_id.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Gender_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Gender_ID"]);
            //Cmb_R_City.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_City_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_City_ID"]);
            //Cmb_R_Nat.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Nationalty_ID"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Nationalty_ID"]);
           
            check_Browser_Btn();
        }

        private void Grdrec_rem_SelectionChanged(object sender, EventArgs e)
        {
            if (Chanage_Grd_det)
            {

                chag_grd_rem = false;
                checkBox1.Enabled = false;
                Txt_rbirth_Date.DataBindings.Clear();
                Txt_sec_no.Text = "";


                //-------------------------
                Mr_cur_id = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["r_CUR_ID"]);
                Mp_cur_id = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["pr_CUR_ID"]);

                Md_ver_check = Convert.ToInt16(((DataRowView)rec_rem.Current).Row["d_ver_check"]);
                MS_ver_check = Convert.ToInt16(((DataRowView)rec_rem.Current).Row["s_ver_check"]);
                Md_ver_flag = Convert.ToInt16(((DataRowView)rec_rem.Current).Row["d_ver_flag"]);
                MS_ver_flag = Convert.ToInt16(((DataRowView)rec_rem.Current).Row["s_ver_flag"]);
                Rem_Flag = Convert.ToInt16(((DataRowView)rec_rem.Current).Row["rem_flag"]);
                check_confirm();
                // resd_cmb.SelectedIndex = 0;
                //if (Rem_Flag != 1)
                //{
                //    label76.Visible = true;
                //    Cbo_Oper.Visible = true;
                //}
                //else
                //{
                //    label76.Visible = false;
                //    Cbo_Oper.Visible = false;
                //}

                if (Mr_cur_id != connection.Loc_Cur_Id)
                {

                    checkBox1.Enabled = true;
                    checkBox1_CheckedChanged(null, null);
                    if (Cbo_Oper.SelectedIndex == 1)
                    { Txt_procer_TextChanged(null, null); }
                }
                else
                {
                    checkBox1.Enabled = false;
                    checkBox1.Checked = false;
                    txt_locamount_rem.ResetText();
                    txt_maxrate_rem.ResetText();
                    txt_minrate_rem.ResetText();
                    Txt_Rem_Amount.ResetText();
                    TxtDiscount_Amount.ResetText();
                    Txt_ExRate_Rem.ResetText();
                    cmb_cur.DataSource = new DataTable();
                }
                // --------------
                btnlang = 2;
                ENAR_BTN.Enabled = true;
                ENAR_BTN.BackColor = System.Drawing.Color.LightGreen;
                ar_sen.BackColor = System.Drawing.Color.WhiteSmoke;
                label38.Text = "";
                // --------------
                Sql_Txt = " Exec get_per_info " + "'" + ((DataRowView)rec_rem.Current).Row["r_name"] + "'";
                connection.SqlExec(Sql_Txt, "per_info_tbl_rec");

                if (connection.SQLDS.Tables["per_info_tbl_rec"].Rows.Count > 0)
                {
                    //-------------per_info
                    Grd_CustSen_Name.Columns["Column17"].DataPropertyName = "Per_EName";

                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = "Gender_Ename";


                    Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = "Nat_EName";

                    Cmb_R_City.DataSource = binding_cbo_rcity;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = "ECity_Name";

                    Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                    Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_r_Doc_Type.DisplayMember = "Fmd_EName";

                    cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                    cmb_job_receiver.ValueMember = "Job_ID";
                    cmb_job_receiver.DisplayMember = "Job_EName";



                    resd_cmb.DataSource = binding_cmb_resd;
                    resd_cmb.ValueMember = "resd_flag";
                    resd_cmb.DisplayMember = "resd_flag_rec_Ename";

                    //resd_cmb.Items.Clear();
                    //resd_cmb.Items.Add("Resident");
                    //resd_cmb.Items.Add("Non-Resident");
                    //resd_cmb.SelectedIndex = 0;

                    //--------------------------------------------
                    checkBox2.Enabled = true;
                    checkBox2.Checked = false;
                    Chanage_Grd_per_det = false;
                    binding_Grd_cust_Reciever.DataSource = connection.SQLDS.Tables["per_info_tbl_rec"];
                    Grd_CustSen_Name.DataSource = binding_Grd_cust_Reciever;
                    chag_grd_rem = true;
                    Chanage_Grd_per_det = true;
                    Grd_CustSen_Name_SelectionChanged(null, null);
                    //byte Count = 0;
                    //Count = Convert.ToByte(((DataRowView)binding_Grd_cust_Reciever.Current).Row["upload_flag"]);
                    //if (Count == 1)
                    //{ Btn_Browser.Enabled = true; }
                    //else
                    //{ Btn_Browser.Enabled = false; }
                    check_Browser_Btn();
                }
                else
                {
                    checkBox2.Checked = false;
                    checkBox2.Enabled = false;
                    Grd_CustSen_Name.DataSource = new DataTable();
                    //Btn_Browser.Enabled = false;
                    check_Browser_Btn();
                    get_information();
                }

                //try
                //{
                //    label77.Visible = true;
                //    connection.SqlExec("Exec no_rec_rem '" + ((DataRowView)rec_rem.Current).Row["r_Name"].ToString() + "'", "Get_count_Rem_tbl");
                //    if (connection.SQLDS.Tables["Get_count_Rem_tbl"].Rows.Count > 0)
                //    {

                //        label77.Text = " No. of receiving remittances for " + connection.SQLDS.Tables["Get_count_Rem_tbl1"].Rows[0]["Rem_Day"].ToString() + " days is " + connection.SQLDS.Tables["Get_count_Rem_tbl"].Rows.Count;
                //    }
                //    else
                //    { label77.Text = ""; }
                //}
                //catch
                //{ label77.Text = ""; }

            }
        }

        private void check_Browser_Btn()
        {
            if (connection.SQLDS.Tables["per_info_tbl_rec"].Rows.Count > 0)
            {
                byte Count = 0;
                Count = Convert.ToByte(((DataRowView)binding_Grd_cust_Reciever.Current).Row["upload_flag"]);
                if (Count == 1)
                { Btn_Browser.Enabled = true; }
                else
                { Btn_Browser.Enabled = false; }
            }
            else
            { Btn_Browser.Enabled = false; }
        }

        private void Grd_CustSen_Name_SelectionChanged(object sender, EventArgs e)
        {
            if (Chanage_Grd_per_det && checkBox2.Checked == true && connection.SQLDS.Tables["per_info_tbl_rec"].Rows.Count > 0)
            {
                get_information_per();
            }
            else
            {
                get_information();
            }
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                // Grd_CustSen_Name.Enabled = true; 
                ENAR_BTN.Enabled = true;

                get_information_per();

            }
            else
            {
                //Grd_CustSen_Name.Enabled = false; 
                ENAR_BTN.Enabled = true;
                label38.Text = "";

                get_information();
            }

        }
        private void Txt_sec_no_Leave(object sender, EventArgs e)
        {
            string Mcod_rem = ((DataRowView)rec_rem.Current).Row["Code_Rem"].ToString();
            if (Txt_sec_no.Text != Mcod_rem)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter code rem" : "ادخل رقم السري الصحيح", MyGeneral_Lib.LblCap);
                return;
            }

        }

        private void Txt_ExRate_Rem_TextChanged(object sender, EventArgs e)
        {
            txt_locamount_rem.Text = (Convert.ToDecimal(Txt_ExRate_Rem.Text) * Convert.ToDecimal(Txt_Rem_Amount.Text)).ToString();
            // Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(txt_locamount_comm.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
            Discount_Calculator();
            Txt_Tot_amount.Text = (Convert.ToDecimal(txt_locamount_rem.Text) + Convert.ToDecimal(TxtDiscount_Amount.Text)).ToString();
        }

        //private void Grd_CustRec_Name_SelectionChanged(object sender, EventArgs e)
        //{
        //    R_per_id = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_id"]);
        //    Txtr_Suburb.DataBindings.Clear();
        //    Txt_R_Phone.DataBindings.Clear();
        //    cmb_job_receiver.DataBindings.Clear();
        //    Txtr_Street.DataBindings.Clear();
        //    Txtr_State.DataBindings.Clear();
        //    Txtr_Post_Code.DataBindings.Clear();
        //   // Txt_another_rec.DataBindings.Clear();
        //    Cmb_R_City.DataBindings.Clear();
        //    Cmb_R_Nat.DataBindings.Clear();
        //    resd_cmb.DataBindings.Clear();

        //    Txtr_Suburb.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Suburb"].ToString();
        //    Txt_R_Phone.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Phone"].ToString();
        //  //  Txt_R_job.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Occupation"].ToString();
        //    Txtr_Street.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Street"].ToString();
        //    Txtr_State.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_State"].ToString();
        //    Txtr_Post_Code.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Post_Code"].ToString();
        //    //Txt_another_rec.Text = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_eName"].ToString();
        //    Cmb_R_City.SelectedValue = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_City_ID"]);
        //    Cmb_R_Nat.SelectedValue = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_Nationalty_ID"]);
        //    cmb_job_receiver.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Occupation_id"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_Occupation_id"]);
        //    resd_cmb.SelectedValue = Convert.ToString(((DataRowView)binding_Grd_cust_Reciever.Current).Row["resd_flag"]) == "" ? 0 : Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["resd_flag"]);

        //}
        private void check_confirm()
        {
            if (Rem_Flag != 1)
            {
                if (MS_ver_check == 1 && Md_ver_check == 1)
                {
                    if (MS_ver_flag == -1 && Md_ver_flag == 0)
                    {
                        label60.Text = "The remittance is unconfirmed from the source of the sender";
                    }
                    if (MS_ver_flag == 0 && Md_ver_flag == -1)
                    {
                        label60.Text = "The remittance is unconfirmed from the source of Receiver";

                    }
                    if (MS_ver_flag == -1 && Md_ver_flag == -1)
                    {
                        label60.Text = "The remittance is unconfirmed from both sides";
                    }

                    if (MS_ver_flag == 0 && Md_ver_flag == 0)
                    {
                        label60.Text = "";
                    }

                }
                if (MS_ver_check == 0 && Md_ver_check == 1)
                {

                    if (Md_ver_flag == -1)
                    {
                        label60.Text = "The remittance is unconfirmed from the source of Receiver";
                    }
                    if (Md_ver_flag == 0)
                    {
                        label60.Text = "";
                    }


                }
                if (MS_ver_check == 1 && Md_ver_check == 0)
                {

                    if (MS_ver_flag == -1)
                    {
                        label60.Text = "The remittance is unconfirmed from the source of the sender";
                    }

                    if (MS_ver_flag == 0)
                    {
                        label60.Text = "";
                    }

                }

                if (MS_ver_check == 0 && Md_ver_check == 0)
                {
                    label60.Text = "";
                }
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Btn_Add.Enabled = false;
            #region Validation

            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) == 0 && checkBox1.Checked == true)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب كتابة المعادل" : " Must Full the Exchange Rate", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (TxtBox_User.Text == "" && Cbo_Oper.SelectedIndex == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Cbo_Oper.SelectedIndex == 1 && Convert.ToInt16(Grd_procer.RowCount) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "you must choose the broker Customer " : "يجب اختيار الوسيط ", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;

            }
            //-----------------------------------
            string Mcod_rem = ((DataRowView)rec_rem.Current).Row["Code_Rem"].ToString();
            if (Txt_sec_no.Text != Mcod_rem)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter code rem" : "ادخل رقم السري الصحيح", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Rem_Flag != 1)
            {
                if (MS_ver_check == 1 && Md_ver_check == 1)
                {
                    if (MS_ver_flag == -1 && Md_ver_flag == 0)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You can not pay because they are uncertain from the source of the sender" : "لا يمكن الدفع لانها غير مؤكدة من مصدر المرسل", MyGeneral_Lib.LblCap);
                        Btn_Add.Enabled = true;
                        return;
                    }
                    if (MS_ver_flag == 0 && Md_ver_flag == -1)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You can not pay because they are uncertain from the source of receipt" : "لا يمكن الدفع لانها غير مؤكدة من مصدر الاستلام", MyGeneral_Lib.LblCap);
                        Btn_Add.Enabled = true;
                        return;
                    }
                    if (MS_ver_flag == -1 && Md_ver_flag == -1)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You can not pay because they are uncertain from both sides" : "لا يمكن الدفع لانها غير مؤكدة من الطرفين", MyGeneral_Lib.LblCap);
                        Btn_Add.Enabled = true;
                        return;
                    }
                }
                if (MS_ver_check == 0 && Md_ver_check == 1)
                {

                    if (Md_ver_flag == -1)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You can not pay because they are uncertain from the Payer" : "لا يمكن الدفع لانها غير مؤكدة قبل الدافع", MyGeneral_Lib.LblCap);
                        Btn_Add.Enabled = true;
                        return;
                    }

                }
                if (MS_ver_check == 1 && Md_ver_check == 0)
                {

                    if (MS_ver_flag == -1)
                    {
                        MessageBox.Show(connection.Lang_id == 2 ? "You can not pay for it are uncertain by the source" : "لا يمكن الدفع لانها غير مؤكدة قبل المصدر", MyGeneral_Lib.LblCap);
                        Btn_Add.Enabled = true;
                        return;
                    }

                }
            }

            //if (Txt_another_rec.Text == "" && binding_Grd_cust_Reciever.Count < 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter Other receiver Name" : "ادخل اسم المستلم الاخر", MyGeneral_Lib.LblCap);
            //    Btn_Add.Enabled = true;
            //    return;
            //}

            if (Txt_R_Phone.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter Phone No." : "ادخل رقم الهاتف", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txt_R_T_Purpose.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the purpose of rem." : "ادخــل غـــرض التحويــل", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txt_R_Relation.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the relation of receiver to sender " : "ادخــل علاقــة المستلــم بالمرســل", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txt_r_Doc_No.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter The Document No." : "ادخل رقم الوثيقة", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Txt_Doc_r_Issue.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the issue" : "ادخل محل اصدار الوثيقة", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Txtr_Street.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the suburb of the reciever" : "ادخل الحـــي", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Txtr_Street.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the  Street of the reciever " : "ادخل الزقاق", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (cmb_Gender_id.SelectedValue == null || Convert.ToInt16(cmb_Gender_id.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Gender" : "ادخل الجنـــس", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Convert.ToInt16(Cmb_R_Nat.SelectedValue) == 0 || Cmb_R_Nat.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Nationalty of receiver" : "ادخل جنسية المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Convert.ToInt16(Cmb_R_City.SelectedValue) == 0 || Cmb_R_City.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter city of receiver" : "اختر مدينة المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Convert.ToInt16(Cmb_r_Doc_Type.SelectedValue) <= 0 || Cmb_r_Doc_Type.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter type of the Document" : "اختر نوع وثيقة المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }


            if (Convert.ToInt16(cmb_job_receiver.SelectedValue) == 0 || cmb_job_receiver.SelectedValue == null)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the job" : "ادخل المهنة المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Txtr_Suburb.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the suburb of receiver" : "ادخل حــي المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txtr_Street.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the street of receiver" : "ادخل زقاق المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Txtr_State.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the State of receiver" : "ادخل ولاية المستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txt_sec_no.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter the Code of remittences" : "ادخل الرقم السري", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            //if (Txt_another_rec.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter the another name" : "ادخل الاسم الاخر", MyGeneral_Lib.LblCap);
            //    Btn_Add.Enabled = true;
            //    return;
            //}
            //if (txt_Mother_name.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Enter mother name" : "ادخل الاسم الام", MyGeneral_Lib.LblCap);
            //    return;
            //}
            if (txt_rbirth_place.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter Birth Palce" : "ادخل مكان الولادة", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txt_Doc_r_Exp.Checked == false)
            {
                Txt_Doc_r_Exp.Format = DateTimePickerFormat.Custom;
                r_doc_exp_null = Txt_Doc_r_Exp.CustomFormat = @format_null;
            }

            DateTime from_date = Convert.ToDateTime(DateTime.Now.ToString("d"));
            DateTime todate = Convert.ToDateTime(Txt_rbirth_Date.Value.Date);

            try
            {
                //string to = (rbirth_Date.Substring(0, 2).ToString() + "/" + rbirth_Date.Substring(2, 2).ToString() + "/" + rbirth_Date.Substring(6,4).ToString());
                // DateTime todate = Convert.ToDateTime(r_birth_date);
                TimeSpan difference = from_date - todate;
                if (Convert.ToDecimal(difference.TotalDays / 365.25) < 16)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "The age of  receiver smaller of legal age" : "عمر المستلم اصغر من السن القانوني", MyGeneral_Lib.LblCap);
                    Txt_rbirth_Date.Focus();
                    Btn_Add.Enabled = true;
                    return;
                }
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Enter receiver Birth Date" : "يرجى ادخال تاريخ التولد للمستلم", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Txt_Doc_r_Exp.Checked == true)
            {

                DateTime Expire = Convert.ToDateTime(Txt_Doc_r_Exp.Value.Date);

                if (Expire < from_date)
                {
                    MessageBox.Show(connection.Lang_id == 2 ? "Document expired" : "الوثيقةالمرسل منتهيةالصلاحية ", MyGeneral_Lib.LblCap);
                    Btn_Add.Enabled = true;
                    return;
                }
            }


            if (checkBox1.Checked == false && (Mr_cur_id != Mp_cur_id))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Delivery of the remmittance in local currency" : "تسليم الحوالة بالعملة المحلية", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }

            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) > Convert.ToDecimal(txt_maxrate_rem.Text))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Can not be a process because the Buy Price above the upper limit" : "لايمكن اجراء العملية لان سعر الشراء اعلى من الحد الاعلى", MyGeneral_Lib.LblCap);
                Btn_Add.Enabled = true;
                return;
            }
            if (Convert.ToDecimal(Txt_ExRate_Rem.Text) < Convert.ToDecimal(txt_minrate_rem.Text))
            {
                MessageBox.Show(connection.Lang_id == 2 ? "Please pay attention to Buy Price less than the minimum" : "يرجى الانتباة الى سعر الشراء اقل من الحد الادنى", MyGeneral_Lib.LblCap);


            }

            //if (Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue) == 2 && Txt_check_no.Text.Trim() == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 2 ? "Please enter the check number" : "يرجى ادخال رقم الصك", MyGeneral_Lib.LblCap);
            //    Btn_Add.Enabled = true;
            //    return;
            //}

            string s_doc_ida = MyGeneral_Lib.DateChecking(((DataRowView)rec_rem.Current).Row["s_doc_ida"].ToString());
            string s_doc_eda = MyGeneral_Lib.DateChecking(((DataRowView)rec_rem.Current).Row["s_doc_eda"].ToString());

            #endregion
            //int cur_id_pr = 0;
            //decimal Amount_r = 0;
            //if (checkBox1.Checked == true)
            //{
            //    cur_id_pr = Convert.ToInt32(Txt_Loc_Cur.Tag);
            //    Amount_r = Convert.ToDecimal(Txt_Tot_amount.Text);
            //    Cur_real_Id = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["r_CUR_ID"]);
            //    Cur_real_Amount = Convert.ToDecimal(((DataRowView)rec_rem.Current).Row["R_amount"]);

            //}
            //else
            //{
            //    cur_id_pr = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["r_CUR_ID"]);
            //    Amount_r = Convert.ToDecimal(((DataRowView)rec_rem.Current).Row["R_amount"]);
            //    Cur_real_Id = Convert.ToInt32(((DataRowView)rec_rem.Current).Row["r_CUR_ID"]);
            //    Cur_real_Amount = Convert.ToDecimal(((DataRowView)rec_rem.Current).Row["R_amount"]);

            //}

            //connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            //connection.SQLCMD.Parameters.AddWithValue("@Cur_id_Rem", cur_id_pr);
            //connection.SQLCMD.Parameters.AddWithValue("@Amount_Rem", Amount_r);
            //connection.SQLCMD.Parameters.AddWithValue("@Cur_id_Com", 0);
            //connection.SQLCMD.Parameters.AddWithValue("@Amount_Com", 0);
            //connection.SQLCMD.Parameters.AddWithValue("@T_id", connection.T_ID);
            //connection.SQLCMD.Parameters.AddWithValue("@oper_id", 8);
            //connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            //connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            //connection.SqlExec("Chk", connection.SQLCMD);
            //if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            //{
            //    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
            //    connection.SQLCMD.Parameters.Clear();
            //    Btn_Add.Enabled = true;
            //    return;
            //}
            connection.SQLCMD.Parameters.Clear();
            //-----------الاتصال بلمركز الحوالات
            string sen = ((DataRowView)rec_rem.Current).Row["S_name"].ToString();
            string rec = checkBox2.Checked == true ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim();
            string mv_notes = " :ح-مر" + sen + "," + " :مس " + rec;


            string rbirth_Date = Txt_rbirth_Date.Value.ToString("yyyy/MM/dd");
            string doc_r_date_ida = Txt_Doc_r_Date.Value.ToString("yyyy/MM/dd");
            string Doc_r_Exp = Txt_Doc_r_Exp.Value.ToString("yyyy/MM/dd");
            //------------
            try
            {


                connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
                Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

                if (connection.SQLDS.Tables.Contains("per_rem_blacklist_tbl"))
                {
                    connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
                }

                if (cond_bl_rem != 0)//في حال اختيار نوع البحث بلا من واجهة السيتنك فلا يبحث في قوائم البحث 
                {

                    DataTable Per_blacklist_tbl = new DataTable();
                    string[] Column4 = { "Per_AName", "Per_EName" };
                    string[] DType4 = { "System.String", "System.String" };

                    Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                    Per_blacklist_tbl.Rows.Clear();



                    Per_blacklist_tbl.Rows.Add((Grd_CustSen_Name.Rows.Count > 0 && checkBox2.Checked == true) ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim() ,"");
                    Per_blacklist_tbl.Rows.Add(Txt_Sender.Text.Trim(), Txt_Sender.Text.Trim());
                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist_conf]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                        connection.SQLCMD.Parameters.AddWithValue("@snat_Id", ((DataRowView)rec_rem.Current).Row["snat_Id"].ToString());
                        connection.SQLCMD.Parameters.AddWithValue("@rnat_Id", ((DataRowView)binding_cb_Rnat_id.Current).Row["nat_Id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sa_coun_id", ((DataRowView)rec_rem.Current).Row["sa_coun_id"].ToString());
                        connection.SQLCMD.Parameters.AddWithValue("@r_coun_id", ((DataRowView)binding_cbo_rcity.Current).Row["con_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@s_job", 0);
                        connection.SQLCMD.Parameters.AddWithValue("@r_job", ((DataRowView)binding_cmb_job_receiver.Current).Row["job_ID"]);
                        connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_id"]);
                        connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", ((DataRowView)rec_rem.Current).Row["sFrm_doc_id"]);


                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl1");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();

                    }
                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();

                    }
                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0 || connection.SQLDS.Tables["per_rem_blacklist_tbl1"].Rows.Count > 0)
                    {
                        Rem_Blacklist_confirm_ok AddFrm = new Rem_Blacklist_confirm_ok();
                        AddFrm.ShowDialog(this);
                        if (Rem_Blacklist_confirm_ok.Back_btn == 1)
                        {
                            Get_Black_list();
                            MessageBox.Show(connection.Lang_id == 1 ? "تم رفض الحوالة" : "Rimmetance has been refused", MyGeneral_Lib.LblCap);
                            Btn_Add.Enabled = true;
                            this.Close();
                            return;
                        }
                    }
                    else
                    {
                        Rem_Blacklist_confirm_ok.Back_btn = 0;
                        Rem_Blacklist_confirm_ok.Notes_BL = "";
                    }

                }
            }
            catch { }
            //try
            //{
            //    connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
            //    Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

            //    if (connection.SQLDS.Tables.Contains("per_rem_blacklist_tbl"))
            //    {
            //        connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
            //    }

            //    if (cond_bl_rem != 0)
            //    {

            //        if (Txt_Sender.Text == "" && Grd_CustSen_Name.RowCount <= 0)
            //        {
            //            receiver_name_BL = "";
            //        }

            //        else
            //        {
            //            receiver_name_BL = checkBox2.Checked == true ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim();
            //        }

            //        connection.SQLCS.Open();
            //        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
            //        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            //        connection.SQLCMD.Connection = connection.SQLCS;
            //        connection.SQLCMD.Parameters.AddWithValue("@search_name", receiver_name_BL);
            //        IDataReader obj = connection.SQLCMD.ExecuteReader();
            //        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
            //        obj.Close();
            //        connection.SQLCS.Close();
            //        connection.SQLCMD.Parameters.Clear();
            //        connection.SQLCMD.Dispose();

            //        if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
            //        {

            //            DialogResult Dr = MessageBox.Show("The Receiver's name is in the blacklists Do you want to continue ?", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //            if (Dr == DialogResult.Yes)
            //            {
            //                Rem_Blacklist AddFrm = new Rem_Blacklist();
            //                AddFrm.ShowDialog(this);
            //            }


            //            if (Dr == DialogResult.No)
            //            {
            //                Btn_Add.Enabled = true;
            //                return;
            //            }
            //        }
            //    }
            //}
            //catch
            //{ }
            //------------

            if (Cbo_Oper.SelectedIndex == 0)
            {
                ArrayList ItemList1 = new ArrayList();

                ItemList1.Insert(0, Mr_cur_id);
                ItemList1.Insert(1, ((DataRowView)rec_rem.Current).Row["R_amount"]);
                ItemList1.Insert(2, Txtr_State.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());
                ItemList1.Insert(3, Txt_r_Doc_No.Text);
                ItemList1.Insert(4, doc_r_date_ida);//Txt_Doc_r_Date.Value.Date.ToString(@format_invers1)
                ItemList1.Insert(5, Txt_Doc_r_Exp.Checked == false ? r_doc_exp_null : Doc_r_Exp);//Txt_Doc_r_Exp.Value.Date.ToString(@format_invers1)
                ItemList1.Insert(6, Txt_Doc_r_Issue.Text);
                ItemList1.Insert(7, Cmb_r_Doc_Type.SelectedValue);
                ItemList1.Insert(8, Cmb_R_Nat.SelectedValue);
                ItemList1.Insert(9, Txt_Reciever.Text.Trim());
                ItemList1.Insert(10, Txt_R_Phone.Text);
                ItemList1.Insert(11, Rem_Flag);
                ItemList1.Insert(12, connection.T_ID);
                ItemList1.Insert(13, connection.user_id);//user_id
                ItemList1.Insert(14, TxtIn_Rec_Date.Text);//nrec_date
                ItemList1.Insert(15, ((DataRowView)rec_rem.Current).Row["rem_no"]);
                ItemList1.Insert(16, mv_notes);
                ItemList1.Insert(17, rbirth_Date);////Txt_rbirth_Date.Value.Date.ToString(@format_invers1)
                ItemList1.Insert(18, ((DataRowView)rec_rem.Current).Row["rem_id"]);
                ItemList1.Insert(19, Txt_notes.Text.Trim());
                ItemList1.Insert(20, "2");
                ItemList1.Insert(21, connection.login_Id);//login_id
                ItemList1.Insert(22, connection.Cust_online_Id);
                ItemList1.Insert(23, Txt_sec_no.Text.Trim());
                ItemList1.Insert(24, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["t_city_ID"]));//--------------------------------------------------------مهم t_city_id
                ItemList1.Insert(25, Convert.ToInt32(((DataRowView)binding_cmb_job_receiver.Current).Row["Job_ID"]));
                ItemList1.Insert(26, Convert.ToInt32(Cmb_R_City.SelectedValue));//r_city_id 
                ItemList1.Insert(27, "");   // Param_Result rem_n
                ItemList1.Insert(28, "");   // Param_Result2 vo_no
                ItemList1.Insert(29, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//@Paid_Rate -----------------------------?
                ItemList1.Insert(30, Txtr_Street.Text.Trim());
                ItemList1.Insert(31, Txtr_Suburb.Text.Trim());
                ItemList1.Insert(32, Txtr_Post_Code.Text.Trim());
                ItemList1.Insert(33, Txtr_State.Text.Trim());
                ItemList1.Insert(34, R_per_id);//---@R_per_id
                ItemList1.Insert(35, "");//---@Er_name تم رفعه
                ItemList1.Insert(36, Txt_mail.Text.Trim());//---@@per_Email
                ItemList1.Insert(37, txt_Mother_name.Text.Trim());//---@per_Mother_name
                ItemList1.Insert(38, cmb_Gender_id.SelectedValue);//---@per_Gender_ID
                ItemList1.Insert(39, ((DataRowView)binding_cbo_rcity.Current).Row["Cit_AName"]);//---@r_ACITY_NAME
                ItemList1.Insert(40, ((DataRowView)binding_cbo_rcity.Current).Row["Cit_eName"]);//---@r_ECITY_NAME
                ItemList1.Insert(41, ((DataRowView)binding_cbo_rcity.Current).Row["Con_AName"]);//---@r_ACOUN_NAME
                ItemList1.Insert(42, ((DataRowView)binding_cbo_rcity.Current).Row["Con_eName"]);//---@r_ECOUN_NAME
                ItemList1.Insert(43, ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_AName"]);//---@rFRM_ADOC_NA
                ItemList1.Insert(44, ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_eName"]);//---@rFRM_EDOC_NA
                ItemList1.Insert(45, ((DataRowView)binding_cb_Rnat_id.Current).Row["Nat_AName"]);//---@r_A_NAT_NAME
                ItemList1.Insert(46, ((DataRowView)binding_cb_Rnat_id.Current).Row["Nat_eName"]);//---@r_E_NAT_NAME
                ItemList1.Insert(47, checkBox1.Checked == true ? 1 : 0);//---@@buy_sal_flag
                ItemList1.Insert(48, checkBox1.Checked == true ? param_Exch_rate_rem : 0);//---@rem_amount_exch
                ItemList1.Insert(49, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//-----@@rem_amount_real
                ItemList1.Insert(50, 0);//---@rem_amount_exch
                ItemList1.Insert(51, 0);//---@rem_amount_real
                ItemList1.Insert(52, connection.Loc_Cur_Id);//---@LOC_CUR_ID
                ItemList1.Insert(53, 1);//---@Catg_Id
                ItemList1.Insert(54, connection.Box_Cust_Id);//---@@cust_box_id
                ItemList1.Insert(55, Convert.ToDecimal(Txt_Tot_amount.Text.Trim()));//---@tot_loc_amount
                ItemList1.Insert(56, ((DataRowView)rec_rem.Current).Row["s_name"]);//---@s_name
                ItemList1.Insert(57, ((DataRowView)rec_rem.Current).Row["s_phone"]);//---@s_phone
                ItemList1.Insert(58, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["s_city_id"]));//---@s_city_id
                ItemList1.Insert(59, ((DataRowView)rec_rem.Current).Row["s_doc_no"]);//---@s_doc_no
                ItemList1.Insert(60, s_doc_ida == "0" || s_doc_ida == "-1" ? "" : s_doc_ida);//---@@s_doc_ida
                ItemList1.Insert(61, s_doc_eda == "0" || s_doc_eda == "-1" ? "" : s_doc_eda);//---@s_doc_eda
                ItemList1.Insert(62, ((DataRowView)rec_rem.Current).Row["s_doc_issue"]);//---@s_doc_issue
                ItemList1.Insert(63, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["sfrm_doc_id"]));//---@sfrm_doc_id
                ItemList1.Insert(64, ((DataRowView)rec_rem.Current).Row["snat_id"]);//---@snat_id
                ItemList1.Insert(65, ((DataRowView)rec_rem.Current).Row["t_purpose"]);//---@t_purpose
                ItemList1.Insert(66, ((DataRowView)rec_rem.Current).Row["sbirth_date"]);//---@sbirth_date
                ItemList1.Insert(67, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["Pr_cur_id"]));//---@Pr_cur_id
                ItemList1.Insert(68, ((DataRowView)rec_rem.Current).Row["sbirth_place"]);//---@sbirth_place
                ItemList1.Insert(69, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["sa_city_id"]));//---@sa_city_id
                ItemList1.Insert(70, 0);//---@job_sen
                ItemList1.Insert(71, ((DataRowView)rec_rem.Current).Row["Source_money"]);//---@Soruce_money
                ItemList1.Insert(72, ((DataRowView)rec_rem.Current).Row["Relation_S_R"]);//---@Relionship
                ItemList1.Insert(73, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["Case_purpose_id"]));//---@Case_purpose_id
                ItemList1.Insert(74, ((DataRowView)rec_rem.Current).Row["R_ACUR_NAME"]);//---@R_ACUR_NAME
                ItemList1.Insert(75, ((DataRowView)rec_rem.Current).Row["R_ECUR_NAME"]);//---@R_ECUR_NAME
                ItemList1.Insert(76, ((DataRowView)rec_rem.Current).Row["PR_ACUR_NAME"]);//---@PR_ACUR_NAME
                ItemList1.Insert(77, ((DataRowView)rec_rem.Current).Row["PR_ECUR_NAME"]);//---@PR_ECUR_NAME
                ItemList1.Insert(78, ((DataRowView)rec_rem.Current).Row["S_ACITY_NAME"]);//---@S_ACITY_NAME
                ItemList1.Insert(79, ((DataRowView)rec_rem.Current).Row["S_ECITY_NAME"]);//---@S_ECITY_NAME
                ItemList1.Insert(80, ((DataRowView)rec_rem.Current).Row["S_ACOUN_NAME"]);//---@S_ACOUN_NAME
                ItemList1.Insert(81, ((DataRowView)rec_rem.Current).Row["S_ECOUN_NAME"]);//---@S_ECOUN_NAME
                ItemList1.Insert(82, ((DataRowView)rec_rem.Current).Row["S_Street"]);//---@S_Street
                ItemList1.Insert(83, ((DataRowView)rec_rem.Current).Row["S_Suburb"]);//---@S_Suburb
                ItemList1.Insert(84, ((DataRowView)rec_rem.Current).Row["S_Post_Code"]);//---@S_Post_Code
                ItemList1.Insert(85, ((DataRowView)rec_rem.Current).Row["S_State"]);//---@S_State
                ItemList1.Insert(86, ((DataRowView)rec_rem.Current).Row["SFRM_ADOC_NA"]);//---@sFRM_ADOC_NA
                ItemList1.Insert(87, ((DataRowView)rec_rem.Current).Row["sFRM_EDOC_NA"]);//---@sFRM_EDOC_NA
                ItemList1.Insert(88, ((DataRowView)rec_rem.Current).Row["s_A_NAT_NAME"]);//---@s_A_NAT_NAME
                ItemList1.Insert(89, ((DataRowView)rec_rem.Current).Row["s_E_NAT_NAME"]);//---@s_E_NAT_NAME
                ItemList1.Insert(90, ((DataRowView)rec_rem.Current).Row["t_ACITY_NAME"]);//---@t_ACITY_NAME  
                ItemList1.Insert(91, ((DataRowView)rec_rem.Current).Row["t_ECITY_NAME"]);//---@t_ECITY_NAME
                ItemList1.Insert(92, ((DataRowView)rec_rem.Current).Row["Sa_ACITY_NAME"]);//---@Sa_ACITY_NAME
                ItemList1.Insert(93, ((DataRowView)rec_rem.Current).Row["Sa_ECITY_NAME"]);//---@Sa_ECITY_NAME
                ItemList1.Insert(94, ((DataRowView)rec_rem.Current).Row["Sa_ACOUN_NAME"]);//---@Sa_ACOUN_NAME
                ItemList1.Insert(95, ((DataRowView)rec_rem.Current).Row["Sa_ECOUN_NAME"]);//---@Sa_ECOUN_NAME
                ItemList1.Insert(96, ((DataRowView)rec_rem.Current).Row["t_ACOUN_NAME"]);//---@t_ACOUN_NAME
                ItemList1.Insert(97, ((DataRowView)rec_rem.Current).Row["t_ECOUN_NAME"]);//---@t_ECOUN_NAME
                ItemList1.Insert(98, ((DataRowView)rec_rem.Current).Row["Case_purpose_Aname"]);//---@Case_purpose_Aname
                ItemList1.Insert(99, ((DataRowView)rec_rem.Current).Row["Case_purpose_Ename"]);//---@Case_purpose_Ename
                ItemList1.Insert(100, Convert.ToDecimal(TxtDiscount_Amount.Text));//---@TxtDiscount_Amount
                ItemList1.Insert(101, txt_rbirth_place.Text);
                ItemList1.Insert(102, connection.User_Name);
                ItemList1.Insert(103, Delivery_Rem_Search.local_inter_flag);
                ItemList1.Insert(104, connection.Lang_id);
                ItemList1.Insert(105, ((DataRowView)rec_rem.Current).Row["S_Mother_name"]);
                ItemList1.Insert(106, ((DataRowView)rec_rem.Current).Row["S_EName"]);
                ItemList1.Insert(107, ((DataRowView)rec_rem.Current).Row["S_Gender_Aname"]);
                ItemList1.Insert(108, ((DataRowView)rec_rem.Current).Row["S_Gender_Ename"]);
                ItemList1.Insert(109, ((DataRowView)binding_Gender_id.Current).Row["Gender_Aname"]);
                ItemList1.Insert(110, ((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]);
                ItemList1.Insert(111, Convert.ToString(Cmb_phone_Code_R.SelectedValue));
                ItemList1.Insert(112, ((DataRowView)rec_rem.Current).Row["sender_Code_phone"]);
                ItemList1.Insert(113, ((DataRowView)rec_rem.Current).Row["S_Acust_name"]);
                ItemList1.Insert(114, ((DataRowView)rec_rem.Current).Row["S_ecust_name"]);
                ItemList1.Insert(115, ((DataRowView)rec_rem.Current).Row["s_Email"]);
                ItemList1.Insert(116, ((DataRowView)rec_rem.Current).Row["s_notes"]);
                ItemList1.Insert(117, connection.Loc_Cur_Id);
                ItemList1.Insert(118, Convert.ToInt16(resd_cmb.SelectedValue));
                ItemList1.Insert(119, btnlang);
                ItemList1.Insert(120, Txt_Social_No.Text);
                ItemList1.Insert(121, Txt_R_T_Purpose.Text.Trim() != "" ? Txt_R_T_Purpose.Text.Trim() : "");
                ItemList1.Insert(122, Txt_Real_Paid_Name.Text.Trim() != "" ? Txt_Real_Paid_Name.Text.Trim() : "");
                ItemList1.Insert(123, Txt_R_details_job.Text.Trim() != "" ? Txt_R_details_job.Text.Trim() : "");
                ItemList1.Insert(124, Txt_R_Relation.Text.Trim() != "" ? Txt_R_Relation.Text.Trim() : "");
                ItemList1.Insert(125, Rem_Blacklist_confirm_ok.Notes_BL);
                ItemList1.Insert(126, Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue));//rem_pay_id 
                ItemList1.Insert(127, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_aname"]);//---@rem_pay_aname
                ItemList1.Insert(128, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_ename"]);//---@rem_pay_ename
                ItemList1.Insert(129, Txt_Pay_Info.Text.Trim() == "" ? "" : Txt_Pay_Info.Text.Trim());//---@check_no
                //ItemList1.Insert(0, Mr_cur_id);
                //ItemList1.Insert(1, ((DataRowView)rec_rem.Current).Row["R_amount"]);
                //ItemList1.Insert(2, Txtr_State.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());
                //ItemList1.Insert(3, Txt_r_Doc_No.Text);
                //ItemList1.Insert(4, doc_r_date_ida);//Txt_Doc_r_Date.Value.Date.ToString(@format_invers1)
                //ItemList1.Insert(5, Txt_Doc_r_Exp.Checked == false ? r_doc_exp_null : Doc_r_Exp);//Txt_Doc_r_Exp.Value.Date.ToString(@format_invers1)
                //ItemList1.Insert(6, Txt_Doc_r_Issue.Text);
                //ItemList1.Insert(7, Cmb_r_Doc_Type.SelectedValue);
                //ItemList1.Insert(8, Cmb_R_Nat.SelectedValue);
                //ItemList1.Insert(9, Txt_Reciever.Text.Trim());
                //ItemList1.Insert(10, Txt_R_Phone.Text);
                //ItemList1.Insert(11, "11");
                //ItemList1.Insert(12, connection.T_ID);
                //ItemList1.Insert(13, connection.user_id);//user_id
                //ItemList1.Insert(14, TxtIn_Rec_Date.Text);//nrec_date
                //ItemList1.Insert(15, ((DataRowView)rec_rem.Current).Row["rem_no"]);
                //ItemList1.Insert(16, mv_notes);
                //ItemList1.Insert(17, rbirth_Date);////Txt_rbirth_Date.Value.Date.ToString(@format_invers1)
                //ItemList1.Insert(18, ((DataRowView)rec_rem.Current).Row["rem_id"]);
                //ItemList1.Insert(19, Txt_notes.Text.Trim());
                //ItemList1.Insert(20, "2");
                //ItemList1.Insert(21, connection.login_Id);//login_id
                //ItemList1.Insert(22, connection.Cust_online_Id);
                //ItemList1.Insert(23, Txt_sec_no.Text.Trim());
                //ItemList1.Insert(24, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["t_city_ID"]));//--------------------------------------------------------مهم t_city_id
                //ItemList1.Insert(25, Txt_R_job.Text.Trim());
                //ItemList1.Insert(26, Convert.ToInt32(Cmb_R_City.SelectedValue));//r_city_id 
                //ItemList1.Insert(27, "");   // Param_Result rem_n
                //ItemList1.Insert(28, "");   // Param_Result2 vo_no
                //ItemList1.Insert(29, 0);//@Paid_Rate -----------------------------?
                //ItemList1.Insert(30, Txtr_Street.Text.Trim());
                //ItemList1.Insert(31, Txtr_Suburb.Text.Trim());
                //ItemList1.Insert(32, Txtr_Post_Code.Text.Trim());
                //ItemList1.Insert(33, Txtr_State.Text.Trim());
                //ItemList1.Insert(34, R_per_id);//---@R_per_id
                //ItemList1.Insert(35, Txt_another_rec.Text.Trim());//---@Er_name
                //ItemList1.Insert(36, Txt_mail.Text.Trim());//---@@per_Email
                //ItemList1.Insert(37, txt_Mother_name.Text.Trim());//---@per_Mother_name
                //ItemList1.Insert(38, cmb_Gender_id.SelectedValue);//---@per_Gender_ID
                //ItemList1.Insert(39, ((DataRowView)binding_cbo_rcity.Current).Row["Cit_AName"]);//---@r_ACITY_NAME
                //ItemList1.Insert(40, ((DataRowView)binding_cbo_rcity.Current).Row["Cit_eName"]);//---@r_ECITY_NAME
                //ItemList1.Insert(41, ((DataRowView)binding_cbo_rcity.Current).Row["Con_AName"]);//---@r_ACOUN_NAME
                //ItemList1.Insert(42, ((DataRowView)binding_cbo_rcity.Current).Row["Con_eName"]);//---@r_ECOUN_NAME
                //ItemList1.Insert(43, ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_AName"]);//---@rFRM_ADOC_NA
                //ItemList1.Insert(44, ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_eName"]);//---@rFRM_EDOC_NA
                //ItemList1.Insert(45, ((DataRowView)binding_cb_Rnat_id.Current).Row["Nat_AName"]);//---@r_A_NAT_NAME
                //ItemList1.Insert(46, ((DataRowView)binding_cb_Rnat_id.Current).Row["Nat_eName"]);//---@r_E_NAT_NAME
                //ItemList1.Insert(47, checkBox1.Checked == true ? 1 : 0);//---@@buy_sal_flag
                //ItemList1.Insert(48, checkBox1.Checked == true ? param_Exch_rate_rem : 0);//---@rem_amount_exch
                //ItemList1.Insert(49, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//-----@@rem_amount_real
                //ItemList1.Insert(50, 0);//---@rem_amount_exch
                //ItemList1.Insert(51, 0);//---@rem_amount_real
                //ItemList1.Insert(52, connection.Loc_Cur_Id);//---@LOC_CUR_ID
                //ItemList1.Insert(53, 1);//---@Catg_Id
                //ItemList1.Insert(54, Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_id"]));//---@@brocker_id
                //ItemList1.Insert(55, Convert.ToDecimal(Txt_Tot_amount.Text.Trim()));//---@tot_loc_amount
                //ItemList1.Insert(56, ((DataRowView)rec_rem.Current).Row["s_name"]);//---@s_name
                //ItemList1.Insert(57, ((DataRowView)rec_rem.Current).Row["s_phone"]);//---@s_phone
                //ItemList1.Insert(58, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["s_city_id"]));//---@s_city_id
                //ItemList1.Insert(59, ((DataRowView)rec_rem.Current).Row["s_doc_no"]);//---@s_doc_no
                //ItemList1.Insert(60, s_doc_ida == "0" || s_doc_ida == "-1" ? "" : s_doc_ida);//---@@s_doc_ida
                //ItemList1.Insert(61, s_doc_eda == "0" || s_doc_eda == "-1" ? "" : s_doc_eda);//---@s_doc_eda
                //ItemList1.Insert(62, ((DataRowView)rec_rem.Current).Row["s_doc_issue"]);//---@s_doc_issue
                //ItemList1.Insert(63, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["sfrm_doc_id"]));//---@sfrm_doc_id
                //ItemList1.Insert(64, ((DataRowView)rec_rem.Current).Row["snat_id"]);//---@snat_id
                //ItemList1.Insert(65, ((DataRowView)rec_rem.Current).Row["t_purpose"]);//---@t_purpose
                //ItemList1.Insert(66, ((DataRowView)rec_rem.Current).Row["sbirth_date"]);//---@sbirth_date
                //ItemList1.Insert(67, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["Pr_cur_id"]));//---@Pr_cur_id
                //ItemList1.Insert(68, ((DataRowView)rec_rem.Current).Row["sbirth_place"]);//---@sbirth_place
                //ItemList1.Insert(69, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["sa_city_id"]));//---@sa_city_id
                //ItemList1.Insert(70, ((DataRowView)rec_rem.Current).Row["s_job"]);//---@job_sen
                //ItemList1.Insert(71, ((DataRowView)rec_rem.Current).Row["Source_money"]);//---@Soruce_money
                //ItemList1.Insert(72, ((DataRowView)rec_rem.Current).Row["Relation_S_R"]);//---@Relionship
                //ItemList1.Insert(73, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["Case_purpose_id"]));//---@Case_purpose_id
                //ItemList1.Insert(74, ((DataRowView)rec_rem.Current).Row["R_ACUR_NAME"]);//---@R_ACUR_NAME
                //ItemList1.Insert(75, ((DataRowView)rec_rem.Current).Row["R_ECUR_NAME"]);//---@R_ECUR_NAME
                //ItemList1.Insert(76, ((DataRowView)rec_rem.Current).Row["PR_ACUR_NAME"]);//---@PR_ACUR_NAME
                //ItemList1.Insert(77, ((DataRowView)rec_rem.Current).Row["PR_ECUR_NAME"]);//---@PR_ECUR_NAME
                //ItemList1.Insert(78, ((DataRowView)rec_rem.Current).Row["S_ACITY_NAME"]);//---@S_ACITY_NAME
                //ItemList1.Insert(79, ((DataRowView)rec_rem.Current).Row["S_ECITY_NAME"]);//---@S_ECITY_NAME
                //ItemList1.Insert(80, ((DataRowView)rec_rem.Current).Row["S_ACOUN_NAME"]);//---@S_ACOUN_NAME
                //ItemList1.Insert(81, ((DataRowView)rec_rem.Current).Row["S_ECOUN_NAME"]);//---@S_ECOUN_NAME
                //ItemList1.Insert(82, ((DataRowView)rec_rem.Current).Row["S_Street"]);//---@S_Street
                //ItemList1.Insert(83, ((DataRowView)rec_rem.Current).Row["S_Suburb"]);//---@S_Suburb
                //ItemList1.Insert(84, ((DataRowView)rec_rem.Current).Row["S_Post_Code"]);//---@S_Post_Code
                //ItemList1.Insert(85, ((DataRowView)rec_rem.Current).Row["S_State"]);//---@S_State
                //ItemList1.Insert(86, ((DataRowView)rec_rem.Current).Row["SFRM_ADOC_NA"]);//---@sFRM_ADOC_NA
                //ItemList1.Insert(87, ((DataRowView)rec_rem.Current).Row["sFRM_EDOC_NA"]);//---@sFRM_EDOC_NA
                //ItemList1.Insert(88, ((DataRowView)rec_rem.Current).Row["s_A_NAT_NAME"]);//---@s_A_NAT_NAME
                //ItemList1.Insert(89, ((DataRowView)rec_rem.Current).Row["s_E_NAT_NAME"]);//---@s_E_NAT_NAME
                //ItemList1.Insert(90, ((DataRowView)rec_rem.Current).Row["t_ACITY_NAME"]);//---@t_ACITY_NAME  
                //ItemList1.Insert(91, ((DataRowView)rec_rem.Current).Row["t_ECITY_NAME"]);//---@t_ECITY_NAME
                //ItemList1.Insert(92, ((DataRowView)rec_rem.Current).Row["Sa_ACITY_NAME"]);//---@Sa_ACITY_NAME
                //ItemList1.Insert(93, ((DataRowView)rec_rem.Current).Row["Sa_ECITY_NAME"]);//---@Sa_ECITY_NAME
                //ItemList1.Insert(94, ((DataRowView)rec_rem.Current).Row["Sa_ACOUN_NAME"]);//---@Sa_ACOUN_NAME
                //ItemList1.Insert(95, ((DataRowView)rec_rem.Current).Row["Sa_ECOUN_NAME"]);//---@Sa_ECOUN_NAME
                //ItemList1.Insert(96, ((DataRowView)rec_rem.Current).Row["t_ACOUN_NAME"]);//---@t_ACOUN_NAME
                //ItemList1.Insert(97, ((DataRowView)rec_rem.Current).Row["t_ECOUN_NAME"]);//---@t_ECOUN_NAME
                //ItemList1.Insert(98, ((DataRowView)rec_rem.Current).Row["Case_purpose_Aname"]);//---@Case_purpose_Aname
                //ItemList1.Insert(99, ((DataRowView)rec_rem.Current).Row["Case_purpose_Ename"]);//---@Case_purpose_Ename
                //ItemList1.Insert(100, Convert.ToDecimal(TxtDiscount_Amount.Text));//---@TxtDiscount_Amount
                //ItemList1.Insert(101, txt_rbirth_place.Text);
                //ItemList1.Insert(102, connection.User_Name);
                //ItemList1.Insert(103, Delivery_Rem_Search.local_inter_flag);
                //ItemList1.Insert(104, connection.Lang_id);
                //ItemList1.Insert(105, ((DataRowView)rec_rem.Current).Row["S_Mother_name"]);
                //ItemList1.Insert(106, ((DataRowView)rec_rem.Current).Row["S_EName"]);
                //ItemList1.Insert(107, ((DataRowView)rec_rem.Current).Row["S_Gender_Aname"]);
                //ItemList1.Insert(108, ((DataRowView)rec_rem.Current).Row["S_Gender_Ename"]);
                //ItemList1.Insert(109, ((DataRowView)binding_Gender_id.Current).Row["Gender_Aname"]);
                //ItemList1.Insert(110, ((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]);
                //ItemList1.Insert(111, Convert.ToString(Cmb_phone_Code_R.SelectedValue));
                //ItemList1.Insert(112, ((DataRowView)rec_rem.Current).Row["sender_Code_phone"]);
                //ItemList1.Insert(113, ((DataRowView)rec_rem.Current).Row["S_Acust_name"]);
                //ItemList1.Insert(114, ((DataRowView)rec_rem.Current).Row["S_ecust_name"]);
                //ItemList1.Insert(115, ((DataRowView)rec_rem.Current).Row["s_Email"]);
                //ItemList1.Insert(116, ((DataRowView)rec_rem.Current).Row["s_notes"]);

                MyGeneral_Lib.Copytocliptext("Add_rem_in_Voucher", ItemList1);
                connection.scalar("Add_rem_in_Voucher", ItemList1);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Btn_Add.Enabled = true;
                    return;

                }

                connection.SQLCMD.Parameters.Clear();

                //  print_Rpt_Pdf();
                print_Rpt_Pdf_NEW();
                Btn_Add.Enabled = true;
                this.Close();
            }
            else
            {

                ArrayList ItemList1 = new ArrayList();
                ItemList1.Insert(0, Mr_cur_id);
                ItemList1.Insert(1, ((DataRowView)rec_rem.Current).Row["R_amount"]);
                ItemList1.Insert(2, Txtr_State.Text.Trim() + '/' + Txtr_Street.Text.Trim() + '/' + Txtr_Suburb.Text.Trim() + '/' + Txtr_Post_Code.Text.Trim());
                ItemList1.Insert(3, Txt_r_Doc_No.Text);
                ItemList1.Insert(4, doc_r_date_ida);//Txt_Doc_r_Date.Value.Date.ToString(@format_invers1)
                ItemList1.Insert(5, Txt_Doc_r_Exp.Checked == false ? r_doc_exp_null : Doc_r_Exp);//Txt_Doc_r_Exp.Value.Date.ToString(@format_invers1)
                ItemList1.Insert(6, Txt_Doc_r_Issue.Text);
                ItemList1.Insert(7, Cmb_r_Doc_Type.SelectedValue);
                ItemList1.Insert(8, Cmb_R_Nat.SelectedValue);
                ItemList1.Insert(9, Txt_Reciever.Text.Trim());
                ItemList1.Insert(10, Txt_R_Phone.Text);
                ItemList1.Insert(11, Rem_Flag);
                ItemList1.Insert(12, connection.T_ID);
                ItemList1.Insert(13, connection.user_id);//user_id
                ItemList1.Insert(14, TxtIn_Rec_Date.Text);//nrec_date
                ItemList1.Insert(15, ((DataRowView)rec_rem.Current).Row["rem_no"]);
                ItemList1.Insert(16, mv_notes);
                ItemList1.Insert(17, rbirth_Date);////Txt_rbirth_Date.Value.Date.ToString(@format_invers1)
                ItemList1.Insert(18, ((DataRowView)rec_rem.Current).Row["rem_id"]);
                ItemList1.Insert(19, Txt_notes.Text.Trim());
                ItemList1.Insert(20, "2");
                ItemList1.Insert(21, connection.login_Id);//login_id
                ItemList1.Insert(22, connection.Cust_online_Id);
                ItemList1.Insert(23, Txt_sec_no.Text.Trim());
                ItemList1.Insert(24, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["t_city_ID"]));//--------------------------------------------------------مهم t_city_id
                ItemList1.Insert(25, Convert.ToInt32(((DataRowView)binding_cmb_job_receiver.Current).Row["Job_ID"]));
                ItemList1.Insert(26, Convert.ToInt32(Cmb_R_City.SelectedValue));//r_city_id 
                ItemList1.Insert(27, "");   // Param_Result rem_n
                ItemList1.Insert(28, "");   // Param_Result2 vo_no
                ItemList1.Insert(29, 0);//@Paid_Rate -----------------------------?
                ItemList1.Insert(30, Txtr_Street.Text.Trim());
                ItemList1.Insert(31, Txtr_Suburb.Text.Trim());
                ItemList1.Insert(32, Txtr_Post_Code.Text.Trim());
                ItemList1.Insert(33, Txtr_State.Text.Trim());
                ItemList1.Insert(34, R_per_id);//---@R_per_id
                ItemList1.Insert(35, "");//---@Er_name تم رفعه
                ItemList1.Insert(36, Txt_mail.Text.Trim());//---@@per_Email
                ItemList1.Insert(37, txt_Mother_name.Text.Trim());//---@per_Mother_name
                ItemList1.Insert(38, cmb_Gender_id.SelectedValue);//---@per_Gender_ID
                ItemList1.Insert(39, ((DataRowView)binding_cbo_rcity.Current).Row["Cit_AName"]);//---@r_ACITY_NAME
                ItemList1.Insert(40, ((DataRowView)binding_cbo_rcity.Current).Row["Cit_eName"]);//---@r_ECITY_NAME
                ItemList1.Insert(41, ((DataRowView)binding_cbo_rcity.Current).Row["Con_AName"]);//---@r_ACOUN_NAME
                ItemList1.Insert(42, ((DataRowView)binding_cbo_rcity.Current).Row["Con_eName"]);//---@r_ECOUN_NAME
                ItemList1.Insert(43, ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_AName"]);//---@rFRM_ADOC_NA
                ItemList1.Insert(44, ((DataRowView)binding_Rfmd_id.Current).Row["Fmd_eName"]);//---@rFRM_EDOC_NA
                ItemList1.Insert(45, ((DataRowView)binding_cb_Rnat_id.Current).Row["Nat_AName"]);//---@r_A_NAT_NAME
                ItemList1.Insert(46, ((DataRowView)binding_cb_Rnat_id.Current).Row["Nat_eName"]);//---@r_E_NAT_NAME
                ItemList1.Insert(47, checkBox1.Checked == true ? 1 : 0);//---@@buy_sal_flag
                ItemList1.Insert(48, 1);//---@Catg_Id
                ItemList1.Insert(49, 0);//---@@sub_cust_id    
                ItemList1.Insert(50, ((DataRowView)rec_rem.Current).Row["s_name"]);//---@s_name
                ItemList1.Insert(51, ((DataRowView)rec_rem.Current).Row["s_phone"]);//---@s_phone
                ItemList1.Insert(52, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["s_city_id"]));//---@s_city_id
                ItemList1.Insert(53, ((DataRowView)rec_rem.Current).Row["s_doc_no"]);//---@s_doc_no
                ItemList1.Insert(54, s_doc_ida == "0" || s_doc_ida == "-1" ? "" : s_doc_ida);//---@@s_doc_ida
                ItemList1.Insert(55, s_doc_eda == "0" || s_doc_eda == "-1" ? "" : s_doc_eda);//---@s_doc_eda
                ItemList1.Insert(56, ((DataRowView)rec_rem.Current).Row["s_doc_issue"]);//---@s_doc_issue
                ItemList1.Insert(57, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["sfrm_doc_id"]));//---@sfrm_doc_id
                ItemList1.Insert(58, ((DataRowView)rec_rem.Current).Row["snat_id"]);//---@snat_id
                ItemList1.Insert(59, ((DataRowView)rec_rem.Current).Row["t_purpose"]);//---@t_purpose
                ItemList1.Insert(60, ((DataRowView)rec_rem.Current).Row["sbirth_date"]);//---@sbirth_date
                ItemList1.Insert(61, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["Pr_cur_id"]));//---@Pr_cur_id
                ItemList1.Insert(62, ((DataRowView)rec_rem.Current).Row["sbirth_place"]);//---@sbirth_place
                ItemList1.Insert(63, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["sa_city_id"]));//---@sa_city_id
                ItemList1.Insert(64, 0);//---@job_sen تم الاستغناء عنه
                ItemList1.Insert(65, ((DataRowView)rec_rem.Current).Row["Source_money"]);//---@Soruce_money
                ItemList1.Insert(66, ((DataRowView)rec_rem.Current).Row["Relation_S_R"]);//---@Relionship
                ItemList1.Insert(67, Convert.ToInt32(((DataRowView)rec_rem.Current).Row["Case_purpose_id"]));//---@Case_purpose_id
                ItemList1.Insert(68, ((DataRowView)rec_rem.Current).Row["R_ACUR_NAME"]);//---@R_ACUR_NAME
                ItemList1.Insert(69, ((DataRowView)rec_rem.Current).Row["R_ECUR_NAME"]);//---@R_ECUR_NAME
                ItemList1.Insert(70, ((DataRowView)rec_rem.Current).Row["PR_ACUR_NAME"]);//---@PR_ACUR_NAME
                ItemList1.Insert(71, ((DataRowView)rec_rem.Current).Row["PR_ECUR_NAME"]);//---@PR_ECUR_NAME
                ItemList1.Insert(72, ((DataRowView)rec_rem.Current).Row["S_ACITY_NAME"]);//---@S_ACITY_NAME
                ItemList1.Insert(73, ((DataRowView)rec_rem.Current).Row["S_ECITY_NAME"]);//---@S_ECITY_NAME
                ItemList1.Insert(74, ((DataRowView)rec_rem.Current).Row["S_ACOUN_NAME"]);//---@S_ACOUN_NAME
                ItemList1.Insert(75, ((DataRowView)rec_rem.Current).Row["S_ECOUN_NAME"]);//---@S_ECOUN_NAME
                ItemList1.Insert(76, ((DataRowView)rec_rem.Current).Row["S_Street"]);//---@S_Street
                ItemList1.Insert(77, ((DataRowView)rec_rem.Current).Row["S_Suburb"]);//---@S_Suburb
                ItemList1.Insert(78, ((DataRowView)rec_rem.Current).Row["S_Post_Code"]);//---@S_Post_Code
                ItemList1.Insert(79, ((DataRowView)rec_rem.Current).Row["S_State"]);//---@S_State
                ItemList1.Insert(80, ((DataRowView)rec_rem.Current).Row["SFRM_ADOC_NA"]);//---@sFRM_ADOC_NA
                ItemList1.Insert(81, ((DataRowView)rec_rem.Current).Row["sFRM_EDOC_NA"]);//---@sFRM_EDOC_NA
                ItemList1.Insert(82, ((DataRowView)rec_rem.Current).Row["s_A_NAT_NAME"]);//---@s_A_NAT_NAME
                ItemList1.Insert(83, ((DataRowView)rec_rem.Current).Row["s_E_NAT_NAME"]);//---@s_E_NAT_NAME
                ItemList1.Insert(84, ((DataRowView)rec_rem.Current).Row["t_ACITY_NAME"]);//---@t_ACITY_NAME  
                ItemList1.Insert(85, ((DataRowView)rec_rem.Current).Row["t_ECITY_NAME"]);//---@t_ECITY_NAME
                ItemList1.Insert(86, ((DataRowView)rec_rem.Current).Row["Sa_ACITY_NAME"]);//---@Sa_ACITY_NAME
                ItemList1.Insert(87, ((DataRowView)rec_rem.Current).Row["Sa_ECITY_NAME"]);//---@Sa_ECITY_NAME
                ItemList1.Insert(88, ((DataRowView)rec_rem.Current).Row["Sa_ACOUN_NAME"]);//---@Sa_ACOUN_NAME
                ItemList1.Insert(89, ((DataRowView)rec_rem.Current).Row["Sa_ECOUN_NAME"]);//---@Sa_ECOUN_NAME
                ItemList1.Insert(90, ((DataRowView)rec_rem.Current).Row["t_ACOUN_NAME"]);//---@t_ACOUN_NAME
                ItemList1.Insert(91, ((DataRowView)rec_rem.Current).Row["t_ECOUN_NAME"]);//---@t_ECOUN_NAME
                ItemList1.Insert(92, ((DataRowView)rec_rem.Current).Row["Case_purpose_Aname"]);//---@Case_purpose_Aname
                ItemList1.Insert(93, ((DataRowView)rec_rem.Current).Row["Case_purpose_Ename"]);//---@Case_purpose_Ename
                //ItemList1.Insert(100, Convert.ToDecimal(TxtDiscount_Amount.Text));//---@TxtDiscount_Amount
                ItemList1.Insert(94, txt_rbirth_place.Text);
                ItemList1.Insert(95, connection.User_Name);
                ItemList1.Insert(96, Delivery_Rem_Search.local_inter_flag);
                ItemList1.Insert(97, connection.Lang_id);
                ItemList1.Insert(98, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//---@rem_amount_exch_cust سعر الحقيقي
                ItemList1.Insert(99, ((DataRowView)rec_rem.Current).Row["S_Mother_name"]);
                ItemList1.Insert(100, ((DataRowView)rec_rem.Current).Row["S_EName"]);
                ItemList1.Insert(101, ((DataRowView)rec_rem.Current).Row["S_Gender_Aname"]);
                ItemList1.Insert(102, ((DataRowView)rec_rem.Current).Row["S_Gender_Ename"]);
                ItemList1.Insert(103, ((DataRowView)binding_Gender_id.Current).Row["Gender_Aname"]);
                ItemList1.Insert(104, ((DataRowView)binding_Gender_id.Current).Row["Gender_Ename"]);
                ItemList1.Insert(105, Convert.ToString(Cmb_phone_Code_R.SelectedValue));
                ItemList1.Insert(106, ((DataRowView)rec_rem.Current).Row["sender_Code_phone"]);
                ItemList1.Insert(107, ((DataRowView)rec_rem.Current).Row["s_Email"]);
                ItemList1.Insert(108, ((DataRowView)rec_rem.Current).Row["s_notes"]);
                ItemList1.Insert(109, 0);   // Vo_No1 
                ItemList1.Insert(110, "");   // @Case_Date 
                ItemList1.Insert(111, ((DataRowView)rec_rem.Current).Row["S_Acust_name"]);
                ItemList1.Insert(112, ((DataRowView)rec_rem.Current).Row["S_ecust_name"]);
                ItemList1.Insert(113, Grd_procer.RowCount > 0 ? ((DataRowView)_BS_sub_cust.Current).Row["ASub_CustName"] : "");
                ItemList1.Insert(114, Grd_procer.RowCount > 0 ? ((DataRowView)_BS_sub_cust.Current).Row["ESub_CustNmae"] : "");
                ItemList1.Insert(115, connection.Loc_Cur_Id);//---@LOC_CUR_ID
                ItemList1.Insert(116, Grd_procer.RowCount > 0 ? Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["ACC_id"]) : 0);//??
                ItemList1.Insert(117, Convert.ToDecimal(Txt_Tot_amount.Text.Trim()));//---@tot_loc_amount
                ItemList1.Insert(118, checkBox1.Checked == true ? Convert.ToDecimal(Txt_ExRate_Rem.Text) : 0);//-----@@rem_amount_real
                ItemList1.Insert(119, 0);//-----@@com_amount_real
                ItemList1.Insert(120, checkBox1.Checked == true ? param_Exch_rate_rem : 0);//@rem_amount_exch
                ItemList1.Insert(121, 0);//-----@@com_amount_exch
                ItemList1.Insert(122, Grd_procer.RowCount > 0 ? Convert.ToInt16(((DataRowView)_BS_sub_cust.Current).Row["Sub_Cust_ID"]) : 0);
                ItemList1.Insert(123, "");
                ItemList1.Insert(124, "");
                ItemList1.Insert(125, connection.Loc_Cur_Id);
                ItemList1.Insert(126, Convert.ToInt16(resd_cmb.SelectedValue));
                ItemList1.Insert(127, btnlang);
                ItemList1.Insert(128, Txt_Social_No.Text);
                ItemList1.Insert(129, Txt_R_T_Purpose.Text.Trim() != "" ? Txt_R_T_Purpose.Text.Trim() : "");
                ItemList1.Insert(130, Txt_Real_Paid_Name.Text.Trim() != "" ? Txt_Real_Paid_Name.Text.Trim() : "");
                ItemList1.Insert(131, Txt_R_details_job.Text.Trim() != "" ? Txt_R_details_job.Text.Trim() : "");
                ItemList1.Insert(132, Txt_R_Relation.Text.Trim() != "" ? Txt_R_Relation.Text.Trim() : "");
                ItemList1.Insert(133, Rem_Blacklist_confirm_ok.Notes_BL);
                ItemList1.Insert(134, Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue));//rem_pay_id 
                ItemList1.Insert(135, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_aname"]);//---@rem_pay_aname
                ItemList1.Insert(136, ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_ename"]);//---@rem_pay_ename
                ItemList1.Insert(137, Txt_Pay_Info.Text.Trim() == "" ? "" : Txt_Pay_Info.Text.Trim());//---@check_no


                MyGeneral_Lib.Copytocliptext("Add_rem_in_Voucher_daily", ItemList1);
                connection.scalar("Add_rem_in_Voucher_daily", ItemList1);
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    Btn_Add.Enabled = true;
                    return;

                }

                connection.SQLCMD.Parameters.Clear();

                //  print_Rpt_Pdf();
                print_Rpt_Pdf_NEW();
                Btn_Add.Enabled = true;
                this.Close();

            }

        }

        private void print_Rpt_Pdf_NEW()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int int_nrecdate = 0;
            string Frm_id = "";
            User = TxtUser.Text;
            string phone = "";
            int procker = 0;
            string prockr_name = "";
            string procker_ename = "";
            string local_Cur1 = "";
            string local_ECur1 = "";
            string forgin_Cur1 = "";
            string forgin_ECur1 = "";
            //string Com_ToWord = "";
            //string Com_ToEWord = "";
            // double com_amount = 0;
            //string com_Cur_code = "";
             string Rem_Pay_Type_Aname = "";         
             string Rem_Pay_Type_Ename = "";
             int Rem_Pay_Type_id = 0;
 


            phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();
            Exch_rate_com = Txt_ExRate_Rem.Text;
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);

            rem_no = ((DataRowView)rec_rem.Current).Row["rem_no"].ToString();
            string SqlTxt = "Exec Report_Rem_Information_new" + "'" + rem_no + "'," + connection.T_ID;
            connection.SqlExec(SqlTxt, "Rem_Info");

            if (Cbo_Oper.SelectedIndex == 1)
            {
                if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0)
                {
                    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
                    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
                    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
                }

                //if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]) != 0)
                //{
                //    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_id"]);
                //    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_aname"].ToString();
                //    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_paid_ename"].ToString();
                //}

                //if (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0)
                //{
                //    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]);
                //    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_Aname"].ToString();
                //    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_ename"].ToString();
                //}
            }
            else
            {
                procker = 0;
                prockr_name = "";
                procker_ename = "";
            }

                local_Cur = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
                local_Cur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Aname"].ToString();
                local_ECur1 = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Ename"].ToString();

                forgin_Cur = connection.SQLDS.Tables["Rem_Info"].Rows[0]["Cur_Code"].ToString();
                forgin_Cur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ACUR_NAME"].ToString();
                forgin_ECur1 = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_ECUR_NAME"].ToString();

                s_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_Ejob"].ToString();
                r_Ejob = connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_Ejob"].ToString();
                S_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["S_Social_No"].ToString();
                R_Social_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Social_No"].ToString();




                Vo_No = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"]);

                if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000) //اصلي
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)- ";

                    //if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]) != 0)// في حالة وجود عمولة 
                    //{
                    //    com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ocomm"]);
                    //    com_Cur_code = connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_code"].ToString();
                    //    ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    //    Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ACUR_NAME"].ToString() + " لاغير)- ";
                    //    Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["s_ECUR_NAME"].ToString() + " not else.)- ";
                    //}
                }
                else// عملة محلية
                {
                    Rem_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]);
                    ToWord toWord = new ToWord(Rem_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";

                    //if (Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]) != 0)// في حالة وجود عمولة 
                    //{
                    //    com_amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["com_amount_cust"]);
                    //    com_Cur_code = local_Cur;
                    //    ToWord toWord_com = new ToWord(com_amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
                    //    Com_ToWord = "-(" + toWord_com.ConvertToArabic() + " " + local_Cur1 + " لاغير)- ";
                    //    Com_ToEWord = "-(" + toWord_com.ConvertToEnglish() + " " + local_ECur1 + " not else.)- ";
                    //}
                }

                // Vo_No = connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString();


                term = TxtTerm_Name.Text;


                //if
                //  ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) == 0) ||
                //   (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) == 0))
                //{
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //    {
                //        Oper_Id = 8;

                //    }
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //    {
                //        Oper_Id = 10;
                //    }
                //}

                //if
                //   ((Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]) != 0) ||
                //    (Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["sub_cust_id"]) != 0))
                //{
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) == 0.000))
                //    {
                //        Oper_Id = 22;

                //    }
                //    if ((Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000))
                //    {
                //        Oper_Id = 35;
                //    }
                //}

                Oper_Id = Convert.ToInt16(connection.SQLDS.Tables["Rem_Info"].Rows[0]["oper_id"]);
                try
                {

                    Delivery_Rem_rpt_1 ObjRpt = new Delivery_Rem_rpt_1();
                    Delivery_Rem_rpt_eng_1 ObjRptEng = new Delivery_Rem_rpt_eng_1();


                    DataTable Dt_Param = CustomControls.CustomParam_Dt();
                    Dt_Param.Rows.Add("_Date", _Date);
                    Dt_Param.Rows.Add("local_Cur", local_Cur);
                    Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
                    Dt_Param.Rows.Add("Vo_No", Vo_No);
                    Dt_Param.Rows.Add("Oper_Id", Oper_Id);
                    Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
                    Dt_Param.Rows.Add("User", User);
                    Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
                    Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
                    Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
                    Dt_Param.Rows.Add("term", term);
                    Dt_Param.Rows.Add("Frm_id", Frm_id);
                    Dt_Param.Rows.Add("phone", phone);
                    // Dt_Param.Rows.Add("com_cur", com_Cur_code);
                    //Dt_Param.Rows.Add("Com_ToWord", Com_ToWord);
                    //Dt_Param.Rows.Add("Com_ToEWord", Com_ToEWord);
                    Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
                    Dt_Param.Rows.Add("procker", procker);
                    Dt_Param.Rows.Add("procker_ename", procker_ename);
                    Dt_Param.Rows.Add("prockr_name", prockr_name);
                    Dt_Param.Rows.Add("s_Ejob", s_Ejob);
                    Dt_Param.Rows.Add("r_Ejob", r_Ejob);
                    Dt_Param.Rows.Add("S_Social_No", S_Social_No);
                    Dt_Param.Rows.Add("R_Social_No", R_Social_No);
                    Dt_Param.Rows.Add("Real_Paid_name", Txt_Real_Paid_Name.Text.Trim());
                    Dt_Param.Rows.Add("Anote_report", Anote_report);
                    Dt_Param.Rows.Add("Enote_report", Enote_report);
                    Dt_Param.Rows.Add("Rem_Pay_Type_Aname", ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_aname"]);
                    Dt_Param.Rows.Add("Rem_Pay_Type_Ename", ((DataRowView)binding_Rem_Pay_Type.Current).Row["rem_pay_ename"]);
                    Dt_Param.Rows.Add("Rem_Pay_Type_id", Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue));





                    connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
                    connection.SQLDS.Tables["Reports_setting_Tbl"].TableName = "Reports_setting_Tbl";
                    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);
                }
                catch
                { }
            
        }
        //private void print_Rpt_Pdf()
        //{
        //    //-------------------------report
        //    string phone = "";
        //    phone = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["PHONE"].ToString();
        //    string Cur_ToWord = "";
        //    string Cur_ToEWord = "";
        //    User = connection.User_Name;
        //    string forgin_ECur1 = "";
        //    string Frm_id = "";
        //    rem_no = ((DataRowView)rec_rem.Current).Row["rem_no"].ToString();
        //    _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
        //    //int_nrecdate = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);
        //    string SqlTxt = "Exec Report_Rem_Information " + "'" + rem_no + "'";
        //    connection.SqlExec(SqlTxt, "Rem_Info");
        //    procker = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_id"]);
        //    prockr_name = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_aname"].ToString();
        //    procker_ename = connection.SQLDS.Tables["Rem_Info"].Rows[0]["cust_broker_ename"].ToString();
        //    forgin_ECur1 = connection.SqlExec("select Cur_ENAME from Cur_Tbl "
        //          + "where Cur_id = " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Cur_Id"], "Cur_loc_tbl").Rows[0]["Cur_ENAME"].ToString();
        //    local_Cur = connection.SqlExec("select Cur_code from Cur_Tbl "
        //        + "where Cur_id = " + connection.Loc_Cur_Id, "Cur_loc_tbl").Rows[0]["Cur_code"].ToString();
        //    forgin_Cur = connection.SqlExec("select Cur_code from Cur_Tbl "
        //        + "where Cur_id = " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Cur_Id"], "Cur_loc_tbl").Rows[0]["Cur_code"].ToString();
        //    forgin_Cur1 = connection.SqlExec("select Cur_ANAME from Cur_Tbl "
        //        + "where Cur_id = " + connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_Cur_Id"], "Cur_loc_tbl").Rows[0]["Cur_ANAME"].ToString();
        //    Vo_No = Convert.ToInt32(connection.SQLDS.Tables["Rem_Info"].Rows[0]["vo_no"].ToString());
        //    param_Exch_rate_com = Convert.ToDecimal(Txt_ExRate_Rem.Text);
        //    //Convert.ToDecimal(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rate_online"].ToString());
        //    Exch_rate_com = (param_Exch_rate_com).ToString();
        //    double amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"].ToString());
        //    //double Com_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["r_ocomm"]);
        //    //Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["rem_amount_cust"]) != 0.000------- for check all
        //    term = TxtTerm_Name.Text;

        //    if (checkBox1.Checked == true)
        //    {
        //        Total_Amount = Convert.ToDouble(Txt_Tot_amount.Text);
        //        Rem_Amount = amount;

        //        Oper_Id = 10;

        //    }

        //    if (checkBox1.Checked == false)
        //    {
        //        //Rem_Amount = Convert.ToDouble(Txtr_amount.Text);
        //        Total_Amount = Convert.ToDouble(connection.SQLDS.Tables["Rem_Info"].Rows[0]["R_amount"]);

        //        Oper_Id = 8;
        //    }
        //    //} 
        //    //if (checkBox1.Checked = true && Convert.ToDouble(txt_locamount_rem.Text) != 0.000) 
        //    //{
        //    //}
        //    //if (checkBox1.Checked = true && Convert.ToDouble(txt_locamount_rem.Text) == 0.000)
        //    //{
        //    //}

        //    // if (Convert.ToInt16(Cmb_R_CUR_ID.SelectedValue) != Convert.ToInt16(Cmb_Comm_Cur.SelectedValue) && (checkBox1.Checked == false))
        //    //{
        //    //    Total_Amount = Convert.ToDouble(Txt_Tot_amount.Text);
        //    //}

        //    ToWord toWord = new ToWord(Total_Amount, new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));

        //    if (Oper_Id == 8)
        //    {
        //        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + forgin_Cur1 + " لاغير)- ";
        //        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + forgin_ECur1 + " not else.)-";
        //    }
        //    if (Oper_Id == 10)
        //    {
        //        Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
        //        Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + Txt_Loc_Cur.Text + " not else.)-";
        //    }

        //   // Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
        //    Delivery_Rem_rpt_eng ObjRpt = new Delivery_Rem_rpt_eng();
        //    Delivery_Rem_rpt_eng ObjRptEng = new Delivery_Rem_rpt_eng();
        //    DataTable Dt_Param = CustomControls.CustomParam_Dt();
        //    Dt_Param.Rows.Add("_Date", _Date);
        //    Dt_Param.Rows.Add("local_Cur", local_Cur);
        //    Dt_Param.Rows.Add("forgin_Cur", forgin_Cur);
        //    Dt_Param.Rows.Add("Vo_No", Vo_No);
        //    Dt_Param.Rows.Add("Oper_Id", Oper_Id);
        //    Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
        //    Dt_Param.Rows.Add("User", User);
        //    Dt_Param.Rows.Add("param_Exch_rate_rem", param_Exch_rate_com);
        //    Dt_Param.Rows.Add("Exch_rate_com", Exch_rate_com);
        //    Dt_Param.Rows.Add("Cur_ToWord", Cur_ToWord);
        //    Dt_Param.Rows.Add("Cur_ToEWord", Cur_ToEWord);
        //    Dt_Param.Rows.Add("Rem_Amount", Rem_Amount);
        //    Dt_Param.Rows.Add("Total_Amount", Total_Amount);
        //    Dt_Param.Rows.Add("term", term);
        //    Dt_Param.Rows.Add("Frm_id", Frm_id);
        //    Dt_Param.Rows.Add("phone", phone);
        //    Dt_Param.Rows.Add("procker", procker);
        //    Dt_Param.Rows.Add("procker_ename", procker_ename);
        //    Dt_Param.Rows.Add("prockr_name", prockr_name);

        //    //Dt_Param.Rows.Add("int_nrecdate", int_nrecdate);
        //    connection.SQLDS.Tables["Rem_Info"].TableName = "Rem_Info";
        //    Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_Id, true);

        //    this.Visible = false;
        //    RptLangFrm.ShowDialog(this);
        //    this.Visible = true;
        //    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Rem_Info"]);
        //}
        //------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------
        private void Delivery_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_rcity = false;
            chang_cur_rrate = false;
            chag_grd_rem = false;
            chang_procer = false;
            Chanage_Grd_det = false;
            Chanage_Grd_per_det = false;

            string[] Used_Tbl = { "Full_information_tab_inRem", "Full_information_tab_inRem1", "Full_information_tab_inRem2",
                                    "Full_information_tab_inRem3","Full_information_tab_inRem4" , "Delivery_Tbl", "Cur_Buy_Sal_Tbl",
                                "per_info_tbl_rec" ,"per_info_tbl_rec1" ,"Is_Archive_tbl"};

            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }



            Del_Btn = 0;
        }
        //------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            Int16 Flag_Archive = Convert.ToInt16(connection.SqlExec("Select  top(1) Flag_Archive from Companies ", "Is_Archive_tbl").Rows[0]["Flag_Archive"]);
            if (Flag_Archive == 1)// يملك خاصية رفع الوثائق
            {

                int Per_id = Convert.ToInt32(((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_id"]);
                string Per_AName = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString();
                string Per_EName = ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString();
                Del_Btn = 1;

                Person_Document_main UpdFrm = new Person_Document_main(Per_AName, Per_id, Per_EName);
                //this.Visible = false;
                UpdFrm.ShowDialog(this);
                //this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " الشركة لا تملك خاصية اضافة الوثائق" : " the company does not have permission to add document ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Cbo_Oper_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cbo_Oper.SelectedIndex == 0)
            {

                Txt_procer.Visible = false;
                label75.Visible = false;
                Grd_procer.Visible = false;
                label68.Visible = false;
                Txt_Accproc.Visible = false;
                TxtDiscount_Amount.Visible = true;
                label59.Visible = true;
                if (Txt_procer.Text != "")
                {
                    Txt_procer.Text = "";

                }
                checkBox1.Enabled = true;
            }
            if (Cbo_Oper.SelectedIndex == 1)
            {

                Txt_procer.Visible = true;
                label75.Visible = true;
                Grd_procer.Visible = true;
                label68.Visible = true;
                Txt_Accproc.Visible = true;
                TxtDiscount_Amount.Visible = false;
                label59.Visible = false;
                checkBox1.Enabled = false;
            }
            checkBox1.Checked = false;
        }

        private void Txt_procer_TextChanged(object sender, EventArgs e)
        {
            if (Txt_procer.Text != "")
            {
                string SqlTxt_sub = "";
                chang_procer = false;
                SqlTxt_sub = " Select  * From  Sub_CUSTOMERS_online where Cur_Id = " + Convert.ToInt32(((DataRowView)rec_rem.Current).Row["pr_CUR_ID"])
                   + " And ( ASub_CustName like '" + Txt_procer.Text + "%' or sub_cust_id like '" + Txt_procer.Text + "%') and t_id =" + connection.T_ID;
                _BS_sub_cust.DataSource = connection.SqlExec(SqlTxt_sub, "Sub_Cust_tbl");
                if (connection.SQLDS.Tables["Sub_Cust_tbl"].Rows.Count > 0)
                {
                    chang_procer = true;
                    Grd_procer.DataSource = _BS_sub_cust;

                }



            }

            else
            {
                Txt_Accproc.Text = "";
                chang_procer = false;
                Grd_procer.DataSource = new BindingSource();

            }
        }

        private void Grd_procer_SelectionChanged(object sender, EventArgs e)
        {
            if (chang_procer)
            {
                Txt_Accproc.Text = ((DataRowView)_BS_sub_cust.Current).Row["Acc_eName"].ToString();
            }
        }

        private void label77_Click(object sender, EventArgs e)
        {
            try
            {
                label77.Enabled = false;
                string R_birth_Date = Txt_rbirth_Date.Value.ToString("yyyy/MM/dd");

                string current_date = DateTime.Now.ToString("yyyy/MM/dd");

                if (R_birth_Date == current_date)
                {

                    R_birth_Date = "";
                }

                if (Txt_Reciever.Text == "" && Grd_CustSen_Name.RowCount <= 0)
                {
                    receiver_name_rem = "";
                }

                else
                {
                    receiver_name_rem = checkBox2.Checked == true ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim();
                }
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@name", receiver_name_rem);
                connection.SQLCMD.Parameters.AddWithValue("@s_doc_no", "");
                connection.SQLCMD.Parameters.AddWithValue("@s_Social_No", "");
                connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", Txt_r_Doc_No.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", Txt_Social_No.Text.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", Convert.ToInt16(Cmb_r_Doc_Type.SelectedValue));
                connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 1);
                connection.SQLCMD.Parameters.AddWithValue("@sbirth_date", "");
                connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", R_birth_Date.Trim());

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");
                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                label77.Enabled = true;
                if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
                {

                    Rem_Count AddFrm = new Rem_Count(2);
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label77.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            catch
            {
                label77.Enabled = true;
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            //try
            //{

            //    string R_birth_Date = Txt_rbirth_Date.Value.ToString("yyyy/MM/dd");

            //    string current_date = DateTime.Now.ToString("yyyy/MM/dd");

            //    if (R_birth_Date == current_date)
            //    {

            //        R_birth_Date = "";
            //    }

            //    if (Txt_Reciever.Text == "" && Grd_CustSen_Name.RowCount <= 0)
            //    {
            //        receiver_name_rem = "";
            //    }

            //    else
            //    {
            //        receiver_name_rem = checkBox2.Checked == true ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim();
            //    }
            //    connection.SQLCS.Open();
            //    connection.SQLCMD.CommandText = "[dbo].[similar_per_rem]";
            //    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            //    connection.SQLCMD.Connection = connection.SQLCS;
            //    connection.SQLCMD.CommandTimeout = 0;
            //    connection.SQLCMD.Parameters.AddWithValue("@name", receiver_name_rem);
            //    connection.SQLCMD.Parameters.AddWithValue("@s_doc_no", "");
            //    connection.SQLCMD.Parameters.AddWithValue("@s_Social_No", "");
            //    connection.SQLCMD.Parameters.AddWithValue("@sFrm_doc_id", 0);
            //    connection.SQLCMD.Parameters.AddWithValue("@r_doc_no", Txt_r_Doc_No.Text.Trim());
            //    connection.SQLCMD.Parameters.AddWithValue("@r_Social_No", Txt_Social_No.Text.Trim());
            //    connection.SQLCMD.Parameters.AddWithValue("@rFrm_doc_id", Convert.ToInt16(Cmb_r_Doc_Type.SelectedValue));
            //    connection.SQLCMD.Parameters.AddWithValue("@r_type_id", 1);
            //    connection.SQLCMD.Parameters.AddWithValue("@sbirth_date", "");
            //    connection.SQLCMD.Parameters.AddWithValue("@rbirth_date", R_birth_Date.Trim());

            //    IDataReader obj = connection.SQLCMD.ExecuteReader();
            //    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "similar_per_rem_tbl");
            //    obj.Close();
            //    connection.SQLCS.Close();
            //    connection.SQLCMD.Parameters.Clear();
            //    connection.SQLCMD.Dispose();

            //    if (connection.SQLDS.Tables["similar_per_rem_tbl"].Rows.Count > 0)
            //    {

            //        Rem_Count AddFrm = new Rem_Count(2);
            //        AddFrm.ShowDialog(this);
            //    }
            //    else
            //    {

            //        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
            //        return;
            //    }
            //}
            //catch
            //{   }
        }

        private void Cbo_Rcur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CHN)
            {
                Decimal Amount = 0;
                DataTable DT = new DataTable();
                DT = connection.SQLDS.Tables["Delivery_Tbl"].DefaultView.ToTable().Select("r_Cur_id = " + Cbo_Rcur.SelectedValue).CopyToDataTable();
                Amount = Convert.ToDecimal(DT.Compute("Sum(R_Amount)", ""));
                Tot_Amount.Text = Amount.ToString();
                Txt_RCount.Text = DT.Rows.Count.ToString();
            }
        }

        private void Print_All_Click(object sender, EventArgs e)
        {
            DataTable Dt_reveal_Delivery = new DataTable();

            Dt_reveal_Delivery = connection.SQLDS.Tables["Delivery_Tbl"].DefaultView.ToTable(false, "rem_no", "R_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME",
            connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA",
             "Case_Date", "S_name", "S_phone", "R_name", "R_phone").Select().CopyToDataTable();

            DataGridView[] Export_GRD = { Grdrec_rem };
            DataTable[] Export_DT = { Dt_reveal_Delivery };


            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void ar_sen_Click(object sender, EventArgs e)
        {
            if (Grd_CustSen_Name.Rows.Count != 0 && checkBox2.Checked == true)//اذا كان معرف مسبقا
            {
                if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_AName"].ToString() != "") //ويحتوي على اسم انكليزي
                {
                    btnlang = 1;
                    ar_sen.BackColor = System.Drawing.Color.LightGreen;
                    ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;

                    //chang_sender = true;
                    //   Grd_CustSen_Name_SelectionChanged(null, null);

                    Grd_CustSen_Name.Columns["Column17"].DataPropertyName = "Per_AName";

                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = "Gender_Aname";

                    Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = "Nat_AName";

                    Cmb_R_City.DataSource = binding_cbo_rcity;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = "ACity_Name";

                    Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                    Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_r_Doc_Type.DisplayMember = "Fmd_AName";

                    cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                    cmb_job_receiver.ValueMember = "Job_ID";
                    cmb_job_receiver.DisplayMember = "Job_AName";


                    resd_cmb.DataSource = binding_cmb_resd;
                    resd_cmb.ValueMember = "resd_flag";
                    resd_cmb.DisplayMember = "resd_flag_rec_Aname";

                    //resd_cmb.Items.Clear();
                    //resd_cmb.Items.Add("مقيم");
                    //resd_cmb.Items.Add("غير مقيم");
                    //resd_cmb.SelectedIndex = 0;


                }
                else// في حال لا يحتوي على per_aname
                {
                    ar_sen.Enabled = false;
                    label38.Text = "يرجى تعريف الاسم باللغة العربية";
                    btnlang = 2;
                }
            }
            else// في حال اسم جديد فقط تنكلب الكومبو
            {
                btnlang = 1;
                ar_sen.BackColor = System.Drawing.Color.LightGreen;
                ENAR_BTN.BackColor = System.Drawing.Color.WhiteSmoke;


                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = "Gender_Aname";

                Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = "Nat_AName";

                Cmb_R_City.DataSource = binding_cbo_rcity;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = "ACity_Name";

                Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_r_Doc_Type.DisplayMember = "Fmd_AName";

                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = "Job_AName";

                resd_cmb.DataSource = binding_cmb_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = "resd_flag_rec_Aname";

                //resd_cmb.Items.Clear();
                //resd_cmb.Items.Add("مقيم");
                //resd_cmb.Items.Add("غير مقيم");
                //resd_cmb.SelectedIndex = 0;
            }
        }

        private void ENAR_BTN_Click(object sender, EventArgs e)
        {
            //انكليزي

            if (Grd_CustSen_Name.Rows.Count != 0 && checkBox2.Checked == true)//اذا كان معرف مسبقا
            {
                if (((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString() != "") //ويحتوي على اسم انكليزي
                {
                    btnlang = 2;
                    ENAR_BTN.BackColor = System.Drawing.Color.LightGreen;
                    ar_sen.BackColor = System.Drawing.Color.WhiteSmoke;
                    // chang_sender = true;
                    Grd_CustSen_Name_SelectionChanged(null, null);
                    Grd_CustSen_Name.Columns["Column17"].DataPropertyName = "Per_EName";

                    cmb_Gender_id.DataSource = binding_Gender_id;
                    cmb_Gender_id.ValueMember = "Gender_id";
                    cmb_Gender_id.DisplayMember = "Gender_Ename";

                    Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                    Cmb_R_Nat.ValueMember = "Nat_ID";
                    Cmb_R_Nat.DisplayMember = "Nat_EName";

                    Cmb_R_City.DataSource = binding_cbo_rcity;
                    Cmb_R_City.ValueMember = "Cit_ID";
                    Cmb_R_City.DisplayMember = "ECity_Name";

                    Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                    Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                    Cmb_r_Doc_Type.DisplayMember = "Fmd_EName";

                    cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                    cmb_job_receiver.ValueMember = "Job_ID";
                    cmb_job_receiver.DisplayMember = "Job_EName";



                    resd_cmb.DataSource = binding_cmb_resd;
                    resd_cmb.ValueMember = "resd_flag";
                    resd_cmb.DisplayMember = "resd_flag_rec_Ename";



                    //resd_cmb.Items.Clear();
                    //resd_cmb.Items.Add("Resident");
                    //resd_cmb.Items.Add("Non-Resident");
                    //resd_cmb.SelectedIndex = 0;


                }
                else// في حال لا يحتوي على per_ename
                {
                    ENAR_BTN.Enabled = false;
                    //ENAR_BTN.Text = "No";
                    // ENAR_BTN.BackColor = System.Drawing.Color.Red;
                    label38.Text = "يرجى تعريف الاسم باللغة الانكليزية";
                    btnlang = 1;
                }
            }
            else// في حال اسم جديد فقط تنكلب الكومبو
            {
                btnlang = 2;
                ENAR_BTN.BackColor = System.Drawing.Color.LightGreen;
                ar_sen.BackColor = System.Drawing.Color.WhiteSmoke;


                cmb_Gender_id.DataSource = binding_Gender_id;
                cmb_Gender_id.ValueMember = "Gender_id";
                cmb_Gender_id.DisplayMember = "Gender_Ename";

                Cmb_R_Nat.DataSource = binding_cb_Rnat_id;
                Cmb_R_Nat.ValueMember = "Nat_ID";
                Cmb_R_Nat.DisplayMember = "Nat_EName";

                Cmb_R_City.DataSource = binding_cbo_rcity;
                Cmb_R_City.ValueMember = "Cit_ID";
                Cmb_R_City.DisplayMember = "ECity_Name";

                Cmb_r_Doc_Type.DataSource = binding_Rfmd_id;
                Cmb_r_Doc_Type.ValueMember = "Fmd_ID";
                Cmb_r_Doc_Type.DisplayMember = "Fmd_EName";

                cmb_job_receiver.DataSource = binding_cmb_job_receiver;
                cmb_job_receiver.ValueMember = "Job_ID";
                cmb_job_receiver.DisplayMember = "Job_EName";

                resd_cmb.DataSource = binding_cmb_resd;
                resd_cmb.ValueMember = "resd_flag";
                resd_cmb.DisplayMember = "resd_flag_rec_Ename";





                //resd_cmb.Items.Clear();
                //resd_cmb.Items.Add("Resident");
                //resd_cmb.Items.Add("Non-Resident");
                //resd_cmb.SelectedIndex = 0;
            }
        }


        private void label80_Click(object sender, EventArgs e)
        {

            try
            {

                label80.Enabled = false;
                DataTable Per_blacklist_tbl = new DataTable();
                string[] Column4 = { "Per_AName", "Per_EName" };
                string[] DType4 = { "System.String", "System.String" };

                Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);


                Per_blacklist_tbl.Rows.Clear();

                if (Grd_CustSen_Name.Rows.Count > 0)
                {
                    Per_blacklist_tbl.Rows.Add(((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString(), ((DataRowView)binding_Grd_cust_Reciever.Current).Row["Per_EName"].ToString());
                }
                else
                { Per_blacklist_tbl.Rows.Add(Txt_Sender.Text.Trim(), Txt_Sender.Text.Trim()); }

                try
                {
                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label80.Enabled = true;
                }
                catch
                {
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();
                    label80.Enabled = true;
                }
                if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
                {
                    Rem_Blacklist AddFrm = new Rem_Blacklist();
                    AddFrm.ShowDialog(this);
                }
                else
                {
                    label80.Enabled = true;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                    return;

                }
            }
            catch { label80.Enabled = true; }

            //try
            //{


            //    if (Txt_Sender.Text == "" && Grd_CustSen_Name.RowCount <= 0)
            //    {
            //        receiver_name_BL = "";
            //    }

            //    else
            //    {
            //        receiver_name_BL = checkBox2.Checked == true ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim();
            //    }

            //    connection.SQLCS.Open();
            //    connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
            //    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            //    connection.SQLCMD.Connection = connection.SQLCS;
            //    connection.SQLCMD.Parameters.AddWithValue("@search_name", receiver_name_BL);
            //    IDataReader obj = connection.SQLCMD.ExecuteReader();
            //    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
            //    obj.Close();
            //    connection.SQLCS.Close();
            //    connection.SQLCMD.Parameters.Clear();
            //    connection.SQLCMD.Dispose();

            //    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
            //    {
            //        Rem_Blacklist AddFrm = new Rem_Blacklist();
            //        AddFrm.ShowDialog(this);
            //    }
            //    else
            //    {
            //        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
            //        return;
            //    }
            //}
            //catch
            //{  }
        }
        //------------------------------------------------------
        
        private void Get_Black_list()
        {
            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Refuse_confirm_All]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@rem_no", ((DataRowView)rec_rem.Current).Row["rem_no"]);
                connection.SQLCMD.Parameters.AddWithValue("@case_id", 2);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_confirm_ok.Notes_BL.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@note_prog", "incoming Rem.");
                connection.SQLCMD.Parameters.AddWithValue("@oper_id", 0);
                connection.SQLCMD.Parameters.AddWithValue("@vo_no", 0);
                connection.SQLCMD.Parameters.AddWithValue("@per_id", (Grd_CustSen_Name.RowCount > 0 && checkBox2.Checked == true) ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row["per_id"] : 0);
                connection.SQLCMD.Parameters.AddWithValue("@for_cur_id", ((DataRowView)rec_rem.Current).Row["R_cur_id"]);
                connection.SQLCMD.Parameters.AddWithValue("@Bill_Amount",((DataRowView)rec_rem.Current).Row["R_amount"]);
                connection.SQLCMD.Parameters.AddWithValue("@Per_Name", (Grd_CustSen_Name.Rows.Count > 0 && checkBox2.Checked == true) ? ((DataRowView)binding_Grd_cust_Reciever.Current).Row[btnlang == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Reciever.Text.Trim());

                IDataReader obj = connection.SQLCMD.ExecuteReader();

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch
            {
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }
        }

        //private void Cbo_Rem_Pay_Type_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (change_Rem_Pay_Type)
        //    {
        //        if (Convert.ToInt16(Cbo_Rem_Pay_Type.SelectedValue) == 2)
        //        {

        //            label62.Visible = true;
        //            Txt_check_no.Visible = true;
        //        }

        //        else
        //        {
        //            label62.Visible = false;
        //            Txt_check_no.Visible = false;
        //        }
        //    }
        //}
    }
}