﻿namespace Integration_Accounting_Sys
{
    partial class Remittances_Query_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Ext = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Cmb_R_CUR_ID = new System.Windows.Forms.ComboBox();
            this.Cmb_Scity_ID = new System.Windows.Forms.ComboBox();
            this.Txt_R_name = new System.Windows.Forms.TextBox();
            this.Txtrem_no = new System.Windows.Forms.TextBox();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_Rec_Phone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_S_name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb_t_city = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Cmb_rem_case = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CboCust_Id = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Cmb_rem_type = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmb_range = new System.Windows.Forms.ComboBox();
            this.cmb_Loc_international = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_purpose = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_notes = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Cust_Check = new System.Windows.Forms.CheckBox();
            this.Chk_Cust = new System.Windows.Forms.CheckBox();
            this.Chk_Print = new System.Windows.Forms.CheckBox();
            this.Check = new System.Windows.Forms.CheckBox();
            this.User_Check = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.Txtr_amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Cbo_Data_Type = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(449, 125);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 627;
            this.label2.Text = "اسم المستلـــــم :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(449, 209);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 629;
            this.label4.Text = "المبلـــــــــــــغ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(449, 150);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(83, 16);
            this.label5.TabIndex = 631;
            this.label5.Text = "رقـم الحوالــــــة:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(3, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 16);
            this.label6.TabIndex = 634;
            this.label6.Text = "عملة الحوالــــة:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(3, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 638;
            this.label8.Text = "مدينة الارســال :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-3, 299);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(900, 2);
            this.flowLayoutPanel1.TabIndex = 641;
            // 
            // Btn_Ext
            // 
            this.Btn_Ext.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ext.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ext.Location = new System.Drawing.Point(445, 308);
            this.Btn_Ext.Name = "Btn_Ext";
            this.Btn_Ext.Size = new System.Drawing.Size(83, 29);
            this.Btn_Ext.TabIndex = 8;
            this.Btn_Ext.Text = "انهــــاء";
            this.Btn_Ext.UseVisualStyleBackColor = true;
            this.Btn_Ext.Click += new System.EventHandler(this.Btn_Ext_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(363, 308);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(83, 29);
            this.Btn_Add.TabIndex = 7;
            this.Btn_Add.Text = "موافــــق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Cmb_R_CUR_ID
            // 
            this.Cmb_R_CUR_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_CUR_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_CUR_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_CUR_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_CUR_ID.FormattingEnabled = true;
            this.Cmb_R_CUR_ID.Location = new System.Drawing.Point(90, 205);
            this.Cmb_R_CUR_ID.Name = "Cmb_R_CUR_ID";
            this.Cmb_R_CUR_ID.Size = new System.Drawing.Size(221, 24);
            this.Cmb_R_CUR_ID.TabIndex = 4;
            // 
            // Cmb_Scity_ID
            // 
            this.Cmb_Scity_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Scity_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Scity_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Scity_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Scity_ID.FormattingEnabled = true;
            this.Cmb_Scity_ID.Location = new System.Drawing.Point(90, 173);
            this.Cmb_Scity_ID.Name = "Cmb_Scity_ID";
            this.Cmb_Scity_ID.Size = new System.Drawing.Size(221, 24);
            this.Cmb_Scity_ID.TabIndex = 6;
            // 
            // Txt_R_name
            // 
            this.Txt_R_name.Location = new System.Drawing.Point(542, 124);
            this.Txt_R_name.MaxLength = 49;
            this.Txt_R_name.Name = "Txt_R_name";
            this.Txt_R_name.Size = new System.Drawing.Size(225, 20);
            this.Txt_R_name.TabIndex = 1;
            // 
            // Txtrem_no
            // 
            this.Txtrem_no.Location = new System.Drawing.Point(541, 148);
            this.Txtrem_no.MaxLength = 14;
            this.Txtrem_no.Name = "Txtrem_no";
            this.Txtrem_no.Size = new System.Drawing.Size(226, 20);
            this.Txtrem_no.TabIndex = 3;
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "yyyy/mm/dd";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(246, 43);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(110, 20);
            this.TxtToDate.TabIndex = 971;
            this.TxtToDate.ValueChanged += new System.EventHandler(this.TxtToDate_ValueChanged);
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "yyyy/mm/dd";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(93, 43);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(114, 20);
            this.TxtFromDate.TabIndex = 970;
            this.TxtFromDate.ValueChanged += new System.EventHandler(this.TxtFromDate_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(211, 46);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(36, 14);
            this.label1.TabIndex = 969;
            this.label1.Text = "الى :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(3, 46);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(87, 14);
            this.label11.TabIndex = 968;
            this.label11.Text = "تاريــخ الانشاء:";
            // 
            // Txt_Rec_Phone
            // 
            this.Txt_Rec_Phone.Location = new System.Drawing.Point(92, 148);
            this.Txt_Rec_Phone.MaxLength = 49;
            this.Txt_Rec_Phone.Name = "Txt_Rec_Phone";
            this.Txt_Rec_Phone.Size = new System.Drawing.Size(219, 20);
            this.Txt_Rec_Phone.TabIndex = 972;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(3, 150);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(86, 16);
            this.label3.TabIndex = 973;
            this.label3.Text = "هاتف المستلـــم :";
            // 
            // Txt_S_name
            // 
            this.Txt_S_name.Location = new System.Drawing.Point(91, 123);
            this.Txt_S_name.MaxLength = 49;
            this.Txt_S_name.Name = "Txt_S_name";
            this.Txt_S_name.Size = new System.Drawing.Size(220, 20);
            this.Txt_S_name.TabIndex = 974;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(3, 125);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 975;
            this.label9.Text = "اسم المرســــل:";
            // 
            // cmb_t_city
            // 
            this.cmb_t_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_t_city.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_t_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_t_city.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_t_city.FormattingEnabled = true;
            this.cmb_t_city.Location = new System.Drawing.Point(541, 173);
            this.cmb_t_city.Name = "cmb_t_city";
            this.cmb_t_city.Size = new System.Drawing.Size(226, 24);
            this.cmb_t_city.TabIndex = 976;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(449, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 16);
            this.label10.TabIndex = 977;
            this.label10.Text = "مدينة الاستلام :";
            // 
            // Cmb_rem_case
            // 
            this.Cmb_rem_case.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_rem_case.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_rem_case.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_rem_case.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_rem_case.FormattingEnabled = true;
            this.Cmb_rem_case.Location = new System.Drawing.Point(541, 70);
            this.Cmb_rem_case.Name = "Cmb_rem_case";
            this.Cmb_rem_case.Size = new System.Drawing.Size(226, 24);
            this.Cmb_rem_case.TabIndex = 978;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(449, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 16);
            this.label13.TabIndex = 979;
            this.label13.Text = "حـــالتـــــــــــها:";
            // 
            // CboCust_Id
            // 
            this.CboCust_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCust_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCust_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCust_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCust_Id.FormattingEnabled = true;
            this.CboCust_Id.Location = new System.Drawing.Point(92, 96);
            this.CboCust_Id.Name = "CboCust_Id";
            this.CboCust_Id.Size = new System.Drawing.Size(219, 24);
            this.CboCust_Id.TabIndex = 982;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(3, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 983;
            this.label14.Text = "الفـــرع/الوكــلاء:";
            // 
            // Cmb_rem_type
            // 
            this.Cmb_rem_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_rem_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_rem_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_rem_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_rem_type.FormattingEnabled = true;
            this.Cmb_rem_type.Items.AddRange(new object[] {
            "حوالة صادرة ",
            "حوالة واردة"});
            this.Cmb_rem_type.Location = new System.Drawing.Point(92, 69);
            this.Cmb_rem_type.Name = "Cmb_rem_type";
            this.Cmb_rem_type.Size = new System.Drawing.Size(219, 24);
            this.Cmb_rem_type.TabIndex = 980;
            this.Cmb_rem_type.SelectedIndexChanged += new System.EventHandler(this.Cmb_rem_type_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(3, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 16);
            this.label15.TabIndex = 981;
            this.label15.Text = "نوع الحوالــــة:";
            // 
            // cmb_range
            // 
            this.cmb_range.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_range.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_range.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_range.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_range.FormattingEnabled = true;
            this.cmb_range.Items.AddRange(new object[] {
            "يساوي",
            "فما فوق",
            "فما دون"});
            this.cmb_range.Location = new System.Drawing.Point(689, 205);
            this.cmb_range.Name = "cmb_range";
            this.cmb_range.Size = new System.Drawing.Size(78, 24);
            this.cmb_range.TabIndex = 984;
            // 
            // cmb_Loc_international
            // 
            this.cmb_Loc_international.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Loc_international.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Loc_international.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Loc_international.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Loc_international.FormattingEnabled = true;
            this.cmb_Loc_international.Items.AddRange(new object[] {
            "شبكة محلية",
            "شبكة Online "});
            this.cmb_Loc_international.Location = new System.Drawing.Point(541, 97);
            this.cmb_Loc_international.Name = "cmb_Loc_international";
            this.cmb_Loc_international.Size = new System.Drawing.Size(226, 24);
            this.cmb_Loc_international.TabIndex = 985;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(449, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 16);
            this.label7.TabIndex = 986;
            this.label7.Text = " مسار الحوالـــــة:";
            // 
            // txt_purpose
            // 
            this.txt_purpose.Location = new System.Drawing.Point(87, 238);
            this.txt_purpose.MaxLength = 49;
            this.txt_purpose.Name = "txt_purpose";
            this.txt_purpose.Size = new System.Drawing.Size(673, 20);
            this.txt_purpose.TabIndex = 987;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(3, 240);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(77, 16);
            this.label16.TabIndex = 988;
            this.label16.Text = "الغـــــــــــرض:";
            // 
            // txt_notes
            // 
            this.txt_notes.Location = new System.Drawing.Point(87, 264);
            this.txt_notes.MaxLength = 49;
            this.txt_notes.Name = "txt_notes";
            this.txt_notes.Size = new System.Drawing.Size(673, 20);
            this.txt_notes.TabIndex = 989;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(3, 266);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(76, 16);
            this.label17.TabIndex = 990;
            this.label17.Text = "التفاصيــــــــل:";
            // 
            // Cust_Check
            // 
            this.Cust_Check.AutoSize = true;
            this.Cust_Check.BackColor = System.Drawing.SystemColors.Control;
            this.Cust_Check.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cust_Check.ForeColor = System.Drawing.Color.Navy;
            this.Cust_Check.Location = new System.Drawing.Point(314, 123);
            this.Cust_Check.Name = "Cust_Check";
            this.Cust_Check.Size = new System.Drawing.Size(71, 20);
            this.Cust_Check.TabIndex = 991;
            this.Cust_Check.Text = "اينما وجد";
            this.Cust_Check.UseVisualStyleBackColor = false;
            // 
            // Chk_Cust
            // 
            this.Chk_Cust.AutoSize = true;
            this.Chk_Cust.BackColor = System.Drawing.SystemColors.Control;
            this.Chk_Cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Cust.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Cust.Location = new System.Drawing.Point(769, 124);
            this.Chk_Cust.Name = "Chk_Cust";
            this.Chk_Cust.Size = new System.Drawing.Size(71, 20);
            this.Chk_Cust.TabIndex = 992;
            this.Chk_Cust.Text = "اينما وجد";
            this.Chk_Cust.UseVisualStyleBackColor = false;
            // 
            // Chk_Print
            // 
            this.Chk_Print.AutoSize = true;
            this.Chk_Print.BackColor = System.Drawing.SystemColors.Control;
            this.Chk_Print.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Print.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Print.Location = new System.Drawing.Point(314, 148);
            this.Chk_Print.Name = "Chk_Print";
            this.Chk_Print.Size = new System.Drawing.Size(71, 20);
            this.Chk_Print.TabIndex = 993;
            this.Chk_Print.Text = "اينما وجد";
            this.Chk_Print.UseVisualStyleBackColor = false;
            // 
            // Check
            // 
            this.Check.AutoSize = true;
            this.Check.BackColor = System.Drawing.SystemColors.Control;
            this.Check.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check.ForeColor = System.Drawing.Color.Navy;
            this.Check.Location = new System.Drawing.Point(769, 238);
            this.Check.Name = "Check";
            this.Check.Size = new System.Drawing.Size(71, 20);
            this.Check.TabIndex = 994;
            this.Check.Text = "اينما وجد";
            this.Check.UseVisualStyleBackColor = false;
            // 
            // User_Check
            // 
            this.User_Check.AutoSize = true;
            this.User_Check.BackColor = System.Drawing.SystemColors.Control;
            this.User_Check.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User_Check.ForeColor = System.Drawing.Color.Navy;
            this.User_Check.Location = new System.Drawing.Point(770, 262);
            this.User_Check.Name = "User_Check";
            this.User_Check.Size = new System.Drawing.Size(71, 20);
            this.User_Check.TabIndex = 995;
            this.User_Check.Text = "اينما وجد";
            this.User_Check.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(681, 7);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(50, 14);
            this.label12.TabIndex = 998;
            this.label12.Text = "التاريـخ:";
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(392, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(254, 23);
            this.TxtUser.TabIndex = 997;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(323, 7);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(70, 14);
            this.label18.TabIndex = 996;
            this.label18.Text = "المستخدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(9, 3);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(296, 23);
            this.TxtTerm_Name.TabIndex = 1000;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-6, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(905, 1);
            this.flowLayoutPanel2.TabIndex = 642;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(733, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(152, 22);
            this.TxtIn_Rec_Date.TabIndex = 999;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // Txtr_amount
            // 
            this.Txtr_amount.BackColor = System.Drawing.Color.White;
            this.Txtr_amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtr_amount.Location = new System.Drawing.Point(541, 206);
            this.Txtr_amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtr_amount.Name = "Txtr_amount";
            this.Txtr_amount.NumberDecimalDigits = 3;
            this.Txtr_amount.NumberDecimalSeparator = ".";
            this.Txtr_amount.NumberGroupSeparator = ",";
            this.Txtr_amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txtr_amount.Size = new System.Drawing.Size(146, 23);
            this.Txtr_amount.TabIndex = 2;
            this.Txtr_amount.Text = "0.000";
            // 
            // Cbo_Data_Type
            // 
            this.Cbo_Data_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Data_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Data_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Data_Type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Data_Type.FormattingEnabled = true;
            this.Cbo_Data_Type.Items.AddRange(new object[] {
            "بـــيــــانـــات حالية",
            "بـــيــــانـــات مرحلة"});
            this.Cbo_Data_Type.Location = new System.Drawing.Point(541, 41);
            this.Cbo_Data_Type.Name = "Cbo_Data_Type";
            this.Cbo_Data_Type.Size = new System.Drawing.Size(226, 24);
            this.Cbo_Data_Type.TabIndex = 1168;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(421, 46);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label20.Size = new System.Drawing.Size(114, 16);
            this.label20.TabIndex = 1170;
            this.label20.Text = "نـــــوع الــبــيـــانــات :";
            // 
            // Remittances_Query_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 342);
            this.ControlBox = false;
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Cbo_Data_Type);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.User_Check);
            this.Controls.Add(this.Check);
            this.Controls.Add(this.Chk_Print);
            this.Controls.Add(this.Chk_Cust);
            this.Controls.Add(this.Cust_Check);
            this.Controls.Add(this.txt_notes);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txt_purpose);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cmb_Loc_international);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmb_range);
            this.Controls.Add(this.CboCust_Id);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Cmb_rem_type);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Cmb_rem_case);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cmb_t_city);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Txt_S_name);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Txt_Rec_Phone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txtrem_no);
            this.Controls.Add(this.Txt_R_name);
            this.Controls.Add(this.Txtr_amount);
            this.Controls.Add(this.Cmb_Scity_ID);
            this.Controls.Add(this.Cmb_R_CUR_ID);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Btn_Ext);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Remittances_Query_search";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "508";
            this.Text = "استعلام عن قيود الحوالات";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Remittances_Query_search_FormClosed);
            this.Load += new System.EventHandler(this.Remittances_Query_search_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button Btn_Ext;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.ComboBox Cmb_R_CUR_ID;
        private System.Windows.Forms.ComboBox Cmb_Scity_ID;
        private System.Windows.Forms.Sample.DecimalTextBox Txtr_amount;
        private System.Windows.Forms.TextBox Txt_R_name;
        private System.Windows.Forms.TextBox Txtrem_no;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_Rec_Phone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_S_name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmb_t_city;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Cmb_rem_case;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox CboCust_Id;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox Cmb_rem_type;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmb_range;
        private System.Windows.Forms.ComboBox cmb_Loc_international;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_purpose;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_notes;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox Cust_Check;
        private System.Windows.Forms.CheckBox Chk_Cust;
        private System.Windows.Forms.CheckBox Chk_Print;
        private System.Windows.Forms.CheckBox Check;
        private System.Windows.Forms.CheckBox User_Check;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ComboBox Cbo_Data_Type;
        private System.Windows.Forms.Label label20;
    }
}