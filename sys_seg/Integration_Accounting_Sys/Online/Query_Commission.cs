﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

namespace Integration_Accounting_Sys
{
    public partial class Query_Commission: Form
    {
       
        BindingSource Bs_Quey = new BindingSource();
        BindingSource binding_cbo_city = new BindingSource();
        BindingSource binding_tcity_id = new BindingSource();
        BindingSource binding_rcur = new BindingSource();
        BindingSource binding_pcur = new BindingSource();
        BindingSource binding_Query = new BindingSource();
        BindingSource binding_Query2 = new BindingSource();
        int cust_id_online = 0;
       
        

        public Query_Commission()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_comm_info.AutoGenerateColumns = false;
        }


        private void Query_Rem_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id == 2)
            {
                Cbo_R_Type_Id.Items[0] = "Out remittance";
                Cbo_R_Type_Id.Items[1] = "In remittance";

                Column2.DataPropertyName = "ECur_Name_rem";
                Column4.DataPropertyName = "ECur_Name_pay";
                Column1.DataPropertyName = "ECur_Name_comm";
                Column8.DataPropertyName = "Tecity_name";
                Column9.DataPropertyName = "ecity_name";
               
            }

            string SqlTxt = " Select  CUST_ID From  TERMINALS where t_id = " + connection.T_ID;
            connection.SqlExec(SqlTxt, "Cust_tbl");            
            cust_id_online = Convert.ToInt32(connection.SQLDS.Tables["Cust_tbl"].Rows[0]["CUST_ID"]);

            connection.SqlExec("exec Get_information_inrem " + cust_id_online, "Get_info_Tbl");
            binding_cbo_city.DataSource = connection.SQLDS.Tables["Get_info_Tbl"].Select("CITY_ID <>" + 0).CopyToDataTable();
            CmbScity.DataSource = binding_cbo_city;
            CmbScity.ValueMember = "CITY_ID";
            CmbScity.DisplayMember = connection.Lang_id == 1 ? "ACity_Name" : "eCity_Name";

            Cbo_R_Type_Id.SelectedIndex = 0;
            

         //connection.SqlExec("exec Search_Qurey_Rem "+ Cbo_R_Type_Id.SelectedIndex ,"Qurey_Rem_tbl");  
         
         //CmbRcity.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl1"];
         //CmbRcity.DisplayMember = connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME";
         //CmbRcity.ValueMember = "t_city_id";

         //Cmb_R_CUR.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl2"];
         //Cmb_R_CUR.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
         //Cmb_R_CUR.ValueMember = "R_Cur_Id";

         //Cmb_Pr_Cur_id.DataSource = connection.SQLDS.Tables["Qurey_Rem_tbl3"];
         //Cmb_Pr_Cur_id.DisplayMember = connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME";
         //Cmb_Pr_Cur_id.ValueMember = "PR_cur_id";

        string  SqlTxt1 = " exec Full_information_Web " + connection.Lang_id + "," + 0 + "," + cust_id_online;
            connection.SqlExec(SqlTxt1, "Full_information_tab");

            binding_tcity_id.DataSource = connection.SQLDS.Tables["Full_information_tab"];

            CmbRcity.DataSource = binding_tcity_id;
            CmbRcity.ValueMember = "Cit_ID";
            CmbRcity.DisplayMember = connection.Lang_id == 1 ? "acity_name" : "ecity_name";
            //----عملة الاصدار
           
            binding_rcur.DataSource = connection.SQLDS.Tables["Full_information_tab4"];
            Cmb_R_CUR.DataSource = binding_rcur;
            Cmb_R_CUR.ValueMember = "cur_id";
            Cmb_R_CUR.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
            //--------عملة الدفع
            
            binding_pcur.DataSource = connection.SQLDS.Tables["Full_information_tab4"];
            Cmb_Pr_Cur_id.DataSource = binding_pcur;
            Cmb_Pr_Cur_id.ValueMember = "cur_id";
            Cmb_Pr_Cur_id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
            //-------
       
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Cbo_R_Type_Id.SelectedIndex == 0)
            {
                if (connection.SQLDS.Tables["Query_comm_tbl"].Rows.Count > 0)
                {
                    DataTable DT = new DataTable();
                    DataTable Exp_Dt = connection.SQLDS.Tables["Query_comm_tbl"].DefaultView.ToTable(false, "R_MIN_AMNT", "R_MAX_AMNT", connection.Lang_id == 1 ? "ACur_Name_rem" : "ECur_Name_rem", connection.Lang_id == 1 ? "ACur_Name_pay" : "ECur_Name_pay", connection.Lang_id == 1 ? "ACur_Name_comm" : "ECur_Name_comm",
                        connection.Lang_id == 1 ? "Tacity_name" : "TEcity_name", connection.Lang_id == 1 ? "acity_name" : "Ecity_name", "comm_type", "OUT_COMM_CUT_CS", "OUT_COMM_PER_CS").Select().CopyToDataTable();

                    DataTable[] _EXP_DT = { Exp_Dt };
                    DataGridView[] Export_GRD = { Grd_comm_info };
                    RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, DT, Export_GRD, _EXP_DT, true, false, connection.Lang_id == 1 ? "كشف عمولات " : " Commission Reveal ");
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                }
                else
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عمولات" : " No commissions", MyGeneral_Lib.LblCap);
                    return;

                }
            }
            else
            {

                if (connection.SQLDS.Tables["Query_comm_tbl"].Rows.Count > 0)
                {
                    DataTable DT1 = new DataTable();
                    DataTable Exp_Dt1 = connection.SQLDS.Tables["Query_comm_tbl"].DefaultView.ToTable(false, "R_MIN_AMNT", "R_MAX_AMNT", connection.Lang_id == 1 ? "ACur_Name_rem" : "ECur_Name_rem", connection.Lang_id == 1 ? "ACur_Name_pay" : "ECur_Name_pay", connection.Lang_id == 1 ? "ACur_Name_comm" : "ECur_Name_comm",
                                                                                                      connection.Lang_id == 1 ? "Tacity_name" : "TEcity_name", connection.Lang_id == 1 ? "acity_name" : "Ecity_name", "comm_type", "IN_COMM_CUT_CS", "IN_COMM_PER_CS").Select().CopyToDataTable();

                    DataTable[] _EXP_DT1 = { Exp_Dt1 };
                    DataGridView[] Export_GRD1 = { Grd_comm_info };
                    RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(null, null, false, false, true, DT1, Export_GRD1, _EXP_DT1, true, false, connection.Lang_id == 1 ? "كشف عمولات " : " Commission Reveal ");
                    this.Visible = false;
                    RptLangFrm.ShowDialog(this);
                    this.Visible = true;
                }
                else
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عمولات" : " No commissions", MyGeneral_Lib.LblCap);
                    return;

                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            string Sqltexe = "EXec Query_comm_online_agent  " + connection.Cust_online_Id + "," + CmbRcity.SelectedValue + ","
                + Cmb_R_CUR.SelectedValue + "," + Cmb_Pr_Cur_id.SelectedValue + ","
                + CmbScity.SelectedValue + "," + Cbo_R_Type_Id.SelectedIndex + "," + Convert.ToDecimal(Txt_R_MIN_AMNT.Text) + "," + Convert.ToDecimal(Txt_R_MAX_AMNT.Text)+ "," + connection.Lang_id +","+ " ' ' ";

            connection.SqlExec(Sqltexe, "Query_comm_tbl");
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();

            if (connection.SQLDS.Tables["Query_comm_tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عمولات " : "No Commission", MyGeneral_Lib.LblCap);
                Grd_comm_info.DataSource = new BindingSource();
                return;
            }

            if (Cbo_R_Type_Id.SelectedIndex == 0)
            {
                binding_Query.DataSource = connection.SQLDS.Tables["Query_comm_tbl"];
                Grd_comm_info.DataSource = binding_Query;
                Column5.DataPropertyName = "OUT_COMM_CUT_CS";
                Column6.DataPropertyName = "OUT_COMM_PER_CS";
            }
            else
            {
                binding_Query2.DataSource = connection.SQLDS.Tables["Query_comm_tbl"];
                Grd_comm_info.DataSource = binding_Query2;
                Column5.DataPropertyName = "IN_COMM_CUT_CS";
                Column6.DataPropertyName = "IN_COMM_PER_CS";
            }
        }
   
        private void CmbScity_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void CmbRcity_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void Cmb_R_CUR_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void Cmb_Pr_Cur_id_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void Cbo_R_Type_Id_SelectedValueChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void Txt_R_MIN_AMNT_TextChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void Txt_R_MAX_AMNT_TextChanged(object sender, EventArgs e)
        {
            Grd_comm_info.DataSource = new BindingSource();
        }

        private void Query_Commission_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "Get_info_Tbl", "Full_information_tab", "Full_information_tab4", "Query_comm_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
            cust_id_online = 0;
        }

    }
}
