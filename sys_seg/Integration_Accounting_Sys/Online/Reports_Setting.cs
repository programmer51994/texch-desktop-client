﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Reports_Setting : Form
    {
        string sql_txt = "";
        BindingSource setting_bs = new BindingSource();
        public Reports_Setting()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }

        private void Reports_Setting_Load(object sender, EventArgs e)
        {
            sql_txt = " select * from Reports_setting_Tbl";
            connection.SqlExec(sql_txt, "Reports_setting_Tbl");
            if (connection.SQLDS.Tables["Reports_setting_Tbl"].Rows.Count > 0)
            {
                setting_bs.DataSource = connection.SQLDS.Tables["Reports_setting_Tbl"];


                Chk_S_Address.DataBindings.Clear();
                Chk_R_Address.DataBindings.Clear();
                Chk_S_Nat.DataBindings.Clear();
                Chk_R_Nat.DataBindings.Clear();
                Chk_S_Job.DataBindings.Clear();
                Chk_R_Job.DataBindings.Clear();
                Chk_S_social.DataBindings.Clear();
                Chk_R_social.DataBindings.Clear();
                Chk_Real_Paid_Name.DataBindings.Clear();
                Chk_Commission.DataBindings.Clear();
                Chk_T_city.DataBindings.Clear();
                Chk_S_City.DataBindings.Clear();
                Chk_R_City.DataBindings.Clear();
                Chk_s_notes.DataBindings.Clear();
                Chk_R_notes.DataBindings.Clear();
                chk_company_title.DataBindings.Clear();

                Chk_S_City.DataBindings.Add("Checked", setting_bs, "Chk_S_City");
                Chk_R_City.DataBindings.Add("Checked", setting_bs, "Chk_R_City");
                Chk_S_Address.DataBindings.Add("Checked", setting_bs, "Chk_S_Address");
                Chk_R_Address.DataBindings.Add("Checked", setting_bs, "Chk_R_Address");
                Chk_S_Nat.DataBindings.Add("Checked", setting_bs, "Chk_S_Nat");
                Chk_R_Nat.DataBindings.Add("Checked", setting_bs, "Chk_R_Nat");
                Chk_S_Job.DataBindings.Add("Checked", setting_bs, "Chk_S_Job");
                Chk_R_Job.DataBindings.Add("Checked", setting_bs, "Chk_R_Job");
                Chk_S_social.DataBindings.Add("Checked", setting_bs, "Chk_S_social");
                Chk_R_social.DataBindings.Add("Checked", setting_bs, "Chk_R_social");
                Chk_Real_Paid_Name.DataBindings.Add("Checked", setting_bs, "Chk_Real_Paid_Name");
                Chk_Commission.DataBindings.Add("Checked", setting_bs, "Chk_Commission");
                Chk_T_city.DataBindings.Add("Checked", setting_bs, "Chk_T_city");
                Chk_s_notes.DataBindings.Add("Checked", setting_bs, "Chk_s_notes");
                Chk_R_notes.DataBindings.Add("Checked", setting_bs, "Chk_R_notes");
                chk_company_title.DataBindings.Add("Checked", setting_bs, "chk_company_title");
            }


        }

        private void AddBtn_Click(object sender, EventArgs e)
        {

            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_City", Chk_S_City.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_R_City", Chk_R_City.Checked); 
            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Address", Chk_S_Address.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_R_Address", Chk_R_Address.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Nat", Chk_S_Nat.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_R_Nat", Chk_R_Nat.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Job", Chk_S_Job.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_R_Job", Chk_R_Job.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_social", Chk_S_social.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_R_social", Chk_R_social.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_Real_Paid_Name", Chk_Real_Paid_Name.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_Commission", Chk_Commission.Checked);       
            connection.SQLCMD.Parameters.AddWithValue("@Chk_T_city", Chk_T_city.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_s_notes", Chk_s_notes.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_R_notes", Chk_R_notes.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@chk_company_title", chk_company_title.Checked);
            connection.SQLCMD.Parameters.AddWithValue("@User_id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

            connection.SqlExec("Add_upd_Report_setting", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                return;
            }

            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();
            connection.SQLCMD.Parameters.Clear();

            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Reports_Setting_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Str = { "Reports_setting_Tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }
        }


    }
}
