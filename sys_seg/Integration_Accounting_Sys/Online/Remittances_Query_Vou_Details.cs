﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Remittances_Query_Vou_Details : Form
    {
        BindingSource binding_Grd1 = new BindingSource();
        BindingSource binding_Grd2 = new BindingSource();
        BindingSource binding_Grd3 = new BindingSource();
        BindingSource binding_Cbo = new BindingSource();
        bool Change_Grd1_details = false;
        bool Change_Grd2_details = false;
        bool Change_Bob = false;
        DataTable Voucher_Cur = new DataTable();

        DataTable DT_GRD1 = new DataTable();
        Int64 rem_id = 0;
        public Remittances_Query_Vou_Details()
        {
            InitializeComponent();
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_rem_view.AutoGenerateColumns = false;
            Grd_Rem_Cases.AutoGenerateColumns = false;
            Grd_rem_vo.AutoGenerateColumns = false;
        }

        private void Remittances_Inquery_main_Load(object sender, EventArgs e)
        {

            if (connection.SQLDS.Tables["TBL_Remittances_Query"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 2 ? "No Data fro this selection " : "لا توجد بيانات تحقق الشروط", MyGeneral_Lib.LblCap);
                this.Close();
            }
            Change_Grd1_details = false;
            binding_Grd1.DataSource = connection.SQLDS.Tables["TBL_Remittances_Query"];
            Grd_rem_view.DataSource = binding_Grd1;
            Change_Grd1_details = true;
            Grd_rem_view_SelectionChanged(null, null);

            Change_Bob = false;
            binding_Cbo.DataSource = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(true, "R_cur_id", "R_ACUR_NAME", "R_ECUR_NAME").Select().CopyToDataTable();
            Cbo_Rcur.DataSource = binding_Cbo;
            Cbo_Rcur.DisplayMember = connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME";
            Cbo_Rcur.ValueMember = "R_cur_id";
            Change_Bob = true;
            Cbo_Rcur_SelectedIndexChanged(null, null);
            if (connection.Lang_id == 2)
            {
                Column2.DataPropertyName = "R_ECUR_NAME";
                Column4.DataPropertyName = "PR_ECUR_NAME";
                Column8.DataPropertyName = "t_ECITY_NAME";
                Column9.DataPropertyName = "S_ECITY_NAME";
                Column5.DataPropertyName = "ECASE_NA";
                Column29.DataPropertyName = "s_E_NAT_NAME";
                Column33.DataPropertyName = "r_ECITY_NAME";
                Column34.DataPropertyName = "r_ECOUN_NAME";
                Column54.DataPropertyName = "s_ECUR_NAME";
                Column56.DataPropertyName = "c_ECUR_NAME";
                Column13.DataPropertyName = "e_Send_rem_flag";
                Column10.DataPropertyName = "E_local_international";
                dataGridViewTextBoxColumn2.DataPropertyName = "ECase_na";
                dataGridViewTextBoxColumn8.DataPropertyName = "Acc_EName";
                dataGridViewTextBoxColumn9.DataPropertyName = "Cur_ENAME";
            }
        }

        private void Grd_rem_view_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_Grd1_details)
            {

                TxtS_name.DataBindings.Clear();
                TxtS_phone.DataBindings.Clear();
                Txts_nat.DataBindings.Clear();
                TxtSacity.DataBindings.Clear();
                TxtS_address.DataBindings.Clear();
                Txt_Purpose.DataBindings.Clear();


                TxtS_name.DataBindings.Add("Text", binding_Grd1, "S_name");
                TxtS_phone.DataBindings.Add("Text", binding_Grd1, "S_phone");
                Txts_nat.DataBindings.Add("Text", binding_Grd1, connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME");
                TxtSacity.DataBindings.Add("Text", binding_Grd1, connection.Lang_id == 1 ? "Sa_ACITY_NAME" : "Sa_ECITY_NAME");
                TxtS_address.DataBindings.Add("Text", binding_Grd1, "S_address");
                Txt_Purpose.DataBindings.Add("Text", binding_Grd1, "T_purpose");

                Change_Grd2_details = false;
                rem_id = Convert.ToInt64(((DataRowView)binding_Grd1.Current).Row["rem_id"]);
                DataTable rem_case_dt = new DataTable();
                rem_case_dt = connection.SQLDS.Tables["TBL_Remittances_Query1"].Select().CopyToDataTable();
                DataView dv1 = rem_case_dt.DefaultView;
                dv1.Sort = "Nrec_Date1  ASC";
                DataTable sortedDT = dv1.ToTable();
                binding_Grd2.DataSource = rem_case_dt.DefaultView.ToTable(true, "vo_no", "Acase_Na", "Ecase_Na", "Nrec_Date1", "Nrec_Date", "User_Name", "Ref_id", "CASE_ID").Select("Ref_id = " + rem_id).CopyToDataTable();
                Grd_Rem_Cases.DataSource = binding_Grd2;
                Change_Grd2_details = true;
                Grd_Rem_Cases_SelectionChanged(null, null);

                
            }

        }

        private void Grd_Rem_Cases_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_Grd2_details)
            {
                DataTable DT_VOU = new DataTable();
                int vo_no = 0;
                int Nrec_Date = 0;
                int CASE_ID = 0;
                vo_no = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["vo_no"]);
               // DateTime   Nrec_Date1 = Convert.ToDateTime(((DataRowView)binding_Grd2.Current).Row["Nrec_Date1"]);
                Nrec_Date = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["Nrec_Date"]);
                CASE_ID = Convert.ToInt32(((DataRowView)binding_Grd2.Current).Row["CASE_ID"]);


                DT_VOU = connection.SQLDS.Tables["TBL_Remittances_Query1"].DefaultView.ToTable().Select("Ref_id = " + rem_id + "and Vo_no = " + vo_no + "and Nrec_Date = " + Nrec_Date + " and CASE_ID = " + CASE_ID).CopyToDataTable();
                binding_Grd3.DataSource = DT_VOU;
                Grd_rem_vo.DataSource = binding_Grd3;

                txt_RC_amount_deb.Text = DT_VOU.Compute("Sum(DFor_Amount)", "").ToString();
                txt_RC_amount_crd.Text = DT_VOU.Compute("Sum(CFor_Amount)", "").ToString();
                txt_LC_amount_deb.Text = DT_VOU.Compute("Sum(Dloc_Amount)", "").ToString();
                txt_LC_amount_crd.Text = DT_VOU.Compute("Sum(Cloc_Amount)", "").ToString();
            }


        }

        private void Cbo_Rcur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change_Bob)
            {
                Decimal Amount = 0;
                DataTable DT = new DataTable();
                DT = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable().Select("r_Cur_id = " + Cbo_Rcur.SelectedValue).CopyToDataTable();
                Amount = Convert.ToDecimal(DT.Compute("Sum(R_Amount)", ""));
                Tot_Amount.Text = Amount.ToString();
                Txt_RCount.Text = DT.Rows.Count.ToString();
            }
        }

        private void Btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Print_All_Click(object sender, EventArgs e)
        {
            if (Grd_rem_view.Rows.Count > 0)
            {
                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["TBL_Remittances_Query"];
                DataGridView[] Export_GRD = { Grd_rem_view };
                DataTable[] Export_DT = { connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false,connection.Lang_id==1?"a_local_international":"E_local_international",connection.Lang_id==1?"a_Send_rem_flag":"E_Send_rem_flag", "rem_no", "R_amount", 
              connection.Lang_id==1? "R_ACUR_NAME":"R_ECUR_NAME", connection.Lang_id==1?"PR_ACUR_NAME":"PR_ECUR_NAME", connection.Lang_id==1?"t_ACITY_NAME":"t_ECITY_NAME",connection.Lang_id==1?"S_ACITY_NAME":"S_ECITY_NAME",connection.Lang_id==1?"ACASE_NA":"ECASE_NA","Case_Date", "C_DATE","user_NAME","S_name","S_phone", "S_address","S_Street"
               ,"S_Suburb","S_Post_Code", "S_State","S_doc_no","S_doc_ida","S_doc_eda", "S_doc_issue","s_Email",connection.Lang_id==1?"s_A_NAT_NAME":"s_E_NAT_NAME","s_notes", "r_name",
               "R_phone",connection.Lang_id==1?"r_ACITY_NAME":"r_ECITY_NAME",connection.Lang_id==1?"r_ACOUN_NAME":"r_ECOUN_NAME", "r_address","r_Street","r_Suburb","r_Post_Code", "r_State","r_doc_no","r_doc_ida","r_doc_eda"
               , "r_doc_issue","r_Email","T_purpose","r_notes","vo_no","In_rec_date","sbirth_date","rbirth_date","SBirth_Place","s_ocomm",connection.Lang_id==1?"s_ACUR_NAME":"s_ECUR_NAME"
               ,"c_ocomm",connection.Lang_id==1?"c_ACUR_NAME":"c_ECUR_NAME","rate_online","Source_money","Relation_S_R").Select().CopyToDataTable()};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Print_Details_Click(object sender, EventArgs e)
        {
             DataTable Dt = new DataTable();
             DataTable Dt_details = new DataTable();
             DataTable Export_DT_main = new DataTable();
             DataTable Export_DT_details =new DataTable();
             string rem_no = ((DataRowView)binding_Grd1.Current).Row["rem_no"].ToString();
             rem_id = Convert.ToInt64(((DataRowView)binding_Grd1.Current).Row["rem_id"]);



             DataGridView G = new DataGridView();
             G.Columns.Add("Column1", connection.Lang_id == 1 ? "نوع الحوالة" : "Remmitances type");
             G.Columns.Add("Column2", connection.Lang_id == 1 ? "رقم الحوالة" : "Remmitance NO");
             G.Columns.Add("Column3", connection.Lang_id == 1 ? "مبلغ الحوالة" : "remittence amount");
             G.Columns.Add("Column4", connection.Lang_id == 1 ? "عملة الحوالة" : "remittence currency");
             G.Columns.Add("Column5", connection.Lang_id == 1 ? "عملة التسليم" : "Paying currency");
             G.Columns.Add("Column6", connection.Lang_id == 1 ? "مدينة التسليم" : "Paying city");
             G.Columns.Add("Column7", connection.Lang_id == 1 ? "مدينة الارسال" : "sending city");
             G.Columns.Add("Column8", connection.Lang_id == 1 ? "إسم المرسل" : "Sender name");
             G.Columns.Add("Column9", connection.Lang_id == 1 ? "هاتف المرسل" : "sender phone");
             G.Columns.Add("Column10", connection.Lang_id == 1 ? "عنوان المرسل" : "sender address");
             G.Columns.Add("Column11", connection.Lang_id == 1 ? "الزقاق" : "street");
             G.Columns.Add("Column12", connection.Lang_id == 1 ? "الحي" : "suburb");
             G.Columns.Add("Column13", connection.Lang_id == 1 ? "الرمز البريدي" : "Post NO");
             G.Columns.Add("Column14", connection.Lang_id == 1 ? "المحافظة" : "city");
             G.Columns.Add("Column15", connection.Lang_id == 1 ? "رقم هوية المرسل" : "Sender Document no.");
             G.Columns.Add("Column16", connection.Lang_id == 1 ? "تاريخ الاصدار" : "Issuing date");
             G.Columns.Add("Column17", connection.Lang_id == 1 ? "تاريخ الانتهاء" : "expire date");
             G.Columns.Add("Column18", connection.Lang_id == 1 ? "نوع الوثيقة" : "Document type");
             G.Columns.Add("Column19", connection.Lang_id == 1 ? "ايميل المرسل" : "Sender Email");
             G.Columns.Add("Column20", connection.Lang_id == 1 ? "جنسية المرسل" : "Sender nationality");
             G.Columns.Add("Column21", connection.Lang_id == 1 ? "ملاحظات المرسل" : "Sender notes");
             G.Columns.Add("Column22", connection.Lang_id == 1 ? "اسم المستلم" : "Reciver name");
             G.Columns.Add("Column23", connection.Lang_id == 1 ? "هاتف المستلم" : "Reciver phone");
             G.Columns.Add("Column24", connection.Lang_id == 1 ? "مدينة المستلم" : "Reciver city");
             G.Columns.Add("Column25", connection.Lang_id == 1 ? "بلد المستلم" : "Reciver country");
             G.Columns.Add("Column26", connection.Lang_id == 1 ? "العنوان" : "Address");
             G.Columns.Add("Column27", connection.Lang_id == 1 ? "الزقاق" : "Street");
             G.Columns.Add("Column28", connection.Lang_id == 1 ? "الحي" : "Suburb");
             G.Columns.Add("Column29", connection.Lang_id == 1 ? "الرمز البريدي" : "Post code");
             G.Columns.Add("Column30", connection.Lang_id == 1 ? "محافظة" : "city");
             G.Columns.Add("Column31", connection.Lang_id == 1 ? "رقم الوثيقة" : "Document NO");
             G.Columns.Add("Column32", connection.Lang_id == 1 ? "تاريخ الاصدار" : "issuing date");
             G.Columns.Add("Column33", connection.Lang_id == 1 ? "تاريخ الانتهاء" : "Expire date");
             G.Columns.Add("Column34", connection.Lang_id == 1 ? "نوع الوثيقة" : "Document type");
             G.Columns.Add("Column35", connection.Lang_id == 1 ? "ايميل المستلم" : "Reciver Email");
             G.Columns.Add("Column36", connection.Lang_id == 1 ? "غرض الارسال" : "Sending purpse");
             G.Columns.Add("Column37", connection.Lang_id == 1 ? "ملاحظات المستلم" : "Reciver notes");
             G.Columns.Add("Column38", connection.Lang_id == 1 ? "رقم السند" : "Bond NO");
             G.Columns.Add("Column39", connection.Lang_id == 1 ? "تولد مرسل" : "sender birthdate");
             G.Columns.Add("Column40", connection.Lang_id == 1 ? "تولد مستلم" : "Reciver birthdate");
             G.Columns.Add("Column41", connection.Lang_id == 1 ? "محل تولد المرسل" : "Sender birth place");
             G.Columns.Add("Column42", connection.Lang_id == 1 ? "عمولة مقبوضة من الزبون" : "Commission recived from customer");
             G.Columns.Add("Column43", connection.Lang_id == 1 ? "عملة العمولة المقبوضة من الزبون" : "Commission currency recived from customer");
             G.Columns.Add("Column44", connection.Lang_id == 1 ? "عمولة مركز الاون لاين" : "Online center commission");
             G.Columns.Add("Column45", connection.Lang_id == 1 ? "عملة عمولة مركز الاون لاين" : "Online center commission currency");
             G.Columns.Add("Column46", connection.Lang_id == 1 ? "سعر التعادل" : "Exchande rate");
             G.Columns.Add("Column47", connection.Lang_id == 1 ? "مصدر المال" : "Money source");
             G.Columns.Add("Column48", connection.Lang_id == 1 ? "علاقة المرسل والمستلم" : "sender and reciver relaithionship");



             DataGridView G1 = new DataGridView();
             G1.Columns.Add("Column1", connection.Lang_id == 1 ? "رقم السند" : "Bond NO");
             G1.Columns.Add("Column2", connection.Lang_id == 1 ? "حالة الحوالة" : "Remmitance Case");
             G1.Columns.Add("Column3", connection.Lang_id == 1 ? "تاريخ و وقت الانشاء" : "Issuing date and time");
             G1.Columns.Add("Column4", connection.Lang_id == 1 ? "المستخدم" : "User");
             G1.Columns.Add("Column3", connection.Lang_id == 1 ? "العملة الاصلية مدين" : "Debit original currency");
             G1.Columns.Add("Column4", connection.Lang_id == 1 ? "العملة الاصلية دائن" : "Credit original currency");
             G1.Columns.Add("Column5", connection.Lang_id == 1 ? "رقم الحساب" : "Accountant NO");
             G1.Columns.Add("Column6", connection.Lang_id == 1 ? "إسم الحساب" : "Accountant name");
             G1.Columns.Add("Column7", connection.Lang_id == 1 ? "العملة" : "Currency");
             G1.Columns.Add("Column8", connection.Lang_id == 1 ? "سعر الصرف" : "exchange rate");
             G1.Columns.Add("Column9", connection.Lang_id == 1 ? "السعر الفعلي" : "Real price");
             G1.Columns.Add("Column10", connection.Lang_id == 1 ? "العملة المحلية مدين" : "Dedit in local currency");
             G1.Columns.Add("Column11", connection.Lang_id == 1 ? "العملة المحلية دائن" : "credit in local currency");
             G1.Columns.Add("Column12", connection.Lang_id == 1 ? "وقت وتاريخ الانشاء" : "issuing date and time");



             Dt = connection.SQLDS.Tables["TBL_Remittances_Query"];
             Dt_details = connection.SQLDS.Tables["TBL_Remittances_Query1"].Select("Ref_id = " + rem_id).CopyToDataTable();
             Export_DT_main = connection.SQLDS.Tables["TBL_Remittances_Query"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "a_local_international" : "E_local_international", "rem_no", "R_amount",
                 connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", connection.Lang_id == 1 ? "PR_ACUR_NAME" : "PR_ECUR_NAME", connection.Lang_id == 1 ? "t_ACITY_NAME" : "t_ECITY_NAME", connection.Lang_id == 1 ? "S_ACITY_NAME" : "S_ECITY_NAME", "S_name", "S_phone", "S_address", "S_Street"
                 , "S_Suburb", "S_Post_Code", "S_State", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", connection.Lang_id == 1 ? "s_A_NAT_NAME" : "s_E_NAT_NAME", "s_notes", "r_name",
                 "R_phone", connection.Lang_id == 1 ? "r_ACITY_NAME" : "r_ECITY_NAME", connection.Lang_id == 1 ? "r_ACOUN_NAME" : "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "r_doc_no", "r_doc_ida", "r_doc_eda"
                 , "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "sbirth_date", "rbirth_date", "SBirth_Place", "s_ocomm", connection.Lang_id == 1 ? "s_ACUR_NAME" : "s_ECUR_NAME"
                 , "c_ocomm", connection.Lang_id == 1 ? "c_ACUR_NAME" : "c_ECUR_NAME", "rate_online", "Source_money", "Relation_S_R").Select("rem_no = '" + rem_no + "'").CopyToDataTable();

             Export_DT_details = Dt_details.DefaultView.ToTable(false, "vo_no", connection.Lang_id == 1 ? "ACase_na" : "ECase_na", "Nrec_Date1", "User_name",
                 "DFor_Amount", "CFor_Amount", "Acc_id", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Exch_Price", "REAL_PRICE", "Dloc_Amount",
             "Cloc_Amount", "C_date").Select().CopyToDataTable();


            //Export_DT_details.DefaultView.Sort = "vo_no ASC";

            DataView dv = Export_DT_details.DefaultView;
            dv.Sort = "C_date ,vo_no  ASC";
            DataTable sortedDT = dv.ToTable();



            DataGridView[] Export_GRD = { G, G1 };
            DataTable[] Export_DT = { Export_DT_main, sortedDT };
             MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void Remittances_Query_Vou_Details_FormClosed(object sender, FormClosedEventArgs e)
        {
             Change_Grd1_details = false;
             Change_Grd2_details = false;
             Change_Bob = false;
             string[] Used_Tbl = { "TBL_Remittances_Query", "TBL_Remittances_Query1" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
    }
}
