﻿namespace Integration_Accounting_Sys
{
    partial class outlier_Rem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_per = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_per_id = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remove_all_btn = new System.Windows.Forms.Button();
            this.remove_one_btn = new System.Windows.Forms.Button();
            this.move_all_btn = new System.Windows.Forms.Button();
            this.move_one_btn = new System.Windows.Forms.Button();
            this.Names = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtFromDate = new System.Windows.Forms.DateTimePicker();
            this.TxtToDate = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.OperType = new System.Windows.Forms.ComboBox();
            this.RemType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Grd_per_info = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.min_rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.max_rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbo_curType = new System.Windows.Forms.ComboBox();
            this.Cbo_curs = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CHK1 = new System.Windows.Forms.CheckBox();
            this.Grd_Rem = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column80 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_outlier = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.RemNum = new System.Windows.Forms.TextBox();
            this.Amount = new System.Windows.Forms.TextBox();
            this.Graph_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per_info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_outlier)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(10, 5);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(878, 1);
            this.flowLayoutPanel2.TabIndex = 530;
            // 
            // Grd_per
            // 
            this.Grd_per.AllowUserToAddRows = false;
            this.Grd_per.AllowUserToDeleteRows = false;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle47;
            this.Grd_per.BackgroundColor = System.Drawing.Color.White;
            this.Grd_per.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_per.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_per.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle48;
            this.Grd_per.ColumnHeadersHeight = 45;
            this.Grd_per.ColumnHeadersVisible = false;
            this.Grd_per.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5});
            this.Grd_per.Location = new System.Drawing.Point(485, 110);
            this.Grd_per.Name = "Grd_per";
            this.Grd_per.ReadOnly = true;
            this.Grd_per.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_per.RowHeadersVisible = false;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle49.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per.RowsDefaultCellStyle = dataGridViewCellStyle49;
            this.Grd_per.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_per.Size = new System.Drawing.Size(397, 94);
            this.Grd_per.TabIndex = 12;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "per_Name";
            this.Column5.HeaderText = "الاسم";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 5;
            // 
            // Grd_per_id
            // 
            this.Grd_per_id.AllowUserToAddRows = false;
            this.Grd_per_id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per_id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle50;
            this.Grd_per_id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_per_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_per_id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle51.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_per_id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle51;
            this.Grd_per_id.ColumnHeadersHeight = 45;
            this.Grd_per_id.ColumnHeadersVisible = false;
            this.Grd_per_id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle52.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle52.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_per_id.DefaultCellStyle = dataGridViewCellStyle52;
            this.Grd_per_id.Location = new System.Drawing.Point(16, 110);
            this.Grd_per_id.Name = "Grd_per_id";
            this.Grd_per_id.ReadOnly = true;
            this.Grd_per_id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_per_id.RowHeadersVisible = false;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per_id.RowsDefaultCellStyle = dataGridViewCellStyle53;
            this.Grd_per_id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_per_id.Size = new System.Drawing.Size(397, 94);
            this.Grd_per_id.TabIndex = 7;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "per_id";
            this.Column1.HeaderText = "الرمز";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "per_Name";
            this.Column2.HeaderText = "الاسم";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 5;
            // 
            // remove_all_btn
            // 
            this.remove_all_btn.Enabled = false;
            this.remove_all_btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.remove_all_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.remove_all_btn.Location = new System.Drawing.Point(415, 178);
            this.remove_all_btn.Name = "remove_all_btn";
            this.remove_all_btn.Size = new System.Drawing.Size(67, 22);
            this.remove_all_btn.TabIndex = 11;
            this.remove_all_btn.Text = "<<";
            this.remove_all_btn.UseVisualStyleBackColor = true;
            this.remove_all_btn.Click += new System.EventHandler(this.remove_all_btn_Click);
            // 
            // remove_one_btn
            // 
            this.remove_one_btn.Enabled = false;
            this.remove_one_btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.remove_one_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.remove_one_btn.Location = new System.Drawing.Point(415, 157);
            this.remove_one_btn.Name = "remove_one_btn";
            this.remove_one_btn.Size = new System.Drawing.Size(67, 22);
            this.remove_one_btn.TabIndex = 10;
            this.remove_one_btn.Text = "<";
            this.remove_one_btn.UseVisualStyleBackColor = true;
            this.remove_one_btn.Click += new System.EventHandler(this.remove_one_btn_Click);
            // 
            // move_all_btn
            // 
            this.move_all_btn.Enabled = false;
            this.move_all_btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.move_all_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.move_all_btn.Location = new System.Drawing.Point(415, 136);
            this.move_all_btn.Name = "move_all_btn";
            this.move_all_btn.Size = new System.Drawing.Size(67, 22);
            this.move_all_btn.TabIndex = 9;
            this.move_all_btn.Text = ">>";
            this.move_all_btn.UseVisualStyleBackColor = true;
            this.move_all_btn.Click += new System.EventHandler(this.move_all_btn_Click);
            // 
            // move_one_btn
            // 
            this.move_one_btn.Enabled = false;
            this.move_one_btn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.move_one_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.move_one_btn.Location = new System.Drawing.Point(415, 115);
            this.move_one_btn.Name = "move_one_btn";
            this.move_one_btn.Size = new System.Drawing.Size(67, 22);
            this.move_one_btn.TabIndex = 8;
            this.move_one_btn.Text = ">";
            this.move_one_btn.UseVisualStyleBackColor = true;
            this.move_one_btn.Click += new System.EventHandler(this.move_one_btn_Click);
            // 
            // Names
            // 
            this.Names.BackColor = System.Drawing.Color.White;
            this.Names.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Names.Location = new System.Drawing.Point(102, 88);
            this.Names.Name = "Names";
            this.Names.Size = new System.Drawing.Size(311, 22);
            this.Names.TabIndex = 9;
            this.Names.TextChanged += new System.EventHandler(this.Names_TextChanged);
            this.Names.Leave += new System.EventHandler(this.Names_Leave);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 6);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 686);
            this.flowLayoutPanel1.TabIndex = 585;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(888, 6);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 686);
            this.flowLayoutPanel4.TabIndex = 590;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(10, 729);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(878, 1);
            this.flowLayoutPanel5.TabIndex = 591;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(14, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 14);
            this.label6.TabIndex = 634;
            this.label6.Text = "معلومات شخصية ....";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(14, 237);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(873, 1);
            this.flowLayoutPanel7.TabIndex = 633;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(400, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 16);
            this.label7.TabIndex = 626;
            this.label7.Text = "نــــوع الــعمليه :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(12, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 16);
            this.label11.TabIndex = 624;
            this.label11.Text = "مجــــموع مبالغ :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(12, 38);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 16);
            this.label18.TabIndex = 616;
            this.label18.Text = "عـــدد الحــوالات :";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(13, 382);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(873, 1);
            this.flowLayoutPanel9.TabIndex = 649;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(17, 91);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 16);
            this.label27.TabIndex = 652;
            this.label27.Text = "اســــــــم الزبون:";
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.Checked = false;
            this.TxtFromDate.CustomFormat = "yyyy/mm/dd";
            this.TxtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtFromDate.Location = new System.Drawing.Point(101, 12);
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.ShowCheckBox = true;
            this.TxtFromDate.Size = new System.Drawing.Size(104, 20);
            this.TxtFromDate.TabIndex = 1;
            this.TxtFromDate.ValueChanged += new System.EventHandler(this.TxtFromDate_ValueChanged);
            // 
            // TxtToDate
            // 
            this.TxtToDate.Checked = false;
            this.TxtToDate.CustomFormat = "yyyy/mm/dd";
            this.TxtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtToDate.Location = new System.Drawing.Point(249, 12);
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.ShowCheckBox = true;
            this.TxtToDate.Size = new System.Drawing.Size(102, 20);
            this.TxtToDate.TabIndex = 2;
            this.TxtToDate.ValueChanged += new System.EventHandler(this.TxtToDate_ValueChanged);
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(13, 533);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(873, 1);
            this.flowLayoutPanel10.TabIndex = 1108;
            // 
            // OperType
            // 
            this.OperType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.OperType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OperType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OperType.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OperType.FormattingEnabled = true;
            this.OperType.Items.AddRange(new object[] {
            "وارده",
            "صادره"});
            this.OperType.Location = new System.Drawing.Point(482, 60);
            this.OperType.Name = "OperType";
            this.OperType.Size = new System.Drawing.Size(142, 24);
            this.OperType.TabIndex = 6;
            this.OperType.SelectedIndexChanged += new System.EventHandler(this.OperType_SelectedIndexChanged);
            // 
            // RemType
            // 
            this.RemType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.RemType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.RemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RemType.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemType.FormattingEnabled = true;
            this.RemType.Items.AddRange(new object[] {
            "الجميع",
            "اونلاين",
            "اوفلاين"});
            this.RemType.Location = new System.Drawing.Point(482, 32);
            this.RemType.Name = "RemType";
            this.RemType.Size = new System.Drawing.Size(142, 24);
            this.RemType.TabIndex = 5;
            this.RemType.SelectedIndexChanged += new System.EventHandler(this.RemType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(399, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 16);
            this.label9.TabIndex = 1119;
            this.label9.Text = "نــــوع الــحواله :";
            // 
            // Grd_per_info
            // 
            this.Grd_per_info.AllowUserToAddRows = false;
            this.Grd_per_info.AllowUserToDeleteRows = false;
            dataGridViewCellStyle54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle54.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per_info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle54;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle55.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_per_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle55;
            this.Grd_per_info.ColumnHeadersHeight = 30;
            this.Grd_per_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column4,
            this.Column6,
            this.Column7,
            this.Column8,
            this.min_rate,
            this.max_rate});
            this.Grd_per_info.Location = new System.Drawing.Point(19, 244);
            this.Grd_per_info.Name = "Grd_per_info";
            this.Grd_per_info.ReadOnly = true;
            this.Grd_per_info.RowHeadersWidth = 22;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_per_info.RowsDefaultCellStyle = dataGridViewCellStyle61;
            this.Grd_per_info.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.Grd_per_info.Size = new System.Drawing.Size(865, 130);
            this.Grd_per_info.TabIndex = 1121;
            this.Grd_per_info.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_per_info_CellContentClick);
            this.Grd_per_info.SelectionChanged += new System.EventHandler(this.Grd_per_info_SelectionChanged);
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "per_name";
            this.Column3.HeaderText = "اسم الشخص";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 170;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "rem_count";
            this.Column4.HeaderText = "عدد الحوالات";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "rem_sum";
            dataGridViewCellStyle56.Format = "n3";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle56;
            this.Column6.HeaderText = "مجموع المبالغ";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "mean";
            dataGridViewCellStyle57.Format = "n3";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle57;
            this.Column7.HeaderText = "المعدل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "standard_deviation";
            dataGridViewCellStyle58.Format = "n3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle58;
            this.Column8.HeaderText = "الانحراف";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 120;
            // 
            // min_rate
            // 
            this.min_rate.DataPropertyName = "min_rate";
            dataGridViewCellStyle59.Format = "n3";
            this.min_rate.DefaultCellStyle = dataGridViewCellStyle59;
            this.min_rate.HeaderText = "الحد الادنى";
            this.min_rate.Name = "min_rate";
            this.min_rate.ReadOnly = true;
            this.min_rate.Width = 120;
            // 
            // max_rate
            // 
            this.max_rate.DataPropertyName = "max_rate";
            dataGridViewCellStyle60.Format = "n3";
            this.max_rate.DefaultCellStyle = dataGridViewCellStyle60;
            this.max_rate.HeaderText = "الحد الاعلى";
            this.max_rate.Name = "max_rate";
            this.max_rate.ReadOnly = true;
            this.max_rate.Width = 120;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button5.Location = new System.Drawing.Point(737, 210);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(145, 24);
            this.button5.TabIndex = 11;
            this.button5.Text = "بحـــــــث";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(658, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 1128;
            this.label4.Text = "نــوع العــمله :";
            // 
            // cbo_curType
            // 
            this.cbo_curType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_curType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_curType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_curType.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_curType.FormattingEnabled = true;
            this.cbo_curType.Items.AddRange(new object[] {
            "عمله المعادل",
            "عمله اصليه"});
            this.cbo_curType.Location = new System.Drawing.Point(738, 32);
            this.cbo_curType.Name = "cbo_curType";
            this.cbo_curType.Size = new System.Drawing.Size(142, 24);
            this.cbo_curType.TabIndex = 7;
            this.cbo_curType.SelectedIndexChanged += new System.EventHandler(this.cbo_curType_SelectedIndexChanged);
            // 
            // Cbo_curs
            // 
            this.Cbo_curs.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_curs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_curs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_curs.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_curs.FormattingEnabled = true;
            this.Cbo_curs.Location = new System.Drawing.Point(737, 63);
            this.Cbo_curs.Name = "Cbo_curs";
            this.Cbo_curs.Size = new System.Drawing.Size(142, 24);
            this.Cbo_curs.TabIndex = 8;
            this.Cbo_curs.SelectedIndexChanged += new System.EventHandler(this.Cbo_curs_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(658, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 16);
            this.label8.TabIndex = 1131;
            this.label8.Text = "الـــعــمـــلات :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(22, 377);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 14);
            this.label10.TabIndex = 1132;
            this.label10.Text = "الحــــــوالات ....";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(16, 522);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 14);
            this.label12.TabIndex = 1133;
            this.label12.Text = "الحــوالات الشــاذه ....";
            // 
            // CHK1
            // 
            this.CHK1.AutoSize = true;
            this.CHK1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK1.ForeColor = System.Drawing.Color.Maroon;
            this.CHK1.Location = new System.Drawing.Point(482, 87);
            this.CHK1.Name = "CHK1";
            this.CHK1.Size = new System.Drawing.Size(135, 20);
            this.CHK1.TabIndex = 10;
            this.CHK1.Text = "استثناء المردودات";
            this.CHK1.UseVisualStyleBackColor = true;
            // 
            // Grd_Rem
            // 
            this.Grd_Rem.AllowUserToAddRows = false;
            this.Grd_Rem.AllowUserToDeleteRows = false;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle62.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle62;
            this.Grd_Rem.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle63.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle63.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle63;
            this.Grd_Rem.ColumnHeadersHeight = 24;
            this.Grd_Rem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column10,
            this.Column13,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.Column14,
            this.dataGridViewTextBoxColumn5,
            this.Column9,
            this.dataGridViewTextBoxColumn6,
            this.Column19,
            this.dataGridViewTextBoxColumn7,
            this.Column11,
            this.dataGridViewTextBoxColumn8,
            this.Column12,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column20,
            this.Column21,
            this.Column28,
            this.dataGridViewCheckBoxColumn4,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column29,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column35,
            this.Column40,
            this.dataGridViewTextBoxColumn13,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44,
            this.Column45,
            this.Column48,
            this.Column49,
            this.Column52,
            this.Column53,
            this.Column57,
            this.Column58,
            this.Column59,
            this.Column60,
            this.Column61,
            this.Column62,
            this.Column71,
            this.Column72,
            this.Column75,
            this.Column76,
            this.Column50,
            this.Column51,
            this.Column54,
            this.Column55,
            this.Column56,
            this.Column63,
            this.Column70,
            this.Column73,
            this.Column74,
            this.Column80});
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle74.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle74.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle74.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle74.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle74.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle74.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Rem.DefaultCellStyle = dataGridViewCellStyle74;
            this.Grd_Rem.Location = new System.Drawing.Point(16, 394);
            this.Grd_Rem.MultiSelect = false;
            this.Grd_Rem.Name = "Grd_Rem";
            this.Grd_Rem.ReadOnly = true;
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle75.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle75.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle75.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle75.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle75.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rem.RowHeadersDefaultCellStyle = dataGridViewCellStyle75;
            this.Grd_Rem.RowHeadersVisible = false;
            dataGridViewCellStyle76.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Rem.RowsDefaultCellStyle = dataGridViewCellStyle76;
            this.Grd_Rem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.Grd_Rem.Size = new System.Drawing.Size(866, 125);
            this.Grd_Rem.TabIndex = 1202;
            this.Grd_Rem.VirtualMode = true;
            this.Grd_Rem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_Rem_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "rem_no";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحولة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 81;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "id";
            this.Column10.HeaderText = "محور الحوالات";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "ACASE_NA";
            this.Column13.HeaderText = "نوع العمليه";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 84;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "R_amount";
            dataGridViewCellStyle64.Format = "n3";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewTextBoxColumn3.HeaderText = "مبلغ الحوالة";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 88;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "R_ACUR_NAME";
            this.dataGridViewTextBoxColumn4.HeaderText = "عملة الحوالة";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 88;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "rem_loc_amount";
            dataGridViewCellStyle65.Format = "n3";
            this.Column14.DefaultCellStyle = dataGridViewCellStyle65;
            this.Column14.HeaderText = "المبلغ محلي";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PR_ACUR_NAME";
            this.dataGridViewTextBoxColumn5.HeaderText = "عملة التسليم";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 88;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "S_ACITY_NAME";
            this.Column9.HeaderText = "مدينة الارسال";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 97;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "t_ACITY_NAME";
            this.dataGridViewTextBoxColumn6.HeaderText = "مدينة الاستلام";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 96;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "user_name";
            this.Column19.HeaderText = "المستخدم";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 76;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Case_Date";
            dataGridViewCellStyle66.Format = "dd/MM/yyyy";
            dataGridViewCellStyle66.NullValue = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle66;
            this.dataGridViewTextBoxColumn7.HeaderText = "تاريخ الحالة";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 91;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "C_DATE";
            dataGridViewCellStyle67.Format = "dd\'/\'MM\'/\'yyyy";
            dataGridViewCellStyle67.NullValue = "dd\'/\'MM\'/\'yyyy";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle67;
            this.Column11.HeaderText = "تاريخ تنظيم السجلات";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 131;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "S_name";
            this.dataGridViewTextBoxColumn8.HeaderText = "اسم المرسل";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 87;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "S_phone";
            this.Column12.HeaderText = "هاتف المرسل";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 97;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "S_address";
            this.Column16.HeaderText = "عنوان المرسل";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 97;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "S_Street";
            this.Column17.HeaderText = "الزقاق";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 64;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "S_Suburb";
            this.Column18.HeaderText = "الحي";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 57;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column20.DataPropertyName = "S_Post_Code";
            this.Column20.HeaderText = "الرقم البريدي";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Visible = false;
            this.Column20.Width = 94;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column21.DataPropertyName = "S_State";
            this.Column21.HeaderText = "المحافظة";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 76;
            // 
            // Column28
            // 
            this.Column28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column28.DataPropertyName = "S_Social_No";
            this.Column28.HeaderText = "الرقم الوطني للمرسل";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.Visible = false;
            this.Column28.Width = 129;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.DataPropertyName = "sFRM_ADOC_NA";
            this.dataGridViewCheckBoxColumn4.HeaderText = "نوع وثيقة المرسل";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.ReadOnly = true;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column22.DataPropertyName = "S_doc_no";
            this.Column22.HeaderText = "رقم وثيقة المرسل";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Visible = false;
            this.Column22.Width = 114;
            // 
            // Column23
            // 
            this.Column23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column23.DataPropertyName = "S_doc_ida";
            dataGridViewCellStyle68.Format = "dd/MM/yyyy";
            this.Column23.DefaultCellStyle = dataGridViewCellStyle68;
            this.Column23.HeaderText = "تاريخ اصدار وثيقة المرسل";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.Width = 155;
            // 
            // Column24
            // 
            this.Column24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column24.DataPropertyName = "S_doc_eda";
            dataGridViewCellStyle69.Format = "dd/MM/yyyy";
            this.Column24.DefaultCellStyle = dataGridViewCellStyle69;
            this.Column24.HeaderText = "تاريخ انتهاء وثيقة المرسل";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.Width = 155;
            // 
            // Column25
            // 
            this.Column25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column25.DataPropertyName = "S_doc_issue";
            this.Column25.HeaderText = "اصدار وثيقة المرسل";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.Width = 124;
            // 
            // Column26
            // 
            this.Column26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column26.DataPropertyName = "s_Email";
            this.Column26.HeaderText = "ايميل المرسل";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.Width = 94;
            // 
            // Column29
            // 
            this.Column29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column29.DataPropertyName = "s_A_NAT_NAME";
            this.Column29.HeaderText = "جنسية المرسل";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            // 
            // Column30
            // 
            this.Column30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column30.DataPropertyName = "s_notes";
            this.Column30.HeaderText = "ملاحظات المرسل";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Width = 113;
            // 
            // Column31
            // 
            this.Column31.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column31.DataPropertyName = "r_name";
            this.Column31.HeaderText = "اسم المستلم";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.Width = 86;
            // 
            // Column32
            // 
            this.Column32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column32.DataPropertyName = "R_phone";
            this.Column32.HeaderText = "هاتف المستلم";
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.Width = 96;
            // 
            // Column33
            // 
            this.Column33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column33.DataPropertyName = "r_ACITY_NAME";
            this.Column33.HeaderText = "مدينة المستلم";
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            this.Column33.Width = 93;
            // 
            // Column34
            // 
            this.Column34.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column34.DataPropertyName = "r_ACOUN_NAME";
            this.Column34.HeaderText = "بلد المستلم";
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            this.Column34.Width = 81;
            // 
            // Column36
            // 
            this.Column36.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column36.DataPropertyName = "r_address";
            this.Column36.HeaderText = "عنوان المستلم";
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            this.Column36.Width = 96;
            // 
            // Column37
            // 
            this.Column37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column37.DataPropertyName = "r_Street";
            this.Column37.HeaderText = "الزقاق";
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            this.Column37.Width = 64;
            // 
            // Column38
            // 
            this.Column38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column38.DataPropertyName = "r_Suburb";
            this.Column38.HeaderText = "الحي للمستلم";
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            this.Column38.Width = 93;
            // 
            // Column39
            // 
            this.Column39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column39.DataPropertyName = "r_Post_Code";
            this.Column39.HeaderText = "الرمز البريدي للمستلم";
            this.Column39.Name = "Column39";
            this.Column39.ReadOnly = true;
            this.Column39.Visible = false;
            this.Column39.Width = 131;
            // 
            // Column35
            // 
            this.Column35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column35.DataPropertyName = "r_State";
            this.Column35.HeaderText = "محافظة المستلم";
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            this.Column35.Width = 106;
            // 
            // Column40
            // 
            this.Column40.DataPropertyName = "R_Social_No";
            this.Column40.HeaderText = "الرقم الوطني للمستلم";
            this.Column40.Name = "Column40";
            this.Column40.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "rFRM_ADOC_NA";
            this.dataGridViewTextBoxColumn13.HeaderText = "نوع وثيقة المستلم";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // Column41
            // 
            this.Column41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column41.DataPropertyName = "r_doc_no";
            this.Column41.HeaderText = "رقم وثيقة المستلم";
            this.Column41.Name = "Column41";
            this.Column41.ReadOnly = true;
            this.Column41.Width = 113;
            // 
            // Column42
            // 
            this.Column42.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column42.DataPropertyName = "r_doc_ida";
            dataGridViewCellStyle70.Format = "dd/MM/yyyy";
            this.Column42.DefaultCellStyle = dataGridViewCellStyle70;
            this.Column42.HeaderText = "تاريخ اصدار الوثيقة المستلم";
            this.Column42.Name = "Column42";
            this.Column42.ReadOnly = true;
            this.Column42.Width = 160;
            // 
            // Column43
            // 
            this.Column43.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column43.DataPropertyName = "r_doc_eda";
            dataGridViewCellStyle71.Format = "dd/MM/yyyy";
            this.Column43.DefaultCellStyle = dataGridViewCellStyle71;
            this.Column43.HeaderText = "تاريخ انتهاء الوثيقة المستلم";
            this.Column43.Name = "Column43";
            this.Column43.ReadOnly = true;
            this.Column43.Width = 160;
            // 
            // Column44
            // 
            this.Column44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column44.DataPropertyName = "r_doc_issue";
            this.Column44.HeaderText = "اصدار وثيقة المستلم";
            this.Column44.Name = "Column44";
            this.Column44.ReadOnly = true;
            this.Column44.Width = 123;
            // 
            // Column45
            // 
            this.Column45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column45.DataPropertyName = "r_Email";
            this.Column45.HeaderText = "ايميل المستلم";
            this.Column45.Name = "Column45";
            this.Column45.ReadOnly = true;
            this.Column45.Width = 93;
            // 
            // Column48
            // 
            this.Column48.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column48.DataPropertyName = "T_purpose";
            this.Column48.HeaderText = "غرض الارسال";
            this.Column48.Name = "Column48";
            this.Column48.ReadOnly = true;
            this.Column48.Width = 101;
            // 
            // Column49
            // 
            this.Column49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column49.DataPropertyName = "r_notes";
            this.Column49.HeaderText = "ملاحظات المستلم";
            this.Column49.Name = "Column49";
            this.Column49.ReadOnly = true;
            this.Column49.Width = 112;
            // 
            // Column52
            // 
            this.Column52.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column52.DataPropertyName = "vo_no";
            this.Column52.HeaderText = "رقم السند";
            this.Column52.Name = "Column52";
            this.Column52.ReadOnly = true;
            this.Column52.Width = 77;
            // 
            // Column53
            // 
            this.Column53.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column53.DataPropertyName = "In_rec_date";
            dataGridViewCellStyle72.Format = "dd/MM/yyyy";
            this.Column53.DefaultCellStyle = dataGridViewCellStyle72;
            this.Column53.HeaderText = "تاريخ السجلات";
            this.Column53.Name = "Column53";
            this.Column53.ReadOnly = true;
            this.Column53.Width = 104;
            // 
            // Column57
            // 
            this.Column57.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column57.DataPropertyName = "sbirth_date";
            this.Column57.HeaderText = "تولد المرسل";
            this.Column57.Name = "Column57";
            this.Column57.ReadOnly = true;
            this.Column57.Width = 88;
            // 
            // Column58
            // 
            this.Column58.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column58.DataPropertyName = "rbirth_date";
            this.Column58.HeaderText = "تولد المستلم";
            this.Column58.Name = "Column58";
            this.Column58.ReadOnly = true;
            this.Column58.Width = 87;
            // 
            // Column59
            // 
            this.Column59.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column59.DataPropertyName = "s_ocomm";
            this.Column59.HeaderText = "عمولة المرسل";
            this.Column59.Name = "Column59";
            this.Column59.ReadOnly = true;
            this.Column59.Visible = false;
            this.Column59.Width = 97;
            // 
            // Column60
            // 
            this.Column60.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column60.DataPropertyName = "s_ACUR_NAME";
            this.Column60.HeaderText = "عملة المرسل";
            this.Column60.Name = "Column60";
            this.Column60.ReadOnly = true;
            this.Column60.Visible = false;
            this.Column60.Width = 91;
            // 
            // Column61
            // 
            this.Column61.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column61.DataPropertyName = "c_ocomm";
            this.Column61.HeaderText = "مبلغ العمولة";
            this.Column61.Name = "Column61";
            this.Column61.ReadOnly = true;
            this.Column61.Visible = false;
            this.Column61.Width = 89;
            // 
            // Column62
            // 
            this.Column62.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column62.DataPropertyName = "c_ACUR_NAME";
            this.Column62.HeaderText = "عملة العمولة";
            this.Column62.Name = "Column62";
            this.Column62.ReadOnly = true;
            this.Column62.Visible = false;
            this.Column62.Width = 89;
            // 
            // Column71
            // 
            this.Column71.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column71.DataPropertyName = "rate_online";
            this.Column71.HeaderText = "سعر التعادل";
            this.Column71.Name = "Column71";
            this.Column71.ReadOnly = true;
            this.Column71.Visible = false;
            this.Column71.Width = 90;
            // 
            // Column72
            // 
            this.Column72.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column72.DataPropertyName = "SBirth_Place";
            this.Column72.HeaderText = "مكان تولد المستلم";
            this.Column72.Name = "Column72";
            this.Column72.ReadOnly = true;
            this.Column72.Visible = false;
            this.Column72.Width = 116;
            // 
            // Column75
            // 
            this.Column75.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column75.DataPropertyName = "Source_money";
            this.Column75.HeaderText = "مصدر المال";
            this.Column75.Name = "Column75";
            this.Column75.ReadOnly = true;
            this.Column75.Width = 87;
            // 
            // Column76
            // 
            this.Column76.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column76.DataPropertyName = "Relation_S_R";
            this.Column76.HeaderText = "علاقة المرسل بالمستلم";
            this.Column76.Name = "Column76";
            this.Column76.ReadOnly = true;
            this.Column76.Width = 138;
            // 
            // Column50
            // 
            this.Column50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column50.DataPropertyName = "R_T_purpose";
            this.Column50.HeaderText = "غرض المستلم ";
            this.Column50.Name = "Column50";
            this.Column50.ReadOnly = true;
            this.Column50.Width = 102;
            // 
            // Column51
            // 
            this.Column51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column51.DataPropertyName = "r_Relation";
            this.Column51.HeaderText = "علاقة المستلم بالمرسل";
            this.Column51.Name = "Column51";
            this.Column51.ReadOnly = true;
            this.Column51.Width = 138;
            // 
            // Column54
            // 
            this.Column54.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column54.DataPropertyName = "s_job";
            this.Column54.HeaderText = "مهنة المرسل";
            this.Column54.Name = "Column54";
            this.Column54.ReadOnly = true;
            this.Column54.Width = 91;
            // 
            // Column55
            // 
            this.Column55.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column55.DataPropertyName = "s_details_job";
            this.Column55.HeaderText = "تفاصيل مهنة المرسل";
            this.Column55.Name = "Column55";
            this.Column55.ReadOnly = true;
            this.Column55.Width = 129;
            // 
            // Column56
            // 
            this.Column56.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column56.DataPropertyName = "r_job";
            this.Column56.HeaderText = "مهنة المستلم";
            this.Column56.Name = "Column56";
            this.Column56.ReadOnly = true;
            this.Column56.Width = 90;
            // 
            // Column63
            // 
            this.Column63.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column63.DataPropertyName = "r_details_job";
            this.Column63.HeaderText = "تفاصيل مهنة المستلم";
            this.Column63.Name = "Column63";
            this.Column63.ReadOnly = true;
            this.Column63.Width = 128;
            // 
            // Column70
            // 
            this.Column70.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column70.DataPropertyName = "Real_Paid_Name";
            this.Column70.HeaderText = "الاسم كما في الهوية";
            this.Column70.Name = "Column70";
            this.Column70.ReadOnly = true;
            this.Column70.Width = 128;
            // 
            // Column73
            // 
            this.Column73.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column73.DataPropertyName = "S_Acust_name";
            this.Column73.HeaderText = "اسم الوكيل المصدر";
            this.Column73.Name = "Column73";
            this.Column73.ReadOnly = true;
            this.Column73.Visible = false;
            this.Column73.Width = 120;
            // 
            // Column74
            // 
            this.Column74.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column74.DataPropertyName = "D_Acust_name";
            this.Column74.HeaderText = "اسم الوكيل الدافع";
            this.Column74.Name = "Column74";
            this.Column74.ReadOnly = true;
            this.Column74.Visible = false;
            this.Column74.Width = 115;
            // 
            // Column80
            // 
            this.Column80.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column80.DataPropertyName = "create_date";
            dataGridViewCellStyle73.Format = "dd/MM/yyyy";
            this.Column80.DefaultCellStyle = dataGridViewCellStyle73;
            this.Column80.HeaderText = "تاريخ انشاء الحوالة في مركز الاونلاين";
            this.Column80.Name = "Column80";
            this.Column80.ReadOnly = true;
            this.Column80.Visible = false;
            this.Column80.Width = 214;
            // 
            // Grd_outlier
            // 
            this.Grd_outlier.AllowUserToAddRows = false;
            this.Grd_outlier.AllowUserToDeleteRows = false;
            dataGridViewCellStyle77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle77.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_outlier.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle77;
            this.Grd_outlier.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle78.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle78.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle78.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle78.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_outlier.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle78;
            this.Grd_outlier.ColumnHeadersHeight = 24;
            this.Grd_outlier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.Column47,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.Column46,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66,
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewTextBoxColumn76,
            this.dataGridViewTextBoxColumn77,
            this.dataGridViewTextBoxColumn78});
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle90.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_outlier.DefaultCellStyle = dataGridViewCellStyle90;
            this.Grd_outlier.Location = new System.Drawing.Point(14, 540);
            this.Grd_outlier.MultiSelect = false;
            this.Grd_outlier.Name = "Grd_outlier";
            this.Grd_outlier.ReadOnly = true;
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle91.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle91.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle91.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle91.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle91.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_outlier.RowHeadersDefaultCellStyle = dataGridViewCellStyle91;
            this.Grd_outlier.RowHeadersVisible = false;
            dataGridViewCellStyle92.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_outlier.RowsDefaultCellStyle = dataGridViewCellStyle92;
            this.Grd_outlier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.Grd_outlier.Size = new System.Drawing.Size(868, 118);
            this.Grd_outlier.TabIndex = 1203;
            this.Grd_outlier.VirtualMode = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "rem_no";
            this.dataGridViewTextBoxColumn9.HeaderText = "رقم الحولة";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 81;
            // 
            // Column47
            // 
            this.Column47.DataPropertyName = "id";
            this.Column47.HeaderText = "محور الحوالات";
            this.Column47.Name = "Column47";
            this.Column47.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ACASE_NA";
            dataGridViewCellStyle79.Format = "n3";
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle79;
            this.dataGridViewTextBoxColumn11.HeaderText = "نوع العمليه";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 84;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn14.DataPropertyName = "R_amount";
            dataGridViewCellStyle80.Format = "n3";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewTextBoxColumn14.HeaderText = "مبلغ الحوالة";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 88;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "R_ACUR_NAME";
            this.dataGridViewTextBoxColumn15.HeaderText = "عملة الحوالة";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 88;
            // 
            // Column46
            // 
            this.Column46.DataPropertyName = "rem_loc_amount";
            dataGridViewCellStyle81.Format = "n3";
            this.Column46.DefaultCellStyle = dataGridViewCellStyle81;
            this.Column46.HeaderText = "المبلغ محلي";
            this.Column46.Name = "Column46";
            this.Column46.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "PR_ACUR_NAME";
            this.dataGridViewTextBoxColumn16.HeaderText = "عملة التسليم";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 88;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn18.DataPropertyName = "S_ACITY_NAME";
            this.dataGridViewTextBoxColumn18.HeaderText = "مدينة الارسال";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 97;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn19.DataPropertyName = "t_ACITY_NAME";
            this.dataGridViewTextBoxColumn19.HeaderText = "مدينة الاستلام";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 96;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn20.DataPropertyName = "user_name";
            this.dataGridViewTextBoxColumn20.HeaderText = "المستخدم";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 76;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn21.DataPropertyName = "Case_Date";
            dataGridViewCellStyle82.Format = "dd/MM/yyyy";
            dataGridViewCellStyle82.NullValue = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle82;
            this.dataGridViewTextBoxColumn21.HeaderText = "تاريخ الحالة";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 91;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn22.DataPropertyName = "C_DATE";
            dataGridViewCellStyle83.Format = "dd/MM/yyyy";
            dataGridViewCellStyle83.NullValue = "dd\'/\'MM\'/\'yyyy";
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridViewTextBoxColumn22.HeaderText = "تاريخ تنظيم السجلات";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 131;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn23.DataPropertyName = "S_name";
            this.dataGridViewTextBoxColumn23.HeaderText = "اسم المرسل";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 87;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn24.DataPropertyName = "S_phone";
            this.dataGridViewTextBoxColumn24.HeaderText = "هاتف المرسل";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 97;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn25.DataPropertyName = "S_address";
            this.dataGridViewTextBoxColumn25.HeaderText = "عنوان المرسل";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 97;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn26.DataPropertyName = "S_Street";
            this.dataGridViewTextBoxColumn26.HeaderText = "الزقاق";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 64;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn27.DataPropertyName = "S_Suburb";
            this.dataGridViewTextBoxColumn27.HeaderText = "الحي";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 57;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn28.DataPropertyName = "S_Post_Code";
            this.dataGridViewTextBoxColumn28.HeaderText = "الرقم البريدي";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Visible = false;
            this.dataGridViewTextBoxColumn28.Width = 94;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn29.DataPropertyName = "S_State";
            this.dataGridViewTextBoxColumn29.HeaderText = "المحافظة";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 76;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn30.DataPropertyName = "S_Social_No";
            this.dataGridViewTextBoxColumn30.HeaderText = "الرقم الوطني للمرسل";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Visible = false;
            this.dataGridViewTextBoxColumn30.Width = 129;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "sFRM_ADOC_NA";
            this.dataGridViewTextBoxColumn31.HeaderText = "نوع وثيقة المرسل";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn32.DataPropertyName = "S_doc_no";
            this.dataGridViewTextBoxColumn32.HeaderText = "رقم وثيقة المرسل";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 114;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn33.DataPropertyName = "S_doc_ida";
            dataGridViewCellStyle84.Format = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn33.DefaultCellStyle = dataGridViewCellStyle84;
            this.dataGridViewTextBoxColumn33.HeaderText = "تاريخ اصدار وثيقة المرسل";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 155;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn34.DataPropertyName = "S_doc_eda";
            dataGridViewCellStyle85.Format = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridViewTextBoxColumn34.HeaderText = "تاريخ انتهاء وثيقة المرسل";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 155;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn35.DataPropertyName = "S_doc_issue";
            this.dataGridViewTextBoxColumn35.HeaderText = "اصدار وثيقة المرسل";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 124;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn36.DataPropertyName = "s_Email";
            this.dataGridViewTextBoxColumn36.HeaderText = "ايميل المرسل";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 94;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn37.DataPropertyName = "s_A_NAT_NAME";
            this.dataGridViewTextBoxColumn37.HeaderText = "جنسية المرسل";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn38.DataPropertyName = "s_notes";
            this.dataGridViewTextBoxColumn38.HeaderText = "ملاحظات المرسل";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 113;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn39.DataPropertyName = "r_name";
            this.dataGridViewTextBoxColumn39.HeaderText = "اسم المستلم";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 86;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn40.DataPropertyName = "R_phone";
            this.dataGridViewTextBoxColumn40.HeaderText = "هاتف المستلم";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 96;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn41.DataPropertyName = "r_ACITY_NAME";
            this.dataGridViewTextBoxColumn41.HeaderText = "مدينة المستلم";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 93;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn42.DataPropertyName = "r_ACOUN_NAME";
            this.dataGridViewTextBoxColumn42.HeaderText = "بلد المستلم";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 81;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn43.DataPropertyName = "r_address";
            this.dataGridViewTextBoxColumn43.HeaderText = "عنوان المستلم";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 96;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn44.DataPropertyName = "r_Street";
            this.dataGridViewTextBoxColumn44.HeaderText = "الزقاق";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 64;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn45.DataPropertyName = "r_Suburb";
            this.dataGridViewTextBoxColumn45.HeaderText = "الحي للمستلم";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 93;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn46.DataPropertyName = "r_Post_Code";
            this.dataGridViewTextBoxColumn46.HeaderText = "الرمز البريدي للمستلم";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Visible = false;
            this.dataGridViewTextBoxColumn46.Width = 131;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn47.DataPropertyName = "r_State";
            this.dataGridViewTextBoxColumn47.HeaderText = "محافظة المستلم";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            this.dataGridViewTextBoxColumn47.Width = 106;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "R_Social_No";
            this.dataGridViewTextBoxColumn48.HeaderText = "الرقم الوطني للمستلم";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Visible = false;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "rFRM_ADOC_NA";
            this.dataGridViewTextBoxColumn49.HeaderText = "نوع وثيقة المستلم";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn50.DataPropertyName = "r_doc_no";
            this.dataGridViewTextBoxColumn50.HeaderText = "رقم وثيقة المستلم";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            this.dataGridViewTextBoxColumn50.Width = 113;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn51.DataPropertyName = "r_doc_ida";
            dataGridViewCellStyle86.Format = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn51.DefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridViewTextBoxColumn51.HeaderText = "تاريخ اصدار الوثيقة المستلم";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Width = 160;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn52.DataPropertyName = "r_doc_eda";
            dataGridViewCellStyle87.Format = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn52.DefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridViewTextBoxColumn52.HeaderText = "تاريخ انتهاء الوثيقة المستلم";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Width = 160;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn53.DataPropertyName = "r_doc_issue";
            this.dataGridViewTextBoxColumn53.HeaderText = "اصدار وثيقة المستلم";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            this.dataGridViewTextBoxColumn53.Width = 123;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn54.DataPropertyName = "r_Email";
            this.dataGridViewTextBoxColumn54.HeaderText = "ايميل المستلم";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Width = 93;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn55.DataPropertyName = "T_purpose";
            this.dataGridViewTextBoxColumn55.HeaderText = "غرض الارسال";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            this.dataGridViewTextBoxColumn55.Width = 101;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn56.DataPropertyName = "r_notes";
            this.dataGridViewTextBoxColumn56.HeaderText = "ملاحظات المستلم";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            this.dataGridViewTextBoxColumn56.Width = 112;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn57.DataPropertyName = "vo_no";
            this.dataGridViewTextBoxColumn57.HeaderText = "رقم السند";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            this.dataGridViewTextBoxColumn57.Width = 77;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn58.DataPropertyName = "In_rec_date";
            dataGridViewCellStyle88.Format = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn58.DefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridViewTextBoxColumn58.HeaderText = "تاريخ السجلات";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            this.dataGridViewTextBoxColumn58.Width = 104;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn59.DataPropertyName = "sbirth_date";
            this.dataGridViewTextBoxColumn59.HeaderText = "تولد المرسل";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.ReadOnly = true;
            this.dataGridViewTextBoxColumn59.Width = 88;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn60.DataPropertyName = "rbirth_date";
            this.dataGridViewTextBoxColumn60.HeaderText = "تولد المستلم";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Width = 87;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn61.DataPropertyName = "s_ocomm";
            this.dataGridViewTextBoxColumn61.HeaderText = "عمولة المرسل";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.ReadOnly = true;
            this.dataGridViewTextBoxColumn61.Visible = false;
            this.dataGridViewTextBoxColumn61.Width = 97;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn62.DataPropertyName = "s_ACUR_NAME";
            this.dataGridViewTextBoxColumn62.HeaderText = "عملة المرسل";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.ReadOnly = true;
            this.dataGridViewTextBoxColumn62.Visible = false;
            this.dataGridViewTextBoxColumn62.Width = 91;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn63.DataPropertyName = "c_ocomm";
            this.dataGridViewTextBoxColumn63.HeaderText = "مبلغ العمولة";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.ReadOnly = true;
            this.dataGridViewTextBoxColumn63.Visible = false;
            this.dataGridViewTextBoxColumn63.Width = 89;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn64.DataPropertyName = "c_ACUR_NAME";
            this.dataGridViewTextBoxColumn64.HeaderText = "عملة العمولة";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.ReadOnly = true;
            this.dataGridViewTextBoxColumn64.Visible = false;
            this.dataGridViewTextBoxColumn64.Width = 89;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn65.DataPropertyName = "rate_online";
            this.dataGridViewTextBoxColumn65.HeaderText = "سعر التعادل";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.ReadOnly = true;
            this.dataGridViewTextBoxColumn65.Visible = false;
            this.dataGridViewTextBoxColumn65.Width = 90;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn66.DataPropertyName = "SBirth_Place";
            this.dataGridViewTextBoxColumn66.HeaderText = "مكان تولد المستلم";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.ReadOnly = true;
            this.dataGridViewTextBoxColumn66.Visible = false;
            this.dataGridViewTextBoxColumn66.Width = 116;
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn67.DataPropertyName = "Source_money";
            this.dataGridViewTextBoxColumn67.HeaderText = "مصدر المال";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.ReadOnly = true;
            this.dataGridViewTextBoxColumn67.Width = 87;
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn68.DataPropertyName = "Relation_S_R";
            this.dataGridViewTextBoxColumn68.HeaderText = "علاقة المرسل بالمستلم";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.ReadOnly = true;
            this.dataGridViewTextBoxColumn68.Width = 138;
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn69.DataPropertyName = "R_T_purpose";
            this.dataGridViewTextBoxColumn69.HeaderText = "غرض المستلم ";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.ReadOnly = true;
            this.dataGridViewTextBoxColumn69.Width = 102;
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn70.DataPropertyName = "r_Relation";
            this.dataGridViewTextBoxColumn70.HeaderText = "علاقة المستلم بالمرسل";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.ReadOnly = true;
            this.dataGridViewTextBoxColumn70.Width = 138;
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn71.DataPropertyName = "s_job";
            this.dataGridViewTextBoxColumn71.HeaderText = "مهنة المرسل";
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.ReadOnly = true;
            this.dataGridViewTextBoxColumn71.Width = 91;
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn72.DataPropertyName = "s_details_job";
            this.dataGridViewTextBoxColumn72.HeaderText = "تفاصيل مهنة المرسل";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.ReadOnly = true;
            this.dataGridViewTextBoxColumn72.Width = 129;
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn73.DataPropertyName = "r_job";
            this.dataGridViewTextBoxColumn73.HeaderText = "مهنة المستلم";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.ReadOnly = true;
            this.dataGridViewTextBoxColumn73.Width = 90;
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn74.DataPropertyName = "r_details_job";
            this.dataGridViewTextBoxColumn74.HeaderText = "تفاصيل مهنة المستلم";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.ReadOnly = true;
            this.dataGridViewTextBoxColumn74.Width = 128;
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn75.DataPropertyName = "Real_Paid_Name";
            this.dataGridViewTextBoxColumn75.HeaderText = "الاسم كما في الهوية";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.ReadOnly = true;
            this.dataGridViewTextBoxColumn75.Width = 128;
            // 
            // dataGridViewTextBoxColumn76
            // 
            this.dataGridViewTextBoxColumn76.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn76.DataPropertyName = "S_Acust_name";
            this.dataGridViewTextBoxColumn76.HeaderText = "اسم الوكيل المصدر";
            this.dataGridViewTextBoxColumn76.Name = "dataGridViewTextBoxColumn76";
            this.dataGridViewTextBoxColumn76.ReadOnly = true;
            this.dataGridViewTextBoxColumn76.Visible = false;
            this.dataGridViewTextBoxColumn76.Width = 120;
            // 
            // dataGridViewTextBoxColumn77
            // 
            this.dataGridViewTextBoxColumn77.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn77.DataPropertyName = "D_Acust_name";
            this.dataGridViewTextBoxColumn77.HeaderText = "اسم الوكيل الدافع";
            this.dataGridViewTextBoxColumn77.Name = "dataGridViewTextBoxColumn77";
            this.dataGridViewTextBoxColumn77.ReadOnly = true;
            this.dataGridViewTextBoxColumn77.Visible = false;
            this.dataGridViewTextBoxColumn77.Width = 115;
            // 
            // dataGridViewTextBoxColumn78
            // 
            this.dataGridViewTextBoxColumn78.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn78.DataPropertyName = "create_date";
            dataGridViewCellStyle89.Format = "dd/MM/yyyy";
            this.dataGridViewTextBoxColumn78.DefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridViewTextBoxColumn78.HeaderText = "تاريخ انشاء الحوالة في مركز الاونلاين";
            this.dataGridViewTextBoxColumn78.Name = "dataGridViewTextBoxColumn78";
            this.dataGridViewTextBoxColumn78.ReadOnly = true;
            this.dataGridViewTextBoxColumn78.Visible = false;
            this.dataGridViewTextBoxColumn78.Width = 214;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(261, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 16);
            this.label1.TabIndex = 1204;
            this.label1.Text = "فـــمـــــا فــــــوق";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(261, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 16);
            this.label2.TabIndex = 1205;
            this.label2.Text = "فـــمـــــا فــــــوق";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(352, 661);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 28);
            this.button1.TabIndex = 1206;
            this.button1.Text = "تصديــر";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(448, 661);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 28);
            this.button2.TabIndex = 1207;
            this.button2.Text = "انهــــــاء";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(13, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 16);
            this.label3.TabIndex = 1208;
            this.label3.Text = "الــتاريـــخ مـن :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(206, 12);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 16);
            this.label13.TabIndex = 1209;
            this.label13.Text = "إلــــى :";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(9, 691);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(878, 1);
            this.flowLayoutPanel3.TabIndex = 531;
            // 
            // RemNum
            // 
            this.RemNum.BackColor = System.Drawing.Color.White;
            this.RemNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.RemNum.Location = new System.Drawing.Point(102, 40);
            this.RemNum.Name = "RemNum";
            this.RemNum.Size = new System.Drawing.Size(159, 22);
            this.RemNum.TabIndex = 1210;
            // 
            // Amount
            // 
            this.Amount.BackColor = System.Drawing.Color.White;
            this.Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Amount.Location = new System.Drawing.Point(102, 63);
            this.Amount.Name = "Amount";
            this.Amount.Size = new System.Drawing.Size(159, 22);
            this.Amount.TabIndex = 1211;
            // 
            // Graph_button
            // 
            this.Graph_button.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Graph_button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Graph_button.Location = new System.Drawing.Point(545, 661);
            this.Graph_button.Name = "Graph_button";
            this.Graph_button.Size = new System.Drawing.Size(99, 28);
            this.Graph_button.TabIndex = 1212;
            this.Graph_button.Text = "الرسم البياني";
            this.Graph_button.UseVisualStyleBackColor = true;
            this.Graph_button.Click += new System.EventHandler(this.Graph_button_Click);
            // 
            // outlier_Rem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 696);
            this.Controls.Add(this.Grd_per_info);
            this.Controls.Add(this.Graph_button);
            this.Controls.Add(this.Amount);
            this.Controls.Add(this.RemNum);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Grd_outlier);
            this.Controls.Add(this.Grd_Rem);
            this.Controls.Add(this.CHK1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Cbo_curs);
            this.Controls.Add(this.cbo_curType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.RemType);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.OperType);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Grd_per);
            this.Controls.Add(this.Grd_per_id);
            this.Controls.Add(this.remove_all_btn);
            this.Controls.Add(this.remove_one_btn);
            this.Controls.Add(this.move_all_btn);
            this.Controls.Add(this.move_one_btn);
            this.Controls.Add(this.Names);
            this.Controls.Add(this.flowLayoutPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "outlier_Rem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "553";
            this.Text = "Outlier_Rem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.outlier_Rem_FormClosing);
            this.Load += new System.EventHandler(this.outlier_Rem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_per_info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_outlier)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridView Grd_per;
        private System.Windows.Forms.DataGridView Grd_per_id;
        private System.Windows.Forms.Button remove_all_btn;
        private System.Windows.Forms.Button remove_one_btn;
        private System.Windows.Forms.Button move_all_btn;
        private System.Windows.Forms.Button move_one_btn;
        private System.Windows.Forms.TextBox Names;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker TxtFromDate;
        private System.Windows.Forms.DateTimePicker TxtToDate;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.ComboBox OperType;
        private System.Windows.Forms.ComboBox RemType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView Grd_per_info;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbo_curType;
        private System.Windows.Forms.ComboBox Cbo_curs;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox CHK1;
        private System.Windows.Forms.DataGridView Grd_Rem;
        private System.Windows.Forms.DataGridView Grd_outlier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn min_rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn max_rate;
        private System.Windows.Forms.TextBox RemNum;
        private System.Windows.Forms.TextBox Amount;
        private System.Windows.Forms.Button Graph_button;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn77;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn78;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column52;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column53;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column57;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column58;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column59;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column60;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column61;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column62;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column71;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column72;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column75;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column76;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column50;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column51;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column54;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column55;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column56;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column63;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column70;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column73;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column74;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column80;
    }
}