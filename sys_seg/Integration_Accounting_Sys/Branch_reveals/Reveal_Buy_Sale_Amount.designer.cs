﻿namespace Integration_Accounting_Sys
{
    partial class Reveal_Buy_Sale_Amount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Grd_buy_sale = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Browser = new System.Windows.Forms.Button();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Txt_from_date = new System.Windows.Forms.DateTimePicker();
            this.Txt_To_date = new System.Windows.Forms.DateTimePicker();
            this.cmb_Term = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this.Txt_Net_total_Sale = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Net_Loc_RSale = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Sale_Loc_Exch = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Net_Loc_Sale = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Net_total_Buy = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Net_loc_RBuy = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Buy_Loc_Exch = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_Net_Loc_Buy = new System.Windows.Forms.Sample.DecimalTextBox();
            this.searchTextBox11 = new Integration_Accounting_Sys.SearchTextBox();
            this.searchTextBox9 = new Integration_Accounting_Sys.SearchTextBox();
            this.Grd_years = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_year = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel17.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_buy_sale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-1, 30);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1015, 1);
            this.flowLayoutPanel9.TabIndex = 533;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 612);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1013, 22);
            this.statusStrip1.TabIndex = 537;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(56, 17);
            this.LblRec.Text = "Records";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(506, 583);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 6;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(418, 583);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 26);
            this.AddBtn.TabIndex = 5;
            this.AddBtn.Text = "طباعة";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel17);
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 152);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1, 275);
            this.flowLayoutPanel3.TabIndex = 544;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-4, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 302);
            this.flowLayoutPanel1.TabIndex = 545;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Controls.Add(this.flowLayoutPanel18);
            this.flowLayoutPanel17.Location = new System.Drawing.Point(-4, 311);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(1, 325);
            this.flowLayoutPanel17.TabIndex = 544;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(-4, 3);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(1, 302);
            this.flowLayoutPanel18.TabIndex = 545;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(-1004, 642);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(1001, 1);
            this.flowLayoutPanel16.TabIndex = 543;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(-1004, 649);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(1001, 0);
            this.flowLayoutPanel19.TabIndex = 582;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(6, 426);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1000, 1);
            this.flowLayoutPanel5.TabIndex = 546;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(5, 152);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1001, 1);
            this.flowLayoutPanel2.TabIndex = 543;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(1005, 153);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 274);
            this.flowLayoutPanel6.TabIndex = 555;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(11, 548);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(199, 14);
            this.label9.TabIndex = 567;
            this.label9.Text = "اجمالي صافي المشتريات مقابـــل";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(11, 518);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 14);
            this.label10.TabIndex = 569;
            this.label10.Text = "اجمالـي رد المشتريــات مقابــــــــل";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(9, 489);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(273, 14);
            this.label11.TabIndex = 571;
            this.label11.Text = "إجمالي المشتريات مقابل عملات اجنبية اخرى :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(11, 460);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(198, 14);
            this.label12.TabIndex = 573;
            this.label12.Text = "إجمالــي المشتريــات مقابــــــــــــل";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(520, 460);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 14);
            this.label5.TabIndex = 581;
            this.label5.Text = "اجمالـي المبيعــات مقابـــــــــل";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(6, 578);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1000, 1);
            this.flowLayoutPanel4.TabIndex = 583;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(6, 441);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1000, 1);
            this.flowLayoutPanel7.TabIndex = 547;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(1005, 441);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1, 137);
            this.flowLayoutPanel8.TabIndex = 584;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(6, 442);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1, 136);
            this.flowLayoutPanel10.TabIndex = 585;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(16, 430);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(282, 14);
            this.label13.TabIndex = 765;
            this.label13.Text = "المجاميع على مستوى العملة بعملتها المحلية ...";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DarkRed;
            this.label14.Location = new System.Drawing.Point(197, 119);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(82, 14);
            this.label14.TabIndex = 789;
            this.label14.Text = "الفترة الــى /";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DarkRed;
            this.label15.Location = new System.Drawing.Point(5, 119);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(72, 14);
            this.label15.TabIndex = 788;
            this.label15.Text = "الفترة من /";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(207, 460);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 14);
            this.label16.TabIndex = 792;
            this.label16.Text = "مقابل :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(207, 518);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 14);
            this.label17.TabIndex = 793;
            this.label17.Text = "مقابل :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(207, 548);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 14);
            this.label18.TabIndex = 794;
            this.label18.Text = "مقابل :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(696, 460);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 14);
            this.label19.TabIndex = 795;
            this.label19.Text = "مقابل :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(696, 518);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 799;
            this.label6.Text = "مقابل :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(520, 518);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(176, 14);
            this.label21.TabIndex = 798;
            this.label21.Text = "اجمالي رد المبيعـات مقابــــــل";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(696, 548);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 801;
            this.label7.Text = "مقابل :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(520, 548);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(178, 14);
            this.label8.TabIndex = 800;
            this.label8.Text = "اجمالي صافي المبيعات مقابل";
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(5, 152);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(1001, 1);
            this.flowLayoutPanel12.TabIndex = 543;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(5, 152);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(1, 275);
            this.flowLayoutPanel13.TabIndex = 544;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-4, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(1, 302);
            this.flowLayoutPanel14.TabIndex = 545;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(790, 119);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 14);
            this.label22.TabIndex = 556;
            this.label22.Text = "مشتريـــات";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(907, 119);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 14);
            this.label23.TabIndex = 557;
            this.label23.Text = "مبيعــــات";
            // 
            // Grd_buy_sale
            // 
            this.Grd_buy_sale.AllowUserToAddRows = false;
            this.Grd_buy_sale.AllowUserToDeleteRows = false;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_buy_sale.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_buy_sale.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.Grd_buy_sale.ColumnHeadersHeight = 25;
            this.Grd_buy_sale.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.Grd_buy_sale.Location = new System.Drawing.Point(11, 159);
            this.Grd_buy_sale.Name = "Grd_buy_sale";
            this.Grd_buy_sale.RowHeadersWidth = 25;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_buy_sale.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.Grd_buy_sale.Size = new System.Drawing.Size(988, 262);
            this.Grd_buy_sale.TabIndex = 554;
            this.Grd_buy_sale.SelectionChanged += new System.EventHandler(this.Grd_limit_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "For_cur_id";
            this.Column1.HeaderText = "رمز العملة";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Cur_ANAME";
            this.Column2.HeaderText = "اسم العملة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "NetBuy_For_Aud";
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.PaleTurquoise;
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.Format = "N3";
            dataGridViewCellStyle20.NullValue = null;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.White;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle20;
            this.Column3.HeaderText = "جمهور";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 170;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "NetBuy_For_Bank";
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.PaleTurquoise;
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.Format = "N3";
            dataGridViewCellStyle21.NullValue = null;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle21;
            this.Column4.HeaderText = "بنوك";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 170;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "NetBuy_For_Eco";
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.PaleTurquoise;
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.Format = "N3";
            dataGridViewCellStyle22.NullValue = null;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle22;
            this.Column5.HeaderText = "صرافين";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 170;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "NetBuy_For_Exch";
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.PaleTurquoise;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.Format = "N3";
            dataGridViewCellStyle23.NullValue = null;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle23;
            this.Column6.HeaderText = "مبادلة";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 170;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Total_Net_Buy";
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.PaleTurquoise;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.Format = "N3";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle24;
            this.Column7.HeaderText = "مجموع";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 170;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Net_Sale_For_Aud";
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle25;
            this.Column8.HeaderText = "جمهور";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 170;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Net_Sale_For_Bank";
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle26;
            this.Column9.HeaderText = "بنوك";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 170;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Net_Sale_For_Eco";
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.Format = "N3";
            this.Column10.DefaultCellStyle = dataGridViewCellStyle27;
            this.Column10.HeaderText = "صرافين";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 170;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Net_Sale_For_Exch";
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.Format = "N3";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle28;
            this.Column11.HeaderText = "مبادلة";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 170;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Total_Net_Sale";
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.Format = "N3";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle29;
            this.Column12.HeaderText = "مجموع";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 170;
            // 
            // Btn_Browser
            // 
            this.Btn_Browser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Browser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Btn_Browser.Location = new System.Drawing.Point(685, 113);
            this.Btn_Browser.Name = "Btn_Browser";
            this.Btn_Browser.Size = new System.Drawing.Size(99, 26);
            this.Btn_Browser.TabIndex = 804;
            this.Btn_Browser.Text = "عرض";
            this.Btn_Browser.UseVisualStyleBackColor = true;
            this.Btn_Browser.Click += new System.EventHandler(this.button1_Click);
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-3, 145);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(1044, 1);
            this.flowLayoutPanel11.TabIndex = 806;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-3, 145);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(1044, 1);
            this.flowLayoutPanel15.TabIndex = 805;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(621, 7);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(94, 14);
            this.label3.TabIndex = 813;
            this.label3.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(721, 3);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(118, 23);
            this.Txt_Loc_Cur.TabIndex = 812;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(381, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(237, 23);
            this.TxtUser.TabIndex = 811;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(10, 3);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(278, 23);
            this.TxtTerm_Name.TabIndex = 809;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(852, 7);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(50, 14);
            this.label1.TabIndex = 808;
            this.label1.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(907, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(99, 23);
            this.TxtIn_Rec_Date.TabIndex = 810;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(303, 7);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(76, 14);
            this.label24.TabIndex = 807;
            this.label24.Text = "المستخــدم:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(520, 489);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(261, 14);
            this.label20.TabIndex = 796;
            this.label20.Text = "إجمالي المبيعات مقابل عملات اجنبية اخرى :";
            // 
            // Txt_from_date
            // 
            this.Txt_from_date.Checked = false;
            this.Txt_from_date.CustomFormat = "dd/mm/yyyy";
            this.Txt_from_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_from_date.Location = new System.Drawing.Point(83, 116);
            this.Txt_from_date.Name = "Txt_from_date";
            this.Txt_from_date.ShowCheckBox = true;
            this.Txt_from_date.Size = new System.Drawing.Size(110, 20);
            this.Txt_from_date.TabIndex = 919;
            // 
            // Txt_To_date
            // 
            this.Txt_To_date.Checked = false;
            this.Txt_To_date.CustomFormat = "dd/mm/yyyy";
            this.Txt_To_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_To_date.Location = new System.Drawing.Point(282, 116);
            this.Txt_To_date.Name = "Txt_To_date";
            this.Txt_To_date.ShowCheckBox = true;
            this.Txt_To_date.Size = new System.Drawing.Size(117, 20);
            this.Txt_To_date.TabIndex = 920;
            // 
            // cmb_Term
            // 
            this.cmb_Term.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_Term.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_Term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Term.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Term.FormattingEnabled = true;
            this.cmb_Term.Items.AddRange(new object[] {
            "أيقاف الحوالة",
            "ألغاء الأيقاف "});
            this.cmb_Term.Location = new System.Drawing.Point(471, 114);
            this.cmb_Term.Name = "cmb_Term";
            this.cmb_Term.Size = new System.Drawing.Size(177, 24);
            this.cmb_Term.TabIndex = 1165;
            this.cmb_Term.SelectedIndexChanged += new System.EventHandler(this.cmb_Term_SelectedIndexChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(405, 119);
            this.label65.Name = "label65";
            this.label65.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label65.Size = new System.Drawing.Size(64, 14);
            this.label65.TabIndex = 1166;
            this.label65.Text = "الفــــــــرع:";
            // 
            // Txt_Net_total_Sale
            // 
            this.Txt_Net_total_Sale.BackColor = System.Drawing.Color.White;
            this.Txt_Net_total_Sale.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Net_total_Sale.Location = new System.Drawing.Point(784, 544);
            this.Txt_Net_total_Sale.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Net_total_Sale.Name = "Txt_Net_total_Sale";
            this.Txt_Net_total_Sale.NumberDecimalDigits = 3;
            this.Txt_Net_total_Sale.NumberDecimalSeparator = ".";
            this.Txt_Net_total_Sale.NumberGroupSeparator = ",";
            this.Txt_Net_total_Sale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Net_total_Sale.Size = new System.Drawing.Size(208, 23);
            this.Txt_Net_total_Sale.TabIndex = 821;
            this.Txt_Net_total_Sale.Text = "0.000";
            // 
            // Txt_Net_Loc_RSale
            // 
            this.Txt_Net_Loc_RSale.BackColor = System.Drawing.Color.White;
            this.Txt_Net_Loc_RSale.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Net_Loc_RSale.Location = new System.Drawing.Point(784, 514);
            this.Txt_Net_Loc_RSale.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Net_Loc_RSale.Name = "Txt_Net_Loc_RSale";
            this.Txt_Net_Loc_RSale.NumberDecimalDigits = 3;
            this.Txt_Net_Loc_RSale.NumberDecimalSeparator = ".";
            this.Txt_Net_Loc_RSale.NumberGroupSeparator = ",";
            this.Txt_Net_Loc_RSale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Net_Loc_RSale.Size = new System.Drawing.Size(208, 23);
            this.Txt_Net_Loc_RSale.TabIndex = 820;
            this.Txt_Net_Loc_RSale.Text = "0.000";
            // 
            // Txt_Sale_Loc_Exch
            // 
            this.Txt_Sale_Loc_Exch.BackColor = System.Drawing.Color.White;
            this.Txt_Sale_Loc_Exch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Sale_Loc_Exch.Location = new System.Drawing.Point(784, 485);
            this.Txt_Sale_Loc_Exch.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Sale_Loc_Exch.Name = "Txt_Sale_Loc_Exch";
            this.Txt_Sale_Loc_Exch.NumberDecimalDigits = 3;
            this.Txt_Sale_Loc_Exch.NumberDecimalSeparator = ".";
            this.Txt_Sale_Loc_Exch.NumberGroupSeparator = ",";
            this.Txt_Sale_Loc_Exch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Sale_Loc_Exch.Size = new System.Drawing.Size(208, 23);
            this.Txt_Sale_Loc_Exch.TabIndex = 819;
            this.Txt_Sale_Loc_Exch.Text = "0.000";
            // 
            // Txt_Net_Loc_Sale
            // 
            this.Txt_Net_Loc_Sale.BackColor = System.Drawing.Color.White;
            this.Txt_Net_Loc_Sale.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Net_Loc_Sale.Location = new System.Drawing.Point(784, 456);
            this.Txt_Net_Loc_Sale.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Net_Loc_Sale.Name = "Txt_Net_Loc_Sale";
            this.Txt_Net_Loc_Sale.NumberDecimalDigits = 3;
            this.Txt_Net_Loc_Sale.NumberDecimalSeparator = ".";
            this.Txt_Net_Loc_Sale.NumberGroupSeparator = ",";
            this.Txt_Net_Loc_Sale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Net_Loc_Sale.Size = new System.Drawing.Size(208, 23);
            this.Txt_Net_Loc_Sale.TabIndex = 818;
            this.Txt_Net_Loc_Sale.Text = "0.000";
            // 
            // Txt_Net_total_Buy
            // 
            this.Txt_Net_total_Buy.BackColor = System.Drawing.Color.White;
            this.Txt_Net_total_Buy.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Net_total_Buy.Location = new System.Drawing.Point(289, 544);
            this.Txt_Net_total_Buy.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Net_total_Buy.Name = "Txt_Net_total_Buy";
            this.Txt_Net_total_Buy.NumberDecimalDigits = 3;
            this.Txt_Net_total_Buy.NumberDecimalSeparator = ".";
            this.Txt_Net_total_Buy.NumberGroupSeparator = ",";
            this.Txt_Net_total_Buy.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Net_total_Buy.Size = new System.Drawing.Size(208, 23);
            this.Txt_Net_total_Buy.TabIndex = 817;
            this.Txt_Net_total_Buy.Text = "0.000";
            // 
            // Txt_Net_loc_RBuy
            // 
            this.Txt_Net_loc_RBuy.BackColor = System.Drawing.Color.White;
            this.Txt_Net_loc_RBuy.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Net_loc_RBuy.Location = new System.Drawing.Point(289, 514);
            this.Txt_Net_loc_RBuy.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Net_loc_RBuy.Name = "Txt_Net_loc_RBuy";
            this.Txt_Net_loc_RBuy.NumberDecimalDigits = 3;
            this.Txt_Net_loc_RBuy.NumberDecimalSeparator = ".";
            this.Txt_Net_loc_RBuy.NumberGroupSeparator = ",";
            this.Txt_Net_loc_RBuy.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Net_loc_RBuy.Size = new System.Drawing.Size(208, 23);
            this.Txt_Net_loc_RBuy.TabIndex = 816;
            this.Txt_Net_loc_RBuy.Text = "0.000";
            // 
            // Txt_Buy_Loc_Exch
            // 
            this.Txt_Buy_Loc_Exch.BackColor = System.Drawing.Color.White;
            this.Txt_Buy_Loc_Exch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buy_Loc_Exch.Location = new System.Drawing.Point(289, 485);
            this.Txt_Buy_Loc_Exch.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Buy_Loc_Exch.Name = "Txt_Buy_Loc_Exch";
            this.Txt_Buy_Loc_Exch.NumberDecimalDigits = 3;
            this.Txt_Buy_Loc_Exch.NumberDecimalSeparator = ".";
            this.Txt_Buy_Loc_Exch.NumberGroupSeparator = ",";
            this.Txt_Buy_Loc_Exch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Buy_Loc_Exch.Size = new System.Drawing.Size(208, 23);
            this.Txt_Buy_Loc_Exch.TabIndex = 815;
            this.Txt_Buy_Loc_Exch.Text = "0.000";
            // 
            // Txt_Net_Loc_Buy
            // 
            this.Txt_Net_Loc_Buy.BackColor = System.Drawing.Color.White;
            this.Txt_Net_Loc_Buy.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Net_Loc_Buy.Location = new System.Drawing.Point(289, 456);
            this.Txt_Net_Loc_Buy.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Net_Loc_Buy.Name = "Txt_Net_Loc_Buy";
            this.Txt_Net_Loc_Buy.NumberDecimalDigits = 3;
            this.Txt_Net_Loc_Buy.NumberDecimalSeparator = ".";
            this.Txt_Net_Loc_Buy.NumberGroupSeparator = ",";
            this.Txt_Net_Loc_Buy.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Net_Loc_Buy.Size = new System.Drawing.Size(208, 23);
            this.Txt_Net_Loc_Buy.TabIndex = 814;
            this.Txt_Net_Loc_Buy.Text = "0.000";
            // 
            // searchTextBox11
            // 
            this.searchTextBox11.BackColor = System.Drawing.Color.Gainsboro;
            this.searchTextBox11.Enabled = false;
            this.searchTextBox11.Location = new System.Drawing.Point(862, 116);
            this.searchTextBox11.Name = "searchTextBox11";
            this.searchTextBox11.Size = new System.Drawing.Size(40, 20);
            this.searchTextBox11.TabIndex = 803;
            // 
            // searchTextBox9
            // 
            this.searchTextBox9.BackColor = System.Drawing.Color.White;
            this.searchTextBox9.Enabled = false;
            this.searchTextBox9.Location = new System.Drawing.Point(969, 116);
            this.searchTextBox9.Name = "searchTextBox9";
            this.searchTextBox9.Size = new System.Drawing.Size(40, 20);
            this.searchTextBox9.TabIndex = 802;
            // 
            // Grd_years
            // 
            this.Grd_years.AllowUserToAddRows = false;
            this.Grd_years.AllowUserToDeleteRows = false;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle31;
            this.Grd_years.BackgroundColor = System.Drawing.Color.White;
            this.Grd_years.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_years.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_years.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.Grd_years.ColumnHeadersHeight = 45;
            this.Grd_years.ColumnHeadersVisible = false;
            this.Grd_years.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_years.DefaultCellStyle = dataGridViewCellStyle33;
            this.Grd_years.Location = new System.Drawing.Point(14, 61);
            this.Grd_years.Name = "Grd_years";
            this.Grd_years.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_years.RowHeadersVisible = false;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_years.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this.Grd_years.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_years.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_years.Size = new System.Drawing.Size(317, 48);
            this.Grd_years.TabIndex = 1169;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FromNrec_date";
            this.dataGridViewTextBoxColumn2.HeaderText = "التاريخ من";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 175;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TONrec_date";
            this.dataGridViewTextBoxColumn1.HeaderText = "التاريخ الى";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // cbo_year
            // 
            this.cbo_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_year.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_year.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_year.FormattingEnabled = true;
            this.cbo_year.Items.AddRange(new object[] {
            "البيانات الحالية",
            "البيانات المرحلة"});
            this.cbo_year.Location = new System.Drawing.Point(102, 36);
            this.cbo_year.Name = "cbo_year";
            this.cbo_year.Size = new System.Drawing.Size(230, 24);
            this.cbo_year.TabIndex = 1167;
            this.cbo_year.SelectedIndexChanged += new System.EventHandler(this.cbo_year_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(9, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 14);
            this.label4.TabIndex = 1168;
            this.label4.Text = "نـــــوع البحـــث:";
            // 
            // Reveal_Buy_Sale_Amount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 634);
            this.Controls.Add(this.Grd_years);
            this.Controls.Add(this.cbo_year);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.cmb_Term);
            this.Controls.Add(this.Txt_To_date);
            this.Controls.Add(this.Txt_from_date);
            this.Controls.Add(this.Grd_buy_sale);
            this.Controls.Add(this.Txt_Net_total_Sale);
            this.Controls.Add(this.Txt_Net_Loc_RSale);
            this.Controls.Add(this.Txt_Sale_Loc_Exch);
            this.Controls.Add(this.Txt_Net_Loc_Sale);
            this.Controls.Add(this.Txt_Net_total_Buy);
            this.Controls.Add(this.Txt_Net_loc_RBuy);
            this.Controls.Add(this.Txt_Buy_Loc_Exch);
            this.Controls.Add(this.Txt_Net_Loc_Buy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.flowLayoutPanel11);
            this.Controls.Add(this.flowLayoutPanel15);
            this.Controls.Add(this.Btn_Browser);
            this.Controls.Add(this.searchTextBox11);
            this.Controls.Add(this.searchTextBox9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel13);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel12);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reveal_Buy_Sale_Amount";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "464";
            this.Text = "تقرير المبيعات والمشتريات";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Reveal_Buy_Sale_Amount_FormClosed);
            this.Load += new System.EventHandler(this.Reveal_Buy_Sale_Amount_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel17.ResumeLayout(false);
            this.flowLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_buy_sale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_years)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private SearchTextBox searchTextBox9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private SearchTextBox searchTextBox11;
        private System.Windows.Forms.DataGridView Grd_buy_sale;
        private System.Windows.Forms.Button Btn_Browser;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Net_Loc_Buy;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Buy_Loc_Exch;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Net_loc_RBuy;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Net_total_Buy;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Net_total_Sale;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Net_Loc_RSale;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Sale_Loc_Exch;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Net_Loc_Sale;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DateTimePicker Txt_from_date;
        private System.Windows.Forms.DateTimePicker Txt_To_date;
        private System.Windows.Forms.ComboBox cmb_Term;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.DataGridView Grd_years;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.ComboBox cbo_year;
        private System.Windows.Forms.Label label4;
    }
}