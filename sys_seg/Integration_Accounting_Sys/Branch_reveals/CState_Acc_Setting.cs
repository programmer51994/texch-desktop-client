﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class CState_Acc_Setting : Form
    {
        BindingSource _Bs_cbo = new BindingSource();
         BindingSource _Bs_Grd_nat = new BindingSource();
     

        DataTable Acc_Tbl = new DataTable();
        //DataTable DT_If_empty = new DataTable();

       
       
      
   
        public CState_Acc_Setting()
        {
            InitializeComponent();

            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            
        }

        private void CState_Acc_Setting_Load(object sender, EventArgs e)
        {

            //Create_Tbl();
            Grd_Acc_Tree.AutoGenerateColumns = false;

            Cbo_cstate.SelectedIndex=-1;

            Grd_Acc_Tree.Columns["Column5"].HeaderText = connection.Lang_id == 1 ? "تأشير" : "Chk";
            Grd_Acc_Tree.Columns["Column4"].HeaderText = connection.Lang_id == 1 ? "الرمز" : "ID";
            Grd_Acc_Tree.Columns["Column2"].HeaderText = connection.Lang_id == 1 ? "الاسم عربي" : "Arabic Name";
            Grd_Acc_Tree.Columns["Column3"].HeaderText = connection.Lang_id == 1 ? "الاسم اجنبي" : "English Name";

            
                      _Bs_cbo.DataSource = connection.SqlExec("EXec Cstate_getData","Cstate_getData_tbl");
                     Cbo_cstate.DataSource = _Bs_cbo;
                     Cbo_cstate.DisplayMember = connection.Lang_id == 1 ? "Cstate_AName" : "Cstate_ENAME";
                     Cbo_cstate.ValueMember = "Cstate_ID";

                     Acc_Tbl = connection.SQLDS.Tables["Cstate_getData_tbl1"];
                     _Bs_Grd_nat.DataSource = Acc_Tbl;
                     Grd_Acc_Tree.DataSource = _Bs_Grd_nat;

                  

         }
        //private void Create_Tbl()
        //{

        //    string[] Column1 = { "ID" };
        //    string[] DType1 = { "System.Int16" };
        //    DT_If_empty = CustomControls.Custom_DataTable("DT_If_empty", Column1, DType1);
        //    //Grd_per.DataSource = DT_per;



        //}

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Add_Btn_Click(object sender, EventArgs e)
        {
            DataTable DT_Cstate_Setting = new DataTable();

            

        
            try
            {
                DT_Cstate_Setting = Acc_Tbl.DefaultView.ToTable(false, "Chk", "Acc_ID").Select("Chk > 0").CopyToDataTable();
                DT_Cstate_Setting = DT_Cstate_Setting.DefaultView.ToTable(false, "Acc_ID").Select().CopyToDataTable();
            }
            catch
            {

                //DT_Cstate_Setting = DT_If_empty;
            }

            if (DT_Cstate_Setting.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم يتم تحديد بيانات " : "There is no Data was selected", MyGeneral_Lib.LblCap);
                return;
            }

//            @Cstate_Acc_Id_tbl   Cstate_Acc_Id_Type  readonly,
//@Cstate_ID int,
//@user_ID  int,
//@Param_Result		varchar(150)	output

            connection.SQLCMD.Parameters.AddWithValue("@Cstate_Acc_Id_tbl",DT_Cstate_Setting);
            connection.SQLCMD.Parameters.AddWithValue("@Cstate_ID",Cbo_cstate.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@user_ID",connection.user_id);
            //connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            //connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Cstate_Setting", connection.SQLCMD);

            //if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            //{
            //    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
            //    connection.SQLCMD.Parameters.Clear();
            //    return;
            //}
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);




            string[] Str1 = { "Cstate_getData_tbl", "Cstate_getData_tbl1"};
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }

            CState_Acc_Setting_Load(null, null);
            //this.Close();
        }

        private void CState_Acc_Setting_FormClosing(object sender, FormClosingEventArgs e)
        {
            string[] Used_Tbl = { "Cstate_getData_tbl", "Cstate_getData_tbl1"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
               
            }
        }


       

     

      

 
    




   


      


     
   
      

       
 
    }
}

    
