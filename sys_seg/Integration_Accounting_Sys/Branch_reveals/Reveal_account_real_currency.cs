﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_account_real_currency: Form
    {
        #region Definshion

        DataTable DT_1 = new DataTable();
        DataTable DT_G2 = new DataTable();
        BindingSource BS_1 = new BindingSource();
        BindingSource BS_2 = new BindingSource();
        BindingSource BS_3 = new BindingSource();
        BindingSource BS_4 = new BindingSource();
        BindingSource BS_5 = new BindingSource();
        DataTable Grid2_tbl = new DataTable();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataGridView Date_Grd2 = new DataGridView();
        DataTable Date_Tbl2 = new DataTable();
        DataTable Dt = new DataTable();
        string Acc_no_current = "";
        Int16 T_id_current = 0;
        Int32 Cust_id_current = 0;
        Int16 Cur_id_current = 0;
        Int16 for_cur_id = 0;
        
        Int32 Acc_id_current = 0;
        Int32 Vo_no_current = 0;
        Int32 Oper_id_current = 0;
        string nrec_date_current;
        bool Xchange1 = false;
        bool Xchange2 = false;
        bool Xchange3 = false;
        bool Xchange4 = false;
        string Dot_Acc_no_current = "";
        //Int16 level_rep = 0;
        #endregion

        public Reveal_account_real_currency(string FromDate, string ToDate, Int32 from_nrec_date, Int32 to_nrec_date)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
       connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

            string @format = "00/00/0000";
            
            if (from_nrec_date != 0)
            {
                TxtFromDate.Text = FromDate;
            }
            else {
                TxtFromDate.Text = @format;
                }

            if (to_nrec_date != 0)
            {
                TxtToDate.Text = ToDate;
               
            }
            else {
                TxtToDate.Text = @format;
               }

            G1.AutoGenerateColumns = false;
            G2.AutoGenerateColumns = false;
            G3.AutoGenerateColumns = false;
          
            if (connection.Lang_id != 1)
            {
                 
                Column7.DataPropertyName = "Acc_EName";
                Column20.DataPropertyName = "Cur_ENAME";
                dataGridViewTextBoxColumn2.DataPropertyName = "ETerm_Name";
                Column21.DataPropertyName = "Cur_ENAME";
                Column12.DataPropertyName = "Acc_Ename";
                Column10.DataPropertyName = "Ecust_Name";             
                Column11.DataPropertyName = "cur_ename";
                Column15.DataPropertyName = "ETerm_Name";

            }
        }
        //--------------------------------------------------------------
        private void Terminal_Details_trail_balance_Load(object sender, EventArgs e)
        {

           
            Xchange1 = false ;
            BS_1.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl"];
            G1.DataSource = BS_1;
            if (G1.RowCount == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد قيود " : "There is no data to show", MyGeneral_Lib.LblCap);
                Btn_Ext_Click(null, null);
                return;
            }
            Xchange1 = true;
            G1_SelectionChanged(null, null);
            
        }
        //--------------------------------------------------------------
        private void G1_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange1)
            {
                try
                {
                    Acc_no_current = ((DataRowView)BS_1.Current).Row["acc_no"].ToString();
                    for_cur_id = Convert.ToInt16(((DataRowView)BS_1.Current).Row["for_cur_id"]);
                    Dot_Acc_no_current = ((DataRowView)BS_1.Current).Row["Dot_acc_no"].ToString();
                    //LblRec.Text = connection.Records();
                    Xchange2 = false;
                   
                    DataTable _Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable().Select("Dot_acc_no like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable(); ;
                       
                    BS_2.DataSource = _Dt;
                    G2.DataSource = BS_2;
                    Xchange2 = true;
                    G2_SelectionChanged(null, null);
                }
                catch
                {
                    G2.DataSource = new DataTable();
                    G3.DataSource = new DataTable();
                }

            }

        }
        //--------------------------------------------------------------
        private void G2_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange2)
            {

                try
                {
                    Xchange3 = false;
                  
                    T_id_current = Convert.ToInt16(((DataRowView)BS_2.Current).Row["T_id"]);
                    for_cur_id = Convert.ToInt16(((DataRowView)BS_1.Current).Row["for_cur_id"]);
                    Dot_Acc_no_current = ((DataRowView)BS_1.Current).Row["Dot_acc_no"].ToString();
                    
                    BS_3.DataSource = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and Dot_acc_no like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable();
                    G3.DataSource = BS_3;
                    Xchange3 = true;
                    LblRec.Text = connection.Records(BS_3);
                  
                }
                catch
                {
                    G3.DataSource = new DataTable();
                }

            }
        }
      
     
        //--------------------------------------------------------------
        private void Btn_Browser_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                Date();
               

                DataTable Dt = new DataTable();
                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no",connection.Lang_id==1 ?  "Acc_AName" : "Acc_EName", 
              connection.Lang_id==1 ?  "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount","OLoc_Amount","pLoc_Amount","Eloc_Amount").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { Date_Grd,G1 };
                DataTable[] Export_DT = { Date_Tbl,Dt};
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        //--------------------------------------------------------------G1
        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
          
            for_cur_id = Convert.ToInt16(((DataRowView)BS_1.Current).Row["for_cur_id"]);
            Dot_Acc_no_current = ((DataRowView)BS_1.Current).Row["Dot_acc_no"].ToString();

                DataTable Dt = new DataTable();
                DataTable Dt_1 = new DataTable();

                Dt = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no",connection.Lang_id==1 ?  "Acc_AName" : "Acc_EName",
              connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "Dot_acc_no", "for_cur_id").Select("Dot_acc_no like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable(); 
              Dt_1 = Dt.DefaultView.ToTable(false, "acc_no",connection.Lang_id==1 ?  "Acc_AName" : "Acc_EName", 
              connection.Lang_id==1 ?  "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount","OLoc_Amount","pLoc_Amount","Eloc_Amount").Select().CopyToDataTable();
             
         //---------------------------------------------------G2

              DataTable Dt_G2 = new DataTable();
              DataTable Dt_G2_1 = new DataTable();


              Dt_G2 = connection.SQLDS.Tables["Account_Balancing_new_tbl1"].DefaultView.ToTable(false, "T_id", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name",
            connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "From_Date1","To_Date1", "Dot_acc_no", "for_cur_id").Select("Dot_acc_no like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable();
              Dt_G2_1 = Dt_G2.DefaultView.ToTable(false, "T_id", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name",
            connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "From_Date1", "To_Date1").Select().CopyToDataTable();

            //--------------------------------------------------G3


              DataTable Dt_G3 = new DataTable();
              DataTable Dt_G3_1 = new DataTable();


              Dt_G3 = connection.SQLDS.Tables["Account_Balancing_new_tbl2"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", "cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name",
            connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount", "Dot_acc_no", "for_cur_id").Select("Dot_acc_no like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable();
              Dt_G3_1 = Dt_G3.DefaultView.ToTable(false, connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName", connection.Lang_id == 1 ? "ATerm_Name" : "ETerm_Name", "cust_id", connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name",
            connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount").Select().CopyToDataTable();

            
            
            DataGridView[] Export_GRD = { G1,G2,G3};
            DataTable[] Export_DT = { Dt_1, Dt_G2_1, Dt_G3_1};
              MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

    
        }
        //--------------------------------------------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Terminal_Details_trail_balance_FormClosed(object sender, FormClosedEventArgs e)
        {
             Xchange1 = false;
             Xchange2 = false;
             Xchange3 = false;
             Xchange4 = false;
        }

      

        private void Date()
        {

           
            Date_Grd = new DataGridView();

            if (Date_Grd.ColumnCount > 0)
            {
                Date_Grd.Columns.Remove("Column1");
                Date_Grd.Columns.Remove("Column2");
                Date_Tbl.Columns.Remove("From_Date1");
                Date_Tbl.Columns.Remove("To_Date1");
            }

            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date1", "To_Date1" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            string date_from = ((DataRowView)BS_2.Current).Row["From_Date1"].ToString();
            string date_To = ((DataRowView)BS_2.Current).Row["To_Date1"].ToString();
            Date_Tbl.Rows.Add(new object[] { date_from, date_To });


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (G1.Rows.Count > 0)
            {
                string[] Str = { "ExpDt" };
                foreach (string Tbl in Str)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                        connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
                }

                string frm_name = "";
                string frm_Ename = "";

                frm_name = "كشف الارصدة بالعملة الاصلية";
                frm_Ename = "Disclosure of balances in original currency";
       
                string user_name = connection.User_Name;
                string date_from = (((DataRowView)BS_2.Current).Row["From_Date1"]).ToString();
                string date_to = (((DataRowView)BS_2.Current).Row["To_Date1"]).ToString();

                Reveal_account_real_currency_Rpt2 ObjRpt = new Reveal_account_real_currency_Rpt2();
                Reveal_account_real_currency_Rpt2_eng ObjRptEng = new Reveal_account_real_currency_Rpt2_eng();
                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                Dt_Param.Rows.Add("frm_name", frm_name);
                Dt_Param.Rows.Add("frm_Ename", frm_Ename);
                Dt_Param.Rows.Add("user_name", user_name);
                Dt_Param.Rows.Add("date_from", date_from);
                Dt_Param.Rows.Add("date_to", date_to);


                DataTable ExpDt = new DataTable();
                ExpDt = connection.SQLDS.Tables["Account_Balancing_new_tbl"].DefaultView.ToTable(false, "acc_no", "Acc_AName" , "Acc_EName",
                "Cur_ANAME" , "Cur_ENAME", "OFor_Amount", "PFor_Amount", "EFor_Amount", "OLoc_Amount", "pLoc_Amount", "Eloc_Amount").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { G1 };
                DataTable[] Export_DT = { ExpDt };

                ExpDt.TableName = "ExpDt";
                connection.SQLDS.Tables.Add(ExpDt);

                RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, true, Dt_Param, Export_GRD, Export_DT, true, false, this.Text);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                //connection.SQLDS.Tables["Exp_DT"].TableName = "Account_Balancing_new_tbl";
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للطباعة" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }
    }
}
