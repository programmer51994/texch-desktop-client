﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Report;
using System.Drawing;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Buy_Sale_Amount : Form
    {
        #region MyRegion
        string SqlTxt = "";
        bool CboChange = false;
        bool GrdChange = false;
        int from_nrec_date = 0;
        int to_nrec_date = 0;

        DataTable DT = new DataTable();
        BindingSource Bs_Grd_buy_sale = new BindingSource();
        DataTable TBL = new DataTable();
        BindingSource date_BS = new BindingSource();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataTable Date_Tbl1 = new DataTable();
        string from_date;
        string To_date;
        DataGridView Loc_Cur_Grd = new DataGridView();
        DataGridView Loc_Cur_Grd1 = new DataGridView();
        DataGridView Loc_Cur_Grd2 = new DataGridView();
        DataGridView Loc_Cur_Grd3 = new DataGridView();
        DataTable Loc_Cur_Tbl = new DataTable();
        string @format = "dd/MM/yyyy";
        BindingSource binding_cmb_Term = new BindingSource();
        #endregion

        public Reveal_Buy_Sale_Amount()
        {
            InitializeComponent();
            connection.SQLBSGrd.DataSource = new BindingSource();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_buy_sale.AutoGenerateColumns = false;

            if (connection.Lang_id != 2)
            {
                //    Column1.DataPropertyName = "Chk";
                Column2.DataPropertyName = "Cur_ENAME";
                //    Column3.DataPropertyName = "Acc_Ename";
                //    Column4.DataPropertyName = "ECust_name";
            }
        }

        private void Reveal_Buy_Sale_Amount_Load(object sender, EventArgs e)
        {
            Txt_from_date.Format = DateTimePickerFormat.Custom;
            Txt_from_date.CustomFormat = @format;          
          
            //Txt_from_date.DataBindings.Clear();
            //Txt_from_date.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl2"], "From_Date");

            Txt_from_date.Text = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["From_Date"].ToString();

            Txt_To_date.Format = DateTimePickerFormat.Custom;
            Txt_To_date.CustomFormat = @format;
        //////////////////////////////////////
            connection.SqlExec("Exec Search_Account_Balance_new ", "Account_Balance_new_Tbl");
        /////////////////////////////////////
            string sql_cmb_Term = " Select 0 as T_ID, 0 as cust_id, 'جميع الافرع' as ACUST_NAME, 'All TERMINALS' as ECUST_NAME "
                                + " From CUSTOMERS "
                                + " union"
                                + " Select T_ID, A.cust_id, ACUST_NAME, ECUST_NAME "
                                + " From CUSTOMERS A, TERMINALS B "
                                + " where A.CUST_ID = B.CUST_ID "
                                + " AND  B.T_ID in(select t_id from User_Terminals where User_State not in(2,3) and t_user_id= " + connection.user_id + ")";

            connection.SqlExec(sql_cmb_Term, "sql_cmb_Term_tab");
            binding_cmb_Term.DataSource = connection.SQLDS.Tables["sql_cmb_Term_tab"];
            cmb_Term.DataSource = binding_cmb_Term;
            cmb_Term.ValueMember = "T_ID";
            cmb_Term.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
        }


        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            // DataTable Reveal_Buy_sale = new DataTable();
            //Reveal_Buy_sale = connection.SQLDS.Tables["Tbl_buy_sale"];

            if (Grd_buy_sale.RowCount == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود لطباعتهاالرجاء إختيار الفترة" : "There are no iformation to print please enter the periode date ", MyGeneral_Lib.LblCap);
                Txt_from_date.Focus();
                return;
            }

            Reveal_Buy_Sale_Amount_Rep ObjRpt = new Reveal_Buy_Sale_Amount_Rep();
            Reveal_Buy_Sale_Amount_ERep ObjRpteng = new Reveal_Buy_Sale_Amount_ERep();
            //connection.SqlExec("EXec Account_tree_TreeView", "Tree_Tbl_Rep");
            //   DataTable DT = new DataTable();

            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("User_rep", connection.User_Name);
            Dt_Param.Rows.Add("loc_cur_name", Txt_Loc_Cur.Text);
            Dt_Param.Rows.Add("from_date", Txt_from_date.Text);
            Dt_Param.Rows.Add("To_date", Txt_To_date.Text);
            DataTable Exp_Dt = connection.SQLDS.Tables["Tbl_buy_sale"].DefaultView.ToTable(false, "For_cur_id", "Cur_ANAME","Cur_ENAME" ,"NetBuy_For_Aud",
                           "NetBuy_For_Bank", "NetBuy_For_Eco", "NetBuy_For_Exch",
                           "Total_Net_Buy", "Net_Sale_For_Aud", "Net_Sale_For_Bank", "Net_Sale_For_Eco",
                            "Net_Sale_For_Exch", "Total_Net_Sale").Select().CopyToDataTable();
            Date();
            Loc_cur();
            Loc_cur1();
            Loc_cur2();
            Loc_cur3();
            DataTable[] _Exp_Dt = { Date_Tbl, Exp_Dt, Loc_Cur_Tbl, Loc_Cur_Tbl, Loc_Cur_Tbl, Loc_Cur_Tbl };
            DataGridView[] GRd = { Date_Grd, Grd_buy_sale, Loc_Cur_Grd, Loc_Cur_Grd1, Loc_Cur_Grd2, Loc_Cur_Grd3 };
            RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRpteng, true, true, true, Dt_Param, GRd, _Exp_Dt, true, true, this.Text);
            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;

        }
        private void Date()
        {


            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من " : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            Date_Tbl.Rows.Add(new object[] { Txt_from_date.Text, Txt_To_date.Text });
        }
        private void Loc_cur()
        {
            Loc_Cur_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "إجمالي المشتريات مقابل " + label16.Text : "");
            Loc_Cur_Grd.Columns.Add("Column2", Txt_Net_Loc_Buy.Text);
            Loc_Cur_Grd.Columns.Add("Column3", " ");
            Loc_Cur_Grd.Columns.Add("Column4", connection.Lang_id == 1 ? "اجمالي المبيعات مقابل " + label16.Text : "");
            Loc_Cur_Grd.Columns.Add("Column5", Txt_Net_Loc_Sale.Text);
        }
        private void Loc_cur1()
        {
            Loc_Cur_Grd1.Columns.Add("Column1", connection.Lang_id == 1 ? "إجمالي المشتريات مقابل عملات اجنبيةاخرى :" : "");
            Loc_Cur_Grd1.Columns.Add("Column2", Txt_Buy_Loc_Exch.Text);
            Loc_Cur_Grd1.Columns.Add("Column3", " ");
            Loc_Cur_Grd1.Columns.Add("Column4", connection.Lang_id == 1 ? "إجمالي المبيعات مقابل عملات اجنبيةاخرى" : "");
            Loc_Cur_Grd1.Columns.Add("Column5", Txt_Sale_Loc_Exch.Text);
        }

        private void Loc_cur2()
        {
            Loc_Cur_Grd2.Columns.Add("Column1", connection.Lang_id == 1 ? "اجمالي ردالمشتريات مقابل " + label16.Text : "");
            Loc_Cur_Grd2.Columns.Add("Column2", Txt_Net_loc_RBuy.Text);
            Loc_Cur_Grd2.Columns.Add("Column3", " ");
            Loc_Cur_Grd2.Columns.Add("Column4", connection.Lang_id == 1 ? "اجمالي ردالمبيعات مقابل " + label16.Text : "");
            Loc_Cur_Grd2.Columns.Add("Column5", Txt_Net_Loc_RSale.Text);
        }


        private void Loc_cur3()
        {
            Loc_Cur_Grd3.Columns.Add("Column1", connection.Lang_id == 1 ? "اجمالي صافي المشتريات مقابل " + label16.Text : "");
            Loc_Cur_Grd3.Columns.Add("Column2", Txt_Net_total_Buy.Text);
            Loc_Cur_Grd3.Columns.Add("Column3", " ");
            Loc_Cur_Grd3.Columns.Add("Column4", connection.Lang_id == 1 ? " اجمالي صافي المبيعات مقابل" + label16.Text : "");
            Loc_Cur_Grd3.Columns.Add("Column5", Txt_Net_total_Sale.Text);

            //---------------------------------------------------------important
            //    Loc_Cur_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "إجمالي المشتريات مقابل " + label16.Text : "");
            //Loc_Cur_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "إجمالي المشتريات مقابل عملات اجنبيةاخرى :" : "");
            //Loc_Cur_Grd.Columns.Add("Column3", connection.Lang_id == 1 ? "اجمالي ردالمشتريات مقابل " + label16.Text : "");
            //Loc_Cur_Grd.Columns.Add("Column4", connection.Lang_id == 1 ? "اجمالي صافي المشتريات مقابل " + label16.Text : "");

            //Loc_Cur_Grd.Columns.Add("Column5", connection.Lang_id == 1 ? "اجمالي المبيعات مقابل " + label16.Text : "");
            //Loc_Cur_Grd.Columns.Add("Column6", connection.Lang_id == 1 ? "إجمالي المبيعات مقابل عملات اجنبيةاخرى"  : "");
            //Loc_Cur_Grd.Columns.Add("Column7", connection.Lang_id == 1 ? "اجمالي ردالمبيعات مقابل " + label16.Text : "");
            //Loc_Cur_Grd.Columns.Add("Column8", connection.Lang_id == 1 ? " اجمالي صافي المبيعات مقابل" + label16.Text : "");


            //string[] Column = { "Net_Loc_Buy", "Buy_Loc_Exch", "Net_loc_RBuy", "Net_total_Buy",
            //                  "Net_Loc_Sale","Sale_Loc_Exch","Net_Loc_RSale","Net_total_Sale"};
            //string[] DType = { "System.Decimal" , "System.Decimal", "System.Decimal", "System.Decimal", 
            //                     "System.Decimal", "System.Decimal" ,"System.Decimal","System.Decimal"};
            //Loc_Cur_Tbl = CustomControls.Custom_DataTable("Loc_Cur_Tbl", Column, DType);

            //Loc_Cur_Tbl.Rows.Add(new object[] { Txt_Net_Loc_Buy.Text, Txt_Buy_Loc_Exch.Text, Txt_Net_loc_RBuy.Text, Txt_Net_total_Buy.Text
            //                           ,Txt_Net_Loc_Sale.Text,Txt_Sale_Loc_Exch.Text,Txt_Net_Loc_RSale.Text,Txt_Net_total_Sale.Text});
        }
        private void Grd_limit_SelectionChanged(object sender, EventArgs e)
        {
            // LblRec.Text = connection.Records(BS_Grd);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((Txt_from_date.Text == "0000/00/00") || (Txt_from_date.Text == "    /  /  ") || (Txt_from_date.Text == "") || (Txt_from_date.Text == "00/00/0000") || (Txt_from_date.Checked == false))
            {

                SqlTxt = "Select  Min(nrec_date)  as min_date from VOUCHER";
                from_date = connection.SqlExec(SqlTxt, "date_tbl").Rows[0]["min_date"].ToString();
                int From = Convert.ToInt32(from_date);
                int d = From % 100;
                int m = (From / 100) % 100;
                int y = From / 10000;

                var result = new DateTime(y, m, d);
                // DateTime datee = DateTime.ParseExact(this.from_date, "dd/MM/yyyy", null);
                // DateTime Fdt = DateTime.Parse(from_date.ToString());
                Txt_from_date.Text = result.ToShortDateString();
            }
            else
            {
                from_date = Txt_from_date.Text;
            }
            //---------------------------------------------
            if ((Txt_To_date.Text == "0000/00/00") || (Txt_To_date.Text == "    /  /  ") || (Txt_To_date.Text == "") || (Txt_To_date.Text == "00/00/0000") || (Txt_To_date.Checked == false))
            {

                SqlTxt = "Select  max(nrec_date) as max_date from VOUCHER";
                To_date = connection.SqlExec(SqlTxt, "date_tbl").Rows[0]["max_date"].ToString();

                int To = Convert.ToInt32(To_date);
                int d = To % 100;
                int m = (To / 100) % 100;
                int y = To / 10000;

                var result = new DateTime(y, m, d);
                Txt_To_date.Text = result.ToShortDateString();
            }
            else
            {
                To_date = Txt_To_date.Text;
            }

            //string From_Date = MyGeneral_Lib.DateChecking(Txt_from_date.Text);

            DateTime date = Txt_from_date.Value.Date;
            from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;



            // string To_Date = MyGeneral_Lib.DateChecking(Txt_To_date.Text);
            DateTime date1 = Txt_To_date.Value.Date;
            to_nrec_date = date1.Day + date1.Month * 100 + date1.Year * 10000;

            string[] Str = { "Tbl_buy_sale", "Tbl_buy_sale1" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(connection.SQLDS.Tables[Tbl]);
            }


            try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = cbo_year.SelectedIndex == 0 ? "Reveal_Buy_Sale" : "Reveal_Buy_Sale_PHst";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                if (cbo_year.SelectedIndex == 0)
                {
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@T_id", Convert.ToInt16(cmb_Term.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                }
                else //old
                {
                    connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@T_id", Convert.ToInt16(cmb_Term.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                }

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Tbl_buy_sale");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Tbl_buy_sale1");
                obj.Close();
                connection.SQLCS.Close();

                Bs_Grd_buy_sale.DataSource = connection.SQLDS.Tables["Tbl_buy_sale"];
                if (connection.SQLDS.Tables["Tbl_buy_sale"].Rows.Count > 0)
                {
                    Grd_buy_sale.DataSource = Bs_Grd_buy_sale;

                    if (connection.SQLDS.Tables["Tbl_buy_sale1"].Rows.Count > 0)
                    {
                        Txt_Net_Loc_Buy.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_Loc_Buy"].ToString();
                        Txt_Buy_Loc_Exch.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Buy_Loc_Exch"].ToString();
                        Txt_Net_loc_RBuy.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_loc_RBuy"].ToString();
                        Txt_Net_total_Buy.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_total_Buy"].ToString();
                        Txt_Net_Loc_Sale.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_Loc_Sale"].ToString();
                        Txt_Sale_Loc_Exch.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Sale_Loc_Exch"].ToString();
                        Txt_Net_Loc_RSale.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_Loc_RSale"].ToString();
                        Txt_Net_total_Sale.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_total_Sale"].ToString();


                        label16.Text = Txt_Loc_Cur.Text + ":";
                        label17.Text = Txt_Loc_Cur.Text + ":";
                        label18.Text = Txt_Loc_Cur.Text + ":";
                        label19.Text = Txt_Loc_Cur.Text + ":";
                        label6.Text = Txt_Loc_Cur.Text + ":";
                        label7.Text = Txt_Loc_Cur.Text + ":";
                    }

                    Grd_buy_sale.Columns["Column3"].DefaultCellStyle.BackColor = Color.Gainsboro;
                    Grd_buy_sale.Columns["Column4"].DefaultCellStyle.BackColor = Color.Gainsboro;
                    Grd_buy_sale.Columns["Column5"].DefaultCellStyle.BackColor = Color.Gainsboro;
                    Grd_buy_sale.Columns["Column6"].DefaultCellStyle.BackColor = Color.Gainsboro;
                    Grd_buy_sale.Columns["Column7"].DefaultCellStyle.BackColor = Color.Gainsboro;
                    Grd_buy_sale.Columns["Column8"].DefaultCellStyle.BackColor = Color.White;
                    Grd_buy_sale.Columns["Column9"].DefaultCellStyle.BackColor = Color.White;
                    Grd_buy_sale.Columns["Column10"].DefaultCellStyle.BackColor = Color.White;
                    Grd_buy_sale.Columns["Column11"].DefaultCellStyle.BackColor = Color.White;
                    Grd_buy_sale.Columns["Column12"].DefaultCellStyle.BackColor = Color.White;
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط" : "No Data", MyGeneral_Lib.LblCap);
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }




            //Bs_Grd_buy_sale.DataSource = connection.SqlExec(SqlTxt, "Tbl_buy_sale");
            //if (connection.SQLDS.Tables["Tbl_buy_sale"].Rows.Count > 0)
            //{
            //    Grd_buy_sale.DataSource = Bs_Grd_buy_sale;

            //    if (connection.SQLDS.Tables["Tbl_buy_sale1"].Rows.Count > 0)
            //    {
            //        Txt_Net_Loc_Buy.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_Loc_Buy"].ToString();
            //        Txt_Buy_Loc_Exch.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Buy_Loc_Exch"].ToString();
            //        Txt_Net_loc_RBuy.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_loc_RBuy"].ToString();
            //        Txt_Net_total_Buy.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_total_Buy"].ToString();
            //        Txt_Net_Loc_Sale.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_Loc_Sale"].ToString();
            //        Txt_Sale_Loc_Exch.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Sale_Loc_Exch"].ToString();
            //        Txt_Net_Loc_RSale.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_Loc_RSale"].ToString();
            //        Txt_Net_total_Sale.Text = connection.SQLDS.Tables["Tbl_buy_sale1"].Rows[0]["Net_total_Sale"].ToString();


            //        label16.Text = Txt_Loc_Cur.Text + ":";
            //        label17.Text = Txt_Loc_Cur.Text + ":";
            //        label18.Text = Txt_Loc_Cur.Text + ":";
            //        label19.Text = Txt_Loc_Cur.Text + ":";
            //        label6.Text = Txt_Loc_Cur.Text + ":";
            //        label7.Text = Txt_Loc_Cur.Text + ":";
            //    }

            //    Grd_buy_sale.Columns["Column3"].DefaultCellStyle.BackColor = Color.Gainsboro;
            //    Grd_buy_sale.Columns["Column4"].DefaultCellStyle.BackColor = Color.Gainsboro;
            //    Grd_buy_sale.Columns["Column5"].DefaultCellStyle.BackColor = Color.Gainsboro;
            //    Grd_buy_sale.Columns["Column6"].DefaultCellStyle.BackColor = Color.Gainsboro;
            //    Grd_buy_sale.Columns["Column7"].DefaultCellStyle.BackColor = Color.Gainsboro;
            //    Grd_buy_sale.Columns["Column8"].DefaultCellStyle.BackColor = Color.White;
            //    Grd_buy_sale.Columns["Column9"].DefaultCellStyle.BackColor = Color.White;
            //    Grd_buy_sale.Columns["Column10"].DefaultCellStyle.BackColor = Color.White;
            //    Grd_buy_sale.Columns["Column11"].DefaultCellStyle.BackColor = Color.White;
            //    Grd_buy_sale.Columns["Column12"].DefaultCellStyle.BackColor = Color.White;
            //}
            //else
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط" : "No Data", MyGeneral_Lib.LblCap);
            //    return;
            //}
        }


        public void Row_Color()
        {
            //    for  (int i =0 ; i < Grd_buy_sale.Rows.Count;i++)
            //    {
            //        for (int j = 2; j < Grd_buy_sale.Rows[i].Cells[j].RowIndex; j++)
            //        {
            //            // int val = Convert.ToInt16(Grd_buy_sale.Rows[i].Cells[2].Value);
            //           // if (val < 5)
            //                //{
            //                Grd_buy_sale.Rows[i].Cells[j].DefaultCellStyle.BackColor = Color.Red;
            //        }


            // }
        }

        private void Reveal_Buy_Sale_Amount_FormClosed(object sender, FormClosedEventArgs e)
        {
            CboChange = false;
           GrdChange = false;
        }

        private void cmb_Term_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grd_buy_sale.DataSource = new BindingSource();
            Txt_Net_Loc_Buy.Text = "0.000";
            Txt_Buy_Loc_Exch.Text = "0.000";
            Txt_Net_loc_RBuy.Text = "0.000";
            Txt_Net_total_Buy.Text = "0.000";
            Txt_Net_Loc_Sale.Text = "0.000";
            Txt_Sale_Loc_Exch.Text = "0.000";
            Txt_Net_Loc_RSale.Text = "0.000";
            Txt_Net_total_Sale.Text = "0.000";
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                try
                {
                    Grd_years.Enabled = false;
                    Grd_years.DataSource = new BindingSource();
                    //TxtFromDate.DataBindings.Clear();
                    //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl2"], "From_Nrec_Date");

                    Txt_from_date.Text = connection.SQLDS.Tables["HPage_Tbl4"].Rows[0]["From_Nrec_Date"].ToString();
                    Txt_To_date.Checked = false;
                    //checkBox1.Visible = false;
                }
                catch { }
            }
            if (cbo_year.SelectedIndex == 1)// old 
            {
                try
                {
                    Grd_years.Enabled = true;
                    //_bsyears.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl3"];
                    //Grd_years.DataSource = _bsyears;

                    if (connection.SQLDS.Tables["Account_Balance_new_Tbl3"].Rows.Count > 0)
                    {
                        Grd_years.DataSource = connection.SQLDS.Tables["Account_Balance_new_Tbl3"].Select("t_id =" + connection.T_ID).CopyToDataTable();

                        Txt_from_date.Text = connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["min_nrec_date"].ToString();
                        Txt_To_date.Text = connection.SQLDS.Tables["Account_Balance_new_Tbl4"].Rows[0]["max_nrec_date"].ToString();
                    }

                    //Change_grd = true;
                    //Grd_years_SelectionChanged(null, null);
                    // checkBox1.Visible = true;
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                        cbo_year.SelectedIndex = 0;
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }


            }
        }


    }




    //    private int GetColumnIndexByName(DataGridView grid, string Column3)
    //{

    //    int i = 1;
    //    int n = Grd_buy_sale.RowCount;
    //    int m = Grd_buy_sale.ColumnCount;
    //   foreach (DataControlField col in grid.Columns)
    //    {
    //        while (i < n)
    //            {
    //               this.Grd_buy_sale.DefaultCellStyle.BackColor = Color.Pink;
    //     }

    //   }

    //   return -1;
    //}





}