﻿namespace Integration_Accounting_Sys
{
    partial class Search_Acc_Account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TxtCur_Name = new System.Windows.Forms.TextBox();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCust_Name = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.Chk_Cur = new System.Windows.Forms.CheckBox();
            this.Chk_Cust = new System.Windows.Forms.CheckBox();
            this.Grd_Cur_Id = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cur = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust_Id = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc_Id = new System.Windows.Forms.DataGridView();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk_Acc = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.TxtAcc_Name = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.Cbo_Level = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CHK_OP_ACC = new System.Windows.Forms.CheckBox();
            this.TxtToDate = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtFromDate = new Integration_Accounting_Sys.MyDateTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtCur_Name
            // 
            this.TxtCur_Name.BackColor = System.Drawing.Color.White;
            this.TxtCur_Name.Enabled = false;
            this.TxtCur_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCur_Name.Location = new System.Drawing.Point(97, 248);
            this.TxtCur_Name.MaxLength = 49;
            this.TxtCur_Name.Name = "TxtCur_Name";
            this.TxtCur_Name.Size = new System.Drawing.Size(222, 22);
            this.TxtCur_Name.TabIndex = 15;
            this.TxtCur_Name.TextChanged += new System.EventHandler(this.TxtCur_Name_TextChanged);
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.AddBtn.Location = new System.Drawing.Point(246, 572);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(89, 27);
            this.AddBtn.TabIndex = 31;
            this.AddBtn.Text = "مـوافـق";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ExtBtn.Location = new System.Drawing.Point(334, 572);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 27);
            this.ExtBtn.TabIndex = 32;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 39);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(658, 1);
            this.flowLayoutPanel1.TabIndex = 143;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(5, 39);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 530);
            this.flowLayoutPanel5.TabIndex = 143;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(663, 39);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 530);
            this.flowLayoutPanel7.TabIndex = 144;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-1, 31);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(769, 1);
            this.flowLayoutPanel2.TabIndex = 525;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(331, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(160, 22);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(555, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(97, 22);
            this.TxtIn_Rec_Date.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(499, 8);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 521;
            this.label2.Text = "التاريـخ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(249, 8);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 520;
            this.label1.Text = "المستخــدم:";
            // 
            // TxtCust_Name
            // 
            this.TxtCust_Name.BackColor = System.Drawing.Color.White;
            this.TxtCust_Name.Enabled = false;
            this.TxtCust_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtCust_Name.Location = new System.Drawing.Point(95, 402);
            this.TxtCust_Name.MaxLength = 149;
            this.TxtCust_Name.Name = "TxtCust_Name";
            this.TxtCust_Name.Size = new System.Drawing.Size(224, 22);
            this.TxtCust_Name.TabIndex = 23;
            this.TxtCust_Name.TextChanged += new System.EventHandler(this.TxtCust_Name_TextChanged);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button5.Location = new System.Drawing.Point(332, 285);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 22);
            this.button5.TabIndex = 17;
            this.button5.Text = ">";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button6.Location = new System.Drawing.Point(332, 307);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(83, 22);
            this.button6.TabIndex = 18;
            this.button6.Text = ">>";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button7.Location = new System.Drawing.Point(332, 329);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 22);
            this.button7.TabIndex = 19;
            this.button7.Text = "<";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button8.Location = new System.Drawing.Point(332, 351);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(83, 22);
            this.button8.TabIndex = 20;
            this.button8.Text = "<<";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button12
            // 
            this.button12.Enabled = false;
            this.button12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button12.Location = new System.Drawing.Point(331, 500);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(83, 22);
            this.button12.TabIndex = 28;
            this.button12.Text = "<<";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button4_Click);
            // 
            // button11
            // 
            this.button11.Enabled = false;
            this.button11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button11.Location = new System.Drawing.Point(331, 479);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(83, 22);
            this.button11.TabIndex = 27;
            this.button11.Text = "<";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button3_Click);
            // 
            // button10
            // 
            this.button10.Enabled = false;
            this.button10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button10.Location = new System.Drawing.Point(331, 457);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(83, 22);
            this.button10.TabIndex = 26;
            this.button10.Text = ">>";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button2_Click);
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button9.Location = new System.Drawing.Point(331, 435);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(83, 22);
            this.button9.TabIndex = 25;
            this.button9.Text = ">";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button1_Click);
            // 
            // Chk_Cur
            // 
            this.Chk_Cur.AutoSize = true;
            this.Chk_Cur.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Cur.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Cur.Location = new System.Drawing.Point(11, 250);
            this.Chk_Cur.Name = "Chk_Cur";
            this.Chk_Cur.Size = new System.Drawing.Size(84, 18);
            this.Chk_Cur.TabIndex = 14;
            this.Chk_Cur.Text = "العملــــــة:";
            this.Chk_Cur.UseVisualStyleBackColor = true;
            this.Chk_Cur.CheckedChanged += new System.EventHandler(this.Chk_Cur_CheckedChanged);
            // 
            // Chk_Cust
            // 
            this.Chk_Cust.AutoSize = true;
            this.Chk_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Cust.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Cust.Location = new System.Drawing.Point(11, 404);
            this.Chk_Cust.Name = "Chk_Cust";
            this.Chk_Cust.Size = new System.Drawing.Size(83, 18);
            this.Chk_Cust.TabIndex = 22;
            this.Chk_Cust.Text = " الثـــانوي:";
            this.Chk_Cust.UseVisualStyleBackColor = true;
            this.Chk_Cust.CheckedChanged += new System.EventHandler(this.Chk_Cust_CheckedChanged);
            // 
            // Grd_Cur_Id
            // 
            this.Grd_Cur_Id.AllowUserToAddRows = false;
            this.Grd_Cur_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Cur_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cur_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cur_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cur_Id.ColumnHeadersHeight = 45;
            this.Grd_Cur_Id.ColumnHeadersVisible = false;
            this.Grd_Cur_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cur_Id.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cur_Id.Location = new System.Drawing.Point(97, 271);
            this.Grd_Cur_Id.Name = "Grd_Cur_Id";
            this.Grd_Cur_Id.ReadOnly = true;
            this.Grd_Cur_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cur_Id.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cur_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cur_Id.Size = new System.Drawing.Size(222, 122);
            this.Grd_Cur_Id.TabIndex = 16;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Cur_id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "رمز العملة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 70;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Cur_Aname";
            this.Column2.HeaderText = "اسم العملة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Grd_Cur
            // 
            this.Grd_Cur.AllowUserToAddRows = false;
            this.Grd_Cur.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cur.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cur.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Cur.ColumnHeadersHeight = 45;
            this.Grd_Cur.ColumnHeadersVisible = false;
            this.Grd_Cur.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cur.DefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Cur.Location = new System.Drawing.Point(432, 271);
            this.Grd_Cur.Name = "Grd_Cur";
            this.Grd_Cur.ReadOnly = true;
            this.Grd_Cur.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cur.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Cur.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cur.Size = new System.Drawing.Size(222, 122);
            this.Grd_Cur.TabIndex = 21;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Cur_id";
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "رمز العملة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 60;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Cur_Aname";
            this.Column5.HeaderText = "اسم العملة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 160;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Cur_Ename";
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 160;
            // 
            // Grd_Cust_Id
            // 
            this.Grd_Cust_Id.AllowUserToAddRows = false;
            this.Grd_Cust_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Cust_Id.ColumnHeadersHeight = 45;
            this.Grd_Cust_Id.ColumnHeadersVisible = false;
            this.Grd_Cust_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust_Id.DefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Cust_Id.Location = new System.Drawing.Point(96, 425);
            this.Grd_Cust_Id.Name = "Grd_Cust_Id";
            this.Grd_Cust_Id.ReadOnly = true;
            this.Grd_Cust_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust_Id.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Cust_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust_Id.Size = new System.Drawing.Size(222, 111);
            this.Grd_Cust_Id.TabIndex = 24;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Cust_id";
            this.Column7.Frozen = true;
            this.Column7.HeaderText = "رمز الثانوي";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 60;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Acust_name";
            this.Column8.HeaderText = "اسم الثانوي";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 160;
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            this.Grd_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_Cust.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Cust.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Cust.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Cust.ColumnHeadersHeight = 45;
            this.Grd_Cust.ColumnHeadersVisible = false;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.Column11});
            this.Grd_Cust.Location = new System.Drawing.Point(431, 425);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.ReadOnly = true;
            this.Grd_Cust.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Cust.RowHeadersVisible = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Cust.Size = new System.Drawing.Size(222, 111);
            this.Grd_Cust.TabIndex = 29;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Cust_id";
            this.Column10.Frozen = true;
            this.Column10.HeaderText = "رمز الثانوي";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 60;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Acust_name";
            this.Column11.HeaderText = "اسم الثانوي";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 160;
            // 
            // Grd_Acc
            // 
            this.Grd_Acc.AllowUserToAddRows = false;
            this.Grd_Acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_Acc.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Acc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_Acc.ColumnHeadersHeight = 45;
            this.Grd_Acc.ColumnHeadersVisible = false;
            this.Grd_Acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column16,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Acc.DefaultCellStyle = dataGridViewCellStyle18;
            this.Grd_Acc.Location = new System.Drawing.Point(435, 131);
            this.Grd_Acc.Name = "Grd_Acc";
            this.Grd_Acc.ReadOnly = true;
            this.Grd_Acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Acc.RowHeadersVisible = false;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this.Grd_Acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Acc.Size = new System.Drawing.Size(222, 110);
            this.Grd_Acc.TabIndex = 13;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز العملة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Acc_Aname";
            this.Column16.HeaderText = "اسم العملة";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 160;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Cur_Ename";
            this.dataGridViewTextBoxColumn3.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 160;
            // 
            // Grd_Acc_Id
            // 
            this.Grd_Acc_Id.AllowUserToAddRows = false;
            this.Grd_Acc_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle20;
            this.Grd_Acc_Id.BackgroundColor = System.Drawing.Color.White;
            this.Grd_Acc_Id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Acc_Id.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.Grd_Acc_Id.ColumnHeadersHeight = 45;
            this.Grd_Acc_Id.ColumnHeadersVisible = false;
            this.Grd_Acc_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column17});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Acc_Id.DefaultCellStyle = dataGridViewCellStyle22;
            this.Grd_Acc_Id.Location = new System.Drawing.Point(99, 131);
            this.Grd_Acc_Id.Name = "Grd_Acc_Id";
            this.Grd_Acc_Id.ReadOnly = true;
            this.Grd_Acc_Id.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grd_Acc_Id.RowHeadersVisible = false;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.Grd_Acc_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Id.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Acc_Id.Size = new System.Drawing.Size(222, 110);
            this.Grd_Acc_Id.TabIndex = 8;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Acc_Id";
            this.Column15.Frozen = true;
            this.Column15.HeaderText = "رمز العملة";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 70;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Acc_Aname";
            this.Column17.HeaderText = "اسم العملة";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 150;
            // 
            // Chk_Acc
            // 
            this.Chk_Acc.AutoSize = true;
            this.Chk_Acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Chk_Acc.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Acc.Location = new System.Drawing.Point(11, 110);
            this.Chk_Acc.Name = "Chk_Acc";
            this.Chk_Acc.Size = new System.Drawing.Size(85, 18);
            this.Chk_Acc.TabIndex = 6;
            this.Chk_Acc.Text = "الحســـاب:";
            this.Chk_Acc.UseVisualStyleBackColor = true;
            this.Chk_Acc.CheckedChanged += new System.EventHandler(this.chk_Acc_CheckedChanged);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button4.Location = new System.Drawing.Point(334, 211);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 22);
            this.button4.TabIndex = 12;
            this.button4.Text = "<<";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button12_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button3.Location = new System.Drawing.Point(334, 189);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 22);
            this.button3.TabIndex = 11;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button11_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(334, 167);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 22);
            this.button2.TabIndex = 10;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button10_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(334, 145);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 22);
            this.button1.TabIndex = 9;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button9_Click);
            // 
            // TxtAcc_Name
            // 
            this.TxtAcc_Name.BackColor = System.Drawing.Color.White;
            this.TxtAcc_Name.Enabled = false;
            this.TxtAcc_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtAcc_Name.Location = new System.Drawing.Point(99, 108);
            this.TxtAcc_Name.MaxLength = 199;
            this.TxtAcc_Name.Name = "TxtAcc_Name";
            this.TxtAcc_Name.Size = new System.Drawing.Size(222, 22);
            this.TxtAcc_Name.TabIndex = 7;
            this.TxtAcc_Name.TextChanged += new System.EventHandler(this.TxtAcc_Name_TextChanged);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(6, 568);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(658, 1);
            this.flowLayoutPanel3.TabIndex = 144;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(101, 53);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 567;
            this.label3.Text = "التــاريــخ من :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(324, 53);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 566;
            this.label5.Text = "إلــــى :";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(12, 4);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(231, 23);
            this.TxtTerm_Name.TabIndex = 0;
            // 
            // Cbo_Level
            // 
            this.Cbo_Level.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Level.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Level.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Level.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Level.FormattingEnabled = true;
            this.Cbo_Level.Location = new System.Drawing.Point(99, 77);
            this.Cbo_Level.Name = "Cbo_Level";
            this.Cbo_Level.Size = new System.Drawing.Size(223, 24);
            this.Cbo_Level.TabIndex = 5;
            this.Cbo_Level.SelectedIndexChanged += new System.EventHandler(this.CboCust_Type_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(31, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 572;
            this.label4.Text = "المستوى:";
            // 
            // CHK_OP_ACC
            // 
            this.CHK_OP_ACC.AutoSize = true;
            this.CHK_OP_ACC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHK_OP_ACC.ForeColor = System.Drawing.Color.Navy;
            this.CHK_OP_ACC.Location = new System.Drawing.Point(11, 542);
            this.CHK_OP_ACC.Name = "CHK_OP_ACC";
            this.CHK_OP_ACC.Size = new System.Drawing.Size(278, 18);
            this.CHK_OP_ACC.TabIndex = 30;
            this.CHK_OP_ACC.Text = "عكس الحساب للأرصدة ذات الطبيعة الشاذة.";
            this.CHK_OP_ACC.UseVisualStyleBackColor = true;
            // 
            // TxtToDate
            // 
            this.TxtToDate.DateSeperator = '/';
            this.TxtToDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.TxtToDate.Location = new System.Drawing.Point(372, 49);
            this.TxtToDate.Mask = "0000/00/00";
            this.TxtToDate.Name = "TxtToDate";
            this.TxtToDate.PromptChar = ' ';
            this.TxtToDate.Size = new System.Drawing.Size(132, 23);
            this.TxtToDate.TabIndex = 4;
            this.TxtToDate.Text = "00000000";
            this.TxtToDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtFromDate
            // 
            this.TxtFromDate.DateSeperator = '/';
            this.TxtFromDate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.TxtFromDate.Location = new System.Drawing.Point(186, 49);
            this.TxtFromDate.Mask = "0000/00/00";
            this.TxtFromDate.Name = "TxtFromDate";
            this.TxtFromDate.PromptChar = ' ';
            this.TxtFromDate.Size = new System.Drawing.Size(132, 23);
            this.TxtFromDate.TabIndex = 3;
            this.TxtFromDate.Text = "00000000";
            this.TxtFromDate.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Search_Acc_Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 601);
            this.Controls.Add(this.CHK_OP_ACC);
            this.Controls.Add(this.TxtToDate);
            this.Controls.Add(this.TxtFromDate);
            this.Controls.Add(this.Cbo_Level);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.Grd_Acc);
            this.Controls.Add(this.Grd_Acc_Id);
            this.Controls.Add(this.Chk_Acc);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TxtAcc_Name);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.Grd_Cust_Id);
            this.Controls.Add(this.Grd_Cur);
            this.Controls.Add(this.Grd_Cur_Id);
            this.Controls.Add(this.Chk_Cust);
            this.Controls.Add(this.Chk_Cur);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.TxtCust_Name);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.TxtCur_Name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Search_Acc_Account";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "430";
            this.Text = "Search_Acc_Account";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Search_Acc_Account_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Search_New_FormClosed);
            this.Load += new System.EventHandler(this.Search_New_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtCur_Name;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtCust_Name;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox Chk_Cur;
        private System.Windows.Forms.CheckBox Chk_Cust;
        private System.Windows.Forms.DataGridView Grd_Cur_Id;
        private System.Windows.Forms.DataGridView Grd_Cur;
        private System.Windows.Forms.DataGridView Grd_Cust_Id;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridView Grd_Acc;
        private System.Windows.Forms.DataGridView Grd_Acc_Id;
        private System.Windows.Forms.CheckBox Chk_Acc;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TxtAcc_Name;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.ComboBox Cbo_Level;
        private System.Windows.Forms.Label label4;
        private MyDateTextBox TxtFromDate;
        private MyDateTextBox TxtToDate;
        private System.Windows.Forms.CheckBox CHK_OP_ACC;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
    }
}