﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Acc_Main : Form
    {
        #region Defintion
        bool CustChange = false;
        bool CurChange = false;
        bool AccChange = false;
        BindingSource Cust_Bs = new BindingSource();
        BindingSource Cur_Bs = new BindingSource();
        BindingSource Acc_Bs = new BindingSource();
        int Cust_Id = 0;
        int Cur_Id = 0;
        int Acc_Id = 0;
        DataTable Cur_DT = new DataTable();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataTable Cust_DT = new DataTable();
        DataTable Dt = new DataTable();
        #endregion
        //---------------------------
        public Reveal_Acc_Main(string FromDate, string ToDate)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            TxtFromDate.Text = FromDate;
            TxtToDate.Text = ToDate;

            GrdAcc_Id.AutoGenerateColumns = false;
            Grd_CurBalance.AutoGenerateColumns = false;
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Vo_Details.AutoGenerateColumns = false;
            #region Arabic/English
            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "Acc_Ename";
                Column15.DataPropertyName = "ECust_Name";
                Column7.DataPropertyName = "Cur_Ename";
                Column25.DataPropertyName = "Acc_Ename";
                dataGridViewTextBoxColumn1.DataPropertyName = "EOper_Name";
            }
            #endregion
        }
        //---------------------------
        private void Reveal_Acc_Main2_Load(object sender, EventArgs e)
        {
            if (TxtFromDate.Text == "0000/00/00")
            {
                TxtFromDate.Text = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl5"].Rows[0]["Min"].ToString();
            }
            if (TxtToDate.Text == "0000/00/00")
            {
                TxtToDate.Text = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl6"].Rows[0]["Max"].ToString();
            }
            AccChange = false;
            Acc_Bs.DataSource = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl"];
            GrdAcc_Id.DataSource = Acc_Bs;
            if (connection.SQLDS.Tables["Reveal_Acc_Account_Tbl"].Rows.Count > 0)
            {
                AccChange = true;
                GrdAcc_Id_SelectionChanged(sender, e);
            }
        }
        //---------------------------
        private void GrdAcc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (AccChange)
            {
                Cur_DT = new DataTable();
                CurChange = false;
                Acc_Id = Convert.ToInt32(((DataRowView)Acc_Bs.Current).Row["Acc_Id"]);
                Cur_DT = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl1"].Select("Acc_Id = " + Acc_Id).CopyToDataTable();

                var query = from rec in Cur_DT.AsEnumerable()
                            select new
                            {
                                Cur_Aname = rec["Cur_Aname"],
                                Cur_Ename = rec["Cur_Ename"],
                                For_Cur_Id = rec["For_Cur_Id"],
                                Acc_Id = rec["Acc_Id"],
                                DOFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? rec["OFor_Amount"] : 0,
                                COFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["OFor_Amount"])),

                                DPFor_Amount = Convert.ToDecimal(rec["PFor_Amount"]) > 0 ? rec["PFor_Amount"] : 0,
                                CPFor_Amount = Convert.ToDecimal(rec["PFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["PFor_Amount"])),

                                DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? rec["EFor_Amount"] : 0,
                                CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
                            };
                Cur_DT = CustomControls.IEnumerableToDataTable(query);
                Cur_Bs.DataSource = Cur_DT;
                Grd_CurBalance.DataSource = Cur_Bs;
                if (Cur_Bs.List.Count > 0)
                {
                    CurChange = true;
                    Grd_CurBalance_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void Grd_CurBalance_SelectionChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                CustChange = false;
                Cur_Id = Convert.ToInt32(((DataRowView)Cur_Bs.Current).Row["For_Cur_Id"]);
                 Cust_DT = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl3"].Select("Acc_Id = " + Acc_Id + " And For_Cur_Id = " + Cur_Id).CopyToDataTable();

                var query = from rec in Cust_DT.AsEnumerable()
                            select new
                            {
                                Acust_Name = rec["Acust_Name"],
                                Ecust_Name = rec["Ecust_Name"],
                                For_Cur_Id = rec["For_Cur_Id"],
                                Cust_Id = rec["Cust_Id"],
                                Acc_Id = rec["Acc_Id"],
                                DOFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? rec["OFor_Amount"] : 0,
                                COFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["OFor_Amount"])),

                                DPFor_Amount = Convert.ToDecimal(rec["PFor_Amount"]) > 0 ? rec["PFor_Amount"] : 0,
                                CPFor_Amount = Convert.ToDecimal(rec["PFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["PFor_Amount"])),

                                DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? rec["EFor_Amount"] : 0,
                                CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
                            };
                Cust_DT = CustomControls.IEnumerableToDataTable(query);
                Cust_Bs.DataSource = Cust_DT;
                Grd_Cust.DataSource = Cust_Bs;
                if (Cust_Bs.List.Count > 0)
                {
                    CustChange = true;
                    Grd_Cust_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (CustChange)
            {
                Cust_Id = Convert.ToInt32(((DataRowView)Cust_Bs.Current).Row["Cust_Id"]);
                string Dot_Acc_No = ((DataRowView)Acc_Bs.Current).Row["Dot_Acc_No"].ToString();
                //---------------------------------------
                decimal MBalance = 0;
                
                try
                {
                    Dt = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl4"].Select("Cust_Id = " + Cust_Id
                                                                                                   + " And For_Cur_Id = " + Cur_Id
                                                                                                   + " And Dot_Acc_No Like '" + Dot_Acc_No + "%'").CopyToDataTable();
                }
                catch
                {
                    Dt = new DataTable();
                }
                foreach (DataRow Drow in Dt.Rows)
                {
                    if (Convert.ToInt32(Drow["RowNum"]) == 1)
                    {
                        MBalance = Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]);
                    }

                    if (Convert.ToInt32(Drow["RowNum"]) > 1)
                    {
                        MBalance += Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]);
                    }
                    Drow["Balance_For"] = MBalance;
                    Drow["CFor_Amount"] = Math.Abs(Convert.ToDecimal(Drow["CFor_Amount"]));
                }
                //---------------
                Grd_Vo_Details.DataSource = Dt;
            }
        }
        //---------------------------
        private void Btn_PrintAll_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Print_Details_Click(object sender, EventArgs e)
        {

        }
        //---------------------------
        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {

            Btn_Export_Details.Enabled = false;
            try
            {
                DataTable Dt_Cur = new DataTable();
                DataTable Dt_Cust = new DataTable();
                
                Date();
                DataTable Acc_Tbl = connection.SQLDS.Tables["Reveal_Acc_Account_Tbl"].DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Oloc_Amount", "Ploc_amount", "Eloc_amount").Select("Acc_id = " + ((DataRowView)Acc_Bs.Current).Row["Acc_id"]).CopyToDataTable();
                Dt_Cur = Cur_DT.DefaultView.ToTable(false, "for_cur_id", connection.Lang_id == 1 ? "cur_Aname" : "cur_Ename", "DOFor_Amount", "COFor_Amount", "DPFor_Amount", "CPFor_Amount", "DEFor_Amount", "CEFor_Amount").Select(" for_cur_id = " + ((DataRowView)Cur_Bs.Current).Row["for_cur_id"]).CopyToDataTable();
                Dt_Cust = Cust_DT.DefaultView.ToTable(false, "Cust_id", connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name", "DOFor_Amount", "COFor_Amount", "DPFor_Amount", "CPFor_Amount", "DEFor_Amount", "CEFor_Amount").Select("cust_id = " + ((DataRowView)Cust_Bs.Current).Row["Cust_id"]).CopyToDataTable();
                Dt = Dt.DefaultView.ToTable(false, "DFor_Amount", "CFor_Amount", "Balance_For", "Acc_Id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "Vo_no", connection.Lang_id == 1 ? "AOper_Name" : "EOper_Name", "Exch_Price", "Real_Price", "DLoc_Amount", "CLOC_Amount", "Notes", "Nrec_date1", "C_date");
                DataGridView[] Export_GRD = { Date_Grd, GrdAcc_Id, Grd_CurBalance, Grd_Cust, Grd_Vo_Details };
                DataTable[] Export_DT = { Date_Tbl, Acc_Tbl, Dt_Cur, Dt_Cust, Dt };

                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            catch
            {
              MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير  " : " No Details to Export ", MyGeneral_Lib.LblCap);
              Btn_Export_Details.Enabled = true;
              return;
             }
            Btn_Export_Details.Enabled = true;
        }
        //---------------------------
        private void button1_Click(object sender, EventArgs e)
        {

            //------------جلب جميع العملات لكل cust_id 
            var query = from rec in connection.SQLDS.Tables["Reveal_Acc_Account_Tbl1"].AsEnumerable()
                        select new
                        {
                            Cur_Aname = rec["Cur_Aname"],
                            Cur_Ename = rec["Cur_Ename"],
                            For_Cur_Id = rec["For_Cur_Id"],
                            Acc_Id = rec["Acc_Id"],
                            DOFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? rec["OFor_Amount"] : 0,
                            COFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["OFor_Amount"])),

                            DPFor_Amount = Convert.ToDecimal(rec["PFor_Amount"]) > 0 ? rec["PFor_Amount"] : 0,
                            CPFor_Amount = Convert.ToDecimal(rec["PFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["PFor_Amount"])),

                            DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? rec["EFor_Amount"] : 0,
                            CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
                      };
            DataTable Exp_cur = CustomControls.IEnumerableToDataTable(query);

           
            //-----------------------دمج Tables

            var result = (from drow in connection.SQLDS.Tables["Reveal_Acc_Account_Tbl"].AsEnumerable()
                          join drow1 in Exp_cur.AsEnumerable()
                              on drow.Field<Int32>("Acc_id") equals drow1.Field<Int32>("Acc_id")
                          orderby drow.Field<Int32>("Acc_id")
                          select new
                          {
                              Acc_Id = drow["Acc_Id"],
                              Acc_name = drow[connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename"],
                              Oloc_Amount = drow["Oloc_Amount"],
                              Ploc_Amount = drow["Ploc_Amount"],
                              Eloc_Amount = drow["Eloc_Amount"],
                             
                              for_Cur_id = drow1["for_cur_id"],
                              Cur_Aname = drow1[connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename"],
                              DOFor_Amount = drow1["DOFor_Amount"],
                              COFor_Amount = drow1["COFor_Amount"],
                              DPFor_Amount = drow1["DPFor_Amount"],
                              CPFor_Amount = drow1["CPFor_Amount"],
                              DEFor_Amount = drow1["DEFor_Amount"],
                              CEFor_Amount = drow1["CEFor_Amount"]
                          }
                       );

            DataTable Exp_Dt = CustomControls.IEnumerableToDataTable(result);
            //-----------------------دمج Datagridview
            int K = 0;
            DataGridView GRD = new DataGridView();
            DataGridView[] Grid_array = { GrdAcc_Id, Grd_CurBalance };
            for (int i = 0; i < Grid_array.Count(); i++)
            {
                for (int j = 0; j < Grid_array[i].ColumnCount; j++)
                {
                    GRD.Columns.Add("Col" + K, Grid_array[i].Columns[j].HeaderText.Trim());
                    K++;
                }
            }
            Date();
            DataGridView[] Export_GRD = { Date_Grd, GRD };
            DataTable[] Export_DT = { Date_Tbl, Exp_Dt };

            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }
        //---------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Reveal_Acc_Main2_FormClosed(object sender, FormClosedEventArgs e)
        {
            CustChange = false;
            CurChange = false;
            AccChange = false;
            string[] Used_Tbl = { "Reveal_Acc_Account_Tbl", "Reveal_Acc_Account_Tbl1", 
                                     "Reveal_Acc_Account_Tbl2", "Reveal_Acc_Account_Tbl3", 
                                     "Reveal_Acc_Account_Tbl4","Reveal_Acc_Account_Tbl5","Reveal_Acc_Account_Tbl6" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        private void Date()
        {

            //string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
            //string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
            //if (From_Date == "-1")
            //    From_Date = "0";
            //if (To_Date == "-1")
            //    To_Date = "0";

            Date_Grd = new DataGridView();
            Date_Tbl = new DataTable();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            Date_Tbl.Rows.Add(new object[] { TxtFromDate.Text ,TxtToDate.Text });
        }
    }
}