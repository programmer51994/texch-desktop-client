﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Cust_Account : Form
    {
        #region Defintion
        string @format = "dd/MM/yyyy";
        //string @format_null = "";
        //string S_doc_exp_null = "0000/00/00";
        DataTable DT_Cur = new DataTable();
        DataTable DT_Cust = new DataTable();
        DataTable DT_Acc = new DataTable();
        DataTable DT_Cur_TBL = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        DataTable DT_Acc_TBL = new DataTable();
        BindingSource _bsyears = new BindingSource();
        int Cur_id = 0;
        int Cust_id = 0;
        int Acc_Id = 0;
        string Filter = "";
        bool Change_grd = false;
        int min_nrec_date_int = 0;
        int from_min_nrec_date_int = 0;
        #endregion
        //---------------------------
        public Search_Cust_Account()
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cur.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Acc.AutoGenerateColumns = false;
            Grd_years.AutoGenerateColumns = false;
            Create_Tbl();
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cur_Id.Columns["Column2"].DataPropertyName = "Cur_Ename";
                Grd_Cur.Columns["Column5"].DataPropertyName = "Cur_Ename";
                Grd_Cust_Id.Columns["Column8"].DataPropertyName = "Ecust_name";
                Grd_Cust.Columns["Column11"].DataPropertyName = "Ecust_name";
                Grd_Acc_Id.Columns["Column15"].DataPropertyName = "Acc_Ename";
                Grd_Acc.Columns["Column16"].DataPropertyName = "Acc_Ename";
            }
            #endregion
            #endregion
        }
        //---------------------------
        private void Create_Tbl()
        {
            string[] Column = { "cur_id", "Cur_ANAME", "Cur_ENAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Cur = CustomControls.Custom_DataTable("DT_Cur", Column, DType);
            Grd_Cur.DataSource = DT_Cur;
            //-----------
            string[] Column1 = { "Cust_id", "ACUST_NAME", "ECUST_NAME" };
            string[] DType1 = { "System.Int16", "System.String", "System.String" };
            DT_Cust = CustomControls.Custom_DataTable("DT_Cust", Column1, DType1);
            Grd_Cust.DataSource = DT_Cust;
            //---------
            string[] Column2 = { "Acc_id", "Acc_ANAME", "Acc_Ename" };
            string[] DType2 = { "System.Int16", "System.String", "System.String" };
            DT_Acc = CustomControls.Custom_DataTable("DT_Acc", Column2, DType2);
            Grd_Acc.DataSource = DT_Acc;
        }
        //---------------------------
        private void Search_Cust_Account_Load(object sender, EventArgs e)
        {
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;
            //TxtFromDate.DataBindings.Clear();
            //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl4"], "From_Nrec_Date");

            TxtFromDate.Text = connection.SQLDS.Tables["HPage_Tbl4"].Rows[0]["From_Nrec_Date"].ToString();

            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            connection.SqlExec("Exec Search_Cust_Account " + connection.T_ID, "Cust_Cur_Acc_Tbl");
            CboCust_Type.DataSource = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl3"];
            CboCust_Type.DisplayMember = connection.Lang_id == 1 ? "Cust_Type_AName" : "Cust_Type_EName";
            CboCust_Type.ValueMember = "Cust_Type_ID";
            CboCust_Type.SelectedIndex = 0;
            connection.SqlExec("Exec Search_Record_Operation", "Records_Tbl");
            cbo_year.SelectedIndex = 0;
        }
        //-----------------------------------------------------
        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Cust_type_id = "";
                Cust_id = 0;
                string Cond_Str_Cust_Id = "";
                string Con_Str = "";
                if (DT_Cust.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cust, "Cust_Id", out Cond_Str_Cust_Id);
                    Con_Str = " And CUST_ID not in(" + Cond_Str_Cust_Id + ")";
                }
                int.TryParse(TxtCust_Name.Text, out Cust_id);

                if (Convert.ToInt16(CboCust_Type.SelectedValue) != 0)
                    Cust_type_id = " And Cust_Type_Id = " + CboCust_Type.SelectedValue.ToString();

                Filter = " (Acust_name like '" + TxtCust_Name.Text + "%' or  Ecust_name like '" + TxtCust_Name.Text + "%'"
                            + " Or Cust_id = " + Cust_id + ")" + Con_Str + Cust_type_id;
                DT_Cust_TBL = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl"].DefaultView.ToTable(true, "Cust_id", "ACust_name", "Ecust_name", "cust_type_id").Select(Filter).CopyToDataTable();
                Grd_Cust_Id.DataSource = DT_Cust_TBL;

            }
            catch
            {
                Grd_Cust_Id.DataSource = new DataTable();
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {

                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }
        //-----------------------------------------------
        private void Chk_Cust_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cust.Checked)
            {
                TxtCust_Name.Enabled = true;
                TxtCust_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cust.Clear();
                DT_Cust_TBL.Clear();
                TxtCust_Name.ResetText();
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                TxtCust_Name.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cust.NewRow();

            row["Cust_id"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["Cust_Id"];
            row["ACUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ACust_name"];
            row["ECUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ECust_name"];
            DT_Cust.Rows.Add(row);
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);

            button3.Enabled = true;
            button4.Enabled = true;
            if (Grd_Cust_Id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }

            Chk_Cur_CheckedChanged(null, null);
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cust_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cust.NewRow();
                row["Cust_id"] = DT_Cust_TBL.Rows[i]["Cust_id"];
                row["ACUST_NAME"] = DT_Cust_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Cust_TBL.Rows[i]["ECUST_NAME"];
                DT_Cust.Rows.Add(row);
            }
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Enabled = true;
            Chk_Cur_CheckedChanged(null, null);
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button3_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows[Grd_Cust.CurrentRow.Index].Delete();
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            if (Grd_Cust.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button4_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows.Clear();
            TxtCust_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
            Chk_Cur_CheckedChanged(null, null);
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------------
        private void chk_Acc_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Acc.Checked)
            {
                TxtAcc_Name.Enabled = true;
                TxtAcc_Name_TextChanged(null, null);
            }
            else
            {
                DT_Acc.Clear();
                DT_Acc_TBL.Clear();
                TxtAcc_Name.ResetText();
                button9.Enabled = false;
                button10.Enabled = false;
                button11.Enabled = false;
                button12.Enabled = false;
                TxtAcc_Name.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Cust_id = 0;
                Cur_id = 0;
                string Cond_Str_Cust_Id = "";
                string Con_Str = "";
                string Cond_Str_Cur_Id = "";
                string Con_Str2 = "";
                if (DT_Cust.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cust, "Cust_Id", out Cond_Str_Cust_Id);
                    Con_Str = " And  CUST_ID  in(" + Cond_Str_Cust_Id + ")";
                }
                if (DT_Cur.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out Cond_Str_Cur_Id);
                    Con_Str2 = " And  Cur_ID not in(" + Cond_Str_Cur_Id + ")";
                }
                int.TryParse(TxtCur_Name.Text, out Cur_id);

                Filter = " (Cur_Aname like '" + TxtCur_Name.Text + "%' or  Cur_Ename like '" + TxtCur_Name.Text + "%'"
                            + " Or Cur_id = " + Cur_id + ")" + Con_Str + Con_Str2;
                # endregion
                DT_Cur_TBL = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl1"].Select(Filter).CopyToDataTable();
                DT_Cur_TBL = DT_Cur_TBL.DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
                Grd_Cur_Id.DataSource = DT_Cur_TBL;
            }
            catch
            {
                Grd_Cur_Id.DataSource = new DataTable();
            }
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                button5.Enabled = false;
                button6.Enabled = false;
            }
            else
            {
                button5.Enabled = true;
                button6.Enabled = true;
            }
        }
        //-----------------------------------------------
        private void Chk_Cur_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked)
            {
                TxtCur_Name.Enabled = true;
                TxtCur_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cur.Clear();
                DT_Cur_TBL.Clear();
                TxtCur_Name.ResetText();
                button5.Enabled = false;
                button6.Enabled = false;
                button7.Enabled = false;
                button8.Enabled = false;
                TxtCur_Name.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button5_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cur.NewRow();

            row["Cur_id"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_Id"];
            row["Cur_aname"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_aname"];
            row["Cur_ename"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_ename"];
            DT_Cur.Rows.Add(row);
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);

            button7.Enabled = true;
            button8.Enabled = true;
            if (Grd_Cur_Id.Rows.Count == 0)
            {
                button5.Enabled = false;
                button6.Enabled = false;
            }

            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button6_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cur_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cur.NewRow();
                row["Cur_id"] = DT_Cur_TBL.Rows[i]["Cur_id"];
                row["Cur_aname"] = DT_Cur_TBL.Rows[i]["Cur_aname"];
                row["Cur_ename"] = DT_Cur_TBL.Rows[i]["Cur_ename"];
                DT_Cur.Rows.Add(row);
            }
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = true;
            button8.Enabled = true;
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button7_Click(object sender, EventArgs e)
        {
            DT_Cur.Rows[Grd_Cur.CurrentRow.Index].Delete();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Cur.Rows.Count == 0)
            {
                button7.Enabled = false;
                button8.Enabled = false;
            }
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button8_Click(object sender, EventArgs e)
        {
            DT_Cur.Rows.Clear();
            TxtCur_Name_TextChanged(null, null);
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = false;
            button8.Enabled = false;
            chk_Acc_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void TxtAcc_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Cust_id = 0;
                Cur_id = 0;
                Acc_Id = 0;
                string Cond_Str_Cust_Id = "";
                string Con_Str = "";
                string Cond_Str_Cur_Id = "";
                string Con_Str2 = "";
                string Cond_Str_Acc_Id = "";
                string Con_Str3 = "";
                if (DT_Cust.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cust, "Cust_Id", out Cond_Str_Cust_Id);
                    Con_Str = " And  CUST_ID  in(" + Cond_Str_Cust_Id + ")";
                }
                if (DT_Cur.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out Cond_Str_Cur_Id);
                    Con_Str2 = " And  Cur_ID in(" + Cond_Str_Cur_Id + ")";
                }
                if (DT_Acc.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Cond_Str_Acc_Id);
                    Con_Str3 = " And  Acc_ID not in(" + Cond_Str_Acc_Id + ")";
                }
                int.TryParse(TxtAcc_Name.Text, out Acc_Id);

                Filter = " (Acc_Aname like '" + TxtAcc_Name.Text + "%' or  Acc_Ename like '" + TxtAcc_Name.Text + "%'"
                            + " Or Acc_id = " + Acc_Id + ")" + Con_Str + Con_Str2 + Con_Str3;
                # endregion
                DT_Acc_TBL = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl2"].Select(Filter).CopyToDataTable();
                DT_Acc_TBL = DT_Acc_TBL.DefaultView.ToTable(true, "Acc_Id", "Acc_Aname", "Acc_Ename").Select().CopyToDataTable();
                Grd_Acc_Id.DataSource = DT_Acc_TBL;
            }

            catch
            {
                Grd_Acc_Id.DataSource = new DataTable();
            }
            if (Grd_Acc_Id.Rows.Count <= 0)
            {
                button9.Enabled = false;
                button10.Enabled = false;
            }
            else
            {
                button9.Enabled = true;
                button10.Enabled = true;
            }
        }
        //-----------------------------------------------
        private void button9_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Acc.NewRow();

            row["Acc_id"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_Id"];
            row["Acc_aname"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_aname"];
            row["Acc_ename"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_ename"];
            DT_Acc.Rows.Add(row);
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);

            button11.Enabled = true;
            button12.Enabled = true;
            if (Grd_Acc_Id.Rows.Count == 0)
            {
                button9.Enabled = false;
                button10.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button10_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Acc_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Acc.NewRow();
                row["Acc_id"] = DT_Acc_TBL.Rows[i]["Acc_id"];
                row["Acc_aname"] = DT_Acc_TBL.Rows[i]["Acc_aname"];
                row["Acc_ename"] = DT_Acc_TBL.Rows[i]["Acc_ename"];
                DT_Acc.Rows.Add(row);
            }
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button9.Enabled = false;
            button10.Enabled = false;
            button11.Enabled = true;
            button12.Enabled = true;
        }
        //-----------------------------------------------
        private void button11_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows[Grd_Acc.CurrentRow.Index].Delete();
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Acc.Rows.Count == 0)
            {
                button11.Enabled = false;
                button12.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button12_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows.Clear();
            TxtAcc_Name_TextChanged(null, null);
            button9.Enabled = true;
            button10.Enabled = true;
            button11.Enabled = false;
            button12.Enabled = false;

        }
        //-----------------------------------------------
        private void Search_New_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Cust_Cur_Acc_Tbl", "Cust_Cur_Acc_Tbl1", "Cust_Cur_Acc_Tbl2", "Cust_Cur_Acc_Tbl3", "Reveal_Cust_Account_Tbl", "Records_Tbl5" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
           //string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
           //string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
           string InRecDate = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);
            
            //if (Convert.ToInt32(From_Date) > Convert.ToInt32(InRecDate))
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? " تحقق من التأريخ " : "Check The Date Please ", MyGeneral_Lib.LblCap);
            //    return;
            //}
          
            //if (Convert.ToInt32(To_Date) == 0 && Convert.ToInt32(From_Date) == 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "تحقق من التواريخ  " : "Check The Date Please ", MyGeneral_Lib.LblCap);
            //    return;
            //}

            #endregion
            string Cust_IDStr = "";
            string Cur_IDStr = "";
            string ACC_IDStr = "";
            int from_nrec_date = 0;
            int to_nrec_date = 0;

            if (cbo_year.SelectedIndex == 0)
            {
                if (TxtFromDate.Checked == true)
                {

                    DateTime date = TxtFromDate.Value.Date;
                    from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked == true)
                {

                    DateTime date = TxtToDate.Value.Date;
                    to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    to_nrec_date = 0;
                }

                if (TxtFromDate.Checked == true)
                {
                    DateTime min_nrec_date = Convert.ToDateTime(connection.SQLDS.Tables["HPage_Tbl4"].Rows[0]["From_Nrec_Date"].ToString());
                    min_nrec_date_int = min_nrec_date.Day + min_nrec_date.Month * 100 + min_nrec_date.Year * 10000;

                    string message_date = connection.SQLDS.Tables["HPage_Tbl4"].Rows[0]["From_Nrec_Date"].ToString();

                    DateTime from_min_nrec_date = TxtFromDate.Value.Date;
                    from_min_nrec_date_int = from_min_nrec_date.Day + from_min_nrec_date.Month * 100 + from_min_nrec_date.Year * 10000;

                    if (from_min_nrec_date_int < min_nrec_date_int)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب ان يكون تاريخ الفترة من اكبر او يساوي" + message_date : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                        return;
                    }

                }

                if (Convert.ToInt32(from_nrec_date) > Convert.ToInt32(to_nrec_date) && Convert.ToInt32(to_nrec_date) != 0)
             
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }

            }
            if (cbo_year.SelectedIndex == 1)//old
            {
                if (TxtFromDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة من" : "Please Check from date", MyGeneral_Lib.LblCap);
                    return;
                }

                if (TxtToDate.Checked != true)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار فترة الى" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }

                DateTime date_from = TxtFromDate.Value.Date;
                from_nrec_date = date_from.Day + date_from.Month * 100 + date_from.Year * 10000;
                DateTime date_to = TxtToDate.Value.Date;
                to_nrec_date = date_to.Day + date_to.Month * 100 + date_to.Year * 10000;

                int Real_from_date = Convert.ToInt32(connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["min_nrec_date_int"]);
                int Real_To_date = Convert.ToInt32(connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["max_nrec_date_int"]);


                if ((from_nrec_date < Real_from_date) || (from_nrec_date > Real_To_date) || (to_nrec_date < Real_from_date) || (to_nrec_date > Real_To_date))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " الرجاء اختيار التاريخ ضمن الفتره" : "Please Check To date", MyGeneral_Lib.LblCap);
                    return;
                }
                if (from_nrec_date > to_nrec_date)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره من اصغر من تاريخ الفتره الى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                    return;
                }


            }



            MyGeneral_Lib.ColumnToString(DT_Cust, "Cust_Id", out  Cust_IDStr);
            MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out  Cur_IDStr);
            MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out  ACC_IDStr);
            object[] Sparam = {Cust_IDStr,connection.T_ID.ToString(),
                                   Cur_IDStr,ACC_IDStr,from_nrec_date,
                                  to_nrec_date,0,connection.user_id,""};
            AddBtn.Enabled = false;
            ExtBtn.Enabled = true;
            if (cbo_year.SelectedIndex == 0)
            {
                MyGeneral_Lib.Copytocliptext("Reveal_Cust_Account", Sparam);
                connection.SqlExec("Reveal_Cust_Account", "Reveal_Cust_Account_Tbl", Sparam);
                connection.SQLCMD.Parameters.Clear();
            }
            else
            {

                MyGeneral_Lib.Copytocliptext("Reveal_Cust_Account_phst", Sparam);
                connection.SqlExec("Reveal_Cust_Account_phst", "Reveal_Cust_Account_Tbl", Sparam);
                connection.SQLCMD.Parameters.Clear();
            }


            if (connection.Col_Name != "")
            {
                MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            Reveal_Cust_Main Frm = new Reveal_Cust_Main(from_nrec_date.ToString(), to_nrec_date.ToString());
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;
            AddBtn.Enabled = true;
        }
        //-------------------------------------------------------------------
        private void CboCust_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            Chk_Cust_CheckedChanged(sender, e);
            Chk_Cur_CheckedChanged(sender, e);
            chk_Acc_CheckedChanged(sender, e);
            DT_Cust.Clear();
            DT_Cur.Clear();
            DT_Acc.Clear();
        }

        private void Search_Cust_Account_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void cbo_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_year.SelectedIndex == 0)//البيانات الحالية
            {
                Grd_years.Enabled = false;
                Grd_years.DataSource = new BindingSource();
                //TxtFromDate.DataBindings.Clear();
                //TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl4"], "From_Nrec_Date");

                TxtFromDate.Text = connection.SQLDS.Tables["HPage_Tbl4"].Rows[0]["From_Nrec_Date"].ToString();
                TxtToDate.Checked = false;


            }
            if (cbo_year.SelectedIndex == 1)// old 
            {

                try
                {
                    Grd_years.Enabled = true;
                    //_bsyears.DataSource = connection.SQLDS.Tables["Records_Tbl4"];
                    //Grd_years.DataSource = _bsyears;

                    if (connection.SQLDS.Tables["Records_Tbl4"].Rows.Count > 0)
                    {
                        Grd_years.DataSource = connection.SQLDS.Tables["Records_Tbl4"].Select("t_id =" + connection.T_ID).CopyToDataTable();

                        TxtFromDate.Text = connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["min_nrec_date"].ToString();
                        TxtToDate.Text = connection.SQLDS.Tables["Records_Tbl5"].Rows[0]["max_nrec_date"].ToString();
                        //Change_grd = true;
                        //Grd_years_SelectionChanged(null, null);
                    }
                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                        cbo_year.SelectedIndex = 0;
                        return;
                    }
                }

                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " لا توجد بيانات مرحلة " : " There is no data ", MyGeneral_Lib.LblCap);
                    cbo_year.SelectedIndex = 0;
                    return;
                }
            }

        }

        //private void Grd_years_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (Change_grd)
        //    {
        //        TxtFromDate.Enabled = true;
        //        TxtToDate.Enabled = true;
        //        TxtFromDate.DataBindings.Clear();
        //        TxtToDate.DataBindings.Clear();
        //        TxtFromDate.DataBindings.Add("Text", _bsyears, "FromNrec_date");
        //        TxtToDate.DataBindings.Add("Text", _bsyears, "TONrec_date");

        //    }
        //}
    }
}