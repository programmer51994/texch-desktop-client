﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Cust_Main : Form
    {
        #region Definshion
         bool CustChange = false;
         bool CurChange = false;
         bool AccChange = false;
         BindingSource Cust_Bs = new BindingSource();
         BindingSource Cur_Bs = new BindingSource();
         BindingSource Acc_Bs = new BindingSource();
         int Cust_Id = 0;
         int Cur_Id = 0;
         int Acc_Id = 0;
         int Oper_Id = 0;
         DataTable Cur_DT = new DataTable();
         DataGridView Date_Grd = new DataGridView();
         DataTable Date_Tbl = new DataTable();
         DataTable Acc_DT = new DataTable();
         DataTable Cust_Acc = new DataTable();
         string From_ = "";
         string TO_ = "";
        #endregion
        //---------------------------
        public Reveal_Cust_Main(string FromDate, string ToDate)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            From_ = FromDate;
            TO_ = ToDate;

            Grd_Cust.AutoGenerateColumns = false;
            Grd_CurBalance.AutoGenerateColumns = false;
            GrdAcc_Id.AutoGenerateColumns = false;
            Grd_Vo_Details.AutoGenerateColumns = false;
            #region Arabic/English
            if (connection.Lang_id != 1)
            {
                Column15.DataPropertyName = "Acc_Ename";
                Column7.DataPropertyName = "Cur_Ename";
                Column26.DataPropertyName = "Acc_Ename";
                dataGridViewTextBoxColumn1.DataPropertyName = "EOper_Name";
            }
            #endregion
        }
        //---------------------------
        private void Reveal_Cust_Main_Load(object sender, EventArgs e)

        {
            if (From_ != "0" && TO_ == "0")
            {
                TxtFromDate.Text = From_;
                TxtToDate.Text = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl5"].Rows[0]["Max"].ToString();
            
            }
            if (From_ != "0" && TO_ != "0")
            {
                TxtFromDate.Text = From_;
                TxtToDate.Text = TO_;
            
            }


            if (From_ == "0" && TO_ != "0")
            {
                TxtToDate.Text = TO_;
                TxtFromDate.Text = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl5"].Rows[0]["Min"].ToString();

            }


            if (From_ == "0" && TO_ == "0")
            {

                TxtFromDate.Text = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl5"].Rows[0]["Min"].ToString();
                TxtToDate.Text = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl6"].Rows[0]["Max"].ToString();
            }



            //if (From_ == "0")
            //{
            //    TxtFromDate.Text = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl5"].Rows[0]["Min"].ToString();
            //}
            //else
            //{
            //    TxtFromDate.Text = From_;
                
            //}
            //if (TO_ =="0")
            //{
            //    TxtToDate.Text = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl6"].Rows[0]["Max"].ToString();
            //}
            //else
            //{ TxtToDate.Text = TO_; }

            CustChange = false;
            Cust_Bs.DataSource = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl"];
            Grd_Cust.DataSource = Cust_Bs;
            if (connection.SQLDS.Tables["Reveal_Cust_Account_Tbl"].Rows.Count > 0)
            {
                CustChange = true;
                Grd_Cust_SelectionChanged(sender, e);
            }
        }
        //---------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (CustChange)
            {
                Cur_DT = new DataTable();
                CurChange = false;
                Cust_Id = Convert.ToInt32(((DataRowView)Cust_Bs.Current).Row["Cust_Id"]);
                Cur_DT = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl1"].Select("Cust_Id = " + Cust_Id).CopyToDataTable();

                var query = from rec in Cur_DT.AsEnumerable()
                            select new
            {
                Cur_Aname = rec["Cur_Aname"],
                Cur_Ename = rec["Cur_Ename"],
                For_Cur_Id = rec["For_Cur_Id"],
                Cust_Id = rec["Cust_Id"],
                DOFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? rec["OFor_Amount"] : 0,
                COFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["OFor_Amount"])),

                DPFor_Amount = Convert.ToDecimal(rec["For_Amount"]) > 0 ? rec["For_Amount"] : 0,
                CPFor_Amount = Convert.ToDecimal(rec["For_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["For_Amount"])),

                DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? rec["EFor_Amount"] : 0,
                CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
            };
                Cur_DT = CustomControls.IEnumerableToDataTable(query);
                Cur_Bs.DataSource = Cur_DT;
                Grd_CurBalance.DataSource = Cur_Bs;
                if (Cur_Bs.List.Count > 0)
                {
                    CurChange = true;
                    Grd_CurBalance_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void Grd_CurBalance_SelectionChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                AccChange = false;
                Cur_Id = Convert.ToInt32(((DataRowView)Cur_Bs.Current).Row["For_Cur_Id"]);
                 Acc_DT = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl3"].Select("Cust_Id = " + Cust_Id + " And For_Cur_Id = " + Cur_Id).CopyToDataTable();

                var query = from rec in Acc_DT.AsEnumerable()
                            select new
                            {
                                Acc_Aname = rec["Acc_Aname"],
                                Acc_Ename = rec["Acc_Ename"],
                                For_Cur_Id = rec["For_Cur_Id"],
                                Cust_Id = rec["Cust_Id"],
                                Acc_Id = rec["Acc_Id"],
                                DOFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? rec["OFor_Amount"] : 0,
                                COFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["OFor_Amount"])),

                                DPFor_Amount = Convert.ToDecimal(rec["For_Amount"]) > 0 ? rec["For_Amount"] : 0,
                                CPFor_Amount = Convert.ToDecimal(rec["For_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["For_Amount"])),

                                DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? rec["EFor_Amount"] : 0,
                                CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
                            };
                Acc_DT = CustomControls.IEnumerableToDataTable(query);
                Acc_Bs.DataSource = Acc_DT;
                GrdAcc_Id.DataSource = Acc_Bs;
                if (Acc_Bs.List.Count > 0)
                {
                    AccChange = true;
                    GrdAcc_Id_SelectionChanged(sender, e);
                }
            }
        }
        //---------------------------
        private void GrdAcc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (AccChange)
            {
                Acc_Id = Convert.ToInt32(((DataRowView)Acc_Bs.Current).Row["Acc_Id"]);
                //---------------------------------------
                decimal MBalance = 0;
               
                try
                {
                    Cust_Acc = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl4"].Select("Cust_Id = " + Cust_Id
                                                                                                   + " And For_Cur_Id = " + Cur_Id
                                                                                                   + " And Acc_Id = " + Acc_Id).CopyToDataTable();
                    //Dt.DefaultView.Sort = "C_date ASC"; 
                }
                catch
                {
                    Cust_Acc = new DataTable();
                }
                foreach (DataRow Drow in Cust_Acc.Rows)
                {
                    if (Convert.ToInt32(Drow["RowNum"]) == 0)
                    {
                        MBalance = Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]);
                    }

                    if (Convert.ToInt32(Drow["RowNum"]) >= 1)
                    {
                        MBalance += Convert.ToDecimal(Drow["DFor_Amount"]) + Convert.ToDecimal(Drow["CFor_Amount"]);
                    }
                    Drow["Balance_For"] = MBalance;
                }
                //---------------
                Grd_Vo_Details.DataSource = Cust_Acc;
            }
        }
        //---------------------------
        private void Btn_PrintAll_Click(object sender, EventArgs e)
        {
            string From_Date = (TxtFromDate.Text);
            string To_Date = TxtToDate.Text;
            Reavel_cust_All_rpt ObjRpt = new Reavel_cust_All_rpt();
            Reavel_cust_All_rpt ObjRptEng = new Reavel_cust_All_rpt();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Acust_Name", ((DataRowView)Cust_Bs.Current).Row["Acust_Name"].ToString());
            Dt_Param.Rows.Add("Ecust_Name", ((DataRowView)Cust_Bs.Current).Row["Ecust_Name"].ToString());
            Dt_Param.Rows.Add("Cur_Aname", ((DataRowView)Cur_Bs.Current).Row["Cur_Aname"].ToString());
            Dt_Param.Rows.Add("Cur_ename", ((DataRowView)Cur_Bs.Current).Row["Cur_ename"].ToString());
            Dt_Param.Rows.Add("Acc_Aname", ((DataRowView)Acc_Bs.Current).Row["Acc_Aname"].ToString());
            Dt_Param.Rows.Add("Acc_ename", ((DataRowView)Acc_Bs.Current).Row["Acc_ename"].ToString());
            Dt_Param.Rows.Add("Max_DATE", To_Date);
            Dt_Param.Rows.Add("Min_DATE", From_Date);

            Cust_Acc.TableName = "Cust_Acc";
            connection.SQLDS.Tables.Add(Cust_Acc);
            connection.SQLDS.Tables["Cust_Acc"].TableName = "Cust_Acc";
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0,true);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Cust_Acc"]);
        }
        //---------------------------
        private void Btn_Print_Details_Click(object sender, EventArgs e)
        {
            //DataTable Cust_Acc=new DataTable();
            //Cust_Acc=Dt;
            string From_Date = (TxtFromDate.Text);
            string To_Date = TxtToDate.Text;
            Reavel_cust_main_rpt ObjRpt = new Reavel_cust_main_rpt();
            Reavel_cust_main_rpt_eng ObjRptEng = new Reavel_cust_main_rpt_eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Acust_Name", ((DataRowView)Cust_Bs.Current).Row["Acust_Name"].ToString());
            Dt_Param.Rows.Add("Ecust_Name", ((DataRowView)Cust_Bs.Current).Row["Ecust_Name"].ToString());
            Dt_Param.Rows.Add("Cur_Aname", ((DataRowView)Cur_Bs.Current).Row["Cur_Aname"].ToString());
            Dt_Param.Rows.Add("Cur_ename", ((DataRowView)Cur_Bs.Current).Row["Cur_ename"].ToString());
            Dt_Param.Rows.Add("Acc_Aname", ((DataRowView)Acc_Bs.Current).Row["Acc_Aname"].ToString());
            Dt_Param.Rows.Add("Acc_ename", ((DataRowView)Acc_Bs.Current).Row["Acc_ename"].ToString());
            Dt_Param.Rows.Add("Max_DATE", To_Date);
            Dt_Param.Rows.Add("Min_DATE", From_Date);

            Cust_Acc.TableName = "Cust_Acc";
            connection.SQLDS.Tables.Add(Cust_Acc);
            connection.SQLDS.Tables["Cust_Acc"].TableName = "Cust_Acc";
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove(connection.SQLDS.Tables["Cust_Acc"]);
        }
        //---------------------------
        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {
            if (Cust_Acc.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد حركات للقيود" : "No recordes");
                return; 
            
            
            }
            DataTable Dt_cur = new DataTable();
            DataTable Dt_Acc = new DataTable();
            DataTable Dt_Details = new DataTable();
            Date();
            DataTable cust_Tbl = connection.SQLDS.Tables["Reveal_Cust_Account_Tbl"].DefaultView.ToTable(false, "cust_id", "Acust_name", "ecust_name", "Phone", connection.Lang_id == 1 ? "A_address" : "E_Address").Select("cust_id = " + ((DataRowView)Acc_Bs.Current).Row["cust_id"]).CopyToDataTable();
            Dt_cur = Cur_DT.DefaultView.ToTable(false, "for_cur_id", connection.Lang_id == 1 ? "cur_Aname" : "cur_Ename", "DOFor_Amount", "COFor_Amount", "DPFor_Amount", "CPFor_Amount", "DEFor_Amount", "CEFor_Amount").Select(" for_cur_id = " + ((DataRowView)Cur_Bs.Current).Row["for_cur_id"]).CopyToDataTable();
            Dt_Acc = Acc_DT.DefaultView.ToTable(false, "Acc_id", connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename", "DOFor_Amount", "COFor_Amount", "DPFor_Amount", "CPFor_Amount", "DEFor_Amount", "CEFor_Amount").Select(" Acc_id = " + ((DataRowView)Acc_Bs.Current).Row["Acc_id"]).CopyToDataTable();
            Dt_Details = Cust_Acc.DefaultView.ToTable(false, "DFor_Amount", "CFor_Amount", "Balance_For", "Acc_Id", "Acc_Aname", "Vo_No", connection.Lang_id == 1 ? "AOper_Name" : "EOper_Name", "Exch_Price", "Real_Price", "DLoc_Amount", "CLOC_Amount", "Notes", "Nrec_date1", "C_Date").Select().CopyToDataTable();
            DataGridView[] Export_GRD = { Date_Grd, Grd_Cust, Grd_CurBalance, GrdAcc_Id, Grd_Vo_Details };
            DataTable[] Export_DT = { Date_Tbl, cust_Tbl, Dt_cur, Dt_Acc, Dt_Details };

            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

        }
        //---------------------------
        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------
        private void Reveal_Cust_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            CustChange = false;
            CurChange = false;
            AccChange = false;
            string[] Used_Tbl = { "Reveal_Cust_Account_Tbl", "Reveal_Cust_Account_Tbl1", "Reveal_Cust_Account_Tbl2",
                                     "Reveal_Cust_Account_Tbl3","Reveal_Cust_Account_Tbl4","Reveal_Cust_Account_Tbl5","Reveal_Cust_Account_Tbl6" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //------------------------------------------ 
        private void button1_Click(object sender, EventArgs e)
        {

            //------------جلب جميع العملات لكل cust_id 
            var query = from rec in connection.SQLDS.Tables["Reveal_Cust_Account_Tbl1"].AsEnumerable()
                        select new
                        {
                            Cur_Aname = rec["Cur_Aname"],
                            Cur_Ename = rec["Cur_Ename"],
                            For_Cur_Id = rec["For_Cur_Id"],
                            Cust_Id = rec["Cust_Id"],
                            DOFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? rec["OFor_Amount"] : 0,
                            COFor_Amount = Convert.ToDecimal(rec["OFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["OFor_Amount"])),

                            DPFor_Amount = Convert.ToDecimal(rec["For_Amount"]) > 0 ? rec["For_Amount"] : 0,
                            CPFor_Amount = Convert.ToDecimal(rec["For_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["For_Amount"])),

                            DEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? rec["EFor_Amount"] : 0,
                            CEFor_Amount = Convert.ToDecimal(rec["EFor_Amount"]) > 0 ? 0 : Math.Abs(Convert.ToDecimal(rec["EFor_Amount"])),
                        };
            DataTable Exp_cur = CustomControls.IEnumerableToDataTable(query);
            //-----------------------دمج Tables

            var result = (from drow in connection.SQLDS.Tables["Reveal_Cust_Account_Tbl"].AsEnumerable()
                           join drow1 in Exp_cur.AsEnumerable()
                               on drow.Field<Int32>("Cust_id") equals drow1.Field<Int32>("Cust_id")
                          select new
                          {
                              Cust_Id = drow["Cust_id"],
                              Acust_name = drow["Acust_name"],
                              Ecust_name = drow["Ecust_name"],
                              phone = drow["phone"],
                              A_Address = drow[connection.Lang_id == 1 ? "A_Address" : "E_Address"],
                              for_Cur_id = drow1["for_cur_id"],
                              Cur_Aname = drow1[connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename"],
                              DOFor_Amount = drow1["DOFor_Amount"],
                              COFor_Amount = drow1["COFor_Amount"],
                              DPFor_Amount = drow1["DPFor_Amount"],
                              CPFor_Amount = drow1["CPFor_Amount"],
                              DEFor_Amount = drow1["DEFor_Amount"],
                              CEFor_Amount = drow1["CEFor_Amount"]
                          }
                       );

            DataTable Exp_Dt = CustomControls.IEnumerableToDataTable(result);
            //-----------------------دمج Datagridview
            int K = 0;
            DataGridView GRD = new DataGridView();
            DataGridView[] Grid_array = { Grd_Cust, Grd_CurBalance };
            for (int i = 0; i < Grid_array.Count(); i++)
            {
                for (int j = 0; j < Grid_array[i].ColumnCount; j++)
                {
                    GRD.Columns.Add("Col" + K, Grid_array[i].Columns[j].HeaderText.Trim());
                    K++;
                }
            }
            Date();
            DataGridView[] Export_GRD = { Date_Grd, GRD };
            DataTable[] Export_DT = { Date_Tbl, Exp_Dt };

            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

        }
        //------------------------------------------ 
        private void Date()
        {

            //string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
            //string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
            //if (From_Date == "-1")
            //    From_Date = "0";
            //if (To_Date == "-1")
            //    To_Date = "0";

            Date_Grd = new DataGridView();
            Date_Tbl = new DataTable();
            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date", "To_Date" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            Date_Tbl.Rows.Add(new object[] { TxtFromDate.Text , TxtToDate.Text });
        }
    }
}