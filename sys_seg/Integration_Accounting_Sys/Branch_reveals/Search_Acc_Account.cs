﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Search_Acc_Account : Form
    {
        #region Defintion
        DataTable DT_Cur = new DataTable();
        DataTable DT_Cust = new DataTable();
        DataTable DT_Acc = new DataTable();
        DataTable DT_Cur_TBL = new DataTable();
        DataTable DT_Cust_TBL = new DataTable();
        DataTable DT_Acc_TBL = new DataTable();
        int Cur_id = 0;
        int Cust_id = 0;
        int Acc_Id = 0;
        string Filter = "";
        string Str_Id = "";
        string Con_Str = "";
        #endregion
        //---------------------------
        public Search_Acc_Account()
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cur.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Acc.AutoGenerateColumns = false;
            Create_Tbl();
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Cur_Id.Columns["Column2"].DataPropertyName = "Cur_Ename";
                Grd_Cur.Columns["Column5"].DataPropertyName = "Cur_Ename";
                Grd_Cust_Id.Columns["Column8"].DataPropertyName = "Ecust_name";
                Grd_Cust.Columns["Column11"].DataPropertyName = "Ecust_name";
                Grd_Acc_Id.Columns["Column17"].DataPropertyName = "Acc_Ename";
                Grd_Acc.Columns["Column16"].DataPropertyName = "Acc_Ename";
            }
            #endregion
            #endregion
        }
        //---------------------------
        private void Create_Tbl()
        {
            string[] Column = { "cur_id", "Cur_ANAME", "Cur_ENAME" };
            string[] DType = { "System.Int16", "System.String", "System.String" };
            DT_Cur = CustomControls.Custom_DataTable("DT_Cur", Column, DType);
            Grd_Cur.DataSource = DT_Cur;
            //-----------
            string[] Column1 = { "Cust_id", "ACUST_NAME", "ECUST_NAME" };
            string[] DType1 = { "System.Int16", "System.String", "System.String" };
            DT_Cust = CustomControls.Custom_DataTable("DT_Cust", Column1, DType1);
            Grd_Cust.DataSource = DT_Cust;
            //---------
            string[] Column2 = { "Acc_id", "Acc_ANAME", "Acc_Ename","Dot_Acc_No" };
            string[] DType2 = { "System.Int16", "System.String", "System.String", "System.String" };
            DT_Acc = CustomControls.Custom_DataTable("DT_Acc", Column2, DType2);
            Grd_Acc.DataSource = DT_Acc;
        }
        //-----------------------------------------------------
        private void Search_New_Load(object sender, EventArgs e)
        {
            connection.SqlExec("Exec Search_Acc_Account " + connection.T_ID, "Cust_Cur_Acc_Tbl");
            Cbo_Level.DataSource = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl3"];
            Cbo_Level.ValueMember = "Acc_Level";
            Cbo_Level.DisplayMember = connection.Lang_id == 1 ? "AAcc_Level" : "EAcc_Level";
            Cbo_Level.SelectedIndex = 0;
        }
        //-----------------------------------------------
        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Cust_id = 0;
                Cur_id = 0;
                Acc_Id = 0;
                 Str_Id = "";
                 //Con_Str = "";
                 Con_Str = "";
                 if (DT_Acc.Rows.Count > 0 && Convert.ToByte(Cbo_Level.SelectedValue) <= 0)
                 {
                     MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                     Con_Str = " And  Acc_Id  in(" + Str_Id + ")";
                 }
                 else if (DT_Acc.Rows.Count > 0 && Convert.ToByte(Cbo_Level.SelectedValue) > 0)
                 {
                     Str_Id = "";
                     string Acc_ID_Str = "";
                     foreach (DataRow Drow in DT_Acc.Rows)
                     {
                         DataTable Dt = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl"].Select("Dot_Acc_No like '" + Drow["Dot_Acc_No"] + "%'").CopyToDataTable();
                         MyGeneral_Lib.ColumnToString(Dt, "Acc_Id", out Str_Id);
                         Acc_ID_Str += Str_Id;
                     }
                     Con_Str = " And  Acc_Id  in(" + Acc_ID_Str + ")";
                 }

                 if (DT_Cur.Rows.Count > 0)
                 {
                     MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out Str_Id);
                     Con_Str += " And  Cur_ID in(" + Str_Id + ")";
                 }
                
                if (DT_Cust.Rows.Count > 0)
                {
                    Str_Id = "";
                    MyGeneral_Lib.ColumnToString(DT_Cust, "Cust_Id", out Str_Id);
                    Con_Str += " And  Cust_Id not in(" + Str_Id + ")";
                }
                int.TryParse(TxtCust_Name.Text, out Cust_id);

                Filter = " (Acust_Name like '" + TxtCust_Name.Text + "%' or  ECust_Name like '" + TxtCust_Name.Text + "%'"
                            + " Or Cust_id = " + Cust_id + ")" + Con_Str;
                # endregion
                DT_Cust_TBL = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl2"].Select(Filter).CopyToDataTable();
                DT_Cust_TBL = DT_Cust_TBL.DefaultView.ToTable(true, "Cust_id", "Acust_Name", "ECust_Name").Select().CopyToDataTable();
                Grd_Cust_Id.DataSource = DT_Cust_TBL;
            }

            catch
            {
                Grd_Cust_Id.DataSource = new DataTable();
            }
            if (Grd_Cust_Id.Rows.Count <= 0)
            {
                button9.Enabled = false;
                button10.Enabled = false;
            }
            else
            {
                button9.Enabled = true;
                button10.Enabled = true;
            }
        }
        //-----------------------------------------------
        private void Chk_Cust_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cust.Checked)
            {
                TxtCust_Name.Enabled = true;
                TxtCust_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cust.Clear();
                DT_Cust_TBL.Clear();
                TxtCust_Name.ResetText();
                button9.Enabled = false;
                button10.Enabled = false;
                button11.Enabled = false;
                button12.Enabled = false;
                TxtCust_Name.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cust.NewRow();

            row["Cust_id"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["Cust_Id"];
            row["ACUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ACust_name"];
            row["ECUST_NAME"] = DT_Cust_TBL.Rows[Grd_Cust_Id.CurrentRow.Index]["ECust_name"];
            DT_Cust.Rows.Add(row);
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);

            button11.Enabled = true;
            button12.Enabled = true;
            if (Grd_Cust_Id.Rows.Count == 0)
            {
                button9.Enabled = false;
                button10.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cust_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cust.NewRow();
                row["Cust_id"] = DT_Cust_TBL.Rows[i]["Cust_id"];
                row["ACUST_NAME"] = DT_Cust_TBL.Rows[i]["ACUST_NAME"];
                row["ECUST_NAME"] = DT_Cust_TBL.Rows[i]["ECUST_NAME"];
                DT_Cust.Rows.Add(row);
            }
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button9.Enabled = false;
            button10.Enabled = false;
            button11.Enabled = true;
            button12.Enabled = true;
        }
        //-----------------------------------------------
        private void button3_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows[Grd_Cust.CurrentRow.Index].Delete();
            TxtCust_Name.Text = "";
            TxtCust_Name_TextChanged(null, null);
            button9.Enabled = true;
            button10.Enabled = true;
            if (Grd_Cust.Rows.Count == 0)
            {
                button11.Enabled = false;
                button12.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button4_Click(object sender, EventArgs e)
        {
            DT_Cust.Rows.Clear();
            TxtCust_Name_TextChanged(null, null);
            button9.Enabled = true;
            button10.Enabled = true;
            button11.Enabled = false;
            button12.Enabled = false;
        }
        //-----------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        //-----------------------------------------------
        private void chk_Acc_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Acc.Checked)
            {
                TxtAcc_Name.Enabled = true;
                TxtAcc_Name_TextChanged(null, null);
            }
            else
            {
                DT_Acc.Clear();
                DT_Acc_TBL.Clear();
                TxtAcc_Name.ResetText();
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                TxtAcc_Name.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void TxtCur_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                #region Filter
                Acc_Id = 0;
                Cur_id = 0;
               Str_Id = "";
                 Con_Str = "";
                if (DT_Acc.Rows.Count > 0 && Convert.ToByte(Cbo_Level.SelectedValue) <= 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                    Con_Str = " And  Acc_Id  in(" + Str_Id + ")";
                }
                else if (DT_Acc.Rows.Count > 0 && Convert.ToByte(Cbo_Level.SelectedValue) > 0)
                {
                    Str_Id = "";
                    string Acc_ID_Str = "";
                    foreach (DataRow Drow in DT_Acc.Rows)
                    {
                        DataTable Dt = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl"].Select("Dot_Acc_No like '" + Drow["Dot_Acc_No"] + "%'").CopyToDataTable();
                        MyGeneral_Lib.ColumnToString(Dt, "Acc_Id", out Str_Id);
                        Acc_ID_Str += Str_Id + ",";
                    }
                    Con_Str = " And  ACC_Id  in(" + Acc_ID_Str + ")";
                }

                if (DT_Cur.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out Str_Id);
                    Con_Str += " And  Cur_ID not in(" + Str_Id + ")";
                }
                int.TryParse(TxtCur_Name.Text, out Cur_id);

                Filter = " (Cur_Aname like '" + TxtCur_Name.Text + "%' or  Cur_Ename like '" + TxtCur_Name.Text + "%'"
                            + " Or Cur_id = " + Cur_id + ") " + Con_Str;
                # endregion
                DT_Cur_TBL = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl1"].Select(Filter).CopyToDataTable();
                DT_Cur_TBL = DT_Cur_TBL.DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
                Grd_Cur_Id.DataSource = DT_Cur_TBL;
            }
            catch
            {
                Grd_Cur_Id.DataSource = new DataTable();
            }
            if (Grd_Cur_Id.Rows.Count <= 0)
            {
                button5.Enabled = false;
                button6.Enabled = false;
            }
            else
            {
                button5.Enabled = true;
                button6.Enabled = true;
            }
        }
        //-----------------------------------------------
        private void Chk_Cur_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Cur.Checked)
            {
                TxtCur_Name.Enabled = true;
                TxtCur_Name_TextChanged(null, null);
            }
            else
            {
                DT_Cur.Clear();
                DT_Cur_TBL.Clear();
                TxtCur_Name.ResetText();
                button5.Enabled = false;
                button6.Enabled = false;
                button7.Enabled = false;
                button8.Enabled = false;
                TxtCur_Name.Enabled = false;
            }
        }
        //-----------------------------------------------
        private void button5_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Cur.NewRow();

            row["Cur_id"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_Id"];
            row["Cur_aname"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_aname"];
            row["Cur_ename"] = DT_Cur_TBL.Rows[Grd_Cur_Id.CurrentRow.Index]["Cur_ename"];
            DT_Cur.Rows.Add(row);
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);

            button7.Enabled = true;
            button8.Enabled = true;
            if (Grd_Cur_Id.Rows.Count == 0)
            {
                button5.Enabled = false;
                button6.Enabled = false;
            }

            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button6_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Cur_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Cur.NewRow();
                row["Cur_id"] = DT_Cur_TBL.Rows[i]["Cur_id"];
                row["Cur_aname"] = DT_Cur_TBL.Rows[i]["Cur_aname"];
                row["Cur_ename"] = DT_Cur_TBL.Rows[i]["Cur_ename"];
                DT_Cur.Rows.Add(row);
            }
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = true;
            button8.Enabled = true;
            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button7_Click(object sender, EventArgs e)
        {
            DT_Cur.Rows[Grd_Cur.CurrentRow.Index].Delete();
            TxtCur_Name.Text = "";
            TxtCur_Name_TextChanged(null, null);
            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Cur.Rows.Count == 0)
            {
                button7.Enabled = false;
                button8.Enabled = false;
            }
            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button8_Click(object sender, EventArgs e)
        {
            DT_Cur.Rows.Clear();
            TxtCur_Name_TextChanged(null, null);
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = false;
            button8.Enabled = false;
            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void TxtAcc_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Acc_Id = 0;
                 Con_Str = "";
                 Str_Id = "";
                if (DT_Acc.Rows.Count > 0)
                {
                    MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out Str_Id);
                    Con_Str = " And Acc_Id not in(" + Str_Id + ")";
                }
                int.TryParse(TxtAcc_Name.Text, out Acc_Id);

               
                    Con_Str += " And Acc_Level = " + Cbo_Level.SelectedValue.ToString();
                   
                
                Filter = " (Acc_Aname like '" + TxtAcc_Name.Text + "%' or  Acc_Ename like '" + TxtAcc_Name.Text + "%'"
                            + " Or Acc_id = " + Acc_Id + ")" + Con_Str;
                DT_Acc_TBL = connection.SQLDS.Tables["Cust_Cur_Acc_Tbl"].DefaultView.ToTable(true, "Acc_Id", "Acc_Aname", "Acc_Ename", "Acc_Level","Dot_Acc_No").Select(Filter).CopyToDataTable();
                Grd_Acc_Id.DataSource = DT_Acc_TBL;

            }
            catch
            {
                Grd_Acc_Id.DataSource = new DataTable();
            }
            if (Grd_Acc_Id.Rows.Count <= 0)
            {

                button1.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
            }
        }
        //-----------------------------------------------
        private void button9_Click(object sender, EventArgs e)
        {
            DataRow row = DT_Acc.NewRow();

            row["Acc_id"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_Id"];
            row["Acc_aname"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_aname"];
            row["Acc_ename"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Acc_ename"];
            row["Dot_Acc_No"] = DT_Acc_TBL.Rows[Grd_Acc_Id.CurrentRow.Index]["Dot_Acc_No"];
            DT_Acc.Rows.Add(row);
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);

            button3.Enabled = true;
            button4.Enabled = true;
            if (Grd_Acc_Id.Rows.Count == 0)
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button10_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DT_Acc_TBL.Rows.Count; i++)
            {
                DataRow row = DT_Acc.NewRow();
                row["Acc_id"] = DT_Acc_TBL.Rows[i]["Acc_id"];
                row["Acc_aname"] = DT_Acc_TBL.Rows[i]["Acc_aname"];
                row["Acc_ename"] = DT_Acc_TBL.Rows[i]["Acc_ename"];
                row["Dot_Acc_No"] = DT_Acc_TBL.Rows[i]["Dot_Acc_No"];
                DT_Acc.Rows.Add(row);
            }
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Enabled = true;
            Chk_Cur_CheckedChanged(null, null);
            Chk_Cust_CheckedChanged(null, null);

        }
        //-----------------------------------------------
        private void button11_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows[Grd_Acc.CurrentRow.Index].Delete();
            TxtAcc_Name.Text = "";
            TxtAcc_Name_TextChanged(null, null);
            button5.Enabled = true;
            button6.Enabled = true;
            if (Grd_Acc.Rows.Count == 0)
            {
                button3.Enabled = false;
                button4.Enabled = false;
            }
            Chk_Cur_CheckedChanged(null, null);
            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void button12_Click(object sender, EventArgs e)
        {
            DT_Acc.Rows.Clear();
            TxtAcc_Name_TextChanged(null, null);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
            Chk_Cur_CheckedChanged(null, null);
            Chk_Cust_CheckedChanged(null, null);
        }
        //-----------------------------------------------
        private void Search_New_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Cust_Cur_Acc_Tbl", "Cust_Cur_Acc_Tbl1", "Cust_Cur_Acc_Tbl2", "Cust_Cur_Acc_Tbl3", "Reveal_Acc_Account_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            string From_Date = MyGeneral_Lib.DateChecking(TxtFromDate.Text);
            string To_Date = MyGeneral_Lib.DateChecking(TxtToDate.Text);
            string InRecDate = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);


            if (Convert.ToInt32(From_Date) > Convert.ToInt32(InRecDate))
            {
                MessageBox.Show(connection.Lang_id == 1 ? " تحقق من التأريخ " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                return;
            }
           
            if ((From_Date == "-1" || To_Date == "-1"))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تأكد من الفترة " : "Enter The Date Please ", MyGeneral_Lib.LblCap);

                return;
            }
            if (Convert.ToInt32(From_Date) > Convert.ToInt32(To_Date) && Convert.ToInt32(To_Date) != 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " تاريخ بداية الفترة اكبر من تاريخ النهاية " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            string Cust_IDStr = "";
            string Cur_IDStr = "";
            string ACC_IDStr = "";

            MyGeneral_Lib.ColumnToString(DT_Cust, "Cust_Id", out  Cust_IDStr);
            MyGeneral_Lib.ColumnToString(DT_Cur, "Cur_Id", out  Cur_IDStr);
            MyGeneral_Lib.ColumnToString(DT_Acc, "Acc_Id", out  ACC_IDStr);
            object[] Sparam = {Cust_IDStr,connection.T_ID.ToString(),
                                   Cur_IDStr,ACC_IDStr,From_Date,
                                  To_Date,1,Cbo_Level.SelectedValue,Convert.ToByte(CHK_OP_ACC.Checked),connection.user_id,""};
            AddBtn.Enabled = false;
            MyGeneral_Lib.Copytocliptext("Reveal_Acc_Account", Sparam);
            connection.SqlExec("Reveal_Acc_Account", "Reveal_Acc_Account_Tbl", Sparam);

            if (connection.Col_Name != "")
            {
                MessageBox.Show(connection.Col_Name, MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            Reveal_Acc_Main Frm = new Reveal_Acc_Main(TxtFromDate.Text, TxtToDate.Text);
            this.Visible = false;
            Frm.ShowDialog(this);
            this.Visible = true;
            AddBtn.Enabled = true;
        }
        //-------------------------------------------------------------------
        private void CboCust_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

            chk_Acc_CheckedChanged(sender, e);
            Chk_Cur_CheckedChanged(sender, e);
            Chk_Cust_CheckedChanged(sender, e);
            DT_Acc.Clear();
            DT_Cur.Clear();
            DT_Cust.Clear();
        }

        private void Search_Acc_Account_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}