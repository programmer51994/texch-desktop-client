﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class outlier_Rem : Form
    {
        #region Defintion

        string @format = "dd/MM/yyyy";
        DataTable DT_per = new DataTable();
        DataTable DT_per_TBL = new DataTable();
        DataTable DT_selected_per = new DataTable();
        DataTable DT_rem2 = new DataTable();
        DataTable DT_out = new DataTable();
        DataTable DT_per_info_Export = new DataTable();
        DataTable DT_Rem_Export = new DataTable();
        DataTable DT_Outlier_Export = new DataTable();

        BindingSource BS_names = new BindingSource();
        BindingSource BS_Cur = new BindingSource();
        

        BindingSource BS_Grid_persons = new BindingSource();
        BindingSource BS_Grid_Rem = new BindingSource();
        BindingSource BS_Grid_outlier = new BindingSource();
        
        bool type_change = false;
        bool cur_type_change = false;
        bool Change_grd = false;
        string per_name="";


        Int64 from_nrec_date = 0;
        Int64 to_nrec_date = 0;
        
        string Sql_Text = "";
        string Sql_Text1 = "";
        
        int per_id = 0;
        string Filter = "";

        string Rem_type = "";
        int curs_id = 0;
        int check = 0;

        //***
        string InOut_remType ="";
        //string all_remType = "";
        int oper_Type = 0;
        //***
        
     
        public static Int64 frm_date = 0;
        public static Int64 to_date = 0;
        public static Int64 t_id_new = 0;





        #endregion

         public outlier_Rem()
        {
            InitializeComponent(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            //Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);



            Create_Tbl();
            Grd_per_id.AutoGenerateColumns = false;
            Grd_per.AutoGenerateColumns = false;
            Grd_per_info.AutoGenerateColumns = false;
            Grd_Rem.AutoGenerateColumns = false;
            Grd_outlier.AutoGenerateColumns = false;

        
        }

         private void outlier_Rem_Load(object sender, EventArgs e)
         {
             OperType.SelectedIndex = 0;
             RemType.SelectedIndex = 0;
             RemNum.Text = "3";

              MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
             
             //graph_diagram.UseColumnTextForButtonValue = true;

             

             TxtFromDate.CustomFormat = "00/00/0000";
             TxtToDate.CustomFormat = "00/00/0000";
             if (connection.Lang_id == 2)
             {
                 RemType.Items[0] = "All";
                 RemType.Items[1] = "Online";
                 RemType.Items[2] = "Offline";

                 OperType.Items[0] = "Rem. In";
                 OperType.Items[1] = "Rem. Out";

                 //cbo_curType
                 cbo_curType.Items[0] = "Rate Currency";
                 cbo_curType.Items[1] = "Original Currency";
             }


            
               
       
                 Grd_per_info.Columns["Column3"].HeaderText = connection.Lang_id == 1 ? "اسم الشخص" : "Person Name";
                 Grd_per_info.Columns["Column4"].HeaderText = connection.Lang_id == 1 ? "عدد الحوالات" : "Rem. Number";
                 Grd_per_info.Columns["Column6"].HeaderText = connection.Lang_id == 1 ? "مجموع المبالغ" : "Amounts sum";
                 Grd_per_info.Columns["Column7"].HeaderText = connection.Lang_id == 1 ? "المعدل" : "Mean";
                 Grd_per_info.Columns["Column8"].HeaderText = connection.Lang_id == 1 ? "معدل الانحراف" : "Standard dev.";
                 Grd_per_info.Columns["min_rate"].HeaderText = connection.Lang_id == 1 ? "الحد الادنى" : "Mini rate";
                 Grd_per_info.Columns["max_rate"].HeaderText = connection.Lang_id == 1 ? "الحد الاعلى" : "Max rate";



                 Grd_Rem.Columns["dataGridViewTextBoxColumn1"].HeaderText = connection.Lang_id == 1 ? "رقم الحولة" : "rem no";
                 Grd_Rem.Columns["Column10"].HeaderText = connection.Lang_id == 1 ? "محور الحوالات" : "Rem Axsis";
                 //Column13
                 Grd_Rem.Columns["Column13"].HeaderText = connection.Lang_id == 1 ? "نوع العمليه" : "Oper. Type";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn3"].HeaderText = connection.Lang_id == 1 ? "مبلغ الحوالة" : "Rem Amount";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn4"].HeaderText = connection.Lang_id == 1 ? "عملة الحوالة" : "Rem Cur.";
                 Grd_Rem.Columns["Column14"].HeaderText = connection.Lang_id == 1 ? "المبلغ محلي" : "Local Amount";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn5"].HeaderText = connection.Lang_id == 1 ? "عملة التسليم" : "Deliver Currency";
                 //Grd_Rem.Columns["Column27"].HeaderText = connection.Lang_id == 1 ? "الجهة المصدرة" : "Customer Name";
                 Grd_Rem.Columns["Column9"].HeaderText = connection.Lang_id == 1 ? "مدينة الارسال" : "Sender City";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn6"].HeaderText = connection.Lang_id == 1 ? "مدينة الاستلام" : "Deliver City";
                 Grd_Rem.Columns["Column19"].HeaderText = connection.Lang_id == 1 ? "المستخدم" : "Users";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn7"].HeaderText = connection.Lang_id == 1 ? "تاريخ الحالة" : "Case Date";
                 Grd_Rem.Columns["Column11"].HeaderText = connection.Lang_id == 1 ? "تاريخ تنظيم السجلات" : "Organize Date records";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn8"].HeaderText = connection.Lang_id == 1 ? "اسم المرسل" : "Sender Name";
                 Grd_Rem.Columns["Column12"].HeaderText = connection.Lang_id == 1 ? "هاتف المرسل" : "Sender Phone";
                 Grd_Rem.Columns["Column16"].HeaderText = connection.Lang_id == 1 ? "عنوان المرسل" : "Sender Address";
                 Grd_Rem.Columns["Column17"].HeaderText = connection.Lang_id == 1 ? "الزقاق" : "Street";
                 Grd_Rem.Columns["Column18"].HeaderText = connection.Lang_id == 1 ? "الحي" : "Sender Subrub";
                 Grd_Rem.Columns["Column20"].HeaderText = connection.Lang_id == 1 ? "الرقم البريدي" : "Sender Post Code";
                 Grd_Rem.Columns["Column21"].HeaderText = connection.Lang_id == 1 ? "المحافظة" : "Sender State";
                 Grd_Rem.Columns["Column28"].HeaderText = connection.Lang_id == 1 ? "الرقم الوطني للمرسل" : "SIN ID sender";
                 Grd_Rem.Columns["dataGridViewCheckBoxColumn4"].HeaderText = connection.Lang_id == 1 ? "نوع وثيقة المرسل" : "S. Doc. type";
                 Grd_Rem.Columns["Column22"].HeaderText = connection.Lang_id == 1 ? "رقم وثيقة المرسل" : "Sender Doc No.";
                 Grd_Rem.Columns["Column23"].HeaderText = connection.Lang_id == 1 ? "تاريخ اصدار وثيقة المرسل" : "Sender Doc  Date";
                 Grd_Rem.Columns["Column24"].HeaderText = connection.Lang_id == 1 ? "تاريخ انتهاء وثيقة المرسل" : "Sender End Doc. Date";
                 Grd_Rem.Columns["Column25"].HeaderText = connection.Lang_id == 1 ? "اصدار وثيقة المرسل" : "Sender Doc Issue";
                 Grd_Rem.Columns["Column26"].HeaderText = connection.Lang_id == 1 ? "ايميل المرسل" : "Sender Email";
                 Grd_Rem.Columns["Column29"].HeaderText = connection.Lang_id == 1 ? "جنسية المرسل" : "Sender Nationalty";
                 Grd_Rem.Columns["Column30"].HeaderText = connection.Lang_id == 1 ? "ملاحظات المرسل" : "Sender Notes";
                 Grd_Rem.Columns["Column31"].HeaderText = connection.Lang_id == 1 ? "اسم المستلم" : "Receiver Name";
                 Grd_Rem.Columns["Column32"].HeaderText = connection.Lang_id == 1 ? "هاتف المستلم" : "Receiver phone";
                 Grd_Rem.Columns["Column33"].HeaderText = connection.Lang_id == 1 ? "مدينة المستلم" : "Receiver City";
                 Grd_Rem.Columns["Column34"].HeaderText = connection.Lang_id == 1 ? "بلد المستلم" : "Receiver Country";
                 Grd_Rem.Columns["Column36"].HeaderText = connection.Lang_id == 1 ? "عنوان المستلم" : "Receiver  Address";
                 Grd_Rem.Columns["Column37"].HeaderText = connection.Lang_id == 1 ? "الزقاق" : "Receiver  street";
                 Grd_Rem.Columns["Column38"].HeaderText = connection.Lang_id == 1 ? "الحي للمستلم" : "Receiver  Suburb";
                 Grd_Rem.Columns["Column39"].HeaderText = connection.Lang_id == 1 ? "الرمز البريدي للمستلم" : "Receiver Post Code";
                 Grd_Rem.Columns["Column35"].HeaderText = connection.Lang_id == 1 ? "محافظة المستلم" : "Reciver's city";
                 Grd_Rem.Columns["Column40"].HeaderText = connection.Lang_id == 1 ? "الرقم الوطني للمستلم" : "SIN ID receiver";
                 Grd_Rem.Columns["dataGridViewTextBoxColumn13"].HeaderText = connection.Lang_id == 1 ? "نوع وثيقة المستلم" : "R. Doc .Type";
                 Grd_Rem.Columns["Column42"].HeaderText = connection.Lang_id == 1 ? "تاريخ اصدار الوثيقة المستلم" : "Doc. issue date";
                 Grd_Rem.Columns["Column43"].HeaderText = connection.Lang_id == 1 ? "تاريخ انتهاء الوثيقة المستلم" : "Doc. issue ex. date";
                 Grd_Rem.Columns["Column44"].HeaderText = connection.Lang_id == 1 ? "اصدار وثيقة المستلم" : "R. Doc Issue";
                 Grd_Rem.Columns["Column45"].HeaderText = connection.Lang_id == 1 ? "ايميل المستلم" : "Receiver E_Mail";
                 Grd_Rem.Columns["Column48"].HeaderText = connection.Lang_id == 1 ? "غرض الارسال" : "Sending purpose";
                 Grd_Rem.Columns["Column49"].HeaderText = connection.Lang_id == 1 ? "ملاحظات المستلم" : "Receiver notes";
                 //
                 Grd_Rem.Columns["Column52"].HeaderText = connection.Lang_id == 1 ? "رقم السند" : "voucher number";
                 Grd_Rem.Columns["Column53"].HeaderText = connection.Lang_id == 1 ? "تاريخ السجلات" : "In records date";
                 Grd_Rem.Columns["Column57"].HeaderText = connection.Lang_id == 1 ? "تولد المرسل" : "Sender Birht date";
                 Grd_Rem.Columns["Column58"].HeaderText = connection.Lang_id == 1 ? "تولد المستلم" : "Receiver birthdate";
                 Grd_Rem.Columns["Column59"].HeaderText = connection.Lang_id == 1 ? "عمولة المرسل" : "sender commission";
                 Grd_Rem.Columns["Column60"].HeaderText = connection.Lang_id == 1 ? "عملة المرسل" : "Sending currency";
                 Grd_Rem.Columns["Column61"].HeaderText = connection.Lang_id == 1 ? "مبلغ العمولة" : "Commission amount";
                 Grd_Rem.Columns["Column62"].HeaderText = connection.Lang_id == 1 ? "عملة العمولة" : "Commission currency";
                 Grd_Rem.Columns["Column71"].HeaderText = connection.Lang_id == 1 ? "سعر التعادل" : "Exchange rate";
                 Grd_Rem.Columns["Column72"].HeaderText = connection.Lang_id == 1 ? "مكان تولد المستلم" : "Reciver birth place";
                 Grd_Rem.Columns["Column75"].HeaderText = connection.Lang_id == 1 ? "مصدر المال" : "Money source";
                 Grd_Rem.Columns["Column76"].HeaderText = connection.Lang_id == 1 ? "علاقة المرسل بالمستلم" : "Sender/Receiver relationship";
                 Grd_Rem.Columns["Column50"].HeaderText = connection.Lang_id == 1 ? "غرض المستلم" : " Purpose";
                 Grd_Rem.Columns["Column51"].HeaderText = connection.Lang_id == 1 ? "علاقة المستلم بالمرسل" : "Receiver/Sender relationship";

                 Grd_Rem.Columns["Column54"].HeaderText = connection.Lang_id == 1 ? "مهنة المرسل" : "Sender  job";
                 Grd_Rem.Columns["Column55"].HeaderText = connection.Lang_id == 1 ? "تفاصيل مهنة المرسل" : "details sender job";
                 Grd_Rem.Columns["Column56"].HeaderText = connection.Lang_id == 1 ? "مهنة المستلم" : "Receiver Job";
                 Grd_Rem.Columns["Column63"].HeaderText = connection.Lang_id == 1 ? "تفاصيل مهنة المستلم" : "details receiver  job";
                 Grd_Rem.Columns["Column70"].HeaderText = connection.Lang_id == 1 ? "الاسم كما في الهوية" : "Real Paid Name";
                 Grd_Rem.Columns["Column73"].HeaderText = connection.Lang_id == 1 ? "اسم الوكيل المصدر" : "Sender Agent Name";
                 Grd_Rem.Columns["Column74"].HeaderText = connection.Lang_id == 1 ? "اسم الوكيل الدافع" : "Receiver Agent Name";
                 Grd_Rem.Columns["Column80"].HeaderText = connection.Lang_id == 1 ? "تاريخ انشاء الحوالة في مركز الاونلاين" : "The date of creation of rem in online";
                 Grd_Rem.Columns["Column41"].HeaderText = connection.Lang_id == 1 ? "رقم وثيقة المستلم" : "R Doc. Num.";
                 //Grd_Rem.Columns["Column15"].HeaderText = connection.Lang_id == 1 ? "الرقم السري" : "Code Rem";
                 //  Grd_Rem.Columns["Column45"].HeaderText = connection.Lang_id == 1 ? "ايميل المستلم" : "S. deviation";


                 //-----------------------
                 Grd_outlier.Columns["dataGridViewTextBoxColumn9"].HeaderText = connection.Lang_id == 1 ? "رقم الحولة" : "rem no";
                 Grd_outlier.Columns["Column47"].HeaderText = connection.Lang_id == 1 ? "محور الحوالات" : "Rem Axsis";
                 //Column13
                 Grd_outlier.Columns["dataGridViewTextBoxColumn11"].HeaderText = connection.Lang_id == 1 ? "نوع العمليه" : "Oper. Type";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn14"].HeaderText = connection.Lang_id == 1 ? "مبلغ الحوالة" : "Rem Amount";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn15"].HeaderText = connection.Lang_id == 1 ? "عملة الحوالة" : "Rem Cur.";
                 Grd_outlier.Columns["Column46"].HeaderText = connection.Lang_id == 1 ? "المبلغ محلي" : "Local Amount";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn16"].HeaderText = connection.Lang_id == 1 ? "عملة التسليم" : "Deliver Currency";
                 //Grd_outlier.Columns["dataGridViewTextBoxColumn17"].HeaderText = connection.Lang_id == 1 ? "الجهة المصدرة" : "Customer Name";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn18"].HeaderText = connection.Lang_id == 1 ? "مدينة الارسال" : "Sender City";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn19"].HeaderText = connection.Lang_id == 1 ? "مدينة الاستلام" : "Deliver City";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn20"].HeaderText = connection.Lang_id == 1 ? "المستخدم" : "Users";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn21"].HeaderText = connection.Lang_id == 1 ? "تاريخ الحالة" : "Case Date";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn22"].HeaderText = connection.Lang_id == 1 ? "تاريخ تنظيم السجلات" : "Organize Date records";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn23"].HeaderText = connection.Lang_id == 1 ? "اسم المرسل" : "Sender Name";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn24"].HeaderText = connection.Lang_id == 1 ? "هاتف المرسل" : "Sender Phone";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn25"].HeaderText = connection.Lang_id == 1 ? "عنوان المرسل" : "Sender Address";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn26"].HeaderText = connection.Lang_id == 1 ? "الزقاق" : "Street";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn27"].HeaderText = connection.Lang_id == 1 ? "الحي" : "Sender Subrub";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn28"].HeaderText = connection.Lang_id == 1 ? "الرقم البريدي" : "Sender Post Code";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn29"].HeaderText = connection.Lang_id == 1 ? "المحافظة" : "Sender State";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn30"].HeaderText = connection.Lang_id == 1 ? "الرقم الوطني للمرسل" : "SIN ID sender";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn31"].HeaderText = connection.Lang_id == 1 ? "نوع وثيقة المرسل" : "S. Doc. type";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn32"].HeaderText = connection.Lang_id == 1 ? "رقم وثيقة المرسل" : "Sender Doc No.";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn33"].HeaderText = connection.Lang_id == 1 ? "تاريخ اصدار وثيقة المرسل" : "Sender Doc  Date";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn34"].HeaderText = connection.Lang_id == 1 ? "تاريخ انتهاء وثيقة المرسل" : "Sender End Doc. Date";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn35"].HeaderText = connection.Lang_id == 1 ? "اصدار وثيقة المرسل" : "Sender Doc Issue";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn36"].HeaderText = connection.Lang_id == 1 ? "ايميل المرسل" : "Sender Email";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn37"].HeaderText = connection.Lang_id == 1 ? "جنسية المرسل" : "Sender Nationalty";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn38"].HeaderText = connection.Lang_id == 1 ? "ملاحظات المرسل" : "Sender Notes";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn39"].HeaderText = connection.Lang_id == 1 ? "اسم المستلم" : "Receiver Name";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn40"].HeaderText = connection.Lang_id == 1 ? "هاتف المستلم" : "Receiver phone";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn41"].HeaderText = connection.Lang_id == 1 ? "مدينة المستلم" : "Receiver City";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn42"].HeaderText = connection.Lang_id == 1 ? "بلد المستلم" : "Receiver Country";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn43"].HeaderText = connection.Lang_id == 1 ? "عنوان المستلم" : "Receiver  Address";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn44"].HeaderText = connection.Lang_id == 1 ? "الزقاق" : "Receiver  street";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn45"].HeaderText = connection.Lang_id == 1 ? "الحي للمستلم" : "Receiver  Suburb";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn46"].HeaderText = connection.Lang_id == 1 ? "الرمز البريدي للمستلم" : "Receiver Post Code";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn47"].HeaderText = connection.Lang_id == 1 ? "محافظة المستلم" : "Reciver's city";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn48"].HeaderText = connection.Lang_id == 1 ? "الرقم الوطني للمستلم" : "SIN ID receiver";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn49"].HeaderText = connection.Lang_id == 1 ? "نوع وثيقة المستلم" : "R. Doc .Type";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn50"].HeaderText = connection.Lang_id == 1 ? "رقـم وثيقـة المسلم" : "R. Doc. Num";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn51"].HeaderText = connection.Lang_id == 1 ? "تاريخ اصدار وثيقة المستلم" : "Doc. issue date";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn52"].HeaderText = connection.Lang_id == 1 ? "تاريخ انتهاء وثيقة المستلم" : "R. Doc Issue Exp.";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn53"].HeaderText = connection.Lang_id == 1 ? "اصدار وثيقة المستلم" : "R. Doc Issue";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn54"].HeaderText = connection.Lang_id == 1 ? "ايميل المستلم" : "Receiver E_Mail";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn55"].HeaderText = connection.Lang_id == 1 ? "غرض الارسال" : "Sending purpose";
                 //
                 Grd_outlier.Columns["dataGridViewTextBoxColumn56"].HeaderText = connection.Lang_id == 1 ? "ملاحظات المستلم" : "Receiver notes";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn57"].HeaderText = connection.Lang_id == 1 ? "رقم السند" : "voucher number";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn58"].HeaderText = connection.Lang_id == 1 ? "تاريخ السجلات" : "In records date";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn59"].HeaderText = connection.Lang_id == 1 ? "تولد المرسل" : "Sender Birht date";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn60"].HeaderText = connection.Lang_id == 1 ? "تولد المستلم" : "Receiver birthdate";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn61"].HeaderText = connection.Lang_id == 1 ? "عمولة المرسل" : "sender commission";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn62"].HeaderText = connection.Lang_id == 1 ? "عملة المرسل" : "Sending currency";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn63"].HeaderText = connection.Lang_id == 1 ? "مبلغ العمولة" : "Commission amount";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn64"].HeaderText = connection.Lang_id == 1 ? "عملة العمولة" : "Commission currency";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn65"].HeaderText = connection.Lang_id == 1 ? "سعر التعادل" : "Exchange rate";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn66"].HeaderText = connection.Lang_id == 1 ? "مكان تولد المستلم" : "Reciver birth place";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn67"].HeaderText = connection.Lang_id == 1 ? "مصدر المال" : "Money source";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn68"].HeaderText = connection.Lang_id == 1 ? "علاقة المرسل بالمستلم" : "Sender/Receiver relationship";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn69"].HeaderText = connection.Lang_id == 1 ? "غرض المستلم" : " Purpose";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn70"].HeaderText = connection.Lang_id == 1 ? "علاقة المستلم بالمرسل" : "Receiver/Sender relationship";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn71"].HeaderText = connection.Lang_id == 1 ? "مهنة المرسل" : "Sender  job";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn72"].HeaderText = connection.Lang_id == 1 ? "تفاصيل مهنة المرسل" : "details sender job";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn73"].HeaderText = connection.Lang_id == 1 ? "مهنة المستلم" : "Receiver Job";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn74"].HeaderText = connection.Lang_id == 1 ? "تفاصيل مهنة المستلم" : "details receiver  job";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn75"].HeaderText = connection.Lang_id == 1 ? "الاسم كما في الهوية" : "Real Paid Name";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn76"].HeaderText = connection.Lang_id == 1 ? "اسم الوكيل المصدر" : "Sender Agent Name";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn77"].HeaderText = connection.Lang_id == 1 ? "اسم الوكيل الدافع" : "Receiver Agent Name";
                 Grd_outlier.Columns["dataGridViewTextBoxColumn78"].HeaderText = connection.Lang_id == 1 ? "تاريخ انشاء الحوالة في مركز الاونلاين" : "The date of creation of rem in online";
                 //Grd_outlier.Columns["dataGridViewTextBoxColumn79"].HeaderText = connection.Lang_id == 1 ? "الرقم السري" : "Code Rem";


                 if (connection.Lang_id == 2)
                 {

                 //---------Engilish dataproperty
                 //Normal Rem:

                 Column13.DataPropertyName = "ECASE_NA";
                 dataGridViewTextBoxColumn4.DataPropertyName = "R_ECUR_NAME";
                 dataGridViewTextBoxColumn5.DataPropertyName = "PR_ECUR_NAME";
                 ////Column27.DataPropertyName = "customer_Ename";
                 Column9.DataPropertyName = "S_ECITY_NAME";
                 dataGridViewTextBoxColumn6.DataPropertyName = "t_ECITY_NAME";
                 dataGridViewCheckBoxColumn4.DataPropertyName = "sFRM_EDOC_NA";
                 Column29.DataPropertyName = "s_E_NAT_NAME";
                 Column33.DataPropertyName = "r_ECITY_NAME";
                 Column34.DataPropertyName = "r_ECOUN_NAME";
                 dataGridViewTextBoxColumn13.DataPropertyName = "rFRM_EDOC_NA";
                 Column60.DataPropertyName = "s_ECUR_NAME";
                 Column62.DataPropertyName = "c_ECUR_NAME";
               //  Column73.DataPropertyName = "S_Ecust_name";
                // Column74.DataPropertyName = "D_Ecust_name ";

                 //Outlier Rem. :
                 dataGridViewTextBoxColumn11.DataPropertyName = "ECASE_NA";
                 dataGridViewTextBoxColumn15.DataPropertyName = "R_ECUR_NAME";
                 dataGridViewTextBoxColumn16.DataPropertyName = "PR_ECUR_NAME";
                 //dataGridViewTextBoxColumn17.DataPropertyName = "customer_Ename";
                 dataGridViewTextBoxColumn18.DataPropertyName = "S_ECITY_NAME";
                 dataGridViewTextBoxColumn19.DataPropertyName = "t_ECITY_NAME";
                 dataGridViewTextBoxColumn31.DataPropertyName = "sFRM_EDOC_NA";
                 dataGridViewTextBoxColumn37.DataPropertyName = "s_E_NAT_NAME";
                 dataGridViewTextBoxColumn41.DataPropertyName = "r_ECITY_NAME";
                 dataGridViewTextBoxColumn42.DataPropertyName = "r_ECOUN_NAME";
                 dataGridViewTextBoxColumn49.DataPropertyName = "rFRM_EDOC_NA";
                 dataGridViewTextBoxColumn62.DataPropertyName = "s_ECUR_NAME";
                 dataGridViewTextBoxColumn64.DataPropertyName = "c_ECUR_NAME";
                 //Column73.DataPropertyName = "S_Ecust_name";
                // Column74.DataPropertyName = "D_Ecust_name ";
                
             }



             //??
             type_change = false;
             OperType.SelectedIndex = 0;
             //OperType_SelectedIndexChanged(null, null);
             //type_change = true;
             

             cur_type_change = false;
             cbo_curType.SelectedIndex = 0;
             cbo_curType_SelectedIndexChanged(null, null);
             cur_type_change = true;

             Cbo_curs.Visible = false;
             label8.Visible = false;

             Change_grd = false;
             

         }
         private void Create_Tbl()
         {
             
             string[] Column1 = { "Per_Name"};
             string[] DType1 = { "System.String"};
             DT_per = CustomControls.Custom_DataTable("DT_per", Column1, DType1);
             Grd_per.DataSource = DT_per;
            


         }

         private void getdata()
         {

             #region text_time



             if (TxtFromDate.Checked == true)
             {

                 DateTime date = TxtFromDate.Value.Date;
                 from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                 //from_nrec_date = TxtFromDate.Value.ToString("yyyy/MM/dd");

             }
             else
             {
                 from_nrec_date = 0;
             }
             //-------------------------
             if (TxtToDate.Checked == true)
             {


                 DateTime date = TxtToDate.Value.Date;
                 to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                 //  to_nrec_date = TxtToDate.Value.ToString("yyyy/MM/dd");
             }
             else
             {
                 to_nrec_date = 0;
             }
             //---------------------------------
        
             //-----------------------------------------------------

           
             //--------------------------

            

             #endregion

             
         }

    

     
         private void OperType_SelectedIndexChanged(object sender, EventArgs e)
         {

             if (type_change==true)
             {
                 //***
              
                 Grd_per_info.DataSource = new DataTable();
                 Grd_Rem.DataSource = new DataTable();
                 Grd_outlier.DataSource = new DataTable();
           
                // Names.Text = "";

                 if (Grd_per_id.Rows.Count > 0)
                 {

                     move_one_btn.Enabled = true;
                     move_all_btn.Enabled = true;
                     remove_one_btn.Enabled = false;
                     remove_all_btn.Enabled=false;
                     
                 }

                 if (Grd_per.Rows.Count > 0)
                 {
                   
                     remove_one_btn.Enabled =true;
                     remove_all_btn.Enabled = true;
                 }
                 //******
                 
             }

             if (connection.SQLDS.Tables.Contains("matching_personstbl"))
             { connection.SQLDS.Tables.Remove("matching_personstbl"); }

             Grd_per_id.DataSource = new DataTable();
            // Grd_per.DataSource = new DataTable();

             if (DT_per.Rows.Count > 0)
             { DT_per.Clear(); }


             if (DT_per_TBL.Rows.Count > 0)
             { DT_per_TBL.Clear(); }
             Names.Text = "";
         

            

                 if (OperType.SelectedIndex == 1)
                 {
                     if (connection.Lang_id == 1)
                     {
                         label27.Text = "اسم المـــرسل: ";
                     }
                     else
                     {
                         label27.Text = "S. Name";
                     }
                    
                 }
                 if (OperType.SelectedIndex == 0)
                 {
                     if(connection.Lang_id == 1)
                     {
                         label27.Text = "اسم المســـتلم: ";
                     }
                     else
                     {
                         label27.Text = "R. Name";
                     }
               
                 }
                 type_change = false;

                // outlier_Rem_Load(null, null);
                 
                       

         }

         private void button5_Click(object sender, EventArgs e)
         {
             Change_grd = false;
             //type_change = false;

             Grd_per_info.DataSource = new DataTable();
             Grd_Rem.DataSource = new DataTable();
             Grd_outlier.DataSource = new DataTable();

             string[] comm_Tbl = { "outlier_remittance_detection_Tbl", "outlier_remittance_detection_Tbl1", "outlier_remittance_detection_Tbl2" };

             foreach (string Tbl in comm_Tbl)
             {
                 if (connection.SQLDS.Tables.Contains(Tbl))
                 {
                     connection.SQLDS.Tables.Remove(Tbl);
                 }
             } 

           

             

             getdata();
             if (Convert.ToDecimal(RemNum.Text.Trim())<3)
             {
                 MessageBox.Show(connection.Lang_id == 1 ? "يجب ان لايقل عدد الحوالات عن 3 حوالات " : "The minimum number of Rem. is 3 ", MyGeneral_Lib.LblCap);
                 return;
             }

             if (Amount.Text == null)
             {
                 MessageBox.Show(connection.Lang_id == 1 ? "يجب تحديد مجموع المبالغ " : "Enter Rem Amount", MyGeneral_Lib.LblCap);
                 return;
             }

           

             //____.
                  if (TxtFromDate.Checked == false)
             {

               MessageBox.Show(connection.Lang_id == 1 ? "يرجى تأكيد الحد الادنى من التاريخ " : "SELECT THE MINIMUM DATE", MyGeneral_Lib.LblCap);
                 return;

             }
             
             //-------------------------
             if (TxtToDate.Checked == false)
             {
                 MessageBox.Show(connection.Lang_id == 1 ? "يرجى تأكيد الحد الاعلى من التاريخ " : "SELECT THE MAXIMUM DATE", MyGeneral_Lib.LblCap);
                 return;

             }
             if (from_nrec_date > to_nrec_date)
             {
                 MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره الادنى اصغر من تاريخ الفتره الاعلى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                 return;
             }
             if (from_nrec_date == to_nrec_date)
             {
                 MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفترهالادنى اصغر من تاريخ الفتره الاعلى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                 return;
             }
                 //ـــ.

        

             


//             @date_from			int,
//@date_to			int,
//@rem_count			int,
//@rem_amount			int,
//@rem_type_id		tinyint,
//@rem_oper_id		tinyint,
//@rem_cur_type_id	tinyint,
//@rem_cur_id			smallint,
//@rem_return			tinyint,
//@per_name_tbl		per_name_tbl_data_type readonly	
             if (DT_per.Rows.Count > 0)
             {
                 DT_selected_per = DT_per;
             }

           

             if (RemType.SelectedIndex == 0)
             {
                 InOut_remType = "1,11";
             }
             if (RemType.SelectedIndex == 1)
             {
                 InOut_remType ="1";
             }

             if (RemType.SelectedIndex == 2)
             {
                 InOut_remType = "11";
             }

             if (OperType.SelectedIndex == 0)
             {
                 oper_Type = 1;
             }

             if (OperType.SelectedIndex == 1)
             {
                 oper_Type = 2;
             }

             if (CHK1.Checked == false)
             {
                 check = 0;
             }
             if (CHK1.Checked == true)
             {
                 check = 1;
             }

           

                         //try{

            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "outlier_remittance_detection_procedure";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.CommandTimeout = 0;
            connection.SQLCMD.Parameters.AddWithValue("@date_from", from_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@date_to", to_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@rem_count", Convert.ToDecimal(RemNum.Text.Trim()));
            if (Amount.Text =="")
            {
                connection.SQLCMD.Parameters.AddWithValue("@rem_amount", 0);
            }
            else
            connection.SQLCMD.Parameters.AddWithValue("@rem_amount", Convert.ToDecimal(Amount.Text.Trim()));
            connection.SQLCMD.Parameters.AddWithValue("@rem_type_id", InOut_remType + ",");
            connection.SQLCMD.Parameters.AddWithValue("@rem_oper_id", oper_Type );
            connection.SQLCMD.Parameters.AddWithValue("@rem_cur_type_id", cbo_curType.SelectedIndex);
             if (cbo_curType.SelectedIndex == 0)
            connection.SQLCMD.Parameters.AddWithValue("@rem_cur_id", 0);
             else
             connection.SQLCMD.Parameters.AddWithValue("@rem_cur_id", Cbo_curs.SelectedValue);
            //connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            
            connection.SQLCMD.Parameters.AddWithValue("@rem_return", check);
            //connection.SQLCMD.Parameters.AddWithValue("@t_id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@per_name_tbl",DT_per);
            

            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "outlier_remittance_detection_Tbl");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "outlier_remittance_detection_Tbl1");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "outlier_remittance_detection_Tbl2");
            
            obj.Close();
            connection.SQLCS.Close();

            
                connection.SQLCMD.Parameters.Clear();
      

                 BS_Grid_persons.DataSource = connection.SQLDS.Tables["outlier_remittance_detection_tbl"];
                 Grd_per_info.DataSource = BS_Grid_persons;

                 BS_Grid_Rem.DataSource = connection.SQLDS.Tables["outlier_remittance_detection_tbl1"];
                 Grd_Rem.DataSource = BS_Grid_Rem;

                 BS_Grid_outlier.DataSource = connection.SQLDS.Tables["outlier_remittance_detection_tbl2"];
                 Grd_outlier.DataSource = BS_Grid_outlier;

               
           //  }


             Change_grd = true;
             Grd_per_info_SelectionChanged(null, null);

             type_change = true;
             //OperType_SelectedIndexChanged(null, null);

                if (connection.SQLDS.Tables["outlier_remittance_detection_tbl"].Rows.Count <= 0)
             {
                 MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط " : "No data For this cases", MyGeneral_Lib.LblCap);
                 return;
             }
     
         }

         private void TxtFromDate_ValueChanged(object sender, EventArgs e)
         {
             if (TxtFromDate.Checked == true)
             {
                 TxtFromDate.Format = DateTimePickerFormat.Custom;
                 TxtFromDate.CustomFormat = @format;
             }
             else
             {
                 TxtFromDate.Format = DateTimePickerFormat.Custom;
                 TxtFromDate.CustomFormat = "00/00/0000";
             }
         }

         private void TxtToDate_ValueChanged(object sender, EventArgs e)
         {
             if (TxtToDate.Checked == true)
             {
                 TxtToDate.Format = DateTimePickerFormat.Custom;
                 TxtToDate.CustomFormat = @format;
             }
             else
             {
                 TxtToDate.Format = DateTimePickerFormat.Custom;
                 TxtToDate.CustomFormat = "00/00/0000";
             }
         }

         private void outlier_Rem_FormClosing(object sender, FormClosingEventArgs e)
         {
             string[] Used_Tbl = { "matching_personstbl", "cur_for_outlier_remtbl","outlier_remittance_detection_tbl","outlier_remittance_detection_tbl1","outlier_remittance_detection_tbl2"};
             foreach (string Tbl in Used_Tbl)
             {
                 if (connection.SQLDS.Tables.Contains(Tbl))
                 {
                     connection.SQLDS.Tables.Remove(Tbl);
                 }
                 type_change = false;
                 Change_grd = false;
             }
         }

         private void cbo_curType_SelectedIndexChanged(object sender, EventArgs e)
         {
             //***
             Grd_per_info.DataSource = new DataTable();
             Grd_Rem.DataSource = new DataTable();
             Grd_outlier.DataSource = new DataTable();

             // Names.Text = "";

             if (Grd_per_id.Rows.Count > 0)
             {

                 move_one_btn.Enabled = true;
                 move_all_btn.Enabled = true;
                 remove_one_btn.Enabled = false;
                 remove_all_btn.Enabled = false;

             }

             if (Grd_per.Rows.Count > 0)
             {

                 remove_one_btn.Enabled = true;
                 remove_all_btn.Enabled = true;
             }
             //******


             if (cur_type_change == true)
             {
                 if (cbo_curType.SelectedIndex == 1)
                 {
                     Cbo_curs.Visible = true;
                     label8.Visible = true;
                     BS_Cur.DataSource = connection.SqlExec("EXec cur_for_outlier_rem " + cbo_curType.SelectedIndex + "", "cur_for_outlier_remtbl");
                     Cbo_curs.DataSource = BS_Cur;
                     Cbo_curs.DisplayMember = connection.Lang_id == 1 ? "cur_ANAME" : "cur_ENAME";
                     Cbo_curs.ValueMember = "cur_ID";
                 }
                 if (cbo_curType.SelectedIndex == 0)
                 {
                     //BS_Cur.DataSource = connection.SqlExec("EXec cur_for_outlier_rem " + OperType.SelectedIndex + "", "cur_for_outlier_remtbl");
                     Cbo_curs.Visible = false;
                     label8.Visible = false;

                 }
             }

         }

         private void move_one_btn_Click(object sender, EventArgs e)
         {
           

                  DataRow row = DT_per.NewRow();

            //row["Per_ID"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_ID"];
            row["Per_Name"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_Name"];
            //row["Per_EName"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_EName"];
            DT_per.Rows.Add(row);
            //Names.Text = "";
            //Names_TextChanged(null, null);

            remove_one_btn.Enabled = true;
            remove_all_btn.Enabled = true;
            if (Grd_per_id.Rows.Count == 0)
            {
                move_one_btn.Enabled = false;
                move_all_btn.Enabled = false;
            }

         }

         private void move_all_btn_Click(object sender, EventArgs e)
         {
             //Grd_per.DataSource = DT_per_TBL;

             for (int i = 0; i < DT_per_TBL.Rows.Count; i++)
             {
                 DataRow row = DT_per.NewRow();
                 //row["Per_ID"] = DT_per_TBL.Rows[i]["Per_ID"];
                 row["Per_Name"] = DT_per_TBL.Rows[i]["Per_Name"];
                 //row["Per_EName"] = DT_per_TBL.Rows[i]["Per_EName"];
                 DT_per.Rows.Add(row);
             }
            // Names.Text = "";
             //Names_TextChanged(null, null);
          
           //  DT_per_TBL.Rows.Clear();
             move_one_btn.Enabled = false;
             move_all_btn.Enabled = false;
             remove_one_btn.Enabled = true;
             remove_all_btn.Enabled = true;
         }

         private void remove_one_btn_Click(object sender, EventArgs e)
         {
            
             DT_per.Rows[Grd_per.CurrentRow.Index].Delete();
            //Names.Text= "";
             //*so as not duplicate search,just bring the same previouse table search//cause we didn't clear the table when leave ,just in the textchange
             //Names_TextChanged(null, null);
            // Names_Leave(null, null);
             move_one_btn.Enabled = true;
             move_all_btn.Enabled = true;
             if (Grd_per.Rows.Count == 0)
             {
                 remove_one_btn.Enabled = false;
                 remove_all_btn.Enabled = false;
             }
         }

         private void remove_all_btn_Click(object sender, EventArgs e)
         {

             
             DT_per.Rows.Clear();
             //Names_TextChanged(null, null);
             Names_Leave(null, null);
             move_one_btn.Enabled = true;
             move_all_btn.Enabled = true;
             remove_one_btn.Enabled = false;
             remove_all_btn.Enabled = false;
         }

         private void Grd_Rem_CellContentClick(object sender, DataGridViewCellEventArgs e)
         {

         }

         private void Names_Leave(object sender, EventArgs e)
         {
         
                 //}



                 //*****



                 //if (connection.SQLDS.Tables.Contains("matching_personstbl"))
                 //{ connection.SQLDS.Tables.Remove("matching_personstbl"); }

                 //if (DT_per.Rows.Count > 0)
                 //{ DT_per.Clear(); }


                 //if (DT_per_TBL.Rows.Count > 0)
                 //{ DT_per_TBL.Clear(); }
                 //else
                 //{

                 //*****
                 try
                 {
                     //int per_id_serach = 0;
                     //int.TryParse(TxtCust_Name.Text, out per_id_serach);


                     string Sqltexe1 = "EXec search_for_outlier_Rem  '" + Names.Text.Trim() + "'," + OperType.SelectedIndex + "";


                     connection.SqlExec(Sqltexe1, "matching_personstbl");

                     if (connection.SQLDS.Tables["matching_personstbl"].Rows.Count == 0)
                     {
                         MessageBox.Show(connection.Lang_id == 1 ? "يرجى التأكد من الاسم !  " : "Please Check the Name!", MyGeneral_Lib.LblCap);
                         return;



                         //ــــــ

                     }

                 }
                 //  }


                 catch
                 { Grd_per_id.DataSource = new DataTable(); }



             

             //}
            //-------//*
             try
             {
                 if (connection.SQLDS.Tables["matching_personstbl"].Rows.Count > 0)
                 {
                     DT_per_TBL = connection.SQLDS.Tables["matching_personstbl"];
                     per_id = 0;
                     string Cond_Str_per_id = "";
                     string Con_Str = "";
                     if (DT_per.Rows.Count > 0)
                     {
                         MyGeneral_Lib.ColumnToString(DT_per, "per_id", out Cond_Str_per_id);
                         Con_Str = " And per_id not in(" + Cond_Str_per_id + ")";
                     }
                     int.TryParse(Names.Text, out per_id);





                     Filter = " (Per_Name like '" + Names.Text + "%'"
                               + " Or per_id = " + per_id + ")" + Con_Str;


                     DT_per_TBL = connection.SQLDS.Tables["matching_personstbl"].DefaultView.ToTable(true, "per_id", "Per_Name").Select(Filter).CopyToDataTable();
                     Grd_per_id.DataSource = DT_per_TBL;
                 }
             }
             catch
             {
}
             if (Grd_per_id.Rows.Count <= 0)
             {

                 move_one_btn.Enabled = false;
                 move_all_btn.Enabled = false;
             }
             else
             {
                 move_one_btn.Enabled = true;
                 move_all_btn.Enabled = true;
             }

         }


       

         private void button2_Click(object sender, EventArgs e)
         {
             this.Close();
         }

         private void Grd_per_info_SelectionChanged(object sender, EventArgs e)
         {
             if (connection.SQLDS.Tables["outlier_remittance_detection_tbl1"].Rows.Count > 0)
             {

                 if (Change_grd)
                 {
                     if (OperType.SelectedIndex == 0)
                     {
                         {
                             per_name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();

                             try
                             {

                                 DT_rem2 = connection.SQLDS.Tables["outlier_remittance_detection_Tbl1"].Select(" R_name LIKE '" + per_name + "'").CopyToDataTable();
                                 BS_Grid_Rem.DataSource = DT_rem2;
                                 Grd_Rem.DataSource = BS_Grid_Rem;
                             }

                             catch { Grd_Rem.DataSource = new BindingSource(); }

                             try
                             {

                                  DT_out = connection.SQLDS.Tables["outlier_remittance_detection_Tbl2"].Select("R_name LIKE '" + per_name + "'").CopyToDataTable();
                                  BS_Grid_outlier.DataSource = DT_out;
                                 //BS_Grd_Buffer_Cust_Account_Auth.DataSource = Buffer_Cust_Account_Auth_tbl;
                                 Grd_outlier.DataSource = BS_Grid_outlier;

                             }

                             catch { Grd_outlier.DataSource = new BindingSource(); }


                         }

                     }
                     else
                     {
                         if (OperType.SelectedIndex == 1)
                         {
                             per_name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();

                             try
                             {

                                  DT_rem2 = connection.SQLDS.Tables["outlier_remittance_detection_Tbl1"].Select(" S_name LIKE '" + per_name + "'").CopyToDataTable();
                                  BS_Grid_Rem.DataSource = DT_rem2;
                                 Grd_Rem.DataSource = BS_Grid_Rem;
                             }

                             catch { Grd_Rem.DataSource = new BindingSource(); }

                             try
                             {
                                 DT_out = connection.SQLDS.Tables["outlier_remittance_detection_Tbl2"].Select("S_name LIKE '" + per_name + "'").CopyToDataTable();
                                 BS_Grid_outlier.DataSource = DT_out;
                                 //BS_Grd_Buffer_Cust_Account_Auth.DataSource = Buffer_Cust_Account_Auth_tbl;
                                 Grd_outlier.DataSource = BS_Grid_outlier;

                             }

                             catch { Grd_outlier.DataSource = new BindingSource(); }


                         }
                     }
                 }
             }
             else
             {
                 Grd_per_info.DataSource = new BindingSource();
                 Grd_Rem.DataSource = new BindingSource();
                 Grd_outlier.DataSource = new BindingSource();
                
             }
             //else
             //{

         }

         private void Names_TextChanged(object sender, EventArgs e)
         {
             if (connection.SQLDS.Tables.Contains("matching_personstbl"))
             { connection.SQLDS.Tables.Remove("matching_personstbl"); }

             if (DT_per.Rows.Count > 0)
             { DT_per.Clear(); }


             if (DT_per_TBL.Rows.Count > 0)
             { DT_per_TBL.Clear(); }
         }

         private void Grd_per_info_CellContentClick(object sender, DataGridViewCellEventArgs e)
         {

         }

         private void Graph_button_Click(object sender, EventArgs e)
         {
             if (Grd_per_info.Rows.Count > 0)
             {
                 //standard_deviation
                 int maximum = Convert.ToInt32(((DataRowView)BS_Grid_persons.Current).Row["max_rate"]);
                 int minimum = Convert.ToInt32(((DataRowView)BS_Grid_persons.Current).Row["min_rate"]);
                 int stand_div = Convert.ToInt32(((DataRowView)BS_Grid_persons.Current).Row["standard_deviation"]);
                 int meanValue = Convert.ToInt32(((DataRowView)BS_Grid_persons.Current).Row["mean"]);
                 string per_Name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();

                 int RemAmount = Convert.ToInt32(((DataRowView)BS_Grid_Rem.Current).Row["rem_loc_amount"]);
                 int RemOut = Convert.ToInt32(((DataRowView)BS_Grid_outlier.Current).Row["rem_loc_amount"]);

                 // int RemNumber = Convert.ToInt32(((DataRowView)BS_Grid_Rem.Current).Row["rem_no"]);
                 string RemNumber = ((DataRowView)BS_Grid_Rem.Current).Row["rem_no"].ToString();
            
                 graph addfrm = new graph(maximum, minimum, stand_div, meanValue, per_Name, RemAmount, RemOut, DT_rem2, DT_out);
                 addfrm.ShowDialog(this);
             }
             else
             {
                 MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد حوالات لعرض رسمها البياني  " : "There is No Rem. To show the graph diagram", MyGeneral_Lib.LblCap);
                 return;
             }
         }

         private void button1_Click(object sender, EventArgs e)
         {

            
             string person_info = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();
         

             DT_per_info_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl"].DefaultView.ToTable(false, "per_name", "rem_count", "rem_sum", "mean", "standard_deviation", "min_rate", "max_rate"
                 ).Select("per_name = '" + person_info + "'").CopyToDataTable();
           

             if (OperType.SelectedIndex == 0)
             {
                 {
                     if (connection.Lang_id == 1)
                     {
                         per_name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();



                         DT_Rem_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl1"].DefaultView.ToTable(false, "rem_no", "id", "ACASE_NA", "R_amount", "R_ACUR_NAME", "rem_loc_amount", "PR_ACUR_NAME", "S_ACITY_NAME", "t_ACITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_ADOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_A_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ACITY_NAME", "r_ACOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_ADOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ACUR_NAME", "c_ocomm", "c_ACUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_job", "s_details_job", "r_job", "r_details_job", "Real_Paid_Name", "S_Acust_name", "D_Acust_name", "create_date"
         ).Select(" R_name LIKE '" + per_name + "'").CopyToDataTable();
                       






                         DT_Outlier_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl2"].DefaultView.ToTable(false, "rem_no", "id", "ACASE_NA", "R_amount", "R_ACUR_NAME", "rem_loc_amount", "PR_ACUR_NAME", "S_ACITY_NAME", "t_ACITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_ADOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_A_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ACITY_NAME", "r_ACOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_ADOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ACUR_NAME", "c_ocomm", "c_ACUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_job", "s_details_job", "r_job", "r_details_job", "Real_Paid_Name", "S_Acust_name", "D_Acust_name", "create_date"
         ).Select("R_name LIKE '" + per_name + "'").CopyToDataTable();
                    
                     }
                     else
                     {
                         
                         per_name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();



                         DT_Rem_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl1"].DefaultView.ToTable(false, "rem_no", "id", "ECASE_NA", "R_amount", "R_ECUR_NAME", "rem_loc_amount", "PR_ECUR_NAME", "S_ECITY_NAME", "t_ECITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_E_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ECITY_NAME", "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ECUR_NAME", "c_ocomm", "c_ECUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_Ejob", "s_details_job", "r_Ejob", "r_details_job", "Real_Paid_Name", "S_Ecust_name", "D_Ecust_name", "create_date"
         ).Select(" R_name LIKE '" + per_name + "'").CopyToDataTable();
                 






                         DT_Outlier_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl2"].DefaultView.ToTable(false, "rem_no", "id", "ECASE_NA", "R_amount", "R_ECUR_NAME", "rem_loc_amount", "PR_ECUR_NAME", "S_ECITY_NAME", "t_ECITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_E_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ECITY_NAME", "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ECUR_NAME", "c_ocomm", "c_ECUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_Ejob", "s_details_job", "r_Ejob", "r_details_job", "Real_Paid_Name", "S_Ecust_name", "D_Ecust_name", "create_date"
         ).Select("R_name LIKE '" + per_name + "'").CopyToDataTable();
                       
                     }
                 }

                     

                     

                 }

             
             else
             {
                 if (OperType.SelectedIndex == 1)
                 {
                     if (connection.Lang_id == 1)
                     {
                         per_name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();


                         DT_Rem_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl1"].DefaultView.ToTable(false, "rem_no", "id", "ACASE_NA", "R_amount", "R_ACUR_NAME", "rem_loc_amount", "PR_ACUR_NAME", "S_ACITY_NAME", "t_ACITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_ADOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_A_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ACITY_NAME", "r_ACOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_ADOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ACUR_NAME", "c_ocomm", "c_ACUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_job", "s_details_job", "r_job", "r_details_job", "Real_Paid_Name", "S_Acust_name", "D_Acust_name", "create_date"
             ).Select(" S_name LIKE '" + per_name + "'").CopyToDataTable();



                         DT_Outlier_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl2"].DefaultView.ToTable(false, "rem_no", "id", "ACASE_NA", "R_amount", "R_ACUR_NAME", "rem_loc_amount", "PR_ACUR_NAME", "S_ACITY_NAME", "t_ACITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_ADOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_A_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ACITY_NAME", "r_ACOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_ADOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ACUR_NAME", "c_ocomm", "c_ACUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_job", "s_details_job", "r_job", "r_details_job", "Real_Paid_Name", "S_Acust_name", "D_Acust_name", "create_date"
             ).Select(" S_name LIKE '" + per_name + "'").CopyToDataTable();
                         BS_Grid_outlier.DataSource = DT_out;
                         //BS_Grd_Buffer_Cust_Account_Auth.DataSource = Buffer_Cust_Account_Auth_tbl;
                         Grd_outlier.DataSource = BS_Grid_outlier;
                     }
                     else
                     {
                         per_name = ((DataRowView)BS_Grid_persons.Current).Row["per_name"].ToString();


                         DT_Rem_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl1"].DefaultView.ToTable(false, "rem_no", "id", "ECASE_NA", "R_amount", "R_ECUR_NAME", "rem_loc_amount", "PR_ECUR_NAME", "S_ECITY_NAME", "t_ECITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_E_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ECITY_NAME", "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ECUR_NAME", "c_ocomm", "c_ECUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_Ejob", "s_details_job", "r_Ejob", "r_details_job", "Real_Paid_Name", "S_Ecust_name", "D_Ecust_name", "create_date"
         ).Select(" S_name LIKE '" + per_name + "'").CopyToDataTable();



                         DT_Outlier_Export = connection.SQLDS.Tables["outlier_remittance_detection_Tbl2"].DefaultView.ToTable(false, "rem_no", "id", "ECASE_NA", "R_amount", "R_ECUR_NAME", "rem_loc_amount", "PR_ECUR_NAME", "S_ECITY_NAME", "t_ECITY_NAME", "user_name", "Case_Date", "C_DATE", "S_name", "S_phone", "S_address", "S_Street", "S_Suburb", "S_Post_Code", "S_State", "S_Social_No", "sFRM_EDOC_NA", "S_doc_no", "S_doc_ida", "S_doc_eda", "S_doc_issue", "s_Email", "s_E_NAT_NAME", "s_notes", "r_name", "R_phone", "r_ECITY_NAME", "r_ECOUN_NAME", "r_address", "r_Street", "r_Suburb", "r_Post_Code", "r_State", "R_Social_No", "rFRM_EDOC_NA", "r_doc_no", "r_doc_ida", "r_doc_eda", "r_doc_issue", "r_Email", "T_purpose", "r_notes", "vo_no", "In_rec_date", "sbirth_date", "rbirth_date", "s_ocomm", "s_ECUR_NAME", "c_ocomm", "c_ECUR_NAME", "rate_online", "SBirth_Place", "Source_money", "Relation_S_R", "R_T_purpose", "r_Relation", "s_Ejob", "s_details_job", "r_Ejob", "r_details_job", "Real_Paid_Name", "S_Ecust_name", "D_Ecust_name", "create_date"
         ).Select(" S_name LIKE '" + per_name + "'").CopyToDataTable();
                         BS_Grid_outlier.DataSource = DT_out;
                         //BS_Grd_Buffer_Cust_Account_Auth.DataSource = Buffer_Cust_Account_Auth_tbl;
                         Grd_outlier.DataSource = BS_Grid_outlier;
                     }

                     
                    


                 }
             }



                 DataGridView[] Export_GRD = { Grd_per_info, Grd_Rem, Grd_outlier };
                 DataTable[] Export_DT = { DT_per_info_Export, DT_Rem_Export, DT_Outlier_Export };
                 MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);

             }

         private void RemType_SelectedIndexChanged(object sender, EventArgs e)
         {
             //**
             Grd_per_info.DataSource = new DataTable();
             Grd_Rem.DataSource = new DataTable();
             Grd_outlier.DataSource = new DataTable();

             // Names.Text = "";

             if (Grd_per_id.Rows.Count > 0)
             {

                 move_one_btn.Enabled = true;
                 move_all_btn.Enabled = true;
                 remove_one_btn.Enabled = false;
                 remove_all_btn.Enabled = false;

             }

             if (Grd_per.Rows.Count > 0)
             {

                 remove_one_btn.Enabled = true;
                 remove_all_btn.Enabled = true;
             }
             //******
         }

         private void Cbo_curs_SelectedIndexChanged(object sender, EventArgs e)
         {
             //**
             Grd_per_info.DataSource = new DataTable();
             Grd_Rem.DataSource = new DataTable();
             Grd_outlier.DataSource = new DataTable();

             // Names.Text = "";

             if (Grd_per_id.Rows.Count > 0)
             {

                 move_one_btn.Enabled = true;
                 move_all_btn.Enabled = true;
                 remove_one_btn.Enabled = false;
                 remove_all_btn.Enabled = false;

             }

             if (Grd_per.Rows.Count > 0)
             {

                 remove_one_btn.Enabled = true;
                 remove_all_btn.Enabled = true;
             }
             //******
         }
         

       

   
         }

         

     

      

       
       

      

     

        

     

  

      

       


        

     

      

       


       

     

    }

