﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class CState_Acc_main : Form
    {
        BindingSource _Bs_cbo = new BindingSource();
         BindingSource _Bs_Grd_Acc_T = new BindingSource();
         int CS_ID = 0;
     

        DataTable Acc_Tbl = new DataTable();
        DataTable Get_data_Tbl = new DataTable();
        DataTable Del_Tbl = new DataTable();
        //DataTable DT_If_empty = new DataTable();
        string SqlText = "";
        bool change_cState = false;




        public CState_Acc_main()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            //Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);

            
        }

        private void CState_Acc_main_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            change_cState = false;
            Grd_Acc_Tree.AutoGenerateColumns = false;
            Get_data_Tbl = connection.SqlExec("EXec Cstate_getData", "Cstate_getData_tbl");

           
                //Grd_Acc_Tree.Columns["Column1"].Visible = true;
                Grd_Acc_Tree.Columns["Column1"].HeaderText = connection.Lang_id == 1 ? "" : "";
                Grd_Acc_Tree.Columns["Column4"].HeaderText = connection.Lang_id == 1 ? "الرمز" : "ID";
               Grd_Acc_Tree.Columns["Column2"].HeaderText = connection.Lang_id == 1 ? "الاسم عربي" : "Arabic Name";
               Grd_Acc_Tree.Columns["Column3"].HeaderText = connection.Lang_id == 1 ? "الاسم اجنبي" : "English Name";
              
            

            //string sql_text = " select 0 as CState_ID , 'الجميع' as Cstate_Aname , 'ALL' as CState_Ename From Cstate_Tbl"
            //                  + " union "
            //                  + "select  CState_ID , Cstate_Aname , CState_Ename From Cstate_Tbl ";

           //_Bs_cbo.DataSource = connection.SqlExec(sql_text, "Cstate_getData_tbl");
            _Bs_cbo.DataSource = connection.SQLDS.Tables["Cstate_getData_tbl2"];
            Cbo_cstate.DataSource = _Bs_cbo;
            Cbo_cstate.DisplayMember = connection.Lang_id == 1 ? "Cstate_AName" : "Cstate_ENAME";
            Cbo_cstate.ValueMember = "Cstate_ID";

          
            change_cState = true;
            Cbo_cstate_SelectedIndexChanged(sender, e);

        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {

            CState_Acc_Setting Addfrm = new CState_Acc_Setting();
            this.Visible = false;
            Addfrm.ShowDialog(this);
            CState_Acc_main_Load(sender,e);
            this.Visible = true;
        }

        //CheckBox HCheckBox = ((CheckBox)sender);
        //    foreach (DataGridViewRow Row in Grd_TermPermission.Rows)
        //        ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

        //    Grd_TermPermission.RefreshEdit();
       

        private void CState_Acc_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Str1 = { "Cstate_getData_tbl", "Cstate_getData_tbl1","Cstate_getData_tbl2","Cstate_getData_tbl3","Cstate_Main_tbl"};
            foreach (string Tbl in Str1)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void Cbo_cstate_SelectedIndexChanged(object sender, EventArgs e)
     {
            
        checkBox1.Checked = false;
        if(change_cState == true)
        {
            if (connection.SQLDS.Tables.Contains("Cstate_Main_TBL"))
            {
            connection.SQLDS.Tables.Remove("Cstate_Main_TBL");
            }

            connection.SqlExec("Cstate_Main " + Cbo_cstate.SelectedValue , "Cstate_Main_TBL");
            _Bs_Grd_Acc_T.DataSource = connection.SQLDS.Tables["Cstate_Main_TBL"];
            Grd_Acc_Tree.DataSource = _Bs_Grd_Acc_T;

            }
        }

        private void Add_Btn_Click(object sender, EventArgs e)
        {

            DataTable Del_Tbl = new DataTable();




            try
            {
                Del_Tbl = connection.SQLDS.Tables["Cstate_Main_TBL"].DefaultView.ToTable(false, "Chk", "CState_Acc_ID").Select("Chk > 0").CopyToDataTable();
                Del_Tbl = Del_Tbl.DefaultView.ToTable(false, "CState_Acc_ID").Select().CopyToDataTable();
            }
            catch
            {

                //DT_Cstate_Setting = DT_If_empty;
            }

            if (Del_Tbl.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لم يتم تحديد بيانات " : "There is no Data was selected", MyGeneral_Lib.LblCap);
                return;
            }

            //Del_Tbl.
            ////    Acc_Tbl
            //    try
            //{
            //    Del_Tbl = Acc_Tbl.DefaultView.ToTable(false, "Chk", "CState_Acc_ID").Select("Chk > 0").CopyToDataTable();
            //    Del_Tbl = Del_Tbl.DefaultView.ToTable(false, "CState_Acc_ID").Select().CopyToDataTable();
            //}
            //catch
            //{

            //    //Del_Tbl = struct_Dt_Tbl;
            //}

            //if (Del_Tbl.Rows.Count == 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "لم يتم تحديد بيانات " : "There is no Data was selected", MyGeneral_Lib.LblCap);
            //    return;
            //}


            //            @CState_Acc_ID  Cstate_Acc_Id_Type readonly,
            //@CState_ID smallint
            //CS_ID =
            connection.SQLCMD.Parameters.AddWithValue("@CState_Acc_ID", Del_Tbl);
            connection.SQLCMD.Parameters.AddWithValue("@CState_ID", Cbo_cstate.SelectedValue);
            //Grd_Acc_Tree.CurrentRow.["Cstate_Acc_ID"]);
            connection.SqlExec("Cstate_Setting_Upd", connection.SQLCMD);

            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? " تم الحذف بتاريخ " + DateTime.Now : " Deleting Done " + DateTime.Now, MyGeneral_Lib.LblCap);

            CState_Acc_main_Load(sender, e);
        }
        //------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-------------------------------------------------------------
        private void checkBox1_MouseClick(object sender, MouseEventArgs e)
        {
            CheckBox HCheckBox = ((CheckBox)sender);
            foreach (DataGridViewRow Row in Grd_Acc_Tree.Rows)
                ((DataGridViewCheckBoxCell)Row.Cells["Column1"]).Value = HCheckBox.Checked;

            Grd_Acc_Tree.RefreshEdit();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Grd_Acc_Tree.Columns["Column1"].Visible = false;
            //Column5
            Grd_Acc_Tree.Columns["Column1"].HeaderText = connection.Lang_id == 1 ? "الرمز" : "ID";
            Grd_Acc_Tree.Columns["Column4"].HeaderText = connection.Lang_id == 1 ? "رقم الحساب" : "Acc Number";
            Grd_Acc_Tree.Columns["Column5"].HeaderText = connection.Lang_id == 1 ? "الاسم عربي" : "Arabic Name";
            Grd_Acc_Tree.Columns["Column2"].HeaderText = connection.Lang_id == 1 ? "الاسم اجنبي" : "English Name";
            Grd_Acc_Tree.Columns["Column3"].HeaderText = connection.Lang_id == 1 ? "" : "";

            

            DataTable ExpDt = connection.SQLDS.Tables["Cstate_Main_TBL"].DefaultView.ToTable(false,"CState_Acc_ID","ACC_NO","Acc_AName" , "Acc_EName");
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_Acc_Tree };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);

           Grd_Acc_Tree.Columns["Column1"].Visible = true;

           Grd_Acc_Tree.Columns["Column1"].HeaderText = connection.Lang_id == 1 ? "" : "";
           Grd_Acc_Tree.Columns["Column4"].HeaderText = connection.Lang_id == 1 ? "الرمز" : "ID";
           Grd_Acc_Tree.Columns["Column5"].HeaderText = connection.Lang_id == 1 ? "رقم الحساب" : "Acc Number";
           Grd_Acc_Tree.Columns["Column2"].HeaderText = connection.Lang_id == 1 ? "الاسم عربي" : "Arabic Name";
           Grd_Acc_Tree.Columns["Column3"].HeaderText = connection.Lang_id == 1 ? "الاسم اجنبي" : "English Name";
            //CState_Acc_main_Load(sender,e);
        }

       
        



        

        
        }


       

     

      

 
    




   


      


     
   
      

       
 
    }

    
