﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace Integration_Accounting_Sys 
{
    public partial class graph : Form
    {
        string name = "";
        int max = 0;
        int min = 0;
        int Sdiv = 0;
        int mean = 0;
        int amount = 0;
        int out_ = 0;
      //  string id_rem = "";
       // string RNo ="";
        DataTable DT_RM;
        DataTable DT_Out;

        public graph(int ma, int mi, int Sd, int me, string na, int remAm, int aoutL, DataTable DT_Rem_Graph, DataTable DT_o)
        {
            InitializeComponent();
            name =na;
             max =ma;
             min =mi;
             Sdiv =Sd;
             mean = me;
             amount = remAm;
             out_ = aoutL;
             //id_rem = id;
           //  RNo = RNum;
             DT_RM = DT_Rem_Graph;
             DT_Out = DT_o;

           

            
        }

        public graph()
        {
            // TODO: Complete member initialization
        }

        public string Content { get; set; }

        private void graph_Load(object sender, EventArgs e)
        {
            Names.Text = name;
            if (connection.Lang_id != 1)
            {
                button2.Text = "Back >>";
            }
            //DataView dt1 = new DataView(DT_RM);
            //DataView dt2= new DataView(DT_RM);
            chart1.Series.Clear();

            
            for (int i = 0; i < DT_RM.Rows.Count; i++)
            {
                chart1.Series.Add("Rem"+Convert.ToString(i));
                chart1.Series["Rem" + Convert.ToString(i)].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
                chart1.Series["Rem" + Convert.ToString(i)].MarkerSize = 14;
                chart1.Series["Rem" + Convert.ToString(i)].Color = Color.Blue;
                chart1.Series["Rem" + Convert.ToString(i)].Points.AddXY(DT_RM.Rows[i]["id"], DT_RM.Rows[i]["rem_loc_amount"]);
                chart1.Series["Rem" + Convert.ToString(i)].ToolTip = Convert.ToString(DT_RM.Rows[i]["rem_no"]);
                chart1.Series["Rem" + Convert.ToString(i)].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
                //it's taking the last rem no because each time put a no. in the tool tip till reach the last rem no
                //so i decided to divided the seriese,each rem putting in a diffrent series .lets try!
                //and the name of seriese with i extention beacause its not allow to use existing series name
                //loop was only for the values of points and now it's for the name of series also
                //Elaf^_^
              
            
                
            }
            //--------------------------------------------------------------------------------------------------------

            chart1.Series.Add("Mean");
            for (int i = 0; i <= DT_RM.Rows.Count+5; i++)
            {
                chart1.Series["Mean"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                //chart1.Series["Mean"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.wi
                chart1.Series["Mean"].BorderWidth = 3;
                //chart1.Series["Mean"].MarkerBorderWidth = 1000;
                chart1.Series["Mean"].Color = Color.Red;
                chart1.Series["Mean"].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None;
                
             //   s.Appearance.PointShape = Telerik.Charting.Styles.DefaultFigures.Diamond;
                chart1.Series["Mean"].Points.AddXY(i, mean);
            }


            //--------------------------------------------------------------------------------------------------------
           
                chart1.Series.Add("MiniMumRem");
                for (int i = 0; i <= DT_RM.Rows.Count+5; i++)
                {
                    chart1.Series["MiniMumRem"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    //chart1.Series["MiniMumRem"].MarkerSize = 25;
                    chart1.Series["MiniMumRem"].BorderWidth = 3;
                    chart1.Series["MiniMumRem"].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None;
                    chart1.Series["MiniMumRem"].Color = Color.Red;
                    chart1.Series["MiniMumRem"].Points.AddXY(i, min);
            }//loop should e after the add of series ecause it will warn you " already exists"

            //--------------------------------------------------------------------------------------------------------

                chart1.Series.Add("maximumRem");
                for (int i = 0; i <= DT_RM.Rows.Count+5; i++)
                {
                    chart1.Series["maximumRem"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    chart1.Series["maximumRem"].BorderWidth = 3;
                    chart1.Series["maximumRem"].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None;
                    chart1.Series["maximumRem"].Color = Color.Red;
                    chart1.Series["maximumRem"].Points.AddXY(i, max);
            }

           //--------------------------------------------------------------------------------------------------------
                
                for (int i = 0; i < DT_Out.Rows.Count; i++)
                {
                    chart1.Series.Add("Outs"+i);
                    chart1.Series["Outs" + i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
                    chart1.Series["Outs" + i].MarkerSize = 14;
                    chart1.Series["Outs" + i].Color = Color.Red;
                    chart1.Series["Outs" + i].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Cross;
                    chart1.Series["Outs" + i].Points.AddXY(DT_Out.Rows[i]["id"], DT_Out.Rows[i]["rem_loc_amount"]);
                    chart1.Series["Outs" + i].ToolTip = Convert.ToString(DT_Out.Rows[i]["rem_no"]);
                }
            //   chart1.Size=graph.

         //--------------------------------------------------------------------------------------------------------E.

               

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        
      
    }
}
