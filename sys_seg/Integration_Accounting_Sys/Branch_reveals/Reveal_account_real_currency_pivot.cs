﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_account_real_currency_pivot : Form
    {
        string @format = "dd/MM/yyyy";
        BindingSource BS_cbo_cust = new BindingSource();
        BindingSource BS_Grid0 = new BindingSource();
        BindingSource BS_G1 = new BindingSource();
        BindingSource BS_G2 = new BindingSource();
        BindingSource BS_G3 = new BindingSource();
        DataGridView Date_Grd = new DataGridView();
        DataTable Date_Tbl = new DataTable();
        DataTable DT_G1 = new DataTable();
        DataTable DT_G3 = new DataTable();
         //BindingSource _Bs_Grd_nat = new BindingSource();
         string SqlText = "";
         
     

        DataTable Acc_Tbl = new DataTable();
        //DataTable DT_If_empty = new DataTable();
        DataTable DT_Term = new DataTable();
        DataTable _Dt = new DataTable();
        //DataTable _Dt2 = new DataTable();
        DataTable _DT2 = new DataTable();
        DataTable DT_Grd0 = new DataTable();
        //DataTable DT_Gr = new DataTable();
        DataTable DT_data = new DataTable();
        DataTable DT_Cur = new DataTable();
       // DataTable Dt_Param = CustomControls.CustomParam_Dt();
        string Acc_no_current = "";
        string Cur_Name = "";
        Int16 T_id_current = 0;
        Int32 Cust_id_current = 0;
        Int16 Cur_id_current = 0;
        Int16 for_cur_id = 0;

        Int32 Acc_id_current = 0;
        Int32 Vo_no_current = 0;
        Int32 Oper_id_current = 0;
        string nrec_date_current;
        string cur_nameG0 = "";
        bool Xchange1 = false;
        bool Xchange2 = false;
        bool Xchange3 = false;
        bool Xchange4 = false;
        bool Xchange0 = false;
        string Dot_Acc_no_current = "";

        Int64 from_nrec_date = 0;
        Int64 to_nrec_date = 0;

       // bool Xchange1 = false;

        public static Int64 frm_date = 0;
        public static Int64 to_date = 0;
        public static Int64 t_id_new = 0;




        public Reveal_account_real_currency_pivot()
        {
            InitializeComponent();

            //Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            
            Create_Tbl();
            Create_Tbl2();
            G1.AutoGenerateColumns = false;
            G2.AutoGenerateColumns = false;
            G3.AutoGenerateColumns = false;

            
        }

        private void Rev_ACC_Loc_Real_Load(object sender, EventArgs e)
        {

            
            Create_Tbl();
            Create_Tbl2();
            TxtFromDate.CustomFormat = "00/00/0000";
            TxtToDate.CustomFormat = "00/00/0000";



         
 
            G1.Columns["Column6"].HeaderText = connection.Lang_id == 1 ? "رمز الحساب" : "Acc No.";
            G1.Columns["Column7"].HeaderText = connection.Lang_id == 1 ? "الحســـــــاب" : "Account";
            G1.Columns["Column20"].HeaderText = connection.Lang_id == 1 ? " اسم العملة" : "Curr. Name";
            G1.Columns["Column16"].HeaderText = connection.Lang_id == 1 ? "المعدل" : "Cstate";
            //G1.Columns["Column17"].HeaderText = connection.Lang_id == 1 ? "الفئة اجنبي" : "E Cstate";
            G1.Columns["Column3"].HeaderText = connection.Lang_id == 1 ? "اول الفترة-(ع.ص)" : "Beginning of period R.C.";
            G1.Columns["Column4"].HeaderText = connection.Lang_id == 1 ? "الفترة-(ع.ص)" : "Period R. C.";
            G1.Columns["Column5"].HeaderText = connection.Lang_id == 1 ? "نهاية الفترة-(ع.ص)" : "End of period R. C.";
            G1.Columns["Column8"].HeaderText = connection.Lang_id == 1 ? "اول الفترة-(ع.م)" : "Beggining of period L.C.";
            G1.Columns["Column31"].HeaderText = connection.Lang_id == 1 ? "الفترة-(ع.م)" : "Period L.c.";
            G1.Columns["Column32"].HeaderText = connection.Lang_id == 1 ? "نهاية الفترة-(ع.م)" : "End of period L.C.";
            //----
            


            G2.Columns["dataGridViewTextBoxColumn1"].HeaderText = connection.Lang_id == 1 ? " رمز الفرع" : "Term id";
            G2.Columns["dataGridViewTextBoxColumn2"].HeaderText = connection.Lang_id == 1 ? " الاســـــــــم" : "Name";
            G2.Columns["Column21"].HeaderText = connection.Lang_id == 1 ? " اسم العملة" : "Curr. Name";
            G2.Columns["Column43"].HeaderText = connection.Lang_id == 1 ? " اول الفترة-(ع.ص)" : "Beginning of period R.C.";
            G2.Columns["Column44"].HeaderText = connection.Lang_id == 1 ? " الفترة-(ع.ص)" : "Period R.C.";
            G2.Columns["Column45"].HeaderText = connection.Lang_id == 1 ? " نهاية الفترة-(ع.ص)" : "End of period R.C.";
            G2.Columns["Column40"].HeaderText = connection.Lang_id == 1 ? " اول الفترة-(ع.م)" : "Beginning of period L.C.";
            G2.Columns["Column41"].HeaderText = connection.Lang_id == 1 ? " الفترة-(ع.م)" : "Period L.C.";
            G2.Columns["Column42"].HeaderText = connection.Lang_id == 1 ? " نهاية الفترة-(ع.م)" : "End of period L.C.";
            G2.Columns["Column1"].HeaderText = connection.Lang_id == 1 ? " التاريـــخ من " : "Date From";
            G2.Columns["Column2"].HeaderText = connection.Lang_id == 1 ? "الـــــــــــــــــى" : "To";

            //*****
            



            G3.Columns["Column12"].HeaderText = connection.Lang_id == 1 ? " الحساب" : "Acc.";
            G3.Columns["Column15"].HeaderText = connection.Lang_id == 1 ? " اسم الفرع" : "Term NAme";
            G3.Columns["Column9"].HeaderText = connection.Lang_id == 1 ? " رمز الثانوي" : "Cust. ID";
            G3.Columns["Column10"].HeaderText = connection.Lang_id == 1 ? " أسم الثانوي" : "Cust Name";
            G3.Columns["Column11"].HeaderText = connection.Lang_id == 1 ? "اسم العملة" : "Curr. Name";
            G3.Columns["Column13"].HeaderText = connection.Lang_id == 1 ? "  اول الفترة-(ع.ص)" : "Beginning of period R.C.";
            G3.Columns["Column33"].HeaderText = connection.Lang_id == 1 ? "الفترة-(ع.ص)" : "Period R.C.";
            G3.Columns["Column35"].HeaderText = connection.Lang_id == 1 ? " نهاية الفترة-(ع.ص)" : "End of period R.C.";
            G3.Columns["Column14"].HeaderText = connection.Lang_id == 1 ? "اول الفترة-(ع.م)" : "Beginning of period L.C.";
            G3.Columns["Column36"].HeaderText = connection.Lang_id == 1 ? " الفترة-(ع.م) " : "Period L.C.";
            G3.Columns["Column34"].HeaderText = connection.Lang_id == 1 ? "نهاية الفترة-(ع.م)" : "End of period L.C.";


            if (connection.Lang_id == 2)
            {


                Column7.DataPropertyName = "Acc_EName";
                Column20.DataPropertyName = "Cur_ENAME";
                Column16.DataPropertyName = "Cstate_EName";
                dataGridViewTextBoxColumn2.DataPropertyName ="ETerm_Name";
                Column21.DataPropertyName = "Cur_ENAME";
                Column12.DataPropertyName = "Acc_Ename";
                Column15.DataPropertyName = "ETerm_Name";
                Column10.DataPropertyName = "Ecust_Name";
                Column11.DataPropertyName = "cur_EName";
                

            }



            //BS_cbo_cust.DataSource = connection.SqlExec("EXec Rev_ACC_Loc_Real_get_Data", "Rev_ACC_Loc_Real_get_Data_Tbl");
            string SqlText = " select a.T_ID , b.Acust_Name ,b.ECust_Name from TERMINALS a,Customers b"
                              + " where a.Cust_id=b.CUST_ID";
            BS_cbo_cust.DataSource = connection.SqlExec(SqlText, "Term_tbl");

            Cbo_cust.DataSource = BS_cbo_cust;
            Cbo_cust.DisplayMember = connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name";
            Cbo_cust.ValueMember = "T_ID";

            Xchange1 = false;
            Xchange0 = false;


            

        }

        private void Create_Tbl()
        {

            string[] Column1 = { "T_ID" };
            string[] DType1 = { "System.Int16"};
            DT_Term = CustomControls.Custom_DataTable("DT_Term", Column1, DType1);
            //Grd_per.DataSource = DT_per;



        }

        private void Create_Tbl2()
        {
           

            //string[] Column1 = { "F_date", "T_Date"};
            //string[] DType1 = { "System.String", "System.String" };
            //DT_date = CustomControls.Custom_DataTable("DT_Gr", Column1, DType1);
            ////Grid0.DataSource = DT_Gr;


            //string[] Column1 = { "العملة", "بنوك دائن", "بنوك مدين", "حوالات دائن", "حوالات مدين", "ذمم دائن", "ذمم مدين", "نقد دائن", "نقد مدين", "المجموع", "المعادل" };
            //string[] DType1 = { "System.String", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal", "System.Decimal" };
            //DT_data = CustomControls.Custom_DataTable("DT_data", Column1, DType1);


        }



        private void getdata()
        {

            #region text_time



            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //from_nrec_date = TxtFromDate.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                from_nrec_date = 0;
            }
            //-------------------------
            if (TxtToDate.Checked == true)
            {


                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;

                //  to_nrec_date = TxtToDate.Value.ToString("yyyy/MM/dd");
            }
            else
            {
                to_nrec_date = 0;
            }
            //---------------------------------




            #endregion


        }

     

        private void button1_Click(object sender, EventArgs e)
        {
            Xchange1 = false;
            Xchange2 = false;
            Xchange0 = false;

            if (DT_Term.Rows.Count > 0)
            { DT_Term.Clear(); }

            if (DT_data.Rows.Count > 0)
            { DT_data.Clear(); }

            if (DT_Grd0.Rows.Count > 0)
            { DT_Grd0.Clear(); }

            Grid0.DataSource = new DataTable();
            G1.DataSource = new DataTable();
            G2.DataSource = new DataTable();
            G3.DataSource = new DataTable();

            string[] Used_Tbl = { "Reveal_real_currency_Tbl", "Reveal_real_currency_Tbl1", "Reveal_real_currency_Tbl2","Reveal_real_currency_Tbl3","term_tbl"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

         

            getdata();
            if (from_nrec_date > to_nrec_date)
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب ان يكون تاريخ الفتره الادنى اصغر من تاريخ الفتره الاعلى" : " Minimunm Date must be samller than Maximum Date ", MyGeneral_Lib.LblCap);
                return;
            }
          
            DataRow row = DT_Term.NewRow();

            //row["Per_ID"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_ID"];
            row["T_ID"] = Cbo_cust.SelectedValue;
            //row["Per_EName"] = DT_per_TBL.Rows[Grd_per_id.CurrentRow.Index]["Per_EName"];
            DT_Term.Rows.Add(row);

            

            if (TxtFromDate.Checked == false)
            {

                MessageBox.Show(connection.Lang_id == 1 ? "يرجى تأكيد الحد الادنى من التاريخ " : "SELECT THE MINIMUM DATE", MyGeneral_Lib.LblCap);
                return;

            }

            //-------------------------
            if (TxtToDate.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى تأكيد الحد الاعلى من التاريخ " : "SELECT THE MAXIMUM DATE", MyGeneral_Lib.LblCap);
                return;

            }



        

            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "Reveal_real_currency";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.CommandTimeout = 0;
            connection.SQLCMD.Parameters.AddWithValue("@FromNrec_Date", from_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@ToNrec_Date", to_nrec_date);
            connection.SQLCMD.Parameters.AddWithValue("@Tid_Tbl", DT_Term);

            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_real_currency_Tbl");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_real_currency_Tbl1");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_real_currency_Tbl2");
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Reveal_real_currency_Tbl3");

            obj.Close();
            connection.SQLCS.Close();


            connection.SQLCMD.Parameters.Clear();

            if (connection.SQLDS.Tables["Reveal_real_currency_Tbl"].Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لايوجد نتائج ! " : "No Results!", MyGeneral_Lib.LblCap);
                return;
            }
            else
            {
                BS_Grid0.DataSource = connection.SQLDS.Tables["Reveal_real_currency_Tbl3"];
                Grid0.DataSource = BS_Grid0;
                DT_Grd0 = connection.SQLDS.Tables["Reveal_real_currency_Tbl3"];





                DT_G1 = connection.SQLDS.Tables["Reveal_real_currency_Tbl"];
                BS_G1.DataSource = DT_G1;
                G1.DataSource = BS_G1;

                BS_G2.DataSource = connection.SQLDS.Tables["Reveal_real_currency_Tbl1"];
                G2.DataSource = BS_G2;

                BS_G3.DataSource = connection.SQLDS.Tables["Reveal_real_currency_Tbl2"];
                G3.DataSource = BS_G3;

                ////DataTable Dt_Param = CustomControls.CustomParam_Dt();
                //string Fr_Date = (TxtFromDate.Text);
                //string T_Date = TxtToDate.Text;
                //Dt_Param.Rows.Add("Max_Date", Fr_Date);
                //Dt_Param.Rows.Add("Min_Date", T_Date);

                //TxtToDate.CustomFormat = "00/00/0000";
                //TxtFromDate.CustomFormat = "00/00/0000";
                //TxtToDate.Checked = false;
                //TxtFromDate.Checked = false;

                Xchange0 = true;
                Grid0_SelectionChanged(sender, e);
            }

        }

        private void Reveal_account_real_currency_pivot_FormClosing(object sender, FormClosingEventArgs e)
        {
            string[] Used_Tbl = { "Reveal_real_currency_Tbl", "Reveal_real_currency_Tbl1", "Reveal_real_currency_Tbl2", "Term_tbl","Reveal_real_currency_Tbl3"};
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
                Xchange1 = false;
                Xchange2 = false;
                Xchange3 = false;
                Xchange4 = false;
            }
        }

     

      


        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (TxtFromDate.Checked == true)
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = @format;
            }
            else
            {
                TxtFromDate.Format = DateTimePickerFormat.Custom;
                TxtFromDate.CustomFormat = "00/00/0000";
            }
        }

        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
              if (TxtToDate.Checked == true)
              {
                  TxtToDate.Format = DateTimePickerFormat.Custom;
                  TxtToDate.CustomFormat = @format;
              }
              else
              {
                  TxtToDate.Format = DateTimePickerFormat.Custom;
                  TxtToDate.CustomFormat = "00/00/0000";
              }
        }

        private void G1_SelectionChanged(object sender, EventArgs e)
        {
           if (Xchange1 ==true)
           {

               try
               {
                   Acc_no_current = ((DataRowView)BS_G1.Current).Row["acc_no"].ToString();
                   for_cur_id = Convert.ToInt16(((DataRowView)BS_G1.Current).Row["for_cur_id"]);
                   Dot_Acc_no_current = ((DataRowView)BS_G1.Current).Row["Dot_Acc_No"].ToString();
                   //LblRec.Text = connection.Records();
                   Xchange2 = false;

                   _Dt = connection.SQLDS.Tables["Reveal_real_currency_Tbl1"].DefaultView.ToTable().Select("Dot_Acc_No like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable(); ;

                   BS_G2.DataSource = _Dt;
                   G2.DataSource = BS_G2;
                   Xchange2 = true;
                   G2_SelectionChanged(null, null);
               }
               catch
               {
                   G2.DataSource = new DataTable();
                   G3.DataSource = new DataTable();
               }
           }

        }

        private void G2_SelectionChanged(object sender, EventArgs e)
        {
            if (Xchange2)
            {

                try
                {
                    Xchange3 = false;

                    T_id_current = Convert.ToInt16(((DataRowView)BS_G2.Current).Row["T_id"]);
                    for_cur_id = Convert.ToInt16(((DataRowView)BS_G1.Current).Row["for_cur_id"]);
                    Dot_Acc_no_current = ((DataRowView)BS_G1.Current).Row["Dot_Acc_No"].ToString();

                     DT_G3 = connection.SQLDS.Tables["Reveal_real_currency_Tbl2"].DefaultView.ToTable().Select("T_id = " + T_id_current + "and Dot_Acc_No like'" + Dot_Acc_no_current + "%'" + " and for_cur_id =" + for_cur_id).CopyToDataTable();
                     BS_G3.DataSource = DT_G3;
                    G3.DataSource = BS_G3;
                    Xchange3 = true;
                    //LblRec.Text = connection.Records(BS_G3);

                }
                catch
                {
                    G3.DataSource = new DataTable();
                }

            }
        }

        private void Date()
        {


            Date_Grd = new DataGridView();

            if (Date_Grd.ColumnCount > 0)
            {
                Date_Grd.Columns.Remove("Column1");
                Date_Grd.Columns.Remove("Column2");
                Date_Tbl.Columns.Remove("From_Date1");
                Date_Tbl.Columns.Remove("To_Date1");
            }

            Date_Grd.Columns.Add("Column1", connection.Lang_id == 1 ? "الفترة من:" : "From:");
            Date_Grd.Columns.Add("Column2", connection.Lang_id == 1 ? "الفترة الى:" : "To:");

            string[] Column = { "From_Date1", "To_Date1" };
            string[] DType = { "System.String", "System.String" };
            Date_Tbl = CustomControls.Custom_DataTable("Date_Tbl", Column, DType);
            string date_from = ((DataRowView)BS_G2.Current).Row["From_Date1"].ToString();
            string date_To = ((DataRowView)BS_G2.Current).Row["To_Date1"].ToString();
            Date_Tbl.Rows.Add(new object[] { date_from, date_To });


        }

      
        
        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
            


                DT_data = DT_Grd0;
                DataView dv = DT_data.DefaultView;
                //dv.Sort = "rem_flag ASC ,r_cur_id";
                DT_data = dv.ToTable();


                //object sum;
                //sum = DT_data.Compute("Sum(المعادل)", ""); //DT is the DataTable and Amount is the Column to SUM in DataTable.
                //Label.Text = sum.ToString(); //Display sum on the Label.”

                //string Fr_Date = (TxtFromDate.Text);
                //string T_Date = TxtToDate.Text;
                //Dt_Param.Rows.Add("Max_Date",Fr_Date);
                //Dt_Param.Rows.Add("Min_Date",T_Date);
                //Dt_Param.Rows.Add("Summation",sum);

                DataTable Dt_Param = CustomControls.CustomParam_Dt();
                string Fr_Date = (TxtFromDate.Text);
                string T_Date = TxtToDate.Text;
                Dt_Param.Rows.Add("Max_Date", Fr_Date);
                Dt_Param.Rows.Add("Min_Date", T_Date);


                DT_data.TableName = "DT_data";
                connection.SQLDS.Tables.Add(DT_data);
                reveal_account_real_currency ObjRpt = new reveal_account_real_currency();
                reveal_account_real_currency_eng ObjRptEng = new reveal_account_real_currency_eng();
                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 0, true);

                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;


                connection.SQLDS.Tables.Remove("DT_data");
            //}
            //else
            //{

            //}

        }

        private void Btn_Browser_Click_1(object sender, EventArgs e)
        {


            //******
            Date();

            DataGridView[] Export_GRD = { Date_Grd,Grid0,G1,G2,G3 };
            DataTable[] Export_DT = {Date_Tbl, DT_Grd0,DT_G1,_Dt,DT_G3};
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void Grid0_SelectionChanged(object sender, EventArgs e)
        {

            if(Xchange0==true)
            {

                try
                {
                    cur_nameG0 = ((DataRowView)BS_Grid0.Current).Row["العملة"].ToString();

                    DataTable DT_Cur = connection.SQLDS.Tables["Reveal_real_currency_Tbl"].DefaultView.ToTable().Select("Cur_AName like'" + cur_nameG0 + "%'").CopyToDataTable(); ;

                    BS_G1.DataSource = DT_Cur;
                    G1.DataSource = BS_G1;
                    Xchange1 = true;
                    G1_SelectionChanged(null, null);
                    Xchange2 = true;
                    G2_SelectionChanged(null, null);
                }
                catch
                {
                    G1.DataSource = new DataTable();
                    //G3.DataSource = new DataTable();
                }
            }

        }

        private void Cbo_cust_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (DT_Term.Rows.Count > 0)
            { DT_Term.Clear(); }

            if (DT_data.Rows.Count > 0)
            { DT_data.Clear(); }

            if (DT_Grd0.Rows.Count > 0)
            { DT_Grd0.Clear(); }

            Grid0.DataSource = new DataTable();
            G1.DataSource = new DataTable();
            G2.DataSource = new DataTable();
            G3.DataSource = new DataTable();

            string[] Used_Tbl = { "Reveal_real_currency_Tbl", "Reveal_real_currency_Tbl1", "Reveal_real_currency_Tbl2", "Reveal_real_currency_Tbl3", "term_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

      

        

          
        }

            







        }
      


       

     

      

 
    




   


      


     
   
      

       
 
    


    
