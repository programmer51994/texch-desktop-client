﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using System.Linq;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Buy_Sal_Exch : Form
    {
        #region Defintion

        BindingSource _bs_GrdCur_Tot = new BindingSource();
        BindingSource _bs_GrdCur_nrec = new BindingSource();
        BindingSource _bs_GrdCur_Det = new BindingSource();
        BindingSource _bs_T_id_Tbl = new BindingSource();
        bool Change_cur = false;
        bool Change_det = false;
        string @format = "dd/MM/yyyy";
        #endregion
        public Reveal_Buy_Sal_Exch()
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            GrdCur_Tot.AutoGenerateColumns = false;
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #endregion

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            string[] Used_Tbl = { "Exchang_Buy_Sale_tbl", "Exchang_Buy_Sale_tbl1", "Exchang_Buy_Sale_tbl2" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }

            Change_cur = false;
            int from_nrec_date = 0;
            int to_nrec_date = 0;
            if (TxtFromDate.Checked == true)
            {

                DateTime date = TxtFromDate.Value.Date;
                from_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;


            }
            else
            {
                from_nrec_date = 0;
            }
            if (TxtToDate.Checked == true)
            {

                DateTime date = TxtToDate.Value.Date;
                to_nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
            }
            else
            {
                to_nrec_date = 0;
            }

            try
            {

                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "Reveal_Buy_Sale_Exchange";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@from_Nrec_Date", from_nrec_date);
                connection.SQLCMD.Parameters.AddWithValue("@to_nrec_date", to_nrec_date);
                connection.SQLCMD.Parameters.AddWithValue("@T_id", CboCust_Id.SelectedValue);
                connection.SQLCMD.Parameters.AddWithValue("@lang_id", connection.Lang_id);
                

                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Exchang_Buy_Sale_tbl");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Exchang_Buy_Sale_tbl1");
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Exchang_Buy_Sale_tbl2");
                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                if (connection.SQLDS.Tables["Exchang_Buy_Sale_tbl"].Rows.Count > 0)
                {
                    Btn_Ok.Enabled = true ;
                    Btn_Excel.Enabled = true;
                    _bs_GrdCur_Tot.DataSource = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl"];
                    GrdCur_Tot.DataSource = _bs_GrdCur_Tot;
                    Change_cur = true;
                    GrdCur_Tot_SelectionChanged(null, null);
                }
                else
                {
                    MessageBox.Show("لا توجد بيانات  تحقق الشروط" + (Char)Keys.Enter + " No record For this Condition");
                    Change_cur = false;
                    Change_det = false; 
                    GrdCur_Det.DataSource = new BindingSource();
                    GrdCur_nrec.DataSource = new BindingSource();
                    GrdCur_Tot.DataSource = new BindingSource();
                    Btn_Ok.Enabled = false;
                    Btn_Excel.Enabled = false;
                    return;
                }

            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }


            connection.SQLCMD.Parameters.Clear();
            

        }

        private void Reveal_Buy_Sal_Exch_Load(object sender, EventArgs e)
        {
            TxtFromDate.Format = DateTimePickerFormat.Custom;
            TxtFromDate.CustomFormat = @format;

            TxtFromDate.DataBindings.Clear();
            TxtFromDate.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl1"], "From_Date");
           
            TxtToDate.Format = DateTimePickerFormat.Custom;
            TxtToDate.CustomFormat = @format;

            GrdCur_Tot.AutoGenerateColumns = false;
            GrdCur_nrec.AutoGenerateColumns = false;
            GrdCur_Det.AutoGenerateColumns = false;
           
            //TxtToDate.CustomFormat = " ";
            Btn_Ok.Enabled = false;
            Btn_Excel.Enabled = false;
            string Sql_Text = " select B.T_id,A.ACUST_NAME  as Acust_name , A.ECUST_NAME  as Ecust_name    "
                       + "  From CUSTOMERS A, TERMINALS B   "
                       + "  Where A.CUST_ID = B.CUST_ID "
                       + "  And A.Cust_Flag <>  0 "
                       + " union "
                       + " select 0 as T_id,'--Selected--' as Acust_name , '--Selected--'  as Ecust_name "
                       + " order by T_id ";

            _bs_T_id_Tbl.DataSource = connection.SqlExec(Sql_Text, "cust_TBL");
            CboCust_Id.DataSource = _bs_T_id_Tbl;
            CboCust_Id.DisplayMember = connection.Lang_id == 1 ? "Acust_name" : "ECUST_NAME";
            CboCust_Id.ValueMember = "T_id";
        }

        private void GrdCur_Tot_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_cur)
            {
                try
                {
                    Change_det = false;
                    _bs_GrdCur_nrec.DataSource = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl1"].Select("FOR_CUR_ID =" + Convert.ToInt32(((DataRowView)_bs_GrdCur_Tot.Current).Row["FOR_CUR_ID"])).CopyToDataTable();
                    GrdCur_nrec.DataSource = _bs_GrdCur_nrec;
                    Change_det = true;
                    GrdCur_nrec_SelectionChanged(null, null);
                }
                catch 
                {
                    GrdCur_nrec.DataSource = new BindingSource();
                    GrdCur_Det.DataSource = new BindingSource();
                }
            }
        }

        private void GrdCur_nrec_SelectionChanged(object sender, EventArgs e)
        {
            if (Change_det)
            {

               
                    int for_cur_id = Convert.ToInt32(((DataRowView)_bs_GrdCur_Tot.Current).Row["FOR_CUR_ID"]);
                    int nrec_date = Convert.ToInt32(((DataRowView)_bs_GrdCur_nrec.Current).Row["nrec_date"]);


                    _bs_GrdCur_Det.DataSource = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl2"].Select("FOR_CUR_ID =" + for_cur_id + "and nrec_date = " + nrec_date).CopyToDataTable();
                    GrdCur_Det.DataSource = _bs_GrdCur_Det;
                    LblRec.Text = connection.Records(_bs_GrdCur_Det);
              
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtFromDate_ValueChanged(object sender, EventArgs e)
        {
            //if (TxtFromDate.Checked == true)
            //{
            //    TxtFromDate.Format = DateTimePickerFormat.Custom;
            //    TxtFromDate.CustomFormat = @format;
            //}
            //else
            //{
            //    TxtFromDate.Format = DateTimePickerFormat.Custom;
            //    TxtFromDate.CustomFormat = " ";
            //}
        }

        private void TxtToDate_ValueChanged(object sender, EventArgs e)
        {
            //if (TxtToDate.Checked == true)
            //{
            //    TxtToDate.Format = DateTimePickerFormat.Custom;
            //    TxtToDate.CustomFormat = @format;

            //}
            //else
            //{
            //    TxtToDate.Format = DateTimePickerFormat.Custom;
            //    TxtToDate.CustomFormat = " ";
            //}
        }

        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            if (GrdCur_Tot.Rows.Count > 0)
            {
                DataTable Export_DT_tot = new DataTable();
                Export_DT_tot = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl"].DefaultView.ToTable(false, "for_Cur_id", connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName", "Total_Buy",
                "Total_Sale", "notes_tot_details").Select().CopyToDataTable();
                DataGridView[] Export_GRD = { GrdCur_Tot};
                DataTable[] Export_DT = { Export_DT_tot };
                MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Btn_Excel_Click(object sender, EventArgs e)
        {
                if (GrdCur_Tot.Rows.Count > 0)
            {
                DataTable Export_DT_Tot = new DataTable();
                DataTable Export_DT_nrec = new DataTable();
                DataTable Export_DT_nrec_1 = new DataTable();
                DataTable Export_Dt_Det = new DataTable();
                DataTable Export_Dt_Det_1 = new DataTable();
         
      
            int for_cur_id = Convert.ToInt16(((DataRowView)_bs_GrdCur_Tot.Current).Row["for_Cur_id"]);


            Export_DT_Tot = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl"].DefaultView.ToTable(false, "for_Cur_id", connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName", "Total_Buy",
                "Total_Sale", "notes_tot_details").Select("for_Cur_id = " +  for_cur_id ).CopyToDataTable();


            Export_DT_nrec = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl1"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName", "Nrec_date1",
            "sum_buy_for_amount", "Buy_tot", "sum_Sale_for_amount", "Sale_tot", "note_details", "for_cur_id").Select("for_Cur_id = " + for_cur_id).CopyToDataTable();
            Export_DT_nrec_1 = Export_DT_nrec.DefaultView.ToTable(false, connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName", "Nrec_date1",
            "sum_buy_for_amount", "Buy_tot", "sum_Sale_for_amount", "Sale_tot", "note_details").Select().CopyToDataTable();


            Export_Dt_Det = connection.SQLDS.Tables["Exchang_Buy_Sale_tbl2"].DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "Nrec_date1",
            "FOR_AMOUNT", "REAL_PRICE", "VO_NO", "Exch_Price", "Loc_amount", "NOTES", "for_cur_id").Select("for_Cur_id = " + for_cur_id).CopyToDataTable();
            Export_Dt_Det_1 = Export_Dt_Det.DefaultView.ToTable(false, connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME", "Nrec_date1",
            "FOR_AMOUNT", "REAL_PRICE", "VO_NO", "Exch_Price", "Loc_amount", "NOTES").Select().CopyToDataTable();

            DataGridView[] Export_GRD = { GrdCur_Tot, GrdCur_nrec, GrdCur_Det };
            DataTable[] Export_DT = { Export_DT_Tot, Export_DT_nrec_1, Export_Dt_Det_1};
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
             }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد تفاصيل للتصدير" : "No Data To export ", MyGeneral_Lib.LblCap);
                return;
            }
        

        }
    }
}