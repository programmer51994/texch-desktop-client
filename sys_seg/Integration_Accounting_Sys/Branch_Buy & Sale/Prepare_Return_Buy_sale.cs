﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Prepare_Return_Buy_sale : Form
    {
        #region MyRegion
        string Sql_Text = "";
        int Oper_Id = 0;
        Int16 Record_Flag = 0;
        string Nrec_Date = "";
        string Vo_No = "";
        bool Change = false;
        DataTable Dt = new DataTable();
        private BindingSource _Bs = new BindingSource();
         BindingSource _Bs_Return = new BindingSource();
         string @format = "dd/MM/yyyy";
         string S_doc_exp_null = "";
         string @format_NULL = "0000/00/00";
         
        #endregion
        public Prepare_Return_Buy_sale()
        {
            InitializeComponent();
           
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            GrdVou_Details.AutoGenerateColumns = false;

        }
        //---------------------------------------------------
        private void Prepare_Return_Buy_sale_Load(object sender, EventArgs e)
        {
         

            Change = false;
            Sql_Text = " Select Oper_id , Aoper_name , Eoper_name from Operations where oper_id in (12,13,18,19,47) "
                    + " Union "
                    + " Select 18 As oper_id , 'قيد يوميه بيع العملات الاجنبية لاكثر من ثانوي ' As Aoper_name ,'daily record sell foreign currencies' as Eoper_name"
                    + " union "
                    + " Select 19 As oper_id , 'قيد يوميه شراء العملات الاجنبية لاكثر من ثانوي ' As Aoper_name ,'daily record Buy foreign currencies' as Eoper_name ";


            CboOper_Id.DataSource = connection.SqlExec(Sql_Text, "Oper_TBL");
            CboOper_Id.DisplayMember = connection.Lang_id == 1 ? "Aoper_name" : "Eoper_name";
            CboOper_Id.ValueMember = "Oper_id";
            if (connection.SQLDS.Tables["Oper_TBL"].Rows.Count > 0)
            {
                Sql_Text = " Select Cast(Cast(MAX(Nrec_date) As Varchar(8)) As Date)    As Nrec_date  from Daily_Opening "
                           + " where  R_Close_Flag = 0 "
                           + "  and  Acc_Close_Flag = 0 "
                           + "  and  ACC_Close_Date  is null ";
                connection.SqlExec(Sql_Text, "Nrec_Date_Tbl");
               
                Change = true;
                CboOper_Id_SelectedIndexChanged(null, null);
            }
        }
        //---------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Txt_Vo_No.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد رقم السند" : "Insert Vo. No. Please", MyGeneral_Lib.LblCap);
                Txt_Vo_No.Focus();
                return;
            }
      

                DateTime date = TxtNrec_Date.Value.Date;
                Nrec_Date = (date.Day + date.Month * 100 + date.Year * 10000).ToString();


          
              //Nrec_Date = (TxtNrec_Date.Text);
            if (TxtNrec_Date.Text == "" || TxtNrec_Date.Text == "0000/00/00" || TxtNrec_Date.Text == "00/00/0000"||Nrec_Date == "0" || Nrec_Date == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ  القيد" : "Please Insert Issue Date of formal Document", MyGeneral_Lib.LblCap);
                TxtNrec_Date.Focus();
                return;
            }
            #endregion

            Vo_No = Txt_Vo_No.Text;
            Oper_Id = Convert.ToInt16(CboOper_Id.SelectedValue);


            object[] Sparam = { Nrec_Date, connection.T_ID, 
                                  0,0,0,0,Vo_No,Vo_No,0,Oper_Id ,0,0,0};

            Dt = (DataTable)connection.SqlExec("Main_Vouchers", "Main_Voucher_Tbl", Sparam);
            try
            {

                Dt = connection.SQLDS.Tables["Main_Voucher_Tbl"];
                if (Convert.ToInt16(connection.SQLDS.Tables["Main_Voucher_Tbl"].Rows[0]["Record_flag"]) == 0)
                {
                    BtnOk.Text = connection.Lang_id ==1?"تهيئة":"Prepare";
                }
                else
                {
                    BtnOk.Text =connection.Lang_id ==1? "الغاء التهيئة ":"Cancel Prepare";
                }
            }
            catch
            {
                Dt = new DataTable();
            }

            if (Dt.Rows.Count <= 0)
            {
                GrdVou_Details.DataSource = new BindingSource();
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط" : "No Record for this Conditions", MyGeneral_Lib.LblCap);
                return;
            }

            _Bs_Return.DataSource = Dt.Select(" FOR_CUR_ID <> Loc_Cur_Id").CopyToDataTable();
            GrdVou_Details.DataSource = _Bs_Return;
            if (Dt.Rows.Count > 0)
            {
                GrdVou_Details_SelectionChanged(null, null);
            }
        }
        //---------------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (GrdVou_Details.RowCount < 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود " : "No Record ", MyGeneral_Lib.LblCap);
                return;
            }
            Record_Flag = Convert.ToInt16(connection.SQLDS.Tables["Main_Voucher_Tbl"].Rows[0]["Record_flag"]);
            Record_Flag = (short)(Record_Flag ^ 1);
            connection.SQLCMD.Parameters.AddWithValue("@Vo_No", Vo_No);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", Nrec_Date);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", Oper_Id);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Record_Flag", Record_Flag);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Modify_Voucher", connection.SQLCMD);
            int VO_NO = 0;
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Txt_Vo_No.ResetText();
           // TxtNrec_Date.Text = "00000000";
            GrdVou_Details.DataSource = new BindingSource();
            Change = false;
            CboOper_Id.SelectedIndex = 0;
            BtnOk.Text = "تهيئة";
            LblRec.Text = connection.Records(new BindingSource());
        }
        //---------------------------------------------------
        private void GrdVou_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_Return);

        }
        //---------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //---------------------------------------------------
        private void Prepare_Return_Buy_sale_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
           
            string[] Used_Tbl = { "Main_Voucher_Tbl", "Oper_TBL", "Nrec_Date_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------------------------------------
        private void CboOper_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change)
            {

                if (Convert.ToInt16(CboOper_Id.SelectedValue) == 47)
                {
                    TxtNrec_Date.Enabled = true;
                    TxtNrec_Date.Format = DateTimePickerFormat.Custom;
                    TxtNrec_Date.CustomFormat = S_doc_exp_null;
                }
                else
                {
                    TxtNrec_Date.Format = DateTimePickerFormat.Custom;
                    TxtNrec_Date.CustomFormat = @format;
                    TxtNrec_Date.Text = connection.SQLDS.Tables["Nrec_Date_Tbl"].Rows[0]["Nrec_date"].ToString();
                    TxtNrec_Date.Enabled = false;
                    
                }
            }
        }

        private void Prepare_Return_Buy_sale_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

       
    }
}