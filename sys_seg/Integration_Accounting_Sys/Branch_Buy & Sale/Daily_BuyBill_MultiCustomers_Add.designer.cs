﻿namespace Integration_Accounting_Sys
{
    partial class Daily_BuyBill_MultiCustomers_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Grd_Cust_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Cur_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Acc_Id = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TxtCurr_Name = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Txt_Cust = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_Acc = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Details = new System.Windows.Forms.TextBox();
            this.Cbo_Cust_Type = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Ok = new System.Windows.Forms.Button();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.Grd_Rec_Daily = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.Cbo_AddType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.CboCatg_Id = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtMax_Price = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtMin_Price = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtExchange_Rate = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtRealPrice = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txtfor_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rec_Daily)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Grd_Cust_Id
            // 
            this.Grd_Cust_Id.AllowUserToAddRows = false;
            this.Grd_Cust_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Cust_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust_Id.ColumnHeadersHeight = 40;
            this.Grd_Cust_Id.ColumnHeadersVisible = false;
            this.Grd_Cust_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.Grd_Cust_Id.Location = new System.Drawing.Point(682, 61);
            this.Grd_Cust_Id.Name = "Grd_Cust_Id";
            this.Grd_Cust_Id.ReadOnly = true;
            this.Grd_Cust_Id.RowHeadersVisible = false;
            this.Grd_Cust_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust_Id.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_Cust_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cust_Id.Size = new System.Drawing.Size(327, 92);
            this.Grd_Cust_Id.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Cust_Id";
            this.dataGridViewTextBoxColumn5.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 128;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ACust_Name";
            this.dataGridViewTextBoxColumn6.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 210;
            // 
            // Grd_Cur_Id
            // 
            this.Grd_Cur_Id.AllowUserToAddRows = false;
            this.Grd_Cur_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Cur_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cur_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Cur_Id.ColumnHeadersHeight = 40;
            this.Grd_Cur_Id.ColumnHeadersVisible = false;
            this.Grd_Cur_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.Grd_Cur_Id.Location = new System.Drawing.Point(391, 61);
            this.Grd_Cur_Id.Name = "Grd_Cur_Id";
            this.Grd_Cur_Id.ReadOnly = true;
            this.Grd_Cur_Id.RowHeadersVisible = false;
            this.Grd_Cur_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cur_Id.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Cur_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Cur_Id.Size = new System.Drawing.Size(275, 92);
            this.Grd_Cur_Id.TabIndex = 7;
            this.Grd_Cur_Id.SelectionChanged += new System.EventHandler(this.Grd_Cur_Id_SelectionChanged_1);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Cur_Id";
            this.dataGridViewTextBoxColumn3.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 128;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Cur_Aname";
            this.dataGridViewTextBoxColumn4.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 210;
            // 
            // Grd_Acc_Id
            // 
            this.Grd_Acc_Id.AllowUserToAddRows = false;
            this.Grd_Acc_Id.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_Acc_Id.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Acc_Id.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Acc_Id.ColumnHeadersHeight = 40;
            this.Grd_Acc_Id.ColumnHeadersVisible = false;
            this.Grd_Acc_Id.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.Grd_Acc_Id.Location = new System.Drawing.Point(12, 61);
            this.Grd_Acc_Id.Name = "Grd_Acc_Id";
            this.Grd_Acc_Id.ReadOnly = true;
            this.Grd_Acc_Id.RowHeadersVisible = false;
            this.Grd_Acc_Id.RowHeadersWidth = 15;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Acc_Id.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Acc_Id.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Acc_Id.Size = new System.Drawing.Size(350, 92);
            this.Grd_Acc_Id.TabIndex = 5;
            this.Grd_Acc_Id.SelectionChanged += new System.EventHandler(this.Grd_Acc_Id_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 138;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Acc_Aname";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم الحساب";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 210;
            // 
            // TxtCurr_Name
            // 
            this.TxtCurr_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurr_Name.Location = new System.Drawing.Point(461, 38);
            this.TxtCurr_Name.MaxLength = 49;
            this.TxtCurr_Name.Name = "TxtCurr_Name";
            this.TxtCurr_Name.Size = new System.Drawing.Size(205, 23);
            this.TxtCurr_Name.TabIndex = 6;
            this.TxtCurr_Name.TextChanged += new System.EventHandler(this.TxtCurr_Name_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(388, 42);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(67, 14);
            this.label15.TabIndex = 600;
            this.label15.Text = "العملــــــــة";
            // 
            // Txt_Cust
            // 
            this.Txt_Cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cust.Location = new System.Drawing.Point(747, 38);
            this.Txt_Cust.MaxLength = 149;
            this.Txt_Cust.Name = "Txt_Cust";
            this.Txt_Cust.Size = new System.Drawing.Size(262, 23);
            this.Txt_Cust.TabIndex = 8;
            this.Txt_Cust.TextChanged += new System.EventHandler(this.Txt_Cust_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(679, 42);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(62, 14);
            this.label14.TabIndex = 598;
            this.label14.Text = "الــثـانــوي";
            // 
            // Txt_Acc
            // 
            this.Txt_Acc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Acc.Location = new System.Drawing.Point(92, 38);
            this.Txt_Acc.MaxLength = 199;
            this.Txt_Acc.Name = "Txt_Acc";
            this.Txt_Acc.Size = new System.Drawing.Size(270, 23);
            this.Txt_Acc.TabIndex = 4;
            this.Txt_Acc.TextChanged += new System.EventHandler(this.Txt_Acc_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(15, 42);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(71, 14);
            this.label9.TabIndex = 596;
            this.label9.Text = "الحســــــاب";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(603, 8);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(94, 14);
            this.label3.TabIndex = 595;
            this.label3.Text = "العملة المحلية:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(702, 4);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(143, 23);
            this.Txt_Loc_Cur.TabIndex = 2;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(386, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(198, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(2, 4);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(297, 23);
            this.TxtTerm_Name.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(863, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 587;
            this.label4.Text = "التاريـخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(918, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(91, 23);
            this.TxtIn_Rec_Date.TabIndex = 3;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(5, 30);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1010, 1);
            this.flowLayoutPanel9.TabIndex = 589;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(305, 8);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 586;
            this.label1.Text = "المستخــدم:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(13, 225);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(67, 14);
            this.label13.TabIndex = 610;
            this.label13.Text = "الـتفاصيـل:";
            // 
            // Txt_Details
            // 
            this.Txt_Details.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Details.Location = new System.Drawing.Point(83, 221);
            this.Txt_Details.MaxLength = 199;
            this.Txt_Details.Name = "Txt_Details";
            this.Txt_Details.Size = new System.Drawing.Size(512, 22);
            this.Txt_Details.TabIndex = 18;
            // 
            // Cbo_Cust_Type
            // 
            this.Cbo_Cust_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Cust_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Cust_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Cust_Type.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Cust_Type.FormattingEnabled = true;
            this.Cbo_Cust_Type.Location = new System.Drawing.Point(342, 165);
            this.Cbo_Cust_Type.Name = "Cbo_Cust_Type";
            this.Cbo_Cust_Type.Size = new System.Drawing.Size(252, 22);
            this.Cbo_Cust_Type.TabIndex = 11;
            this.Cbo_Cust_Type.SelectedIndexChanged += new System.EventHandler(this.Cbo_Cust_Type_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(245, 169);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(86, 14);
            this.label12.TabIndex = 609;
            this.label12.Text = "الـحـالــــــــــــة:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(245, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 14);
            this.label10.TabIndex = 608;
            this.label10.Text = "السعر الفعلي:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(13, 197);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(63, 14);
            this.label16.TabIndex = 607;
            this.label16.Text = "المــبــلــغ:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(433, 197);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 14);
            this.label11.TabIndex = 613;
            this.label11.Text = "سعر التعادل:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(607, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 14);
            this.label5.TabIndex = 615;
            this.label5.Text = "الحد الادنى للشراء:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(809, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 14);
            this.label6.TabIndex = 617;
            this.label6.Text = "الحد الاعلى للشراء:";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(507, 553);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(99, 28);
            this.ExtBtn.TabIndex = 23;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 550);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1011, 1);
            this.flowLayoutPanel3.TabIndex = 624;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(5, 268);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1011, 1);
            this.flowLayoutPanel2.TabIndex = 623;
            // 
            // Btn_Ok
            // 
            this.Btn_Ok.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ok.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ok.Location = new System.Drawing.Point(408, 553);
            this.Btn_Ok.Name = "Btn_Ok";
            this.Btn_Ok.Size = new System.Drawing.Size(99, 28);
            this.Btn_Ok.TabIndex = 22;
            this.Btn_Ok.Text = "موافق";
            this.Btn_Ok.UseVisualStyleBackColor = true;
            this.Btn_Ok.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(919, 238);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(90, 27);
            this.BtnDel.TabIndex = 20;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(829, 238);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(90, 27);
            this.BtnAdd.TabIndex = 19;
            this.BtnAdd.Text = "اضــافــة";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // Grd_Rec_Daily
            // 
            this.Grd_Rec_Daily.AllowUserToAddRows = false;
            this.Grd_Rec_Daily.AllowUserToDeleteRows = false;
            this.Grd_Rec_Daily.AllowUserToOrderColumns = true;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Grd_Rec_Daily.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Rec_Daily.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.Grd_Rec_Daily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Grd_Rec_Daily.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.Grd_Rec_Daily.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Rec_Daily.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Rec_Daily.ColumnHeadersHeight = 40;
            this.Grd_Rec_Daily.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column6,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column7,
            this.Column8,
            this.Column11});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Rec_Daily.DefaultCellStyle = dataGridViewCellStyle16;
            this.Grd_Rec_Daily.Location = new System.Drawing.Point(12, 279);
            this.Grd_Rec_Daily.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Grd_Rec_Daily.Name = "Grd_Rec_Daily";
            this.Grd_Rec_Daily.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.Grd_Rec_Daily.RowHeadersWidth = 15;
            this.Grd_Rec_Daily.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grd_Rec_Daily.RowTemplate.Height = 26;
            this.Grd_Rec_Daily.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grd_Rec_Daily.Size = new System.Drawing.Size(997, 262);
            this.Grd_Rec_Daily.TabIndex = 21;
            this.Grd_Rec_Daily.SelectionChanged += new System.EventHandler(this.Grd_Rec_Daily_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "dFor_Amount";
            dataGridViewCellStyle12.Format = "N3";
            dataGridViewCellStyle12.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column1.HeaderText = "الكمية المشتراة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 170;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "cLoc_Amount";
            dataGridViewCellStyle13.Format = "N3";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column2.HeaderText = "المبـلـــــــــغ ـ( ع . م )ـ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 170;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_id";
            this.Column6.HeaderText = "رقم الحساب";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 75;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "ACC_Name";
            this.Column3.HeaderText = "اسم الحساب";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Cur_Name";
            this.Column4.HeaderText = "العملة";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 120;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "cust_Name";
            this.Column5.HeaderText = "الحساب الثانوي";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 120;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Exch_price";
            dataGridViewCellStyle14.Format = "N7";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle14;
            this.Column7.HeaderText = "سعر الصرف";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 120;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "dLoc_amount";
            dataGridViewCellStyle15.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle15;
            this.Column8.HeaderText = "المبلغ المحلي ";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 120;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "notes";
            this.Column11.HeaderText = "التفاصيل";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 250;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 585);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1021, 22);
            this.statusStrip1.TabIndex = 626;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(56, 17);
            this.LblRec.Text = "Records";
            // 
            // Cbo_AddType
            // 
            this.Cbo_AddType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_AddType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_AddType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_AddType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_AddType.FormattingEnabled = true;
            this.Cbo_AddType.Items.AddRange(new object[] {
            "النوع",
            "من حساب",
            "من مذكورين"});
            this.Cbo_AddType.Location = new System.Drawing.Point(730, 165);
            this.Cbo_AddType.Name = "Cbo_AddType";
            this.Cbo_AddType.Size = new System.Drawing.Size(279, 22);
            this.Cbo_AddType.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(603, 169);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(116, 14);
            this.label7.TabIndex = 628;
            this.label7.Text = "نـــــوع الاضافـــــــة :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 31);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 520);
            this.flowLayoutPanel1.TabIndex = 629;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(1015, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 520);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // CboCatg_Id
            // 
            this.CboCatg_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCatg_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCatg_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCatg_Id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCatg_Id.FormattingEnabled = true;
            this.CboCatg_Id.Items.AddRange(new object[] {
            "",
            "بيـــع عمـلات اجنبيـــة",
            "شراء عمـلات اجنبيـــة"});
            this.CboCatg_Id.Location = new System.Drawing.Point(83, 165);
            this.CboCatg_Id.Name = "CboCatg_Id";
            this.CboCatg_Id.Size = new System.Drawing.Size(159, 22);
            this.CboCatg_Id.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 169);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(64, 14);
            this.label8.TabIndex = 631;
            this.label8.Text = "الجهـــــــة:";
            // 
            // TxtMax_Price
            // 
            this.TxtMax_Price.BackColor = System.Drawing.Color.White;
            this.TxtMax_Price.Enabled = false;
            this.TxtMax_Price.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMax_Price.Location = new System.Drawing.Point(934, 193);
            this.TxtMax_Price.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtMax_Price.Name = "TxtMax_Price";
            this.TxtMax_Price.NumberDecimalDigits = 3;
            this.TxtMax_Price.NumberDecimalSeparator = ".";
            this.TxtMax_Price.NumberGroupSeparator = ",";
            this.TxtMax_Price.Size = new System.Drawing.Size(75, 22);
            this.TxtMax_Price.TabIndex = 17;
            this.TxtMax_Price.Text = "0.000";
            // 
            // TxtMin_Price
            // 
            this.TxtMin_Price.BackColor = System.Drawing.Color.White;
            this.TxtMin_Price.Enabled = false;
            this.TxtMin_Price.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMin_Price.Location = new System.Drawing.Point(730, 193);
            this.TxtMin_Price.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtMin_Price.Name = "TxtMin_Price";
            this.TxtMin_Price.NumberDecimalDigits = 3;
            this.TxtMin_Price.NumberDecimalSeparator = ".";
            this.TxtMin_Price.NumberGroupSeparator = ",";
            this.TxtMin_Price.Size = new System.Drawing.Size(76, 22);
            this.TxtMin_Price.TabIndex = 16;
            this.TxtMin_Price.Text = "0.000";
            // 
            // TxtExchange_Rate
            // 
            this.TxtExchange_Rate.BackColor = System.Drawing.Color.White;
            this.TxtExchange_Rate.Enabled = false;
            this.TxtExchange_Rate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExchange_Rate.Location = new System.Drawing.Point(520, 193);
            this.TxtExchange_Rate.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtExchange_Rate.Name = "TxtExchange_Rate";
            this.TxtExchange_Rate.NumberDecimalDigits = 3;
            this.TxtExchange_Rate.NumberDecimalSeparator = ".";
            this.TxtExchange_Rate.NumberGroupSeparator = ",";
            this.TxtExchange_Rate.Size = new System.Drawing.Size(75, 22);
            this.TxtExchange_Rate.TabIndex = 15;
            this.TxtExchange_Rate.Text = "0.000";
            // 
            // TxtRealPrice
            // 
            this.TxtRealPrice.BackColor = System.Drawing.Color.White;
            this.TxtRealPrice.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRealPrice.Location = new System.Drawing.Point(342, 193);
            this.TxtRealPrice.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtRealPrice.Name = "TxtRealPrice";
            this.TxtRealPrice.NumberDecimalDigits = 3;
            this.TxtRealPrice.NumberDecimalSeparator = ".";
            this.TxtRealPrice.NumberGroupSeparator = ",";
            this.TxtRealPrice.Size = new System.Drawing.Size(88, 22);
            this.TxtRealPrice.TabIndex = 14;
            this.TxtRealPrice.Text = "0.000";
            // 
            // Txtfor_Amount
            // 
            this.Txtfor_Amount.BackColor = System.Drawing.Color.White;
            this.Txtfor_Amount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtfor_Amount.Location = new System.Drawing.Point(83, 193);
            this.Txtfor_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txtfor_Amount.Name = "Txtfor_Amount";
            this.Txtfor_Amount.NumberDecimalDigits = 3;
            this.Txtfor_Amount.NumberDecimalSeparator = ".";
            this.Txtfor_Amount.NumberGroupSeparator = ",";
            this.Txtfor_Amount.Size = new System.Drawing.Size(159, 22);
            this.Txtfor_Amount.TabIndex = 13;
            this.Txtfor_Amount.Text = "0.000";
            // 
            // Daily_BuyBill_MultiCustomers_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 607);
            this.Controls.Add(this.CboCatg_Id);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Cbo_AddType);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Btn_Ok);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Grd_Rec_Daily);
            this.Controls.Add(this.TxtMax_Price);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtMin_Price);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtExchange_Rate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtRealPrice);
            this.Controls.Add(this.Txtfor_Amount);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_Details);
            this.Controls.Add(this.Cbo_Cust_Type);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Grd_Cust_Id);
            this.Controls.Add(this.Grd_Cur_Id);
            this.Controls.Add(this.Grd_Acc_Id);
            this.Controls.Add(this.TxtCurr_Name);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Txt_Cust);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_Acc);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Daily_BuyBill_MultiCustomers_Add";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "410";
            this.Text = "Daily_BuyBill_MultiCustomers_Add";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Daily_BuyBill_MultiCustomers_Add_FormClosed);
            this.Load += new System.EventHandler(this.Daily_BuyBill_MultiCustomers_Add_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cur_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Acc_Id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Rec_Daily)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Grd_Cust_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridView Grd_Cur_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridView Grd_Acc_Id;
        private System.Windows.Forms.TextBox TxtCurr_Name;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txt_Cust;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_Acc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Sample.DecimalTextBox TxtRealPrice;
        private System.Windows.Forms.Sample.DecimalTextBox Txtfor_Amount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_Details;
        private System.Windows.Forms.ComboBox Cbo_Cust_Type;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Sample.DecimalTextBox TxtExchange_Rate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Sample.DecimalTextBox TxtMin_Price;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Sample.DecimalTextBox TxtMax_Price;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button Btn_Ok;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.DataGridView Grd_Rec_Daily;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.ComboBox Cbo_AddType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.ComboBox CboCatg_Id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;

    }
}