﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Daily_Buy_BillCurrency_Add : Form
    {
        #region MyRegion
        string Sql_Text = "";
        private BindingSource _Bs = new BindingSource();
        BindingSource _Bs_cbo = new BindingSource();
        bool Change = false;
        bool CustChange = false;
        bool ACCChange = false;
        bool ChangeCbo = true;
        TextBox Txt = new TextBox();
        TextBox Txt1 = new TextBox();
        string _Date = "";
        DateTime C_Date;
        int VO_NO = 0;
        DataTable Daily_Bill = new DataTable();
        int MCRow = 0;
        BindingSource _Bs1 = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        #endregion

        public Daily_Buy_BillCurrency_Add()
        {
            InitializeComponent();
            
          
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, Txt, TxtIn_Rec_Date, TxtTerm_Name);
            Create_Table();
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            Grd_Cust.AutoGenerateColumns = false;

        }
        //------------------------------------------------------------------
        private void Daily_Buy_BillCurrency_Add_Load(object sender, EventArgs e)
        {



            #region Arabic/English
            if (connection.Lang_id != 1)
            {
                Grd_Cust_Id.Columns["Column10"].DataPropertyName = "Ecust_name";
                Grd_Acc_Id.Columns["Column4"].DataPropertyName = "Acc_EName";
            }
            #endregion

            Sql_Text = "SELECT CATG_ID, CATG_ANAME, CATG_ENAME FROM CATG_Tbl";
            Change = true;
            Txt_Acc_TextChanged(sender, e);

            CboCatg_Id.DataSource = connection.SqlExec(Sql_Text, "Cbo_Catg");
            CboCatg_Id.ValueMember = "CATG_ID";
            CboCatg_Id.DisplayMember = connection.Lang_id == 1 ? "CATG_ANAME" : "CATG_ENAME";
            textBox1.Text = Txt_Loc_Cur.Text;
        }
        //-------------------------------------------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                ACCChange = false;
                object[] sparam = { 0, Txt_Acc.Text.Trim(), connection.T_ID, connection.user_id, 0 };
                connection.SqlExec("Get_Acc_Info ", "D_Acc_Name_Tbl", sparam);
                try
                {
                    _Bs1.DataSource = connection.SQLDS.Tables["D_Acc_Name_Tbl"].Select("Sub_Flag > 0").CopyToDataTable();
                }
                catch
                {
                    _Bs1.DataSource = null;
                    _Bs2.DataSource = null;
                    return;
                }

                Grd_Acc_Id.DataSource = _Bs1;

                if (connection.SQLDS.Tables["D_Acc_Name_Tbl"].Rows.Count > 0)
                {
                    ACCChange = true;
                    Grd_Acc_Id_SelectionChanged(sender, e);
                }
                else
                    Grd_Cust_Id.DataSource = null;
            }
        }
        //-------------------------------------------------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                Txt_Cust.Text = "";
                Txt_Cust_TextChanged(sender, e);
            }
        }
        //-------------------------------------------------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                CustChange = false;
                string SqlTxt = "SELECT Acust_Name,Ecust_name,A.Cust_Id "
                                + " From Customers_Accounts A, Customers B "
                                + " Where  A.Cust_Id = B.Cust_Id "
                                + " And A.Cus_State_Id <> 0 "
                                + " And  ACC_Id = " + ((DataRowView)_Bs1.Current).Row["Acc_Id"]
                                + " AND A.T_Id =  " + connection.T_ID
                                + " And Cust_Flag  <> 0 "
                                + " And A.Cur_Id = " + Txt_Loc_Cur.Tag
                                + " And (ACUST_NAME Like '" + Txt_Cust.Text + "%' "
                                            + " Or ECUST_NAME Like '" + Txt_Cust.Text + "%'"
                                            + " Or A.CUST_ID Like '" + Txt_Cust.Text + "%')"
                               + " Order by " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");

                _Bs2.DataSource = connection.SqlExec(SqlTxt, "D_Cust_Name_Tbl");

                Grd_Cust_Id.DataSource = _Bs2;

            }
        }
        //-------------------------------------------------------------------
        private void Grd_Cust_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            if (e.Control is ComboBox && Grd_Cust.CurrentCell.ColumnIndex == Cbo_Cur_Id.Index)
            {
                ComboBox combo = e.Control as ComboBox;
                if (combo != null)
                {
                    combo.SelectedIndexChanged -= new EventHandler(Cbo_Cur_Id_SelectedIndexChanged);

                    // Add the event handler. 
                    combo.SelectedIndexChanged += new System.EventHandler(Cbo_Cur_Id_SelectedIndexChanged);
                }
                e.Control.KeyPress -= TextboxNumeric_KeyPress;
                if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column1.Index || (int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column2.Index)
                {
                    e.Control.KeyPress += TextboxNumeric_KeyPress;
                }


            }
        }
        //-------------------------------------------------------------------
        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == (char)46))
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
        }
        //----------------------------
        private void Cbo_Cur_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChangeCbo)
            {
                ComboBox CboBox = (ComboBox)sender;
                if (CboBox.Text != "System.Data.DataRowView" && CboBox.Text != string.Empty && CboBox.SelectedValue.ToString() != "System.Data.DataRowView")
                {

                    //=========================
                    DataRow[] Row = connection.SQLDS.Tables["Daily_Buy_Sal_Tbl"].Select("For_Cur_id = " + Convert.ToInt16(CboBox.SelectedValue));
                    MCRow = Grd_Cust.CurrentRow.Index;

                    Daily_Bill.Rows[MCRow].SetField("Real_price", Row[0]["Min_B_price"]);
                    Daily_Bill.Rows[MCRow].SetField("Exch_price", Row[0]["Exch_rate"]);
                    Daily_Bill.Rows[MCRow].SetField("Min_price", Row[0]["Min_B_price"]);
                    Daily_Bill.Rows[MCRow].SetField("Max_price", Row[0]["Max_B_price"]);
                    Daily_Bill.Rows[MCRow].SetField("For_cur_id", Row[0]["For_Cur_id"]);
                    Daily_Bill.Rows[MCRow].SetField("ACC_ID", ((DataRowView)_Bs1.Current).Row["ACC_Id"]);
                    Daily_Bill.Rows[MCRow].SetField("Cust_ID", ((DataRowView)_Bs2.Current).Row["Cust_ID"]);
                    Grd_Cust.CurrentRow.Cells[Cbo_Cur_Id.Index].Value = connection.Lang_id == 1 ? Row[0]["Cur_Aname"] : Row[0]["Cur_Ename"];
                    Daily_Bill.Rows[MCRow].SetField("Cur_Code", Row[0]["Cur_code"]);
                    Daily_Bill.Rows[MCRow].SetField("Header", "سعر الشـــراء");
                    Daily_Bill.Rows[MCRow].SetField("EHeader", "Buy Price");
                    Daily_Bill.Rows[MCRow].SetField("Notes", Grd_Cust.CurrentRow.Cells[Column6.Index].Value);
                    Daily_Bill.Rows[MCRow].SetField("V_For_Cur_ANAME", Row[0]["Cur_Aname"]);
                    Daily_Bill.Rows[MCRow].SetField("V_For_Cur_ENAME", Row[0]["Cur_Ename"]);

                    Grd_Cust.Refresh();

                    try
                    {
                        decimal MFor_Amount = ((DataRowView)_Bs.Current).Row["For_Amount"] == DBNull.Value ? 0 : Convert.ToDecimal(((DataRowView)_Bs.Current).Row["For_Amount"]);
                        decimal MReal_Price = ((DataRowView)_Bs.Current).Row["Real_Price"] == DBNull.Value ? 0 : Convert.ToDecimal(((DataRowView)_Bs.Current).Row["Real_Price"]);
                        decimal Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 3);
                        Daily_Bill.Rows[Grd_Cust.CurrentRow.Index].SetField("Loc_amount", Loc_Amount);
                        Grd_Cust.Refresh();

                        Loc_Amount = 0;

                        Loc_Amount = Convert.ToDecimal(Daily_Bill.Compute("Sum(Loc_Amount)", ""));
                        TxtTLoc_Amount.Text = Loc_Amount.ToString();
                        // ------------------الخصم
                    }
                    catch { }
                    TxtMax_Price.DataBindings.Clear();
                    TxtMin_Price.DataBindings.Clear();
                    TxtMin_Price.DataBindings.Add("Text", _Bs, "Min_price");
                    TxtMax_Price.DataBindings.Add("Text", _Bs, "Max_price");
                }


            }

        }
        //------------------------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price","Cur_Code","Header","EHeader",
                        "Cur_ToWord","Cur_ToEWord","V_Ref_No", "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                        "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                        "V_D_ETerm_Name", "V_User_Name"



                         
            };

            string[] DType =
            {
                        "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                        "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                        "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal","System.String","System.String","System.String",
                        "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String",
                        "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String"
                        ,"System.String","System.String","System.String"
            };

            Daily_Bill = CustomControls.Custom_DataTable("Daily_Bill", Column, DType);
            Grd_Cust.DataSource = _Bs;
        }
        //-------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Grd_Acc_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب" : "Select Account please", MyGeneral_Lib.LblCap);
                return;
            }
            if (Grd_Cust_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الثانوي" : "Select Customer Please", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الجهـــة" : "Select Catogary Please", MyGeneral_Lib.LblCap);

                CboCatg_Id.Focus();
                return;
            }
            int Rows = Daily_Bill.Rows.Count;
            if (Rows >= 10)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الحد الاقصى للفاتورة 10 عمليات " : "Max 10 operation", MyGeneral_Lib.LblCap);
                return;
            }

            int Rec_No = (from DataRow row in Daily_Bill.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0 && Daily_Bill.Rows.Count > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود" : "Check record please", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            Txt_Cust.Enabled = false;
            Txt_Acc.Enabled = false;
            Grd_Cust_Id.Enabled = false;
            Grd_Acc_Id.Enabled = false;
            CboCatg_Id.Enabled = false;
            //==================
            int Cust_Id = 0;
            int Acc_id = 0;
            // 
            Cust_Id = ((DataRowView)_Bs2.Current).Row["Cust_Id"] == DBNull.Value ? 0 : Convert.ToInt32(((DataRowView)_Bs2.Current).Row["Cust_Id"]);
            Acc_id = ((DataRowView)_Bs1.Current).Row["Acc_id"] == DBNull.Value ? 0 : Convert.ToInt32(((DataRowView)_Bs1.Current).Row["Acc_Id"]);
            string Sql_Txt2 = "Exec Get_Cur_Buy_Sal " + connection.T_ID + " , " + Cust_Id + " , " + Acc_id;
            _Bs_cbo.DataSource = connection.SqlExec(Sql_Txt2, "Daily_Buy_Sal_Tbl");
            Cbo_Cur_Id.DataSource = _Bs_cbo;
            Cbo_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
            Cbo_Cur_Id.ValueMember = "For_Cur_id";
            //_Bs_cbo.DataSource = connection.SqlExec(Sql_Txt2, "Daily_Buy_Sal_Tbl");
            //=============================

            string V_Catg_Ename = connection.SQLDS.Tables["Cbo_Catg"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string V_Catg_Aname = connection.SQLDS.Tables["Cbo_Catg"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Aname"].ToString();
            DataRow DRow = Daily_Bill.NewRow();
            DRow["V_Acc_AName"] = ((DataRowView)_Bs1.Current).Row["Acc_Aname"];
            DRow["V_Acc_EName"] = ((DataRowView)_Bs1.Current).Row["Acc_Ename"];
            DRow["V_ACUST_NAME"] = ((DataRowView)_Bs2.Current).Row["ACUST_NAME"];
            DRow["V_ECUST_NAME"] = ((DataRowView)_Bs2.Current).Row["ECUST_NAME"];
            //DRow["V_For_Cur_ANAME"] = connection.SQLDS.Tables["Daily_Buy_Sal_Tbl"].Rows[Cbo_Cur_Id.Index]["Cur_AName"];
            //DRow["V_For_Cur_ENAME"] = connection.SQLDS.Tables["Daily_Buy_Sal_Tbl"].Rows[Cbo_Cur_Id.Index]["Cur_EName"];
            DRow["V_Loc_Cur_ANAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"];
            DRow["V_Loc_Cur_ENAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"];
            DRow["V_A_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"];
            DRow["V_E_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"];
            DRow["V_CATG_ANAME"] = V_Catg_Aname;
            DRow["V_CATG_ENAME"] = V_Catg_Ename;
            DRow["V_AOPER_NAME"] = "";
            DRow["V_EOPER_NAME"] = "";
            DRow["V_ACASE_NA"] = "";
            DRow["V_ECASE_NA"] = "";
            DRow["V_S_ATerm_Name"] = "";
            DRow["V_S_ETerm_Name"] = "";
            DRow["V_D_ATerm_Name"] = "";
            DRow["V_D_ETerm_Name"] = "";
            DRow["V_User_Name"] = TxtUser.Text;


            Daily_Bill.Rows.Add(DRow);
            _Bs.DataSource = Daily_Bill;
            Grd_Cust.BeginEdit(true);
        }
        //-------------------------------------------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {

            if (Daily_Bill.Rows.Count > 0)
            {
                DialogResult Dr;

                Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد " + (Grd_Cust.CurrentRow.Index + 1)
                    : "Are you sure you want to delete record " + (Grd_Cust.CurrentRow.Index + 1), MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);


                if (Dr == DialogResult.Yes)
                {
                    TxtTLoc_Amount.ResetText();
                    Daily_Bill.Rows[Grd_Cust.CurrentRow.Index].Delete();
                    if (Grd_Cust.RowCount == 0)
                    {
                        TxtMin_Price.ResetText();
                        TxtMax_Price.ResetText();
                    }
                    TxtTLoc_Amount.Text = Daily_Bill.Compute("Sum(Loc_amount)", "").ToString();
                }

            }
        }
        //-------------------------------------------------------------------
        private void Grd_Cust_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //Daily_Bill.Rows[Grd_Cust.CurrentRow.Index].SetField(e.ColumnIndex, 0.000);
            Grd_Cust.Refresh();
        }
        //-------------------------------------------------------------------
        private void Daily_Buy_BillCurrency_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Cbo_Catg", "D_Acc_Name_Tbl", "D_Cust_Name_Tbl", "Daily_Buy_Sal_Tbl", "Daily_Bill" };
            Change = false;
            CustChange = false;
            ACCChange = false;
            ChangeCbo = false;


            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-------------------------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }

        //-------------------------------------------------------------------
        private void Grd_Cust_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                decimal MFor_Amount = Grd_Cust.CurrentRow.Cells["Column1"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column1"].Value);
                decimal MReal_Price = Grd_Cust.CurrentRow.Cells["Column2"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column2"].Value);
                decimal Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 3);
                if (Loc_Amount != 0)
                {
                    Daily_Bill.Rows[Grd_Cust.CurrentRow.Index].SetField("Loc_amount", Loc_Amount);
                    Grd_Cust.Refresh();
                    Loc_Amount = Convert.ToDecimal(Daily_Bill.Compute("Sum(Loc_amount)", ""));
                    TxtTLoc_Amount.Text = Loc_Amount.ToString();
                }
            }
            catch { };
        }
        //-------------------------------------------------------------------
        private void BackBtn_Click(object sender, EventArgs e)
        {
            ACCChange = false;
            Change = false;
            Daily_Bill.Rows.Clear();
            TxtMax_Price.ResetText();
            TxtMin_Price.ResetText();
            Txt_Acc.ResetText();
            Txt_Cust.ResetText();
            TxtTLoc_Amount.ResetText();
            ACCChange = true;
            Change = true;
            Txt_Cust.Enabled = true;
            Txt_Acc.Enabled = true;
            Grd_Cust_Id.Enabled = true;
            Grd_Acc_Id.Enabled = true;
            CboCatg_Id.Enabled = true;

            Daily_Buy_BillCurrency_Add_Load(sender, e);
        }
        //-------------------------------------------------------------------
        private void Btn_OK_Click(object sender, EventArgs e)
        {

            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No Opening Balance ", MyGeneral_Lib.LblCap);
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No user defined", MyGeneral_Lib.LblCap);
                return;
            }

            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الجهة" : "Select Catagory Please", MyGeneral_Lib.LblCap);
                CboCatg_Id.Focus();
                return;
            }

            if (Daily_Bill.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "No Record", MyGeneral_Lib.LblCap);
                return;
            }

            int Rec_No = (from DataRow row in Daily_Bill.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود" : "Check records Please", MyGeneral_Lib.LblCap);
                return;
            }

            int Recount = 0;

            Recount = (from DataRow row in Daily_Bill.Rows
                       where (decimal)row["Real_price"] > (decimal)row["Max_price"]
                       select row).Count();
            if (Recount > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "السعر الفعلي للشراء اعلى من الحد الاعلى" : " Buy Real price is greater than Upper limit ", MyGeneral_Lib.LblCap);
                return;
            }

            Recount = 0;
            Recount = (from DataRow row in Daily_Bill.Rows
                       where (decimal)row["Real_price"] < (decimal)row["Min_price"]
                       select row).Count();
            if (Recount > 0)
            {
                DialogResult _Dr = MessageBox.Show(connection.Lang_id == 1 ? "سعر الشراء اقل من الحد الادنى هل تريد الاستمرار " : "Buy Price less than the minimum price do you to continue ?", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (_Dr == DialogResult.No)
                {
                    return;
                }
            }
            #endregion
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            ToWord toWord = new ToWord(Convert.ToDouble(TxtTLoc_Amount.Text),
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            Daily_Bill.Rows[MCRow].SetField("Cur_ToWord", Cur_ToWord);
            Daily_Bill.Rows[MCRow].SetField("Cur_ToEWord", Cur_ToEWord);


            DataTable Dt = new DataTable();
            Dt = Daily_Bill.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "V_Ref_No", "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                              "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                              "V_D_ETerm_Name", "V_User_Name"
                );

            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Catg_Id", CboCatg_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@LOC_CUR_ID", Txt_Loc_Cur.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 19);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Frm_Id", 1); //-----قيد يومية شراء عملات اجنبية
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Daily_Voucher_Buy_Sale", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Daily_Bill.Rows.Clear();
            TxtTLoc_Amount.ResetText();
            ACCChange = false;
            Change = false;
            TxtMax_Price.ResetText();
            TxtMin_Price.ResetText();
            Txt_Acc.ResetText();
            Txt_Cust.ResetText();
            TxtTLoc_Amount.ResetText();
            ACCChange = true;
            Change = true;
            Txt_Cust.Enabled = true;
            Txt_Acc.Enabled = true;
            Grd_Cust_Id.Enabled = true;
            Grd_Acc_Id.Enabled = true;
            CboCatg_Id.Enabled = true;
        }
        //-------For Printing-----------
        private void print_Rpt_Pdf()
        {
            C_Date = Convert.ToDateTime(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            string Catg_Ename = connection.SQLDS.Tables["Cbo_Catg"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب الدائن" + ": " +
                              ((DataRowView)_Bs2.Current).Row["Acust_Name"].ToString();
            string ECur_Cust = "Credit Account" + ": " + ((DataRowView)_Bs2.Current).Row["Ecust_Name"].ToString() + " / " +
                                      connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();


            Daily_Buy_Sale_BillCurrency_Rpt ObjRpt = new Daily_Buy_Sale_BillCurrency_Rpt();
            Daily_Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Daily_Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", CboCatg_Id.Text);
            Dt_Param.Rows.Add("Catg_EName", Catg_Ename);
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 19);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("ACur_Cust", ACur_Cust);
            Dt_Param.Rows.Add("ECur_Cust", ECur_Cust);
            DataRow Dr;
            int y = Daily_Bill.Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = Daily_Bill.NewRow();
                Daily_Bill.Rows.Add(Dr);
            }
            connection.SQLDS.Tables.Add(Daily_Bill);

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 19);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Daily_Bill");

        }
    }
}