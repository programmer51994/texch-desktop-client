﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;


namespace Integration_Accounting_Sys
{
    public partial class Sale_BillCurrency_Main : Form
    {
        #region MyRegion
        bool GrdChange = false;
        DataTable Dt_Details = new DataTable();
        DataTable Voucher_Cur = new DataTable();
        DataTable Bill_Cur = new DataTable();
        DataTable Insert_Per_Back = new DataTable();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs_Sale = new BindingSource();
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        #endregion
        //--------------------
        public Sale_BillCurrency_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Currency.AutoGenerateColumns = false;
            GrdCurrency_Details.AutoGenerateColumns = false;
        }
        //--------------------
        private void Buy_BillCurrency_Main_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }
            if (TxtBox_User.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }

            #endregion 
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column3"].DataPropertyName = "catg_Ename";
                Grd_Currency.Columns["Column13"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Details.Columns["Column6"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Details.Columns["Column14"].DataPropertyName = "Ecust_name";

            }

            Getdata(12);
        }
        private void Getdata(int Oper_Id)
        {
            GrdChange = false;
            TxtTLoc_Amount.ResetText();
            Voucher_Cur.Rows.Clear();
            Dt_Details.Rows.Clear();
            _Bs = new BindingSource();

            object[] Sparams = {TxtIn_Rec_Date.Text, connection.T_ID, connection.user_id, 
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo, Gen_Search.Catg_Id, 
                                   Oper_Id, 0, 0, 0 };
            connection.SqlExec("Main_Vouchers", "Bill_VouCher_Tbl", Sparams);
            //==============================
            DataTable _Dt = connection.SQLDS.Tables["Bill_VouCher_Tbl"];
            var query = from row in _Dt.AsEnumerable()
                        where row.Field<Int16>("for_cur_id") == Convert.ToInt16(Txt_Loc_Cur.Tag)
                        && row.Field<int>("Cust_id") != 0
                        group row by new
                        {
                            Vo_No = row.Field<int>("vo_no"),
                            Create_Date = row.Field<DateTime>("C_Date1"),
                            Catg_Aname = row.Field<string>("catg_aname"),
                            Catg_Ename = row.Field<string>("catg_ename"),
                            Nrec_Date = row.Field<int>("nrec_date"),
                            APer_Name = row.Field<string>("APer_Name"),
                            EPer_Name  = row.Field<string>("EPer_Name"),
                            Per_Id = row.Field<int>("Per_Id")

                        } into grp
                        orderby grp.Key.Vo_No
                        select new
                        {
                            Key = grp.Key,
                            Vo_No = grp.Key.Vo_No,
                            Create_Date = grp.Key.Create_Date,
                            Catg_Aname = grp.Key.Catg_Aname,
                            Catg_Ename = grp.Key.Catg_Ename,
                            Nrec_Date = grp.Key.Nrec_Date,
                            APer_Name =  grp.Key.APer_Name,
                            EPer_Name =  grp.Key.EPer_Name,
                            Per_Id = grp.Key.Per_Id,
                            Loc_Amount = Math.Abs(grp.Sum(r => r.Field<decimal>("loc_amount")))
                         
                        };

            Voucher_Cur = CustomControls.IEnumerableToDataTable(query);

            _Bs_Sale.DataSource = Voucher_Cur;

            Grd_Cust.DataSource = _Bs_Sale;
            if (Voucher_Cur.Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Cust_SelectionChanged(null, null);
                //------------
                TxtTLoc_Amount.Text = Voucher_Cur.Compute("Sum(loc_amount)", "").ToString();
            }
            //-------------------
            var Currency_query = from row in _Dt.AsEnumerable()
                                 where row.Field<Int16>("for_cur_id") != Convert.ToInt16(Txt_Loc_Cur.Tag)
                                 group row by new
                                 {
                                     For_Cur_Id = row.Field<Int16>("for_cur_id"),
                                     Cur_Aname = row.Field<string>("cur_aname"),
                                     Cur_Ename = row.Field<string>("cur_ename"),

                                 } into Currency_grp
                                 orderby Currency_grp.Key.For_Cur_Id
                                 select new
                                 {
                                     For_Cur_Id = Currency_grp.Key.For_Cur_Id,
                                     Cur_Aname = Currency_grp.Key.Cur_Aname,
                                     Cur_Ename = Currency_grp.Key.Cur_Ename,
                                     For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount")))
                                 };

            DataTable Dt = CustomControls.IEnumerableToDataTable(Currency_query);
            Grd_Currency.DataSource = Dt;
        }

        //---------------------------Close Form-----------------------------
        private void Sale_BillCurrency_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
            
            string[] Used_Tbl = { "Bill_VouCher_Tbl", "Bill_Cur" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //------------------------------------------------------------------

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Sale_BillCurrency_Add AddFrm = new Sale_BillCurrency_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Getdata(12);
            this.Visible = true;
        }

        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                Dt_Details = new DataTable();
                DataRowView Drv = _Bs_Sale.Current as DataRowView;
                DataRow Dr = Drv.Row;
                int MVo_No = Dr.Field<int>("vo_no");
                int Nrec_Date = Dr.Field<int>("Nrec_Date");
                var Query = from row in connection.SQLDS.Tables["Bill_VouCher_Tbl"].AsEnumerable()
                            where row.Field<int>("Vo_No") == MVo_No && row.Field<int>("Nrec_date") == Nrec_Date && row.Field<Int16>("Loc_Cur_Id") != row.Field<Int16>("For_Cur_Id")
                            select new
                            {
                                For_Amount = Math.Abs(row.Field<decimal>("For_Amount")),
                                Cur_Aname = row.Field<string>("Cur_Aname"),
                                Cur_Ename = row.Field<string>("Cur_Ename"),
                                Real_Price = row.Field<decimal>("Real_Price"),
                                Exch_Price = row.Field<decimal>("Exch_Price"),
                                Loc_Amount = Math.Abs(row.Field<decimal>("Loc_Amount")),
                                Notes = row.Field<string>("Notes"),
                                ACust_Name = row.Field<string>("Acust_name"),
                                ECust_Name = row.Field<string>("Ecust_name")
                            };
                Dt_Details = CustomControls.IEnumerableToDataTable(Query);
                //-----------Discount Amount
                decimal Loc_Amount = (from t in Dt_Details.AsEnumerable()
                                      select decimal.Round((Convert.ToDecimal(t["For_Amount"]) * Convert.ToDecimal(t["Real_Price"])), 3)).Sum();

                decimal Mdiff_Amoun = Loc_Amount % connection.Discount_Amount;
                if (Mdiff_Amoun > (connection.Discount_Amount / 2))
                {
                    Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
                }
                else
                {
                    Mdiff_Amoun = Mdiff_Amoun * -1;
                }
                if (Mdiff_Amoun != 0)
                {
                    DataRow DRow = Dt_Details.NewRow();
                    DRow["For_Amount"] = Mdiff_Amoun;
                    DRow["Cur_Aname"] = Txt_Loc_Cur.Text.Trim();
                    DRow["Real_Price"] = 1;
                    DRow["Exch_Price"] = 1;
                    DRow["Loc_Amount"] = Mdiff_Amoun;
                    DRow["Notes"] = Mdiff_Amoun > 0 ? connection.Lang_id == 1 ? "خصم ممنوح" : "Granted" : connection.Lang_id == 1 ? "خصم مكتسب" : "Earned";
                    Dt_Details.Rows.Add(DRow);
                }
                //---------
                _Bs.DataSource = Dt_Details;
                GrdCurrency_Details.DataSource = _Bs;
                LblRec.Text = connection.Records(_Bs_Sale);
            }

        }
        //------------------------------------------------------------------

        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Getdata(12);
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(12);
            SearchFrm.ShowDialog(this);
            Getdata(12);
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            Create_Table();
            Insert_Per_Back.Rows.Add(new object[]{
                                    Convert.ToInt32(((DataRowView)_Bs_Sale.Current).Row["Per_Id"]),
                                    ((DataRowView)_Bs_Sale.Current).Row["Aper_Name"].ToString(),
                                    ((DataRowView)_Bs_Sale.Current).Row["EPer_Name"].ToString()});

            int Vo_No = Convert.ToInt16(((DataRowView)_Bs_Sale.Current).Row["Vo_No"]);
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + ",12," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt,"Bill_Cur");
            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Bill_Cur"].Rows[0]["C_Date"]);
            string  _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            Box_ACust_Cur = Cur_Code + "/" + "الحساب المدين" + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
            Box_ECust_Cur = "Debit Account : " + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            double Total_Amount = Convert.ToDouble(connection.SQLDS.Tables["Bill_Cur"].Compute("Sum(loc_amount)", "")) + Convert.ToDouble(connection.SQLDS.Tables["Bill_Cur"].Rows[0]["Discount_Amount"]);

            ToWord toWord = new ToWord(Total_Amount,
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            connection.SQLDS.Tables["Bill_Cur"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Bill_Cur"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Bill_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Bill_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            //DataColumn Col = new DataColumn();
            //Col.DataType = System.Type.GetType("System.Int32");
            //Col.ColumnName = "OPer_Id";
            //connection.SQLDS.Tables["Bill_Cur"].Columns.Add(Col);
            //connection.SQLDS.Tables["Bill_Cur"].Rows[0].SetField("Oper_Id", 12);

            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();

            Buy_Sale_BillCurrency_Rpt ObjRpt = new Buy_Sale_BillCurrency_Rpt();
            Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", ((DataRowView)_Bs_Sale.Current).Row["Catg_Aname"].ToString());
            Dt_Param.Rows.Add("Catg_EName", ((DataRowView)_Bs_Sale.Current).Row["Catg_Ename"].ToString());
            Dt_Param.Rows.Add("Vo_No",Vo_No);
            Dt_Param.Rows.Add("Oper_Id", 12);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date); 
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Main");
            Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
            Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
            Dt_Param.Rows.Add("Anote_report", Anote_report);
            Dt_Param.Rows.Add("Enote_report", Enote_report);

            DataRow Dr;
            int y = connection.SQLDS.Tables["Bill_Cur"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Bill_Cur"].NewRow();
                connection.SQLDS.Tables["Bill_Cur"].Rows.Add(Dr);
            }
                connection.SQLDS.Tables.Add(Insert_Per_Back);

                Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 12);

               this.Visible = false;
               RptLangFrm.ShowDialog(this);
               this.Visible = true;
              connection.SQLDS.Tables.Remove("Bill_Cur");
                connection.SQLDS.Tables.Remove("Insert_Per_Back");
        }
        //---------------
        private void Create_Table()
        {
            string[] Column =
            {
                         "Per_Id", "APer_Name", "EPer_Name"
            };
            string[] DType =
            {
                "System.Int32","System.String","System.String"
            };

            Insert_Per_Back = CustomControls.Custom_DataTable("Insert_Per_Back", Column, DType);
        }

        private void Sale_BillCurrency_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Sale_BillCurrency_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        
    }
}