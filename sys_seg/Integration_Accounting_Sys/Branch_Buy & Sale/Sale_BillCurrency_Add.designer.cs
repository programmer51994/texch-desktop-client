﻿namespace Integration_Accounting_Sys
{
    partial class Sale_BillCurrency_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CboCatg_Id = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.Grd_Cust = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cbo_Cur_Id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_browser = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.LblDiscount = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.TxtAmotherName = new System.Windows.Forms.TextBox();
            this.Cbo_Gender = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CboCit_Id = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CboNat_id = new System.Windows.Forms.ComboBox();
            this.TxtNo_Doc = new System.Windows.Forms.TextBox();
            this.TxtIss_Doc = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CboDoc_id = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Txt_Post_code = new System.Windows.Forms.TextBox();
            this.Txt_Street = new System.Windows.Forms.TextBox();
            this.Txt_Suburb = new System.Windows.Forms.TextBox();
            this.TxtA_Address = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.Grd_Per = new System.Windows.Forms.DataGridView();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.Txt_Loc_Cur = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.txt_sin_id = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txt_birth_place = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.resd_cmb = new System.Windows.Forms.ComboBox();
            this.Cbo_Occupation = new System.Windows.Forms.ComboBox();
            this.TxtBirth_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtDoc_Da = new System.Windows.Forms.DateTimePicker();
            this.TxtExp_Da = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_Job_details = new System.Windows.Forms.TextBox();
            this.chk_trans = new System.Windows.Forms.CheckBox();
            this.Txt_EName = new System.Windows.Forms.TextBox();
            this.label_BL = new System.Windows.Forms.Label();
            this.TXTReal_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtDiscount_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtTLoc_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Per)).BeginInit();
            this.SuspendLayout();
            // 
            // CboCatg_Id
            // 
            this.CboCatg_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCatg_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCatg_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCatg_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCatg_Id.FormattingEnabled = true;
            this.CboCatg_Id.Location = new System.Drawing.Point(84, 45);
            this.CboCatg_Id.Name = "CboCatg_Id";
            this.CboCatg_Id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CboCatg_Id.Size = new System.Drawing.Size(182, 24);
            this.CboCatg_Id.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(5, 49);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 194;
            this.label4.Text = "الجهــــــــــة:";
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(516, 603);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(116, 28);
            this.ExtBtn.TabIndex = 23;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(387, 6);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(211, 23);
            this.TxtUser.TabIndex = 1;
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(932, 49);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(90, 23);
            this.BtnDel.TabIndex = 24;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(843, 49);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(90, 23);
            this.BtnAdd.TabIndex = 0;
            this.BtnAdd.Text = "اضــافــة-F9-";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(401, 603);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(116, 28);
            this.BtnOk.TabIndex = 22;
            this.BtnOk.Text = "مـوافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.BackColor = System.Drawing.Color.White;
            this.TxtBox_User.Enabled = false;
            this.TxtBox_User.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_User.Location = new System.Drawing.Point(686, 6);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(161, 23);
            this.TxtBox_User.TabIndex = 2;
            // 
            // Grd_Cust
            // 
            this.Grd_Cust.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Cust.ColumnHeadersHeight = 40;
            this.Grd_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Cbo_Cur_Id,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.btn_browser});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.Format = "N3";
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_Cust.DefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Cust.Location = new System.Drawing.Point(8, 78);
            this.Grd_Cust.Name = "Grd_Cust";
            this.Grd_Cust.RowHeadersWidth = 15;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Cust.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.Grd_Cust.Size = new System.Drawing.Size(1017, 187);
            this.Grd_Cust.TabIndex = 1;
            this.Grd_Cust.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_Cust_CellContentClick);
            this.Grd_Cust.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_Cust_CellValueChanged);
            this.Grd_Cust.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Grd_Cust_DataError);
            this.Grd_Cust.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.Grd_Cust_EditingControlShowing);
            this.Grd_Cust.SelectionChanged += new System.EventHandler(this.Grd_Cust_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "For_Amount";
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = "0.000";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "الكمية";
            this.Column1.Name = "Column1";
            // 
            // Cbo_Cur_Id
            // 
            this.Cbo_Cur_Id.HeaderText = "العملـــــــــة";
            this.Cbo_Cur_Id.Name = "Cbo_Cur_Id";
            this.Cbo_Cur_Id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Cbo_Cur_Id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Real_price";
            dataGridViewCellStyle4.Format = "N7";
            dataGridViewCellStyle4.NullValue = "0.000000";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column2.HeaderText = "السعر الفعلي";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Exch_price";
            dataGridViewCellStyle5.Format = "N7";
            dataGridViewCellStyle5.NullValue = "0.000000";
            this.Column3.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column3.HeaderText = "سعر التعادل";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Loc_amount";
            dataGridViewCellStyle6.Format = "N3";
            dataGridViewCellStyle6.NullValue = "0.000";
            this.Column5.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column5.HeaderText = "المبلغ بالعملة المحلية";
            this.Column5.Name = "Column5";
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Notes";
            this.Column6.HeaderText = "التفاصيل";
            this.Column6.Name = "Column6";
            this.Column6.Width = 250;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Min_Price";
            dataGridViewCellStyle7.Format = "N6";
            dataGridViewCellStyle7.NullValue = "0.000000";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column7.HeaderText = "الحد الادنى للشراء";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Max_Price";
            dataGridViewCellStyle8.Format = "N6";
            dataGridViewCellStyle8.NullValue = "0.000000";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column8.HeaderText = "الحد الاعلى للشراء";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // btn_browser
            // 
            this.btn_browser.HeaderText = "عرض العملة";
            this.btn_browser.Name = "btn_browser";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(10, 6);
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.ReadOnly = true;
            this.TxtTerm_Name.Size = new System.Drawing.Size(252, 23);
            this.TxtTerm_Name.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(860, 9);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 180;
            this.label3.Text = "التاريـخ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(611, 9);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(74, 14);
            this.label2.TabIndex = 184;
            this.label2.Text = "الصــــندوق:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(921, 6);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(101, 23);
            this.TxtIn_Rec_Date.TabIndex = 3;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(3, 35);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1026, 1);
            this.flowLayoutPanel9.TabIndex = 182;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(275, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 14);
            this.label1.TabIndex = 179;
            this.label1.Text = "اسم المستخــدم:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 299);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1026, 1);
            this.flowLayoutPanel1.TabIndex = 203;
            // 
            // LblDiscount
            // 
            this.LblDiscount.AutoSize = true;
            this.LblDiscount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDiscount.ForeColor = System.Drawing.Color.Maroon;
            this.LblDiscount.Location = new System.Drawing.Point(185, 275);
            this.LblDiscount.Name = "LblDiscount";
            this.LblDiscount.Size = new System.Drawing.Size(0, 16);
            this.LblDiscount.TabIndex = 545;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(3, 35);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 264);
            this.flowLayoutPanel7.TabIndex = 555;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(1028, 35);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1, 264);
            this.flowLayoutPanel3.TabIndex = 556;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-3, 600);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1039, 1);
            this.flowLayoutPanel4.TabIndex = 660;
            // 
            // Txt_Name
            // 
            this.Txt_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Name.Location = new System.Drawing.Point(91, 309);
            this.Txt_Name.MaxLength = 149;
            this.Txt_Name.Name = "Txt_Name";
            this.Txt_Name.Size = new System.Drawing.Size(252, 23);
            this.Txt_Name.TabIndex = 1;
            this.Txt_Name.TextChanged += new System.EventHandler(this.Txt_Name_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(25, 313);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(60, 14);
            this.label9.TabIndex = 674;
            this.label9.Text = "ألاســـــم:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 632);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1033, 22);
            this.statusStrip1.TabIndex = 544;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // TxtPhone
            // 
            this.TxtPhone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Location = new System.Drawing.Point(436, 379);
            this.TxtPhone.MaxLength = 15;
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(243, 23);
            this.TxtPhone.TabIndex = 4;
            // 
            // TxtAmotherName
            // 
            this.TxtAmotherName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmotherName.Location = new System.Drawing.Point(779, 405);
            this.TxtAmotherName.MaxLength = 49;
            this.TxtAmotherName.Name = "TxtAmotherName";
            this.TxtAmotherName.Size = new System.Drawing.Size(243, 23);
            this.TxtAmotherName.TabIndex = 8;
            // 
            // Cbo_Gender
            // 
            this.Cbo_Gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Gender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Gender.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Gender.FormattingEnabled = true;
            this.Cbo_Gender.Location = new System.Drawing.Point(436, 404);
            this.Cbo_Gender.Name = "Cbo_Gender";
            this.Cbo_Gender.Size = new System.Drawing.Size(243, 24);
            this.Cbo_Gender.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(347, 409);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 14);
            this.label21.TabIndex = 688;
            this.label21.Text = "الجنـــــــــــس:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.Maroon;
            this.label26.Location = new System.Drawing.Point(348, 335);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(155, 14);
            this.label26.TabIndex = 687;
            this.label26.Text = "المعلومـــات العامــــــــة ....";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(346, 344);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(868, 1);
            this.flowLayoutPanel2.TabIndex = 686;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(349, 383);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 14);
            this.label20.TabIndex = 683;
            this.label20.Text = "الهــاتـــــــــف:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(690, 409);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 14);
            this.label19.TabIndex = 682;
            this.label19.Text = "اســــــــم الام:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(347, 357);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 14);
            this.label12.TabIndex = 681;
            this.label12.Text = "البلد/المدينة:";
            // 
            // CboCit_Id
            // 
            this.CboCit_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCit_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCit_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCit_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCit_Id.FormattingEnabled = true;
            this.CboCit_Id.Location = new System.Drawing.Point(436, 352);
            this.CboCit_Id.Name = "CboCit_Id";
            this.CboCit_Id.Size = new System.Drawing.Size(243, 24);
            this.CboCit_Id.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(345, 435);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 14);
            this.label8.TabIndex = 680;
            this.label8.Text = "المـهنــــــــــــة:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(688, 357);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 14);
            this.label7.TabIndex = 679;
            this.label7.Text = "الجنسيـــــــــة:";
            // 
            // CboNat_id
            // 
            this.CboNat_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboNat_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboNat_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboNat_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNat_id.FormattingEnabled = true;
            this.CboNat_id.Location = new System.Drawing.Point(781, 352);
            this.CboNat_id.Name = "CboNat_id";
            this.CboNat_id.Size = new System.Drawing.Size(243, 24);
            this.CboNat_id.TabIndex = 3;
            // 
            // TxtNo_Doc
            // 
            this.TxtNo_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNo_Doc.Location = new System.Drawing.Point(93, 516);
            this.TxtNo_Doc.MaxLength = 49;
            this.TxtNo_Doc.Name = "TxtNo_Doc";
            this.TxtNo_Doc.Size = new System.Drawing.Size(157, 23);
            this.TxtNo_Doc.TabIndex = 12;
            // 
            // TxtIss_Doc
            // 
            this.TxtIss_Doc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIss_Doc.Location = new System.Drawing.Point(352, 516);
            this.TxtIss_Doc.MaxLength = 49;
            this.TxtIss_Doc.Name = "TxtIss_Doc";
            this.TxtIss_Doc.Size = new System.Drawing.Size(157, 23);
            this.TxtIss_Doc.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Maroon;
            this.label22.Location = new System.Drawing.Point(16, 457);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 14);
            this.label22.TabIndex = 703;
            this.label22.Text = "الوثــــــــــــــــيقة....";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(0, 468);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(517, 1);
            this.flowLayoutPanel6.TabIndex = 702;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(263, 548);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 14);
            this.label17.TabIndex = 700;
            this.label17.Text = "تاريـخ النــفـاذ:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(2, 492);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 14);
            this.label13.TabIndex = 701;
            this.label13.Text = "نوع الوثيقـــــة:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(2, 520);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 14);
            this.label14.TabIndex = 699;
            this.label14.Text = "رقم الوثيقــــة:";
            // 
            // CboDoc_id
            // 
            this.CboDoc_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboDoc_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDoc_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboDoc_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDoc_id.FormattingEnabled = true;
            this.CboDoc_id.Location = new System.Drawing.Point(93, 487);
            this.CboDoc_id.Name = "CboDoc_id";
            this.CboDoc_id.Size = new System.Drawing.Size(275, 24);
            this.CboDoc_id.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(263, 520);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 14);
            this.label15.TabIndex = 698;
            this.label15.Text = "جـهـة الاصدار:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(5, 548);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 14);
            this.label11.TabIndex = 697;
            this.label11.Text = "تاريخ الاصدار:";
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 516;
            this.lineShape1.X2 = 516;
            this.lineShape1.Y1 = 468;
            this.lineShape1.Y2 = 600;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1033, 654);
            this.shapeContainer1.TabIndex = 706;
            this.shapeContainer1.TabStop = false;
            // 
            // Txt_Post_code
            // 
            this.Txt_Post_code.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Post_code.Location = new System.Drawing.Point(606, 572);
            this.Txt_Post_code.MaxLength = 99;
            this.Txt_Post_code.Name = "Txt_Post_code";
            this.Txt_Post_code.Size = new System.Drawing.Size(268, 23);
            this.Txt_Post_code.TabIndex = 21;
            // 
            // Txt_Street
            // 
            this.Txt_Street.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Street.Location = new System.Drawing.Point(607, 544);
            this.Txt_Street.MaxLength = 99;
            this.Txt_Street.Name = "Txt_Street";
            this.Txt_Street.Size = new System.Drawing.Size(267, 23);
            this.Txt_Street.TabIndex = 20;
            // 
            // Txt_Suburb
            // 
            this.Txt_Suburb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Suburb.Location = new System.Drawing.Point(607, 516);
            this.Txt_Suburb.MaxLength = 99;
            this.Txt_Suburb.Name = "Txt_Suburb";
            this.Txt_Suburb.Size = new System.Drawing.Size(267, 23);
            this.Txt_Suburb.TabIndex = 19;
            // 
            // TxtA_Address
            // 
            this.TxtA_Address.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtA_Address.Location = new System.Drawing.Point(607, 488);
            this.TxtA_Address.MaxLength = 99;
            this.TxtA_Address.Name = "TxtA_Address";
            this.TxtA_Address.Size = new System.Drawing.Size(267, 23);
            this.TxtA_Address.TabIndex = 18;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(520, 576);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 14);
            this.label27.TabIndex = 712;
            this.label27.Text = "الرمز البريدي:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(526, 548);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 14);
            this.label25.TabIndex = 711;
            this.label25.Text = "الزقــــــــــاق:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(526, 520);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 14);
            this.label24.TabIndex = 710;
            this.label24.Text = "الحــــــــــــي:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.Maroon;
            this.label23.Location = new System.Drawing.Point(550, 457);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(111, 14);
            this.label23.TabIndex = 709;
            this.label23.Text = "العنــــــــــــوان ......";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(517, 468);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(521, 1);
            this.flowLayoutPanel8.TabIndex = 708;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(524, 492);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 14);
            this.label18.TabIndex = 707;
            this.label18.Text = "الولايــــــــــة :";
            // 
            // Grd_Per
            // 
            this.Grd_Per.AllowUserToAddRows = false;
            this.Grd_Per.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Per.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_Per.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Per.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_Per.ColumnHeadersHeight = 40;
            this.Grd_Per.ColumnHeadersVisible = false;
            this.Grd_Per.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column20,
            this.Column21});
            this.Grd_Per.Location = new System.Drawing.Point(13, 333);
            this.Grd_Per.Name = "Grd_Per";
            this.Grd_Per.ReadOnly = true;
            this.Grd_Per.RowHeadersVisible = false;
            this.Grd_Per.RowHeadersWidth = 15;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Per.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Per.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Per.Size = new System.Drawing.Size(330, 75);
            this.Grd_Per.TabIndex = 5;
            this.Grd_Per.SelectionChanged += new System.EventHandler(this.Grd_Per_SelectionChanged_1);
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "per_Id";
            dataGridViewCellStyle13.NullValue = "0";
            this.Column20.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column20.HeaderText = "رقم الحساب";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 50;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Per_AName";
            this.Column21.HeaderText = "اسم الحساب";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 300;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(10, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 14);
            this.label5.TabIndex = 199;
            this.label5.Text = "الخصـــــــم:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(741, 275);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(131, 14);
            this.label28.TabIndex = 718;
            this.label28.Text = "المبلغ الواجب قبضــه:";
            // 
            // Txt_Loc_Cur
            // 
            this.Txt_Loc_Cur.BackColor = System.Drawing.Color.White;
            this.Txt_Loc_Cur.Enabled = false;
            this.Txt_Loc_Cur.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Loc_Cur.Location = new System.Drawing.Point(591, 271);
            this.Txt_Loc_Cur.Name = "Txt_Loc_Cur";
            this.Txt_Loc_Cur.Size = new System.Drawing.Size(150, 23);
            this.Txt_Loc_Cur.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(305, 275);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(136, 14);
            this.label6.TabIndex = 201;
            this.label6.Text = "المبلغ بالعملة المحلية:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Maroon;
            this.label77.Location = new System.Drawing.Point(9, 437);
            this.label77.Name = "label77";
            this.label77.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label77.Size = new System.Drawing.Size(107, 14);
            this.label77.TabIndex = 904;
            this.label77.Text = "Label_Text_Msg";
            this.label77.Click += new System.EventHandler(this.label77_Click);
            // 
            // txt_sin_id
            // 
            this.txt_sin_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_sin_id.Location = new System.Drawing.Point(93, 572);
            this.txt_sin_id.MaxLength = 49;
            this.txt_sin_id.Name = "txt_sin_id";
            this.txt_sin_id.Size = new System.Drawing.Size(157, 23);
            this.txt_sin_id.TabIndex = 16;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(1, 576);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 14);
            this.label29.TabIndex = 907;
            this.label29.Text = "الرقم الوطني:";
            // 
            // txt_birth_place
            // 
            this.txt_birth_place.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_birth_place.Location = new System.Drawing.Point(779, 379);
            this.txt_birth_place.MaxLength = 49;
            this.txt_birth_place.Name = "txt_birth_place";
            this.txt_birth_place.Size = new System.Drawing.Size(150, 23);
            this.txt_birth_place.TabIndex = 5;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(687, 383);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 14);
            this.label30.TabIndex = 909;
            this.label30.Text = " الــــــتـــولــــد:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(253, 575);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(93, 14);
            this.label50.TabIndex = 915;
            this.label50.Text = "نوع الاقامـــــــة:";
            // 
            // resd_cmb
            // 
            this.resd_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.resd_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.resd_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resd_cmb.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resd_cmb.FormattingEnabled = true;
            this.resd_cmb.Location = new System.Drawing.Point(352, 571);
            this.resd_cmb.Name = "resd_cmb";
            this.resd_cmb.Size = new System.Drawing.Size(157, 24);
            this.resd_cmb.TabIndex = 17;
            // 
            // Cbo_Occupation
            // 
            this.Cbo_Occupation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Occupation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Occupation.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Occupation.FormattingEnabled = true;
            this.Cbo_Occupation.Location = new System.Drawing.Point(436, 430);
            this.Cbo_Occupation.Name = "Cbo_Occupation";
            this.Cbo_Occupation.Size = new System.Drawing.Size(243, 24);
            this.Cbo_Occupation.TabIndex = 9;
            // 
            // TxtBirth_Da
            // 
            this.TxtBirth_Da.CustomFormat = "dd/mm/yyyy";
            this.TxtBirth_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtBirth_Da.Location = new System.Drawing.Point(932, 380);
            this.TxtBirth_Da.Name = "TxtBirth_Da";
            this.TxtBirth_Da.Size = new System.Drawing.Size(93, 20);
            this.TxtBirth_Da.TabIndex = 7;
            // 
            // TxtDoc_Da
            // 
            this.TxtDoc_Da.CustomFormat = "dd/mm/yyyy";
            this.TxtDoc_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtDoc_Da.Location = new System.Drawing.Point(93, 545);
            this.TxtDoc_Da.Name = "TxtDoc_Da";
            this.TxtDoc_Da.Size = new System.Drawing.Size(157, 20);
            this.TxtDoc_Da.TabIndex = 14;
            // 
            // TxtExp_Da
            // 
            this.TxtExp_Da.CustomFormat = "dd/mm/yyyy";
            this.TxtExp_Da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TxtExp_Da.Location = new System.Drawing.Point(352, 545);
            this.TxtExp_Da.Name = "TxtExp_Da";
            this.TxtExp_Da.Size = new System.Drawing.Size(157, 20);
            this.TxtExp_Da.TabIndex = 15;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(683, 435);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 14);
            this.label16.TabIndex = 921;
            this.label16.Text = "تفاصيل المهنة:";
            // 
            // txt_Job_details
            // 
            this.txt_Job_details.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Job_details.Location = new System.Drawing.Point(778, 431);
            this.txt_Job_details.Name = "txt_Job_details";
            this.txt_Job_details.Size = new System.Drawing.Size(243, 23);
            this.txt_Job_details.TabIndex = 10;
            // 
            // chk_trans
            // 
            this.chk_trans.AutoSize = true;
            this.chk_trans.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk_trans.ForeColor = System.Drawing.Color.Maroon;
            this.chk_trans.Location = new System.Drawing.Point(670, 311);
            this.chk_trans.Name = "chk_trans";
            this.chk_trans.Size = new System.Drawing.Size(103, 18);
            this.chk_trans.TabIndex = 923;
            this.chk_trans.Text = "ترجمة الاسم ";
            this.chk_trans.UseVisualStyleBackColor = true;
            this.chk_trans.CheckedChanged += new System.EventHandler(this.chk_trans_CheckedChanged);
            // 
            // Txt_EName
            // 
            this.Txt_EName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EName.Location = new System.Drawing.Point(344, 309);
            this.Txt_EName.MaxLength = 149;
            this.Txt_EName.Name = "Txt_EName";
            this.Txt_EName.Size = new System.Drawing.Size(323, 23);
            this.Txt_EName.TabIndex = 922;
            //this.Txt_EName.TextChanged += new System.EventHandler(this.Txt_EName_TextChanged);
            // 
            // label_BL
            // 
            this.label_BL.AutoSize = true;
            this.label_BL.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_BL.ForeColor = System.Drawing.Color.Maroon;
            this.label_BL.Location = new System.Drawing.Point(14, 413);
            this.label_BL.Name = "label_BL";
            this.label_BL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_BL.Size = new System.Drawing.Size(146, 14);
            this.label_BL.TabIndex = 924;
            this.label_BL.Text = "قـــــوائـــــــــم الـــمــــنــــع";
            this.label_BL.Click += new System.EventHandler(this.label_BL_Click);
            // 
            // TXTReal_Amount
            // 
            this.TXTReal_Amount.BackColor = System.Drawing.Color.White;
            this.TXTReal_Amount.Enabled = false;
            this.TXTReal_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTReal_Amount.Location = new System.Drawing.Point(871, 271);
            this.TXTReal_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TXTReal_Amount.Name = "TXTReal_Amount";
            this.TXTReal_Amount.NumberDecimalDigits = 3;
            this.TXTReal_Amount.NumberDecimalSeparator = ".";
            this.TXTReal_Amount.NumberGroupSeparator = ",";
            this.TXTReal_Amount.Size = new System.Drawing.Size(148, 23);
            this.TXTReal_Amount.TabIndex = 7;
            this.TXTReal_Amount.Text = "0.000";
            // 
            // TxtDiscount_Amount
            // 
            this.TxtDiscount_Amount.BackColor = System.Drawing.Color.White;
            this.TxtDiscount_Amount.Enabled = false;
            this.TxtDiscount_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscount_Amount.Location = new System.Drawing.Point(84, 271);
            this.TxtDiscount_Amount.Name = "TxtDiscount_Amount";
            this.TxtDiscount_Amount.NumberDecimalDigits = 3;
            this.TxtDiscount_Amount.NumberDecimalSeparator = ".";
            this.TxtDiscount_Amount.NumberGroupSeparator = ",";
            this.TxtDiscount_Amount.Size = new System.Drawing.Size(99, 23);
            this.TxtDiscount_Amount.TabIndex = 4;
            this.TxtDiscount_Amount.Text = "0.000";
            this.TxtDiscount_Amount.TextChanged += new System.EventHandler(this.TxtDiscount_Amount_TextChanged);
            // 
            // TxtTLoc_Amount
            // 
            this.TxtTLoc_Amount.BackColor = System.Drawing.Color.White;
            this.TxtTLoc_Amount.Enabled = false;
            this.TxtTLoc_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTLoc_Amount.Location = new System.Drawing.Point(441, 271);
            this.TxtTLoc_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.TxtTLoc_Amount.Name = "TxtTLoc_Amount";
            this.TxtTLoc_Amount.NumberDecimalDigits = 3;
            this.TxtTLoc_Amount.NumberDecimalSeparator = ".";
            this.TxtTLoc_Amount.NumberGroupSeparator = ",";
            this.TxtTLoc_Amount.Size = new System.Drawing.Size(148, 23);
            this.TxtTLoc_Amount.TabIndex = 5;
            this.TxtTLoc_Amount.Text = "0.000";
            // 
            // Sale_BillCurrency_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 654);
            this.Controls.Add(this.label_BL);
            this.Controls.Add(this.chk_trans);
            this.Controls.Add(this.Txt_EName);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txt_Job_details);
            this.Controls.Add(this.TxtExp_Da);
            this.Controls.Add(this.TxtDoc_Da);
            this.Controls.Add(this.TxtBirth_Da);
            this.Controls.Add(this.Cbo_Occupation);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.resd_cmb);
            this.Controls.Add(this.txt_birth_place);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.txt_sin_id);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.TXTReal_Amount);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Grd_Per);
            this.Controls.Add(this.Txt_Post_code);
            this.Controls.Add(this.Txt_Street);
            this.Controls.Add(this.Txt_Suburb);
            this.Controls.Add(this.TxtA_Address);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.TxtNo_Doc);
            this.Controls.Add(this.TxtIss_Doc);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.CboDoc_id);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.TxtAmotherName);
            this.Controls.Add(this.Cbo_Gender);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CboCit_Id);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CboNat_id);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Txt_Name);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.LblDiscount);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtDiscount_Amount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtTLoc_Amount);
            this.Controls.Add(this.Txt_Loc_Cur);
            this.Controls.Add(this.CboCatg_Id);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.TxtBox_User);
            this.Controls.Add(this.Grd_Cust);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sale_BillCurrency_Add";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "396";
            this.Text = "Sale_BillCurrency_Add";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Sale_BillCurrency_Add_FormClosed);
            this.Load += new System.EventHandler(this.Sale_BillCurrency_Add_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Sale_BillCurrency_Add_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Cust)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Per)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CboCatg_Id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.DataGridView Grd_Cust;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label LblDiscount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.TextBox Txt_Name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.TextBox TxtAmotherName;
        private System.Windows.Forms.ComboBox Cbo_Gender;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CboCit_Id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CboNat_id;
        private System.Windows.Forms.TextBox TxtNo_Doc;
        private System.Windows.Forms.TextBox TxtIss_Doc;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox CboDoc_id;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.TextBox Txt_Post_code;
        private System.Windows.Forms.TextBox Txt_Street;
        private System.Windows.Forms.TextBox Txt_Suburb;
        private System.Windows.Forms.TextBox TxtA_Address;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView Grd_Per;
        private System.Windows.Forms.Sample.DecimalTextBox TXTReal_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtTLoc_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox TxtDiscount_Amount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Txt_Loc_Cur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox txt_sin_id;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txt_birth_place;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox resd_cmb;
        private System.Windows.Forms.ComboBox Cbo_Occupation;
        private System.Windows.Forms.DateTimePicker TxtBirth_Da;
        private System.Windows.Forms.DateTimePicker TxtDoc_Da;
        private System.Windows.Forms.DateTimePicker TxtExp_Da;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewComboBoxColumn Cbo_Cur_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewButtonColumn btn_browser;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_Job_details;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.CheckBox chk_trans;
        private System.Windows.Forms.TextBox Txt_EName;
        private System.Windows.Forms.Label label_BL;
    }
}