﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Return_Cash_Buy_Sale_Add : Form
    {
        #region MyRegion
        string Sql_Text = "";
        private BindingSource _Bs = new BindingSource();
        int Vo_No = 0;
        string Nrec_Date = "";
        int Oper_id = 0;
        string _Date = "";
         DateTime C_Date;
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        decimal Mdiff_Amoun = 0;
        decimal Discount_Amount = 0;
        BindingSource _Bs_return = new BindingSource();
        string @format = "dd/MM/yyyy";
        string @format_NULL = "";
       // string @format_NULL = "0000/00/00";
        #endregion


        public Return_Cash_Buy_Sale_Add()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            GrdVou_Details.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                GrdVou_Details.Columns["Column3"].DataPropertyName = "Cur_Ename";
            }
        }
        //-------------------------------
      
        private void Return_Buy_sale_Load(object sender, EventArgs e)
        {
            TxtNrec_Date.Format = DateTimePickerFormat.Custom;
            TxtNrec_Date.CustomFormat = @format;

            Sql_Text = "Select Oper_Id,AOper_Name,EOper_Name from OPERATIONS Where OPER_ID in(12,13)";
            CboOper_Id.DataSource = connection.SqlExec(Sql_Text, "Oper_Tbl");
            CboOper_Id.DisplayMember = connection.Lang_id == 1 ? "AOper_Name" : "EOper_Name";
            CboOper_Id.ValueMember = "Oper_Id";
        }
        //----------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Txt_Vo_No.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد سند القيد" : "Insert the VO. No. Please", MyGeneral_Lib.LblCap);
                Txt_Vo_No.Focus();
                return;
            }
            DateTime date = TxtNrec_Date.Value.Date;
            Nrec_Date = (date.Day + date.Month * 100 + date.Year * 10000).ToString();

            if (Nrec_Date == "0" || Nrec_Date == "-1")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ السند" : "Please Insert Date of record", MyGeneral_Lib.LblCap);
                TxtNrec_Date.Focus();
                return;
            }
            #endregion

            Vo_No = Convert.ToInt32(Txt_Vo_No.Text);
            //Nrec_Date = TxtNrec_Date.Text;
            Oper_id = Convert.ToInt16(CboOper_Id.SelectedValue);
            //Nrec_Date = MyGeneral_Lib.DateChecking(TxtNrec_Date.Text);

            object[] Sparam = { Convert.ToInt32(Nrec_Date),
                                 connection.T_ID,0,0,0,0,Vo_No,Vo_No,0,Oper_id,
                               0,0,1};
            connection.SqlExec("Main_Vouchers", "Main_Vouchers_Tbl", Sparam);
            if (connection.SQLDS.Tables["Main_Vouchers_Tbl"].Rows.Count <= 0)
            {
                GrdVou_Details.DataSource = new BindingSource();
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قيود تحقق الشروط" : "No. Records", MyGeneral_Lib.LblCap);
                return;
            }
            else
            {
                _Bs_return.DataSource = connection.SQLDS.Tables["Main_Vouchers_Tbl"].Select("For_Cur_id <> Loc_Cur_Id").CopyToDataTable();
                GrdVou_Details.DataSource = _Bs_return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (GrdVou_Details.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قيود" : "No. Records", MyGeneral_Lib.LblCap);
                return;
            }
            connection.SQLCMD.Parameters.AddWithValue("@VO_NO", Vo_No);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", Convert.ToInt32(Nrec_Date));
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", Oper_id);
            connection.SQLCMD.Parameters.AddWithValue("@Cust_Id", TxtBox_User.Tag);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Return_Cash_Buy_Sale", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Vo_No)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Discount_Calculator();
            print_Rpt_Pdf();
            Txt_Vo_No.ResetText();

            CboOper_Id.SelectedIndex = 0;
            _Bs_return.DataSource = new BindingSource();

        }
        //---------------------------------------------
        private void Return_Buy_sale_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Used_Tbl = { "Main_Vouchers_Tbl", "Oper_Tbl", "Return_Bill_Cur" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //----------------------------
        private void GrdVou_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_return);
        }
        //------------------------
        private void Discount_Calculator()
        {
            decimal Loc_Amount = connection.SQLDS.Tables["Main_Vouchers_Tbl"].Compute("Sum(Loc_Amount)", "") == DBNull.Value ? 0 : Convert.ToDecimal(connection.SQLDS.Tables["Main_Vouchers_Tbl"].Compute("Sum(Loc_amount)", ""));
            Mdiff_Amoun = Loc_Amount % connection.Discount_Amount;
            if (Mdiff_Amoun > (connection.Discount_Amount / 2))
            {
                Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
            }
            else
            {
                Mdiff_Amoun = Mdiff_Amoun * -1; //----مكتسب
            }
            Discount_Amount = Mdiff_Amoun;
        }
      //------------------------
        private void print_Rpt_Pdf()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int New_Oper_id = 0;
            if (Oper_id == 12)
                New_Oper_id = 14;//رد بيــــع 
            else
                New_Oper_id = 15;//رد شــــراء
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
           
          //  string SqlTxt = "Exec Main_Report " + Txt_Vo_No.Text + "," + TxtIn_Rec_Date.Text + "," + Oper_id + "," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + "," + New_Oper_id + "," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Return_Bill_Cur");

            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["C_Date"]);

            double Loc_Amount = Convert.ToDouble(connection.SQLDS.Tables["Return_Bill_Cur"].Compute("Sum(Loc_Amount)", ""));
            double Total_Amount = Convert.ToDouble(Convert.ToDouble(Loc_Amount) + Convert.ToDouble(connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Discount_Amount"] == DBNull.Value ? 0 : connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Discount_Amount"])*-1);
            ToWord toWord = new ToWord(Total_Amount,
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";

          
            connection.SQLDS.Tables["Return_Bill_Cur"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Return_Bill_Cur"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            Box_ACust_Cur = Cur_Code + "/" + (Oper_id == 12 ? "الحساب الدائن" : "الحساب المدين") + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
            Box_ECust_Cur = "Credit Account : " + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
            string Catg_Ename = connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Catg_Ename"].ToString();
            string Catg_AEname = connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Catg_Aname"].ToString();
            Return_Buy_Sale_BillCurrency_Rpt ObjRpt = new Return_Buy_Sale_BillCurrency_Rpt();
            Return_Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Return_Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", Catg_AEname);
            Dt_Param.Rows.Add("Catg_EName", Catg_Ename);
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", New_Oper_id);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
            Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
            DataRow Dr;
            int y = connection.SQLDS.Tables["Return_Bill_Cur"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Return_Bill_Cur"].NewRow();
                connection.SQLDS.Tables["Return_Bill_Cur"].Rows.Add(Dr);
            }
          
            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, New_Oper_id);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Return_Bill_Cur");
        }
    }
}