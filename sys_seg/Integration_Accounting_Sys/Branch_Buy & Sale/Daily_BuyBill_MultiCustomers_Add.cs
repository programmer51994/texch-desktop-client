﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Daily_BuyBill_MultiCustomers_Add : Form
    {
        #region MyRegion
        string Sql_Text = "";
        bool Change = false;
        bool ACCChange = false;
        bool CurChange = false;
        bool DCChange = false;
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CurId = new BindingSource();
        BindingSource Bs_CustId = new BindingSource();
        BindingSource _Bs = new BindingSource();
        DataTable Voucher_Cur = new DataTable();
        DataTable Debit_Tbl = new DataTable();
        DataTable Credit_Tbl = new DataTable();
        int For_Cur_Id = 0;
        #endregion
        //--------------------------
        public Daily_BuyBill_MultiCustomers_Add()
        {
            InitializeComponent();
            #region Arabic/English
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id != 1)
            {
                Cbo_AddType.Items.Clear();
                Cbo_AddType.Items.Add("Type");
                Cbo_AddType.Items.Add("From Account");
                Cbo_AddType.Items.Add("From MultiAccount");


                Grd_Acc_Id.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "Acc_Ename";
                Grd_Cur_Id.Columns["dataGridViewTextBoxColumn4"].DataPropertyName = "Cur_Ename";
                Grd_Cust_Id.Columns["dataGridViewTextBoxColumn6"].DataPropertyName = "Ecust_name";
  
            }
            #endregion
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cur_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            Grd_Rec_Daily.AutoGenerateColumns = false;
            Create_Table();
        }
        //--------------------------
        private void Daily_BuyBill_MultiCustomers_Add_Load(object sender, EventArgs e)
        {
            DCChange = false;
            Sql_Text = "select * from Catg_Tbl ";
            CboCatg_Id.DataSource = connection.SqlExec(Sql_Text, "Catg_Tbl");
            CboCatg_Id.DisplayMember = connection.Lang_id == 1 ? "CATG_ANAME" : "CATG_ENAME";
            CboCatg_Id.ValueMember = "CATG_ID";
            //--------------------
            Sql_Text = " SELECT DC_Id, DC_Aname, DC_Ename FROM Debt_Credit where DC_Id <> 0 Order by DC_Id DESC";
            connection.SqlExec(Sql_Text, "DC_Id_tab");

            Debit_Tbl = connection.SQLDS.Tables["DC_Id_tab"].Select("DC_Id = 1").CopyToDataTable();
            Credit_Tbl = connection.SQLDS.Tables["DC_Id_tab"].Select("DC_Id = -1").CopyToDataTable();
            Cbo_Cust_Type.DataSource = Debit_Tbl;
            Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
            Cbo_Cust_Type.ValueMember = "DC_Id";
            if (connection.SQLDS.Tables["DC_Id_tab"].Rows.Count > 0)
            {
                DCChange = true;
                Cbo_Cust_Type_SelectedIndexChanged(sender, e);
            }
            Cbo_AddType.SelectedIndex = 0;
            Change = true;
        }
        //--------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                ACCChange = false;
                CurChange = false;
                Bs_CurId.DataSource = null;
                Bs_CustId.DataSource = null;
                connection.SqlExec("Get_Acc_Info", "Acc_Name_MultiCust_Tbl",
                    new object[] { 0, Txt_Acc.Text.Trim(), connection.T_ID, connection.user_id, 0 });
                try
                {
                    Bs_AccId.DataSource = connection.SQLDS.Tables["Acc_Name_MultiCust_Tbl"].Select("Sub_Flag > 0").CopyToDataTable();
                }
                catch
                {
                    Bs_AccId.DataSource = null;
                }
                Grd_Acc_Id.DataSource = Bs_AccId;

                if (Bs_AccId.List.Count > 0)
                {
                    ACCChange = true;
                    Grd_Acc_Id_SelectionChanged(sender, e);
                }
            }
        }
        //--------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                Txt_Cust.Text = "";
                TxtCurr_Name.Text = "";
                TxtCurr_Name_TextChanged(sender, e);
            }
        }
        //--------------------------
        private void TxtCurr_Name_TextChanged(object sender, EventArgs e)
        {
            if (ACCChange)
            {
                CurChange = false;
                Bs_CustId.DataSource = null;

                Sql_Text = " SELECT Cur_AName,Cur_EName,A.Cur_id,Exch_Rate,Min_B_Price,Max_B_Price  "
                            + " FROM Cur_Tbl A ,CCp_List B  ,Currencies_Rate C "
                            + " 	Where A.Cur_id = B.Cur_id "
                            + " 	And T_Id = " + connection.T_ID
                            + " And B.Cur_ID = C.Cur_ID "
                            + "	And (A.Cur_id like  '" + TxtCurr_Name.Text.Trim() + "%' "
                            + "			Or Cur_AName like '" + TxtCurr_Name.Text.Trim() + "%' "
                            + "			Or Cur_EName like '" + TxtCurr_Name.Text.Trim() + "%' ) "
                            + " And A.Cur_Id > 0 "
                            + " And A.Cur_ID in(Select Cur_ID From CUSTOMERS_ACCOUNTS Where ACC_Id = " + Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]) + " ) "
                            + " And T_ID = " + connection.T_ID
                            + " Order by "+(connection.Lang_id==1?"Cur_AName":"Cur_EName ");
                Bs_CurId.DataSource = connection.SqlExec(Sql_Text, "Cur_Acc_MultiCust_Tbl");

                Grd_Cur_Id.DataSource = Bs_CurId;
                if (connection.SQLDS.Tables["Cur_Acc_MultiCust_Tbl"].Rows.Count > 0)
                {
                    CurChange = true;
                    Grd_Cur_Id_SelectionChanged_1(sender, e);
                }
            }
        }
       
        //--------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (CurChange)
            {
                Bs_CustId.DataSource = connection.SqlExec("Get_Cust_Info ", "MultiCust_Name_Tbl",
                           new object[] { ((DataRowView)Bs_CurId.Current).Row["Cur_Id"],
                           Txt_Cust.Text.Trim(), connection.T_ID, ((DataRowView)Bs_AccId.Current).Row["Acc_Id"],1});

                Grd_Cust_Id.DataSource = Bs_CustId;
            }
        }
        //----------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price",
                        "cFor_Amount","cLoc_amount","ACC_Name","Cur_Name","cust_Name","dFor_Amount","dLoc_amount","DC_Id","V_Ref_No"
                        , "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                        "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                        "V_D_ETerm_Name", "V_User_Name"
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal",
                "System.Decimal","System.Decimal","System.String","System.String","System.String","System.Decimal","System.Decimal","System.Int32","System.String",
                 "System.String","System.String","System.String","System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String"
                ,"System.String","System.String","System.String"
            };

            Voucher_Cur = CustomControls.Custom_DataTable("Voucher_Cur", Column, DType);
            Grd_Rec_Daily.DataSource = _Bs;
        }
        //----------------------------
        private void Grd_Rec_Daily_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }
        //----------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Definitions
            Decimal MdFor_Amount = 0;
            Decimal MdLoc_amount = 0;
            Decimal McLoc_amount = 0;
            Decimal MFor_Amount = 0;
            Decimal MLoc_amount = 0;
            Decimal Real_Price = 0;
            Decimal Exch_Rate = 0;
            byte  Rpt_Rec_Id  = 0 ;
            
            int DC_Id = Convert.ToInt32(Cbo_Cust_Type.SelectedValue);
            string Cur_Name = "";
            #endregion
            DCChange = false;
            #region Validations
            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الجهـــة":"Select The Catagory Please ", MyGeneral_Lib.LblCap);
                CboCatg_Id.Focus();
                return;
            }

            if (Grd_Acc_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"حدد الحساب":"Select The Account Please ", MyGeneral_Lib.LblCap);
                Txt_Acc.Focus();
                return;
            }

            if (Grd_Cur_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"حدد العملة":"Select The Currency Please", MyGeneral_Lib.LblCap);
                TxtCurr_Name.Focus();
                return;
            }

            if (Grd_Cust_Id.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"حدد الثانوي":"Select The Customer Please ", MyGeneral_Lib.LblCap);
                Txt_Cust.Focus();
                return;
            }

            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) < -1)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"حدد حالة القيد":"Select Record State Please ", MyGeneral_Lib.LblCap);
                Cbo_Cust_Type.Focus();
                return;
            }

            if (Convert.ToInt16(Cbo_AddType.SelectedIndex) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"اختر نوع الاضافة":"Select The Type Please", MyGeneral_Lib.LblCap);
                Cbo_AddType.Focus();
                return;
            }

            if (Convert.ToDecimal(Txtfor_Amount.Text) <= 0 && Convert.ToInt32(Cbo_Cust_Type.SelectedValue) > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"ادخل المبلغ":"Enter The Amount Please", MyGeneral_Lib.LblCap);
                Txtfor_Amount.Focus();
                return;
            }

            if (Convert.ToDecimal(TxtRealPrice.Text) > Convert.ToDecimal(TxtMax_Price.Text))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "السعر الفعلي للشراء اعلى من الحد الاعلى" : "Buy Real Price is Greater than Upper limit ", MyGeneral_Lib.LblCap);
                TxtRealPrice.Focus();
                return;
            }

            if (Convert.ToDecimal(TxtRealPrice.Text) < Convert.ToDecimal(TxtMin_Price.Text))
            {
                DialogResult _Dr = MessageBox.Show(connection.Lang_id == 1 ?"سعر الشراء اقل من الحد الادنى هل تريد الاستمرار ":"Buy Price less than Minimum Price Are You Want to Continue ", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (_Dr == DialogResult.No)
                {
                    TxtRealPrice.Focus();
                    return;
                }
            }

            //-------------------------فحص هل الحساب الدائن يملك حساب عملة محلية لغرض الشراء
            Sql_Text = "Select Cur_Id "
                           + " From Customers_Accounts "
                               + " WHere Cur_Id = " + Txt_Loc_Cur.Tag
                                   + " And T_Id = " + connection.T_ID
                                   + " And Cust_Id = " + ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
            connection.SqlExec(Sql_Text, "CurCount_Tble");

            if (connection.SQLDS.Tables["CurCount_Tble"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ?"يجب تعريف الثانوي بالعملة المحلية  ":"Must Define The Customer Account with local Amouunt"
                + ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ?"Acust_Name":"Ecust_name"].ToString() 
                + (connection.Lang_id==1?" لهذه العملة ":"To This Currency") + Txt_Loc_Cur.Text.ToString(), MyGeneral_Lib.LblCap);
                Cbo_Cust_Type.Focus();
                return;
            }
            //------------
            if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) == 1) //-----------اذا كان مدين
            {
                MdFor_Amount = Convert.ToDecimal(Txtfor_Amount.Text);
                MdLoc_amount = decimal.Round(MdFor_Amount * Convert.ToDecimal(TxtExchange_Rate.Text), 3);
                MFor_Amount = MdFor_Amount;
                MLoc_amount = MdLoc_amount;
                Real_Price = Convert.ToDecimal(TxtRealPrice.Text);
                Exch_Rate = Convert.ToDecimal(TxtExchange_Rate.Text);
                For_Cur_Id = Convert.ToInt32(((DataRowView)Bs_CurId.Current).Row["Cur_Id"]);
                Cur_Name = ((DataRowView)Bs_CurId.Current).Row[connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename"].ToString();
               Rpt_Rec_Id = 1;
                //-----------اعادة ملئ الكومبو الخاص بحالة الحساب "دائن" بأعتبار ان آخر نفذة مدخلة كانت مدينة
                if (Cbo_AddType.SelectedIndex == 1) //----------------اذا كان من حساب
                {
                    Cbo_Cust_Type.DataSource = Credit_Tbl;
                    Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
                    Cbo_Cust_Type.ValueMember = "DC_Id";
                }
                //-----------اعادة ملئ الكومبو الخاص بحالة الحساب بقيمة مدين ودائن
                if (Cbo_AddType.SelectedIndex == 2) //----------------اذا كان من مذكورين
                {
                    Cbo_Cust_Type.DataSource = connection.SQLDS.Tables["DC_Id_tab"];
                    Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
                    Cbo_Cust_Type.ValueMember = "DC_Id";
                }
            }

            else if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) < 0 && Cbo_AddType.SelectedIndex == 1) //---------اذا كان دائن ونوع الاضافة من حساب
            {
                Sql_Text = "Select Cur_Id "
                           + " From Customers_Accounts "
                               + " WHere Cur_Id = " + For_Cur_Id
                                   + " And T_Id = " + connection.T_ID
                                   + " And Cust_Id = " + ((DataRowView)Bs_CustId.Current).Row["Cust_Id"];
                connection.SqlExec(Sql_Text, "CurCount_Tble");

                if (connection.SQLDS.Tables["CurCount_Tble"].Rows.Count <= 0)
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "يجب تعريف الحساب الثانوي " : "Must Define The Customer Account" 
                    + ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "Acust_Name" : "Ecust_name"].ToString() 
                    + (connection.Lang_id == 1 ? " لهذه العملة " : "To This Currency") + Txt_Loc_Cur.Text.ToString(), MyGeneral_Lib.LblCap);
                  Cbo_Cust_Type.Focus();
                    return;
                }
                McLoc_amount = decimal.Round(Convert.ToDecimal(Txtfor_Amount.Text) * Convert.ToDecimal(TxtRealPrice.Text), 3);
                MdLoc_amount = McLoc_amount;
                MFor_Amount = McLoc_amount * -1;
                MLoc_amount = McLoc_amount * -1;
                For_Cur_Id = Convert.ToInt32(Txt_Loc_Cur.Tag);
                Cur_Name = Txt_Loc_Cur.Text;
                Real_Price = 1;
                Exch_Rate = 1;
                Rpt_Rec_Id = 0;
                //-----------اعادة ملئ الكومبو الخاص بحالة الحساب "مدين" بأعتبار ان آخر نفذة مدخلة كانت دائنة
                Cbo_Cust_Type.DataSource = Debit_Tbl;
                Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
                Cbo_Cust_Type.ValueMember = "DC_Id";
            }

            else if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) < 0 && Cbo_AddType.SelectedIndex == 2) //-----------اذا كان دائن ونوع الاضافة من مذكورين
            {
                McLoc_amount = (from t in Voucher_Cur.AsEnumerable()
                                select decimal.Round((Convert.ToDecimal(t["For_Amount"]) * Convert.ToDecimal(t["Real_Price"])), 3)).Sum();
                MdLoc_amount = McLoc_amount;
                MFor_Amount = McLoc_amount * -1;
                MLoc_amount = McLoc_amount * -1;
                For_Cur_Id = Convert.ToInt32(Txt_Loc_Cur.Tag);
                Cur_Name = Txt_Loc_Cur.Text;
                Real_Price = 1;
                Exch_Rate = 1;
                Rpt_Rec_Id = 0;
                BtnAdd.Enabled = false;
            }
            #endregion

            string V_Catg_Ename = connection.SQLDS.Tables["Catg_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string V_Catg_Aname = connection.SQLDS.Tables["Catg_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Aname"].ToString();
            #region Addrowtodatatable
            DataRow DRow = Voucher_Cur.NewRow();
            DRow["Exch_price"] = Exch_Rate;
            DRow["acc_id"] = Convert.ToInt32(((DataRowView)Bs_AccId.Current).Row["Acc_Id"]);
            DRow["For_Cur_Id"] = For_Cur_Id;
            DRow["dFor_Amount"] = MdFor_Amount;
            DRow["dLoc_amount"] = MdLoc_amount;
            DRow["cLoc_amount"] = McLoc_amount;
            DRow["ACC_Name"] = ((DataRowView)Bs_AccId.Current).Row[connection.Lang_id == 1 ? "Acc_Aname" : "Acc_Ename"];
            DRow["Cur_Name"] = Cur_Name;
            DRow["cust_Name"] = ((DataRowView)Bs_CustId.Current).Row[connection.Lang_id == 1 ? "ACust_Name" : "ECust_Name"];
            DRow["Notes"] = Txt_Details.Text;
            DRow["For_Amount"] = MFor_Amount;
            DRow["Loc_amount"] = MLoc_amount;
            DRow["Loc_Cur_Id"] = connection.Loc_Cur_Id;
            DRow["Cust_id"] = Convert.ToInt32(((DataRowView)Bs_CustId.Current).Row["Cust_Id"]);
            DRow["Real_price"] = Real_Price;
            DRow["DC_ID"] = DC_Id;
            DRow["Catg_ID"] = CboCatg_Id.SelectedValue;
            DRow["Rpt_Rec_Id"] = Rpt_Rec_Id ;
            DRow["V_Acc_AName"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Aname"];
            DRow["V_Acc_EName"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Ename"]; ;
            DRow["V_ACUST_NAME"] = ((DataRowView)Bs_CustId.Current).Row["Acust_Name"];
            DRow["V_ECUST_NAME"] = ((DataRowView)Bs_CustId.Current).Row["Ecust_Name"];
            DRow["V_For_Cur_ANAME"] = ((DataRowView)Bs_CurId.Current).Row["Cur_AName"];
            DRow["V_For_Cur_ENAME"] = ((DataRowView)Bs_CurId.Current).Row["Cur_EName"];
            DRow["V_Loc_Cur_ANAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"];
            DRow["V_Loc_Cur_ENAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"];
            DRow["V_A_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"];
            DRow["V_E_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"];
            DRow["V_CATG_ANAME"] = V_Catg_Aname;
            DRow["V_CATG_ENAME"] = V_Catg_Ename;
            DRow["V_AOPER_NAME"] = "";
            DRow["V_EOPER_NAME"] = "";
            DRow["V_ACASE_NA"] = "";
            DRow["V_ECASE_NA"] = "";
            DRow["V_S_ATerm_Name"] = "";
            DRow["V_S_ETerm_Name"] = "";
            DRow["V_D_ATerm_Name"] = "";
            DRow["V_D_ETerm_Name"] = "";
            DRow["V_User_Name"] = TxtUser.Text;   
            Voucher_Cur.Rows.Add(DRow);
            #endregion
            
            _Bs.DataSource = Voucher_Cur;
            Cbo_AddType.Enabled = false;
            DCChange = true;
            Cbo_Cust_Type_SelectedIndexChanged(sender, e);
        }
        //--------------------------
        private void Cbo_Cust_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DCChange)
            {
                if (Convert.ToInt16(Cbo_Cust_Type.SelectedValue) < 0)
                {
                    Txtfor_Amount.Enabled = false;
                    TxtRealPrice.Enabled = false;
                }
                else
                {
                    Change = false;
                    ACCChange = false;
                    CurChange = false;
                    Txt_Acc.ResetText();
                    TxtCurr_Name.ResetText();
                    Txt_Cust.ResetText();
                    Txtfor_Amount.ResetText();
                    Txtfor_Amount.Enabled = true;
                    TxtRealPrice.Enabled = true;
                    Change = true;
                    Txt_Acc_TextChanged(sender, e);
                }
            }
        }
        //--------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation

            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1?"لا توجد ارصدة افتتاحية":"No opening Balance", MyGeneral_Lib.LblCap);
                TxtIn_Rec_Date.Focus();
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1?"لا يوجد منظم للقيد":"No User Defined", MyGeneral_Lib.LblCap);
                return;
            }

            if (Voucher_Cur.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1?"لا توجد قيود للاضافة":"There is No Records To Add", MyGeneral_Lib.LblCap);
                return;
            }

            int Rec_No = (from DataRow row in Voucher_Cur.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0)
            {
                MessageBox.Show(connection.Lang_id == 1?"دقق القيود":"Check The Records", MyGeneral_Lib.LblCap);
                return;
            }

            
            int Sum = Voucher_Cur.Compute("Sum(DC_ID)", "") == DBNull.Value ? 0 : Convert.ToInt16(Voucher_Cur.Compute("Sum(DC_ID)", ""));
            int Reccount = (from rec in Voucher_Cur.AsEnumerable()
                            where (int)rec["DC_Id"] == -1
                            select new { }).Count();

            if ((Sum != 0 && Convert.ToInt16(Cbo_AddType.SelectedIndex) == 1) || (Reccount != 1 && Convert.ToInt16(Cbo_AddType.SelectedIndex) == 2))
            {
                MessageBox.Show(connection.Lang_id == 1?"القيود غير مكتملة":"incomplete Records", MyGeneral_Lib.LblCap);
                Cbo_Cust_Type.Focus();
                return;
            }
       
            //----------------*******************************
            if (Cbo_AddType.SelectedIndex == 2) //-----------من مذكورين
            {
                int Cust_id = Convert.ToInt32(Voucher_Cur.Select("DC_Id = -1").CopyToDataTable().Rows[0]["Cust_Id"]); 
                string Cust_Name = Voucher_Cur.Select("DC_Id = 1").CopyToDataTable().Rows[0]["Cust_Name"].ToString();
              
                foreach (DataRow Drow in Voucher_Cur.Select("DC_Id = 1"))
                {
                    Sql_Text = "Select Cur_Id "
                                + " From Customers_Accounts "
                                    + " WHere Cur_Id = " + Drow["For_Cur_Id"]
                                        + " And T_Id = " + connection.T_ID
                                        + " And Cust_Id = " + Cust_id;
                    connection.SqlExec(Sql_Text, "CurCount_Tbl");

                    if (connection.SQLDS.Tables["CurCount_Tbl"].Rows.Count <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب تعريف الحساب الثانوي " : "Must Define The Customer Account" + Cust_Name + (connection.Lang_id == 1 ? " لهذه العملة " : "To This Currency") + Drow["Cur_Name"].ToString(), MyGeneral_Lib.LblCap);
                        Cbo_Cust_Type.Focus();
                        return;
                    }
                }
            }
            #endregion
            DataTable Dt = new DataTable();
            Dt = Voucher_Cur.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                            "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "V_Ref_No",
                            "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                            "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                            "V_D_ETerm_Name", "V_User_Name");

            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 19);
            connection.SQLCMD.Parameters.AddWithValue("@Catg_Id", CboCatg_Id.SelectedValue);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Voucher_Buy_MultiCust_Daily", connection.SQLCMD);
            int VO_NO = 0;
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            Voucher_Cur.Rows.Clear();
            BtnAdd.Enabled = true;
            Cbo_AddType.SelectedIndex = 0;
            Cbo_AddType.Enabled = true;
        }
        //--------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Voucher_Cur.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بالتأكيد حذف القيد" :
                    "Are you sure you want to remove this record", MyGeneral_Lib.LblCap,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (Dr == DialogResult.Yes)
                {
                    int DC_Id = Convert.ToInt32(((DataRowView)_Bs.Current).Row["DC_ID"]); //---------جلب قيمة القيد الحالي هل هو مدين ام دائن
                    int CRow = Grd_Rec_Daily.CurrentRow.Index; //----------موقع الصف الحالي

                    Voucher_Cur.Rows[CRow].Delete(); //-----مسح الصف الحالي المؤشر لغرض الحذف
                    //------------اذا كان نوع الاضافة "من حساب"
                    if (Cbo_AddType.SelectedIndex == 1)
                    {
                        if (DC_Id == 1 && (CRow < Voucher_Cur.Rows.Count)) //---------اذا كان مدين ويوجد للقيد نفذة دائن
                        {
                            Voucher_Cur.Rows[CRow].Delete();
                        }
                        else if (DC_Id == -1) //---------اذا كان القيد المؤشر دائن فيجب مسح نفذة المدين معه
                        {
                            Voucher_Cur.Rows[CRow + DC_Id].Delete();
                        }
                        //------------اعادة ملئ الكومبو بقيمة مدين فقط
                        DCChange = false;
                        Cbo_Cust_Type.DataSource = Debit_Tbl;
                        Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
                        Cbo_Cust_Type.ValueMember = "DC_Id";
                        DCChange = true;
                        Cbo_Cust_Type_SelectedIndexChanged(sender, e);
                    }
                    else //------------اذا كان نوع الاضافة من مذكورين
                    {
                        if (DC_Id == 1 && Grd_Rec_Daily.RowCount>0) //-------اذا كان القيد المحذوف مدين فيجب اعادة ضبط قيمة الدائن
                        {
                            decimal MLoc_amount = (from t in Voucher_Cur.AsEnumerable()
                                                   where (int)t["DC_Id"] == DC_Id
                                                   select decimal.Round((Convert.ToDecimal(t["For_Amount"]) * Convert.ToDecimal(t["Real_Price"])), 3)).Sum();
                            Voucher_Cur.Rows[Voucher_Cur.Rows.Count - 1].SetField("Loc_amount", MLoc_amount * -1);
                            Voucher_Cur.Rows[Voucher_Cur.Rows.Count - 1].SetField("For_amount", MLoc_amount * -1);
                            Voucher_Cur.Rows[Voucher_Cur.Rows.Count - 1].SetField("cLoc_amount", MLoc_amount);
                            Voucher_Cur.Rows[Voucher_Cur.Rows.Count - 1].SetField("dLoc_amount", MLoc_amount);
                        }
                        else //---------اذا كان المحذوف دائن يجب تفعيل مفتاح اضافة
                        {
                            BtnAdd.Enabled = true;
                        }
                    }
                    //----------
                    if (Voucher_Cur.Rows.Count <= 0) //-----------اذا حذف القيود المضافة بالكامل
                    {
                        DCChange = false;
                        BtnAdd.Enabled = true;
                        Cbo_AddType.Enabled = true;
                        //------------اعادة ملئ الكومبو بقيمة مدين فقط
                        Cbo_Cust_Type.DataSource = Debit_Tbl;
                        Cbo_Cust_Type.DisplayMember = connection.Lang_id == 1 ? "DC_Aname" : "DC_Ename";
                        Cbo_Cust_Type.ValueMember = "DC_Id";
                        DCChange = true;
                        Cbo_Cust_Type_SelectedIndexChanged(sender, e);
                    }
                    Grd_Rec_Daily.Refresh();
                }
            }
        }
        //--------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //--------------------------
        private void Daily_BuyBill_MultiCustomers_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
             Change = false;
             ACCChange = false;
             CurChange = false;
             DCChange = false;
             string[] Used_Tbl = { "CurCount_Tbl", "MultiCust_Name_Tbl", "Cur_Acc_MultiCust_Tbl", "Acc_Name_MultiCust_Tbl", "DC_Id_tab", "Catg_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Grd_Cur_Id_SelectionChanged_1(object sender, EventArgs e)
        {
            if (CurChange)
            {
                TxtExchange_Rate.Text = ((DataRowView)Bs_CurId.Current).Row["Exch_Rate"].ToString();
                TxtRealPrice.Text = ((DataRowView)Bs_CurId.Current).Row["Min_B_Price"].ToString();
                TxtMin_Price.Text = ((DataRowView)Bs_CurId.Current).Row["Min_B_Price"].ToString();
                TxtMax_Price.Text = ((DataRowView)Bs_CurId.Current).Row["Max_B_Price"].ToString();
                Txt_Cust_TextChanged(sender, e);
            }
        }

      
    }
}