﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;
namespace Integration_Accounting_Sys
{
    public partial class Daily_SaleBillCurrency_Add : Form
    {
        #region MyRegion
        BindingSource B_S = new BindingSource();
        DataTable Daily_Bill = new DataTable();
        BindingSource Bs_AccId = new BindingSource();
        BindingSource Bs_CustId = new BindingSource();
        bool Acc_Change = false;
        bool Cust_Change = false;
        string Sql_Txt;
        int Cust_id;
        Int16 acc_id;
        string _Date = "";
        DateTime C_Date;
        int VO_NO = 0;
        int MCRow = 0;
        #endregion
        //-------------------------------------------------------
        public Daily_SaleBillCurrency_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Create_Table();
            Grd_Acc_Id.AutoGenerateColumns = false;
            Grd_Cust_Id.AutoGenerateColumns = false;
            GrdDaily_Sale.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Grd_Acc_Id.Columns["ColumnAcc_Name"].DataPropertyName = "Acc_Ename";
                Grd_Cust_Id.Columns["ColumnCust_Name"].DataPropertyName = "Ecust_name";
            }

        }
        //-------------------------------------------------------
        private void Create_Table()
        {
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amnt", "Min_price", "Max_price","Cur_Code","Header","EHeader",
                        "Cur_ToWord","Cur_ToEWord","V_Ref_No", "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                        "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                        "V_D_ETerm_Name", "V_User_Name"
            };

            string[] DType =
                    {   "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                        "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32", "System.String",
                        "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal","System.String","System.String","System.String",
                        "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String",
                        "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String"
                        ,"System.String","System.String","System.String"
            };

            Daily_Bill = CustomControls.Custom_DataTable("Daily_Bill", Column, DType);
            GrdDaily_Sale.DataSource = B_S;
        }
        //-------------------------------------------------------
        private void Daily_SaleBillCurrency_Add_Load(object sender, EventArgs e)
        {

            Sql_Txt = "select CATG_ID, CATG_ANAME, CATG_ENAME from Catg_Tbl ";
            CboCatg_Id.DataSource = connection.SqlExec(Sql_Txt, "Catgo_Tbl");
            CboCatg_Id.DisplayMember = connection.Lang_id == 1 ? "CATG_ANAME" : "CATG_ENAME";
            CboCatg_Id.ValueMember = "CATG_ID";
            Txt_Acc_TextChanged(null, null);
        }
        //-------------------------------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            Acc_Change = false;

            connection.SqlExec("exec Get_Acc_Info " + 0 + ','
                                                                      + "'" + Txt_Acc.Text + "'" + ',' + connection.T_ID + ','
                                                                      + connection.user_id + ',' + 0, "RecACCSal_TREE_tbl");
            try
            {
                Bs_AccId.DataSource = connection.SQLDS.Tables["RecACCSal_TREE_tbl"].Select("Sub_Flag > 0").CopyToDataTable();
            }
            catch
            {
                Bs_AccId.DataSource = null;
                Bs_CustId.DataSource = null;
                return;
            }
            Grd_Acc_Id.DataSource = Bs_AccId;
            if (connection.SQLDS.Tables["RecACCSal_TREE_tbl"].Rows.Count > 0)
            {
                Acc_Change = true;
                Grd_Acc_Id_SelectionChanged(null, null);
            }
            else Grd_Cust_Id.DataSource = null; 
        }
        //-------------------------------------------------------
        private void Grd_Acc_Id_SelectionChanged(object sender, EventArgs e)
        {
            if (Acc_Change)
            {
                Txt_Cust.Text = "";
                Cust_Change = true;
               
                Txt_Cust_TextChanged(null, null);

            }

        }
        //-------------------------------------------------------
        private void Txt_Cust_TextChanged(object sender, EventArgs e)
        {
            if (Cust_Change  && connection.SQLDS.Tables["RecACCSal_TREE_tbl"].Rows.Count > 0)
            {
                string cust_Srch = "'" + Txt_Cust.Text + "%'";
                Sql_Txt = "   SELECT Acust_Name,Ecust_name,A.Cust_Id "
                          + " From Customers_Accounts A, Customers B "
                          + " Where  A.Cust_Id = B.Cust_Id And A.Cus_State_Id = 1 "
                          + " And  ACC_Id =  " + ((DataRowView)Bs_AccId.Current).Row["Acc_id"]
                          + " AND A.T_Id = " + connection.T_ID
                          + " And A.Cur_Id = " + Txt_Loc_Cur.Tag
                          + " And Cust_Flag  <> 0 And A.CUS_STATE_ID <> 0"
                          + " And (ACUST_NAME Like " + cust_Srch
                          + "Or	ECUST_NAME Like " + cust_Srch
                          + " Or	A.CUST_ID Like  " + cust_Srch + ")"
                          + "Order by " + (connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME");


                Bs_CustId.DataSource = connection.SqlExec(Sql_Txt, "Cust_table");
                Grd_Cust_Id.DataSource = Bs_CustId;
            }
        }
        //-------------------------------------------------------
        private void GrdDaily_Sale_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is ComboBox && GrdDaily_Sale.CurrentCell.ColumnIndex == Cbo_Cur_Id.Index)
            {
                ComboBox combo = e.Control as ComboBox;
                if (combo != null)
                {
                    combo.SelectedIndexChanged -= new EventHandler(Cbo_Cur_Id_SelectedIndexChanged);


                    // Add the event handler. 

                    combo.SelectedIndexChanged += new System.EventHandler(Cbo_Cur_Id_SelectedIndexChanged);
                }
                e.Control.KeyPress -= TextboxNumeric_KeyPress;
                if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column1.Index || (int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column2.Index)
                {
                    e.Control.KeyPress += TextboxNumeric_KeyPress;
                }
            }
        }
        //-------------------------------------------------------
        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == (char)46))
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
        }
        //-------------------------------------------------------
        private void Cbo_Cur_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox CboBox = (ComboBox)sender;
            if (CboBox.Text != "System.Data.DataRowView" && CboBox.Text != string.Empty && CboBox.SelectedValue.ToString() != "System.Data.DataRowView")
            {
               
                //=========================
                DataRow[] Row = connection.SQLDS.Tables["Daily_Sal_Tbl"].Select("for_Cur_Id = " + Convert.ToInt16(CboBox.SelectedValue));
                MCRow = GrdDaily_Sale.CurrentRow.Index;
                Daily_Bill.Rows[MCRow].SetField("Real_price", Row[0]["Max_S_price"]);
                Daily_Bill.Rows[MCRow].SetField("Exch_price", Row[0]["Exch_rate"]);
                Daily_Bill.Rows[MCRow].SetField("Min_price", Row[0]["Min_S_price"]);
                Daily_Bill.Rows[MCRow].SetField("Max_price", Row[0]["Max_S_price"]);
                Daily_Bill.Rows[MCRow].SetField("For_cur_id", Row[0]["For_Cur_id"]);
                Daily_Bill.Rows[MCRow].SetField("Cust_ID", Cust_id);
                Daily_Bill.Rows[MCRow].SetField("ACC_ID", ((DataRowView)Bs_AccId.Current).Row["ACC_Id"]);
                Daily_Bill.Rows[MCRow].SetField("Cur_Code", Row[0]["Cur_code"]);
                Daily_Bill.Rows[MCRow].SetField("Header", "سعر البيـــع");
                Daily_Bill.Rows[MCRow].SetField("EHeader", "Sale Price");
                GrdDaily_Sale.CurrentRow.Cells[Cbo_Cur_Id.Index].Value = connection.Lang_id == 1 ? Row[0]["Cur_Aname"] : Row[0]["Cur_Ename"];
                Daily_Bill.Rows[MCRow].SetField("V_For_Cur_ANAME", Row[0]["Cur_Aname"]);
                Daily_Bill.Rows[MCRow].SetField("V_For_Cur_ENAME", Row[0]["Cur_Ename"]);
                GrdDaily_Sale.Refresh();
                try
                {
                    decimal MFor_Amount = ((DataRowView)B_S.Current).Row["For_Amount"] == DBNull.Value ? 0 : Convert.ToDecimal(((DataRowView)B_S.Current).Row["For_Amount"]);
                    decimal MReal_Price = ((DataRowView)B_S.Current).Row["Real_price"] == DBNull.Value ? 0 : Convert.ToDecimal(((DataRowView)B_S.Current).Row["Real_price"]);
                    decimal Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 3);
                    Daily_Bill.Rows[GrdDaily_Sale.CurrentRow.Index].SetField("Loc_amount", Loc_Amount);
                    GrdDaily_Sale.Refresh();
                    Loc_Amount = 0;
                    Loc_Amount = Convert.ToDecimal(Daily_Bill.Compute("Sum(Loc_Amount)", ""));
                    TxtTLoc_Amount.Text = Loc_Amount.ToString();
                }
                catch { }
                Txt_Max_Sale.DataBindings.Clear();
                Txt_Min_Sale.DataBindings.Clear();
                Txt_Max_Sale.DataBindings.Add("Text", B_S, "Max_price");
                Txt_Min_Sale.DataBindings.Add("Text", B_S, "Min_price");
            }
        }
        //-------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Grd_Acc_Id.Rows.Count <= 0)
            {

                MessageBox.Show(connection.Lang_id == 1 ? "حدد الحساب ":"Please Select The Account", MyGeneral_Lib.LblCap);
                return;
            }

            if (Grd_Cust_Id.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1?"حدد الثانوي ":"Please Select The Customer", MyGeneral_Lib.LblCap);
                return;

            }

            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show( connection.Lang_id == 1?"حدد الجهة" : "Please Select The Catagory", MyGeneral_Lib.LblCap);
                CboCatg_Id.Focus();
                return;
            }


            int Rows = Daily_Bill.Rows.Count;
            if (Rows >= 10)
            {
                MessageBox.Show(connection.Lang_id == 1?"الحد الاقصى للفاتورة 10 عمليات ":"The Maximum Of Bill 10 operations", MyGeneral_Lib.LblCap);
                return;
            }



            int Rec_No = (from DataRow row in Daily_Bill.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0 && Daily_Bill.Rows.Count > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "السعر الفعلي للبيع اقل من الحد الادنى" : "Sale real Price less than miminum limit", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            // ==================
            Cust_id = Convert.ToInt32(((DataRowView)Bs_CustId.Current).Row["Cust_id"]);
            acc_id = Convert.ToInt16(((DataRowView)Bs_AccId.Current).Row["acc_id"]);
            Cbo_Cur_Id.DataSource = connection.SqlExec("Get_Cur_Buy_Sal " + connection.T_ID + " , " + Cust_id + " , " + acc_id, "Daily_Sal_Tbl");
            Cbo_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
            Cbo_Cur_Id.ValueMember = "for_Cur_Id";
            //------------------------------------------------------------------------------
            string V_Catg_Ename = connection.SQLDS.Tables["Catgo_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string V_Catg_Aname = connection.SQLDS.Tables["Catgo_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Aname"].ToString();
            DataRow DRow = Daily_Bill.NewRow();
            DRow["V_Acc_AName"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Aname"];
            DRow["V_Acc_EName"] = ((DataRowView)Bs_AccId.Current).Row["Acc_Ename"];
            DRow["V_ACUST_NAME"] = ((DataRowView)Bs_CustId.Current).Row["ACUST_NAME"];
            DRow["V_ECUST_NAME"] = ((DataRowView)Bs_CustId.Current).Row["ECUST_NAME"];
            //DRow["V_For_Cur_ANAME"] = connection.SQLDS.Tables["Daily_Sal_Tbl"].Rows[Cbo_Cur_Id.Index]["Cur_AName"];
            //DRow["V_For_Cur_ENAME"] = connection.SQLDS.Tables["Daily_Sal_Tbl"].Rows[Cbo_Cur_Id.Index]["Cur_EName"];
            DRow["V_Loc_Cur_ANAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"];
            DRow["V_Loc_Cur_ENAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"];
            DRow["V_A_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"];
            DRow["V_E_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"];
            DRow["V_CATG_ANAME"] = V_Catg_Aname;
            DRow["V_CATG_ENAME"] = V_Catg_Ename;
            DRow["V_AOPER_NAME"] = "";
            DRow["V_EOPER_NAME"] = "";
            DRow["V_ACASE_NA"] = "";
            DRow["V_ECASE_NA"] = "";
            DRow["V_S_ATerm_Name"] = "";
            DRow["V_S_ETerm_Name"] = "";
            DRow["V_D_ATerm_Name"] = "";
            DRow["V_D_ETerm_Name"] = "";
            DRow["V_User_Name"] = TxtUser.Text;

        
            Daily_Bill.Rows.Add(DRow);
            B_S.DataSource = Daily_Bill;
            Txt_Curname.Text = Txt_Loc_Cur.Text; 
            Enable_Disable(false);
            GrdDaily_Sale.BeginEdit(true);
        }
        //-------------------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            #region Validation
            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "No Opening Balances", MyGeneral_Lib.LblCap);
                TxtIn_Rec_Date.Focus();
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "No User Defined", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الجهة" : "Please Select The Catagory", MyGeneral_Lib.LblCap);
                CboCatg_Id.Focus();
                return;
            }
            if (Daily_Bill.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "There Are No Records To Add", MyGeneral_Lib.LblCap);
                return;
            }
            int Rec_No = (from DataRow row in Daily_Bill.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود":"Please Check The Records", MyGeneral_Lib.LblCap);
                return;
            }
           
            int Recount = 0;

            string Price = "Min_price";
            Recount = (from DataRow row in Daily_Bill.Rows
                       where (decimal)row["Real_price"] < (decimal)row[Price]
                       select row).Count();
            if (Recount > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "السعر الفعلي للبيع اقل من الحد الادنى" : "Sale real Price less than miminum limit", MyGeneral_Lib.LblCap);
              //  MessageBox.Show(connection.Lang_id == 1 ? "دقق السعر الفعلي":"Please Check The Real Price", MyGeneral_Lib.LblCap);
                return;
            }

            Recount = 0;
            Price = "";
            Price = "Max_price";
            Recount = (from DataRow row in Daily_Bill.Rows
                       where (decimal)row["Real_price"] > (decimal)row[Price]
                       select row).Count();
            if (Recount > 0)
            {
                DialogResult _Dr = MessageBox.Show(connection.Lang_id == 1 ? "سعر البيع اكبر من الحد الاعلى هل تريد الاستمرار " : "Selling Price is Greater Than The Upper Limit You Want To Continue", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (_Dr == DialogResult.No)
                {
                    return;
                }
            }

            #endregion
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            ToWord toWord = new ToWord(Convert.ToDouble(TxtTLoc_Amount.Text),
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            Daily_Bill.Rows[MCRow].SetField("Cur_ToWord", Cur_ToWord);
            Daily_Bill.Rows[MCRow].SetField("Cur_ToEWord", Cur_ToEWord);
          
            DataTable Dt = new DataTable();
            Dt = Daily_Bill.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "V_Ref_No", "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                              "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                              "V_D_ETerm_Name", "V_User_Name");

            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Catg_Id", CboCatg_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@LOC_CUR_ID", Txt_Loc_Cur.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 18);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@Frm_Id", 2); //-----قيد يومية بيع عملات اجنبية
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
           
            connection.SqlExec("Add_Daily_Voucher_Buy_Sale", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            print_Rpt_Pdf();
            connection.SQLCMD.Parameters.Clear();
            Daily_Bill.Rows.Clear();
            TxtTLoc_Amount.ResetText();
            Txt_Max_Sale.ResetText();
            Txt_Min_Sale.ResetText();
            Txt_Acc.ResetText();
            Enable_Disable(true);
        }
        //-------------------------------------------------------
        private void Enable_Disable(bool Enable)
        {
            Txt_Acc.Enabled = Enable; Txt_Cust.Enabled = Enable; Grd_Acc_Id.Enabled = Enable; Grd_Cust_Id.Enabled = Enable; CboCatg_Id.Enabled = Enable;
        
        }
        //-------------------------------------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Daily_Bill.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show("هل تريد بالتأكيد حذف القيد " + (GrdDaily_Sale.CurrentRow.Index + 1), MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    TxtTLoc_Amount.ResetText();
                    Daily_Bill.Rows[GrdDaily_Sale.CurrentRow.Index].Delete();
                    TxtTLoc_Amount.Text = Daily_Bill.Compute("Sum(Loc_amount)", "").ToString();
                    if (GrdDaily_Sale.RowCount  ==0)
                    {
                        Txt_Max_Sale.ResetText();
                        Txt_Min_Sale.ResetText();
                        Txt_Curname.ResetText();
                    }
                }
            }
        }
        //-------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }
        //-------------------------------------------------------
        private void GrdDaily_Sale_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                decimal MFor_Amount = GrdDaily_Sale.CurrentRow.Cells["Column1"].Value == DBNull.Value ? 0 : Convert.ToDecimal(GrdDaily_Sale.CurrentRow.Cells["Column1"].Value);
                decimal MReal_Price = GrdDaily_Sale.CurrentRow.Cells["Column2"].Value == DBNull.Value ? 0 : Convert.ToDecimal(GrdDaily_Sale.CurrentRow.Cells["Column2"].Value);
                decimal Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 3);
                Daily_Bill.Rows[GrdDaily_Sale.CurrentRow.Index].SetField("Loc_amount", Loc_Amount);
                GrdDaily_Sale.Refresh();
                Loc_Amount = Convert.ToDecimal(Daily_Bill.Compute("Sum(Loc_amount)", ""));
                TxtTLoc_Amount.Text = Loc_Amount.ToString();
            }
            catch { };
        }
        //-------------------------------------------------------
        private void GrdDaily_Sale_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            GrdDaily_Sale.Refresh();
        }
        //-------------------------------------------------------
        private void BackBtn_Click(object sender, EventArgs e)
        {
            Daily_Bill.Clear();
            Enable_Disable(true);
            Txt_Acc.ResetText(); Txt_Cust.ResetText();
        }
        //-------------------------------------------------------
        private void GrdDaily_Sale_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(B_S);
        }
        //-------------------------------------------------------
        private void Daily_SaleBillCurrency_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            Acc_Change = false;
            Cust_Change = false;
            string[] Used_Tbl = { "Catgo_Tbl", "RecACCSal_TREE_tbl", "Cust_table", "Daily_Sal_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //-------For Printing-----------
        private void print_Rpt_Pdf()
        {
            C_Date = Convert.ToDateTime(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            string Catg_Ename = connection.SQLDS.Tables["Catgo_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب المدين" + ": " +
                              ((DataRowView)Bs_CustId.Current).Row["Acust_Name"].ToString();
            string ECur_Cust = "Debt Account" + ": " + ((DataRowView)Bs_CustId.Current).Row["Ecust_Name"].ToString() + " / " +
                                      connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();


            Daily_Buy_Sale_BillCurrency_Rpt ObjRpt = new Daily_Buy_Sale_BillCurrency_Rpt();
            Daily_Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Daily_Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", CboCatg_Id.Text);
            Dt_Param.Rows.Add("Catg_EName", Catg_Ename);
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 18);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("ACur_Cust", ACur_Cust);
            Dt_Param.Rows.Add("ECur_Cust", ECur_Cust);
            DataRow Dr;
            int y = Daily_Bill.Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = Daily_Bill.NewRow();
                Daily_Bill.Rows.Add(Dr);
            }
            connection.SQLDS.Tables.Add(Daily_Bill);

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 18);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Daily_Bill");

        }
    }
}
           