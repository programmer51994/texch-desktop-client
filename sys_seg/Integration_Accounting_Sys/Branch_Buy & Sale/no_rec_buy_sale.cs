﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class no_rec_buy_sale : Form
    {

        BindingSource BS_Sale =new BindingSource();
        BindingSource BS_Buy = new BindingSource();
        Decimal Amount_sale = 0;
        Decimal Amount_buy = 0;
       

        public no_rec_buy_sale()
        {
            InitializeComponent();
          
           MyGeneral_Lib.Form_Orientation(this);
           //connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Grd_buy.AutoGenerateColumns = false;
            Grd_sale.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                Column1.DataPropertyName = "Cur_ENAME";
                Column1.HeaderText = "Currency name";
                Column2.HeaderText = "Amounts of buying in F.C";
                Column3.DataPropertyName = "Cur_ENAME";
                Column3.HeaderText = "Currency name";
                Column4.HeaderText = "Amounts of selling in F.C";
                Column5.HeaderText = "Amounts of buying in L.C";
                Column6.HeaderText = "Amounts of selling in L.C";
                label22.Text = "Amounts of buying ....";
                label1.Text="Amounts of selling ....";
                label52.Text = "total amount of the puying in L.C";
                label2.Text = "total amount of the selling in L.C";
                label3.Text = "total amount of the buy and sale in L.C";


              

            }
        }

        private void no_rec_buy_sale_Load(object sender, EventArgs e)
        {


            BS_Buy.DataSource = connection.SQLDS.Tables["tot_buy_tbl1"];
            Grd_buy.DataSource = BS_Buy;
            BS_Sale.DataSource = connection.SQLDS.Tables["tot_sale_tbl1"];
            Grd_sale.DataSource = BS_Sale;


            if (connection.SQLDS.Tables["tot_buy_tbl1"].Rows.Count > 0)
            {
                
                DataTable DT_buy = new DataTable();
                DT_buy = connection.SQLDS.Tables["tot_buy_tbl1"].DefaultView.ToTable(false, "tot_LOC_AMOUNT_buy").Select().CopyToDataTable();
                Amount_buy = Convert.ToDecimal(DT_buy.Compute("Sum(tot_LOC_AMOUNT_buy)", ""));
                Tot_Amount_buy.Text = Amount_buy.ToString();
            }

            if (connection.SQLDS.Tables["tot_sale_tbl1"].Rows.Count > 0)
            {

               
                DataTable DT_sale = new DataTable();
                DT_sale = connection.SQLDS.Tables["tot_sale_tbl1"].DefaultView.ToTable(false, "tot_LOC_AMOUNT_sale").Select().CopyToDataTable();
                Amount_sale = Convert.ToDecimal(DT_sale.Compute("Sum(tot_LOC_AMOUNT_sale)", ""));
                Tot_Amount_sale.Text = Amount_sale.ToString();

            }

            Decimal Amount_sale_buy = 0;
            Amount_sale_buy = Amount_buy + Amount_sale;
            Tot_Amount_sale_buy.Text = Amount_sale_buy.ToString();






            if (connection.SQLDS.Tables["tot_sale_tbl1"].Rows.Count > 0)
                {

                    Txt_per_Aname.Text = connection.SQLDS.Tables["tot_sale_tbl1"].Rows[0]["per_aname"].ToString();
                    Txt_per_Ename.Text = connection.SQLDS.Tables["tot_sale_tbl1"].Rows[0]["per_ename"].ToString();
                }

                else
                {
                    Txt_per_Aname.Text = connection.SQLDS.Tables["tot_buy_tbl1"].Rows[0]["per_aname"].ToString();
                    Txt_per_Ename.Text = connection.SQLDS.Tables["tot_buy_tbl1"].Rows[0]["per_ename"].ToString();
                }
         

             
         

        }
    }
}
