﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Return_Cash_Daily_Main : Form
    {
        #region MyRegion
        bool CboChange = false;
        bool GrdChange = false;
        DataTable Dt_Details = new DataTable();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs_Grd_Return = new BindingSource();
        string Sql_Txt = "";
        int Oper_id = 0;
        string _Date = "";
        DateTime C_Date;
        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        string ACur_Cust = "";
        string ECur_Cust = "";
        int Vo_No = 0;
        #endregion

        public Return_Cash_Daily_Main()
        {
            InitializeComponent();
            
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            if (connection.Box_Cust_Id <= 0)
            {
                BtnAdd.Enabled = false;
            }
            Grd_Return.AutoGenerateColumns = false;
            GrdVou_Details.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                GrdVou_Details.Columns["Column12"].DataPropertyName = "Ecust_name";
                GrdVou_Details.Columns["Column3"].DataPropertyName = "Cur_Ename";
                Grd_Return.Columns["Column9"].DataPropertyName = "Catg_Ename";
            }
        }
        //----------------------------------
        private void Return_Cash_Daily_Main_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }
            //if (TxtBox_User.Text == "")
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "يجب ربط المستخدم بصندوق فرعي" : "you must add Box user", MyGeneral_Lib.LblCap);
            //    this.Dispose();
            //    this.Close();
            //    return;
            //}

            #endregion 
            CboChange = false;
            Sql_Txt = "Select OPER_ID,AOPER_NAME,EOPER_NAME from OPERATIONS where OPER_ID in(14,15,20,21)"
                    + " Union "
                    + " Select 20 As oper_id , 'قيد يوميه رد بيع العملات الاجنبية لاكثر من ثانوي ' As Aoper_name ,' return daily record sell foreign currencies' as Eoper_name"
                    + " union "
                    + " Select 21 As oper_id , 'قيد يوميه رد شراء العملات الاجنبية لاكثر من ثانوي ' As Aoper_name ,'return daily record Buy foreign currencies' as Eoper_name ";
            CboOper_Id.DataSource = connection.SqlExec(Sql_Txt, "Oper_Tbl");
            CboOper_Id.ValueMember = "OPER_ID";
            CboOper_Id.DisplayMember = connection.Lang_id == 1 ? "AOper_Name" : "EOper_Name";
            if (connection.SQLDS.Tables["Oper_Tbl"].Rows.Count > 0)
            {
                CboChange = true;
                CboOper_Id_SelectedIndexChanged(sender, e);
            }
        }
        //----------------------------------
        private void Get_Data(int oper_id)
        {
            GrdChange = false;
            object[] Sparams = {TxtIn_Rec_Date.Text, connection.T_ID, connection.user_id, 
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo, Gen_Search.Catg_Id, 
                                   oper_id, 0, 0, 2};
            connection.SqlExec("Main_Vouchers", "Return_VouCher_Tbl", Sparams);

            DataTable _Dt = connection.SQLDS.Tables["Return_VouCher_Tbl"];
            var query = from row in _Dt.AsEnumerable()
                        where row.Field<Int16>("For_cur_id") != row.Field<Int16>("Loc_cur_id")
                        group row by new
                        {
                            Vo_No = row.Field<int>("vo_no"),
                            Create_Date = row.Field<string>("alter_date"),
                            Catg_Aname = row.Field<string>("catg_aname"),
                            Catg_Ename = row.Field<string>("catg_ename"),
                            Nrec_Date = row.Field<int>("nrec_date"),
                            USER_NAME = row.Field<string>("USER_NAME"),

                        } into grp
                        orderby grp.Key.Vo_No
                        select new
                        {
                            Key = grp.Key,
                            Vo_No = grp.Key.Vo_No,
                            Create_Date = grp.Key.Create_Date,
                            Catg_Aname = grp.Key.Catg_Aname,
                            Catg_Ename = grp.Key.Catg_Ename,
                            Nrec_Date = grp.Key.Nrec_Date,
                            USER_NAME = grp.Key.USER_NAME,
                            Loc_Amount = Math.Abs(grp.Sum(DataRow => DataRow.Field<decimal>("Loc_amount")))
                        };

            Dt_Details = CustomControls.IEnumerableToDataTable(query);
            _Bs_Grd_Return.DataSource = Dt_Details;
            Grd_Return.DataSource = _Bs_Grd_Return;
            if (Dt_Details.Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Return_SelectionChanged(null, null);
            }
        }
        //----------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(13);
            SearchFrm.ShowDialog(this);
            Get_Data(Convert.ToInt16(CboOper_Id.SelectedValue));
        }
        //----------------------------------
        private void CboOper_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboChange)
            {
                Gen_Search.For_Cur_ID = 0;
                Gen_Search.Min_Qty = 0;
                Gen_Search.Max_Qty = 0;
                Gen_Search.Min_Vo = 0;
                Gen_Search.Max_Vo = 0;
                Gen_Search.Catg_Id = 0;
                Oper_id = Convert.ToInt16(CboOper_Id.SelectedValue);
                Get_Data(Convert.ToInt16(CboOper_Id.SelectedValue));

            }
        }
        //----------------------------------
        private void Grd_Return_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                int Nrec_Date = Convert.ToInt32(((DataRowView)_Bs_Grd_Return.Current).Row["Nrec_Date"]);
                int Vo_No = Convert.ToInt32(((DataRowView)_Bs_Grd_Return.Current).Row["Vo_No"]);
                int Oper_Id = Convert.ToInt16(CboOper_Id.SelectedValue);
                string Filter = "Vo_No = " + Vo_No + " And Nrec_Date = " + Nrec_Date + " And Oper_Id = " + Oper_Id + " And T_Id = " + connection.T_ID + " And For_Cur_Id <> Loc_Cur_Id";
                DataTable Dt = connection.SQLDS.Tables["Return_VouCher_Tbl"].Select(Filter).CopyToDataTable();
                GrdVou_Details.DataSource = Dt;
                LblRec.Text = connection.Records(_Bs_Grd_Return);
            }
            else
                GrdVou_Details.DataSource = new BindingSource();
        }
        //----------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Get_Data(Convert.ToInt16(CboOper_Id.SelectedValue));
        }
        //----------------------------------
        private void Return_Cash_Daily_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
            CboChange = false;
           
            string[] Used_Tbl = { "Return_VouCher_Tbl", "Oper_Tbl", "Return_Bill_Cur" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Return_Cash_Buy_Sale_Add AddFrm = new Return_Cash_Buy_Sale_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Return_Cash_Daily_Main_Load(null, null);
            this.Visible = true;
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Return_Rec_Daily_Add AddFrm = new Return_Rec_Daily_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Return_Cash_Daily_Main_Load(null, null);
            this.Visible = true;
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            Vo_No = Convert.ToInt32(((DataRowView)_Bs_Grd_Return.Current).Row["VO_NO"]);
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + "," + Oper_id + "," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Return_Bill_Cur");


            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["C_Date"]);

            double Loc_Amount = Convert.ToDouble(connection.SQLDS.Tables["Return_Bill_Cur"].Compute("Sum(Loc_Amount)", ""));
            double Total_Amount = Convert.ToDouble(connection.SQLDS.Tables["Return_Bill_Cur"].Compute("Sum(loc_amount)", "")) + Convert.ToDouble(connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Discount_Amount"] == DBNull.Value ? 0 : connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Discount_Amount"]);

            ToWord toWord = new ToWord(Total_Amount,
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
         
            connection.SQLDS.Tables["Return_Bill_Cur"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Return_Bill_Cur"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            ACur_Cust = Cur_Code + "/" + (Oper_id == 18 ? "الحساب الدائن" : "الحساب المدين") + ":" + connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Acust_Name"].ToString();
            ECur_Cust = (Oper_id == 18 ? "Credit Account : ":"Debit Account : ") + connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
         
            Box_ACust_Cur = Cur_Code + "/" + (Oper_id == 12 ? "الحساب الدائن" : "الحساب المدين") + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
            Box_ECust_Cur = (Oper_id == 12 ?"Credit Account : ":"Debit Account : ") + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;

          
            string Catg_Ename = connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Catg_Ename"].ToString();
            string Catg_AEname = connection.SQLDS.Tables["Return_Bill_Cur"].Rows[0]["Catg_Aname"].ToString();
            Return_Buy_Sale_BillCurrency_Rpt ObjRpt = new Return_Buy_Sale_BillCurrency_Rpt();
            Return_Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Return_Buy_Sale_BillCurrency_Rpt_Eng();
            
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", Catg_AEname);
            Dt_Param.Rows.Add("Catg_EName", Catg_Ename);
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", Oper_id);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Main");
         
            DataRow Dr;
            int y = connection.SQLDS.Tables["Return_Bill_Cur"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Return_Bill_Cur"].NewRow();
                connection.SQLDS.Tables["Return_Bill_Cur"].Rows.Add(Dr);
            }
            Branch_RptLang_MsgBox RptLangFrm;
            if (Oper_id == 14 || Oper_id == 15)
            {
                Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
                Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
                RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, Oper_id);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Return_Bill_Cur");
            }
            else
            {
                Return_Daily_Buy_Sale_BillCurrency_Rpt DailyRpt = new Return_Daily_Buy_Sale_BillCurrency_Rpt();
                Return_Daily_Buy_Sale_BillCurrency_Rpt_Eng DailyRptEng = new Return_Daily_Buy_Sale_BillCurrency_Rpt_Eng();
                connection.SQLDS.Tables["Return_Bill_Cur"].TableName = "Return_Daily_Bill";
                Dt_Param.Rows.Add("ACur_Cust", ACur_Cust);
                Dt_Param.Rows.Add("ECur_Cust", ECur_Cust);
                RptLangFrm = new Branch_RptLang_MsgBox(DailyRpt, DailyRptEng, true, true, Dt_Param, Oper_id);
                this.Visible = false;
                RptLangFrm.ShowDialog(this);
                this.Visible = true;
                connection.SQLDS.Tables.Remove("Return_Daily_Bill");
            }  
        }

        private void Return_Cash_Daily_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    Btn_Add_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F2:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Return_Cash_Daily_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}