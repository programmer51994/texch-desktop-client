﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;
namespace Integration_Accounting_Sys
{
    public partial class Daily_SaleBillCurrency_Main : Form
    {
        #region MyRegion
        bool GrdChange = false;
        DataTable Dt_Details = new DataTable();
        DataTable Voucher_Cur = new DataTable();
        BindingSource _Bs = new BindingSource();
        BindingSource _Bs_Grd_Main = new BindingSource();
        #endregion
        //------------------------------------------------------------------
        public Daily_SaleBillCurrency_Main()
        {
            InitializeComponent();
             
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Currency.AutoGenerateColumns = false;
            GrdCurrency_Details.AutoGenerateColumns = false;
        }
        //------------------------------------------------------------------
        private void Buy_BillCurrency_Main_Load(object sender, EventArgs e)
        {

            #region Check_open_Close()
            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }

            #endregion 
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column3"].DataPropertyName = "catg_Ename";
                Grd_Currency.Columns["Column13"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Details.Columns["Column6"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Details.Columns["Column9"].DataPropertyName = "Ecust_name";
            }

            Getdata(18);
        }
        //------------------------------------------------------------------
        private void Getdata(int Oper_Id)
        {
            GrdChange = false;
            TxtTLoc_Amount.ResetText();
            Voucher_Cur.Rows.Clear();
            Dt_Details.Rows.Clear();
            _Bs = new BindingSource();
            object[] Sparams = {TxtIn_Rec_Date.Text, connection.T_ID, connection.user_id, 
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo, Gen_Search.Catg_Id, 
                                   Oper_Id, 0, 0, 0 };
            connection.SqlExec("Main_Vouchers", "Daily_Sal_Main_Tbl", Sparams);
            //==============================
            DataTable _Dt = connection.SQLDS.Tables["Daily_Sal_Main_Tbl"];
            var query = from row in _Dt.AsEnumerable()
                        where row.Field<Int16>("for_cur_id") == Convert.ToInt16(Txt_Loc_Cur.Tag)
                        group row by new
                        {
                            Vo_No = row.Field<int>("vo_no"),
                            Create_Date = row.Field<string>("alter_date"),
                            Catg_Aname = row.Field<string>("catg_aname"),
                            Catg_Ename = row.Field<string>("catg_ename"),
                            Nrec_Date = row.Field<int>("nrec_date"),
                            Acust_name = row.Field<string>("acust_name"),
                            Ecust_name = row.Field<string>("ecust_name")
                        } into grp
                        orderby grp.Key.Vo_No
                        select new
                        {
                            Key = grp.Key,
                            Vo_No = grp.Key.Vo_No,
                            Create_Date = grp.Key.Create_Date,
                            Catg_Aname = grp.Key.Catg_Aname,
                            Catg_Ename = grp.Key.Catg_Ename,
                            Nrec_Date = grp.Key.Nrec_Date,
                            Loc_Amount = Math.Abs(grp.Sum(r => r.Field<decimal>("loc_amount"))),
                            Acust_name = grp.Key.Acust_name,
                            Ecust_name = grp.Key.Ecust_name

                        };

            Voucher_Cur = CustomControls.IEnumerableToDataTable(query);

            _Bs_Grd_Main.DataSource = Voucher_Cur;
            Grd_Cust.DataSource = _Bs_Grd_Main;
            if (Voucher_Cur.Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Cust_SelectionChanged(null, null);
                //------------
                TxtTLoc_Amount.Text = Voucher_Cur.Compute("Sum(loc_amount)", "").ToString();
            }
            //-------------------
            var Currency_query = from row in _Dt.AsEnumerable()
                                 where row.Field<Int16>("for_cur_id") != Convert.ToInt16(Txt_Loc_Cur.Tag)
                                 group row by new
                                 {
                                     For_Cur_Id = row.Field<Int16>("for_cur_id"),
                                     Cur_Aname = row.Field<string>("cur_aname"),
                                     Cur_Ename = row.Field<string>("cur_ename"),

                                 } into Currency_grp
                                 orderby Currency_grp.Key.For_Cur_Id
                                 select new
                                 {
                                     For_Cur_Id = Currency_grp.Key.For_Cur_Id,
                                     Cur_Aname = Currency_grp.Key.Cur_Aname,
                                     Cur_Ename = Currency_grp.Key.Cur_Ename,
                                     For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount")))
                                 };

            DataTable Dt = CustomControls.IEnumerableToDataTable(Currency_query);
            Grd_Currency.DataSource = Dt;
        }
        //---------------------------Close Form-----------------------------
        private void Sale_BillCurrency_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
          
            string[] Used_Tbl = { "Daily_Sal_Main_Tbl", "Daily_Bill" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Daily_SaleBillCurrency_Add AddFrm = new Daily_SaleBillCurrency_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Getdata(18);
            this.Visible = true;
        }
        //------------------------------------------------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                Dt_Details = new DataTable();
                DataRowView Drv = _Bs_Grd_Main.Current as DataRowView;
                DataRow Dr = Drv.Row;
                int MVo_No = Dr.Field<int>("vo_no");
                int Nrec_Date = Dr.Field<int>("Nrec_Date");
                var Query = from row in connection.SQLDS.Tables["Daily_Sal_Main_Tbl"].AsEnumerable()
                            where row.Field<int>("Vo_No") == MVo_No && row.Field<int>("Nrec_date") == Nrec_Date && row.Field<Int16>("Loc_Cur_Id") != row.Field<Int16>("For_Cur_Id")
                            select new
                            {
                                For_Amount = Math.Abs(row.Field<decimal>("For_Amount")),
                                Cur_Aname = row.Field<string>("Cur_Aname"),
                                Cur_Ename = row.Field<string>("Cur_Ename"),
                                Real_Price = row.Field<decimal>("Real_Price"),
                                Exch_Price = row.Field<decimal>("Exch_Price"),
                                Loc_Amount = Math.Abs(row.Field<decimal>("Loc_Amount")),
                                Notes = row.Field<string>("Notes"),
                                Acust_name = row.Field<string>("Acust_name"),
                                Ecust_name = row.Field<string>("Ecust_name")
                            };
                Dt_Details = CustomControls.IEnumerableToDataTable(Query);
                //---------
                _Bs.DataSource = Dt_Details;
                GrdCurrency_Details.DataSource = _Bs;
                LblRec.Text = connection.Records(connection.SQLBS);
            }

        }
        //------------------------------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Getdata(18);
        }
        //------------------------------------------------------------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(18);
            SearchFrm.ShowDialog(this);
            Getdata(18);
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            int Vo_No = Convert.ToInt16(((DataRowView)_Bs_Grd_Main.Current).Row["Vo_No"]);
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + ",18," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Daily_Bill");
            DateTime C_Date;
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Daily_Bill"].Rows[0]["C_Date"]);
            string _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            ToWord toWord = new ToWord(Convert.ToDouble(connection.SQLDS.Tables["Daily_Bill"].Compute("Sum(loc_amount)", "")),
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            string ACur_Cust = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString() + " / الحساب المدين" + ": " +
                                ((DataRowView)_Bs.Current).Row["Acust_Name"].ToString();
            string ECur_Cust = "Debt Account" + ": " + ((DataRowView)_Bs.Current).Row["Ecust_Name"].ToString() + " / " +
                                      connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();

            connection.SQLDS.Tables["Daily_Bill"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Daily_Bill"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Daily_Bill"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Daily_Bill"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);

            Daily_Buy_Sale_BillCurrency_Rpt ObjRpt = new Daily_Buy_Sale_BillCurrency_Rpt();
            Daily_Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Daily_Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", ((DataRowView)_Bs_Grd_Main.Current).Row["Catg_Aname"].ToString());
            Dt_Param.Rows.Add("Catg_EName", ((DataRowView)_Bs_Grd_Main.Current).Row["Catg_Ename"].ToString());
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", 19);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Main");
            Dt_Param.Rows.Add("ACur_Cust", ACur_Cust);
            Dt_Param.Rows.Add("ECur_Cust", ECur_Cust);
            DataRow Dr;
            int y = connection.SQLDS.Tables["Daily_Bill"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Daily_Bill"].NewRow();
                connection.SQLDS.Tables["Daily_Bill"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 18);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Daily_Bill");
        }

        private void Daily_SaleBillCurrency_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    printToolStripButton_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Daily_SaleBillCurrency_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}