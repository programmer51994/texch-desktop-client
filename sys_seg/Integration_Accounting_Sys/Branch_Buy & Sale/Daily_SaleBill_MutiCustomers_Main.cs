﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Daily_SaleBill_MutiCustomers_Main : Form
    {
        #region MyRegion
            bool GrdChange = false;
            DataTable Dt_Details = new DataTable();
            DataTable Voucher_Cur = new DataTable();
            BindingSource _Bs = new BindingSource();
            BindingSource _Bs_11 = new BindingSource();
            #endregion
        //----------------
        public Daily_SaleBill_MutiCustomers_Main()
        {
            InitializeComponent();
            
            #region Arabic/English
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id != 1)
            {
                Grd_Cust.Columns["Column3"].DataPropertyName = "catg_Ename";
                Grd_Currency.Columns["Column13"].DataPropertyName = "Cur_Ename";
                GrdCurrency_Details.Columns["Column6"].DataPropertyName = "Cur_Ename";

            }
            #endregion
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Currency.AutoGenerateColumns = false;
            GrdCurrency_Details.AutoGenerateColumns = false;
        }
        //----------------
        private void Daily_BuyBill_MutiCustomers_Main_Load(object sender, EventArgs e)
        {
            #region Check_open_Close()
            if (Page_Setting.Chk_Value != "")
            {
                if (Page_Setting.Chk_Value != "0")
                {
                    MessageBox.Show(Page_Setting.Chk_Value, MyGeneral_Lib.LblCap);
                }
                this.Dispose();
                this.Close();
                return;
            }

            if (Page_Setting.Chk_Is_Close != "")
            {
                MessageBox.Show(Page_Setting.Chk_Is_Close, MyGeneral_Lib.LblCap);
                this.Dispose();
                this.Close();
                return;
            }
            #endregion 
            Getdata(18);
        }
        //----------------
        private void Getdata(int Oper_Id)
        {
            GrdChange = false;
            TxtTLoc_Amount.ResetText();
            Voucher_Cur.Rows.Clear();
            Dt_Details.Rows.Clear();
            _Bs = new BindingSource();

            object[] Sparams = {TxtIn_Rec_Date.Text, connection.T_ID, connection.user_id, 
                                   Gen_Search.For_Cur_ID, Gen_Search.Min_Qty,Gen_Search.Max_Qty,
                                   Gen_Search.Min_Vo, Gen_Search.Max_Vo, Gen_Search.Catg_Id, 
                                   Oper_Id, 0, 0, 0 };
            connection.SqlExec("Main_Vouchers", "Bill_VouCher_MUlti_Tbl", Sparams);
            //==============================
            DataTable _Dt = connection.SQLDS.Tables["Bill_VouCher_MUlti_Tbl"];
            var query = from row in _Dt.AsEnumerable()
                        where row.Field<Int16>("for_cur_id") == Convert.ToInt16(Txt_Loc_Cur.Tag)
                        group row by new
                        {
                            Vo_No = row.Field<int>("vo_no"),
                            Create_Date = row.Field<string>("alter_date"),
                            Catg_Aname = row.Field<string>("catg_aname"),
                            Catg_Ename = row.Field<string>("catg_ename"),
                            Nrec_Date = row.Field<int>("nrec_date")
                        } into grp
                        orderby grp.Key.Vo_No
                        select new
                        {
                            Key = grp.Key,
                            Vo_No = grp.Key.Vo_No,
                            Create_Date = grp.Key.Create_Date,
                            Catg_Aname = grp.Key.Catg_Aname,
                            Catg_Ename = grp.Key.Catg_Ename,
                            Nrec_Date = grp.Key.Nrec_Date,
                            Loc_Amount = grp.Sum(r => r.Field<decimal>("loc_amount"))
                        };

            Voucher_Cur = CustomControls.IEnumerableToDataTable(query);

            _Bs_11.DataSource = Voucher_Cur;
            Grd_Cust.DataSource = _Bs_11;
            if (Voucher_Cur.Rows.Count > 0)
            {
                GrdChange = true;
                Grd_Cust_SelectionChanged(null, null);
                //------------
                TxtTLoc_Amount.Text = Voucher_Cur.Compute("Sum(loc_amount)", "").ToString();
            }
            //-------------------
            var Currency_query = from row in _Dt.AsEnumerable()
                                 where row.Field<Int16>("for_cur_id") != Convert.ToInt16(Txt_Loc_Cur.Tag)
                                 group row by new
                                 {
                                     For_Cur_Id = row.Field<Int16>("for_cur_id"),
                                     Cur_Aname = row.Field<string>("cur_aname"),
                                     Cur_Ename = row.Field<string>("cur_ename"),

                                 } into Currency_grp
                                 orderby Currency_grp.Key.For_Cur_Id
                                 select new
                                 {
                                     For_Cur_Id = Currency_grp.Key.For_Cur_Id,
                                     Cur_Aname = Currency_grp.Key.Cur_Aname,
                                     Cur_Ename = Currency_grp.Key.Cur_Ename,
                                     For_Amount = Math.Abs(Currency_grp.Sum(r => r.Field<decimal>("For_Amount")))
                                 };

            DataTable Dt = CustomControls.IEnumerableToDataTable(Currency_query);
            Grd_Currency.DataSource = Dt;
        }
        //----------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                Dt_Details = new DataTable();
                DataRowView Drv = _Bs_11.Current as DataRowView;
                DataRow Dr = Drv.Row;
                int MVo_No = Dr.Field<int>("vo_no");
                int Nrec_Date = Dr.Field<int>("Nrec_Date");

                try
                {
                    Dt_Details = connection.SQLDS.Tables["Bill_VouCher_MUlti_Tbl"].Select("Vo_No = " + MVo_No + " And Nrec_date = " + Nrec_Date + " And Loc_Cur_Id <> For_Cur_id").CopyToDataTable();
                }
                catch { };
                //var Query = from row in connection.SQLDS.Tables["Bill_VouCher_MUlti_Tbl"].AsEnumerable()
                //            where row.Field<int>("Vo_No") == MVo_No && row.Field<int>("Nrec_date") == Nrec_Date && row.Field<Int16>("Loc_Cur_Id") != row.Field<Int16>("For_Cur_Id")
                //            select new
                //            {
                //                For_Amount = Math.Abs(row.Field<decimal>("For_Amount")),
                //                Cur_Aname = row.Field<string>("Cur_Aname"),
                //                Cur_Ename = row.Field<string>("Cur_Ename"),
                //                Real_Price = row.Field<decimal>("Real_Price"),
                //                Exch_Price = row.Field<decimal>("Exch_Price"),
                //                Loc_Amount = Math.Abs(row.Field<decimal>("Loc_Amount")),
                //                Notes = row.Field<string>("Notes"),
                //                Acust_name = row.Field<string>("Acust_name"),
                //                Ecust_name = row.Field<string>("Ecust_name")
                //            };
                //Dt_Details = CustomControls.IEnumerableToDataTable(Query);

               _Bs.DataSource = Dt_Details;
                GrdCurrency_Details.DataSource = _Bs;
                LblRec.Text = connection.Records(connection.SQLBS);
            }
        }
        //----------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Daily_SaleBill_MultiCustomers_Add AddFrm = new Daily_SaleBill_MultiCustomers_Add();
            this.Visible = false;
            AddFrm.ShowDialog();
            Getdata(18);
            this.Visible = true;
        }
        //----------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Gen_Search.For_Cur_ID = 0;
            Gen_Search.Min_Qty = 0;
            Gen_Search.Max_Qty = 0;
            Gen_Search.Min_Vo = 0;
            Gen_Search.Max_Vo = 0;
            Gen_Search.Catg_Id = 0;
            Getdata(18);
        }
        //----------------
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            Gen_Search SearchFrm = new Gen_Search(18);
            SearchFrm.ShowDialog(this);
            Getdata(18);
        }
        //----------------
        private void Daily_BuyBill_MutiCustomers_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            GrdChange = false;
           
            string[] Used_Tbl = { "Bill_VouCher_MUlti_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void Daily_SaleBill_MutiCustomers_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    SearchBtn_Click(sender, e);
                    break;
          
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Daily_SaleBill_MutiCustomers_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}
