﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Buy_BillCurrency_Local_Add : Form
    {
        #region Declareations
        string Sql_Text = "";
        string @format = "dd/MM/yyyy";
        string @format_doc = "dd/MM/yyyy";
        string @format_null = "";
        string S_doc_exp_null = "0000/00/00";
        private BindingSource _Bs = new BindingSource();
        private BindingSource _Bs2 = new BindingSource();
        private DataTable Bill_Cur;
        private decimal Mdiff_Amoun = 0;
        int MFor_cur_id = 0;
        BindingSource _Bs1 = new BindingSource();
        BindingSource _Bs_2 = new BindingSource();
        BindingSource _Bs_3 = new BindingSource();
        BindingSource _Bs_4 = new BindingSource();
        BindingSource _Bs_5 = new BindingSource();
        int Language = 0; //Arabic
        DataTable Per_inf_TBL = new DataTable();
        int per_id = 0;
        string Per_Name = "";
        string Per_Occupation_Aname = "";
        string Per_Occupation_Ename = "";

        string Ord_Strt = "";
        string _Date = "";
        int MCRow = 0;
        Int16 Con_Id = 0;
        string COUN_K_PH = "";
        DateTime C_Date;
        int VO_NO = 0;
        int Rpt_Per_id = 0;

        string Box_ACust_Cur = "";
        string Box_ECust_Cur = "";
        string Birth_date = "0000/00/00";
        string Doc_date = "0000/00/00";
        string Doc_Edate = "0000/00/00";
        int Reg_id = 0;
        String Real_Amount = "";
        //    Int64 per_ID= 0;
        bool chan_buy = false;
        decimal Loc_Amount = 0;
        decimal MReal_Price = 1;
        string Txt_Name_BL = "";
        DataTable Per_blacklist_tbl = new DataTable();
        public static string per_id_BL = "";
        public static DataTable Refuse_buy_sale_tbl;
        #endregion

        public Buy_BillCurrency_Local_Add()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, TxtBox_User, TxtIn_Rec_Date, TxtTerm_Name);
            btn_browser.Text = "Browse";
            Create_Table();
            Grd_Cust.AutoGenerateColumns = false;
            Grd_Per.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
                Grd_Per.Columns["Column21"].DataPropertyName = "Per_EName";
            }


        }
        //----------------------------
        private void Create_Table()
        {
            //Added  (Exch_Rate_Real) field to pass the undivided exhchange rate value from the currencies_rate table
            string[] Column =
            {
                        "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                        "Notes", "CATG_ID","NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                        "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id","Discount_amount", "Min_price", "Max_price","Cur_Code","Header","EHeader",
                        "Cur_ToWord","Cur_ToEWord","V_Ref_No" ,"cFor_Amount_From","cLoc_amount_From" ,"dFor_Amount_Form","dLoc_amount_From" ,
                        "V_Acc_AName","V_Acc_EName","V_ACUST_NAME","V_ECUST_NAME","V_For_Cur_ANAME","V_For_Cur_ENAME","V_Loc_Cur_ANAME","V_Loc_Cur_ENAME",
                        "V_A_Term_Name", "V_E_Term_Name","V_CATG_ANAME","V_CATG_ENAME","V_AOPER_NAME","V_EOPER_NAME","V_ACASE_NA","V_ECASE_NA",
                        "V_S_ATerm_Name","V_S_ETerm_Name","V_D_ATerm_Name","V_D_ETerm_Name","V_User_Name","Exch_Rate_Real","Exch_Rate_Api"
                       
            };

            string[] DType =
            {
                "System.Int32","System.Int32","System.Int32","System.Decimal",  "System.Decimal", "System.Decimal", "System.Decimal","System.Int32",
                "System.String", "System.Int32", "System.String","System.Byte","System.Int64","System.Byte","System.Byte", "System.Int32", "System.Int32",
                "System.String",
                "System.Byte","System.String","System.Decimal", "System.Byte", "System.Decimal", "System.Decimal", "System.Decimal","System.String"
                ,"System.String","System.String",
                "System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.Decimal","System.Decimal"
                
            };

            Bill_Cur = CustomControls.Custom_DataTable("Bill_Cur", Column, DType);
            Grd_Cust.DataSource = _Bs;

            string[] Column3 =
            {
                        "Per_ID", "Per_AName", "Per_EName", "per_City_ID", "per_Coun_ID", "Per_Birth_place", "per_Phone", "per_Another_Phone",
                        "per_Email", "per_Frm_Doc_ID","per_Frm_Doc_NO", "per_Frm_Doc_Date", "per_Frm_Doc_EDate", "per_Frm_Doc_IS", "per_Nationalty_ID", "per_Birth_day", "per_Gender_ID", "per_Occupation",
                        "per_Mother_name", "cust_ID", "User_ID", "registerd_ID","COUN_K_PH", "Per_ACITY_NAME", "Per_ECITY_NAME","Per_ACOUN_NAME","Per_ECOUN_NAME","Per_Street",
                        "Per_Suburb","Per_Post_Code","Per_State" ,"PerFRM_ADOC_NA","PerFRM_EDOC_NA","Per_A_NAT_NAME" ,"Per_E_NAT_NAME","Per_Gender_Aname","Per_Gender_Ename","Login_id",
                        "per_EFrm_Doc_IS","EOccupation","mother_Ename","Per_EBirth_place","Per_EStreet","Per_ESuburb","Per_EState","Social_No","resd_flag","Per_Occupation_id","details_job"
                       
            };
            string[] DType3 =
            {
                "System.Int32","System.String","System.String","System.Int16",  "System.Int16", "System.String", "System.String","System.String",
                "System.String", "System.Int16", "System.String","System.String" ,"System.String","System.String","System.Int16", "System.String", "System.Byte", "System.String",
                "System.String","System.Int16","System.Int16", "System.Int16", "System.String", "System.String", "System.String","System.String","System.String","System.String",
                "System.String","System.String","System.String", "System.String","System.String","System.String","System.String","System.String","System.String","System.Int16",
                "System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.String","System.Int16","System.Int16", "System.String"
            };

            Per_inf_TBL = CustomControls.Custom_DataTable("Per_inf_TBL", Column3, DType3);

            string[] Column4 = { "Per_AName", "Per_EName" };
            string[] DType4 = { "System.String", "System.String" };

            Per_blacklist_tbl = CustomControls.Custom_DataTable("Per_blacklist_tbl", Column4, DType4);
        }
        //-------------------------------
        private void GetData()
        {
            Sql_Text = " Exec all_master ";
            connection.SqlExec(Sql_Text, "all_master");

            _Bs1.DataSource = connection.SQLDS.Tables["all_master1"].Select("Nat_id Not in(-1)").CopyToDataTable();
            CboNat_id.DataSource = _Bs1;
            CboNat_id.DisplayMember = connection.Lang_id == 1 ? "Nat_ANAME" : "Nat_eNAME";
            CboNat_id.ValueMember = "Nat_Id";

            _Bs_2.DataSource = connection.SQLDS.Tables["all_master2"].Select("Fmd_id not in (-1)").CopyToDataTable();
            CboDoc_id.DataSource = _Bs_2;
            CboDoc_id.DisplayMember = connection.Lang_id == 1 ? "Fmd_aname" : "Fmd_Ename";
            CboDoc_id.ValueMember = "Fmd_id";

            _Bs_3.DataSource = connection.SQLDS.Tables["all_master6"];
            CboCit_Id.DataSource = _Bs_3;
            CboCit_Id.DisplayMember = connection.Lang_id == 1 ? "cit_Con_Aname" : "cit_Con_Ename";
            CboCit_Id.ValueMember = "Cit_ID";

            _Bs_4.DataSource = connection.SQLDS.Tables["all_master7"].Select("Gender_id not in (-1)").CopyToDataTable();
            Cbo_Gender.DataSource = _Bs_4;
            Cbo_Gender.DisplayMember = connection.Lang_id == 1 ? "Gender_Aname" : "Gender_Ename";
            Cbo_Gender.ValueMember = "Gender_id";

            _Bs_5.DataSource = connection.SQLDS.Tables["all_master8"].Select("Job_ID not in (-1)").CopyToDataTable();
            Cbo_Occupation.DataSource = _Bs_5;
            Cbo_Occupation.DisplayMember = connection.Lang_id == 1 ? "Job_AName" : "Job_EName";
            Cbo_Occupation.ValueMember = "Job_ID";

            if (connection.Lang_id == 1)
            {
                resd_cmb.Items.Clear();
                resd_cmb.Items.Add("مقيم");
                resd_cmb.Items.Add("غير مقيم");
                resd_cmb.SelectedIndex = 0;
            }
            else
            {
                resd_cmb.Items.Clear();
                resd_cmb.Items.Add("Resident");
                resd_cmb.Items.Add("Non-Resident");
                resd_cmb.SelectedIndex = 0;
            }


        }
        //----------------------------
        private void PreventChar(KeyPressEventArgs e)
        {
            if (Language == 0) //--------عربي
            {
                if (((int)e.KeyChar >= 65) && ((int)e.KeyChar <= 122))
                {
                    e.Handled = true;
                }
            }
            else if ((((int)e.KeyChar < 65) || ((int)e.KeyChar > 122)) && (e.KeyChar != '\b') && (e.KeyChar != ' ') && (e.KeyChar != '-') && (((int)e.KeyChar <= 47) || ((int)e.KeyChar >= 58)))
            {
                e.Handled = true;
            }
        }
        //----------------------------
        private void Buy_BillCurrency_Add_Load(object sender, EventArgs e)
        {
            TxtBirth_Da.Format = DateTimePickerFormat.Custom;
            TxtBirth_Da.CustomFormat = @format;

            TxtExp_Da.Format = DateTimePickerFormat.Custom;
            TxtExp_Da.CustomFormat = @format;

            TxtDoc_Da.Format = DateTimePickerFormat.Custom;
            TxtDoc_Da.CustomFormat = @format;

            label77.Visible = false;
            GetData();

            //------------فحص حسابات صندوق العملة المحلية
            Sql_Text = "Select Acc_Id From Customers_Accounts "
                            + " Where Cust_id = " + TxtBox_User.Tag
                                + " And T_id = " + connection.T_ID
                                + " And Cur_id = " + Txt_Loc_Cur.Tag;
            connection.SqlExec(Sql_Text, "Bill_Acc_No");

            if (connection.SQLDS.Tables["Bill_Acc_No"].Rows.Count <= 0)
            {
                MessageBox.Show("لا توجد حسابات معرفة لصندوق العملة المحلية", MyGeneral_Lib.LblCap);
                return;
            }
            //------------
            Sql_Text = "select * from Catg_Tbl ";
            CboCatg_Id.DataSource = connection.SqlExec(Sql_Text, "Catg_Tbl");
            CboCatg_Id.DisplayMember = connection.Lang_id == 1 ? "CATG_ANAME" : "CATG_ENAME";
            CboCatg_Id.ValueMember = "CATG_ID";
            //==========================جلب العملات المعرفة لهذا الصندوق في جدول العملاء المركزيين
            Cbo_Cur_Id.DataSource = connection.SqlExec("Exec Get_Cur_Buy_Sal " + connection.T_ID + " , " + TxtBox_User.Tag + " , " + 0, "Cur_Buy_Sal_Tbl");
            Cbo_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName";
            Cbo_Cur_Id.ValueMember = "For_Cur_id";

            btn_browser.UseColumnTextForButtonValue = true;
            BtnAdd_Click(null, null);
        }
        //----------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region MyRegion
            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الجهـــة" : "Select Issuer", MyGeneral_Lib.LblCap);
                CboCatg_Id.Focus();
                return;
            }
            int Rows = Bill_Cur.Rows.Count;
            if (Rows >= 10)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الحد الاقصى للفاتورة 10 عمليات " : "Bill Maximam limit 10 operation", MyGeneral_Lib.LblCap);
                return;
            }
            int Rec_No = (from DataRow row in Bill_Cur.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0 && Bill_Cur.Rows.Count > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود" : "Check the record", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion
            string V_Catg_Ename = connection.SQLDS.Tables["Catg_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string V_Catg_Aname = connection.SQLDS.Tables["Catg_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Aname"].ToString();
            DataRow DRow = Bill_Cur.NewRow();
            DRow["V_Acc_AName"] = "";
            DRow["V_Acc_EName"] = "";
            DRow["V_ACUST_NAME"] = connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"];
            DRow["V_ECUST_NAME"] = connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"];
            DRow["V_For_Cur_ANAME"] = "";
            DRow["V_For_Cur_ENAME"] = "";
            DRow["V_Loc_Cur_ANAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_AName"];
            DRow["V_Loc_Cur_ENAME"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"];
            DRow["V_A_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Acust_Name"];
            DRow["V_E_Term_Name"] = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Ecust_Name"];
            DRow["V_CATG_ANAME"] = V_Catg_Aname;
            DRow["V_CATG_ENAME"] = V_Catg_Ename;
            DRow["V_AOPER_NAME"] = "";
            DRow["V_EOPER_NAME"] = "";
            DRow["V_ACASE_NA"] = "";
            DRow["V_ECASE_NA"] = "";
            DRow["V_S_ATerm_Name"] = "";
            DRow["V_S_ETerm_Name"] = "";
            DRow["V_D_ATerm_Name"] = "";
            DRow["V_D_ETerm_Name"] = "";
            DRow["V_User_Name"] = TxtUser.Text;
            Bill_Cur.Rows.Add(DRow);
            _Bs.DataSource = Bill_Cur;
            // Btn_Add.Enabled = true;
            Grd_Cust.BeginEdit(true);
        }

        //----------------------------
        private void Grd_Cust_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is ComboBox && Grd_Cust.CurrentCell.ColumnIndex == Cbo_Cur_Id.Index)
            {
                ComboBox combo = e.Control as ComboBox;
                if (combo != null)
                {
                    combo.SelectedIndexChanged -= new EventHandler(Cbo_Cur_Id_SelectedIndexChanged);

                    // Add the event handler. 
                    combo.SelectedIndexChanged += new System.EventHandler(Cbo_Cur_Id_SelectedIndexChanged);
                }
            }

            e.Control.KeyPress -= TextboxNumeric_KeyPress;
            if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column1.Index || (int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == Column2.Index)
            {
                e.Control.KeyPress += TextboxNumeric_KeyPress;
            }
            ///////////////////////////////////////
            if (e.Control is Button && Grd_Cust.CurrentCell.ColumnIndex == btn_browser.Index)
            {
                Button btn = e.Control as Button;
                if (btn != null)
                {
                    btn.Click -= new EventHandler(btn_browser_Click);
                    btn.Click += new EventHandler(btn_browser_Click);
                }
            }

        }
        //--------------
        private void btn_browser_Click(object sender, EventArgs e)
        {
            if (MFor_cur_id > 0)
            {
                //Cur_Pic_Main AddFrm = new Cur_Pic_Main(Convert.ToInt16(Grd_Cust.CurrentRow.Cells[Cbo_Cur_Id.Index].Value));
                Cur_Pic_Main AddFrm = new Cur_Pic_Main(MFor_cur_id);
                this.Visible = false;
                AddFrm.ShowDialog();
                this.Visible = true;
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "اختر العملة" : "Select the currency", MyGeneral_Lib.LblCap);
                return;
            }
        }
        //----------------
        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == (char)46))
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
        }
        //----------------------------
        private void Cbo_Cur_Id_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox CboBox = (ComboBox)sender;
            if (CboBox.Text != "System.Data.DataRowView" && CboBox.Text != string.Empty && CboBox.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                int MCRow = 0;
                //=========================
                DataRow[] Row = connection.SQLDS.Tables["Cur_Buy_Sal_Tbl"].Select("For_Cur_id = " + Convert.ToInt16(CboBox.SelectedValue));

                MCRow = Grd_Cust.CurrentRow.Index;

                Bill_Cur.Rows[MCRow].SetField("Real_price", Row[0]["Min_B_price"]);
                Bill_Cur.Rows[MCRow].SetField("Exch_price", Row[0]["Exch_rate"]);
                Bill_Cur.Rows[MCRow].SetField("Min_price", Row[0]["Min_B_price"]);
                Bill_Cur.Rows[MCRow].SetField("Max_price", Row[0]["Max_B_price"]);
                Bill_Cur.Rows[MCRow].SetField("For_cur_id", Row[0]["For_Cur_id"]);
                Bill_Cur.Rows[MCRow].SetField("Exch_Rate_Real", Row[0]["Exch_Rate_Real"]);
                Bill_Cur.Rows[MCRow].SetField("Exch_Rate_Api", Row[0]["Exch_Rate_Real"]); //Added  (Exch_Rate_Real) field to pass the undivided exhchange rate value from the currencies_rate table
                //------------- report  
                Bill_Cur.Rows[MCRow].SetField("Cur_Code", Row[0]["Cur_code"]);
                Bill_Cur.Rows[MCRow].SetField("Header", "سعر الشـــراء");
                Bill_Cur.Rows[MCRow].SetField("EHeader", "Buy Price");
                Bill_Cur.Rows[MCRow].SetField("Notes", Grd_Cust.CurrentRow.Cells[Column6.Index].Value);
                //----------------------

                MFor_cur_id = Convert.ToInt16(CboBox.SelectedValue);
                Grd_Cust.CurrentRow.Cells[Cbo_Cur_Id.Index].Value = connection.Lang_id == 1 ? Row[0]["Cur_Aname"] : Row[0]["Cur_Ename"];

                Grd_Cust.Refresh();

                try
                {
                    decimal MFor_Amount = ((DataRowView)_Bs.Current).Row["Loc_amount"] == DBNull.Value ? 0 : Convert.ToDecimal(((DataRowView)_Bs.Current).Row["Loc_amount"]);
                    decimal MReal_Price = ((DataRowView)_Bs.Current).Row["Exch_Rate_Real"] == DBNull.Value ? 0 : Convert.ToDecimal(((DataRowView)_Bs.Current).Row["Exch_Rate_Real"]);
                    
                    decimal Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 3);
                    Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("For_Amount", Loc_Amount);
                    Grd_Cust.Refresh();

                    Loc_Amount = 0;

                    Loc_Amount = Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_Amount)", ""));
                    TxtTLoc_Amount.Text = Loc_Amount.ToString();
                    // ------------------الخصم
                    Discount_Calculator();
                    Real_Amount = Convert.ToString(Loc_Amount + Convert.ToDecimal(TxtDiscount_Amount.Text));
                    TXTReal_Amount.Text = Real_Amount.ToString();
                }
                catch { }
            }
        }
        //----------------------------
        private void Grd_Cust_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                decimal MFor_Amount = Grd_Cust.CurrentRow.Cells["Column5"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column5"].Value);
                MReal_Price = Grd_Cust.CurrentRow.Cells["Column2"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column2"].Value);
                if (Grd_Cust.CurrentCell.ColumnIndex != 4)
                {

                    Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 3);
                }
                if (Grd_Cust.CurrentCell.ColumnIndex == 4)
                {
                    Loc_Amount = Grd_Cust.CurrentRow.Cells["Column1"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column1"].Value);
                    Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("Exch_Rate_Real", (Loc_Amount / MFor_Amount));

                }
                Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("Real_price", decimal.Round(1 / Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column2"].Value.ToString()), 7));
                Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("For_Amount", Loc_Amount);
                Grd_Cust.Refresh();
                Loc_Amount = Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_amount)", ""));
                TxtTLoc_Amount.Text = Loc_Amount.ToString();
                Discount_Calculator();
                Real_Amount = Convert.ToString(Loc_Amount + Convert.ToDecimal(TxtDiscount_Amount.Text));
                TXTReal_Amount.Text = Real_Amount.ToString();


               
                //old work (ahmad yousif)  for refference

                /*
                decimal MFor_Amount = Grd_Cust.CurrentRow.Cells["Column1"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column1"].Value);
                MReal_Price = Grd_Cust.CurrentRow.Cells["Column2"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column2"].Value);
                if (Grd_Cust.CurrentCell.ColumnIndex != 4)
                {

                    Loc_Amount = decimal.Round(MFor_Amount * MReal_Price, 4);
                }
                if (Grd_Cust.CurrentCell.ColumnIndex == 4)
                {
                    Loc_Amount = Grd_Cust.CurrentRow.Cells["Column5"].Value == DBNull.Value ? 0 : Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column5"].Value);
                    Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("Exch_Rate_Real", decimal.Round(Loc_Amount / MFor_Amount, 7));
                    


                }
                Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("Loc_amount", Loc_Amount);
                Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField("Real_price", decimal.Round(1 / Convert.ToDecimal(Grd_Cust.CurrentRow.Cells["Column2"].Value.ToString()), 7));
                Grd_Cust.Refresh();
                Loc_Amount = Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_amount)", ""));
                TxtTLoc_Amount.Text = Loc_Amount.ToString();
                Discount_Calculator();
                Real_Amount = Convert.ToString(Loc_Amount + Convert.ToDecimal(TxtDiscount_Amount.Text));
                TXTReal_Amount.Text = Real_Amount.ToString();
                 * */
            }
            catch { };
        }
        // ----------------------------الخصم
        private void Discount_Calculator()
        {
            decimal Loc_Amount = Bill_Cur.Compute("Sum(Loc_amount)", "") == DBNull.Value ? 0 : Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_amount)", ""));
            Mdiff_Amoun = Loc_Amount % connection.Discount_Amount;
            if (Mdiff_Amoun > (connection.Discount_Amount / 2))
            {
                Mdiff_Amoun = (connection.Discount_Amount - Mdiff_Amoun); //--------ممنوح
            }
            else
            {
                Mdiff_Amoun = Mdiff_Amoun * -1; //----مكتسب
            }
            TxtDiscount_Amount.Text = Mdiff_Amoun.ToString();
        }
        //----------------------------
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Bill_Cur.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show("هل تريد بالتأكيد حذف القيد " + (Grd_Cust.CurrentRow.Index + 1), MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {
                    TxtTLoc_Amount.ResetText();
                    TXTReal_Amount.ResetText();
                    Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].Delete();
                    TxtTLoc_Amount.Text = Bill_Cur.Compute("Sum(Loc_amount)", "").ToString();
                    Discount_Calculator();
                    if (Bill_Cur.Compute("Sum(Loc_amount)", "") != DBNull.Value)
                    {
                        Real_Amount = Convert.ToString(Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_amount)", "")) + Convert.ToDecimal(TxtDiscount_Amount.Text));
                        TXTReal_Amount.Text = Real_Amount.ToString();
                    }
                }
            }
            if (Bill_Cur.Rows.Count <= 0)
            {

            }
        }
        //----------------------------
        private void Grd_Cust_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs);
        }
        //----------------------------
        private void Grd_Cust_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Bill_Cur.Rows[Grd_Cust.CurrentRow.Index].SetField(e.ColumnIndex, 0);
        }
        //----------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {



            if (Grd_Per.Rows.Count != 0)
            {


                if (((DataRowView)_Bs2.Current).Row["Per_AName"].ToString() == "" && connection.Lang_id == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف الزبون من واجهة تعريف الزبائن" : "Please define the customer from the customer identification interface", MyGeneral_Lib.LblCap);
                    return;
                }

                if (((DataRowView)_Bs2.Current).Row["Per_EName"].ToString() == "" && connection.Lang_id == 2)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف الزبون من واجهة تعريف الزبائن" : "Please define the customer from the customer identification interface", MyGeneral_Lib.LblCap);
                    return;
                }

            }

            if (TxtIn_Rec_Date.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد ارصدة افتتاحية" : "There are no balances editorial ", MyGeneral_Lib.LblCap);
                TxtIn_Rec_Date.Focus();
                return;
            }
            if (connection.user_id <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد منظم للقيد" : "There are no operator", MyGeneral_Lib.LblCap);
                return;
            }
            if (Convert.ToInt16(TxtBox_User.Tag) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا يوجد امين صندوق معرف " : "There is no Treasurer Defined", MyGeneral_Lib.LblCap);
                TxtBox_User.Focus();
                return;
            }
            if (Convert.ToInt16(CboCatg_Id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد الجهة" : "Select the issuer", MyGeneral_Lib.LblCap);
                CboCatg_Id.Focus();
                return;
            }

            if (Bill_Cur.Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "There are no record for add", MyGeneral_Lib.LblCap);
                return;
            }

            int Rec_No = (from DataRow row in Bill_Cur.Rows
                          where
                              row["For_Cur_Id"] == DBNull.Value || (int)row["For_Cur_Id"] <= 0 ||
                              row["For_Amount"] == DBNull.Value || (decimal)row["For_Amount"] == decimal.Zero ||
                              row["Real_price"] == DBNull.Value || (decimal)row["Real_price"] == decimal.Zero
                          select row).Count();
            if (Rec_No > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "دقق القيود" : "check the records", MyGeneral_Lib.LblCap);
                return;
            }

            int Recount = 0;

            string Price = "Max_price";

            Recount = (from DataRow row in Bill_Cur.Rows
                       where (decimal)row["Real_price"] > (decimal)row[Price]
                       select row).Count();
            if (Recount > 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "السعر الفعلي للشراء اعلى من الحد الاعلى" : "Buy real price greater than the upper limit", MyGeneral_Lib.LblCap);
                return;
            }

            Recount = 0;
            Price = "";
            Price = "Min_price";
            Recount = (from DataRow row in Bill_Cur.Rows
                       where (decimal)row["Real_price"] < (decimal)row[Price]
                       select row).Count();
            if (Recount > 0)
            {
                DialogResult _Dr = MessageBox.Show(connection.Lang_id == 1 ? "سعر الشراء اقل من الحد الادنى هل تريد الاستمرار " : "Buy price less than the minimum limit Do you want to continue?", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (_Dr == DialogResult.No)
                {
                    return;
                }
            }




            if (Grd_Per.Rows.Count != 0)
            {


                if (((DataRowView)_Bs2.Current).Row["Per_AName"].ToString() == "" && connection.Lang_id == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف الزبون من واجهة تعريف الزبائن" : "Please define the customer from the customer identification interface", MyGeneral_Lib.LblCap);
                    return;
                }

                if (((DataRowView)_Bs2.Current).Row["Per_EName"].ToString() == "" && connection.Lang_id == 2)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تعريف الزبون من واجهة تعريف الزبائن" : "Please define the customer from the customer identification interface", MyGeneral_Lib.LblCap);
                    return;
                }


            }




            DataTable Dt = new DataTable();
            Dt = Bill_Cur.DefaultView.ToTable(false,
                              "Acc_Id", "CUST_ID", "For_cur_id", "Exch_price", "Real_price", "For_Amount", "Loc_amount", "LOC_CUR_ID",
                              "Notes", "CATG_ID", "NREC_DATE", "OPER_ID", "REF_ID", "RECORD_FLAG", "CASE_ID", "S_T_Id", "D_T_Id", "Check_No",
                              "Rem_Source", "MTCN", "F_F_Rate", "Rpt_Rec_Id", "V_Ref_No",
                        "V_Acc_AName", "V_Acc_EName", "V_ACUST_NAME", "V_ECUST_NAME", "V_For_Cur_ANAME", "V_For_Cur_ENAME", "V_Loc_Cur_ANAME", "V_Loc_Cur_ENAME", "V_A_Term_Name",
                        "V_E_Term_Name", "V_CATG_ANAME", "V_CATG_ENAME", "V_AOPER_NAME", "V_EOPER_NAME", "V_ACASE_NA", "V_ECASE_NA", "V_S_ATerm_Name", "V_S_ETerm_Name", "V_D_ATerm_Name",
                        "V_D_ETerm_Name", "V_User_Name", "Exch_Rate_Real");//Added  (Exch_Rate_Real) field to pass the undivided exhchange rate value from the currencies_rate table
            Per_Name = Txt_Name.Text;

            #region Insert Table

            if ((Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_Amount)", "")) > Convert.ToDecimal(connection.Limit_Exchange_LC)) &&
               (Convert.ToDecimal(connection.Limit_Exchange_LC) != 0) && (Txt_Name.Text == string.Empty)) // تجاوز الحد الاعلى والمستفيد غير مكتوب
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لقد تجاوزت الحد الاعلى للمعاملة الواحدة يجب ادامة كافة معلومات المستفيد " : "Buy real price greater than the upper limit", MyGeneral_Lib.LblCap);
                return;
            }

            try
            {
                Reg_id = Convert.ToInt16(((DataRowView)_Bs2.Current).Row["registerd_ID"]); // الزبون معرف من قبل الادارة
            }
            catch
            { Reg_id = 0; }

            Doc_date = TxtDoc_Da.Value.ToString("yyyy/MM/dd");
            Doc_Edate = TxtExp_Da.Value.ToString("yyyy/MM/dd");
            Birth_date = TxtBirth_Da.Value.ToString("yyyy/MM/dd");
            if ((Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_Amount)", "")) > Convert.ToDecimal(connection.Limit_Exchange_LC)) &&
                 (Convert.ToDecimal(connection.Limit_Exchange_LC) != 0) && (Txt_Name.Text != "")
                 && Reg_id == 0)// تجاوز الحد الاعلى والمستفيد بعض ملعوماته غير مكتوبة ومعرف من قبل الكاشير
            {


                //if ((Convert.ToDecimal(Bill_Cur.Compute("Sum(Loc_Amount)", "")) > Convert.ToDecimal(connection.Limit_Exchange_LC) &&
                //    Convert.ToDecimal(connection.Limit_Exchange_LC) != 0) ) // تجاوز الحد الاعلى ومعلومات الزبون غير مكتملة
                //{
                //#all person info Validation
                //سقف المعاملة اكبر ادخال جميع معلومات المستفيد


                if (Txt_Name.Text != "")
                {
                    #region Validation
                    //if (Txt_Ename.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 1 ? "  تجاوزت الحد الاعلى للمعاملة الواحدة ادخل الاسم الاجنبـــي" : "Insert  the english name", MyGeneral_Lib.LblCap);
                    //    Txt_Ename.Focus();
                    //    return;
                    //}
                    //if (TxtAmotherName.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 1 ? " تجاوزت الحد الاعلى للمعاملة الواحدة ادخل اســـم الام" : "Insert  your Mother Name ", MyGeneral_Lib.LblCap);
                    //    TxtAmotherName.Focus();
                    //    return;
                    //}
                    //if (TxtPhone.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 1 ? " ادخل رقم الهاتف " : "Insert  Phone number", MyGeneral_Lib.LblCap);
                    //    TxtPhone.Focus();
                    //    return;
                    //}
                    if (Convert.ToInt16(CboDoc_id.SelectedIndex) < 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثيقة  " : "Please select the type of Doc.", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToInt16(Cbo_Gender.SelectedIndex) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "  حدد الجنس" : "Please select the gender.", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToInt16(CboNat_id.SelectedIndex) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "   حدد الجنسيــــة" : "Please select the Nationalty ", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (Convert.ToInt16(CboCit_Id.SelectedIndex) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " حدد المدينــــة " : "Please select your city", MyGeneral_Lib.LblCap);
                        return;
                    }
                    if (TxtIss_Doc.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "ادخل جهة اصدار الوثيقة " : "Insert Issuer of Formal Document please", MyGeneral_Lib.LblCap);
                        TxtIss_Doc.Focus();
                        return;
                    }
                    if (TxtNo_Doc.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الوثيقة " : "Please Insert Formal Document No.", MyGeneral_Lib.LblCap);
                        TxtNo_Doc.Focus();
                        return;
                    }

                    //  Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
                    if (TxtDoc_Da.Text == "0" || TxtDoc_Da.Text == "" || TxtDoc_Da.Text == "0000/00/00" || TxtDoc_Da.Text == "00/00/0000")//(Ord_Strt == "-1" || Ord_Strt == "0")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ اصدار الوثيقة " : "Please Insert Issue Date of formal Document", MyGeneral_Lib.LblCap);
                        TxtDoc_Da.Focus();
                        return;
                    }


                    // Ord_Strt = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
                    if (TxtBirth_Da.Text == "0" || TxtBirth_Da.Text == "" || TxtBirth_Da.Text == "0000/00/00" || TxtBirth_Da.Text == "00/00/0000")//(Ord_Strt == "-1" || Ord_Strt == "0")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ التولد " : "Please make sure of date of Birth", MyGeneral_Lib.LblCap);
                        TxtBirth_Da.Focus();
                        return;
                    }



                    if (TxtA_Address.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "ادخل الولاية  " : "Please Insert the state", MyGeneral_Lib.LblCap);
                        TxtA_Address.Focus();
                        return;
                    }

                    if (Txt_Suburb.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "ادخل الحـــي  " : "Please Insert the suburb", MyGeneral_Lib.LblCap);
                        Txt_Suburb.Focus();
                        return;
                    }
                    if (Txt_Street.Text == "")
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "ادخل الزقاق " : "Please Insert the street", MyGeneral_Lib.LblCap);
                        Txt_Street.Focus();
                        return;
                    }

                    //if (TxtOccupation.Text == "")
                    //{
                    //    MessageBox.Show(connection.Lang_id == 1 ? "ادخل المهنة  " : "Please Insert the street", MyGeneral_Lib.LblCap);
                    //    TxtOccupation.Focus();
                    //    return;
                    //}

                    if (Convert.ToInt16(CboDoc_id.SelectedIndex) <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثقية " : "Please select your city", MyGeneral_Lib.LblCap);
                        return;
                    }
                }
                // }



                    #endregion

            }
            if ((Txt_Name.Text != ""))
            {

                if (Convert.ToInt16(Cbo_Gender.SelectedIndex) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "  حدد الجنس" : "Please select the gender.", MyGeneral_Lib.LblCap);
                    return;
                }
                if (Convert.ToInt16(CboNat_id.SelectedIndex) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "  حدد الجنسيــــة" : "Please select the Nationalty ", MyGeneral_Lib.LblCap);
                    return;
                }
                if (Convert.ToInt16(CboCit_Id.SelectedIndex) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "  حدد المدينــــة" : "Please select your city", MyGeneral_Lib.LblCap);
                    return;
                }


                //Ord_Strt = MyGeneral_Lib.DateChecking(TxtBirth_Da.Text);
                if (TxtBirth_Da.Text == "0" || TxtBirth_Da.Text == "" || TxtBirth_Da.Text == "0000/00/00" || TxtBirth_Da.Text == "00/00/0000")//(Ord_Strt == "-1" || Ord_Strt == "0")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ التولد " : "Please make sure of date of Birth", MyGeneral_Lib.LblCap);
                    TxtBirth_Da.Focus();
                    return;
                }
                if (Convert.ToInt16(CboDoc_id.SelectedIndex) <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " حدد نوع الوثقية " : "Please select your Document", MyGeneral_Lib.LblCap);
                    return;
                }
                if (TxtNo_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل رقم الوثيقة " : "Please Insert Formal Document No.", MyGeneral_Lib.LblCap);
                    TxtNo_Doc.Focus();
                    return;
                }
                // Ord_Strt = MyGeneral_Lib.DateChecking(TxtDoc_Da.Text);
                if (TxtDoc_Da.Text == "0" || TxtDoc_Da.Text == "" || TxtDoc_Da.Text == "0000/00/00" || TxtDoc_Da.Text == "00/00/0000")//(Ord_Strt == "-1" || Ord_Strt == "0")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل تاريخ اصدار الوثيقة " : "Please Insert Issue Date of formal Document", MyGeneral_Lib.LblCap);
                    TxtDoc_Da.Focus();
                    return;
                }
                if (TxtIss_Doc.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل جهة اصدار الوثيقة  " : "Insert Issuer of Formal Document please", MyGeneral_Lib.LblCap);
                    TxtIss_Doc.Focus();
                    return;
                }

                if (TxtA_Address.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الولاية  " : "Please Insert the state", MyGeneral_Lib.LblCap);
                    TxtA_Address.Focus();
                    return;
                }

                if (Txt_Suburb.Text == "")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الحـــي ة " : "Please Insert the suburb", MyGeneral_Lib.LblCap);
                    Txt_Suburb.Focus();
                    return;
                }
                //if (Txt_Street.Text == "")
                //{
                //    MessageBox.Show(connection.Lang_id == 1 ? "ادخل الزقاق  " : "Please Insert the street", MyGeneral_Lib.LblCap);
                //    Txt_Street.Focus();
                //    return;
                //}




            }



            if ((Convert.ToInt16(Txt_Name.Tag) == 0) && (Txt_Name.Text.Trim() != ""))
            {
                try
                {

                    per_id = Convert.ToInt32(((DataRowView)_Bs2.Current).Row["per_id"]);
                    Per_Name = ((DataRowView)_Bs2.Current).Row[connection.Lang_id == 1 ? "Per_AName" : "Per_EName"].ToString();

                }
                catch
                {
                    per_id = 0;
                    Per_Name = Txt_Name.Text.Trim();
                }

                Con_Id = Convert.ToInt16(((DataRowView)_Bs_3.Current).Row["Con_ID"]);
                COUN_K_PH = ((DataRowView)_Bs_3.Current).Row["COUN_K_PH"].ToString();
                Per_Occupation_Aname = ((DataRowView)_Bs_5.Current).Row["Job_AName"].ToString();
                Per_Occupation_Ename = ((DataRowView)_Bs_5.Current).Row["Job_EName"].ToString();
                try
                {
                    Per_inf_TBL.Rows.Clear();

                    Per_inf_TBL.Rows.Add(new object[] { per_id, Per_Name, connection.Lang_id == 1 ? Txt_EName.Text.Trim():Per_Name, CboCit_Id.SelectedValue, Con_Id ,  txt_birth_place.Text.Trim() ,TxtPhone.Text.Trim(),string.Empty ,
                                                     string.Empty , CboDoc_id.SelectedValue,TxtNo_Doc.Text,Doc_date,Doc_Edate,TxtIss_Doc.Text, CboNat_id.SelectedValue,Birth_date,Cbo_Gender.SelectedValue,
                                                     Per_Occupation_Aname, TxtAmotherName.Text,0,0,0,COUN_K_PH ,((DataRowView)_Bs_3.Current).Row["cit_Aname"].ToString(),((DataRowView)_Bs_3.Current).Row["cit_Ename"].ToString(),((DataRowView)_Bs_3.Current).Row["Con_Aname"].ToString(),((DataRowView)_Bs_3.Current).Row["Con_Ename"].ToString(), 
                                                     Txt_Street.Text.Trim(),Txt_Suburb.Text.Trim(),Txt_Post_code.Text.Trim(),TxtA_Address.Text.Trim(),((DataRowView)_Bs_2.Current).Row["Fmd_Aname"].ToString(),((DataRowView)_Bs_2.Current).Row["Fmd_Ename"].ToString(),((DataRowView)_Bs1.Current).Row["Nat_ANAME"].ToString(),
                                                    ((DataRowView)_Bs1.Current).Row["Nat_ENAME"].ToString(),((DataRowView)_Bs_4.Current).Row["Gender_Aname"].ToString(),((DataRowView)_Bs_4.Current).Row["Gender_Ename"].ToString(),0
                                                    , TxtIss_Doc.Text.Trim(), Per_Occupation_Ename, TxtAmotherName.Text.Trim(), txt_birth_place.Text.Trim(), Txt_Street.Text.Trim(), Txt_Suburb.Text.Trim(), TxtA_Address.Text.Trim(), txt_sin_id.Text,resd_cmb.SelectedIndex,Cbo_Occupation.SelectedValue,txt_Job_details.Text.Trim()});


                }
                catch { }
            #endregion


                if (Txt_Name.Text != "" || Grd_Per.RowCount > 0)
                {


                    try
                    {

                        connection.SqlExec("select (isnull((select  distinct cond_bl_rem FROM  TBL_BList_online_Search),0)) as cond_bl_rem", "cond_bl_rem_tbl");
                        Int16 cond_bl_rem = Convert.ToInt16(connection.SQLDS.Tables["cond_bl_rem_tbl"].Rows[0]["cond_bl_rem"]);

                        if (connection.SQLDS.Tables.Contains("per_rem_blacklist_tbl"))
                        {
                            connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
                        }

                        if (cond_bl_rem != 0)
                        {

                            Per_blacklist_tbl.Rows.Clear();

                            Per_blacklist_tbl.Rows.Add(Grd_Per.Rows.Count > 0 ? ((DataRowView)_Bs2.Current).Row[connection.Lang_id == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Name.Text.Trim(), Txt_EName.Text.Trim());


                            try
                            {
                                connection.SQLCS.Open();
                                connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
                                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                                connection.SQLCMD.Connection = connection.SQLCS;
                                connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                                IDataReader obj = connection.SQLCMD.ExecuteReader();
                                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                                obj.Close();
                                connection.SQLCS.Close();
                                connection.SQLCMD.Parameters.Clear();
                                connection.SQLCMD.Dispose();
                            }

                            catch
                            {
                                connection.SQLCS.Close();
                                connection.SQLCMD.Parameters.Clear();
                                connection.SQLCMD.Dispose();
                            }

                            if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
                            {
                                Rem_Blacklist_OK AddFrm = new Rem_Blacklist_OK();
                                AddFrm.ShowDialog(this);

                                if (Rem_Blacklist_OK.Back_btn == 1)
                                {
                                    Get_Black_list();
                                    this.Close();
                                    return;
                                }

                            }
                            else
                            {
                                Rem_Blacklist_OK.Back_btn = 0;
                                Rem_Blacklist_OK.Notes_BL = "";
                            }
                        }
                    }

                    catch { }
                }


                connection.SQLCMD.Parameters.AddWithValue("@Person_Info_Type", Per_inf_TBL);
                connection.SQLCMD.Parameters.AddWithValue("@registerd_ID", 0);
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@Form_id", per_id == 0 ? 1 : 2);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                connection.SqlExec("Person_info_Add_Upd_2", connection.SQLCMD);

                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                        !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Rpt_Per_id)))
                {
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                    connection.SQLCMD.Parameters.Clear();
                    //this.Enabled = true;
                    return;
                }

                connection.SQLCMD.Parameters.Clear();
                Per_inf_TBL.Rows.Clear();
            }
            if ((Convert.ToInt16(Txt_Name.Tag) == 1))
            {
                Rpt_Per_id = Convert.ToInt32(((DataRowView)_Bs2.Current).Row["per_id"]);
            }
            //-------------------------------------------اضافة القيد في الفاوجر
            connection.SQLCMD.Parameters.AddWithValue("@VoucherType", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@Per_id", Rpt_Per_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Cust_Id", TxtBox_User.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Catg_Id", CboCatg_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@DiscountAmount", Mdiff_Amoun);
            connection.SQLCMD.Parameters.AddWithValue("@LOC_CUR_ID", Txt_Loc_Cur.Tag);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", 13);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", TxtIn_Rec_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@NOTES_BL", Rem_Blacklist_OK.Notes_BL.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@person_name", Grd_Per.Rows.Count > 0 ? connection.Lang_id == 1 ? ((DataRowView)_Bs2.Current).Row["Per_AName"].ToString() : ((DataRowView)_Bs2.Current).Row["Per_EName"].ToString() : Txt_Name.Text.Trim());
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_New_Voucher", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out VO_NO)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                this.Enabled = true;
                return;
            }

            //-------------------------------------------

            for (int i = 0; i < Bill_Cur.Rows.Count; i++)//To set Real Price field to the exch_rate_real value to show on the print
            {
                Bill_Cur.Rows[i].SetField("Real_price",Convert.ToDecimal(Bill_Cur.Rows[i]["Exch_Rate_Real"]));
            }
            print_Rpt_Pdf();

            connection.SQLCMD.Parameters.Clear();
            Bill_Cur.Rows.Clear();

            TxtDiscount_Amount.ResetText();
            TxtTLoc_Amount.ResetText();
            Txt_Name.Text = "";
            Clear_All_Texts();



        }
        //----------------------------
        private void TxtDiscount_Amount_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(TxtDiscount_Amount.Text) > 0)
            {
                LblDiscount.Text = connection.Lang_id == 1 ? "ممنوح" : "Granted";
            }
            else if (Convert.ToDecimal(TxtDiscount_Amount.Text) < 0)
            {
                LblDiscount.Text = connection.Lang_id == 1 ? "مكتسب" : "Earned";
            }
            else
            {
                LblDiscount.Text = "";
            }
        }
        //----------------------------
        private void ExtBtn_Click(object sender, System.EventArgs e)
        {
            //  this.Dispose();
            this.Close();
        }
        //----------------------------
        private void Buy_BillCurrency_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Catg_Tbl", "Bill_Acc_No", "Cur_Buy_Sal_Tbl", "Limit_Buy_Sale_Tbl", "Bill_Cur", "Insert_Per_Back", "all_master", "all_master1", "all_master2", "Per_Info", "all_master4", "all_master6" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //----------------------------
        private void Grd_Cust_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                btn_browser_Click(sender, e);
            }
        }
        //----------------------------
        private void Txt_Name_TextChanged(object sender, EventArgs e)
        {
            chan_buy = false;
            chk_trans.Checked = false;
            if (Txt_Name.Text.Trim() != "")
            {

                connection.SqlExec(" exec get_person_info_1 '" + Txt_Name.Text + "'", "Per_Info");
                _Bs2.DataSource = connection.SQLDS.Tables["Per_Info"];

                if (connection.SQLDS.Tables["Per_Info"].Rows.Count > 0)
                {
                    chan_buy = false;
                    Grd_Per.DataSource = _Bs2;

                    TxtNo_Doc.DataBindings.Clear();
                    TxtIss_Doc.DataBindings.Clear();
                    TxtDoc_Da.DataBindings.Clear();
                    TxtBirth_Da.DataBindings.Clear();
                    Cbo_Occupation.DataBindings.Clear();
                    TxtA_Address.DataBindings.Clear();
                    TxtExp_Da.DataBindings.Clear();
                    CboDoc_id.DataBindings.Clear();
                    CboNat_id.DataBindings.Clear();
                    CboCit_Id.DataBindings.Clear();
                    CboCit_Id.DataBindings.Clear();
                    TxtAmotherName.DataBindings.Clear();
                    TxtPhone.DataBindings.Clear();
                    Txt_Suburb.DataBindings.Clear();
                    Txt_Street.DataBindings.Clear();
                    Txt_EName.DataBindings.Clear();
                    Txt_Post_code.DataBindings.Clear();
                    Txt_Name.DataBindings.Clear();
                    Cbo_Gender.DataBindings.Clear();
                    resd_cmb.DataBindings.Clear();
                    txt_sin_id.DataBindings.Clear();
                    txt_birth_place.DataBindings.Clear();
                    txt_Job_details.DataBindings.Clear();
                    TxtNo_Doc.DataBindings.Add("Text", _Bs2, "per_Frm_Doc_NO");
                    TxtIss_Doc.DataBindings.Add("Text", _Bs2, connection.Lang_id == 1 ? "per_Frm_Doc_IS" : "per_EFrm_Doc_IS");
                    TxtDoc_Da.DataBindings.Add("Text", _Bs2, "per_Frm_Doc_Date");
                    TxtBirth_Da.DataBindings.Add("Text", _Bs2, "per_Birth_day");
                    Cbo_Occupation.DataBindings.Add("SelectedValue", _Bs2, "per_Occupation_id");
                    TxtA_Address.DataBindings.Add("Text", _Bs2, connection.Lang_id == 1 ? "Per_State" : "Per_EState");
                    TxtExp_Da.DataBindings.Add("Text", _Bs2, "per_Frm_Doc_EDate");
                    TxtAmotherName.DataBindings.Add("Text", _Bs2, connection.Lang_id == 1 ? "per_Mother_name" : "Mother_Ename");
                    TxtPhone.DataBindings.Add("Text", _Bs2, "per_Phone");
                    Txt_Suburb.DataBindings.Add("text", _Bs2, connection.Lang_id == 1 ? "Per_Suburb" : "Per_ESuburb");
                    Txt_Street.DataBindings.Add("text", _Bs2, connection.Lang_id == 1 ? "Per_Street" : "Per_EStreet");
                    Txt_EName.DataBindings.Add("text", _Bs2, "Per_EName");
                    Txt_Post_code.DataBindings.Add("text", _Bs2, "Per_Post_Code");
                    CboNat_id.DataBindings.Add("SelectedValue", _Bs2, "per_Nationalty_ID");
                    CboDoc_id.DataBindings.Add("SelectedValue", _Bs2, "per_Frm_Doc_ID");
                    CboCit_Id.DataBindings.Add("SelectedValue", _Bs2, "per_City_ID");
                    Txt_Name.DataBindings.Add("Tag", _Bs2, "registerd_ID");
                    Cbo_Gender.DataBindings.Add("SelectedValue", _Bs2, "per_Gender_ID");
                    txt_sin_id.DataBindings.Add("text", _Bs2, "Social_No");
                    txt_birth_place.DataBindings.Add("text", _Bs2, connection.Lang_id == 1 ? "Per_Birth_place" : "Per_EBirth_place");
                    resd_cmb.DataBindings.Add("SelectedIndex", _Bs2, "resd_flag");
                    txt_Job_details.DataBindings.Add("Text", _Bs2, "details_job");


                    chan_buy = true;
                    Grd_Per_SelectionChanged(null, null);

                }

                else
                {
                    Clear_All_Texts();
                    label77.Text = "";
                }
            }
            else
            {
                Clear_All_Texts();
                label77.Text = "";
            }

        }
        //----------------------------
        private void Clear_All_Texts()
        {


            TxtNo_Doc.ResetText();
            TxtIss_Doc.ResetText();
            Cbo_Occupation.SelectedValue = 0;
            TxtA_Address.ResetText();
            TxtAmotherName.ResetText();
            TxtPhone.ResetText();
            Txt_Suburb.ResetText();
            Txt_Street.ResetText();
            Txt_Post_code.ResetText();
            TxtA_Address.ResetText();
            Txt_Suburb.ResetText();
            Txt_Street.ResetText();
            txt_Job_details.ResetText();
            Txt_EName.ResetText();
            txt_birth_place.ResetText();
            txt_sin_id.ResetText();
            Txt_Post_code.ResetText();
            CboDoc_id.SelectedValue = 0;
            CboNat_id.SelectedValue = 0;
            CboCit_Id.SelectedValue = 0;
            Cbo_Gender.SelectedValue = 0;
            TxtExp_Da.Text = "";
            TxtDoc_Da.Text = "";
            TxtBirth_Da.Text = "";
            resd_cmb.SelectedIndex = 0;
            Txt_Name.Tag = 0;
            Grd_Per.DataSource = new BindingSource();


            //-----------------------------------------
            TxtNo_Doc.Enabled = true;
            TxtIss_Doc.Enabled = true;
            Cbo_Occupation.Enabled = true;
            TxtA_Address.Enabled = true;
            TxtAmotherName.Enabled = true;
            TxtPhone.Enabled = true;
            Txt_Suburb.Enabled = true;
            Txt_Street.Enabled = true;
            Txt_Post_code.Enabled = true;
            TxtA_Address.Enabled = true;
            Txt_Suburb.Enabled = true;
            Txt_Street.Enabled = true;
            txt_birth_place.Enabled = true;
            txt_sin_id.Enabled = true;
            Txt_Post_code.Enabled = true;
            CboDoc_id.Enabled = true;
            CboNat_id.Enabled = true;
            CboCit_Id.Enabled = true;
            Cbo_Gender.Enabled = true;
            TxtExp_Da.Enabled = true;
            TxtDoc_Da.Enabled = true;
            TxtBirth_Da.Enabled = true;
            resd_cmb.Enabled = true;



        }
        //---------------------------------
        private void TxtAmotherName_KeyPress(object sender, KeyPressEventArgs e)
        {
            PreventChar(e);
        }
        //----------------------------
        private void print_Rpt_Pdf()
        {
            //-------------------------report
            Sql_Text = " select Per_Id   ,Per_AName as Aper_Name,Per_EName as Eper_Name "
                    + " from person_info "
                    + " where per_id = " + Rpt_Per_id;


            connection.SqlExec(Sql_Text, "Insert_Per_Back");



            string Anote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Anote_report"].ToString();
            string Enote_report = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Enote_report"].ToString();

            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            double Total_Amount = Convert.ToDouble(TxtTLoc_Amount.Text) + Convert.ToDouble(TxtDiscount_Amount.Text);
            ToWord toWord = new ToWord(Total_Amount,
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            Decimal Dis = Convert.ToDecimal(TxtDiscount_Amount.Text);
            Bill_Cur.Rows[MCRow].SetField("Cur_ToWord", Cur_ToWord);
            Bill_Cur.Rows[MCRow].SetField("Cur_ToEWord", Cur_ToEWord);
            Bill_Cur.Rows[MCRow].SetField("Discount_amount", Dis);
            Bill_Cur.Rows[MCRow].SetField("Oper_Id", 13);
            //--------------------------

            C_Date = Convert.ToDateTime(connection.SQLCMD.Parameters["@Param_Result_CDate"].Value);
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            Box_ACust_Cur = Cur_Code + "/" + "الحساب الدائن" + ":" + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Acust_Name"].ToString();
            Box_ECust_Cur = "Credit Account : " + connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;
            string Catg_Ename = connection.SQLDS.Tables["Catg_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Ename"].ToString();
            string Catg_AEname = connection.SQLDS.Tables["Catg_Tbl"].Rows[Convert.ToInt16(CboCatg_Id.SelectedIndex)]["Catg_Aname"].ToString();
            Buy_Sale_BillCurrency_Rpt ObjRpt = new Buy_Sale_BillCurrency_Rpt();
            Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", Catg_AEname);
            Dt_Param.Rows.Add("Catg_EName", Catg_Ename);
            Dt_Param.Rows.Add("Vo_No", VO_NO);
            Dt_Param.Rows.Add("Oper_Id", 13);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("Box_ACust_Cur", Box_ACust_Cur);
            Dt_Param.Rows.Add("Box_ECust_Cur", Box_ECust_Cur);
            Dt_Param.Rows.Add("Anote_report", Anote_report);
            Dt_Param.Rows.Add("Enote_report", Enote_report);

            DataRow Dr;
            int y = Bill_Cur.Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = Bill_Cur.NewRow();
                Bill_Cur.Rows.Add(Dr);
            }
            connection.SQLDS.Tables.Add(Bill_Cur);
            // connection.SQLDS.Tables.Add(Insert_Per_Back);

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, 13);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Bill_Cur");
            connection.SQLDS.Tables.Remove("Insert_Per_Back");

        }
        //----------------------------
        private void Grd_Per_SelectionChanged(object sender, EventArgs e)
        {
            if (chan_buy)
            {
                chk_trans.Checked = false;
                //if (Txt_EName.Text == "")
                //{ chk_trans.Enabled = true; }
                //else { chk_trans.Enabled = false; }

                if (_Bs2.List.Count != 0 && Convert.ToInt16(Txt_Name.Tag) == 1)//معرف من قبل الادارة المعلومات مغلقة
                {
                    TxtNo_Doc.Enabled = false;
                    TxtIss_Doc.Enabled = false;
                    TxtDoc_Da.Enabled = false;
                    TxtBirth_Da.Enabled = false;
                    Cbo_Occupation.Enabled = false;
                    TxtA_Address.Enabled = false;
                    TxtExp_Da.Enabled = false;
                    CboDoc_id.Enabled = false;
                    CboNat_id.Enabled = false;
                    CboCit_Id.Enabled = false;
                    CboCit_Id.Enabled = false;
                    TxtAmotherName.Enabled = false;
                    TxtPhone.Enabled = false;
                    Cbo_Gender.Enabled = false;

                    //  Txt_Ename.Enabled = false;
                    Txt_Street.Enabled = false;
                    Txt_Suburb.Enabled = false;
                    txt_sin_id.Enabled = false;
                    txt_birth_place.Enabled = false;
                    Txt_Post_code.Enabled = false;
                    resd_cmb.Enabled = false;




                }
                else
                {
                    TxtNo_Doc.Enabled = true;
                    TxtIss_Doc.Enabled = true;
                    TxtDoc_Da.Enabled = true;
                    TxtBirth_Da.Enabled = true;
                    Cbo_Occupation.Enabled = true;
                    TxtA_Address.Enabled = true;
                    TxtExp_Da.Enabled = true;
                    CboDoc_id.Enabled = true;
                    CboNat_id.Enabled = true;
                    CboCit_Id.Enabled = true;
                    CboCit_Id.Enabled = true;
                    TxtAmotherName.Enabled = true;
                    TxtPhone.Enabled = true;
                    Cbo_Gender.Enabled = true;
                    // Txt_Ename.Enabled = true;
                    Txt_Street.Enabled = true;
                    Txt_Suburb.Enabled = true;
                    Txt_Post_code.Enabled = true;
                    txt_sin_id.Enabled = true;
                    txt_birth_place.Enabled = true;
                    resd_cmb.Enabled = true;


                }

                try
                {

                    string[] Used_Tbl = { "tot_sale_tbl1", "tot_buy_tbl1", "sale_buy_day_tbl1" };
                    foreach (string Tbl in Used_Tbl)
                    {
                        if (connection.SQLDS.Tables.Contains(Tbl))
                        {
                            connection.SQLDS.Tables.Remove(Tbl);
                        }
                    }

                    per_id = Convert.ToInt32(((DataRowView)_Bs2.Current).Row["per_ID"]);

                    connection.SQLCS.Open();
                    connection.SQLCMD.CommandText = "no_Rec_vou_buy_sale";
                    connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                    connection.SQLCMD.Connection = connection.SQLCS;
                    connection.SQLCMD.Parameters.AddWithValue("@per_id", per_id);

                    IDataReader obj = connection.SQLCMD.ExecuteReader();
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "tot_sale_tbl1");
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "tot_buy_tbl1");
                    connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "sale_buy_day_tbl1");

                    obj.Close();
                    connection.SQLCS.Close();
                    connection.SQLCMD.Parameters.Clear();
                    connection.SQLCMD.Dispose();


                    if (connection.SQLDS.Tables["tot_sale_tbl1"].Rows.Count > 0 || connection.SQLDS.Tables["tot_buy_tbl1"].Rows.Count > 0)
                    {
                        label77.Visible = true;

                        if (connection.Lang_id == 1)
                        {

                            label77.Text = " مبالغ البيع و الشراء من قبل الزبون لمدة " + connection.SQLDS.Tables["sale_buy_day_tbl1"].Rows[0]["buy_sale_day"].ToString() + " يوم  ";
                        }
                        else
                        {
                            label77.Text = "Amounts of buy and sale for a period of" + connection.SQLDS.Tables["sale_buy_day_tbl1"].Rows[0]["buy_sale_day"].ToString() + " day  ";
                        }
                    }
                    else
                    { label77.Text = ""; }
                }
                catch
                { label77.Text = ""; }


            }

        }

        private void Buy_BillCurrency_Add_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                //case Keys.F7:
                //    UpdBtn_Click(sender, e);
                //    break;
                //case Keys.F5:
                //    AllBtn_Click(sender, e);
                //    break;
                //case Keys.F4:
                //    AllBtn_Click(sender, e);
                //break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }




        private void label77_Click(object sender, EventArgs e)
        {

            no_rec_buy_sale AddFrm = new no_rec_buy_sale();
            AddFrm.ShowDialog();

        }

        private void chk_trans_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_trans.Checked == true)
            {
                string trns_name = "";


                if (Grd_Per.Rows.Count > 0)
                {

                    trns_name = ((DataRowView)_Bs2.Current).Row["Per_AName"].ToString();

                }
                else
                {

                    trns_name = Txt_Name.Text.Trim();
                }


                if (trns_name != "")
                {
                    string strTranslatedText = null;
                    try
                    {
                        TranslatorService.LanguageServiceClient client = new TranslatorService.LanguageServiceClient();
                        client = new TranslatorService.LanguageServiceClient();
                        strTranslatedText = client.Translate("6CE9C85A41571C050C379F60DA173D286384E0F2", trns_name, "", "en");
                        Txt_EName.Text = strTranslatedText;
                    }
                    catch
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يوجد خطأ ما،يرجى المحاولة لاحقاً" : "Tere is something went wrong,Try again later", MyGeneral_Lib.LblCap);
                        return;
                    }
                }




            }

            if (chk_trans.Checked == false)
            {

                if (Grd_Per.Rows.Count > 0)
                {
                    Txt_EName.DataBindings.Clear();
                    Txt_EName.DataBindings.Add("text", _Bs2, "Per_EName");
                }

                else { Txt_EName.Text = ""; }
            }
        }

        private void label_BL_Click(object sender, EventArgs e)
        {
            if ((Convert.ToInt16(Txt_Name.Tag) == 0) && (Txt_Name.Text.Trim() != ""))
            {

                try
                {

                    if (connection.SQLDS.Tables.Contains("per_rem_blacklist_tbl"))
                    {
                        connection.SQLDS.Tables.Remove("per_rem_blacklist_tbl");
                    }

                    //string EName_str = "";


                    //if (Txt_EName.Text.Trim() == "")
                    //     { EName_str = connection.SQLDS.Tables["Per_Info"].Rows.Count > 0 ? ((DataRowView)_Bs2.Current).Row[connection.Lang_id == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Name.Text.Trim(); }
                    //else { EName_str = Txt_EName.Text.Trim(); }


                    Per_blacklist_tbl.Rows.Clear();

                    Per_blacklist_tbl.Rows.Add(Grd_Per.Rows.Count > 0 ? ((DataRowView)_Bs2.Current).Row[connection.Lang_id == 1 ? "Per_AName" : "Per_EName"].ToString() : Txt_Name.Text.Trim(), Txt_EName.Text.Trim());

                    try
                    {
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[per_rem_blacklist]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@name_similer", Per_blacklist_tbl);
                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                        connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "per_rem_blacklist_tbl");
                        obj.Close();
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                    }

                    catch
                    {
                        connection.SQLCS.Close();
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                    }

                    if (connection.SQLDS.Tables["per_rem_blacklist_tbl"].Rows.Count > 0)
                    {
                        Rem_Blacklist AddFrm = new Rem_Blacklist();
                        AddFrm.ShowDialog(this);
                    }

                    else
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد قوائم للعرض" : "There are no lists to display", MyGeneral_Lib.LblCap);
                        return;
                    }
                }

                catch { }

            }

        }
        //---------------------------
        private void Get_Black_list()
        {
            int per_id_refuse = 0;
            string per_name_refuse = "";
            per_id_refuse = Grd_Per.Rows.Count > 0 ? Convert.ToInt32(((DataRowView)_Bs2.Current).Row["per_id"]) : 0;
            per_name_refuse = Grd_Per.Rows.Count > 0 ? connection.Lang_id == 1 ? ((DataRowView)_Bs2.Current).Row["per_aname"].ToString() : ((DataRowView)_Bs2.Current).Row["per_ename"].ToString() : Txt_Name.Text.Trim();
            string[] Column = { "for_cur_id", "Amount" };

            string[] DType = { "System.Int32", "System.Decimal" };

            Refuse_buy_sale_tbl = CustomControls.Custom_DataTable("Refuse_buy_sale_tbl", Column, DType);

            Refuse_buy_sale_tbl = Bill_Cur.DefaultView.ToTable(false, "For_Cur_id", "For_Amount").Select().CopyToDataTable();


            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Refuse_buy_sale]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.Parameters.AddWithValue("@note_user", Rem_Blacklist_OK.Notes_BL.Trim());
                connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                connection.SQLCMD.Parameters.AddWithValue("@oper_id", 13);
                connection.SQLCMD.Parameters.AddWithValue("@per_id", per_id_refuse);
                connection.SQLCMD.Parameters.AddWithValue("@per_name", per_name_refuse);
                connection.SQLCMD.Parameters.AddWithValue("@Refuse_buy_sale", Refuse_buy_sale_tbl);

                IDataReader obj = connection.SQLCMD.ExecuteReader();

                obj.Close();
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

            catch
            {
                connection.SQLCS.Close();
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
            }

        }

    }
}