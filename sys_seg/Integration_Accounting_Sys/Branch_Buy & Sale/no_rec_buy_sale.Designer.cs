﻿namespace Integration_Accounting_Sys
{
    partial class no_rec_buy_sale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Grd_buy = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_per_Aname = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_sale = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_per_Ename = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Tot_Amount_sale_buy = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Tot_Amount_sale = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Tot_Amount_buy = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_buy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_sale)).BeginInit();
            this.SuspendLayout();
            // 
            // Grd_buy
            // 
            this.Grd_buy.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_buy.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_buy.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_buy.ColumnHeadersHeight = 30;
            this.Grd_buy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column5});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_buy.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grd_buy.Location = new System.Drawing.Point(3, 51);
            this.Grd_buy.Name = "Grd_buy";
            this.Grd_buy.RowHeadersWidth = 15;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_buy.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_buy.Size = new System.Drawing.Size(575, 123);
            this.Grd_buy.TabIndex = 2;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "cur_aname";
            this.Column1.HeaderText = "عملة الشراء";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "tot_FOR_AMOUNT_buy";
            this.Column2.HeaderText = "مبالغ الشراء بالعملة الاصلية";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.DataPropertyName = "tot_LOC_AMOUNT_buy";
            this.Column5.HeaderText = "مبالغ الشراء بالعملة المحلية";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Txt_per_Aname
            // 
            this.Txt_per_Aname.BackColor = System.Drawing.Color.White;
            this.Txt_per_Aname.Enabled = false;
            this.Txt_per_Aname.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_per_Aname.Location = new System.Drawing.Point(5, 7);
            this.Txt_per_Aname.Name = "Txt_per_Aname";
            this.Txt_per_Aname.Size = new System.Drawing.Size(273, 23);
            this.Txt_per_Aname.TabIndex = 185;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Maroon;
            this.label22.Location = new System.Drawing.Point(5, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 14);
            this.label22.TabIndex = 649;
            this.label22.Text = "مبالغ الشراء....";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(76, 44);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(520, 1);
            this.flowLayoutPanel6.TabIndex = 648;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(5, 196);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 14);
            this.label1.TabIndex = 652;
            this.label1.Text = "مبالغ البيع....";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(81, 206);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(529, 1);
            this.flowLayoutPanel1.TabIndex = 651;
            // 
            // Grd_sale
            // 
            this.Grd_sale.AllowUserToAddRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_sale.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_sale.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_sale.ColumnHeadersHeight = 30;
            this.Grd_sale.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column4,
            this.Column6});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = null;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_sale.DefaultCellStyle = dataGridViewCellStyle7;
            this.Grd_sale.Location = new System.Drawing.Point(5, 214);
            this.Grd_sale.Name = "Grd_sale";
            this.Grd_sale.RowHeadersWidth = 15;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_sale.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_sale.Size = new System.Drawing.Size(573, 123);
            this.Grd_sale.TabIndex = 650;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "cur_aname";
            this.Column3.HeaderText = "عملة البيع";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.DataPropertyName = "tot_FOR_AMOUNT_sale";
            this.Column4.HeaderText = "مبالغ البيع بالعملة الاصلية ";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column6.DataPropertyName = "tot_LOC_AMOUNT_sale";
            this.Column6.HeaderText = "مبالغ البيع بالعملة المحلية";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Txt_per_Ename
            // 
            this.Txt_per_Ename.BackColor = System.Drawing.Color.White;
            this.Txt_per_Ename.Enabled = false;
            this.Txt_per_Ename.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_per_Ename.Location = new System.Drawing.Point(299, 7);
            this.Txt_per_Ename.Name = "Txt_per_Ename";
            this.Txt_per_Ename.Size = new System.Drawing.Size(273, 23);
            this.Txt_per_Ename.TabIndex = 653;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(198, 182);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(210, 14);
            this.label52.TabIndex = 1128;
            this.label52.Text = "مجموع مبالغ الشراء بالعملة المحلية";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(206, 345);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 14);
            this.label2.TabIndex = 1130;
            this.label2.Text = "مجموع مبالغ البيع بالعملة المحلية";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(161, 380);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(247, 14);
            this.label3.TabIndex = 1132;
            this.label3.Text = "مجموع مبالغ البيع والشراء بالعملة المحلية";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(-6, 371);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(609, 1);
            this.flowLayoutPanel2.TabIndex = 1134;
            // 
            // Tot_Amount_sale_buy
            // 
            this.Tot_Amount_sale_buy.BackColor = System.Drawing.Color.White;
            this.Tot_Amount_sale_buy.Enabled = false;
            this.Tot_Amount_sale_buy.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount_sale_buy.Location = new System.Drawing.Point(419, 376);
            this.Tot_Amount_sale_buy.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount_sale_buy.Name = "Tot_Amount_sale_buy";
            this.Tot_Amount_sale_buy.NumberDecimalDigits = 3;
            this.Tot_Amount_sale_buy.NumberDecimalSeparator = ".";
            this.Tot_Amount_sale_buy.NumberGroupSeparator = ",";
            this.Tot_Amount_sale_buy.ReadOnly = true;
            this.Tot_Amount_sale_buy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Tot_Amount_sale_buy.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount_sale_buy.TabIndex = 1133;
            this.Tot_Amount_sale_buy.Text = "0.000";
            // 
            // Tot_Amount_sale
            // 
            this.Tot_Amount_sale.BackColor = System.Drawing.Color.White;
            this.Tot_Amount_sale.Enabled = false;
            this.Tot_Amount_sale.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount_sale.Location = new System.Drawing.Point(417, 341);
            this.Tot_Amount_sale.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount_sale.Name = "Tot_Amount_sale";
            this.Tot_Amount_sale.NumberDecimalDigits = 3;
            this.Tot_Amount_sale.NumberDecimalSeparator = ".";
            this.Tot_Amount_sale.NumberGroupSeparator = ",";
            this.Tot_Amount_sale.ReadOnly = true;
            this.Tot_Amount_sale.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Tot_Amount_sale.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount_sale.TabIndex = 1131;
            this.Tot_Amount_sale.Text = "0.000";
            // 
            // Tot_Amount_buy
            // 
            this.Tot_Amount_buy.BackColor = System.Drawing.Color.White;
            this.Tot_Amount_buy.Enabled = false;
            this.Tot_Amount_buy.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tot_Amount_buy.Location = new System.Drawing.Point(418, 178);
            this.Tot_Amount_buy.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Tot_Amount_buy.Name = "Tot_Amount_buy";
            this.Tot_Amount_buy.NumberDecimalDigits = 3;
            this.Tot_Amount_buy.NumberDecimalSeparator = ".";
            this.Tot_Amount_buy.NumberGroupSeparator = ",";
            this.Tot_Amount_buy.ReadOnly = true;
            this.Tot_Amount_buy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Tot_Amount_buy.Size = new System.Drawing.Size(157, 23);
            this.Tot_Amount_buy.TabIndex = 1129;
            this.Tot_Amount_buy.Text = "0.000";
            // 
            // no_rec_buy_sale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 406);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.Tot_Amount_sale_buy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Tot_Amount_sale);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Tot_Amount_buy);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.Txt_per_Ename);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Grd_sale);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.Txt_per_Aname);
            this.Controls.Add(this.Grd_buy);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "no_rec_buy_sale";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "مبالغ البيع والشراء";
            this.Load += new System.EventHandler(this.no_rec_buy_sale_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_buy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_sale)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Grd_buy;
        private System.Windows.Forms.TextBox Txt_per_Aname;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridView Grd_sale;
        private System.Windows.Forms.TextBox Txt_per_Ename;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount_buy;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount_sale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Sample.DecimalTextBox Tot_Amount_sale_buy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}