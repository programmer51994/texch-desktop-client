﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Cur_Pic_Main : Form
    {
        #region Defintion
            string Sql_Text = "";
            int MCur_id = 0;
            bool chang_cbo = false;
            bool chang_cbo_cur = false;
            int Mid_identity = 0;
            DataTable Dt = new DataTable();
            BindingSource cur_Bs = new BindingSource();
            Byte[] img_data = new Byte[0];
            DataTable Dt_Pic = new DataTable();
            DataTable Dt_N_P = new DataTable();
        #endregion

        public Cur_Pic_Main()//
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            MCur_id = 0;
        }

        public Cur_Pic_Main(int MFor_Amount = 0)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            MCur_id = MFor_Amount;
        }
        //-------------------------------------------------------------------
        private void Cur_Pic_Main_Load(object sender, EventArgs e)
        {
                chang_cbo_cur = false;
                Sql_Text = " SELECT distinct A.cur_id,B.Cur_ENAME ,B.Cur_ANAME "
                          + " From cur_pic A,Cur_Tbl B "
                          + " Where A.CUR_ID=B.Cur_ID ";
                          

            if ( MCur_id >0 )
            {
                Sql_Text += "And A.cur_id = "+MCur_id;
                Cbo_cur.Enabled = false;
            }
            Sql_Text += " Order By Cur_ANAME ";
 
                Cbo_cur.DataSource = connection.SqlExec(Sql_Text, "Cur_Name_Tbl");
                if (connection.SQLDS.Tables["Cur_Name_Tbl"].Rows.Count > 0)
                {
                    Cbo_cur.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
                    Cbo_cur.ValueMember = "cur_id";
                    chang_cbo_cur = true;
                    Cbo_cur_SelectedIndexChanged(sender, e);
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد صورة لهذه العملة" : "No Picture for this currency", MyGeneral_Lib.LblCap);
                    this.Close();
                }
      
        }
        //-------------------------------------------------------------------
        private void Display_Pic(DataTable DT)
        {
            img_data = (Byte[])(DT.Rows[0]["cur_pic"]);
            MemoryStream mem = new MemoryStream(img_data);
            pictureBox1.Image = Image.FromStream(mem);
        }
        //-------------------------------------------------------------------
        private void Cbo_cur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_cbo_cur)
            {
                chang_cbo = false;
                Sql_Text = " SELECT A.cur_id, 'division1' = case  when CAST(division as int ) > 0 "
                +" then cast(CAST(division as int )AS varchar(40)) "
                + "when  CAST(division as int )  <= 0  then cast(division as varchar(50))"
                + " end,division, B.Cur_ANAME ,B.Cur_ENAME"
                + " FROM  currency_division A, Cur_Tbl B"
                + " where A.cur_id = B.Cur_ID "
                + " And A.cur_id = " + Cbo_cur.SelectedValue
                + " And  division in (Select  division_cur from cur_pic where cur_id = "+ Cbo_cur.SelectedValue +")"
                +" Order by " + (connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME");
                CboDiv_id.DataSource = connection.SqlExec(Sql_Text, "currency_division");
                CboDiv_id.DisplayMember = "division1";
                CboDiv_id.ValueMember = "division";


                if (connection.SQLDS.Tables["currency_division"].Rows.Count > 0)
                {
                    chang_cbo = true;
                    CboDiv_id_SelectedIndexChanged(null, null);
                }
            }
        }
        //-------------------------------------------------------------------
        private void CboDiv_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chang_cbo)
            {
                Sql_Text = " SELECT CAST(CAST(cur_pic as varchar(max)) as varbinary(max)) as cur_pic ,"
                        + " id_identity_cur FROM cur_pic "
                        + " where cur_id =  " + Cbo_cur.SelectedValue
                        + " and division_cur = " + CboDiv_id.SelectedValue;
                connection.SqlExec(Sql_Text, "cur_pic");
                try
                {
                    Dt_Pic = connection.SQLDS.Tables["cur_pic"].DefaultView.ToTable(false, "cur_pic", "id_identity_cur").Select("id_identity_cur = max(id_identity_cur)").CopyToDataTable();
                    Display_Pic(Dt_Pic);
                    Mid_identity = Convert.ToInt32(Dt_Pic.Rows[0]["id_identity_cur"]);
                    if (connection.SQLDS.Tables["cur_pic"].Rows.Count == 1)
                    {
                        AddBtn.Enabled = false;
                        Btn_Excel.Enabled = false;
                    }
                    else
                    {
                        Btn_Excel.Enabled = true;
                    }

                }
                catch
                {
                    pictureBox1.Image = null;
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد صورة لهذه العملة" : "No Picture for this currency", MyGeneral_Lib.LblCap);

                }

            }
        }
        //-------------------------------------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            Mid_identity = Mid_identity + 1;
            try
            {
                Dt_N_P = connection.SQLDS.Tables["cur_pic"].DefaultView.ToTable(false, "cur_pic", "id_identity_cur").Select("id_identity_cur = " + Mid_identity).CopyToDataTable();
                Display_Pic(Dt_N_P);
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد صورة اخرى للعملة" : "No other Picture for this currency", MyGeneral_Lib.LblCap);
                Mid_identity--;
            }
        }
        //-------------------------------------------------------------------
        private void Btn_Excel_Click(object sender, EventArgs e)
        {
            AddBtn.Enabled = true;
            Mid_identity = Mid_identity -1;
            
            try

            {
                Dt_N_P = connection.SQLDS.Tables["cur_pic"].DefaultView.ToTable(false, "cur_pic", "id_identity_cur").Select("id_identity_cur = " + Mid_identity).CopyToDataTable();
                Display_Pic(Dt_N_P);
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد صورة اخرى للعملة" : "No other Picture for this currency", MyGeneral_Lib.LblCap);
                Mid_identity++;
            }
        }
         //-------------------------------------------------------------------
        private void Cur_Pic_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_cbo = false;
            chang_cbo_cur = false;

            string[] Used_Tbl = { "currency_division", "cur_pic", "Cur_Name_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

    }
}