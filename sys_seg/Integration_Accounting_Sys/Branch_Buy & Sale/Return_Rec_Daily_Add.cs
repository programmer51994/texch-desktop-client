﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Return_Rec_Daily_Add : Form
    {
        #region Definition
        string SqlTxt = "";
         int Oper_id = 0;
         int Vo_No = 0;
         string Nrec_Date ="";
         DataTable Return_Daily;
         string _Date = "";
         DateTime C_Date;
         string ACur_Cust = "";
         string ECur_Cust = "";
         BindingSource _Bs_return_daily = new BindingSource();
         string @format = "dd/MM/yyyy";
         string @format_NULL = "";
        #endregion
         public Return_Rec_Daily_Add()
        {
            InitializeComponent();
          
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            GrdVou_Details.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                GrdVou_Details.Columns["Column3"].DataPropertyName = "Cur_Ename";
            }

        }
         //------------------------------------------
        private void Return_Rec_Daily_Add_Load(object sender, EventArgs e)
        {
            Txt_NrecDate.Format = DateTimePickerFormat.Custom;
            Txt_NrecDate.CustomFormat = @format;

            SqlTxt = "Select OPER_ID,AOPER_NAME,EOPER_NAME from OPERATIONS  "
                    + " where OPER_ID in (18,19)"
                    +" Union "
                    + " Select 18 As oper_id , 'قيد يوميه بيع العملات الاجنبية لاكثر من ثانوي ' As Aoper_name ,'daily record sell foreign currencies' as Eoper_name"
                    + " union "
                    + " Select 19 As oper_id , 'قيد يوميه شراء العملات الاجنبية لاكثر من ثانوي ' As Aoper_name ,'daily record Buy foreign currencies' as Eoper_name ";
            CboOper_Id.DataSource = connection.SqlExec(SqlTxt, "Oper_Tbl");
            CboOper_Id.DisplayMember = connection.Lang_id == 1 ? "AOPER_NAME" : "EOPER_NAME";
            CboOper_Id.ValueMember = "OPER_ID";
        }
        //------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            #region Validation
            if(Txt_Vo_No.Text == "")
            {
                MessageBox.Show(connection.Lang_id ==1 ?"ادخل رقــم القــيد":"Enter Record No. Please", MyGeneral_Lib.LblCap);
                Txt_Vo_No.Focus();
                    return;
            }

            DateTime date = Txt_NrecDate.Value.Date;
            Nrec_Date = (date.Day + date.Month * 100 + date.Year * 10000).ToString();

              
             if (Nrec_Date == "-1" || Nrec_Date == "0")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل التأريخ " : "Enter The Date Please ", MyGeneral_Lib.LblCap);
                Txt_NrecDate.Focus();
                
                return;
            }
            #endregion 
            Oper_id = Convert.ToInt32(CboOper_Id.SelectedValue);
            Vo_No = Convert.ToInt32(Txt_Vo_No.Text);
             object[] Sparams = {Nrec_Date,connection.T_ID, connection.user_id, 0, 
                                 0,0, Vo_No ,Vo_No,0, 
                                  Oper_id, 0, 0, 1 };
             connection.SqlExec("Main_Vouchers", "Return_Daily_Tbl", Sparams);
             if (connection.SQLDS.Tables["Return_Daily_Tbl"].Rows.Count > 0)
             {
                 _Bs_return_daily.DataSource = connection.SQLDS.Tables["Return_Daily_Tbl"].Select("For_cur_id <> Loc_cur_id").CopyToDataTable();
                //_____Binding Source For **connection.Records(connection.SQLBSGrd)**
                 GrdVou_Details.DataSource = _Bs_return_daily;
                }
             else
             {
                 GrdVou_Details.DataSource = new BindingSource();
                 MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود تحقق الشروط " : "There are No Records ", MyGeneral_Lib.LblCap);
              }
        }
        //------------------------------------------
        private void GrdVou_Details_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_Bs_return_daily);
        }
        //------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------------------------
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (GrdVou_Details.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للإضافة" : "There Are No Records To Add", MyGeneral_Lib.LblCap);
                return;
            }
            connection.SQLCMD.Parameters.AddWithValue("@Vo_No", Vo_No);
            connection.SQLCMD.Parameters.AddWithValue("@T_Id", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@Oper_Id", Oper_id);
            connection.SQLCMD.Parameters.AddWithValue("@Nrec_Date", Convert.ToInt32(Nrec_Date));
            connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SQLCMD.Parameters.Add("@Param_Result_CDate", SqlDbType.DateTime, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result_CDate"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Return_Daily_Buy_Sale", connection.SQLCMD);
          
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" &&
                    !(int.TryParse(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), out Vo_No)))
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
             connection.SQLCMD.Parameters.Clear();
             print_Rpt_Pdf();
             _Bs_return_daily.DataSource = new BindingSource();
            // Txt_NrecDate.Text = "00000000";
             Txt_Vo_No.ResetText();
        }
        //------------------------------------------
        private void Return_Rec_Daily_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
          
            string[] Used_Tbl = { "Oper_Tbl", "Return_Daily_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //--------------------
        private void print_Rpt_Pdf()
        {
            //-------------------------report
            string Cur_ToWord = "";
            string Cur_ToEWord = "";
            int New_Oper_id = 0;
            if (Oper_id == 18)
                New_Oper_id = 20;//رد بيــــــع 
            else
                New_Oper_id = 21;//رد شــــراء
            _Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            String Cur_Code = connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Code"].ToString();
            string SqlTxt = "Exec Main_Report " + Vo_No + "," + TxtIn_Rec_Date.Text + "," + New_Oper_id + "," + connection.T_ID + "," + connection.Loc_Cur_Id + "," + connection.user_id;
            connection.SqlExec(SqlTxt, "Return_Daily_Bill");
            C_Date = Convert.ToDateTime(connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0]["C_Date"]);
            double Total_Amount = Convert.ToDouble(connection.SQLDS.Tables["Return_Daily_Bill"].Compute("Sum(Loc_Amount)", ""));
            ToWord toWord = new ToWord(Total_Amount,
                new MyCurrencyInfo(MyCurrencyInfo.Currencies.IQD));
            Cur_ToWord = "-(" + toWord.ConvertToArabic() + " " + Txt_Loc_Cur.Text + " لاغير)- ";
            Cur_ToEWord = "-(" + toWord.ConvertToEnglish() + " " + connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_EName"].ToString() + " not else.)-";
            connection.SQLDS.Tables["Return_Daily_Bill"].Columns.Add("Cur_ToWord");
            connection.SQLDS.Tables["Return_Daily_Bill"].Columns.Add("Cur_ToEWord");
            connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0].SetField("Cur_ToWord", Cur_ToWord);
            connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0].SetField("Cur_ToEWord", Cur_ToEWord);
            ACur_Cust = Cur_Code + "/" + (Oper_id == 18 ? "الحساب الدائن" : "الحساب المدين") + ":" + connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0]["Acust_Name"].ToString();
            ECur_Cust = (Oper_id == 18 ?"Credit Account : ":"Debit Account : " )+ connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0]["Ecust_Name"].ToString() + "/" + Cur_Code;

            string Catg_Ename = connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0]["Catg_Ename"].ToString();
            string Catg_AEname = connection.SQLDS.Tables["Return_Daily_Bill"].Rows[0]["Catg_Aname"].ToString();
            Return_Daily_Buy_Sale_BillCurrency_Rpt ObjRpt = new Return_Daily_Buy_Sale_BillCurrency_Rpt();
            Return_Daily_Buy_Sale_BillCurrency_Rpt_Eng ObjRptEng = new Return_Daily_Buy_Sale_BillCurrency_Rpt_Eng();
            DataTable Dt_Param = CustomControls.CustomParam_Dt();
            Dt_Param.Rows.Add("Catg_Name", Catg_AEname);
            Dt_Param.Rows.Add("Catg_EName", Catg_Ename);
            Dt_Param.Rows.Add("Vo_No", Vo_No);
            Dt_Param.Rows.Add("Oper_Id", New_Oper_id);
            Dt_Param.Rows.Add("Year", TxtIn_Rec_Date.Text.Substring(0, 4));
            Dt_Param.Rows.Add("C_Date", C_Date);
            Dt_Param.Rows.Add("_Date", _Date);
            Dt_Param.Rows.Add("Frm_Id", "Add");
            Dt_Param.Rows.Add("ACur_Cust", ACur_Cust);
            Dt_Param.Rows.Add("ECur_Cust", ECur_Cust);
            DataRow Dr;
            int y = connection.SQLDS.Tables["Return_Daily_Bill"].Rows.Count;
            for (int i = 0; i < (10 - y); i++)
            {
                Dr = connection.SQLDS.Tables["Return_Daily_Bill"].NewRow();
                connection.SQLDS.Tables["Return_Daily_Bill"].Rows.Add(Dr);
            }

            Branch_RptLang_MsgBox RptLangFrm = new Branch_RptLang_MsgBox(ObjRpt, ObjRptEng, true, true, Dt_Param, New_Oper_id);

            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
            connection.SQLDS.Tables.Remove("Return_Daily_Bill");
        }
    }
}
