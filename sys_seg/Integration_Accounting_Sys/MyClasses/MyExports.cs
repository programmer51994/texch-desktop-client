﻿using System;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;
using System.Data.OleDb;
//**
namespace Integration_Accounting_Sys
{
    public static class MyExports
    {
        public static DataSet output = new DataSet();
        public static int i_row_coun = 0;

        public static void Err2_Excel(DataTable Voucher_Cur)
        {
            #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
            bool IsOpen = FileInUse("C://Entry_Err.Xls");
            if (IsOpen)
            {
                MessageBox.Show("الملف مفتوح يرجى غلق الملف اولا لغرض كتابة الخطأ", "رسالة تحذير");
                return;
            }
            #endregion
            //......................................
            //Directory.CreateDirectory(@"C:\Errors");
            //StreamWriter Wr = new StreamWriter(@"C:\Errors\Entry_Err.Xls", false);
            StreamWriter Wr = new StreamWriter("C://Entry_Err.Xls", true, Encoding.Unicode);
            for (int i = 0; i < Voucher_Cur.Columns.Count - 1; i++)
            {
                Wr.Write("\t");
            }
            Wr.Write("Err_Date : " + DateTime.Now);
            Wr.Write("\n");
            #region Write Columns Name to excel File
            for (int i = 0; i < Voucher_Cur.Columns.Count; i++)
            {
                Wr.Write(Voucher_Cur.Columns[i].ColumnName + "\t");
            }
            Wr.WriteLine();
            #endregion

            #region Write rows to excel file
            for (int i = 0; i < (Voucher_Cur.Rows.Count); i++)
            {
                for (int j = 0; j < Voucher_Cur.Columns.Count; j++)
                {
                    if (Voucher_Cur.Rows[i][j] != null)
                        Wr.Write(Voucher_Cur.Rows[i][j] + "\t");
                    else
                    {
                        Wr.Write("\t");
                    }
                }
                Wr.WriteLine();
            }
            #endregion

            Wr.Write("\n"); Wr.Write("\n");
            Wr.Close();
        }
        //---------------------------------------------------------
        public static void DT2_Excel(DataTable Voucher_Cur)
        {
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string FLName = SVFDlg.FileName;

                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(FLName);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion

                //......................................
                StreamWriter Wr = new StreamWriter(FLName, true, Encoding.Unicode);
                for (int i = 0; i < Voucher_Cur.Columns.Count - 1; i++)
                {
                    Wr.Write("\t");
                }
                Wr.Write("\n");
                #region Write Columns Name to excel File
                for (int i = 0; i < Voucher_Cur.Columns.Count; i++)
                {
                    Wr.Write(Voucher_Cur.Columns[i].ColumnName + "\t");
                }
                Wr.WriteLine();
                #endregion

                #region Write rows to excel file
                for (int i = 0; i < (Voucher_Cur.Rows.Count); i++)
                {
                    for (int j = 0; j < Voucher_Cur.Columns.Count; j++)
                    {
                        if (Voucher_Cur.Rows[i][j] != null)
                            Wr.Write(Voucher_Cur.Rows[i][j] + "\t");
                        else
                        {
                            Wr.Write("\t");
                        }
                    }
                    Wr.WriteLine();
                }
                #endregion

                Wr.Write("\n"); Wr.Write("\n");
                Wr.Close();
                MessageBox.Show("Data Exported Successfully", "Success");
            }
        }
        //---------------------------------------------------------
        public static void To_Excel(DataGridView GRD)
        {
            if (GRD.RowCount <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير", MyGeneral_Lib.LblCap);
                return;
            }
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string FLName = SVFDlg.FileName;

                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(FLName);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion

                StreamWriter Wr = new StreamWriter(FLName, true, Encoding.Unicode);

                for (int i = 0; i < GRD.ColumnCount - 1; i++)
                {
                    Wr.Write("\t");
                }
                Wr.Write("EXP_Date : " + DateTime.Now);
                Wr.Write("\n");
                #region Write Columns Name to Excel File
                for (int i = 0; i < GRD.ColumnCount; i++)
                {
                    if (GRD.Columns[i].Visible)
                    {
                        Wr.Write(GRD.Columns[i].HeaderText + "\t");
                    }
                }
                Wr.WriteLine();
                #endregion
                #region Write Rows to Excel File
                for (int i = 0; i < (GRD.RowCount); i++)
                {
                    for (int j = 0; j < GRD.ColumnCount; j++)
                    {
                        if (GRD.Columns[j].Visible)
                        {
                            if (GRD.Rows[i].Cells[j].Value != null)
                                Wr.Write(GRD.Rows[i].Cells[j].Value + "\t");
                            else
                            {
                                Wr.Write("\t");
                            }
                        }
                    }
                    Wr.WriteLine();
                }
                #endregion
                Wr.Write("\n"); Wr.Write("\n");
                Wr.Close();
                MessageBox.Show("Data Exported Successfully", "Success");
            }
        }
        //---------------------------------------------------------
        public static void To_ExcelByHtml(DataGridView pDataGridView, bool Visible_Flage = false)
        {
            if (pDataGridView.RowCount <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير", MyGeneral_Lib.LblCap);
                return;
            }
            #region SaveFileDialog
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";
            #endregion

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string pFilePath = SVFDlg.FileName;
                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(pFilePath);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion
                //#E4E2E2
                String file2exp = "<HTML><BR/><TABLE BORDER = 1 ><TR><TH style= background-color:White></TH>";

                // Headers
                foreach (DataGridViewColumn col in pDataGridView.Columns)
                {
                    if (col.Visible || Visible_Flage)
                    {
                        file2exp += "<TH style= background-color:White>" + col.HeaderText + "</TH>";
                    }
                }
                file2exp += "</TR>";

                int cnt = 1; // row counter

                foreach (DataGridViewRow row in pDataGridView.Rows)
                {
                    if (!row.Visible)
                    {
                        if (!Visible_Flage)
                        {
                            continue;
                        }
                    }
                    file2exp += "<TR><TD>" + cnt++ + "</TD>";

                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (!cell.Visible)
                        {
                            if (!Visible_Flage)
                            {
                                continue;
                            }
                        }
                        file2exp += "<TD style= background-color:White>" + cell.Value.ToString() + "</TD>";
                    }
                    file2exp += "</TR>";
                }

                file2exp += "</TABLE></HTML>";

                File.WriteAllText(pFilePath, file2exp, Encoding.Unicode);
                MessageBox.Show("Data Exported Successfully", "Success");
            }
        }
        //---------------------------------------------------------
        public static DataSet ImportExcels(string Tbl_Name, bool hasHeaders)
        {
            string FileName = "";
            output.Tables.Clear();
            OpenFileDialog OFD = new OpenFileDialog();
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                FileName = OFD.FileName;
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn = "";


                try
                {
                    if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx") //Connect to Excel 2007 (and later) files with the Xlsx file extension
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
                    else //Connect to Excel 97-2003
                        strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();

                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    foreach (DataRow schemaRow in schemaTable.Rows)
                    {
                        string sheet = schemaRow["TABLE_NAME"].ToString();

                        if (!sheet.EndsWith("_"))
                        {
                            try
                            {

                                OleDbCommand cmd = new OleDbCommand("SELECT *, 0 as تسلسل FROM [" + sheet + "]", conn);
                                cmd.CommandType = CommandType.Text;

                                DataTable outputTable = new DataTable(Tbl_Name + i_row_coun.ToString());
                                output.Tables.Add(outputTable);
                                new OleDbDataAdapter(cmd).Fill(outputTable);

                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                        }
                        i_row_coun++;
                    }
                    conn.Close();
                }
            }
            return output;


        }
        //---------------------------------------------------------
        /// <summary>
        /// Check Weather Entry_Err.Xls Is OPen
        /// </summary>
        /// <returns>True If OPen and Vice Versa</returns>
        static bool FileInUse(string FilePath)
        {
            try
            {
                using (FileStream Fs = new FileStream(FilePath, FileMode.OpenOrCreate))
                    return false;
            }
            catch (IOException)
            {
                return true;
            }
        }
        //------------------------------*******************************---------------------------
        public static void To_ExcelByHtml(DataTable ExpDt, DataGridView pDataGridView, bool Visible_Flag = false)
        {
            if (ExpDt.Rows.Count <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير", MyGeneral_Lib.LblCap);
                return;
            }
            #region SaveFileDialog
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";
            #endregion

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string pFilePath = SVFDlg.FileName;
                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(pFilePath);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion
                //#E4E2E2
                String file2exp = "<HTML><BR/><TABLE BORDER = 1 ><TR><TH style= background-color:White></TH>";

                // Headers
                foreach (DataGridViewColumn col in pDataGridView.Columns)
                {
                    if (col.Visible || Visible_Flag)
                    {
                        file2exp += "<TH style= background-color:White>" + col.HeaderText + "</TH>";
                    }
                }
                file2exp += "</TR>";

                int cnt = 1; // row counter

                foreach (DataRow row in ExpDt.Rows)
                {
                    if (!Visible_Flag)
                    {
                        continue;
                    }
                    file2exp += "<TR><TD>" + cnt++ + "</TD>";

                    foreach (object ItemArr in row.ItemArray)
                    {
                        if (!Visible_Flag)
                        {
                            continue;
                        }
                        file2exp += "<TD style= background-color:White>" + ItemArr.ToString() + "</TD>";
                    }
                    file2exp += "</TR>";
                }

                file2exp += "</TABLE></HTML>";

                File.WriteAllText(pFilePath, file2exp, Encoding.Unicode);
                MessageBox.Show("Data Exported Successfully", "Success");
            }
        }
        //---------------------------------------------
        public static void To_ExcelByHtml(DataTable[] ExpDt, DataGridView[] pDataGridView, bool Visible_Flag = false, bool isTerminal = false, String TitleName = "")
        {
            if (ExpDt.Length <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير", MyGeneral_Lib.LblCap);
                return;
            }
            if (pDataGridView.Length <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير1", MyGeneral_Lib.LblCap);
                return;
            }
            DataGridView Grd = pDataGridView[0] as DataGridView;
            //String TitleName = Grd.FindForm().Text.ToString();
            //--------------
            #region SaveFileDialog
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";
            #endregion

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string pFilePath = SVFDlg.FileName;
                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(pFilePath);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion
                //#E4E2E2
                String file2exp = String.Empty;
                int colspan = 0;
                foreach (DataGridView DGrd in pDataGridView)
                {
                    if (DGrd.ColumnCount > colspan)
                    {
                        colspan = DGrd.ColumnCount;
                    }
                  //  colspan = DGrd.ColumnCount;
                }

                file2exp += "<Table border=1><tr><TD align='center' bgcolor='#D1DAA7' style='font-size:18px' colspan = " + Convert.ToString(colspan + 1) + ">" + TitleName + "</TD></tr>";
                if (isTerminal)
                {
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'Right'" : "'left'") + " bgcolor='#D1DAA7' style='font-size:10px' colspan = " + Convert.ToString(colspan) + " > " + connection.T_Name + "</TD>";
                    file2exp += "<TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'Right'" : "'Left'") + " bgcolor='#D1DAA7' style='font-size:12px' > " + Convert.ToString(connection.Lang_id == 1 ? " المستخدم: " : " User Name: ") + connection.User_Name + " </TR>";
                    file2exp += "<tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'left'" : "'right'") + "bgcolor='#D1DAA7' style='font-size:12px' colspan = " + Convert.ToString(colspan + 1) + " > " + Convert.ToString(connection.Lang_id == 1 ? " وقت وتاريخ التنظيم: " : "Printed On : ") + DateTime.Now + "</TD>";
                }
                else
                {
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'Right'" : "'Left'") + " bgcolor='#D1DAA7' style='font-size:12px' > " + Convert.ToString(connection.Lang_id == 1 ? " المستخدم: " : " User Name: ") + connection.User_Name + " </TR>";
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'left'" : "'right'") + "bgcolor='#D1DAA7' style='font-size:12px' colspan = " + Convert.ToString(colspan + 1) + " > " + Convert.ToString(connection.Lang_id == 1 ? " وقت وتاريخ التنظيم: " : "Printed On : ") + DateTime.Now + "</TD>";
                }

                // Headers
                file2exp += "<TR><TD bgcolor='#FFEFD5'>" + Convert.ToString(connection.Lang_id == 1 ? "تسلسل" : "Seq") + "</TD>";

                for (int j = 0; j < pDataGridView.Length; j++)
                {
                    DataGridView Grd_Arr = pDataGridView[j] as DataGridView;
                    //-------------

                    //string[] ColName = new string[Grd_Arr.ColumnCount];
                    //int Coli = 0;
                    foreach (DataGridViewColumn col in Grd_Arr.Columns)
                    {
                        if (col.Visible || Visible_Flag)
                        {
                            file2exp += "<TD bgcolor='#FFEFD5'>" + col.HeaderText + "</TH>";
                            //ColName[Coli] = col.DataPropertyName;
                            //Coli++;
                        }
                    }
                    file2exp += "</TR>";

                    int cnt = 1; // row counter
                    DataTable Dt_Arr = ExpDt[j] as DataTable;
                    //DataTable Dt2Exp = Dt_Arr.DefaultView.ToTable(false, ColName);
                    foreach (DataRow row in Dt_Arr.Rows)
                    {
                        file2exp += "<TR><TD>" + cnt++ + "</TD>";

                        foreach (object ItemArr in row.ItemArray)
                        {
                            if (!Visible_Flag)
                            {
                                continue;
                            }
                            file2exp += "<TD style= background-color:White>" + ItemArr.ToString() + "</TD>";
                        }

                    }
                        if (j < (pDataGridView.Length - 1))
                        {
                            file2exp += "</TR>";
                            file2exp += "<TR></TR>";
                            file2exp += "<TR><TD bgcolor='#FFEFD5'>" + Convert.ToString(connection.Lang_id == 1 ? "تسلسل" : "Seq") + "</TD>";
                        }
                    //}
                }
                //---------------------------------------------------
                file2exp += "</TABLE></HTML>";

                File.WriteAllText(pFilePath, file2exp, Encoding.Unicode);
                MessageBox.Show("Data Exported Successfully", "Success");
            }
        }
        //---------------------------------------------------------
        public static void To_ExcelByHtml(DataGridView[] pDataGridView, bool Visible_Flage = false, bool isTerminal = false, String TitleName = "")
        {
            if (pDataGridView.Length <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير2", MyGeneral_Lib.LblCap);
                return;
            }
            DataGridView Grd = pDataGridView[0] as DataGridView;
            //String TitleName = Grd.FindForm().Text.ToString();

            #region SaveFileDialog
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";
            #endregion

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string pFilePath = SVFDlg.FileName;
                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(pFilePath);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion
                //#E4E2E2

                String file2exp = String.Empty;
                int colspan = 0;
                foreach (DataGridView DGrd in pDataGridView)
                {
                    if (DGrd.ColumnCount > colspan)
                    {
                        colspan = DGrd.ColumnCount;
                    }
                    colspan = DGrd.ColumnCount;
                }

                file2exp += "<Table border=1><tr><TD align='center' bgcolor='#D1DAA7' style='font-size:18px' colspan = " + Convert.ToString(colspan + 1) + ">" + TitleName + "</TD></tr>";
                if (isTerminal)
                {
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'Right'" : "'left'") + " bgcolor='#D1DAA7' style='font-size:10px' colspan = " + Convert.ToString(colspan) + " > " + connection.T_Name + "</TD>";
                    file2exp += "<TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'Right'" : "'Left'") + " bgcolor='#D1DAA7' style='font-size:12px' > " + Convert.ToString(connection.Lang_id == 1 ? " المستخدم: " : " User Name: ") + connection.User_Name + " </TR>";
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'left'" : "'right'") + "bgcolor='#D1DAA7' style='font-size:12px' colspan = " + Convert.ToString(colspan + 1) + " > " + Convert.ToString(connection.Lang_id == 1 ? " وقت وتاريخ التنظيم: " : "Printed On : ") + DateTime.Now + "</TD>";
                }
                else
                {
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'Right'" : "'Left'") + " bgcolor='#D1DAA7' style='font-size:12px' > " + Convert.ToString(connection.Lang_id == 1 ? " المستخدم: " : " User Name: ") + connection.User_Name + " </TR>";
                    file2exp += "<tr></tr><tr><TD align=" + Convert.ToString(connection.Lang_id == 1 ? "'left'" : "'right'") + "bgcolor='#D1DAA7' style='font-size:12px' colspan = " + Convert.ToString(colspan + 1) + " > " + Convert.ToString(connection.Lang_id == 1 ? " وقت وتاريخ التنظيم: " : "Printed On : ") + DateTime.Now + "</TD>";
                }

                // Headers
                file2exp += "<TR><TD bgcolor='#FFEFD5'>" + Convert.ToString(connection.Lang_id == 1 ? "تسلسل" : "Seq") + "</TD>";

                for (int j = 0; j < pDataGridView.Length; j++)
                {
                    DataGridView Grd_Arr = pDataGridView[j] as DataGridView;
                    //-------------
                    foreach (DataGridViewColumn col in Grd_Arr.Columns)
                    {
                        if (col.Visible || Visible_Flage)
                        {
                            file2exp += "<TD bgcolor='#FFEFD5'>" + col.HeaderText + "</TH>";
                        }
                    }
                    file2exp += "</TR>";

                    int cnt = 1; // row counter

                    foreach (DataGridViewRow row in Grd_Arr.Rows)
                    {
                        if (!row.Visible)
                        {
                            if (!Visible_Flage)
                            {
                                continue;
                            }
                        }
                        file2exp += "<TR><TD>" + cnt++ + "</TD>";

                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            if (!cell.Visible)
                            {
                                if (!Visible_Flage)
                                {
                                    continue;
                                }
                            }
                            file2exp += "<TD style= background-color:White>" + cell.Value.ToString() + "</TD>";
                        }
                        file2exp += "</TR>";
                    }
                    if (j < (pDataGridView.Length - 1))
                    {
                        file2exp += "</TR>";
                        file2exp += "<TR></TR>";
                        file2exp += "<TR><TD bgcolor='#FFEFD5'>" + Convert.ToString(connection.Lang_id == 1 ? "تسلسل" : "Seq") + "</TD>";
                    }
                }

                file2exp += "</TABLE></HTML>";

                File.WriteAllText(pFilePath, file2exp, Encoding.Unicode);
                MessageBox.Show("Data Exported Successfully", "Success");
            }


        }


         public static void To_TxtFile (DataTable DT_ToTxt , DataTable Dt_Head)
         {
         
              if (DT_ToTxt.Rows.Count <= 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "لا يـــوجد قيـــود للتـــصدير" : "There is no data to be exported", MyGeneral_Lib.LblCap);
                        return;
                    }
           
     

                    //------- check textfile if open :

                    SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Text File | *.txt|All files (*.*)|*.*";
            //SVFDlg.Filter = "Text File | *.txt";

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                string FLName = SVFDlg.FileName;

                #region  Call FileInUse() Function to Check Weather Entry_Err.Xls Is OPen
                bool IsOpen = FileInUse(FLName);
                if (IsOpen)
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }
                #endregion

                //--------Export to textfile :
                int i = 0;
                StreamWriter sw = null;

                sw = new StreamWriter(FLName, false);

                for (i = 0; i < Dt_Head.Columns.Count - 1; i++)
                {

                    sw.Write(Dt_Head.Columns[i].ColumnName + '\t');

                }
                sw.Write(Dt_Head.Columns[i].ColumnName);
                sw.WriteLine();

                foreach (DataRow row in DT_ToTxt.Rows)
                {
                    object[] array = row.ItemArray;

                    for (i = 0; i < array.Length - 1; i++)
                    {
                        sw.Write(array[i].ToString() + '\t');
                    }
                    sw.Write(array[i].ToString());
                    sw.WriteLine();

                }

                sw.Close();


                MessageBox.Show(connection.Lang_id == 1 ? "تم تصدير البيانات بنجاح" : "Data exported sucessfully", MyGeneral_Lib.LblCap);
                return;

                //-----------------------
                //    }
                //}
            }
            }

    }
}