﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integration_Accounting_Sys
{
    class MyCurrencyInfo
    {
        public enum Currencies { IQD =0,USD };

        #region Constructors

        public MyCurrencyInfo(Currencies currency)
        {
            switch (currency)
            {
                case Currencies.IQD:
                    EnglishCurrencyName = " IRQ Dinar";
                    EnglishPluralCurrencyName = " IRQ Dinars";
                    Arabic1CurrencyName = "دينار عراقي";
                    Arabic2CurrencyName = "ديناران عراقيان";
                    Arabic310CurrencyName = "دنانير عراقية";
                    Arabic1199CurrencyName = "دينارا عراقيا";
                    break;
                case Currencies.USD:
                    EnglishCurrencyName = " USD Dollar";
                    EnglishPluralCurrencyName = " USD Dollars";
                    Arabic1CurrencyName = "دولار امريكي";
                    Arabic2CurrencyName = "دولاران امريكيان";
                    Arabic310CurrencyName = "دولارات امريكية";
                    Arabic1199CurrencyName = "دولارا امريكيا";
                    break;
            }
        }

        #endregion
        //------------------------------------------------------------
        #region Properties

        /// <summary>
        /// English Currency Name for single use
        /// IRQ Dinar
        /// </summary>
        public string EnglishCurrencyName { get; set; }
        //------------------------------------------------------------
        /// <summary>
        /// English Plural Currency Name for Numbers over 1
        /// IRQ Dinars
        /// </summary>
        public string EnglishPluralCurrencyName { get; set; }
        //------------------------------------------------------------
        /// <summary>
        /// Arabic Currency Name for 1 unit only
        /// دينار عراقي
        /// </summary>
        public string Arabic1CurrencyName { get; set; }
        //------------------------------------------------------------
        /// <summary>
        /// Arabic Currency Name for 2 units only
        /// ديناران عراقيان
        /// </summary>
        public string Arabic2CurrencyName { get; set; }
        //------------------------------------------------------------
        /// <summary>
        /// Arabic Currency Name for 3 to 10 units
        /// خمسة دنانير عراقية
        /// </summary>
        public string Arabic310CurrencyName { get; set; }
        //------------------------------------------------------------
        /// <summary>
        /// Arabic Currency Name for 11 to 99 units
        /// خمسة و سبعون دينارا عراقياً
        /// </summary>
        public string Arabic1199CurrencyName { get; set; }
        #endregion
    }
}
