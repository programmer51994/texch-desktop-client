﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Reflection;

namespace Integration_Accounting_Sys
{
    public static class CustomControls
    {
        /// <summary>
        /// Used To Create DataTable With Some VAlues And Restricted By Data Type
        /// </summary>
        /// <param name="DtName"> DataTable Name</param>
        /// <param name="Columns"> Array Of Columns To Struct To DataTable</param>
        /// <param name="Rows">Array Of String Rows Value To Add To DataTable</param>
        /// <param name="Values">Array Of int Rows Value To Add To DataTable </param>
        /// <param name="DType">Array Of Columns DataType To Struct To DataTable Columns </param>
        /// <returns> Return DataTable With Some VAlues And Restricted By Data Type</returns>
        public static DataTable Custom_DataTable(string DtName, string[] Columns, string[] Rows, int[] Values, string[] DType)
        {
            DataTable SQLDT = new DataTable(DtName);

            for (int i = 0; i < Columns.Length; i++)
            {
                DataColumn Col = new DataColumn();
                Col.DataType = System.Type.GetType(DType[i]);
                Col.ColumnName = Columns[i];
                SQLDT.Columns.Add(Col);
            }
            for (int i = 0; i < Rows.Length; i++)
            {
                SQLDT.Rows.Add(new object[] { Values[i], Rows[i] });
            }
            return SQLDT;
        }
        //========================================================================
        /// <summary>
        /// Used To Create DataTable Without VAlues  But With Restricted Data Type
        /// </summary>
        /// <param name="DtName">DataTable Name</param>
        /// <param name="Columns">Array Of Columns To Struct To DataTable</param>
        /// <param name="DType">Array Of Columns DataType To Struct To DataTable Columns</param>
        /// <returns>Return DataTable Without VAlues But With Restricted Data Type</returns>
        public static DataTable Custom_DataTable(string DtName, string[] Columns, string[] DType)
        {
            DataTable SQLDT = new DataTable(DtName);

            for (int i = 0; i < Columns.Length; i++)
            {
                DataColumn Col = new DataColumn();
                Col.DataType = System.Type.GetType(DType[i]);
                Col.ColumnName = Columns[i];
                SQLDT.Columns.Add(Col);

            }
            return SQLDT;
        }
        //========================================================================
        /// <summary>
        /// Used To Create DataTable With Some VAlues Without Restricted By Data Type
        /// </summary>
        /// <param name="DtName"> DataTable Name</param>
        /// <param name="Columns"> Array Of Columns To Struct To DataTable</param>
        /// <param name="Rows">Array Of String Rows Value To Add To DataTable</param>
        /// <param name="Values">Array Of int Rows Value To Add To DataTable </param>
        /// <returns> Return DataTable With Some VAlues Without Restricted By Data Type</returns>
        public static DataTable Custom_DataTable(string DtName, string[] Columns, string[] Rows, int[] Values)
        {
            DataTable SQLDT;
            SQLDT = new DataTable(DtName);

            for (int i = 0; i < Columns.Length; i++)
            {
                DataColumn Col = new DataColumn();
                // Col.DataType = System.Type.GetType(DType[i]);
                Col.ColumnName = Columns[i];
                SQLDT.Columns.Add(Col);

            }

            for (int i = 0; i < Rows.Length; i++)
            {
                SQLDT.Rows.Add(new object[] { Values[i], Rows[i] });
            }
            return SQLDT;
        }
        //========================================================================
        /// <summary>
        /// Used To Create DataTable Without Some VAlues  And Without Restricted By Data Type
        /// </summary>
        /// <param name="DtName">DataTable Name</param>
        /// <param name="Columns">Array Of Columns To Struct To DataTable</param>
        /// <returns>Return DataTable Without Some VAlues  And Without Restricted By Data Type</returns>
        public static DataTable Custom_DataTable(string DtName, string[] Columns)
        {
            DataTable SQLDT = new DataTable(DtName);

            for (int i = 0; i < Columns.Length; i++)
            {
                DataColumn Col = new DataColumn();
                Col.ColumnName = Columns[i];
                SQLDT.Columns.Add(Col);
            }
            return SQLDT;
        }
        //-----------------

        public static DataTable CustomParam_Dt()
        {
            DataTable Param_DT = new DataTable();
            Param_DT.TableName = "Param_DT";
            //------------
            DataColumn DC_Name = new DataColumn();
            DC_Name.ColumnName = "Param_Name";
            DC_Name.DataType = System.Type.GetType("System.String");
            DataColumn DC_Value = new DataColumn();
            DC_Value.ColumnName = "Param_Value";
            DC_Value.DataType = System.Type.GetType("System.Object");
            //------------
            Param_DT.Columns.Add(DC_Name);
            Param_DT.Columns.Add(DC_Value);
            return Param_DT;
        }
        //-----------------
        public static DataTable IEnumerableToDataTable(IEnumerable ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (PropertyInfo pi in pis)
                    {
                        dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
