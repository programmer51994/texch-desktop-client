﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public static class connection
    {
        #region  Declaration OF VARIABLES And PROPERTIES
        public static string MyConnStr = GetConn();
        public static int user_id = 7;
        public static int T_ID = 1;
        public static int Lang_id = 1;
        public static int Comp_Id = 1;
        public static int Frm_Id = 500;
        public static int Frm_Id_Main = 1;
        public static int Acc_No = 0;
        public static string Col_Name = "";
        public static string User_Name = "test1";
        public static string T_Name = "";
        public static int Rpt_Id = 0;
        public static int Rpt_Lang_Id = 0;
        public static int Rpt_Test = 0;
        public static decimal Discount_Amount = 0;
        public static int Box_Cust_Id = 0;
        public static int Loc_Cur_Id = 0;
        public static int Node_id = 0;
        public static int page_no = 0;
        public static Decimal Limit_Exchange_LC = 0;
        public static int login_Id;
        public static int Cust_online_Id;
        public static string sProcessorID;
        public static Int32 Idel_Timeout;
        public static string IP;
        public static string login_name = "";
        public static int city_id_online = 0;
        public static int Term_cust_ID = 0;
        public static byte comm_rem_flag = 0;
         
        #endregion
        //----------------------------------------------------------------------------
        #region PROPERTIES Used For Searching
        /// <summary> Below PRPERTIES Used For Searching At Gen_Search Form </summary>
        public static int For_Cur_ID { get; set; }
        public static decimal Min_Qty { get; set; }
        public static decimal Max_Qty { get; set; }
        public static int Min_Vo { get; set; }
        public static int Max_Vo { get; set; }
        public static int Catg_Id { get; set; }
        #endregion
        //----------------------------------------------------------------------------
        #region  INITIALIZATION OF OBJECTS
        /// <summary> INITIALIZATION OF OBJECTS AND SET CONSTRUCTORS </summary>
        public static DataSet SQLDS = new DataSet();
        public static DataSet RptDS = new DataSet();
        public static SqlConnection SQLCS = new SqlConnection(MyConnStr);
        public static SqlDataAdapter SQLDA = new SqlDataAdapter();
        public static SqlCommand SQLCMD = new SqlCommand();
        public static BindingSource SQLBS = new BindingSource();
        public static BindingSource SQLBSGrd = new BindingSource();
        public static BindingSource SQLBSCbo = new BindingSource();
        public static BindingSource SQLBSMainGrd = new BindingSource();
        public static BindingSource SQLBSSubGrd = new BindingSource();
        public static BindingSource SQLBSSubGrd1 = new BindingSource();
        public static BindingSource SQLBSLst = new BindingSource();
        public static SqlDataReader SQLDR;
        public static DataTable SQLDT;
        #endregion
        //----------------------------------------------------------------------------
        /// <summary>EXECUTE SP OR QUERY AND RETREIVE DATA INTO SET OF TABLE(S)</summary>
        /// <param name="SqlText"> STORED PROCDURE NAME OR QUERY TEXT </param>
        /// <param name="TblName"> TABLE NAME(S) OF RETEEIVING DATA </param>
        /// <returns> RETURN DATASET  </returns>
        public static DataTable SqlExec(string SqlText, string TblName)
        {
            #region ClrTbl
            for (int j = 0; j < SQLDS.Tables.Count; j++)
            {
                if (j == 0)
                {
                    if (SQLDS.Tables.Contains(TblName))
                    {
                        SQLDS.Tables.Remove(TblName);
                    }
                }
                else
                {
                    string Tbl = TblName + j.ToString();
                    if (SQLDS.Tables.Contains(Tbl))
                    {
                        SQLDS.Tables.Remove(Tbl);
                    }
                }
            }
            #endregion

            try
            {
                SQLDA.SelectCommand = new SqlCommand(SqlText, SQLCS);
                SQLDA.Fill(SQLDS, TblName);
                SQLCS.Close();
                SQLDA.Dispose();
                SQLCMD.Dispose();
            }

            catch (Exception ex)
            {
                MyErrorHandler.ExceptionHandler(ex);
            }
            return SQLDS.Tables[TblName];
        }
        //--------------------------------------------------------------
        /// <summary> Execute SQL Delete Command </summary>
        /// <param name="SqlText"> Delete Command Text</param>
        /// <returns> Return Number of Row(s) Got Deleted</returns>
        public static int SqlExec(string SqlText)
        {
            int RowDel = 0;
            try
            {
                SQLCMD = new SqlCommand(SqlText, SQLCS);
                SQLCS.Open();
                RowDel = SQLCMD.ExecuteNonQuery();
                SQLCS.Close();
                SQLDA.Dispose();
                SQLCMD.Dispose();
            }

            catch (Exception ex)
            {
                MyErrorHandler.ExceptionHandler(ex);
            }
            return RowDel;
        }
        //--------------------------------------------------------------------------------
        public static object ImgSqlExec(string ProcedureName)
        {
            Rpt_Test = 0;
            SQLCMD.CommandType = CommandType.StoredProcedure;
            SQLCMD.CommandText = ProcedureName;
            try
            {
                SQLCMD.Connection = SQLCS;
                SQLCS.Open();
                SQLCMD.ExecuteNonQuery();
                SQLCS.Close();
                SQLCMD.Dispose();
            }

            catch (Exception ex)
            {
                if (ex is SqlException)
                {

                    if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                    {
                        Rpt_Test = -1;
                        MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                        "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                        return -1;
                    }
                    else
                        MyErrorHandler.SQLHandler(ex);
                }
                else
                {
                    MyErrorHandler.Handler(ex);
                }
            }
            return 0;
        }
        //-----------------------------------------------------------------------------
        /// <summary>
        /// EXECUTE SP AND RETREIVE RESULT OF PROCESS AND STATE OF USER
        /// </summary>
        /// <returns> RETURN A RESULT OF PROCESS AND STATE OF USER </returns>
        public static int SqlExec(string ProcedureName, params SqlParameter[] values)
        {
            Rpt_Test = 0;
            SQLCMD.CommandType = CommandType.StoredProcedure;
            SQLCMD.CommandText = ProcedureName;
            if (values != null && values.Length > 0)
                SQLCMD.Parameters.AddRange(values);
            try
            {
                SQLCMD.Connection = SQLCS;
                SQLCS.Open();
                SQLCMD.CommandTimeout = 0;
                SQLCMD.ExecuteNonQuery();
                SQLCS.Close();
                SQLCMD.Dispose();
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                    {
                        Rpt_Test = -1;
                        MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                        "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                        return -1;
                    }
                    else
                        MyErrorHandler.SQLHandler(ex);
                }
                else
                {
                    MyErrorHandler.Handler(ex);
                }
            }

            return 0;
        }
        //-----------------------------------------------------------------------------
        /// <summary> Below Function Used To Get Connection String From TextFile   test_shipping </summary>
        /// <returns> Return Connection String</returns>
        public static string GetConn()
        {
            //string connStr = "server = 10.50.101.241 ; database = TTS_DB ; user id = sa ; password = !11#C001$ ; connection timeout = 1000";
            //string connStr = "server = 192.168.3.72 ; database = Is_Building ; user id = sa ; password = CS!tmt#GeNe ; connection timeout = 1000";
            //string connStr = "server = 192.168.3.95 ; database = Is_Anwar ; user id = sa ; password = !07#IS$ ; connection timeout = 1000";
            //string connStr = "server = 192.168.3.95  ; database = Is_Shipping ; user id = sa ; password = !07#IS$ ; connection timeout = 1000";
            // string connStr = "server = 192.168.3.216  ; database = TEC_DB ; user id = TAC_TE ; password = EX$Taif#001! ; connection timeout = 0";
            // string connStr = "server = 192.168.44.253  ; database =  TAC_London ; user id = sa ; password = !10#T001$ ; connection timeout = 0";
            // string connStr = @"server = 192.168.9.251\sql2014 ; database = ATC_Taoseel ; user id = sa ; password = t@wseelt0; connection timeout = 0";
            // string connStr = "server = 192.168.49.241  ; database = TAC_CANADA ; user id = Canada_Taif_Log ; password = !01#CA005$ ; connection timeout = 0";
            //string connStr = "server = 192.168.28.4  ; database = TAC_AUSTRALIA_New ; user id = sa ; password = !01#T001$ ; connection timeout = 0";
            // string connStr = @"server = 192.168.30.5  ; database = TAC_Jordan ; user id = sa ; password = !02#T002$ ; connection timeout = 0";
            //string connStr = @"server = 192.168.38.38  ; database = TAC_Dubai ; user id = sa ; password = !09#T001$ ; connection timeout = 0";
            // string connStr = "server =192.168.27.246  ; database = TAC_INTER ; user id = sa ; password = !09#T001$ ; connection timeout = 0";
            //string connStr = "server = 192.168.49.241  ; database = TEST_TAC_USA ; user id = Canada_Taif_Log ; password = !01#CA005$ ; connection timeout = 0";
            // string connStr = "server = 192.168.16.3; database = TAC_Exchange ; user id = sa ; password = !03#T009$  ; connection timeout = 0";
          //  string connStr = "server = 192.168.3.64  ; database = TEC_DB ; user id = TAC_Zay ; password = EX$Taif#001! ; connection timeout = 0";
            // string connStr = "server = 192.168.3.23  ; database = Tac_MRABA ; user id = sa ; password = MR$Taif#001! ; connection timeout = 0";
            //----------------------------------------------------------
            // string connStr = @"server = 192.168.30.231\SQL2014 ; database = tac_jordan ; user id = sa ; password = !02#T002$ ; connection timeout = 1000";
            // string connStr = "server = 192.168.3.82 ; database = canda_login_test ; user id = sa ; password = !11#C001$  ; connection timeout = 0";  
            // string connStr = "server = 192.167.33.145 ; database = SOUNDOS_TEC_DB  ; user id = sa ; password = !10#T001$ ; connection timeout = 1000";
            // string connStr = @"server = PROG7-PC\SQL2014 ; database = aus_closing ; user id = sa ; password = 123 ; connection timeout = 30";
            // string connStr = @"server = PROG7-PC\SQL2014 ; database = aus_closing ; user id = sa ; password = 123 ; connection timeout = 30";
          //    string connStr ="server =PROG5-NAWAR; database = Online_Iraq ; user id = sa ; password = 123 ; connection timeout = 10000";
            // string connStr = @"server = 175.33.178.254:3389 ; database = TAC_AUSTRALIA ; user id = sa ; password = !01#T001$ ; connection timeout = 10000";
            // string connStr = "server = 192.168.44.253 ; database = TEST_TEST_TAC_LONDON ; user id = sa ; password = !10#T001$ ; connection timeout = 0";
            // string connStr = "server = 192.168.3.95 ; database = Is_Anwar ; user id = sa ; password = !07#IS$ ; connection timeout = 1000";
            // string connStr = "server = 192.168.3.95  ; database = Is_Shipping ; user id = sa ; password = !07#IS$ ; connection timeout = 1000";
            // string connStr = "server = 192.168.3.211  ; database = test_exchange_db ; user id = sa ; password = EX$Taif#001! ; connection timeout = 0";
            //string connStr = "server = 192.168.3.151 ; database = Online_London ; user id = sa ; password = 123456  ; connection timeout = 0";
            string connStr = "server = 192.168.3.151 ; database = Online_basket_2020 ; user id = sa ; password = 123456  ; connection timeout = 0"; 
            //string connStr = "server = 192.168.3.95  ; database = test_shipping ; user id = sa ; password = !07#IS$ ; connection timeout = 1000";
            return connStr;
        }
        //-----------------------------------------------------------------------------
        /// <summary> Retrieve A Custom Parameter To Stored procedure That Need It </summary>
        /// <param name="SqlText"> SP Name </param>
        /// <param name="list"> An Array Of Parameters That Contain Parameter Value </param>
        /// <returns> Return A Result  </returns>
        public static int scalar(string SqlText, ArrayList list)
        {
            if (SQLDS.Tables.Contains("tblparam"))
            {
                SQLDS.Tables.Remove("tblparam");
            }
            int i = 0;
            SqlParameter[] sqlParams = new SqlParameter[list.Count];
            SqlExec("select * from param_Proc_Name where proc_id in"
                + "( select proc_id from Proc_Name where proc_name = '"
                + SqlText.Trim() + "')  order by rec_proc_id asc ", "tblparam");

            //SQLDT = SQLDS.Tables["tblparam"];
            if (list.Count != SQLDS.Tables["tblparam"].Rows.Count)
            {
                MessageBox.Show("No. of parameters not equal to procedure parameters");
                return -1;
            }
            foreach (DataRow row in SQLDS.Tables["tblparam"].Rows)
            {
                if (list[i] == null)
                {
                    list[i] = 0;
                }
                if (row[1].ToString() == "0")
                {
                    if (list[i] is byte[])
                    {
                        sqlParams[i] = new SqlParameter(row[0].ToString(), SqlDbType.Image);
                        sqlParams[i].Value = list[i];
                    }
                    else
                    {
                        sqlParams[i] = new SqlParameter(row[0].ToString(), list[i].ToString());
                    }
                }
                else
                {
                    sqlParams[i] = new SqlParameter(row[0].ToString(), SqlDbType.VarChar, 200);
                    sqlParams[i].Direction = ParameterDirection.Output;
                }
                i++;
            }
            SqlExec(SqlText, sqlParams);
            return 0;
        }
        //-----------------------------------------------------------------------------------------------------------
        public static object SqlExec(string SqlText, string TblName, object[] Sparameters)
        {
            #region ClrTbl
            for (int j = 0; j < SQLDS.Tables.Count; j++)
            {
                if (j == 0)
                {
                    if (SQLDS.Tables.Contains(TblName))
                    {
                        SQLDS.Tables.Remove(TblName);
                    }
                }
                else
                {
                    string Tbl = TblName + j.ToString();
                    if (SQLDS.Tables.Contains(Tbl))
                    {
                        SQLDS.Tables.Remove(Tbl);
                    }
                }
            }

            if (SQLDS.Tables.Contains("tblparam"))
            {
                SQLDS.Tables.Remove("tblparam");
            }
            #endregion

            int i = 0;
            SqlParameter[] sqlParams = new SqlParameter[Sparameters.Length];
            SqlExec("select * from param_Proc_Name where proc_id in"
                  + "( select proc_id from Proc_Name where proc_name = '"
                  + SqlText.Trim() + "')  order by rec_proc_id asc ", "tblparam");

            if (Sparameters.Length != SQLDS.Tables["tblparam"].Rows.Count)
            {
                MessageBox.Show("No. of parameters not equal to procedure parameters");
                return 0;
            }

            foreach (DataRow row in SQLDS.Tables["tblparam"].Rows)
            {
                if (Sparameters[i] == null)
                {
                    Sparameters[i] = 0;
                }
                //sqlParams[i] = new SqlParameter(row[0].ToString(), Sparameters[i].ToString());
                if (row[1].ToString() == "0")
                {
                    sqlParams[i] = new SqlParameter(row[0].ToString(), Sparameters[i].ToString());
                }
                else
                {
                    sqlParams[i] = new SqlParameter(row[0].ToString(), SqlDbType.VarChar, 200);
                    sqlParams[i].Direction = ParameterDirection.Output;
                }
                i++;
            }

            SQLCMD.CommandType = CommandType.StoredProcedure;
            SQLCMD.CommandText = SqlText;
            SQLCMD.CommandTimeout = 0;
            if (sqlParams != null && sqlParams.Length > 0)
                SQLCMD.Parameters.AddRange(sqlParams);
            SQLCMD.Connection = SQLCS;
            try
            {
                SQLCS.Open();
                SQLDA.SelectCommand = SQLCMD;
                //SQLDS.Clear();
                SQLDA.Fill(SQLDS, TblName);
                SQLCS.Close();
                SQLCMD.Dispose();
                SQLDS.Dispose();
                if (SQLCMD.Parameters.Contains("@Param_Result"))
                {
                    Col_Name = SQLCMD.Parameters["@Param_Result"].Value.ToString();
                }
                SQLCMD.Parameters.Clear();
            }
            catch (Exception ex)
            {
                MyErrorHandler.ExceptionHandler(ex);
            }
            return SQLDS.Tables[TblName];


        }
        //-------------------------------------------
        public static void SqlExec(string SQLTxt, ArrayList list, string ConnStr)
        {
            SqlConnection SqlCon = new SqlConnection(ConnStr);

            #region MyRegion
            if (SQLDS.Tables.Contains("tblparam"))
            {
                SQLDS.Tables.Remove("tblparam");
            }

            SqlExec("select * from param_Proc_Name where proc_id in"
                        + "( select proc_id from Proc_Name where proc_name = '" + SQLTxt.Trim() + "')  order by rec_proc_id asc ", "tblparam");

            if (list.Count != SQLDS.Tables["tblparam"].Rows.Count)
            {
                MessageBox.Show("No. of parameters not equal to procedure parameters");
                return;
            }

            int i = 0;
            SqlParameter[] sqlParams = new SqlParameter[list.Count];
            foreach (DataRow row in SQLDS.Tables["tblparam"].Rows)
            {
                if (list[i] == null)
                {
                    list[i] = 0;
                }
                if (row[1].ToString() == "0")
                {
                    if (list[i] is byte[])
                    {
                        sqlParams[i] = new SqlParameter(row[0].ToString(), SqlDbType.Image);
                        sqlParams[i].Value = list[i];
                    }
                    else
                    {
                        sqlParams[i] = new SqlParameter(row[0].ToString(), list[i].ToString());
                    }
                }
                else
                {
                    sqlParams[i] = new SqlParameter(row[0].ToString(), SqlDbType.VarChar, 200);
                    sqlParams[i].Direction = ParameterDirection.Output;
                }
                i++;
            }
            #endregion

            try
            {
                SQLCS.Open();

                SQLCMD = SQLCS.CreateCommand();
                SqlCommand command1 = SqlCon.CreateCommand();
                SqlTransaction transaction = null;
                SqlTransaction transaction1 = null;

                // Start a local transaction.
                transaction = SQLCS.BeginTransaction("OuterTransaction");

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SQLCMD.Connection = SQLCS;
                SQLCMD.Transaction = transaction;

                try
                {
                    SQLCMD.CommandType = CommandType.StoredProcedure;
                    SQLCMD.CommandText = SQLTxt;
                    SQLCMD.CommandTimeout = 0;

                    if (sqlParams != null && sqlParams.Length > 0)
                        SQLCMD.Parameters.AddRange(sqlParams);

                    SQLCMD.ExecuteNonQuery();

                    try
                    {
                        SqlCon.Open();
                        transaction1 = SqlCon.BeginTransaction("NestedTransaction");
                        SQLCMD.Connection = SqlCon;
                        SQLCMD.Transaction = transaction1;

                        SQLCMD.CommandType = CommandType.StoredProcedure;
                        SQLCMD.CommandText = SQLTxt;
                        SQLCMD.CommandTimeout = 0;
                        SQLCMD.Parameters.Clear();
                        if (sqlParams != null && sqlParams.Length > 0)
                            SQLCMD.Parameters.AddRange(sqlParams);

                        SQLCMD.ExecuteNonQuery();

                        transaction1.Commit();
                        transaction.Commit();

                        SQLCMD.Dispose();
                        SqlCon.Close();
                        SQLCS.Close();
                    }

                    catch (Exception ex)
                    {
                        if (ex is SqlException)
                        {
                            if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                            {

                                MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                                "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                                try
                                {
                                    transaction1.Rollback();
                                }
                                catch { }
                                try
                                {
                                    transaction.Rollback();
                                }
                                catch { }
                                return;
                            }
                            else
                            {
                                MyErrorHandler.SQLHandler(ex);
                                try
                                {
                                    transaction1.Rollback();
                                }
                                catch { }
                                try
                                {
                                    transaction.Rollback();
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            MyErrorHandler.Handler(ex);
                            try
                            {
                                transaction1.Rollback();
                            }
                            catch { }
                            try
                            {
                                transaction.Rollback();
                            }
                            catch { }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex is SqlException)
                    {
                        if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                        {

                            MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                            "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                            try
                            {
                                transaction1.Rollback();
                            }
                            catch { }
                            try
                            {
                                transaction.Rollback();
                            }
                            catch { }
                            return;
                        }
                        else
                        {
                            MyErrorHandler.SQLHandler(ex);
                            try
                            {
                                transaction1.Rollback();
                            }
                            catch { }
                            try
                            {
                                transaction.Rollback();
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        MyErrorHandler.Handler(ex);
                        try
                        {
                            transaction1.Rollback();
                        }
                        catch { }
                        try
                        {
                            transaction.Rollback();
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                MyErrorHandler.Handler(ex);
            }
        }
        //-------------------------------------------
        public static void SqlExec(string SQLTxt, SqlCommand SQLCMD, string ConnStr)
        {
            try
            {
                SqlConnection SqlCon = new SqlConnection(ConnStr);
                SqlCon.Open();
                SQLCMD.CommandText = SQLTxt;
                SQLCMD.CommandType = CommandType.StoredProcedure;
                SQLCMD.Connection = SqlCon;
                SQLCMD.ExecuteNonQuery();
                SQLCS.Close();
                SQLCMD.Dispose();
            }

            catch (Exception ex)
            {
                SQLCS.Close();
                if (ex is SqlException)
                {
                    if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                    {

                        MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                        "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                        return;
                    }
                    else
                    {
                        MyErrorHandler.SQLHandler(ex);
                    }
                }
                else
                {
                    MyErrorHandler.SQLHandler(ex);
                }
            }
            //SqlConnection SqlCon = new SqlConnection(ConnStr);
            //SqlCommand Cmd = new SqlCommand();

            //foreach (SqlParameter SQLPAram in SQLCMD.Parameters)
            //{
            //    Cmd.Parameters.AddWithValue(SQLPAram.ParameterName.ToString(), SQLPAram.Value);
            //}

            //try
            //{
            //    SQLCS.Open();

            //    SQLCMD = SQLCS.CreateCommand();
            //    SqlCommand command1 = SqlCon.CreateCommand();
            //    SqlTransaction transaction = null;
            //    SqlTransaction transaction1 = null;

            //    // Start a local transaction.
            //    transaction = SQLCS.BeginTransaction("OuterTransaction");

            //    // Must assign both transaction object and connection 
            //    // to Command object for a pending local transaction
            //    SQLCMD.Connection = SQLCS;
            //    SQLCMD.Transaction = transaction;

            //    try
            //    {
            //        SQLCMD.CommandType = CommandType.StoredProcedure;
            //        SQLCMD.CommandText = SQLTxt;
            //        SQLCMD.CommandTimeout = 3000;
            //        SQLCMD.Parameters.Clear();

            //        foreach (SqlParameter SQLPAram in Cmd.Parameters)
            //        {
            //            SQLCMD.Parameters.AddWithValue(SQLPAram.ParameterName.ToString(), SQLPAram.Value);
            //        }

            //        SQLCMD.ExecuteNonQuery();

            //        try
            //        {
            //            SqlCon.Open();
            //            transaction1 = SqlCon.BeginTransaction("NestedTransaction");
            //            SQLCMD.Connection = SqlCon;
            //            SQLCMD.Transaction = transaction1;

            //            SQLCMD.CommandType = CommandType.StoredProcedure;
            //            SQLCMD.CommandText = SQLTxt;
            //            SQLCMD.CommandTimeout = 3000;

            //            SQLCMD.Parameters.Clear();
            //            foreach (SqlParameter SQLPAram in Cmd.Parameters)
            //            {
            //                SQLCMD.Parameters.AddWithValue(SQLPAram.ParameterName.ToString(), SQLPAram.Value);
            //            }

            //            SQLCMD.ExecuteNonQuery();

            //            transaction1.Commit();
            //            transaction.Commit();

            //            SQLCMD.Dispose();
            //            SqlCon.Close();
            //            SQLCS.Close();
            //        }

            //        catch (Exception ex)
            //        {
            //            if (ex is SqlException)
            //            {
            //                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
            //                {

            //                    MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
            //                                    "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
            //                    try
            //                    {
            //                        transaction1.Rollback();
            //                    }
            //                    catch { }
            //                    try
            //                    {
            //                        transaction.Rollback();
            //                    }
            //                    catch { }
            //                    return;
            //                }
            //                else
            //                {
            //                    MyErrorHandler.SQLHandler(ex);
            //                    try
            //                    {
            //                        transaction1.Rollback();
            //                    }
            //                    catch { }
            //                    try
            //                    {
            //                        transaction.Rollback();
            //                    }
            //                    catch { }
            //                }
            //            }
            //            else
            //            {
            //                MyErrorHandler.Handler(ex);
            //                try
            //                {
            //                    transaction1.Rollback();
            //                }
            //                catch { }
            //                try
            //                {
            //                    transaction.Rollback();
            //                }
            //                catch { }
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        if (ex is SqlException)
            //        {
            //            if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
            //            {

            //                MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
            //                                "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
            //                try
            //                {
            //                    transaction1.Rollback();
            //                }
            //                catch { }
            //                try
            //                {
            //                    transaction.Rollback();
            //                }
            //                catch { }
            //                return;
            //            }
            //            else
            //            {
            //                MyErrorHandler.SQLHandler(ex);
            //                try
            //                {
            //                    transaction1.Rollback();
            //                }
            //                catch { }
            //                try
            //                {
            //                    transaction.Rollback();
            //                }
            //                catch { }
            //            }
            //        }
            //        else
            //        {
            //            MyErrorHandler.Handler(ex);
            //            try
            //            {
            //                transaction1.Rollback();
            //            }
            //            catch { }
            //            try
            //            {
            //                transaction.Rollback();
            //            }
            //            catch { }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MyErrorHandler.Handler(ex);
            //}
        }
        //-------------------------------------------
        public static void SqlExec(string SQLTxt, SqlCommand SQLCMD)
        {
            try
            {
                SQLCS.Open();
                SQLCMD.CommandText = SQLTxt;
                SQLCMD.CommandType = CommandType.StoredProcedure;
                SQLCMD.Connection = connection.SQLCS;
                SQLCMD.CommandTimeout = 0;
                SQLCMD.ExecuteNonQuery();
                SQLCS.Close();
                SQLCMD.Dispose();
            }

            catch (Exception ex)
            {
                SQLCS.Close();
                if (ex is SqlException)
                {
                    if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                    {

                        MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                        "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                        return;
                    }
                    else
                    {
                        MyErrorHandler.SQLHandler(ex);
                    }
                }
                else
                {
                    MyErrorHandler.SQLHandler(ex);
                }
            }
        }
        //-----------------------------------------------------------------------------------------------------------
        public static string Records()
        {
            string s1st = " Record  ";
            string s2nd = " Of  ";

            if (Lang_id == 1)
            {
                s1st = " القيد  ";
                s2nd = " من  ";
            }
            string Record = s1st + (connection.SQLBS.Position + 1) + s2nd + (connection.SQLBS.Count);
            return Record;

        }
        //-----------------------------------------------------------------------------------------------------------
        public static string Records(BindingSource _Bs)
        {
            string s1st = " Record  ";
            string s2nd = " Of  ";

            if (Lang_id == 1)
            {
                s1st = " القيد  ";
                s2nd = " من  ";
            }
            string Record = s1st + (_Bs.Position + 1) + s2nd + (_Bs.Count);
            return Record;

        }
        //-----------------------------------------------------------------------------------------------------------
        public static void Control_Cap(Form Frm, int Tag)
        {
            try
            {
                string RTL = Lang_id == 1 ? "A" : "E";
                string sqltext;

                sqltext = " select B.Obj_Name , Obj_" + RTL + "Cap as cap, PObj_Name,C.Frm_" + RTL + "Cap ,Cast(GetDate() as Date) as CurrentDate "
                        + " From Frm_Obj A ,Object_UI B, Forms_UI C where "
                        + " A.Obj_Id = B.Obj_Id "
                        + " and A.Frm_Id = C.Frm_Id"
                        + " and C.Frm_Id = " + Tag;


                DataTable DT = SqlExec(sqltext, "TblObj");

                //int i = 0;
                DataGridView Dgv = new DataGridView();
                foreach (DataRow Row in DT.Rows)
                {
                    if (Frm.Controls[Row[2].ToString()] is DataGridView)
                    {
                        Dgv = Frm.Controls[Row[2].ToString()] as DataGridView;
                        Dgv.Columns[Row[0].ToString()].HeaderText = Row[1].ToString();
                        //i++;
                    }
                    else if (Frm.Controls[Row[2].ToString()] is BindingNavigator)
                    {
                        BindingNavigator Bna = Frm.Controls[Row[2].ToString()] as BindingNavigator;
                        Bna.Items[Row[0].ToString()].Text = Row[1].ToString();
                    }
                    else if (Frm.Controls[Row[2].ToString()] is GroupBox)
                    {
                        GroupBox Grb = Frm.Controls[Row[2].ToString()] as GroupBox;
                        Grb.Text = Row[1].ToString();
                        if (Grb.Controls[Row[0].ToString()] is CheckBox)
                        {
                            Grb.Controls[Row[0].ToString()].Text = Row[1].ToString();
                        }
                    }
                    else if (Frm.Controls[Row[2].ToString()] is FlowLayoutPanel)
                    {
                        FlowLayoutPanel FLB = Frm.Controls[Row[2].ToString()] as FlowLayoutPanel;
                        FLB.Controls[Row[0].ToString()].Text = Row[1].ToString();
                        if (Lang_id != 1)
                        {
                            FLB.Controls[Row[0].ToString()].RightToLeft = RightToLeft.No;
                            FLB.RightToLeft = RightToLeft.No;
                        }
                    }
                    else if (Row[2].ToString() == "")
                    {
                        Frm.Controls[Row[0].ToString()].Text = Row[1].ToString();
                    }
                    Frm.Text = Row[3].ToString();
                }
                //if (i != 0)
                //{
                //    for (int j = i; j < Dgv.ColumnCount; j++)
                //        Dgv.Columns[j].Visible = false;
                //}
            }
            catch
            { }
        }
    }
}