﻿using System.Windows.Forms;
using System;
using System.Data;

namespace Integration_Accounting_Sys
{
    public static class Page_Setting
    {

        public static string Chk_Value = "";
        public static string Chk_Is_Close = "";
        public static int chk_daily_opening = 0;
        public static int d_cur_nrec_date = 0;
        public static int int_nrecdate = 0;
        public static void Header_Page(Form Frm, TextBox Txt_Loc_Cur, TextBox Txt_User,
            TextBox TxtBox_User, MaskedTextBox TxtCur_Date, TextBox TxtTerm_Name)
        {
            Chk_Value = "";
            Chk_Is_Close = "";
            Txt_Loc_Cur.DataBindings.Clear();
            Txt_User.DataBindings.Clear();
            TxtBox_User.DataBindings.Clear();
            TxtCur_Date.DataBindings.Clear();
            TxtTerm_Name.DataBindings.Clear();
            string Nrec_Date = "";
            int R_Close_Flag = 0;
            string Cur_date = "";
            int user_state = 0;
             chk_daily_opening = 0;
             d_cur_nrec_date = 0;

             connection.SqlExec("Select Count(*)  as count from Daily_Opening where t_id =" + connection.T_ID, "Check_tbl");
             if (Convert.ToInt16(connection.SQLDS.Tables["Check_tbl"].Rows[0]["count"]) == 0)
             {
                 String Sql_Text = " Select convert(varchar(12), STARTDATE ,111) As STARTDATE1,"
                    + " (day(STARTDATE)+ month(STARTDATE)*100 + year(STARTDATE)*10000) as STARTDATE "
                  + " From  TERMINALS  where t_id = " + connection.T_ID;
                 String Start_date = connection.SqlExec(Sql_Text, "Date_TbL").Rows[0]["STARTDATE1"].ToString();

                 DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريد بدء عملية تقييد بتاريخ " + Start_date :
                   "Do you want To Satrt Recording " + Start_date, MyGeneral_Lib.LblCap,
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                 if (Dr == DialogResult.Yes)
                 {

                     connection.SQLCMD.Parameters.AddWithValue("@T_ID", connection.T_ID);
                     connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                     connection.SQLCMD.Parameters.AddWithValue("@Start_date", connection.SQLDS.Tables["Date_TbL"].Rows[0]["STARTDATE"]);
                     connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                     connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

                     connection.SqlExec("Add_Daily_Opning", connection.SQLCMD);

                     if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                     {
                         MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                         connection.SQLCMD.Parameters.Clear();
                         return;
                     }
                     connection.SQLCMD.Parameters.Clear();

                 }
                 else
                 {
                     Chk_Value = "0";
                     return;

                 }

             }
            object[] Sparams = { connection.user_id, connection.T_ID };
            connection.SqlExec("Header_Page", "HPage_Tbl", Sparams);

            TxtCur_Date.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl2"], "Nrec_Date");
            TxtTerm_Name.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl"], connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name");
            connection.Discount_Amount = Convert.ToDecimal(connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["DISCOUNT_AMNT"]);
            Txt_Loc_Cur.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl"], connection.Lang_id == 1 ? "Cur_AName" : "Cur_EName");
            Txt_Loc_Cur.DataBindings.Add("Tag", connection.SQLDS.Tables["HPage_Tbl"], "Loc_Cur_Id");
            Txt_User.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl"], "User_Name");
            TxtBox_User.DataBindings.Add("Text", connection.SQLDS.Tables["HPage_Tbl1"], connection.Lang_id == 1 ? "Acust_Name" : "Ecust_Name");
            TxtBox_User.DataBindings.Add("Tag", connection.SQLDS.Tables["HPage_Tbl1"], "Cust_Id");
            connection.Limit_Exchange_LC = Convert.ToDecimal(connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Limit_Exchange_LC"]);
            if( connection.SQLDS.Tables["HPage_Tbl1"].Rows.Count >0)
            {
            connection.Box_Cust_Id = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl1"].Rows[0]["CUST_id"]);
            connection.Loc_Cur_Id = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Loc_Cur_Id"]);
                
            }
        
            Cur_date =  connection.SQLDS.Tables["HPage_Tbl"].Rows[0]["Cur_Date"].ToString();
            Nrec_Date = connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["Nrec_Date"].ToString();
            int_nrecdate = Convert.ToInt32( connection.SQLDS.Tables["HPage_Tbl2"].Rows[0]["int_nrecdate"]);
            R_Close_Flag = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl3"].Rows[0]["R_Close_Flag"]);
            chk_daily_opening = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl6"].Rows[0]["chk_daily_opening"]);
            user_state = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl5"].Rows[0]["User_State"]);
            d_cur_nrec_date = Convert.ToInt32(connection.SQLDS.Tables["HPage_Tbl7"].Rows[0]["d_cur_nrec_date"]);
            //-----------------

            if (chk_daily_opening == 1)//auto
           {
                
                if (R_Close_Flag == 0 && d_cur_nrec_date == int_nrecdate) //في حالة الاغلاق لليوم السابق وبدون افتتاح يوم عمل جديد
                {
                    Chk_Is_Close = connection.Lang_id == 1 ? "لا يمكن التقييد لعدم افتتاح السجلات" : "the records is not opened you cant do new record ";
                }

                if (R_Close_Flag == 0 && d_cur_nrec_date < int_nrecdate && user_state == 0) //في حالة الاغلاق لليوم السابق وبدون افتتاح يوم عمل جديد
                {
                    Chk_Is_Close = connection.Lang_id == 1 ? "يرجى تفعيل المستخدم" : "please active user ";
                }
            }

            if (Convert.ToDateTime(Nrec_Date) > Convert.ToDateTime(Cur_date))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "التاريخ الحالي اقل من أخر تاريخ للأفتتاح": " The current date is minimal the the opening date ", MyGeneral_Lib.LblCap);
                return;
            }

            if (R_Close_Flag == 1 && Cur_date != Nrec_Date ) //في حالة الاغلاق لليوم السابق وبدون افتتاح يوم عمل جديد
            {
                Chk_Is_Close = connection.Lang_id == 1 ? "لا يمكن التقييد لعدم افتتاح السجلات " : "the records is not opened you cant do new record ";
            }

         

            if (R_Close_Flag == 0)
            {
                if (Cur_date != Nrec_Date)
                {
                    DialogResult Dresult = MessageBox.Show(connection.Lang_id == 1 ?" التاريخ الحالي " + Cur_date
                           + " و تاريخ الارصدة المثبتة " + Nrec_Date
                           + " هل تريد الاستمرار ؟ " : " the current date is  " + Cur_date
                           + " and the settled account date " + Nrec_Date
                           + " do you want to continue? ", MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo);

                    if (Dresult == DialogResult.No)
                    {
                        Frm.Load += (s, e) => Frm.Close();
                        return;
                    }

                    TxtCur_Date.DataBindings.Clear();
                    TxtCur_Date.Text = Nrec_Date;
                }
            }


            if (connection.SQLDS.Tables["HPage_Tbl2"].Rows.Count > 0)
            {
               
                if (R_Close_Flag == 1 && Cur_date == Nrec_Date)
                {
                    Chk_Value = connection.Lang_id == 1 ? "لا يمكن الافتتاح " : "You Can't Open ";
                }

            }
        }
        //------------------------------
        public static void Header_Page(TextBox Txt_User, MaskedTextBox TxtCur_Date)
        {
            object[] Sparams = { connection.Comp_Id, connection.user_id , connection.T_ID};
            connection.SqlExec("Header_Page1", "HeaderPage_Tbl", Sparams);
            Txt_User.DataBindings.Clear();
            TxtCur_Date.DataBindings.Clear();
            Txt_User.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl"], "User_Name");
            TxtCur_Date.DataBindings.Add("Text", connection.SQLDS.Tables["HeaderPage_Tbl"], "Cur_Date");
        }
    }
}