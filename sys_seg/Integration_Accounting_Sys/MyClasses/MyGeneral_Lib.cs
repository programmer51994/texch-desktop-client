﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Integration_Accounting_Sys
{
    public static class MyGeneral_Lib
    {
        public static int Tag_Edit = 1;
        public static int Emp_Out = 15;
        public static string LblCap = connection.Lang_id == 1 ? "رسالة تحذير" : "Warning Message";
        public static void Form_Orientation(Form Frm)
        {
            if (connection.Lang_id == 1)
            {
                Frm.RightToLeft = RightToLeft.Yes;
                Frm.RightToLeftLayout = true;

            }
            else
            {
                Frm.RightToLeft = RightToLeft.No;
                Frm.RightToLeftLayout = false;
            }
        }
        //------------------------------------------------------------------------------------------
        public static string Dec_2_36(long Dec_No)
        {
            #region Create Dec2_36 DateTable Used For Hex
            DataTable Dec2_36 = new DataTable();
            string[] Columns = { "No", "Code" };
            string[] DTypes = { "System.Int32", "System.String" };
            string[] Code = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F",
                                "G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U",
                                "V","W","X","Y","Z"};
            int[] No = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10, 11, 12, 13, 14, 15, 
                           16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                           31, 32, 33, 34, 35 };
            Dec2_36 = CustomControls.Custom_DataTable("Dec2_36", Columns, Code, No, DTypes);
            #endregion
            string Y_36 = string.Empty;
            while (true)
            {
                DataRow[] Record = Dec2_36.DefaultView.ToTable().Select("No = " + (Dec_No % 36));
                Y_36 = Record[0]["Code"] + Y_36;
                Dec_No = (int)Dec_No / 36;
                if (Dec_No == 0)
                    break;
            }

            return Y_36;
        }
        //------------------------------------------------------------------------------------------
        public static string Dec_2_16(long Int_No)
        {
            // Convert integer Int_No as a hex in a string variable 
            string HexValue = Int_No.ToString("X");
            return HexValue;
        }
        //------------------------------------------------------------------------------------------
        /// <summary>
        /// Check and reformat input stream 
        /// </summary>
        /// <param name="DateStr"> Date string </param>
        /// <returns> 
        /// -1 for each Exception
        /// 0 for 0 date string
        /// default Formatted Date </returns>
        public static string DateChecking(string DateStr)
        {
            #region Defintions && Substring
            string LblMsg = "DateExceptions";
            string YearStr = string.Empty;
            string Monthstr = string.Empty;
            string DayStr = string.Empty;
            int Year = 0;
            int Month = 0;
            int Day = 0;
            bool LeapYear = false;
            try
            {
                YearStr = DateStr.Substring(0, 4);

                int.TryParse(YearStr.Trim(), out Year);

                Monthstr = DateStr.Substring(5, 2);

                int.TryParse(Monthstr.Trim(), out Month);

                DayStr = DateStr.Substring(8, 2);

                int.TryParse(DayStr.Trim(), out Day);
            }
            catch
            {
                return "0";
            }
            #endregion

            if (Year == 0 && Month == 0 && Day == 0)
                return "0";

            if (Year <= 0)
            {
                MessageBox.Show("Year out of range", LblMsg);
                return "-1";
            }
            if (Year < 100)
            {
                YearStr = (2000 + Year).ToString();
            }
            //-----------Check leap year
            //-----------LeapYear = true
            if (Year % 4 == 0)
            {
                if (!(Year % 100 == 0 && Year % 400 != 0))
                {
                    LeapYear = true;
                }
            }

            if (Month > 12 || Month <= 0)
            {
                MessageBox.Show("Month out of range", LblMsg);
                return "-1";
            }
            if (Month < 10)
            {
                Monthstr = "0" + Month.ToString();
            }

            if (Day > 31 || Day <= 0)
            {
                MessageBox.Show("Day out of range", LblMsg);
                return "-1";
            }
            if (Day < 10)
            {
                DayStr = "0" + Day.ToString();
            }

            #region Check Day Count
            switch (Month)
            {
                case 1:
                    if (Day > 31)
                    {
                        MessageBox.Show("January month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 2:
                    if (Day > 29 && LeapYear)
                    {
                        MessageBox.Show("Leap year \nFebruary month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    if (Day > 28 && !(LeapYear))
                    {
                        MessageBox.Show("Is not a leap year \nFebruary month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 3:
                    if (Day > 31)
                    {
                        MessageBox.Show("March month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 4:
                    if (Day > 30)
                    {
                        MessageBox.Show("April month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 5:
                    if (Day > 31)
                    {
                        MessageBox.Show("May month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 6:
                    if (Day > 30)
                    {
                        MessageBox.Show("June month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 7:
                    if (Day > 31)
                    {
                        MessageBox.Show("July month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 8:
                    if (Day > 31)
                    {
                        MessageBox.Show("August month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 9:
                    if (Day > 30)
                    {
                        MessageBox.Show("September month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 10:
                    if (Day > 31)
                    {
                        MessageBox.Show("October month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 11:
                    if (Day > 30)
                    {
                        MessageBox.Show("November month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
                case 12:
                    if (Day > 31)
                    {
                        MessageBox.Show("December month \nDay out of range", LblMsg);
                        return "-1";
                    }
                    break;
            }
            #endregion

            return YearStr + Monthstr + DayStr;

        }
        /// <summary>
        /// Set procedure execution to clipbord
        /// </summary>
        /// <param name="Proc_Name"> Procedure Name </param>
        /// <param name="ItemList"> Parameters ArrayList  </param>
        public static void Copytocliptext(string Proc_Name, ArrayList ItemList)
        {
            var result = string.Join("','", ItemList.ToArray().Select(o => o.ToString()).ToArray());
            Clipboard.SetText("Exec " + Proc_Name + " '" + result + "'");
        }
        public static SqlParameter[] GetSqlParameterArray(ArrayList ItemList)
        {
            return (SqlParameter[])ItemList.ToArray(typeof(SqlParameter));
        }
        public static void Copytocliptext(string Proc_Name, SqlParameter[] ItemList)
        {


            var result = string.Join("','", (string)ItemList.ToArray().Select(o => o.ToString()).ToArray().ToString());
            Clipboard.SetText("Exec " + Proc_Name + " '" + result + "'");
        }
        //-----------------------
        //----------------------- cliptext add with value
        public static void Copytocliptext(string Proc_Name, SqlCommand SqlCommmand)
        {
            int count = 0;
            string cliptxt = "";
            count = SqlCommmand.Parameters.Count;
            for (int i = 0; i < count; i++)
            {
                var command = SqlCommmand.Parameters[i].Value;
                //if (SqlCommmand.Parameters[i].SqlDbType == SqlDbType.VarChar || SqlCommmand.Parameters[i].SqlDbType == SqlDbType.Text || SqlCommmand.Parameters[i].Value == "System.String" )
                //{
                command = "'" + command.ToString() + "'";
                //}
                cliptxt += command + ",";
            }

            cliptxt = cliptxt.Remove(cliptxt.Length - 1);
            Clipboard.SetText("Exec " + Proc_Name + " " + cliptxt);
        }
        /// <summary>
        /// Set procedure execution to clipbord
        /// </summary>
        /// <param name="Proc_Name"> Procedure Name </param>
        /// <param name="ItemList"> Parameters Array object</param>
        public static void Copytocliptext(string Proc_Name, object[] ItemList)
        {
            var result = string.Join("','", ItemList.ToArray().Select(o => o.ToString()).ToArray());
            Clipboard.SetText("Exec " + Proc_Name + " '" + result + "'");
        }
        //-----------------------
        /// <summary>
        /// Set sql command text execution to clipbord
        /// </summary>
        /// <param name="SqlTxt"></param>
        public static void Copytocliptext(string SqlTxt)
        {
            Clipboard.SetText("Exec " + SqlTxt);
        }
        //---------------------------
        public static void ColumnToString(DataTable Dt, string ColumnName, out string strColName)
        {
            var Lst = (from row in Dt.AsEnumerable() select row[ColumnName]).ToList();
            strColName = string.Join(",", Lst.ToArray().Select(o => o.ToString()).ToArray());
        }
        //---------------------------
        public static string TranslateText(string inputStr, string fromLanguage, string toLanguage)
        {
            if (CheckInternetConnection())
            {
                try
                {
                    string languagePair = fromLanguage + "|" + toLanguage;

                    string translation = "";
                    string[] laynes = inputStr.Split('\n');

                    string url = String.Format("http://www.google.com/translate_t?hl=en&ie=UTF8&text={0}&langpair={1}", inputStr, languagePair);
                    WebClient webClient = new WebClient();
                    webClient.Encoding = UTF8Encoding.Default;

                    byte[] resultData = webClient.DownloadData(url);
                    string charset = Regex.Match(webClient.ResponseHeaders["Content-Type"], "(?<=charset=)[\\w-]+").Value;

                    string result = Encoding.GetEncoding(charset).GetString(resultData);

                    if (laynes.Length > 1)
                    {
                        for (int i = 0; i < laynes.Length; i++)
                        {
                            result = result.Substring(result.IndexOf("<span title=\"") + "<span title=\"".Length);
                            if (i != laynes.Length - 1)
                            {
                                result = result.Substring(result.IndexOf(">") + 101);
                            }
                            else
                            {
                                result = result.Substring(result.IndexOf(">") + 1);
                            }
                            translation += result.Substring(0, result.IndexOf("</span>"));
                        }
                        //result = WebUtility.HtmlDecode(result.Trim()); <---- translated from Japanese to English
                        return translation.Replace("<br>", "\n");
                    }
                    else
                    {
                        result = result.Substring(result.IndexOf("<span title=\"") + "<span title=\"".Length);
                        result = result.Substring(result.IndexOf(">") + 1);
                        result = result.Substring(0, result.IndexOf("</span>"));
                        //result = WebUtility.HtmlDecode(result.Trim());
                        return result.Replace("<br>", "\n");
                    }

                }
                catch
                {
                    return "-1";
                }
            }
            else
            {
                return "-1";
            }
        }
        //---------------------------
        public static DataTable PopulateLanguageList()
        {
            DataTable LangList_Tbl = new DataTable();
            LangList_Tbl.Columns.Add("Lang_Name");
            LangList_Tbl.Columns.Add("Lang_Code");
            LangList_Tbl.Rows.Add("Arabic", "ar");
            LangList_Tbl.Rows.Add("Czech", "cs");
            LangList_Tbl.Rows.Add("Danish", "da");
            LangList_Tbl.Rows.Add("German", "de");
            LangList_Tbl.Rows.Add("English", "en");
            LangList_Tbl.Rows.Add("Estonian", "et");
            LangList_Tbl.Rows.Add("Finnish", "fi");
            LangList_Tbl.Rows.Add("Dutch", "nl");
            LangList_Tbl.Rows.Add("Greek", "el");
            LangList_Tbl.Rows.Add("Hebrew", "he");
            LangList_Tbl.Rows.Add("Haitian Creole", "ht");
            LangList_Tbl.Rows.Add("Hindi", "hi");
            LangList_Tbl.Rows.Add("Hungarian", "hu");
            LangList_Tbl.Rows.Add("Indonesian", "id");
            LangList_Tbl.Rows.Add("Italian", "it");
            LangList_Tbl.Rows.Add("Japanese", "ja");
            LangList_Tbl.Rows.Add("Korean", "ko");
            LangList_Tbl.Rows.Add("Lithuanian", "lt");
            LangList_Tbl.Rows.Add("Latvian", "lv");
            LangList_Tbl.Rows.Add("Norwegian", "no");
            LangList_Tbl.Rows.Add("Polish", "pl");
            LangList_Tbl.Rows.Add("Portuguese", "pt");
            LangList_Tbl.Rows.Add("Romanian", "ro");
            LangList_Tbl.Rows.Add("Spanish", "es");
            LangList_Tbl.Rows.Add("Russian", "ru");
            LangList_Tbl.Rows.Add("Slovak", "sk");
            LangList_Tbl.Rows.Add("Slovene", "sl");
            LangList_Tbl.Rows.Add("Swedish", "sv");
            LangList_Tbl.Rows.Add("Thai", "th");
            LangList_Tbl.Rows.Add("Turkish", "tr");
            LangList_Tbl.Rows.Add("Ukranian", "uk");
            LangList_Tbl.Rows.Add("Vietnamese", "vi");
            LangList_Tbl.Rows.Add("Simplified Chinese", "zh-CHS");
            LangList_Tbl.Rows.Add("Traditional Chinese", "zh-CHT");
            return LangList_Tbl;
        }
        //---------------------------
        public static bool CheckInternetConnection()
        {
            try
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply pingStatus = ping.Send("www.google.com", 1000);
                if (pingStatus.Status == System.Net.NetworkInformation.IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }


        internal static void Copytocliptext(string p, string p_2, object[] p_3)
        {
            throw new NotImplementedException();
        }
    }
}