﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace Integration_Accounting_Sys
{
    class NewExpoert
    {
        public static void Export_ToExcel(String TitleName, DataTable EXPORT_DT, DataGridView GRD_Header, bool Visible_Flage = false)
        {
            //----- Visible_Flage= false معناها كل الكولوم الظاهرة
            //----- Visible_Flage= true  معناها كل الكولوم الظاهرة والمخفية
            string pFilePath = "";
            if (EXPORT_DT.Rows.Count <= 0)
            {
                MessageBox.Show("لا توجد قيود للتصدير2", MyGeneral_Lib.LblCap);
                return;
            }

            #region SaveFileDialog
            SaveFileDialog SVFDlg = new SaveFileDialog();
            SVFDlg.Title = "Save Export Files";
            SVFDlg.Filter = "Excel Worksheets|*.xls|All files (*.*)|*.*";
            #endregion

            if (SVFDlg.ShowDialog() == DialogResult.OK && SVFDlg.FileName != "")
            {
                pFilePath = SVFDlg.FileName;
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                string data = null;
                int i = 0;
                int j = 0;
                // int colspan = 0;
                int ColumnCount = 0;
                DateTime date_Now = Convert.ToDateTime(DateTime.Now.ToString("d"));
                if (Visible_Flage == false)
                {
                    ColumnCount = GRD_Header.Columns.GetColumnCount(DataGridViewElementStates.Visible);
                }
                else
                {
                    ColumnCount = GRD_Header.ColumnCount;
                }
                //-----------------------------------------
                //Titel Name
                //for (j = 0; j <= GRD_Header.ColumnCount - 1; j++)
                //{
                //    if (GRD_Header.ColumnCount > colspan)
                //    {
                //        colspan = GRD_Header.ColumnCount;
                //    }
                //    colspan = GRD_Header.ColumnCount;
                //}

                for (j = 1; j <= GRD_Header.ColumnCount; j++)
                {
                    if (j == ColumnCount / 2) //--منتصف عدد الكولمات يكون اسم الموضوع
                    {
                        xlWorkSheet.Cells[i + 1, j] = TitleName;
                    }
                }
                //-----------------------------------------
                //user Name & Date
                for (j = 1; j <= GRD_Header.ColumnCount; j++)
                {
                    if (j == ColumnCount)
                    {
                        xlWorkSheet.Cells[i + 2, j] = connection.Lang_id == 1 ? "أســـم المستخدم :-" + connection.User_Name : "User Name :-" + connection.User_Name;
                        xlWorkSheet.Cells[i + 3, j] = connection.Lang_id == 1 ? "التــــاريـــخ :-" + date_Now : "Date :-" + date_Now;
                    }
                }
                //-----------------------------------------
                //loop header in Grid 
                int count = 0;
                for (j = 0; j <= GRD_Header.ColumnCount - 1; j++)
                {

                    if (GRD_Header.Columns[j].Visible == false && Visible_Flage == false)
                    {
                        continue;
                    }
                    else
                    {
                        count = count + 1;
                        string colName = GRD_Header.Columns[j].HeaderText;
                        xlWorkSheet.Cells[i + 5, count] = colName;
                    }
                }
                //-----------------------------------------
                //loop row in Table
                for (i = 0; i <= EXPORT_DT.Rows.Count - 1; i++)
                {
                    for (j = 0; j <= EXPORT_DT.Columns.Count - 1; j++)
                    {
                        data = EXPORT_DT.Rows[i].ItemArray[j].ToString();
                        xlWorkSheet.Cells[i + 6, j + 1] = data;
                    }
                }
                //-----------------------------------------
                try
                {
                    xlWorkBook.SaveAs(pFilePath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();
                }
                catch
                {
                    MessageBox.Show("الملف مفتوح يرجى غلق الملف أولا لغرض التصدير", "رسالة تحذير");
                    return;
                }

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                MessageBox.Show("Data Exported Successfully", "Success");
            }

        }

        public static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}