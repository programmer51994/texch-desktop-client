﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
namespace Integration_Accounting_Sys
{
  public static  class MyErrorHandler
    {
        private static string CurrentDirectory = string.Empty;

        public static void ExceptionHandler(Exception ex)
        {
            //connection.SQLCMD.Parameters.Clear();
            //object[] Sparams = { connection.Comp_Id, connection.user_id, "" };
            //connection.SqlExec("User_LogOut", "LogOut_User_Tbl", Sparams);

            CurrentDirectory = Directory.GetCurrentDirectory();

            if (ex is SqlException)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint") || ex.Message.StartsWith("Violation of PRIMARY KEY constraint"))
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن ادخال قيمة مكررة" :
                                    "You cannot enter duplicate value", MyGeneral_Lib.LblCap);
                    return;
                }
                SQLHandler(ex as SqlException);
            }
            else
            {
                Handler(ex);
            }
        }
        //-----------------------------------------------------------------------------------
        public static int Handler(Exception err)
        {
            //connection.SQLCMD.Parameters.Clear();
            //object[] Sparams = { connection.Comp_Id, connection.user_id, "" };
            //connection.SqlExec("User_LogOut", "LogOut_User_Tbl", Sparams);

            DriveInfo[] drives = DriveInfo.GetDrives();


            foreach (DriveInfo d in drives)
            {


                if (d.IsReady == true && d.Name != "C:\\")
                {


                    string di = d.ToString();
   
            string pathString = System.IO.Path.Combine(di, "error_handler.txt");
            StreamWriter wrFile = new StreamWriter(pathString, true);

            //CurrentDirectory = Directory.GetCurrentDirectory();
            //string full_path = Path.GetFullPath("c:/error_handler.txt");
           // StreamWriter wrFile = new StreamWriter(full_path, true);
            //StreamWriter wrFile = new StreamWriter( CurrentDirectory + "//SysErr.txt", true);
            wrFile.WriteLine("--------------------Date:" + DateTime.Now.ToString() + "--------------------------");
            wrFile.WriteLine("Date:     " + DateTime.Now.ToString());
            if (err.InnerException != null)
            {
                wrFile.WriteLine("InnerExp: " + err.InnerException.Message);
                wrFile.WriteLine("Object:   " + err.InnerException.Source);
            }
            wrFile.WriteLine("Object:   " + err.StackTrace);
            wrFile.WriteLine("Message:  " + err.Message);
            wrFile.WriteLine("Source:   " + err.Source);
            wrFile.WriteLine();
            wrFile.Close();
            MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن الاتصال بمصدر البيانات" :
                            "Can not connect to a data source", MyGeneral_Lib.LblCap);
            try
            {
                Environment.Exit(0);
            }
            catch { }




                } //d.IsReady == true
            } //  foreach

            return 0;
        }
        //-----------------------------------------------------------------------------------
        public static int SQLHandler(Exception err)
        {
            //connection.SQLCMD.Parameters.Clear();
            //object[] Sparams = { connection.Comp_Id, connection.user_id, "" };
            //connection.SqlExec("User_LogOut", "LogOut_User_Tbl", Sparams);

            //CurrentDirectory = Directory.GetCurrentDirectory();
            //string full_path = Path.GetFullPath("c:/error_handler.txt");
            //StreamWriter wrFile = new StreamWriter(full_path, true);


            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (DriveInfo d in drives)
            {


                if (d.IsReady == true && d.Name != "C:\\")
                {


                    string di = d.ToString();
                    string pathString = System.IO.Path.Combine(di, "error_handler.txt");
                    StreamWriter wrFile = new StreamWriter(pathString, true);

                    wrFile.WriteLine("--------------------Date:" + DateTime.Now.ToString() + "--------------------------");
                    wrFile.WriteLine("Date:     " + DateTime.Now.ToString());
                    if (err.InnerException != null)
                    {
                        wrFile.WriteLine("InnerExp: " + err.InnerException.Message);
                        wrFile.WriteLine("Object:   " + err.InnerException.Source);
                    }
                    wrFile.WriteLine("Object:   " + err.StackTrace);
                    wrFile.WriteLine("Message:  " + err.Message);
                    wrFile.WriteLine("Source:   " + err.Source);
                    wrFile.WriteLine();
                    wrFile.Close();
                    MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن الاتصال بمصدر البيانات" :
                                    "Can not connect to a data source", MyGeneral_Lib.LblCap);
                    try
                    {
                        Environment.Exit(0);
                    }
                    catch { }
                    



                } //d.IsReady == true
            } //  foreach



            
            return 0;


         
        }
    }
}
