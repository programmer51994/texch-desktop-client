﻿namespace Integration_Accounting_Sys
{
    partial class Gen_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CboCurr_Id = new System.Windows.Forms.ComboBox();
            this.CboCatg_Id = new System.Windows.Forms.ComboBox();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtMax_Vo = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtMin_Vo = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtMax_Qty = new System.Windows.Forms.Sample.DecimalTextBox();
            this.TxtMin_Qty = new System.Windows.Forms.Sample.DecimalTextBox();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(6, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(110, 14);
            this.label4.TabIndex = 101;
            this.label4.Text = "رقـم القيــــد  مــن:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(102, 14);
            this.label1.TabIndex = 100;
            this.label1.Text = "الكميــــــــة  مــن:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(6, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(103, 14);
            this.label2.TabIndex = 106;
            this.label2.Text = "اســم الجهــــــــة:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(290, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(34, 14);
            this.label3.TabIndex = 107;
            this.label3.Text = "الــى";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(104, 14);
            this.label5.TabIndex = 109;
            this.label5.Text = "اســـم العملــــــة:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(290, 49);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(34, 14);
            this.label6.TabIndex = 110;
            this.label6.Text = "الــى";
            // 
            // CboCurr_Id
            // 
            this.CboCurr_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCurr_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCurr_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCurr_Id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCurr_Id.FormattingEnabled = true;
            this.CboCurr_Id.Location = new System.Drawing.Point(118, 75);
            this.CboCurr_Id.Margin = new System.Windows.Forms.Padding(4);
            this.CboCurr_Id.Name = "CboCurr_Id";
            this.CboCurr_Id.Size = new System.Drawing.Size(372, 22);
            this.CboCurr_Id.TabIndex = 4;
            // 
            // CboCatg_Id
            // 
            this.CboCatg_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboCatg_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCatg_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboCatg_Id.Enabled = false;
            this.CboCatg_Id.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCatg_Id.FormattingEnabled = true;
            this.CboCatg_Id.Items.AddRange(new object[] {
            "",
            "بيـــع عمـلات اجنبيـــة",
            "شراء عمـلات اجنبيـــة"});
            this.CboCatg_Id.Location = new System.Drawing.Point(118, 113);
            this.CboCatg_Id.Margin = new System.Windows.Forms.Padding(4);
            this.CboCatg_Id.Name = "CboCatg_Id";
            this.CboCatg_Id.Size = new System.Drawing.Size(372, 22);
            this.CboCatg_Id.TabIndex = 5;
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(248, 158);
            this.ExtBtn.Margin = new System.Windows.Forms.Padding(4);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(104, 29);
            this.ExtBtn.TabIndex = 7;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.ForeColor = System.Drawing.Color.Navy;
            this.SearchBtn.Location = new System.Drawing.Point(144, 158);
            this.SearchBtn.Margin = new System.Windows.Forms.Padding(4);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(104, 29);
            this.SearchBtn.TabIndex = 6;
            this.SearchBtn.Text = "مـوافــق";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.Search_Btn_Click);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-6, 152);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(500, 1);
            this.flowLayoutPanel9.TabIndex = 185;
            // 
            // TxtMax_Vo
            // 
            this.TxtMax_Vo.AllowSpace = false;
            this.TxtMax_Vo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMax_Vo.Location = new System.Drawing.Point(331, 45);
            this.TxtMax_Vo.Name = "TxtMax_Vo";
            this.TxtMax_Vo.Size = new System.Drawing.Size(159, 22);
            this.TxtMax_Vo.TabIndex = 3;
            this.TxtMax_Vo.Text = "0";
            // 
            // TxtMin_Vo
            // 
            this.TxtMin_Vo.AllowSpace = false;
            this.TxtMin_Vo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMin_Vo.Location = new System.Drawing.Point(118, 45);
            this.TxtMin_Vo.Name = "TxtMin_Vo";
            this.TxtMin_Vo.Size = new System.Drawing.Size(159, 22);
            this.TxtMin_Vo.TabIndex = 2;
            this.TxtMin_Vo.Text = "0";
            // 
            // TxtMax_Qty
            // 
            this.TxtMax_Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMax_Qty.Location = new System.Drawing.Point(331, 16);
            this.TxtMax_Qty.Margin = new System.Windows.Forms.Padding(4);
            this.TxtMax_Qty.Name = "TxtMax_Qty";
            this.TxtMax_Qty.NumberDecimalDigits = 3;
            this.TxtMax_Qty.NumberDecimalSeparator = ".";
            this.TxtMax_Qty.NumberGroupSeparator = ",";
            this.TxtMax_Qty.Size = new System.Drawing.Size(159, 22);
            this.TxtMax_Qty.TabIndex = 1;
            this.TxtMax_Qty.Text = "0.000";
            // 
            // TxtMin_Qty
            // 
            this.TxtMin_Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMin_Qty.Location = new System.Drawing.Point(118, 16);
            this.TxtMin_Qty.Margin = new System.Windows.Forms.Padding(4);
            this.TxtMin_Qty.Name = "TxtMin_Qty";
            this.TxtMin_Qty.NumberDecimalDigits = 3;
            this.TxtMin_Qty.NumberDecimalSeparator = ".";
            this.TxtMin_Qty.NumberGroupSeparator = ",";
            this.TxtMin_Qty.Size = new System.Drawing.Size(159, 22);
            this.TxtMin_Qty.TabIndex = 0;
            this.TxtMin_Qty.Text = "0.000";
            // 
            // Gen_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 189);
            this.Controls.Add(this.TxtMax_Vo);
            this.Controls.Add(this.TxtMin_Vo);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.TxtMax_Qty);
            this.Controls.Add(this.TxtMin_Qty);
            this.Controls.Add(this.CboCatg_Id);
            this.Controls.Add(this.CboCurr_Id);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Gen_Search";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "392";
            this.Text = "Gen_Search";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Gen_Search_FormClosed);
            this.Load += new System.EventHandler(this.Gen_Search_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CboCurr_Id;
        private System.Windows.Forms.ComboBox CboCatg_Id;
        private System.Windows.Forms.Sample.DecimalTextBox TxtMin_Qty;
        private System.Windows.Forms.Sample.DecimalTextBox TxtMax_Qty;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private NumericTextBox TxtMin_Vo;
        private NumericTextBox TxtMax_Vo;
    }
}