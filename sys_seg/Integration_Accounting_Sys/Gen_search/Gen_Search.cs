﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Gen_Search : Form
    {
        #region PROPERTIES Used For Searching
        /// <summary> Below PRPERTIES Used For Searching At Gen_Search Form </summary>
        public static int For_Cur_ID { get; set; }
        public static decimal Min_Qty { get; set; }
        public static decimal Max_Qty { get; set; }
        public static int Min_Vo { get; set; }
        public static int Max_Vo { get; set; }
        public static int Catg_Id { get; set; }
        #endregion

        string Sql_Text = "";
        int Oper_Id;
        public Gen_Search(int OP_Id)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Oper_Id = OP_Id;
        }

        private void Gen_Search_Load(object sender, EventArgs e)
        {
            Sql_Text = " SELECT Cur_ID,Cur_aname,Cur_ename from CUR_Tbl "
                + " where Cur_id in(select Distinct for_Cur_Id from Voucher)"
                + " union "
                + " select 0 as cur_id,' (جميع العملات) ',' (All Currencies) ' order by " + (connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename");

            CboCurr_Id.DataSource = connection.SqlExec(Sql_Text, "SrchCurr_Tbl");
            CboCurr_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_aname" : "Cur_ename";
            CboCurr_Id.ValueMember = "Cur_Id";
            //...............................
            Sql_Text = "select Catg_Id,Catg_Aname,Catg_Ename from Catg_Tbl "
                        + " union "
                        + " select 0 as cur_id,' (جميع الجهات) ',' (All Categories) ' order by " + (connection.Lang_id == 1 ? "Catg_Aname" : "Catg_Ename");

            CboCatg_Id.DataSource = connection.SqlExec(Sql_Text, "Catg_Tbl");
            CboCatg_Id.DisplayMember = connection.Lang_id == 1 ? "CATG_ANAME" : "CATG_ENAME";
            CboCatg_Id.ValueMember = "CATG_ID";
            CboCatg_Id.Enabled = true;
        }
        //...............................
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //...............................
        private void Search_Btn_Click(object sender, EventArgs e)
        {
            For_Cur_ID = Convert.ToInt32(CboCurr_Id.SelectedValue);
            Min_Qty = Convert.ToDecimal(TxtMin_Qty.Text.Trim());
            Max_Qty = Convert.ToDecimal(TxtMax_Qty.Text.Trim());
            Min_Vo = Convert.ToInt32(TxtMin_Vo.Text.Trim());
            Max_Vo = Convert.ToInt32(TxtMax_Vo.Text.Trim());
            Catg_Id = Convert.ToInt16(CboCatg_Id.SelectedValue);
            this.Close();
        }

        private void Gen_Search_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "SrchCurr_Tbl", "Catg_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables[Tbl].Dispose();
                    connection.SQLDS.Tables[Tbl].Clear();
                }
            }
        }
    }
}
