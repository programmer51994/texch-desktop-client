﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Companies_Setting : Form
    {
        #region Declareations
        string Sql_test = "";
        string Sql_test1 = "";
        #endregion

        public Companies_Setting()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        }
        //----------------------------------------
        private void Companies_Settings_Load(object sender, EventArgs e)
        {
            Sql_test = " Select top(1) isnull(Dep_Rec_Setting,0) as Dep_Rec_Setting , Dep_Day ,Dep_Reord  , Dep_Cal_Method from Companies ";
            connection.SqlExec(Sql_test, "Setting_tbl");
            Cbo_Rec_Setting.SelectedIndex = Convert.ToInt16(connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Rec_Setting"]);
            Txt_Dep_day.Text = connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Day"].ToString();

            Check.Checked = Convert.ToBoolean(connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Reord"]);

            Sql_test1 = " Select  Method_Id, method_aname, method_ename from calc_method";
            Cbo_method.DataSource = connection.SqlExec(Sql_test1, "Method_tbl");
            Cbo_method.DisplayMember = (connection.Lang_id) == 1 ? "method_aname" : "method_ename";
            Cbo_method.ValueMember = "Method_Id";

            Cbo_method.SelectedValue = Convert.ToInt16(connection.SQLDS.Tables["Setting_tbl"].Rows[0]["Dep_Cal_Method"]);
            if (connection.Lang_id == 2)
            {
                Cbo_Rec_Setting.Items[0] = "Manually";
                Cbo_Rec_Setting.Items[1] = "Automatically";
            }

        }
        //----------------------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region Validation
            if (Txt_Dep_day.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " يجب كتابة تاريخ الاندثار اعتبار من  " : "Check The Date Please ", MyGeneral_Lib.LblCap);
                return;
            }
            #endregion

            connection.SQLCMD.Parameters.AddWithValue("@Dep_Cal_Method", Cbo_method.SelectedValue);// شهر سنة نصف سنة فصلي
            connection.SQLCMD.Parameters.AddWithValue("@Dep_Rec_Setting", Cbo_Rec_Setting.SelectedIndex);
            connection.SQLCMD.Parameters.AddWithValue("@Dep_Day", Convert.ToByte(Txt_Dep_day.Text));
            connection.SQLCMD.Parameters.AddWithValue("@Dep_Reord", Convert.ToInt16(Check.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Companies_Setting", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }
        //----------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}