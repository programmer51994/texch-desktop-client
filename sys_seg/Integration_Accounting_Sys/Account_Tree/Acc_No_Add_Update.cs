﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Acc_No_Add_Update : Form
    {
        #region MyRegion
        string Sql_Text = "";
        string Cond_Str = "";
        string MACC_NO_Dot = "";
        int MAcc_Parent = 0;
        int MAcc_op = 0;
        int Mform_flag = 0;
        int Macc_id = 0;
        byte MAcc_Level = 0;
        bool TxtChange = false;
        bool TxtChange_acc = false;
        string name = connection.Lang_id == 1 ? "a" : "e";
        BindingSource _BS_Add_Tree = new BindingSource();
        BindingSource _BS_Add= new BindingSource();
        public static int page_no = 0;
        #endregion

        public Acc_No_Add_Update(byte form_flag)
        {
            Mform_flag = form_flag;
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
           
            Grdacc_no.AutoGenerateColumns = false;
            Grdop_acc.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Cbo_class_id.Items.Clear();
                Cbo_class_id.Items.Add("Center");
                Cbo_class_id.Items.Add("Cent.&Branches");
                Grdacc_no.Columns["Column2"].DataPropertyName = "Acc_EName";
                Grdop_acc.Columns["Column9"].DataPropertyName = "Acc_EName";
              

            }
            #endregion
        }

        private void Acc_No_Add_Update_Load(object sender, EventArgs e)
        {
            TxtChange = false;
            TxtChange_acc = false;
            ////---------------------------
            Txt_AccName_A.Text = "";
            Txt_AccName_E.Text = "";
            Txt_ADesc.Text = "";
            Txt_EDesc.Text = "";
            TxtRef_acc.Text = "";
            TxtOp_acc.Text = "";
            Txt_AccSrch.Text = "";
            label18.Checked = false;
            label21.Checked = false;
            label20.Checked = false;
            label19.Checked = false;
            Grdacc_no.DataSource = null;
            Grdop_acc.DataSource = null;
            //if (Mform_flag == 1)

            //-----------------------
            Sql_Text = " SELECT  DC_Id, DC_Aname, DC_Ename FROM  Debt_Credit order by cast (DC_Id  as varchar(2))";
            Cbo_AccType.DataSource = connection.SqlExec(Sql_Text, "Debt_Credit");
            Cbo_AccType.ValueMember = "DC_Id";
            Cbo_AccType.DisplayMember = connection.Lang_id == 1 ? "DC_ANAME" : "DC_ENAME";
            //---------------------
            Cbo_class_id.SelectedIndex = 0;
            //-------------------

            ////----------------------
            if (Mform_flag == 2)
            {
                this.Text = connection.Lang_id == 1 ? "تعديــل الحســـاب" :"Update Account" ; 
                GetData_update();
            }
            else
            {
                TxtChange_acc = true;
                Txt_AccSrch_TextChanged(null, null);
            }

            TxtChange = true;

        }
        //-----------------------------------------
        private void GetData_Add(int flag, string acc_Namex)
        {
            if (TxtChange_acc)
            {
                int acc_id = 0;
                if (int.TryParse(acc_Namex, out acc_id))
                {
                    if (Mform_flag == 1)
                    {
                        Cond_Str = " where (Acc_ID like '" + acc_id + "%'"
                                   + " or "
                                   + "  Ref_Acc_No like '" + acc_id + "%')"
                                   + " and pacc_id  != 0 ";
                    }
                    else
                    {
                        Cond_Str = " where (Acc_ID like '" + acc_id + "'"
                                       + " or "
                                       + "  Ref_Acc_No like '" + acc_id + "')";
                    }
                }
                else
                {

                    Cond_Str = " where Acc_" + name + "Name like'" + acc_Namex + "%'" + " and pacc_id  != 0 "; ;
                }


                Sql_Text = " select  " + name + "_Description,acc_no,Acc_ID,Acc_" + name
                         + "Name, Ref_Acc_No,Acc_Level,Dot_Acc_No "
                         + " from Account_Tree " + Cond_Str;



                if (flag == 1)
                {


                    _BS_Add_Tree.DataSource = connection.SqlExec(Sql_Text, "Account_Tree");
                    Grdacc_no.DataSource = _BS_Add_Tree;
                    Txt_Desc_per.DataBindings.Clear();
                    Txt_Desc_per.DataBindings.Add("Text", _BS_Add_Tree, name + "_Description");


                }
                if (flag == 2)
                {
                    Sql_Text = Sql_Text + " and pacc_id  != 0 "; ;
                    _BS_Add.DataSource = connection.SqlExec(Sql_Text, "Account_Tree2");
                    Grdop_acc.DataSource = _BS_Add;

                }

            }
        }
        //---------------------------
        private void Txt_AccSrch_TextChanged(object sender, EventArgs e)
        {
            GetData_Add(1, Txt_AccSrch.Text.Trim());
        }
        //-----------------------------------------
        private void GetData_update()
        {
            TxtChange = true;
            DataRowView Drv = Account_Tree_Main._Bs_Main_Tree.Current as DataRowView;
            DataRow Dr = Drv.Row;
            Txt_AccName_A.Text = Dr.Field<string>("Acc_AName");
            Txt_AccName_E.Text = Dr.Field<string>("Acc_EName");
            Txt_ADesc.Text = Dr.Field<string>("A_Description");
            Txt_EDesc.Text = Dr.Field<string>("E_Description");
            TxtRef_acc.Text = Dr.Field<string>("Ref_Acc_No");
            Cbo_AccType.SelectedValue = Dr.Field<Int16>("DC_ID");
            Cbo_class_id.SelectedIndex = Dr.Field<byte>("ACC_CLASS");
            label21.Checked = Convert.ToBoolean(Dr.Field<byte>("sub_flag"));
            label20.Checked = Convert.ToBoolean(Dr.Field<byte>("EVAL_FLAG"));
            label19.Checked = Convert.ToBoolean(Dr.Field<byte>("RECORD_FLAG"));
            label18.Checked = Convert.ToBoolean(Dr.Field<byte>("control_sw"));
            Chk_Acc_Sett.Checked = Convert.ToBoolean(Dr.Field<byte>("Acc_Sett"));
            Txt_AccSrch.Text = Dr.Field<int>("PAcc_Id").ToString();
            TxtChange_acc = true;
            Txt_AccSrch_TextChanged(null, null);
            TxtOp_acc.Text = Dr.Field<int>("op_inv_acc").ToString();

            Sql_Text = " select " + name + "_Description as Description from Account_Tree where acc_no like '" + Txt_AccSrch.Text.Trim() + "'";
            Txt_Desc_per.DataBindings.Clear();
            Txt_Desc_per.DataBindings.Add("Text", connection.SqlExec(Sql_Text, "Account_Tree1"), "Description");
            //---------
            Macc_id = Dr.Field<Int32>("Acc_Id");
            
            Txt_AccSrch.Enabled = false;
        }
        //-----------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------
        private void TxtOp_acc_TextChanged(object sender, EventArgs e)
        {
            if (TxtChange)
            {
                string name = TxtOp_acc.Text == "" ? "  " : TxtOp_acc.Text.Trim();
                GetData_Add(2, name);
            }
        }
        //-----------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            /////--------------------------------
            MACC_NO_Dot = "";
            MAcc_Parent = 0;
            MAcc_op = 0;
            MAcc_Level = 0;
           //------------------
            if (connection.SQLDS.Tables.Contains("Account_Tree"))
            {
                if (connection.SQLDS.Tables["Account_Tree"].Rows.Count > 0)
                {
                    DataRowView Drvname = _BS_Add_Tree.Current as DataRowView;
                    DataRow Drname = Drvname.Row;
                    MAcc_Parent = Drname.Field<Int32>("acc_id");
                    MAcc_Level = Drname.Field<byte>("Acc_Level");
                    MACC_NO_Dot = Drname.Field<string>("Dot_Acc_No");
                }
            }
            //////--------------
            if (connection.SQLDS.Tables["Account_Tree"].Rows.Count == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء ادخال الحساب المتفرع من" : "Please Enter Sub_Account", MyGeneral_Lib.LblCap);
               
            }
            ////-----------------
            if (Txt_AccName_A.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "ادخل الاسم العربي" : "Please enter the Arabic Name", MyGeneral_Lib.LblCap);
                Txt_AccName_A.Focus();
                return;
            }

            if (Txt_AccName_E.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل الاسم الاجنبي" : "Please enter the English Name", MyGeneral_Lib.LblCap);
                Txt_AccName_E.Focus();
                return;
               
            }
            if (TxtRef_acc.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? " ادخل رقم المرجع " : "Please enter the Ref. Name", MyGeneral_Lib.LblCap);
                TxtRef_acc.Focus();
                return;
               
            }
            
            if (Chk_Acc_Sett.Checked == true &&  label19.Checked == false )
            {
                MessageBox.Show(connection.Lang_id == 1 ? "_(لا يمكن تفعيل تسوية محاسبية بأثر رجعي لان الحساب لا يمتلك خاصية _(يقبل الحركة" : "could not add account settlment because the account dose not alow movement", MyGeneral_Lib.LblCap);
                Txt_AccName_E.Focus();
                return;

            }
          
            //----------------------
            if (connection.SQLDS.Tables.Contains("Account_Tree2"))
            {
                if (connection.SQLDS.Tables["Account_Tree2"].Rows.Count > 0)
                {
                    DataRowView Drvname3 = _BS_Add.Current as DataRowView;
                    DataRow Drname3 = Drvname3.Row;
                    MAcc_op = Drname3.Field<int>("acc_id");
                }
            }

            
         
            
            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Txt_AccName_A.Text.Trim());
            ItemList.Insert(1, Txt_AccName_E.Text.Trim());
            ItemList.Insert(2, MAcc_Parent);
            ItemList.Insert(3, TxtRef_acc.Text.Trim());
            ItemList.Insert(4, Cbo_AccType.SelectedValue);
            ItemList.Insert(5, Txt_ADesc.Text.Trim());
            ItemList.Insert(6, Txt_EDesc.Text.Trim());
            ItemList.Insert(7, Cbo_class_id.SelectedIndex);
            ItemList.Insert(8, Convert.ToInt32(label21.Checked));
            ItemList.Insert(9, Convert.ToInt32(label18.Checked));
            ItemList.Insert(10, Convert.ToInt32(label19.Checked));
            ItemList.Insert(11, Convert.ToInt32(label20.Checked));
            ItemList.Insert(12, MAcc_op);
            ItemList.Insert(13, MAcc_Level);
            ItemList.Insert(14, connection.user_id);
            ItemList.Insert(15, "");
            ItemList.Insert(16, MACC_NO_Dot);
            ItemList.Insert(17, Mform_flag == 1 ? Macc_id = 0 : Macc_id);
            ItemList.Insert(18, Mform_flag);
            ItemList.Insert(19, Convert.ToInt32(Chk_Acc_Sett.Checked));
            connection.scalar("Add_Account_Tree", ItemList);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            connection.SQLCMD.Parameters.Clear();
           


            if (Mform_flag == 1)
            {
                TxtChange = false;
                string[] Used_Tbl = { "Account_Tree", "Account_Tree2", "Account_Tree3", "Account_Tree4" };
                foreach (string Tbl in Used_Tbl)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                    {
                        connection.SQLDS.Tables.Remove(Tbl);
                    }
                }
                Acc_No_Add_Update_Load(null, null);
                
            }
            else
            {
                this.Close();
            }



        }
        //-----------------------------------------
        private void Acc_No_Add_Update_FormClosed(object sender, FormClosedEventArgs e)
        {
            
           TxtChange_acc = false;
            TxtChange = false;
            
            string[] Used_Tbl = { "Account_Tree", "Account_Tree2", "Account_Tree3", "Account_Tree4" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Acc_No_Add_Update_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            page_no = 2;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}