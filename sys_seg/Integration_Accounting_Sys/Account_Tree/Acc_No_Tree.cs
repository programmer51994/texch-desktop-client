﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class Acc_No_Tree : Form
    {
        string Sql_Text = "";
        String Acc_No = "";
        BindingSource _BS_Tree = new BindingSource();
        public Acc_No_Tree()
        {

            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
           
        }

        private void Acc_No_Tree_Load(object sender, EventArgs e)
        {

            Sql_Text = " Exec [Account_tree_TreeView] ";
            DataTable Dt = connection.SqlExec(Sql_Text, "Tree_ftp");
            _BS_Tree.DataSource = connection.SqlExec(Sql_Text, "Tree_ftp");
            

            PopulateTreeView(treeView1.Nodes, 0, Dt);
            
        
        }
        protected void PopulateTreeView(TreeNodeCollection parentNode, int parentID, DataTable Dt)
        {
            foreach (DataRow folder in Dt.Rows)
            {
                if (Convert.ToInt32(folder["pacc_id"]) == parentID)
                {
                    String key = folder["Acc_no"].ToString();
                    String text = folder["Acc_no"].ToString() + "        " + folder[connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName"].ToString();
                    TreeNodeCollection newParentNode = parentNode.Add(key, text, key).Nodes;
                    PopulateTreeView(newParentNode, Convert.ToInt32(folder["acc_id"]), Dt);
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //Acc_No = treeView1.SelectedNode.ToString();
            //Acc_No = Acc_No.Substring(10, Acc_No.Length - 10);
            //Sub_end = Acc_No.IndexOf(' ');
            //Acc_No = Acc_No.Substring(0, Sub_end);

            Acc_No = treeView1.SelectedNode.ImageKey.ToString();
         
            try
            {
                _BS_Tree.DataSource = connection.SQLDS.Tables["Tree_ftp"].DefaultView.ToTable().Select("Acc_No ='" + Acc_No + "'").CopyToDataTable();
                Txt_Acc_Id.DataBindings.Add("text", _BS_Tree, "Acc_id");
                txtDe.DataBindings.Add("Text", _BS_Tree, connection.Lang_id == 1 ? "A_Description" : "E_Description");
                txtper_acc.DataBindings.Add("Text", _BS_Tree, "dep_per");
                TxtOp_acc_id.DataBindings.Add("Text", _BS_Tree, "op_inv_acc");
                Txtdb_acc_id.DataBindings.Add("Text", _BS_Tree, "db_dep_acc");
                Txtcd_acc_id.DataBindings.Add("Text", _BS_Tree, "cd_dep_acc");
                label18.DataBindings.Add("Checked", _BS_Tree, "control_sw");
                label21.DataBindings.Add("Checked", _BS_Tree, "sub_flag");
                label20.DataBindings.Add("Checked", _BS_Tree, "EVAL_FLAG");
                label19.DataBindings.Add("Checked", _BS_Tree, "RECORD_FLAG");
                TxtOp_acc.DataBindings.Add("Text", _BS_Tree, "op_inv_acc_AName");
                Txtdb_acc.DataBindings.Add("Text", _BS_Tree, "db_dep_acc_Aname");
                Txtcd_acc.DataBindings.Add("Text", _BS_Tree, "cd_dep_acc_Aname");
                Chk_Acc_Sett.DataBindings.Add("Checked", _BS_Tree, "Acc_Sett");
                
            }
            catch 
            {
            }

          
        }

        private void Acc_No_Tree_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            string[] Used_Tbl = { "Tree_ftp"  };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

      
        private void Btn_Add_Click(object sender, EventArgs e)
        {
             //public RptLang_MsgBox(ReportClass RPT, ReportClass RPTeng, bool Print_Flag = false, bool Pdf_Flag = false, bool Excel_Flag = false, DataTable  DT = null, DataGridView[] Grd = null, DataTable[] EXP_DT = null, bool Visible_Flag = false, bool isTerminal = false, String TitleName = "")
            Account_Tree_ViewRep ObjRpt = new Account_Tree_ViewRep();
            Account_Tree_ViewRepEng ObjRpteng = new Account_Tree_ViewRepEng();
            DataTable DT = new DataTable();
            DT = CustomControls.CustomParam_Dt();
            DT.Rows.Add("User_Name", TxtUser.Text);
            RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRpteng, true, true, false, DT);
            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;
        
        }

        private void Acc_No_Tree_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

       
    }
}