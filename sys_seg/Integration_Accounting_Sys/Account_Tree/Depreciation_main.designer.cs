﻿namespace Integration_Accounting_Sys
{
    partial class Depreciation_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Depreciation_main));
            this.Dep_RD = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GrdAccount_Tree = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BND = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnUpd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.Btn_PrintAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.SearchBtn = new System.Windows.Forms.ToolStripLabel();
            this.Txt_Acc = new System.Windows.Forms.ToolStripTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel58 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel59 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel60 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel123 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel124 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel125 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel126 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel127 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel128 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel129 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel130 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel131 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel132 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel133 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel134 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel135 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel136 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel137 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel138 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel139 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel140 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel141 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel142 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel143 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel144 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel145 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel146 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel147 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel148 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel149 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel150 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel151 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel152 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel153 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel154 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel155 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel156 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel157 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel158 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel159 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel160 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel161 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel162 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel163 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel164 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel165 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel166 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel167 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel168 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel169 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel170 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel171 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel172 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel173 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel174 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel175 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel176 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel177 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel178 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel179 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel180 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel181 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel182 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel183 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel184 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel185 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel186 = new System.Windows.Forms.FlowLayoutPanel();
            this.Txt_Desc_per = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel61 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel62 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel63 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel64 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel65 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel66 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel67 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel68 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel69 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel70 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel71 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel72 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel73 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel74 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel75 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel76 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel77 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel78 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel79 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel80 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel81 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel82 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel83 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel84 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel85 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel86 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel87 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel88 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel89 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel90 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel91 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel92 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel93 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel94 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel95 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel96 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel97 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel98 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel99 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel100 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel101 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel102 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel103 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel104 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel105 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel106 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel107 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel108 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel109 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel110 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel111 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel112 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel113 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel114 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel115 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel116 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel117 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel118 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel119 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel120 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel121 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel122 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtper_acc = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAccount_Tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).BeginInit();
            this.BND.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel16.SuspendLayout();
            this.flowLayoutPanel21.SuspendLayout();
            this.flowLayoutPanel26.SuspendLayout();
            this.flowLayoutPanel31.SuspendLayout();
            this.flowLayoutPanel36.SuspendLayout();
            this.flowLayoutPanel41.SuspendLayout();
            this.flowLayoutPanel46.SuspendLayout();
            this.flowLayoutPanel51.SuspendLayout();
            this.flowLayoutPanel56.SuspendLayout();
            this.flowLayoutPanel123.SuspendLayout();
            this.flowLayoutPanel125.SuspendLayout();
            this.flowLayoutPanel130.SuspendLayout();
            this.flowLayoutPanel135.SuspendLayout();
            this.flowLayoutPanel140.SuspendLayout();
            this.flowLayoutPanel145.SuspendLayout();
            this.flowLayoutPanel150.SuspendLayout();
            this.flowLayoutPanel155.SuspendLayout();
            this.flowLayoutPanel160.SuspendLayout();
            this.flowLayoutPanel165.SuspendLayout();
            this.flowLayoutPanel170.SuspendLayout();
            this.flowLayoutPanel175.SuspendLayout();
            this.flowLayoutPanel180.SuspendLayout();
            this.flowLayoutPanel185.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel61.SuspendLayout();
            this.flowLayoutPanel66.SuspendLayout();
            this.flowLayoutPanel71.SuspendLayout();
            this.flowLayoutPanel76.SuspendLayout();
            this.flowLayoutPanel81.SuspendLayout();
            this.flowLayoutPanel86.SuspendLayout();
            this.flowLayoutPanel91.SuspendLayout();
            this.flowLayoutPanel96.SuspendLayout();
            this.flowLayoutPanel101.SuspendLayout();
            this.flowLayoutPanel106.SuspendLayout();
            this.flowLayoutPanel111.SuspendLayout();
            this.flowLayoutPanel116.SuspendLayout();
            this.flowLayoutPanel121.SuspendLayout();
            this.SuspendLayout();
            // 
            // Dep_RD
            // 
            this.Dep_RD.AutoSize = true;
            this.Dep_RD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Dep_RD.Enabled = false;
            this.Dep_RD.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Dep_RD.ForeColor = System.Drawing.Color.Navy;
            this.Dep_RD.Location = new System.Drawing.Point(534, 308);
            this.Dep_RD.Name = "Dep_RD";
            this.Dep_RD.Size = new System.Drawing.Size(62, 20);
            this.Dep_RD.TabIndex = 3;
            this.Dep_RD.Text = "اندثـــار";
            this.Dep_RD.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(293, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 16);
            this.label7.TabIndex = 693;
            this.label7.Text = "%";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(4, 310);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 16);
            this.label1.TabIndex = 692;
            this.label1.Text = "نسبــة الاندثــار السنــــــــــــوي :";
            // 
            // GrdAccount_Tree
            // 
            this.GrdAccount_Tree.AllowUserToAddRows = false;
            this.GrdAccount_Tree.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAccount_Tree.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdAccount_Tree.CausesValidation = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAccount_Tree.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GrdAccount_Tree.ColumnHeadersHeight = 40;
            this.GrdAccount_Tree.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdAccount_Tree.DefaultCellStyle = dataGridViewCellStyle3;
            this.GrdAccount_Tree.Location = new System.Drawing.Point(5, 39);
            this.GrdAccount_Tree.Name = "GrdAccount_Tree";
            this.GrdAccount_Tree.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAccount_Tree.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAccount_Tree.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.GrdAccount_Tree.Size = new System.Drawing.Size(795, 248);
            this.GrdAccount_Tree.TabIndex = 1;
            this.GrdAccount_Tree.SelectionChanged += new System.EventHandler(this.GrdAccount_Tree_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ACC_ID";
            this.Column1.HeaderText = "رقم الحساب ";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Acc_AName";
            this.Column2.HeaderText = "الاسم";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = " Ref_Acc_No";
            this.Column3.HeaderText = "رقم المرجع";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "db_dep_acc";
            this.Column4.HeaderText = "رقم الحساب المدين";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "db_dep_acc_Aname";
            this.Column5.HeaderText = "الاسم";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 200;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "cd_dep_acc";
            this.Column6.HeaderText = "رقم الحساب الدائن";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "cd_dep_acc_Aname";
            this.Column7.HeaderText = "الاسم";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 200;
            // 
            // BND
            // 
            this.BND.AddNewItem = null;
            this.BND.AllowItemReorder = true;
            this.BND.AllowMerge = false;
            this.BND.AutoSize = false;
            this.BND.CountItem = null;
            this.BND.DeleteItem = null;
            this.BND.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnAdd,
            this.toolStripSeparator3,
            this.BtnUpd,
            this.toolStripSeparator2,
            this.AllBtn,
            this.toolStripSeparator4,
            this.Btn_PrintAll,
            this.toolStripSeparator7,
            this.SearchBtn,
            this.Txt_Acc});
            this.BND.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.BND.Location = new System.Drawing.Point(0, 0);
            this.BND.MoveFirstItem = null;
            this.BND.MoveLastItem = null;
            this.BND.MoveNextItem = null;
            this.BND.MovePreviousItem = null;
            this.BND.Name = "BND";
            this.BND.PositionItem = null;
            this.BND.Size = new System.Drawing.Size(801, 30);
            this.BND.TabIndex = 0;
            this.BND.Text = "BND";
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("BtnAdd.Image")));
            this.BtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(80, 27);
            this.BtnAdd.Tag = "2";
            this.BtnAdd.Text = "اضافة جديد";
            this.BtnAdd.ToolTipText = "F9";
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // BtnUpd
            // 
            this.BtnUpd.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.BtnUpd.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpd.Image")));
            this.BtnUpd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnUpd.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.BtnUpd.Name = "BtnUpd";
            this.BtnUpd.Size = new System.Drawing.Size(60, 27);
            this.BtnUpd.Tag = "3";
            this.BtnUpd.Text = "تعـديــل";
            this.BtnUpd.ToolTipText = "F7";
            this.BtnUpd.Click += new System.EventHandler(this.BtnUpd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // AllBtn
            // 
            this.AllBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.AllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AllBtn.Image")));
            this.AllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AllBtn.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(84, 27);
            this.AllBtn.Tag = "4";
            this.AllBtn.Text = "عرض الكـل";
            this.AllBtn.ToolTipText = "F4";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // Btn_PrintAll
            // 
            this.Btn_PrintAll.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_PrintAll.Image = ((System.Drawing.Image)(resources.GetObject("Btn_PrintAll.Image")));
            this.Btn_PrintAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Btn_PrintAll.Name = "Btn_PrintAll";
            this.Btn_PrintAll.Size = new System.Drawing.Size(70, 27);
            this.Btn_PrintAll.Text = "طبــــاعـة";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Margin = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.Image = ((System.Drawing.Image)(resources.GetObject("SearchBtn.Image")));
            this.SearchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(135, 27);
            this.SearchBtn.Tag = "8";
            this.SearchBtn.Text = "بـحــث رقم/اسم الحساب";
            this.SearchBtn.ToolTipText = "F5";
            // 
            // Txt_Acc
            // 
            this.Txt_Acc.Name = "Txt_Acc";
            this.Txt_Acc.Size = new System.Drawing.Size(200, 30);
            this.Txt_Acc.TextChanged += new System.EventHandler(this.Txt_Acc_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel31);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel123);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-2, 334);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(811, 1);
            this.flowLayoutPanel1.TabIndex = 707;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(804, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel2.TabIndex = 634;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(796, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel3.TabIndex = 633;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(27, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel4.TabIndex = 632;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(43, 241);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel5.TabIndex = 631;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(198, 249);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel6.TabIndex = 676;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel7.TabIndex = 634;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel8.TabIndex = 633;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel9.TabIndex = 632;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel10.TabIndex = 631;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel11.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel11.TabIndex = 676;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel12.TabIndex = 634;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel13.TabIndex = 633;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel14.TabIndex = 632;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel15.TabIndex = 631;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel17);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel18);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel20);
            this.flowLayoutPanel16.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(198, 256);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel16.TabIndex = 677;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel17.TabIndex = 634;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel18.TabIndex = 633;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel19.TabIndex = 632;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel20.TabIndex = 631;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel22);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel23);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel25);
            this.flowLayoutPanel21.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel21.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel21.TabIndex = 676;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel22.TabIndex = 634;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel23.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel23.TabIndex = 633;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel24.TabIndex = 632;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel25.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel25.TabIndex = 631;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel28);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel30);
            this.flowLayoutPanel26.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel26.TabIndex = 676;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel27.TabIndex = 634;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel28.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel28.TabIndex = 633;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel29.TabIndex = 632;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel30.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel30.TabIndex = 631;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel31.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel31.Location = new System.Drawing.Point(198, 263);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel31.TabIndex = 678;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel32.TabIndex = 634;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel33.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel33.TabIndex = 633;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel34.TabIndex = 632;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel35.TabIndex = 631;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel36.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel36.TabIndex = 676;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel37.TabIndex = 634;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel38.TabIndex = 633;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel39.TabIndex = 632;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel40.TabIndex = 631;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel41.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel41.TabIndex = 676;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel42.TabIndex = 634;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel43.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel43.TabIndex = 633;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel44.TabIndex = 632;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel45.TabIndex = 631;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel49);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel46.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel46.TabIndex = 677;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel47.TabIndex = 634;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel48.TabIndex = 633;
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel49.TabIndex = 632;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel50.TabIndex = 631;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel51.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel51.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel51.TabIndex = 676;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel52.TabIndex = 634;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel53.TabIndex = 633;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel54.TabIndex = 632;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel55.TabIndex = 631;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel58);
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel59);
            this.flowLayoutPanel56.Controls.Add(this.flowLayoutPanel60);
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel56.TabIndex = 676;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel57.TabIndex = 634;
            // 
            // flowLayoutPanel58
            // 
            this.flowLayoutPanel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel58.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel58.Name = "flowLayoutPanel58";
            this.flowLayoutPanel58.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel58.TabIndex = 633;
            // 
            // flowLayoutPanel59
            // 
            this.flowLayoutPanel59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel59.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel59.Name = "flowLayoutPanel59";
            this.flowLayoutPanel59.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel59.TabIndex = 632;
            // 
            // flowLayoutPanel60
            // 
            this.flowLayoutPanel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel60.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel60.Name = "flowLayoutPanel60";
            this.flowLayoutPanel60.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel60.TabIndex = 631;
            // 
            // flowLayoutPanel123
            // 
            this.flowLayoutPanel123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel123.Controls.Add(this.flowLayoutPanel124);
            this.flowLayoutPanel123.Location = new System.Drawing.Point(190, 263);
            this.flowLayoutPanel123.Name = "flowLayoutPanel123";
            this.flowLayoutPanel123.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel123.TabIndex = 679;
            // 
            // flowLayoutPanel124
            // 
            this.flowLayoutPanel124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel124.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel124.Name = "flowLayoutPanel124";
            this.flowLayoutPanel124.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel124.TabIndex = 632;
            // 
            // flowLayoutPanel125
            // 
            this.flowLayoutPanel125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel126);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel127);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel128);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel129);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel130);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel140);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel155);
            this.flowLayoutPanel125.Controls.Add(this.flowLayoutPanel185);
            this.flowLayoutPanel125.Location = new System.Drawing.Point(1, 32);
            this.flowLayoutPanel125.Name = "flowLayoutPanel125";
            this.flowLayoutPanel125.Size = new System.Drawing.Size(799, 1);
            this.flowLayoutPanel125.TabIndex = 708;
            // 
            // flowLayoutPanel126
            // 
            this.flowLayoutPanel126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel126.Location = new System.Drawing.Point(792, 3);
            this.flowLayoutPanel126.Name = "flowLayoutPanel126";
            this.flowLayoutPanel126.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel126.TabIndex = 634;
            // 
            // flowLayoutPanel127
            // 
            this.flowLayoutPanel127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel127.Location = new System.Drawing.Point(784, 3);
            this.flowLayoutPanel127.Name = "flowLayoutPanel127";
            this.flowLayoutPanel127.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel127.TabIndex = 633;
            // 
            // flowLayoutPanel128
            // 
            this.flowLayoutPanel128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel128.Location = new System.Drawing.Point(15, 3);
            this.flowLayoutPanel128.Name = "flowLayoutPanel128";
            this.flowLayoutPanel128.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel128.TabIndex = 632;
            // 
            // flowLayoutPanel129
            // 
            this.flowLayoutPanel129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel129.Location = new System.Drawing.Point(31, 241);
            this.flowLayoutPanel129.Name = "flowLayoutPanel129";
            this.flowLayoutPanel129.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel129.TabIndex = 631;
            // 
            // flowLayoutPanel130
            // 
            this.flowLayoutPanel130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel131);
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel132);
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel133);
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel134);
            this.flowLayoutPanel130.Controls.Add(this.flowLayoutPanel135);
            this.flowLayoutPanel130.Location = new System.Drawing.Point(186, 249);
            this.flowLayoutPanel130.Name = "flowLayoutPanel130";
            this.flowLayoutPanel130.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel130.TabIndex = 676;
            // 
            // flowLayoutPanel131
            // 
            this.flowLayoutPanel131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel131.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel131.Name = "flowLayoutPanel131";
            this.flowLayoutPanel131.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel131.TabIndex = 634;
            // 
            // flowLayoutPanel132
            // 
            this.flowLayoutPanel132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel132.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel132.Name = "flowLayoutPanel132";
            this.flowLayoutPanel132.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel132.TabIndex = 633;
            // 
            // flowLayoutPanel133
            // 
            this.flowLayoutPanel133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel133.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel133.Name = "flowLayoutPanel133";
            this.flowLayoutPanel133.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel133.TabIndex = 632;
            // 
            // flowLayoutPanel134
            // 
            this.flowLayoutPanel134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel134.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel134.Name = "flowLayoutPanel134";
            this.flowLayoutPanel134.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel134.TabIndex = 631;
            // 
            // flowLayoutPanel135
            // 
            this.flowLayoutPanel135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel136);
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel137);
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel138);
            this.flowLayoutPanel135.Controls.Add(this.flowLayoutPanel139);
            this.flowLayoutPanel135.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel135.Name = "flowLayoutPanel135";
            this.flowLayoutPanel135.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel135.TabIndex = 676;
            // 
            // flowLayoutPanel136
            // 
            this.flowLayoutPanel136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel136.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel136.Name = "flowLayoutPanel136";
            this.flowLayoutPanel136.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel136.TabIndex = 634;
            // 
            // flowLayoutPanel137
            // 
            this.flowLayoutPanel137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel137.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel137.Name = "flowLayoutPanel137";
            this.flowLayoutPanel137.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel137.TabIndex = 633;
            // 
            // flowLayoutPanel138
            // 
            this.flowLayoutPanel138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel138.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel138.Name = "flowLayoutPanel138";
            this.flowLayoutPanel138.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel138.TabIndex = 632;
            // 
            // flowLayoutPanel139
            // 
            this.flowLayoutPanel139.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel139.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel139.Name = "flowLayoutPanel139";
            this.flowLayoutPanel139.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel139.TabIndex = 631;
            // 
            // flowLayoutPanel140
            // 
            this.flowLayoutPanel140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel140.Controls.Add(this.flowLayoutPanel141);
            this.flowLayoutPanel140.Controls.Add(this.flowLayoutPanel142);
            this.flowLayoutPanel140.Controls.Add(this.flowLayoutPanel143);
            this.flowLayoutPanel140.Controls.Add(this.flowLayoutPanel144);
            this.flowLayoutPanel140.Controls.Add(this.flowLayoutPanel145);
            this.flowLayoutPanel140.Location = new System.Drawing.Point(186, 256);
            this.flowLayoutPanel140.Name = "flowLayoutPanel140";
            this.flowLayoutPanel140.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel140.TabIndex = 677;
            // 
            // flowLayoutPanel141
            // 
            this.flowLayoutPanel141.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel141.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel141.Name = "flowLayoutPanel141";
            this.flowLayoutPanel141.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel141.TabIndex = 634;
            // 
            // flowLayoutPanel142
            // 
            this.flowLayoutPanel142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel142.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel142.Name = "flowLayoutPanel142";
            this.flowLayoutPanel142.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel142.TabIndex = 633;
            // 
            // flowLayoutPanel143
            // 
            this.flowLayoutPanel143.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel143.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel143.Name = "flowLayoutPanel143";
            this.flowLayoutPanel143.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel143.TabIndex = 632;
            // 
            // flowLayoutPanel144
            // 
            this.flowLayoutPanel144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel144.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel144.Name = "flowLayoutPanel144";
            this.flowLayoutPanel144.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel144.TabIndex = 631;
            // 
            // flowLayoutPanel145
            // 
            this.flowLayoutPanel145.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel146);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel147);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel148);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel149);
            this.flowLayoutPanel145.Controls.Add(this.flowLayoutPanel150);
            this.flowLayoutPanel145.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel145.Name = "flowLayoutPanel145";
            this.flowLayoutPanel145.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel145.TabIndex = 676;
            // 
            // flowLayoutPanel146
            // 
            this.flowLayoutPanel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel146.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel146.Name = "flowLayoutPanel146";
            this.flowLayoutPanel146.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel146.TabIndex = 634;
            // 
            // flowLayoutPanel147
            // 
            this.flowLayoutPanel147.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel147.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel147.Name = "flowLayoutPanel147";
            this.flowLayoutPanel147.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel147.TabIndex = 633;
            // 
            // flowLayoutPanel148
            // 
            this.flowLayoutPanel148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel148.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel148.Name = "flowLayoutPanel148";
            this.flowLayoutPanel148.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel148.TabIndex = 632;
            // 
            // flowLayoutPanel149
            // 
            this.flowLayoutPanel149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel149.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel149.Name = "flowLayoutPanel149";
            this.flowLayoutPanel149.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel149.TabIndex = 631;
            // 
            // flowLayoutPanel150
            // 
            this.flowLayoutPanel150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel150.Controls.Add(this.flowLayoutPanel151);
            this.flowLayoutPanel150.Controls.Add(this.flowLayoutPanel152);
            this.flowLayoutPanel150.Controls.Add(this.flowLayoutPanel153);
            this.flowLayoutPanel150.Controls.Add(this.flowLayoutPanel154);
            this.flowLayoutPanel150.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel150.Name = "flowLayoutPanel150";
            this.flowLayoutPanel150.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel150.TabIndex = 676;
            // 
            // flowLayoutPanel151
            // 
            this.flowLayoutPanel151.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel151.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel151.Name = "flowLayoutPanel151";
            this.flowLayoutPanel151.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel151.TabIndex = 634;
            // 
            // flowLayoutPanel152
            // 
            this.flowLayoutPanel152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel152.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel152.Name = "flowLayoutPanel152";
            this.flowLayoutPanel152.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel152.TabIndex = 633;
            // 
            // flowLayoutPanel153
            // 
            this.flowLayoutPanel153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel153.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel153.Name = "flowLayoutPanel153";
            this.flowLayoutPanel153.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel153.TabIndex = 632;
            // 
            // flowLayoutPanel154
            // 
            this.flowLayoutPanel154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel154.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel154.Name = "flowLayoutPanel154";
            this.flowLayoutPanel154.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel154.TabIndex = 631;
            // 
            // flowLayoutPanel155
            // 
            this.flowLayoutPanel155.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel156);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel157);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel158);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel159);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel160);
            this.flowLayoutPanel155.Controls.Add(this.flowLayoutPanel170);
            this.flowLayoutPanel155.Location = new System.Drawing.Point(186, 263);
            this.flowLayoutPanel155.Name = "flowLayoutPanel155";
            this.flowLayoutPanel155.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel155.TabIndex = 678;
            // 
            // flowLayoutPanel156
            // 
            this.flowLayoutPanel156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel156.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel156.Name = "flowLayoutPanel156";
            this.flowLayoutPanel156.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel156.TabIndex = 634;
            // 
            // flowLayoutPanel157
            // 
            this.flowLayoutPanel157.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel157.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel157.Name = "flowLayoutPanel157";
            this.flowLayoutPanel157.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel157.TabIndex = 633;
            // 
            // flowLayoutPanel158
            // 
            this.flowLayoutPanel158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel158.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel158.Name = "flowLayoutPanel158";
            this.flowLayoutPanel158.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel158.TabIndex = 632;
            // 
            // flowLayoutPanel159
            // 
            this.flowLayoutPanel159.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel159.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel159.Name = "flowLayoutPanel159";
            this.flowLayoutPanel159.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel159.TabIndex = 631;
            // 
            // flowLayoutPanel160
            // 
            this.flowLayoutPanel160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel160.Controls.Add(this.flowLayoutPanel161);
            this.flowLayoutPanel160.Controls.Add(this.flowLayoutPanel162);
            this.flowLayoutPanel160.Controls.Add(this.flowLayoutPanel163);
            this.flowLayoutPanel160.Controls.Add(this.flowLayoutPanel164);
            this.flowLayoutPanel160.Controls.Add(this.flowLayoutPanel165);
            this.flowLayoutPanel160.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel160.Name = "flowLayoutPanel160";
            this.flowLayoutPanel160.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel160.TabIndex = 676;
            // 
            // flowLayoutPanel161
            // 
            this.flowLayoutPanel161.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel161.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel161.Name = "flowLayoutPanel161";
            this.flowLayoutPanel161.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel161.TabIndex = 634;
            // 
            // flowLayoutPanel162
            // 
            this.flowLayoutPanel162.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel162.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel162.Name = "flowLayoutPanel162";
            this.flowLayoutPanel162.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel162.TabIndex = 633;
            // 
            // flowLayoutPanel163
            // 
            this.flowLayoutPanel163.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel163.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel163.Name = "flowLayoutPanel163";
            this.flowLayoutPanel163.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel163.TabIndex = 632;
            // 
            // flowLayoutPanel164
            // 
            this.flowLayoutPanel164.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel164.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel164.Name = "flowLayoutPanel164";
            this.flowLayoutPanel164.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel164.TabIndex = 631;
            // 
            // flowLayoutPanel165
            // 
            this.flowLayoutPanel165.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel165.Controls.Add(this.flowLayoutPanel166);
            this.flowLayoutPanel165.Controls.Add(this.flowLayoutPanel167);
            this.flowLayoutPanel165.Controls.Add(this.flowLayoutPanel168);
            this.flowLayoutPanel165.Controls.Add(this.flowLayoutPanel169);
            this.flowLayoutPanel165.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel165.Name = "flowLayoutPanel165";
            this.flowLayoutPanel165.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel165.TabIndex = 676;
            // 
            // flowLayoutPanel166
            // 
            this.flowLayoutPanel166.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel166.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel166.Name = "flowLayoutPanel166";
            this.flowLayoutPanel166.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel166.TabIndex = 634;
            // 
            // flowLayoutPanel167
            // 
            this.flowLayoutPanel167.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel167.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel167.Name = "flowLayoutPanel167";
            this.flowLayoutPanel167.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel167.TabIndex = 633;
            // 
            // flowLayoutPanel168
            // 
            this.flowLayoutPanel168.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel168.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel168.Name = "flowLayoutPanel168";
            this.flowLayoutPanel168.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel168.TabIndex = 632;
            // 
            // flowLayoutPanel169
            // 
            this.flowLayoutPanel169.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel169.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel169.Name = "flowLayoutPanel169";
            this.flowLayoutPanel169.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel169.TabIndex = 631;
            // 
            // flowLayoutPanel170
            // 
            this.flowLayoutPanel170.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel170.Controls.Add(this.flowLayoutPanel171);
            this.flowLayoutPanel170.Controls.Add(this.flowLayoutPanel172);
            this.flowLayoutPanel170.Controls.Add(this.flowLayoutPanel173);
            this.flowLayoutPanel170.Controls.Add(this.flowLayoutPanel174);
            this.flowLayoutPanel170.Controls.Add(this.flowLayoutPanel175);
            this.flowLayoutPanel170.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel170.Name = "flowLayoutPanel170";
            this.flowLayoutPanel170.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel170.TabIndex = 677;
            // 
            // flowLayoutPanel171
            // 
            this.flowLayoutPanel171.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel171.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel171.Name = "flowLayoutPanel171";
            this.flowLayoutPanel171.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel171.TabIndex = 634;
            // 
            // flowLayoutPanel172
            // 
            this.flowLayoutPanel172.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel172.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel172.Name = "flowLayoutPanel172";
            this.flowLayoutPanel172.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel172.TabIndex = 633;
            // 
            // flowLayoutPanel173
            // 
            this.flowLayoutPanel173.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel173.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel173.Name = "flowLayoutPanel173";
            this.flowLayoutPanel173.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel173.TabIndex = 632;
            // 
            // flowLayoutPanel174
            // 
            this.flowLayoutPanel174.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel174.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel174.Name = "flowLayoutPanel174";
            this.flowLayoutPanel174.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel174.TabIndex = 631;
            // 
            // flowLayoutPanel175
            // 
            this.flowLayoutPanel175.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel175.Controls.Add(this.flowLayoutPanel176);
            this.flowLayoutPanel175.Controls.Add(this.flowLayoutPanel177);
            this.flowLayoutPanel175.Controls.Add(this.flowLayoutPanel178);
            this.flowLayoutPanel175.Controls.Add(this.flowLayoutPanel179);
            this.flowLayoutPanel175.Controls.Add(this.flowLayoutPanel180);
            this.flowLayoutPanel175.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel175.Name = "flowLayoutPanel175";
            this.flowLayoutPanel175.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel175.TabIndex = 676;
            // 
            // flowLayoutPanel176
            // 
            this.flowLayoutPanel176.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel176.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel176.Name = "flowLayoutPanel176";
            this.flowLayoutPanel176.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel176.TabIndex = 634;
            // 
            // flowLayoutPanel177
            // 
            this.flowLayoutPanel177.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel177.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel177.Name = "flowLayoutPanel177";
            this.flowLayoutPanel177.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel177.TabIndex = 633;
            // 
            // flowLayoutPanel178
            // 
            this.flowLayoutPanel178.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel178.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel178.Name = "flowLayoutPanel178";
            this.flowLayoutPanel178.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel178.TabIndex = 632;
            // 
            // flowLayoutPanel179
            // 
            this.flowLayoutPanel179.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel179.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel179.Name = "flowLayoutPanel179";
            this.flowLayoutPanel179.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel179.TabIndex = 631;
            // 
            // flowLayoutPanel180
            // 
            this.flowLayoutPanel180.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel180.Controls.Add(this.flowLayoutPanel181);
            this.flowLayoutPanel180.Controls.Add(this.flowLayoutPanel182);
            this.flowLayoutPanel180.Controls.Add(this.flowLayoutPanel183);
            this.flowLayoutPanel180.Controls.Add(this.flowLayoutPanel184);
            this.flowLayoutPanel180.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel180.Name = "flowLayoutPanel180";
            this.flowLayoutPanel180.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel180.TabIndex = 676;
            // 
            // flowLayoutPanel181
            // 
            this.flowLayoutPanel181.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel181.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel181.Name = "flowLayoutPanel181";
            this.flowLayoutPanel181.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel181.TabIndex = 634;
            // 
            // flowLayoutPanel182
            // 
            this.flowLayoutPanel182.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel182.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel182.Name = "flowLayoutPanel182";
            this.flowLayoutPanel182.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel182.TabIndex = 633;
            // 
            // flowLayoutPanel183
            // 
            this.flowLayoutPanel183.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel183.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel183.Name = "flowLayoutPanel183";
            this.flowLayoutPanel183.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel183.TabIndex = 632;
            // 
            // flowLayoutPanel184
            // 
            this.flowLayoutPanel184.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel184.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel184.Name = "flowLayoutPanel184";
            this.flowLayoutPanel184.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel184.TabIndex = 631;
            // 
            // flowLayoutPanel185
            // 
            this.flowLayoutPanel185.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel185.Controls.Add(this.flowLayoutPanel186);
            this.flowLayoutPanel185.Location = new System.Drawing.Point(178, 263);
            this.flowLayoutPanel185.Name = "flowLayoutPanel185";
            this.flowLayoutPanel185.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel185.TabIndex = 679;
            // 
            // flowLayoutPanel186
            // 
            this.flowLayoutPanel186.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel186.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel186.Name = "flowLayoutPanel186";
            this.flowLayoutPanel186.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel186.TabIndex = 632;
            // 
            // Txt_Desc_per
            // 
            this.Txt_Desc_per.BackColor = System.Drawing.Color.White;
            this.Txt_Desc_per.Enabled = false;
            this.Txt_Desc_per.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Desc_per.Location = new System.Drawing.Point(137, 343);
            this.Txt_Desc_per.Multiline = true;
            this.Txt_Desc_per.Name = "Txt_Desc_per";
            this.Txt_Desc_per.ReadOnly = true;
            this.Txt_Desc_per.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_Desc_per.Size = new System.Drawing.Size(653, 60);
            this.Txt_Desc_per.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(-2, 341);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 16);
            this.label2.TabIndex = 710;
            this.label2.Text = "وصـــــــف الحســــــــاب :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 412);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(801, 22);
            this.statusStrip1.TabIndex = 711;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // flowLayoutPanel61
            // 
            this.flowLayoutPanel61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel62);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel63);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel64);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel65);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel66);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel76);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel91);
            this.flowLayoutPanel61.Controls.Add(this.flowLayoutPanel121);
            this.flowLayoutPanel61.Location = new System.Drawing.Point(-1, 295);
            this.flowLayoutPanel61.Name = "flowLayoutPanel61";
            this.flowLayoutPanel61.Size = new System.Drawing.Size(817, 1);
            this.flowLayoutPanel61.TabIndex = 712;
            // 
            // flowLayoutPanel62
            // 
            this.flowLayoutPanel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel62.Location = new System.Drawing.Point(810, 3);
            this.flowLayoutPanel62.Name = "flowLayoutPanel62";
            this.flowLayoutPanel62.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel62.TabIndex = 634;
            // 
            // flowLayoutPanel63
            // 
            this.flowLayoutPanel63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel63.Location = new System.Drawing.Point(802, 3);
            this.flowLayoutPanel63.Name = "flowLayoutPanel63";
            this.flowLayoutPanel63.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel63.TabIndex = 633;
            // 
            // flowLayoutPanel64
            // 
            this.flowLayoutPanel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel64.Location = new System.Drawing.Point(33, 3);
            this.flowLayoutPanel64.Name = "flowLayoutPanel64";
            this.flowLayoutPanel64.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel64.TabIndex = 632;
            // 
            // flowLayoutPanel65
            // 
            this.flowLayoutPanel65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel65.Location = new System.Drawing.Point(49, 241);
            this.flowLayoutPanel65.Name = "flowLayoutPanel65";
            this.flowLayoutPanel65.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel65.TabIndex = 631;
            // 
            // flowLayoutPanel66
            // 
            this.flowLayoutPanel66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel66.Controls.Add(this.flowLayoutPanel67);
            this.flowLayoutPanel66.Controls.Add(this.flowLayoutPanel68);
            this.flowLayoutPanel66.Controls.Add(this.flowLayoutPanel69);
            this.flowLayoutPanel66.Controls.Add(this.flowLayoutPanel70);
            this.flowLayoutPanel66.Controls.Add(this.flowLayoutPanel71);
            this.flowLayoutPanel66.Location = new System.Drawing.Point(204, 249);
            this.flowLayoutPanel66.Name = "flowLayoutPanel66";
            this.flowLayoutPanel66.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel66.TabIndex = 676;
            // 
            // flowLayoutPanel67
            // 
            this.flowLayoutPanel67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel67.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel67.Name = "flowLayoutPanel67";
            this.flowLayoutPanel67.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel67.TabIndex = 634;
            // 
            // flowLayoutPanel68
            // 
            this.flowLayoutPanel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel68.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel68.Name = "flowLayoutPanel68";
            this.flowLayoutPanel68.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel68.TabIndex = 633;
            // 
            // flowLayoutPanel69
            // 
            this.flowLayoutPanel69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel69.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel69.Name = "flowLayoutPanel69";
            this.flowLayoutPanel69.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel69.TabIndex = 632;
            // 
            // flowLayoutPanel70
            // 
            this.flowLayoutPanel70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel70.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel70.Name = "flowLayoutPanel70";
            this.flowLayoutPanel70.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel70.TabIndex = 631;
            // 
            // flowLayoutPanel71
            // 
            this.flowLayoutPanel71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel71.Controls.Add(this.flowLayoutPanel72);
            this.flowLayoutPanel71.Controls.Add(this.flowLayoutPanel73);
            this.flowLayoutPanel71.Controls.Add(this.flowLayoutPanel74);
            this.flowLayoutPanel71.Controls.Add(this.flowLayoutPanel75);
            this.flowLayoutPanel71.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel71.Name = "flowLayoutPanel71";
            this.flowLayoutPanel71.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel71.TabIndex = 676;
            // 
            // flowLayoutPanel72
            // 
            this.flowLayoutPanel72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel72.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel72.Name = "flowLayoutPanel72";
            this.flowLayoutPanel72.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel72.TabIndex = 634;
            // 
            // flowLayoutPanel73
            // 
            this.flowLayoutPanel73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel73.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel73.Name = "flowLayoutPanel73";
            this.flowLayoutPanel73.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel73.TabIndex = 633;
            // 
            // flowLayoutPanel74
            // 
            this.flowLayoutPanel74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel74.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel74.Name = "flowLayoutPanel74";
            this.flowLayoutPanel74.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel74.TabIndex = 632;
            // 
            // flowLayoutPanel75
            // 
            this.flowLayoutPanel75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel75.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel75.Name = "flowLayoutPanel75";
            this.flowLayoutPanel75.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel75.TabIndex = 631;
            // 
            // flowLayoutPanel76
            // 
            this.flowLayoutPanel76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel76.Controls.Add(this.flowLayoutPanel77);
            this.flowLayoutPanel76.Controls.Add(this.flowLayoutPanel78);
            this.flowLayoutPanel76.Controls.Add(this.flowLayoutPanel79);
            this.flowLayoutPanel76.Controls.Add(this.flowLayoutPanel80);
            this.flowLayoutPanel76.Controls.Add(this.flowLayoutPanel81);
            this.flowLayoutPanel76.Location = new System.Drawing.Point(204, 256);
            this.flowLayoutPanel76.Name = "flowLayoutPanel76";
            this.flowLayoutPanel76.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel76.TabIndex = 677;
            // 
            // flowLayoutPanel77
            // 
            this.flowLayoutPanel77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel77.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel77.Name = "flowLayoutPanel77";
            this.flowLayoutPanel77.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel77.TabIndex = 634;
            // 
            // flowLayoutPanel78
            // 
            this.flowLayoutPanel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel78.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel78.Name = "flowLayoutPanel78";
            this.flowLayoutPanel78.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel78.TabIndex = 633;
            // 
            // flowLayoutPanel79
            // 
            this.flowLayoutPanel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel79.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel79.Name = "flowLayoutPanel79";
            this.flowLayoutPanel79.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel79.TabIndex = 632;
            // 
            // flowLayoutPanel80
            // 
            this.flowLayoutPanel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel80.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel80.Name = "flowLayoutPanel80";
            this.flowLayoutPanel80.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel80.TabIndex = 631;
            // 
            // flowLayoutPanel81
            // 
            this.flowLayoutPanel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel82);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel83);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel84);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel85);
            this.flowLayoutPanel81.Controls.Add(this.flowLayoutPanel86);
            this.flowLayoutPanel81.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel81.Name = "flowLayoutPanel81";
            this.flowLayoutPanel81.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel81.TabIndex = 676;
            // 
            // flowLayoutPanel82
            // 
            this.flowLayoutPanel82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel82.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel82.Name = "flowLayoutPanel82";
            this.flowLayoutPanel82.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel82.TabIndex = 634;
            // 
            // flowLayoutPanel83
            // 
            this.flowLayoutPanel83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel83.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel83.Name = "flowLayoutPanel83";
            this.flowLayoutPanel83.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel83.TabIndex = 633;
            // 
            // flowLayoutPanel84
            // 
            this.flowLayoutPanel84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel84.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel84.Name = "flowLayoutPanel84";
            this.flowLayoutPanel84.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel84.TabIndex = 632;
            // 
            // flowLayoutPanel85
            // 
            this.flowLayoutPanel85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel85.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel85.Name = "flowLayoutPanel85";
            this.flowLayoutPanel85.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel85.TabIndex = 631;
            // 
            // flowLayoutPanel86
            // 
            this.flowLayoutPanel86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel86.Controls.Add(this.flowLayoutPanel87);
            this.flowLayoutPanel86.Controls.Add(this.flowLayoutPanel88);
            this.flowLayoutPanel86.Controls.Add(this.flowLayoutPanel89);
            this.flowLayoutPanel86.Controls.Add(this.flowLayoutPanel90);
            this.flowLayoutPanel86.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel86.Name = "flowLayoutPanel86";
            this.flowLayoutPanel86.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel86.TabIndex = 676;
            // 
            // flowLayoutPanel87
            // 
            this.flowLayoutPanel87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel87.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel87.Name = "flowLayoutPanel87";
            this.flowLayoutPanel87.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel87.TabIndex = 634;
            // 
            // flowLayoutPanel88
            // 
            this.flowLayoutPanel88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel88.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel88.Name = "flowLayoutPanel88";
            this.flowLayoutPanel88.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel88.TabIndex = 633;
            // 
            // flowLayoutPanel89
            // 
            this.flowLayoutPanel89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel89.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel89.Name = "flowLayoutPanel89";
            this.flowLayoutPanel89.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel89.TabIndex = 632;
            // 
            // flowLayoutPanel90
            // 
            this.flowLayoutPanel90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel90.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel90.Name = "flowLayoutPanel90";
            this.flowLayoutPanel90.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel90.TabIndex = 631;
            // 
            // flowLayoutPanel91
            // 
            this.flowLayoutPanel91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel92);
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel93);
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel94);
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel95);
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel96);
            this.flowLayoutPanel91.Controls.Add(this.flowLayoutPanel106);
            this.flowLayoutPanel91.Location = new System.Drawing.Point(204, 263);
            this.flowLayoutPanel91.Name = "flowLayoutPanel91";
            this.flowLayoutPanel91.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel91.TabIndex = 678;
            // 
            // flowLayoutPanel92
            // 
            this.flowLayoutPanel92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel92.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel92.Name = "flowLayoutPanel92";
            this.flowLayoutPanel92.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel92.TabIndex = 634;
            // 
            // flowLayoutPanel93
            // 
            this.flowLayoutPanel93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel93.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel93.Name = "flowLayoutPanel93";
            this.flowLayoutPanel93.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel93.TabIndex = 633;
            // 
            // flowLayoutPanel94
            // 
            this.flowLayoutPanel94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel94.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel94.Name = "flowLayoutPanel94";
            this.flowLayoutPanel94.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel94.TabIndex = 632;
            // 
            // flowLayoutPanel95
            // 
            this.flowLayoutPanel95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel95.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel95.Name = "flowLayoutPanel95";
            this.flowLayoutPanel95.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel95.TabIndex = 631;
            // 
            // flowLayoutPanel96
            // 
            this.flowLayoutPanel96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel96.Controls.Add(this.flowLayoutPanel97);
            this.flowLayoutPanel96.Controls.Add(this.flowLayoutPanel98);
            this.flowLayoutPanel96.Controls.Add(this.flowLayoutPanel99);
            this.flowLayoutPanel96.Controls.Add(this.flowLayoutPanel100);
            this.flowLayoutPanel96.Controls.Add(this.flowLayoutPanel101);
            this.flowLayoutPanel96.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel96.Name = "flowLayoutPanel96";
            this.flowLayoutPanel96.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel96.TabIndex = 676;
            // 
            // flowLayoutPanel97
            // 
            this.flowLayoutPanel97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel97.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel97.Name = "flowLayoutPanel97";
            this.flowLayoutPanel97.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel97.TabIndex = 634;
            // 
            // flowLayoutPanel98
            // 
            this.flowLayoutPanel98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel98.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel98.Name = "flowLayoutPanel98";
            this.flowLayoutPanel98.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel98.TabIndex = 633;
            // 
            // flowLayoutPanel99
            // 
            this.flowLayoutPanel99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel99.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel99.Name = "flowLayoutPanel99";
            this.flowLayoutPanel99.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel99.TabIndex = 632;
            // 
            // flowLayoutPanel100
            // 
            this.flowLayoutPanel100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel100.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel100.Name = "flowLayoutPanel100";
            this.flowLayoutPanel100.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel100.TabIndex = 631;
            // 
            // flowLayoutPanel101
            // 
            this.flowLayoutPanel101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel102);
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel103);
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel104);
            this.flowLayoutPanel101.Controls.Add(this.flowLayoutPanel105);
            this.flowLayoutPanel101.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel101.Name = "flowLayoutPanel101";
            this.flowLayoutPanel101.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel101.TabIndex = 676;
            // 
            // flowLayoutPanel102
            // 
            this.flowLayoutPanel102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel102.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel102.Name = "flowLayoutPanel102";
            this.flowLayoutPanel102.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel102.TabIndex = 634;
            // 
            // flowLayoutPanel103
            // 
            this.flowLayoutPanel103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel103.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel103.Name = "flowLayoutPanel103";
            this.flowLayoutPanel103.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel103.TabIndex = 633;
            // 
            // flowLayoutPanel104
            // 
            this.flowLayoutPanel104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel104.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel104.Name = "flowLayoutPanel104";
            this.flowLayoutPanel104.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel104.TabIndex = 632;
            // 
            // flowLayoutPanel105
            // 
            this.flowLayoutPanel105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel105.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel105.Name = "flowLayoutPanel105";
            this.flowLayoutPanel105.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel105.TabIndex = 631;
            // 
            // flowLayoutPanel106
            // 
            this.flowLayoutPanel106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel107);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel108);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel109);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel110);
            this.flowLayoutPanel106.Controls.Add(this.flowLayoutPanel111);
            this.flowLayoutPanel106.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel106.Name = "flowLayoutPanel106";
            this.flowLayoutPanel106.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel106.TabIndex = 677;
            // 
            // flowLayoutPanel107
            // 
            this.flowLayoutPanel107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel107.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel107.Name = "flowLayoutPanel107";
            this.flowLayoutPanel107.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel107.TabIndex = 634;
            // 
            // flowLayoutPanel108
            // 
            this.flowLayoutPanel108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel108.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel108.Name = "flowLayoutPanel108";
            this.flowLayoutPanel108.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel108.TabIndex = 633;
            // 
            // flowLayoutPanel109
            // 
            this.flowLayoutPanel109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel109.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel109.Name = "flowLayoutPanel109";
            this.flowLayoutPanel109.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel109.TabIndex = 632;
            // 
            // flowLayoutPanel110
            // 
            this.flowLayoutPanel110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel110.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel110.Name = "flowLayoutPanel110";
            this.flowLayoutPanel110.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel110.TabIndex = 631;
            // 
            // flowLayoutPanel111
            // 
            this.flowLayoutPanel111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel112);
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel113);
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel114);
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel115);
            this.flowLayoutPanel111.Controls.Add(this.flowLayoutPanel116);
            this.flowLayoutPanel111.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel111.Name = "flowLayoutPanel111";
            this.flowLayoutPanel111.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel111.TabIndex = 676;
            // 
            // flowLayoutPanel112
            // 
            this.flowLayoutPanel112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel112.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel112.Name = "flowLayoutPanel112";
            this.flowLayoutPanel112.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel112.TabIndex = 634;
            // 
            // flowLayoutPanel113
            // 
            this.flowLayoutPanel113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel113.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel113.Name = "flowLayoutPanel113";
            this.flowLayoutPanel113.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel113.TabIndex = 633;
            // 
            // flowLayoutPanel114
            // 
            this.flowLayoutPanel114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel114.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel114.Name = "flowLayoutPanel114";
            this.flowLayoutPanel114.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel114.TabIndex = 632;
            // 
            // flowLayoutPanel115
            // 
            this.flowLayoutPanel115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel115.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel115.Name = "flowLayoutPanel115";
            this.flowLayoutPanel115.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel115.TabIndex = 631;
            // 
            // flowLayoutPanel116
            // 
            this.flowLayoutPanel116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel116.Controls.Add(this.flowLayoutPanel117);
            this.flowLayoutPanel116.Controls.Add(this.flowLayoutPanel118);
            this.flowLayoutPanel116.Controls.Add(this.flowLayoutPanel119);
            this.flowLayoutPanel116.Controls.Add(this.flowLayoutPanel120);
            this.flowLayoutPanel116.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel116.Name = "flowLayoutPanel116";
            this.flowLayoutPanel116.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel116.TabIndex = 676;
            // 
            // flowLayoutPanel117
            // 
            this.flowLayoutPanel117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel117.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel117.Name = "flowLayoutPanel117";
            this.flowLayoutPanel117.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel117.TabIndex = 634;
            // 
            // flowLayoutPanel118
            // 
            this.flowLayoutPanel118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel118.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel118.Name = "flowLayoutPanel118";
            this.flowLayoutPanel118.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel118.TabIndex = 633;
            // 
            // flowLayoutPanel119
            // 
            this.flowLayoutPanel119.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel119.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel119.Name = "flowLayoutPanel119";
            this.flowLayoutPanel119.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel119.TabIndex = 632;
            // 
            // flowLayoutPanel120
            // 
            this.flowLayoutPanel120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel120.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel120.Name = "flowLayoutPanel120";
            this.flowLayoutPanel120.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel120.TabIndex = 631;
            // 
            // flowLayoutPanel121
            // 
            this.flowLayoutPanel121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel121.Controls.Add(this.flowLayoutPanel122);
            this.flowLayoutPanel121.Location = new System.Drawing.Point(196, 263);
            this.flowLayoutPanel121.Name = "flowLayoutPanel121";
            this.flowLayoutPanel121.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel121.TabIndex = 679;
            // 
            // flowLayoutPanel122
            // 
            this.flowLayoutPanel122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel122.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel122.Name = "flowLayoutPanel122";
            this.flowLayoutPanel122.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel122.TabIndex = 632;
            // 
            // txtper_acc
            // 
            this.txtper_acc.BackColor = System.Drawing.Color.White;
            this.txtper_acc.Enabled = false;
            this.txtper_acc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtper_acc.Location = new System.Drawing.Point(167, 307);
            this.txtper_acc.Name = "txtper_acc";
            this.txtper_acc.NumberDecimalDigits = 3;
            this.txtper_acc.NumberDecimalSeparator = ".";
            this.txtper_acc.NumberGroupSeparator = ",";
            this.txtper_acc.Size = new System.Drawing.Size(124, 23);
            this.txtper_acc.TabIndex = 713;
            this.txtper_acc.Text = "0.000";
            // 
            // Depreciation_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 434);
            this.Controls.Add(this.txtper_acc);
            this.Controls.Add(this.flowLayoutPanel61);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_Desc_per);
            this.Controls.Add(this.flowLayoutPanel125);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.BND);
            this.Controls.Add(this.GrdAccount_Tree);
            this.Controls.Add(this.Dep_RD);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Depreciation_main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "434";
            this.Text = "Depreciation_main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Depreciation_main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Depreciation_main_FormClosed);
            this.Load += new System.EventHandler(this.Depreciation_main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Depreciation_main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.GrdAccount_Tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BND)).EndInit();
            this.BND.ResumeLayout(false);
            this.BND.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel16.ResumeLayout(false);
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel26.ResumeLayout(false);
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel36.ResumeLayout(false);
            this.flowLayoutPanel41.ResumeLayout(false);
            this.flowLayoutPanel46.ResumeLayout(false);
            this.flowLayoutPanel51.ResumeLayout(false);
            this.flowLayoutPanel56.ResumeLayout(false);
            this.flowLayoutPanel123.ResumeLayout(false);
            this.flowLayoutPanel125.ResumeLayout(false);
            this.flowLayoutPanel130.ResumeLayout(false);
            this.flowLayoutPanel135.ResumeLayout(false);
            this.flowLayoutPanel140.ResumeLayout(false);
            this.flowLayoutPanel145.ResumeLayout(false);
            this.flowLayoutPanel150.ResumeLayout(false);
            this.flowLayoutPanel155.ResumeLayout(false);
            this.flowLayoutPanel160.ResumeLayout(false);
            this.flowLayoutPanel165.ResumeLayout(false);
            this.flowLayoutPanel170.ResumeLayout(false);
            this.flowLayoutPanel175.ResumeLayout(false);
            this.flowLayoutPanel180.ResumeLayout(false);
            this.flowLayoutPanel185.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel61.ResumeLayout(false);
            this.flowLayoutPanel66.ResumeLayout(false);
            this.flowLayoutPanel71.ResumeLayout(false);
            this.flowLayoutPanel76.ResumeLayout(false);
            this.flowLayoutPanel81.ResumeLayout(false);
            this.flowLayoutPanel86.ResumeLayout(false);
            this.flowLayoutPanel91.ResumeLayout(false);
            this.flowLayoutPanel96.ResumeLayout(false);
            this.flowLayoutPanel101.ResumeLayout(false);
            this.flowLayoutPanel106.ResumeLayout(false);
            this.flowLayoutPanel111.ResumeLayout(false);
            this.flowLayoutPanel116.ResumeLayout(false);
            this.flowLayoutPanel121.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Dep_RD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView GrdAccount_Tree;
        private System.Windows.Forms.BindingNavigator BND;
        private System.Windows.Forms.ToolStripButton BtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BtnUpd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton AllBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton Btn_PrintAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel SearchBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel58;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel59;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel123;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel124;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel125;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel126;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel127;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel128;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel129;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel130;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel131;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel132;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel133;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel134;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel135;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel136;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel137;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel138;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel139;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel140;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel141;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel142;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel143;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel144;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel145;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel146;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel147;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel148;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel149;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel150;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel151;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel152;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel153;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel154;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel155;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel156;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel157;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel158;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel159;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel160;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel161;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel162;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel163;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel164;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel165;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel166;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel167;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel168;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel169;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel170;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel171;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel172;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel173;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel174;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel175;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel176;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel177;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel178;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel179;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel180;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel181;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel182;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel183;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel184;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel185;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel186;
        private System.Windows.Forms.TextBox Txt_Desc_per;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel61;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel62;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel63;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel64;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel65;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel66;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel67;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel68;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel69;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel70;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel71;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel72;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel73;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel74;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel75;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel76;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel77;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel78;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel79;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel80;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel81;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel82;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel83;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel84;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel85;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel86;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel87;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel88;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel89;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel90;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel91;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel92;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel93;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel94;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel95;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel96;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel97;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel98;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel99;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel100;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel101;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel102;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel103;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel104;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel105;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel106;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel107;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel108;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel109;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel110;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel111;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel112;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel113;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel114;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel115;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel116;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel117;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel118;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel119;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel120;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel121;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel122;
        private System.Windows.Forms.ToolStripTextBox Txt_Acc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.Sample.DecimalTextBox txtper_acc;
    }
}