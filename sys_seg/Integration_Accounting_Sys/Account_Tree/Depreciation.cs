﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Depreciation : Form
    {

        #region MyRegion
        string Sql_Text = "";
        string Cond_Str = "";
        int MAcc_cd = 0;
        int MAcc_db = 0;
        int Mform_flag = 0;
        int Macc_id = 0;
        bool TxtChange_acc = false;
        Int16 Dep_Extin = 0;
        String Cond_Str_1 = "";
        bool TxtChange = false;
        bool Change = false;
        Int16 DC_ID = 0;
        string name = connection.Lang_id == 1 ? "a" : "e";
        BindingSource _Bs_Dep_Add = new BindingSource();
        BindingSource _Bs_Dep_Upd = new BindingSource();
        BindingSource _Bs_Dep = new BindingSource();
        #endregion

        public Depreciation(byte form_flag)
        {
            Mform_flag = form_flag;
            InitializeComponent();
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            #region Arabic/Enghlis
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id != 1)
            {
                Column3.DataPropertyName = "acc_ename";
                Column6.DataPropertyName = "acc_ename";
                Column9.DataPropertyName = "acc_ename";

            }
            #endregion
        }

        private void Depreciation_Load(object sender, EventArgs e)
        {

            Grdcd_acc.AutoGenerateColumns = false;
            Grddb_acc.AutoGenerateColumns = false;
            Grdacc_no.AutoGenerateColumns = false;

            txtper_acc.Text = "";
            Dep_RD.Checked = false;
          //  Ext_RD.Checked = false;
            Grdacc_no.DataSource = null;
            Grdcd_acc.DataSource = null;
            Grddb_acc.DataSource = null;
            TxtChange_acc = false;
            Txtcd_acc.Text = "";
            Txtdb_acc.Text = "";
            Txt_AccSrch.Text = "";
            Dep_RD.Checked = true;

            if (Mform_flag == 1)
            {
                Txt_AccSrch_TextChanged(sender, e);
                if (_Bs_Dep_Add.Count == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "  لا توجد حسابات للاندثار" : " There is No Account to Depreciation ", MyGeneral_Lib.LblCap);
                    this.Close();
                }


            }
            else
            {
                GetData();
            }
        }
        //------------------------------------------------------------------------
        private void Txt_AccSrch_TextChanged(object sender, EventArgs e)
        {
            TxtChange_acc = true;
            GetData_Add(3, Txt_AccSrch.Text.Trim());
        }
        //------------------------------------------------------------------------
        private void Txtdb_acc_TextChanged(object sender, EventArgs e)
        {
            if (Txtdb_acc.Text == "")
            {
                _Bs_Dep_Upd.DataSource = null;

            }
            else
            {
                TxtChange_acc = true;
                GetData_Add(1, Txtdb_acc.Text.Trim());
            }

        }
        //------------------------------------------------------------------------
        private void Txtcd_acc_TextChanged(object sender, EventArgs e)
        {
            if (Txtcd_acc.Text == "")
            {
                _Bs_Dep.DataSource = null;

            }
            else
            {
                TxtChange_acc = true;
                GetData_Add(2, Txtcd_acc.Text.Trim());
            }
        }

        //------------------------------------------------------------------------
        private void GetData_Add(int flag, string acc_Namex)
        {
            if (TxtChange_acc)
            {
                int acc_id = 0;
                if (int.TryParse(acc_Namex, out acc_id))
                {
                    if (Mform_flag == 1)
                    {
                        Cond_Str = " where (Acc_ID like '" + acc_id + "%'"
                                   + " or "
                                   + "  Ref_Acc_No like '" + acc_id + "%')"
                                   + " and pacc_id  != 0 ";
                    }
                    else
                    {
                        Cond_Str = " where (Acc_ID like '" + acc_id + "'"
                                       + " or "
                                       + "  Ref_Acc_No like '" + acc_id + "')";
                    }
                }
                else
                {

                    Cond_Str = " where Acc_" + name + "Name like'" + acc_Namex + "%'" + " and pacc_id  != 0 "; ;
                }


                Sql_Text = " select  " + name + "_Description,acc_no,Acc_ID,Acc_" + name
                         + "Name, Ref_Acc_No,Acc_Level,Dot_Acc_No ,Dc_ID"
                         + " from Account_Tree " + Cond_Str;

                if (flag == 1)
                {
                    Sql_Text = Sql_Text + "and DC_ID = 1  and pacc_id  != 0  and Record_flag = 1 and acc_id not in (" + Macc_id + ")";
                    _Bs_Dep_Upd.DataSource = connection.SqlExec(Sql_Text, "Account_Tbl1");
                    Grddb_acc.DataSource = _Bs_Dep_Upd;

                }
                if (flag == 2)
                {
                    Sql_Text = Sql_Text + "and DC_ID = -1 and pacc_id  != 0  and Record_flag = 1 and acc_id not in (" + Macc_id + ")";
                    _Bs_Dep.DataSource = connection.SqlExec(Sql_Text, "Account_Tbl2");
                    Grdcd_acc.DataSource = _Bs_Dep;

                }
                if (flag == 3)
                {

                    Change = false;
                    if (Mform_flag == 1)
                    { Cond_Str_1 = "and dep_per = 0.00"; }

                    Sql_Text = Sql_Text + " and pacc_id  != 0  AND RECORD_FLAG = 1 AND Acc_No LIKE '1.1.%'" + Cond_Str_1;
                    _Bs_Dep_Add.DataSource = connection.SqlExec(Sql_Text, "Account_Tbl3");

                    if (connection.SQLDS.Tables["Account_Tbl3"].Rows.Count > 0)
                    {

                        Grdacc_no.DataSource = _Bs_Dep_Add;
                        Change = true;
                        Grdacc_no_SelectionChanged(null, null);


                    }
                }

            }
        }
        //------------------------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Validation
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Change = false;
            if (txtper_acc.Text == "")
            {
                MessageBox.Show(connection.Lang_id == 1 ? "  حدد نسبة الاندثار السنوي" : "Please enter Depreciation Amount ", MyGeneral_Lib.LblCap);
                txtper_acc.Focus();
                return;
            }


        #endregion

            if (!( Dep_RD.Checked))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد اندثار" : "Please Select Depreciation/Extinguish", MyGeneral_Lib.LblCap);
                Dep_RD.Focus();
                return;
            }
            if (Dep_RD.Checked)
                Dep_Extin = 1;
            else
                Dep_Extin = 0;

            if (_Bs_Dep_Upd.Count != 0)
            {
                MAcc_db = Convert.ToInt32(((DataRowView)_Bs_Dep_Upd.Current).Row["Acc_ID"]);
            }
            if (_Bs_Dep.Count != 0)
            {
                MAcc_cd = Convert.ToInt32(((DataRowView)_Bs_Dep.Current).Row["Acc_ID"]);
            }

            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Macc_id);
            ItemList.Insert(1, MAcc_cd);
            ItemList.Insert(2, MAcc_db);
            ItemList.Insert(3, Convert.ToDecimal(txtper_acc.Text));
            ItemList.Insert(4, Dep_Extin);
            ItemList.Insert(5, connection.user_id);
            ItemList.Insert(6, Mform_flag);
            ItemList.Insert(7, "");

            connection.scalar("ADD_Upd_Depreciation", ItemList);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            connection.SQLCMD.Parameters.Clear();

            //if (Mform_flag == 1)
            //{
            //    TxtChange = false;
            //    string[] Used_Tbl = { "Account_Tree", "Account_Tree2", "Account_Tree3", "Account_Tree4" };
            //    foreach (string Tbl in Used_Tbl)
            //    {
            //        if (connection.SQLDS.Tables.Contains(Tbl))
            //        {
            //            connection.SQLDS.Tables.Remove(Tbl);
            //        }
            //    }
            if (Mform_flag == 1)
            {
                Depreciation_Load(null, null);
            }
            else
            {
                this.Close();
            }

            //}
            //else
            //{
            //    this.Close(); 
            //}
        }
        //------------------------------------------------------------------------
        private void GetData()
        {

            TxtChange = true;
            DataRowView Drv = Depreciation_main._Bs_Dep_Main.Current as DataRowView;
            DataRow Dr = Drv.Row;
            Txt_AccSrch.Text = Dr.Field<int>("Acc_ID").ToString();
            txtper_acc.Text = Dr.Field<decimal>("dep_per").ToString();

            Txt_Desc_per.Text = Dr.Field<string>("A_Description");

            bool DEP = Dr.Field<bool>("Depreciation");
            if (DEP == true)
            {
                Dep_RD.Checked = true;
              //  Ext_RD.Checked = false;
            }

            else
            {
                Dep_RD.Checked = false;
               // Ext_RD.Checked = true;

            }

            Txtdb_acc.Text = Dr.Field<int>("db_dep_acc").ToString();
            Txtcd_acc.Text = Dr.Field<int>("cd_dep_acc").ToString();
            Txtdb_acc.Enabled = false;
            Txtcd_acc.Enabled = false;
            Txt_AccSrch.Enabled = false;
            Dep_RD.Enabled = false;
           // Ext_RD.Enabled = false;
            Grdacc_no.Enabled = false;


        }
        //-----------------------------------------------------------------------------------------------------------
        private void Grdacc_no_SelectionChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                DC_ID = Convert.ToInt16(((DataRowView)_Bs_Dep_Add.Current).Row["DC_ID"]);
                Macc_id = Convert.ToInt32(((DataRowView)_Bs_Dep_Add.Current).Row["Acc_id"]);

                //if (DC_ID == 1)
                //{

                //    Txtdb_acc.Enabled = false;
                //    Txtcd_acc.Enabled = true;

                //    Txtdb_acc.Text = "";
                //    Txtcd_acc.Text = "";


                //}
                //else if (DC_ID == -1)
                //{


                //    Txtdb_acc.Enabled = true;
                //    Txtcd_acc.Enabled = false;
                //    Txtdb_acc.Text = "";
                //    Txtcd_acc.Text = "";

                //}
                //else
                //{
                //    Txtdb_acc.Enabled = true;
                //    Txtcd_acc.Enabled = true;
                //}

            }

        }
       //------------------------------------------------------------------------
        private void Depreciation_FormClosed(object sender, FormClosedEventArgs e)
        {
            TxtChange_acc = false;
            Change = false;
            TxtChange = false;

            string[] Used_Tbl = { "Account_Tbl1", "Account_Tbl2", "Account_Tbl3" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Depreciation_HelpButtonClicked(object sender, CancelEventArgs e)
        {
           connection.page_no  = 6;
           Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}