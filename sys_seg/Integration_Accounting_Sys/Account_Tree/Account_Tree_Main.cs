﻿using System;
using System.Data;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using Integration_Accounting_Sys.Report;

namespace Integration_Accounting_Sys
{
    public partial class Account_Tree_Main : Form
    {

        string Sql_Text = "";
        bool txt = false;
        bool GrdChange = false;
        public static int page_no = 0;
        public static BindingSource _Bs_Main_Tree = new BindingSource();
        public Account_Tree_Main()
        {
            InitializeComponent();
            GrdAccount_Tree.AutoGenerateColumns = false;

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            if (connection.Lang_id != 1)
            {
                GrdAccount_Tree.Columns["column2"].DataPropertyName = "Acc_EName";
                GrdAccount_Tree.Columns["column4"].DataPropertyName = "DC_Ename";
                GrdAccount_Tree.Columns["column6"].DataPropertyName = "Acc_Class_Ename";

                label19.Text = "Control Acc.";
                label20.Text = "Recordable";
                label21.Text = "Evaluable Acc";
                label22.Text = "Has Sub Acc.";
                label10.Text = "Opposite Acc:";

            }
        }
        //---------------------------------------------
        private void Account_Tree_Main_Load(object sender, EventArgs e)
        {
            Txt_AccSrch.Text = "";
            Txt_AccSrch_TextChanged(null, null);
        }
        //---------------------------------------------
        private void Txt_AccSrch_TextChanged(object sender, EventArgs e)
        {
            GetData();
        }
        //---------------------------------------------
        private void GetData()
        {
            GrdChange = false;
            txt = false;
            object[] Sparam = { Txt_AccSrch.Text.Trim() };
            _Bs_Main_Tree.DataSource = connection.SqlExec("Account_tree_main", "Account_Tree", Sparam);
            GrdAccount_Tree.DataSource = _Bs_Main_Tree;
            if (connection.SQLDS.Tables["Account_Tree"].Rows.Count > 0)
            {

                GrdChange = true;
                GrdAccount_Tree_SelectionChanged(null, null);
            }
        }
        //---------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Acc_No_Add_Update AddFrm = new Acc_No_Add_Update(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Account_Tree_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------
        private void UpdBtn_Click(object sender, EventArgs e)
        {
            Acc_No_Add_Update AddFrm = new Acc_No_Add_Update(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Account_Tree_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------
        private void AllBtn_Click(object sender, EventArgs e)
        {
            Txt_AccSrch.Text = "";
            GetData();
        }
        //---------------------------------------------
        private void TxtOp_acc_id_TextChanged(object sender, EventArgs e)
        {
            if (txt)
            {
                if (TxtOp_acc_id.Text != "")
                {
                    Sql_Text = " SELECT  Acc_AName, Acc_EName,acc_id FROM  Account_Tree  "
                             + " where acc_id ='" + TxtOp_acc_id.Text + "'";
                    connection.SqlExec(Sql_Text, "Acc_AName");

                    if (connection.SQLDS.Tables["Acc_AName"].Rows.Count > 0)
                    {
                        if (connection.Lang_id == 1)
                        {
                            TxtOp_acc.Text = connection.SQLDS.Tables["Acc_AName"].Rows[0]["Acc_AName"].ToString();

                        }
                        else
                        {
                            TxtOp_acc.Text = connection.SQLDS.Tables["Acc_AName"].Rows[0]["Acc_EName"].ToString();
                        }
                    }
                    else
                    {
                        TxtOp_acc.Text = "";
                    }
                }
                else
                {
                    TxtOp_acc.Text = "";
                }

            }
        }
        //---------------------------------------------
        private void Account_Tree_Main_FormClosed_1(object sender, FormClosedEventArgs e)
        {

            txt = false;
            GrdChange = false;
            string[] Used_Tbl = { "Account_Tree", "Acc_AName" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
        //---------------------------------------------
        private void label18_Click(object sender, EventArgs e)
        {
            Acc_No_Tree AddFrm1 = new Acc_No_Tree();
            this.Visible = false;
            AddFrm1.ShowDialog(this);
            Account_Tree_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------
        private void printToolStripButton_Click(object sender, EventArgs e)
        {

            Account_Tree_Rep ObjRpt = new Account_Tree_Rep();
            Account_Tree_Rep_Eng ObjRpteng = new Account_Tree_Rep_Eng();
            //connection.SqlExec("EXec Account_tree_TreeView", "Tree_Tbl_Rep");
            DataTable DT = new DataTable();
            DataTable Exp_Dt = connection.SQLDS.Tables["Account_Tree"].DefaultView.ToTable(false, "Acc_Id", connection.Lang_id == 1 ? "Acc_AName" : "Acc_EName",
                                                                                          "Acc_No", connection.Lang_id == 1 ? "DC_ANAME" : "DC_Ename", "Ref_Acc_No", "ACC_CLASS", "dep_per", "cd_dep_acc", "db_dep_acc",
                                                                                          "op_inv_acc", "EVAL_FLAG", "control_sw", "sub_flag", "RECORD_FLAG",
                                                                                           "Acc_Sett" ,"Depreciation", "User_Name").Select().CopyToDataTable();

            DataTable[] _Exp_Dt = { Exp_Dt };
            DataGridView[] GRd = { GrdAccount_Tree };
            RptLang_MsgBox RptLangFrm = new RptLang_MsgBox(ObjRpt, ObjRpteng, true, true, true, DT, GRd, _Exp_Dt, true, false, this.Text);
            this.Visible = false;
            RptLangFrm.ShowDialog(this);
            this.Visible = true;

        }
        //--------------------------------------------------------------------
        private void GrdAccount_Tree_SelectionChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                LblRec.Text = connection.Records(_Bs_Main_Tree);
                txt = false;

                if (connection.SQLDS.Tables["Account_Tree"].Rows.Count > 0)
                {
                    int Mpacc_id = 0;
                    DataRowView Drv = _Bs_Main_Tree.Current as DataRowView;
                    DataRow Dr = Drv.Row;
                    Mpacc_id = Dr.Field<Int32>("Pacc_id");

                    if (Mpacc_id > 0)
                    {
                        BtnUpd.Enabled = true;
                    }
                    else
                    {
                        BtnUpd.Enabled = false;
                    }
                    txt = true;
                    TxtOp_acc_id_TextChanged(null, null);

                }


                TxtOp_acc_id.DataBindings.Clear();
                label19.DataBindings.Clear();
                label22.DataBindings.Clear();
                label21.DataBindings.Clear();
                label20.DataBindings.Clear();
                txtDe.DataBindings.Clear();
                Chk_Acc_Sett.DataBindings.Clear();
                txtDe.DataBindings.Add("Text", _Bs_Main_Tree, connection.Lang_id == 1 ? "A_Description" : "E_Description");
                TxtOp_acc_id.DataBindings.Add("Text", _Bs_Main_Tree, "op_inv_acc");
                label19.DataBindings.Add("Checked", _Bs_Main_Tree, "control_sw");
                label22.DataBindings.Add("Checked", _Bs_Main_Tree, "sub_flag");
                label21.DataBindings.Add("Checked", _Bs_Main_Tree, "EVAL_FLAG");
                label20.DataBindings.Add("Checked", _Bs_Main_Tree, "RECORD_FLAG");
                Chk_Acc_Sett.DataBindings.Add("Checked", _Bs_Main_Tree, "Acc_Sett");
            }
        }
        //--------------------------------------------------------------------
        private void Account_Tree_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    UpdBtn_Click(sender, e);
                    break;
                case Keys.F5:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.F4:
                    AllBtn_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void Account_Tree_Main_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            page_no = 1;
            Help_Desc.help_description   EmpFrm = new  Help_Desc.help_description(page_no);
            EmpFrm.ShowDialog(this);
        
        }
    }
}