﻿namespace Integration_Accounting_Sys
{
    partial class Acc_No_Tree
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.label2 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.CheckBox();
            this.txtDe = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txtcd_acc_id = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtOp_acc = new System.Windows.Forms.TextBox();
            this.Txtcd_acc = new System.Windows.Forms.TextBox();
            this.Txtdb_acc = new System.Windows.Forms.TextBox();
            this.Txtdb_acc_id = new System.Windows.Forms.TextBox();
            this.TxtOp_acc_id = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtper_acc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Acc_Id = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel121 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Chk_Acc_Sett = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView1.ForeColor = System.Drawing.Color.Black;
            this.treeView1.Location = new System.Drawing.Point(6, 36);
            this.treeView1.Name = "treeView1";
            this.treeView1.RightToLeftLayout = true;
            this.treeView1.Size = new System.Drawing.Size(933, 382);
            this.treeView1.TabIndex = 3;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(2, 419);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 14);
            this.label2.TabIndex = 675;
            this.label2.Text = "خصائــــص الحساب ....";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(13, 484);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 20);
            this.label21.TabIndex = 8;
            this.label21.Text = "له ثانوي";
            this.label21.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(13, 460);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 20);
            this.label20.TabIndex = 6;
            this.label20.Text = "اعادة تقيم";
            this.label20.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.Enabled = false;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(202, 484);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 20);
            this.label19.TabIndex = 9;
            this.label19.Text = "يقبل الحركة";
            this.label19.UseVisualStyleBackColor = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.Enabled = false;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(202, 460);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 20);
            this.label18.TabIndex = 7;
            this.label18.Text = "حساب وسيط";
            this.label18.UseVisualStyleBackColor = false;
            // 
            // txtDe
            // 
            this.txtDe.BackColor = System.Drawing.Color.White;
            this.txtDe.Enabled = false;
            this.txtDe.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtDe.Location = new System.Drawing.Point(11, 540);
            this.txtDe.Multiline = true;
            this.txtDe.Name = "txtDe";
            this.txtDe.Size = new System.Drawing.Size(925, 45);
            this.txtDe.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(11, 520);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 14);
            this.label1.TabIndex = 681;
            this.label1.Text = "تفاصـيــــــل الحساب ....";
            // 
            // Txtcd_acc_id
            // 
            this.Txtcd_acc_id.BackColor = System.Drawing.Color.White;
            this.Txtcd_acc_id.Enabled = false;
            this.Txtcd_acc_id.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Txtcd_acc_id.Location = new System.Drawing.Point(723, 484);
            this.Txtcd_acc_id.Name = "Txtcd_acc_id";
            this.Txtcd_acc_id.ReadOnly = true;
            this.Txtcd_acc_id.Size = new System.Drawing.Size(55, 22);
            this.Txtcd_acc_id.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(644, 486);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 16);
            this.label9.TabIndex = 686;
            this.label9.Text = "الحساب الدائن";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(603, 462);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 16);
            this.label10.TabIndex = 687;
            this.label10.Text = "الحساب المناظر للشواذ";
            // 
            // TxtOp_acc
            // 
            this.TxtOp_acc.BackColor = System.Drawing.Color.White;
            this.TxtOp_acc.Enabled = false;
            this.TxtOp_acc.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtOp_acc.Location = new System.Drawing.Point(781, 460);
            this.TxtOp_acc.Name = "TxtOp_acc";
            this.TxtOp_acc.ReadOnly = true;
            this.TxtOp_acc.Size = new System.Drawing.Size(151, 22);
            this.TxtOp_acc.TabIndex = 12;
            // 
            // Txtcd_acc
            // 
            this.Txtcd_acc.BackColor = System.Drawing.Color.White;
            this.Txtcd_acc.Enabled = false;
            this.Txtcd_acc.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Txtcd_acc.Location = new System.Drawing.Point(780, 484);
            this.Txtcd_acc.Name = "Txtcd_acc";
            this.Txtcd_acc.ReadOnly = true;
            this.Txtcd_acc.Size = new System.Drawing.Size(152, 22);
            this.Txtcd_acc.TabIndex = 16;
            // 
            // Txtdb_acc
            // 
            this.Txtdb_acc.BackColor = System.Drawing.Color.White;
            this.Txtdb_acc.Enabled = false;
            this.Txtdb_acc.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Txtdb_acc.Location = new System.Drawing.Point(470, 484);
            this.Txtdb_acc.Name = "Txtdb_acc";
            this.Txtdb_acc.ReadOnly = true;
            this.Txtdb_acc.Size = new System.Drawing.Size(171, 22);
            this.Txtdb_acc.TabIndex = 14;
            // 
            // Txtdb_acc_id
            // 
            this.Txtdb_acc_id.BackColor = System.Drawing.Color.White;
            this.Txtdb_acc_id.Enabled = false;
            this.Txtdb_acc_id.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Txtdb_acc_id.Location = new System.Drawing.Point(412, 484);
            this.Txtdb_acc_id.Name = "Txtdb_acc_id";
            this.Txtdb_acc_id.ReadOnly = true;
            this.Txtdb_acc_id.Size = new System.Drawing.Size(55, 22);
            this.Txtdb_acc_id.TabIndex = 13;
            // 
            // TxtOp_acc_id
            // 
            this.TxtOp_acc_id.BackColor = System.Drawing.Color.White;
            this.TxtOp_acc_id.Enabled = false;
            this.TxtOp_acc_id.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtOp_acc_id.Location = new System.Drawing.Point(724, 460);
            this.TxtOp_acc_id.Name = "TxtOp_acc_id";
            this.TxtOp_acc_id.ReadOnly = true;
            this.TxtOp_acc_id.Size = new System.Drawing.Size(55, 22);
            this.TxtOp_acc_id.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(333, 486);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 685;
            this.label8.Text = "الحساب المدين";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(538, 462);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 16);
            this.label7.TabIndex = 684;
            this.label7.Text = "%";
            // 
            // txtper_acc
            // 
            this.txtper_acc.BackColor = System.Drawing.Color.White;
            this.txtper_acc.Enabled = false;
            this.txtper_acc.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtper_acc.Location = new System.Drawing.Point(412, 460);
            this.txtper_acc.Name = "txtper_acc";
            this.txtper_acc.ReadOnly = true;
            this.txtper_acc.Size = new System.Drawing.Size(120, 22);
            this.txtper_acc.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(333, 462);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 16);
            this.label6.TabIndex = 682;
            this.label6.Text = "نسبـة الاندثـار";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(14, 440);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "رقــــم الحســــاب :";
            // 
            // Txt_Acc_Id
            // 
            this.Txt_Acc_Id.BackColor = System.Drawing.Color.White;
            this.Txt_Acc_Id.Enabled = false;
            this.Txt_Acc_Id.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Txt_Acc_Id.Location = new System.Drawing.Point(115, 439);
            this.Txt_Acc_Id.Name = "Txt_Acc_Id";
            this.Txt_Acc_Id.ReadOnly = true;
            this.Txt_Acc_Id.Size = new System.Drawing.Size(123, 22);
            this.Txt_Acc_Id.TabIndex = 5;
            // 
            // flowLayoutPanel121
            // 
            this.flowLayoutPanel121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel121.Location = new System.Drawing.Point(1, 31);
            this.flowLayoutPanel121.Name = "flowLayoutPanel121";
            this.flowLayoutPanel121.Size = new System.Drawing.Size(954, 2);
            this.flowLayoutPanel121.TabIndex = 702;
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(118, 4);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(275, 22);
            this.TxtUser.TabIndex = 0;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(541, 4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(214, 22);
            this.TxtIn_Rec_Date.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(479, 8);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 698;
            this.label4.Text = "التاريـــخ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 8);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(106, 14);
            this.label5.TabIndex = 697;
            this.label5.Text = "اسم المستخــدم:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(97, 430);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(842, 1);
            this.flowLayoutPanel1.TabIndex = 703;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 589);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(934, 1);
            this.flowLayoutPanel2.TabIndex = 704;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(8, 530);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(930, 1);
            this.flowLayoutPanel3.TabIndex = 705;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 430);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1, 160);
            this.flowLayoutPanel4.TabIndex = 706;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(938, 431);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 159);
            this.flowLayoutPanel5.TabIndex = 707;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(326, 431);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 99);
            this.flowLayoutPanel6.TabIndex = 707;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(828, 2);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(111, 25);
            this.Btn_Add.TabIndex = 2;
            this.Btn_Add.Text = "طباعـــــة الشجــرة";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Chk_Acc_Sett
            // 
            this.Chk_Acc_Sett.AutoSize = true;
            this.Chk_Acc_Sett.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Chk_Acc_Sett.Enabled = false;
            this.Chk_Acc_Sett.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Acc_Sett.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Acc_Sett.Location = new System.Drawing.Point(68, 502);
            this.Chk_Acc_Sett.Name = "Chk_Acc_Sett";
            this.Chk_Acc_Sett.Size = new System.Drawing.Size(155, 20);
            this.Chk_Acc_Sett.TabIndex = 708;
            this.Chk_Acc_Sett.Text = "تسوية محاسبية بأثر رجعي";
            this.Chk_Acc_Sett.UseVisualStyleBackColor = false;
            // 
            // Acc_No_Tree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 598);
            this.Controls.Add(this.Chk_Acc_Sett);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel121);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_Acc_Id);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txtcd_acc_id);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TxtOp_acc);
            this.Controls.Add(this.Txtcd_acc);
            this.Controls.Add(this.Txtdb_acc);
            this.Controls.Add(this.Txtdb_acc_id);
            this.Controls.Add(this.TxtOp_acc_id);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtper_acc);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDe);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.treeView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Acc_No_Tree";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "167";
            this.Text = "Acc_No_Tree";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Acc_No_Tree_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Acc_No_Tree_FormClosed);
            this.Load += new System.EventHandler(this.Acc_No_Tree_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox label21;
        private System.Windows.Forms.CheckBox label20;
        private System.Windows.Forms.CheckBox label19;
        private System.Windows.Forms.CheckBox label18;
        private System.Windows.Forms.TextBox txtDe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txtcd_acc_id;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtOp_acc;
        private System.Windows.Forms.TextBox Txtcd_acc;
        private System.Windows.Forms.TextBox Txtdb_acc;
        private System.Windows.Forms.TextBox Txtdb_acc_id;
        private System.Windows.Forms.TextBox TxtOp_acc_id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtper_acc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Acc_Id;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel121;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.CheckBox Chk_Acc_Sett;
    }
}