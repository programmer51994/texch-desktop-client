﻿namespace Integration_Accounting_Sys
{
    partial class Acc_No_Add_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.CheckBox();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtOp_acc = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grdacc_no = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_Desc_per = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_AccSrch = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_ADesc = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Txt_EDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_AccName_A = new System.Windows.Forms.TextBox();
            this.Txt_AccName_E = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Grdop_acc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cbo_AccType = new System.Windows.Forms.ComboBox();
            this.Cbo_class_id = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.Chk_Acc_Sett = new System.Windows.Forms.CheckBox();
            this.TxtRef_acc = new Integration_Accounting_Sys.NumericTextBox();
            this.TxtIn_Rec_Date = new Integration_Accounting_Sys.MyDateTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grdacc_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdop_acc)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(217, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 16);
            this.label7.TabIndex = 667;
            this.label7.Text = "صنف الحساب:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(26, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.TabIndex = 668;
            this.label5.Text = "رقـم المرجــــــع:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.DarkRed;
            this.label10.Location = new System.Drawing.Point(9, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 16);
            this.label10.TabIndex = 664;
            this.label10.Text = "خصائص الحساب....";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(257, 332);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 20);
            this.label21.TabIndex = 12;
            this.label21.Text = "له ثانوي";
            this.label21.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(257, 310);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 20);
            this.label20.TabIndex = 10;
            this.label20.Text = "اعادة تقيم";
            this.label20.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(76, 332);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 20);
            this.label19.TabIndex = 11;
            this.label19.Text = "يقبل الحركة";
            this.label19.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(76, 310);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 20);
            this.label18.TabIndex = 9;
            this.label18.Text = "حساب وسيط";
            this.label18.UseVisualStyleBackColor = true;
            // 
            // ExtBtn
            // 
            this.ExtBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExtBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(454, 390);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(89, 26);
            this.ExtBtn.TabIndex = 19;
            this.ExtBtn.Text = "انهاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(903, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(2, 249);
            this.flowLayoutPanel4.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(438, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 16);
            this.label13.TabIndex = 679;
            this.label13.Text = "الحساب المناظر للشواذ";
            // 
            // TxtOp_acc
            // 
            this.TxtOp_acc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOp_acc.Location = new System.Drawing.Point(558, 293);
            this.TxtOp_acc.Name = "TxtOp_acc";
            this.TxtOp_acc.Size = new System.Drawing.Size(347, 22);
            this.TxtOp_acc.TabIndex = 16;
            this.TxtOp_acc.TextChanged += new System.EventHandler(this.TxtOp_acc_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(437, 277);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(467, 2);
            this.flowLayoutPanel1.TabIndex = 652;
            // 
            // Grdacc_no
            // 
            this.Grdacc_no.AllowUserToAddRows = false;
            this.Grdacc_no.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdacc_no.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdacc_no.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grdacc_no.ColumnHeadersHeight = 24;
            this.Grdacc_no.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column4});
            this.Grdacc_no.Location = new System.Drawing.Point(451, 66);
            this.Grdacc_no.Name = "Grdacc_no";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdacc_no.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.Grdacc_no.Size = new System.Drawing.Size(446, 124);
            this.Grdacc_no.TabIndex = 14;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "acc_id";
            this.Column3.HeaderText = "رقـم الحساب";
            this.Column3.Name = "Column3";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Ref_Acc_No";
            this.Column1.HeaderText = "رقم المرجع";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Acc_AName";
            this.Column2.HeaderText = "اسم الحساب";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "a_Description";
            this.Column5.HeaderText = "a_Description";
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "E_Description";
            this.Column4.HeaderText = "E_Description";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-3, 383);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(918, 2);
            this.flowLayoutPanel6.TabIndex = 656;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(2, 29);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(902, 2);
            this.flowLayoutPanel5.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(454, 192);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 16);
            this.label12.TabIndex = 11;
            this.label12.Text = "وصف الحساب المتفرع منه:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_Desc_per
            // 
            this.Txt_Desc_per.BackColor = System.Drawing.Color.White;
            this.Txt_Desc_per.Enabled = false;
            this.Txt_Desc_per.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Desc_per.Location = new System.Drawing.Point(451, 209);
            this.Txt_Desc_per.Multiline = true;
            this.Txt_Desc_per.Name = "Txt_Desc_per";
            this.Txt_Desc_per.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_Desc_per.Size = new System.Drawing.Size(446, 52);
            this.Txt_Desc_per.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.DarkRed;
            this.label11.Location = new System.Drawing.Point(470, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 16);
            this.label11.TabIndex = 660;
            this.label11.Text = "متفـرع من:";
            // 
            // Txt_AccSrch
            // 
            this.Txt_AccSrch.BackColor = System.Drawing.Color.White;
            this.Txt_AccSrch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Txt_AccSrch.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AccSrch.Location = new System.Drawing.Point(545, 42);
            this.Txt_AccSrch.Name = "Txt_AccSrch";
            this.Txt_AccSrch.Size = new System.Drawing.Size(352, 23);
            this.Txt_AccSrch.TabIndex = 13;
            this.Txt_AccSrch.TextChanged += new System.EventHandler(this.Txt_AccSrch_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(19, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 16);
            this.label8.TabIndex = 657;
            this.label8.Text = "وصف الحساب بالعربي:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_ADesc
            // 
            this.Txt_ADesc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ADesc.Location = new System.Drawing.Point(9, 167);
            this.Txt_ADesc.Multiline = true;
            this.Txt_ADesc.Name = "Txt_ADesc";
            this.Txt_ADesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_ADesc.Size = new System.Drawing.Size(389, 52);
            this.Txt_ADesc.TabIndex = 7;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(435, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(2, 249);
            this.flowLayoutPanel3.TabIndex = 651;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(26, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 16);
            this.label6.TabIndex = 647;
            this.label6.Text = "طبيعــة الحسـاب:";
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(366, 390);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(89, 26);
            this.Btn_Add.TabIndex = 18;
            this.Btn_Add.Text = "موافـق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Txt_EDesc
            // 
            this.Txt_EDesc.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_EDesc.Location = new System.Drawing.Point(9, 236);
            this.Txt_EDesc.Multiline = true;
            this.Txt_EDesc.Name = "Txt_EDesc";
            this.Txt_EDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Txt_EDesc.Size = new System.Drawing.Size(389, 52);
            this.Txt_EDesc.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(26, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 16);
            this.label4.TabIndex = 645;
            this.label4.Text = "الأسم الإنكليـزي:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(19, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 16);
            this.label9.TabIndex = 648;
            this.label9.Text = "وصف الحساب بالانكليزي:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(26, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 646;
            this.label3.Text = "الأســـم العربــي:";
            // 
            // Txt_AccName_A
            // 
            this.Txt_AccName_A.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AccName_A.Location = new System.Drawing.Point(116, 37);
            this.Txt_AccName_A.Name = "Txt_AccName_A";
            this.Txt_AccName_A.Size = new System.Drawing.Size(282, 23);
            this.Txt_AccName_A.TabIndex = 2;
            // 
            // Txt_AccName_E
            // 
            this.Txt_AccName_E.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_AccName_E.Location = new System.Drawing.Point(116, 63);
            this.Txt_AccName_E.Name = "Txt_AccName_E";
            this.Txt_AccName_E.Size = new System.Drawing.Size(282, 23);
            this.Txt_AccName_E.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(47, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 642;
            this.label1.Text = "المستـخدم:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_UsrName.Location = new System.Drawing.Point(118, 4);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.Size = new System.Drawing.Size(279, 23);
            this.Txt_UsrName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(613, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 639;
            this.label2.Text = "التأريخ:";
            // 
            // Grdop_acc
            // 
            this.Grdop_acc.AllowUserToAddRows = false;
            this.Grdop_acc.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdop_acc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grdop_acc.BackgroundColor = System.Drawing.Color.White;
            this.Grdop_acc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grdop_acc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grdop_acc.ColumnHeadersHeight = 45;
            this.Grdop_acc.ColumnHeadersVisible = false;
            this.Grdop_acc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.Column9});
            this.Grdop_acc.Location = new System.Drawing.Point(437, 316);
            this.Grdop_acc.Name = "Grdop_acc";
            this.Grdop_acc.ReadOnly = true;
            this.Grdop_acc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.Grdop_acc.RowHeadersVisible = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grdop_acc.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grdop_acc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grdop_acc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grdop_acc.Size = new System.Drawing.Size(468, 50);
            this.Grdop_acc.TabIndex = 17;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "acc_id";
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "رقم الحساب";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Ref_Acc_No";
            this.dataGridViewTextBoxColumn6.HeaderText = "رقم المرجع";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 50;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Acc_AName";
            this.Column9.HeaderText = "اسم الحساب";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 250;
            // 
            // Cbo_AccType
            // 
            this.Cbo_AccType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_AccType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_AccType.DisplayMember = "0,1";
            this.Cbo_AccType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_AccType.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_AccType.FormattingEnabled = true;
            this.Cbo_AccType.Location = new System.Drawing.Point(116, 117);
            this.Cbo_AccType.Name = "Cbo_AccType";
            this.Cbo_AccType.Size = new System.Drawing.Size(100, 24);
            this.Cbo_AccType.TabIndex = 5;
            this.Cbo_AccType.ValueMember = "0,1";
            // 
            // Cbo_class_id
            // 
            this.Cbo_class_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_class_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_class_id.DisplayMember = "0,1";
            this.Cbo_class_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_class_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_class_id.FormattingEnabled = true;
            this.Cbo_class_id.Items.AddRange(new object[] {
            "مركز",
            "مركز و فروع"});
            this.Cbo_class_id.Location = new System.Drawing.Point(293, 117);
            this.Cbo_class_id.Name = "Cbo_class_id";
            this.Cbo_class_id.Size = new System.Drawing.Size(105, 24);
            this.Cbo_class_id.TabIndex = 6;
            this.Cbo_class_id.ValueMember = "0,1";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(12, 376);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(404, 1);
            this.flowLayoutPanel7.TabIndex = 653;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(13, 302);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(402, 1);
            this.flowLayoutPanel8.TabIndex = 681;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(415, 303);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1, 73);
            this.flowLayoutPanel9.TabIndex = 652;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(12, 305);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(1, 71);
            this.flowLayoutPanel10.TabIndex = 682;
            // 
            // Chk_Acc_Sett
            // 
            this.Chk_Acc_Sett.AutoSize = true;
            this.Chk_Acc_Sett.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Acc_Sett.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Acc_Sett.Location = new System.Drawing.Point(137, 352);
            this.Chk_Acc_Sett.Name = "Chk_Acc_Sett";
            this.Chk_Acc_Sett.Size = new System.Drawing.Size(155, 20);
            this.Chk_Acc_Sett.TabIndex = 683;
            this.Chk_Acc_Sett.Text = "تسوية محاسبية بأثر رجعي";
            this.Chk_Acc_Sett.UseVisualStyleBackColor = true;
            // 
            // TxtRef_acc
            // 
            this.TxtRef_acc.AllowSpace = false;
            this.TxtRef_acc.Location = new System.Drawing.Point(116, 91);
            this.TxtRef_acc.Name = "TxtRef_acc";
            this.TxtRef_acc.Size = new System.Drawing.Size(282, 20);
            this.TxtRef_acc.TabIndex = 4;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.DateSeperator = '/';
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(666, 5);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(230, 20);
            this.TxtIn_Rec_Date.TabIndex = 1;
            this.TxtIn_Rec_Date.Text = "00000000";
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Acc_No_Add_Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 422);
            this.Controls.Add(this.Chk_Acc_Sett);
            this.Controls.Add(this.TxtRef_acc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.flowLayoutPanel10);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.flowLayoutPanel8);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Grdop_acc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cbo_class_id);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TxtOp_acc);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.Grdacc_no);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_Desc_per);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Txt_AccSrch);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Txt_ADesc);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Cbo_AccType);
            this.Controls.Add(this.Txt_EDesc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_AccName_A);
            this.Controls.Add(this.Txt_AccName_E);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_UsrName);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Acc_No_Add_Update";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "164";
            this.Text = "Acc_No_Add_Update";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Acc_No_Add_Update_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Acc_No_Add_Update_FormClosed);
            this.Load += new System.EventHandler(this.Acc_No_Add_Update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grdacc_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grdop_acc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtOp_acc;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MyDateTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.DataGridView Grdacc_no;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_Desc_per;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_AccSrch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_ADesc;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.TextBox Txt_EDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_AccName_A;
        private System.Windows.Forms.TextBox Txt_AccName_E;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.Label label2;
        //   private TCCS_NEW.NumericTextBox txtper_acc;
        private System.Windows.Forms.DataGridView Grdop_acc;
        private System.Windows.Forms.CheckBox label21;
        private System.Windows.Forms.CheckBox label20;
        private System.Windows.Forms.CheckBox label19;
        private System.Windows.Forms.CheckBox label18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.ComboBox Cbo_AccType;
        private System.Windows.Forms.ComboBox Cbo_class_id;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private NumericTextBox TxtRef_acc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.CheckBox Chk_Acc_Sett;

    }
}