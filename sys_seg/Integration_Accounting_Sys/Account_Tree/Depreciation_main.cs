﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Depreciation_main : Form
    {
        bool GrdChange = false;
       public static BindingSource _Bs_Dep_Main = new BindingSource();
        public Depreciation_main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            #region Arabic/Enghlis
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "Acc_EName";
                Column5.DataPropertyName = "db_dep_acc_Ename";
                Column7.DataPropertyName = "cd_dep_acc_Ename";

            }
            #endregion

        }
        //-----------------------------------------------------------------------
        private void Depreciation_main_Load(object sender, EventArgs e)
        {
            GrdAccount_Tree.AutoGenerateColumns = false;
            Txt_Acc.Text = "";
            Txt_Acc_TextChanged(null, null);
        }
        //-----------------------------------------------------------------------
        private void Txt_Acc_TextChanged(object sender, EventArgs e)
        {
            GrdChange = false;
            object[] Sparam = { Txt_Acc.Text.Trim() };
            _Bs_Dep_Main.DataSource = connection.SqlExec("Main_Depreciation", "Dep_Account", Sparam);
            if (connection.SQLDS.Tables["Dep_Account"].Rows.Count > 0)
            {
                GrdAccount_Tree.DataSource = _Bs_Dep_Main;
                GrdChange = true;
                GrdAccount_Tree_SelectionChanged(null, null);
            }

        }
        //-----------------------------------------------------------------------
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Depreciation AddFrm = new Depreciation(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Depreciation_main_Load(sender, e);
            this.Visible = true;
        }
        //-----------------------------------------------------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            Depreciation AddFrm = new Depreciation(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Depreciation_main_Load(sender, e);
            this.Visible = true;
        }
        //-----------------------------------------------------------------------
        private void Depreciation_main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    BtnAdd_Click(sender, e);
                    break;
                case Keys.F7:
                    BtnUpd_Click(sender, e);
                    break;
                //case Keys.F5:
                //    AllBtn_Click(sender, e);
                //    break;
                //case Keys.F4:
                //    AllBtn_Click(sender, e);
                //    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //-----------------------------------------------------------------------
        private void GrdAccount_Tree_SelectionChanged(object sender, EventArgs e)
        {

            if (GrdChange)
            {
                LblRec.Text = connection.Records();

                //Ext_RD.DataBindings.Clear();
                txtper_acc.DataBindings.Clear();
                Dep_RD.DataBindings.Clear();
                Txt_Desc_per.DataBindings.Clear();

                txtper_acc.DataBindings.Add("Text", _Bs_Dep_Main, "dep_per");
                Txt_Desc_per.DataBindings.Add("Text", _Bs_Dep_Main, "A_Description");
                bool DEP = Convert.ToBoolean(((DataRowView)_Bs_Dep_Main.Current).Row["Depreciation"]);
                if (DEP == true)
                {
                    Dep_RD.Checked = true;
                   // Ext_RD.Checked = false;
                }
                else
                {
                    Dep_RD.Checked = false;
                   // Ext_RD.Checked = true;

                }
            }
        }
        //-----------------------------------------------------------------------
        private void Depreciation_main_FormClosed(object sender, FormClosedEventArgs e)
        {

            GrdChange = false;

            string[] Used_Tbl = { "Dep_Account" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Depreciation_main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }
    }
}