﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class Comm_Online_In_Add : Form
    {

        BindingSource BS_Sub_Cust = new BindingSource();
        BindingSource BS_S_CITY_ID = new BindingSource();
        BindingSource BS_S_COUN_ID = new BindingSource();
        BindingSource BS_T_CITY_ID = new BindingSource();
        BindingSource BS_T_COUN_ID = new BindingSource();
        BindingSource BS_PR_Cur_Id = new BindingSource();
        BindingSource Bs_Sub_Cust = new BindingSource();
        BindingSource Bs_cur_id = new BindingSource();
        BindingSource Bs_IN_COMM_CUR = new BindingSource();
        BindingSource Bs_IN_Send_Id = new BindingSource();
        BindingSource Bs_IN_CS_Id = new BindingSource();
        BindingSource _Bs_Grd_comm_info = new BindingSource();
        DataTable DT1 = new DataTable();
        DataTable Comm_TBl = new DataTable();
        DataTable Cust_TBl = new DataTable();
        DataTable Dt_Cust_TBl = new DataTable();
        //-----------------------------------------------------
        public Comm_Online_In_Add()
        {
            InitializeComponent();
            Grd_comm_info.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id == 2)
            {
                cbo_order_type.Items[0] = "Agencies";
                dataGridViewTextBoxColumn3.DataPropertyName = "ESub_CustNmae";
            }
        }

        private void Comm_Online_In_Add_Load(object sender, EventArgs e)
        {
            Create_Table();


            Cbo_S_Con_Id.Enabled = false;
            Cbo_T_Con_ID.Enabled = false;
            Cbo_S_Cit_ID.Enabled = false;
            Cbo_T_Cit_ID.Enabled = false;
            Cbo_PR_Cur_Id.Enabled = false;

            connection.SqlExec("EXEC COMM_ONLINE_IN_SEARCH", "COMM_ONLINE_IN_TBL");

            BS_S_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL"].DefaultView.ToTable(true, "Con_ID", "Con_AName", "Con_EName").Select().CopyToDataTable();
            Cbo_S_Con_Id.DataSource = BS_S_COUN_ID;
            Cbo_S_Con_Id.ValueMember = "Con_ID";
            Cbo_S_Con_Id.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

            BS_S_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL"];
            Cbo_S_Cit_ID.DataSource = BS_S_CITY_ID;
            Cbo_S_Cit_ID.ValueMember = "Cit_ID";
            Cbo_S_Cit_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";

            BS_T_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL1"].DefaultView.ToTable(true, "Con_AName", "Con_EName", "Con_ID").Select().CopyToDataTable();
            Cbo_T_Con_ID.DataSource = BS_T_COUN_ID;
            Cbo_T_Con_ID.ValueMember = "Con_ID";
            Cbo_T_Con_ID.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

            BS_T_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL1"];
            Cbo_T_Cit_ID.DataSource = BS_T_CITY_ID;
            Cbo_T_Cit_ID.ValueMember = "Cit_ID";
            Cbo_T_Cit_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";

            BS_PR_Cur_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL2"];
            Cbo_PR_Cur_Id.DataSource = BS_PR_Cur_Id;
            Cbo_PR_Cur_Id.ValueMember = "cur_id";
            Cbo_PR_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
       
            Bs_cur_id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL4"].DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
            Cbo_R_Cur_Id.DataSource = Bs_cur_id;
            Cbo_R_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
            Cbo_R_Cur_Id.ValueMember = "Cur_id";

            Bs_IN_COMM_CUR.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL4"].DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
            IN_COMM_CUR.DataSource = Bs_IN_COMM_CUR;
            IN_COMM_CUR.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
            IN_COMM_CUR.ValueMember = "Cur_id";

            Bs_IN_CS_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL5"].DefaultView.ToTable(true, "type_rem_ID", "Type_rem_ANAME", "Type_rem_ENAME").Select("type_rem_ID <> 0").CopyToDataTable();
            IN_CS_Id.DataSource = Bs_IN_CS_Id;
            IN_CS_Id.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            IN_CS_Id.ValueMember = "type_rem_ID";

            cbo_order_type.SelectedIndex = 0;
 

        }


      

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (Chk_T_Cit_Flag.Checked == false && Chk_T_Con_Flag.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلــــد او مدينــــة الاستلام" : "you must Select Reciever Country or city", MyGeneral_Lib.LblCap);

                return;
            }
            if (Chk_S_Con_Flag.Checked == true && Convert.ToInt16(Cbo_S_Con_Id.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلــــد الاصـــدار" : "you must Select Issuer Country", MyGeneral_Lib.LblCap);
                Cbo_S_Con_Id.Enabled = true;
                return;
            }

            if (Chk_T_Con_Flag.Checked == true && Convert.ToInt16(Cbo_T_Con_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلـــد الاستـــلام" : "you must Select  Reciever Country", MyGeneral_Lib.LblCap);
                Cbo_T_Con_ID.Enabled = true;
                return;
            }

            if (Chk_S_Cit_Flag.Checked == true && Convert.ToInt16(Cbo_S_Cit_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد مدينــة الاصــدار" : "you must Select Issuer City", MyGeneral_Lib.LblCap);
                Cbo_S_Cit_ID.Enabled = true;
                return;
            }

            if (Chk_T_Cit_Flag.Checked == true && Convert.ToInt16(Cbo_T_Cit_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد مدينــة الاستـــلام" : "you must Select Reciever City", MyGeneral_Lib.LblCap);
                Cbo_T_Cit_ID.Enabled = true;
                return;
            }

            if (Chk_Pr_Cur_Id_Flag.Checked == true && Convert.ToInt16(Cbo_PR_Cur_Id.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد عملة الدفع " : "you must Select Payment currency", MyGeneral_Lib.LblCap);
                Cbo_T_Cit_ID.Enabled = true;
                return;
            }

            DataTable DT_cust_grd = new DataTable();
            try
            {
                DT_cust_grd = Dt_Cust_TBl.DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae", "Order_type").Select("Chk > 0").CopyToDataTable();
                int Order_type = Convert.ToInt16(DT_cust_grd.Rows[0]["Order_type"]);
            }

            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار الوكيل" : "Please select the Agent.", MyGeneral_Lib.LblCap);
                return;
            }

            DataRow DRow = Comm_TBl.NewRow();
            Comm_TBl.Rows.Add(DRow);
            _Bs_Grd_comm_info.DataSource = Comm_TBl;
            //  Grd_comm_info.BeginEdit(true);

        }
        private void Create_Table()
        {

            string[] Column = {"From_Amount","R_Cur_Id","R_CUR_AName","R_CUR_EName","IN_Send_Id","IN_Send_AName","IN_Send_EName"
                                  ,"IN_COMM_CUR","IN_COMM_CUR_AName","IN_COMM_CUR_EName","IN_COMM_PER","IN_COMM_CUT",
                                  "IN_CS_Id","IN_CS_AName","IN_CS_EName","IN_COMM_PER_CS" ,"IN_COMM_CUT_CS" ,"Min_Tot_comm","Max_Tot_comm"};
            string[] DType = {"System.Decimal","System.Int32","System.String","System.String" ,"System.Int16","System.String","System.String"
                                 ,"System.Int16","System.String","System.String","System.Decimal","System.Decimal"
                                 ,"System.Int16","System.String","System.String","System.Decimal","System.Decimal","System.Decimal","System.Decimal"};

            Comm_TBl = CustomControls.Custom_DataTable("Comm_TBl", Column, DType);
            Grd_comm_info.DataSource = _Bs_Grd_comm_info;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            DataTable DT_cust = new DataTable();
            try
            {
                DT_cust = Dt_Cust_TBl.DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae").Select("Chk > 0").CopyToDataTable();
                DT_cust = DT_cust.DefaultView.ToTable(true, "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae").Select().CopyToDataTable();
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار الوكيل" : "Please select the Agent.", MyGeneral_Lib.LblCap);
                return;
            }

            if (Chk_T_Cit_Flag.Checked == false && Chk_T_Con_Flag.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلــــد او مدينــــة الاستلام" : "you must Select Reciever Country or city", MyGeneral_Lib.LblCap);

                return;
            }
            if (Chk_S_Con_Flag.Checked == true && Convert.ToInt16(Cbo_S_Con_Id.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلــــد الاصـــدار" : "you must Select Issuer Country", MyGeneral_Lib.LblCap);
                Cbo_S_Con_Id.Enabled = true;
                return;
            }

            if (Chk_T_Con_Flag.Checked == true && Convert.ToInt16(Cbo_T_Con_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلـــد الاستـــلام" : "you must Select  Reciever Country", MyGeneral_Lib.LblCap);
                Cbo_T_Con_ID.Enabled = true;
                return;
            }

            if (Chk_S_Cit_Flag.Checked == true && Convert.ToInt16(Cbo_S_Cit_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد مدينــة الاصــدار" : "you must Select Issuer City", MyGeneral_Lib.LblCap);
                Cbo_S_Cit_ID.Enabled = true;
                return;
            }

            if (Chk_T_Cit_Flag.Checked == true && Convert.ToInt16(Cbo_T_Cit_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد مدينــة الاستـــلام" : "you must Select Reciever City", MyGeneral_Lib.LblCap);
                Cbo_T_Cit_ID.Enabled = true;
                return;
            }

            if (Chk_Pr_Cur_Id_Flag.Checked == true && Convert.ToInt16(Cbo_PR_Cur_Id.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد عملة الدفع " : "you must Select Payment currency", MyGeneral_Lib.LblCap);
                Cbo_T_Cit_ID.Enabled = true;
                return;
            }

            if (Grd_comm_info.Rows.Count > 0)
            {
                decimal From_Amount = 0;

                try
                {
                    From_Amount = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[0].Value);
                }
                catch { From_Amount = 0; }
                ///////////////////
                int Cbo_R_Cur = 0;
                try
                {
                    Cbo_R_Cur = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[1].Value);
                }
                catch { Cbo_R_Cur = 0; }
                
                int IN_COMM_CUR = 0;
                try
                {
                    IN_COMM_CUR = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[2].Value);
                }
                catch { IN_COMM_CUR = 0; }
                /////////////////////////
                int IN_CS_Id = 0;
                try
                {
                    IN_CS_Id = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[3].Value);
                }
                catch { IN_CS_Id = 0; }
                /////////////////////
                 
                decimal IN_COMM_PER_CS = 0;
                try
                {
                    IN_COMM_PER_CS = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[4].Value);
                }
                catch { IN_COMM_PER_CS = 0; }
                ///////////////////////////

                decimal IN_COMM_CUT_CS = 0;
                try
                {
                    IN_COMM_CUT_CS = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[5].Value);
                }
                catch { IN_COMM_CUT_CS = 0; }

               


                foreach (DataGridViewRow row in Grd_comm_info.Rows)

                    if (From_Amount == 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد قيمه ابتداء العمولة" : "please chek from amount", MyGeneral_Lib.LblCap);

                        return;
                    }
                if (Cbo_R_Cur == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد عملة الحوالة" : "please chek currency of remittence", MyGeneral_Lib.LblCap);

                    return;
                }
               
                if (IN_COMM_CUR == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد عملة العمولة " : "please chek commission currency with sender", MyGeneral_Lib.LblCap);

                    return;
                }
               

                if (IN_CS_Id == 0 && cbo_order_type.SelectedIndex == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد حالة عمولة المصدر مع الادارة" : "please chek commission state with sender", MyGeneral_Lib.LblCap);

                    return;
                }


                //if (IN_COMM_PER_CS == 0 && IN_COMM_CUT_CS == 0 && cbo_order_type.SelectedIndex == 0)
                //{
                //    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد نسبة العمولة او القيمة المقطوعة للعمولة من المصدر مع الادارة" : "please chek commission amount or percentage with sender", MyGeneral_Lib.LblCap);

                //    return;
                //}

            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اضافة العمولات اولا" : "Commissions must be added first", MyGeneral_Lib.LblCap);
                return;
            }


          

            connection.SQLCMD.Parameters.Clear();

            connection.SQLCMD.Parameters.AddWithValue("@Sub_in_TBl_Type", DT_cust);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Con_Flag", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? 1 : Convert.ToByte(Chk_S_Con_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_S_Con_Id", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? ((DataRowView)BS_S_CITY_ID.Current).Row["Con_ID"] : Convert.ToInt32(Cbo_S_Con_Id.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@S_Con_AName", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? ((DataRowView)BS_S_CITY_ID.Current).Row["Con_AName"] : (Convert.ToByte(Chk_S_Con_Flag.Checked) == 1 ? ((DataRowView)BS_S_COUN_ID.Current).Row["Con_AName"] : ""));
            connection.SQLCMD.Parameters.AddWithValue("@S_Con_EName", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? ((DataRowView)BS_S_CITY_ID.Current).Row["Con_EName"] : (Convert.ToByte(Chk_S_Con_Flag.Checked) == 1 ? ((DataRowView)BS_S_COUN_ID.Current).Row["Con_EName"] : ""));

            connection.SQLCMD.Parameters.AddWithValue("@Chk_T_Con_Flag", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? 1 : Convert.ToByte(Chk_T_Con_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_T_Con_ID", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? ((DataRowView)BS_T_CITY_ID.Current).Row["Con_ID"] : (Convert.ToInt32(Cbo_T_Con_ID.SelectedValue)));
            connection.SQLCMD.Parameters.AddWithValue("@T_Con_AName", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? ((DataRowView)BS_T_CITY_ID.Current).Row["Con_AName"] : (Convert.ToByte(Chk_T_Con_Flag.Checked) == 1 ? ((DataRowView)BS_T_COUN_ID.Current).Row["Con_AName"] : ""));
            connection.SQLCMD.Parameters.AddWithValue("@T_Con_EName", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? ((DataRowView)BS_T_CITY_ID.Current).Row["Con_EName"] : (Convert.ToByte(Chk_T_Con_Flag.Checked) == 1 ? ((DataRowView)BS_T_COUN_ID.Current).Row["Con_EName"] : ""));


            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Cit_Flag", Convert.ToByte(Chk_S_Cit_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_S_Cit_ID", Convert.ToInt32(Cbo_S_Cit_ID.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@S_Cit_AName", Convert.ToByte(Chk_S_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_S_CITY_ID.Current).Row["Cit_AName"] : "");
            connection.SQLCMD.Parameters.AddWithValue("@S_Cit_EName", Convert.ToByte(Chk_S_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_S_CITY_ID.Current).Row["Cit_EName"] : "");


            connection.SQLCMD.Parameters.AddWithValue("@Chk_T_Cit_Flag", Convert.ToByte(Chk_T_Cit_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_T_Cit_ID", Convert.ToInt32(Cbo_T_Cit_ID.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@T_Cit_AName", Convert.ToByte(Chk_T_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_T_CITY_ID.Current).Row["Cit_AName"] : "");
            connection.SQLCMD.Parameters.AddWithValue("@T_Cit_EName", Convert.ToByte(Chk_T_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_T_CITY_ID.Current).Row["Cit_EName"] : "");

            connection.SQLCMD.Parameters.AddWithValue("@Chk_PR_CUR_ID_Flag", Convert.ToByte(Chk_Pr_Cur_Id_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_PR_CUR_ID", Convert.ToInt32(Cbo_PR_Cur_Id.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@PR_CUR_AName", Convert.ToByte(Chk_Pr_Cur_Id_Flag.Checked) == 1 ? ((DataRowView)BS_PR_Cur_Id.Current).Row["Cur_AName"] : "");
            connection.SQLCMD.Parameters.AddWithValue("@PR_CUR_EName", Convert.ToByte(Chk_Pr_Cur_Id_Flag.Checked) == 1 ? ((DataRowView)BS_PR_Cur_Id.Current).Row["Cur_EName"] : "");

            connection.SQLCMD.Parameters.AddWithValue("@Comm_In_TBl_Type", Comm_TBl);
            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 250).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

            connection.SqlExec("Comm_Online_IN_AddUpd", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            MessageBox.Show(connection.Lang_id == 1 ? "تمت الاضافة بنجاح" : "Successfully added", MyGeneral_Lib.LblCap);
            // return;
            Grd_comm_info.DataSource = new BindingSource();
            Chk_Pr_Cur_Id_Flag.Checked = false;
            Cbo_PR_Cur_Id.SelectedIndex = 0;
            Chk_S_Cit_Flag.Checked = false;
            Cbo_S_Cit_ID.SelectedIndex = 0;
            Chk_S_Con_Flag.Checked = false;
            Cbo_S_Con_Id.SelectedIndex = 0;
            Chk_T_Cit_Flag.Checked = false;
            Cbo_T_Cit_ID.SelectedIndex = 0;
            Chk_T_Con_Flag.Checked = false;
            Cbo_T_Con_ID.SelectedIndex = 0;
            Txt_Sub_Cust.Text = "";
            Comm_TBl.Reset();
            Create_Table();
            foreach (DataRow row in Dt_Cust_TBl.Rows)
            {

                row["Chk"] = 0;

            }
            connection.SQLCMD.Parameters.Clear();

        }
        //----------------------------------------------
        private void Chk_S_Con_Flag_CheckedChanged_1(object sender, EventArgs e)
        {
            if (Chk_S_Con_Flag.Checked == true)
            {
                Chk_S_Cit_Flag.Checked = false;
                Cbo_S_Con_Id.Enabled = true;
                Cbo_S_Cit_ID.Enabled = false;
            }
            else
            {
                Cbo_S_Con_Id.SelectedValue = 0;
                Cbo_S_Con_Id.Enabled = false;
            }
        }
        //----------------------------------------------
        private void Chk_S_Cit_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_S_Cit_Flag.Checked == true)
            {
                Chk_S_Con_Flag.Checked = false;
                Cbo_S_Con_Id.Enabled = false;
                Cbo_S_Cit_ID.Enabled = true;
            }
            else
            {
                Cbo_S_Cit_ID.SelectedValue = 0;
                Cbo_S_Cit_ID.Enabled = false;
            }
        }
        //----------------------------------------------
        private void Chk_T_Con_Flag_CheckedChanged_1(object sender, EventArgs e)
        {
            if (Chk_T_Con_Flag.Checked == true)
            {
                Chk_T_Cit_Flag.Checked = false;
                Cbo_T_Con_ID.Enabled = true;
                Cbo_T_Cit_ID.Enabled = false;
            }
            else
            {
                Cbo_T_Con_ID.SelectedValue = 0;
                Cbo_T_Con_ID.Enabled = false;
            }
        }
        //----------------------------------------------
        private void Chk_T_Cit_Flag_CheckedChanged_1(object sender, EventArgs e)
        {
            if (Chk_T_Cit_Flag.Checked == true)
            {
                Chk_T_Con_Flag.Checked = false;
                Cbo_T_Con_ID.Enabled = false;
                Cbo_T_Cit_ID.Enabled = true;
            }
            else
            {
                Cbo_T_Cit_ID.SelectedValue = 0;
                Cbo_T_Cit_ID.Enabled = false;
            }
        }
        //----------------------------------------------
        private void Chk_Pr_Cur_Id_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Pr_Cur_Id_Flag.Checked == true)
            { Cbo_PR_Cur_Id.Enabled = true; }
            else
            {
                Cbo_PR_Cur_Id.Enabled = false;
                Cbo_PR_Cur_Id.SelectedValue = 0;
                Cbo_PR_Cur_Id.Enabled = false;
            }
        }

        private void Grd_comm_info_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            if (Grd_comm_info.CurrentCell.ColumnIndex == Cbo_R_Cur_Id.Index)
            {
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["R_Cur_Id"] = ((DataRowView)Bs_cur_id.Current).Row["Cur_Id"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["R_CUR_AName"] = ((DataRowView)Bs_cur_id.Current).Row["Cur_ANAME"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["R_CUR_EName"] = ((DataRowView)Bs_cur_id.Current).Row["Cur_ENAME"];
            }
            //--------------------------------------------------------------------------------------------------

           


            if (Grd_comm_info.CurrentCell.ColumnIndex == IN_COMM_CUR.Index)
            {
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["IN_COMM_CUR"] = ((DataRowView)Bs_IN_COMM_CUR.Current).Row["Cur_Id"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["IN_COMM_CUR_AName"] = ((DataRowView)Bs_IN_COMM_CUR.Current).Row["Cur_ANAME"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["IN_COMM_CUR_EName"] = ((DataRowView)Bs_IN_COMM_CUR.Current).Row["Cur_ENAME"];
            }
            //---------------------------------------------------------------------------------------------------
            //if(Grd_comm_info.CurrentCell.ColumnIndex == IN_CS_Id.Index)
            //{
            if (Grd_comm_info.CurrentCell.ColumnIndex == IN_CS_Id.Index)
            {
                if (cbo_order_type.SelectedIndex == 0)
                {
                    ((DataRowView)_Bs_Grd_comm_info.Current).Row["IN_CS_Id"] = ((DataRowView)Bs_IN_CS_Id.Current).Row["type_rem_ID"];
                    ((DataRowView)_Bs_Grd_comm_info.Current).Row["IN_CS_AName"] = ((DataRowView)Bs_IN_CS_Id.Current).Row["Type_rem_ANAME"];
                    ((DataRowView)_Bs_Grd_comm_info.Current).Row["IN_CS_EName"] = ((DataRowView)Bs_IN_CS_Id.Current).Row["Type_rem_ENAME"];
                }
            }
        }

        private void Txt_CUST_TextChanged(object sender, EventArgs e)
        {
            Get_details_Info();
        }

        private void Get_details_Info()
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);

                

                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "( Sub_Cust_ID  = " + Sub_cust_id + " OR ASub_CustName like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " Sub_Cust_ID > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Comm_TBl.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show("هل تريد بالتأكيد حذف القيد " + (Grd_comm_info.CurrentRow.Index + 1), MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {

                    Comm_TBl.Rows[Grd_comm_info.CurrentRow.Index].Delete();

                }
                if (Comm_TBl.Rows.Count <= 0)
                {

                }
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void cbo_order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                 Dt_Cust_TBl = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL4"].DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae", "Order_type").Select("Order_type = 1").CopyToDataTable();
                Dt_Cust_TBl.TableName = "Dt_Cust_TBl";
                Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                Grd_Sub_Cust.DataSource = Bs_Sub_Cust;

              
                
                if (Comm_TBl.Rows.Count > 0)
                {
                   
                    Comm_TBl.Clear();  

                 
                }
            
        }

        private void Comm_Online_In_Add_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "COMM_ONLINE_IN_TBL", "COMM_ONLINE_IN_TBL1", "COMM_ONLINE_IN_TBL2", "COMM_ONLINE_IN_TBL4", "COMM_ONLINE_IN_TBL5" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

  
        

       
    }
}
