﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Comm_Online_Out_Add_New2 : Form
    {
        BindingSource BS_Sub_Cust = new BindingSource();
        BindingSource BS_S_CITY_ID = new BindingSource();
        BindingSource BS_S_COUN_ID = new BindingSource();
        BindingSource BS_T_CITY_ID = new BindingSource();
        BindingSource BS_T_COUN_ID = new BindingSource();
        BindingSource BS_PR_Cur_Id = new BindingSource();
        BindingSource Bs_Sub_Cust = new BindingSource();
        BindingSource Bs_cur_id = new BindingSource();
        BindingSource Bs_OUT_COMM_CUR = new BindingSource();
        BindingSource Bs_OUT_Send_Id = new BindingSource();
        BindingSource Bs_OUT_CS_Id = new BindingSource();

        DataTable DT1 = new DataTable();
        BindingSource _Bs_Grd_comm_info = new BindingSource();

        DataTable Comm_TBl = new DataTable();
        DataTable Cust_TBl = new DataTable();
        DataTable Dt_Cust_TBl = new DataTable();
        DataTable DT_Con_Sys_Id = new DataTable();
        DataTable DT_Cit_Sys_Id = new DataTable();
        //-----------------------------------------------------new online
        BindingSource Bs_OTO_Out_Id = new BindingSource();

        public Comm_Online_Out_Add_New2()
        {
            InitializeComponent();
            Grd_comm_info.AutoGenerateColumns = false;
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            if (connection.Lang_id == 2)
            {
                cbo_order_type.Items[0] = "companies";
                cbo_order_type.Items[1] = "Agencies";
                dataGridViewTextBoxColumn3.DataPropertyName = "ESub_CustNmae";
            }
        }

        private void Create_Table()
        {

            string[] Column = {"From_Amount","R_Cur_Id","R_CUR_AName","R_CUR_EName","OUT_Send_Id","OUT_Send_AName","OUT_Send_EName"
                                  ,"OUT_COMM_CUR","Out_COMM_CUR_AName","Out_COMM_CUR_EName","OUT_COMM_PER","OUT_COMM_CUT",
                                  "OUT_CS_Id","OUT_CS_AName","OUT_CS_EName","OUT_COMM_PER_CS" ,"OUT_COMM_CUT_CS" ,
                                  "OTO_Out_Id", "OTO_Out_AName", "OTO_Out_EName", "OTO_Out_COMM_PER", "OTO_Out_COMM_CUT",
                                  "Min_Tot_comm","Max_Tot_comm"};
            string[] DType = {"System.Decimal","System.Int32","System.String","System.String" ,"System.Int16","System.String","System.String"
                                 ,"System.Int16","System.String","System.String","System.Decimal","System.Decimal"
                                 ,"System.Int16","System.String","System.String","System.Decimal","System.Decimal",
                                 "System.Int16","System.String","System.String","System.Decimal","System.Decimal",
                                 "System.Decimal","System.Decimal"};

            Comm_TBl = CustomControls.Custom_DataTable("Comm_TBl", Column, DType);
            Grd_comm_info.DataSource = _Bs_Grd_comm_info;
        }

        private void Comm_Online_Out_Add_New2_Load(object sender, EventArgs e)
        {
            Create_Table();

            Cbo_S_Con_Id.Enabled = false;
            Cbo_T_Con_ID.Enabled = false;
            Cbo_S_Cit_ID.Enabled = false;
            Cbo_T_Cit_ID.Enabled = false;
            Cbo_PR_Cur_Id.Enabled = false;

            connection.SqlExec("EXEC COMM_ONLINE_OUT_SEARCH", "COMM_ONLINE_OUT_TBL");

            BS_S_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_TBL"].DefaultView.ToTable(true, "COUN_ID", "Con_AName", "Con_EName").Select().CopyToDataTable();
            Cbo_S_Con_Id.DataSource = BS_S_COUN_ID;
            Cbo_S_Con_Id.ValueMember = "COUN_ID";
            Cbo_S_Con_Id.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

            BS_S_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_TBL"];
            Cbo_S_Cit_ID.DataSource = BS_S_CITY_ID;
            Cbo_S_Cit_ID.ValueMember = "CITY_ID";
            Cbo_S_Cit_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";

            BS_T_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_TBL1"].DefaultView.ToTable(true, "Con_AName", "Con_EName", "Con_ID").Select().CopyToDataTable();
            Cbo_T_Con_ID.DataSource = BS_T_COUN_ID;
            Cbo_T_Con_ID.ValueMember = "Con_ID";
            Cbo_T_Con_ID.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

            BS_T_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_TBL1"];
            Cbo_T_Cit_ID.DataSource = BS_T_CITY_ID;
            Cbo_T_Cit_ID.ValueMember = "Cit_ID";
            Cbo_T_Cit_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";

            BS_PR_Cur_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_TBL2"];
            Cbo_PR_Cur_Id.DataSource = BS_PR_Cur_Id;
            Cbo_PR_Cur_Id.ValueMember = "cur_id";
            Cbo_PR_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";

            connection.SqlExec("Exec Comm_Get_Sub_Cust", "Cur_Comm_Tbl");

            Bs_cur_id.DataSource = connection.SQLDS.Tables["Cur_Comm_Tbl"].DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
            Cbo_R_Cur_Id.DataSource = Bs_cur_id;
            Cbo_R_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
            Cbo_R_Cur_Id.ValueMember = "Cur_id";

            Bs_OUT_COMM_CUR.DataSource = connection.SQLDS.Tables["Cur_Comm_Tbl"].DefaultView.ToTable(true, "Cur_id", "Cur_Aname", "Cur_Ename").Select().CopyToDataTable();
            OUT_COMM_CUR.DataSource = Bs_OUT_COMM_CUR;
            OUT_COMM_CUR.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
            OUT_COMM_CUR.ValueMember = "Cur_id";

            Bs_OUT_Send_Id.DataSource = connection.SQLDS.Tables["Cur_Comm_Tbl1"].Select().CopyToDataTable().DefaultView.ToTable(true, "type_rem_ID", "Type_rem_ANAME", "Type_rem_ENAME").Select("type_rem_ID <> 0").CopyToDataTable();
            OUT_Send_Id.DataSource = Bs_OUT_Send_Id;
            OUT_Send_Id.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            OUT_Send_Id.ValueMember = "type_rem_ID";

            Bs_OUT_CS_Id.DataSource = connection.SQLDS.Tables["Cur_Comm_Tbl1"].DefaultView.ToTable(true, "type_rem_ID", "Type_rem_ANAME", "Type_rem_ENAME").Select("type_rem_ID <> 0").CopyToDataTable();
            OUT_CS_Id.DataSource = Bs_OUT_CS_Id;
            OUT_CS_Id.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            OUT_CS_Id.ValueMember = "type_rem_ID";

            Bs_OTO_Out_Id.DataSource = connection.SQLDS.Tables["Cur_Comm_Tbl1"].DefaultView.ToTable(true, "type_rem_ID", "Type_rem_ANAME", "Type_rem_ENAME").Select("type_rem_ID <> 0").CopyToDataTable();
            OTO_Out_Id.DataSource = Bs_OTO_Out_Id;
            OTO_Out_Id.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            OTO_Out_Id.ValueMember = "type_rem_ID";

            cbo_order_type.SelectedIndex = 0;
            //cbo_order_type_SelectedIndexChanged(null, null);
            //OUT_Send_Id.Visible = true;
            //Column6.Visible = true;
            //Column7.Visible = true;

            //OUT_CS_Id.Visible = true;
            //Column9.Visible = true;
            //Column1.Visible = true;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            DataTable DT_cust_grd = new DataTable();
            try
            {
                DT_cust_grd = Dt_Cust_TBl.DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae", "Order_type").Select("Chk > 0").CopyToDataTable();
                int Order_type = Convert.ToInt16(DT_cust_grd.Rows[0]["Order_type"]);
                if (Order_type == 0) //الفرع
                {
                    OUT_Send_Id.Visible = true;
                    Column6.Visible = true;
                    Column7.Visible = true;

                    OUT_CS_Id.Visible = true;
                    Column9.Visible = true;
                    Column1.Visible = true;
                }
                else //الوكيل
                {
                    OUT_CS_Id.Visible = true;
                    Column9.Visible = true;
                    Column1.Visible = true;

                    OUT_Send_Id.Visible = true;
                    Column6.Visible = true;
                    Column7.Visible = true;
                }
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار الوكيل" : "Please select the Agent.", MyGeneral_Lib.LblCap);
                return;
            }

            DataRow DRow = Comm_TBl.NewRow();
            Comm_TBl.Rows.Add(DRow);
            _Bs_Grd_comm_info.DataSource = Comm_TBl;
            Cbo_T_Con_ID_SelectedIndexChanged(null, null);
            Cbo_T_Cit_ID_SelectedIndexChanged(null, null);
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (Chk_S_Cit_Flag.Checked == false && Chk_S_Con_Flag.Checked == false)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلــــد او مدينــــة الاصـــدار" : "you must Select Issuer Country or city", MyGeneral_Lib.LblCap);
                return;
            }
            if (Chk_S_Con_Flag.Checked == true && Convert.ToInt16(Cbo_S_Con_Id.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلــــد الاصـــدار" : "you must Select Issuer Country", MyGeneral_Lib.LblCap);
                Cbo_S_Con_Id.Enabled = true;
                return;
            }

            if (Chk_T_Con_Flag.Checked == true && Convert.ToInt16(Cbo_T_Con_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد بلـــد الاستـــلام" : "you must Select  Reciever Country", MyGeneral_Lib.LblCap);
                Cbo_T_Con_ID.Enabled = true;
                return;
            }

            if (Chk_S_Cit_Flag.Checked == true && Convert.ToInt16(Cbo_S_Cit_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد مدينــة الاصــدار" : "you must Select Issuer City", MyGeneral_Lib.LblCap);
                Cbo_S_Cit_ID.Enabled = true;
                return;
            }

            if (Chk_T_Cit_Flag.Checked == true && Convert.ToInt16(Cbo_T_Cit_ID.SelectedValue) == 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "حدد مدينــة الاستـــلام" : "you must Select Reciever City", MyGeneral_Lib.LblCap);
                Cbo_T_Cit_ID.Enabled = true;
                return;
            }
            if (Grd_comm_info.Rows.Count > 0)
            {
                decimal From_Amount = 0;

                try
                {
                    From_Amount = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[0].Value);
                }
                catch { From_Amount = 0; }
                int Cbo_R_Cur = 0;
                try
                {
                    Cbo_R_Cur = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[1].Value);
                }
                catch { Cbo_R_Cur = 0; }
                int OUT_Send = 0;
                try
                {
                    OUT_Send = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[2].Value);
                }
                catch { OUT_Send = 0; }
                int OUT_COMM_CUR = 0;
                try
                {
                    OUT_COMM_CUR = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[3].Value);
                }
                catch { OUT_COMM_CUR = 0; }
                decimal OUT_COMM_PER = 0;
                try
                {
                    OUT_COMM_PER = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[4].Value);
                }
                catch { OUT_COMM_PER = 0; }
                decimal OUT_COMM_CUT = 0;
                try
                {
                    OUT_COMM_CUT = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[5].Value);
                }
                catch { OUT_COMM_CUT = 0; }

                decimal OUT_COMM_PER_CS = 0;
                try
                {
                    OUT_COMM_PER_CS = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[7].Value);
                }
                catch { OUT_COMM_PER_CS = 0; }

                decimal OUT_COMM_CUT_CS = 0;
                try
                {
                    OUT_COMM_CUT_CS = Convert.ToDecimal(Grd_comm_info.Rows[0].Cells[8].Value);
                }
                catch { OUT_COMM_CUT_CS = 0; }

                int OUT_CS_Id = 0;
                try
                {
                    OUT_CS_Id = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[6].Value);
                }
                catch { OUT_CS_Id = 0; }

                int OUT_OTO_Id = 0;
                try
                {
                    OUT_OTO_Id = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[9].Value);
                }
                catch { OUT_OTO_Id = 0; }

                int OTO_Out_COM_PER = 0;
                try
                {
                    OTO_Out_COM_PER = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[10].Value);
                }
                catch { OTO_Out_COM_PER = 0; }

                int OTO_Out_COM_CUT = 0;
                try
                {
                    OTO_Out_COM_CUT = Convert.ToInt16(Grd_comm_info.Rows[0].Cells[11].Value);
                }
                catch { OTO_Out_COM_CUT = 0; }

                foreach (DataGridViewRow row in Grd_comm_info.Rows)

                    if (From_Amount == 0)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد قيمه ابتداء العمولة" : "please chek from amount", MyGeneral_Lib.LblCap);

                        return;
                    }

                if (Cbo_R_Cur == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد عملة الحوالة" : "please chek currency of remittence", MyGeneral_Lib.LblCap);

                    return;
                }
                if (OUT_Send == 0 && cbo_order_type.SelectedIndex == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد حالة العمولة مع المرسل" : "please chek commission state with sender", MyGeneral_Lib.LblCap);

                    return;
                }
                if (OUT_COMM_CUR == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد عملة العمولة " : "please chek commission currency with sender", MyGeneral_Lib.LblCap);

                    return;
                }

                if (OUT_CS_Id == 0 && cbo_order_type.SelectedIndex == 1)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "يرجى تحديد حالة عمولة المصدر مع الادارة" : "please chek commission state with sender", MyGeneral_Lib.LblCap);

                    return;
                }
            }

            DataTable DT_cust = new DataTable();
            try
            {
                DT_cust = Dt_Cust_TBl.DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae").Select("Chk > 0").CopyToDataTable();
                DT_cust = DT_cust.DefaultView.ToTable(true, "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae").Select().CopyToDataTable();
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار الوكيل" : "Please select the Agent.", MyGeneral_Lib.LblCap);
                return;
            }

            connection.SQLCMD.Parameters.Clear();

            connection.SQLCMD.Parameters.AddWithValue("@Sub_TBl_Type", DT_cust);
            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Con_Flag", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? 1 : Convert.ToByte(Chk_S_Con_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_S_Con_Id", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? ((DataRowView)BS_S_CITY_ID.Current).Row["COUN_ID"] : Convert.ToInt32(Cbo_S_Con_Id.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@S_Con_AName", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? ((DataRowView)BS_S_CITY_ID.Current).Row["Con_AName"] : (Convert.ToByte(Chk_S_Con_Flag.Checked) == 1 ? ((DataRowView)BS_S_COUN_ID.Current).Row["Con_AName"] : ""));
            connection.SQLCMD.Parameters.AddWithValue("@S_Con_EName", Convert.ToBoolean(Chk_S_Cit_Flag.Checked) == true ? ((DataRowView)BS_S_CITY_ID.Current).Row["Con_EName"] : (Convert.ToByte(Chk_S_Con_Flag.Checked) == 1 ? ((DataRowView)BS_S_COUN_ID.Current).Row["Con_EName"] : ""));

            connection.SQLCMD.Parameters.AddWithValue("@Chk_T_Con_Flag", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? 1 : Convert.ToByte(Chk_T_Con_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_T_Con_ID", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? ((DataRowView)BS_T_CITY_ID.Current).Row["Con_id"] : (Convert.ToInt32(Cbo_T_Con_ID.SelectedValue)));
            connection.SQLCMD.Parameters.AddWithValue("@T_Con_AName", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? ((DataRowView)BS_T_CITY_ID.Current).Row["Con_AName"] : (Convert.ToByte(Chk_T_Con_Flag.Checked) == 1 ? ((DataRowView)BS_T_COUN_ID.Current).Row["Con_AName"] : ""));
            connection.SQLCMD.Parameters.AddWithValue("@T_Con_EName", Convert.ToBoolean(Chk_T_Cit_Flag.Checked) == true ? ((DataRowView)BS_T_CITY_ID.Current).Row["Con_EName"] : (Convert.ToByte(Chk_T_Con_Flag.Checked) == 1 ? ((DataRowView)BS_T_COUN_ID.Current).Row["Con_EName"] : ""));

            connection.SQLCMD.Parameters.AddWithValue("@Chk_S_Cit_Flag", Convert.ToByte(Chk_S_Cit_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_S_Cit_ID", Convert.ToInt32(Cbo_S_Cit_ID.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@S_Cit_AName", Convert.ToByte(Chk_S_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_S_CITY_ID.Current).Row["Cit_AName"] : "");
            connection.SQLCMD.Parameters.AddWithValue("@S_Cit_EName", Convert.ToByte(Chk_S_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_S_CITY_ID.Current).Row["Cit_EName"] : "");

            connection.SQLCMD.Parameters.AddWithValue("@Chk_T_Cit_Flag", Convert.ToByte(Chk_T_Cit_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_T_Cit_ID", Convert.ToInt32(Cbo_T_Cit_ID.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@T_Cit_AName", Convert.ToByte(Chk_T_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_T_CITY_ID.Current).Row["Cit_AName"] : "");
            connection.SQLCMD.Parameters.AddWithValue("@T_Cit_EName", Convert.ToByte(Chk_T_Cit_Flag.Checked) == 1 ? ((DataRowView)BS_T_CITY_ID.Current).Row["Cit_EName"] : "");

            connection.SQLCMD.Parameters.AddWithValue("@Chk_PR_CUR_ID_Flag", Convert.ToByte(Chk_Pr_Cur_Id_Flag.Checked));
            connection.SQLCMD.Parameters.AddWithValue("@Cbo_PR_CUR_ID", Convert.ToInt32(Cbo_PR_Cur_Id.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@PR_CUR_AName", Convert.ToByte(Chk_Pr_Cur_Id_Flag.Checked) == 1 ? ((DataRowView)BS_PR_Cur_Id.Current).Row["Cur_AName"] : "");
            connection.SQLCMD.Parameters.AddWithValue("@PR_CUR_EName", Convert.ToByte(Chk_Pr_Cur_Id_Flag.Checked) == 1 ? ((DataRowView)BS_PR_Cur_Id.Current).Row["Cur_EName"] : "");

            connection.SQLCMD.Parameters.AddWithValue("@Comm_TBl_Type", Comm_TBl);
            connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 250).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;

            connection.SqlExec("Comm_Online_Out_AddUpd", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            MessageBox.Show(connection.Lang_id == 1 ? "تمت الاضافة بنجاح" : "Successfully added", MyGeneral_Lib.LblCap);

            Grd_comm_info.DataSource = new BindingSource();
            Chk_Pr_Cur_Id_Flag.Checked = false;
            Cbo_PR_Cur_Id.SelectedIndex = 0;
            Chk_S_Cit_Flag.Checked = false;
            Cbo_S_Cit_ID.SelectedIndex = 0;
            Chk_S_Con_Flag.Checked = false;
            Cbo_S_Con_Id.SelectedIndex = 0;
            Chk_T_Cit_Flag.Checked = false;
            Cbo_T_Cit_ID.SelectedIndex = 0;
            Chk_T_Con_Flag.Checked = false;
            Cbo_T_Con_ID.SelectedIndex = 0;
            Txt_Sub_Cust.Text = "";
            Comm_TBl.Reset();
            Create_Table();
            foreach (DataRow row in Dt_Cust_TBl.Rows)
            {
                row["Chk"] = 0;
            }
            connection.SQLCMD.Parameters.Clear();
        }

        private void Chk_S_Con_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_S_Con_Flag.Checked == true)
            {
                Chk_S_Cit_Flag.Checked = false;
                Cbo_S_Con_Id.Enabled = true;
                Cbo_S_Cit_ID.Enabled = false;
            }
            else
            {
                Cbo_S_Con_Id.SelectedValue = 0;
                Cbo_S_Con_Id.Enabled = false;
            }
        }

        private void Chk_S_Cit_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_S_Cit_Flag.Checked == true)
            {
                Chk_S_Con_Flag.Checked = false;
                Cbo_S_Con_Id.Enabled = false;
                Cbo_S_Cit_ID.Enabled = true;
            }
            else
            {
                Cbo_S_Cit_ID.SelectedValue = 0;
                Cbo_S_Cit_ID.Enabled = false;
            }
        }

        private void Chk_T_Con_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_T_Con_Flag.Checked == true)
            {
                Chk_T_Cit_Flag.Checked = false;
                Cbo_T_Con_ID.Enabled = true;
                Cbo_T_Cit_ID.Enabled = false;
            }
            else
            {
                Cbo_T_Con_ID.SelectedValue = 0;
                Cbo_T_Con_ID.Enabled = false;
            }
        }

        private void Chk_T_Cit_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_T_Cit_Flag.Checked == true)
            {
                Chk_T_Con_Flag.Checked = false;
                Cbo_T_Con_ID.Enabled = false;
                Cbo_T_Cit_ID.Enabled = true;
            }
            else
            {
                Cbo_T_Cit_ID.SelectedValue = 0;
                Cbo_T_Cit_ID.Enabled = false;
            }
        }

        private void Chk_Pr_Cur_Id_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Chk_Pr_Cur_Id_Flag.Checked == true)
            { Cbo_PR_Cur_Id.Enabled = true; }
            else
            {
                Cbo_PR_Cur_Id.Enabled = false;
                Cbo_PR_Cur_Id.SelectedValue = 0;
                Cbo_PR_Cur_Id.Enabled = false;
            }
        }

        private void Grd_comm_info_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (Grd_comm_info.CurrentCell.ColumnIndex == Cbo_R_Cur_Id.Index)
            {
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["R_Cur_Id"] = ((DataRowView)Bs_cur_id.Current).Row["Cur_Id"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["R_CUR_AName"] = ((DataRowView)Bs_cur_id.Current).Row["Cur_ANAME"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["R_CUR_EName"] = ((DataRowView)Bs_cur_id.Current).Row["Cur_ENAME"];
            }
            //--------------------------------------------------------------------------------------------------
            if (Grd_comm_info.CurrentCell.ColumnIndex == OUT_Send_Id.Index)
            {
                //if (cbo_order_type.SelectedIndex == 0)
                //{
                    ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_Send_Id"] = ((DataRowView)Bs_OUT_Send_Id.Current).Row["type_rem_ID"];
                    ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_Send_AName"] = ((DataRowView)Bs_OUT_Send_Id.Current).Row["Type_rem_ANAME"];
                    ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_Send_EName"] = ((DataRowView)Bs_OUT_Send_Id.Current).Row["Type_rem_ENAME"];
                //}
            }
            //---------------------------------------------------------------------------------------------------
            if (Grd_comm_info.CurrentCell.ColumnIndex == OUT_COMM_CUR.Index)
            {
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_COMM_CUR"] = ((DataRowView)Bs_OUT_COMM_CUR.Current).Row["Cur_Id"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["Out_COMM_CUR_AName"] = ((DataRowView)Bs_OUT_COMM_CUR.Current).Row["Cur_ANAME"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["Out_COMM_CUR_EName"] = ((DataRowView)Bs_OUT_COMM_CUR.Current).Row["Cur_ENAME"];
            }
            //---------------------------------------------------------------------------------------------------
            if (Grd_comm_info.CurrentCell.ColumnIndex == OUT_CS_Id.Index)
            {
                //if (cbo_order_type.SelectedIndex == 1)
                //{
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_CS_Id"] = ((DataRowView)Bs_OUT_CS_Id.Current).Row["type_rem_ID"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_CS_AName"] = ((DataRowView)Bs_OUT_CS_Id.Current).Row["Type_rem_ANAME"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OUT_CS_EName"] = ((DataRowView)Bs_OUT_CS_Id.Current).Row["Type_rem_ENAME"];
                //}
            }
            //------------------------------------------------------------------------------------------------------
            if (Grd_comm_info.CurrentCell.ColumnIndex == OTO_Out_Id.Index)
            {
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OTO_Out_Id"] = ((DataRowView)Bs_OTO_Out_Id.Current).Row["type_rem_ID"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OTO_Out_AName"] = ((DataRowView)Bs_OTO_Out_Id.Current).Row["Type_rem_ANAME"];
                ((DataRowView)_Bs_Grd_comm_info.Current).Row["OTO_Out_EName"] = ((DataRowView)Bs_OTO_Out_Id.Current).Row["Type_rem_ENAME"];
            }
        }

        private void Txt_Sub_Cust_TextChanged(object sender, EventArgs e)
        {
            Get_details_Info();
        }

        private void Get_details_Info()
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);

                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "( Sub_Cust_ID  = " + Sub_cust_id + " OR ASub_CustName like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " Sub_Cust_ID > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Comm_TBl.Rows.Count > 0)
            {
                DialogResult Dr = MessageBox.Show("هل تريد بالتأكيد حذف القيد " + (Grd_comm_info.CurrentRow.Index + 1), MyGeneral_Lib.LblCap, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dr == DialogResult.Yes)
                {

                    Comm_TBl.Rows[Grd_comm_info.CurrentRow.Index].Delete();

                }
                if (Comm_TBl.Rows.Count <= 0)
                {

                }
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Comm_Online_Out_Add_New2_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "COMM_ONLINE_OUT_TBL", "COMM_ONLINE_OUT_TBL1", "COMM_ONLINE_OUT_TBL2", "Cur_Comm_Tbl", "Cur_Comm_Tbl1", "Cur_Comm_Tbl2" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void cbo_order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_order_type.SelectedIndex == 0)
            {
                Dt_Cust_TBl = connection.SQLDS.Tables["Cur_Comm_Tbl"].DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae", "Order_type").Select(" Order_type = 0 ").CopyToDataTable();
                Dt_Cust_TBl.TableName = "Dt_Cust_TBl";
                Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                Grd_Sub_Cust.DataSource = Bs_Sub_Cust;

                if (Comm_TBl.Rows.Count > 0)
                {
                    Comm_TBl.Clear();
                }
            }
            else
            {
                Dt_Cust_TBl = connection.SQLDS.Tables["Cur_Comm_Tbl"].DefaultView.ToTable(true, "Chk", "Sub_Cust_ID", "ASub_CustName", "ESub_CustNmae", "Order_type").Select("Order_type = 1").CopyToDataTable();
                Dt_Cust_TBl.TableName = "Dt_Cust_TBl";
                Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                Grd_Sub_Cust.DataSource = Bs_Sub_Cust;

                if (Comm_TBl.Rows.Count > 0)
                {
                    Comm_TBl.Clear();
                }
            }
            Cbo_T_Con_ID_SelectedIndexChanged(null,null);
            Cbo_T_Cit_ID_SelectedIndexChanged(null, null);
        }

        private void Cbo_T_Con_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Chk_T_Con_Flag.Checked == true)
            {
                Int16 CON_ID = Convert.ToInt16(Cbo_T_Con_ID.SelectedValue);

                try
                {
                    DT_Con_Sys_Id = connection.SQLDS.Tables["Cur_Comm_Tbl2"].DefaultView.ToTable(true, "Country_System_ID", "Country_ID", "City_ID", "System_ID").Select(" Country_ID = " + CON_ID).CopyToDataTable();

                    if (DT_Con_Sys_Id.Rows.Count > 0)
                    {
                        OTO_Out_Id.Visible = false;
                        Column8.Visible = false;
                        Column10.Visible = false;
                    }
                    else
                    {
                        OTO_Out_Id.Visible = true;
                        Column8.Visible = true;
                        Column10.Visible = true;
                    }
                }
                catch
                {
                    OTO_Out_Id.Visible = true;
                    Column8.Visible = true;
                    Column10.Visible = true;
                }
            }
        }

        private void Cbo_T_Cit_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Chk_T_Cit_Flag.Checked == true)
            {
                Int16 CIT_ID = Convert.ToInt16(Cbo_T_Cit_ID.SelectedValue);

                try
                {
                    DT_Cit_Sys_Id = connection.SQLDS.Tables["Cur_Comm_Tbl2"].DefaultView.ToTable(true, "Country_System_ID", "Country_ID", "City_ID", "System_ID").Select(" City_ID = " + CIT_ID).CopyToDataTable();

                    if (DT_Cit_Sys_Id.Rows.Count > 0)
                    {
                        OTO_Out_Id.Visible = false;
                        Column8.Visible = false;
                        Column10.Visible = false;
                    }
                    else
                    {
                        OTO_Out_Id.Visible = true;
                        Column8.Visible = true;
                        Column10.Visible = true;
                    }
                }
                catch
                {
                    OTO_Out_Id.Visible = true;
                    Column8.Visible = true;
                    Column10.Visible = true;
                }
            }
        }
    }
}