﻿namespace Integration_Accounting_Sys
{
    partial class Comm_Online_IN_Add_New2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cbo_order_type = new System.Windows.Forms.ComboBox();
            this.Chk_T_Cit_Flag = new System.Windows.Forms.CheckBox();
            this.Chk_T_Con_Flag = new System.Windows.Forms.CheckBox();
            this.Chk_S_Cit_Flag = new System.Windows.Forms.CheckBox();
            this.Chk_S_Con_Flag = new System.Windows.Forms.CheckBox();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.Grd_comm_info = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Chk_Pr_Cur_Id_Flag = new System.Windows.Forms.CheckBox();
            this.Cbo_PR_Cur_Id = new System.Windows.Forms.ComboBox();
            this.Cbo_T_Con_ID = new System.Windows.Forms.ComboBox();
            this.Cbo_T_Cit_ID = new System.Windows.Forms.ComboBox();
            this.Cbo_S_Con_Id = new System.Windows.Forms.ComboBox();
            this.Cbo_S_Cit_ID = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cbo_R_Cur_Id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OUT_Send_Id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OUT_COMM_CUR = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUT_CS_Id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OTO_IN_Id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbo_order_type
            // 
            this.cbo_order_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_order_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_order_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_order_type.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_order_type.FormattingEnabled = true;
            this.cbo_order_type.Items.AddRange(new object[] {
            "فروع",
            "وكالات"});
            this.cbo_order_type.Location = new System.Drawing.Point(304, 3);
            this.cbo_order_type.Name = "cbo_order_type";
            this.cbo_order_type.Size = new System.Drawing.Size(87, 24);
            this.cbo_order_type.TabIndex = 1250;
            this.cbo_order_type.SelectedIndexChanged += new System.EventHandler(this.cbo_order_type_SelectedIndexChanged);
            // 
            // Chk_T_Cit_Flag
            // 
            this.Chk_T_Cit_Flag.AutoSize = true;
            this.Chk_T_Cit_Flag.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_T_Cit_Flag.ForeColor = System.Drawing.Color.Navy;
            this.Chk_T_Cit_Flag.Location = new System.Drawing.Point(749, 59);
            this.Chk_T_Cit_Flag.Name = "Chk_T_Cit_Flag";
            this.Chk_T_Cit_Flag.Size = new System.Drawing.Size(90, 20);
            this.Chk_T_Cit_Flag.TabIndex = 1249;
            this.Chk_T_Cit_Flag.Text = "مدينــة الاستـلام";
            this.Chk_T_Cit_Flag.UseVisualStyleBackColor = true;
            this.Chk_T_Cit_Flag.CheckedChanged += new System.EventHandler(this.Chk_T_Cit_Flag_CheckedChanged);
            // 
            // Chk_T_Con_Flag
            // 
            this.Chk_T_Con_Flag.AutoSize = true;
            this.Chk_T_Con_Flag.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_T_Con_Flag.ForeColor = System.Drawing.Color.Navy;
            this.Chk_T_Con_Flag.Location = new System.Drawing.Point(748, 31);
            this.Chk_T_Con_Flag.Name = "Chk_T_Con_Flag";
            this.Chk_T_Con_Flag.Size = new System.Drawing.Size(89, 20);
            this.Chk_T_Con_Flag.TabIndex = 1248;
            this.Chk_T_Con_Flag.Text = "بلـــد الاستـــلام";
            this.Chk_T_Con_Flag.UseVisualStyleBackColor = true;
            this.Chk_T_Con_Flag.CheckedChanged += new System.EventHandler(this.Chk_T_Con_Flag_CheckedChanged);
            // 
            // Chk_S_Cit_Flag
            // 
            this.Chk_S_Cit_Flag.AutoSize = true;
            this.Chk_S_Cit_Flag.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_S_Cit_Flag.ForeColor = System.Drawing.Color.Navy;
            this.Chk_S_Cit_Flag.Location = new System.Drawing.Point(398, 58);
            this.Chk_S_Cit_Flag.Name = "Chk_S_Cit_Flag";
            this.Chk_S_Cit_Flag.Size = new System.Drawing.Size(95, 20);
            this.Chk_S_Cit_Flag.TabIndex = 1247;
            this.Chk_S_Cit_Flag.Text = "مدينــة الاصــدار";
            this.Chk_S_Cit_Flag.UseVisualStyleBackColor = true;
            this.Chk_S_Cit_Flag.CheckedChanged += new System.EventHandler(this.Chk_S_Cit_Flag_CheckedChanged);
            // 
            // Chk_S_Con_Flag
            // 
            this.Chk_S_Con_Flag.AutoSize = true;
            this.Chk_S_Con_Flag.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_S_Con_Flag.ForeColor = System.Drawing.Color.Navy;
            this.Chk_S_Con_Flag.Location = new System.Drawing.Point(399, 31);
            this.Chk_S_Con_Flag.Name = "Chk_S_Con_Flag";
            this.Chk_S_Con_Flag.Size = new System.Drawing.Size(94, 20);
            this.Chk_S_Con_Flag.TabIndex = 1246;
            this.Chk_S_Con_Flag.Text = "بلــــد الاصـــدار";
            this.Chk_S_Con_Flag.UseVisualStyleBackColor = true;
            this.Chk_S_Con_Flag.CheckedChanged += new System.EventHandler(this.Chk_S_Con_Flag_CheckedChanged);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.Location = new System.Drawing.Point(549, 532);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(97, 25);
            this.ExtBtn.TabIndex = 1245;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.BtnOk.ForeColor = System.Drawing.Color.Navy;
            this.BtnOk.Location = new System.Drawing.Point(452, 532);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(97, 25);
            this.BtnOk.TabIndex = 1244;
            this.BtnOk.Text = "مـوافــق";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(1019, 145);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(77, 26);
            this.BtnDel.TabIndex = 1243;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.BtnAdd.ForeColor = System.Drawing.Color.Navy;
            this.BtnAdd.Location = new System.Drawing.Point(943, 145);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(77, 26);
            this.BtnAdd.TabIndex = 1242;
            this.BtnAdd.Text = "اضــافــة";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // Grd_comm_info
            // 
            this.Grd_comm_info.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_comm_info.ColumnHeadersHeight = 40;
            this.Grd_comm_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Cbo_R_Cur_Id,
            this.OUT_Send_Id,
            this.OUT_COMM_CUR,
            this.Column6,
            this.Column7,
            this.OUT_CS_Id,
            this.Column9,
            this.Column1,
            this.OTO_IN_Id,
            this.Column8,
            this.Column10,
            this.Column4,
            this.Column2});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.Format = "N3";
            dataGridViewCellStyle12.NullValue = null;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_comm_info.DefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_comm_info.Location = new System.Drawing.Point(8, 177);
            this.Grd_comm_info.Name = "Grd_comm_info";
            this.Grd_comm_info.RowHeadersWidth = 15;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_comm_info.Size = new System.Drawing.Size(1081, 351);
            this.Grd_comm_info.TabIndex = 1241;
            this.Grd_comm_info.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grd_comm_info_CellEndEdit);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(9, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 16);
            this.label2.TabIndex = 1234;
            this.label2.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(83, 5);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(220, 22);
            this.Txt_Sub_Cust.TabIndex = 1233;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(1094, 171);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1, 359);
            this.flowLayoutPanel6.TabIndex = 1231;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(2, 171);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1, 361);
            this.flowLayoutPanel5.TabIndex = 1230;
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(8, 28);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(384, 139);
            this.Grd_Sub_Cust.TabIndex = 1232;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Chk";
            this.dataGridViewCheckBoxColumn1.FalseValue = "0";
            this.dataGridViewCheckBoxColumn1.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn1.TrueValue = "1";
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Sub_Cust_ID";
            dataGridViewCellStyle16.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 89;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ASub_CustName";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 225;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(263, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel2.TabIndex = 86;
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            this.LblRec.Visible = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(2, 171);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1092, 1);
            this.flowLayoutPanel3.TabIndex = 1228;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 530);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1093, 1);
            this.flowLayoutPanel1.TabIndex = 1229;
            // 
            // Chk_Pr_Cur_Id_Flag
            // 
            this.Chk_Pr_Cur_Id_Flag.AutoSize = true;
            this.Chk_Pr_Cur_Id_Flag.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chk_Pr_Cur_Id_Flag.ForeColor = System.Drawing.Color.Navy;
            this.Chk_Pr_Cur_Id_Flag.Location = new System.Drawing.Point(398, 85);
            this.Chk_Pr_Cur_Id_Flag.Name = "Chk_Pr_Cur_Id_Flag";
            this.Chk_Pr_Cur_Id_Flag.Size = new System.Drawing.Size(95, 20);
            this.Chk_Pr_Cur_Id_Flag.TabIndex = 1240;
            this.Chk_Pr_Cur_Id_Flag.Text = "عملــــــة الدفـــع";
            this.Chk_Pr_Cur_Id_Flag.UseVisualStyleBackColor = true;
            this.Chk_Pr_Cur_Id_Flag.CheckedChanged += new System.EventHandler(this.Chk_Pr_Cur_Id_Flag_CheckedChanged);
            // 
            // Cbo_PR_Cur_Id
            // 
            this.Cbo_PR_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_PR_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_PR_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_PR_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_PR_Cur_Id.FormattingEnabled = true;
            this.Cbo_PR_Cur_Id.Location = new System.Drawing.Point(506, 83);
            this.Cbo_PR_Cur_Id.Name = "Cbo_PR_Cur_Id";
            this.Cbo_PR_Cur_Id.Size = new System.Drawing.Size(235, 24);
            this.Cbo_PR_Cur_Id.TabIndex = 1239;
            // 
            // Cbo_T_Con_ID
            // 
            this.Cbo_T_Con_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_T_Con_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_T_Con_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_T_Con_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_T_Con_ID.FormattingEnabled = true;
            this.Cbo_T_Con_ID.Location = new System.Drawing.Point(851, 29);
            this.Cbo_T_Con_ID.Name = "Cbo_T_Con_ID";
            this.Cbo_T_Con_ID.Size = new System.Drawing.Size(244, 24);
            this.Cbo_T_Con_ID.TabIndex = 1238;
            this.Cbo_T_Con_ID.SelectedIndexChanged += new System.EventHandler(this.Cbo_T_Con_ID_SelectedIndexChanged);
            // 
            // Cbo_T_Cit_ID
            // 
            this.Cbo_T_Cit_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_T_Cit_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_T_Cit_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_T_Cit_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_T_Cit_ID.FormattingEnabled = true;
            this.Cbo_T_Cit_ID.Location = new System.Drawing.Point(851, 57);
            this.Cbo_T_Cit_ID.Name = "Cbo_T_Cit_ID";
            this.Cbo_T_Cit_ID.Size = new System.Drawing.Size(244, 24);
            this.Cbo_T_Cit_ID.TabIndex = 1237;
            this.Cbo_T_Cit_ID.SelectedIndexChanged += new System.EventHandler(this.Cbo_T_Cit_ID_SelectedIndexChanged);
            // 
            // Cbo_S_Con_Id
            // 
            this.Cbo_S_Con_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_S_Con_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_S_Con_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_S_Con_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_S_Con_Id.FormattingEnabled = true;
            this.Cbo_S_Con_Id.Location = new System.Drawing.Point(506, 29);
            this.Cbo_S_Con_Id.Name = "Cbo_S_Con_Id";
            this.Cbo_S_Con_Id.Size = new System.Drawing.Size(235, 24);
            this.Cbo_S_Con_Id.TabIndex = 1236;
            // 
            // Cbo_S_Cit_ID
            // 
            this.Cbo_S_Cit_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_S_Cit_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_S_Cit_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_S_Cit_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_S_Cit_ID.FormattingEnabled = true;
            this.Cbo_S_Cit_ID.Location = new System.Drawing.Point(506, 56);
            this.Cbo_S_Cit_ID.Name = "Cbo_S_Cit_ID";
            this.Cbo_S_Cit_ID.Size = new System.Drawing.Size(235, 24);
            this.Cbo_S_Cit_ID.TabIndex = 1235;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 559);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1099, 22);
            this.statusStrip1.TabIndex = 1227;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "From_Amount";
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = "0.000";
            this.Column3.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column3.HeaderText = "يبدء من";
            this.Column3.Name = "Column3";
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 150;
            // 
            // Cbo_R_Cur_Id
            // 
            this.Cbo_R_Cur_Id.DataPropertyName = "(none)";
            this.Cbo_R_Cur_Id.HeaderText = "عملة الحوالة";
            this.Cbo_R_Cur_Id.Name = "Cbo_R_Cur_Id";
            this.Cbo_R_Cur_Id.Width = 150;
            // 
            // OUT_Send_Id
            // 
            this.OUT_Send_Id.DataPropertyName = "(none)";
            this.OUT_Send_Id.HeaderText = "حالة العمولة";
            this.OUT_Send_Id.Name = "OUT_Send_Id";
            this.OUT_Send_Id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OUT_Send_Id.Width = 140;
            // 
            // OUT_COMM_CUR
            // 
            this.OUT_COMM_CUR.DataPropertyName = "(none)";
            this.OUT_COMM_CUR.HeaderText = "عملة العمولة";
            this.OUT_COMM_CUR.Name = "OUT_COMM_CUR";
            this.OUT_COMM_CUR.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OUT_COMM_CUR.Width = 150;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "OUT_COMM_PER";
            dataGridViewCellStyle4.Format = "N3";
            dataGridViewCellStyle4.NullValue = "0.000";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column6.HeaderText = "النسبة %";
            this.Column6.Name = "Column6";
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column6.Width = 130;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "OUT_COMM_CUT";
            dataGridViewCellStyle5.Format = "N3";
            dataGridViewCellStyle5.NullValue = "0.000";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column7.HeaderText = "المقطوعة";
            this.Column7.Name = "Column7";
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 130;
            // 
            // OUT_CS_Id
            // 
            this.OUT_CS_Id.HeaderText = "حالة العمولة(الادارة)";
            this.OUT_CS_Id.Name = "OUT_CS_Id";
            this.OUT_CS_Id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OUT_CS_Id.Width = 140;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "OUT_COMM_PER_CS";
            dataGridViewCellStyle6.Format = "N3";
            dataGridViewCellStyle6.NullValue = "0.000";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column9.HeaderText = "النسبة %";
            this.Column9.Name = "Column9";
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Width = 130;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "OUT_COMM_CUT_CS";
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = "0.000";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column1.HeaderText = "مقطوعة";
            this.Column1.Name = "Column1";
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 130;
            // 
            // OTO_IN_Id
            // 
            this.OTO_IN_Id.HeaderText = "حالة عمولة الادارة (أونلاين)";
            this.OTO_IN_Id.Name = "OTO_IN_Id";
            this.OTO_IN_Id.Width = 140;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "OTO_IN_COMM_PER";
            dataGridViewCellStyle8.Format = "N3";
            dataGridViewCellStyle8.NullValue = "0.000";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column8.HeaderText = "نسبة الادارة (أونلاين) %";
            this.Column8.Name = "Column8";
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 130;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "OTO_IN_COMM_CUT";
            dataGridViewCellStyle9.Format = "N3";
            dataGridViewCellStyle9.NullValue = "0.000";
            this.Column10.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column10.HeaderText = "مقطوعة الادارة (أونلاين)";
            this.Column10.Name = "Column10";
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Min_Tot_comm";
            dataGridViewCellStyle10.Format = "N3";
            dataGridViewCellStyle10.NullValue = "0.000";
            this.Column4.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column4.HeaderText = "اقل قيمة مقطوعة";
            this.Column4.Name = "Column4";
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 105;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Max_Tot_comm";
            dataGridViewCellStyle11.Format = "N3";
            dataGridViewCellStyle11.NullValue = "0.000";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column2.HeaderText = "اعلى قيمة مقطوعة";
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 105;
            // 
            // Comm_Online_IN_Add_New2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 581);
            this.Controls.Add(this.cbo_order_type);
            this.Controls.Add(this.Chk_T_Cit_Flag);
            this.Controls.Add(this.Chk_T_Con_Flag);
            this.Controls.Add(this.Chk_S_Cit_Flag);
            this.Controls.Add(this.Chk_S_Con_Flag);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.Grd_comm_info);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Chk_Pr_Cur_Id_Flag);
            this.Controls.Add(this.Cbo_PR_Cur_Id);
            this.Controls.Add(this.Cbo_T_Con_ID);
            this.Controls.Add(this.Cbo_T_Cit_ID);
            this.Controls.Add(this.Cbo_S_Con_Id);
            this.Controls.Add(this.Cbo_S_Cit_ID);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Comm_Online_IN_Add_New2";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comm_Online_IN_Add_New2";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Comm_Online_Out_Add_New2_FormClosed);
            this.Load += new System.EventHandler(this.Comm_Online_IN_Add_New2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbo_order_type;
        private System.Windows.Forms.CheckBox Chk_T_Cit_Flag;
        private System.Windows.Forms.CheckBox Chk_T_Con_Flag;
        private System.Windows.Forms.CheckBox Chk_S_Cit_Flag;
        private System.Windows.Forms.CheckBox Chk_S_Con_Flag;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.DataGridView Grd_comm_info;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox Chk_Pr_Cur_Id_Flag;
        private System.Windows.Forms.ComboBox Cbo_PR_Cur_Id;
        private System.Windows.Forms.ComboBox Cbo_T_Con_ID;
        private System.Windows.Forms.ComboBox Cbo_T_Cit_ID;
        private System.Windows.Forms.ComboBox Cbo_S_Con_Id;
        private System.Windows.Forms.ComboBox Cbo_S_Cit_ID;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn Cbo_R_Cur_Id;
        private System.Windows.Forms.DataGridViewComboBoxColumn OUT_Send_Id;
        private System.Windows.Forms.DataGridViewComboBoxColumn OUT_COMM_CUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewComboBoxColumn OUT_CS_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewComboBoxColumn OTO_IN_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}