﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace Integration_Accounting_Sys
{
    public partial class search_Comm_Online_Out : Form
    {
        BindingSource BS_Sub_Cust = new BindingSource();
        BindingSource BS_S_CITY_ID = new BindingSource();
        BindingSource BS_S_COUN_ID = new BindingSource();
        BindingSource BS_T_CITY_ID = new BindingSource();
        BindingSource BS_T_COUN_ID = new BindingSource();
        BindingSource BS_PR_Cur_Id = new BindingSource();
        BindingSource BS_comm_info = new BindingSource();
        BindingSource BS_R_CUR_ID = new BindingSource();
        BindingSource BS_Comm_Cur_Id = new BindingSource();
        BindingSource BS_Send_Id = new BindingSource();
        Byte current_cust_id = 0;
        bool change_cbo = false;
        bool Change_COUN = false;
        bool Change_CITY = false;
        DataTable DT_T_COUN_ID = new DataTable();
        DataTable DT_T_CITY_ID = new DataTable();
        DataTable DT_comm_info = new DataTable();

        public search_Comm_Online_Out()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_comm_info.AutoGenerateColumns = false;
        }

        private void search_Comm_Online_Out_Load(object sender, EventArgs e)
        {
            change_cbo = false;
            Change_COUN = false;
            Change_CITY = false;

            if (connection.Lang_id != 1)
            {
                Column10.DataPropertyName = "Cust_EName";
                Column5.DataPropertyName = "COMM_CUR_EName";
                Column4.DataPropertyName = "Send_EName";
                Column11.DataPropertyName = "CS_EName";
                Column12.DataPropertyName = "S_Cit_EName";
                Column13.DataPropertyName = "S_Con_EName";
                Column14.DataPropertyName = "T_Cit_EName";
                Column2.DataPropertyName = "R_CUR_EName";
                Column15.DataPropertyName = "T_Con_EName";
                Column16.DataPropertyName = "PR_CUR_EName";
                Cmb_search_type_in_out.Items[0] = "Commissions Out";
                Cmb_search_type_in_out.Items[1] = "Commissions In";
            }

            if (connection.Lang_id == 1)
            {
                Cmb_search_type_in_out.Items[0] = "عمولات الصادر";
                Cmb_search_type_in_out.Items[1] = "عمولات الوارد";
            }

            Cmb_Sign.Items[0] = "=";
            Cmb_Sign.Items[1] = "<=";
            Cmb_Sign.Items[2] = ">=";

            Cmb_Sign.SelectedIndex = 0;
            Cmb_search_type_in_out.SelectedIndex = 0;

            change_cbo = true;
            Cmb_search_type_in_out_SelectedIndexChanged(null, null);
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            current_cust_id = Convert.ToByte(((DataRowView)BS_Sub_Cust.Current).Row["order_type"]);

            //if (current_cust_id == 0)
            //{ 
            //Column7.Visible=true;
            //Column6.Visible=true;
            //Column4.Visible = true;

            //Column1.Visible = false;
            //Column9.Visible = false;
            //Column11.Visible = false;
            //}
            //else if (current_cust_id == 1)
            //{
            //Column7.Visible = false;
            //Column6.Visible = false;
            //Column4.Visible = false;

            //Column1.Visible = true;
            //Column9.Visible = true;
            //Column11.Visible = true;
            //}
            //else 
            //if (Cmb_search_type_in_out.SelectedIndex == 1)
            //{
            //Column7.Visible = false;
            //Column6.Visible = false;
            //Column4.Visible = false;

            //Column1.Visible = true;
            //Column9.Visible = true;
            //Column11.Visible = true;

            //Column21.Visible = true;
            //Column23.Visible = true;
            //Column22.Visible = true; //oTo
            //}
            //else if (Cmb_search_type_in_out.SelectedIndex == 0)
            //{
            //Column7.Visible = true;
            //Column6.Visible = true;
            //Column4.Visible = true;
            //Column1.Visible = true;
            //Column9.Visible = true;
            //Column11.Visible = true;

            //Column21.Visible = true;
            //Column23.Visible = true;
            //Column22.Visible = true; //oTo
            //}

            if (connection.SQLDS.Tables.Contains("comm_out_main_tbl"))
            {
                connection.SQLDS.Tables.Remove("comm_out_main_tbl");
            }

            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "comm_out_main";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.CommandTimeout = 0;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.Parameters.AddWithValue("@Cust_id", Cmb_Sub_Cust.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@S_COUN_ID", Cmb_S_COUN_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@S_CITY_ID", Cmb_S_CITY_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@T_COUN_ID", Cmb_T_COUN_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@T_CITY_ID", Cmb_T_CITY_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@PR_Cur_Id", Cmb_PR_Cur_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Comm_Cur_Id", Cmb_Comm_Cur_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Comm_tpye_id", Cmb_Comm_tpye_id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@R_CUR_ID", Cmb_R_CUR_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@From_Amount", Convert.ToDecimal(Txt_From_Amount.Text.Trim()));
            connection.SQLCMD.Parameters.AddWithValue("@Sign", Cmb_Sign.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_ID", connection.T_ID);
            connection.SQLCMD.Parameters.AddWithValue("@order_type", current_cust_id);
            connection.SQLCMD.Parameters.AddWithValue("@flag_search_type_in_out", Cmb_search_type_in_out.SelectedIndex);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            MyGeneral_Lib.Copytocliptext("comm_out_main", connection.SQLCMD);
            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "comm_out_main_tbl");
            obj.Close();
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();
            if (connection.SQLDS.Tables["comm_out_main_tbl"].Rows.Count > 0)
            {
                BS_comm_info.DataSource = connection.SQLDS.Tables["comm_out_main_tbl"];
                Grd_comm_info.DataSource = BS_comm_info;
            }
            else
            {
                Grd_comm_info.DataSource = new BindingSource();
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Btn_End_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void search_Comm_Online_Out_FormClosed(object sender, FormClosedEventArgs e)
        {
            change_cbo = false;

            string[] Used_Tbl = { "COMM_ONLINE_OUT_IN_TBL", "COMM_ONLINE_OUT_IN_TBL1", "COMM_ONLINE_OUT_IN_TBL2", "COMM_ONLINE_OUT_IN_TBL3","COMM_ONLINE_OUT_IN_TBL4" ,"comm_out_main_tbl"
                                ,"COMM_ONLINE_OUT_IN_TBL5","COMM_ONLINE_OUT_IN_TBL6","COMM_ONLINE_OUT_IN_TBL7"};

            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Print_All_Click(object sender, EventArgs e)
        {
            try
            {
                if (connection.SQLDS.Tables["comm_out_main_tbl"].Rows.Count > 0)
                {
                    DataTable[] Export_DT = { connection.SQLDS.Tables["comm_out_main_tbl"].DefaultView.ToTable(false,
                                                  connection.Lang_id == 1 ? "Cust_AName" : "Cust_EName", "From_Amount",
                                                  connection.Lang_id == 1 ? "R_CUR_AName" : "R_CUR_EName", 
                                                  connection.Lang_id == 1 ? "COMM_CUR_AName" : "COMM_CUR_EName", 
                                                  connection.Lang_id == 1 ? "PR_CUR_AName": "PR_CUR_EName",
                                                  connection.Lang_id == 1 ? "Send_AName" : "Send_EName",  "COMM_CUT", "COMM_PER",  
                                                  connection.Lang_id == 1 ? "CS_AName":"CS_EName", "COMM_CUT_CS", "COMM_PER_CS", 
                                                  connection.Lang_id == 1 ? "OTO_Out_AName":"OTO_Out_EName", "OTO_Out_COMM_CUT", "OTO_Out_COMM_PER",
                                                  connection.Lang_id == 1 ? "S_Cit_AName": "S_Cit_EName",
                                                  connection.Lang_id == 1 ? "S_Con_AName": "S_Con_EName", 
                                                  connection.Lang_id == 1 ? "T_Cit_AName": "T_Cit_EName",  
                                                  connection.Lang_id == 1 ? "T_Con_AName": "T_Con_EName",  "Discount", "Str_Date_Discount", 
                                                  "Period_Discount","Min_Tot_comm","Max_Tot_comm").Select().CopyToDataTable()};
                    DataGridView[] Export_GRD = { Grd_comm_info };
                    MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للتصدير" : "There is no data to export", MyGeneral_Lib.LblCap);
                    return;
                }
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للتصدير" : "There is no data to export", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Cmb_search_type_in_out_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_cbo)
            {

                string[] Used_Tbl = { "COMM_ONLINE_OUT_IN_TBL", "COMM_ONLINE_OUT_IN_TBL1", "COMM_ONLINE_OUT_IN_TBL2", "COMM_ONLINE_OUT_IN_TBL3", 
                                      "COMM_ONLINE_OUT_IN_TBL4", "comm_out_main_tbl", "COMM_ONLINE_OUT_IN_TBL5", "COMM_ONLINE_OUT_IN_TBL6", 
                                      "COMM_ONLINE_OUT_IN_TBL7", "COMM_ONLINE_OUT_IN_TBL7"};

                foreach (string Tbl in Used_Tbl)
                {
                    if (connection.SQLDS.Tables.Contains(Tbl))
                    {
                        connection.SQLDS.Tables.Remove(Tbl);
                    }
                }

                if (Cmb_search_type_in_out.SelectedIndex == 0)
                { connection.SqlExec("EXEC  COMM_ONLINE_OUT_SEARCH_discount", "COMM_ONLINE_OUT_IN_TBL"); }
                else { connection.SqlExec("EXEC COMM_ONLINE_IN_SEARCH_discount", "COMM_ONLINE_OUT_IN_TBL"); }



                BS_S_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL"].DefaultView.ToTable(true, "Con_AName", "Con_EName", "Con_ID").Select().CopyToDataTable();
                Cmb_S_COUN_ID.DataSource = BS_S_COUN_ID;
                Cmb_S_COUN_ID.ValueMember = "Con_ID";
                Cmb_S_COUN_ID.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

                BS_T_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL1"].DefaultView.ToTable(true, "Con_AName", "Con_EName", "Con_ID").Select().CopyToDataTable();
                Cmb_T_COUN_ID.DataSource = BS_T_COUN_ID;
                Cmb_T_COUN_ID.ValueMember = "Con_ID";
                Cmb_T_COUN_ID.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

                BS_T_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL1"];
                Cmb_T_CITY_ID.DataSource = BS_T_CITY_ID;
                Cmb_T_CITY_ID.ValueMember = "Cit_ID";
                Cmb_T_CITY_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";

                BS_S_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL"];
                Cmb_S_CITY_ID.DataSource = BS_S_CITY_ID;
                Cmb_S_CITY_ID.ValueMember = "Cit_ID";
                Cmb_S_CITY_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";


                BS_PR_Cur_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL4"];
                Cmb_PR_Cur_Id.DataSource = BS_PR_Cur_Id;
                Cmb_PR_Cur_Id.ValueMember = "cur_id";
                Cmb_PR_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";

                BS_Sub_Cust.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL5"];
                Cmb_Sub_Cust.DataSource = BS_Sub_Cust;
                Cmb_Sub_Cust.ValueMember = "Sub_Cust_ID";
                Cmb_Sub_Cust.DisplayMember = connection.Lang_id == 1 ? "ASub_CustName" : "ESub_CustNmae";


                BS_R_CUR_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL2"];
                Cmb_R_CUR_ID.DataSource = BS_R_CUR_ID;
                Cmb_R_CUR_ID.ValueMember = "cur_id";
                Cmb_R_CUR_ID.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";


                BS_Comm_Cur_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL3"];
                Cmb_Comm_Cur_Id.DataSource = BS_Comm_Cur_Id;
                Cmb_Comm_Cur_Id.ValueMember = "cur_id";
                Cmb_Comm_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";


                BS_Send_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL6"];
                Cmb_Comm_tpye_id.DataSource = BS_Send_Id;
                Cmb_Comm_tpye_id.ValueMember = "type_rem_ID";
                Cmb_Comm_tpye_id.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";

                Cmb_Comm_tpye_id.SelectedValue = 0;
                Cmb_Sub_Cust.SelectedValue = 0;
                
                Change_COUN = true;                
                Cmb_T_COUN_ID_SelectedIndexChanged(null, null);
                Change_CITY = true;
                Cmb_T_CITY_ID_SelectedIndexChanged(null, null);
            }
        }

        private void Cmb_T_COUN_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change_COUN)
            {
                Int16 CON_ID = 0;
                CON_ID = Convert.ToInt16(Cmb_T_COUN_ID.SelectedValue);

                try
                {
                    DT_T_COUN_ID = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL8"].DefaultView.ToTable(true, "Country_System_ID", "Country_ID", "City_ID", "System_ID").Select(" Country_ID = " + CON_ID).CopyToDataTable();

                    if (DT_T_COUN_ID.Rows.Count > 0)
                    {
                        Column21.Visible = false;
                        Column23.Visible = false;
                        Column22.Visible = false; 
                    }
                    else
                    {
                        Column21.Visible = true;
                        Column23.Visible = true;
                        Column22.Visible = true;
                    }
                }
                catch
                {
                    Column21.Visible = true;
                    Column23.Visible = true;
                    Column22.Visible = true;
                }
            }
        }

        private void Cmb_T_CITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change_CITY)
            {
                Int16 CIT_ID = 0;
                CIT_ID = Convert.ToInt16(Cmb_T_CITY_ID.SelectedValue);

                try
                {
                    DT_T_CITY_ID = connection.SQLDS.Tables["COMM_ONLINE_OUT_IN_TBL8"].DefaultView.ToTable(true, "Country_System_ID", "Country_ID", "City_ID", "System_ID").Select(" City_ID = " + CIT_ID).CopyToDataTable();

                    if (DT_T_CITY_ID.Rows.Count > 0)
                    {
                        Column21.Visible = false;
                        Column23.Visible = false;
                        Column22.Visible = false;                        
                    }
                    else
                    {
                        Column21.Visible = true;
                        Column23.Visible = true;
                        Column22.Visible = true;
                    }
                }
                catch
                {
                    Column21.Visible = true;
                    Column23.Visible = true;
                    Column22.Visible = true;
                }
            }
        }
    }
}