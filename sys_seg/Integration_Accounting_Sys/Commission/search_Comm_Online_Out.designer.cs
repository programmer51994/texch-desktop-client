﻿namespace Integration_Accounting_Sys
{
    partial class search_Comm_Online_Out
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.Cmb_S_CITY_ID = new System.Windows.Forms.ComboBox();
            this.Cmb_S_COUN_ID = new System.Windows.Forms.ComboBox();
            this.Cmb_T_CITY_ID = new System.Windows.Forms.ComboBox();
            this.Cmb_T_COUN_ID = new System.Windows.Forms.ComboBox();
            this.Cmb_PR_Cur_Id = new System.Windows.Forms.ComboBox();
            this.Grd_comm_info = new System.Windows.Forms.DataGridView();
            this.Cmb_Sub_Cust = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Btn_End = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Cmb_R_CUR_ID = new System.Windows.Forms.ComboBox();
            this.Cmb_Comm_Cur_Id = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Cmb_Comm_tpye_id = new System.Windows.Forms.ComboBox();
            this.Cmb_Sign = new System.Windows.Forms.ComboBox();
            this.Print_All = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.Cmb_search_type_in_out = new System.Windows.Forms.ComboBox();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Txt_From_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 616);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1031, 22);
            this.statusStrip1.TabIndex = 65;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            this.LblRec.Visible = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(6, 253);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1021, 2);
            this.flowLayoutPanel3.TabIndex = 87;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(7, 583);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1021, 2);
            this.flowLayoutPanel1.TabIndex = 88;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(191, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(825, 2);
            this.flowLayoutPanel2.TabIndex = 86;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(5, 255);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(2, 330);
            this.flowLayoutPanel5.TabIndex = 89;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(1026, 253);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(2, 331);
            this.flowLayoutPanel6.TabIndex = 90;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(4, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 14);
            this.label2.TabIndex = 1203;
            this.label2.Text = "الفرع/الوكيل :";
            // 
            // Cmb_S_CITY_ID
            // 
            this.Cmb_S_CITY_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_S_CITY_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_S_CITY_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_S_CITY_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_S_CITY_ID.FormattingEnabled = true;
            this.Cmb_S_CITY_ID.Location = new System.Drawing.Point(563, 116);
            this.Cmb_S_CITY_ID.Name = "Cmb_S_CITY_ID";
            this.Cmb_S_CITY_ID.Size = new System.Drawing.Size(301, 24);
            this.Cmb_S_CITY_ID.TabIndex = 1208;
            // 
            // Cmb_S_COUN_ID
            // 
            this.Cmb_S_COUN_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_S_COUN_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_S_COUN_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_S_COUN_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_S_COUN_ID.FormattingEnabled = true;
            this.Cmb_S_COUN_ID.Location = new System.Drawing.Point(113, 116);
            this.Cmb_S_COUN_ID.Name = "Cmb_S_COUN_ID";
            this.Cmb_S_COUN_ID.Size = new System.Drawing.Size(301, 24);
            this.Cmb_S_COUN_ID.TabIndex = 1210;
            // 
            // Cmb_T_CITY_ID
            // 
            this.Cmb_T_CITY_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_T_CITY_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_T_CITY_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_T_CITY_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_T_CITY_ID.FormattingEnabled = true;
            this.Cmb_T_CITY_ID.Location = new System.Drawing.Point(563, 147);
            this.Cmb_T_CITY_ID.Name = "Cmb_T_CITY_ID";
            this.Cmb_T_CITY_ID.Size = new System.Drawing.Size(301, 24);
            this.Cmb_T_CITY_ID.TabIndex = 1212;
            this.Cmb_T_CITY_ID.SelectedIndexChanged += new System.EventHandler(this.Cmb_T_CITY_ID_SelectedIndexChanged);
            // 
            // Cmb_T_COUN_ID
            // 
            this.Cmb_T_COUN_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_T_COUN_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_T_COUN_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_T_COUN_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_T_COUN_ID.FormattingEnabled = true;
            this.Cmb_T_COUN_ID.Location = new System.Drawing.Point(113, 147);
            this.Cmb_T_COUN_ID.Name = "Cmb_T_COUN_ID";
            this.Cmb_T_COUN_ID.Size = new System.Drawing.Size(301, 24);
            this.Cmb_T_COUN_ID.TabIndex = 1214;
            this.Cmb_T_COUN_ID.SelectedIndexChanged += new System.EventHandler(this.Cmb_T_COUN_ID_SelectedIndexChanged);
            // 
            // Cmb_PR_Cur_Id
            // 
            this.Cmb_PR_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_PR_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_PR_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_PR_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_PR_Cur_Id.FormattingEnabled = true;
            this.Cmb_PR_Cur_Id.Location = new System.Drawing.Point(563, 85);
            this.Cmb_PR_Cur_Id.Name = "Cmb_PR_Cur_Id";
            this.Cmb_PR_Cur_Id.Size = new System.Drawing.Size(301, 24);
            this.Cmb_PR_Cur_Id.TabIndex = 1215;
            // 
            // Grd_comm_info
            // 
            this.Grd_comm_info.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_comm_info.ColumnHeadersHeight = 40;
            this.Grd_comm_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.Column3,
            this.Column2,
            this.Column5,
            this.Column16,
            this.Column4,
            this.Column7,
            this.Column6,
            this.Column11,
            this.Column1,
            this.Column9,
            this.Column21,
            this.Column23,
            this.Column22,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column8,
            this.Column20});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.Format = "N3";
            dataGridViewCellStyle11.NullValue = null;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_comm_info.DefaultCellStyle = dataGridViewCellStyle11;
            this.Grd_comm_info.Location = new System.Drawing.Point(12, 259);
            this.Grd_comm_info.Name = "Grd_comm_info";
            this.Grd_comm_info.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_comm_info.RowHeadersWidth = 15;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_comm_info.Size = new System.Drawing.Size(1010, 323);
            this.Grd_comm_info.TabIndex = 1217;
            // 
            // Cmb_Sub_Cust
            // 
            this.Cmb_Sub_Cust.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Sub_Cust.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Sub_Cust.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Sub_Cust.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Sub_Cust.FormattingEnabled = true;
            this.Cmb_Sub_Cust.Location = new System.Drawing.Point(113, 85);
            this.Cmb_Sub_Cust.Name = "Cmb_Sub_Cust";
            this.Cmb_Sub_Cust.Size = new System.Drawing.Size(301, 24);
            this.Cmb_Sub_Cust.TabIndex = 1220;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(5, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 14);
            this.label1.TabIndex = 1221;
            this.label1.Text = "بلـــــد الاصــــدار :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(445, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 14);
            this.label3.TabIndex = 1222;
            this.label3.Text = "مدينـــة الاصــــدار :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(4, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 14);
            this.label4.TabIndex = 1223;
            this.label4.Text = "بلـــد الاستــــلام :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(445, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 14);
            this.label5.TabIndex = 1224;
            this.label5.Text = "مدينـــة الاستــلام :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(445, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 14);
            this.label6.TabIndex = 1225;
            this.label6.Text = "عملــــــة الدفـــــع :";
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(908, 199);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(103, 31);
            this.Btn_Add.TabIndex = 1226;
            this.Btn_Add.Text = "البحــــــــث";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(464, 8);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(318, 23);
            this.TxtUser.TabIndex = 1231;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(330, 12);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(130, 14);
            this.label7.TabIndex = 1230;
            this.label7.Text = "اسم المستخــــــــــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(8, 8);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(318, 23);
            this.TxtTerm_Name.TabIndex = 1229;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(788, 12);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(65, 14);
            this.label8.TabIndex = 1227;
            this.label8.Text = "التاريــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(859, 8);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(163, 23);
            this.TxtIn_Rec_Date.TabIndex = 1228;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(0, 43);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1035, 1);
            this.flowLayoutPanel9.TabIndex = 1232;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(11, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(12, 243);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 1233;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel8.TabIndex = 630;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Maroon;
            this.label16.Location = new System.Drawing.Point(6, 33);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(166, 14);
            this.label16.TabIndex = 1234;
            this.label16.Text = "شــــروط البحـــــــــــث..........";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(6, 233);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(164, 14);
            this.label9.TabIndex = 1235;
            this.label9.Text = "عـــــرض العـــمـــولات..........";
            // 
            // Btn_End
            // 
            this.Btn_End.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_End.ForeColor = System.Drawing.Color.Navy;
            this.Btn_End.Location = new System.Drawing.Point(515, 587);
            this.Btn_End.Name = "Btn_End";
            this.Btn_End.Size = new System.Drawing.Size(125, 29);
            this.Btn_End.TabIndex = 1236;
            this.Btn_End.Text = "انهــــــــــــــــاء";
            this.Btn_End.UseVisualStyleBackColor = true;
            this.Btn_End.Click += new System.EventHandler(this.Btn_End_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(2, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 1244;
            this.label10.Text = "عملــــة العمولـة :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(3, 211);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 14);
            this.label11.TabIndex = 1243;
            this.label11.Text = "عملــــة الحوالـة :";
            // 
            // Cmb_R_CUR_ID
            // 
            this.Cmb_R_CUR_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_R_CUR_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_R_CUR_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_R_CUR_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_R_CUR_ID.FormattingEnabled = true;
            this.Cmb_R_CUR_ID.Location = new System.Drawing.Point(113, 206);
            this.Cmb_R_CUR_ID.Name = "Cmb_R_CUR_ID";
            this.Cmb_R_CUR_ID.Size = new System.Drawing.Size(301, 24);
            this.Cmb_R_CUR_ID.TabIndex = 1242;
            // 
            // Cmb_Comm_Cur_Id
            // 
            this.Cmb_Comm_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Comm_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Comm_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Comm_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Comm_Cur_Id.FormattingEnabled = true;
            this.Cmb_Comm_Cur_Id.Location = new System.Drawing.Point(113, 177);
            this.Cmb_Comm_Cur_Id.Name = "Cmb_Comm_Cur_Id";
            this.Cmb_Comm_Cur_Id.Size = new System.Drawing.Size(301, 24);
            this.Cmb_Comm_Cur_Id.TabIndex = 1241;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(444, 211);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 14);
            this.label34.TabIndex = 1245;
            this.label34.Text = "المبلــــغ يبـدء من :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(444, 182);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 14);
            this.label12.TabIndex = 1248;
            this.label12.Text = "حـالــة العمـولـــــة :";
            // 
            // Cmb_Comm_tpye_id
            // 
            this.Cmb_Comm_tpye_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Comm_tpye_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Comm_tpye_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Comm_tpye_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Comm_tpye_id.FormattingEnabled = true;
            this.Cmb_Comm_tpye_id.Location = new System.Drawing.Point(563, 177);
            this.Cmb_Comm_tpye_id.Name = "Cmb_Comm_tpye_id";
            this.Cmb_Comm_tpye_id.Size = new System.Drawing.Size(301, 24);
            this.Cmb_Comm_tpye_id.TabIndex = 1247;
            // 
            // Cmb_Sign
            // 
            this.Cmb_Sign.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Sign.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Sign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Sign.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Sign.FormattingEnabled = true;
            this.Cmb_Sign.Items.AddRange(new object[] {
            "يساوي",
            "فمادون",
            "فمافوق"});
            this.Cmb_Sign.Location = new System.Drawing.Point(761, 206);
            this.Cmb_Sign.Name = "Cmb_Sign";
            this.Cmb_Sign.Size = new System.Drawing.Size(103, 24);
            this.Cmb_Sign.TabIndex = 1249;
            // 
            // Print_All
            // 
            this.Print_All.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Print_All.ForeColor = System.Drawing.Color.Navy;
            this.Print_All.Location = new System.Drawing.Point(391, 587);
            this.Print_All.Name = "Print_All";
            this.Print_All.Size = new System.Drawing.Size(125, 29);
            this.Print_All.TabIndex = 1250;
            this.Print_All.Text = "تصدير اجمالي";
            this.Print_All.UseVisualStyleBackColor = true;
            this.Print_All.Click += new System.EventHandler(this.Print_All_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(4, 59);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 14);
            this.label13.TabIndex = 1252;
            this.label13.Text = "نــوع الــعــمـولـة :";
            // 
            // Cmb_search_type_in_out
            // 
            this.Cmb_search_type_in_out.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_search_type_in_out.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_search_type_in_out.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_search_type_in_out.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_search_type_in_out.FormattingEnabled = true;
            this.Cmb_search_type_in_out.Items.AddRange(new object[] {
            "عمولات صادر",
            "عمولات وارد"});
            this.Cmb_search_type_in_out.Location = new System.Drawing.Point(113, 54);
            this.Cmb_search_type_in_out.Name = "Cmb_search_type_in_out";
            this.Cmb_search_type_in_out.Size = new System.Drawing.Size(301, 24);
            this.Cmb_search_type_in_out.TabIndex = 1251;
            this.Cmb_search_type_in_out.SelectedIndexChanged += new System.EventHandler(this.Cmb_search_type_in_out_SelectedIndexChanged);
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "Cust_AName";
            this.Column10.HeaderText = "الفرع/الوكيل";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 93;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "From_Amount";
            this.Column3.HeaderText = "يبدء من";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 52;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_CUR_AName";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.Width = 81;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "COMM_CUR_AName";
            this.Column5.HeaderText = "عملة عمولة";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column5.Width = 78;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "PR_CUR_AName";
            this.Column16.HeaderText = "عملة الدفع";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 76;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "Send_AName";
            this.Column4.HeaderText = "حالة العمولة مع مرسل";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.Width = 102;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "COMM_CUT";
            dataGridViewCellStyle3.Format = "N3";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column7.HeaderText = "المقطوعة مع المرسل";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 89;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "COMM_PER";
            dataGridViewCellStyle4.Format = "N3";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column6.HeaderText = "نسبة المرسل %";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 89;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.DataPropertyName = "CS_AName";
            this.Column11.HeaderText = "حالة العمولة  مع الادارة";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 91;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "COMM_CUT_CS";
            dataGridViewCellStyle5.Format = "N3";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column1.HeaderText = "المقطوعة  مع الادارة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 92;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "COMM_PER_CS";
            dataGridViewCellStyle6.Format = "N3";
            this.Column9.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column9.HeaderText = "نسبة الادارة %";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 86;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "OTO_Out_AName";
            this.Column21.HeaderText = "حاله العموله مع الاداره (اونلاين)";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "OTO_Out_COMM_CUT";
            this.Column23.HeaderText = "مقطوعه مع الادارة (اونلاين)";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "OTO_Out_COMM_PER";
            this.Column22.HeaderText = "نسبه مع الاداره% (اونلاين)";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "S_Cit_AName";
            this.Column12.HeaderText = "مدينة الاصدار";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 87;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "S_Con_AName";
            this.Column13.HeaderText = "بلد الاصدار";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 76;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "T_Cit_AName";
            this.Column14.HeaderText = "مدينة الاستلام";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 88;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column15.DataPropertyName = "T_Con_AName";
            this.Column15.HeaderText = "بلد الاستلام";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 78;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "Discount";
            dataGridViewCellStyle7.Format = "N3";
            this.Column17.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column17.HeaderText = "الخصم";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 62;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "Str_Date_Discount";
            dataGridViewCellStyle8.Format = "dd/MM/yyyy";
            dataGridViewCellStyle8.NullValue = null;
            this.Column18.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column18.HeaderText = "تاريخ بداية الخصم";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 107;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "Period_Discount";
            this.Column19.HeaderText = "مدة الخصم";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 76;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "Min_Tot_comm";
            dataGridViewCellStyle9.Format = "N3";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column8.HeaderText = "اقل قيمة";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 68;
            // 
            // Column20
            // 
            this.Column20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column20.DataPropertyName = "Max_Tot_comm";
            dataGridViewCellStyle10.Format = "N3";
            this.Column20.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column20.HeaderText = "اعلى قيمة";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 57;
            // 
            // Txt_From_Amount
            // 
            this.Txt_From_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_From_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_From_Amount.Location = new System.Drawing.Point(563, 207);
            this.Txt_From_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_From_Amount.Name = "Txt_From_Amount";
            this.Txt_From_Amount.NumberDecimalDigits = 3;
            this.Txt_From_Amount.NumberDecimalSeparator = ".";
            this.Txt_From_Amount.NumberGroupSeparator = ",";
            this.Txt_From_Amount.ReadOnly = true;
            this.Txt_From_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_From_Amount.Size = new System.Drawing.Size(197, 23);
            this.Txt_From_Amount.TabIndex = 1246;
            this.Txt_From_Amount.Text = "0.000";
            // 
            // search_Comm_Online_Out
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 638);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Cmb_search_type_in_out);
            this.Controls.Add(this.Print_All);
            this.Controls.Add(this.Cmb_Sign);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Cmb_Comm_tpye_id);
            this.Controls.Add(this.Txt_From_Amount);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Cmb_R_CUR_ID);
            this.Controls.Add(this.Cmb_Comm_Cur_Id);
            this.Controls.Add(this.Btn_End);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cmb_Sub_Cust);
            this.Controls.Add(this.Grd_comm_info);
            this.Controls.Add(this.Cmb_PR_Cur_Id);
            this.Controls.Add(this.Cmb_T_COUN_ID);
            this.Controls.Add(this.Cmb_T_CITY_ID);
            this.Controls.Add(this.Cmb_S_COUN_ID);
            this.Controls.Add(this.Cmb_S_CITY_ID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "search_Comm_Online_Out";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "547";
            this.Text = "search Comm Online Out";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.search_Comm_Online_Out_FormClosed);
            this.Load += new System.EventHandler(this.search_Comm_Online_Out_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Cmb_S_CITY_ID;
        private System.Windows.Forms.ComboBox Cmb_S_COUN_ID;
        private System.Windows.Forms.ComboBox Cmb_T_CITY_ID;
        private System.Windows.Forms.ComboBox Cmb_T_COUN_ID;
        private System.Windows.Forms.ComboBox Cmb_PR_Cur_Id;
        private System.Windows.Forms.DataGridView Grd_comm_info;
        private System.Windows.Forms.ComboBox Cmb_Sub_Cust;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button Btn_End;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Cmb_R_CUR_ID;
        private System.Windows.Forms.ComboBox Cmb_Comm_Cur_Id;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_From_Amount;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Cmb_Comm_tpye_id;
        private System.Windows.Forms.ComboBox Cmb_Sign;
        private System.Windows.Forms.Button Print_All;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox Cmb_search_type_in_out;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;

    }
}