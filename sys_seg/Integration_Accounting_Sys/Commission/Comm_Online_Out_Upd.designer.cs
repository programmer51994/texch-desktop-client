﻿namespace Integration_Accounting_Sys
{
    partial class Comm_Online_Out_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Sub_Cust = new System.Windows.Forms.TextBox();
            this.Grd_Sub_Cust = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cbo_PR_Cur_Id = new System.Windows.Forms.ComboBox();
            this.Cbo_T_Con_ID = new System.Windows.Forms.ComboBox();
            this.Cbo_T_Cit_ID = new System.Windows.Forms.ComboBox();
            this.Cbo_S_Con_Id = new System.Windows.Forms.ComboBox();
            this.Cbo_S_Cit_ID = new System.Windows.Forms.ComboBox();
            this.Cbo_option = new System.Windows.Forms.ComboBox();
            this.Search_Btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_Start_Discount = new System.Windows.Forms.DateTimePicker();
            this.Cmb_Sign = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Cbo_R_Cur_Id = new System.Windows.Forms.ComboBox();
            this.OUT_COMM_CUR = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Cmb_Comm_tpye_id = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.Grd_comm_info = new System.Windows.Forms.DataGridView();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtTerm_Name = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_End = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.Txt_Discount_Period = new Integration_Accounting_Sys.NumericTextBox();
            this.Txt_Discount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Txt_From_Amount = new System.Windows.Forms.Sample.DecimalTextBox();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 14);
            this.label2.TabIndex = 1206;
            this.label2.Text = "بحــــــــــث:";
            // 
            // Txt_Sub_Cust
            // 
            this.Txt_Sub_Cust.BackColor = System.Drawing.Color.White;
            this.Txt_Sub_Cust.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Txt_Sub_Cust.Location = new System.Drawing.Point(76, 41);
            this.Txt_Sub_Cust.Name = "Txt_Sub_Cust";
            this.Txt_Sub_Cust.Size = new System.Drawing.Size(322, 22);
            this.Txt_Sub_Cust.TabIndex = 1205;
            this.Txt_Sub_Cust.TextChanged += new System.EventHandler(this.Txt_Sub_Cust_TextChanged);
            // 
            // Grd_Sub_Cust
            // 
            this.Grd_Sub_Cust.AllowUserToAddRows = false;
            this.Grd_Sub_Cust.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grd_Sub_Cust.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Sub_Cust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grd_Sub_Cust.ColumnHeadersHeight = 30;
            this.Grd_Sub_Cust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3});
            this.Grd_Sub_Cust.GridColor = System.Drawing.SystemColors.Control;
            this.Grd_Sub_Cust.Location = new System.Drawing.Point(6, 64);
            this.Grd_Sub_Cust.Name = "Grd_Sub_Cust";
            this.Grd_Sub_Cust.RowHeadersWidth = 20;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Sub_Cust.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grd_Sub_Cust.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Sub_Cust.Size = new System.Drawing.Size(392, 139);
            this.Grd_Sub_Cust.TabIndex = 1204;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "chk";
            this.dataGridViewCheckBoxColumn1.FalseValue = "0";
            this.dataGridViewCheckBoxColumn1.HeaderText = "تأشير";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn1.TrueValue = "1";
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Sub_Cust_ID";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز الثانوي";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 89;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ASub_CustName";
            this.dataGridViewTextBoxColumn3.HeaderText = "أســـــم الثـــــــانوي";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 230;
            // 
            // Cbo_PR_Cur_Id
            // 
            this.Cbo_PR_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_PR_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_PR_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_PR_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_PR_Cur_Id.FormattingEnabled = true;
            this.Cbo_PR_Cur_Id.Location = new System.Drawing.Point(506, 97);
            this.Cbo_PR_Cur_Id.Name = "Cbo_PR_Cur_Id";
            this.Cbo_PR_Cur_Id.Size = new System.Drawing.Size(232, 24);
            this.Cbo_PR_Cur_Id.TabIndex = 1230;
            // 
            // Cbo_T_Con_ID
            // 
            this.Cbo_T_Con_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_T_Con_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_T_Con_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_T_Con_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_T_Con_ID.FormattingEnabled = true;
            this.Cbo_T_Con_ID.Location = new System.Drawing.Point(506, 68);
            this.Cbo_T_Con_ID.Name = "Cbo_T_Con_ID";
            this.Cbo_T_Con_ID.Size = new System.Drawing.Size(232, 24);
            this.Cbo_T_Con_ID.TabIndex = 1229;
            // 
            // Cbo_T_Cit_ID
            // 
            this.Cbo_T_Cit_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_T_Cit_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_T_Cit_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_T_Cit_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_T_Cit_ID.FormattingEnabled = true;
            this.Cbo_T_Cit_ID.Location = new System.Drawing.Point(843, 68);
            this.Cbo_T_Cit_ID.Name = "Cbo_T_Cit_ID";
            this.Cbo_T_Cit_ID.Size = new System.Drawing.Size(232, 24);
            this.Cbo_T_Cit_ID.TabIndex = 1228;
            // 
            // Cbo_S_Con_Id
            // 
            this.Cbo_S_Con_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_S_Con_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_S_Con_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_S_Con_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_S_Con_Id.FormattingEnabled = true;
            this.Cbo_S_Con_Id.Location = new System.Drawing.Point(506, 41);
            this.Cbo_S_Con_Id.Name = "Cbo_S_Con_Id";
            this.Cbo_S_Con_Id.Size = new System.Drawing.Size(232, 24);
            this.Cbo_S_Con_Id.TabIndex = 1227;
            // 
            // Cbo_S_Cit_ID
            // 
            this.Cbo_S_Cit_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_S_Cit_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_S_Cit_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_S_Cit_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_S_Cit_ID.FormattingEnabled = true;
            this.Cbo_S_Cit_ID.Location = new System.Drawing.Point(843, 41);
            this.Cbo_S_Cit_ID.Name = "Cbo_S_Cit_ID";
            this.Cbo_S_Cit_ID.Size = new System.Drawing.Size(232, 24);
            this.Cbo_S_Cit_ID.TabIndex = 1226;
            // 
            // Cbo_option
            // 
            this.Cbo_option.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_option.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_option.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_option.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_option.FormattingEnabled = true;
            this.Cbo_option.Items.AddRange(new object[] {
            "حذف",
            "خصم"});
            this.Cbo_option.Location = new System.Drawing.Point(848, 182);
            this.Cbo_option.Name = "Cbo_option";
            this.Cbo_option.Size = new System.Drawing.Size(102, 24);
            this.Cbo_option.TabIndex = 1239;
            this.Cbo_option.SelectedIndexChanged += new System.EventHandler(this.Cbo_option_SelectedIndexChanged);
            // 
            // Search_Btn
            // 
            this.Search_Btn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Search_Btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Search_Btn.Location = new System.Drawing.Point(952, 181);
            this.Search_Btn.Name = "Search_Btn";
            this.Search_Btn.Size = new System.Drawing.Size(129, 26);
            this.Search_Btn.TabIndex = 1240;
            this.Search_Btn.Text = "بــحـــــــــــــث";
            this.Search_Btn.UseVisualStyleBackColor = true;
            this.Search_Btn.Click += new System.EventHandler(this.Search_Btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(749, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 1241;
            this.label1.Text = "العـــمـــليــــــــة :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(6, 527);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 14);
            this.label3.TabIndex = 1244;
            this.label3.Text = "الخصــــــم:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(230, 527);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 14);
            this.label4.TabIndex = 1246;
            this.label4.Text = "مدتــــــه:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(417, 527);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 14);
            this.label5.TabIndex = 1247;
            this.label5.Text = "اعتبارا من:";
            // 
            // Txt_Start_Discount
            // 
            this.Txt_Start_Discount.CustomFormat = "dd/mm/yyyy";
            this.Txt_Start_Discount.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Txt_Start_Discount.Location = new System.Drawing.Point(491, 524);
            this.Txt_Start_Discount.Name = "Txt_Start_Discount";
            this.Txt_Start_Discount.Size = new System.Drawing.Size(181, 20);
            this.Txt_Start_Discount.TabIndex = 1248;
            // 
            // Cmb_Sign
            // 
            this.Cmb_Sign.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Sign.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Sign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Sign.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Sign.FormattingEnabled = true;
            this.Cmb_Sign.Items.AddRange(new object[] {
            "يساوي",
            "فمادون",
            "فمافوق"});
            this.Cmb_Sign.Location = new System.Drawing.Point(1013, 124);
            this.Cmb_Sign.Name = "Cmb_Sign";
            this.Cmb_Sign.Size = new System.Drawing.Size(61, 24);
            this.Cmb_Sign.TabIndex = 1252;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(400, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 1256;
            this.label10.Text = "عملــــة العمولـة :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(400, 158);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 14);
            this.label11.TabIndex = 1255;
            this.label11.Text = "عملـــة الحوالـــة :";
            // 
            // Cbo_R_Cur_Id
            // 
            this.Cbo_R_Cur_Id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_R_Cur_Id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_R_Cur_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_R_Cur_Id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_R_Cur_Id.FormattingEnabled = true;
            this.Cbo_R_Cur_Id.Location = new System.Drawing.Point(506, 153);
            this.Cbo_R_Cur_Id.Name = "Cbo_R_Cur_Id";
            this.Cbo_R_Cur_Id.Size = new System.Drawing.Size(232, 24);
            this.Cbo_R_Cur_Id.TabIndex = 1254;
            // 
            // OUT_COMM_CUR
            // 
            this.OUT_COMM_CUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.OUT_COMM_CUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OUT_COMM_CUR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OUT_COMM_CUR.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OUT_COMM_CUR.FormattingEnabled = true;
            this.OUT_COMM_CUR.Location = new System.Drawing.Point(506, 124);
            this.OUT_COMM_CUR.Name = "OUT_COMM_CUR";
            this.OUT_COMM_CUR.Size = new System.Drawing.Size(232, 24);
            this.OUT_COMM_CUR.TabIndex = 1253;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(400, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 1257;
            this.label6.Text = "بلــــــد الاصــــدار :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(741, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 14);
            this.label7.TabIndex = 1258;
            this.label7.Text = "مدينـــة الاصـدار :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(400, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 14);
            this.label8.TabIndex = 1259;
            this.label8.Text = "عملــــة الدفـــــع :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(741, 102);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 14);
            this.label12.TabIndex = 1261;
            this.label12.Text = "حـــالة العمولــة :";
            // 
            // Cmb_Comm_tpye_id
            // 
            this.Cmb_Comm_tpye_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cmb_Comm_tpye_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cmb_Comm_tpye_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Comm_tpye_id.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Comm_tpye_id.FormattingEnabled = true;
            this.Cmb_Comm_tpye_id.Location = new System.Drawing.Point(843, 97);
            this.Cmb_Comm_tpye_id.Name = "Cmb_Comm_tpye_id";
            this.Cmb_Comm_tpye_id.Size = new System.Drawing.Size(232, 24);
            this.Cmb_Comm_tpye_id.TabIndex = 1260;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(400, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 14);
            this.label9.TabIndex = 1262;
            this.label9.Text = "بلـــــد الاستــلام :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(741, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 14);
            this.label13.TabIndex = 1263;
            this.label13.Text = "مدينة الاستلام :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(741, 129);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(101, 14);
            this.label34.TabIndex = 1264;
            this.label34.Text = "الـمبلغ يبـدء من :";
            // 
            // Grd_comm_info
            // 
            this.Grd_comm_info.AllowUserToAddRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_comm_info.ColumnHeadersHeight = 40;
            this.Grd_comm_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column10,
            this.Column3,
            this.Column2,
            this.Column5,
            this.Column16,
            this.Column4,
            this.Column7,
            this.Column6,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column9,
            this.Column11,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.Format = "N3";
            dataGridViewCellStyle12.NullValue = null;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grd_comm_info.DefaultCellStyle = dataGridViewCellStyle12;
            this.Grd_comm_info.Location = new System.Drawing.Point(4, 214);
            this.Grd_comm_info.Name = "Grd_comm_info";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_comm_info.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.Grd_comm_info.RowHeadersWidth = 15;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_comm_info.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.Grd_comm_info.Size = new System.Drawing.Size(1084, 295);
            this.Grd_comm_info.TabIndex = 1265;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.Location = new System.Drawing.Point(421, 557);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(125, 29);
            this.Btn_Add.TabIndex = 1266;
            this.Btn_Add.Text = "موافـــق";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // TxtUser
            // 
            this.TxtUser.BackColor = System.Drawing.Color.White;
            this.TxtUser.Enabled = false;
            this.TxtUser.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Location = new System.Drawing.Point(462, 3);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.ReadOnly = true;
            this.TxtUser.Size = new System.Drawing.Size(318, 23);
            this.TxtUser.TabIndex = 1271;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(328, 7);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(130, 14);
            this.label14.TabIndex = 1270;
            this.label14.Text = "اسم المستخــــــــــدم:";
            // 
            // TxtTerm_Name
            // 
            this.TxtTerm_Name.BackColor = System.Drawing.Color.White;
            this.TxtTerm_Name.Enabled = false;
            this.TxtTerm_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTerm_Name.Location = new System.Drawing.Point(6, 3);
            this.TxtTerm_Name.Multiline = true;
            this.TxtTerm_Name.Name = "TxtTerm_Name";
            this.TxtTerm_Name.Size = new System.Drawing.Size(318, 23);
            this.TxtTerm_Name.TabIndex = 1269;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(786, 7);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(65, 14);
            this.label15.TabIndex = 1267;
            this.label15.Text = "التاريــــــخ:";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(857, 3);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(220, 23);
            this.TxtIn_Rec_Date.TabIndex = 1268;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-14, 32);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(1124, 1);
            this.flowLayoutPanel9.TabIndex = 1272;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(100, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel4.TabIndex = 630;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-18, 209);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1124, 1);
            this.flowLayoutPanel1.TabIndex = 1273;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(100, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel2.TabIndex = 630;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(-18, 516);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1124, 1);
            this.flowLayoutPanel3.TabIndex = 1274;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(100, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel5.TabIndex = 630;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-18, 551);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(1124, 1);
            this.flowLayoutPanel6.TabIndex = 1275;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(100, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1019, 1);
            this.flowLayoutPanel7.TabIndex = 630;
            // 
            // Btn_End
            // 
            this.Btn_End.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_End.ForeColor = System.Drawing.Color.Navy;
            this.Btn_End.Location = new System.Drawing.Point(546, 557);
            this.Btn_End.Name = "Btn_End";
            this.Btn_End.Size = new System.Drawing.Size(125, 29);
            this.Btn_End.TabIndex = 1276;
            this.Btn_End.Text = "انهــــــــــــــــاء";
            this.Btn_End.UseVisualStyleBackColor = true;
            this.Btn_End.Click += new System.EventHandler(this.Btn_End_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(201, 527);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 14);
            this.label16.TabIndex = 1280;
            this.label16.Text = "%";
            // 
            // Txt_Discount_Period
            // 
            this.Txt_Discount_Period.AllowSpace = false;
            this.Txt_Discount_Period.Location = new System.Drawing.Point(291, 524);
            this.Txt_Discount_Period.Name = "Txt_Discount_Period";
            this.Txt_Discount_Period.Size = new System.Drawing.Size(120, 20);
            this.Txt_Discount_Period.TabIndex = 1279;
            // 
            // Txt_Discount
            // 
            this.Txt_Discount.BackColor = System.Drawing.Color.White;
            this.Txt_Discount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Discount.Location = new System.Drawing.Point(76, 523);
            this.Txt_Discount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_Discount.Name = "Txt_Discount";
            this.Txt_Discount.NumberDecimalDigits = 3;
            this.Txt_Discount.NumberDecimalSeparator = ".";
            this.Txt_Discount.NumberGroupSeparator = ",";
            this.Txt_Discount.ReadOnly = true;
            this.Txt_Discount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_Discount.Size = new System.Drawing.Size(126, 23);
            this.Txt_Discount.TabIndex = 1278;
            this.Txt_Discount.Text = "0.000";
            // 
            // Txt_From_Amount
            // 
            this.Txt_From_Amount.BackColor = System.Drawing.Color.White;
            this.Txt_From_Amount.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_From_Amount.Location = new System.Drawing.Point(843, 125);
            this.Txt_From_Amount.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.Txt_From_Amount.Name = "Txt_From_Amount";
            this.Txt_From_Amount.NumberDecimalDigits = 3;
            this.Txt_From_Amount.NumberDecimalSeparator = ".";
            this.Txt_From_Amount.NumberGroupSeparator = ",";
            this.Txt_From_Amount.ReadOnly = true;
            this.Txt_From_Amount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_From_Amount.Size = new System.Drawing.Size(169, 23);
            this.Txt_From_Amount.TabIndex = 1277;
            this.Txt_From_Amount.Text = "0.000";
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "chk";
            this.Column8.FalseValue = "0";
            this.Column8.HeaderText = "تأشير";
            this.Column8.Name = "Column8";
            this.Column8.TrueValue = "1";
            this.Column8.Width = 43;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.DataPropertyName = "Out_Cust_AName";
            this.Column10.HeaderText = "الوكيل/الفرع";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 93;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "From_Amount";
            this.Column3.HeaderText = "يبدء من";
            this.Column3.Name = "Column3";
            this.Column3.Width = 52;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "R_CUR_AName";
            this.Column2.HeaderText = "عملة الحوالة";
            this.Column2.Name = "Column2";
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.Width = 81;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "Out_COMM_CUR_AName";
            this.Column5.HeaderText = "عملة عمولة";
            this.Column5.Name = "Column5";
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column5.Width = 78;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.DataPropertyName = "PR_CUR_AName";
            this.Column16.HeaderText = "عملة الدفع";
            this.Column16.Name = "Column16";
            this.Column16.Width = 76;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "OUT_Send_AName";
            this.Column4.HeaderText = "حالة العمولة (المرسل)";
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.Width = 124;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "Out_COMM_CUT";
            dataGridViewCellStyle7.Format = "N3";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column7.HeaderText = "المقطوعة (المرسل)";
            this.Column7.Name = "Column7";
            this.Column7.Width = 112;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "Out_COMM_PER";
            dataGridViewCellStyle8.Format = "N3";
            this.Column6.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column6.HeaderText = "النسبة % (المرسل)";
            this.Column6.Name = "Column6";
            this.Column6.Width = 113;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "S_Cit_AName";
            this.Column12.HeaderText = "مدينة الاصدار";
            this.Column12.Name = "Column12";
            this.Column12.Width = 87;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column13.DataPropertyName = "S_Con_AName";
            this.Column13.HeaderText = "بلد الاصدار";
            this.Column13.Name = "Column13";
            this.Column13.Width = 76;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column14.DataPropertyName = "T_Cit_AName";
            this.Column14.HeaderText = "مدينة الاستلام";
            this.Column14.Name = "Column14";
            this.Column14.Width = 88;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column15.DataPropertyName = "T_Con_AName";
            this.Column15.HeaderText = "بلد الاستلام";
            this.Column15.Name = "Column15";
            this.Column15.Width = 78;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.DataPropertyName = "Discount";
            dataGridViewCellStyle9.Format = "N3";
            this.Column17.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column17.HeaderText = "الخصم";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 62;
            // 
            // Column18
            // 
            this.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column18.DataPropertyName = "Str_Date_Discount";
            dataGridViewCellStyle10.Format = "G";
            dataGridViewCellStyle10.NullValue = null;
            this.Column18.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column18.HeaderText = "تاريخ بداية الخصم";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 107;
            // 
            // Column19
            // 
            this.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column19.DataPropertyName = "Period_Discount";
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.NullValue = null;
            this.Column19.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column19.HeaderText = "مدة الخصم";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 76;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "OUT_CS_AName";
            this.Column9.HeaderText = "حالة العمولة(الادارة)";
            this.Column9.Name = "Column9";
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "OUT_COMM_CUT_CS";
            this.Column11.HeaderText = "المقطوعة (الادارة)";
            this.Column11.Name = "Column11";
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "OUT_COMM_PER_CS";
            this.Column20.HeaderText = "النسبة % (الادارة)";
            this.Column20.Name = "Column20";
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "OTO_Out_AName";
            this.Column21.HeaderText = "حالة عمولة الادارة (اونلاين)";
            this.Column21.Name = "Column21";
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "OTO_Out_COMM_CUT";
            this.Column22.HeaderText = "المقطوعة مع الادارة (اونلاين)";
            this.Column22.Name = "Column22";
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "OTO_Out_COMM_PER";
            this.Column23.HeaderText = "النسبة مع الادارة (اونلاين)";
            this.Column23.Name = "Column23";
            // 
            // Comm_Online_Out_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 593);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Txt_Discount_Period);
            this.Controls.Add(this.Txt_Discount);
            this.Controls.Add(this.Txt_From_Amount);
            this.Controls.Add(this.Btn_End);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtUser);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TxtTerm_Name);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Grd_comm_info);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Cmb_Comm_tpye_id);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Cbo_R_Cur_Id);
            this.Controls.Add(this.OUT_COMM_CUR);
            this.Controls.Add(this.Cmb_Sign);
            this.Controls.Add(this.Txt_Start_Discount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Search_Btn);
            this.Controls.Add(this.Cbo_option);
            this.Controls.Add(this.Cbo_PR_Cur_Id);
            this.Controls.Add(this.Cbo_T_Con_ID);
            this.Controls.Add(this.Cbo_T_Cit_ID);
            this.Controls.Add(this.Cbo_S_Con_Id);
            this.Controls.Add(this.Cbo_S_Cit_ID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_Sub_Cust);
            this.Controls.Add(this.Grd_Sub_Cust);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Comm_Online_Out_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "545";
            this.Text = "ADD_DISCOUNT_COMM";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ADD_DISCOUNT_COMM_FormClosed);
            this.Load += new System.EventHandler(this.Comm_Online_Out_Upd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Sub_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_comm_info)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Sub_Cust;
        private System.Windows.Forms.DataGridView Grd_Sub_Cust;
        private System.Windows.Forms.ComboBox Cbo_PR_Cur_Id;
        private System.Windows.Forms.ComboBox Cbo_T_Con_ID;
        private System.Windows.Forms.ComboBox Cbo_T_Cit_ID;
        private System.Windows.Forms.ComboBox Cbo_S_Con_Id;
        private System.Windows.Forms.ComboBox Cbo_S_Cit_ID;
        private System.Windows.Forms.ComboBox Cbo_option;
        private System.Windows.Forms.Button Search_Btn;
        private System.Windows.Forms.Label label1;
       //private System.Windows.Forms.Sample.DecimalTextBox Txt_Discount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker Txt_Start_Discount;
        private System.Windows.Forms.ComboBox Cmb_Sign;
       // private System.Windows.Forms.Sample.DecimalTextBox Txt_From_Amount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Cbo_R_Cur_Id;
        private System.Windows.Forms.ComboBox OUT_COMM_CUR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Cmb_Comm_tpye_id;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridView Grd_comm_info;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TxtTerm_Name;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Button Btn_End;
        //private NumericTextBox Txt_Discount_Period;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_From_Amount;
        private System.Windows.Forms.Sample.DecimalTextBox Txt_Discount;
        private NumericTextBox Txt_Discount_Period;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
    }
}