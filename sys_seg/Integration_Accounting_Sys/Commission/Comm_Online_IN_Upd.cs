﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Comm_Online_IN_Upd : Form
    {
        string @format = "dd/MM/yyyy";
        BindingSource BS_Sub_Cust = new BindingSource();
        BindingSource BS_S_CITY_ID = new BindingSource();
        BindingSource BS_S_COUN_ID = new BindingSource();
        BindingSource BS_T_CITY_ID = new BindingSource();
        BindingSource BS_T_COUN_ID = new BindingSource();
        BindingSource BS_PR_Cur_Id = new BindingSource();
        BindingSource Bs_Sub_Cust = new BindingSource();
        BindingSource Bs_R_cur_id = new BindingSource();
        BindingSource Bs_OUT_COMM_CUR = new BindingSource();
        BindingSource Bs_Comm_tpye_id = new BindingSource();
        BindingSource Bs_OUT_CS_Id = new BindingSource();
        BindingSource BS_comm_info = new BindingSource();
        DataTable Dt_Cust_TBl = new DataTable();
        DataTable Dt_Comm = new DataTable();
        DataTable DT1 = new DataTable();

        public Comm_Online_IN_Upd()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_comm_info.AutoGenerateColumns = false;
            if (connection.Lang_id == 2)
            {
                Cbo_option.Items[0] = "Delete";
                Cbo_option.Items[1] = "Discount";

                Column2.DataPropertyName = "R_CUR_EName";
                Column5.DataPropertyName = "in_COMM_CUR_EName";
                Column16.DataPropertyName = "PR_CUR_EName";
                Column4.DataPropertyName = "Comm_Type_ename";
                Column12.DataPropertyName = "S_Cit_EName";
                Column13.DataPropertyName = "S_Con_EName";
                Column14.DataPropertyName = "T_Cit_EName";
                Column15.DataPropertyName = "T_Con_EName";
                Column10.DataPropertyName = "in_cust_Ename";
                dataGridViewTextBoxColumn3.DataPropertyName = "ESub_CustNmae";

            }
        }

        private void Comm_Online_Out_Upd_Load(object sender, EventArgs e)
        {
            Cmb_Sign.Items[0] = "=";
            Cmb_Sign.Items[1] = "<=";
            Cmb_Sign.Items[2] = ">=";


            Txt_Start_Discount.Format = DateTimePickerFormat.Custom;
            Txt_Start_Discount.CustomFormat = @format;


            connection.SqlExec("EXEC Update_COMM_ONLINE_IN_SEARCH_discount", "COMM_ONLINE_IN_TBL");

            BS_S_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL"].DefaultView.ToTable(true, "Con_AName", "Con_EName", "COUN_ID").Select().CopyToDataTable(); ;
            Cbo_S_Con_Id.DataSource = BS_S_COUN_ID;
            Cbo_S_Con_Id.ValueMember = "COUN_ID";
            Cbo_S_Con_Id.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

            BS_S_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL"];
            Cbo_S_Cit_ID.DataSource = BS_S_CITY_ID;
            Cbo_S_Cit_ID.ValueMember = "CITY_ID";
            Cbo_S_Cit_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";

            BS_T_COUN_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL1"].DefaultView.ToTable(true, "Con_AName", "Con_EName", "Con_ID").Select().CopyToDataTable();
            Cbo_T_Con_ID.DataSource = BS_T_COUN_ID;
            Cbo_T_Con_ID.ValueMember = "Con_ID";
            Cbo_T_Con_ID.DisplayMember = connection.Lang_id == 1 ? "Con_AName" : "Con_EName";

            BS_T_CITY_ID.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL1"];
            Cbo_T_Cit_ID.DataSource = BS_T_CITY_ID;
            Cbo_T_Cit_ID.ValueMember = "Cit_ID";
            Cbo_T_Cit_ID.DisplayMember = connection.Lang_id == 1 ? "Cit_AName" : "Cit_EName";


            BS_PR_Cur_Id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL4"];
            Cbo_PR_Cur_Id.DataSource = BS_PR_Cur_Id;
            Cbo_PR_Cur_Id.ValueMember = "cur_id";
            Cbo_PR_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";


            Bs_OUT_COMM_CUR.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL3"];
            OUT_COMM_CUR.DataSource = Bs_OUT_COMM_CUR;
            OUT_COMM_CUR.DisplayMember = connection.Lang_id == 1 ? "Cur_Aname" : "Cur_Ename";
            OUT_COMM_CUR.ValueMember = "Cur_id";

            Bs_R_cur_id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL2"];
            Cbo_R_Cur_Id.DataSource = Bs_R_cur_id;
            Cbo_R_Cur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME";
            Cbo_R_Cur_Id.ValueMember = "Cur_id";


            if (connection.SQLDS.Tables["COMM_ONLINE_IN_TBL7"].Rows.Count > 0)
            {
                Dt_Cust_TBl = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL7"].DefaultView.ToTable().Select().CopyToDataTable();
                Dt_Cust_TBl.TableName = "Dt_Cust_TBl";
                Bs_Sub_Cust.DataSource = Dt_Cust_TBl;
                Grd_Sub_Cust.DataSource = Bs_Sub_Cust;
            }


            Bs_Comm_tpye_id.DataSource = connection.SQLDS.Tables["COMM_ONLINE_IN_TBL6"];
            Cmb_Comm_tpye_id.DataSource = Bs_Comm_tpye_id;
            Cmb_Comm_tpye_id.DisplayMember = connection.Lang_id == 1 ? "Type_rem_ANAME" : "Type_rem_ENAME";
            Cmb_Comm_tpye_id.ValueMember = "type_rem_ID";

            Cmb_Sign.SelectedIndex = 0;
            Cbo_option.SelectedIndex = 1;
            Cmb_Comm_tpye_id.SelectedValue = 0;
            //***
           

        }

        private void Search_Btn_Click(object sender, EventArgs e)
        {
            DataTable DT_cust = new DataTable();
            try
            {
                DT_cust = Dt_Cust_TBl.DefaultView.ToTable(true, "Chk", "Sub_Cust_ID").Select("Chk > 0").CopyToDataTable();
                DT_cust = DT_cust.DefaultView.ToTable(true, "Sub_Cust_ID").Select().CopyToDataTable();
            }
            catch
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار الوكيل" : "Please select the Agent.", MyGeneral_Lib.LblCap);
                Grd_comm_info.DataSource = new BindingSource();
                return;
            }



            if (connection.SQLDS.Tables.Contains("comm_search_tbl"))
            {
                connection.SQLDS.Tables.Remove("comm_search_tbl");
            }

            string Cust_IDStr = "";
            MyGeneral_Lib.ColumnToString(DT_cust, "Sub_Cust_ID", out  Cust_IDStr);


            connection.SQLCS.Open();
            connection.SQLCMD.CommandText = "IN_comm_search";
            connection.SQLCMD.CommandType = CommandType.StoredProcedure;
            connection.SQLCMD.CommandTimeout = 0;
            connection.SQLCMD.Connection = connection.SQLCS;
            connection.SQLCMD.Parameters.AddWithValue("@Cust_IDStr", Cust_IDStr);
            connection.SQLCMD.Parameters.AddWithValue("@S_COUN_ID", Cbo_S_Con_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@S_CITY_ID", Cbo_S_Cit_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@T_COUN_ID", Cbo_T_Con_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@T_CITY_ID", Cbo_T_Cit_ID.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@PR_Cur_Id", Cbo_PR_Cur_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Out_Comm_Cur_Id", OUT_COMM_CUR.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@Comm_tpye_id", Cmb_Comm_tpye_id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@R_CUR_ID", Cbo_R_Cur_Id.SelectedValue);
            connection.SQLCMD.Parameters.AddWithValue("@From_Amount", Convert.ToDecimal(Txt_From_Amount.Text.Trim()));
            connection.SQLCMD.Parameters.AddWithValue("@Sign", Cmb_Sign.Text.Trim());
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@T_ID", connection.T_ID);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            MyGeneral_Lib.Copytocliptext("IN_comm_search", connection.SQLCMD);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                connection.SQLCMD.Parameters.Clear();
                return;
            }

            IDataReader obj = connection.SQLCMD.ExecuteReader();
            connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "comm_search_tbl");
            obj.Close();
            connection.SQLCS.Close();
            connection.SQLCMD.Parameters.Clear();
            connection.SQLCMD.Dispose();

            if (connection.SQLDS.Tables["comm_search_tbl"].Rows.Count > 0)
            {
                Dt_Comm = connection.SQLDS.Tables["comm_search_tbl"].DefaultView.ToTable().Select().CopyToDataTable();
                Dt_Comm.TableName = "Dt_Comm";
                BS_comm_info.DataSource=Dt_Comm ;
                Grd_comm_info.DataSource = BS_comm_info;
            }

            else
            {

                Grd_comm_info.DataSource = new BindingSource();
                MessageBox.Show(connection.Lang_id == 1 ? "لاتوجد بيانات للعرض" : "There is no data to display", MyGeneral_Lib.LblCap);
                return;
            }

            Txt_Discount.Text = "0.000";
            Txt_Discount_Period.Text = "";
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (Grd_comm_info.RowCount > 0)
            {

                #region Validation

                if (Cbo_option.SelectedIndex == 1 && Txt_Discount.Text == "0.000")
                  {
                      MessageBox.Show(connection.Lang_id == 1 ? "يرجى ادخال الخصم" : "Please enter your discount", MyGeneral_Lib.LblCap);
                      return;
                  }


                  if (Cbo_option.SelectedIndex == 1 && Txt_Discount_Period.Text == "")
                  {
                      MessageBox.Show(connection.Lang_id == 1 ? "يرجى ادخال مدة الخصم" : "Please enter your discount period", MyGeneral_Lib.LblCap);
                      return;
                  }
                 

                  DateTime Current_Date = Convert.ToDateTime(DateTime.Now.ToString("d"));
                  DateTime Start_Discount_Date = Convert.ToDateTime(Txt_Start_Discount.Value.Date.ToString("d"));

                  if (Start_Discount_Date < Current_Date)
                  {
                      MessageBox.Show(connection.Lang_id == 2 ? "The start date of the discount may not be smaller than the current date" : "لا يجوز ان يكون تاريخ بداية الخصم اصغر من تاريخ الحالي", MyGeneral_Lib.LblCap);
                      return;
                  }

                #endregion

                  try
               {
                       Dt_Comm = Dt_Comm.DefaultView.ToTable(true, "Chk", "in_Comm_Rem_ID").Select("Chk > 0").CopyToDataTable();
                       Dt_Comm = Dt_Comm.DefaultView.ToTable(true, "in_Comm_Rem_ID").Select().CopyToDataTable();
               }

            catch { 
                        MessageBox.Show(connection.Lang_id == 1 ? "يرجى اختيار الوكيل" : "Please select the Agent.", MyGeneral_Lib.LblCap);
                        return;
                  }



                         string Start_Discount = Txt_Start_Discount.Value.ToString("yyyy/MM/dd");

                         connection.SQLCMD.Parameters.AddWithValue("@IN_comm_Type", Dt_Comm);
                        connection.SQLCMD.Parameters.AddWithValue("@Flag_Case", Cbo_option.SelectedIndex);
                        connection.SQLCMD.Parameters.AddWithValue("@user_id", connection.user_id);
                        connection.SQLCMD.Parameters.AddWithValue("@Discount_Amount",Cbo_option.SelectedIndex ==1 ? Txt_Discount.Text.Trim() : "0.000");
                        connection.SQLCMD.Parameters.AddWithValue("@Discount_Date", Cbo_option.SelectedIndex == 1 ? Start_Discount : "0000/00/00");
                        connection.SQLCMD.Parameters.AddWithValue("@Dicount_Period", Cbo_option.SelectedIndex == 1 ? Convert.ToInt16(Txt_Discount_Period.Text.Trim()):0);
                        connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                        connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                        MyGeneral_Lib.Copytocliptext("Comm_Online_in_Disscount_Delete", connection.SQLCMD);
                        connection.SqlExec("Comm_Online_in_Disscount_Delete", connection.SQLCMD);

                        if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "" )
                        {
                            MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                            connection.SQLCMD.Parameters.Clear();
                            return;
                        }
                        connection.SQLCMD.Parameters.Clear();
                        MessageBox.Show(connection.Lang_id == 1 ? " تم التحديث بتاريخ " + DateTime.Now : " Updating Done " + DateTime.Now, MyGeneral_Lib.LblCap);
                        this.Close();
            
                   }
      else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يرجى البحث اولا" : "Please search first", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void Cbo_option_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (Cbo_option.SelectedIndex == 0)
            {
            label3.Visible=false;
            Txt_Discount.Visible=false;
            label4.Visible=false;
            Txt_Discount_Period.Visible=false;
            label5.Visible=false;
            Txt_Start_Discount.Visible = false;
            Txt_Discount.Text = "0.000";
            Txt_Discount_Period.Text = "";


            }

            if (Cbo_option.SelectedIndex == 1)
            {
                label3.Visible = true;
                Txt_Discount.Visible = true;
                label4.Visible = true;
                Txt_Discount_Period.Visible = true;
                label5.Visible = true;
                Txt_Start_Discount.Visible = true;
                Txt_Discount.Text = "0.000";
                Txt_Discount_Period.Text = "";
            }
        }

        private void ADD_DISCOUNT_COMM_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "COMM_ONLINE_in_TBL", "COMM_ONLINE_in_TBL1", "COMM_ONLINE_in_TBL2", "COMM_ONLINE_in_TBL3", "COMM_ONLINE_in_TBL4", "comm_search_tbl" 
                                ,"COMM_ONLINE_in_TBL5","COMM_ONLINE_in_TBL6","COMM_ONLINE_in_TBL7"};

            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Txt_Sub_Cust_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int Sub_cust_id = 0;
                int.TryParse(Txt_Sub_Cust.Text, out Sub_cust_id);


                if (Txt_Sub_Cust.Text != "")
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = "( Sub_Cust_ID  = " + Sub_cust_id + " OR ASub_CustName like '%" + Txt_Sub_Cust.Text.Trim() + "%'  )";
                    DT1 = DT1.DefaultView.ToTable();
                }
                else
                {
                    DT1 = Dt_Cust_TBl;
                    DT1.DefaultView.RowFilter = " Sub_Cust_ID > " + Sub_cust_id;
                    DT1 = DT1.DefaultView.ToTable();
                }
            }
            catch { }
        }
        //***
        private void Btn_End_Click(object sender, EventArgs e)
        {
            this.Close();
        }

         

       
    }
}
