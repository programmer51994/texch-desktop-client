﻿using System;
using System.Data;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reveals;

namespace Integration_Accounting_Sys
{
    public partial class Annual_closing_Main : Form
    {
        BindingSource BS_Cbo_Terminals = new BindingSource();
        BindingSource BS_Grd_Acc_Close = new BindingSource();
        string @format = "dd/MM/yyyy";
        Int32  nrec_date = 0;
        bool change_Cbot_Ter = false;

        public Annual_closing_Main()
        {
            InitializeComponent(); 
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_Acc_Close.AutoGenerateColumns = false;

            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Acc_Close.Columns["Column2"].DataPropertyName = "ACC_EName";
                Grd_Acc_Close.Columns["Column3"].DataPropertyName = "acc_id_enote"; 
            }
            #endregion
        }

        private void Annual_closing_Main_Load(object sender, EventArgs e)
        {
            change_Cbot_Ter = false;

            Txt_nrec_date.Format = DateTimePickerFormat.Custom;
            Txt_nrec_date.CustomFormat = @format;


            connection.SqlExec("Annual_closing_Main", "Annual_closing_Main_Tbl");

            if (connection.SQLDS.Tables["Annual_closing_Main_Tbl"].Rows.Count > 0)
            {
                BS_Cbo_Terminals.DataSource = connection.SQLDS.Tables["Annual_closing_Main_Tbl"];
                Cbo_Terminals.DataSource = BS_Cbo_Terminals;
                Cbo_Terminals.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
                Cbo_Terminals.ValueMember = "t_id";

                change_Cbot_Ter = true;
                Cbo_Terminals_SelectedIndexChanged(null, null);
            }

       else {

           MessageBox.Show(connection.Lang_id == 1 ? "يجب ضبط اعدادات الاغلاق السنوي للفرع اولا" : "The annual closing settings of the branch must be set first", MyGeneral_Lib.LblCap);
                this.Close();
                return;
            }

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (Grd_Acc_Close.RowCount > 0)
            {
                if (Txt_nrec_date.Checked == true)
                {

                    DateTime date = Txt_nrec_date.Value.Date;
                    nrec_date = date.Day + date.Month * 100 + date.Year * 10000;
                }
                else
                {
                    MessageBox.Show(connection.Lang_id == 1 ? " يجب اختيار تاريخ الاغلاق السنوي" : "The annual closing date should be chosen", MyGeneral_Lib.LblCap);
                    return;
                }

                DialogResult Dr = MessageBox.Show(connection.Lang_id == 1 ? "هل تريدالاغلاق؟" : "Do you want to close", MyGeneral_Lib.LblCap,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Dr == DialogResult.Yes)
                {


                    connection.SqlExec("End_f_close_Chk_Rem " + nrec_date + "," + Convert.ToInt16(Cbo_Terminals.SelectedValue), "Chk_Rem_tbl");
                    if (connection.SQLDS.Tables["Chk_Rem_tbl"].Rows.Count > 0)
                    {
                        Rem_Count_closing AddFrm = new Rem_Count_closing();
                        AddFrm.ShowDialog(this);
                        if (Rem_Count_closing.ok_btn == 0)
                        {
                            MessageBox.Show(connection.Lang_id == 1 ? "تم ايقاف الاغلاق لوجود حوالات معلقة" : "The closure has been suspended because of pending Rem.", MyGeneral_Lib.LblCap);
                            return;
                        }

                        else
                        { MessageBox.Show(connection.Lang_id == 1 ? "سيتم متابعة الاغلاق لتجاهلك الحوالات المعلقة" : "You cancel pending Rem.", MyGeneral_Lib.LblCap); }
                    }
                        
                    connection.SQLCMD.Parameters.AddWithValue("@T_id", Convert.ToInt16(Cbo_Terminals.SelectedValue));
                    connection.SQLCMD.Parameters.AddWithValue("@nrec_date", nrec_date);
                    connection.SQLCMD.Parameters.AddWithValue("@USER_ID", connection.user_id);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SqlExec("End_f_close", connection.SQLCMD);
               
                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        return;
                    }
                    else 
                    {
                        connection.SQLCMD.Parameters.Clear();
                        MessageBox.Show(connection.Lang_id == 1 ? " تم الاغلاق السنوي" : "The Annual closing Completed", MyGeneral_Lib.LblCap);
                        this.Close();
                    }
                }
            }
        }

        private void Cbo_Terminals_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (change_Cbot_Ter)
            {

                BS_Grd_Acc_Close.DataSource = connection.SQLDS.Tables["Annual_closing_Main_Tbl1"].DefaultView.ToTable().Select("t_id =" + Convert.ToInt16(Cbo_Terminals.SelectedValue)).CopyToDataTable();
                Grd_Acc_Close.DataSource = BS_Grd_Acc_Close;
            }


        }

        private void Annual_closing_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Annual_closing_Main_TBl", "Annual_closing_Main_TBl1" };

            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);

                }
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
