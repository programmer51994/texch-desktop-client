﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integration_Accounting_Sys.Reports;
using Integration_Accounting_Sys.Branch_Report;

namespace Integration_Accounting_Sys
{
    public partial class Annual_closing : Form
    {
        

        string Filter = "";
        BindingSource BS_Acc_profit = new BindingSource();
        BindingSource BS_Acc_losses = new BindingSource();
        BindingSource BS_Acc_Center = new BindingSource();
        BindingSource BS_Cbo_terminal = new BindingSource();
        BindingSource BS_C_Acc_profit_losses = new BindingSource();
        BindingSource BS_Acc_Terminals = new BindingSource();
       
        Int16 center_flag = 0;
        

        bool Change_Acc_profit = false;
        bool Change_Acc_losses = false;
        bool Change_Acc_Center = false;
        bool Change_Cbo_terminal = false;
        bool Change_C_Acc_profit_losses = false;
        bool Change_Acc_Terminals = false;


      
       
 
        public Annual_closing()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(this, Txt_Loc_Cur, TxtUser, new TextBox(), TxtIn_Rec_Date,  new TextBox());

            Grd_Acc_losses.AutoGenerateColumns = false;
            Grd_Acc_profit.AutoGenerateColumns = false;
            Grd_Acc_Center.AutoGenerateColumns = false;
            Grd_C_Acc_profit_losses.AutoGenerateColumns = false;
            Grd_Acc_Terminals.AutoGenerateColumns = false;



            Grd_Acc_profit.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Acc_profit.MultiSelect = false;

            Grd_Acc_losses.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Acc_losses.MultiSelect = false;

            Grd_Acc_Center.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Acc_Center.MultiSelect = false;


            Grd_C_Acc_profit_losses.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_C_Acc_profit_losses.MultiSelect = false;

            Grd_Acc_Terminals.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Grd_Acc_Terminals.MultiSelect = false;


            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Grd_Acc_profit.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "ACC_EName";
                Grd_Acc_losses.Columns["dataGridViewTextBoxColumn5"].DataPropertyName = "ACC_EName";
                Grd_Acc_Center.Columns["dataGridViewTextBoxColumn61"].DataPropertyName = "ACC_EName";
                Grd_C_Acc_profit_losses.Columns["dataGridViewTextBoxColumn10"].DataPropertyName = "ACC_EName";
                Grd_Acc_Terminals.Columns["dataGridViewTextBoxColumn6"].DataPropertyName = "ACC_EName";
            }
            #endregion

        }


     
        private void Annual_closing_Load(object sender, EventArgs e)
        {

            Change_Acc_profit = false;
            Change_Acc_losses = false;
            Change_Acc_Center = false;
            Change_Cbo_terminal = false;
            Change_C_Acc_profit_losses = false;
            Change_Acc_Terminals = false;

            connection.SqlExec(" SELECT   A.CUST_ID, ACUST_NAME, ECUST_NAME ,center_flag ,t_id "
                                + " FROM CUSTOMERS A ,terminals B "
                                + "where A.cust_id =B.CUST_ID "
                                + "and t_id not in (select distinct t_id from acc_end_close)", "Annual_closing_Terminals_TBl");

            if (connection.SQLDS.Tables["Annual_closing_Terminals_TBl"].Rows.Count > 0)
            {
                BS_Cbo_terminal.DataSource = connection.SQLDS.Tables["Annual_closing_Terminals_TBl"];
                Cbo_terminal.DataSource = BS_Cbo_terminal;
                Cbo_terminal.DisplayMember = connection.Lang_id == 1 ? "ACUST_NAME" : "ECUST_NAME";
                Cbo_terminal.ValueMember = "t_id";

                Change_Cbo_terminal = true;
                Cbo_terminal_SelectedValueChanged(null, null);
            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "جميع الافرع تم ضبط اعدادات اغلاقها السنوي" : "All branches have their annual closing settings set", MyGeneral_Lib.LblCap);
                this.Close();
                return;
            }

        }

        private void Cbo_terminal_SelectedValueChanged(object sender, EventArgs e)
        {
            
            if (Change_Cbo_terminal)
            {
                connection.SqlExec("Annual_closing_Add " + Convert.ToInt32(Cbo_terminal.SelectedValue), "Annual_closing_Add_TBl");

                Change_Acc_profit = true;
                //Txt_Acc_profit_TextChanged(null, null);

                Change_Acc_losses = true;
                //Txt_Acc_losses_TextChanged(null, null);

                Change_Acc_Center = true;
                Txt_Acc_Center_TextChanged(null, null);

                Change_C_Acc_profit_losses = true;
                Txt_C_Acc_profit_losses_TextChanged(null, null);

                Change_Acc_Terminals = true;
                Txt_Acc_Termials_TextChanged(null, null);


                Txt_Acc_profit.Text = "";
                Txt_Acc_losses.Text = "";
                Txt_Acc_Center.Text = "";
                Txt_Acc_Termials.Text = "";
                Txt_C_Acc_profit_losses.Text = "";


                center_flag = Convert.ToInt16(connection.SQLDS.Tables["Annual_closing_Add_TBl"].DefaultView.ToTable().Select("T_ID =" + Convert.ToInt16(Cbo_terminal.SelectedValue)).CopyToDataTable().Rows[0]["center_flag"]);

                if (center_flag == 1)
                {
                    label13.Visible = false;
                    label12.Visible = false;
                    label10.Visible = false;
                    label8.Visible = false;
                    label7.Visible = false;
                    Txt_C_Acc_profit_losses.Visible = false;
                    Txt_Acc_Termials.Visible = false;
                    Txt_Acc_Center.Visible = false;
                    Grd_C_Acc_profit_losses.Visible = false;
                    Grd_Acc_Terminals.Visible = false;
                    Grd_Acc_Center.Visible = false;
                    flowLayoutPanel7.Visible = false;
                    flowLayoutPanel2.Visible = false;

                    this.Height = 295;

                    Btn_OK.Location = new Point(275, 230);
                    ExtBtn.Location = new Point(374, 230);
                    flowLayoutPanel3.Size = new Size(1, 186);
                    flowLayoutPanel1.Size = new Size(1, 186);

                }

                else
                {
                    label13.Visible = true;
                    label12.Visible = true;
                    label10.Visible = true;
                    label8.Visible = true;
                    label7.Visible = true;
                    Txt_C_Acc_profit_losses.Visible = true;
                    Txt_Acc_Termials.Visible = true;
                    Txt_Acc_Center.Visible = true;
                    Grd_C_Acc_profit_losses.Visible = true;
                    Grd_Acc_Terminals.Visible = true;
                    Grd_Acc_Center.Visible = true;
                    flowLayoutPanel7.Visible = true;
                    flowLayoutPanel2.Visible = true;


                    this.Height = 570;

                    Btn_OK.Location = new Point(275, 508);
                    ExtBtn.Location = new Point(374, 508);
                    flowLayoutPanel3.Size = new Size(1, 466);
                    flowLayoutPanel1.Size = new Size(1, 466);

  
                }
            }
        }

        private void Txt_Acc_profit_TextChanged(object sender, EventArgs e)
        {
            if (Change_Acc_profit)
            {
                try
                {
                    
                    Int32 ACC_Id = 0;
                    int.TryParse(Txt_Acc_profit.Text, out ACC_Id);

                    Filter = " (ACC_AName like '" + Txt_Acc_profit.Text.Trim() + "%' or  Acc_EName like '" + Txt_Acc_profit.Text.Trim() + "%'"
                                + " Or ACC_Id = " + ACC_Id + ")";

                    BS_Acc_profit.DataSource = connection.SQLDS.Tables["Annual_closing_Add_TBl1"].Select(Filter).CopyToDataTable();
                    Grd_Acc_profit.DataSource = BS_Acc_profit;

                }
                catch
                {
                    Grd_Acc_profit.DataSource = new DataTable();
                }
            }
        }

        private void Txt_Acc_losses_TextChanged(object sender, EventArgs e)
        {
            if (Change_Acc_losses)
            {
                try
                {
                    Int32 ACC_Id = 0;
                    int.TryParse(Txt_Acc_losses.Text, out ACC_Id);

                    Filter = " (ACC_AName like '" + Txt_Acc_losses.Text.Trim() + "%' or  Acc_EName like '" + Txt_Acc_losses.Text.Trim() + "%'"
                                + " Or ACC_Id = " + ACC_Id + ")";

                   
                    BS_Acc_losses.DataSource =  connection.SQLDS.Tables["Annual_closing_Add_TBl1"].Select(Filter).CopyToDataTable();
                    Grd_Acc_losses.DataSource = BS_Acc_losses;

                }
                catch
                {
                    Grd_Acc_losses.DataSource = new DataTable();
                }   
            }
        }


        private void Txt_Acc_Center_TextChanged(object sender, EventArgs e)
        {
            if (Change_Acc_Center)
            {
                try
                {
                    Int32 ACC_Id = 0;
                    int.TryParse(Txt_Acc_Center.Text, out ACC_Id);

                    Filter = " (ACC_AName like '" + Txt_Acc_Center.Text.Trim() + "%' or  Acc_EName like '" + Txt_Acc_Center.Text.Trim() + "%'"
                                + " Or ACC_Id = " + ACC_Id + ")";

                      
                    BS_Acc_Center.DataSource  = connection.SQLDS.Tables["Annual_closing_Add_TBl2"].DefaultView.ToTable().Select(Filter).CopyToDataTable();
                    Grd_Acc_Center.DataSource = BS_Acc_Center;

                }
                catch
                {
                    Grd_Acc_Center.DataSource = new DataTable();
                }
            }
        }




        private void Txt_C_Acc_profit_losses_TextChanged(object sender, EventArgs e)
        {
            if (Change_C_Acc_profit_losses)
            {
                try
                {
                    Int32 ACC_Id = 0;
                    int.TryParse(Txt_C_Acc_profit_losses.Text, out ACC_Id);

                    Filter = " (ACC_AName like '" + Txt_C_Acc_profit_losses.Text.Trim() + "%' or  Acc_EName like '" + Txt_C_Acc_profit_losses.Text.Trim() + "%'"
                                + " Or ACC_Id = " + ACC_Id + ")";


                    BS_C_Acc_profit_losses.DataSource = connection.SQLDS.Tables["Annual_closing_Add_TBl1"].DefaultView.ToTable().Select(Filter).CopyToDataTable();
                    Grd_C_Acc_profit_losses.DataSource = BS_C_Acc_profit_losses;

                }
                catch
                {
                    Grd_C_Acc_profit_losses.DataSource = new DataTable();
                }
            }
        }

        

        private void Txt_Acc_Termials_TextChanged(object sender, EventArgs e)
        {
            if (Change_Acc_Terminals)
            {
                try
                {
                    Int32 ACC_Id = 0;
                    int.TryParse(Txt_Acc_Termials.Text, out ACC_Id);

                    Filter = " (ACC_AName like '" + Txt_Acc_Termials.Text.Trim() + "%' or  Acc_EName like '" + Txt_Acc_Termials.Text.Trim() + "%'"
                                + " Or ACC_Id = " + ACC_Id + ")";

                     
                    BS_Acc_Terminals.DataSource = connection.SQLDS.Tables["Annual_closing_Add_TBl3"].DefaultView.ToTable().Select(Filter).CopyToDataTable();
                    Grd_Acc_Terminals.DataSource = BS_Acc_Terminals;

                }
                catch
                {
                    Grd_Acc_Terminals.DataSource = new DataTable();
                }
            }
        }

        private void Btn_OK_Click(object sender, EventArgs e)
       
       
        
        
        {

            if (Grd_Acc_profit.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار حساب الارباح" : "You must choose a profit account", MyGeneral_Lib.LblCap);
                return;
            }

            if (Grd_Acc_losses.Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار حساب الخسائر" : "You must choose a losses account", MyGeneral_Lib.LblCap);
                return;
            }



            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id_profit", Convert.ToInt32(((DataRowView)BS_Acc_profit.Current).Row["ACC_Id"]));
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id_losses", Convert.ToInt32(((DataRowView)BS_Acc_losses.Current).Row["ACC_Id"]));
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id_Center", center_flag == 0 ? Convert.ToInt32(((DataRowView)BS_Acc_Center.Current).Row["ACC_Id"]) : 0);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id_Termials", center_flag == 0 ? Convert.ToInt32(((DataRowView)BS_Acc_Terminals.Current).Row["ACC_Id"]) : 0);
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Id_C_profit_losses",  center_flag == 0 ? Convert.ToInt32(((DataRowView)BS_C_Acc_profit_losses.Current).Row["ACC_Id"]) : 0);
            connection.SQLCMD.Parameters.AddWithValue("@t_id",Convert.ToInt16(Cbo_terminal.SelectedValue));
            connection.SQLCMD.Parameters.AddWithValue("@user_id", 2);
            connection.SqlExec("Insert_Acc_End_Close", connection.SQLCMD);
            connection.SQLCMD.Parameters.Clear();
            MessageBox.Show(connection.Lang_id == 1 ? "تم ضبط الاعدادات" : "Settings set", MyGeneral_Lib.LblCap);
            this.Close();
        }

        private void Annual_closing_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Used_Tbl = { "Annual_closing_Add_TBl", "Annual_closing_Add_TBl1", "Annual_closing_Add_TBl2", "Annual_closing_Add_TBl3","Annual_closing_Terminals_TBl"};

            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);

                }
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}