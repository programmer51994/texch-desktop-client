﻿using System;
using System.Data;
using System.Windows.Forms;


namespace Integration_Accounting_Sys
{
    public partial class Rem_Count_closing : Form
    {
        BindingSource Bs_Rem = new BindingSource();
        public static Int16 ok_btn = 0;
        //-----------------------
        public Rem_Count_closing()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Grd_Rem.AutoGenerateColumns = false;


        }
        private void Rem_Count_closing_Load(object sender, EventArgs e)
        {
            if (connection.Lang_id != 1)
            {
                Column4.DataPropertyName = "R_ECUR_NAME";
                Column6.DataPropertyName = "ECASE_NA";
                Column7.DataPropertyName = "e_local_international";
                //----------------------------------------------------//
                Column2.HeaderText = "Rem. No.";
                Column3.HeaderText = "Amount";
                Column4.HeaderText = "Rem. Currancy";
                Column5.HeaderText = "Rrm. Date";
                Column6.HeaderText = "Rem. State";
                Column7.HeaderText = "Rem. Type";

                btn_ok.Text = "continue";
                btn_Back.Text = "fall back";
                Btn_Export.Text = "Export";

                label6.Text = "There are pending Rem.";


            }
            ok_btn = 0;
            Bs_Rem.DataSource = connection.SQLDS.Tables["Chk_Rem_tbl"];
            Grd_Rem.DataSource = Bs_Rem;

        }



        private void btn_ok_Click(object sender, EventArgs e)
        {
            ok_btn = 1;
            this.Close();
        }
        private void btn_Back_Click(object sender, EventArgs e)
        {
            ok_btn = 0;
            this.Close();
        }

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            DataTable ExpDt = connection.SQLDS.Tables["Chk_Rem_tbl"].DefaultView.ToTable(false, "rem_no", "r_amount", connection.Lang_id == 1 ? "R_ACUR_NAME" : "R_ECUR_NAME", "c_date", connection.Lang_id == 1 ? "ACASE_NA" : "ECASE_NA",
            connection.Lang_id == 1 ? "a_local_international" : "e_local_international");
            DataTable[] _EXP_DT = { ExpDt };
            DataGridView[] Export_GRD = { Grd_Rem };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }

        private void Rem_Count_closing_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Used_Tbl = { "Chk_Rem_tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
    }
}