﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Terminal_Acc_Close : Form
    {
        String Sql_Text = "";
        bool Change = false;
        public Terminal_Acc_Close()
        {
            InitializeComponent();
            Grd_close_date.AutoGenerateColumns = false;
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
        }

        

        private void Terminal_Acc_Close_Load(object sender, EventArgs e)
        {

            Change = false; 
            Sql_Text = "  select 0 as chk,A.STARTDATE,  A. T_ID,B.acust_name, B.ECUST_NAME "
                    + "  from  Terminals A , CUSTOMERS B "
                      + " where A.cust_Id = B.cust_id ";


                 
           Cbo_Terminal_ID.DataSource = connection.SqlExec(Sql_Text, "Terminals_Tbl");
           Cbo_Terminal_ID.ValueMember = "T_ID";
            Cbo_Terminal_ID.DisplayMember = (connection.Lang_id) == 1 ? "ACUST_NAME" : "ECUST_NAME";
            if (connection.SQLDS.Tables["Terminals_Tbl"].Rows.Count > 0)
            {
                Change = true;
                Cbo_Terminal_ID_SelectedIndexChanged(null, null);

            }
           
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {


            if (connection.SQLDS.Tables["Terminal_Acc_Close_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show("لا توجد قيود للاضافة", MyGeneral_Lib.LblCap);
                return;
            }

        
           
            this.Close();  
        }

        private void Grd_close_date_SelectionChanged(object sender, EventArgs e)
        {
          //  LblRec.Text = connection.Records(connection.SQLBS);
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Terminal_Acc_Close_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change = false;
            string[] Tbl = { "Terminal_Acc_Close_Tbl", "Terminals_Tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }

        private void Cbo_Terminal_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change)
            {
                Grd_close_date.DataSource = connection.SQLDS.Tables["Terminals_Tbl"].Select("T_ID = " + Cbo_Terminal_ID.SelectedValue).CopyToDataTable();
            }
        }



      
    }
}
    
