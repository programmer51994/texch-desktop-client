﻿using System;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;


namespace Integration_Accounting_Sys
{
    public partial class Branch_RptLang_MsgBox : Form
    {
        #region MyRegion
        ReportClass ObjRpt = new ReportClass();
        ReportClass ObjRpteng = new ReportClass();
        DataTable Prm_Dt = new DataTable() ;
        int Oper_Id = 0;
        bool IsComp = false;
        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RPT">اسم الريبورت العربي</param>
        /// <param name="RPTeng">اسم الريبورت الانكليزي</param>
        /// <param name="Print_Flag">تفعيل زر الطباعة</param>
        /// <param name="Pdf_Flag">تفعيل زر الpdf</param>
        /// <param name="Excel_Flag">تفعيل زر الاكسل</param>
        /// <param name="DT"> يستخدم ل</param>
        /// <param name="Grd">اسم الكرد المستخدم في الاكسل</param>
        /// <param name="EXP_DT">تستخدم لقراءة الداته مباشرة من datatable لاستخدامها في الاكسل</param>
        /// <param name="Visible_Flag"> طباعة الكرد كاملة مع الcolumn المخفية =True المستخدمة في الاكسل</param>
        public Branch_RptLang_MsgBox(ReportClass RPT, ReportClass RPTeng, bool Print_Flag = false, bool Pdf_Flag = false, DataTable DT = null, int oper_id = 0, bool isComp = false)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Btn_Rpt.Enabled = Print_Flag;
            Btn_Excel.Enabled = Pdf_Flag;
            Prm_Dt = DT;
            ObjRpt = RPT;
            ObjRpteng = RPTeng;
            Oper_Id = oper_id;
            IsComp = isComp;

            
        }
      
        //-----------------------------------------------------
        private void RptLang_MsgBox_Load(object sender, EventArgs e)
        {
            string Sql_Text = " Select AOPER_NAME,EOPER_NAME,OPER_ID from OPERATIONS where OPER_ID ="+Oper_Id;

            connection.SqlExec(Sql_Text, "Oper_Tbl");

            if (IsComp)
            {
                Sql_Text = "SELECT  AComp_Name,EComp_Name,logo FROM   Companies  where  comp_Id=" + connection.Comp_Id;
                connection.SqlExec(Sql_Text, "comp_name");
            }
           
            ObjRpt.SetDataSource(connection.SQLDS);
            ObjRpteng.SetDataSource(connection.SQLDS);
            try
            {
                if (Prm_Dt.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Prm_Dt.Rows)
                    {
                        ObjRpt.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                        ObjRpteng.SetParameterValue(Dr["Param_Name"].ToString(), Dr["Param_Value"]);
                    }
                }
            }
            catch { }
            CboLang_Rpt.SelectedIndex = 0;
            numericUpDown1.Minimum = 1;
            numericUpDown1.Maximum = 2;
        }
        //-----------------------------------------------------
      
        private void Prt_Btn_Click(object sender, EventArgs e)
        {
            if (CboLang_Rpt.SelectedIndex == 1)
            {
                try
                {
                ObjRpteng.PrintToPrinter(Convert.ToInt16( numericUpDown1.Value), false, 0, 0);
                }
                catch { }
            }
            else
            {
                try
                {
                    ObjRpt.PrintToPrinter(Convert.ToInt16(numericUpDown1.Value), false, 0, 0);
                }
                catch { }
            }
            this.Close();
        }
        //---------------------------
        private void Btn_Pdf_Click(object sender, EventArgs e)
        {
            string FileName = string.Empty;
            if (CboLang_Rpt.SelectedIndex == 1)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileName = saveFileDialog1.FileName + ".pdf";
                    ObjRpteng.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");
                    
                }
            }
            else
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileName = saveFileDialog1.FileName + ".pdf";
                   ObjRpt.ExportToDisk(ExportFormatType.PortableDocFormat, saveFileDialog1.FileName + ".pdf");
                }
            }
            System.Diagnostics.Process.Start(FileName);
            this.Close();
        }
    }
}
