﻿namespace Integration_Accounting_Sys
{
    partial class Accountant_Close_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.GrdAcc_Close_price = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grd_Close_Hst = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel189 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel190 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel191 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel192 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel193 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel194 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel195 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel196 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel197 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel198 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel199 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel200 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel201 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel202 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel203 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel204 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel205 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel206 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel207 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel208 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel209 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel210 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel211 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel212 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel213 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel214 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel215 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel216 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel217 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel218 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel219 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel220 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel221 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel222 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel223 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel224 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel225 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel226 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel227 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel228 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel229 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel230 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel231 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel232 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel233 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel234 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel235 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel236 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel237 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel238 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel239 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel240 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel241 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel242 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel243 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel244 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel245 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel246 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel247 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel248 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel249 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel250 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.Btn_Ok = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.CboClose_Date = new System.Windows.Forms.ComboBox();
            this.Grd_Term = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel46 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel47 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel48 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel49 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel50 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel51 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel52 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel54 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel55 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel56 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel57 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel58 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel59 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel60 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel61 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel62 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel63 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel64 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.Cbo_CloseingState = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.TxtClsoe_Date = new Integration_Accounting_Sys.MyDateTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAcc_Close_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Close_Hst)).BeginInit();
            this.flowLayoutPanel189.SuspendLayout();
            this.flowLayoutPanel194.SuspendLayout();
            this.flowLayoutPanel199.SuspendLayout();
            this.flowLayoutPanel204.SuspendLayout();
            this.flowLayoutPanel209.SuspendLayout();
            this.flowLayoutPanel214.SuspendLayout();
            this.flowLayoutPanel219.SuspendLayout();
            this.flowLayoutPanel224.SuspendLayout();
            this.flowLayoutPanel229.SuspendLayout();
            this.flowLayoutPanel234.SuspendLayout();
            this.flowLayoutPanel239.SuspendLayout();
            this.flowLayoutPanel244.SuspendLayout();
            this.flowLayoutPanel249.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Term)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel18.SuspendLayout();
            this.flowLayoutPanel23.SuspendLayout();
            this.flowLayoutPanel28.SuspendLayout();
            this.flowLayoutPanel33.SuspendLayout();
            this.flowLayoutPanel38.SuspendLayout();
            this.flowLayoutPanel43.SuspendLayout();
            this.flowLayoutPanel48.SuspendLayout();
            this.flowLayoutPanel53.SuspendLayout();
            this.flowLayoutPanel58.SuspendLayout();
            this.flowLayoutPanel63.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-13, 34);
            this.flowLayoutPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(870, 1);
            this.flowLayoutPanel9.TabIndex = 612;
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(656, 6);
            this.TxtIn_Rec_Date.Margin = new System.Windows.Forms.Padding(4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(171, 23);
            this.TxtIn_Rec_Date.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(571, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 610;
            this.label4.Text = "الـــــــــتاريـخ:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Txt_UsrName.Location = new System.Drawing.Point(125, 6);
            this.Txt_UsrName.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.ReadOnly = true;
            this.Txt_UsrName.Size = new System.Drawing.Size(245, 23);
            this.Txt_UsrName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(113, 14);
            this.label1.TabIndex = 608;
            this.label1.Text = "اسم المســـتخدم :";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 563);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(840, 22);
            this.statusStrip1.TabIndex = 708;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // GrdAcc_Close_price
            // 
            this.GrdAcc_Close_price.AllowUserToAddRows = false;
            this.GrdAcc_Close_price.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAcc_Close_price.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAcc_Close_price.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GrdAcc_Close_price.ColumnHeadersHeight = 30;
            this.GrdAcc_Close_price.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Column6,
            this.Column7,
            this.Column8});
            this.GrdAcc_Close_price.Location = new System.Drawing.Point(8, 263);
            this.GrdAcc_Close_price.Name = "GrdAcc_Close_price";
            this.GrdAcc_Close_price.RowHeadersWidth = 20;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAcc_Close_price.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.GrdAcc_Close_price.Size = new System.Drawing.Size(824, 109);
            this.GrdAcc_Close_price.StandardTab = true;
            this.GrdAcc_Close_price.TabIndex = 709;
            this.GrdAcc_Close_price.VirtualMode = true;
            this.GrdAcc_Close_price.SelectionChanged += new System.EventHandler(this.GrdAcc_Close_price_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Cur_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز العملة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 110;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Cur_AName";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم العملة";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 240;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_Close_Price";
            this.Column6.HeaderText = "سعر التعادل";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 160;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Str_Nrec_Date";
            this.Column7.HeaderText = "اعتبار من ";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 150;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "User_Name";
            this.Column8.HeaderText = "المستخدمين";
            this.Column8.Name = "Column8";
            this.Column8.Width = 140;
            // 
            // Grd_Close_Hst
            // 
            this.Grd_Close_Hst.AllowUserToAddRows = false;
            this.Grd_Close_Hst.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Close_Hst.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Close_Hst.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Grd_Close_Hst.ColumnHeadersHeight = 30;
            this.Grd_Close_Hst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.Column1,
            this.dataGridViewTextBoxColumn7});
            this.Grd_Close_Hst.Location = new System.Drawing.Point(8, 401);
            this.Grd_Close_Hst.Name = "Grd_Close_Hst";
            this.Grd_Close_Hst.RowHeadersWidth = 20;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Close_Hst.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.Grd_Close_Hst.Size = new System.Drawing.Size(824, 127);
            this.Grd_Close_Hst.StandardTab = true;
            this.Grd_Close_Hst.TabIndex = 735;
            this.Grd_Close_Hst.VirtualMode = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Cur_id";
            this.dataGridViewTextBoxColumn3.HeaderText = "رمز العملة";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 110;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Cur_AName";
            this.dataGridViewTextBoxColumn4.HeaderText = "اسم العملة";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 200;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Acc_Close_Price";
            this.dataGridViewTextBoxColumn5.HeaderText = "سعر التعادل";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 140;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Str_Nrec_Date";
            this.dataGridViewTextBoxColumn6.HeaderText = "اعتبار من ";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 110;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "End_Nrec_Date";
            this.Column1.HeaderText = "ولغايـــة";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 110;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "User_Name";
            this.dataGridViewTextBoxColumn7.HeaderText = "المستخدمين";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 130;
            // 
            // flowLayoutPanel189
            // 
            this.flowLayoutPanel189.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel190);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel191);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel192);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel193);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel194);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel204);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel219);
            this.flowLayoutPanel189.Controls.Add(this.flowLayoutPanel249);
            this.flowLayoutPanel189.Location = new System.Drawing.Point(260, 389);
            this.flowLayoutPanel189.Name = "flowLayoutPanel189";
            this.flowLayoutPanel189.Size = new System.Drawing.Size(678, 2);
            this.flowLayoutPanel189.TabIndex = 737;
            // 
            // flowLayoutPanel190
            // 
            this.flowLayoutPanel190.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel190.Location = new System.Drawing.Point(671, 3);
            this.flowLayoutPanel190.Name = "flowLayoutPanel190";
            this.flowLayoutPanel190.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel190.TabIndex = 634;
            // 
            // flowLayoutPanel191
            // 
            this.flowLayoutPanel191.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel191.Location = new System.Drawing.Point(663, 3);
            this.flowLayoutPanel191.Name = "flowLayoutPanel191";
            this.flowLayoutPanel191.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel191.TabIndex = 633;
            // 
            // flowLayoutPanel192
            // 
            this.flowLayoutPanel192.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel192.Location = new System.Drawing.Point(-90, 241);
            this.flowLayoutPanel192.Name = "flowLayoutPanel192";
            this.flowLayoutPanel192.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel192.TabIndex = 632;
            // 
            // flowLayoutPanel193
            // 
            this.flowLayoutPanel193.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel193.Location = new System.Drawing.Point(-90, 249);
            this.flowLayoutPanel193.Name = "flowLayoutPanel193";
            this.flowLayoutPanel193.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel193.TabIndex = 631;
            // 
            // flowLayoutPanel194
            // 
            this.flowLayoutPanel194.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel195);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel196);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel197);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel198);
            this.flowLayoutPanel194.Controls.Add(this.flowLayoutPanel199);
            this.flowLayoutPanel194.Location = new System.Drawing.Point(65, 257);
            this.flowLayoutPanel194.Name = "flowLayoutPanel194";
            this.flowLayoutPanel194.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel194.TabIndex = 676;
            // 
            // flowLayoutPanel195
            // 
            this.flowLayoutPanel195.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel195.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel195.Name = "flowLayoutPanel195";
            this.flowLayoutPanel195.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel195.TabIndex = 634;
            // 
            // flowLayoutPanel196
            // 
            this.flowLayoutPanel196.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel196.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel196.Name = "flowLayoutPanel196";
            this.flowLayoutPanel196.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel196.TabIndex = 633;
            // 
            // flowLayoutPanel197
            // 
            this.flowLayoutPanel197.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel197.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel197.Name = "flowLayoutPanel197";
            this.flowLayoutPanel197.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel197.TabIndex = 632;
            // 
            // flowLayoutPanel198
            // 
            this.flowLayoutPanel198.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel198.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel198.Name = "flowLayoutPanel198";
            this.flowLayoutPanel198.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel198.TabIndex = 631;
            // 
            // flowLayoutPanel199
            // 
            this.flowLayoutPanel199.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel200);
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel201);
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel202);
            this.flowLayoutPanel199.Controls.Add(this.flowLayoutPanel203);
            this.flowLayoutPanel199.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel199.Name = "flowLayoutPanel199";
            this.flowLayoutPanel199.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel199.TabIndex = 676;
            // 
            // flowLayoutPanel200
            // 
            this.flowLayoutPanel200.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel200.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel200.Name = "flowLayoutPanel200";
            this.flowLayoutPanel200.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel200.TabIndex = 634;
            // 
            // flowLayoutPanel201
            // 
            this.flowLayoutPanel201.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel201.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel201.Name = "flowLayoutPanel201";
            this.flowLayoutPanel201.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel201.TabIndex = 633;
            // 
            // flowLayoutPanel202
            // 
            this.flowLayoutPanel202.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel202.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel202.Name = "flowLayoutPanel202";
            this.flowLayoutPanel202.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel202.TabIndex = 632;
            // 
            // flowLayoutPanel203
            // 
            this.flowLayoutPanel203.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel203.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel203.Name = "flowLayoutPanel203";
            this.flowLayoutPanel203.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel203.TabIndex = 631;
            // 
            // flowLayoutPanel204
            // 
            this.flowLayoutPanel204.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel205);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel206);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel207);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel208);
            this.flowLayoutPanel204.Controls.Add(this.flowLayoutPanel209);
            this.flowLayoutPanel204.Location = new System.Drawing.Point(65, 264);
            this.flowLayoutPanel204.Name = "flowLayoutPanel204";
            this.flowLayoutPanel204.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel204.TabIndex = 677;
            // 
            // flowLayoutPanel205
            // 
            this.flowLayoutPanel205.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel205.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel205.Name = "flowLayoutPanel205";
            this.flowLayoutPanel205.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel205.TabIndex = 634;
            // 
            // flowLayoutPanel206
            // 
            this.flowLayoutPanel206.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel206.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel206.Name = "flowLayoutPanel206";
            this.flowLayoutPanel206.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel206.TabIndex = 633;
            // 
            // flowLayoutPanel207
            // 
            this.flowLayoutPanel207.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel207.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel207.Name = "flowLayoutPanel207";
            this.flowLayoutPanel207.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel207.TabIndex = 632;
            // 
            // flowLayoutPanel208
            // 
            this.flowLayoutPanel208.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel208.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel208.Name = "flowLayoutPanel208";
            this.flowLayoutPanel208.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel208.TabIndex = 631;
            // 
            // flowLayoutPanel209
            // 
            this.flowLayoutPanel209.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel210);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel211);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel212);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel213);
            this.flowLayoutPanel209.Controls.Add(this.flowLayoutPanel214);
            this.flowLayoutPanel209.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel209.Name = "flowLayoutPanel209";
            this.flowLayoutPanel209.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel209.TabIndex = 676;
            // 
            // flowLayoutPanel210
            // 
            this.flowLayoutPanel210.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel210.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel210.Name = "flowLayoutPanel210";
            this.flowLayoutPanel210.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel210.TabIndex = 634;
            // 
            // flowLayoutPanel211
            // 
            this.flowLayoutPanel211.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel211.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel211.Name = "flowLayoutPanel211";
            this.flowLayoutPanel211.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel211.TabIndex = 633;
            // 
            // flowLayoutPanel212
            // 
            this.flowLayoutPanel212.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel212.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel212.Name = "flowLayoutPanel212";
            this.flowLayoutPanel212.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel212.TabIndex = 632;
            // 
            // flowLayoutPanel213
            // 
            this.flowLayoutPanel213.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel213.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel213.Name = "flowLayoutPanel213";
            this.flowLayoutPanel213.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel213.TabIndex = 631;
            // 
            // flowLayoutPanel214
            // 
            this.flowLayoutPanel214.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel215);
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel216);
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel217);
            this.flowLayoutPanel214.Controls.Add(this.flowLayoutPanel218);
            this.flowLayoutPanel214.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel214.Name = "flowLayoutPanel214";
            this.flowLayoutPanel214.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel214.TabIndex = 676;
            // 
            // flowLayoutPanel215
            // 
            this.flowLayoutPanel215.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel215.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel215.Name = "flowLayoutPanel215";
            this.flowLayoutPanel215.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel215.TabIndex = 634;
            // 
            // flowLayoutPanel216
            // 
            this.flowLayoutPanel216.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel216.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel216.Name = "flowLayoutPanel216";
            this.flowLayoutPanel216.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel216.TabIndex = 633;
            // 
            // flowLayoutPanel217
            // 
            this.flowLayoutPanel217.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel217.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel217.Name = "flowLayoutPanel217";
            this.flowLayoutPanel217.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel217.TabIndex = 632;
            // 
            // flowLayoutPanel218
            // 
            this.flowLayoutPanel218.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel218.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel218.Name = "flowLayoutPanel218";
            this.flowLayoutPanel218.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel218.TabIndex = 631;
            // 
            // flowLayoutPanel219
            // 
            this.flowLayoutPanel219.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel220);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel221);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel222);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel223);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel224);
            this.flowLayoutPanel219.Controls.Add(this.flowLayoutPanel234);
            this.flowLayoutPanel219.Location = new System.Drawing.Point(65, 271);
            this.flowLayoutPanel219.Name = "flowLayoutPanel219";
            this.flowLayoutPanel219.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel219.TabIndex = 678;
            // 
            // flowLayoutPanel220
            // 
            this.flowLayoutPanel220.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel220.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel220.Name = "flowLayoutPanel220";
            this.flowLayoutPanel220.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel220.TabIndex = 634;
            // 
            // flowLayoutPanel221
            // 
            this.flowLayoutPanel221.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel221.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel221.Name = "flowLayoutPanel221";
            this.flowLayoutPanel221.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel221.TabIndex = 633;
            // 
            // flowLayoutPanel222
            // 
            this.flowLayoutPanel222.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel222.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel222.Name = "flowLayoutPanel222";
            this.flowLayoutPanel222.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel222.TabIndex = 632;
            // 
            // flowLayoutPanel223
            // 
            this.flowLayoutPanel223.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel223.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel223.Name = "flowLayoutPanel223";
            this.flowLayoutPanel223.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel223.TabIndex = 631;
            // 
            // flowLayoutPanel224
            // 
            this.flowLayoutPanel224.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel225);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel226);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel227);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel228);
            this.flowLayoutPanel224.Controls.Add(this.flowLayoutPanel229);
            this.flowLayoutPanel224.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel224.Name = "flowLayoutPanel224";
            this.flowLayoutPanel224.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel224.TabIndex = 676;
            // 
            // flowLayoutPanel225
            // 
            this.flowLayoutPanel225.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel225.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel225.Name = "flowLayoutPanel225";
            this.flowLayoutPanel225.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel225.TabIndex = 634;
            // 
            // flowLayoutPanel226
            // 
            this.flowLayoutPanel226.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel226.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel226.Name = "flowLayoutPanel226";
            this.flowLayoutPanel226.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel226.TabIndex = 633;
            // 
            // flowLayoutPanel227
            // 
            this.flowLayoutPanel227.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel227.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel227.Name = "flowLayoutPanel227";
            this.flowLayoutPanel227.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel227.TabIndex = 632;
            // 
            // flowLayoutPanel228
            // 
            this.flowLayoutPanel228.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel228.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel228.Name = "flowLayoutPanel228";
            this.flowLayoutPanel228.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel228.TabIndex = 631;
            // 
            // flowLayoutPanel229
            // 
            this.flowLayoutPanel229.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel230);
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel231);
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel232);
            this.flowLayoutPanel229.Controls.Add(this.flowLayoutPanel233);
            this.flowLayoutPanel229.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel229.Name = "flowLayoutPanel229";
            this.flowLayoutPanel229.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel229.TabIndex = 676;
            // 
            // flowLayoutPanel230
            // 
            this.flowLayoutPanel230.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel230.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel230.Name = "flowLayoutPanel230";
            this.flowLayoutPanel230.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel230.TabIndex = 634;
            // 
            // flowLayoutPanel231
            // 
            this.flowLayoutPanel231.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel231.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel231.Name = "flowLayoutPanel231";
            this.flowLayoutPanel231.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel231.TabIndex = 633;
            // 
            // flowLayoutPanel232
            // 
            this.flowLayoutPanel232.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel232.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel232.Name = "flowLayoutPanel232";
            this.flowLayoutPanel232.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel232.TabIndex = 632;
            // 
            // flowLayoutPanel233
            // 
            this.flowLayoutPanel233.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel233.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel233.Name = "flowLayoutPanel233";
            this.flowLayoutPanel233.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel233.TabIndex = 631;
            // 
            // flowLayoutPanel234
            // 
            this.flowLayoutPanel234.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel235);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel236);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel237);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel238);
            this.flowLayoutPanel234.Controls.Add(this.flowLayoutPanel239);
            this.flowLayoutPanel234.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel234.Name = "flowLayoutPanel234";
            this.flowLayoutPanel234.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel234.TabIndex = 677;
            // 
            // flowLayoutPanel235
            // 
            this.flowLayoutPanel235.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel235.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel235.Name = "flowLayoutPanel235";
            this.flowLayoutPanel235.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel235.TabIndex = 634;
            // 
            // flowLayoutPanel236
            // 
            this.flowLayoutPanel236.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel236.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel236.Name = "flowLayoutPanel236";
            this.flowLayoutPanel236.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel236.TabIndex = 633;
            // 
            // flowLayoutPanel237
            // 
            this.flowLayoutPanel237.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel237.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel237.Name = "flowLayoutPanel237";
            this.flowLayoutPanel237.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel237.TabIndex = 632;
            // 
            // flowLayoutPanel238
            // 
            this.flowLayoutPanel238.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel238.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel238.Name = "flowLayoutPanel238";
            this.flowLayoutPanel238.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel238.TabIndex = 631;
            // 
            // flowLayoutPanel239
            // 
            this.flowLayoutPanel239.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel240);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel241);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel242);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel243);
            this.flowLayoutPanel239.Controls.Add(this.flowLayoutPanel244);
            this.flowLayoutPanel239.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel239.Name = "flowLayoutPanel239";
            this.flowLayoutPanel239.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel239.TabIndex = 676;
            // 
            // flowLayoutPanel240
            // 
            this.flowLayoutPanel240.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel240.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel240.Name = "flowLayoutPanel240";
            this.flowLayoutPanel240.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel240.TabIndex = 634;
            // 
            // flowLayoutPanel241
            // 
            this.flowLayoutPanel241.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel241.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel241.Name = "flowLayoutPanel241";
            this.flowLayoutPanel241.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel241.TabIndex = 633;
            // 
            // flowLayoutPanel242
            // 
            this.flowLayoutPanel242.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel242.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel242.Name = "flowLayoutPanel242";
            this.flowLayoutPanel242.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel242.TabIndex = 632;
            // 
            // flowLayoutPanel243
            // 
            this.flowLayoutPanel243.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel243.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel243.Name = "flowLayoutPanel243";
            this.flowLayoutPanel243.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel243.TabIndex = 631;
            // 
            // flowLayoutPanel244
            // 
            this.flowLayoutPanel244.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel245);
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel246);
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel247);
            this.flowLayoutPanel244.Controls.Add(this.flowLayoutPanel248);
            this.flowLayoutPanel244.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel244.Name = "flowLayoutPanel244";
            this.flowLayoutPanel244.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel244.TabIndex = 676;
            // 
            // flowLayoutPanel245
            // 
            this.flowLayoutPanel245.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel245.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel245.Name = "flowLayoutPanel245";
            this.flowLayoutPanel245.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel245.TabIndex = 634;
            // 
            // flowLayoutPanel246
            // 
            this.flowLayoutPanel246.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel246.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel246.Name = "flowLayoutPanel246";
            this.flowLayoutPanel246.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel246.TabIndex = 633;
            // 
            // flowLayoutPanel247
            // 
            this.flowLayoutPanel247.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel247.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel247.Name = "flowLayoutPanel247";
            this.flowLayoutPanel247.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel247.TabIndex = 632;
            // 
            // flowLayoutPanel248
            // 
            this.flowLayoutPanel248.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel248.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel248.Name = "flowLayoutPanel248";
            this.flowLayoutPanel248.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel248.TabIndex = 631;
            // 
            // flowLayoutPanel249
            // 
            this.flowLayoutPanel249.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel249.Controls.Add(this.flowLayoutPanel250);
            this.flowLayoutPanel249.Location = new System.Drawing.Point(57, 271);
            this.flowLayoutPanel249.Name = "flowLayoutPanel249";
            this.flowLayoutPanel249.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel249.TabIndex = 679;
            // 
            // flowLayoutPanel250
            // 
            this.flowLayoutPanel250.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel250.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel250.Name = "flowLayoutPanel250";
            this.flowLayoutPanel250.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel250.TabIndex = 632;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.DarkRed;
            this.label5.Location = new System.Drawing.Point(1, 379);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(259, 14);
            this.label5.TabIndex = 736;
            this.label5.Text = "المعلومات التاريخيةلاسعار اغلاق العملات ....";
            // 
            // Btn_Ok
            // 
            this.Btn_Ok.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ok.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ok.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Btn_Ok.Location = new System.Drawing.Point(331, 535);
            this.Btn_Ok.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Ok.Name = "Btn_Ok";
            this.Btn_Ok.Size = new System.Drawing.Size(90, 26);
            this.Btn_Ok.TabIndex = 740;
            this.Btn_Ok.Text = "موافق";
            this.Btn_Ok.UseVisualStyleBackColor = true;
            this.Btn_Ok.Click += new System.EventHandler(this.Btn_Ok_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ExtBtn.Location = new System.Drawing.Point(420, 535);
            this.ExtBtn.Margin = new System.Windows.Forms.Padding(4);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(90, 26);
            this.ExtBtn.TabIndex = 741;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(-5, 532);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(862, 1);
            this.flowLayoutPanel4.TabIndex = 742;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(11, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 14);
            this.label3.TabIndex = 743;
            this.label3.Text = "اغلاق السجلات المحاسبية حسب : ";
            // 
            // CboClose_Date
            // 
            this.CboClose_Date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboClose_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboClose_Date.FormattingEnabled = true;
            this.CboClose_Date.Location = new System.Drawing.Point(429, 44);
            this.CboClose_Date.Name = "CboClose_Date";
            this.CboClose_Date.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CboClose_Date.Size = new System.Drawing.Size(146, 24);
            this.CboClose_Date.TabIndex = 744;
            this.CboClose_Date.SelectedIndexChanged += new System.EventHandler(this.CboClose_Date_SelectedIndexChanged);
            // 
            // Grd_Term
            // 
            this.Grd_Term.AllowUserToAddRows = false;
            this.Grd_Term.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Term.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grd_Term.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Grd_Term.ColumnHeadersHeight = 30;
            this.Grd_Term.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.dataGridViewTextBoxColumn8,
            this.Column9,
            this.Column10,
            this.Column3});
            this.Grd_Term.Location = new System.Drawing.Point(8, 74);
            this.Grd_Term.Name = "Grd_Term";
            this.Grd_Term.RowHeadersWidth = 20;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd_Term.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grd_Term.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Grd_Term.Size = new System.Drawing.Size(824, 168);
            this.Grd_Term.StandardTab = true;
            this.Grd_Term.TabIndex = 745;
            this.Grd_Term.VirtualMode = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Chk";
            this.Column2.FalseValue = "0";
            this.Column2.HeaderText = "تأشير ";
            this.Column2.Name = "Column2";
            this.Column2.TrueValue = "1";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "T_id";
            this.dataGridViewTextBoxColumn8.HeaderText = "رمز الفرع";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 110;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "ACust_name";
            this.Column9.HeaderText = "أســــم الفـــرع";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 240;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "acc_Close_Date";
            this.Column10.HeaderText = "  ت اخـــر اغـــلاق محاسبي";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 180;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Real_Close_Date";
            this.Column3.HeaderText = "  ت اخـــر اغـــلاق فـعلــــي";
            this.Column3.Name = "Column3";
            this.Column3.Width = 180;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel18);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel33);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel63);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(156, 255);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(701, 2);
            this.flowLayoutPanel1.TabIndex = 747;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(694, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel2.TabIndex = 634;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(686, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel3.TabIndex = 633;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(-67, 241);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel5.TabIndex = 632;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(-67, 249);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel6.TabIndex = 631;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel10);
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel11);
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel12);
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel13);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(88, 257);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel7.TabIndex = 676;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel8.TabIndex = 634;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel10.TabIndex = 633;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel11.TabIndex = 632;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel12.TabIndex = 631;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel14);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel15);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel16);
            this.flowLayoutPanel13.Controls.Add(this.flowLayoutPanel17);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel13.TabIndex = 676;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel14.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel14.TabIndex = 634;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel15.TabIndex = 633;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel16.TabIndex = 632;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel17.TabIndex = 631;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel18.Controls.Add(this.flowLayoutPanel19);
            this.flowLayoutPanel18.Controls.Add(this.flowLayoutPanel20);
            this.flowLayoutPanel18.Controls.Add(this.flowLayoutPanel21);
            this.flowLayoutPanel18.Controls.Add(this.flowLayoutPanel22);
            this.flowLayoutPanel18.Controls.Add(this.flowLayoutPanel23);
            this.flowLayoutPanel18.Location = new System.Drawing.Point(88, 264);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel18.TabIndex = 677;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel19.TabIndex = 634;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel20.TabIndex = 633;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel21.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel21.TabIndex = 632;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel22.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel22.TabIndex = 631;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel25);
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel23.Controls.Add(this.flowLayoutPanel28);
            this.flowLayoutPanel23.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel23.TabIndex = 676;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel24.TabIndex = 634;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel25.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel25.TabIndex = 633;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel26.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel26.TabIndex = 632;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel27.TabIndex = 631;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel28.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel28.Controls.Add(this.flowLayoutPanel30);
            this.flowLayoutPanel28.Controls.Add(this.flowLayoutPanel31);
            this.flowLayoutPanel28.Controls.Add(this.flowLayoutPanel32);
            this.flowLayoutPanel28.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel28.TabIndex = 676;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel29.TabIndex = 634;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel30.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel30.TabIndex = 633;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel31.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel31.TabIndex = 632;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel32.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel32.TabIndex = 631;
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel34);
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel35);
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel36);
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel37);
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel38);
            this.flowLayoutPanel33.Controls.Add(this.flowLayoutPanel48);
            this.flowLayoutPanel33.Location = new System.Drawing.Point(88, 271);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel33.TabIndex = 678;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel34.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel34.TabIndex = 634;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel35.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel35.TabIndex = 633;
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel36.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel36.TabIndex = 632;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel37.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel37.TabIndex = 631;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel38.Controls.Add(this.flowLayoutPanel39);
            this.flowLayoutPanel38.Controls.Add(this.flowLayoutPanel40);
            this.flowLayoutPanel38.Controls.Add(this.flowLayoutPanel41);
            this.flowLayoutPanel38.Controls.Add(this.flowLayoutPanel42);
            this.flowLayoutPanel38.Controls.Add(this.flowLayoutPanel43);
            this.flowLayoutPanel38.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel38.TabIndex = 676;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel39.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel39.TabIndex = 634;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel40.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel40.TabIndex = 633;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel41.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel41.TabIndex = 632;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel42.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel42.TabIndex = 631;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel44);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel45);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel46);
            this.flowLayoutPanel43.Controls.Add(this.flowLayoutPanel47);
            this.flowLayoutPanel43.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel43.TabIndex = 676;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel44.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel44.TabIndex = 634;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel45.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel45.TabIndex = 633;
            // 
            // flowLayoutPanel46
            // 
            this.flowLayoutPanel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel46.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel46.Name = "flowLayoutPanel46";
            this.flowLayoutPanel46.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel46.TabIndex = 632;
            // 
            // flowLayoutPanel47
            // 
            this.flowLayoutPanel47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel47.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel47.Name = "flowLayoutPanel47";
            this.flowLayoutPanel47.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel47.TabIndex = 631;
            // 
            // flowLayoutPanel48
            // 
            this.flowLayoutPanel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel48.Controls.Add(this.flowLayoutPanel49);
            this.flowLayoutPanel48.Controls.Add(this.flowLayoutPanel50);
            this.flowLayoutPanel48.Controls.Add(this.flowLayoutPanel51);
            this.flowLayoutPanel48.Controls.Add(this.flowLayoutPanel52);
            this.flowLayoutPanel48.Controls.Add(this.flowLayoutPanel53);
            this.flowLayoutPanel48.Location = new System.Drawing.Point(-5, 264);
            this.flowLayoutPanel48.Name = "flowLayoutPanel48";
            this.flowLayoutPanel48.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel48.TabIndex = 677;
            // 
            // flowLayoutPanel49
            // 
            this.flowLayoutPanel49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel49.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel49.Name = "flowLayoutPanel49";
            this.flowLayoutPanel49.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel49.TabIndex = 634;
            // 
            // flowLayoutPanel50
            // 
            this.flowLayoutPanel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel50.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel50.Name = "flowLayoutPanel50";
            this.flowLayoutPanel50.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel50.TabIndex = 633;
            // 
            // flowLayoutPanel51
            // 
            this.flowLayoutPanel51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel51.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel51.Name = "flowLayoutPanel51";
            this.flowLayoutPanel51.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel51.TabIndex = 632;
            // 
            // flowLayoutPanel52
            // 
            this.flowLayoutPanel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel52.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel52.Name = "flowLayoutPanel52";
            this.flowLayoutPanel52.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel52.TabIndex = 631;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel54);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel55);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel56);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel57);
            this.flowLayoutPanel53.Controls.Add(this.flowLayoutPanel58);
            this.flowLayoutPanel53.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel53.TabIndex = 676;
            // 
            // flowLayoutPanel54
            // 
            this.flowLayoutPanel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel54.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel54.Name = "flowLayoutPanel54";
            this.flowLayoutPanel54.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel54.TabIndex = 634;
            // 
            // flowLayoutPanel55
            // 
            this.flowLayoutPanel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel55.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel55.Name = "flowLayoutPanel55";
            this.flowLayoutPanel55.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel55.TabIndex = 633;
            // 
            // flowLayoutPanel56
            // 
            this.flowLayoutPanel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel56.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel56.Name = "flowLayoutPanel56";
            this.flowLayoutPanel56.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel56.TabIndex = 632;
            // 
            // flowLayoutPanel57
            // 
            this.flowLayoutPanel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel57.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel57.Name = "flowLayoutPanel57";
            this.flowLayoutPanel57.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel57.TabIndex = 631;
            // 
            // flowLayoutPanel58
            // 
            this.flowLayoutPanel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel58.Controls.Add(this.flowLayoutPanel59);
            this.flowLayoutPanel58.Controls.Add(this.flowLayoutPanel60);
            this.flowLayoutPanel58.Controls.Add(this.flowLayoutPanel61);
            this.flowLayoutPanel58.Controls.Add(this.flowLayoutPanel62);
            this.flowLayoutPanel58.Location = new System.Drawing.Point(-5, 257);
            this.flowLayoutPanel58.Name = "flowLayoutPanel58";
            this.flowLayoutPanel58.Size = new System.Drawing.Size(608, 1);
            this.flowLayoutPanel58.TabIndex = 676;
            // 
            // flowLayoutPanel59
            // 
            this.flowLayoutPanel59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel59.Location = new System.Drawing.Point(601, 3);
            this.flowLayoutPanel59.Name = "flowLayoutPanel59";
            this.flowLayoutPanel59.Size = new System.Drawing.Size(2, 231);
            this.flowLayoutPanel59.TabIndex = 634;
            // 
            // flowLayoutPanel60
            // 
            this.flowLayoutPanel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel60.Location = new System.Drawing.Point(593, 3);
            this.flowLayoutPanel60.Name = "flowLayoutPanel60";
            this.flowLayoutPanel60.Size = new System.Drawing.Size(2, 232);
            this.flowLayoutPanel60.TabIndex = 633;
            // 
            // flowLayoutPanel61
            // 
            this.flowLayoutPanel61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel61.Location = new System.Drawing.Point(-160, 241);
            this.flowLayoutPanel61.Name = "flowLayoutPanel61";
            this.flowLayoutPanel61.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel61.TabIndex = 632;
            // 
            // flowLayoutPanel62
            // 
            this.flowLayoutPanel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel62.Location = new System.Drawing.Point(-160, 249);
            this.flowLayoutPanel62.Name = "flowLayoutPanel62";
            this.flowLayoutPanel62.Size = new System.Drawing.Size(763, 2);
            this.flowLayoutPanel62.TabIndex = 631;
            // 
            // flowLayoutPanel63
            // 
            this.flowLayoutPanel63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel63.Controls.Add(this.flowLayoutPanel64);
            this.flowLayoutPanel63.Location = new System.Drawing.Point(80, 271);
            this.flowLayoutPanel63.Name = "flowLayoutPanel63";
            this.flowLayoutPanel63.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel63.TabIndex = 679;
            // 
            // flowLayoutPanel64
            // 
            this.flowLayoutPanel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel64.Location = new System.Drawing.Point(-5, 3);
            this.flowLayoutPanel64.Name = "flowLayoutPanel64";
            this.flowLayoutPanel64.Size = new System.Drawing.Size(2, 227);
            this.flowLayoutPanel64.TabIndex = 632;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.DarkRed;
            this.label6.Location = new System.Drawing.Point(-1, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 14);
            this.label6.TabIndex = 746;
            this.label6.Text = "أســـعار اغلاق العملات......";
            // 
            // Cbo_CloseingState
            // 
            this.Cbo_CloseingState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_CloseingState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_CloseingState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_CloseingState.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_CloseingState.FormattingEnabled = true;
            this.Cbo_CloseingState.Items.AddRange(new object[] {
            "أيام العمل الفعلية",
            "أيـــام السنــــــة"});
            this.Cbo_CloseingState.Location = new System.Drawing.Point(220, 44);
            this.Cbo_CloseingState.Name = "Cbo_CloseingState";
            this.Cbo_CloseingState.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Cbo_CloseingState.Size = new System.Drawing.Size(146, 24);
            this.Cbo_CloseingState.TabIndex = 749;
            this.Cbo_CloseingState.SelectedIndexChanged += new System.EventHandler(this.Cbo_CloseingState_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(372, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 14);
            this.label2.TabIndex = 750;
            this.label2.Text = "لغـــايــة:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.button1.ForeColor = System.Drawing.Color.Navy;
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(702, 535);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 26);
            this.button1.TabIndex = 751;
            this.button1.Text = "معلومات تأريخية للاغلاق";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TxtClsoe_Date
            // 
            this.TxtClsoe_Date.BackColor = System.Drawing.Color.White;
            this.TxtClsoe_Date.DateSeperator = '/';
            this.TxtClsoe_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClsoe_Date.Location = new System.Drawing.Point(578, 45);
            this.TxtClsoe_Date.Mask = "0000/00/00";
            this.TxtClsoe_Date.Name = "TxtClsoe_Date";
            this.TxtClsoe_Date.PromptChar = ' ';
            this.TxtClsoe_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtClsoe_Date.Size = new System.Drawing.Size(107, 22);
            this.TxtClsoe_Date.TabIndex = 748;
            this.TxtClsoe_Date.Text = "00000000";
            this.TxtClsoe_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // Accountant_Close_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 585);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Cbo_CloseingState);
            this.Controls.Add(this.TxtClsoe_Date);
            this.Controls.Add(this.Grd_Term);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CboClose_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.Btn_Ok);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.flowLayoutPanel189);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Grd_Close_Hst);
            this.Controls.Add(this.GrdAcc_Close_price);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Txt_UsrName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Accountant_Close_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "453";
            this.Text = "Accountant_Close_Main";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Accountant_Close_Main_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Acc_Close_Price_Main_FormClosed);
            this.Load += new System.EventHandler(this.Acc_Close_Price_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Acc_Close_Price_Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.GrdAcc_Close_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Close_Hst)).EndInit();
            this.flowLayoutPanel189.ResumeLayout(false);
            this.flowLayoutPanel194.ResumeLayout(false);
            this.flowLayoutPanel199.ResumeLayout(false);
            this.flowLayoutPanel204.ResumeLayout(false);
            this.flowLayoutPanel209.ResumeLayout(false);
            this.flowLayoutPanel214.ResumeLayout(false);
            this.flowLayoutPanel219.ResumeLayout(false);
            this.flowLayoutPanel224.ResumeLayout(false);
            this.flowLayoutPanel229.ResumeLayout(false);
            this.flowLayoutPanel234.ResumeLayout(false);
            this.flowLayoutPanel239.ResumeLayout(false);
            this.flowLayoutPanel244.ResumeLayout(false);
            this.flowLayoutPanel249.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd_Term)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel18.ResumeLayout(false);
            this.flowLayoutPanel23.ResumeLayout(false);
            this.flowLayoutPanel28.ResumeLayout(false);
            this.flowLayoutPanel33.ResumeLayout(false);
            this.flowLayoutPanel38.ResumeLayout(false);
            this.flowLayoutPanel43.ResumeLayout(false);
            this.flowLayoutPanel48.ResumeLayout(false);
            this.flowLayoutPanel53.ResumeLayout(false);
            this.flowLayoutPanel58.ResumeLayout(false);
            this.flowLayoutPanel63.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView GrdAcc_Close_price;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridView Grd_Close_Hst;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel189;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel190;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel191;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel192;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel193;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel194;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel195;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel196;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel197;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel198;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel199;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel200;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel201;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel202;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel203;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel204;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel205;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel206;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel207;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel208;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel209;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel210;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel211;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel212;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel213;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel214;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel215;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel216;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel217;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel218;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel219;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel220;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel221;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel222;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel223;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel224;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel225;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel226;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel227;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel228;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel229;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel230;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel231;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel232;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel233;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel234;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel235;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel236;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel237;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel238;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel239;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel240;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel241;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel242;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel243;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel244;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel245;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel246;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel247;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel248;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel249;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel250;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Btn_Ok;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.ComboBox CboClose_Date;
        private System.Windows.Forms.DataGridView Grd_Term;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel46;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel47;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel51;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel57;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel58;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel59;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel61;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel62;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel63;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel64;
        private System.Windows.Forms.Label label6;
        private MyDateTextBox TxtClsoe_Date;
        private System.Windows.Forms.ComboBox Cbo_CloseingState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button button1;
    }
}