﻿namespace Integration_Accounting_Sys
{
    partial class Acc_Close_Price_Add_Upd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.LblRec = new System.Windows.Forms.ToolStripStatusLabel();
            this.GrdAcc_Close = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddBtn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.BtnDel = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Cbo_Cur_ID = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Btn_Ok = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtIn_Rec_Date = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_UsrName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtStart_Date = new Integration_Accounting_Sys.MyDateTextBox();
            this.TxtClose_Price = new System.Windows.Forms.Sample.DecimalTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAcc_Close)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblRec
            // 
            this.LblRec.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.LblRec.ForeColor = System.Drawing.Color.Black;
            this.LblRec.Name = "LblRec";
            this.LblRec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblRec.Size = new System.Drawing.Size(60, 17);
            this.LblRec.Text = "Records";
            // 
            // GrdAcc_Close
            // 
            this.GrdAcc_Close.AllowUserToAddRows = false;
            this.GrdAcc_Close.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAcc_Close.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAcc_Close.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GrdAcc_Close.ColumnHeadersHeight = 40;
            this.GrdAcc_Close.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Column6,
            this.Column7,
            this.Column8});
            this.GrdAcc_Close.Location = new System.Drawing.Point(13, 103);
            this.GrdAcc_Close.Name = "GrdAcc_Close";
            this.GrdAcc_Close.RowHeadersWidth = 20;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrdAcc_Close.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.GrdAcc_Close.Size = new System.Drawing.Size(824, 323);
            this.GrdAcc_Close.StandardTab = true;
            this.GrdAcc_Close.TabIndex = 734;
            this.GrdAcc_Close.VirtualMode = true;
            this.GrdAcc_Close.SelectionChanged += new System.EventHandler(this.GrdAcc_Close_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Chk";
            this.Column1.HeaderText = "تاشير";
            this.Column1.Name = "Column1";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Cur_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رمز العملة";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Cur_AName";
            this.dataGridViewTextBoxColumn2.HeaderText = "اسم العملة";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 240;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Acc_Close_Price";
            dataGridViewCellStyle3.Format = "N6";
            dataGridViewCellStyle3.NullValue = null;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column6.HeaderText = "سعر التعادل";
            this.Column6.Name = "Column6";
            this.Column6.Width = 200;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Str_Nrec_Date";
            this.Column7.HeaderText = "اعتبار من ";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 210;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "User_Name";
            this.Column8.HeaderText = "المستخدمين";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 260;
            // 
            // AddBtn
            // 
            this.AddBtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.Navy;
            this.AddBtn.Location = new System.Drawing.Point(764, 65);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(81, 23);
            this.AddBtn.TabIndex = 733;
            this.AddBtn.Text = "استيراد";
            this.AddBtn.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblRec});
            this.statusStrip1.Location = new System.Drawing.Point(0, 477);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(13, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(847, 22);
            this.statusStrip1.TabIndex = 731;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // BtnDel
            // 
            this.BtnDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDel.ForeColor = System.Drawing.Color.Navy;
            this.BtnDel.Location = new System.Drawing.Point(675, 65);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(90, 23);
            this.BtnDel.TabIndex = 717;
            this.BtnDel.Text = "حــذف";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(258, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 14);
            this.label5.TabIndex = 730;
            this.label5.Text = "سعر الاغلاق:";
            // 
            // Cbo_Cur_ID
            // 
            this.Cbo_Cur_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Cbo_Cur_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbo_Cur_ID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbo_Cur_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbo_Cur_ID.FormattingEnabled = true;
            this.Cbo_Cur_ID.Location = new System.Drawing.Point(346, 41);
            this.Cbo_Cur_ID.Margin = new System.Windows.Forms.Padding(4);
            this.Cbo_Cur_ID.Name = "Cbo_Cur_ID";
            this.Cbo_Cur_ID.Size = new System.Drawing.Size(232, 24);
            this.Cbo_Cur_ID.TabIndex = 714;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(8, 95);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel2.TabIndex = 726;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(8, 433);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(834, 1);
            this.flowLayoutPanel4.TabIndex = 729;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(844, 95);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1, 339);
            this.flowLayoutPanel1.TabIndex = 728;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(7, 95);
            this.flowLayoutPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(1, 338);
            this.flowLayoutPanel7.TabIndex = 727;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(-15, 36);
            this.flowLayoutPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(870, 1);
            this.flowLayoutPanel9.TabIndex = 725;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Add.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Add.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Btn_Add.Location = new System.Drawing.Point(586, 65);
            this.Btn_Add.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(90, 23);
            this.Btn_Add.TabIndex = 716;
            this.Btn_Add.Text = "اضافة";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Btn_Ok
            // 
            this.Btn_Ok.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.Btn_Ok.ForeColor = System.Drawing.Color.Navy;
            this.Btn_Ok.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Btn_Ok.Location = new System.Drawing.Point(333, 445);
            this.Btn_Ok.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Ok.Name = "Btn_Ok";
            this.Btn_Ok.Size = new System.Drawing.Size(90, 26);
            this.Btn_Ok.TabIndex = 718;
            this.Btn_Ok.Text = "موافق";
            this.Btn_Ok.UseVisualStyleBackColor = true;
            this.Btn_Ok.Click += new System.EventHandler(this.Btn_Ok_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.ExtBtn.ForeColor = System.Drawing.Color.Navy;
            this.ExtBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ExtBtn.Location = new System.Drawing.Point(423, 445);
            this.ExtBtn.Margin = new System.Windows.Forms.Padding(4);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(90, 26);
            this.ExtBtn.TabIndex = 720;
            this.ExtBtn.Text = "انـهـاء";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(15, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(88, 14);
            this.label2.TabIndex = 724;
            this.label2.Text = "أعتبــــاراً مــــن";
            // 
            // TxtIn_Rec_Date
            // 
            this.TxtIn_Rec_Date.BackColor = System.Drawing.Color.White;
            this.TxtIn_Rec_Date.Enabled = false;
            this.TxtIn_Rec_Date.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.TxtIn_Rec_Date.Location = new System.Drawing.Point(674, 4);
            this.TxtIn_Rec_Date.Margin = new System.Windows.Forms.Padding(4);
            this.TxtIn_Rec_Date.Mask = "0000/00/00";
            this.TxtIn_Rec_Date.Name = "TxtIn_Rec_Date";
            this.TxtIn_Rec_Date.PromptChar = ' ';
            this.TxtIn_Rec_Date.Size = new System.Drawing.Size(171, 23);
            this.TxtIn_Rec_Date.TabIndex = 713;
            this.TxtIn_Rec_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(589, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 723;
            this.label4.Text = "الـــــــــتاريـخ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(243, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(95, 14);
            this.label3.TabIndex = 722;
            this.label3.Text = "العمــــــــــــــــلة:";
            // 
            // Txt_UsrName
            // 
            this.Txt_UsrName.BackColor = System.Drawing.Color.White;
            this.Txt_UsrName.Enabled = false;
            this.Txt_UsrName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.Txt_UsrName.Location = new System.Drawing.Point(123, 4);
            this.Txt_UsrName.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_UsrName.Name = "Txt_UsrName";
            this.Txt_UsrName.ReadOnly = true;
            this.Txt_UsrName.Size = new System.Drawing.Size(245, 23);
            this.Txt_UsrName.TabIndex = 712;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(1, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(113, 14);
            this.label1.TabIndex = 721;
            this.label1.Tag = "";
            this.label1.Text = "اسم المســـتخدم :";
            // 
            // TxtStart_Date
            // 
            this.TxtStart_Date.BackColor = System.Drawing.Color.White;
            this.TxtStart_Date.DateSeperator = '/';
            this.TxtStart_Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStart_Date.Location = new System.Drawing.Point(111, 56);
            this.TxtStart_Date.Mask = "0000/00/00";
            this.TxtStart_Date.Name = "TxtStart_Date";
            this.TxtStart_Date.PromptChar = ' ';
            this.TxtStart_Date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtStart_Date.Size = new System.Drawing.Size(126, 22);
            this.TxtStart_Date.TabIndex = 732;
            this.TxtStart_Date.Text = "00000000";
            this.TxtStart_Date.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // TxtClose_Price
            // 
            this.TxtClose_Price.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TxtClose_Price.Location = new System.Drawing.Point(346, 66);
            this.TxtClose_Price.Name = "TxtClose_Price";
            this.TxtClose_Price.NumberDecimalDigits = 7;
            this.TxtClose_Price.NumberDecimalSeparator = ".";
            this.TxtClose_Price.NumberGroupSeparator = ",";
            this.TxtClose_Price.Size = new System.Drawing.Size(232, 22);
            this.TxtClose_Price.TabIndex = 715;
            this.TxtClose_Price.Text = "0.0000000";
            // 
            // Acc_Close_Price_Add_Upd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 499);
            this.Controls.Add(this.GrdAcc_Close);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.TxtStart_Date);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.TxtClose_Price);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cbo_Cur_ID);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel7);
            this.Controls.Add(this.flowLayoutPanel9);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Btn_Ok);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtIn_Rec_Date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Txt_UsrName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Acc_Close_Price_Add_Upd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "452";
            this.Text = "Acc_Close_Price_Add_Upd";
            this.Load += new System.EventHandler(this.Acc_Close_Price_Add_Upd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrdAcc_Close)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripStatusLabel LblRec;
        private System.Windows.Forms.DataGridView GrdAcc_Close;
        private System.Windows.Forms.Button AddBtn;
        private MyDateTextBox TxtStart_Date;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Sample.DecimalTextBox TxtClose_Price;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox Cbo_Cur_ID;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Button Btn_Ok;
        private System.Windows.Forms.Button ExtBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox TxtIn_Rec_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_UsrName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;

    }
}