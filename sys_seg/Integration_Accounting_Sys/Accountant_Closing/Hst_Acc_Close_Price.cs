﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
     
    public partial class Hst_Acc_Close_Price : Form
    {
        int Cur_Id = 0;
        BindingSource _BS_HSt_Grd = new BindingSource();
        public Hst_Acc_Close_Price( int Hst_Cur)
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
           connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            GrdAcc_Close_price.AutoGenerateColumns = false;
            Cur_Id = Hst_Cur; 
        }

        private void Acc_Close_Price_Main_Load(object sender, EventArgs e)
        {
            string Sql_Text = " SELECT  A.Cur_id, Acc_Close_Price,  convert(varchar(12),Str_Nrec_Date,111) as Str_Nrec_Date,convert(varchar(12),End_Nrec_Date,111) as End_Nrec_Date, "
            + " B.Cur_ANAME , B.Cur_ENAME , C.User_Name "
            + " FROM  Acc_Close_Price A , Cur_TBL B , Users C "
            + " where A.Cur_id = B.Cur_ID "
            + " And A.user_id = C.user_id  "
            + " And End_Nrec_Date is not null"
            + " And A.cur_Id = " + Cur_Id;
            _BS_HSt_Grd.DataSource = connection.SqlExec(Sql_Text, "Hst_ACC_Close_Price_Tbl");
            GrdAcc_Close_price.DataSource = _BS_HSt_Grd;
            if (connection.SQLDS.Tables["Hst_ACC_Close_Price_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عملات تاريخية للعملة" : "No History for this currencies", MyGeneral_Lib.LblCap);
                this.Close();
            
            }
        }
        //---------------------------------------------------------
        private void Acc_Close_Price_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Tbl = { "Hst_ACC_Close_Price_Tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }
        //---------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable Details_Acc_Close = new DataTable();
            Details_Acc_Close = connection.SQLDS.Tables["Hst_ACC_Close_Price_Tbl"].DefaultView.ToTable(false, "Cur_id", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Acc_Close_Price", "Str_Nrec_Date","End_Nrec_Date", "User_Name").Select().CopyToDataTable();
            DataGridView[] Export_GRD = { GrdAcc_Close_price };
            DataTable[] Export_DT = { Details_Acc_Close };
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void GrdAcc_Close_price_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(_BS_HSt_Grd);
        }
    }
}