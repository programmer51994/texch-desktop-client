﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Accountant_Close_Main : Form
    {

        string Sql_Text = "";
        bool Chande_Grd = false;
        BindingSource _BS_Term  = new BindingSource();
        BindingSource _BS_Close_Price = new BindingSource();
        BindingSource _BS_ACC_Close_HSt = new BindingSource();
        bool Change_Date = false;
        public Accountant_Close_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            GrdAcc_Close_price.AutoGenerateColumns = false;
            Grd_Close_Hst.AutoGenerateColumns = false;
            Grd_Term.AutoGenerateColumns = false;
            #region Enghlish
            if (connection.Lang_id != 1)
            {
                Cbo_CloseingState.Items.Clear();
                Cbo_CloseingState.Items.Add("Real Work Days");
                Cbo_CloseingState.Items.Add("Day Of Years");
                Grd_Term.Columns["Column9"].DataPropertyName = "ACust_name";
                GrdAcc_Close_price.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "Cur_EName";
                Grd_Close_Hst.Columns["dataGridViewTextBoxColumn4"].DataPropertyName = "Cur_EName";
              

            }
            #endregion

        }
        //---------------------------------------------------------
        private void Acc_Close_Price_Main_Load(object sender, EventArgs e)
        {
            Chande_Grd = false;
            Change_Date = false;    
            TxtClsoe_Date.Visible = false;
            Sql_Text = " Select  Distinct convert(varchar(12) ,Cast(Cast(nrec_date As Varchar(8)) As Date) ,111) As Rec_Date , Nrec_Date "
                       + "from Daily_Opening "
                       + "where Acc_Close_Flag = 0 "
                       + " And R_Close_Flag = 1 "
                       + " order by nrec_date desc ";
            CboClose_Date.DataSource = connection.SqlExec(Sql_Text, "Close_Date_Tbl");
            CboClose_Date.DisplayMember = "Rec_Date";
            CboClose_Date.ValueMember = "Nrec_Date";
            if (connection.SQLDS.Tables["Close_Date_Tbl"].Rows.Count > 0)
            {  
                Change_Date = true;
                CboClose_Date_SelectedIndexChanged(null , null);
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? " لا توجد ايام للاغلاق المحاسبي" : " No Dates for Accountant Closing ", MyGeneral_Lib.LblCap);
                this.Close();
            }

            //Sql_Text = "SELECT 0 as Chk, B.T_ID, ACust_name, ECust_name ,Cast(Cast(Max(Nrec_Date) As Varchar(8)) As Date) As last_Nrec_Date "
            //           + " FROM CUSTOMERS A,TERMINALS B , Daily_Opening C "
            //           + "  where A.CUST_ID = B.CUST_ID  "
            //           + "  And B.T_Id = C.T_Id "
            //           + " And Acc_Close_Flag = 1 "
            //           + "  And B.T_ID in(select T_ID from VOUCHER ) "
            //           + "  And A.Cust_Flag <> 0  "
            //           + " group by B.T_ID, ACust_name, ECust_name "
            //           + " Order By Acust_name ";

            Sql_Text = " Select 0 as chk, ACust_name, ECust_name, B.T_id , convert(varchar(12),Real_Close_Date ,111) as Real_Close_Date ,  convert(varchar(12),acc_Close_Date,111) as acc_Close_Date from "
                       + " (Select Cast(Cast(Max(Nrec_Date) As Varchar(8)) As Date) as Real_Close_Date ,T_Id From Daily_Opening "
                       + " where R_Close_Flag = 1 "
                       + " Group by T_Id ) as b "
                       + " left outer join "
                       + " ( Select Cast(Cast(Max(Nrec_Date) As Varchar(8)) As Date) as acc_Close_Date ,T_Id From Daily_Opening "
                       + " where Acc_Close_Flag = 1 "
                       + " Group by T_Id ) as a "
                       + " on b.T_Id = a.T_Id "
                       + " inner join TERMINALS C "
                       + " on B.T_Id = C.T_ID "
                       + " inner join CUSTOMERS D "
                       + " on C.CUST_ID = D.CUST_ID "
                       + " where  B.T_ID in(select  T_ID from VOUCHER union  select  T_ID from F_T_B) "
                       + " And Cust_Flag <> 0 ";
            _BS_Term.DataSource = connection.SqlExec(Sql_Text, "Term_Tbl");
            Grd_Term.DataSource = _BS_Term;

            //Sql_Text = " SELECT convert(varchar(12),isnull((max(ACC_Close_Date)),getdate()),111) as ACC_Close_Date  "
            //  + " FROM   Daily_Opening ";
            //TxtMaxEnd_Date.Text = connection.SqlExec(Sql_Text, "date_Close_Tbl").Rows[0]["ACC_Close_Date"].ToString();

            Sql_Text = " SELECT 0 as Chk, A.Cur_id, Acc_Close_Price,  convert(varchar(12),Str_Nrec_Date,111) as Str_Nrec_Date, End_Nrec_Date, "
           + " B.Cur_ANAME , B.Cur_ENAME , C.User_Name "
           + " FROM  Acc_Close_Price A , Cur_TBL B , Users C "
           + " where A.Cur_id = B.Cur_ID "
           + " And A.user_id = C.user_id  "
           + " And End_Nrec_Date is null";
            _BS_Close_Price.DataSource = connection.SqlExec(Sql_Text, "ACC_Close_Price_Tbl");
            GrdAcc_Close_price.DataSource = _BS_Close_Price;
            if (connection.SQLDS.Tables["ACC_Close_Price_Tbl"].Rows.Count > 0)
            {
                Chande_Grd = true;
                GrdAcc_Close_price_SelectionChanged(null, null);
            }
            Cbo_CloseingState.SelectedIndex = 0;
           // Cbo_CloseingState_SelectedIndexChanged(null, null);
           

        }
        //---------------------------------------------------------
        private void Acc_Close_Price_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Change_Date = false;
            Chande_Grd = false;
            string[] Tbl = { "ACC_Close_Price_Tbl", "ACC_Close_HSt", "Term_Tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }
        //---------------------------------------------------------
        private void Acc_Close_Price_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Add_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //---------------------------------------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            connection.SQLDS.Tables["ACC_Close_Price_Tbl"].AcceptChanges();
            Acc_Close_Price_Add_Upd AddFrm = new Acc_Close_Price_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Acc_Close_Price_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Acc_Close_Price_Add_Upd AddFrm = new Acc_Close_Price_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Acc_Close_Price_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------
        private void GrdAcc_Close_price_SelectionChanged(object sender, EventArgs e)
        {
          //  LblRec.Text = connection.Records(connection.SQLBSGrd);
            if (Chande_Grd)
            {
                int Cur_ID = Convert.ToInt16(((DataRowView)_BS_Close_Price.Current).Row["Cur_Id"]);
                Sql_Text = " SELECT 0 as Chk, A.Cur_id, Acc_Close_Price,  convert(varchar(12),Str_Nrec_Date,111) as Str_Nrec_Date,convert(varchar(12),End_Nrec_Date,111) as End_Nrec_Date, "
                + " B.Cur_ANAME , B.Cur_ENAME , C.User_Name "
                + " FROM  Acc_Close_Price A , Cur_TBL B , Users C "
                + " where A.Cur_id = B.Cur_ID "
                + " And A.user_id = C.user_id  "
                + " And End_Nrec_Date is not null"
                + " And A.cur_id = " + Cur_ID;
                _BS_ACC_Close_HSt.DataSource = connection.SqlExec(Sql_Text, "ACC_Close_HSt");
                Grd_Close_Hst.DataSource = _BS_ACC_Close_HSt;
            }

        }
        //---------------------------------------------------------
        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            TxtClsoe_Date.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
           
            int Rec_No = (from DataRow row in connection.SQLDS.Tables["Term_Tbl"].Rows
                          where (int)row["chk"] != 0
                          select row).Count();
            if (Rec_No <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "الرجاء اختيار احد الفروع " : "Add Terminals Please ", MyGeneral_Lib.LblCap);
                return;
            }
            string Current_Date = MyGeneral_Lib.DateChecking(TxtIn_Rec_Date.Text);
            if (Convert.ToInt32(TxtClsoe_Date.Text) > Convert.ToInt32(Current_Date))
            {
                MessageBox.Show(connection.Lang_id == 1 ? "تاريخ الاغلاق اعلى من التاريخ الحالي" : "Closing Date Greater Than Current Date", MyGeneral_Lib.LblCap);
                return;
            
            }
            DataTable Dt = new DataTable();
            Dt = connection.SQLDS.Tables["Term_Tbl"].DefaultView.ToTable(false, "T_id", "ACust_name", "ECust_name", "Chk").Select("Chk > 0").CopyToDataTable();
            Dt = Dt.DefaultView.ToTable(false, "T_id", "ACust_name", "ECust_name").Select().CopyToDataTable();

            connection.SqlExec("Accountant_Closing_Chk_Rem " + Convert.ToInt32(TxtClsoe_Date.Text) + "," + Convert.ToInt16(Dt.Rows[0]["T_id"]), "Chk_Rem_tbl");
            if (connection.SQLDS.Tables["Chk_Rem_tbl"].Rows.Count > 0)
            {
                Rem_Count_closing AddFrm = new Rem_Count_closing();
                AddFrm.ShowDialog(this);
                if (Rem_Count_closing.ok_btn == 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تم ايقاف الاغلاق لوجود حوالات معلقة" : "The closure has been suspended because of pending Rem.", MyGeneral_Lib.LblCap);
                    return;
                }

                else
                { MessageBox.Show(connection.Lang_id == 1 ? "سيتم متابعة الاغلاق لتجاهلك الحوالات المعلقة" : "You cancel pending Rem.", MyGeneral_Lib.LblCap); }
            }

            try
            {
                connection.SQLCS.Open();
                connection.SQLCMD.CommandText = "[dbo].[Accountant_Closing]";
                connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                connection.SQLCMD.Connection = connection.SQLCS;
                connection.SQLCMD.CommandTimeout = 0;
                connection.SQLCMD.Parameters.AddWithValue("@Term_Tbl", Dt);
                connection.SQLCMD.Parameters.AddWithValue("@NTo_Date", TxtClsoe_Date.Text);
                connection.SQLCMD.Parameters.AddWithValue("@Closing_State", Cbo_CloseingState.SelectedIndex);
                connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                IDataReader obj = connection.SQLCMD.ExecuteReader();
                connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Accountant_Closing_Tbl");
                obj.Close();
                connection.SQLCS.Close();
                if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                {
                    //if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() == "تمت عملية الاغلاق بنجاح")
                    //{
                    MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                    connection.SQLCMD.Parameters.Clear();
                    this.Close();
                    // }
                }
                connection.SQLCMD.Parameters.Clear();
                connection.SQLCMD.Dispose();
                
            }

            catch (Exception _Err)
            {
                connection.SQLCMD.Parameters.Clear();
                MyErrorHandler.ExceptionHandler(_Err);
            }
            if (connection.SQLDS.Tables["Accountant_Closing_Tbl"].Rows.Count > 0)
            {
                Account_Close_Terminlas AddFrm = new Account_Close_Terminlas();
                this.Visible = false;
                AddFrm.ShowDialog(this);
                //Acc_Close_Price_Main_Load(sender, e);
             //   this.Visible = true;
                this.Close();
            }

            //connection.SQLCMD.Parameters.AddWithValue("@Term_Tbl", Dt);
            //connection.SQLCMD.Parameters.AddWithValue("@NTo_Date", TxtClsoe_Date.Text);
            //connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            //connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            //connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            //connection.SqlExec("Accountant_Closing", connection.SQLCMD);

            //if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            //{
            //    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() == "تمت عملية الاغلاق بنجاح")
            //    {
            //        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
            //        connection.SQLCMD.Parameters.Clear();
            //        this.Close();
            //    }
            //    else
            //    {   MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
            //        connection.SQLCMD.Parameters.Clear();
            //        return;
            //    }
            //}
            //connection.SQLCMD.Parameters.Clear();
            //this.Close();

        }
        //---------------------------------------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cbo_CloseingState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt16(Cbo_CloseingState.SelectedIndex) == 0) //فعلي
            {
                TxtClsoe_Date.Visible = false;
                CboClose_Date.Enabled = true;
               
            }
            else
            {
                TxtClsoe_Date.Visible = true ;
                CboClose_Date.Enabled = false;
                TxtClsoe_Date.Text = connection.SQLDS.Tables["Close_Date_Tbl"].Rows[0]["Nrec_Date"].ToString();
                CboClose_Date.SelectedValue = connection.SQLDS.Tables["Close_Date_Tbl"].Rows[0]["Nrec_Date"].ToString();
            }
        }

        private void CboClose_Date_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Change_Date)
            {
                TxtClsoe_Date.Text = CboClose_Date.SelectedValue.ToString();
            }
        }

        private void Accountant_Close_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Reveal_Acc_Close_Date AddFrm = new Reveal_Acc_Close_Date();
            AddFrm.ShowDialog(this);
        }
    }
}