﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Acc_Close_Price_Main : Form
    {
        BindingSource _BS_Main = new BindingSource();
        public Acc_Close_Price_Main()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            GrdAcc_Close_price.AutoGenerateColumns = false;
            if (connection.Lang_id != 1)
            {
                GrdAcc_Close_price.Columns["Column2"].DataPropertyName = "Cur_EName";
               
            }
        }

        private void Acc_Close_Price_Main_Load(object sender, EventArgs e)
        {
            string Sql_Text = " SELECT 0 as Chk, A.Cur_id, Acc_Close_Price,  convert(varchar(12),Str_Nrec_Date,111) as Str_Nrec_Date, End_Nrec_Date, "
            + " B.Cur_ANAME , B.Cur_ENAME , C.User_Name "
            + " FROM  Acc_Close_Price A , Cur_TBL B , Users C "
            + " where A.Cur_id = B.Cur_ID "
            + " And A.user_id = C.user_id  "
            + " And End_Nrec_Date is null";
            _BS_Main.DataSource = connection.SqlExec(Sql_Text, "ACC_Close_Price_Tbl");
            GrdAcc_Close_price.DataSource = _BS_Main;
           
        }
        //---------------------------------------------------------
        private void Acc_Close_Price_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            string[] Tbl = { "ACC_Close_Price_Tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }
        //---------------------------------------------------------
        private void Acc_Close_Price_Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    Btn_Add_Click(sender, e);
                    break;
                case Keys.Enter:
                    SendKeys.Send("{tab}");
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
        //---------------------------------------------------------
        private void BtnUpd_Click(object sender, EventArgs e)
        {
            connection.SQLDS.Tables["ACC_Close_Price_Tbl"].AcceptChanges();
            //button1_Click(null , null) ;
            //DataTable dt;
            //dt = connection.SQLDS.Tables["ACC_Close_Price_Tbl"].DefaultView.ToTable().Select("Chk > 0 ").CopyToDataTable();
            Acc_Close_Price_Add_Upd AddFrm = new Acc_Close_Price_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Acc_Close_Price_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Acc_Close_Price_Add_Upd AddFrm = new Acc_Close_Price_Add_Upd(1);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Acc_Close_Price_Main_Load(sender, e);
            this.Visible = true;
        }
        //---------------------------------------------------------
        private void GrdAcc_Close_price_SelectionChanged(object sender, EventArgs e)
        {
            //connection.SQLDS.Tables["ACC_Close_Price_Tbl"].AcceptChanges();
            LblRec.Text = connection.Records(_BS_Main);
           
        }
        //---------------------------------------------------------
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            int Cur_ID = Convert.ToInt16(((DataRowView)_BS_Main.Current).Row["Cur_Id"]);
            Hst_Acc_Close_Price AddFrm = new Hst_Acc_Close_Price(Cur_ID);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Acc_Close_Price_Main_Load(sender, e);
            this.Visible = true;

        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {

            //DataTable dt;
            //dt = connection.SQLDS.Tables["ACC_Close_Price_Tbl"].DefaultView.ToTable().Select("Chk > 0 ").CopyToDataTable();
            Acc_Close_Price_Add_Upd AddFrm = new Acc_Close_Price_Add_Upd(2);
            this.Visible = false;
            AddFrm.ShowDialog(this);
            Acc_Close_Price_Main_Load(sender, e);
            this.Visible = true;
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            DataTable Details_Acc_Close = new DataTable();
            Details_Acc_Close = connection.SQLDS.Tables["ACC_Close_Price_Tbl"].DefaultView.ToTable(false, "Cur_id", connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_ENAME", "Acc_Close_Price", "Str_Nrec_Date", "User_Name").Select().CopyToDataTable();
            DataGridView[] Export_GRD = { GrdAcc_Close_price };
            DataTable[] Export_DT = { Details_Acc_Close };
            MyExports.To_ExcelByHtml(Export_DT, Export_GRD, true, true, this.Text);
        }

        private void Acc_Close_Price_Main_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        
        }

        //private void GrdAcc_Close_price_MouseLeave(object sender, EventArgs e)
        //{
        //    if (((DataRowView)_BS_MainCurrent).Row["chk"] != DBNull.Value && Convert.ToInt16(((DataRowView)_BS_Main.Current).Row["chk"]) != 0)
        //    {
        //        DataTable dt;
        //        dt = connection.SQLDS.Tables["ACC_Close_Price_Tbl"].DefaultView.ToTable().Select("Chk > 0 ").CopyToDataTable();
            
        //    }
        //}


        
    }
}
