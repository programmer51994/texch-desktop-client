﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;
using System.IO;

namespace Integration_Accounting_Sys
{
    public partial class Delete_ACC_Close_date : Form
    {

        BindingSource BS_grd_date = new BindingSource();
        string from_date = "";
        DataTable Dt_clos;


        public Delete_ACC_Close_date()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(this, new TextBox(), TxtUser, new TextBox(), TxtIn_Rec_Date, TxtTerm_Name);
            Grd_date.AutoGenerateColumns = false;
            Create_table();   
        }

        private void Delete_ACC_Close_date_Load(object sender, EventArgs e)
        {
            #region EN_AR

            if (connection.Lang_id == 2)
            {
                label1.Text = ":User Name";
                label2.Text = ":Date";
                label5.Text = "Remove the closure be inclusive of all the institution";
                label3.Text = "....Accounting Closing Periods";
                Btn_ok.Text = "OK";
                Btn_Ext.Text = "END";
                dataGridViewCheckBoxColumn1.HeaderText = "Check";
                Column4.HeaderText = "Period From";
                Column6.HeaderText = "Period To";
                Column7.HeaderText = "User Name";
                this.Text = "Removal accounting closing periods";
            }

            #endregion

            string Sqltexe = " select 0 as Check_id ,(Cast(Cast( min(nrec_date) As Varchar(8)) As Date)) as min_nrec_date, min(nrec_date) as min_nrec_date_int ,(Cast(Cast(max(nrec_date) As Varchar(8)) As Date)) as max_nrec_date, max(nrec_date) as max_nrec_date_int, AC_User_Id ,B.User_Name "
                             + " from Daily_Opening A, USERS B "
                             + " where Acc_Close_Flag=1 "
                             + " and A.AC_User_Id = B.USER_ID "
                             + " GROUP BY cast( ACC_Close_Date as date), AC_User_Id,B.User_Name "
                             + " order by min_nrec_date ";


            connection.SqlExec(Sqltexe, "Delete_ACC_Close_Price_tbl");
            if (connection.SQLDS.Tables["Delete_ACC_Close_Price_tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد فترات اغلاق محاسبي" : "There are no accounting closing periods", MyGeneral_Lib.LblCap);
               // this.Close();
            }

            if (connection.SQLDS.Tables["Delete_ACC_Close_Price_tbl"].Rows.Count > 0)
            {    
                Dt_clos = connection.SQLDS.Tables["Delete_ACC_Close_Price_tbl"];
                BS_grd_date.DataSource = Dt_clos;
                Grd_date.DataSource = BS_grd_date;  
            }
        }

        private void Create_table()
        {
            string[] Column = { "check_id", "min_nrec_date", "min_nrec_date_int", "max_nrec_date", "max_nrec_date_int", "User_Name" };

            string[] DType = { "System.Int32", "System.DateTime", "System.Int64", "System.DateTime", "System.Int64", "System.String" };

            Dt_clos = CustomControls.Custom_DataTable("Dt_clos", Column, DType);
        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {    
            this.Close();
        }

        private void Btn_ok_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Delete_ACC_Close_Price_tbl"].Rows.Count > 0)
            {

                try
                {
                    DataTable Dt = new DataTable();
                    Dt = Dt_clos.DefaultView.ToTable(false, "check_id", "min_nrec_date_int").Select("check_id > 0 ").CopyToDataTable();
                    if (Dt.Rows.Count > 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار فترة اغلاق واحدة " : "You must choose one closing period", MyGeneral_Lib.LblCap);

                        foreach (DataGridViewRow row in Grd_date.Rows)
                        {
                            row.Cells[0].Value = 0;
                        }

                        return;
                    }

                    else
                    {   
                        from_date = Dt.Rows[0][1].ToString();
                        connection.SQLCS.Open();
                        connection.SQLCMD.CommandText = "[dbo].[Delete_Account_Closeing]";
                        connection.SQLCMD.CommandType = CommandType.StoredProcedure;
                        connection.SQLCMD.Connection = connection.SQLCS;
                        connection.SQLCMD.Parameters.AddWithValue("@FromNrec", from_date);
                        connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
                        connection.SQLCMD.Parameters.AddWithValue("@Lang_ID", connection.Lang_id);
                        connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                        connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                        IDataReader obj = connection.SQLCMD.ExecuteReader();
                      //connection.SQLDS.Load(obj, LoadOption.PreserveChanges, "Delete_Account_Closeing_tbl");
                        obj.Close();
                        connection.SQLCS.Close();
                        if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                        {

                            MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                            connection.SQLCMD.Parameters.Clear();
                            this.Close();

                        }
                        connection.SQLCMD.Parameters.Clear();
                        connection.SQLCMD.Dispose();
                    }
                }


                catch
                {

                    MessageBox.Show(connection.Lang_id == 1 ? "اختر فترة الاغلاق" : "Choose the closing period", MyGeneral_Lib.LblCap);
                    return;
                }

            }

            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد فترات اغلاق محاسبي" : "There are no accounting closing periods", MyGeneral_Lib.LblCap);
                return;

            }
        }

        private void Delete_ACC_Close_date_FormClosing(object sender, FormClosingEventArgs e)
        {
            string[] Tbl = { "Delete_ACC_Close_Price_tbl", "Dt_clos" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }
    }
}