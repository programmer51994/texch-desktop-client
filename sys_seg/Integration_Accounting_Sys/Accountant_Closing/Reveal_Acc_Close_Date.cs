﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Reveal_Acc_Close_Date : Form
    {

        BindingSource Bs_grd_user_date = new BindingSource();
        BindingSource Bs_grd_cur = new BindingSource();
        bool chang_grd1 = false;
        Int64 from_date = 0;
        Int64 to_date = 0;


        public Reveal_Acc_Close_Date()
        {

   
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
        

            Grd_user_date.AutoGenerateColumns = false;
            Grd_Cur.AutoGenerateColumns = false;
            
            #endregion
        }

        private void Reveal_Acc_Close_Price_Load(object sender, EventArgs e)
        {


            chang_grd1 = false;

            string Sqltexe = " select (Cast(Cast( min(nrec_date) As Varchar(8)) As Date)) as min_nrec_date, min(nrec_date) as min_nrec_date_int ,(Cast(Cast(max(nrec_date) As Varchar(8)) As Date)) as max_nrec_date, max(nrec_date) as max_nrec_date_int, AC_User_Id ,B.User_Name "
                               + " from Daily_Opening A, USERS B "
                               + " where Acc_Close_Flag=1 "
                               + " and A.AC_User_Id = B.USER_ID "
                               + " GROUP BY cast( ACC_Close_Date as date), AC_User_Id,B.User_Name "
                               + " order by min_nrec_date ";



            connection.SqlExec(Sqltexe, "Reveal_ACC_Close_Price_tbl");
            if (connection.SQLDS.Tables["Reveal_ACC_Close_Price_tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد بيانات تحقق الشروط " : "No data For this cases", MyGeneral_Lib.LblCap);
                this.Close();
            }

            if (connection.SQLDS.Tables["Reveal_ACC_Close_Price_tbl"].Rows.Count > 0)
            {
                chang_grd1 = false;
                Bs_grd_user_date.DataSource = connection.SQLDS.Tables["Reveal_ACC_Close_Price_tbl"];
                Grd_user_date.DataSource = Bs_grd_user_date;

                chang_grd1 = true;
                Grd_user_date_SelectionChanged(null, null);
            }
        }

        private void Grd_user_date_SelectionChanged(object sender, EventArgs e)
        {

            if (chang_grd1)
            {
               

                from_date = Convert.ToInt64(((DataRowView)Bs_grd_user_date.Current).Row["min_nrec_date_int"]);
                to_date = Convert.ToInt64(((DataRowView)Bs_grd_user_date.Current).Row["max_nrec_date_int"]);




                string Sqltexe1 = " select  distinct REAL_PRICE,FOR_CUR_ID ,b.Cur_ANAME,B.Cur_ENAME "
                                     + " from voucher A , Cur_Tbl B "
                                     + " where FOR_CUR_ID=B.Cur_ID "
                                     + " and  oper_id=25 "
                                     + " and nrec_date between " + from_date + " and " + to_date
                                     + " order by FOR_CUR_ID ";



                connection.SqlExec(Sqltexe1, "Reveal_ACC_Close_Price_cur_tbl");

                if (connection.SQLDS.Tables["Reveal_ACC_Close_Price_cur_tbl"].Rows.Count > 0)
                {

                    Bs_grd_cur.DataSource = connection.SQLDS.Tables["Reveal_ACC_Close_Price_cur_tbl"];
                    Grd_Cur.DataSource = Bs_grd_cur;
                }
            }

            else { Grd_Cur.DataSource = new BindingSource(); }

        }

        private void Btn_Ext_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void Reveal_Acc_Close_Price_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_grd1 = false;
            string[] Str = { "Reveal_ACC_Close_Price_tbl", "Reveal_ACC_Close_Price_cur_tbl" };
            foreach (string Tbl in Str)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                    connection.SQLDS.Tables.Remove(Tbl);
            }
        }

        private void Btn_Export_Details_Click(object sender, EventArgs e)
        {

            DataTable dt_user_id = connection.SQLDS.Tables["Reveal_ACC_Close_Price_tbl"].DefaultView.ToTable(false,  "min_nrec_date","min_nrec_date_int", "max_nrec_date","max_nrec_date_int","User_Name").Select(" min_nrec_date_int = " + from_date + " and max_nrec_date_int = " + to_date ).CopyToDataTable();
            DataTable dt_user_id_1 = dt_user_id.DefaultView.ToTable(false, "min_nrec_date", "max_nrec_date", "User_Name").Select().CopyToDataTable();
            DataTable dt_cur = connection.SQLDS.Tables["Reveal_ACC_Close_Price_cur_tbl"].DefaultView.ToTable(false, "Cur_ANAME", "Cur_eNAME", "REAL_PRICE").Select().CopyToDataTable();
            DataTable[] _EXP_DT = { dt_user_id_1, dt_cur };
            DataGridView[] Export_GRD = { Grd_user_date, Grd_Cur };
            MyExports.To_ExcelByHtml(_EXP_DT, Export_GRD, true, true, this.Text);
        }
    }
}
