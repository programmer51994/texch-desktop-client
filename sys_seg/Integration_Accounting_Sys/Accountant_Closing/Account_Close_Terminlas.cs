﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{

    public partial class Account_Close_Terminlas : Form
    {
       
        BindingSource _BS_Grd_Close_term = new BindingSource();
        public Account_Close_Terminlas()
        {
            InitializeComponent();
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
             connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            GrdAcc_Close_trem.AutoGenerateColumns = false;
            
        }

        private void Account_Close_Terminlas_Load(object sender, EventArgs e)
        {
            _BS_Grd_Close_term.DataSource = connection.SQLDS.Tables["Accountant_Closing_Tbl"];
            GrdAcc_Close_trem.DataSource = _BS_Grd_Close_term;
        }

        private void GrdAcc_Close_trem_SelectionChanged(object sender, EventArgs e)
        {
          LblRec.Text =   connection.Records(_BS_Grd_Close_term);
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Account_Close_Terminlas_FormClosed(object sender, FormClosedEventArgs e)
        {

            string[] Tbl = { "Accountant_Closing_Tbl" };
            foreach (string Str in Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Str))
                    connection.SQLDS.Tables.Remove(Str);
            }
        }

    }
}