﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using System.Linq;

namespace Integration_Accounting_Sys
{
    public partial class Acc_Close_Price_Add_Upd : Form
    {
        #region Defintion
        String Sql_Text = "";
        DataTable Dt;
        DataTable Dt_close;
        int frm_id;
        BindingSource _BS1 = new BindingSource();
        BindingSource _BS2 = new BindingSource();

        #endregion
        public Acc_Close_Price_Add_Upd(int Form_id)
        {
            InitializeComponent();
            #region MyRegion
            MyGeneral_Lib.Form_Orientation(this);
            Page_Setting.Header_Page(Txt_UsrName, TxtIn_Rec_Date);
            GrdAcc_Close.AutoGenerateColumns = false;
             connection.Control_Cap(this, Convert.ToInt16(this.Tag));

            #region Enghlish
             if (connection.Lang_id != 1)
             {
                 GrdAcc_Close.Columns["dataGridViewTextBoxColumn2"].DataPropertyName = "Cur_EName";

             }
            #endregion
            #endregion
            frm_id = Form_id;
        }

        private void Acc_Close_Price_Add_Upd_Load(object sender, EventArgs e)
        {
            if (frm_id == 1)
            {
                Create_table();
                Sql_Text = " Select Cur_Id,Cur_Aname,Cur_Ename from Cur_Tbl "
                    + " where cur_id not in (select cur_id from Acc_Close_Price Where End_Nrec_Date is null) "
                    + " And cur_id <> 0 "
                    + " And Cur_ID in ( Select for_cur_id from Voucher union Select for_cur_id from F_T_B ) "
                    + " and Cur_id not in ( SELECT Base_Cur_Id FROM  Companies) "
                    + " Order By Cur_Aname ";

                Cbo_Cur_ID.DataSource = connection.SqlExec(Sql_Text, "Cur_Tbl");
                Cbo_Cur_ID.ValueMember = "cur_id";
                Cbo_Cur_ID.DisplayMember = (connection.Lang_id) == 1 ? "Cur_ANAME" : "Cur_ENAME";

                if (connection.SQLDS.Tables["Cur_Tbl"].Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عملات للاضافة" : "There is no currency to be added", MyGeneral_Lib.LblCap);
                    this.Close();
                }

                //Sql_Text = " SELECT convert(varchar(12),isnull((min(Nrec_Date)),getdate()),111) as ACC_Close_Date  "
                //+ " FROM   Daily_Opening ";
                Sql_Text =   " SELECT convert(varchar(12), Cast(Cast(min(Nrec_Date) As Varchar(8)) As Date) ,111) As ACC_Close_Date   FROM   Daily_Opening where Acc_Close_Flag = 0 ";
                TxtStart_Date.Text = connection.SqlExec(Sql_Text, "date_Close_Tbl").Rows[0]["ACC_Close_Date"].ToString();


                //Sql_Text = " SELECT A.Cur_id, Acc_Close_Price, Str_Nrec_Date, End_Nrec_Date, "
                //+ " B.Cur_ANAME , B.Cur_ENAME , C.User_Name "
                //+ " FROM  Acc_Close_Price A , Cur_TBL B , Users C "
                //+ " where A.Cur_id = B.Cur_ID "
                //+ " And A.user_id = C.user_id  "
                //+ " And End_Nrec_Date is null";
                //_BS1.DataSource = connection.SqlExec(Sql_Text, "ACC_Close_Tbl");
                //GrdAcc_Close.DataSource = connection.SQLBS;
                GrdAcc_Close.Columns["Column7"].Visible = false;
                GrdAcc_Close.Columns["Column1"].Visible = false;
                GrdAcc_Close.Columns["Column6"].ReadOnly = true;
            }
            else
            {
                label3.Visible = false;
                label5.Visible = false;
                Cbo_Cur_ID.Visible = false;
                TxtClose_Price.Visible = false;
                Btn_Add.Visible = false;
                BtnDel.Visible = false;
                AddBtn.Visible = false;
                GrdAcc_Close.Columns["Column8"].Visible = false;
                Dt_close = connection.SQLDS.Tables["ACC_Close_Price_Tbl"];
                _BS1.DataSource = Dt_close;
                GrdAcc_Close.DataSource = _BS1;
            }
        }
        private void Create_table()
        {
            string[] Column =
            {
                        "Cur_Id", "Acc_Close_Price", "Str_Nrec_Date", "Cur_Aname" , "User_name"
                       
            };

            string[] DType =
            {
                "System.Int32","System.Decimal","System.DateTime" , "System.String" , "System.String"
                
            };

            Dt_close = CustomControls.Custom_DataTable("Dt_close", Column, DType);
            _BS2.DataSource = Dt_close;
            GrdAcc_Close.DataSource = _BS2;
        }
        //-----------------------------------------------------
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            //string Nrec_Date = MyGeneral_Lib.DateChecking(TxtStart_Date.Text);
            //if (Nrec_Date == "0" || Nrec_Date == "-1")
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ بداية سعر الاغلاق" : "Please make sure of closing date.", MyGeneral_Lib.LblCap);
            //    TxtStart_Date.Focus();
            //    return;
            //}

            //int Rec_No = (from DataRow row in connection.SQLDS.Tables["ACC_Close_Tbl"].Rows
            //              where (Int16)row["Cur_Id"] == Convert.ToInt16(Cbo_Cur_ID.SelectedValue)
            //              select row).Count();
            //if (Rec_No > 0)
            //{
            //    MessageBox.Show(connection.Lang_id == 1 ? "لا يمكن اضافة عملة مكررة" : "Can not add duplicate currency", MyGeneral_Lib.LblCap);
            //    return;
            //}
            if (connection.SQLDS.Tables["Cur_Tbl"].Rows.Count <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عملات للاضافة" : "There are Currencies records", MyGeneral_Lib.LblCap);
                return;

            }

            DataRow DRow = Dt_close.NewRow();
            DRow[0] = Cbo_Cur_ID.SelectedValue;
            DRow[1] = TxtClose_Price.Text;
            DRow[2] = TxtStart_Date.Text.ToString();
            DRow[3] = Cbo_Cur_ID.Text;
            DRow[4] = Txt_UsrName.Text;
            Dt_close.Rows.Add(DRow);

            connection.SQLDS.Tables["Cur_Tbl"].Rows[Cbo_Cur_ID.SelectedIndex].Delete();
            connection.SQLDS.Tables["Cur_Tbl"].AcceptChanges();


            //TxtStart_Date.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;


            //connection.SQLDS.Tables["ACC_Close_Tbl"].Rows.Add(new object[] { Cbo_Cur_ID.SelectedValue, TxtClose_Price.Text, TxtStart_Date.Text.ToString(), null, Cbo_Cur_ID.Text, "", Txt_UsrName.Text });


            //TxtStart_Date.Enabled = false;

            //TxtStart_Date.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            TxtClose_Price.ResetText();
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (Dt_close.Rows.Count > 0)
            {
                string cur_name = connection.Lang_id == 1 ? ((DataRowView)_BS2.Current).Row["Cur_Aname"].ToString() : ((DataRowView)_BS2.Current).Row["Cur_ename"].ToString();
                Int16 cur_id = Convert.ToInt16(((DataRowView)_BS2.Current).Row["Cur_Id"]);

                connection.SQLDS.Tables["Cur_Tbl"].Rows.Add(new object[] { cur_id, cur_name });
                Dt_close.Rows[GrdAcc_Close.CurrentRow.Index].Delete();
            }
            else
            {
                MessageBox.Show(connection.Lang_id == 1 ? "لا توجد عملات للحذف" : "There are NO Currencies to Delete", MyGeneral_Lib.LblCap);
                return;

            }
        }
        //---------------------------------------------------------
        private void Btn_Ok_Click(object sender, EventArgs e)
        {
            if (frm_id == 1)
            {
                if (Dt_close.Rows.Count <= 0)
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "لا توجد قيود للاضافة" : "There are no records", MyGeneral_Lib.LblCap);
                    return;
                }

                // Dt = connection.SQLDS.Tables["ACC_Close_Tbl"].DefaultView.ToTable(false, "Cur_Id", "Acc_Close_Price", "Str_Nrec_Date").Select().CopyToDataTable(); 
                Dt = Dt_close.DefaultView.ToTable(false, "Cur_Id", "Acc_Close_Price", "Str_Nrec_Date").Select().CopyToDataTable();
            }
            else
            {

                //if (Nrec_Date == "0" || Nrec_Date == "-1")
                //{
                //    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ بداية سعر الاغلاق" : "Please make sure of closing date.", MyGeneral_Lib.LblCap);
                //    TxtStart_Date.Focus();
                //    return;
                //}
                //foreach (DataGridViewRow DRow in GrdAcc_Close.Rows)
                //{
                //}
                string Nrec_Date = MyGeneral_Lib.DateChecking(TxtStart_Date.Text);
                if (Nrec_Date == "0" || Nrec_Date == "-1")
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "تأكد من تاريخ اعتبار من" : "Please make sure of closing date.", MyGeneral_Lib.LblCap);
                    TxtStart_Date.Focus();
                    return;
                }
                foreach (DataRow row in connection.SQLDS.Tables["ACC_Close_Price_Tbl"].Rows)
                {
                    Int16 Chk = 0;
                    Chk = Convert.ToInt16(row["Chk"] == DBNull.Value ? 0 : 1);
                    string Nrec_Date1 = MyGeneral_Lib.DateChecking(row["Str_Nrec_Date"].ToString());

                    if (Convert.ToInt32(Nrec_Date) < Convert.ToInt32(Nrec_Date1) && Chk == 1)
                    {
                        MessageBox.Show(connection.Lang_id == 1 ? "تاريخ سعر الغلق السابق اعلى من التاريخ الحالي للعملة " + row["Cur_aname"] : "Check The Date Please ", MyGeneral_Lib.LblCap);
                        return;
                    }

                }
                try
                {
                    Dt = Dt_close.DefaultView.ToTable(false, "Cur_Id", "Acc_Close_Price", "Str_Nrec_Date", "chk").Select("chk >0").CopyToDataTable();
                    Dt = Dt.DefaultView.ToTable(false, "Cur_Id", "Acc_Close_Price", "Str_Nrec_Date").Select().CopyToDataTable();
                }
                catch
                {
                    MessageBox.Show(connection.Lang_id == 1 ? "الرجاء تحديد العملات" : "Please select the currencies", MyGeneral_Lib.LblCap);
                    return;
                }
            }


            //TxtStart_Date.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals; 
            connection.SQLCMD.Parameters.AddWithValue("@Acc_Close_Price_Type", Dt);
            connection.SQLCMD.Parameters.AddWithValue("@Str_Nrec_Date", TxtStart_Date.Text);
            connection.SQLCMD.Parameters.AddWithValue("@User_Id", connection.user_id);
            connection.SQLCMD.Parameters.AddWithValue("@frm_Id", frm_id);
            connection.SQLCMD.Parameters.AddWithValue("@Lang_id", connection.Lang_id);
            connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
            connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
            connection.SqlExec("Add_Upd_Acc_Close_Price", connection.SQLCMD);

            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            this.Close();
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GrdAcc_Close_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records(connection.SQLBSGrd);
        }
    }
}