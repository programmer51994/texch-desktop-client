﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    class NGRD:DataGridView
    {
        protected override void OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        {
            base.OnEditingControlShowing(e);
            if (e.CellStyle.Format.StartsWith("N"))
            {
                TextBox Grdtx = e.Control as TextBox;
                Grdtx.KeyPress += new KeyPressEventHandler(Grdtx_KeyPress);
            }
        }
        //-------------------------------
        /// <summary>
        /// This Method used to handle and delete any char not numeric
        /// </summary>
        /// <param name="sender"> DGV textbox that write on it </param>
        /// <param name="e"> KeyPressEventArgs DataType if char not numeric set e.Handled to true to handle and delete theise char</param>
        void Grdtx_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == (char)8))
            {
                e.Handled = true;
            }
        }
    }
}
