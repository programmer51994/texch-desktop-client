﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms; 

namespace Integration_Accounting_Sys
{
    public partial class SearchTextBoxBND : ToolStripTextBox 
    {
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
           if (e.KeyChar == '\'' || e.KeyChar == '-' || e.KeyChar == '+' || e.KeyChar == '.')
            {
                e.Handled = true;

            }
        } 

    }
}
