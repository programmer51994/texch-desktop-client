﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Integration_Accounting_Sys
{
    public class NumericTextBox : TextBox
    {
        bool allowSpace = false;
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            else  if (!Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            
         else   if (e.KeyChar != (char)8 && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        } 
        //public int IntValue
        //{
        //    get
        //    {
        //        return Int32.Parse(this.Text);
        //    }
        //}

        //public decimal DecimalValue
        //{
        //    get
        //    {
        //        return Decimal.Parse(this.Text);
        //    }
        //}

        public bool AllowSpace
        {
            set
            {
                this.allowSpace = value;
            }

            get
            {
                return this.allowSpace;
            }
        }
    }

}