﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
  public  class MyDateTextBox : MaskedTextBox
    {
      private static char _Dategroup = '/';
        /// <summary>
        /// Set the initial params.
        /// </summary>
      public MyDateTextBox()
      {
          base.Text = "00000000";
          base.TextAlign = HorizontalAlignment.Left;
          base.Mask = "0000/00/00";
          base.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
          base.PromptChar = ' ';
      }
        //----------------------------------
      public  char DateSeperator
        {
            get
            {
                return _Dategroup;
            }
            set
            {
                if (value != _Dategroup)
                {
                _Dategroup = value;// Convert.ToChar(Text.Substring(4, 1));
                }
            }
        }
        //-----------------------------------
        /// <summary>
        /// Set the SelectionStart
        /// </summary>
        /// <param name="e"></param>
        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (this.Visible)
            { 
                this.SelectionStart = 0;
                this.SelectionLength = 0;
            }
        }
        //-------------------------------
        /// <summary>
        /// Call MyGeneral_Lib.DateChecking(this.Text) to check and reformat date string
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            string Result = string.Empty;
            Result = MyGeneral_Lib.DateChecking(this.Text);
            switch (Result)
            {
                case "-1":
                    this.Text = this.Text;
                    break;
                case "0":
                    this.Text = this.Text;
                    break;
                default:
                    this.Text = Result;
                    break;

            }
        }
        //----------------------------------
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{tab}");
                return;
            }
        }
        //-----------------------------------
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            //base.OnKeyPress(e);
            e.Handled = true;
           
            if (!char.IsDigit(e.KeyChar) )//&& e.KeyChar != DateSeperator)
            {
                return;
            }
            int Pos = this.SelectionStart;
            StringBuilder txt = new StringBuilder(this.Text);

            if (Pos == txt.Length) // Pos at the end of the TextBox
            {
                //SendKeys.Send("{tab}");
                // input ignored.
                return;
            }
            int originalTextLength = txt.Length;
            if (txt[Pos] == DateSeperator)
            {
                Pos++;
                this.SelectionStart = Pos;
                txt.Remove(Pos, 1);
                txt.Insert(Pos, e.KeyChar);
                this.Text = txt.ToString();
                Pos++;
                this.SelectionStart = Pos;// +(txt.Length - originalTextLength);
                return;
            }

            // Insert value.
            txt.Remove(Pos, 1);
            txt.Insert(Pos, e.KeyChar);
            this.Text = txt.ToString();
            Pos++;
            this.SelectionStart = Pos;// +(txt.Length - originalTextLength);
        }
    }
}