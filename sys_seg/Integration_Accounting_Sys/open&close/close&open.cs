﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Branch_AccountingSysytem
{

    public partial class R_closeing_main : Form
    {
        string Sql_Text = "";
        bool chang_cur = false;
        DataTable MAIN_division;
        int Mdiv_qty = 0;
        decimal Mdivision = 0;
        int MCRow = 0;
        DataTable _Dt;
        public R_closeing_main()
        {
            InitializeComponent();
            TextBox txt = new TextBox();
            TextBox txt_cur = new TextBox();
            Page_Setting.Header_Page(this, txt_cur, TxtUser, txt, TxtIn_Rec_Date, TxtTerm_Name);
        }

        private void R_closeing_main_Load(object sender, EventArgs e)
        {
            chang_cur = false;
            Create_Table();

            //---------------تجيك تاريخ الاغلاق

            Sql_Text = " select isnull((SELECT MAX(Nrec_date) 	From Daily_Opening  "
                     + " where t_id = " + connection.T_ID
                     + " and R_Close_Flag = 0  and R_Close_Date is null "
                     + " and ( SELECT MAX(Nrec_date) From Daily_Opening ) <=  " + TxtIn_Rec_Date.Text + "),0) as Nrec_date ";

            connection.SqlExec(Sql_Text, "Daily_time");
            if (Convert.ToInt32(connection.SQLDS.Tables["Daily_time"].Rows[0]["Nrec_date"]) == 0)
            {
                MessageBox.Show("لا توجد ارصدة للاغلاق", MyGeneral_Lib.LblCap);
                this.Close();
                
            }
            ///--------------------------------------

            object[] Sparam = { connection.T_ID, TxtIn_Rec_Date.Text, "" };
            connection.SqlExec("main_close_branch_auto", "balance_main_box", Sparam);
            string X = connection.Col_Name;
            if (X != "")
            {
                MessageBox.Show(X, MyGeneral_Lib.LblCap);
                // return;
            }

            ////------------------------جلب العملات الاغلاق المستخدمة للتاريخ اليوم الاغلاق
            Sql_Text = " SELECT  Cur_ID, Cur_ANAME, Cur_ENAME "
                     + " from Cur_Tbl "
                     + " where cur_id in (select for_cur_id from voucher where nrec_date = " + connection.SQLDS.Tables["Daily_time"].Rows[0]["Nrec_date"] + ")";
            Cbocur_Id.DataSource = connection.SqlExec(Sql_Text, "Daily_cur");
            Cbocur_Id.DisplayMember = connection.Lang_id == 1 ? "Cur_ANAME" : "Cur_eNAME";
            Cbocur_Id.ValueMember = "cur_id";
            if (connection.SQLDS.Tables["Daily_cur"].Rows.Count > 0)
            {
                chang_cur = true;
                Cbocur_Id_SelectedIndexChanged(null, null);
            }
        }
        ////----------------------- انشاء الجداول
        private void Create_Table()
        {
            string[] Column =
            {
               
            "nrec_date","for_cur_id", "division","div_qty","t_id","notes",  "user_id",
         
            };

            string[] DType =
            {
                "System.Int32", "System.Int32" ,"System.Decimal","System.Decimal","System.Int32","System.String","System.Int32"

            };

            MAIN_division = CustomControls.Custom_DataTable("MAIN_division", Column, DType);
        }
        ///-------------------------------



        private void Cbocur_Id_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (chang_cur && Convert.ToInt32(Cbocur_Id.SelectedValue) > 0)
            {

                Sql_Text = " select division,0 as div_qty,0.000 as amount ,'' as notes ,cur_id "
                        + " from currency_division where cur_id = " + Cbocur_Id.SelectedValue;
                Grddivision_Cur.DataSource = connection.SqlExec(Sql_Text, "division_Cur");
            }
        }

        private void Grddivision_Cur_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Txtsum_Amount.ResetText();
                MCRow = Grddivision_Cur.CurrentRow.Index;
                _Dt = connection.SQLDS.Tables["division_Cur"];
                Mdiv_qty = Convert.ToInt32(_Dt.Rows[MCRow]["div_qty"]);
                Mdivision = Convert.ToDecimal(_Dt.Rows[MCRow]["division"]);
                _Dt.Rows[MCRow].SetField("amount", Mdiv_qty * Mdivision);
                Txtsum_Amount.Text = _Dt.Compute("Sum(amount)", "").ToString();
                Txtfor_Cur.Text = Cbocur_Id.Text;

            }
            catch { }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {

            DataTable balance = connection.SQLDS.Tables["balance_main_box"].DefaultView.ToTable(true, "tfor_amount", "for_cur_id").Select(" for_cur_id = " + Cbocur_Id.SelectedValue).CopyToDataTable();
            decimal balance1 = Convert.ToDecimal(balance.Rows[0]["tfor_amount"]);
            if (balance1 == Convert.ToDecimal(Txtsum_Amount.Text))
            {
                foreach (DataRow Dr1 in _Dt.DefaultView.ToTable().Rows)
                {
                    if (Convert.ToInt32(Dr1["div_qty"]) > 0 && Convert.ToDecimal(Dr1["amount"]) > 0)
                    {
                        DataRow Dr = MAIN_division.NewRow();
                        Dr["div_qty"] = Convert.ToDecimal(Dr1["div_qty"]);
                        //Dr["amount"] = Convert.ToDecimal(Dr1["amount"]);
                        Dr["notes"] = Dr1["notes"].ToString();
                        Dr["division"] = Convert.ToDecimal(Dr1["division"]);
                        Dr["for_cur_id"] = Convert.ToInt32(Dr1["cur_id"]);
                        Dr["nrec_date"] = TxtIn_Rec_Date.Text;
                        Dr["user_id"] = connection.user_id;
                        Dr["t_id"] = connection.T_ID;
                        MAIN_division.Rows.Add(Dr);


                    }
                }

                connection.SQLDS.Tables["Daily_cur"].Rows.RemoveAt(Cbocur_Id.SelectedIndex);
                if (connection.SQLDS.Tables["Daily_cur"].Rows.Count == 0)
                {

                    connection.SQLCMD.Parameters.AddWithValue("@currency_division", MAIN_division);
                    connection.SQLCMD.Parameters.Add("@Param_Result", SqlDbType.VarChar, 200).Value = "";
                    connection.SQLCMD.Parameters["@Param_Result"].Direction = ParameterDirection.Output;
                    connection.SqlExec("add_currency_divison", connection.SQLCMD);
                    

                    if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
                    {
                        MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString(), MyGeneral_Lib.LblCap);
                        connection.SQLCMD.Parameters.Clear();
                        return;
                    }
                    connection.SQLCMD.Parameters.Clear();
                    this.Visible = false;
                    Daily_Opening_Main AddFrm = new Daily_Opening_Main(false);
                    AddFrm.ShowDialog();
                    this.Close();
                }
                else
                {
                    chang_cur = true;
                    Cbocur_Id_SelectedIndexChanged(null, null);
                    Txtsum_Amount.ResetText();
                    Txtfor_Cur.Text = Cbocur_Id.Text;
                }

            }
            else
            {
                MessageBox.Show("الرصيد الفعلي غير مطابق", MyGeneral_Lib.LblCap);
                return;
            }
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            if (connection.SQLDS.Tables["Daily_cur"].Rows.Count > 0 && Convert.ToInt32(Cbocur_Id.SelectedValue) > 0)
            {
                object[] Sparam = { connection.T_ID, TxtIn_Rec_Date.Text };
                connection.SqlExec("active_user", "aa", Sparam);
                this.Close();
            }
            else
            {
                this.Close();
            }
        }

        private void R_closeing_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            chang_cur = false;
            string[] Used_Tbl = { "aa", "balance_main_box", "Daily_cur", "division_Cur", "Daily_time" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }
    }
}
