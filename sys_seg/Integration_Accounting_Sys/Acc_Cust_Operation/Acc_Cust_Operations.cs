﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace Integration_Accounting_Sys
{
    public partial class Acc_Cust_Operations : Form
    {
        #region MyRegion
        string SqlTxt = "";
        bool CboChange = false;
        bool GrdChange = false;
        BindingSource _Bs1 = new BindingSource();
        BindingSource _Bs2 = new BindingSource();
        BindingSource _Bs3 = new BindingSource();
        #endregion

        public Acc_Cust_Operations()
        {
            InitializeComponent();

            MyGeneral_Lib.Form_Orientation(this);
            connection.Control_Cap(this, Convert.ToInt16(this.Tag));
            Page_Setting.Header_Page(TxtUser, TxtIn_Rec_Date);
            Grd_User.AutoGenerateColumns = false;
            GrdAcc_Name.AutoGenerateColumns = false;
            Grd_Cust_Name.AutoGenerateColumns = false;
            
            if (connection.Lang_id != 1)
            {
                Column2.DataPropertyName = "ECust_name";
                Column6.DataPropertyName = "Acc_Ename";
                Column8.DataPropertyName = "Acc_Ename";
                Column10.DataPropertyName = "ECust_name";
                Column5.DataPropertyName = "Edescription";
            }
        }
        //------------------------
        private void Acc_Cust_Operations_Load(object sender, EventArgs e)
        {
            CboChange = false;
            SqlTxt = "Select OPER_ID,AOPER_NAME,EOPER_NAME From OPERATIONS order by AOPER_NAME ";
            CboOper_id.DataSource = connection.SqlExec(SqlTxt, "Oper_Tbl");
            CboOper_id.DisplayMember = connection.Lang_id == 1?"AOPER_NAME":"EOper_Name";
            CboOper_id.ValueMember = "OPER_ID";

            if (connection.SQLDS.Tables["Oper_Tbl"].Rows.Count > 0)
            {
                CboChange = true;
                CboOper_id_SelectedIndexChanged(sender, e);
            }
        }
        //------------------------
        private void CboOper_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboChange)
            {
                GrdChange = false;
                GrdAcc_Name.DataSource = new BindingSource();
                Grd_Cust_Name.DataSource = new BindingSource();
                SqlTxt = " Select Seq_Id,Acc_AName,Acc_EName,ACUST_NAME,ECUST_NAME, "
                        + " Adescription,Edescription,Idescription,Acc_Description,A.Acc_Id,A.Cust_Id,Cast(Cust_Req_Flag as Tinyint) as Cust_Req_Flag"
                        + " From Acc_Cust_Oper A  "
                            + " Left Outer Join ACCOUNT_TREE B "
                            + " 	On A.Acc_Id = B.Acc_Id "
                            + " Left Outer Join CUSTOMERS C "
                            + " 	On A.Cust_Id = C.CUST_ID "
                            + " Where Oper_Id = " + CboOper_id.SelectedValue;
                _Bs2.DataSource = connection.SqlExec(SqlTxt, "Acc_Cust_Oper_Tbl");
                Grd_User.DataSource = _Bs2;
                if (connection.SQLDS.Tables["Acc_Cust_Oper_Tbl"].Rows.Count > 0)
                {
                    GrdChange = true;
                    TxtAcc_Name_TextChanged(sender, e);
                    TxtCust_Name_TextChanged(sender, e);
                }
            }
        }
        //------------------------
        private void TxtAcc_Name_TextChanged(object sender, EventArgs e)
        {
            if (GrdChange)
            {
                SqlTxt = "Select Acc_Id,Acc_Aname,Acc_Ename From Account_Tree "
                + " Where Record_Flag = 1 "
                + " And (Acc_Aname Like '%" + TxtAcc_Name.Text.Trim() + "%' or Acc_Ename Like '%" + TxtAcc_Name.Text.Trim() + "%'  Or Acc_Id Like '%" + TxtAcc_Name.Text.Trim() + "%' Or Ref_Acc_No Like '%" + TxtAcc_Name.Text.Trim() + "%')";
                _Bs3.DataSource = connection.SqlExec(SqlTxt, "AccountTree_Tbl");
                if (connection.SQLDS.Tables["AccountTree_Tbl"].Rows.Count > 0)
                {
                    GrdAcc_Name.DataSource = _Bs3;
                }
            }
            else
            {
                GrdAcc_Name.DataSource = new BindingSource();
            }
        }
        //------------------------
        private void TxtCust_Name_TextChanged(object sender, EventArgs e)
        {

            if (GrdChange)
            {
                SqlTxt = "Select Cust_Id,ACust_name,ECust_name From Customers Where Cust_Flag = 1 "
                            + " And ( ACust_name Like '%" + TxtCust_Name.Text.Trim() + "%' or Ecust_name Like '%" + TxtCust_Name.Text.Trim() + "%' Or Cust_Id Like '%" + TxtCust_Name.Text.Trim() + "%')"
                           //+ " Union "
                            //+ " Select 0 as Cust_Id,'' as ACust_name,'' as ECust_name "
                            + " Order By Cust_Id, ACust_name";
                _Bs1.DataSource = connection.SqlExec(SqlTxt, "CustomersAcc_Tbl");
                if (connection.SQLDS.Tables["CustomersAcc_Tbl"].Rows.Count > 0)
                {
                    Grd_Cust_Name.DataSource = _Bs1;
                }
            }
            else
            {
                Grd_Cust_Name.DataSource = new BindingSource();
            }
        }
        //------------------------
        private void AddBtn_Click(object sender, EventArgs e)
        {
            #region MyRegion
            if (Convert.ToInt16(CboOper_id.SelectedValue) <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار العملية" : "Please select operation", MyGeneral_Lib.LblCap);
                CboOper_id.Focus();
                return;
            }

            if (Grd_User.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الحساب المطلوب تعديله" : "Please select account for editing", MyGeneral_Lib.LblCap);
                Grd_User.Focus();
                return;
            }

            if (GrdAcc_Name.RowCount <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الحساب الصحيح" : "Please select the correct account ", MyGeneral_Lib.LblCap);
                GrdAcc_Name.Focus();
                return;
            }
            #endregion
            DataRowView Drv = _Bs3.Current as DataRowView;
            DataRow Dr = Drv.Row;

            DataRowView Drv1 = _Bs1.Current as DataRowView;
            DataRow Dr1 = Drv1.Row;

            DataRowView DRV2 = _Bs2.Current as DataRowView;
            DataRow DR2 = DRV2.Row;

            if (DR2.Field<byte>("Cust_Req_Flag") > 0 && Dr1.Field<int>("Cust_Id") <= 0)
            {
                MessageBox.Show(connection.Lang_id == 1 ? "يجب اختيار الثانوي" : "Please select customer", MyGeneral_Lib.LblCap);
                Grd_Cust_Name.Focus();
                return;
            }
            ArrayList ItemList = new ArrayList();
            ItemList.Insert(0, Dr.Field<int>("Acc_Id"));
            ItemList.Insert(1, Dr1.Field<int>("Cust_Id"));
            ItemList.Insert(2, CboOper_id.SelectedValue);
            ItemList.Insert(3, DR2.Field<Byte>("Seq_Id"));
            ItemList.Insert(4, connection.user_id);
            //ItemList.Insert(5, Acc_AName);
            //ItemList.Insert(6, Acc_EName);
            //ItemList.Insert(7, ACust_Name);
            //ItemList.Insert(8, ECust_Name);
            ItemList.Insert(5, "");
            connection.scalar("Edid_Acc_Cust_Oper", ItemList);
            if (connection.SQLCMD.Parameters["@Param_Result"].Value.ToString() != "")
            {
                MessageBox.Show(connection.SQLCMD.Parameters["@Param_Result"].Value.ToString());
                connection.SQLCMD.Parameters.Clear();
                return;
            }
            connection.SQLCMD.Parameters.Clear();
            CboOper_id_SelectedIndexChanged(sender, e);
        }
        //------------------------
        private void Grd_User_SelectionChanged(object sender, EventArgs e)
        {
            LblRec.Text = connection.Records();
        }
        //------------------------
        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //------------------------
        private void Acc_Cust_Operations_FormClosed(object sender, FormClosedEventArgs e)
        {
             CboChange = false;
             GrdChange = false;
            
            string[] Used_Tbl = { "CustomersAcc_Tbl", "AccountTree_Tbl", "Acc_Cust_Oper_Tbl", "Oper_Tbl" };
            foreach (string Tbl in Used_Tbl)
            {
                if (connection.SQLDS.Tables.Contains(Tbl))
                {
                    connection.SQLDS.Tables.Remove(Tbl);
                }
            }
        }

        private void Acc_Cust_Operations_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.page_no = 1;
            Help_Desc.help_description EmpFrm = new Help_Desc.help_description(connection.page_no);
            EmpFrm.ShowDialog(this);
        }

      
        //------------------------
    }
}